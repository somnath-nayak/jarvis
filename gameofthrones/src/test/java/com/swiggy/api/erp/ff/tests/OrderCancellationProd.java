package com.swiggy.api.erp.ff.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.delivery.dp.EndtoEndDP;
import com.swiggy.api.erp.ff.constants.OMSConstants;
import com.swiggy.api.erp.ff.helper.OMSHelper;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Map;

public class OrderCancellationProd extends EndtoEndDP implements OMSConstants {

    OMSHelper omsHelper= new OMSHelper();

    @Test(dataProvider = "orderCancelRestId")
    public void ordercancellation(String restId)
    {
        try {
            List<Map<String, Object>> orderDetailsList = SystemConfigProvider.getTemplate(OMSConstants.SERVICE).queryForList(getOrdersNotInTerminalStateByRestId(restId));
            for (int i = 0; i < orderDetailsList.size(); i++) {
                String order = orderDetailsList.get(i).get("order_id").toString();
                System.out.println(order);
                String cancel=omsHelper.OrderCancellationInProd("65","151", "0", "false",order).ResponseValidator.GetBodyAsText();
                String cancellationStatus= JsonPath.read(cancel, "$.data.cancellation_status.cancellation_initiated").toString().replace("[","").replace("]","");
                Assert.assertEquals(cancellationStatus,"true", "cancellation status is false");
                String message= JsonPath.read(cancel, "$.response.message").toString().replace("[","").replace("]","");
                Assert.assertEquals(message,"Success", "cancellation failure");
            }
        }
        catch(Exception e){
            System.out.println("All orders related to this restaurant id "+restId+" are in delivered or cancelled state");

        }

    }
}
