package com.swiggy.api.erp.delivery.helper;

import com.rabbitmq.client.AMQP;
import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.erp.ff.constants.LosConstants;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.RabbitMQHelper;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.HashMap;

public class DeliveryControllerHelper {

	Initialize gameofthrones = new Initialize();

	DeliveryHelperMethods del=new DeliveryHelperMethods();
	RabbitMQHelper rmqHelper = new RabbitMQHelper();
	


	public Processor partnermappingupdatezone(String zone_id,String partner_id, String partner_zone_map_id)
			throws InterruptedException {

		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("content-type", "application/json");

		GameOfThronesService service = new GameOfThronesService(
				"deliverycontroller", "partnerupdatezone", gameofthrones);
		String[] payloadparams = new String[] { zone_id, partner_id,
				partner_zone_map_id };
		Processor processor = new Processor(service, requestheaders,
				payloadparams);
		return processor;
	}

	public Processor partnermappingupdatecity(String city_id,
			String partner_id, String partner_city_map_id,
			String max_active_orders, String city_helpline)
			throws InterruptedException {
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("content-type", "application/json");

		GameOfThronesService service = new GameOfThronesService(
				"deliverycontroller", "partnerupdatecity", gameofthrones);
		String[] payloadparams = new String[] { city_id, partner_id,
				partner_city_map_id, max_active_orders, city_helpline };
		Processor processor = new Processor(service, requestheaders,
				payloadparams);
		return processor;
	}

	
	public Processor partneraddcity(String city_id,String partner_id,String partner_city_map_id,String max_active_orders,String city_helpline) throws InterruptedException
	{
		 HashMap<String, String> requestheaders = del.doubleheader();


        GameOfThronesService service = new GameOfThronesService("deliverycontroller", "partneraddcity", gameofthrones);
        String[] payloadparams = new String[] {city_id,partner_id,partner_city_map_id,max_active_orders,city_helpline};
        Processor processor = new Processor(service, requestheaders, payloadparams);
		return processor;
	}
	
		
	public Processor partneraddzone(String zone_id, String partner_id, String partner_zone_map_id)
		{
		 HashMap<String, String> requestheaders = del.doubleheader();
	    GameOfThronesService service = new GameOfThronesService("deliverycontroller", "partneraddzone", gameofthrones);
	      
			String[] payloadparams = new String[]{zone_id,partner_id,partner_zone_map_id};
							
				 Processor processor = new Processor(service, requestheaders, payloadparams);
					return processor;
		}
	public Processor partnerdeletecity(String partner_city_map_id)
	{
		 HashMap<String, String> requestheaders = del.doubleheader();
    
    GameOfThronesService service = new GameOfThronesService("deliverycontroller", "partnerdeletecity", gameofthrones);
   
		String[] payloadparams = new String[] {partner_city_map_id};
			
			
		
			 Processor processor = new Processor(service, requestheaders, payloadparams);
				return processor;
	}
	public Processor partnerdeletezone( String partner_zone_map_id)
	{

	HashMap<String, String> requestheaders = del.doubleheader();
    GameOfThronesService service = new GameOfThronesService("deliverycontroller", "partnerdeletezone", gameofthrones);
      
		String[] payloadparams = new String[] {partner_zone_map_id};
		
				
			 Processor processor = new Processor(service, requestheaders, payloadparams);
				return processor;
	}
	public Processor orderConfirmedFromController(String order_id)
	{

	HashMap<String, String> requestheaders = del.doubleheader();
    GameOfThronesService service = new GameOfThronesService("deliverycontroller", "controllerorderconfirm", gameofthrones);
      
		String[] payloadparams = new String[] { order_id };
			Processor processor = new Processor(service, requestheaders, payloadparams);
			return processor;
	}
	public Processor orderArrivedFromController(String order_id)
	{

	HashMap<String, String> requestheaders = del.doubleheader();
    GameOfThronesService service = new GameOfThronesService("deliverycontroller", "controllerorderarrived", gameofthrones);
      
		String[] payloadparams = new String[] { order_id };
			Processor processor = new Processor(service, requestheaders, payloadparams);
			return processor;
			
		
	}
	public Processor orderPickedupFromController(String order_id)
	{

	HashMap<String, String> requestheaders = del.doubleheader();
    GameOfThronesService service = new GameOfThronesService("deliverycontroller", "controllerorderpickedup", gameofthrones);
      
		String[] payloadparams = new String[] { order_id };
			Processor processor = new Processor(service, requestheaders, payloadparams);
			return processor;
			
	}
	
	public Processor orderReachedFromController( String order_id)
	{

	HashMap<String, String> requestheaders = del.doubleheader();
    GameOfThronesService service = new GameOfThronesService("deliverycontroller", "controllerorderreached", gameofthrones);
      
		String[] payloadparams = new String[] { order_id };
			Processor processor = new Processor(service, requestheaders, payloadparams);
			return processor;
			
	}
	public Processor orderDeliveredFromController( String order_id)
	{

	HashMap<String, String> requestheaders = del.doubleheader();
    GameOfThronesService service = new GameOfThronesService("deliverycontroller", "controllerorderdelivered", gameofthrones);
      
		String[] payloadparams = new String[] { order_id };
			Processor processor = new Processor(service, requestheaders, payloadparams);
			return processor;
			
}
	public Processor reasssignFromFF(String order_id,String city_id)
	{

	HashMap<String, String> requestheaders = del.doubleheader();
    GameOfThronesService service = new GameOfThronesService("deliverycontroller", "ReAssign", gameofthrones);
      
		String[] payloadparams = new String[] {order_id,city_id};
			Processor processor = new Processor(service, requestheaders, payloadparams);
			return processor;
			
}


	public String updateAndSelectSLA(String sla_time,String order_id)
	{
		String query1 = "update trips set sla="+sla_time+" where order_id='"+order_id+"'";
		SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(query1);
		String query2 = "select sla from trips where order_id='"+order_id+"'";
		return SystemConfigProvider.getTemplate(DeliveryConstant.dbname).queryForMap(query2).get("sla").toString();
	}


	public String updateAndSelectMaxItems(String item_count,String order_id)
	{
		String query1 = "update trips set item_count="+item_count+" where order_id='"+order_id+"'";
		SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(query1);
		String query2 = "select item_count from trips where order_id='"+order_id+"'";
		return SystemConfigProvider.getTemplate(DeliveryConstant.dbname).queryForMap(query2).get("item_count").toString();
	}

    public String updateAndSelectMaxCustEdge(String max_edge,String order_id,String zone_id)
	{
		String queryToUpdateOrder = "update zone inner join trips on trips.zone_id="+zone_id+ " set max_cust_edge="+max_edge+" where order_id='"+order_id+"'";
		SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(queryToUpdateOrder);
		String queryToSelect = "select max_cust_edge from trips inner join zone on trips.zone_id = zone.id where order_id='"+order_id+"'";
		return SystemConfigProvider.getTemplate(DeliveryConstant.dbname).queryForMap(queryToSelect).get("max_cust_edge").toString();
	}

	public void waitTill(int sec)
	{
		try {
			Thread.sleep(sec * 1000);
		}
		catch (Exception e)
		{
			System.out.println("Unable to wait... "+e.getMessage());
		}
	}

}
