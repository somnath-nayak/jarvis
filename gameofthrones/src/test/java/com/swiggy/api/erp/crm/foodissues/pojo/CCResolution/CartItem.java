package com.swiggy.api.erp.crm.foodissues.pojo.CCResolution;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"menuItemId",
"quantity"
})
public class CartItem {

@JsonProperty("menuItemId")
private String menuItemId;
@JsonProperty("quantity")
private Integer quantity;

@JsonProperty("menuItemId")
public String getMenuItemId() {
return menuItemId;
}

@JsonProperty("menuItemId")
public void setMenuItemId(String menuItemId) {
this.menuItemId = menuItemId;
}

@JsonProperty("quantity")
public Integer getQuantity() {
return quantity;
}

@JsonProperty("quantity")
public void setQuantity(Integer quantity) {
this.quantity = quantity;
}

@Override
public String toString(){
    return getMenuItemId() + ", "+getQuantity();
}


public CartItem(String menuItemId, Integer quantity) {
        this.menuItemId = menuItemId;
        this.quantity = quantity;
    }
}