package com.swiggy.api.erp.delivery.tests;

import com.swiggy.api.erp.delivery.helper.DeliveryDataHelper;

import framework.gameofthrones.Aegon.SystemConfigProvider;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.erp.delivery.constants.RTSConstants;
import com.swiggy.api.erp.delivery.dp.AutoAssignmentDataProvider;
import com.swiggy.api.erp.delivery.helper.AutoassignHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryHelperMethods;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceHelper;
import com.swiggy.api.erp.delivery.helper.RTSHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.Processor;

public class AutoAssignmentTest extends AutoAssignmentDataProvider {
	//static String header = null, auth, auth1, cycleauth, auth2;
	DeliveryServiceHelper deliveryServiceHelper = new DeliveryServiceHelper();
	Initialize init = new Initialize();
	AutoassignHelper autoassignHelper = new AutoassignHelper();
	DeliveryHelperMethods deliveryHelperMethods = new DeliveryHelperMethods();
	CheckoutHelper checkoutHelper = new CheckoutHelper();
	DeliveryDataHelper deliveryDataHelper= new DeliveryDataHelper();
	RTSHelper rtshelp=new RTSHelper();


	public Boolean checkDEAssignment(String de_id,String order_id,String orderJson) {
        Boolean assignstatus=false;
        deliveryServiceHelper.makeDEFree(de_id);
        deliveryServiceHelper.makeDEActive(de_id);
        Boolean cronstatus = false;
        try {
            cronstatus = autoassignHelper.runAutoAssign(new JSONObject(orderJson).getString("restaurant_city_code").toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (cronstatus) {
            String query = "select de_id from trips where order_id=" + order_id;
             assignstatus = deliveryDataHelper.polldb("deliverydb", query, "de_id", de_id, 2, 60);
        }
    return assignstatus;
	}


	@AfterMethod
	public void closeallbatches()
	{
		deliveryHelperMethods.dbhelperupdate(DeliveryConstant.updatebatch);
		deliveryHelperMethods.dbhelperupdate(DeliveryConstant.assigndetounassinged);

	}
	@Test(dataProvider = "QE-224",priority=1, groups = "assignmentregression", description = "Verify whether order is getting assigned to DE if first mile distance is less than configred limit")
	public void autoassigntest224(String order_id, String de_id,String deauth,
								  String version, String lat, String lng, String orderJson) throws InterruptedException {

		deliveryServiceHelper.makeDEFree(de_id);
		deliveryServiceHelper.makeDEActive(de_id);
		Processor processor2 = autoassignHelper.locupdate(lat, lng, deauth);
		Boolean cronstatus=false;
		Assert.assertEquals(processor2.ResponseValidator.GetResponseCode(), 200);
		try {
			 cronstatus=autoassignHelper.runAutoAssign(new JSONObject(orderJson).getString("restaurant_city_code").toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		String query = "select de_id from trips where order_id="+order_id;
		boolean assignstatus=deliveryDataHelper.polldb("deliverydb",query,"de_id",de_id,2,60);
		deliveryServiceHelper.makeDEFree(de_id);
		Assert.assertTrue(assignstatus);


	}
	@Test(dataProvider = "QE-225",priority=2,groups = "assignmentregression", description = "Verify whether order is not getting assigned to DE if first mile distance is more than configured limit")
	public void autoassigntest225(String order_id, String de_id,
								  String version, String lat, String lng, String orderJson, int zone_id) throws InterruptedException {
		//get the configured limit
		Boolean cronstatus=false;
		deliveryServiceHelper.makeDEFree(de_id);
		deliveryServiceHelper.makeDEActive(de_id);
		String auth1=deliveryDataHelper.delocationupdate(lat,lng, de_id, version);
		try {
			 cronstatus=autoassignHelper.runAutoAssign(new JSONObject(orderJson).getString("restaurant_city_code").toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		if(cronstatus) {
			String query = "select de_id from trips where order_id="+order_id;
			boolean assignstatus=deliveryDataHelper.polldb("deliverydb",query,"de_id",de_id,2,60);
			deliveryServiceHelper.makeDEFree(de_id);
			Assert.assertTrue(assignstatus);
		}
		else{Assert.assertTrue(false);}
		SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).execute("update auto_assignment_param_values set value = 4 where zone_id ="+zone_id+" and parameter_id=5");
	}
	//Is there a API to update DEzone
	@Test(dataProvider = "QE-226",priority=3, groups = "assignmentregression", description = "Verify whether order of zone A is not assigned to DE of whom zone A is not one of the service zone")
	public void autoassigntest226(String order_id, String de_id, String version, String lat, String lng, String orderJson) throws InterruptedException {
		SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).execute("update delivery_boys set zone_id = 35 where id="+de_id);
		deliveryServiceHelper.makeDEFree(de_id);
		deliveryServiceHelper.makeDEActive(de_id);
		Boolean cronstatus= false;
		try {
			cronstatus=autoassignHelper.runAutoAssign(new JSONObject(orderJson).getString("restaurant_city_code").toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}

		if(cronstatus) {
			String query = "select de_id from trips where order_id="+order_id;
			boolean assignstatus=deliveryDataHelper.polldb("deliverydb",query,"de_id",de_id,2,60);
			deliveryServiceHelper.makeDEFree(de_id);
			Assert.assertTrue(!assignstatus);
		}
		else{Assert.assertTrue(true);}


	}
	//Is this required
	@Test(dataProvider = "QE-227",priority=4, groups = "assignmentregression", description = "Verify whether order of zone A is assigned to DE of whom A is one of the Sservice zone")
	public void autoassigntest227(String order_id, String de_id,
								  String version, String lat, String lng) throws InterruptedException {
		deliveryServiceHelper.makeDEFree(de_id);
		deliveryServiceHelper.makeDEActive(de_id);
		String auth1=deliveryDataHelper.delocationupdate(lat,lng, de_id, version);
		rtshelp.runassignment(RTSConstants.cityid);
		String query = DeliveryConstant.get_deid + order_id + ";";
		Object assigndeid = deliveryHelperMethods.dbhelperget(query, "de_id");
		if ((null == assigndeid)
				|| (!de_id.equalsIgnoreCase(assigndeid.toString()))) {
			deliveryServiceHelper.makeDEFree(de_id);
			deliveryServiceHelper.deLogout(auth1);
			Assert.assertTrue(false);
		} else {
			deliveryServiceHelper.makeDEFree(de_id);
			deliveryServiceHelper.deLogout(auth1);
			Assert.assertTrue(true);
		}
	}

	//Where have we checked auto reject here
	@Test(dataProvider = "QE-228",priority=5, groups = "assignmentregression", description = "Verify order A is getting auto/system rejected if DE D does not confirm the order within 3 mins")
	public void autoassigntest228(String order_id, String de_id, String version, String lat, String lng,String orderJson) throws InterruptedException {
		deliveryServiceHelper.makeDEFree(de_id);
		deliveryServiceHelper.makeDEActive(de_id);
		Boolean cronstatus= false;
		try {
			cronstatus=autoassignHelper.runAutoAssign(new JSONObject(orderJson).getString("restaurant_city_code").toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}

		if(cronstatus) {
			String query = "select de_id from trips where order_id="+order_id;
			boolean assignstatus=deliveryDataHelper.polldb("deliverydb",query,"de_id",de_id,2,60);
			deliveryServiceHelper.makeDEFree(de_id);
			Assert.assertTrue(!assignstatus);
			deliveryServiceHelper.systemRejectOrder(de_id, order_id);
			try {
				autoassignHelper.runAutoAssign(new JSONObject(orderJson).getString("restaurant_city_code").toString());
			    }
			    catch(JSONException e)
				{
					e.printStackTrace();
				}
			 assignstatus=deliveryDataHelper.polldb("deliverydb",query,"de_id",de_id,2,60);
			if(assignstatus)
			{
				Assert.assertTrue(true);
			}
			else
			{
				Assert.assertTrue(false);
			}
		}
	}
	// ***********************************************************************************************
//Please explain
	@Test(dataProvider = "QE-229",priority=6, enabled = true, groups = {
			"assignmentregression", "Karunakar" }, description = "Verify order A is not getting auto-assigned to DE D if DE D already auto/system rejected the order A")
	public void autoassigntest229(String order_id, String de_id1,
								  String de_id2, String version, String lat, String lng)
			throws InterruptedException {
		Assert.assertNotNull(order_id,
				"Order is not created through checkout helper");
		String query = "update trips set customer_zone=35 where order_id="
				+ order_id + ";";
		deliveryHelperMethods.dbhelperupdate(query);
		deliveryServiceHelper.makeDEActive(de_id1);
		Thread.sleep(2000);
		deliveryServiceHelper.makeDEActive(de_id2);
		deliveryServiceHelper.makeDEFree(de_id1);
		deliveryServiceHelper.makeDEFree(de_id2);
		// DE 1 Login
		String auth1=deliveryDataHelper.delocationupdate(lat,lng, de_id1, version);
		// DE 2 Login
		String auth2=deliveryDataHelper.delocationupdate(lat,lng, de_id2, version);
		deliveryServiceHelper.makeDEFree(de_id1);
		deliveryServiceHelper.makeDEFree(de_id2);
		rtshelp.runassignment(RTSConstants.cityid);
		query = DeliveryConstant.get_deid + order_id + ";";
		Object assigndeid = deliveryHelperMethods.dbhelperget(query, "de_id");
		if (assigndeid.toString() == de_id1) {
			deliveryServiceHelper.systemRejectOrder(assigndeid.toString(), order_id);
			Thread.sleep(10000);
			query = DeliveryConstant.get_deid + order_id + ";";
			Object assigndeid2 = deliveryHelperMethods.dbhelperget(query, "de_id");
			System.out.println(assigndeid2.toString() + " Assigned");
			deliveryServiceHelper.deLogout(auth1);
			deliveryServiceHelper.deLogout(auth2);
			Assert.assertTrue(true);
		} else if (assigndeid.toString() == de_id2) {
			deliveryServiceHelper.systemRejectOrder(assigndeid.toString(), order_id);
			Thread.sleep(10000);
			query = DeliveryConstant.get_deid + order_id + ";";
			Object assigndeid3 = deliveryHelperMethods.dbhelperget(query, "de_id").toString();
			System.out.println(assigndeid3.toString() + " Assigned");
			deliveryServiceHelper.deLogout(auth1);
			deliveryServiceHelper.deLogout(auth2);
			Assert.assertTrue(false);
		} else {
			System.out.println("Order is not getting assigned to any DE");
			deliveryServiceHelper.deLogout(auth1);
			deliveryServiceHelper.deLogout(auth2);
			Assert.assertTrue(true);
		}
	}
	//Why Logout is required here
	// **********************************************************************************************************
	@Test(dataProvider = "QE-230",priority=7, enabled = true, groups = {
			"assignmentregression", "Karunakar" }, description = "verify order A is assigned to DE D1 if it was auto/system rejected by DE D")
	public void autoassigntest230(String order_id, String de_id1, String de_id2, String version, String lat, String lng,String orderJson, String de_auth, String reject_deAuth) throws InterruptedException {

/*		deliveryServiceHelper.makeDEFree(de_id1);
		deliveryServiceHelper.makeDEFree(de_id2);
		deliveryServiceHelper.makeDEActive(de_id1);
		deliveryServiceHelper.makeDEActive(de_id2);
		// DE 1 Login
        try {
            autoassignHelper.runAutoAssign(new JSONObject(orderJson).getString("restaurant_city_code").toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String query = DeliveryConstant.get_deid + order_id + ";";
		Object assigndeid = deliveryHelperMethods.dbhelperget(query, "de_id");*/
        Boolean asssignedStatus1=checkDEAssignment(de_id1,order_id,orderJson);
		//if (null==assigndeid || !(assigndeid.toString().equalsIgnoreCase(de_id1))) {
		    if (asssignedStatus1) {
                deliveryServiceHelper.systemRejectOrder(de_id1, order_id);
                try {
                    autoassignHelper.runAutoAssign(new JSONObject(orderJson).getString("restaurant_city_code").toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            Boolean asssignedStatus2=checkDEAssignment(de_id2,order_id,orderJson);
			if(asssignedStatus2)
            {
                Assert.assertTrue(true);
            }
            else
            {
                Assert.assertTrue(false);
            }
            deliveryServiceHelper.makeDEFree(de_id1);
			//deliveryServiceHelper.deLogout(de_auth);
			deliveryServiceHelper.makeDEFree(de_id2);
			//deliveryServiceHelper.deLogout(reject_deAuth);

			/*}
			else
			{
				deliveryServiceHelper.makeDEFree(de_id1);
				deliveryServiceHelper.deLogout(reject_deAuth);
				deliveryServiceHelper.makeDEFree(de_id2);
				deliveryServiceHelper.deLogout(auth2);
				Assert.assertTrue(true);
			}
		}
		else {
			deliveryServiceHelper.makeDEFree(de_id1);
			deliveryServiceHelper.makeDEFree(de_id2);
			deliveryServiceHelper.markDEInactive(de_id1, "1");
			deliveryServiceHelper.markDEInactive(de_id2, "1");
			Assert.assertTrue(false);
		}*/
	}
	// **********************************************************************************************************
	@Test(dataProvider = "QE-231",priority=8, enabled = true, groups = {
			"assignmentregression", "Karunakar" }, description = "Verify with DE score is added to DE score when with DE order is assigned to DE")
	// WithDE Order
	public void autoassigntest231(String order_id, String withdeorder_id, String de_id, String version, String lat, String lng, String orderJson) throws InterruptedException {
/*		String query = null;
		*//*String query1 = "update trips set customer_zone=35 where order_id="
				+ order_id + ";";
		deliveryHelperMethods.dbhelperupdate(query1);*//*
		deliveryServiceHelper.makeDEActive(de_id);
		deliveryServiceHelper.makeDEFree(de_id);
		// DE 1 Login

		rtshelp.runassignment(RTSConstants.cityid);
		query = DeliveryConstant.get_deid + withdeorder_id + ";";
		Object assigndeid = deliveryHelperMethods.dbhelperget(query, "de_id");
		System.out
				.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
						+ assigndeid.toString());
		if (assigndeid.toString().equalsIgnoreCase(de_id)) {
			deliveryServiceHelper.makeDEFree(de_id);
			//deliveryServiceHelper.deLogout(auth1);
			Assert.assertTrue(true);
		} else {
			deliveryServiceHelper.makeDEFree(de_id);
			//deliveryServiceHelper.deLogout(auth1);
			Assert.assertTrue(false);
		}*/

        Boolean assignstatus=checkDEAssignment(de_id,withdeorder_id,orderJson);
        Assert.assertTrue(assignstatus);
	}

	//how do u know that withde score is not assigned to de PREETESH to Ask
	// ***********************************************************************************************************************
	@Test(dataProvider = "QE-232",priority=9, enabled = true, groups = {
			"assignmentregression", "Karunakar" }, description = "Verify with DE score is not added to DE score when normal order is assigned to DE")
	public void autoassigntest232(String order_id, String de_id, String version, String lat, String lng) throws InterruptedException {
		deliveryServiceHelper.makeDEFree(de_id);
		deliveryServiceHelper.makeDEActive(de_id);
		// DE 1 Login
		String auth1=deliveryDataHelper.delocationupdate(lat,lng, de_id, version);
		rtshelp.runassignment(RTSConstants.cityid);
		String query1 = DeliveryConstant.get_deid + order_id + ";";
		Object assigndeid = deliveryHelperMethods.dbhelperget(query1, "de_id");
		if ((assigndeid.toString()).equalsIgnoreCase(de_id)) {
			deliveryServiceHelper.deLogout(auth1);
			Assert.assertTrue(true);
		} else {
			deliveryServiceHelper.deLogout(auth1);
			Assert.assertTrue(false);
		}
	}

	//Where are we checking the weight calclation PREETESH

	// *********************************************************************************************************************
	@Test(dataProvider = "QE-233",priority=10, groups = { "assignmentregression",
			"Karunakar" }, description = "Verify that Weight of Order Delay is calculated for an order when order delay is greater than the delay threshold")
	public void autoassigntest233(String order_id, String order_id1,
								  String de_id, String version, String lat, String lng)
			throws InterruptedException {
		deliveryServiceHelper.makeDEActive(de_id);
		deliveryServiceHelper.makeDEFree(de_id);
		String auth1=deliveryDataHelper.delocationupdate(lat,lng, de_id, version);
		rtshelp.runassignment(RTSConstants.cityid);
		String query = "Select * from trips where order_id=" + order_id1 + ";";
		Object assigndeid = deliveryHelperMethods.dbhelperget(query, "de_id");
		if ((null == assigndeid)
				|| (!de_id.equalsIgnoreCase(assigndeid.toString()))) {
			deliveryServiceHelper.makeDEFree(de_id);
			deliveryServiceHelper.deLogout(auth1);
			autoassignHelper.updateParamAuto(DeliveryConstant.city, "49",
					DeliveryConstant.autoassign_orderdelaystepfactor,
					DeliveryConstant.autoassign_orderdelaystepfactorvaluemore,
					"test",
					DeliveryConstant.autoassign_orderdelaystepfactorname);
			autoassignHelper.updateParamAuto(DeliveryConstant.city, "49",
					DeliveryConstant.autoassign_weightorderdelayid,
					DeliveryConstant.autoassign_weightorderdelayless, "test",
					DeliveryConstant.autoassign_weightorderdelayname);
			Assert.assertTrue(true);
		} else {
			deliveryServiceHelper.makeDEFree(de_id);
			deliveryServiceHelper.deLogout(auth1);
			autoassignHelper.updateParamAuto(DeliveryConstant.city, "49",
					DeliveryConstant.autoassign_orderdelaystepfactor,
					DeliveryConstant.autoassign_orderdelaystepfactorvaluemore,
					"test",
					DeliveryConstant.autoassign_orderdelaystepfactorname);
			autoassignHelper.updateParamAuto(DeliveryConstant.city, "49",
					DeliveryConstant.autoassign_weightorderdelayid,
					DeliveryConstant.autoassign_weightorderdelayless, "test",
					DeliveryConstant.autoassign_weightorderdelayname);
			Assert.assertTrue(false);
		}
	}
	//Where is Shift weight added to this Preetesh
	@Test(dataProvider = "QE-237",priority=11, groups = "assignmentregression", description = "Verify Within Shift Weight is added to DE score when order is assigned to DE during DE shift time")
	public void autoassigntest237(String order_id,String de_id, String de_id1, String version,
								  String lat, String lng) throws Exception {
		deliveryServiceHelper.makeDEFree(de_id);
		deliveryServiceHelper.makeDEFree(de_id1);
		String auth1=deliveryDataHelper.delocationupdate(lat,lng, de_id, version);
		String auth2=deliveryDataHelper.delocationupdate(lat,lng, de_id1, version);
		rtshelp.runassignment(RTSConstants.cityid);
		String query = "Select * from trips where order_id=" + order_id + ";";
		Object assigndeid = deliveryHelperMethods.dbhelperget(query, "de_id");
		if ((null == assigndeid)
				|| (!de_id.equalsIgnoreCase(assigndeid.toString()))) {
			deliveryServiceHelper.makeDEFree(de_id);
			deliveryServiceHelper.makeDEFree(de_id1);
			deliveryServiceHelper.deLogout(auth1);
			deliveryServiceHelper.deLogout(auth2);
			autoassignHelper.updateParamAuto(DeliveryConstant.city, "34",
					DeliveryConstant.autoassign_withinshiftid,
					DeliveryConstant.autoassign_withinshiftless, "test",
					DeliveryConstant.autoassign_withinshiftname);
			Assert.assertTrue(false);
		} else {
			deliveryServiceHelper.makeDEFree(de_id);
			deliveryServiceHelper.makeDEFree(de_id1);
			deliveryServiceHelper.deLogout(auth1);
			deliveryServiceHelper.deLogout(auth2);
			autoassignHelper.updateParamAuto(DeliveryConstant.city, "34",
					DeliveryConstant.autoassign_withinshiftid,
					DeliveryConstant.autoassign_withinshiftless, "test",
					DeliveryConstant.autoassign_withinshiftname);
			Assert.assertTrue(true);
		}
	}
	//Where i cycle weight verified in here

	@Test(dataProvider = "QE-239",priority=12, enabled = true, groups = {
			"assignmentregression", "Somnath" }, description = "Verify Weight of Cycle DEs is added to DE score when order is assigned to cycle DE")
	public void autoAssign_CycleDE_addingWeight(String order_id,
												String cycleDE_id, String de_id, String version, String lat,
												String lng) throws InterruptedException {
		Assert.assertNotNull(order_id,
				"Order is not created through checkout helper");
		deliveryServiceHelper.makeDEFree(cycleDE_id);
		deliveryServiceHelper.makeDEActive(cycleDE_id);
		deliveryDataHelper.delocationupdate(lat,lng, cycleDE_id, version);
		deliveryServiceHelper.makeDEFree(de_id);
		deliveryServiceHelper.makeDEActive(de_id);
		deliveryDataHelper.delocationupdate(lat,lng, de_id, version);
		rtshelp.runassignment(RTSConstants.cityid);
		String query = "Select * from trips where order_id=" + order_id + ";";
		Object assignDEid = deliveryHelperMethods.dbhelperget(query, "de_id");
		if ((null == assignDEid)
				|| (!cycleDE_id.equalsIgnoreCase(assignDEid.toString()))) {
			deliveryServiceHelper.makeDEFree(cycleDE_id);
			deliveryServiceHelper.markDEInactive(cycleDE_id, "1");
			deliveryServiceHelper.makeDEFree(de_id);
			deliveryServiceHelper.markDEInactive(de_id,"1");
			Assert.assertTrue(false);
		} else {
			deliveryServiceHelper.makeDEFree(cycleDE_id);
			deliveryServiceHelper.markDEInactive(cycleDE_id, "1");
			deliveryServiceHelper.makeDEFree(de_id);
			deliveryServiceHelper.markDEInactive(de_id,"1");
			Assert.assertTrue(true);
		}
	}

	//Where is weight verified here again, It seems irrelevant to the subject it is written for Preetesh
	@Test(dataProvider = "QE-240",priority=13, enabled = true, groups = {
			"assignmentregression", "Somnath" }, description = "Verify Weight of Cycle DEs is not added to DE score when order is not assigned to cycle DE")
	public void autoAssign_CycleDE_notAddingWeight(String cycleDE_id, String de_id, String version, String lat,
												   String lng) throws Exception {
		deliveryServiceHelper.makeDEFree(cycleDE_id);
		deliveryServiceHelper.makeDEActive(cycleDE_id);
		deliveryServiceHelper.makeDEFree(de_id);
		deliveryServiceHelper.makeDEActive(de_id);
		String auth1=deliveryDataHelper.delocationupdate(lat,lng, cycleDE_id, version);
		deliveryServiceHelper.makeDEFree(de_id);
		deliveryServiceHelper.makeDEActive(de_id);// Free Normal DE
		String auth2=deliveryDataHelper.delocationupdate(lat,lng, de_id, version);
		Processor processor3 = checkoutHelper.placeOrder(DeliveryConstant.mobile,
				DeliveryConstant.password, "1636498", "1", "4852");
		String resp2 = processor3.ResponseValidator.GetBodyAsText();
		String order_id = JsonPath.read(resp2, "$.data.order_id").toString();
		Thread.sleep(10000);
		Assert.assertNotNull(order_id,
				"Order is not created through checkout helper");
		String query1 = "update trips set customer_zone=35 where order_id="
				+ order_id + ";";
		deliveryHelperMethods.dbhelperupdate(query1);
		rtshelp.runassignment(RTSConstants.cityid);
		String query = "Select * from trips where order_id=" + order_id + ";";
		Object assignDEid = deliveryHelperMethods.dbhelperget(query, "de_id");
		if ((null == assignDEid)
				|| (cycleDE_id.equalsIgnoreCase(assignDEid.toString()))) {
			deliveryServiceHelper.makeDEFree(de_id);
			deliveryServiceHelper.deLogout(auth1);
			deliveryServiceHelper.makeDEFree(cycleDE_id);
			deliveryServiceHelper.deLogout(auth2);
			Assert.assertTrue(false);
		} else {
			deliveryServiceHelper.makeDEFree(cycleDE_id);
			deliveryServiceHelper.markDEInactive(cycleDE_id, "1");
			deliveryServiceHelper.makeDEFree(de_id);
			deliveryServiceHelper.markDEInactive(de_id, "1");
			Assert.assertTrue(true);
		}
	}
	@Test(dataProvider = "QE-241",priority=14, enabled = true, groups = {
			"assignmentregression", "Somnath" }, description = "Verify order is assigned to cycle DE if first mile distance is less than Max first mile distance for Cycle DEs")
	public void autoAssignTest_CycleDE(String order_id, String de_id,
									   String version, String lat, String lng, String orderJson) throws InterruptedException {

    Boolean assignedStatus= checkDEAssignment(de_id,order_id,orderJson);
    Assert.assertTrue(assignedStatus);

	}
	//We have not updated the params here why? Preetesh
	@Test(dataProvider = "QE-242",priority=33, enabled = true, groups = {
			"assignmentregression", "Somnath" }, description = "Verify order is not assigned to cycle DE if first mile distance is more than Max first mile distance for Cycle DEs")
	public void autoAssignTest_CycleDE_NegativeScenario(String order_id, String de_id, String version, String lat, String lng, String orderJson) throws InterruptedException {

        Boolean assigned_status= checkDEAssignment(de_id,order_id,orderJson);
	    Assert.assertFalse(assigned_status,"Order is wrongly assigned tp cycle de");

/*		deliveryServiceHelper.makeDEFree(de_id);
		String auth1=deliveryDataHelper.delocationupdate(lat,lng, de_id, version);
		rtshelp.runassignment(RTSConstants.cityid);
		String query = "Select * from trips where order_id=" + order_id + ";";
		Object assignDEid = deliveryHelperMethods.dbhelperget(query, "de_id");
		if ((null == assignDEid)
				|| (!de_id.equalsIgnoreCase(assignDEid.toString()))) {
			deliveryServiceHelper.deLogout(auth1);
			Assert.assertTrue(true);
		} else {
			deliveryServiceHelper.makeDEFree(de_id);
			Assert.assertTrue(false);
		}*/
	}
	@Test(dataProvider = "QE-243",priority=15, groups = "assignmentregression", description = "Verify order is assigned to cycle DE if last mile distance is less than Max last mile distance for Cycle DEs")
	public void autoassigntest243(String order_id1, String de_id,
								  String version, String lat, String lng, String orderJson) throws InterruptedException {
/*		Assert.assertNotNull(order_id1,
				"Order is not created through checkout helper");
		deliveryServiceHelper.makeDEFree(de_id);
		deliveryServiceHelper.makeDEActive(de_id);
		String auth1=deliveryDataHelper.delocationupdate(lat,lng, de_id, version);
		rtshelp.runassignment(RTSConstants.cityid);
		String query = "Select * from trips where order_id=" + order_id1 + ";";
		Object assigndeid = deliveryHelperMethods.dbhelperget(query, "de_id");
		if ((assigndeid == null)
				|| (!de_id.equalsIgnoreCase(assigndeid.toString()))) {
			System.out.println("de_id is null/Not assigned to cycle de");
			deliveryServiceHelper.makeDEFree(de_id);
			deliveryServiceHelper.markDEInactive(de_id, "1");
			Assert.assertTrue(false);
		}
		else if (de_id.equalsIgnoreCase(assigndeid.toString())) {
			System.out.println("assigned to cycle de");
			deliveryServiceHelper.makeDEFree(de_id);
			deliveryServiceHelper.deLogout(auth1);
			deliveryServiceHelper.markDEInactive(de_id, "1");
			Assert.assertTrue(true);
		}*/

        Boolean assign_status= checkDEAssignment(de_id,order_id1,orderJson);
        Assert.assertTrue(assign_status,"Not assigned to cycle DE");
	}
	@Test(dataProvider = "QE-244",priority=16, groups = "assignmentregression", description = "Verify order is not assigned to cycle DE if last mile distance is more than Max last mile distance for Cycle DEs")
	public void autoassigntest244(String order_id, String cyclede,
								  String version, String de_id, String lat, String lng, String orderJson) throws InterruptedException {
	    /*		Assert.assertNotNull(order_id,
				"Order is not created through checkout helper");
		deliveryServiceHelper.makeDEFree(cyclede);
		deliveryServiceHelper.makeDEActive(cyclede);
		deliveryServiceHelper.makeDEActive(de_id);
		deliveryServiceHelper.makeDEFree(de_id);
		String auth1=deliveryDataHelper.delocationupdate(lat,lng, cyclede, version);
		rtshelp.runassignment(RTSConstants.cityid);
		String query = DeliveryConstant.get_deid + order_id + ";";
		Object assigndeid = deliveryHelperMethods.dbhelperget(query, "de_id");
		if ((null == assigndeid)
				|| (!cyclede.equalsIgnoreCase(assigndeid.toString()))) {
			deliveryServiceHelper.makeDEFree(cyclede);
			deliveryServiceHelper.deLogout(auth1);
			Assert.assertTrue(true);
		} else {
			deliveryServiceHelper.makeDEFree(cyclede);
			deliveryServiceHelper.deLogout(auth1);
			Assert.assertTrue(false);
		}*/
	    Boolean assign_status=checkDEAssignment(cyclede,order_id,orderJson);
	    Assert.assertFalse(assign_status,"Order  assigned to cycle de");

	}
	@Test(dataProvider = "QE-245",priority=17, groups = "assignmentregression", description = "Verify order is not assigned to cycle DE if total trip distance (FM+LM) is less than Max total trip distance for Cycle DEs")
	public void autoassigntest245(String order_id, String cyclede,String deauth,
								  String version, String lat, String lng, String orderJson)  {
/*		Assert.assertNotNull(order_id,
				"Order is not created through checkout helper");
		deliveryServiceHelper.makeDEFree(cyclede);
		deliveryServiceHelper.makeDEActive(cyclede);
		deliveryServiceHelper.deLogout(deauth);
		String auth1=deliveryDataHelper.delocationupdate(lat,lng, cyclede, version);
		rtshelp.runassignment(RTSConstants.cityid);
		String query = "Select * from trips where order_id=" + order_id + ";";
		Object assigndeid = deliveryHelperMethods.dbhelperget(query, "de_id");
		if ((null == assigndeid)
				|| (!cyclede.equalsIgnoreCase(assigndeid.toString()))) {
			deliveryServiceHelper.makeDEFree(cyclede);
			deliveryServiceHelper.deLogout(auth1);
			Assert.assertTrue(true);
		} else {
			deliveryServiceHelper.makeDEFree(cyclede);
			deliveryServiceHelper.deLogout(auth1);
			Assert.assertTrue(false);
		}*/

        Boolean assign_status=checkDEAssignment(cyclede,order_id,orderJson);
        Assert.assertFalse(assign_status,"Order assigned to cycle de");
	}
	@Test(dataProvider = "QE-246",priority=18, groups = "assignmentregression", description = "Verify order is assigned to cycle DE if total trip distance (FM+LM) is less than Max total trip distance for Cycle DEs")
	public void autoassigntest246(String order_id, String cyclede,
								  String version, String lat, String lng, String orderJson)  {

/*		Assert.assertNotNull(order_id,
				"Order is not created through checkout helper");
		deliveryServiceHelper.makeDEFree(cyclede);
		deliveryServiceHelper.makeDEBusy("2418");
		deliveryServiceHelper.makeDEActive(cyclede);
		String auth1=deliveryDataHelper.delocationupdate(lat,lng, cyclede, version);
		rtshelp.runassignment(RTSConstants.cityid);
		String query = "Select * from trips where order_id=" + order_id + ";";
		Object assigndeid = deliveryHelperMethods.dbhelperget(query, "de_id");
		if ((null == assigndeid)
				|| (!cyclede.equalsIgnoreCase(assigndeid.toString()))) {
			deliveryServiceHelper.makeDEFree(cyclede);
			deliveryServiceHelper.deLogout(auth1);
			Assert.assertTrue(false);
		} else {
			deliveryServiceHelper.makeDEFree(cyclede);
			deliveryServiceHelper.deLogout(auth1);
			Assert.assertTrue(true);

		*/
        Boolean assign_status=checkDEAssignment(cyclede,order_id,orderJson);
        Assert.assertFalse(assign_status,"Order assigned to cycle de");
	}
	@Test(dataProvider = "QE-247", priority=19,groups = "assignmentregression", description = "Verify order assignment to DE is halted by x mins even when DE is eligible for order assignment where 'x' is Min JIT delay configured")
	public void autoassigntest247(String order_id, String de_id, String version, String lat, String lng, String orderJson,String zoneid) throws InterruptedException {
        Boolean assign_status=checkDEAssignment(de_id,order_id,orderJson);
        Assert.assertFalse(assign_status,"Order assigned to de after jit delay");
        autoassignHelper.updateParamAuto(DeliveryConstant.city, zoneid.toString(),
                DeliveryConstant.autoassign_minjitparamid,
                DeliveryConstant.autoassign_minjitvalueless, "test",
                DeliveryConstant.autoassign_minjitparamidname);
        autoassignHelper.updateParamAuto(DeliveryConstant.city, zoneid.toString(),
                DeliveryConstant.autoassign_maxjitparamid,
                DeliveryConstant.autoassign_maxjitvalueless, "test",
                DeliveryConstant.autoassign_maxjitparamidname);
	   /* Assert.assertNotNull(order_id,
				"Order is not created through checkout helper");
		deliveryServiceHelper.makeDEFree(de_id);
		String auth1=deliveryDataHelper.delocationupdate(lat,lng, de_id, version);
		rtshelp.runassignment(RTSConstants.cityid);
		String query = "Select * from trips where order_id=" + order_id + ";";
		Object assigndeid = deliveryHelperMethods.dbhelperget(query, "de_id");
		if ((null == assigndeid)
				|| (!de_id.equalsIgnoreCase(assigndeid.toString()))) {
			deliveryServiceHelper.makeDEFree(de_id);
			autoassignHelper.updateParamAuto(DeliveryConstant.city, "34",
					DeliveryConstant.autoassign_minjitparamid,
					DeliveryConstant.autoassign_minjitvalueless, "test",
					DeliveryConstant.autoassign_minjitparamidname);
			autoassignHelper.updateParamAuto(DeliveryConstant.city, "34",
					DeliveryConstant.autoassign_maxjitparamid,
					DeliveryConstant.autoassign_maxjitvalueless, "test",
					DeliveryConstant.autoassign_maxjitparamidname);
			deliveryServiceHelper.deLogout(auth1);
			Assert.assertTrue(true);
		} else {
			deliveryServiceHelper.makeDEFree(de_id);
			autoassignHelper.updateParamAuto(DeliveryConstant.city, "34",
					DeliveryConstant.autoassign_minjitparamid,
					DeliveryConstant.autoassign_minjitvalueless, "test",
					DeliveryConstant.autoassign_minjitparamidname);
			autoassignHelper.updateParamAuto(DeliveryConstant.city, "34",
					DeliveryConstant.autoassign_maxjitparamid,
					DeliveryConstant.autoassign_maxjitvalueless, "test",
					DeliveryConstant.autoassign_maxjitparamidname);
			deliveryServiceHelper.deLogout(auth1);
			Assert.assertTrue(false);
		}*/
	}
	@Test(dataProvider = "QE-248",priority=20, groups = "assignmentregression", description = "Verify max first mile distance is increased by 'step factor for dynamic First Mile' value when no DE is within normal first mile")
	public void autoassigntest248(String order_id, String de_id, String version, String lat, String lng, String orderJson,String zone_id, String city_id)  {
        Boolean assign_status=checkDEAssignment(de_id,order_id,orderJson);
        Assert.assertFalse(assign_status);
        autoassignHelper.updateParamAuto(city_id, zone_id.toString(),
                DeliveryConstant.autoassigndynamicfirstmiledelay, DeliveryConstant.autoassigndynamicfirstmiledelaymoremin,
                "test", DeliveryConstant.autoassigndynamicfirstmiledelayname);
/*		deliveryServiceHelper.makeDEFree(de_id);
		deliveryServiceHelper.makeDEActive(de_id);
		String auth1=deliveryDataHelper.delocationupdate(lat,lng, de_id, version);
		rtshelp.runassignment(RTSConstants.cityid);
		String query = "Select * from trips where order_id=" + order_id + ";";
		Object assigndeid = deliveryHelperMethods.dbhelperget(query, "de_id");
		if ((null == assigndeid)
				|| (!de_id.equalsIgnoreCase(assigndeid.toString()))) {
			deliveryServiceHelper.makeDEFree(de_id);
			deliveryServiceHelper.deLogout(auth1);
			autoassignHelper.updateParamAuto(DeliveryConstant.city, "34",
					DeliveryConstant.autoassigndynamicfirstmiledelay,
					DeliveryConstant.autoassigndynamicfirstmiledelaymoremin,
					"test",
					DeliveryConstant.autoassigndynamicfirstmiledelayname);
			Assert.assertTrue(false);
		} else {
			deliveryServiceHelper.makeDEFree(de_id);
			deliveryServiceHelper.deLogout(auth1);
			autoassignHelper.updateParamAuto(DeliveryConstant.city, "34",
					DeliveryConstant.autoassigndynamicfirstmiledelay,
					DeliveryConstant.autoassigndynamicfirstmiledelaymoremin,
					"test",
					DeliveryConstant.autoassigndynamicfirstmiledelayname);
			Assert.assertTrue(true);
		}*/

	}
	@Test(dataProvider = "QE-249",priority=21, groups = "assignmentregression", description = "Verify max first mile distance is increased by 'step factor for dynamic First Mile' value only till 'absolute dynamic first mile' when no DE is within normal first mile")
	public void autoassigntest249(String order_id, String de_id,
								  String version, String lat, String lng) throws InterruptedException {
		Assert.assertNotNull(order_id,
				"Order is not created through checkout helper");
		Thread.sleep(5000);
		deliveryServiceHelper.makeDEFree(de_id);
		String auth1=deliveryDataHelper.delocationupdate(lat,lng, de_id, version);
		rtshelp.runassignment(RTSConstants.cityid);
		String query = "Select * from trips where order_id=" + order_id + ";";
		Object assigndeid = deliveryHelperMethods.dbhelperget(query, "de_id");
		if ((null == assigndeid)
				|| (!de_id.equalsIgnoreCase(assigndeid.toString()))) {
			deliveryServiceHelper.makeDEFree(de_id);
			autoassignHelper.updateParamAuto(DeliveryConstant.city, "34",
					DeliveryConstant.autoassigndynamicfirstmiledelay,
					DeliveryConstant.autoassigndynamicfirstmiledelaymoremin,
					"test",
					DeliveryConstant.autoassigndynamicfirstmiledelayname);
			autoassignHelper.updateParamAuto(DeliveryConstant.city, "34",
					DeliveryConstant.autoassignabsoluteDynamicfirstmile,
					DeliveryConstant.autoassignabsoluteDynamicfirstmilemore,
					"test",
					DeliveryConstant.autoassignabsoluteDynamicfirstmilename);
			deliveryServiceHelper.deLogout(auth1);
			Assert.assertTrue(true);
		} else {
			deliveryServiceHelper.makeDEFree(de_id);
			autoassignHelper.updateParamAuto(DeliveryConstant.city, "34",
					DeliveryConstant.autoassigndynamicfirstmiledelay,
					DeliveryConstant.autoassigndynamicfirstmiledelaymoremin,
					"test",
					DeliveryConstant.autoassigndynamicfirstmiledelayname);
			autoassignHelper.updateParamAuto(DeliveryConstant.city, "34",
					DeliveryConstant.autoassignabsoluteDynamicfirstmile,
					DeliveryConstant.autoassignabsoluteDynamicfirstmilemore,
					"test",
					DeliveryConstant.autoassignabsoluteDynamicfirstmilename);
			deliveryServiceHelper.deLogout(auth1);
			Assert.assertTrue(false);
		}
	}
	@Test(dataProvider = "QE-250",priority=22, groups = "assignmentregression", description = "Verify DE of other service zone is considered for assignment if DE is within max customer zone max first mile distance")
	public void autoassigntest250(String order_id,String de_id, String version,
								  String lat, String lng) throws Exception {
		deliveryServiceHelper.makeDEFree(de_id);
		deliveryServiceHelper.makeDEActive(de_id);
		String auth1=deliveryDataHelper.delocationupdate(lat,lng, de_id, version);
		String query1 = "update trips set customer_zone=33 where order_id="
				+ order_id + ";";
		deliveryHelperMethods.dbhelperupdate(query1);
		rtshelp.runassignment(RTSConstants.cityid);
		String query = "Select * from trips where order_id=" + order_id + ";";
		Object assigndeid = deliveryHelperMethods.dbhelperget(query, "de_id");
		if (null == assigndeid || !de_id.equalsIgnoreCase(assigndeid.toString())) {
			deliveryServiceHelper.makeDEFree(de_id);
			deliveryServiceHelper.deLogout(auth1);
			autoassignHelper.updateParamAuto(DeliveryConstant.city, "34",
					DeliveryConstant.autoassigncasfirstmile,
					DeliveryConstant.autoassigncasfirstmileless, "test",
					DeliveryConstant.autoassigncasfirstmilename);
			autoassignHelper.updateParamAuto(DeliveryConstant.city, "34",
					DeliveryConstant.autoassigncasweight,
					DeliveryConstant.autoassigncasweightless, "test",
					DeliveryConstant.autoassigncasweightname);
			Assert.assertTrue(false);
		} else {
			deliveryServiceHelper.makeDEFree(de_id);
			deliveryServiceHelper.deLogout(auth1);
			autoassignHelper.updateParamAuto(DeliveryConstant.city, "34",
					DeliveryConstant.autoassigncasfirstmile,
					DeliveryConstant.autoassigncasfirstmileless, "test",
					DeliveryConstant.autoassigncasfirstmilename);
			autoassignHelper.updateParamAuto(DeliveryConstant.city, "34",
					DeliveryConstant.autoassigncasweight,
					DeliveryConstant.autoassigncasweightless, "test",
					DeliveryConstant.autoassigncasweightname);
			Assert.assertTrue(true);
		}
	}
	@Test(dataProvider = "QE-251",priority=23, groups = "assignmentregression", description = "Verify that cancelled order is not considered for assignment")
	public void autoassigntest251(String order_id, String de_id,
								  String version, String lat, String lng) throws InterruptedException {
		Assert.assertNotNull(order_id,
				"Order is not created through checkout helper");
		String query1 = DeliveryConstant.cancelorderquery + order_id + ";";
		Object cancel = deliveryHelperMethods.dbhelperget(query1, "cancelled");
		if (cancel.toString().equalsIgnoreCase("true")) {
			deliveryServiceHelper.makeDEFree(de_id);
			String auth1=deliveryDataHelper.delocationupdate(lat,lng, de_id, version);
			rtshelp.runassignment(RTSConstants.cityid);
			String query = "Select * from trips where order_id=" + order_id
					+ ";";
			Object assigndeid = deliveryHelperMethods.dbhelperget(query, "de_id");
			if ((null == assigndeid)
					|| (!de_id.equalsIgnoreCase(assigndeid.toString()))) {
				deliveryServiceHelper.makeDEFree(de_id);
				deliveryServiceHelper.deLogout(auth1);
				Assert.assertTrue(true);
			} else {
				deliveryServiceHelper.makeDEFree(de_id);
				deliveryServiceHelper.deLogout(auth1);
				Assert.assertTrue(false);
			}
		} else {
			Assert.assertTrue(false, "Order is not cancelled in delivery DB");
		}
	}
	@Test(dataProvider = "QE-252",priority=24, groups = "assignmentregression", description = "Verify that DE is not considered for assignment if DE is not active")
	public void autoassigntest252(String order_id, String de_id,String version, String lat, String lng, String orderJson){
        Boolean assign_status=checkDEAssignment(de_id,order_id,orderJson);
        Assert.assertTrue(assign_status);
/*		Assert.assertNotNull(order_id,
				"Order is not created through checkout helper");
		rtshelp.runassignment(RTSConstants.cityid);
		String query = "Select * from trips where order_id=" + order_id + ";";
		Object assigndeid = deliveryHelperMethods.dbhelperget(query, "de_id");
		if ((null == assigndeid)
				|| (!de_id.equalsIgnoreCase(assigndeid.toString()))) {
			Assert.assertTrue(true);
		} else {
			deliveryServiceHelper.makeDEFree(de_id);
			Assert.assertTrue(false);
		}*/
	}
	@Test(dataProvider = "QE-253",priority=25, groups = "assignmentregression", description = "Verify that DE is considered from customer zone if first mile is less than first mile threshold")
	public void autoassigntest253(String de_id,
								  String version, String lat, String lng) throws Exception {
		deliveryServiceHelper.makeDEFree(de_id);
		String auth1=deliveryDataHelper.delocationupdate(lat,lng, de_id, version);
		Processor processor3 = checkoutHelper.placeOrder(DeliveryConstant.mobile,
				DeliveryConstant.password, "1118346", "1", "4852");
		String resp2 = processor3.ResponseValidator.GetBodyAsText();
		String order_id = JsonPath.read(resp2, "$.data.order_id").toString();
		Thread.sleep(10000);
		Assert.assertNotNull(order_id,
				"Order is not created through checkout helper");
		String query1 = "update trips set customer_zone=35 where order_id="
				+ order_id + ";";
		deliveryHelperMethods.dbhelperupdate(query1);
		rtshelp.runassignment(RTSConstants.cityid);
		String query = "Select * from trips where order_id=" + order_id + ";";
		Object assigndeid = deliveryHelperMethods.dbhelperget(query, "de_id");
		if (null == assigndeid || !de_id.equalsIgnoreCase(assigndeid.toString())) {
			deliveryServiceHelper.makeDEFree(de_id);
			deliveryServiceHelper.deLogout(auth1);
			autoassignHelper.updateParamAuto(DeliveryConstant.city, "34",
					DeliveryConstant.autoassigncasfirstmile,
					DeliveryConstant.autoassigncasfirstmileless, "test",
					DeliveryConstant.autoassigncasfirstmilename);
			autoassignHelper.updateParamAuto(DeliveryConstant.city, "34",
					DeliveryConstant.autoassigncasweight,
					DeliveryConstant.autoassigncasweightless, "test",
					DeliveryConstant.autoassigncasweightname);
			Assert.assertTrue(false);
		} else {
			deliveryServiceHelper.makeDEFree(de_id);
			deliveryServiceHelper.deLogout(auth1);
			autoassignHelper.updateParamAuto(DeliveryConstant.city, "34",
					DeliveryConstant.autoassigncasfirstmile,
					DeliveryConstant.autoassigncasfirstmileless, "test",
					DeliveryConstant.autoassigncasfirstmilename);
			autoassignHelper.updateParamAuto(DeliveryConstant.city, "34",
					DeliveryConstant.autoassigncasweight,
					DeliveryConstant.autoassigncasweightless, "test",
					DeliveryConstant.autoassigncasweightname);
			Assert.assertTrue(true);
		}
	}
	@Test(dataProvider = "QE-254",priority=26, groups = "assignmentregression", description = "Verify that already busy DE is not considered for assignment")
	public void autoassigntest254(String order_id, String de_id,
								  String version, String lat, String lng) throws InterruptedException {
		Assert.assertNotNull(order_id,
				"Order is not created through checkout helper");
		rtshelp.runassignment(RTSConstants.cityid);
		String query = "Select * from trips where order_id=" + order_id + ";";
		Object assigndeid = deliveryHelperMethods.dbhelperget(query, "de_id");
		if ((null == assigndeid)
				|| (!de_id.equalsIgnoreCase(assigndeid.toString()))) {
			deliveryServiceHelper.makeDEFree(de_id);
			Assert.assertTrue(true);
		} else {
			deliveryServiceHelper.makeDEFree(de_id);
			Assert.assertTrue(false);
		}
	}
	@Test(dataProvider = "QE-255",priority=27, groups = "assignmentregression", description = "Verify that if the order is placed by a new customer than order is not considered for batching and also, there is no delay added for this order")
	public void autoassigntest255(String order_id, String order_id1,
								  String de_id, String version, String lat, String lng)
			throws InterruptedException {
		Assert.assertNotNull(order_id,
				"Order is not created through checkout helper");
		deliveryServiceHelper.makeDEFree(de_id);
		String auth1=deliveryDataHelper.delocationupdate(lat,lng, de_id, version);
		rtshelp.runassignment(RTSConstants.cityid);
		String query = "Select * from trips where order_id=" + order_id + ";";
		Object assigndeid = deliveryHelperMethods.dbhelperget(query, "de_id");
		if ((null == assigndeid)
				|| (!de_id.equalsIgnoreCase(assigndeid.toString()))) {
			deliveryServiceHelper.makeDEFree(de_id);
			deliveryServiceHelper.deLogout(auth1);
			Assert.assertTrue(false);
		} else {
			deliveryServiceHelper.makeDEFree(de_id);
			deliveryServiceHelper.deLogout(auth1);
			Assert.assertTrue(true);
		}
	}
	@Test(dataProvider = "QE-257",priority=28, groups = "assignmentregression", description = "Verify that Weight of Order Delay is calculated for an order when order delay is greater than the delay threshold")
	public void autoassigntest257(String order_id, String order_id1,
								  String de_id, String version, String lat, String lng)
			throws InterruptedException {
		Assert.assertNotNull(order_id,
				"Order is not created through checkout helper");
		deliveryServiceHelper.makeDEFree(de_id);
		deliveryServiceHelper.makeDEActive(de_id);
		String auth1=deliveryDataHelper.delocationupdate(lat,lng, de_id, version);
		rtshelp.runassignment(RTSConstants.cityid);
		String query = "Select * from trips where order_id=" + order_id1 + ";";
		Object assigndeid = deliveryHelperMethods.dbhelperget(query, "de_id");
		if (null == assigndeid || !de_id.equalsIgnoreCase(assigndeid.toString())) {
			deliveryServiceHelper.makeDEFree(de_id);
			deliveryServiceHelper.deLogout(auth1);
			autoassignHelper.updateParamAuto(DeliveryConstant.city, "34",
					DeliveryConstant.autoassign_orderdelaystepfactor,
					DeliveryConstant.autoassign_orderdelaystepfactorvaluemore,
					"test",
					DeliveryConstant.autoassign_orderdelaystepfactorname);
			autoassignHelper.updateParamAuto(DeliveryConstant.city, "34",
					DeliveryConstant.autoassign_weightorderdelayid,
					DeliveryConstant.autoassign_weightorderdelayless, "test",
					DeliveryConstant.autoassign_weightorderdelayname);
			Assert.assertTrue(true);
		} else {
			deliveryServiceHelper.makeDEFree(de_id);
			deliveryServiceHelper.deLogout(auth1);
			autoassignHelper.updateParamAuto(DeliveryConstant.city, "34",
					DeliveryConstant.autoassign_orderdelaystepfactor,
					DeliveryConstant.autoassign_orderdelaystepfactorvaluemore,
					"test",
					DeliveryConstant.autoassign_orderdelaystepfactorname);
			autoassignHelper.updateParamAuto(DeliveryConstant.city, "34",
					DeliveryConstant.autoassign_weightorderdelayid,
					DeliveryConstant.autoassign_weightorderdelayless, "test",
					DeliveryConstant.autoassign_weightorderdelayname);
			Assert.assertTrue(false);
		}
	}

	//Where is the cross area attributr is being set here Preetesh
	// *************************************************************************************************************
	@Test(dataProvider = "QE-259",priority=29, enabled = true, groups = {
			"assignmentregression", "Karunakar" }, description = "Verify whether DE is considered for assignment from both customer and restaurant zone for cross area")
	public void autoassigntest259(String order_id, String de_id, String version, String lat, String lng, String orderJson) {
        Boolean assign_status=checkDEAssignment(de_id,order_id,orderJson);
        Assert.assertTrue(assign_status);
/*		deliveryServiceHelper.makeDEActive(de_id);
		deliveryServiceHelper.makeDEFree(de_id);
		deliveryServiceHelper.makeDEFree(de_id);
		String auth1=deliveryDataHelper.delocationupdate(lat,lng, de_id, version);
		rtshelp.runassignment(RTSConstants.cityid);
		String query = DeliveryConstant.get_deid + order_id + ";";
		Object assigndeid = deliveryHelperMethods.dbhelperget(query, "de_id");
		if (assigndeid.toString().equalsIgnoreCase(de_id)) {
			deliveryServiceHelper.deLogout(auth1);
			Assert.assertTrue(true);
		} else {
			deliveryServiceHelper.makeDEFree(de_id);
			deliveryServiceHelper.deLogout(auth1);
			Assert.assertTrue(false);
		}*/
	}
//How do u know he is considered for assignment...Preetesh, We are just checking the assignment
	@Test(dataProvider = "QE-260",priority=30, groups = "assignmentregression", description = "Verify whether DE is considered for assignment when DE is considered from restaurant zone but customer zone is not one of DE service zone")
	public void autoassigntest260(String order_id, String de_id, String version, String lat, String lng, String orderJson) throws InterruptedException {

	    String query1 = "update trips set customer_zone=35 where order_id="
				+ order_id + ";";
        Boolean assign_status=checkDEAssignment(de_id,order_id,orderJson);
        Assert.assertTrue(assign_status);
		/*deliveryHelperMethods.dbhelperupdate(query1);
		deliveryServiceHelper.makeDEFree(de_id);
		String auth1=deliveryDataHelper.delocationupdate(lat,lng, de_id, version);
		rtshelp.runassignment(RTSConstants.cityid);
		String query = "Select * from trips where order_id=" + order_id + ";";
		Object assigndeid = deliveryHelperMethods.dbhelperget(query, "de_id");
		if (null == assigndeid || !de_id.equalsIgnoreCase(assigndeid.toString())) {
			deliveryServiceHelper.makeDEFree(de_id);
			deliveryServiceHelper.deLogout(auth1);
			Assert.assertTrue(false);
		} else {
			deliveryServiceHelper.makeDEFree(de_id);
			deliveryServiceHelper.deLogout(auth1);
			Assert.assertTrue(true);
		}*/
	}
	@Test(dataProvider = "QE-261",priority=31, enabled = true, groups = {
			"assignmentregression", "Karunakar" }, description = "Verify whether DE is considered for assignment when both restaurant and customer address falls in same zone")
	public void autoassigntest261(String order_id, String de_id,
								  String version, String lat, String lng,String orderJson) {
        Boolean assign_status=checkDEAssignment(de_id,order_id,orderJson);
        Assert.assertTrue(assign_status);
/*		deliveryServiceHelper.makeDEActive(de_id);
		deliveryServiceHelper.makeDEFree(de_id);
		String auth1=deliveryDataHelper.delocationupdate(lat,lng, de_id, version);
		rtshelp.runassignment(RTSConstants.cityid);
		String query = DeliveryConstant.get_deid + order_id + ";";
		Object assigndeid = deliveryHelperMethods.dbhelperget(query, "de_id");
		if (null == assigndeid || !assigndeid.toString().equalsIgnoreCase(de_id)) {
			deliveryServiceHelper.makeDEFree(de_id);
			deliveryServiceHelper.deLogout(auth1);
			Assert.assertTrue(false);
		} else {
			deliveryServiceHelper.makeDEFree(de_id);
			deliveryServiceHelper.deLogout(auth1);
			Assert.assertTrue(true);
		}*/
	}
	@Test(dataProvider = "cyclede12",priority=32, groups = "assignmentregression", description = "Verify order is  not assigned to cycle DE if last mile distance is more than Max last mile distance for Cycle DEs")
	public void autoassigntest329(String order_id, String de_id, String version, String lat, String lng, String orderJson, String zone_id, String city_id) {
        Boolean assign_status=checkDEAssignment(de_id,order_id,orderJson);
        Assert.assertFalse(assign_status);


	    /*Assert.assertNotNull(order_id1,
				"Order is not created through checkout helper");
		deliveryServiceHelper.makeDEFree(de_id);
		deliveryServiceHelper.makeDEActive(de_id);
		String auth1=deliveryDataHelper.delocationupdate(lat,lng, de_id, version);
		rtshelp.runassignment(RTSConstants.cityid);
		String query = "Select * from trips where order_id=" + order_id1 + ";";
		Object assigndeid = deliveryHelperMethods.dbhelperget(query, "de_id");
		if ((assigndeid == null)
				|| (!de_id.equalsIgnoreCase(assigndeid.toString()))) {
			System.out.println("de_id is null/Not assigned to cycle de");
			deliveryServiceHelper.makeDEFree(de_id);
			deliveryServiceHelper.deLogout(auth1);
			deliveryServiceHelper.markDEInactive(de_id, "1");
			Assert.assertTrue(true);
		}
		else {
			deliveryServiceHelper.makeDEFree(de_id);
			deliveryServiceHelper.deLogout(auth1);
			deliveryServiceHelper.markDEInactive(de_id, "1");
			Assert.assertTrue(false);
		}*/
	}
}