package com.swiggy.api.erp.cms.constants;

import java.time.Instant;
import com.swiggy.api.erp.cms.helper.CatalogV2Helper;

/**
 * Created by Suresh.halli on 15/11/18.
 */
public interface CatalogV2Constants {
	 CatalogV2Helper catalogV2Helper=new CatalogV2Helper();
    enum negativejson {
        empty,
        entity,
        maincategory,
        items
        
    }

    String description="Category description";
    String attribute_name="brand";
    String type="STRING";
    String[] allowedValues={"String"+String.valueOf(Instant.now().getEpochSecond())};
    String min="1";
    String max="100";
    String templateId="string-length-range";
    String[] themes={"basic","de-dup"};
    String template_url="string";

    String cgst_expression="";
    String igst_expression="";
    String sgst_expression="";
    boolean inclusive=false;
    String packaging_expression="";
    String price="8";
    String mrp="10";
    boolean enabled=true;
    String sku_id= "SKU_ID"+String.valueOf(Instant.now().getEpochSecond());
    String store_id="STORE_AUTOMATION_001"+String.valueOf(Instant.now().getEpochSecond());
    String key="100-758";
    String sku_cgst="($item_price_after_td)*0.02";
    String sku_igst= "($item_price_after_td)*0.02";
    String sku_packaging_charges= "1";
    String sku_sgst_utgst="($item_price_after_td)*0.02";
    String quantity="10";
    String s3url="https://s3-ap-southeast-1.amazonaws.com/cms-file-uploads-cuat/Test/Confidencd+Level+(7).xlsx";
    String ExpectGetSkuResult1="SKU with ID '";
    String ExpectGetSkuResult2="' doesn't exist";
	String ExpectGetSpinResult1="Product sku with spin: ";
    String ExpectGetSpinResult2=" already exists";

}
