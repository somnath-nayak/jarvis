package com.swiggy.api.erp.cms.pojo.SelfServeV1Item;

public class Addon_combinations {
	 private String third_party_addon_id;

	    private String group_id;

	    private String price;

	    private String third_party_group_id;

	    private String addon_id;

	    public String getThird_party_addon_id ()
	    {
	        return third_party_addon_id;
	    }

	    public void setThird_party_addon_id (String third_party_addon_id)
	    {
	        this.third_party_addon_id = third_party_addon_id;
	    }

	    public String getGroup_id ()
	    {
	        return group_id;
	    }

	    public void setGroup_id (String group_id)
	    {
	        this.group_id = group_id;
	    }

	    public String getPrice ()
	    {
	        return price;
	    }

	    public void setPrice (String price)
	    {
	        this.price = price;
	    }

	    public String getThird_party_group_id ()
	    {
	        return third_party_group_id;
	    }

	    public void setThird_party_group_id (String third_party_group_id)
	    {
	        this.third_party_group_id = third_party_group_id;
	    }

	    public String getAddon_id ()
	    {
	        return addon_id;
	    }

	    public void setAddon_id (String addon_id)
	    {
	        this.addon_id = addon_id;
	    }

	    @Override
	    public String toString()
	    {
	        return "ClassPojo [third_party_addon_id = "+third_party_addon_id+", group_id = "+group_id+", price = "+price+", third_party_group_id = "+third_party_group_id+", addon_id = "+addon_id+"]";
	    }
	}
				
				