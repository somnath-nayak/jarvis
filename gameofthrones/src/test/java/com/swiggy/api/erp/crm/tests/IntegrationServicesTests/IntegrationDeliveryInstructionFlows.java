package com.swiggy.api.erp.crm.tests.IntegrationServicesTests;

import com.swiggy.api.erp.crm.constants.CRMConstants;
import com.swiggy.api.erp.crm.constants.FlowMapper;
import com.swiggy.api.erp.crm.dp.MockServiceDP;
import com.swiggy.api.erp.crm.helper.CRMUpstreamValidation;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.RedisHelper;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static org.testng.Assert.assertTrue;

public class IntegrationDeliveryInstructionFlows  extends MockServiceDP {

    FlowMapper flowMapper = new FlowMapper();
    Initialize gameofthrones = new Initialize();
    CRMUpstreamValidation crmUpstreamValidation = new CRMUpstreamValidation();

    @Test(dataProvider = "deliveryInstructionFlows", groups = {"regression"}, description = "End to end tests for Delivery Instruction")
    public void integrationDeliveryInstructionTests(HashMap<String, String> flowDetails) throws IOException, InterruptedException {
        HashMap<String, String> requestheaders_inbound = new HashMap<String, String>();
        requestheaders_inbound.put("Content-Type", "application/json");
        String ffStatus = flowDetails.get("ffStatus").toString();
        String deliveryStatus = flowDetails.get("delivery_status").toString();
        String flowName = flowDetails.get("flow").toString();
        String test = flowDetails.get("test").toString();
        String nodeId;
        String orderId;// = flowDetails.get("order").toString();
        String[] flow = flowMapper.setFlow().get(flowName);
        String queryparam[] = new String[1];
        int lengthofflow = flow.length;

//      Processor logginResponse = SnDHelper.consumerLogin("7507220659","Test@2211");
        String customerId = "5769085";// logginResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.customer_id");//"387395";
//       redisHelper.setValueJson("checkoutredis", 0, "user_credit_7191663", "{\"userId\":"+customerId+",\"swiggyMoney\":0.0,\"cancellationFee\":0.0}");
//            String getValue = (String) redisHelper.getValue("checkoutredis", 0, "user_credit_7191663");
//            System.out.println("key value" + getValue);

        // HashMap<String, String> map = crmUpstreamValidation.e2e("8884292249", "welcome123", "7053", "7030572", "1", deliveryStatus, ffStatus);
        orderId = flowDetails.get("orderId").toString(); //map.get("order_id");//1055952078, "19838855249";
        Assert.assertNotNull(orderId);
        String conversationId = orderId;
        String deviceId = orderId;

        int cnt = lengthofflow - 1;
        for (int i = 0; i < lengthofflow - 1; i++) {
            nodeId = flow[i];
            if (nodeId == "1") {
                continue;
            } else {
                String nextNodeId = flow[i + 1];
                queryparam[0] = nodeId;
                String[] paylaodparam = new String[]{conversationId, customerId, orderId};

                System.out.println("length of flow" + lengthofflow);
                GameOfThronesService deliveryInstruction = new GameOfThronesService("crm", "isValidDeliveryInstr", gameofthrones);

                Processor deliveryInstructionResponse = new Processor(deliveryInstruction,
                        requestheaders_inbound, paylaodparam, queryparam);

                Assert.assertEquals(deliveryInstructionResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("status"),
                        "SUCCESS", "Actual status code doesnt match with actual status code ");

                String addOnGroupList = deliveryInstructionResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.childrenNodes..id").toString().replace("[", "").replace("]", "");
                List<String> addOnlist = Arrays.asList(addOnGroupList.split(","));

                int index = addOnlist.indexOf(flow[i + 1]);

                System.out.println("addonlist" + addOnlist);
                System.out.println("flowname[i+1]" + flow[i + 1]);

                if (test.equals("negative")) {
                    if (i == cnt)
                        Assert.assertEquals(!addOnlist.contains(nextNodeId), true, "Failure in Negative scenario ");
                    else
                        Assert.assertEquals(addOnlist.contains(nextNodeId), true, "Failure in Negative scenario ");

                } else {
                    if (i < (lengthofflow - 2)) {
                        Assert.assertEquals(deliveryInstructionResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray(
                                "data.childrenNodes.[" + index + "].isLeaf"), false);
                    } else {
                        Assert.assertEquals(deliveryInstructionResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray(
                                "data.childrenNodes.[" + index + "].isLeaf"), true);
                    }
                }
            }
        }
    }
}