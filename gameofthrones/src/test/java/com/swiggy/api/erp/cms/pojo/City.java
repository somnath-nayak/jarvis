package com.swiggy.api.erp.cms.pojo;

import com.swiggy.api.erp.cms.constants.CityConstants;

/**
 * Created by kiran.j on 2/1/18.
 */
public class City {

    private String name;
    private int open_time;
    private int close_time;
    private int enabled;
    private String banner_message;
    private int max_delivery_time;
    private int min_delivery_time;
    private String restaurant_helpline;
    private String slug;
    private int is_open;
    private String db_helpline;
    private String customer_care_number;
    private String north_east_lat_lng;
    private String south_west_lat_lng;
    private String state;
    private int show_ratings;
    private int delivery_charge_threshold;
    private int cap_on_cod_restriction;
    private double cap_on_cod_min_value;
    private String cap_on_cod_message;
    private double compulsory_delivery_charge;
    private float delivery_radius;
    private String closed_reason;
    private int global_flat_fee_applicable;
    private int restaurant_sla_feature_gate;
    private int cart_sla_feature_gate;
    private int assured_max_order_value;
    private int assured_enabled;
    private double long_distance_solr_radius;
    private int id;

    public City() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOpen_time() {
        return open_time;
    }

    public void setOpen_time(int open_time) {
        this.open_time = open_time;
    }

    public int getClose_time() {
        return close_time;
    }

    public void setClose_time(int close_time) {
        this.close_time = close_time;
    }

    public int getEnabled() {
        return enabled;
    }

    public void setEnabled(int enabled) {
        this.enabled = enabled;
    }

    public String getBanner_message() {
        return banner_message;
    }

    public void setBanner_message(String banner_message) {
        this.banner_message = banner_message;
    }

    public int getMax_delivery_time() {
        return max_delivery_time;
    }

    public void setMax_delivery_time(int max_delivery_time) {
        this.max_delivery_time = max_delivery_time;
    }

    public int getMin_delivery_time() {
        return min_delivery_time;
    }

    public void setMin_delivery_time(int min_delivery_time) {
        this.min_delivery_time = min_delivery_time;
    }

    public String getRestaurant_helpline() {
        return restaurant_helpline;
    }

    public void setRestaurant_helpline(String restaurant_helpline) {
        this.restaurant_helpline = restaurant_helpline;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public int getIs_open() {
        return is_open;
    }

    public void setIs_open(int is_open) {
        this.is_open = is_open;
    }

    public String getDb_helpline() {
        return db_helpline;
    }

    public void setDb_helpline(String db_helpline) {
        this.db_helpline = db_helpline;
    }

    public String getCustomer_care_number() {
        return customer_care_number;
    }

    public void setCustomer_care_number(String customer_care_number) {
        this.customer_care_number = customer_care_number;
    }

    public String getNorth_east_lat_lng() {
        return north_east_lat_lng;
    }

    public void setNorth_east_lat_lng(String north_east_lat_lng) {
        this.north_east_lat_lng = north_east_lat_lng;
    }

    public String getSouth_west_lat_lng() {
        return south_west_lat_lng;
    }

    public void setSouth_west_lat_lng(String south_west_lat_lng) {
        this.south_west_lat_lng = south_west_lat_lng;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int isShow_ratings() {
        return show_ratings;
    }

    public void setShow_ratings(int show_ratings) {
        this.show_ratings = show_ratings;
    }

    public int getDelivery_charge_threshold() {
        return delivery_charge_threshold;
    }

    public void setDelivery_charge_threshold(int delivery_charge_threshold) {
        this.delivery_charge_threshold = delivery_charge_threshold;
    }

    public int isCap_on_cod_restriction() {
        return cap_on_cod_restriction;
    }

    public void setCap_on_cod_restriction(int cap_on_cod_restriction) {
        this.cap_on_cod_restriction = cap_on_cod_restriction;
    }

    public double getCap_on_cod_min_value() {
        return cap_on_cod_min_value;
    }

    public void setCap_on_cod_min_value(double cap_on_cod_min_value) {
        this.cap_on_cod_min_value = cap_on_cod_min_value;
    }

    public String getCap_on_cod_message() {
        return cap_on_cod_message;
    }

    public void setCap_on_cod_message(String cap_on_cod_message) {
        this.cap_on_cod_message = cap_on_cod_message;
    }

    public double getCompulsory_delivery_charge() {
        return compulsory_delivery_charge;
    }

    public void setCompulsory_delivery_charge(double compulsory_delivery_charge) {
        this.compulsory_delivery_charge = compulsory_delivery_charge;
    }

    public float getDelivery_radius() {
        return delivery_radius;
    }

    public void setDelivery_radius(float delivery_radius) {
        this.delivery_radius = delivery_radius;
    }

    public String getClosed_reason() {
        return closed_reason;
    }

    public void setClosed_reason(String closed_reason) {
        this.closed_reason = closed_reason;
    }

    public int getGlobal_flat_fee_applicable() {
        return global_flat_fee_applicable;
    }

    public void setGlobal_flat_fee_applicable(int global_flat_fee_applicable) {
        this.global_flat_fee_applicable = global_flat_fee_applicable;
    }

    public int isRestaurant_sla_feature_gate() {
        return restaurant_sla_feature_gate;
    }

    public void setRestaurant_sla_feature_gate(int restaurant_sla_feature_gate) {
        this.restaurant_sla_feature_gate = restaurant_sla_feature_gate;
    }

    public int isCart_sla_feature_gate() {
        return cart_sla_feature_gate;
    }

    public void setCart_sla_feature_gate(int cart_sla_feature_gate) {
        this.cart_sla_feature_gate = cart_sla_feature_gate;
    }

    public int getAssured_max_order_value() {
        return assured_max_order_value;
    }

    public void setAssured_max_order_value(int assured_max_order_value) {
        this.assured_max_order_value = assured_max_order_value;
    }

    public int isAssured_enabled() {
        return assured_enabled;
    }

    public void setAssured_enabled(int assured_enabled) {
        this.assured_enabled = assured_enabled;
    }

    public double getLong_distance_solr_radius() {
        return long_distance_solr_radius;
    }

    public void setLong_distance_solr_radius(double long_distance_solr_radius) {
        this.long_distance_solr_radius = long_distance_solr_radius;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void build() {
        if(this.getName() == null)
            this.setName(CityConstants.name);
        if(this.getOpen_time() == 0)
            this.setOpen_time(CityConstants.open_time);
        if(this.getClose_time() == 0)
            this.setClose_time(CityConstants.close_time);
        if(this.getEnabled() == 0)
            this.setEnabled(CityConstants.enabled);
        if(this.getBanner_message() == null)
            this.setBanner_message(CityConstants.banner_message);
        if(this.getMax_delivery_time() == 0)
            this.setMax_delivery_time(CityConstants.max_delivery_time);
        if(this.getMin_delivery_time() == 0)
            this.setMin_delivery_time(CityConstants.min_delivery_time);
        if(this.getRestaurant_helpline() == null)
            this.setRestaurant_helpline(CityConstants.help_line);
        if(this.getSlug() == null)
            this.setSlug(CityConstants.slug);
        if(this.getState() == null)
            this.setState(CityConstants.state);
        if(this.getIs_open() == 0)
            this.setIs_open(CityConstants.is_open);
        if(this.getCustomer_care_number() == null)
            this.setCustomer_care_number(CityConstants.customer_care);
        if(this.getDb_helpline() == null)
            this.setDb_helpline(CityConstants.db_helpline);
        if(this.getDelivery_radius() == 0)
            this.setDelivery_radius(CityConstants.delivery_radius);
        if(this.getCap_on_cod_message() == null)
            this.setCap_on_cod_message(CityConstants.cap_on_cod);
        this.setAssured_enabled(CityConstants.enabled);
    }
}
