package com.swiggy.api.erp.ff.helper;

import com.rabbitmq.client.AMQP;
import com.swiggy.api.erp.ff.constants.OMSConstants;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Tyrion.RabbitMQHelper;

public class WatchdogHelper {
	
	Initialize gameofthrones = new Initialize();
	RabbitMQHelper rmqhelper  = new RabbitMQHelper();


	
    public String cancelledOrder(String order_id) {
        String message = "{\"cancellation_fee_recovered_flag\": \"\",\"order_cancelled_at\": 1528107157000,\"cancellation_fee_base_order_id\": null,\"cancellation_fee_recovered_order_id\": \"\",	\"responsible_id\": \"3\",\"cancellation_fee_applied\": \"0\",\"food_prepared\": \"false\",\"cancellation_reason\": \"rest_cancel\",\"cancellation_sub_reason\": \"rest_cancel\",\"cancellation_refund_amount\": \"147.0\",\"cancellation_fee_collection_source\": \"Prepaid\",\"initiator_id\": \"3\",\"order_id\": "+order_id+",\"status\": \"cancel\"}";
        rmqhelper.pushMessage(OMSConstants.SERVICE, OMSConstants.WATCHDOG_CANCELLED_ORDER, new AMQP.BasicProperties().builder().contentType("application/json"), message);
        return message;
    }

}
