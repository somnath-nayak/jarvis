package com.swiggy.api.erp.cms.tests;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.dp.slotsDP;
import com.swiggy.api.erp.cms.helper.CMSHelper;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.Processor;

public class DeleteSlots extends slotsDP {
	Initialize gameofthrones = new Initialize();
	CMSHelper cmshelper = new CMSHelper();

	@Test(dataProvider = "slotCreation")
	// public void createTag_method()
	public void RestaurantSlots(String restaurant_id, String open_time, String close_time, String day) {

		System.out.println(
				"***************************************** Delete slot creation started *****************************************");

		Processor p = cmshelper.setRestaurantSlot(restaurant_id, open_time, close_time, day);

		Assert.assertEquals(1, p.ResponseValidator.GetNodeValueAsInt("status"));

		String resp = p.ResponseValidator.GetBodyAsText();
		int message = JsonPath.read(resp, "$.status");
		System.out.println(">>>>>>>>" + message);
		System.out.println(
				"######################################### Delete Restaurant slot creation compleated #########################################");
	}

}
