package com.swiggy.api.erp.cms.tests;

import com.swiggy.api.castleblack.SlackMessenger;
import com.swiggy.api.erp.cms.constants.CmsConstants;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Tyrion.RabbitMQHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class TestRestaurantsInProd {
    Initialize gameofthrones = new Initialize();
    RabbitMQHelper helper = new RabbitMQHelper();
    SqlTemplate sqlTemplateCI = SystemConfigProvider.getTemplate("prodcatalog");
    @Test(description = "Fetch test restaurants from prod DB")
    public void testRestaurants() throws IOException {
        List<Map<String, Object>> test = sqlTemplateCI.queryForList(CmsConstants.fetchTestRestaurants);
        String restau="```"+test.toString()+"```";
        SlackMessenger.webHookSlack(restau,"#test_prod_restaurants");
        Assert.assertNotNull(restau);

    }
}
