package com.swiggy.api.erp.cms.tests.CatalogV2;

import com.swiggy.api.erp.cms.dp.CatalogV2CategoryDP;
import com.swiggy.api.erp.cms.helper.CatalogV2Helper;
import org.testng.annotations.Test;

public class TestCategory extends CatalogV2CategoryDP {
    CatalogV2Helper catalogV2Helper=new CatalogV2Helper();

    @Test(dataProvider = "createcategory",description ="create category API" )
    public void createCategory(String name) {
        //String response = cmsHelper.updateItem(comment, description, name, itemId).ResponseValidator.GetBodyAsText();
        String response=catalogV2Helper.createCategory(name).ResponseValidator.GetBodyAsText();
        //id=JsonPath.read(response,"$.id");
        //Assert.assertNotNull(id, "Id is Null");
    }
}
