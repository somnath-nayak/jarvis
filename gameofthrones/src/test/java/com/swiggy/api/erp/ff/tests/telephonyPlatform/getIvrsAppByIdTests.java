package com.swiggy.api.erp.ff.tests.telephonyPlatform;

import com.swiggy.api.erp.ff.dp.telephonyPlatform.TelephonyPlatformTestData;
import com.swiggy.api.erp.ff.helper.TelephonyPlatformHelper;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.HashMap;

public class getIvrsAppByIdTests extends TelephonyPlatformTestData{
    TelephonyPlatformHelper helper = new TelephonyPlatformHelper();

    @Test(dataProvider = "getIvrsAppByIdData", description = "Test get IVRS App by ID api of telephony to get apps present in the telephony platform by ID")
    public void getIvrsAppByIdAPITests(String appId, HashMap<String, String> expectedResponseMap){
        System.out.println("***************************************** getIvrsAppByIdAPITests started *****************************************");

        Processor response = helper.getIvrsAppById(appId);
        System.out.println(response.ResponseValidator.GetBodyAsText());
        Assert.assertEquals(response.ResponseValidator.GetResponseCode(), Integer.parseInt(expectedResponseMap.get("responseCode")));
        if(response.ResponseValidator.GetResponseCode() == 200){
            Assert.assertEquals(response.ResponseValidator.GetNodeValue("appName"), expectedResponseMap.get("appName"));
            Assert.assertEquals(response.ResponseValidator.GetNodeValueAsInt("appId"), Integer.parseInt(expectedResponseMap.get("appId")));
            Assert.assertEquals(response.ResponseValidator.GetNodeValueAsInt("clientId"), Integer.parseInt(expectedResponseMap.get("clientId")));
            Assert.assertEquals(response.ResponseValidator.GetNodeValue("createdBy"), expectedResponseMap.get("createdBy"));
        }

        System.out.println("######################################### getIvrsAppByIdAPITests compleated #########################################");
    }
}
