package com.swiggy.api.erp.cms.pojo.SelfServeAddNewItem;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.erp.cms.pojo.SelfServeAddNewItem
 **/
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.swiggy.api.erp.cms.helper.BaseServiceHelper;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "description",
        "in_stock",
        "is_perishable",
        "is_spicy",
        "is_veg",
        "name",
        "packing_charges",
        "packing_slab_count",
        "price",
        "serves_how_many",
        "service_charges",
        "item_change_request",
        "s3_image_url",
        "main_category_id",
        "category_id",
        "gst_details"
})
public class AddNewItemForRestaurant {

    BaseServiceHelper baseServiceHelper = new BaseServiceHelper();
    @JsonProperty("description")
    private String description;
    @JsonProperty("in_stock")
    private Integer in_stock;
    @JsonProperty("is_perishable")
    private Integer is_perishable;
    @JsonProperty("is_spicy")
    private Integer is_spicy;
    @JsonProperty("is_veg")
    private Integer is_veg;
    @JsonProperty("name")
    private String name;
    @JsonProperty("packing_charges")
    private Integer packing_charges;
    @JsonProperty("packing_slab_count")
    private Integer packing_slab_count;
    @JsonProperty("price")
    private Integer price;
    @JsonProperty("serves_how_many")
    private Integer serves_how_many;
    @JsonProperty("service_charges")
    private String service_charges;
    @JsonProperty("addons_and_variant_change_request")
    private String addons_and_variant_change_request;
    @JsonProperty("item_change_request")
    private String item_change_request;
    @JsonProperty("s3_image_url")
    private String s3_image_url;
    @JsonProperty("main_category_id")
    private String main_category_id;
    @JsonProperty("category_id")
    private String category_id;
    @JsonProperty("gst_details")
    private Gst_details gst_details;

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("in_stock")
    public Integer getIn_stock() {
        return in_stock;
    }

    @JsonProperty("in_stock")
    public void setIn_stock(Integer in_stock) {
        this.in_stock = in_stock;
    }

    @JsonProperty("is_perishable")
    public Integer getIs_perishable() {
        return is_perishable;
    }

    @JsonProperty("is_perishable")
    public void setIs_perishable(Integer is_perishable) {
        this.is_perishable = is_perishable;
    }

    @JsonProperty("is_spicy")
    public Integer getIs_spicy() {
        return is_spicy;
    }

    @JsonProperty("is_spicy")
    public void setIs_spicy(Integer is_spicy) {
        this.is_spicy = is_spicy;
    }

    @JsonProperty("is_veg")
    public Integer getIs_veg() {
        return is_veg;
    }

    @JsonProperty("is_veg")
    public void setIs_veg(Integer is_veg) {
        this.is_veg = is_veg;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("packing_charges")
    public Integer getPacking_charges() {
        return packing_charges;
    }

    @JsonProperty("packing_charges")
    public void setPacking_charges(Integer packing_charges) {
        this.packing_charges = packing_charges;
    }

    @JsonProperty("packing_slab_count")
    public Integer getPacking_slab_count() {
        return packing_slab_count;
    }

    @JsonProperty("packing_slab_count")
    public void setPacking_slab_count(Integer packing_slab_count) {
        this.packing_slab_count = packing_slab_count;
    }

    @JsonProperty("price")
    public Integer getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(Integer price) {
        this.price = price;
    }

    @JsonProperty("serves_how_many")
    public Integer getServes_how_many() {
        return serves_how_many;
    }

    @JsonProperty("serves_how_many")
    public void setServes_how_many(Integer serves_how_many) {
        this.serves_how_many = serves_how_many;
    }

    @JsonProperty("service_charges")
    public String getService_charges() {
        return service_charges;
    }

    @JsonProperty("service_charges")
    public void setService_charges(String service_charges) {
        this.service_charges = service_charges;
    }

    @JsonProperty("addons_and_variant_change_request")
    public String getAddons_and_variant_change_request() {
        return addons_and_variant_change_request;
    }

    @JsonProperty("addons_and_variant_change_request")
    public void setAddons_and_variant_change_request(String addons_and_variant_change_request) {
        this.addons_and_variant_change_request = addons_and_variant_change_request;
    }

    @JsonProperty("item_change_request")
    public String getItem_change_request() {
        return item_change_request;
    }

    @JsonProperty("item_change_request")
    public void setItem_change_request(String item_change_request) {
        this.item_change_request = item_change_request;
    }

    @JsonProperty("s3_image_url")
    public String getS3_image_url() {
        return s3_image_url;
    }

    @JsonProperty("s3_image_url")
    public void setS3_image_url(String s3_image_url) {
        this.s3_image_url = s3_image_url;
    }

    @JsonProperty("main_category_id")
    public String getMain_category_id() {
        return main_category_id;
    }

    @JsonProperty("main_category_id")
    public void setMain_category_id(String main_category_id) {
        this.main_category_id = main_category_id;
    }

    @JsonProperty("category_id")
    public String getCategory_id() {
        return category_id;
    }

    @JsonProperty("category_id")
    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    @JsonProperty("gst_details")
    public Gst_details getGst_details() {
        return gst_details;
    }

    @JsonProperty("gst_details")
    public void setGst_details(Gst_details gst_details) {
        this.gst_details = gst_details;
    }

    private void setDefaultValues(String category, String subCategory, String name) {
        Gst_details gst_details = new Gst_details();

        if (this.getDescription() == null)
            this.setDescription("SelfServe Automation Item");
        if (this.getIn_stock() == null)
            this.setIn_stock(1);
        if (this.getIs_perishable() == null)
            this.setIs_perishable(1);
        if (this.getIs_spicy() == null)
            this.setIs_spicy(0);
        if (this.getIs_veg() == null)
            this.setIs_veg(0);
        if (this.getName() == null)
            this.setName(name);
        if (this.getPacking_charges() == null)
            this.setPacking_charges(30);
        if (this.getPacking_slab_count() == null)
            this.setPacking_slab_count(5);
        if (this.getPrice() == null)
            this.setPrice(150);
        if (this.getServes_how_many() ==  null)
            this.setServes_how_many(2);
        if (this.getService_charges() == null)
            this.setService_charges("20");
        if (this.getItem_change_request() == null)
            this.setItem_change_request("Item Change And New Test Item");
        if (this.getS3_image_url() == null)
            this.setS3_image_url("https://s3-ap-southeast-1.amazonaws.com/cms-file-uploads-cuat/images/items56502780-e275-4060-923e-aed5b2cf2c16-index.jpg");
        if (this.getMain_category_id() == null)
            this.setMain_category_id(category);
        if (this.getCategory_id() == null)
            this.setCategory_id(subCategory);
        if (this.getGst_details() == null)
            this.setGst_details(gst_details.build());
    }

    public AddNewItemForRestaurant build(String category, String subCategory, String name) {
        setDefaultValues(category, subCategory, name);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("description", description).append("in_stock", in_stock).append("is_perishable", is_perishable).append("is_spicy", is_spicy).append("is_veg", is_veg).append("name", name).append("packing_charges", packing_charges).append("packing_slab_count", packing_slab_count).append("price", price).append("serves_how_many", serves_how_many).append("service_charges", service_charges).append("item_change_request", item_change_request).append("s3_image_url", s3_image_url).append("main_category_id", main_category_id).append("category_id", category_id).append("gst_details", gst_details).toString();
    }

}
