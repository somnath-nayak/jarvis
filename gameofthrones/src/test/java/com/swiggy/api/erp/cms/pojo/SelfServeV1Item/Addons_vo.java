package com.swiggy.api.erp.cms.pojo.SelfServeV1Item;

public class Addons_vo {
	private Addon addon;

    public Addon getAddon ()
    {
        return addon;
    }

    public void setAddon (Addon addon)
    {
        this.addon = addon;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [addon = "+addon+"]";
    }
}
			
			