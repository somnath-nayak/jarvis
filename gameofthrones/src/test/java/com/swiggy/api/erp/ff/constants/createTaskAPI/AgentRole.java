package com.swiggy.api.erp.ff.constants.createTaskAPI;

public enum AgentRole {

    L1_PLACER("L1Placer", 1), L2_PLACER("L2Placer", 2), CALLDE_OE("CallDE_OE", 3), VERIFIER("Verifier", 4);

    private String description;
    private int id;

    AgentRole(String description, int id) {
        this.description = description;
        this.id = id;
    }

    public String toString() {
        return this.description;
    }

    public int getId() {
        return id;
    }
}
