package com.swiggy.api.erp.cms.pojo;

import com.swiggy.api.erp.cms.constants.MenuConstants;

/**
 * Created by kiran.j on 3/13/18.
 */
public class CreateAddonGroup
{
    private String created_by;

    private int addon_free_limit;

    private String id;

    private String third_party_id;

    private String updated_at;

    private int order;

    private int addon_limit;

    private String item_id;

    private String name;

    private String updated_by;

    private int active;

    private String created_at;

    private int addon_min_limit;

    public String getCreated_by ()
    {
        return created_by;
    }

    public void setCreated_by (String created_by)
    {
        this.created_by = created_by;
    }

    public int getAddon_free_limit ()
    {
        return addon_free_limit;
    }

    public void setAddon_free_limit (int addon_free_limit)
    {
        this.addon_free_limit = addon_free_limit;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getThird_party_id ()
    {
        return third_party_id;
    }

    public void setThird_party_id (String third_party_id)
    {
        this.third_party_id = third_party_id;
    }

    public String getUpdated_at ()
    {
        return updated_at;
    }

    public void setUpdated_at (String updated_at)
    {
        this.updated_at = updated_at;
    }

    public int getOrder ()
    {
        return order;
    }

    public void setOrder (int order)
    {
        this.order = order;
    }

    public int getAddon_limit ()
    {
        return addon_limit;
    }

    public void setAddon_limit (int addon_limit)
    {
        this.addon_limit = addon_limit;
    }

    public String getItem_id ()
    {
        return item_id;
    }

    public void setItem_id (String item_id)
    {
        this.item_id = item_id;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getUpdated_by ()
    {
        return updated_by;
    }

    public void setUpdated_by (String updated_by)
    {
        this.updated_by = updated_by;
    }

    public int getActive ()
    {
        return active;
    }

    public void setActive (int active)
    {
        this.active = active;
    }

    public String getCreated_at ()
    {
        return created_at;
    }

    public void setCreated_at (String created_at)
    {
        this.created_at = created_at;
    }

    public int getAddon_min_limit ()
    {
        return addon_min_limit;
    }

    public void setAddon_min_limit (int addon_min_limit)
    {
        this.addon_min_limit = addon_min_limit;
    }

    public CreateAddonGroup build() {
        if(this.getId() == null)
            this.setId(MenuConstants.addons_groups_id);
        if(this.getName() == null)
            this.setName(MenuConstants.addons_groups_name);
        if(this.getItem_id() == null)
            this.setItem_id(MenuConstants.items_id);

        this.setAddon_free_limit(MenuConstants.addon_groups_limit);
        this.setAddon_limit(MenuConstants.addon_groups_limit);


        return this;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [created_by = "+created_by+", addon_free_limit = "+addon_free_limit+", id = "+id+", third_party_id = "+third_party_id+", updated_at = "+updated_at+", order = "+order+", addon_limit = "+addon_limit+", item_id = "+item_id+", name = "+name+", updated_by = "+updated_by+", active = "+active+", created_at = "+created_at+", addon_min_limit = "+addon_min_limit+"]";
    }
}
