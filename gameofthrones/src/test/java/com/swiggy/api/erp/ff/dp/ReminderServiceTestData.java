package com.swiggy.api.erp.ff.dp;

import org.testng.annotations.DataProvider;

public class ReminderServiceTestData {
	
	@DataProvider(name = "setreminder")
	public Object[][] setreminder() throws Exception {
		return new Object[][] {
			{"service1","service2","this is a test message"},
			{"","service2","this is a test message"},
			{"service1","","this is a test message"},
			{"service1","service2",""}
		};
	}
	
	@DataProvider(name = "BadRequest")
	public Object[][] BadRequest() throws Exception {
		return new Object[][] {
			{"123213321"},
			{""},
			{"!@#$%^&*"},
			{"dfhsdkjhfk324"}
		};
	}

}
