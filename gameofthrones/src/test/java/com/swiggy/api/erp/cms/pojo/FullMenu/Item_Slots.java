package com.swiggy.api.erp.cms.pojo.FullMenu;

import com.swiggy.api.erp.cms.constants.MenuConstants;

/**
 * Created by kiran.j on 2/7/18.
 */
public class Item_Slots {

    private int open_time;

    private int day_of_week;

    private int close_time;

    public int getOpen_time() {
        return open_time;
    }

    public void setOpen_time(int open_time) {
        this.open_time = open_time;
    }

    public int getDay_of_week() {
        return day_of_week;
    }

    public void setDay_of_week(int day_of_week) {
        this.day_of_week = day_of_week;
    }

    public int getClose_time() {
        return close_time;
    }

    public void setClose_time(int close_time) {
        this.close_time = close_time;
    }

    public Item_Slots build() {
        setDefaultValues();
        return this;
    }

    public void setDefaultValues() {
        if(this.getOpen_time() == 0)
            this.setOpen_time(MenuConstants.open_time);
        if(this.getDay_of_week() == 0)
            this.setDay_of_week(MenuConstants.day_of_week);
        if(this.getClose_time() == 0)
            this.setClose_time(MenuConstants.close_time);
    }

    @Override
    public String toString() {
        return "{" +
                "open_time=" + open_time +
                ", day_of_week=" + day_of_week +
                ", close_time=" + close_time +
                '}';
    }
}
