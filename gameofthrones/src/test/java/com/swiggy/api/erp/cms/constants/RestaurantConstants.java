package com.swiggy.api.erp.cms.constants;

/**
 * Created by kiran.j on 1/22/18.
 */
public interface RestaurantConstants {

    enum pop_type {
        POP_ONLY,
        RAIN_ONLY,
        RAIN_AND_POP
    }

    String invoice_name = "invoicing_name";
    String city = "city";
    String pan_no = "pan_no";
    String updated_by = "updated_by";
    String name = "name";
    String locality = "locality";
    String bank_ifsc = "bank_ifsc_code";
    String invoicing_email = "invoicing_email";
    String bank_name = "bank_name";
    String area = "area";
    String beneficaiary_name = "benificiary_name";
    String bank_city = "bank_city";
    String z_id = "z_id";
    String bank_account_no = "bank_account_no";
    String commission_exp = "commission_exp";
    String mou = "mou";
    String address = "address";
    String tan_no = "tan_no";
    String pay_by_system_value = "pay_by_system_value";
    String cancelled_cheque = "cancelled_cheque";
    String agreement_type = "agreement_type";
    String restaurant = "Restaurants";
    String rest = "restaurant";
    String insert = "insert";
    String update = "update";
    int enabled = 1;
    int disable = 0;
    String partner_map_insert = "INSERT INTO `swiggy`.`restaurants_partner_map`\n" +
            "(`rest_id`,`partner_id`,`type_of_partner`,`otp`,`is_enabled`,`created_on`,`updated_on`,`active`,`created_at`,`created_by`,`updated_at`,`updated_by`,`partner_info`)\n" +
            "VALUES ";
    String s3_image = "https://swiggy-rest-item-image-upload.s3.amazonaws.com/item_image/417cc63a-1052-11e7-b791-06b53d688da3Friday_24_March_2017__10_55id24548.jpg";
    String image = "http://www.swiggy.in/product/cheesy-mex-fries-2";
    String image_id = "wvjgf0w7pfhivgjrsonk";
}
