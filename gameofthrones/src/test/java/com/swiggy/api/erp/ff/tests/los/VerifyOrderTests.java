package com.swiggy.api.erp.ff.tests.los;

import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.swiggy.api.erp.ff.constants.LosConstants;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.swiggy.api.erp.ff.helper.LOSHelper;
import framework.gameofthrones.Aegon.SystemConfigProvider;


public class VerifyOrderTests {

	Logger log = Logger.getLogger(VerifyOrderTests.class);

	@Test(groups = {"sanity","regression"})
	public void testRelayOrderFromManualRestaurantForVerification() throws SQLException, IOException, InterruptedException{
		log.info("***************************************** testRelayOrderFromManualRestaurantForVerification started *****************************************");

		Date date = new Date();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		DateFormat order_idFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		String order_id = order_idFormat.format(date);
		String order_time = dateFormat.format(date);
		log.info("Order time = "+order_time);
		String epochTime = String.valueOf(Instant.now().getEpochSecond());
		log.info("Epoch time = " +epochTime);
		LOSHelper losHelper = new LOSHelper();
		losHelper.relayManualOrderForVerification(order_id, order_time, epochTime);
		List<Map<String, Object>> orders = SystemConfigProvider.getTemplate(LosConstants.hostName).queryForList("select * from oms_rawdata where order_id = "+ order_id  +";");
		Assert.assertNotEquals(null, orders, "Order is not present in OMS DB");
		log.info("********* Order is created in OMS DB successfully *********");

		log.info("######################################### testRelayOrderFromManualRestaurantForVerification completed #########################################");
	}

	@Test(groups = {"sanity","regression"})
	public void testRelayOrderFromPartnerRestaurantTForVerification() throws SQLException, IOException, InterruptedException{
		log.info("***************************************** testRelayOrderFromPartnerRestaurantTForVerification started *****************************************");

		Date date = new Date();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		DateFormat order_idFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		String order_id = order_idFormat.format(date);
		String order_time = dateFormat.format(date);
		log.info("Order time = "+order_time);
		String epochTime = String.valueOf(Instant.now().getEpochSecond());
		log.info("Epoch time = " +epochTime);
		LOSHelper losHelper = new LOSHelper();
		losHelper.relayPartnerOrderForVerification(order_id, order_time, epochTime);
		List<Map<String, Object>> orders = SystemConfigProvider.getTemplate(LosConstants.hostName).queryForList("select * from oms_rawdata where order_id = "+ order_id  +";");
		Assert.assertNotEquals(null, orders, "Order is not present in OMS DB");
		log.info("********* Order is created in OMS DB successfully *********");

		log.info("######################################### testRelayOrderFromPartnerRestaurantTForVerification completed #########################################");
	}

	@Test(groups = {"sanity","regression"})
	public void testRelayOrderFromThirdPartyRestaurantForVerification() throws SQLException, IOException, InterruptedException{
		log.info("***************************************** testRelayOrderFromThirdPartyRestaurantForVerification started *****************************************");

		Date date = new Date();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		DateFormat order_idFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		String order_id = order_idFormat.format(date);
		String order_time = dateFormat.format(date);
		log.info("Order time = "+order_time);
		String epochTime = String.valueOf(Instant.now().getEpochSecond());
		log.info("Epoch time = " +epochTime);
		LOSHelper losHelper = new LOSHelper();
		losHelper.relayThirdPartyOrderForVerification(order_id, order_time, epochTime);
		List<Map<String, Object>> orders = SystemConfigProvider.getTemplate(LosConstants.hostName).queryForList("select * from oms_rawdata where order_id = "+ order_id  +";");
		Assert.assertNotEquals(null, orders, "Order is not present in OMS DB");
		log.info("********* Order is created in OMS DB successfully *********");

		log.info("######################################### testRelayOrderFromThirdPartyRestaurantForVerification completed #########################################");
	}
	

}
