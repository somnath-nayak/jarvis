package com.swiggy.api.erp.finance.helper;

import com.rabbitmq.client.AMQP;
import com.swiggy.api.erp.finance.constants.FinanceConstants;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Tyrion.RabbitMQHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Created by kiran.j on 7/23/18.
 */
public class DEHelper {

    RabbitMQHelper rmqHelper = new RabbitMQHelper();

    public String deAction(String action, String id)  {
        File file = new File("../Data/Payloads/JSON/fin_delivery");
        String create_de = null;
        try {
            create_de = FileUtils.readFileToString(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        create_de = create_de.replace("${id}", id).replace("${action}", action);
        rmqHelper.pushMessageToExchange(FinanceConstants.oms_queue, FinanceConstants.zone_exchange, new AMQP.BasicProperties().builder().contentType("application/json"), create_de);
        return id;
    }

    public List<Map<String, Object>> getDEFromDb() {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(FinanceConstants.db);
        List<Map<String, Object>> list = sqlTemplate.queryForList(FinanceConstants.get_de);
        return list;
    }

    public List<Map<String, Object>> getDeDetails(String de_id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(FinanceConstants.db);
        List<Map<String, Object>> list = sqlTemplate.queryForList(FinanceConstants.de_details + de_id);
        return list;
    }

}
