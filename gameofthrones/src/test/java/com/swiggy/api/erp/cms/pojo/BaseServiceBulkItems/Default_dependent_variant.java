package com.swiggy.api.erp.cms.pojo.BaseServiceBulkItems;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.erp.cms.pojo.BaseServiceBulkItems
 **/
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "group_id",
        "third_party_group_id",
        "third_party_variation_id",
        "variation_id"
})
public class Default_dependent_variant {

    @JsonProperty("group_id")
    private String group_id;
    @JsonProperty("third_party_group_id")
    private String third_party_group_id;
    @JsonProperty("third_party_variation_id")
    private String third_party_variation_id;
    @JsonProperty("variation_id")
    private String variation_id;

    @JsonProperty("group_id")
    public String getGroup_id() {
        return group_id;
    }

    @JsonProperty("group_id")
    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    @JsonProperty("third_party_group_id")
    public String getThird_party_group_id() {
        return third_party_group_id;
    }

    @JsonProperty("third_party_group_id")
    public void setThird_party_group_id(String third_party_group_id) {
        this.third_party_group_id = third_party_group_id;
    }

    @JsonProperty("third_party_variation_id")
    public String getThird_party_variation_id() {
        return third_party_variation_id;
    }

    @JsonProperty("third_party_variation_id")
    public void setThird_party_variation_id(String third_party_variation_id) {
        this.third_party_variation_id = third_party_variation_id;
    }

    @JsonProperty("variation_id")
    public String getVariation_id() {
        return variation_id;
    }

    @JsonProperty("variation_id")
    public void setVariation_id(String variation_id) {
        this.variation_id = variation_id;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("group_id", group_id).append("third_party_group_id", third_party_group_id).append("third_party_variation_id", third_party_variation_id).append("variation_id", variation_id).toString();
    }

}
