package com.swiggy.api.erp.cms.pojo.AddItemToRest;

import org.apache.commons.lang.builder.ToStringBuilder;

public class Ticket {

    private Integer id;

    /**
     * No args constructor for use in serialization
     *
     */
    public Ticket() {
    }

    /**
     *
     * @param id
     */
    public Ticket(Integer id) {
        super();
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).toString();
    }

}
