package com.swiggy.api.erp.cms.tests.catalogAttribute;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.dp.itemDP;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Tyrion.RabbitMQHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CreateItemStatusCode extends itemDP {

    Initialize gameofthrones = new Initialize();
    RabbitMQHelper helper=new RabbitMQHelper();
    CMSHelper cmsHelper= new CMSHelper();
    SqlTemplate sqlTemplateCI = SystemConfigProvider.getTemplate("cms");
    String itemName;
    int itmId;
    @Test(dataProvider="createitempj",description = "Verify create item API")
    public void createItem(String name){

        int responsecode=cmsHelper.createItem(name).ResponseValidator.GetResponseCode();
        Assert.assertEquals(200,responsecode,"Status code is not 200");


    }
}
