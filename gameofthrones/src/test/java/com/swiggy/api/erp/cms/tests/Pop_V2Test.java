package com.swiggy.api.erp.cms.tests;

import java.io.IOException;

import org.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.dp.SolvingForRainsDP;
import com.swiggy.api.erp.cms.helper.Popv2Helper;

import framework.gameofthrones.JonSnow.Processor;

public class Pop_V2Test extends SolvingForRainsDP {

	String itemId;
	Popv2Helper v2 = new Popv2Helper();

	@Test(dataProvider = "PopItemCreation", priority = 0, description = "Creating pop item and addin todays Schedule for the item")
	public void PopItemCreation(String rest_id, String popUsageType)
			throws IOException, JSONException, InterruptedException {

		Processor p = v2.PopItemCreation(Integer.parseInt(rest_id), popUsageType);
		String response = p.ResponseValidator.GetBodyAsText();
		Thread.sleep(5000);
		itemId = JsonPath.read(response, "$.data.uniqueId").toString();
		Assert.assertEquals(1, p.ResponseValidator.GetNodeValueAsInt("statusCode"),"Unable to Create Item");
		Processor p1 = v2.ItemSchedule(Integer.parseInt(itemId));
		Assert.assertEquals(1, p1.ResponseValidator.GetNodeValueAsInt("statusCode"),"Unable to Item Schedule");
		System.out.println(
				"***********************************created item" + itemId + "***********************************");

	}

	@Test(dataProvider = "itemScheduleAreaCREATEData", priority = 4, description = "Creating a new item-schedule-mapping for new area-schedule slot")
	public void createNewItemScheduleByNewAreaSchedule(int itemId)
			throws JSONException, IOException, InterruptedException {
		Processor p1 = v2.ItemSchedule(itemId);
		String response = p1.ResponseValidator.GetBodyAsText();
		Thread.sleep(5000);
		int statusCode = JsonPath.read(response, "$.statusCode");
		Assert.assertEquals(statusCode, 1,"Unable to creat SCHEDULE");

	}

}
