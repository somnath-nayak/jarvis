package com.swiggy.api.erp.ff.POJO;


import org.codehaus.jackson.annotate.JsonProperty;

public class SessionDetails {

    @JsonProperty("device_id")
    private String deviceId;
    @JsonProperty("user_agent")
    private String userAgent;
    @JsonProperty("timestamp")
    private String timestamp;

    /**
     * No args constructor for use in serialization
     *
     */
    public SessionDetails() {
    }

    /**
     *
     * @param timestamp
     * @param userAgent
     * @param deviceId
     */
    public SessionDetails(String deviceId, String userAgent, String timestamp) {
        super();
        this.deviceId = deviceId;
        this.userAgent = userAgent;
        this.timestamp = timestamp;
    }

    @JsonProperty("device_id")
    public String getDeviceId() {
        return deviceId;
    }

    @JsonProperty("device_id")
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    @JsonProperty("user_agent")
    public String getUserAgent() {
        return userAgent;
    }

    @JsonProperty("user_agent")
    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    @JsonProperty("timestamp")
    public String getTimestamp() {
        return timestamp;
    }

    @JsonProperty("timestamp")
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

}