package com.swiggy.api.erp.delivery.tests;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.erp.delivery.dp.ETA_ServiceDataProvider;
import com.swiggy.api.erp.delivery.helper.*;
import cucumber.api.java.eo.Do;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.KafkaHelper;
import framework.gameofthrones.Tyrion.RedisHelper;
import gherkin.lexer.Pa;
import io.advantageous.boon.core.Str;
import io.gatling.commons.stats.assertion.In;
import org.jbehave.core.context.ContextView;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import scala.Int;
import scala.xml.Null;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

/**
 * 	Created by somnath.nayak on 20/11/18.
 */

public class ETA_ServiceTest extends ETA_ServiceDataProvider {

    ETA_ServiceHelper eta_serviceHelper = new ETA_ServiceHelper();
    DeliveryDataHelper deliveryDataHelper = new DeliveryDataHelper();
    DeliveryServiceHelper deliveryServiceHelper = new DeliveryServiceHelper();
    DeliveryControllerHelper deliveryControllerHelper = new DeliveryControllerHelper();
    RedisHelper redisHelper = new RedisHelper();
    ObjectMapper mapper = new ObjectMapper();

    static String de_id = null;
    static String zone = null;
    static String area = null;
    boolean executeAfterMethod = true;

    /*@BeforeMethod
    public void DE_Creation(){
        zone =eta_serviceHelper.getRestaurantDetails().get("rest_zone");
        area =eta_serviceHelper.getRestaurantDetails().get("rest_area");
        de_id = deliveryDataHelper.CreateDE(Integer.parseInt(zone));
        deliveryDataHelper.updateDELocationInRedis(de_id);
    }

    @AfterMethod
    public void DE_Deletion(){
        if (!executeAfterMethod) {
            System.out.println("********** Skipping AfterMethod for this test **********");
        }else { deliveryDataHelper.DeleteDE(de_id,area); }
        executeAfterMethod=true;
    }*/

    //############################################### Delivery_ETA_Service ###############################################

    @Test(description = "Verify that eta_data will be created in DB for that order_id and reason will be NEW_ORDER_EVENT")
    public void etaDataCreationTest() {
        String orderId = eta_serviceHelper.getOrderId();
        if(!orderId.equals(null)) {
            String reason = SystemConfigProvider.getTemplate("deliveryeta").queryForList("select reason from eta_data where order_id="+ orderId).get(0).get("reason").toString();
            Assert.assertEquals(reason, "NEW_ORDER_EVENT");
        }else {
            Assert.fail();
        }
    }

    @Test(description = "Verify that order_batch for that particular order will be stored in eta redis with the key OB_<order_id>")
    public void storingOrderBatchInETARedisTest() {
        String orderId = eta_serviceHelper.getOrderId();
        if(!orderId.equals(null)) {
            String batch_id = SystemConfigProvider.getTemplate("deliverydb").queryForList(DeliveryConstant.batch_id+ orderId).get(0).get("batch_id").toString();
            Object orderBatch = redisHelper.getValue("deliveryredis",1,"OB_"+orderId);
            Assert.assertEquals(orderBatch, batch_id);
        }else {
            Assert.fail();
        }
    }

    @Test(description = "Verify that Batch details for that particular order will be stored in eta redis with the key BATCH_<batch_id>")
    public void storingBatchDetailsInETARedisTest() {
        String orderId = eta_serviceHelper.getOrderId();
        if(!orderId.equals(null)) {
            Object orderBatch = redisHelper.getValue("deliveryredis",1,"OB_"+orderId);
            String batchDetails = redisHelper.getValue("deliveryredis",1,"BATCH_"+orderBatch).toString();
            System.out.println("Batch details from redis : "+batchDetails);
            String batch_idFromDB = SystemConfigProvider.getTemplate("deliverydb").queryForList(DeliveryConstant.batch_id+ orderId).get(0).get("batch_id").toString();

            String batchIdFromBatchDetails = JsonPath.read(batchDetails, "$.batch_id").toString().replace("[", "").replace("]", "");
            String zoneIdFromBatchDetails = JsonPath.read(batchDetails, "$.zone_id").toString().replace("[", "").replace("]", "");
            String orderIdFromBatchDetails = JsonPath.read(batchDetails, "$.orders..order_id").toString().replace("[\"", "").replace("\"]", "");
            String restaurantIdFromBatchDetails = JsonPath.read(batchDetails, "$..restaurant_id").toString().replace("[", "").replace("]", "");
            String customerLatFromBatchDetails = JsonPath.read(batchDetails, "$.orders..customer_location.lat").toString().replace("[", "").replace("]", "");
            String customerLngFromBatchDetails = JsonPath.read(batchDetails, "$.orders..customer_location.lng").toString().replace("[", "").replace("]", "");

            Assert.assertEquals(batchIdFromBatchDetails,batch_idFromDB);
            Assert.assertEquals(zoneIdFromBatchDetails,eta_serviceHelper.getRestaurantDetails().get("rest_zone"));
            Assert.assertEquals(orderIdFromBatchDetails,orderId);
            Assert.assertEquals(restaurantIdFromBatchDetails,eta_serviceHelper.getRestaurantDetails().get("rest_id"));
            Assert.assertEquals(customerLatFromBatchDetails,eta_serviceHelper.getRestaurantDetails().get("ApproxCustomerLat"));
            Assert.assertEquals(customerLngFromBatchDetails,eta_serviceHelper.getRestaurantDetails().get("ApproxCustomerLng"));
        }else {
            Assert.fail();
        }
    }

    @Test(description = "Verify that if DS is not publishing any data then default duration value of O2A,FM,PREP,LM will be stored in eta redis from eta_config")
    public void settingDefaultEtaDurationValuesInRedisTest() {
        String orderId = eta_serviceHelper.getOrderId();
        if(!orderId.equals(null)) {
            int default_O2A = ((int) Double.parseDouble(SystemConfigProvider.getTemplate("deliveryeta").queryForList("select meta_value from `eta_config` where `meta_key` = \"DEFAULT_O2A_VALUE\"").get(0).get("meta_value").toString()));
            int default_FM = ((int) Double.parseDouble(SystemConfigProvider.getTemplate("deliveryeta").queryForList("select meta_value from `eta_config` where `meta_key` = \"DEFAULT_FMT_VALUE\"").get(0).get("meta_value").toString()));
            int default_PREP = ((int) Double.parseDouble(SystemConfigProvider.getTemplate("deliveryeta").queryForList("select meta_value from `eta_config` where `meta_key` = \"DEFAULT_PREP_VALUE\"").get(0).get("meta_value").toString()));
            int default_LMT = ((int) Double.parseDouble(SystemConfigProvider.getTemplate("deliveryeta").queryForList("select meta_value from `eta_config` where `meta_key` = \"DEFAULT_LMT_VALUE\"").get(0).get("meta_value").toString()));

            int O2A = ((int) Double.parseDouble(redisHelper.getValue("deliveryredis",1, "O2A_DURATION_" + orderId).toString()));
            int FM = ((int)Double.parseDouble(redisHelper.getValue("deliveryredis",1,"FM_DURATION_"+orderId).toString()));
            int PREP = ((int)Double.parseDouble(redisHelper.getValue("deliveryredis",1,"PREP_DURATION_"+orderId).toString()));
            int LM = ((int)Double.parseDouble(redisHelper.getValue("deliveryredis",1,"LM_DURATION_"+orderId).toString()));

            Assert.assertEquals(O2A,default_O2A );
            Assert.assertEquals(FM,default_FM);
            Assert.assertEquals(PREP,default_PREP);
            Assert.assertEquals(LM,default_LMT);

        }else {
            Assert.fail();
        }
    }

    @Test(description = "Verify that tracker won't be created and de_id will remain null in eta_data table for that particular order")
    public void trackerCreationOnNewOrderTest() {
        String orderId = eta_serviceHelper.getOrderId();
        if(!orderId.equals(null)) {
            deliveryServiceHelper.assignOrder(orderId, de_id);
            String de_idInDB = "";
            String tracker_id = "";
            try {
                de_idInDB = SystemConfigProvider.getTemplate("deliveryeta").queryForList("select de_id from `eta_data` where `order_id` =" + orderId).get(0).get("de_id").toString();
                tracker_id = SystemConfigProvider.getTemplate("deliverytracker").queryForList("select id from tracker where de_id=" + de_id).get(0).get("id").toString();
            }catch (Exception e){e.getMessage();}

            Assert.assertEquals(de_idInDB,"");
            Assert.assertEquals(tracker_id,"");
        }else {
            Assert.fail();
        }
    }

    @Test(description = "Verify that tracker will be created and metadata will be the batch_id of that particular order")
    public void trackerCreationOnConfirmedEventTest() {
        String orderId = eta_serviceHelper.getOrderId();
        if(!orderId.equals(null)) {
            String orderBatch = redisHelper.getValue("deliveryredis",1,"OB_"+orderId).toString();
            deliveryServiceHelper.assignOrder(orderId, de_id);
            eta_serviceHelper.processOrder(orderId,"confirmed");
            String tracker_id = "";
            String metadata = "";
            try {
                tracker_id = SystemConfigProvider.getTemplate("deliverytracker").queryForList("select id from tracker where de_id=" + de_id).get(0).get("id").toString();
                metadata = SystemConfigProvider.getTemplate("deliverytracker").queryForList("select metadata from tracker where id=" + tracker_id).get(0).get("metadata").toString();
            }catch (Exception e){e.getMessage();}

            Assert.assertNotEquals(tracker_id,"");
            Assert.assertEquals(metadata,orderBatch);
        }else {
            Assert.fail();
        }
    }

    @Test(description = "Verify that eta_data will be created in DB for that order_id and reason will be CONFIRMED_EVENT ")
    public void reasonForConfirmedEventTest() {
        String orderId = eta_serviceHelper.getOrderId();
        if(!orderId.equals(null)) {
            deliveryServiceHelper.assignOrder(orderId, de_id);
            eta_serviceHelper.processOrder(orderId,"confirmed");
            String reason = SystemConfigProvider.getTemplate("deliveryeta").queryForList("select reason from eta_data where order_id="+ orderId).get(1).get("reason").toString();

            Assert.assertEquals(reason,"CONFIRMED_EVENT");
        }else {
            Assert.fail();
        }
    }

    @Test(description = "Verify that de_id will be stored in DB and assignment task will completed for that particular order")
    public void assignmentTaskCompletionTest() {
        String orderId = eta_serviceHelper.getOrderId();
        if(!orderId.equals(null)) {
            deliveryServiceHelper.assignOrder(orderId, de_id);
            eta_serviceHelper.processOrder(orderId,"confirmed");
            String deInDB = SystemConfigProvider.getTemplate("deliveryeta").queryForList("select de_id from eta_data where order_id="+ orderId +" AND reason like \"CONFIRMED_EVENT\"").get(0).get("de_id").toString();
            String assign_complete = SystemConfigProvider.getTemplate("deliveryeta").queryForList("select assign_complete from eta_data where order_id="+ orderId +" AND reason like \"CONFIRMED_EVENT\"").get(0).get("assign_complete").toString();

            Assert.assertEquals(de_id,deInDB);
            Assert.assertTrue(Boolean.parseBoolean(assign_complete));
        }else {
            Assert.fail();
        }
    }

    @Test(description = "Verify that source, destination and waypoints will be de_location, customer_location and restaurant_location respectively in tracker")
    public void ETA_09() {
        String orderId = eta_serviceHelper.getOrderId();
        if(!orderId.equals(null)) {
            deliveryServiceHelper.assignOrder(orderId, de_id);
            eta_serviceHelper.processOrder(orderId,"confirmed");

            String source = SystemConfigProvider.getTemplate("deliverytracker").queryForList("select source from tracker where de_id=" + de_id).get(0).get("source").toString();
            String destination = SystemConfigProvider.getTemplate("deliverytracker").queryForList("select destination from tracker where de_id=" + de_id).get(0).get("destination").toString();
            String waypoints = SystemConfigProvider.getTemplate("deliverytracker").queryForList("select waypoints from tracker where de_id=" + de_id).get(0).get("waypoints").toString();

            String modifiedDestination = eta_serviceHelper.getLatLngPrecisionOfxDecimalPoints(destination,5);
            String modifiedCUstomerLatLng = eta_serviceHelper.getLatLngPrecisionOfxDecimalPoints(eta_serviceHelper.getRestaurantDetails().get("ApproxCustomerLatLng"),5);
            String modifiedWaypoints = eta_serviceHelper.getLatLngPrecisionOfxDecimalPoints(waypoints,5);
            String modifiedRestLatLng = eta_serviceHelper.getLatLngPrecisionOfxDecimalPoints(eta_serviceHelper.getRestaurantDetails().get("restLatLng"),5);

            Assert.assertEquals(source,"null,null");
            Assert.assertEquals(modifiedDestination,modifiedCUstomerLatLng);
            Assert.assertEquals(modifiedWaypoints,modifiedRestLatLng);
        }else {
            Assert.fail();
        }
    }

    @Test(description = "Verify that eta_data will be created in DB for that order_id and reason will be ARRIVED_EVENT")
    public void ETA_10() {
        String orderId = eta_serviceHelper.getOrderId();
        if(!orderId.equals(null)) {
            deliveryServiceHelper.assignOrder(orderId, de_id);
            eta_serviceHelper.processOrder(orderId,"arrived");
            String reason = SystemConfigProvider.getTemplate("deliveryeta").queryForList("select reason from eta_data where order_id="+ orderId).get(2).get("reason").toString();

            Assert.assertEquals(reason,"ARRIVED_EVENT");
        }else {
            Assert.fail();
        }
    }

    @Test(description = "Verify that de_id will be stored in DB and arrival task will completed for that particular order")
    public void ETA_11() {
        String orderId = eta_serviceHelper.getOrderId();
        if(!orderId.equals(null)) {
            deliveryServiceHelper.assignOrder(orderId, de_id);
            eta_serviceHelper.processOrder(orderId,"arrived");

            String deInDB = SystemConfigProvider.getTemplate("deliveryeta").queryForList("select de_id from eta_data where order_id="+ orderId +" AND reason like \"ARRIVED_EVENT\"").get(0).get("de_id").toString();
            String arrive_complete = SystemConfigProvider.getTemplate("deliveryeta").queryForList("select arrive_complete from eta_data where order_id="+ orderId +" AND reason like \"ARRIVED_EVENT\"").get(0).get("arrive_complete").toString();

            Assert.assertEquals(de_id,deInDB);
            Assert.assertTrue(Boolean.parseBoolean(arrive_complete));
        }else {
            Assert.fail();
        }
    }

    @Test(description = "Verify that source, destination and waypoints will be restaurant_location, customer_location and null respectively in tracker")
    public void ETA_12() {
        String orderId = eta_serviceHelper.getOrderId();
        if(!orderId.equals(null)) {
            deliveryServiceHelper.assignOrder(orderId, de_id);
            eta_serviceHelper.processOrder(orderId,"arrived");

            String source = SystemConfigProvider.getTemplate("deliverytracker").queryForList("select source from tracker where de_id=" + de_id).get(0).get("source").toString();
            String destination = SystemConfigProvider.getTemplate("deliverytracker").queryForList("select destination from tracker where de_id=" + de_id).get(0).get("destination").toString();
            Object waypoints = SystemConfigProvider.getTemplate("deliverytracker").queryForList("select waypoints from tracker where de_id=" + de_id).get(0).get("waypoints");

            String modifiedSource = eta_serviceHelper.getLatLngPrecisionOfxDecimalPoints(source,5);
            String modifiedDestination = eta_serviceHelper.getLatLngPrecisionOfxDecimalPoints(destination,5);
            String modifiedCustomerLatLng = eta_serviceHelper.getLatLngPrecisionOfxDecimalPoints(eta_serviceHelper.getRestaurantDetails().get("ApproxCustomerLatLng"),5);
            String modifiedRestLatLng = eta_serviceHelper.getLatLngPrecisionOfxDecimalPoints(eta_serviceHelper.getRestaurantDetails().get("restLatLng"),5);

            Assert.assertEquals(modifiedSource,modifiedRestLatLng);
            Assert.assertEquals(modifiedDestination,modifiedCustomerLatLng);
            Assert.assertNull(waypoints);
        }else {
            Assert.fail();
        }
    }

    @Test(description = "Verify that eta_data will be created in DB for that order_id and reason will be PICKEDUP_EVENT ")
    public void ETA_13() {
        String orderId = eta_serviceHelper.getOrderId();
        if(!orderId.equals(null)) {
            deliveryServiceHelper.assignOrder(orderId, de_id);
            eta_serviceHelper.processOrder(orderId,"pickedUp");
            String reason = SystemConfigProvider.getTemplate("deliveryeta").queryForList("select reason from eta_data where order_id="+ orderId).get(3).get("reason").toString();

            Assert.assertEquals(reason,"PICKEDUP_EVENT");
        }else {
            Assert.fail();
        }
    }

    @Test(description = "Verify that de_id will be stored in DB and pick up task will completed for that particular order")
    public void ETA_14() {
        executeAfterMethod = false;
        String orderId = eta_serviceHelper.getOrderId();
        if(!orderId.equals(null)) {
            deliveryServiceHelper.assignOrder(orderId, de_id);
            eta_serviceHelper.processOrder(orderId,"pickedUp");

            String deInDB = SystemConfigProvider.getTemplate("deliveryeta").queryForList("select de_id from eta_data where order_id="+ orderId +" AND reason like \"PICKEDUP_EVENT\"").get(0).get("de_id").toString();
            String pickup_complete = SystemConfigProvider.getTemplate("deliveryeta").queryForList("select pickup_complete from eta_data where order_id="+ orderId +" AND reason like \"PICKEDUP_EVENT\"").get(0).get("pickup_complete").toString();

            Assert.assertEquals(de_id,deInDB);
            Assert.assertTrue(Boolean.parseBoolean(pickup_complete));
        }else {
            Assert.fail();
        }
    }

    @Test(description = "Verify that source, destination and waypoints will be restaurant_location, customer_location and null respectively in tracker")
    public void ETA_15() {
        executeAfterMethod = false;
        String orderId = eta_serviceHelper.getOrderId();
        if(!orderId.equals(null)) {
            deliveryServiceHelper.assignOrder(orderId, de_id);
            eta_serviceHelper.processOrder(orderId,"pickedUp");

            String source = SystemConfigProvider.getTemplate("deliverytracker").queryForList("select source from tracker where de_id=" + de_id).get(0).get("source").toString();
            String destination = SystemConfigProvider.getTemplate("deliverytracker").queryForList("select destination from tracker where de_id=" + de_id).get(0).get("destination").toString();
            Object waypoints = SystemConfigProvider.getTemplate("deliverytracker").queryForList("select waypoints from tracker where de_id=" + de_id).get(0).get("waypoints");

            String modifiedSource = eta_serviceHelper.getLatLngPrecisionOfxDecimalPoints(source,5);
            String modifiedDestination = eta_serviceHelper.getLatLngPrecisionOfxDecimalPoints(destination,5);
            String modifiedCustomerLatLng = eta_serviceHelper.getLatLngPrecisionOfxDecimalPoints(eta_serviceHelper.getRestaurantDetails().get("ApproxCustomerLatLng"),5);
            String modifiedRestLatLng = eta_serviceHelper.getLatLngPrecisionOfxDecimalPoints(eta_serviceHelper.getRestaurantDetails().get("restLatLng"),5);

            Assert.assertEquals(modifiedSource,modifiedRestLatLng);
            Assert.assertEquals(modifiedDestination,modifiedCustomerLatLng);
            Assert.assertNull(waypoints);
        }else {
            Assert.fail();
        }
    }

    @Test(description = "Verify that eta_data will be created in DB for that order_id and reason will be REACHED_EVENT")
    public void ETA_16() {
        executeAfterMethod = false;
        String orderId = eta_serviceHelper.getOrderId();
        if(!orderId.equals(null)) {
            deliveryServiceHelper.assignOrder(orderId, de_id);
            eta_serviceHelper.processOrder(orderId,"reached");
            String reason = SystemConfigProvider.getTemplate("deliveryeta").queryForList("select reason from eta_data where order_id="+ orderId).get(4).get("reason").toString();

            Assert.assertEquals(reason,"REACHED_EVENT");
        }else {
            Assert.fail();
        }
    }

    @Test(description = "Verify that tracker will be archived by client")
    public void ETA_17() {
        executeAfterMethod = false;
        String orderId = eta_serviceHelper.getOrderId();
        if(!orderId.equals(null)) {
            deliveryServiceHelper.assignOrder(orderId, de_id);
            eta_serviceHelper.processOrder(orderId,"reached");

            String archived_status = SystemConfigProvider.getTemplate("deliverytracker").queryForList("select archived from tracker where de_id=" + de_id).get(0).get("archived").toString();
            String archivedBy = SystemConfigProvider.getTemplate("deliverytracker").queryForList("select archived_by from tracker where de_id=" + de_id).get(0).get("archived_by").toString();

            Assert.assertTrue(Boolean.parseBoolean(archived_status));
            Assert.assertEquals(archivedBy,"client");
        }else {
            Assert.fail();
        }
    }

    @Test(description = "Verify that source, destination and waypoints will be DE_location, customer_location and null respectively in tracker")
    public void ETA_18() {
        executeAfterMethod = false;
        String orderId = eta_serviceHelper.getOrderId();
        if(!orderId.equals(null)) {
            deliveryServiceHelper.assignOrder(orderId, de_id);
            /*eta_serviceHelper.processOrder(orderId,"pickedUp");
            threadWait(400000);
            String orderBatch = redisHelper.getValue("deliveryredis",1,"OB_"+orderId).toString();
            String msg = eta_serviceHelper.DElocationMesssageForKafka(de_id,orderBatch);
            System.out.println("*************************"+msg);
            kafkaHelper.createKafkaProducer(KAFKA_INCENTIVE_SERVER_NAME);
            kafkaHelper.pushMessage("de_locations",msg);
            threadWait(400000);
            kafkaHelper.createKafkaProducer(KAFKA_INCENTIVE_SERVER_NAME);
            kafkaHelper.pushMessage("de_locations",msg);
            deliveryServiceHelper.zipDialReached(orderId);*/

            eta_serviceHelper.processOrder(orderId,"reached");
            String source = SystemConfigProvider.getTemplate("deliverytracker").queryForList("select source from tracker where de_id=" + de_id).get(0).get("source").toString();
            String destination = SystemConfigProvider.getTemplate("deliverytracker").queryForList("select destination from tracker where de_id=" + de_id).get(0).get("destination").toString();
            Object waypoints = SystemConfigProvider.getTemplate("deliverytracker").queryForList("select waypoints from tracker where de_id=" + de_id).get(0).get("waypoints");

            String modifiedSource = eta_serviceHelper.getLatLngPrecisionOfxDecimalPoints(source,5);
            String modifiedDestination = eta_serviceHelper.getLatLngPrecisionOfxDecimalPoints(destination,5);
            String modifiedCustomerLatLng = eta_serviceHelper.getLatLngPrecisionOfxDecimalPoints(eta_serviceHelper.getRestaurantDetails().get("ApproxCustomerLatLng"),5);
            String modifiedRestLatLng = eta_serviceHelper.getLatLngPrecisionOfxDecimalPoints(eta_serviceHelper.getRestaurantDetails().get("restLatLng"),5);

            Assert.assertEquals(modifiedSource,modifiedRestLatLng);
            Assert.assertEquals(modifiedDestination,modifiedCustomerLatLng);
            Assert.assertNull(waypoints);
        }else {
            Assert.fail();
        }
    }

    @Test(description = "Verify that eta_data will be created in DB for that order_id and reason will be DELIVERED_EVENT")
    public void ETA_19() {
        executeAfterMethod = false;
        String orderId = eta_serviceHelper.getOrderId();
        if(!orderId.equals(null)) {
            deliveryServiceHelper.assignOrder(orderId, de_id);
            eta_serviceHelper.processOrder(orderId,"delivered");
            String reason = SystemConfigProvider.getTemplate("deliveryeta").queryForList("select reason from eta_data where order_id="+ orderId).get(5).get("reason").toString();

            Assert.assertEquals(reason,"DELIVERED_EVENT");
        }else {
            Assert.fail();
        }
    }

    @Test(description = "Verify that de_id will be stored in DB and deliver task will completed for that particular order")
    public void ETA_20() {
        executeAfterMethod = false;
        String orderId = eta_serviceHelper.getOrderId();
        if(!orderId.equals(null)) {
            deliveryServiceHelper.assignOrder(orderId, de_id);
            eta_serviceHelper.processOrder(orderId,"delivered");

            String deInDB = SystemConfigProvider.getTemplate("deliveryeta").queryForList("select de_id from eta_data where order_id="+ orderId +" AND reason like \"DELIVERED_EVENT\"").get(0).get("de_id").toString();
            String deliver_complete = SystemConfigProvider.getTemplate("deliveryeta").queryForList("select deliver_complete from eta_data where order_id="+ orderId +" AND reason like \"DELIVERED_EVENT\"").get(0).get("deliver_complete").toString();

            Assert.assertEquals(de_id,deInDB);
            Assert.assertTrue(Boolean.parseBoolean(deliver_complete));
        }else {
            Assert.fail();
        }
    }

    @Test(description = "Verify that after placing new order assignment,arrival,pickup and delivery time will be calculated based on O2A,FM,PREP and LM_DURATION respectively")
    public void ETA_21() throws Exception {
        String orderId = eta_serviceHelper.getOrderId();
        if(!orderId.equals(null)) {
            String timestamp = SystemConfigProvider.getTemplate("deliveryeta").queryForList("select timestamp from eta_data where order_id="+ orderId +" AND reason like \"NEW_ORDER_EVENT\"").get(0).get("timestamp").toString();
            System.out.println("*******************"+timestamp);
            long current_epochTime = eta_serviceHelper.epochConverter(timestamp,"yyyy-MM-dd HH:mm:ss.S");
            int O2A = ((int) Double.parseDouble(redisHelper.getValue("deliveryredis", 1, "O2A_DURATION_" + orderId).toString()));
            long expectedAssignmentEpochTime = current_epochTime + (O2A * 60000);
            String expectedAssignmentTime = eta_serviceHelper.epochToTimeStampConverter(expectedAssignmentEpochTime,"yyyy-MM-dd HH:mm:ss.S");

            int FM = ((int)Double.parseDouble(redisHelper.getValue("deliveryredis",1,"FM_DURATION_"+orderId).toString()));
            long expectedArrivalEpochTime = expectedAssignmentEpochTime + (FM * 60000);
            String expectedArrivalTime = eta_serviceHelper.epochToTimeStampConverter(expectedArrivalEpochTime,"yyyy-MM-dd HH:mm:ss.S");

            int PREP = ((int)Double.parseDouble(redisHelper.getValue("deliveryredis",1,"PREP_DURATION_"+orderId).toString()));
            long expectedPickupEpochTime = current_epochTime + (PREP * 60000);
            String expectedPickupTime = eta_serviceHelper.epochToTimeStampConverter(expectedPickupEpochTime,"yyyy-MM-dd HH:mm:ss.S");

            int LM = ((int)Double.parseDouble(redisHelper.getValue("deliveryredis",1,"LM_DURATION_"+orderId).toString()));
            long O2D = ((long)Double.parseDouble(redisHelper.getValue("deliveryredis",1,"O2D_TIME_"+orderId).toString()));
            long expectedDeliveryEpochTime = Math.max(expectedPickupEpochTime + (LM * 60000),O2D);
            String expectedDeliveryTime = eta_serviceHelper.epochToTimeStampConverter(expectedDeliveryEpochTime,"yyyy-MM-dd HH:mm:ss.S");

            String actualAssignmentTime = SystemConfigProvider.getTemplate("deliveryeta").queryForList("select assignment from eta_data where order_id="+ orderId +" AND reason like \"NEW_ORDER_EVENT\"").get(0).get("assignment").toString();
            String actualArrivalTime = SystemConfigProvider.getTemplate("deliveryeta").queryForList("select arrival from eta_data where order_id="+ orderId +" AND reason like \"NEW_ORDER_EVENT\"").get(0).get("arrival").toString();
            String actualPickupTime = SystemConfigProvider.getTemplate("deliveryeta").queryForList("select pickup from eta_data where order_id="+ orderId +" AND reason like \"NEW_ORDER_EVENT\"").get(0).get("pickup").toString();
            String actualDeliveryTime = SystemConfigProvider.getTemplate("deliveryeta").queryForList("select delivery from eta_data where order_id="+ orderId +" AND reason like \"NEW_ORDER_EVENT\"").get(0).get("delivery").toString();

            Assert.assertEquals(expectedAssignmentTime,actualAssignmentTime);
            Assert.assertEquals(expectedArrivalTime,actualArrivalTime);
            Assert.assertEquals(expectedPickupTime,actualPickupTime);
            Assert.assertEquals(expectedDeliveryTime,actualDeliveryTime);
        }else {
            Assert.fail();
        }
    }















}
