package com.swiggy.api.erp.cms.pojo.SpinService;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(Include.NON_NULL)//For ignore null in JSON body

public class Meta {
	
	private String created_by;
	public Meta(String created_by){
		this.created_by=created_by;
	}

	public String getCreated_by() {
		return created_by;
	}

	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}

	@Override
	public String toString() {
		return "Meta {created_by=" + created_by + "}";
	}
	
	

}
