package com.swiggy.api.erp.ff.dp;

import com.swiggy.api.erp.ff.constants.LosConstants;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.annotations.DataProvider;

public class CancelAndDeliveredDataProvider {

    @DataProvider(name="differentDeliveryStatus")
    public Object[][] getDifferentStatus()
    {
        return new Object[][]{
                {LosConstants.status[0]},{LosConstants.status[1]},{LosConstants.status[2]},
                {LosConstants.status[3]},{LosConstants.status[4]},{LosConstants.status[5]} ,{LosConstants.unassignStatus},{LosConstants.cancelStatus}};
    }

}
