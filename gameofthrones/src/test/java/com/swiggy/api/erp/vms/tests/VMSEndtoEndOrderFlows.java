package com.swiggy.api.erp.vms.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.ff.helper.OMSHelper;
import com.swiggy.api.erp.vms.constants.RMSConstants;
import com.swiggy.api.erp.vms.dp.VMSEndToEndOrderFlowsDP;
import com.swiggy.api.erp.vms.helper.RMSCommonHelper;
import com.swiggy.api.erp.vms.helper.RMSHelper;
import com.swiggy.api.erp.vms.helper.RMSTDHelper;
import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.snd.helper.SnDHelper;
import com.swiggy.automation.common.utils.APIUtils;
import framework.gameofthrones.JonSnow.Processor;
import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class VMSEndtoEndOrderFlows {
	OMSHelper omsHelper = new OMSHelper();
	RMSHelper rmsHelper = new RMSHelper();
	
	//done
	@Test(dataProvider = "verifyOrderAtRMS", description = "CreateOrder ->Login -> Fetch orders -> Verify order details+confirm Order checks", dataProviderClass =VMSEndToEndOrderFlowsDP.class)
	public void verifyOrderAtRMS(HashMap<String, HashMap<String, String>> dataSet) throws Exception {
		HashMap<String, String> orderMap = dataSet.get("orderData");
		HashMap<String, String> rmsSessionData = dataSet.get("rmsSessionData");
		HashMap<String, String> restaurantData = dataSet.get("restaurantData");
			System.out.println("myAccessToken :" + rmsSessionData.get("accessToken"));
			Thread.sleep(30000);
			String a = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			JsonNode nodeObject = APIUtils.convertStringtoJSON(a);
			Assert.assertEquals(nodeObject.get("statusCode").asInt(), 0);
			Assert.assertEquals(nodeObject.get("statusMessage").asText(), "Success");
			String Obj = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
					Long.parseLong(restaurantData.get("restaurantId")), nodeObject);
			Assert.assertNotNull("FetchOrder API doesnt have OrderCreated " + Obj);
			String resp = rmsHelper.confirm(rmsSessionData.get("accessToken"), orderMap.get("orderId"), restaurantData.get("restaurantId")).ResponseValidator.GetBodyAsText();
			System.out.println(resp);
			int statusCode = JsonPath.read(resp, "$.statusCode");
			Assert.assertEquals(statusCode, 0);
			Assert.assertEquals(JsonPath.read(resp, "$.statusMessage"), "Order Confirmed");
	}
	
	
	//done
	@Test(dataProvider = "verifyFetchOrdersAtRMSAndMarkItemOutOfStock",description = "Create Order -> Login -> Fetch orders -> Mark order OOS(Out ofStock) -> Verify order status", dataProviderClass =VMSEndToEndOrderFlowsDP.class)
	public void verifyFetchOrdersAtRMSAndMarkItemOutOfStock(HashMap<String, HashMap<String, String>> dataSet)
			throws Exception {
		HashMap<String, String> orderMap = dataSet.get("orderData");
		HashMap<String, String> rmsSessionData = dataSet.get("rmsSessionData");
		HashMap<String, String> restaurantData = dataSet.get("restaurantData");
			System.out.println("myAccessToken :" + rmsSessionData.get("accessToken"));
			Thread.sleep(30000);
			String a = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			JsonNode nodeObject = APIUtils.convertStringtoJSON(a);
			Assert.assertEquals(nodeObject.get("statusCode").asInt(), 0);
			Assert.assertEquals(nodeObject.get("statusMessage").asText(), "Success");
			String Obj = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
					Long.parseLong(restaurantData.get("restaurantId")), nodeObject);
			Assert.assertNotNull("FetchOrder API doesnt have OrderCreated " + Obj);

			Processor response = SnDHelper.menuV3RestId(restaurantData.get("restaurantId"));
			ArrayList<Long> itemList = RMSCommonHelper.getInStockItemsFromMenu(response);
			String b = rmsHelper.markItemOOS(orderMap.get("orderId"), "8428273",
					String.valueOf(itemList.get(2)), "", restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			System.out.println(b);
			Thread.sleep(30000);
			String a1 = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			JsonNode nodeObject1 = APIUtils.convertStringtoJSON(a1);
			Assert.assertEquals(nodeObject1.get("statusCode").asInt(), 0);
			Assert.assertEquals(nodeObject1.get("statusMessage").asText(), "Success");
			String Obj1 = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
					Long.parseLong(restaurantData.get("restaurantId")), nodeObject1);
			JsonNode node=APIUtils.convertStringtoJSON(Obj1);
			System.out.println("=============>:::::: "+node.get("status").get("placed_status").asText());
			Assert.assertEquals(node.get("status").get("placed_status").asText(), "out_of_stock");
			System.out.println("----->::::::>>"+Obj1);
			// verification to put
	}
//(data provider not present)
// @Test(dataProvider = "verifyOrderDetailsAfterOOSandEditOrder", description = "Create Order -> Login -> Fetch orders -> Mark order OOS(Out of Stock) -> Edit Order from FF -> Verify order status", dataProviderClass =VMSEndToEndOrderFlowsDP.class)
	public void verifyOrderDetailsAfterOOSandEditOrder(HashMap<String, HashMap<String, String>> dataSet)
			throws Exception {
		OMSHelper omsHelper = new OMSHelper();
		HashMap<String, String> orderMap = dataSet.get("orderData");
		HashMap<String, String> omsSessionData = dataSet.get("omsSessionData");
		HashMap<String, String> rmsSessionData = dataSet.get("rmsSessionData");
		HashMap<String, String> restaurantData = dataSet.get("restaurantData");
		if (RMSCommonHelper.isOrderAvailableAtFF(orderMap.get("orderId"))) {
			boolean flg = omsHelper.verifyOrder(orderMap.get("orderId"), "010", omsSessionData.get("X-CSRFToken"),
					omsSessionData.get("sessionId"));
			System.out.println("myAccessToken :" + rmsSessionData.get("accessToken"));
			String date = RMSCommonHelper.getCurrentDateTimeInRequiredFormat(new Date(), "yyyy-MM-dd'T'HH:mm:ss");
			System.out.println("------------------->" + date);
			Thread.sleep(30000);
			String a = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			JsonNode nodeObject = APIUtils.convertStringtoJSON(a);
			Assert.assertEquals(nodeObject.get("statusCode").asInt(), 0);
			Assert.assertEquals(nodeObject.get("statusMessage").asText(), "Success");
			String Obj = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
					Long.parseLong(restaurantData.get("restaurantId")), nodeObject);
			Assert.assertNotNull("FetchOrder API doesnt have OrderCreated " + Obj);

			Processor response = SnDHelper.menuV3RestId(restaurantData.get("restaurantId"));
			ArrayList<Long> itemList = RMSCommonHelper.getInStockItemsFromMenu(response);

			String b = rmsHelper.markItemOOS(orderMap.get("orderId"), "8428273",
					String.valueOf(itemList.get(2)), "", restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			System.out.println(b);
			//===================
			//Edit Order FF TBD
			//===================
			Thread.sleep(30000);
			String a1 = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			JsonNode nodeObject1 = APIUtils.convertStringtoJSON(a1);
			Assert.assertEquals(nodeObject1.get("statusCode").asInt(), 0);
			Assert.assertEquals(nodeObject1.get("statusMessage").asText(), "Success");
			String Obj1 = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
					Long.parseLong(restaurantData.get("restaurantId")), nodeObject1);
			JsonNode node=APIUtils.convertStringtoJSON(Obj1);
			System.out.println("=============>:::::: "+node.get("status").get("placed_status").asText());
			Assert.assertEquals(node.get("status").get("placed_status").asText(), "out_of_stock");
			
			System.out.println("----->::::::>>"+Obj1);
			// verification to put
		} else {
			Assert.fail("FF issue  .. Order Not Reached FF So This DownStream Dependency Failed For VMS");
		}
	}
 //Done
	@Test(dataProvider = "verifyRequestCallBack", description = "Create Order ->Login -> Fetch orders -> Request callback -> Verify order status",dataProviderClass = VMSEndToEndOrderFlowsDP.class)
	public void verifyRequestCallBack(HashMap<String, HashMap<String, String>> dataSet) throws Exception {
		HashMap<String, String> orderMap = dataSet.get("orderData");
		HashMap<String, String> rmsSessionData = dataSet.get("rmsSessionData");
		HashMap<String, String> restaurantData = dataSet.get("restaurantData");
		if (RMSCommonHelper.isOrderAvailableAtFF(orderMap.get("orderId"))) {
			System.out.println("myAccessToken :" + rmsSessionData.get("accessToken"));
			String date = RMSCommonHelper.getCurrentDateTimeInRequiredFormat(new Date(), "yyyy-MM-dd'T'HH:mm:ss");
			System.out.println("------------------->" + date);
			Thread.sleep(30000);
			String a = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			JsonNode nodeObject = APIUtils.convertStringtoJSON(a);
			Assert.assertEquals(nodeObject.get("statusCode").asInt(), 0);
			Assert.assertEquals(nodeObject.get("statusMessage").asText(), "Success");
			String Obj = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
					Long.parseLong(restaurantData.get("restaurantId")), nodeObject);
			Assert.assertNotNull("FetchOrder API doesnt have OrderCreated " + Obj);
			String response=rmsHelper.callback(rmsSessionData.get("accessToken"),restaurantData.get("restaurantId"), orderMap.get("orderId")).ResponseValidator.GetBodyAsText();
			System.out.println(response);
			int statusCode = JsonPath.read(response, "$.statusCode");
			Assert.assertEquals(statusCode, 0);
			Assert.assertEquals(JsonPath.read(response, "$.statusMessage"), "Call Requested Successfully");
            System.out.println(response);
		} else {
			Assert.fail("FF issue  .. Order Not Reached FF So This DownStream Dependency Failed For VMS");
		}
	}
//done
	 //@Test(dataProvider = "verifyUpdateCallBackRequestFromFF", description ="Create Order -> Login -> Fetch orders -> Request callback -> Update callrequest from FF -> Verify order status", dataProviderClass = VMSEndToEndOrderFlowsDP.class)
	public void verifyUpdateCallBackRequestFromFF(HashMap<String, HashMap<String, String>> dataSet) throws Exception {
		OMSHelper omsHelper = new OMSHelper();
		HashMap<String, String> orderMap = dataSet.get("orderData");
		HashMap<String, String> omsSessionData = dataSet.get("omsSessionData");
		HashMap<String, String> rmsSessionData = dataSet.get("rmsSessionData");
		HashMap<String, String> restaurantData = dataSet.get("restaurantData");
		if (RMSCommonHelper.isOrderAvailableAtFF(orderMap.get("orderId"))) {
			boolean flg = omsHelper.verifyOrder(orderMap.get("orderId"), "010", omsSessionData.get("X-CSRFToken"),
					omsSessionData.get("sessionId"));
			System.out.println("myAccessToken :" + rmsSessionData.get("accessToken"));
			String date = RMSCommonHelper.getCurrentDateTimeInRequiredFormat(new Date(), "yyyy-MM-dd'T'HH:mm:ss");
			System.out.println("------------------->" + date);
			Thread.sleep(30000);
			String a = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			JsonNode nodeObject = APIUtils.convertStringtoJSON(a);
			Assert.assertEquals(nodeObject.get("statusCode").asInt(), 0);
			Assert.assertEquals(nodeObject.get("statusMessage").asText(), "Success");
			String Obj = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
					Long.parseLong(restaurantData.get("restaurantId")), nodeObject);
			Assert.assertNotNull("FetchOrder API doesnt have OrderCreated " + Obj);
			String response=rmsHelper.callback(restaurantData.get("restaurantId"), orderMap.get("orderId"), rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			System.out.println(response);
			int statusCode = JsonPath.read(response, "$.statusCode");
			Assert.assertEquals(statusCode, 0);
			Assert.assertEquals(JsonPath.read(response, "$.statusMessage"), "Call Requested Successfully");
            System.out.println(response);
            
            //============================
            //FF Update Call Implement TBD
          //============================
            
        	Thread.sleep(30000);
			String aa = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			JsonNode nodeObject1 = APIUtils.convertStringtoJSON(aa);
			Assert.assertEquals(nodeObject1.get("statusCode").asInt(), 0);
			Assert.assertEquals(nodeObject1.get("statusMessage").asText(), "Success");
			String Obj1 = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
					Long.parseLong(restaurantData.get("restaurantId")), nodeObject1);
			Assert.assertNotNull("FetchOrder API doesnt have OrderCreated " + Obj1);
            
		} else {
			Assert.fail("FF issue  .. Order Not Reached FF So This DownStream Dependency Failed For VMS");
		}
	}
	 //Done
	@Test(dataProvider = "verifyOOSBySendingMultipleAlertnatives", description ="Create Order -> Login -> Fetch orders -> Confirm order -> Mark orderOOS(Send multiple items as alternatives) -> Verify order status",dataProviderClass = VMSEndToEndOrderFlowsDP.class)
	public void verifyOOSBySendingMultipleAlertnatives(HashMap<String, HashMap<String, String>> dataSet)
			throws Exception {
		HashMap<String, String> orderMap = dataSet.get("orderData");
		HashMap<String, String> rmsSessionData = dataSet.get("rmsSessionData");
		HashMap<String, String> restaurantData = dataSet.get("restaurantData");
		if (RMSCommonHelper.isOrderAvailableAtFF(orderMap.get("orderId"))) {
			System.out.println("myAccessToken :" + rmsSessionData.get("accessToken"));
			String date = RMSCommonHelper.getCurrentDateTimeInRequiredFormat(new Date(), "yyyy-MM-dd'T'HH:mm:ss");
			System.out.println("------------------->" + date);
			Thread.sleep(30000);
			String a = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			JsonNode nodeObject = APIUtils.convertStringtoJSON(a);
			Assert.assertEquals(nodeObject.get("statusCode").asInt(), 0);
			Assert.assertEquals(nodeObject.get("statusMessage").asText(), "Success");
			String Obj = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
					Long.parseLong(restaurantData.get("restaurantId")), nodeObject);
			Assert.assertNotNull("FetchOrder API doesnt have OrderCreated " + Obj);

			Processor response = SnDHelper.menuV3RestId(restaurantData.get("restaurantId"));
			ArrayList<Long> itemList = RMSCommonHelper.getInStockItemsFromMenu(response);

			String b = rmsHelper.markItemOOS(orderMap.get("orderId"), APIUtils.convertStringtoJSON(Obj).get("cart").get("items").get(0).get("item_id").asText(),
					String.valueOf(itemList.get(2)), "", restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			System.out.println(b);
			
			Thread.sleep(30000);
			String a1 = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			JsonNode nodeObject1 = APIUtils.convertStringtoJSON(a1);
			Assert.assertEquals(nodeObject1.get("statusCode").asInt(), 0);
			Assert.assertEquals(nodeObject1.get("statusMessage").asText(), "Success");
			String Obj1 = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
					Long.parseLong(restaurantData.get("restaurantId")), nodeObject1);
			JsonNode node=APIUtils.convertStringtoJSON(Obj1);
			System.out.println("=============>:::::: "+node.get("status").get("placed_status").asText());
			Assert.assertEquals(node.get("status").get("placed_status").asText(), "out_of_stock");
			
			System.out.println("----->::::::>>"+Obj1);
			// verification to put
		} else {
			Assert.fail("FF issue  .. Order Not Reached FF So This DownStream Dependency Failed For VMS");
		}
	}
//done
 //@Test(dataProvider = "verifyOOSBySendingMultipleAlertnativesNEditOrder", description = "Create Order -> Login -> Fetch orders -> Confirm order -> Markorder OOS(Send multiple items as alternatives) -> Edit Order from FF -> Verify order status", dataProviderClass = VMSEndToEndOrderFlowsDP.class)
	public void verifyOOSBySendingMultipleAlertnativesNEditOrder(HashMap<String, HashMap<String, String>> dataSet)
			throws Exception {
		OMSHelper omsHelper = new OMSHelper();
		HashMap<String, String> orderMap = dataSet.get("orderData");
		HashMap<String, String> omsSessionData = dataSet.get("omsSessionData");
		HashMap<String, String> rmsSessionData = dataSet.get("rmsSessionData");
		HashMap<String, String> restaurantData = dataSet.get("restaurantData");
		if (RMSCommonHelper.isOrderAvailableAtFF(orderMap.get("orderId"))) {
			boolean flg = omsHelper.verifyOrder(orderMap.get("orderId"), "010", omsSessionData.get("X-CSRFToken"),
					omsSessionData.get("sessionId"));
			System.out.println("myAccessToken :" + rmsSessionData.get("accessToken"));
			String date = RMSCommonHelper.getCurrentDateTimeInRequiredFormat(new Date(), "yyyy-MM-dd'T'HH:mm:ss");
			System.out.println("------------------->" + date);
			Thread.sleep(30000);
			String a = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			JsonNode nodeObject = APIUtils.convertStringtoJSON(a);
			Assert.assertEquals(nodeObject.get("statusCode").asInt(), 0);
			Assert.assertEquals(nodeObject.get("statusMessage").asText(), "Success");
			String Obj = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
					Long.parseLong(restaurantData.get("restaurantId")), nodeObject);
			Assert.assertNotNull("FetchOrder API doesnt have OrderCreated " + Obj);
			String resp = rmsHelper.confirm(restaurantData.get("restaurantId"), orderMap.get("orderId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			System.out.println(resp);
			int statusCode = JsonPath.read(resp, "$.statusCode");
			Assert.assertEquals(statusCode, 0);
			Assert.assertEquals(JsonPath.read(resp, "$.statusMessage"), "Order Confirmed");
			Processor response = SnDHelper.menuV3RestId(restaurantData.get("restaurantId"));
			ArrayList<Long> itemList = RMSCommonHelper.getInStockItemsFromMenu(response);
			String b = rmsHelper.markItemOOS(orderMap.get("orderId"), APIUtils.convertStringtoJSON(Obj).get("cart").get("items").get(0).get("item_id").asText(),
					String.valueOf(itemList.get(2)), "", restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			System.out.println(b);
			//=====================
			//Edit Order From FF TBD
			//=====================
			Thread.sleep(30000);
			String a1 = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			JsonNode nodeObject1 = APIUtils.convertStringtoJSON(a1);
			Assert.assertEquals(nodeObject1.get("statusCode").asInt(), 0);
			Assert.assertEquals(nodeObject1.get("statusMessage").asText(), "Success");
			String Obj1 = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
					Long.parseLong(restaurantData.get("restaurantId")), nodeObject1);
			JsonNode node=APIUtils.convertStringtoJSON(Obj1);
			System.out.println("=============>:::::: "+node.get("status").get("placed_status").asText());
			Assert.assertEquals(node.get("status").get("placed_status").asText(), "out_of_stock");
			
			System.out.println("----->::::::>>"+Obj1);

		} else {
			Assert.fail("FF issue  .. Order Not Reached FF So This DownStream Dependency Failed For VMS");
		}
	}
//done
	 //@Test(dataProvider = "verifyConfirmOrderAfterOrderEdit", description = "Create Order -> Login -> Fetchorders -> Confirm order -> Mark order OOS(Send multiple items as alternatives) -> Edit Order from FF -> Confirm order -> Verify order status", dataProviderClass = VMSEndToEndOrderFlowsDP.class)
	public void verifyConfirmOrderAfterOrderEdit(HashMap<String, HashMap<String, String>> dataSet) throws Exception {
		OMSHelper omsHelper = new OMSHelper();
		HashMap<String, String> orderMap = dataSet.get("orderData");
		HashMap<String, String> omsSessionData = dataSet.get("omsSessionData");
		HashMap<String, String> rmsSessionData = dataSet.get("rmsSessionData");
		HashMap<String, String> restaurantData = dataSet.get("restaurantData");
		if (RMSCommonHelper.isOrderAvailableAtFF(orderMap.get("orderId"))) {
			boolean flg = omsHelper.verifyOrder(orderMap.get("orderId"), "010", omsSessionData.get("X-CSRFToken"),
					omsSessionData.get("sessionId"));
			System.out.println("myAccessToken :" + rmsSessionData.get("accessToken"));
			String date = RMSCommonHelper.getCurrentDateTimeInRequiredFormat(new Date(), "yyyy-MM-dd'T'HH:mm:ss");
			System.out.println("------------------->" + date);
			Thread.sleep(30000);
			String a = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			JsonNode nodeObject = APIUtils.convertStringtoJSON(a);
			Assert.assertEquals(nodeObject.get("statusCode").asInt(), 0);
			Assert.assertEquals(nodeObject.get("statusMessage").asText(), "Success");
			String Obj = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
					Long.parseLong(restaurantData.get("restaurantId")), nodeObject);
			Assert.assertNotNull("FetchOrder API doesnt have OrderCreated " + Obj);
			String resp = rmsHelper.confirm(restaurantData.get("restaurantId"), orderMap.get("orderId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			System.out.println(resp);
			int statusCode = JsonPath.read(resp, "$.statusCode");
			Assert.assertEquals(statusCode, 0);
			Assert.assertEquals(JsonPath.read(resp, "$.statusMessage"), "Order Confirmed");
			Processor response = SnDHelper.menuV3RestId(restaurantData.get("restaurantId"));
			ArrayList<Long> itemList = RMSCommonHelper.getInStockItemsFromMenu(response);
			
			//Send Multiple alternatives
			String b = rmsHelper.markItemOOS(orderMap.get("orderId"), APIUtils.convertStringtoJSON(Obj).get("cart").get("items").get(0).get("item_id").asText(),
					String.valueOf(itemList.get(2)), "", restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			System.out.println(b);
			//=====================
			//Edit Order From FF TBD
			//=====================
			String resp1 = rmsHelper.confirm(restaurantData.get("restaurantId"), orderMap.get("orderId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			System.out.println(resp1);
			int statusCode1 = JsonPath.read(resp1, "$.statusCode");
			Assert.assertEquals(statusCode1, 0);
			Assert.assertEquals(JsonPath.read(resp, "$.statusMessage"), "Order Confirmed");
			Thread.sleep(30000);
			String a1 = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			JsonNode nodeObject1 = APIUtils.convertStringtoJSON(a1);
			Assert.assertEquals(nodeObject1.get("statusCode").asInt(), 0);
			Assert.assertEquals(nodeObject1.get("statusMessage").asText(), "Success");
			String Obj1 = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
					Long.parseLong(restaurantData.get("restaurantId")), nodeObject1);
			JsonNode node=APIUtils.convertStringtoJSON(Obj1);
			System.out.println("=============>:::::: "+node.get("status").get("placed_status").asText());
			Assert.assertEquals(node.get("status").get("placed_status").asText(), "out_of_stock");
			
			System.out.println("----->::::::>>"+Obj1);

		} else {
			Assert.fail("FF issue  .. Order Not Reached FF So This DownStream Dependency Failed For VMS");
		}
	}
	//done
//	 @Test(dataProvider = "verifyCallBackFromFFafterConfirmOrder", description ="Create Order -> Login -> Fetch orders -> Confirm Order -> Request callback -> Verify order status", dataProviderClass = VMSEndToEndOrderFlowsDP.class)
	public void verifyCallBackFromFFafterConfirmOrder(HashMap<String, HashMap<String, String>> dataSet)
			throws Exception {
		OMSHelper omsHelper = new OMSHelper();
		HashMap<String, String> orderMap = dataSet.get("orderData");
		//HashMap<String, String> omsSessionData = dataSet.get("omsSessionData");
		HashMap<String, String> rmsSessionData = dataSet.get("rmsSessionData");
		HashMap<String, String> restaurantData = dataSet.get("restaurantData");
		if (RMSCommonHelper.isOrderAvailableAtFF(orderMap.get("orderId"))) {
//			boolean flg = omsHelper.verifyOrder(orderMap.get("orderId"), "010", omsSessionData.get("X-CSRFToken"),
//					omsSessionData.get("sessionId"));
			System.out.println("myAccessToken :" + rmsSessionData.get("accessToken"));
			String date = RMSCommonHelper.getCurrentDateTimeInRequiredFormat(new Date(), "yyyy-MM-dd'T'HH:mm:ss");
			System.out.println("------------------->" + date);
			Thread.sleep(30000);
			String a = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			JsonNode nodeObject = APIUtils.convertStringtoJSON(a);
			Assert.assertEquals(nodeObject.get("statusCode").asInt(), 0);
			Assert.assertEquals(nodeObject.get("statusMessage").asText(), "Success");
			String Obj = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
					Long.parseLong(restaurantData.get("restaurantId")), nodeObject);
			Assert.assertNotNull("FetchOrder API doesnt have OrderCreated " + Obj);
			String resp = rmsHelper.confirm(restaurantData.get("restaurantId"), orderMap.get("orderId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			System.out.println(resp);
			int statusCode = JsonPath.read(resp, "$.statusCode");
			Assert.assertEquals(statusCode, 0);
			Assert.assertEquals(JsonPath.read(resp, "$.statusMessage"), "Order Confirmed");
			
			
			String response=rmsHelper.callback(restaurantData.get("restaurantId"), orderMap.get("orderId"), rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			System.out.println(response);
			int statusCode1 = JsonPath.read(response, "$.statusCode");
			Assert.assertEquals(statusCode1, 0);
			Assert.assertEquals(JsonPath.read(response, "$.statusMessage"), "Call Requested Successfully");
            System.out.println(response);
			
			Thread.sleep(30000);
			String a1 = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			JsonNode nodeObject1 = APIUtils.convertStringtoJSON(a1);
			Assert.assertEquals(nodeObject1.get("statusCode").asInt(), 0);
			Assert.assertEquals(nodeObject1.get("statusMessage").asText(), "Success");
			String Obj1 = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
					Long.parseLong(restaurantData.get("restaurantId")), nodeObject1);
			JsonNode node=APIUtils.convertStringtoJSON(Obj1);
			System.out.println("=============>:::::: "+node.get("status").get("placed_status").asText());
			Assert.assertEquals(node.get("status").get("placed_status").asText(), "out_of_stock");
			
			System.out.println("----->::::::>>"+Obj1);

		} else {
			Assert.fail("FF issue  .. Order Not Reached FF So This DownStream Dependency Failed For VMS");
		}
	}
		//done
 //@Test(dataProvider = "verifyUpdateCallBackFromFFafterConfirmOrder", description = "Create Order -> Login -> Fetch orders -> Confirm Order -> Request callback -> Update call reuest from FF -> Verify order status", dataProviderClass = VMSEndToEndOrderFlowsDP.class)
	public void verifyUpdateCallBackFromFFafterConfirmOrder(HashMap<String, HashMap<String, String>> dataSet)
			throws Exception {
		OMSHelper omsHelper = new OMSHelper();
		HashMap<String, String> orderMap = dataSet.get("orderData");
		HashMap<String, String> omsSessionData = dataSet.get("omsSessionData");
		HashMap<String, String> rmsSessionData = dataSet.get("rmsSessionData");
		HashMap<String, String> restaurantData = dataSet.get("restaurantData");
		if (RMSCommonHelper.isOrderAvailableAtFF(orderMap.get("orderId"))) {
			boolean flg = omsHelper.verifyOrder(orderMap.get("orderId"), "010", omsSessionData.get("X-CSRFToken"),
					omsSessionData.get("sessionId"));
			System.out.println("myAccessToken :" + rmsSessionData.get("accessToken"));
			String date = RMSCommonHelper.getCurrentDateTimeInRequiredFormat(new Date(), "yyyy-MM-dd'T'HH:mm:ss");
			System.out.println("------------------->" + date);
			Thread.sleep(30000);
			String a = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			JsonNode nodeObject = APIUtils.convertStringtoJSON(a);
			Assert.assertEquals(nodeObject.get("statusCode").asInt(), 0);
			Assert.assertEquals(nodeObject.get("statusMessage").asText(), "Success");
			String Obj = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
					Long.parseLong(restaurantData.get("restaurantId")), nodeObject);
			Assert.assertNotNull("FetchOrder API doesnt have OrderCreated " + Obj);
			String resp = rmsHelper.confirm(restaurantData.get("restaurantId"), orderMap.get("orderId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			System.out.println(resp);
			int statusCode = JsonPath.read(resp, "$.statusCode");
			Assert.assertEquals(statusCode, 0);
			Assert.assertEquals(JsonPath.read(resp, "$.statusMessage"), "Order Confirmed");
			
			
			String response=rmsHelper.callback(restaurantData.get("restaurantId"), orderMap.get("orderId"), rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			System.out.println(response);
			int statusCode1 = JsonPath.read(response, "$.statusCode");
			Assert.assertEquals(statusCode1, 0);
			Assert.assertEquals(JsonPath.read(response, "$.statusMessage"), "Call Requested Successfully");
            System.out.println(response);
            
            //============================
            //FF Update Call Implement TBD
          //============================
			
			Thread.sleep(30000);
			String a1 = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			JsonNode nodeObject1 = APIUtils.convertStringtoJSON(a1);
			Assert.assertEquals(nodeObject1.get("statusCode").asInt(), 0);
			Assert.assertEquals(nodeObject1.get("statusMessage").asText(), "Success");
			String Obj1 = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
					Long.parseLong(restaurantData.get("restaurantId")), nodeObject1);
			JsonNode node=APIUtils.convertStringtoJSON(Obj1);
			System.out.println("=============>:::::: "+node.get("status").get("placed_status").asText());
			Assert.assertEquals(node.get("status").get("placed_status").asText(), "out_of_stock");
			
			System.out.println("----->::::::>>"+Obj1);

		} else {
			Assert.fail("FF issue  .. Order Not Reached FF So This DownStream Dependency Failed For VMS");
		}
	}
	

	

//	 @Test(dataProvider = "verifyFetchOrderWithPercentageTD", description = "Login -> Create percentage trade discount -> Create Order -> Fetch orders -> Verify order details with applied discount", dataProviderClass =VMSEndToEndOrderFlowsDP.class)
	public void verifyFetchOrderWithPercentageTD(HashMap<String, HashMap<String, String>> dataSet,String payLoad) throws Exception {
		 int count = 1;
			RMSTDHelper rmsTDHelper = new RMSTDHelper();
			Processor p = null;
			OMSHelper omsHelper = new OMSHelper();
			HashMap<String, String> orderMap = dataSet.get("orderData");
			HashMap<String, String> omsSessionData = dataSet.get("omsSessionData");
			HashMap<String, String> rmsSessionData = dataSet.get("rmsSessionData");
			HashMap<String, String> restaurantData = dataSet.get("restaurantData");
			
			
			
			
			do {
				p = rmsTDHelper.createTD(payLoad, rmsSessionData.get("accessToken"));
				if (p.ResponseValidator.GetNodeValueAsInt("statusCode") == -5) {
					String[] res = p.ResponseValidator.GetNodeValue("data.dataRest.data").split(" ");
					RngHelper.disableEnableTD(res[6], "false");
				} else {
					break;
				}
			} while (count++ < 5);
			Assert.assertEquals(0, p.ResponseValidator.GetNodeValueAsInt("statusCode"));	
			
			Thread.sleep(1000);
			
			String[] orderReponse = RMSCommonHelper.createOrder(restaurantData.get("restaurantId"), RMSConstants.consumerAppMobile,
					RMSConstants.consumerAppPassword, RMSConstants.PAYMENTMODE_CASH, RMSConstants.TEST_ORDER_COMMENTS);
			orderMap.put("orderId", orderReponse[0]);
			orderMap.put("orderReponse", orderReponse[1]);
			
			
			if (RMSCommonHelper.isOrderAvailableAtFF(orderMap.get("orderId"))) {
				boolean flg = omsHelper.verifyOrder(orderMap.get("orderId"), "010", omsSessionData.get("X-CSRFToken"),
						omsSessionData.get("sessionId"));
				System.out.println("myAccessToken :" + rmsSessionData.get("accessToken"));
				String date = RMSCommonHelper.getCurrentDateTimeInRequiredFormat(new Date(), "yyyy-MM-dd'T'HH:mm:ss");
				System.out.println("------------------->" + date);
				Thread.sleep(30000);
				String a = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
						rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
				JsonNode nodeObject = APIUtils.convertStringtoJSON(a);
				Assert.assertEquals(nodeObject.get("statusCode").asInt(), 0);
				Assert.assertEquals(nodeObject.get("statusMessage").asText(), "Success");
				String Obj = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
						Long.parseLong(restaurantData.get("restaurantId")), nodeObject);
				Assert.assertNotNull("FetchOrder API doesnt have OrderCreated " + Obj);
				//Verify TD Amount in Fetch Order response
				
			} else {
				Assert.fail("FF issue  .. Order Not Reached FF So This DownStream Dependency Failed For VMS");
			}
			

	}

	//@Test(dataProvider = "verifyFetchOrderWhenOrderAmountLessThanTDMin", description = "Login -> Create percentage trade discount with minimum -> Create Order for less than minimum amount to avail disocunt-> Fetch orders -> Verify order details", dataProviderClass = VMSEndToEndOrderFlowsDP.class)
	public void verifyFetchOrderWhenOrderAmountLessThanTDMin(HashMap<String, HashMap<String, String>> dataSet,String payLoad)
			throws Exception {
		 int count = 1;
			RMSTDHelper rmsTDHelper = new RMSTDHelper();
			Processor p = null;
			OMSHelper omsHelper = new OMSHelper();
			HashMap<String, String> orderMap = dataSet.get("orderData");
			HashMap<String, String> omsSessionData = dataSet.get("omsSessionData");
			HashMap<String, String> rmsSessionData = dataSet.get("rmsSessionData");
			HashMap<String, String> restaurantData = dataSet.get("restaurantData");
			
			
			
			
			do {
				p = rmsTDHelper.createTD(payLoad, rmsSessionData.get("accessToken"));
				if (p.ResponseValidator.GetNodeValueAsInt("statusCode") == -5) {
					String[] res = p.ResponseValidator.GetNodeValue("data.dataRest.data").split(" ");
					RngHelper.disableEnableTD(res[6], "false");
				} else {
					break;
				}
			} while (count++ < 5);
			Assert.assertEquals(0, p.ResponseValidator.GetNodeValueAsInt("statusCode"));	
			
			Thread.sleep(1000);
			
			String[] orderReponse = RMSCommonHelper.createOrder(restaurantData.get("restaurantId"), RMSConstants.consumerAppMobile,
					RMSConstants.consumerAppPassword, RMSConstants.PAYMENTMODE_CASH, RMSConstants.TEST_ORDER_COMMENTS);
			orderMap.put("orderId", orderReponse[0]);
			orderMap.put("orderReponse", orderReponse[1]);
			
			
			if (RMSCommonHelper.isOrderAvailableAtFF(orderMap.get("orderId"))) {
				boolean flg = omsHelper.verifyOrder(orderMap.get("orderId"), "010", omsSessionData.get("X-CSRFToken"),
						omsSessionData.get("sessionId"));
				System.out.println("myAccessToken :" + rmsSessionData.get("accessToken"));
				String date = RMSCommonHelper.getCurrentDateTimeInRequiredFormat(new Date(), "yyyy-MM-dd'T'HH:mm:ss");
				System.out.println("------------------->" + date);
				Thread.sleep(30000);
				String a = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
						rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
				JsonNode nodeObject = APIUtils.convertStringtoJSON(a);
				Assert.assertEquals(nodeObject.get("statusCode").asInt(), 0);
				Assert.assertEquals(nodeObject.get("statusMessage").asText(), "Success");
				String Obj = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
						Long.parseLong(restaurantData.get("restaurantId")), nodeObject);
				Assert.assertNotNull("FetchOrder API doesnt have OrderCreated " + Obj);
				//Verify TD Amount in Fetch Order response
				
			} else {
				Assert.fail("FF issue  .. Order Not Reached FF So This DownStream Dependency Failed For VMS");
			}
	}

	 //@Test(dataProvider = "verifyFetchOrderWhenOrderAmountMoreThanTDMin", description = "Login -> Create percentage trade discount with minimum -> Create Order for more than minimum amount to avail disocunt-> Fetch orders ->Verify order details with applied discount", dataProviderClass = VMSEndToEndOrderFlowsDP.class)
	public void verifyFetchOrderWhenOrderAmountMoreThanTDMin(HashMap<String, HashMap<String, String>> dataSet,String payLoad)
			throws Exception {
		 int count = 1;
			RMSTDHelper rmsTDHelper = new RMSTDHelper();
			Processor p = null;
			OMSHelper omsHelper = new OMSHelper();
			HashMap<String, String> orderMap = dataSet.get("orderData");
			HashMap<String, String> omsSessionData = dataSet.get("omsSessionData");
			HashMap<String, String> rmsSessionData = dataSet.get("rmsSessionData");
			HashMap<String, String> restaurantData = dataSet.get("restaurantData");
			
			
			
			
			do {
				p = rmsTDHelper.createTD(payLoad, rmsSessionData.get("accessToken"));
				if (p.ResponseValidator.GetNodeValueAsInt("statusCode") == -5) {
					String[] res = p.ResponseValidator.GetNodeValue("data.dataRest.data").split(" ");
					RngHelper.disableEnableTD(res[6], "false");
				} else {
					break;
				}
			} while (count++ < 5);
			Assert.assertEquals(0, p.ResponseValidator.GetNodeValueAsInt("statusCode"));	
			
			Thread.sleep(1000);
			
			String[] orderReponse = RMSCommonHelper.createOrder(restaurantData.get("restaurantId"), RMSConstants.consumerAppMobile,
					RMSConstants.consumerAppPassword, RMSConstants.PAYMENTMODE_CASH, RMSConstants.TEST_ORDER_COMMENTS);
			orderMap.put("orderId", orderReponse[0]);
			orderMap.put("orderReponse", orderReponse[1]);
			
			
			if (RMSCommonHelper.isOrderAvailableAtFF(orderMap.get("orderId"))) {
				boolean flg = omsHelper.verifyOrder(orderMap.get("orderId"), "010", omsSessionData.get("X-CSRFToken"),
						omsSessionData.get("sessionId"));
				System.out.println("myAccessToken :" + rmsSessionData.get("accessToken"));
				String date = RMSCommonHelper.getCurrentDateTimeInRequiredFormat(new Date(), "yyyy-MM-dd'T'HH:mm:ss");
				System.out.println("------------------->" + date);
				Thread.sleep(30000);
				String a = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
						rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
				JsonNode nodeObject = APIUtils.convertStringtoJSON(a);
				Assert.assertEquals(nodeObject.get("statusCode").asInt(), 0);
				Assert.assertEquals(nodeObject.get("statusMessage").asText(), "Success");
				String Obj = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
						Long.parseLong(restaurantData.get("restaurantId")), nodeObject);
				Assert.assertNotNull("FetchOrder API doesnt have OrderCreated " + Obj);
				//Verify TD Amount in Fetch Order response
				
			} else {
				Assert.fail("FF issue  .. Order Not Reached FF So This DownStream Dependency Failed For VMS");
			}
	}

	 //@Test(dataProvider = "verifyFetchOrderWithFlatTD", description = "Login -> Create Net amount trade discount -> Create Order -> Fetch orders -> Verify order details with applied discount", dataProviderClass = VMSEndToEndOrderFlowsDP.class)
	public void verifyFetchOrderWithFlatTD(HashMap<String, HashMap<String, String>> dataSet,String payLoad) throws Exception {
		 int count = 1;
			RMSTDHelper rmsTDHelper = new RMSTDHelper();
			Processor p = null;
			OMSHelper omsHelper = new OMSHelper();
			HashMap<String, String> orderMap = dataSet.get("orderData");
			HashMap<String, String> omsSessionData = dataSet.get("omsSessionData");
			HashMap<String, String> rmsSessionData = dataSet.get("rmsSessionData");
			HashMap<String, String> restaurantData = dataSet.get("restaurantData");
			
			
			
			
			do {
				p = rmsTDHelper.createTD(payLoad, rmsSessionData.get("accessToken"));
				if (p.ResponseValidator.GetNodeValueAsInt("statusCode") == -5) {
					String[] res = p.ResponseValidator.GetNodeValue("data.dataRest.data").split(" ");
					RngHelper.disableEnableTD(res[6], "false");
				} else {
					break;
				}
			} while (count++ < 5);
			Assert.assertEquals(0, p.ResponseValidator.GetNodeValueAsInt("statusCode"));	
			
			Thread.sleep(1000);
			
			String[] orderReponse = RMSCommonHelper.createOrder(restaurantData.get("restaurantId"), RMSConstants.consumerAppMobile,
					RMSConstants.consumerAppPassword, RMSConstants.PAYMENTMODE_CASH, RMSConstants.TEST_ORDER_COMMENTS);
			orderMap.put("orderId", orderReponse[0]);
			orderMap.put("orderReponse", orderReponse[1]);
			
			
			if (RMSCommonHelper.isOrderAvailableAtFF(orderMap.get("orderId"))) {
				boolean flg = omsHelper.verifyOrder(orderMap.get("orderId"), "010", omsSessionData.get("X-CSRFToken"),
						omsSessionData.get("sessionId"));
				System.out.println("myAccessToken :" + rmsSessionData.get("accessToken"));
				String date = RMSCommonHelper.getCurrentDateTimeInRequiredFormat(new Date(), "yyyy-MM-dd'T'HH:mm:ss");
				System.out.println("------------------->" + date);
				Thread.sleep(30000);
				String a = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
						rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
				JsonNode nodeObject = APIUtils.convertStringtoJSON(a);
				Assert.assertEquals(nodeObject.get("statusCode").asInt(), 0);
				Assert.assertEquals(nodeObject.get("statusMessage").asText(), "Success");
				String Obj = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
						Long.parseLong(restaurantData.get("restaurantId")), nodeObject);
				Assert.assertNotNull("FetchOrder API doesnt have OrderCreated " + Obj);
				//Verify TD Amount in Fetch Order response
				
			} else {
				Assert.fail("FF issue  .. Order Not Reached FF So This DownStream Dependency Failed For VMS");
			}
			
	}

 //@Test(dataProvider = "verifyFetchOrderWithFlatTDLessthanMinAmount", description = "Login -> Create Net amount trade discount with minimum -> Create Order for less than minimum amount to avail disocunt-> Fetch orders -> Verify order details", dataProviderClass = VMSEndToEndOrderFlowsDP.class)
	public void verifyFetchOrderWithFlatTDLessthanMinAmount(HashMap<String, HashMap<String, String>> dataSet,String payLoad)
			throws Exception {
		 int count = 1;
			RMSTDHelper rmsTDHelper = new RMSTDHelper();
			Processor p = null;
			OMSHelper omsHelper = new OMSHelper();
			HashMap<String, String> orderMap = dataSet.get("orderData");
			HashMap<String, String> omsSessionData = dataSet.get("omsSessionData");
			HashMap<String, String> rmsSessionData = dataSet.get("rmsSessionData");
			HashMap<String, String> restaurantData = dataSet.get("restaurantData");
			
			
			
			
			do {
				p = rmsTDHelper.createTD(payLoad, rmsSessionData.get("accessToken"));
				if (p.ResponseValidator.GetNodeValueAsInt("statusCode") == -5) {
					String[] res = p.ResponseValidator.GetNodeValue("data.dataRest.data").split(" ");
					RngHelper.disableEnableTD(res[6], "false");
				} else {
					break;
				}
			} while (count++ < 5);
			Assert.assertEquals(0, p.ResponseValidator.GetNodeValueAsInt("statusCode"));	
			
			Thread.sleep(1000);
			
			String[] orderReponse = RMSCommonHelper.createOrder(restaurantData.get("restaurantId"), RMSConstants.consumerAppMobile,
					RMSConstants.consumerAppPassword, RMSConstants.PAYMENTMODE_CASH, RMSConstants.TEST_ORDER_COMMENTS);
			orderMap.put("orderId", orderReponse[0]);
			orderMap.put("orderReponse", orderReponse[1]);
			
			
			if (RMSCommonHelper.isOrderAvailableAtFF(orderMap.get("orderId"))) {
				boolean flg = omsHelper.verifyOrder(orderMap.get("orderId"), "010", omsSessionData.get("X-CSRFToken"),
						omsSessionData.get("sessionId"));
				System.out.println("myAccessToken :" + rmsSessionData.get("accessToken"));
				String date = RMSCommonHelper.getCurrentDateTimeInRequiredFormat(new Date(), "yyyy-MM-dd'T'HH:mm:ss");
				System.out.println("------------------->" + date);
				Thread.sleep(30000);
				String a = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
						rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
				JsonNode nodeObject = APIUtils.convertStringtoJSON(a);
				Assert.assertEquals(nodeObject.get("statusCode").asInt(), 0);
				Assert.assertEquals(nodeObject.get("statusMessage").asText(), "Success");
				String Obj = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
						Long.parseLong(restaurantData.get("restaurantId")), nodeObject);
				Assert.assertNotNull("FetchOrder API doesnt have OrderCreated " + Obj);
				//Verify TD Amount in Fetch Order response
				
			} else {
				Assert.fail("FF issue  .. Order Not Reached FF So This DownStream Dependency Failed For VMS");
			}
			
	}

 //@Test(dataProvider = "verifyFetchOrderWithFlatTDMorethanMinAmount", description = "Login -> Create Net amount trade discount with minimum ->Create Order for more than minimum amount to avail disocunt-> Fetch orders -> Verify order details with applied discount", dataProviderClass = VMSEndToEndOrderFlowsDP.class)
	public void verifyFetchOrderWithFlatTDMorethanMinAmount(HashMap<String, HashMap<String, String>> dataSet,String payLoad)
			throws Exception {
		 int count = 1;
			RMSTDHelper rmsTDHelper = new RMSTDHelper();
			Processor p = null;
			OMSHelper omsHelper = new OMSHelper();
			HashMap<String, String> orderMap = dataSet.get("orderData");
			HashMap<String, String> omsSessionData = dataSet.get("omsSessionData");
			HashMap<String, String> rmsSessionData = dataSet.get("rmsSessionData");
			HashMap<String, String> restaurantData = dataSet.get("restaurantData");
			
			
			
			
			do {
				p = rmsTDHelper.createTD(payLoad, rmsSessionData.get("accessToken"));
				if (p.ResponseValidator.GetNodeValueAsInt("statusCode") == -5) {
					String[] res = p.ResponseValidator.GetNodeValue("data.dataRest.data").split(" ");
					RngHelper.disableEnableTD(res[6], "false");
				} else {
					break;
				}
			} while (count++ < 5);
			Assert.assertEquals(0, p.ResponseValidator.GetNodeValueAsInt("statusCode"));	
			
			Thread.sleep(1000);
			
			String[] orderReponse = RMSCommonHelper.createOrder(restaurantData.get("restaurantId"), RMSConstants.consumerAppMobile,
					RMSConstants.consumerAppPassword, RMSConstants.PAYMENTMODE_CASH, RMSConstants.TEST_ORDER_COMMENTS);
			orderMap.put("orderId", orderReponse[0]);
			orderMap.put("orderReponse", orderReponse[1]);
			
			
			if (RMSCommonHelper.isOrderAvailableAtFF(orderMap.get("orderId"))) {
				boolean flg = omsHelper.verifyOrder(orderMap.get("orderId"), "010", omsSessionData.get("X-CSRFToken"),
						omsSessionData.get("sessionId"));
				System.out.println("myAccessToken :" + rmsSessionData.get("accessToken"));
				String date = RMSCommonHelper.getCurrentDateTimeInRequiredFormat(new Date(), "yyyy-MM-dd'T'HH:mm:ss");
				System.out.println("------------------->" + date);
				Thread.sleep(30000);
				String a = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
						rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
				JsonNode nodeObject = APIUtils.convertStringtoJSON(a);
				Assert.assertEquals(nodeObject.get("statusCode").asInt(), 0);
				Assert.assertEquals(nodeObject.get("statusMessage").asText(), "Success");
				String Obj = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
						Long.parseLong(restaurantData.get("restaurantId")), nodeObject);
				Assert.assertNotNull("FetchOrder API doesnt have OrderCreated " + Obj);
				//Verify TD Amount in Fetch Order response
				
			} else {
				Assert.fail("FF issue  .. Order Not Reached FF So This DownStream Dependency Failed For VMS");
			}
	}

 ////@Test(dataProvider = "verifyFetchOrderAfterApplyingCoupon", description ="Create order by applying Coupon Discount ->Login -> Fetch Orders -> Verify Order bill details", dataProviderClass = VMSEndToEndOrderFlowsDP.class)
	public void verifyFetchOrderAfterApplyingCoupon(HashMap<String, HashMap<String, String>> dataSet) throws Exception {
		OMSHelper omsHelper = new OMSHelper();
		HashMap<String, String> orderMap = dataSet.get("orderData");
		HashMap<String, String> omsSessionData = dataSet.get("omsSessionData");
		HashMap<String, String> rmsSessionData = dataSet.get("rmsSessionData");
		HashMap<String, String> restaurantData = dataSet.get("restaurantData");
		if (RMSCommonHelper.isOrderAvailableAtFF(orderMap.get("orderId"))) {
			boolean flg = omsHelper.verifyOrder(orderMap.get("orderId"), "010", omsSessionData.get("X-CSRFToken"),
					omsSessionData.get("sessionId"));
			System.out.println("myAccessToken :" + rmsSessionData.get("accessToken"));
			String date = RMSCommonHelper.getCurrentDateTimeInRequiredFormat(new Date(), "yyyy-MM-dd'T'HH:mm:ss");
			System.out.println("------------------->" + date);
			Thread.sleep(60000);
			String a = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			JsonNode nodeObject = APIUtils.convertStringtoJSON(a);
			Assert.assertEquals(nodeObject.get("statusCode").asInt(), 0);
			Assert.assertEquals(nodeObject.get("statusMessage").asText(), "Success");
			String Obj = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
					Long.parseLong(restaurantData.get("restaurantId")), nodeObject);
			Assert.assertNotNull("FetchOrder API doesnt have OrderCreated " + Obj);

		} else {
			Assert.fail("FF issue  .. Order Not Reached FF So This DownStream Dependency Failed For VMS");
		}
	}

	/// //@Test(dataProvider = "verifyFetchOrderAfterApplyingCouponNMarkOOS", description = "Create order by applying Coupon Discount ->Login -> Fetch Orders -> Mark order OOS -> Verify Order status", dataProviderClass =VMSEndToEndOrderFlowsDP.class)
	public void verifyFetchOrderAfterApplyingCouponNMarkOOS(HashMap<String, HashMap<String, String>> dataSet)
			throws Exception {
		OMSHelper omsHelper = new OMSHelper();
		HashMap<String, String> orderMap = dataSet.get("orderData");
		HashMap<String, String> omsSessionData = dataSet.get("omsSessionData");
		HashMap<String, String> rmsSessionData = dataSet.get("rmsSessionData");
		HashMap<String, String> restaurantData = dataSet.get("restaurantData");
		if (RMSCommonHelper.isOrderAvailableAtFF(orderMap.get("orderId"))) {
			boolean flg = omsHelper.verifyOrder(orderMap.get("orderId"), "010", omsSessionData.get("X-CSRFToken"),
					omsSessionData.get("sessionId"));
			System.out.println("myAccessToken :" + rmsSessionData.get("accessToken"));
			String date = RMSCommonHelper.getCurrentDateTimeInRequiredFormat(new Date(), "yyyy-MM-dd'T'HH:mm:ss");
			System.out.println("------------------->" + date);
			Thread.sleep(60000);
			String a = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			JsonNode nodeObject = APIUtils.convertStringtoJSON(a);
			Assert.assertEquals(nodeObject.get("statusCode").asInt(), 0);
			Assert.assertEquals(nodeObject.get("statusMessage").asText(), "Success");
			String Obj = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
					Long.parseLong(restaurantData.get("restaurantId")), nodeObject);
			Assert.assertNotNull("FetchOrder API doesnt have OrderCreated " + Obj);

		} else {
			Assert.fail("FF issue  .. Order Not Reached FF So This DownStream Dependency Failed For VMS");
		}
	}

	// //@Test(dataProvider = "verifyFetchOrderAfterApplyingCouponNMarkOOSNOrderEdit", description = "Create order by applying Coupon Discount ->Login -> Fetch Orders -> Mark order OOS -> Edit order from FF -> Verify Order status", dataProviderClass = VMSEndToEndOrderFlowsDP.class)
	public void verifyFetchOrderAfterApplyingCouponNMarkOOSNOrderEdit(HashMap<String, HashMap<String, String>> dataSet)
			throws Exception {
		OMSHelper omsHelper = new OMSHelper();
		HashMap<String, String> orderMap = dataSet.get("orderData");
		HashMap<String, String> omsSessionData = dataSet.get("omsSessionData");
		HashMap<String, String> rmsSessionData = dataSet.get("rmsSessionData");
		HashMap<String, String> restaurantData = dataSet.get("restaurantData");
		if (RMSCommonHelper.isOrderAvailableAtFF(orderMap.get("orderId"))) {
			boolean flg = omsHelper.verifyOrder(orderMap.get("orderId"), "010", omsSessionData.get("X-CSRFToken"),
					omsSessionData.get("sessionId"));
			System.out.println("myAccessToken :" + rmsSessionData.get("accessToken"));
			String date = RMSCommonHelper.getCurrentDateTimeInRequiredFormat(new Date(), "yyyy-MM-dd'T'HH:mm:ss");
			System.out.println("------------------->" + date);
			Thread.sleep(60000);
			String a = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			JsonNode nodeObject = APIUtils.convertStringtoJSON(a);
			Assert.assertEquals(nodeObject.get("statusCode").asInt(), 0);
			Assert.assertEquals(nodeObject.get("statusMessage").asText(), "Success");
			String Obj = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
					Long.parseLong(restaurantData.get("restaurantId")), nodeObject);
			Assert.assertNotNull("FetchOrder API doesnt have OrderCreated " + Obj);

		} else {
			Assert.fail("FF issue  .. Order Not Reached FF So This DownStream Dependency Failed For VMS");
		}
	}

	// //@Test(dataProvider = "verifyFetchOrderWithAddons", description = "OrderWithAddon-->Create Order -> Login -> Fetch orders -> Verify order details", dataProviderClass = VMSEndToEndOrderFlowsDP.class)
	public void verifyFetchOrderWithAddons(HashMap<String, HashMap<String, String>> dataSet) throws Exception {
		OMSHelper omsHelper = new OMSHelper();
		HashMap<String, String> orderMap = dataSet.get("orderData");
		HashMap<String, String> omsSessionData = dataSet.get("omsSessionData");
		HashMap<String, String> rmsSessionData = dataSet.get("rmsSessionData");
		HashMap<String, String> restaurantData = dataSet.get("restaurantData");
		if (RMSCommonHelper.isOrderAvailableAtFF(orderMap.get("orderId"))) {
			boolean flg = omsHelper.verifyOrder(orderMap.get("orderId"), "010", omsSessionData.get("X-CSRFToken"),
					omsSessionData.get("sessionId"));
			System.out.println("myAccessToken :" + rmsSessionData.get("accessToken"));
			String date = RMSCommonHelper.getCurrentDateTimeInRequiredFormat(new Date(), "yyyy-MM-dd'T'HH:mm:ss");
			System.out.println("------------------->" + date);
			Thread.sleep(60000);
			String a = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			JsonNode nodeObject = APIUtils.convertStringtoJSON(a);
			Assert.assertEquals(nodeObject.get("statusCode").asInt(), 0);
			Assert.assertEquals(nodeObject.get("statusMessage").asText(), "Success");
			String Obj = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
					Long.parseLong(restaurantData.get("restaurantId")), nodeObject);
			Assert.assertNotNull("FetchOrder API doesnt have OrderCreated " + Obj);

		} else {
			Assert.fail("FF issue  .. Order Not Reached FF So This DownStream Dependency Failed For VMS");
		}
	}

	// //@Test(dataProvider = "verifyFetchOrderWithAddonsAfterOOSNOrderEdit", description = "OrderWithAddon-->Create Order -> Login -> Fetch orders -> Mark order OOS(Out of Stock) -> Edit Order from FF -> Verify order status",dataProviderClass = VMSEndToEndOrderFlowsDP.class)
	public void verifyFetchOrderWithAddonsAfterOOSNOrderEdit(HashMap<String, HashMap<String, String>> dataSet)
			throws Exception {
		OMSHelper omsHelper = new OMSHelper();
		HashMap<String, String> orderMap = dataSet.get("orderData");
		HashMap<String, String> omsSessionData = dataSet.get("omsSessionData");
		HashMap<String, String> rmsSessionData = dataSet.get("rmsSessionData");
		HashMap<String, String> restaurantData = dataSet.get("restaurantData");
		if (RMSCommonHelper.isOrderAvailableAtFF(orderMap.get("orderId"))) {
			boolean flg = omsHelper.verifyOrder(orderMap.get("orderId"), "010", omsSessionData.get("X-CSRFToken"),
					omsSessionData.get("sessionId"));
			System.out.println("myAccessToken :" + rmsSessionData.get("accessToken"));
			String date = RMSCommonHelper.getCurrentDateTimeInRequiredFormat(new Date(), "yyyy-MM-dd'T'HH:mm:ss");
			System.out.println("------------------->" + date);
			Thread.sleep(60000);
			String a = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			JsonNode nodeObject = APIUtils.convertStringtoJSON(a);
			Assert.assertEquals(nodeObject.get("statusCode").asInt(), 0);
			Assert.assertEquals(nodeObject.get("statusMessage").asText(), "Success");
			String Obj = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
					Long.parseLong(restaurantData.get("restaurantId")), nodeObject);
			Assert.assertNotNull("FetchOrder API doesnt have OrderCreated " + Obj);

		} else {
			Assert.fail("FF issue  .. Order Not Reached FF So This DownStream Dependency Failed For VMS");
		}
	}

	// //@Test(dataProvider =
	// "verifyFetchOrderWithAddonsAfterOOSWithVariantNOrderEdit", description =
	// "OrderWithAddon-->Create Order -> Login -> Fetch orders -> Confirm order ->
	// Mark order OOS(Send variant item as alternative) -> Edit Order from FF ->
	// Verify order status", dataProviderClass = VMSEndToEndOrderFlowsDP.class)
	public void verifyFetchOrderWithAddonsAfterOOSWithVariantNOrderEdit(
			HashMap<String, HashMap<String, String>> dataSet) throws Exception {
		OMSHelper omsHelper = new OMSHelper();
		HashMap<String, String> orderMap = dataSet.get("orderData");
		HashMap<String, String> omsSessionData = dataSet.get("omsSessionData");
		HashMap<String, String> rmsSessionData = dataSet.get("rmsSessionData");
		HashMap<String, String> restaurantData = dataSet.get("restaurantData");
		if (RMSCommonHelper.isOrderAvailableAtFF(orderMap.get("orderId"))) {
			boolean flg = omsHelper.verifyOrder(orderMap.get("orderId"), "010", omsSessionData.get("X-CSRFToken"),
					omsSessionData.get("sessionId"));
			System.out.println("myAccessToken :" + rmsSessionData.get("accessToken"));
			String date = RMSCommonHelper.getCurrentDateTimeInRequiredFormat(new Date(), "yyyy-MM-dd'T'HH:mm:ss");
			System.out.println("------------------->" + date);
			Thread.sleep(60000);
			String a = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			JsonNode nodeObject = APIUtils.convertStringtoJSON(a);
			Assert.assertEquals(nodeObject.get("statusCode").asInt(), 0);
			Assert.assertEquals(nodeObject.get("statusMessage").asText(), "Success");
			String Obj = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
					Long.parseLong(restaurantData.get("restaurantId")), nodeObject);
			Assert.assertNotNull("FetchOrder API doesnt have OrderCreated " + Obj);

		} else {
			Assert.fail("FF issue  .. Order Not Reached FF So This DownStream Dependency Failed For VMS");
		}
	}

	// //@Test(dataProvider = "verifyFetchOrderWithAddonsAfterOOSWithVariantNOrderEditNCOnfirm", description = "OrderWithAddon-->Create Order -> Login -> Fetch orders -> Confirm order -> Mark order OOS(Send variant item as alternative) -> Edit Order from FF -> Confirm order -> Verify order status", dataProviderClass = VMSEndToEndOrderFlowsDP.class)
	public void verifyFetchOrderWithAddonsAfterOOSWithVariantNOrderEditNConfirm(
			HashMap<String, HashMap<String, String>> dataSet) throws Exception {
		OMSHelper omsHelper = new OMSHelper();
		HashMap<String, String> orderMap = dataSet.get("orderData");
		HashMap<String, String> omsSessionData = dataSet.get("omsSessionData");
		HashMap<String, String> rmsSessionData = dataSet.get("rmsSessionData");
		HashMap<String, String> restaurantData = dataSet.get("restaurantData");
		if (RMSCommonHelper.isOrderAvailableAtFF(orderMap.get("orderId"))) {
			boolean flg = omsHelper.verifyOrder(orderMap.get("orderId"), "010", omsSessionData.get("X-CSRFToken"),
					omsSessionData.get("sessionId"));
			System.out.println("myAccessToken :" + rmsSessionData.get("accessToken"));
			String date = RMSCommonHelper.getCurrentDateTimeInRequiredFormat(new Date(), "yyyy-MM-dd'T'HH:mm:ss");
			System.out.println("------------------->" + date);
			Thread.sleep(60000);
			String a = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			JsonNode nodeObject = APIUtils.convertStringtoJSON(a);
			Assert.assertEquals(nodeObject.get("statusCode").asInt(), 0);
			Assert.assertEquals(nodeObject.get("statusMessage").asText(), "Success");
			String Obj = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
					Long.parseLong(restaurantData.get("restaurantId")), nodeObject);
			Assert.assertNotNull("FetchOrder API doesnt have OrderCreated " + Obj);

		} else {
			Assert.fail("FF issue  .. Order Not Reached FF So This DownStream Dependency Failed For VMS");
		}
	}

	// //@Test(dataProvider = "verifyFetchOrderWithVariants", description = "Create Order -> Login -> Fetch orders -> Verify order details", dataProviderClass = VMSEndToEndOrderFlowsDP.class)
	public void verifyFetchOrderWithVariants(HashMap<String, HashMap<String, String>> dataSet) throws Exception {
		OMSHelper omsHelper = new OMSHelper();
		HashMap<String, String> orderMap = dataSet.get("orderData");
		HashMap<String, String> omsSessionData = dataSet.get("omsSessionData");
		HashMap<String, String> rmsSessionData = dataSet.get("rmsSessionData");
		HashMap<String, String> restaurantData = dataSet.get("restaurantData");
		if (RMSCommonHelper.isOrderAvailableAtFF(orderMap.get("orderId"))) {
			boolean flg = omsHelper.verifyOrder(orderMap.get("orderId"), "010", omsSessionData.get("X-CSRFToken"),
					omsSessionData.get("sessionId"));
			System.out.println("myAccessToken :" + rmsSessionData.get("accessToken"));
			String date = RMSCommonHelper.getCurrentDateTimeInRequiredFormat(new Date(), "yyyy-MM-dd'T'HH:mm:ss");
			System.out.println("------------------->" + date);
			Thread.sleep(60000);
			String a = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			JsonNode nodeObject = APIUtils.convertStringtoJSON(a);
			Assert.assertEquals(nodeObject.get("statusCode").asInt(), 0);
			Assert.assertEquals(nodeObject.get("statusMessage").asText(), "Success");
			String Obj = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
					Long.parseLong(restaurantData.get("restaurantId")), nodeObject);
			Assert.assertNotNull("FetchOrder API doesnt have OrderCreated " + Obj);

		} else {
			Assert.fail("FF issue  .. Order Not Reached FF So This DownStream Dependency Failed For VMS");
		}
	}

	// //@Test(dataProvider = "verifyFetchOrderWithVariantsAfterOOSNOrderEdit", description = "Create Order -> Login -> Fetch orders -> Mark order OOS(Out of Stock) -> Edit Order from FF -> Verify order status", dataProviderClass =VMSEndToEndOrderFlowsDP.class)
	public void verifyFetchOrderWithVariantsAfterOOSNOrderEdit(HashMap<String, HashMap<String, String>> dataSet)
			throws Exception {
		OMSHelper omsHelper = new OMSHelper();
		HashMap<String, String> orderMap = dataSet.get("orderData");
		HashMap<String, String> omsSessionData = dataSet.get("omsSessionData");
		HashMap<String, String> rmsSessionData = dataSet.get("rmsSessionData");
		HashMap<String, String> restaurantData = dataSet.get("restaurantData");
		if (RMSCommonHelper.isOrderAvailableAtFF(orderMap.get("orderId"))) {
			boolean flg = omsHelper.verifyOrder(orderMap.get("orderId"), "010", omsSessionData.get("X-CSRFToken"),
					omsSessionData.get("sessionId"));
			System.out.println("myAccessToken :" + rmsSessionData.get("accessToken"));
			String date = RMSCommonHelper.getCurrentDateTimeInRequiredFormat(new Date(), "yyyy-MM-dd'T'HH:mm:ss");
			System.out.println("------------------->" + date);
			Thread.sleep(60000);
			String a = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			JsonNode nodeObject = APIUtils.convertStringtoJSON(a);
			Assert.assertEquals(nodeObject.get("statusCode").asInt(), 0);
			Assert.assertEquals(nodeObject.get("statusMessage").asText(), "Success");
			String Obj = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
					Long.parseLong(restaurantData.get("restaurantId")), nodeObject);
			Assert.assertNotNull("FetchOrder API doesnt have OrderCreated " + Obj);

		} else {
			Assert.fail("FF issue  .. Order Not Reached FF So This DownStream Dependency Failed For VMS");
		}
	}

	// //@Test(dataProvider = "verifyFetchOrderWithVariantsAfterOOSWithAddonsNOrderEdit", description = "Create Order -> Login -> Fetch orders -> Confirm order -> Mark order OOS(Send addon item as alternative) -> Edit Order from FF -> Verify orderstatus", dataProviderClass = VMSEndToEndOrderFlowsDP.class)
	public void verifyFetchOrderWithVariantsAfterOOSWithAddonsNOrderEdit(
			HashMap<String, HashMap<String, String>> dataSet) throws Exception {
		OMSHelper omsHelper = new OMSHelper();
		HashMap<String, String> orderMap = dataSet.get("orderData");
		HashMap<String, String> omsSessionData = dataSet.get("omsSessionData");
		HashMap<String, String> rmsSessionData = dataSet.get("rmsSessionData");
		HashMap<String, String> restaurantData = dataSet.get("restaurantData");
		if (RMSCommonHelper.isOrderAvailableAtFF(orderMap.get("orderId"))) {
			boolean flg = omsHelper.verifyOrder(orderMap.get("orderId"), "010", omsSessionData.get("X-CSRFToken"),
					omsSessionData.get("sessionId"));
			System.out.println("myAccessToken :" + rmsSessionData.get("accessToken"));
			String date = RMSCommonHelper.getCurrentDateTimeInRequiredFormat(new Date(), "yyyy-MM-dd'T'HH:mm:ss");
			System.out.println("------------------->" + date);
			Thread.sleep(60000);
			String a = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			JsonNode nodeObject = APIUtils.convertStringtoJSON(a);
			Assert.assertEquals(nodeObject.get("statusCode").asInt(), 0);
			Assert.assertEquals(nodeObject.get("statusMessage").asText(), "Success");
			String Obj = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
					Long.parseLong(restaurantData.get("restaurantId")), nodeObject);
			Assert.assertNotNull("FetchOrder API doesnt have OrderCreated " + Obj);

		} else {
			Assert.fail("FF issue  .. Order Not Reached FF So This DownStream Dependency Failed For VMS");
		}
	}

	// //@Test(dataProvider = "verifyFetchOrderWithVariantsAfterOOSWithAddonsNOrderEditNConfirm",description = "Create Order -> Login -> Fetch orders -> Confirm order -> Mark order OOS(Send addon item as alternative) -> Edit Order from FF -> Confirm order -> Verify order status", dataProviderClass = VMSEndToEndOrderFlowsDP.class)
	public void verifyFetchOrderWithVariantsAfterOOSWithAddonsNOrderEditNConfirm(
			HashMap<String, HashMap<String, String>> dataSet) throws Exception {
		OMSHelper omsHelper = new OMSHelper();
		HashMap<String, String> orderMap = dataSet.get("orderData");
		HashMap<String, String> omsSessionData = dataSet.get("omsSessionData");
		HashMap<String, String> rmsSessionData = dataSet.get("rmsSessionData");
		HashMap<String, String> restaurantData = dataSet.get("restaurantData");
		if (RMSCommonHelper.isOrderAvailableAtFF(orderMap.get("orderId"))) {
			boolean flg = omsHelper.verifyOrder(orderMap.get("orderId"), "010", omsSessionData.get("X-CSRFToken"),
					omsSessionData.get("sessionId"));
			System.out.println("myAccessToken :" + rmsSessionData.get("accessToken"));
			String date = RMSCommonHelper.getCurrentDateTimeInRequiredFormat(new Date(), "yyyy-MM-dd'T'HH:mm:ss");
			System.out.println("------------------->" + date);
			Thread.sleep(60000);
			String a = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			JsonNode nodeObject = APIUtils.convertStringtoJSON(a);
			Assert.assertEquals(nodeObject.get("statusCode").asInt(), 0);
			Assert.assertEquals(nodeObject.get("statusMessage").asText(), "Success");
			String Obj = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
					Long.parseLong(restaurantData.get("restaurantId")), nodeObject);
			Assert.assertNotNull("FetchOrder API doesnt have OrderCreated " + Obj);

		} else {
			Assert.fail("FF issue  .. Order Not Reached FF So This DownStream Dependency Failed For VMS");
		}
	}

//	 @Test(dataProvider = "verifyFetchOrderWithItemsOfAddonsNVariants",description = "Place order with addon and varinat-->Create Order -> Login ->Fetch orders -> Verify order details", dataProviderClass =VMSEndToEndOrderFlowsDP.class)
	public void verifyFetchOrderWithItemsOfAddonsNVariants(HashMap<String, HashMap<String, String>> dataSet)
			throws Exception {
		OMSHelper omsHelper = new OMSHelper();
		HashMap<String, String> orderMap = dataSet.get("orderData");
		HashMap<String, String> omsSessionData = dataSet.get("omsSessionData");
		HashMap<String, String> rmsSessionData = dataSet.get("rmsSessionData");
		HashMap<String, String> restaurantData = dataSet.get("restaurantData");
		if (RMSCommonHelper.isOrderAvailableAtFF(orderMap.get("orderId"))) {
			boolean flg = omsHelper.verifyOrder(orderMap.get("orderId"), "010", omsSessionData.get("X-CSRFToken"),
					omsSessionData.get("sessionId"));
			System.out.println("myAccessToken :" + rmsSessionData.get("accessToken"));
			String date = RMSCommonHelper.getCurrentDateTimeInRequiredFormat(new Date(), "yyyy-MM-dd'T'HH:mm:ss");
			System.out.println("------------------->" + date);
			Thread.sleep(60000);
			String a = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			JsonNode nodeObject = APIUtils.convertStringtoJSON(a);
			Assert.assertEquals(nodeObject.get("statusCode").asInt(), 0);
			Assert.assertEquals(nodeObject.get("statusMessage").asText(), "Success");
			String Obj = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
					Long.parseLong(restaurantData.get("restaurantId")), nodeObject);
			Assert.assertNotNull("FetchOrder API doesnt have OrderCreated " + Obj);

		} else {
			Assert.fail("FF issue  .. Order Not Reached FF So This DownStream Dependency Failed For VMS");
		}
	}

	// //@Test(dataProvider ="verifyFetchOrderWithItemsOfAddonsNVariantsWhenOOSNFFEditOrder", description = "Place order with addon and varinat-->Create Order -> Login -> Fetch orders -> Mark order OOS(Out of Stock) -> Edit Order from FF -> Verify order status", dataProviderClass = VMSEndToEndOrderFlowsDP.class)
	public void verifyFetchOrderWithItemsOfAddonsNVariantsWhenOOSNFFEditOrder(
			HashMap<String, HashMap<String, String>> dataSet) throws Exception {
		OMSHelper omsHelper = new OMSHelper();
		HashMap<String, String> orderMap = dataSet.get("orderData");
		HashMap<String, String> omsSessionData = dataSet.get("omsSessionData");
		HashMap<String, String> rmsSessionData = dataSet.get("rmsSessionData");
		HashMap<String, String> restaurantData = dataSet.get("restaurantData");
		if (RMSCommonHelper.isOrderAvailableAtFF(orderMap.get("orderId"))) {
			boolean flg = omsHelper.verifyOrder(orderMap.get("orderId"), "010", omsSessionData.get("X-CSRFToken"),
					omsSessionData.get("sessionId"));
			System.out.println("myAccessToken :" + rmsSessionData.get("accessToken"));
			String date = RMSCommonHelper.getCurrentDateTimeInRequiredFormat(new Date(), "yyyy-MM-dd'T'HH:mm:ss");
			System.out.println("------------------->" + date);
			Thread.sleep(60000);
			String a = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			JsonNode nodeObject = APIUtils.convertStringtoJSON(a);
			Assert.assertEquals(nodeObject.get("statusCode").asInt(), 0);
			Assert.assertEquals(nodeObject.get("statusMessage").asText(), "Success");
			String Obj = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
					Long.parseLong(restaurantData.get("restaurantId")), nodeObject);
			Assert.assertNotNull("FetchOrder API doesnt have OrderCreated " + Obj);

		} else {
			Assert.fail("FF issue  .. Order Not Reached FF So This DownStream Dependency Failed For VMS");
		}
	}

	// //@Test(dataProvider ="verifyFetchOrderWithItemsOfAddonsNVariantsWhenOOSNFFEditOrderWithNormalItem", description = "Place order with addon and varinat-->Create Order -> Login -> Fetch orders -> Confirm order -> Mark order OOS(Send normal item asalternative) -> Edit Order from FF -> Verify order status", dataProviderClass = VMSEndToEndOrderFlowsDP.class)
	public void verifyFetchOrderWithItemsOfAddonsNVariantsWhenOOSNFFEditOrderWithNormalItem(
			HashMap<String, HashMap<String, String>> dataSet) throws Exception {
		OMSHelper omsHelper = new OMSHelper();
		HashMap<String, String> orderMap = dataSet.get("orderData");
		HashMap<String, String> omsSessionData = dataSet.get("omsSessionData");
		HashMap<String, String> rmsSessionData = dataSet.get("rmsSessionData");
		HashMap<String, String> restaurantData = dataSet.get("restaurantData");
		if (RMSCommonHelper.isOrderAvailableAtFF(orderMap.get("orderId"))) {
			boolean flg = omsHelper.verifyOrder(orderMap.get("orderId"), "010", omsSessionData.get("X-CSRFToken"),
					omsSessionData.get("sessionId"));
			System.out.println("myAccessToken :" + rmsSessionData.get("accessToken"));
			String date = RMSCommonHelper.getCurrentDateTimeInRequiredFormat(new Date(), "yyyy-MM-dd'T'HH:mm:ss");
			System.out.println("------------------->" + date);
			Thread.sleep(60000);
			String a = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			JsonNode nodeObject = APIUtils.convertStringtoJSON(a);
			Assert.assertEquals(nodeObject.get("statusCode").asInt(), 0);
			Assert.assertEquals(nodeObject.get("statusMessage").asText(), "Success");
			String Obj = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
					Long.parseLong(restaurantData.get("restaurantId")), nodeObject);
			Assert.assertNotNull("FetchOrder API doesnt have OrderCreated " + Obj);

		} else {
			Assert.fail("FF issue  .. Order Not Reached FF So This DownStream Dependency Failed For VMS");
		}
	}

//@Test(dataProvider = "verifyConfirmWhenOOSNOrderEditWIthNormalItem",description = "Place order with addon and varinat-->Create Order -> Login -> Fetch orders -> Confirm order -> Mark order OOS(Send normal item as alternative) -> Edit Order from FF -> Confirm order -> Verify order status",dataProviderClass = VMSEndToEndOrderFlowsDP.class)
	public void verifyConfirmWhenOOSNOrderEditWIthNormalItem(HashMap<String, HashMap<String, String>> dataSet)
			throws Exception {
		OMSHelper omsHelper = new OMSHelper();
		HashMap<String, String> orderMap = dataSet.get("orderData");
		HashMap<String, String> omsSessionData = dataSet.get("omsSessionData");
		HashMap<String, String> rmsSessionData = dataSet.get("rmsSessionData");
		HashMap<String, String> restaurantData = dataSet.get("restaurantData");
		if (RMSCommonHelper.isOrderAvailableAtFF(orderMap.get("orderId"))) {
			boolean flg = omsHelper.verifyOrder(orderMap.get("orderId"), "010", omsSessionData.get("X-CSRFToken"),
					omsSessionData.get("sessionId"));
			System.out.println("myAccessToken :" + rmsSessionData.get("accessToken"));
			String date = RMSCommonHelper.getCurrentDateTimeInRequiredFormat(new Date(), "yyyy-MM-dd'T'HH:mm:ss");
			System.out.println("------------------->" + date);
			Thread.sleep(60000);
			String a = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			JsonNode nodeObject = APIUtils.convertStringtoJSON(a);
			Assert.assertEquals(nodeObject.get("statusCode").asInt(), 0);
			Assert.assertEquals(nodeObject.get("statusMessage").asText(), "Success");
			String Obj = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
					Long.parseLong(restaurantData.get("restaurantId")), nodeObject);
			Assert.assertNotNull("FetchOrder API doesnt have OrderCreated " + Obj);

		} else {
			Assert.fail("FF issue  .. Order Not Reached FF So This DownStream Dependency Failed For VMS");
		}
	}

	// //@Test(dataProvider = "verifyFetchOrderhavingMultipleItems", description ="Place order for multiple normal items->Create Order -> Login -> Fetch orders-> Verify order details", dataProviderClass = VMSEndToEndOrderFlowsDP.class)
	public void verifyFetchOrderhavingMultipleItems(HashMap<String, HashMap<String, String>> dataSet) throws Exception {
		OMSHelper omsHelper = new OMSHelper();
		HashMap<String, String> orderMap = dataSet.get("orderData");
		HashMap<String, String> omsSessionData = dataSet.get("omsSessionData");
		HashMap<String, String> rmsSessionData = dataSet.get("rmsSessionData");
		HashMap<String, String> restaurantData = dataSet.get("restaurantData");
		if (RMSCommonHelper.isOrderAvailableAtFF(orderMap.get("orderId"))) {
			boolean flg = omsHelper.verifyOrder(orderMap.get("orderId"), "010", omsSessionData.get("X-CSRFToken"),
					omsSessionData.get("sessionId"));
			System.out.println("myAccessToken :" + rmsSessionData.get("accessToken"));
			String date = RMSCommonHelper.getCurrentDateTimeInRequiredFormat(new Date(), "yyyy-MM-dd'T'HH:mm:ss");
			System.out.println("------------------->" + date);
			Thread.sleep(60000);
			String a = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			JsonNode nodeObject = APIUtils.convertStringtoJSON(a);
			Assert.assertEquals(nodeObject.get("statusCode").asInt(), 0);
			Assert.assertEquals(nodeObject.get("statusMessage").asText(), "Success");
			String Obj = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
					Long.parseLong(restaurantData.get("restaurantId")), nodeObject);
			Assert.assertNotNull("FetchOrder API doesnt have OrderCreated " + Obj);

		} else {
			Assert.fail("FF issue  .. Order Not Reached FF So This DownStream Dependency Failed For VMS");
		}
	}

	// //@Test(dataProvider = "verifyFetchOrderhavingMultipleItemsNOOSNOrderEdit",description = "Place order for multiple normal items->Create Order -> Login-> Fetch orders -> Mark order OOS(Out of Stock) -> Edit Order from FF ->Verify order status", dataProviderClass = VMSEndToEndOrderFlowsDP.class)
	public void verifyFetchOrderhavingMultipleItemsNOOSNOrderEdit(HashMap<String, HashMap<String, String>> dataSet)
			throws Exception {
		OMSHelper omsHelper = new OMSHelper();
		HashMap<String, String> orderMap = dataSet.get("orderData");
		HashMap<String, String> omsSessionData = dataSet.get("omsSessionData");
		HashMap<String, String> rmsSessionData = dataSet.get("rmsSessionData");
		HashMap<String, String> restaurantData = dataSet.get("restaurantData");
		if (RMSCommonHelper.isOrderAvailableAtFF(orderMap.get("orderId"))) {
			boolean flg = omsHelper.verifyOrder(orderMap.get("orderId"), "010", omsSessionData.get("X-CSRFToken"),
					omsSessionData.get("sessionId"));
			System.out.println("myAccessToken :" + rmsSessionData.get("accessToken"));
			String date = RMSCommonHelper.getCurrentDateTimeInRequiredFormat(new Date(), "yyyy-MM-dd'T'HH:mm:ss");
			System.out.println("------------------->" + date);
			Thread.sleep(60000);
			String a = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			JsonNode nodeObject = APIUtils.convertStringtoJSON(a);
			Assert.assertEquals(nodeObject.get("statusCode").asInt(), 0);
			Assert.assertEquals(nodeObject.get("statusMessage").asText(), "Success");
			String Obj = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
					Long.parseLong(restaurantData.get("restaurantId")), nodeObject);
			Assert.assertNotNull("FetchOrder API doesnt have OrderCreated " + Obj);

		} else {
			Assert.fail("FF issue  .. Order Not Reached FF So This DownStream Dependency Failed For VMS");
		}
	}

	// //@Test(dataProvider = "verifyFetchOrderhavingMultipleItemsNOOSNOrderEditNConfirmWithNormalItem", description = "Place order for multiple normal items->Create Order -> Login -> Fetch orders -> Confirm order -> Mark order OOS(Send normal item as alternative) -> Edit Order from FF -> Verify order status", dataProviderClass = VMSEndToEndOrderFlowsDP.class)
	public void verifyFetchOrderhavingMultipleItemsNOOSNOrderEditNConfirmWithNormalItem(
			HashMap<String, HashMap<String, String>> dataSet) throws Exception {
		OMSHelper omsHelper = new OMSHelper();
		HashMap<String, String> orderMap = dataSet.get("orderData");
		HashMap<String, String> omsSessionData = dataSet.get("omsSessionData");
		HashMap<String, String> rmsSessionData = dataSet.get("rmsSessionData");
		HashMap<String, String> restaurantData = dataSet.get("restaurantData");
		if (RMSCommonHelper.isOrderAvailableAtFF(orderMap.get("orderId"))) {
			boolean flg = omsHelper.verifyOrder(orderMap.get("orderId"), "010", omsSessionData.get("X-CSRFToken"),
					omsSessionData.get("sessionId"));
			System.out.println("myAccessToken :" + rmsSessionData.get("accessToken"));
			String date = RMSCommonHelper.getCurrentDateTimeInRequiredFormat(new Date(), "yyyy-MM-dd'T'HH:mm:ss");
			System.out.println("------------------->" + date);
			Thread.sleep(60000);
			String a = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			JsonNode nodeObject = APIUtils.convertStringtoJSON(a);
			Assert.assertEquals(nodeObject.get("statusCode").asInt(), 0);
			Assert.assertEquals(nodeObject.get("statusMessage").asText(), "Success");
			String Obj = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
					Long.parseLong(restaurantData.get("restaurantId")), nodeObject);
			Assert.assertNotNull("FetchOrder API doesnt have OrderCreated " + Obj);

		} else {
			Assert.fail("FF issue  .. Order Not Reached FF So This DownStream Dependency Failed For VMS");
		}
	}

	// //@Test(dataProvider = "verifyFetchOrderhavingMultipleItemsNOOSNOrderEditNConfirmAfterEdit", description = "Place order for multiple normal items->Create Order -> Login -> Fetch orders -> Confirm order -> Mark order OOS(Send normal item as alternative) -> Edit Order from FF -> Confirm order -> Verify order status", dataProviderClass = VMSEndToEndOrderFlowsDP.class)
	public void verifyFetchOrderhavingMultipleItemsNOOSNOrderEditNConfirmAfterEdit(
			HashMap<String, HashMap<String, String>> dataSet) throws Exception {
		OMSHelper omsHelper = new OMSHelper();
		HashMap<String, String> orderMap = dataSet.get("orderData");
		HashMap<String, String> omsSessionData = dataSet.get("omsSessionData");
		HashMap<String, String> rmsSessionData = dataSet.get("rmsSessionData");
		HashMap<String, String> restaurantData = dataSet.get("restaurantData");
		if (RMSCommonHelper.isOrderAvailableAtFF(orderMap.get("orderId"))) {
			boolean flg = omsHelper.verifyOrder(orderMap.get("orderId"), "010", omsSessionData.get("X-CSRFToken"),
					omsSessionData.get("sessionId"));
			System.out.println("myAccessToken :" + rmsSessionData.get("accessToken"));
			String date = RMSCommonHelper.getCurrentDateTimeInRequiredFormat(new Date(), "yyyy-MM-dd'T'HH:mm:ss");
			System.out.println("------------------->" + date);
			Thread.sleep(60000);
			String a = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			JsonNode nodeObject = APIUtils.convertStringtoJSON(a);
			Assert.assertEquals(nodeObject.get("statusCode").asInt(), 0);
			Assert.assertEquals(nodeObject.get("statusMessage").asText(), "Success");
			String Obj = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
					Long.parseLong(restaurantData.get("restaurantId")), nodeObject);
			Assert.assertNotNull("FetchOrder API doesnt have OrderCreated " + Obj);

		} else {
			Assert.fail("FF issue  .. Order Not Reached FF So This DownStream Dependency Failed For VMS");
		}
	}

	// //@Test(dataProvider = "verifyMultileItemsIncludingAddonNVariants", description = "Place order with multiple items including addons and variants->Create Order -> Login -> Fetch orders -> Verify order details", dataProviderClass = VMSEndToEndOrderFlowsDP.class)
	public void verifyMultileItemsIncludingAddonNVariants(HashMap<String, HashMap<String, String>> dataSet)
			throws Exception {
		OMSHelper omsHelper = new OMSHelper();
		HashMap<String, String> orderMap = dataSet.get("orderData");
		HashMap<String, String> omsSessionData = dataSet.get("omsSessionData");
		HashMap<String, String> rmsSessionData = dataSet.get("rmsSessionData");
		HashMap<String, String> restaurantData = dataSet.get("restaurantData");
		if (RMSCommonHelper.isOrderAvailableAtFF(orderMap.get("orderId"))) {
			boolean flg = omsHelper.verifyOrder(orderMap.get("orderId"), "010", omsSessionData.get("X-CSRFToken"),
					omsSessionData.get("sessionId"));
			System.out.println("myAccessToken :" + rmsSessionData.get("accessToken"));
			String date = RMSCommonHelper.getCurrentDateTimeInRequiredFormat(new Date(), "yyyy-MM-dd'T'HH:mm:ss");
			System.out.println("------------------->" + date);
			Thread.sleep(60000);
			String a = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			JsonNode nodeObject = APIUtils.convertStringtoJSON(a);
			Assert.assertEquals(nodeObject.get("statusCode").asInt(), 0);
			Assert.assertEquals(nodeObject.get("statusMessage").asText(), "Success");
			String Obj = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
					Long.parseLong(restaurantData.get("restaurantId")), nodeObject);
			Assert.assertNotNull("FetchOrder API doesnt have OrderCreated " + Obj);

		} else {
			Assert.fail("FF issue  .. Order Not Reached FF So This DownStream Dependency Failed For VMS");
		}
	}

	// //@Test(dataProvider ="verifyMultileItemsIncludingAddonNVariantsAfterOOSNEditOrder", description ="Place order with multiple items including addons and variants->Create Order-> Login -> Fetch orders -> Mark order OOS(Out of Stock) -> Edit Order fromFF -> Verify order status", dataProviderClass =VMSEndToEndOrderFlowsDP.class)
	public void verifyMultileItemsIncludingAddonNVariantsAfterOOSNEditOrder(
			HashMap<String, HashMap<String, String>> dataSet) throws Exception {
		OMSHelper omsHelper = new OMSHelper();
		HashMap<String, String> orderMap = dataSet.get("orderData");
		HashMap<String, String> omsSessionData = dataSet.get("omsSessionData");
		HashMap<String, String> rmsSessionData = dataSet.get("rmsSessionData");
		HashMap<String, String> restaurantData = dataSet.get("restaurantData");
		if (RMSCommonHelper.isOrderAvailableAtFF(orderMap.get("orderId"))) {
			boolean flg = omsHelper.verifyOrder(orderMap.get("orderId"), "010", omsSessionData.get("X-CSRFToken"),
					omsSessionData.get("sessionId"));
			System.out.println("myAccessToken :" + rmsSessionData.get("accessToken"));
			String date = RMSCommonHelper.getCurrentDateTimeInRequiredFormat(new Date(), "yyyy-MM-dd'T'HH:mm:ss");
			System.out.println("------------------->" + date);
			Thread.sleep(60000);
			String a = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			JsonNode nodeObject = APIUtils.convertStringtoJSON(a);
			Assert.assertEquals(nodeObject.get("statusCode").asInt(), 0);
			Assert.assertEquals(nodeObject.get("statusMessage").asText(), "Success");
			String Obj = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
					Long.parseLong(restaurantData.get("restaurantId")), nodeObject);
			Assert.assertNotNull("FetchOrder API doesnt have OrderCreated " + Obj);

		} else {
			Assert.fail("FF issue  .. Order Not Reached FF So This DownStream Dependency Failed For VMS");
		}
	}

	// //@Test(dataProvider ="verifyMultileItemsIncludingAddonNVariantsNConfirmAfterOOSNEditOrder",description = "Place order with multiple items including addons andvariants->Create Order -> Login -> Fetch orders -> Confirm order -> Mark order OOS(Send normal item , addon & variant items as alternatives) -> EditOrder from FF -> Verify order status", dataProviderClass =VMSEndToEndOrderFlowsDP.class)
	public void verifyMultileItemsIncludingAddonNVariantsNConfirmAfterOOSNEditOrder(
			HashMap<String, HashMap<String, String>> dataSet) throws Exception {
		OMSHelper omsHelper = new OMSHelper();
		HashMap<String, String> orderMap = dataSet.get("orderData");
		HashMap<String, String> omsSessionData = dataSet.get("omsSessionData");
		HashMap<String, String> rmsSessionData = dataSet.get("rmsSessionData");
		HashMap<String, String> restaurantData = dataSet.get("restaurantData");
		if (RMSCommonHelper.isOrderAvailableAtFF(orderMap.get("orderId"))) {
			boolean flg = omsHelper.verifyOrder(orderMap.get("orderId"), "010", omsSessionData.get("X-CSRFToken"),
					omsSessionData.get("sessionId"));
			System.out.println("myAccessToken :" + rmsSessionData.get("accessToken"));
			String date = RMSCommonHelper.getCurrentDateTimeInRequiredFormat(new Date(), "yyyy-MM-dd'T'HH:mm:ss");
			System.out.println("------------------->" + date);
			Thread.sleep(60000);
			String a = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			JsonNode nodeObject = APIUtils.convertStringtoJSON(a);
			Assert.assertEquals(nodeObject.get("statusCode").asInt(), 0);
			Assert.assertEquals(nodeObject.get("statusMessage").asText(), "Success");
			String Obj = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
					Long.parseLong(restaurantData.get("restaurantId")), nodeObject);
			Assert.assertNotNull("FetchOrder API doesnt have OrderCreated " + Obj);

		} else {
			Assert.fail("FF issue  .. Order Not Reached FF So This DownStream Dependency Failed For VMS");
		}
	}

	// //@Test(dataProvider = "verifyFetchOrderWithMutipleItemsOnOOSNEditNConfirm",description = "Place order with multiple items including addons andvariants->Create Order -> Login -> Fetch orders -> Confirm order -> Mark order OOS(Send normal item , addon & variant items as alternatives) -> Edit Order from FF -> Confirm order -> Verify order status", dataProviderClass = VMSEndToEndOrderFlowsDP.class)
	public void verifyFetchOrderWithMutipleItemsOnOOSNEditNConfirm(HashMap<String, HashMap<String, String>> dataSet)
			throws Exception {
		OMSHelper omsHelper = new OMSHelper();
		HashMap<String, String> orderMap = dataSet.get("orderData");
		HashMap<String, String> omsSessionData = dataSet.get("omsSessionData");
		HashMap<String, String> rmsSessionData = dataSet.get("rmsSessionData");
		HashMap<String, String> restaurantData = dataSet.get("restaurantData");
		if (RMSCommonHelper.isOrderAvailableAtFF(orderMap.get("orderId"))) {
			boolean flg = omsHelper.verifyOrder(orderMap.get("orderId"), "010", omsSessionData.get("X-CSRFToken"),
					omsSessionData.get("sessionId"));
			System.out.println("myAccessToken :" + rmsSessionData.get("accessToken"));
			String date = RMSCommonHelper.getCurrentDateTimeInRequiredFormat(new Date(), "yyyy-MM-dd'T'HH:mm:ss");
			System.out.println("------------------->" + date);
			Thread.sleep(60000);
			String a = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			JsonNode nodeObject = APIUtils.convertStringtoJSON(a);
			Assert.assertEquals(nodeObject.get("statusCode").asInt(), 0);
			Assert.assertEquals(nodeObject.get("statusMessage").asText(), "Success");
			String Obj = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
					Long.parseLong(restaurantData.get("restaurantId")), nodeObject);
			Assert.assertNotNull("FetchOrder API doesnt have OrderCreated " + Obj);

		} else {
			Assert.fail("FF issue  .. Order Not Reached FF So This DownStream Dependency Failed For VMS");
		}
	}

	// //@Test(dataProvider = "A", description = "", dataProviderClass =
	// VMSEndToEndOrderFlowsDP.class)
	public void A(HashMap<String, HashMap<String, String>> dataSet) throws Exception {
		OMSHelper omsHelper = new OMSHelper();
		HashMap<String, String> orderMap = dataSet.get("orderData");
		HashMap<String, String> omsSessionData = dataSet.get("omsSessionData");
		HashMap<String, String> rmsSessionData = dataSet.get("rmsSessionData");
		HashMap<String, String> restaurantData = dataSet.get("restaurantData");
		if (RMSCommonHelper.isOrderAvailableAtFF(orderMap.get("orderId"))) {
			boolean flg = omsHelper.verifyOrder(orderMap.get("orderId"), "010", omsSessionData.get("X-CSRFToken"),
					omsSessionData.get("sessionId"));
			System.out.println("myAccessToken :" + rmsSessionData.get("accessToken"));
			String date = RMSCommonHelper.getCurrentDateTimeInRequiredFormat(new Date(), "yyyy-MM-dd'T'HH:mm:ss");
			System.out.println("------------------->" + date);
			Thread.sleep(60000);
			String a = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			JsonNode nodeObject = APIUtils.convertStringtoJSON(a);
			Assert.assertEquals(nodeObject.get("statusCode").asInt(), 0);
			Assert.assertEquals(nodeObject.get("statusMessage").asText(), "Success");
			String Obj = RMSCommonHelper.getMeObjectOrderObjectFromList(orderMap.get("orderId"),
					Long.parseLong(restaurantData.get("restaurantId")), nodeObject);
			Assert.assertNotNull("FetchOrder API doesnt have OrderCreated " + Obj);

		} else {
			Assert.fail("FF issue  .. Order Not Reached FF So This DownStream Dependency Failed For VMS");
		}
	}


	
	ArrayList<Long> list = new ArrayList<>();
	@AfterClass
	public void a1() {
		for (Long l : list) {
			RngHelper.disableEnableTD(String.valueOf(l), "false");
		}
	}

}
