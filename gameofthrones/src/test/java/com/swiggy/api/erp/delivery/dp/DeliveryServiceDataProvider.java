package com.swiggy.api.erp.delivery.dp;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;


import org.json.JSONException;
import org.json.JSONObject;
import org.testng.annotations.DataProvider;

import com.google.gdata.data.dublincore.Date;
import com.swiggy.api.erp.delivery.constants.DeliveryConstant;

import com.swiggy.api.erp.delivery.helper.DeliveryDataHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryHelperMethods;
import com.swiggy.api.erp.ff.helper.LOSHelper;
import com.swiggy.api.erp.ff.helper.OMSHelper;

import com.swiggy.api.erp.delivery.helper.AutoassignHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryHelperMethods;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceHelper;

import com.swiggy.api.sf.checkout.helper.CheckoutHelper;

import framework.gameofthrones.JonSnow.Processor;

public class DeliveryServiceDataProvider {

	static String order_id;

	static String order_id1;
	static String order_id2;
	static String order_id3;
	  static LOSHelper loshelper = new LOSHelper();
	DeliveryDataHelper deliveryDataHelper= new DeliveryDataHelper();
	DeliveryHelperMethods del = new DeliveryHelperMethods();
   
	OMSHelper omhelper =new OMSHelper();
	
	@DataProvider(name = "delogin")
	public Object[][] delogin() throws Exception {

		return new Object[][] { {"216",0 },{" ",500},{"1",500},{"ghhhfd",500}
			};
	}

	
	
	
	//System.out.println("order is"+order_id);

	AutoassignHelper autoassignHelper= new AutoassignHelper();
	DeliveryHelperMethods delmeth=new DeliveryHelperMethods();
	DeliveryServiceHelper helpdel=new DeliveryServiceHelper();


	
	@DataProvider(name = "reassign")
	public static Object[][] reassign() throws Exception {

		 order_id1 = loshelper.getAnOrder("pop");

		return new Object[][] {

		{ order_id1, "1", "1", 0 }, { " ", "1", "1", 500 },
				{ order_id1, " ", "1", 500 }, { order_id1, "1", " ", 500 },
				{ " ", " ", " ", 500 }, { "12234253453", "1", "1", 500 } };
	}

	@DataProvider(name = "unassign")

	public static Object[][] unassign() throws Exception {

		order_id1 = loshelper.getAnOrder("pop");

		return new Object[][] {
			

		{ order_id1, "1026", 0 }, { " ", "1026", 1 }, { order_id1, " ", 1 },
				{ order_id1, "812328193", 500 }, { " ", " ", 1 },
				{ "1253453", "1026", 500 } };
	}

	@DataProvider(name = "orderassignn")
	public Object[][] orderrassign() throws Exception {
		

		 order_id1 = loshelper.getAnOrder("pop");
		return new Object[][] {

		{ order_id1, "216", 0 }, { order_id1, "216", 0 }, { " ", "216", 6 },
				{ order_id1, " ", 6 }, { " ", " ", 6 }, { "1253453", "216", 5 } };
	}

	@DataProvider(name = "defree")
	public Object[][] freede() throws Exception {

		return new Object[][] { { "216",200 },{" ",500},{"64765473",500},{"gfhgfd",500}};
	}

	@DataProvider(name = "debusy")
	public Object[][] debusy() throws Exception {

		return new Object[][] { { "216",200 },{" ",500},{"64765473",500},{"gfhgfd",500}};
	}
	public static String createOrder() throws Exception {
		CheckoutHelper helperc = new CheckoutHelper();
		Object[][] data = cartItems();
		Processor p = helperc.placeOrder(DeliveryConstant.mobile,
				DeliveryConstant.password, data[0][0].toString(),
				data[0][1].toString(), data[0][2].toString());
		return order_id = p.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.order_id");
	}

	public static Object[][] cartItems() {

		return new Object[][] { { "3369002", "1", "18410" } };
	}

	@DataProvider
	public static Object[][] OrderCollectData() {
		return new Object[][] { { "12", "4", "100123139", "2016-12-21 06:21:58" } };
	}

	@DataProvider
	public static Object[][] AddArea_MappedDEData() {
		return new Object[][] { { "59902", "12" } };
	}

	@DataProvider(name = "updateDeWeightParam")
    public Object[][] Updateparam() throws Exception {
		   return new Object[][] {
                {"1","1","1","1000","mayur"},
                {"1","1","1","1001","mayur"},
              
        };
    }
	
	@DataProvider(name="orderpay")
	public static Object[][] Orderpay() throws Exception  {
		order_id1 = loshelper.getAnOrder("pop");
		return new Object[][] { 
			
			{order_id1, "12", "500","Success"},
			{"", "10", "500","No trips found with orderId: "},
			{"56454","23","500","No trips found with orderId: 56454"}
			
		
		};
	}

		
		@DataProvider(name = "merge")
	    public Object[][] merge() throws Exception {
			order_id1 = loshelper.getAnOrder("pop");

			order_id2 = loshelper.getAnOrder("pop");

			Thread.sleep(1000);
			return new Object[][] {
	                {order_id1,order_id2,0},
	             };

		}
		
		@DataProvider(name = "nextorder")
		public Object[][] nextorder() throws Exception {

			order_id1 = loshelper.getAnOrder("pop");

			return new Object[][] {
            { order_id1, "216", 0 } };
		}
		
		
		@DataProvider(name = "location")
		public Object[][] location() throws Exception {
			String discardedLocation=deliveryDataHelper.generateDiscardedLocation( 12.8998906,77.2144877,20);
			//[{\"lat\":28.6704681,\"lng\":77.4617869,\"accuracy\":6.0,\"timestamp\":1520419494555,\"batch_id\":\"\"}"
			return new Object[][] { { "12.8998906","77.2144877","1.009",discardedLocation,0,DeliveryConstant.snded_id,DeliveryConstant.delivery_app_version}, 
				{ "","77.2144877","1.009",discardedLocation,0,DeliveryConstant.snded_id,DeliveryConstant.delivery_app_version}};
		
		}
		
		
		@DataProvider(name = "floatingcash")
		public Object[][] floatingcash() throws Exception {
			return new Object[][] {
            { DeliveryConstant.snded_id, DeliveryConstant.delivery_app_version, 0 } };
		}
		
		
		@DataProvider(name = "dynamicCode")
		public Object[][] floatinfcashdynamic() throws Exception {
			return new Object[][] {
            { DeliveryConstant.snded_id, DeliveryConstant.delivery_app_version, 0,"1" },
            { DeliveryConstant.snded_id, DeliveryConstant.delivery_app_version, 0,"2" },
            { DeliveryConstant.snded_id, DeliveryConstant.delivery_app_version, 0,"3"},
            { DeliveryConstant.snded_id, DeliveryConstant.delivery_app_version, 0,"4"},
            { DeliveryConstant.snded_id, DeliveryConstant.delivery_app_version, 1,"0"}
            
			};
            
		}
		
		@DataProvider(name = "blocking")
		public Object[][] blocking() throws Exception {
			return new Object[][] {
            {"1026",200},
            {"00",200},
            {"",1}};
		}
		
		
		@DataProvider(name = "bankdetails")
		public Object[][] bankdetails() throws Exception {
			return new Object[][] {
            { DeliveryConstant.snded_id, DeliveryConstant.delivery_app_version, 0 } };
		}
		

		@DataProvider(name = "startduty")
		public Object[][] startdyty() throws Exception {
			return new Object[][] {
            { DeliveryConstant.snded_id, DeliveryConstant.delivery_app_version, 0 ,"start duty successful"} };
		}
		

		@DataProvider(name = "stopduty")
		public Object[][] stopDuty() throws Exception {
			return new Object[][] {
            { DeliveryConstant.snded_id, DeliveryConstant.delivery_app_version, 0 ,"stop duty successful"} };
		}
		
		@DataProvider(name = "deliveryboyfloatingcash")
		public Object[][] getDefloatingcash() throws Exception {
			return new Object[][] {
            { DeliveryConstant.snded_id, DeliveryConstant.delivery_app_version, 0,"Success" } };
		}
		
		@DataProvider(name = "verifydeisfree")
		public Object[][] checkDeisFree() throws Exception {
			return new Object[][] {
            { DeliveryConstant.snded_id, DeliveryConstant.delivery_app_version, 2,"DE is free" } };
		}
		
		@DataProvider(name = "systemreject")
		public Object[][] systemReject() throws Exception {

			order_id1 = loshelper.getAnOrder("pop");
			return new Object[][] {
            { DeliveryConstant.snded_id,order_id1, 0, "Success" } };
		}

		
		@DataProvider(name = "getdelocation")
		public Object[][] getdeliveryboylocation() throws Exception {
			return new Object[][] {
            { DeliveryConstant.snded_id,  0 }
            
			};
		}
            
		
		
		@DataProvider(name = "updatedezone")
		public Object[][] updatedezone() throws Exception {
			
        String query = "Select id  from de_zone_map where de_id=" + DeliveryConstant.test_deid + ";";
        Object obj = del.dbhelperget(query, "id");
         String zonemapid=  obj.toString();
         
         String query1 = "Select zone_id  from de_zone_map where de_id=" + DeliveryConstant.test_deid + ";";
         Object obj1 = del.dbhelperget(query1, "zone_id");
          String zoneid=  obj1.toString();
			return new Object[][] {
            { DeliveryConstant.test_deid,zonemapid,zoneid,200,"Success",zonemapid}
            
			};
		}
            
		
		@DataProvider(name = "deletzone")
		public Object[][] deletzone() throws Exception {
	  String query = "Select id from de_zone_map" + ";";
        Object obj = del.dbhelperget(query, "id");
         String zonemapid=  obj.toString();
			return new Object[][] {
            {zonemapid,200,"Success"}
            
			};
		}
         
		
		
		@DataProvider(name = "orderhistory")
		public Object[][] orderhistory() throws Exception {
			java.util.Date date = new java.util.Date(System.currentTimeMillis());
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			String currentdate = df.format(date);
			System.out.println(currentdate);
			order_id1 = loshelper.getAnOrder("pop");
		
			return new Object[][] {
            { DeliveryConstant.test_deid, DeliveryConstant.delivery_app_version, 0,"Success", currentdate ,order_id1} };
            
		}
		
		@DataProvider(name = "loginhistory")
		public Object[][] loginhistory() throws Exception {
			java.util.Date date = new java.util.Date(System.currentTimeMillis());
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			String currentdate = df.format(date);
			System.out.println(currentdate);
			
			Calendar today = Calendar.getInstance();
			Calendar yesterday = Calendar.getInstance();
			yesterday.add(Calendar.DATE, -1);
			java.util.Date d = yesterday.getTime(); 
			String yesDate = df.format(d);
			
			System.out.println(yesDate);
			
			
			return new Object[][] {
            { DeliveryConstant.test_deid, DeliveryConstant.delivery_app_version, 0,"Success", yesDate,currentdate},
            
            {DeliveryConstant.test_deid, DeliveryConstant.delivery_app_version,1,"Invalid login history input dates - Start Date : "+currentdate+", "+"is after End Date : "+yesDate,currentdate ,yesDate}
			 };
		}
		
		@DataProvider(name = "enablebatchforresturant")
		public Object[][] enablebatchforresturant() throws Exception {
	  String query = "Select id from restaurant where name like 'Meghana Foods' " + ";";
        Object obj = del.dbhelperget(query, "id");
         String rest1=  obj.toString();
         
         String query1 = "Select id from restaurant where name like 'Chai Point' " + ";";
         Object obj1 = del.dbhelperget(query1, "id");
          String rest2=  obj1.toString();
			return new Object[][] {
            {rest1,rest2,"1",200,"Success"},
            {rest1,rest2,"",200,"Invalid Parameters"},
            {rest1,rest2,"0",200,"Success"}
            
          
            
			};
		}
		
		
		@DataProvider(name = "pointdetailpage")
		public Object[][] pointdetailpage() throws Exception {
			
			return new Object[][] {
            { DeliveryConstant.test_deid, DeliveryConstant.delivery_app_version, 0,"Successfully redirecting to  reward point details", } };
            
		}
		
		@DataProvider(name = "redeempoints")
		public Object[][] redeemPoints() throws Exception {
			
			return new Object[][] {
            { DeliveryConstant.test_deid, DeliveryConstant.delivery_app_version, 0,"Successfully redirecting to redeem points" } };
            
		}
		@DataProvider(name = "getTotalpoints")
		public Object[][] getTotalpoints() throws Exception {
			
			return new Object[][] {
            { DeliveryConstant.test_deid, DeliveryConstant.delivery_app_version, 0,"Total points success" } };
            
		}
		
		
		@DataProvider(name = "getvoucher")
		public Object[][] getvoucher() throws Exception {
			
			return new Object[][] {
            { DeliveryConstant.test_deid, DeliveryConstant.delivery_app_version} };
            
		}
		
		@DataProvider(name = "getRewardConfig")
		public Object[][] getgif() throws Exception {
			
			return new Object[][] {
            { DeliveryConstant.test_deid, DeliveryConstant.delivery_app_version,0,"Loyalty Configs successfully fetched"} };
            
		}
		
		
		@DataProvider(name = "servicestatusupdate")
		public Object[][] Servicestatusupdate() throws Exception {
			LOSHelper loshelper = new LOSHelper();
			
			String order_id = loshelper.getAnOrder("pop");
			omhelper.verifyOrder(order_id, "111");
			System.out.println("order is  "+order_id);
			Thread.sleep(30000);
			String query = "Select bill from trips where order_id=" + order_id + ";";
			Thread.sleep(3000);
			String query1 = "Select pay from trips where order_id=" + order_id + ";";
			Thread.sleep(3000);
			String query2 = "Select collect from trips where order_id=" + order_id + ";";
            
			Object obj1 = del.dbhelperget(query, "bill");
			Object obj2=del.dbhelperget(query1, "pay");
			Object obj3=del.dbhelperget(query2, "collect");
			Thread.sleep(1000);
             String bill = obj1.toString();
			String pay=obj2.toString();
			String collect=obj3.toString();
			return new Object[][] { 
			{ order_id, "confirm",bill,pay,collect,DeliveryConstant.snded_id,DeliveryConstant.delivery_app_version,0} ,
		    { order_id, "Arrived",bill,pay,collect,DeliveryConstant.snded_id,DeliveryConstant.delivery_app_version,0},
			{ order_id, "Pickedup",bill,pay,collect,DeliveryConstant.snded_id,DeliveryConstant.delivery_app_version,0},
			{ order_id, "confirm",bill,pay,collect,DeliveryConstant.snded_id,DeliveryConstant.delivery_app_version,0},
			{ order_id, "confirm",bill,pay,collect,DeliveryConstant.snded_id,DeliveryConstant.delivery_app_version,0},
			{ order_id, "confirm",bill,pay,collect,DeliveryConstant.snded_id,DeliveryConstant.delivery_app_version,0}};
		}
		
		
		@DataProvider(name = "makeActiveFree")
		public Object[][] makeActiveFree() throws Exception {
			
			return new Object[][] {
            { "87617" },{"87618"},{"87610"},{"87619"},{"87620"},{"67615"},{"87616"},{"87614"},{"87613"},{"87612"},{"87611"}};
            
            
		}
		
		public Object createorderjson(String restaurant_id,String restaurant_lat_lng,String restaurant_area_code) throws JSONException, InterruptedException
	    {
	        Map<String, String> defMap= new HashMap<>();
	        defMap.put("restaurant_id",restaurant_id);
	        defMap.put("restaurant_lat_lng",restaurant_lat_lng);
	       // defMap.put("restaurant_customer_distance", restaurant_customer_distance);
	       // defMap.put("is_long_distance", is_long_distance);
	        defMap.put("restaurant_area_code", restaurant_area_code);
	        String order_time=deliveryDataHelper.getcurrentDateTimefororderTime();
	        defMap.put("order_time",order_time);
	        
	        String orderJson=deliveryDataHelper.returnOrderJson(defMap);
	        boolean order_status=deliveryDataHelper.pushOrderJsonToRMQAndValidate(orderJson);
	        Object order_id=null;
	        System.out.println(orderJson);
	        if(order_status)
	        {
	            order_id=(new JSONObject(orderJson)).get("order_id").toString();
	        }
	        return order_id;
	    }
		
		
		@DataProvider(name = "mergeandassign")
	    public Object[][] mergeandassign() throws Exception {
			order_id1 = loshelper.getAnOrder("pop");

			order_id2 = loshelper.getAnOrder("pop");
			order_id3 = loshelper.getAnOrder("pop");
			Thread.sleep(1000);
			return new Object[][] {
	                {order_id1,order_id2,order_id3,DeliveryConstant.snded_id },
	             };

		}
		
		
		
		
	
		@DataProvider(name = "createorders")
		public Object[][] createorders() throws Exception {
			
			return new Object[][] {
				{ "954","12.934292,77.61275599999999", "1"}
				
				
			};
            
		}

//***********************Food Issues Data Provider*****************************
	@DataProvider(name = "FoodIssues")
	public  Object[][] billCheck() throws Exception {
		String finalauth=loginDE();
       // String orderId=createOrder();
        String billProvided="true";
        String inputValue = "131";
        String inputType = "\"BILL\"";
        String verifyWithVendor="true";
		return new Object[][] {	
		{"\"7742220538\"", finalauth,inputValue,inputType, billProvided,verifyWithVendor},
		};
	}
	
	@DataProvider(name = "FoodIssuesAck")
	public  Object[][] FoodIssueAck() throws Exception {
		String finalauth=loginDE();
        String orderId=createOrder();
        String billProvided="true";
		return new Object[][] {	
		{"\"7742220538\"", "Basic R0dZU1dJOjIwMTVTVyFHR1k=", billProvided},
		};
	}
	
	public String loginDE() throws JSONException
	{
		String loginresponse=autoassignHelper.delogin(DeliveryConstant.snded_id, DeliveryConstant.delivery_app_version).ResponseValidator.GetBodyAsText();
		System.out.println("login Response " +loginresponse);
		String otp=(new JSONObject(helpdel.getOtp(DeliveryConstant.snded_id).ResponseValidator.GetBodyAsText())).get("data").toString();
		System.out.println("OTP IS "+otp);
		JSONObject auth=(JSONObject)(new JSONObject(autoassignHelper.deotp(DeliveryConstant.snded_id,otp).ResponseValidator.GetBodyAsText()).get("data"));
		String finalauth=auth.get("Authorization").toString();
		System.out.println("Auth is "+finalauth);
		return finalauth;
	}


	@DataProvider(name = "cancelorder")
	public Object[][] cancelorder() throws Exception {
		
		 order_id1 = loshelper.getAnOrder("pop");
		return new Object[][] {
			{DeliveryConstant.snded_id, DeliveryConstant.delivery_app_version,order_id1,200,"Success"},
			{DeliveryConstant.snded_id, DeliveryConstant.delivery_app_version,"7241849",500,"No trips found with orderId: 7241849"},
			{DeliveryConstant.snded_id, DeliveryConstant.delivery_app_version, "\"NULL\"",200,"Invalid Parameters"}};
			
	}

	
	
	@DataProvider(name = "incoming")
	public Object[][] incoming() throws Exception {
		
		 order_id1 = loshelper.getAnOrder("pop");
		 omhelper.verifyOrder(order_id1, "111");
			System.out.println("order is  "+order_id1);
			Thread.sleep(3000);
//			String query = "Select bill from trips where order_id=" + order_id1 + ";";
//			Thread.sleep(3000);
//			String query1 = "Select pay from trips where order_id=" + order_id1+ ";";
//			Thread.sleep(3000);
			String query2 = "Select collect from trips where order_id=" + order_id1 + ";";
         
			//Object obj1 = del.dbhelperget(query, "bill");
			//Object obj2=del.dbhelperget(query1, "pay");
			Object obj3=del.dbhelperget(query2, "collect");
			Thread.sleep(1000);
            //String bill = obj1.toString();
			//String pay=obj2.toString();
			String collect=obj3.toString();
		     return new Object[][] {
			
			//{DeliveryConstant.snded_id,bill,pay,collect,order_id1,"12.932530:77.602380", 0,"Success"},
			
			{DeliveryConstant.snded_id,order_id1,"49", 0,"Success"}};
	
			
	}
}
