package com.swiggy.api.erp.delivery.tests;

import framework.gameofthrones.Daenerys.Initializer;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.swiggy.api.erp.delivery.dp.DeliveryCerebroDataProvider;
import com.swiggy.api.erp.delivery.helper.DeliveryHelperMethods;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;

public class DeliveryCerebroRegressionTest extends DeliveryCerebroDataProvider {

	//Initialize gameofthrones = new Initialize();
	Initialize gameofthrones =Initializer.getInitializer();
	DeliveryHelperMethods delmeth=new DeliveryHelperMethods();

	@Test(groups = { "regression" }, priority = 0, dataProvider = "ListingData")
	public void listingTest(String lat, String lng) {
		System.out
				.println("******************************ListingTest is started******************************");
		GameOfThronesService service = new GameOfThronesService(
				"deliverycerebro", "Listing", gameofthrones);

		
		Processor processor = new Processor(service,
				delmeth.doubleheader(), new String[] { lat, lng });
		String serviceableRestaurants = processor.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.serviceableRestaurants");
		Assert.assertNotEquals("[]", serviceableRestaurants);
		System.out
				.println("******************************ListingTest is stopped******************************");
	}

}
