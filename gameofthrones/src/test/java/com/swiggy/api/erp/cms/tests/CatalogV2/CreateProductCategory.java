package com.swiggy.api.erp.cms.tests.CatalogV2;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.helper.CatalogV2Helper;
import framework.gameofthrones.Cersei.ToolBox;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class CreateProductCategory {
    SchemaValidatorUtils schemaValidatorUtils = new SchemaValidatorUtils();
    CatalogV2Helper catalogV2Helper=new CatalogV2Helper();
    String catId;
    @Test(description = "Creates product category with rules and with allowed values")
    public void createProductCategory() throws Exception{
        String jsonSchema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/Schemaset/Json/CMS/createproductcategory.txt");
        String response=catalogV2Helper.createCategoryPojo().ResponseValidator.GetBodyAsText();
        System.out.println(response);
        List<String> missingAddedNodeList = schemaValidatorUtils.validateServiceSchema(jsonSchema,response);
        Assert.assertTrue(missingAddedNodeList.isEmpty(),missingAddedNodeList + "Nodes are missing or data type of the object is mismatching");
        catId=JsonPath.read(response,"$.id");
        Assert.assertNotNull(catId);
    }
}
