package com.swiggy.api.erp.delivery.tests;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.erp.delivery.dp.IncentiveDataProvider;
import com.swiggy.api.erp.delivery.helper.AutoassignHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryDataHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryHelperMethods;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceHelper;
import com.swiggy.api.erp.delivery.helper.IncentiveHelper;
import com.swiggy.api.erp.delivery.helper.ServiceablilityHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.snd.helper.SnDHelper;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;


public class IncentiveRegressionTest extends IncentiveDataProvider {
	ServiceablilityHelper serviceablilityHelper = new ServiceablilityHelper();
	Initialize gameofthrones = new Initialize();
	DeliveryHelperMethods delmeth=new DeliveryHelperMethods();
	DeliveryServiceHelper helpdel = new DeliveryServiceHelper();
	SnDHelper sndhelp=new SnDHelper();
	CheckoutHelper chelp=new CheckoutHelper();
	ServiceablilityHelper sHelper=new ServiceablilityHelper();
	AutoassignHelper hepauto = new AutoassignHelper();
    IncentiveHelper payhelp=new IncentiveHelper();
	DeliveryDataHelper DH=new DeliveryDataHelper();
	

	public JsonArray getIncentiveData(String order_id)
	{
	String query=DeliveryConstant.incentiveData+order_id+";";
	String result=delmeth.dbhelperget(query, "incentive_data").toString();
	Gson gson = new Gson();
	JsonObject jsonObject = gson.fromJson(result, JsonObject.class);
	JsonArray ruleName = (JsonArray) jsonObject.get("rules_earned");
     System.out.println(ruleName.toString());
     return ruleName;
	}
	
	public  void  ruleAndBonusCheck(String RuleName, double Bonus, String order_id)
	{
		double  value = 0;
		JsonArray ruleName=getIncentiveData(order_id);
		JsonArray ruleArray = ruleName.getAsJsonArray();
		for (int i = 0; i< ruleArray.size() ; i++) {
			JsonElement obj = ruleArray.get(i);
			String rule = obj.getAsJsonObject().get("RuleName").getAsString();
			if(!rule.equalsIgnoreCase(RuleName))
			continue;
			else{
		         System.out.println(rule);
				  value = obj.getAsJsonObject().get("Bonus").getAsDouble();
				 if(value==Bonus)
				 {Assert.assertTrue(true);
				 }	
				 else
				 {	Assert.assertTrue(false);
				 }
				 break;
			}
		}
	}
	

	
//1.Reject_More_Than_1_Less_Than_5
	@Test(dataProvider="payoutData", enabled=false, description="Verify the rules for the rejected order which has been rejected more than 5 times")
	public void rejectedMoreThanOneLessThanFive(String de_id, String rest_id, String lat, String lan) throws InterruptedException, IOException  {
			String RuleName="Reject_More_Than_1_Less_Than_5";
		//   SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(DeliveryConstant.RuleEnable+RuleName);
			double Bonus=1;
		    String order_id=DH.getOrderId(rest_id, lat, lan);
            payhelp.deReject(de_id, order_id);
            payhelp.deReject(de_id, order_id);
	     //	payhelp.caprD(de_id,order_id);
	     	ruleAndBonusCheck(RuleName,Bonus,order_id);
		 //   SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(DeliveryConstant.RuleDisable+RuleName);

	}
	
//2: Reject_More_Than_2
	@Test(dataProvider="payoutData",  enabled=false, description="Verify the rules for the order which got rejected more than twice")
	public void rejectedMoreThanTwice(String de_id, String rest_id, String lat, String lan) throws InterruptedException, IOException  {
			String RuleName="Reject_More_Than_2";
		   // SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(DeliveryConstant.RuleEnable+RuleName);
			double Bonus=2;
		    String order_id=DH.getOrderId(rest_id, lat, lan);
            payhelp.deReject(de_id, order_id);
            payhelp.deReject(de_id, order_id);
            payhelp.deReject(de_id, order_id);
	     	payhelp.caprD("1026",order_id);
	     	ruleAndBonusCheck(RuleName,Bonus,order_id);
		//   SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(DeliveryConstant.RuleDisable+RuleName);

	}
	
	//3: Rain_Mode_Non_Batched:PASS
	@Test(dataProvider="payoutData", description="Verify the rain mode rule for non batched order")
	public void rainModeNonBatched(String de_id, String rest_id, String lat, String lan) throws InterruptedException, IOException  {
			String RuleName="Rain_Mode_Non_Batched";
		 //   SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).update(DeliveryConstant.RuleEnable+RuleName);
			double Bonus=3;
			String zone_id="1";
		    SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(DeliveryConstant.enableHeavyRainMode+zone_id);
		  // Thread.sleep(5000);
		    String order_id=DH.getOrderId(rest_id, lat, lan);
	     	payhelp.caprD(de_id,order_id);
	     	ruleAndBonusCheck(RuleName,Bonus,order_id);
	     SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(DeliveryConstant.disableHeavyRainMode+zone_id);
		 //   SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).update(DeliveryConstant.RuleDisable+RuleName);

	}
	//4: Rain_Mode_Batched_Single_Restaurant:pass
	@Test(dataProvider="payoutData", description="Verify the rain mode batched rule for single restaurant")
	public void rainModeBatchedSingleRestaurant(String de_id, String rest_id, String lat, String lan) throws InterruptedException, IOException  {
		String RuleName="Rain_Mode_Batched_Single_Restaurant";
	 //   SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).update(DeliveryConstant.RuleEnable+RuleName);
		double Bonus=4;
		String zone_id="1";
	    SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(DeliveryConstant.enableHeavyRainMode+zone_id);
		    String order_id1=DH.getOrderId(rest_id, lat, lan);
		    String order_id2=DH.getOrderId(rest_id, lat, lan);
		    SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(DeliveryConstant.enableHeavyRainMode+zone_id);
			helpdel.mergeorder(order_id1, order_id2);

		    payhelp.caprD(de_id,order_id1);
	     	ruleAndBonusCheck(RuleName,Bonus,order_id1);
			SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(DeliveryConstant.disableHeavyRainMode+zone_id);
			    
	}
	
	//5 RainModeBatchedMulti_Restaurant
	@Test(dataProvider="payoutData", description="Verify the first mile rule is getting applied and correct bonus is getting displayed")
	public void RainModeBatchedMulti_Restaurant(String de_id, String rest_id, String lat, String lan) throws InterruptedException, IOException  {
		//enable rain mode
		String RuleName="'Rain_Mode_Batched_Multi_Restaurant'";
	 //  SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).update(DeliveryConstant.RuleEnable+RuleName);
		double Bonus=5;
		String rest_id2="221";
	    String zone_id="1";
	    SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(DeliveryConstant.enableHeavyRainMode+zone_id);
		    String order_id1=DH.getOrderId(rest_id, lat, lan);
		    String order_id2=DH.getOrderId(rest_id2, lat, lan);
			helpdel.mergeorder(order_id1, order_id2);
			payhelp.caprD(de_id,order_id1);// assign batch
	     	ruleAndBonusCheck(RuleName,Bonus,order_id1);
		 //   SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).update(DeliveryConstant.RuleDisable+RuleName);

	}
	
	//10  Per_Order:Pass
		@Test(dataProvider="payoutData", description="Verify Per order rule")
		public void perOrder(String de_id, String rest_id, String lat, String lan) throws InterruptedException, IOException  {
			String RuleName="Per_Order";
		 //   SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).update(DeliveryConstant.RuleEnable+RuleName);
			double Bonus=10;
			    String order_id=DH.getOrderId(rest_id, lat, lan);
			    payhelp.caprD(de_id,order_id);
		     	ruleAndBonusCheck(RuleName,Bonus,order_id);
		
		//	    SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).update(DeliveryConstant.RuleDisable+RuleName);

		}
	
		//11  secondBatchedOrder:PASS
				@Test(dataProvider="payoutData", description="Verify Second_Batched_Order rule")
				public void secondBatchedOrder(String de_id, String rest_id, String lat, String lan) throws InterruptedException, IOException  {
					String RuleName="Second_Batched_Order";
				   // SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).update(DeliveryConstant.RuleEnable+RuleName);
					double Bonus=11;
					    String order_id1=DH.getOrderId(rest_id, lat, lan);
					    String order_id2=DH.getOrderId(rest_id, lat, lan);
						helpdel.mergeorder(order_id1, order_id2);
				     	payhelp.caprD(de_id,order_id2);
				     	ruleAndBonusCheck(RuleName,Bonus,order_id2);
					//    SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).update(DeliveryConstant.RuleDisable+RuleName);

				}
		
		//Rejection_Penalty
				@Test(dataProvider="payoutData",  enabled=false, description="Verify Per order rule")
				public void rejectionPenalty(String de_id, String rest_id, String lat, String lan) throws InterruptedException, IOException  {
					String RuleName="Rejection_Penalty";
				//    SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).update(DeliveryConstant.RuleEnable+RuleName);
					double Bonus=-14;
					    String order_id=DH.getOrderId(rest_id, lat, lan);
				     	payhelp.deReject(de_id,order_id);
				     //	payhelp.caprD(de_id,order_id);
				     	ruleAndBonusCheck(RuleName,Bonus,order_id);
				//	    SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).update(DeliveryConstant.RuleDisable+RuleName);

				}	
				
			//Rest_Wait_Time_0_5
				
				@Test(dataProvider="payoutData", description="Verify Rest_Wait_Time_0_5 rule")
				public void rest_Wait_Time(String de_id, String rest_id, String lat, String lan) throws InterruptedException, IOException  {
					String RuleName="Rest_Wait_Time_0_5";
				   // SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).update(DeliveryConstant.RuleEnable+RuleName);

					double Bonus=15;
					    String order_id=DH.getOrderId(rest_id, lat, lan);
				     	payhelp.caprD(de_id,order_id);
				     	ruleAndBonusCheck(RuleName,Bonus,order_id);
					  //  SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).update(DeliveryConstant.RuleDisable+RuleName);

				}	
				//Rest_Wait_Time_5_15
				@Test(dataProvider="payoutData", description="Verify Rest_Wait_Time_5_15 rule")
				public void Rest_Wait_Time_5_15(String de_id, String rest_id, String lat, String lan) throws InterruptedException, IOException  {
					String RuleName="Rest_Wait_Time_5_15";
				 //   SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).update(DeliveryConstant.RuleEnable+RuleName);
					double Bonus=16;
					    String order_id=DH.getOrderId(rest_id, lat, lan);
				     	payhelp.caprD(de_id,order_id);
				     	ruleAndBonusCheck(RuleName,Bonus,order_id);
				//	    SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).update(DeliveryConstant.RuleDisable+RuleName);

				}	
				
				//Rest_Wait_Time_Above_15
				@Test(dataProvider="payoutData", description="Verify Rest_Wait_Time_Above_15 rule")
				public void Rest_Wait_Time_Above_15(String de_id, String rest_id, String lat, String lan) throws InterruptedException, IOException  {
					String RuleName="Rest_Wait_Time_Above_15";
				 //   SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).update(DeliveryConstant.RuleEnable+RuleName);

					double Bonus=17;
					    String order_id=DH.getOrderId(rest_id, lat, lan);
				     	payhelp.caprD(de_id,order_id);
				     	ruleAndBonusCheck(RuleName,Bonus,order_id);
					//    SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).update(DeliveryConstant.RuleDisable+RuleName);

				}	
				
				//Last_Mile_Till_4KM
				@Test(dataProvider="LastMile", description="Verify Last_Mile_Till_4KM rule")
				public void Last_Mile_Till_4KM(String de_id, String rest_id, String lat, String lan) throws InterruptedException, IOException  {
					String RuleName="Last_Mile_Till_4KM";
				    //SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).update(DeliveryConstant.RuleEnable+RuleName);

					double Bonus=18;
					    String order_id=DH.getOrderId(rest_id, lat, lan);
				     	payhelp.caprD(de_id,order_id);
				     	ruleAndBonusCheck(RuleName,Bonus,order_id);
					//    SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).update(DeliveryConstant.RuleDisable+RuleName);


				}
				
			//Last_Mile_More_Than_4KM	
				@Test(dataProvider="payoutData", description="Verify Last_Mile_More_Than_4KM rule")
				public void Last_Mile_More_Than_4KM(String de_id, String rest_id, String lat, String lan) throws InterruptedException, IOException  {
					String RuleName="Last_Mile_More_Than_4KM";
				 //   SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).update(DeliveryConstant.RuleEnable+RuleName);

					double Bonus=19;
					    String order_id=DH.getOrderId(rest_id, lat, lan);
				     	payhelp.caprD(de_id,order_id);
				     	ruleAndBonusCheck(RuleName,Bonus,order_id);
					//    SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).update(DeliveryConstant.RuleDisable+RuleName);


				}	
				
				//Pay_First_Mile_Canceled_Order	
				@Test(dataProvider="payoutData", description="Verify Pay_First_Mile_Canceled_Order rule")
				public void Pay_First_Mile_Canceled_Order(String de_id, String rest_id, String lat, String lan) throws InterruptedException, IOException  {
					String RuleName="Pay_First_Mile_Canceled_Order";
				   // SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).update(DeliveryConstant.RuleEnable+RuleName);

					double Bonus=22;
					    String order_id=DH.getOrderId(rest_id, lat, lan);
				     	payhelp.CancelAfterArrived(de_id,order_id);
				     	ruleAndBonusCheck(RuleName,Bonus,order_id);
					 //   SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).update(DeliveryConstant.RuleDisable+RuleName);


				}	
				
				//First_Mile_Till_4KM
				@Test(dataProvider="payoutData", description="Verify First_Mile_Till_4KM rule")
				public void First_Mile_Till_4KM(String de_id, String rest_id, String lat, String lan) throws InterruptedException, IOException  {
					String RuleName="'First_Mile_Till_4KM'";
				  //  SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).update(DeliveryConstant.RuleEnable+RuleName);

					double Bonus=23;
					    String order_id=DH.getOrderId(rest_id, lat, lan);
				     	payhelp.caprD(de_id,order_id);
				     	ruleAndBonusCheck(RuleName,Bonus,order_id);
					//    SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).update(DeliveryConstant.RuleDisable+RuleName);


				}
				
			//first_Mile_More_Than_4KM	
				@Test(dataProvider="payoutData", description="Verify First_Mile_More_Than_4KM rule")
				public void First_Mile_More_Than_4KM(String de_id, String rest_id, String lat, String lan) throws InterruptedException, IOException  {
					String RuleName="First_Mile_More_Than_4KM";
				   // SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).update(DeliveryConstant.RuleEnable+RuleName);

					double Bonus=24;
					    String order_id=DH.getOrderId(rest_id, lat, lan);
				     	payhelp.caprD(de_id,order_id);
				     	ruleAndBonusCheck(RuleName,Bonus,order_id);
					 //   SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).update(DeliveryConstant.RuleDisable+RuleName);


				}	
				
				//Rain_Count_Batch_Less_Than_9	
				@Test(dataProvider="payoutData", description="Rain_Count_Batch_Less_Than_9 Rule")
				public void Rain_Count_Batch_Less_Than_9(String de_id, String rest_id, String lat, String lan) throws InterruptedException, IOException  {
					String RuleName="Rain_Count_Batch_Less_Than_9";
				   // SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).update(DeliveryConstant.RuleEnable+RuleName);
					String zone_id="1";
				    SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(DeliveryConstant.enableHeavyRainMode+zone_id);
				
					double Bonus=25;
					    String order_id=DH.getOrderId(rest_id, lat, lan);
				     	payhelp.caprD(de_id,order_id);
				     	ruleAndBonusCheck(RuleName,Bonus,order_id);
					 //   SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).update(DeliveryConstant.RuleDisable+RuleName);

					     SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(DeliveryConstant.disableHeavyRainMode+zone_id);

				}	
					
				
				
				//EOD
				@Test( dataProvider="payoutData", description="Rain_Count_Batch_Less_Than_9 Rule")
				public void computeEventEodDIsabled(String de_id, String rest_id, String lat, String lan)  {
					String RuleName="'Peaks_Lunch_Weekend'";
					DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");				
					LocalDateTime now = LocalDateTime.now();
					System.out.println(dtf.format(now));
				    SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).update(DeliveryConstant.RuleDisable+RuleName);
					String eventName="eod";
					
				    try {
						if(payhelp.ComputeEvent(eventName,dtf.format(now).toString()))
						{
							String query=DeliveryConstant.DayIdata;
							String result=delmeth.dbhelperget(query, "rule_id").toString();
							if(!result.equalsIgnoreCase("1136"))
							{
							  Assert.assertTrue(true);
							}
							else
							{
						    	Assert.assertTrue(false);

							}
						}
						else
						{
							Assert.assertTrue(false);
						}
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				    SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).update(DeliveryConstant.RuleDisable+RuleName);

					
				}	
				//EOW
				@Test( dataProvider="payoutData", description="Rain_Count_Batch_Less_Than_9 Rule")
				public void computeEventEowDisabled(String de_id, String rest_id, String lat, String lan)  {
					String RuleName="'Shift_Count_Weekend'";
					DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");				
					LocalDateTime now = LocalDateTime.now();
					System.out.println(dtf.format(now));
				    SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).update(DeliveryConstant.RuleDisable+RuleName);
					String eventName="eow";
					
				 //   SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(DeliveryConstant.enableHeavyRainMode+zone_id);
				    try {
						if(payhelp.ComputeEvent(eventName,dtf.format(now).toString()))
						{
							String query=DeliveryConstant.WeeklyIdata;
							String result=delmeth.dbhelperget(query, "rule_id").toString();
							if(!result.equalsIgnoreCase("1135"))
							{
							  Assert.assertTrue(true);
							}
							else
							{
						    	Assert.assertTrue(false);

							}
						}
						else
						{
							Assert.assertTrue(false);
						}
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}	
				
				//EOM
				@Test( dataProvider="payoutData", description="Rain_Count_Batch_Less_Than_9 Rule")
				public void computeEventEomDIsabled(String de_id, String rest_id, String lat, String lan)  {
					String RuleName="'Monthly_incentive'";

					DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");				
					LocalDateTime now = LocalDateTime.now();
					System.out.println(dtf.format(now));
				    SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).update(DeliveryConstant.RuleDisable+RuleName);
					String eventName="eom";
					
				 //   SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(DeliveryConstant.enableHeavyRainMode+zone_id);
				    try {
						if(payhelp.ComputeEvent(eventName,dtf.format(now).toString()))
						{
							String query=DeliveryConstant.MonthlyIdata;
							String result=delmeth.dbhelperget(query, "rule_id").toString();
							if(!result.equalsIgnoreCase("1138"))
							{
							  Assert.assertTrue(true);
							}
							else
							{
						    	Assert.assertTrue(false);

							}
						}
						else
						{
							Assert.assertTrue(false);
						}
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}	
				//EOD
				@Test( dataProvider="payoutData", description="Rain_Count_Batch_Less_Than_9 Rule")
				public void computeEventEod(String de_id, String rest_id, String lat, String lan)   {
					DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");				
					LocalDateTime now = LocalDateTime.now();
					System.out.println(dtf.format(now));
					String eventName="eod";
					
				    try {
						if(payhelp.ComputeEvent(eventName,dtf.format(now).toString()))
						{
							String query=DeliveryConstant.DayIdata;
							String result=delmeth.dbhelperget(query, "rule_id").toString();
							if(result.equalsIgnoreCase("1127"))
							{
							  Assert.assertTrue(true);
							}
							else
							{
						    	Assert.assertTrue(false);

							}
						}
						else
						{
							Assert.assertTrue(false);
						}
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}	
				//EOW 
				@Test( dataProvider="payoutData", description="Rain_Count_Batch_Less_Than_9 Rule")
				public void computeEventEow(String de_id, String rest_id, String lat, String lan)   {
					DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");				
					LocalDateTime now = LocalDateTime.now();
					System.out.println(dtf.format(now));
				   // SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).update(DeliveryConstant.RuleEnable+RuleName);
					String eventName="eow";
					
				 //   SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(DeliveryConstant.enableHeavyRainMode+zone_id);
				    try {
						if(payhelp.ComputeEvent(eventName,dtf.format(now).toString()))
						{
							String query=DeliveryConstant.WeeklyIdata;
							String result=delmeth.dbhelperget(query, "rule_id").toString();
							if(result.equalsIgnoreCase("1116"))
							{
							  Assert.assertTrue(true);
							}
							else
							{
						    	Assert.assertTrue(false);

							}
						}
						else
						{
							Assert.assertTrue(false);
						}
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}	
				//EOM
				@Test( dataProvider="payoutData", description="Rain_Count_Batch_Less_Than_9 Rule")
				public void computeEventEom(String de_id, String rest_id, String lat, String lan)   {
					DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");				
					LocalDateTime now = LocalDateTime.now();
					System.out.println(dtf.format(now));
				   // SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).update(DeliveryConstant.RuleEnable+RuleName);
					String eventName="eom";
					
				 //   SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(DeliveryConstant.enableHeavyRainMode+zone_id);
				    try {
						if(payhelp.ComputeEvent(eventName,dtf.format(now).toString()))
						{
							String query=DeliveryConstant.MonthlyIdata;
							String result=delmeth.dbhelperget(query, "rule_id").toString();
							if(result.equalsIgnoreCase("1137"))
							{
							  Assert.assertTrue(true);
							}
							else
							{
						    	Assert.assertTrue(false);
							}
						}
						else
						{
							Assert.assertTrue(false);
						}
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}	
				//min guarantee
				@Test( dataProvider="payoutData", description="Rain_Count_Batch_Less_Than_9 Rule")
				public void computeEventmgl(String de_id, String rest_id, String lat, String lan) {
					DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");				
					LocalDateTime now = LocalDateTime.now();
					System.out.println(dtf.format(now));
				   // SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).update(DeliveryConstant.RuleEnable+RuleName);
					String eventName="eod";
					
				 //   SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(DeliveryConstant.enableHeavyRainMode+zone_id);
				    try {
						if(payhelp.ComputeEvent(eventName,dtf.format(now).toString()))
						{
							String query=DeliveryConstant.DayIdata;
							String result=delmeth.dbhelperget(query, "rule_id").toString();
							if(result.equalsIgnoreCase("1138"))
							{
							  Assert.assertTrue(false);
							}
							else
							{
						    	Assert.assertTrue(true);
							}
						}
						else
						{
							Assert.assertTrue(false);
						}
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}	
	

	
}
	

	
