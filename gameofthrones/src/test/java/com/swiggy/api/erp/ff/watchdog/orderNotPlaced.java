package com.swiggy.api.erp.ff.watchdog;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.delivery.helper.DeliveryHelperMethods;
import com.swiggy.api.erp.ff.constants.OMSConstants;
import com.swiggy.api.erp.ff.helper.LOSHelper;
import com.swiggy.api.erp.ff.helper.OMSHelper;
import com.swiggy.api.erp.ff.helper.WatchdogHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Tyrion.RabbitMQHelper;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.swiggy.api.erp.ff.dp.orderNotPlacedDP;
import framework.gameofthrones.Tyrion.DBHelper;



import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;

public class orderNotPlaced extends orderNotPlacedDP {

    WatchdogHelper watchdoghelper = new WatchdogHelper();
    LOSHelper loshelper = new LOSHelper();
    RabbitMQHelper rmqhelper = new RabbitMQHelper();
    OMSHelper oms = new OMSHelper();
    Initialize initialize = new Initialize();
    DeliveryHelperMethods dehelper = new DeliveryHelperMethods();
    DBHelper db = new DBHelper();

    @BeforeClass
    public void enableONP(){
        System.out.println("Enable ONP Rule====================================");
        SystemConfigProvider.getTemplate(OMSConstants.WATCHDOG_DB).execute("UPDATE watchdog.eca SET enabled=1 WHERE name = 'DelayedOrderToDeliveryWF';");
        SystemConfigProvider.getTemplate(OMSConstants.WATCHDOG_DB).execute("UPDATE watchdog.eca SET enabled=1 WHERE name = 'WatchdogRelayOrderToDeliveryWorkflow';");
    }


    @Test(priority = 1, groups = { "sanity", "regression" }, dataProvider="createOrderONP", description = "create order which are not be relayed to delivery till restaurant confirms the order")
    public void createOrderONP(String manual_order_id) throws InterruptedException {

        //Verify order is relayed to watchdog
        String message = rmqhelper.getMessage(OMSConstants.RMQHOSTNAME,OMSConstants.ONP_ORDERS);
        System.out.println("Message is " +message);
        Assert.assertEquals(""+JsonPath.read(message, "$.order_id"),manual_order_id);
        Thread.sleep(1000);
        //checking order is present in watchdog_eca_logs table
        List<Map<String, Object>> instanceId = SystemConfigProvider.getTemplate(OMSConstants.WATCHDOG_DB).queryForList("SELECT instance_id,state FROM watchdog.eca_logs where instance_id = "+ manual_order_id  +";");
        Assert.assertEquals(instanceId.get(0).get("instance_id").toString(),manual_order_id);
        Assert.assertEquals(instanceId.get(0).get("state").toString(),"COMPLETED");
        Thread.sleep(50000);
        Boolean deliver_order_id = null;
        deliver_order_id  = DBHelper.pollDB("deliverydb","SELECT * FROM delivery.trips WHERE order_id=" + manual_order_id,"order_id",manual_order_id,15,45);
        if (deliver_order_id){
            Assert.fail("Order already relayed to delivery");
        }else {
            Thread.sleep(300000);
            deliver_order_id = DBHelper.pollDB("deliverydb","SELECT * FROM delivery.trips WHERE order_id=" + manual_order_id,"order_id",manual_order_id,15,45);
            Assert.assertTrue(deliver_order_id);
        }

    }

    @Test(priority = 2, groups = { "sanity", "regression" }, dataProvider="createOrderONP", description = "Create order from the ratautant which are tagged as ONP restaurant and order is confirmed by vendor")
    public void createOnpPlacedOrder(String manual_order_id) throws IOException, TimeoutException, InterruptedException {

        //Verify order is relayed to watchdog
        String message = rmqhelper.getMessage(OMSConstants.RMQHOSTNAME,OMSConstants.ONP_ORDERS);
        System.out.println("Message is " +message);
        Assert.assertEquals(""+JsonPath.read(message, "$.order_id"),manual_order_id);
        Thread.sleep(1000);
        //checking order is present in watchdog_eca_logs table
        List<Map<String, Object>> instanceId = SystemConfigProvider.getTemplate(OMSConstants.WATCHDOG_DB).queryForList("SELECT instance_id,state FROM watchdog.eca_logs where instance_id = "+ manual_order_id  +";");
        Assert.assertEquals(instanceId.get(0).get("instance_id").toString(),manual_order_id);
        Assert.assertEquals(instanceId.get(0).get("state").toString(),"COMPLETED");
        oms.changeOrderStatusToPlaced(manual_order_id,"5GNaGtPyPc5cqhaNdiYynUPfypCfMYfd");
        Thread.sleep(10000);
        try {
            String deliver_order_id = dehelper.dbhelperget("SELECT * FROM delivery.trips WHERE order_id=" + manual_order_id, "order_id").toString();
            Assert.assertEquals(manual_order_id, deliver_order_id);
            System.out.println("Delivery Order Id "+deliver_order_id);
        }
        catch(NullPointerException e){

            Assert.fail(e.getMessage());
        }

    }

    @Test(priority = 3, groups = { "sanity", "regression" }, dataProvider="NormalOrderFlow", description = "Create order from retaurant which is not tagged as ONP restaurant")
    public void NormalOrderFlow(String pop_order_id) throws IOException, TimeoutException, InterruptedException {

        //checking order should not be present in watchdog_eca_logs table
        List<Map<String, Object>> instanceId = SystemConfigProvider.getTemplate(OMSConstants.WATCHDOG_DB).queryForList("SELECT instance_id,state FROM watchdog.eca_logs where instance_id = "+ pop_order_id  +";");
        Assert.assertTrue(instanceId.isEmpty());
        Thread.sleep(10000);
        try {
            String deliver_order_id = dehelper.dbhelperget("SELECT * FROM delivery.trips WHERE order_id=" + pop_order_id, "order_id").toString();
            Assert.assertEquals(pop_order_id, deliver_order_id);
            System.out.println("Delivery Order Id "+deliver_order_id);
        }
        catch(NullPointerException e){

            Assert.fail(e.getMessage());
        }

    }


    @Test(priority = 4,groups = { "sanity", "regression" }, dataProvider="PartnerOrderFlow", description = "verify delivery db entry after restaurant confirms the order")
    public void restaurantOrderConfirm(String partner_order_id) throws InterruptedException {

        //Verify order is relayed to watchdog
        String message = rmqhelper.getMessage(OMSConstants.RMQHOSTNAME,OMSConstants.ONP_ORDERS);
        System.out.println("Message is " +message);
        Assert.assertEquals(""+JsonPath.read(message, "$.order_id"),partner_order_id);
        Thread.sleep(1000);
        //checking order is present in watchdog_eca_logs table
        List<Map<String, Object>> instanceId = SystemConfigProvider.getTemplate(OMSConstants.WATCHDOG_DB).queryForList("SELECT instance_id,state FROM watchdog.eca_logs where instance_id = "+ partner_order_id  +";");
        Assert.assertEquals(instanceId.get(0).get("instance_id").toString(),partner_order_id);
        Assert.assertEquals(instanceId.get(0).get("state").toString(),"COMPLETED");
        oms.changeOrderStatusToPlacedPartner(partner_order_id);
        Thread.sleep(10000);
        try {
            String deliver_order_id = dehelper.dbhelperget("SELECT * FROM delivery.trips WHERE order_id=" + partner_order_id, "order_id").toString();
            Assert.assertEquals(partner_order_id, deliver_order_id);
            System.out.println("Delivery Order Id "+deliver_order_id);
        }
        catch(NullPointerException e){

            Assert.fail(e.getMessage());
        }

    }

}