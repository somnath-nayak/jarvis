package com.swiggy.api.erp.ff.tests.assignmentService;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.ff.constants.OMSConstants;
import com.swiggy.api.erp.ff.dp.assignmentService;
import com.swiggy.api.erp.ff.helper.AssignmentServiceHelper;
import com.swiggy.api.erp.ff.helper.LOSHelper;
import com.swiggy.api.erp.ff.helper.OMSHelper;

import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Tyrion.RabbitMQHelper;
import framework.gameofthrones.Tyrion.RedisHelper;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;

public class ManualOrderAssignmentTest extends assignmentService{
    LOSHelper losHelper = new LOSHelper();
    AssignmentServiceHelper assignmentServiceHelper = new AssignmentServiceHelper();
    RedisHelper redisHelper = new RedisHelper();
    RabbitMQHelper rmqhelper  = new RabbitMQHelper();
    OMSHelper omshelper = new OMSHelper();

    @Test(groups = {"sanity","regression"}, dataProvider = "assignmentValidation", description="oe assignment validation")
    public void testAssignment(int oe_id,int role_id, int city_id) throws IOException, InterruptedException, TimeoutException {
    	
    	Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long orderId = Instant.now().getEpochSecond();
        //deleting redis entry for order and oe
        omshelper.redisFlushAll(OMSConstants.REDIS);
/*      redisHelper.deleteKey(OMSConstants.REDIS,0,"ORDER_SORTED_SET_"+city_id+"_"+role_id);
        redisHelper.deleteKey(OMSConstants.REDIS,0,"OE_SORTED_SET_"+role_id);
        redisHelper.deleteKey(OMSConstants.REDIS,0,"OE_"+role_id);*/
        //deleting mysql entries in assignment table
        SystemConfigProvider.getTemplate(OMSConstants.ASSIGNMENT_DB).execute("delete FROM db_assignment.assignment;");
        //Update OrderCount to 0 
        SystemConfigProvider.getTemplate(OMSConstants.SERVICE).execute("update oms_orderexecutive set current_order_count=0 where id="+oe_id+";");
        //purging TestAssignment Queue
        rmqhelper.purgeQueue("oms","testassignment");
        //placing manual order
        losHelper.createManualOrder(""+orderId, dateFormat.format(date), ""+orderId);
        Thread.sleep(10000);
        //checking whether order entry has created in redis
        String sortedListOrder = redisHelper.Zrange(OMSConstants.REDIS,0,"ORDER_SORTED_SET_"+city_id+"_"+role_id,0,1000).toString();
        System.out.println(sortedListOrder);
        //Assert.assertEquals(sortedListOrder.contains(""+orderId), true);
        //checking order is present in ordersinqueue table
        List<Map<String, Object>> postgresOrders = SystemConfigProvider.getTemplate(OMSConstants.SERVICE).queryForList("select order_id,waiting_for from oms_ordersinqueue where order_id = "+orderId+";");
        Assert.assertEquals(String.valueOf(postgresOrders.get(0).get("order_id")), ""+orderId);
        Assert.assertEquals(String.valueOf(postgresOrders.get(0).get("waiting_for")), "Verifier");
        //OE login
        assignmentServiceHelper.oeLoginRMQ(oe_id,role_id);
        Thread.sleep(5000);
        //checking whether OE entry has created in redis
        String sortedListOe = redisHelper.Zrange(OMSConstants.REDIS,0,"OE_SORTED_SET_"+role_id,0,1000).toString();
        //Assert.assertEquals(sortedListOe.contains(""+oe_id), true);
        //checking whether order is assigned to OE in mysql table
        List<Map<String, Object>> mysqlOrders = SystemConfigProvider.getTemplate(OMSConstants.ASSIGNMENT_DB).queryForList("SELECT order_id FROM db_assignment.assignment where order_id = "+ orderId  +";");
        Assert.assertEquals(String.valueOf(mysqlOrders.get(0).get("order_id")), ""+orderId);
        //checking order count in orderexecutive table
        List<Map<String, Object>> postgresOrderCount = SystemConfigProvider.getTemplate(OMSConstants.SERVICE).queryForList("select current_order_count from oms_orderexecutive where id = "+ oe_id  +";");
        Assert.assertEquals(String.valueOf(postgresOrderCount.get(0).get("current_order_count")), "1");
        Thread.sleep(5000);
        //Checking queue message published
        String message = rmqhelper.getMessage(OMSConstants.RMQHOSTNAME,OMSConstants.ASSIGNMENTQUEUE);
        System.out.println("Message is " +message);
        String oeId = JsonPath.read(message, "$.oeId");
        Assert.assertEquals(oeId, ""+oe_id);
         	
    }
    
    @Test(groups = {"sanity","regression"}, dataProvider = "assignmentValidationUnassignment", description="oe unassignment validation")
    public void testUnAssignment(int oe_id,int role_id, int city_id,int new_state_id) throws IOException, InterruptedException, TimeoutException {
    	
    	Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long orderId = Instant.now().getEpochSecond();
        //deleting redis entry for order and oe
        omshelper.redisFlushAll(OMSConstants.REDIS);
/*      redisHelper.deleteKey(OMSConstants.REDIS,0,"ORDER_SORTED_SET_"+city_id+"_"+role_id);
        redisHelper.deleteKey(OMSConstants.REDIS,0,"OE_SORTED_SET_"+role_id);
        redisHelper.deleteKey(OMSConstants.REDIS,0,"OE_"+role_id);*/
        //deleting mysql entries in assignment table
        SystemConfigProvider.getTemplate(OMSConstants.ASSIGNMENT_DB).execute("delete FROM db_assignment.assignment;");
        //Update OrderCount to 0 
        SystemConfigProvider.getTemplate(OMSConstants.SERVICE).execute("update oms_orderexecutive set current_order_count=0 where id="+oe_id+";");
        //purging TestAssignment Queue
        rmqhelper.purgeQueue("oms","testassignment");
        //placing manual order
        losHelper.createManualOrder(""+orderId, dateFormat.format(date), ""+orderId);
        Thread.sleep(5000);
        //checking whether order entry has created in redis
        String sortedListOrder = redisHelper.Zrange(OMSConstants.REDIS,0,"ORDER_SORTED_SET_"+city_id+"_"+role_id,0,1000).toString();
        System.out.println(sortedListOrder);
        //Assert.assertEquals(sortedListOrder.contains(""+orderId), true);
        //checking order is present in ordersinqueue table
        List<Map<String, Object>> postgresOrders = SystemConfigProvider.getTemplate(OMSConstants.SERVICE).queryForList("select order_id,waiting_for from oms_ordersinqueue where order_id = "+orderId+";");
        Assert.assertEquals(String.valueOf(postgresOrders.get(0).get("order_id")), ""+orderId);
        Assert.assertEquals(String.valueOf(postgresOrders.get(0).get("waiting_for")), "Verifier");
        //OE login
        assignmentServiceHelper.oeLoginRMQ(oe_id,role_id);
        Thread.sleep(5000);
        //checking whether OE entry has created in redis
        String sortedListOe = redisHelper.Zrange(OMSConstants.REDIS,0,"OE_SORTED_SET_"+role_id,0,1000).toString();
        //Assert.assertEquals(sortedListOe.contains(""+oe_id), true);
        //checking whether order is assigned to OE in mysql table
        List<Map<String, Object>> mysqlOrders = SystemConfigProvider.getTemplate(OMSConstants.ASSIGNMENT_DB).queryForList("SELECT order_id FROM db_assignment.assignment where order_id = "+ orderId  +";");
        Assert.assertEquals(String.valueOf(mysqlOrders.get(0).get("order_id")), ""+orderId);
       //checking order count in orderexecutive table
        List<Map<String, Object>> postgresOrderCount = SystemConfigProvider.getTemplate(OMSConstants.SERVICE).queryForList("select current_order_count from oms_orderexecutive where id = "+ oe_id  +";");
        Assert.assertEquals(String.valueOf(postgresOrderCount.get(0).get("current_order_count")), "1");
        Thread.sleep(5000);
        //Checking queue message published
        String message = rmqhelper.getMessage(OMSConstants.RMQHOSTNAME,OMSConstants.ASSIGNMENTQUEUE);
        System.out.println("Message is " +message);
        String oeId = JsonPath.read(message, "$.oeId");
        Assert.assertEquals(oeId, ""+oe_id);
        //OE logout
        assignmentServiceHelper.oeLogoutRMQ(oe_id,role_id,new_state_id);
        Thread.sleep(3000);
        //checking whether order is unassigned to OE in mysql table
        List<Map<String, Object>> mysqlUnassign = SystemConfigProvider.getTemplate(OMSConstants.ASSIGNMENT_DB).queryForList("SELECT reasonForUnassignment_id FROM db_assignment.assignment where order_id = "+ orderId  +";");
        Assert.assertEquals(String.valueOf(mysqlUnassign.get(0).get("reasonForUnassignment_id")), "1");
        //Checking queue message published
        String message1 = rmqhelper.getMessage(OMSConstants.RMQHOSTNAME,OMSConstants.ASSIGNMENTQUEUE);
        System.out.println("Message is " +message1);
        String oeId1 = JsonPath.read(message1, "$.oeId");
        Assert.assertEquals(oeId1, ""+oe_id);
        //checking order count in orderexecutive table
        List<Map<String, Object>> postgresOrderCount1 = SystemConfigProvider.getTemplate(OMSConstants.SERVICE).queryForList("select current_order_count from oms_orderexecutive where id = "+ oe_id  +";");
        Assert.assertEquals(String.valueOf(postgresOrderCount1.get(0).get("current_order_count")), "0");
        //checking order is present in ordersinqueue table
        List<Map<String, Object>> postgresOrders1 = SystemConfigProvider.getTemplate(OMSConstants.SERVICE).queryForList("select order_id,waiting_for from oms_ordersinqueue where order_id = "+orderId+";");
        Assert.assertEquals(String.valueOf(postgresOrders1.get(0).get("order_id")), ""+orderId);
        Assert.assertEquals(String.valueOf(postgresOrders1.get(0).get("waiting_for")), "Verifier");
        
    }   
    
    
    @Test(groups = {"sanity","regression"}, dataProvider = "orderStatus", description="oe unassignment validation post order verification")
    public void testUnAssignmentOnAllGoodProceedVerifier(int oe_id,int role_id, int city_id, int status_id) throws IOException, InterruptedException, TimeoutException {
    	
    	Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long orderId = Instant.now().getEpochSecond();
        //deleting redis entry for order and oe
        omshelper.redisFlushAll(OMSConstants.REDIS);
/*      redisHelper.deleteKey(OMSConstants.REDIS,0,"ORDER_SORTED_SET_"+city_id+"_"+role_id);
        redisHelper.deleteKey(OMSConstants.REDIS,0,"OE_SORTED_SET_"+role_id);
        redisHelper.deleteKey(OMSConstants.REDIS,0,"OE_"+role_id);*/
        //deleting mysql entries in assignment table
        SystemConfigProvider.getTemplate(OMSConstants.ASSIGNMENT_DB).execute("delete FROM db_assignment.assignment;");
        //Update OrderCount to 0 
        SystemConfigProvider.getTemplate(OMSConstants.SERVICE).execute("update oms_orderexecutive set current_order_count=0 where id="+oe_id+";");
        //purging TestAssignment Queue
        rmqhelper.purgeQueue("oms","testassignment");
        //placing manual order
        losHelper.createManualOrder(""+orderId, dateFormat.format(date), ""+orderId);
        Thread.sleep(5000);
        //checking whether order entry has created in redis
        String sortedListOrder = redisHelper.Zrange(OMSConstants.REDIS,0,"ORDER_SORTED_SET_"+city_id+"_"+role_id,0,1000).toString();
        System.out.println(sortedListOrder);
        //Assert.assertEquals(sortedListOrder.contains(""+orderId), true);
        //checking order is present in ordersinqueue table
        List<Map<String, Object>> postgresOrders = SystemConfigProvider.getTemplate(OMSConstants.SERVICE).queryForList("select order_id,waiting_for from oms_ordersinqueue where order_id = "+orderId+";");
        Assert.assertEquals(String.valueOf(postgresOrders.get(0).get("order_id")), ""+orderId);
        Assert.assertEquals(String.valueOf(postgresOrders.get(0).get("waiting_for")), "Verifier");
        //OE login
        assignmentServiceHelper.oeLoginRMQ(oe_id,role_id);
        Thread.sleep(5000);
        //checking whether OE entry has created in redis
        String sortedListOe = redisHelper.Zrange(OMSConstants.REDIS,0,"OE_SORTED_SET_"+role_id,0,1000).toString();
        //Assert.assertEquals(sortedListOe.contains(""+oe_id), true);
        //checking whether order is assigned to OE in mysql table
        List<Map<String, Object>> mysqlOrders = SystemConfigProvider.getTemplate(OMSConstants.ASSIGNMENT_DB).queryForList("SELECT order_id FROM db_assignment.assignment where order_id = "+ orderId  +";");
        Assert.assertEquals(String.valueOf(mysqlOrders.get(0).get("order_id")), ""+orderId);
        //checking order count in orderexecutive table
        List<Map<String, Object>> postgresOrderCount = SystemConfigProvider.getTemplate(OMSConstants.SERVICE).queryForList("select current_order_count from oms_orderexecutive where id = "+ oe_id  +";");
        Assert.assertEquals(String.valueOf(postgresOrderCount.get(0).get("current_order_count")), "1");
        Thread.sleep(5000);
        //OE All good proceed action from verifier
        assignmentServiceHelper.orderStatusRMQ(orderId, status_id);
        //checking whether order is unassigned to OE in mysql table
        List<Map<String, Object>> mysqlUnassign = SystemConfigProvider.getTemplate(OMSConstants.ASSIGNMENT_DB).queryForList("SELECT reasonForUnassignment_id FROM db_assignment.assignment where order_id = "+ orderId  +";");
        Assert.assertEquals(String.valueOf(mysqlUnassign.get(0).get("reasonForUnassignment_id")), "1");
        //checking order count in orderexecutive table
        List<Map<String, Object>> postgresOrderCount1 = SystemConfigProvider.getTemplate(OMSConstants.SERVICE).queryForList("select current_order_count from oms_orderexecutive where id = "+ oe_id  +";");
        Assert.assertEquals(String.valueOf(postgresOrderCount1.get(0).get("current_order_count")), "0");
        
    }   
    
    
    @Test(groups = {"sanity","regression"}, dataProvider = "orderStatusCancelled", description="oe unassignment validation post order cancellation")
    public void testUnAssignmentOnOrderCancelVerifier(int oe_id,int role_id, int city_id,int status_id) throws IOException, InterruptedException, TimeoutException {
    	
    	Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long orderId = Instant.now().getEpochSecond();
        //deleting redis entry for order and oe
        omshelper.redisFlushAll(OMSConstants.REDIS);
/*      redisHelper.deleteKey(OMSConstants.REDIS,0,"ORDER_SORTED_SET_"+city_id+"_"+role_id);
        redisHelper.deleteKey(OMSConstants.REDIS,0,"OE_SORTED_SET_"+role_id);
        redisHelper.deleteKey(OMSConstants.REDIS,0,"OE_"+role_id);*/
        //deleting mysql entries in assignment table
        SystemConfigProvider.getTemplate(OMSConstants.ASSIGNMENT_DB).execute("delete FROM db_assignment.assignment;");
        //Update OrderCount to 0 
        SystemConfigProvider.getTemplate(OMSConstants.SERVICE).execute("update oms_orderexecutive set current_order_count=0 where id="+oe_id+";");
        //purging TestAssignment Queue
        rmqhelper.purgeQueue("oms","testassignment");
        //placing manual order
        losHelper.createManualOrder(""+orderId, dateFormat.format(date), ""+orderId);
        Thread.sleep(5000);
        //checking whether order entry has created in redis
        String sortedListOrder = redisHelper.Zrange(OMSConstants.REDIS,0,"ORDER_SORTED_SET_"+city_id+"_"+role_id,0,1000).toString();
        System.out.println(sortedListOrder);
        //Assert.assertEquals(sortedListOrder.contains(""+orderId), true);
        //checking order is present in ordersinqueue table
        List<Map<String, Object>> postgresOrders = SystemConfigProvider.getTemplate(OMSConstants.SERVICE).queryForList("select order_id,waiting_for from oms_ordersinqueue where order_id = "+orderId+";");
        Assert.assertEquals(String.valueOf(postgresOrders.get(0).get("order_id")), ""+orderId);
        Assert.assertEquals(String.valueOf(postgresOrders.get(0).get("waiting_for")), "Verifier");
        //OE login
        assignmentServiceHelper.oeLoginRMQ(oe_id,role_id);
        Thread.sleep(5000);
        //checking whether OE entry has created in redis
        String sortedListOe = redisHelper.Zrange(OMSConstants.REDIS,0,"OE_SORTED_SET_"+role_id,0,1000).toString();
        //Assert.assertEquals(sortedListOe.contains(""+oe_id), true);
        //checking whether order is assigned to OE in mysql table
        List<Map<String, Object>> mysqlOrders = SystemConfigProvider.getTemplate(OMSConstants.ASSIGNMENT_DB).queryForList("SELECT order_id FROM db_assignment.assignment where order_id = "+ orderId  +";");
        Assert.assertEquals(String.valueOf(mysqlOrders.get(0).get("order_id")), ""+orderId);
        //checking order count in orderexecutive table
        List<Map<String, Object>> postgresOrderCount = SystemConfigProvider.getTemplate(OMSConstants.SERVICE).queryForList("select current_order_count from oms_orderexecutive where id = "+ oe_id  +";");
        Assert.assertEquals(String.valueOf(postgresOrderCount.get(0).get("current_order_count")), "1");
        Thread.sleep(5000);
        //OE All good proceed action from verifier
        assignmentServiceHelper.orderStatusRMQ(orderId, status_id);
        //checking whether order is unassigned to OE in mysql table
        List<Map<String, Object>> mysqlUnassign = SystemConfigProvider.getTemplate(OMSConstants.ASSIGNMENT_DB).queryForList("SELECT reasonForUnassignment_id FROM db_assignment.assignment where order_id = "+ orderId  +";");
        Assert.assertEquals(String.valueOf(mysqlUnassign.get(0).get("reasonForUnassignment_id")), "1");
        //checking order count in orderexecutive table
        List<Map<String, Object>> postgresOrderCount1 = SystemConfigProvider.getTemplate(OMSConstants.SERVICE).queryForList("select current_order_count from oms_orderexecutive where id = "+ oe_id  +";");
        Assert.assertEquals(String.valueOf(postgresOrderCount1.get(0).get("current_order_count")), "0");
        
    }
    
    
    @Test(groups = {"sanity","regression"}, dataProvider = "orderStatusCNR", description="oe unassignment validation post cnr for verification")
    public void testUnAssignmentOnCNRVerification(int oe_id,int role_id, int city_id,int uid) throws IOException, InterruptedException, TimeoutException {
    	
    	Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long orderId = Instant.now().getEpochSecond();
        //deleting redis entry for order and oe
        omshelper.redisFlushAll(OMSConstants.REDIS);
/*      redisHelper.deleteKey(OMSConstants.REDIS,0,"ORDER_SORTED_SET_"+city_id+"_"+role_id);
        redisHelper.deleteKey(OMSConstants.REDIS,0,"OE_SORTED_SET_"+role_id);
        redisHelper.deleteKey(OMSConstants.REDIS,0,"OE_"+role_id);*/
        //deleting mysql entries in assignment table
        SystemConfigProvider.getTemplate(OMSConstants.ASSIGNMENT_DB).execute("delete FROM db_assignment.assignment;");
        //Update OrderCount to 0 
        SystemConfigProvider.getTemplate(OMSConstants.SERVICE).execute("update oms_orderexecutive set current_order_count=0 where id="+oe_id+";");
        //purging TestAssignment Queue
        rmqhelper.purgeQueue("oms","testassignment");
        //placing manual order
        losHelper.createManualOrder(""+orderId, dateFormat.format(date), ""+orderId);
        Thread.sleep(5000);
        //checking whether order entry has created in redis
        String sortedListOrder = redisHelper.Zrange(OMSConstants.REDIS,0,"ORDER_SORTED_SET_"+city_id+"_"+role_id,0,1000).toString();
        System.out.println(sortedListOrder);
        //Assert.assertEquals(sortedListOrder.contains(""+orderId), true);
        //checking order is present in ordersinqueue table
        List<Map<String, Object>> postgresOrders = SystemConfigProvider.getTemplate(OMSConstants.SERVICE).queryForList("select order_id,waiting_for from oms_ordersinqueue where order_id = "+orderId+";");
        Assert.assertEquals(String.valueOf(postgresOrders.get(0).get("order_id")), ""+orderId);
        Assert.assertEquals(String.valueOf(postgresOrders.get(0).get("waiting_for")), "Verifier");
        //OE login
        assignmentServiceHelper.oeLoginRMQ(oe_id,role_id);
        Thread.sleep(5000);
        //checking whether OE entry has created in redis
        String sortedListOe = redisHelper.Zrange(OMSConstants.REDIS,0,"OE_SORTED_SET_"+role_id,0,1000).toString();
        //Assert.assertEquals(sortedListOe.contains(""+oe_id), true);
        //checking whether order is assigned to OE in mysql table
        List<Map<String, Object>> mysqlOrders = SystemConfigProvider.getTemplate(OMSConstants.ASSIGNMENT_DB).queryForList("SELECT order_id FROM db_assignment.assignment where order_id = "+ orderId  +";");
        Assert.assertEquals(String.valueOf(mysqlOrders.get(0).get("order_id")), ""+orderId);
       //checking order count in orderexecutive table
        List<Map<String, Object>> postgresOrderCount = SystemConfigProvider.getTemplate(OMSConstants.SERVICE).queryForList("select current_order_count from oms_orderexecutive where id = "+ oe_id  +";");
        Assert.assertEquals(String.valueOf(postgresOrderCount.get(0).get("current_order_count")), "1");
        Thread.sleep(5000);
        assignmentServiceHelper.oeFreeRMQ(orderId,oe_id,role_id,uid);
        //checking whether order is unassigned to OE in mysql table
        List<Map<String, Object>> mysqlUnassign = SystemConfigProvider.getTemplate(OMSConstants.ASSIGNMENT_DB).queryForList("SELECT reasonForUnassignment_id FROM db_assignment.assignment where order_id = "+ orderId  +";");
        Assert.assertEquals(String.valueOf(mysqlUnassign.get(0).get("reasonForUnassignment_id")), "6");
        //checking order count in orderexecutive table
        List<Map<String, Object>> postgresOrderCount1 = SystemConfigProvider.getTemplate(OMSConstants.SERVICE).queryForList("select current_order_count from oms_orderexecutive where id = "+ oe_id  +";");
        Assert.assertEquals(String.valueOf(postgresOrderCount1.get(0).get("current_order_count")), "0");
        
    }
    
    @Test(groups = {"sanity","regression"}, dataProvider = "orderStatusPlaced", description="oe unassignment validation post order placed by L1/L2")
    public void testUnAssignmentOnOrderPlaced(int oe_id,int role_id, int city_id,int status_id) throws IOException, TimeoutException, InterruptedException {
    	
    	Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long orderId = Instant.now().getEpochSecond();
        //deleting redis entry for order and oe
        omshelper.redisFlushAll(OMSConstants.REDIS);
/*      redisHelper.deleteKey(OMSConstants.REDIS,0,"ORDER_SORTED_SET_"+city_id+"_"+role_id);
        redisHelper.deleteKey(OMSConstants.REDIS,0,"OE_SORTED_SET_"+role_id);
        redisHelper.deleteKey(OMSConstants.REDIS,0,"OE_"+role_id);*/
        //deleting mysql entries in assignment table
        SystemConfigProvider.getTemplate(OMSConstants.ASSIGNMENT_DB).execute("delete FROM db_assignment.assignment;");
        //Update OrderCount to 0 
        SystemConfigProvider.getTemplate(OMSConstants.SERVICE).execute("update oms_orderexecutive set current_order_count=0 where id="+oe_id+";");
        //purging TestAssignment Queue
        rmqhelper.purgeQueue("oms","testassignment");
        //placing manual order
        losHelper.createManualOrder(""+orderId, dateFormat.format(date), ""+orderId);
        Thread.sleep(5000);
        //checking whether order entry has created in redis
        String sortedListOrder = redisHelper.Zrange(OMSConstants.REDIS,0,"ORDER_SORTED_SET_"+city_id+"_"+role_id,0,1000).toString();
        System.out.println(sortedListOrder);
        //Assert.assertEquals(sortedListOrder.contains(""+orderId), true);
        //checking order is present in ordersinqueue table
        List<Map<String, Object>> postgresOrders = SystemConfigProvider.getTemplate(OMSConstants.SERVICE).queryForList("select order_id,waiting_for from oms_ordersinqueue where order_id = "+orderId+";");
        Assert.assertEquals(String.valueOf(postgresOrders.get(0).get("order_id")), ""+orderId);
        Assert.assertEquals(String.valueOf(postgresOrders.get(0).get("waiting_for")), "Verifier");
        //OE login
        assignmentServiceHelper.oeLoginRMQ(oe_id,role_id);
        Thread.sleep(5000);
        //checking whether OE entry has created in redis
        String sortedListOe = redisHelper.Zrange(OMSConstants.REDIS,0,"OE_SORTED_SET_"+role_id,0,1000).toString();
        //Assert.assertEquals(sortedListOe.contains(""+oe_id), true);
        //checking whether order is assigned to OE in mysql table
        List<Map<String, Object>> mysqlOrders = SystemConfigProvider.getTemplate(OMSConstants.ASSIGNMENT_DB).queryForList("SELECT order_id FROM db_assignment.assignment where order_id = "+ orderId  +";");
        Assert.assertEquals(String.valueOf(mysqlOrders.get(0).get("order_id")), ""+orderId);
        //checking order count in orderexecutive table
        List<Map<String, Object>> postgresOrderCount = SystemConfigProvider.getTemplate(OMSConstants.SERVICE).queryForList("select current_order_count from oms_orderexecutive where id = "+ oe_id  +";");
        Assert.assertEquals(String.valueOf(postgresOrderCount.get(0).get("current_order_count")), "1");
        Thread.sleep(5000);
        //OE All good proceed action from verifier
        assignmentServiceHelper.orderStatusRMQ(orderId, status_id);
        //checking whether order is unassigned to OE in mysql table
        List<Map<String, Object>> mysqlUnassign = SystemConfigProvider.getTemplate(OMSConstants.ASSIGNMENT_DB).queryForList("SELECT reasonForUnassignment_id FROM db_assignment.assignment where order_id = "+ orderId  +";");
        Assert.assertEquals(String.valueOf(mysqlUnassign.get(0).get("reasonForUnassignment_id")), "1");
        //checking order count in orderexecutive table
        List<Map<String, Object>> postgresOrderCount1 = SystemConfigProvider.getTemplate(OMSConstants.SERVICE).queryForList("select current_order_count from oms_orderexecutive where id = "+ oe_id  +";");
        Assert.assertEquals(String.valueOf(postgresOrderCount1.get(0).get("current_order_count")), "0");
        
    }
    
    @Test(groups = {"sanity","regression"}, dataProvider = "E2E", description="Manual Order E2E Assignment")
    public void testE2EAssignment(int verifier,int verifier_role_id, int city_id,int status_id, int L1, int l1_role_id,int L2, int l2_role_id,int callDE, int callDE_role_id) throws IOException, TimeoutException, InterruptedException {
    	
    	Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long orderId = Instant.now().getEpochSecond();
        //deleting redis entry for order and oe
        omshelper.redisFlushAll(OMSConstants.REDIS);
        /*redisHelper.deleteKey(OMSConstants.REDIS,0,"ORDER_SORTED_SET_"+city_id+"_"+verifier_role_id);
        redisHelper.deleteKey(OMSConstants.REDIS,0,"OE_SORTED_SET_"+verifier_role_id);
        redisHelper.deleteKey(OMSConstants.REDIS,0,"OE_"+verifier);
        redisHelper.deleteKey(OMSConstants.REDIS,0,"ORDER_SORTED_SET_"+city_id+"_"+l1_role_id);
        redisHelper.deleteKey(OMSConstants.REDIS,0,"OE_SORTED_SET_"+l1_role_id);
        redisHelper.deleteKey(OMSConstants.REDIS,0,"OE_"+L1);
        redisHelper.deleteKey(OMSConstants.REDIS,0,"ORDER_SORTED_SET_"+city_id+"_"+l2_role_id);
        redisHelper.deleteKey(OMSConstants.REDIS,0,"OE_SORTED_SET_"+l2_role_id);
        redisHelper.deleteKey(OMSConstants.REDIS,0,"OE_"+L2);
        redisHelper.deleteKey(OMSConstants.REDIS,0,"ORDER_SORTED_SET_"+city_id+"_"+callDE_role_id);
        redisHelper.deleteKey(OMSConstants.REDIS,0,"OE_SORTED_SET_"+callDE_role_id);
        redisHelper.deleteKey(OMSConstants.REDIS,0,"OE_"+callDE);*/
        //deleting mysql entries in assignment table
        SystemConfigProvider.getTemplate(OMSConstants.ASSIGNMENT_DB).execute("delete FROM db_assignment.assignment;");
        //Update verifier OrderCount to 0 
        SystemConfigProvider.getTemplate(OMSConstants.SERVICE).execute("update oms_orderexecutive set current_order_count=0 where id="+verifier+";");
        //Update L1 OrderCount to 0 
        SystemConfigProvider.getTemplate(OMSConstants.SERVICE).execute("update oms_orderexecutive set current_order_count=0 where id="+L1+";");
        //Update L2 OrderCount to 0 
        SystemConfigProvider.getTemplate(OMSConstants.SERVICE).execute("update oms_orderexecutive set current_order_count=0 where id="+L2+";");
        //purging TestAssignment Queue
        rmqhelper.purgeQueue("oms","testassignment");
        //placing manual order
        losHelper.createManualOrder(""+orderId, dateFormat.format(date), ""+orderId);
        Thread.sleep(10000);
        //checking whether order entry has created in redis
        String sortedListOrder = redisHelper.Zrange(OMSConstants.REDIS,0,"ORDER_SORTED_SET_"+city_id+"_"+verifier_role_id,0,1000).toString();
        System.out.println(sortedListOrder);
        //Assert.assertEquals(sortedListOrder.contains(""+orderId), true, "Order not found in redis sortedListOrder");
        //checking order is present in ordersinqueue table
        List<Map<String, Object>> postgresOrders = SystemConfigProvider.getTemplate(OMSConstants.SERVICE).queryForList("select order_id,waiting_for from oms_ordersinqueue where order_id = "+orderId+" and waiting_for = 'Verifier';");
        Assert.assertEquals(String.valueOf(postgresOrders.get(0).get("order_id")), ""+orderId,"Order not found in ordersinqueue table");
        Assert.assertEquals(String.valueOf(postgresOrders.get(0).get("waiting_for")), "Verifier","Order not found in ordersinqueue table");
        //OE login
        assignmentServiceHelper.oeLoginRMQ(verifier,verifier_role_id);
        Thread.sleep(10000);
        //checking whether OE entry has created in redis
        String sortedListOe = redisHelper.Zrange(OMSConstants.REDIS,0,"OE_SORTED_SET_"+verifier_role_id,0,1000).toString();
        //Assert.assertEquals(sortedListOe.contains(""+verifier), true,"Verfier OE not found in redis sortedListOe");
        //checking whether order is assigned to OE in mysql table
        List<Map<String, Object>> mysqlOrders = SystemConfigProvider.getTemplate(OMSConstants.ASSIGNMENT_DB).queryForList("SELECT order_id FROM db_assignment.assignment where order_id = "+ orderId  +";");
        Assert.assertEquals(String.valueOf(mysqlOrders.get(0).get("order_id")), ""+orderId,"Order not found in mysql assignment table for verification assignment");
        //Checking queue verifierAssignmentMessage published
        String verifierAssignmentMessage = rmqhelper.getMessage(OMSConstants.RMQHOSTNAME,OMSConstants.ASSIGNMENTQUEUE);
        System.out.println("Verifier Message for Assignment " +verifierAssignmentMessage);
        String verifieroeId = JsonPath.read(verifierAssignmentMessage, "$.oeId");
        Assert.assertEquals(verifieroeId, ""+verifier,"Verifier Message for Assignment to push back has some error");
        //checking order count in orderexecutive table
        List<Map<String, Object>> postgresOrderCount = SystemConfigProvider.getTemplate(OMSConstants.SERVICE).queryForList("select current_order_count from oms_orderexecutive where id = "+ verifier  +";");
        Assert.assertEquals(String.valueOf(postgresOrderCount.get(0).get("current_order_count")), "1","Current Order count for verifier after assignment has some error");
        Thread.sleep(10000);
        //OE All good proceed action from verifier
        omshelper.verifyOrder(""+orderId, "001");
        Thread.sleep(10000);
        //Checking queue verifierUnAssignmentMessage published
        String verifierUnAssignmentMessage = rmqhelper.getMessage(OMSConstants.RMQHOSTNAME,OMSConstants.ASSIGNMENTQUEUE);
        System.out.println("Verifier Message for UnAssignment " +verifierUnAssignmentMessage);
        String unassignmentReason = JsonPath.read(verifierUnAssignmentMessage, "$.unassignmentReason");
        Assert.assertEquals("default", ""+unassignmentReason,"Verifier Message for UnAssignment to push back has some error");
        //checking whether order is unassigned to OE in mysql table
        List<Map<String, Object>> mysqlUnassign = SystemConfigProvider.getTemplate(OMSConstants.ASSIGNMENT_DB).queryForList("SELECT reasonForUnassignment_id FROM db_assignment.assignment where order_id = "+ orderId  +";");
        Assert.assertEquals(String.valueOf(mysqlUnassign.get(0).get("reasonForUnassignment_id")), "1","Unassignment in mysql table has some error");
        //checking order count in orderexecutive table
        List<Map<String, Object>> postgresOrderCount1 = SystemConfigProvider.getTemplate(OMSConstants.SERVICE).queryForList("select current_order_count from oms_orderexecutive where id = "+ L1  +";");
        Assert.assertEquals(String.valueOf(postgresOrderCount1.get(0).get("current_order_count")), "0","Current order count after unassingment has some error");
        Thread.sleep(10000);
        //checking order is present in ordersinqueue table
        List<Map<String, Object>> postgresL1Orders = SystemConfigProvider.getTemplate(OMSConstants.SERVICE).queryForList("select order_id,waiting_for from oms_ordersinqueue where order_id = "+orderId+" and waiting_for = 'L1Placer';");
        Assert.assertEquals(String.valueOf(postgresL1Orders.get(0).get("order_id")), ""+orderId,"Order not found in ordersinqueue table");
        Assert.assertEquals(String.valueOf(postgresL1Orders.get(0).get("waiting_for")), "L1Placer","Order not found in ordersinqueue table");
        //OE login
        assignmentServiceHelper.oeLoginRMQ(L1,l1_role_id);
        Thread.sleep(10000);
        //checking whether order is assigned to OE in mysql table
        List<Map<String, Object>> l1mysqlOrders = SystemConfigProvider.getTemplate(OMSConstants.ASSIGNMENT_DB).queryForList("SELECT order_id FROM db_assignment.assignment where order_id = "+ orderId  +" and oe_id="+L1+";");
        Assert.assertEquals(String.valueOf(l1mysqlOrders.get(0).get("order_id")), ""+orderId,"Order not found in mysql assignment table for L1 assignment");
        //Checking queue message published
        String L1message = rmqhelper.getMessage(OMSConstants.RMQHOSTNAME,OMSConstants.ASSIGNMENTQUEUE);
        System.out.println("L1 Message is " +L1message);
        String L1oeId = JsonPath.read(L1message, "$.oeId");
        Assert.assertEquals(L1oeId, ""+L1,"L1 message push back has some error");
        //checking order count in orderexecutive table
        List<Map<String, Object>> L1postgresOrderCount = SystemConfigProvider.getTemplate(OMSConstants.SERVICE).queryForList("select current_order_count from oms_orderexecutive where id = "+L1+";");
        Assert.assertEquals(String.valueOf(L1postgresOrderCount.get(0).get("current_order_count")), "1","Current Order count for L1 after assignment has some error");
        // Assign L2 for an order
        omshelper.reassignPlacer(""+orderId);
        Thread.sleep(10000);
        //checking order is present in ordersinqueue table
        List<Map<String, Object>> postgresL2Orders = SystemConfigProvider.getTemplate(OMSConstants.SERVICE).queryForList("select order_id,waiting_for from oms_ordersinqueue where order_id = "+orderId+" and waiting_for = 'L2Placer';");
        Assert.assertEquals(String.valueOf(postgresL2Orders.get(0).get("order_id")), ""+orderId,"Order not found in ordersinqueue table");
        Assert.assertEquals(String.valueOf(postgresL2Orders.get(0).get("waiting_for")), "L2Placer","Order not found in ordersinqueue table");
        //OE login
        assignmentServiceHelper.oeLoginRMQ(L2,l2_role_id);
        Thread.sleep(10000);
        //checking whether order is assigned to OE in mysql table
        List<Map<String, Object>> l2mysqlOrders = SystemConfigProvider.getTemplate(OMSConstants.ASSIGNMENT_DB).queryForList("SELECT order_id FROM db_assignment.assignment where order_id = "+ orderId  +" and oe_id="+L2+";");
        Assert.assertEquals(String.valueOf(l2mysqlOrders.get(0).get("order_id")), ""+orderId,"Order not found in mysql assignment table for L2 assignment");
        //Checking queue message published
        String L2message = rmqhelper.getMessage(OMSConstants.RMQHOSTNAME,OMSConstants.ASSIGNMENTQUEUE);
        System.out.println("L2 Message is " +L2message);
        String L2oeId = JsonPath.read(L2message, "$.oeId");
        Assert.assertEquals(L2oeId, ""+L2,"L2 message push back has some error");
        //checking order count in orderexecutive table
        List<Map<String, Object>> L2postgresOrderCount = SystemConfigProvider.getTemplate(OMSConstants.SERVICE).queryForList("select current_order_count from oms_orderexecutive where id = "+L2+";");
        Assert.assertEquals(String.valueOf(L2postgresOrderCount.get(0).get("current_order_count")), "1","Current Order count for L1 after assignment has some error");
        // Assign CallDE for an order
        omshelper.deAssistance(""+orderId,"CNR");
        Thread.sleep(10000);
        //checking order is present in ordersinqueue table
        List<Map<String, Object>> postgresCDEOrders = SystemConfigProvider.getTemplate(OMSConstants.SERVICE).queryForList("select order_id,waiting_for from oms_ordersinqueue where order_id = "+orderId+" and waiting_for = 'CallDE_OE';");
        Assert.assertEquals(String.valueOf(postgresCDEOrders.get(0).get("order_id")), ""+orderId,"Order not found in ordersinqueue table");
        Assert.assertEquals(String.valueOf(postgresCDEOrders.get(0).get("waiting_for")), "CallDE_OE","Order not found in ordersinqueue table");
        //OE login
        assignmentServiceHelper.oeLoginRMQ(callDE,callDE_role_id);
        Thread.sleep(10000);
        //checking whether order is assigned to OE in mysql table
        List<Map<String, Object>> CDEmysqlOrders = SystemConfigProvider.getTemplate(OMSConstants.ASSIGNMENT_DB).queryForList("SELECT order_id FROM db_assignment.assignment where order_id = "+ orderId  +" and oe_id="+callDE+";");
        Assert.assertEquals(String.valueOf(CDEmysqlOrders.get(0).get("order_id")), ""+orderId,"Order not found in mysql assignment table for call_de assignment");
        //Checking queue message published
        String callDEmessage = rmqhelper.getMessage(OMSConstants.RMQHOSTNAME,OMSConstants.ASSIGNMENTQUEUE);
        System.out.println("callDE Message is " +callDEmessage);
        String callDEoeId = JsonPath.read(callDEmessage, "$.oeId");
        Assert.assertEquals(callDEoeId, ""+callDE,"callDe message push back has some error");
        //checking order count in orderexecutive table
        List<Map<String, Object>> CDEpostgresOrderCount = SystemConfigProvider.getTemplate(OMSConstants.SERVICE).queryForList("select current_order_count from oms_orderexecutive where id = "+callDE+";");
        Assert.assertEquals(String.valueOf(CDEpostgresOrderCount.get(0).get("current_order_count")), "1","Current Order count for callDe after assignment has some error");
        //deAssistance close request
        omshelper.deAssistanceCloseRequest(""+orderId,"SPOKE_TO_CUSTOMER");
        
    }
    
    @Test(groups = {"sanity","regression"}, dataProvider = "stopDutyValidation", description="oe stopDuty validation", enabled=false)
    public void testStopDuty(int oe_id,int role_id, int city_id,int new_state_id) throws IOException, InterruptedException, TimeoutException {
    	
        //OE login
        assignmentServiceHelper.oeLoginRMQ(oe_id,role_id);
        Thread.sleep(5000);
        //checking start_duty true and enabled in orderexecutive table
        List<Map<String, Object>> postgreslogin = SystemConfigProvider.getTemplate(OMSConstants.SERVICE).queryForList("select start_duty,enabled from oms_orderexecutive where id = "+ oe_id  +";");
        Assert.assertEquals(String.valueOf(postgreslogin.get(0).get("start_duty")), "1");
        Assert.assertEquals(String.valueOf(postgreslogin.get(0).get("enabled")), "1");
        //OE Stopduty
        assignmentServiceHelper.oeLogoutRMQ(oe_id,role_id,new_state_id);
        Thread.sleep(3000);
        //checking start_duty false in orderexecutive table
        List<Map<String, Object>> postgresstopduty = SystemConfigProvider.getTemplate(OMSConstants.SERVICE).queryForList("select start_duty from oms_orderexecutive where id = "+ oe_id  +";");
        Assert.assertEquals(String.valueOf(postgresstopduty.get(0).get("start_duty")), "0");
     
    } 
    
    @Test(groups = {"sanity","regression"}, dataProvider = "orderStarvation", description="order starvation validation")
    public void testOrderStarvation(int oe_id,int role_id, int city_id) throws IOException, InterruptedException, TimeoutException {
    	
    	Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long orderId = Instant.now().getEpochSecond();
        //deleting redis entry for order and oe
        omshelper.redisFlushAll(OMSConstants.REDIS);
        SystemConfigProvider.getTemplate(OMSConstants.ASSIGNMENT_DB).execute("delete FROM ff_assignment.assignment;");
        //Update OrderCount to 0
        SystemConfigProvider.getTemplate(OMSConstants.SERVICE).execute("update oms_orderexecutive set current_order_count=0 where id="+oe_id+";");
        //purging TestAssignment Queue
        rmqhelper.purgeQueue("oms","testassignment");
        //placing manual order
        losHelper.createManualOrder(""+orderId, dateFormat.format(date), ""+orderId);
        Thread.sleep(10000);
        //checking whether order entry has created in redis
        String sortedListOrder = redisHelper.Zrange(OMSConstants.REDIS,0,"ORDER_SORTED_SET_"+city_id+"_"+role_id,0,1000).toString();
        System.out.println(sortedListOrder);
        //Assert.assertEquals(sortedListOrder.contains(""+orderId), true);
        //checking order is present in ordersinqueue table
        List<Map<String, Object>> postgresOrders = SystemConfigProvider.getTemplate(OMSConstants.SERVICE).queryForList("select order_id,waiting_for,assignment_source from oms_ordersinqueue where order_id = "+orderId+";");
        Assert.assertEquals(String.valueOf(postgresOrders.get(0).get("order_id")), ""+orderId);
        Assert.assertEquals(String.valueOf(postgresOrders.get(0).get("waiting_for")), "Verifier");
        Assert.assertEquals(String.valueOf(postgresOrders.get(0).get("assignment_source")), "new");
        //Order Starvation ttl
        Thread.sleep(10000);
        //checking order is present in ordersinqueue table
        List<Map<String, Object>> postgresOrdersAfterStarvation = SystemConfigProvider.getTemplate(OMSConstants.SERVICE).queryForList("select order_id,waiting_for,assignment_source from oms_ordersinqueue where order_id = "+orderId+";");
        Assert.assertEquals(String.valueOf(postgresOrdersAfterStarvation.get(0).get("order_id")), ""+orderId);
        Assert.assertEquals(String.valueOf(postgresOrdersAfterStarvation.get(0).get("waiting_for")), "Verifier");
        Assert.assertEquals(String.valueOf(postgresOrdersAfterStarvation.get(0).get("assignment_source")), "old");
        //checking whether order entry has removed in redis
        String sortedListOrderAfterStarvation = redisHelper.Zrange(OMSConstants.REDIS,0,"ORDER_SORTED_SET_"+city_id+"_"+role_id,0,1000).toString();
        System.out.println(sortedListOrderAfterStarvation);
        //Assert.assertEquals(sortedListOrderAfterStarvation.contains(""+orderId), false);
    }
    
}