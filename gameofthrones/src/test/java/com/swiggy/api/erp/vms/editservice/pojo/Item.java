package com.swiggy.api.erp.vms.editservice.pojo;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"item_id",
"addons",
"variants"
})
public class Item {

@JsonProperty("item_id")
private long item_id;
@JsonProperty("addons")
private List<Addon> addons = null;
@JsonProperty("variants")
private List<Variant> variants = null;

/**
* No args constructor for use in serialization
* 
*/
public Item() {
}

/**
* 
* @param item_id
* @param addons
* @param variants
*/
public Item(long item_id, List<Addon> addons, List<Variant> variants) {
super();
this.item_id = item_id;
this.addons = addons;
this.variants = variants;
}

@JsonProperty("item_id")
public long getItem_id() {
return item_id;
}

@JsonProperty("item_id")
public void setItem_id(long item_id) {
this.item_id = item_id;
}

public Item withItem_id(long item_id) {
this.item_id = item_id;
return this;
}

@JsonProperty("addons")
public List<Addon> getAddons() {
return addons;
}

@JsonProperty("addons")
public void setAddons(List<Addon> addons) {
this.addons = addons;
}

public Item withAddons(List<Addon> addons) {
this.addons = addons;
return this;
}

@JsonProperty("variants")
public List<Variant> getVariants() {
return variants;
}

@JsonProperty("variants")
public void setVariants(List<Variant> variants) {
this.variants = variants;
}

public Item withVariants(List<Variant> variants) {
this.variants = variants;
return this;
}

@Override
public String toString() {
return new ToStringBuilder(this).append("item_id", item_id).append("addons", addons).append("variants", variants).toString();
}

@Override
public int hashCode() {
return new HashCodeBuilder().append(item_id).append(addons).append(variants).toHashCode();
}

@Override
public boolean equals(Object other) {
if (other == this) {
return true;
}
if ((other instanceof Item) == false) {
return false;
}
Item rhs = ((Item) other);
return new EqualsBuilder().append(item_id, rhs.item_id).append(addons, rhs.addons).append(variants, rhs.variants).isEquals();
}

}