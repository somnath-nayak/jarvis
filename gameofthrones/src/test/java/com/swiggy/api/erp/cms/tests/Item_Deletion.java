package com.swiggy.api.erp.cms.tests;
import java.io.IOException;

import org.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.constants.CmsConstants;
import com.swiggy.api.erp.cms.dp.Critical_API_dataProvider;
import com.swiggy.api.erp.cms.helper.Grasshopper_Helper;
import framework.gameofthrones.JonSnow.Processor;


 /*
 *   Item-Deletion:   Upload a file and get the uploaded file-details 
 */

public class Item_Deletion extends Critical_API_dataProvider {
	
	Grasshopper_Helper obj =  new Grasshopper_Helper();
	String generatedFileId="";
	
	
	@Test(priority=0, groups={"Sanity_TC", "Smoke_TC", "Regression_TC"}, 
			description="Upload a file and get the generated FILE-ID")
	public void newFileUpload()
			throws JSONException, InterruptedException
	{
		  Processor p1 = obj.uploadFile();  
		  String response = p1.ResponseValidator.GetBodyAsText();
		  
		  System.out.println("Response data: "+response);
		  int statusCode = JsonPath.read(response, "$.status");
		  Assert.assertEquals(statusCode, 1);
		  
		  String statusMessage = JsonPath.read(response, "$.code");
		  Assert.assertEquals(statusMessage, "Success");
		  
		  String message =  JsonPath.read(response, "$.message");
		  Assert.assertEquals(message, "File Uploaded To S3 And Message Published To Queue");
		  
		  int fileId =  JsonPath.read(response, "$.data");
		  generatedFileId = Integer.toString(fileId);
		  System.out.println("FILE-ID generated : "+ generatedFileId);
		  Assert.assertNotNull(fileId);	  
	}
	
	// Validate newly created FILE details from DB
			 @Test(priority=1, groups={"Sanity_TC", "Smoke_TC", "Regression_TC"}, 
						description="Validate the created File details from DB")
			 public void validateCreatedFileDetailsInDB()
			 {
				 obj.getFileDetailsFromDB(generatedFileId);
			 }

			 
	@Test(priority=2, groups={"Smoke_TC", "Regression_TC"}, 
			description="Upload a file without passing the FILE_PATH")
	public void newFileUpload_WithoutFilePath()
			throws JSONException, InterruptedException
	{
		  Processor p1 = obj.uploadFileWithoutFilePath();
		  String response = p1.ResponseValidator.GetBodyAsText();
		  
		  System.out.println("Response data: "+response);
		  int statusCode = JsonPath.read(response, "$.status");
		  Assert.assertEquals(statusCode, 400);
		  
		  String error = JsonPath.read(response, "$.error");
		  Assert.assertEquals(error, "Bad Request");
		  
		  String message =  JsonPath.read(response, "$.message");
		  Assert.assertEquals(message, "Required request part 'file' is not present");
		 
	}
	
	@Test(priority=3, groups={"Smoke_TC", "Regression_TC"}, 
			description="Upload a file without passing the ENTITY_TYPE")
	public void newFileUpload_WithoutEntityType()
			throws JSONException, InterruptedException
	{
		  Processor p1 = obj.uploadFileWithoutEntityType();
		  String response = p1.ResponseValidator.GetBodyAsText();
		  
		  System.out.println("Response data: "+response);
		  int statusCode = JsonPath.read(response, "$.status");
		  Assert.assertEquals(statusCode, 400);
		  
		  String error = JsonPath.read(response, "$.error");
		  Assert.assertEquals(error, "Bad Request");
		  
		  String message =  JsonPath.read(response, "$.message");
		  Assert.assertEquals(message, "Required String parameter 'entityType' is not present");	 
	}

	@Test(priority=4, groups={"Smoke_TC", "Regression_TC"}, 
			description="Upload a file without passing the USER_ID")
	public void newFileUpload_WithoutUserId()
			throws JSONException, InterruptedException
	{
		  Processor p1 = obj.uploadFileWithoutUserId();
		  String response = p1.ResponseValidator.GetBodyAsText();
		  
		  System.out.println("Response data: "+response);
		  int statusCode = JsonPath.read(response, "$.status");
		  Assert.assertEquals(statusCode, 400);
		  
		  String error = JsonPath.read(response, "$.error");
		  Assert.assertEquals(error, "Bad Request");
		  
		  String message =  JsonPath.read(response, "$.message");
		  Assert.assertEquals(message, "Required String parameter 'userId' is not present");
	}
	
	@Test(priority=5, groups={"Smoke_TC", "Regression_TC"}, 
			description="Upload a file without passing the FILE_NAME")
	public void newFileUpload_WithoutFileName()
			throws JSONException, InterruptedException
	{
		  Processor p1 = obj.uploadFileWithoutFileName();
		  String response = p1.ResponseValidator.GetBodyAsText();
		  
		  System.out.println("Response data: "+response);
		  int statusCode = JsonPath.read(response, "$.status");
		  Assert.assertEquals(statusCode, 400);
		  
		  String error = JsonPath.read(response, "$.error");
		  Assert.assertEquals(error, "Bad Request");
		  
		  String message =  JsonPath.read(response, "$.message");
		  Assert.assertEquals(message, "Required String parameter 'fileName' is not present");
	}
	
	@Test(priority=6, groups={"Smoke_TC", "Regression_TC"}, 
			description="Upload a file without passing the USER_NAME")
	public void newFileUpload_WithoutUserName()
			throws JSONException, InterruptedException
	{
		  Processor p1 = obj.uploadFileWithoutUserName();
		  String response = p1.ResponseValidator.GetBodyAsText();
		  
		  System.out.println("Response data: "+response);
		  int statusCode = JsonPath.read(response, "$.status");
		  Assert.assertEquals(statusCode, 400);
		  
		  String error = JsonPath.read(response, "$.error");
		  Assert.assertEquals(error, "Bad Request");
		  
		  String message =  JsonPath.read(response, "$.message");
		  Assert.assertEquals(message, "Required String parameter 'userName' is not present");	 
	}
	
	@Test(priority=7, groups={"Smoke_TC", "Regression_TC"}, 
			description="Upload a file without passing the SOURCE")
	public void newFileUpload_WithoutSource()
			throws JSONException, InterruptedException
	{
		  Processor p1 = obj.uploadFileWithoutSource();
		  String response = p1.ResponseValidator.GetBodyAsText();
		  
		  System.out.println("Response data: "+response);
		  int statusCode = JsonPath.read(response, "$.status");
		  Assert.assertEquals(statusCode, 400);
		  
		  String error = JsonPath.read(response, "$.error");
		  Assert.assertEquals(error, "Bad Request");
		  
		  String message =  JsonPath.read(response, "$.message");
		  Assert.assertEquals(message, "Required String parameter 'source' is not present");
		 
	}
	
	
	 
	// GET FILE DETAILS BY FILE-ID*************************************************************************************
	@Test(priority=8, groups={"Sanity_TC", "Smoke_TC", "Regression_TC"}, 
			description="Get the file details using valid FILE_ID")
	public void getFileDetails_validFileId()
			throws JSONException, InterruptedException
	{
		  String VALID_FILE_ID=CmsConstants.VALID_FILE_ID;
		  Processor p1 = obj.getFileByFileId(VALID_FILE_ID);
		  String response = p1.ResponseValidator.GetBodyAsText();
		  
		  System.out.println("Response data: "+response);
		  int statusCode = JsonPath.read(response, "$.status");
		  Assert.assertEquals(statusCode, 1);
		  
		  String statusMessage = JsonPath.read(response, "$.code");
		  Assert.assertEquals(statusMessage, "Success");
		  
		  String message =  JsonPath.read(response, "$.message");
		  Assert.assertEquals(message, "Found Progress Report By File Id");
		  
		  String fileId =  JsonPath.read(response, "$.data.file_id").toString();
		  Assert.assertEquals(fileId, CmsConstants.VALID_FILE_ID);
		  
		  String fileStatus =  JsonPath.read(response, "$.data.status");
		  Assert.assertTrue(fileStatus.equals("FINISHED") || fileStatus.equals("GRASSHOPPER_SUCCESS") || 
				  			 fileStatus.equals("FILE_SENT_FOR_PARSING"));
 
		  String fileName =  JsonPath.read(response, "$.data.file_name"); 
		  Assert.assertNotNull(fileName);	
		  
		  double progress =  Double.parseDouble(JsonPath.read(response, "$.data.progress").toString()); 
		  Assert.assertEquals(progress, 100.0);
		  
		  int failedRows = JsonPath.read(response, "$.data.failed_count"); 
		  int passedRows = JsonPath.read(response, "$.data.passed_count"); 
		  
		  Assert.assertTrue(failedRows>=0);
		  Assert.assertTrue(passedRows>=0);
		  
		  String uploaded_file_s3_url = JsonPath.read(response, "$.data.uploaded_file_s3_url").toString(); 
		  String response_file_s3_url = JsonPath.read(response, "$.data.response_file_s3_url").toString(); 
		  String created_at = JsonPath.read(response, "$.data.created_at").toString(); 
		  String updated_at = JsonPath.read(response, "$.data.updated_at").toString(); 
				  
		  
		  Assert.assertNotNull(uploaded_file_s3_url);
		  Assert.assertNotNull(response_file_s3_url);
		  Assert.assertNotNull(created_at);
		  Assert.assertNotNull(updated_at);
		  
	}
	
	
	@Test(priority=9, groups={"Smoke_TC", "Regression_TC"}, 
			description="Get the file details using invalid FILE_ID")
	public void getFileDetails_invalidFileId()
			throws JSONException, InterruptedException
	{
		  String INVALID_FILE_ID=CmsConstants.INVALID_FILE_ID;
		  Processor p1 = obj.getFileByFileId(INVALID_FILE_ID);
		  String response = p1.ResponseValidator.GetBodyAsText();
		  
		  System.out.println("Response data: "+response);
		  int statusCode = JsonPath.read(response, "$.status");
		  Assert.assertEquals(statusCode, 400);
		  
		  String errorMessage = JsonPath.read(response, "$.error").toString();
		  Assert.assertEquals(errorMessage, "Bad Request");
	}
	
	@Test(priority=10, groups={"Regression_TC"}, 
			description="Get the file details using non-existent or deleted FILE_ID")
	public void getFileDetails_nonExistentFileId()
			throws JSONException, InterruptedException
	{
		  String NONEXIST_FILE_ID=CmsConstants.NONEXIST_FILE_ID;
		  Processor p1 = obj.getFileByFileId(NONEXIST_FILE_ID);
		  String response = p1.ResponseValidator.GetBodyAsText();
		  
		  System.out.println("Response data: "+response);
		  int statusCode = JsonPath.read(response, "$.status");
		  Assert.assertEquals(statusCode, 0);
		  
		  String codeMessage = JsonPath.read(response, "$.code").toString();
		  Assert.assertEquals(codeMessage,"Fail");
		  
		  String errorMessage = JsonPath.read(response, "$.message").toString();
		  Assert.assertEquals(errorMessage, "Exception Occurred in Calculating Progress Report By File Id : "+NONEXIST_FILE_ID+" Exception is : ");
	}
	

	@Test(priority=11, groups={"Smoke_TC", "Regression_TC"}, 
			description="Get the file details using blank FILE_ID")
	public void getFileDetails_blankFileId()
			throws JSONException, InterruptedException
	{
		  Processor p1 = obj.getFileByFileId("");
		  String response = p1.ResponseValidator.GetBodyAsText();
		  
		  System.out.println("Response data: "+response);
		  int statusCode = JsonPath.read(response, "$.status");
		  Assert.assertEquals(statusCode, 0);
		  
		  String codeMessage = JsonPath.read(response, "$.code").toString();
		  Assert.assertEquals(codeMessage,"Fail");
		  
		  String errorMessage = JsonPath.read(response, "$.message").toString();
		  Assert.assertEquals(errorMessage, "Exception Occurred in Calculating Progress Report By File Id : null Exception is : ");
	}
	
	
	@Test(priority=12, groups={"Regression_TC"}, 
			description="Get the file details using multiple FILE_IDS separated by commas")
	public void getFileDetails_multipleFileIds()
			throws JSONException, InterruptedException
	{
		 String ids= "283,284,285";
		  Processor p1 = obj.getFileByFileId(ids);
		  String response = p1.ResponseValidator.GetBodyAsText();
		  
		  System.out.println("Response data: "+response);
		  int statusCode = JsonPath.read(response, "$.status");
		  Assert.assertEquals(statusCode, 400);
		  
		  String errorMessage = JsonPath.read(response, "$.error").toString();
		  Assert.assertEquals(errorMessage, "Bad Request");
	}
	
	@Test(priority=13, groups={"Regression_TC"}, 
			description="Get the file details using multiple FILE_IDS separated by commas in Array Format")
	public void getFileDetails_multipleFileIdsArray()
			throws JSONException, InterruptedException
	{
		 String ids= "[283,284,285]";
		  Processor p1 = obj.getFileByFileId(ids);
		  String response = p1.ResponseValidator.GetBodyAsText();
		  
		  System.out.println("Response data: "+response);
		  int statusCode = JsonPath.read(response, "$.status");
		  Assert.assertEquals(statusCode, 400);
		  
		  String errorMessage = JsonPath.read(response, "$.error").toString();
		  Assert.assertEquals(errorMessage, "Bad Request");
	}
	
//*********************************************************************************************************************************
	
	
	// GET FILE DETAILS BY USER-ID************************************************************************************
	
	@Test(priority=14, groups={"Sanity_TC", "Smoke_TC", "Regression_TC"}, 
			description="Get the file details using valid FILE_ID")
	public void getFileDetails_validUserId()
			throws JSONException, InterruptedException
	{
		  String VALID_USER_ID=CmsConstants.VALID_USER_ID;
		  Processor p1 = obj.getFileByUserId(VALID_USER_ID);
		  String response = p1.ResponseValidator.GetBodyAsText();
		  
		  System.out.println("Response data: "+response);
		  int statusCode = JsonPath.read(response, "$.status");
		  Assert.assertEquals(statusCode, 1);
		  
		  String errorMessage = JsonPath.read(response, "$.message").toString();
		  Assert.assertEquals(errorMessage, "Found Progress Report By User Id");
		 
		  String codeMessage = JsonPath.read(response, "$.code").toString();
		  Assert.assertEquals(codeMessage, "Success");
		  
		  String data = JsonPath.read(response, "$.data").toString();
		  Assert.assertTrue(data.length()>2);	  
	}
	
	
	@Test(priority=15, groups={"Smoke_TC", "Regression_TC"}, 
			description="Get the file details using invalid USER_ID")
	public void getFileDetails_invalidUserId()
			throws JSONException, InterruptedException
	{
		  String INVALID_USER_ID=CmsConstants.INVALID_USER_ID;
		  Processor p1 = obj.getFileByUserId(INVALID_USER_ID);
		  String response = p1.ResponseValidator.GetBodyAsText();
		  
		  System.out.println("Response data: "+response);
		  int statusCode = JsonPath.read(response, "$.status");
		  Assert.assertEquals(statusCode, 1);
		  
		  String errorMessage = JsonPath.read(response, "$.message").toString();
		  Assert.assertEquals(errorMessage, "Found Progress Report By User Id");
		 
		  String codeMessage = JsonPath.read(response, "$.code").toString();
		  Assert.assertEquals(codeMessage, "Success");
		  
		  String data = JsonPath.read(response, "$.data").toString();
		  Assert.assertTrue(data.length()<=2);
		  
	}
	
	@Test(priority=16, groups={"Regression_TC"}, 
			description="Get the file details using non-existent or deleted FILE_ID")
	public void getFileDetails_nonExistentUserId()
			throws JSONException, InterruptedException
	{
		  String NONEXIST_USER_ID=CmsConstants.NONEXIST_USER_ID;
		  Processor p1 = obj.getFileByUserId(NONEXIST_USER_ID);
		  String response = p1.ResponseValidator.GetBodyAsText();
		  
		  System.out.println("Response data: "+response);
		  int statusCode = JsonPath.read(response, "$.status");
		  Assert.assertEquals(statusCode, 1);
		  
		  String errorMessage = JsonPath.read(response, "$.message").toString();
		  Assert.assertEquals(errorMessage, "Found Progress Report By User Id");
		 
		  String codeMessage = JsonPath.read(response, "$.code").toString();
		  Assert.assertEquals(codeMessage, "Success");
		  
		  String data = JsonPath.read(response, "$.data").toString();
		  Assert.assertTrue(data.length()<=2);
	}
	

	@Test(priority=17, groups={"Regression_TC"}, 
			description="Get the file details using blank USER_ID")
	public void getFileDetails_blankUserId()
			throws JSONException, InterruptedException
	{
		  Processor p1 = obj.getFileByUserId("");
		  String response = p1.ResponseValidator.GetBodyAsText();
		  
		  System.out.println("Response data: "+response);
		  int statusCode = JsonPath.read(response, "$.status");
		  Assert.assertEquals(statusCode, 1);
		  
		  String errorMessage = JsonPath.read(response, "$.message").toString();
		  Assert.assertEquals(errorMessage, "Found Progress Report By User Id");
		 
		  String codeMessage = JsonPath.read(response, "$.code").toString();
		  Assert.assertEquals(codeMessage, "Success");
		  
		  String data = JsonPath.read(response, "$.data").toString();
		  Assert.assertTrue(data.length()<=2);
	}
	
	
	@Test(priority=18, groups={ "Smoke_TC", "Regression_TC"}, 
			description="Get the file details using multiple USER_IDS separated by commas")
	public void getFileDetails_multipleUserIds()
			throws JSONException, InterruptedException
	{
		  String ids= "1233,1354";
		  Processor p1 = obj.getFileByUserId(ids);
		  String response = p1.ResponseValidator.GetBodyAsText();
		  
		  System.out.println("Response data: "+response);
		  int statusCode = JsonPath.read(response, "$.status");
		  Assert.assertEquals(statusCode, 1);
		  
		  String errorMessage = JsonPath.read(response, "$.message").toString();
		  Assert.assertEquals(errorMessage, "Found Progress Report By User Id");
		 
		  String codeMessage = JsonPath.read(response, "$.code").toString();
		  Assert.assertEquals(codeMessage, "Success");
		  
		  String data = JsonPath.read(response, "$.data").toString();
		  Assert.assertTrue(data.length()<=2);
	}
	
	
	@Test(priority=19, groups={"Smoke_TC", "Regression_TC"}, 
			description="Get the file details using multiple USER_IDS separated by commas in Array Format")
	public void getFileDetails_multipleUserIdsArray()
			throws JSONException, InterruptedException
	{
		  String ids= "[1233,1354]";
		  Processor p1 = obj.getFileByUserId(ids);
		  String response = p1.ResponseValidator.GetBodyAsText();
		  
		  System.out.println("Response data: "+response);
		  int statusCode = JsonPath.read(response, "$.status");
		  Assert.assertEquals(statusCode, 1);
		  
		  String errorMessage = JsonPath.read(response, "$.message").toString();
		  Assert.assertEquals(errorMessage, "Found Progress Report By User Id");
		 
		  String codeMessage = JsonPath.read(response, "$.code").toString();
		  Assert.assertEquals(codeMessage, "Success");
		  
		  String data = JsonPath.read(response, "$.data").toString();
		  Assert.assertTrue(data.length()<=2);
	}
	
	
		// Delete newly created FILE details from DB
	@Test(priority=20, groups={"Sanity_TC", "Smoke_TC", "Regression_TC"}, 
							description="Validate File details from DB")
		public void deleteFileDetailsFromDB()
			{
				obj.deleteFileDetailsFromDB(generatedFileId);
			}
	
	//****************************************************************************************************************
	
	
	@Test(priority=21, groups={"Sanity_TC", "Smoke_TC", "Regression_TC"}, 
			description="validate the newly created Categories")
	public void validateCatCreated()
			throws JSONException, InterruptedException, IOException
	{
			obj.validateCategoriesfromDB();
			System.out.println("Category created in DB and validated...");
			
	}
	
	@Test(priority=22, groups={"Sanity_TC", "Smoke_TC", "Regression_TC"}, 
			description="validate the newly created Subcategories")
	public void validateSubCatCreated()
			throws JSONException, InterruptedException, IOException
	{
			obj.validateSubCategoriesfromDB();
			System.out.println("Sub-Category created in DB and validated...");
			
	}
	
	@Test(priority=23, groups={"Sanity_TC", "Smoke_TC", "Regression_TC"}, 
			description="validate the newly created Item Menu Map")
	public void validateItemMenuMapCreated()
			throws JSONException, InterruptedException, IOException
	{
			obj.validateItemMenuMapfromDB();
			System.out.println("Item Menu Map created in DB and validated...");		
	}
	
	@Test(priority=24, groups={"Sanity_TC", "Smoke_TC", "Regression_TC"}, 
			description="validate the newly created Items")
	public void validateAllItemsCreated()
			throws JSONException, InterruptedException, IOException
	{
			obj.validateItemsfromDB();
			System.out.println("Items created in DB and validated...");		
	}
	
	@Test(priority=25, groups={"Sanity_TC", "Smoke_TC", "Regression_TC"}, 
			description="Delete the newly created Items")
	public void deleteAllCreatedItemsFromDB()
			throws JSONException, InterruptedException, IOException
	{
			obj.deleteAllItemsFromDB();
			System.out.println("Items deleted from DB and validated...");		
	}
	
	@Test(priority=26, groups={"Sanity_TC", "Smoke_TC", "Regression_TC"}, 
			description="Delete the newly created Item Menu Map")
	public void deleteItemMenuMapFromDB()
			throws JSONException, InterruptedException, IOException
	{
			obj.deleteAllItemMenuMapFromDB();
			System.out.println("Item menu maps deleted from DB and validated...");		
	}
	
	
	@Test(priority=27, groups={"Sanity_TC", "Smoke_TC", "Regression_TC"}, 
			description="Delete the newly created Subcategories")
	public void deleteAllSubcategoriesFromDB()
			throws JSONException, InterruptedException, IOException
	{
			obj.deleteAllSubcatsFromDB();
			System.out.println("Subcategories deleted from DB and validated...");		
	}
	
	@Test(priority=28, groups={"Sanity_TC", "Smoke_TC", "Regression_TC"}, 
			description="Delete the newly created Categories")
	public void deleteAllCategoriesFromDB()
			throws JSONException, InterruptedException, IOException
	{
			obj.deleteAllCatsFromDB();
			System.out.println("Categories deleted from DB and validated...");		
	}
	
}
