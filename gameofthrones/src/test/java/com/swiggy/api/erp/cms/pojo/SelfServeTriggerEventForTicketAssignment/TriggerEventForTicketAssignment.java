package com.swiggy.api.erp.cms.pojo.SelfServeTriggerEventForTicketAssignment;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.erp.cms.pojo.SelfServeTriggerEventForTicketAssignment
 **/

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "metadata",
        "tickets"
})
public class TriggerEventForTicketAssignment {

    @JsonProperty("metadata")
    private Metadata metadata;
    @JsonProperty("tickets")
    private List<Ticket> tickets = null;

    @JsonProperty("metadata")
    public Metadata getMetadata() {
        return metadata;
    }

    @JsonProperty("metadata")
    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    @JsonProperty("tickets")
    public List<Ticket> getTickets() {
        return tickets;
    }

    @JsonProperty("tickets")
    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }

    private void setDefaultValues( List<Ticket> ticket) {
        //List<Ticket> ticket = new ArrayList<>();
        Metadata metadata = new Metadata();
        if (this.getMetadata() == null)
            this.setMetadata(metadata.build());
        if (this.getTickets() == null)
            this.setTickets(ticket);
    }

    public TriggerEventForTicketAssignment build(List<Ticket> ticket) {
        setDefaultValues(ticket);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("metadata", metadata).append("tickets", tickets).toString();
    }

}
