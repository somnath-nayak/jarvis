package com.swiggy.api.erp.delivery.constants;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;
import framework.gameofthrones.Aegon.SystemConfigProvider;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface ClusterConstants 
{
	String defaultItem_refid = "90";
	String ref_id = getRefIfd(defaultItem_refid);
	String item_lat = "12.929928";
	String item_lng = "77.629055";
	String area_id = "4";//"1";
	String zone_id = "4";//"1";
	String globalId = "0";
	String cityId = "1";
	String cgName = "ClusterService-Test";
	String exclusionCG = "ExclusionTest";
	String cgForPriorityTest = "PriorityTest";
	String[] cgForTimeSlotCheck = {"Morning-Test", "Evening-Test"};
	String nullTimeSlotCG = "NullTimeSlotCg";
	String[] clustersName = {"Kor-1stRestCluster", "Kor-2ndRestCluster", "HSR-custCluster"};
	String multiClusterCG = "MultiCluster-ConnectionGroup";
	String[] overlappingClusterNames = {"innerCluster", "midCluster", "outerCluster"};
	String overlappingCG = "overlappingClusterConnectionGroup";
	String[] serviceabilityCheckClusterNames = {"Kor-RestCluster", "HSR-CustCluster"};
	String serviceabilityCGName = "Test-con-group";
	
	String selfDelItemLat = "12.94044042";
	String selfDelItemLng = "77.62513551999996";
	String selfDelItem_ref_id = "29786";
	String partnerId = getPartnerId();
	String serviceable = "true";
	String fixedSla = "25";
	String deliveryPartnerActive = "true";
	String selfDeliveryOverriden = "false";
	String deliveryPartnerFixedSla = "30";
	
	String from_cluster_id = "1";
	String last_mile = "4000";
	String to_cluster_id = "1";
	String cluster_id = "1";
	String max_sla = "70";
	String openBF = "1.5";
	String closeBF = "3";
	String time_slot = "0000-2359";
	String item_count = "1";
	String currentBF = "2";
	String sla_extra_time = "4";
	String is_cluster_zone_open = "true";
	String rule_group_type = "demand";
	String connection_type = "inclusion";
	String is_connection_enabled = "true";
	String connection_position = "0";
	String entity_type="demand";
	String customer_cluster_tag = "customer";
	String restaurant_cluster_tag = "restaurant";
	
	String sla_range_width = getSlaRangeWidth();
	String item_count_sla_beefUp = getItemCountSlaBeefUp();
	String cluster_type = "city";
	String clusterType = "zone";
	String degradedState = "normal";
	
	static String getRefIfd(String item_refid)
	{
		
		while(ifItemExists(item_refid))
		{
			item_refid = String.valueOf(Integer.sum(Integer.parseInt(item_refid), 1000));
		}
		return item_refid;
	}
	
	static boolean ifItemExists(String ref_Id)
	{
		List<Map<String, Object>> query = SystemConfigProvider.getTemplate("deliveryclusterdb")
										  .queryForList("select * from items where reference_id='" + ref_Id + "'");
		boolean flag = query.size() != 0;
		return flag;
	}
	
	static String getSlaRangeWidth()
	{
		Map<String, Object> map = SystemConfigProvider.getTemplate("deliveryclusterdb").queryForMap("select attributes from clusters where id='" + cityId + "'");
		ObjectMapper mapper = new ObjectMapper();
		String slaRangeWidth = "";
		String json = "";
		try {
			
				try {
					json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(map.get("attributes")).replace("\\", "").replace("\"{", "{");
				} catch (JsonProcessingException e) {
					e.printStackTrace();
				}
			
			slaRangeWidth = JsonPath.read(json, "$.slaRangeWidth").toString();
			
		} catch (PathNotFoundException e) {
			e.printStackTrace();
			try {
				map = SystemConfigProvider.getTemplate("deliveryclusterdb").queryForMap("select attributes from clusters where id='" + globalId + "'");
				try {
					json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(map.get("attributes")).replace("\\", "").replace("\"{", "{");
				} catch (JsonProcessingException e1) {
					e1.printStackTrace();
				}
				slaRangeWidth = JsonPath.read(json, "$.slaRangeWidth").toString();
			}
			catch (PathNotFoundException e1) {
				e1.printStackTrace();
			}
		}
		return slaRangeWidth;
		
	}
	
	
	static String getPartnerId()
	{
		Map<String, Object> map = SystemConfigProvider.getTemplate("deliveryclusterdb").queryForMap("select attributes from clusters where id='" + globalId + "'");
		ObjectMapper mapper = new ObjectMapper();
		String[] partnerId = new String[5];
		String json = "";
		try {
			
				try {
					json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(map.get("attributes")).replace("\\", "").replace("\"{", "{");
				} catch (JsonProcessingException e) {
					e.printStackTrace();
				}
			
				partnerId = JsonPath.read(json, "$.selfDeliveryPartners").toString().split(",");
			
		} catch (PathNotFoundException e) {
			e.printStackTrace();
			try {
				map = SystemConfigProvider.getTemplate("deliveryclusterdb").queryForMap("select attributes from clusters where id='" + cityId + "'");
				try {
					json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(map.get("attributes")).replace("\\", "").replace("\"{", "{");
				} catch (JsonProcessingException e1) {
					e1.printStackTrace();
				}
				partnerId = JsonPath.read(json, "$.selfDeliveryPartners").toString().split(",");
			}
			catch (PathNotFoundException e1) {
				e1.printStackTrace();
			}
		}
		return partnerId[0];
		
	}
	
	
	static String getItemCountSlaBeefUp()
	{
		Map<String, Object> map = SystemConfigProvider.getTemplate("deliveryclusterdb").queryForMap("select attributes from clusters where id='" + globalId + "'");
		ObjectMapper mapper = new ObjectMapper();
		String itemCountBeefUp = "";
		String json = "";
		try {
			
				try {
					json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(map.get("attributes")).replace("\\", "").replace("\"{", "{");
				} catch (JsonProcessingException e) {
					e.printStackTrace();
				}
			
				itemCountBeefUp = JsonPath.read(json, "$.itemCountSlaBeefUp").toString();
			
		} catch (PathNotFoundException e) {
			e.printStackTrace();
			try {
				map = SystemConfigProvider.getTemplate("deliveryclusterdb").queryForMap("select attributes from clusters where id='" + cityId + "'");
				try {
					json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(map.get("attributes")).replace("\\", "").replace("\"{", "{");
				} catch (JsonProcessingException e1) {
					e1.printStackTrace();
				}
				itemCountBeefUp = JsonPath.read(json, "$.itemCountSlaBeefUp").toString();
			}
			catch (PathNotFoundException e1) {
				e1.printStackTrace();
			}
		}
		return itemCountBeefUp;
	}
	
	
	static HashMap<String, String> multiHeaders(){
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("content-type", "application/json");
		requestheaders.put("Cache-Control", "no-cache");
		requestheaders.put("authorization", "Basic R0dZU1dJOjIwMTVTVyFHR1k=");
		requestheaders.put("postman-token", "f9bd02d4-7417-9a7c-ff9b-50490c920c9c");
		return requestheaders;
	}
	
	static HashMap<String, String> doubleHeaders(){
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("content-type", "application/json");
		requestheaders.put("Cache-Control", "no-cache");
		requestheaders.put("authorization", "Basic R0dZU1dJOjIwMTVTVyFHR1k=");
		return requestheaders;
	}
	
	static HashMap<String, String> singleHeader(){
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("content-type", "application/json");
		requestheaders.put("Cache-Control", "no-cache");
		return requestheaders;
	}

	
	static String getRestaurantId(String restaurant_ref_id)
	{
		String restaurantId = SystemConfigProvider.getTemplate("deliveryclusterdb")
												  .queryForMap("select id from items where reference_id='" + restaurant_ref_id + "'").get("id").toString();
		System.out.println("*****RestaurantId******" + restaurantId);
		return restaurantId;
	}
	
}
