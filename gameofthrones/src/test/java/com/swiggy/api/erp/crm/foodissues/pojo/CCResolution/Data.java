package com.swiggy.api.erp.crm.foodissues.pojo.CCResolution;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"amount",
"reason",
"source",
"couponCode",
"couponId",
"couponAmount",
"couponDescription",
"cartItems",
"restaurantId",
"orderIncoming"
})
public class Data {

@JsonProperty("amount")
private Integer amount;
@JsonProperty("reason")
private String reason;
@JsonProperty("source")
private String source;
@JsonProperty("couponCode")
private String couponCode;
@JsonProperty("couponId")
private Integer couponId;
@JsonProperty("couponAmount")
private Integer couponAmount;
@JsonProperty("couponDescription")
private String couponDescription;
@JsonProperty("cartItems")
private List<CartItem> cartItems = null;
@JsonProperty("restaurantId")
private String restaurantId;
@JsonProperty("orderIncoming")
private Integer orderIncoming;

@JsonProperty("amount")
public Integer getAmount() {
return amount;
}

@JsonProperty("amount")
public void setAmount(Integer amount) {
this.amount = amount;
}

@JsonProperty("reason")
public String getReason() {
return reason;
}

@JsonProperty("reason")
public void setReason(String reason) {
this.reason = reason;
}

@JsonProperty("source")
public String getSource() {
return source;
}

@JsonProperty("source")
public void setSource(String source) {
this.source = source;
}

@JsonProperty("couponCode")
public String getCouponCode() {
return couponCode;
}

@JsonProperty("couponCode")
public void setCouponCode(String couponCode) {
this.couponCode = couponCode;
}

@JsonProperty("couponId")
public Integer getCouponId() {
return couponId;
}

@JsonProperty("couponId")
public void setCouponId(Integer couponId) {
this.couponId = couponId;
}

@JsonProperty("couponAmount")
public Integer getCouponAmount() {
return couponAmount;
}

@JsonProperty("couponAmount")
public void setCouponAmount(Integer couponAmount) {
this.couponAmount = couponAmount;
}

@JsonProperty("couponDescription")
public String getCouponDescription() {
return couponDescription;
}

@JsonProperty("couponDescription")
public void setCouponDescription(String couponDescription) {
this.couponDescription = couponDescription;
}

@JsonProperty("cartItems")
public List<CartItem> getCartItems() {
return cartItems;
}

@JsonProperty("cartItems")
public void setCartItems(List<CartItem> cartItems) {
this.cartItems = cartItems;
}

@JsonProperty("restaurantId")
public String getRestaurantId() {
return restaurantId;
}

@JsonProperty("restaurantId")
public void setRestaurantId(String restaurantId) {
this.restaurantId = restaurantId;
}

@JsonProperty("orderIncoming")
public Integer getOrderIncoming() {
return orderIncoming;
}

@JsonProperty("orderIncoming")
public void setOrderIncoming(Integer orderIncoming) {
this.orderIncoming = orderIncoming;
}

@Override
public String toString(){
    return getAmount() + ", " +getReason() + ", " +getSource() + ", " +getCouponCode() + ", " +getCouponId() + ", " 
           +getCouponAmount() + ", " +getCouponDescription() + ", " +getCartItems() + ", " +getRestaurantId() +
           ", " +getOrderIncoming();
}

}