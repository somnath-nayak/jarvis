package com.swiggy.api.erp.cms.dp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.testng.annotations.DataProvider;

import com.swiggy.api.erp.cms.pojo.Items.Item;

import framework.gameofthrones.Tyrion.JsonHelper;

public class ThirdPartDP {
	
	@DataProvider(name = "FetchDominozMenu")
    public Object[][] FetchDominozMenu() {
		
		Integer[] restaurantIds=new Integer[5];
		restaurantIds[0]=23849;
		restaurantIds[1]=24027;
		restaurantIds[2]=24529;
		
		List<Integer> restIds=Arrays.asList(restaurantIds);
		
	    return new Object[][]{
			  {restIds.toString(),"DOMINOS",null}
		  };
		
		
    }
	@DataProvider(name = "FetchMCDMenu")
    public Object[][] FetchMCDMenu() {
		
		Integer[] restaurantIds=new Integer[5];
		restaurantIds[0]=557;
		restaurantIds[1]=917;
		restaurantIds[2]=23662;
		List<Integer> restIds=Arrays.asList(restaurantIds);
		
	    return new Object[][]{
			  {restIds.toString(),"MCD","LUNCH"},
			  {restIds.toString(),"MCD","BREAKFAST"}
		  };
		
		
    }
	
	
	
	
}
