package com.swiggy.api.erp.finance.helper;

import com.swiggy.api.erp.finance.constants.PgConstants;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Tyrion.DBHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class NodalHelper {

    public String getOrderTotalFromOrderTable(String orderID) {
        List<Map<String, Object>> list;
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(PgConstants.finance_DB);
        String q = PgConstants.get_order_total_from_orders.replace("${order_id}", orderID);
        boolean status = DBHelper.pollDB(PgConstants.finance_DB, q, "order_id", orderID, 2, 90);
        if (status) {
            System.out.println("--Query ::: " + q);
            list = sqlTemplate.queryForList(q);
            String order_total_amount = list.get(0).get("order_total").toString();
            return order_total_amount;
        } else {
            System.out.println("-- order id not found in DB ::: " + orderID);
            return "order id not found";
        }

    }
     public String getOrderTableEntity(String orderEntity,String orderID){
         List<Map<String, Object>> list;
         SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(PgConstants.finance_DB);
         String q = PgConstants.get_order_total_from_orders.replace("${order_id}", orderID);
         boolean status = DBHelper.pollDB(PgConstants.finance_DB, q, "order_id", orderID, 2, 90);
         if (status) {
             System.out.println("--Query ::: " + q);
             list = sqlTemplate.queryForList(q);
             String order_entity = list.get(0).get(orderEntity).toString();
             return order_entity;
         } else {
             System.out.println("-- order id not found in DB ::: " + orderID);
             return "order id not found";
         }

     }

    public String getOrderStatusFromOrderTable(String orderEntity,String orderID){
        List<Map<String, Object>> list;
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(PgConstants.finance_DB);
        String q = PgConstants.get_order_status_from_orders_tinyint.replace("${order_id}", orderID).replace("${*}",orderEntity);

        System.out.println("--Query ::: " + q);
        list = sqlTemplate.queryForList(q);
        if(list.isEmpty()){
            return "Order id not exist";
        }
        else {
            String order_entity = list.get(0).get(orderEntity).toString();

            return order_entity;

        }


    }

    public double pg_igst_calculator(String order_id){
        double pg_igst=pgCommissionWithoutTax(order_id)*0.18;
        return pg_igst;
    }

    public double pg_cgst_calculator(String order_id){
        double pg_cgst=pgCommissionWithoutTax(order_id)*0.09;
        return pg_cgst;
    }
    public double pg_sgst_calculator(String order_id){
        double pg_sgst=pgCommissionWithoutTax(order_id)*0.09;
        return pg_sgst;
    }

    public  double pg_commission_calculator_with_zero_igst(String order_id){
        double pg_commission=pgCommissionWithoutTax(order_id)+pg_cgst_calculator(order_id)+pg_sgst_calculator(order_id);
        return  pg_commission;
    }

    public double amount_transfer_to_nodal_calculator(String txn_type,String orderID){
        if(txn_type==PgConstants.txn_type_forward){
            double ordertotal=Double.parseDouble(getOrderTotalFromOrderTable(orderID));
            double pgComm=pg_commission_calculator_with_zero_igst(orderID);
            double amount_nodal_transfer=ordertotal-pgComm;
           return amount_nodal_transfer;
        }
       else if(txn_type==PgConstants.txn_type_refund)
        {
            double ordertotal=Double.parseDouble(getOrderTotalFromOrderTable(orderID));
            return -ordertotal;

        }
       else
           return 0;

    }
    public void pushToPGTransactionNew(String pg, String mid, String order_id, String txn_id, String txn_type, String txn_date, String settlement_date, String gross_txn_value, String pg_commission, String cgst, String sgst, String igst, String amount_transferred_to_nodal, String utr, String refund_id, String created_at, String txn_details){
        List<Map<String, Object>> list;
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(PgConstants.finance_DB);
        String q=PgConstants.insert_into_pg_txn_new.replace("${pg}",pg).replace( "${mid}",mid).replace( "${order_id}",order_id).replace("${txn_id}",txn_id).replace("${txn_type}",txn_type).replace("${txn_date}",txn_date).replace( "${settlement_date}",settlement_date).replace( "${gross_txn_value}",gross_txn_value).replace("${pg_commission}",pg_commission).replace("${cgst}",cgst).replace("${sgst}",sgst).replace( "${igst}",igst).replace("${amount_transferred_to_nodal}",amount_transferred_to_nodal).replace("${utr}",utr).replace( "${refund_id}",refund_id).replace( "${created_at}",created_at).replace( "${txn_details}",txn_details);
        System.out.println("--Query ::: " + q);
        sqlTemplate.execute(q);

    }
    public void pg_transaction_new_entity_creator(String orderID,String orderTime,String txn_type, int choice){
        String gross_total_value=getOrderTotalFromOrderTable(orderID);
        String igst=pg_igst_calculator(orderID)+"";
        String cgst=pg_cgst_calculator(orderID)+"";
        String sgst=pg_sgst_calculator(orderID)+"";
        String amount_transfer_to_nodal=amount_transfer_to_nodal_calculator(txn_type,orderID)+"";
        String pg_comm=pg_commission_calculator_with_zero_igst(orderID)+"";
        String txn_id_forward=PgConstants.txn_id_forward_prefix+orderID;
        String txn_id_refund=PgConstants.txn_id_refund_prefix+orderID;
        String settlement_date=getSettlementdate(PgConstants.settlement_threshold);


       if(txn_type==PgConstants.txn_type_forward)
            pgSelector(choice,orderID,txn_id_forward,PgConstants.txn_type_forward,orderTime,settlement_date,gross_total_value,pg_comm,cgst,sgst,"0",amount_transfer_to_nodal,PgConstants.refund_id_for_forward_txn,PgConstants.txn_details_for_forward_txn);
       if(txn_type==PgConstants.txn_type_refund)
           pgSelector(choice,orderID,txn_id_refund,PgConstants.txn_type_refund,orderTime,settlement_date,"0","0","0","0","0",amount_transfer_to_nodal,PgConstants.refund_id_prefix_for_refund_txn+orderID,PgConstants.txn_details_for_refund_txn);
    }
    public void pgSelector(int choice,String orderID,String txn_id_forward_refund,String txn_type_frwd_refund,String orderTime,String settlement_date,String gross_total_value,String pg_comm,String cgst,String sgst,String igst,String amount_transfer_to_nodal,String refund_id,String txn_detail){
        switch(choice)
        {
            case 1: {
                pushToPGTransactionNew(PgConstants.citrus_pg, PgConstants.citrus_mid, orderID, txn_id_forward_refund, txn_type_frwd_refund, orderTime, settlement_date, gross_total_value, pg_comm, cgst, sgst, "0", amount_transfer_to_nodal, PgConstants.utr_id_prefix + orderID, refund_id, orderTime,txn_detail );
                break;
            }
            case 2: {
                pushToPGTransactionNew(PgConstants.freecharge_pg, PgConstants.freecharge_mid, orderID, txn_id_forward_refund, txn_type_frwd_refund, orderTime, settlement_date, gross_total_value, pg_comm, cgst, sgst, "0", amount_transfer_to_nodal, PgConstants.utr_id_prefix + orderID, refund_id, orderTime, txn_detail);
                break;
            }
            case 3: {
                pushToPGTransactionNew(PgConstants.lazypay_pg, PgConstants.lazypay_mid, orderID, txn_id_forward_refund, txn_type_frwd_refund, orderTime, settlement_date, gross_total_value, pg_comm, cgst, sgst, "0", amount_transfer_to_nodal, PgConstants.utr_id_prefix + orderID, refund_id, orderTime, txn_detail);
                break;
            }
            case 4: {
                pushToPGTransactionNew(PgConstants.payU_pg, PgConstants.payU_mid, orderID, txn_id_forward_refund, txn_type_frwd_refund, orderTime, settlement_date, gross_total_value, pg_comm, cgst, sgst, "0", amount_transfer_to_nodal, PgConstants.utr_id_prefix + orderID, refund_id, orderTime, txn_detail);
                break;
            }
            case 5: {
                pushToPGTransactionNew(PgConstants.Mobikwik_pg, PgConstants.Mobikwik_mid1, orderID, txn_id_forward_refund, txn_type_frwd_refund, orderTime, settlement_date, gross_total_value, pg_comm, cgst, sgst, "0", amount_transfer_to_nodal, PgConstants.utr_id_prefix + orderID, refund_id, orderTime, txn_detail);
                break;
            }
            case 6: {
                pushToPGTransactionNew(PgConstants.Mobikwik_pg, PgConstants.Mobikwik_mid2, orderID, txn_id_forward_refund, txn_type_frwd_refund, orderTime, settlement_date, gross_total_value, pg_comm, cgst, sgst, "0", amount_transfer_to_nodal, PgConstants.utr_id_prefix + orderID, refund_id, orderTime, txn_detail);
                break;
            }
            case 7: {
                pushToPGTransactionNew(PgConstants.PaytmApi_pg, PgConstants.PaytmApi_mid1, orderID, txn_id_forward_refund, txn_type_frwd_refund, orderTime, settlement_date, gross_total_value, pg_comm, cgst, sgst, "0", amount_transfer_to_nodal, PgConstants.utr_id_prefix + orderID, refund_id, orderTime, txn_detail);
                break;
            }
            case 8: {
                pushToPGTransactionNew(PgConstants.PaytmApi_pg, PgConstants.PaytmApi_mid2, orderID, txn_id_forward_refund, txn_type_frwd_refund, orderTime, settlement_date, gross_total_value, pg_comm, cgst, sgst, "0", amount_transfer_to_nodal, PgConstants.utr_id_prefix + orderID, refund_id, orderTime, txn_detail);
                break;
            }
            case 9: {
                pushToPGTransactionNew(PgConstants.razorpay_pg, PgConstants.razorpay_mid, orderID, txn_id_forward_refund, txn_type_frwd_refund, orderTime, settlement_date, gross_total_value, pg_comm, cgst, sgst, "0", amount_transfer_to_nodal, PgConstants.utr_id_prefix + orderID, refund_id, orderTime, txn_detail);
                break;
            }
            default:
                return;

            }


        }

        public String getOrderFromPGTxnNewTable(String orderID ,String entity){
            List<Map<String, Object>> list;
            SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(PgConstants.finance_DB);
            String q = PgConstants.get_entity_from_pg_txn_table.replace("${order_id}", orderID).replace("${entity}",entity);
            boolean status = DBHelper.pollDB(PgConstants.finance_DB, q, "order_id", orderID, 2, 90);
            if (status) {
                System.out.println("--Query ::: " + q);
                list = sqlTemplate.queryForList(q);
                String pg_entity = list.get(0).get("order_id").toString();
                return pg_entity;
            }
            else {
                System.out.println("-- order not found in DB ::: " + orderID);
                return "order not found";
            }

        }

        public double pgCommissionWithoutTax(String order_id){
            String gross_total_value=getOrderTotalFromOrderTable(order_id);
            double pg_commission_without_tax=Double.parseDouble(gross_total_value)*PgConstants.pg_commission_percent;
            return pg_commission_without_tax;

        }

        public String getSettlementdate(Integer days){
            Calendar calender = Calendar.getInstance();
            calender.add(Calendar.DATE, days);
            SimpleDateFormat sdf = new SimpleDateFormat();
            sdf.applyPattern("yyyy-MM-dd HH:mm:ss");
            String sdf_date = sdf.format(calender.getTime());
            return sdf_date;
        }


}
