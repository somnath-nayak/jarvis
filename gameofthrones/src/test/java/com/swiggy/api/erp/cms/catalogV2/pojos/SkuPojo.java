package com.swiggy.api.erp.cms.catalogV2.pojos;


import java.time.Instant;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.constants.CatalogV2Constants;
import com.swiggy.api.erp.cms.helper.CatalogV2Helper;
import com.swiggy.api.erp.cms.pojo.CategoryServiceCreateCategory.Attributes;


/**
 * Created by HemaGovindaraj on 24/12/18.
 */

public class SkuPojo
{
	CatalogV2Helper catalogV2Helper=new CatalogV2Helper();
	String resCat,NewcatId,resSpin,NewspinId;
	public SkuPojo()
	{
		resCat=catalogV2Helper.createCategoryPojo().ResponseValidator.GetBodyAsText();
		NewcatId=JsonPath.read(resCat,"$.id");
		resSpin=catalogV2Helper.createSpin(resCat).ResponseValidator.GetBodyAsText();
		NewspinId=JsonPath.read(resSpin,"$.spin");
	}

	public SkuPojo(String Newsku)
	{
		this.setStore_id(CatalogV2Constants.store_id+Newsku);
		this.setSku_id(CatalogV2Constants.sku_id+Newsku);
	}

	private CatalogV2PricingPojo pricing;

	private String store_id;

	private String spin;

	private String category_id;

	private boolean enabled;

	private String sku_id;

	private SkuAttributes attributes;

	public CatalogV2PricingPojo getPricing ()
	{
		return pricing;
	}

	public void setPricing (CatalogV2PricingPojo pricing)
	{
		this.pricing = pricing;
	}

	public String getStore_id ()
	{
		return store_id;
	}

	public void setStore_id (String store_id)
	{
		this.store_id = store_id;
	}

	public String getSpin ()
	{
		return spin;
	}

	public void setSpin (String spin)
	{
		this.spin = spin;
	}

	public String getCategory_id ()
	{
		return category_id;
	}

	public void setCategory_id (String category_id)
	{
		this.category_id = category_id;
	}

	public String getSku_id ()
	{
		return sku_id;
	}

	public void setSku_id (String sku_id)
	{
		this.sku_id = sku_id;
	}

	public SkuAttributes getAttributes ()
	{
		return attributes;
	}

	public void setSkuAttributes (SkuAttributes attributes)
	{
		this.attributes = attributes;
	}

	public boolean getEnabled ()
	{
		return enabled;
	}

	public void setEnabled (boolean enabled)
	{
		this.enabled = enabled;
	}

	public SkuPojo build() {
		setDefaultValues();
		return this;
	}

	private void setDefaultValues() {
		SkuAttributes attributes=new SkuAttributes();
		attributes.build();
		CatalogV2PricingPojo pricing= new CatalogV2PricingPojo();
		pricing.build();
		if(this.getCategory_id()==null){
			this.setCategory_id(NewcatId);
		}
		if(!this.getEnabled()){
			this.setEnabled(CatalogV2Constants.enabled);
		}
		if(this.getSku_id()==null){
			this.setSku_id(CatalogV2Constants.sku_id);
		}
		if(this.getSpin()==null){
			this.setSpin(NewspinId);
		}
		if(this.getStore_id()==null){
			this.setStore_id(CatalogV2Constants.store_id);
		}

	}

	@Override
	public String toString()
	{
		return "ClassPojo [pricing = "+pricing+", store_id = "+store_id+", spin = "+spin+", category_id = "+category_id+", sku_id = "+sku_id+", attributes = "+attributes+"]";
	}
}

