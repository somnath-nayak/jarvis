package com.swiggy.api.erp.ff.tests.fraudService;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.ff.POJO.*;
import com.swiggy.api.erp.ff.constants.FraudServiceConstants;
import com.swiggy.api.erp.ff.dp.FraudServiceDP.AreaContingencyDP;
import com.swiggy.api.erp.ff.helper.FraudServiceHelper;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import framework.gameofthrones.Tyrion.WireMockHelper;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;

public class AreaLevelCheck {

    CODCheckPayload ch;
    JsonHelper j = new JsonHelper();
    FraudServiceHelper fsh = new FraudServiceHelper();
    Processor processor;
    WireMockHelper wireMockHelper = new WireMockHelper();


    @BeforeClass
    public void addAreaLevelConfiguration() {
//        wireMockHelper.startMockServer(FraudServiceConstants.mockPort);

        fsh.clearRedisKeys(FraudServiceConstants.areaId_Kormanagala);
        fsh.clearRedisKeys(FraudServiceConstants.areaId_BTM);
        fsh.clearRedisKeys(FraudServiceConstants.areaId_HSR);

        fsh.clearRedisKeys(FraudServiceConstants.CityId_1);
        fsh.clearRedisKeys(FraudServiceConstants.CityId_2);
        fsh.clearRedisKeys(FraudServiceConstants.CityId_3);

        fsh.AddAreaLevelConfigurations(FraudServiceConstants.areaId_Kormanagala, FraudServiceConstants.areaFee_200);
        fsh.AddAreaLevelConfigurations(FraudServiceConstants.areaId_BTM, FraudServiceConstants.areaFee_0);
        fsh.AddCityLevelConfigurations(FraudServiceConstants.CityId_1, FraudServiceConstants.Cityfee_100);
        fsh.AddCityLevelConfigurations(FraudServiceConstants.CityId_5, FraudServiceConstants.Cityfee_0);



        try {
            fsh.mockDeliveryBoyResponse(FraudServiceConstants.statusCode);
            fsh.mockDSResponse(FraudServiceConstants.statusCode, FraudServiceConstants.predication_0, "1");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    @Test(dataProvider="getAreaLevelData", dataProviderClass = AreaContingencyDP.class,description="validation for different area level checks")
    public void AreaContingencyCheck(String requestId, CustomerDetails customerDetails, SessionDetails sessionDetails, DeliveryDetails deliveryDetails, CartDetails cartDetails) throws IOException {
        ch = new CODCheckPayload(requestId, customerDetails, sessionDetails, deliveryDetails, cartDetails);
        String body = j.getObjectToJSON(ch);
        processor = fsh.getCODCheckServiceProcessor(body);
        fsh.statusCheck(processor);
        String response = processor.ResponseValidator.GetBodyAsText();
        String areaId = deliveryDetails.getAreaId().toString();
        String codStatus = JsonPath.read(response, "$.cod_enabled").toString();
        String reason = JsonPath.read(response, "reason");
        int amount = cartDetails.getBillAmount();
        checkForArea(areaId, amount, codStatus, reason);
    }

    public void checkForArea(String areaId, int amount, String codStatus, String reason) {
        switch (areaId) {
            case FraudServiceConstants.areaId_Kormanagala:
                if (amount <= Integer.parseInt(FraudServiceConstants.areaFee_200)) {
                    Assert.assertEquals(codStatus, "true","Wrong COD status is coming in response");
                } else {
                    Assert.assertEquals(codStatus, "false","Wrong COD status is coming in response");
                    Assert.assertEquals(reason, "contingency_check","wrong reason is coming in response");
                }
                break;

            case FraudServiceConstants.areaId_BTM: {
                Assert.assertEquals(codStatus, "false","Wrong COD status is coming in response");
                Assert.assertEquals(reason, "contingency_check","wrong reason is coming in response");
            }
            break;

            case FraudServiceConstants.areaId_HSR: {
                if (amount <= Integer.parseInt(FraudServiceConstants.Cityfee_100)) {
                    Assert.assertEquals(codStatus, "true","Wrong COD status is coming in response");
                } else {
                    Assert.assertEquals(codStatus, "false","Wrong COD status is coming in response");
                    Assert.assertEquals(reason, "contingency_check","wrong reason is coming in response");
                }
            }break;
            case FraudServiceConstants.areaId_Ameerpet: {
                    Assert.assertEquals(codStatus, "true","Wrong COD status is coming in response");
            }break;
            case FraudServiceConstants.areaId_Thane: {

                    Assert.assertEquals(codStatus, "false","Wrong COD status is coming in response");
                    Assert.assertEquals(reason, "contingency_check","wrong reason is coming in response");
                }break;

        }
    }
}
