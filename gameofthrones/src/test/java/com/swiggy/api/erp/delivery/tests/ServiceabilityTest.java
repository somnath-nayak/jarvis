package com.swiggy.api.erp.delivery.tests;


import com.swiggy.api.erp.delivery.helper.ServiceablilityHelper;
import framework.gameofthrones.Daenerys.Initializer;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.swiggy.api.erp.delivery.dp.ServiceabilityDataProvider;
import com.swiggy.api.erp.delivery.helper.DeliveryHelperMethods;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.snd.helper.SnDHelper;
import framework.gameofthrones.Aegon.Initialize;

public class ServiceabilityTest extends ServiceabilityDataProvider{

	ServiceablilityHelper serviceablilityHelper = new ServiceablilityHelper();
	//Initialize gameofthrones = new Initialize();
	Initialize gameofthrones =Initializer.getInitializer();
	DeliveryHelperMethods delmeth=new DeliveryHelperMethods();
	DeliveryServiceHelper helpdel = new DeliveryServiceHelper();
	SnDHelper sndhelp=new SnDHelper();
	CheckoutHelper chelp=new CheckoutHelper();
	ServiceablilityHelper sHelper=new ServiceablilityHelper();

	@Test(dataProvider = "deliveryListing_Simple")
	public void deliveryListing_Simple(String lat, String lng, String city_id, String[] resIds){
		Processor processor = serviceablilityHelper.serviceabilityListing(lat, lng, city_id, resIds);
		Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);

	}
	@Test(dataProvider = "deliveryCartSLA")
	public void deliveryCartSLA(String restaurant_id, String area_id, String city_id, String lat_long,
								String address_lat_long){
		serviceablilityHelper.updateLastMileForZone(4, 1);
		Processor processor = serviceablilityHelper.serviceabilityCartSLAMinParam(restaurant_id, area_id, city_id, lat_long, address_lat_long);
		Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
	}

	@Test(groups = { "regression", "Karunakar QE-388" }, priority = 0, dataProvider = "Listing388", description="Verify the restaurant is not serviceable in Listing when last mile is greater than the config at area level")
	public void serviceabilityInListingLastMileMore (String lat, String lng, String restID, String area_id) throws Exception {
		serviceablilityHelper.updateLastMileForArea(0, 1);
		Thread.sleep(5000);
		Assert.assertNull(sHelper.serviceabilityCheckListing(lat, lng, restID));
		serviceablilityHelper.updateLastMileForArea(4, 1);
	}
	
	@Test(groups = { "regression", "Karunakar QE-391" }, priority = 0, dataProvider = "Listing391", description="Verify the restaurant is serviceable in Listing when last mile is less than the config at area level")
	public void serviceabilityInListingLastMileLess (String lat, String lng, String restID, String area_id) throws Exception {
		serviceablilityHelper.updateLastMileForArea(10, 1);
		Thread.sleep(5000);
		Assert.assertEquals(sHelper.serviceabilityCheckListing(lat, lng, restID).toString(),"SERVICEABLE");
	}
	
	@Test(groups = {"regression", "Somnath QE-389"}, dataProvider = "Listing391", enabled = true,  description ="Verify the restaurant is not serviceable in Listing when last mile is less than the config at restaurant level")
	public void serviceability390(String lat, String lng, String restID,String area_id) throws Exception {
		serviceablilityHelper.updateMaxSecondMileForRestaurant(0, restID);
		Thread.sleep(5000);
	    Assert.assertEquals(sHelper.serviceabilityCheckListing(lat, lng, restID).toString(),"SERVICEABLE_WITH_BANNER");
		serviceablilityHelper.updateMaxSecondMileForRestaurant(10, restID);
	}
	
	@Test(groups = {"regression", "Somnath QE-390"}, dataProvider = "Listing391", enabled = true,  description ="Verify the restaurant is  serviceable in Listing  when last mile is greater than the config at restaurant level")
	public void serviceability389(String lat, String lng, String restID,String area_id) throws Exception {
		serviceablilityHelper.updateMaxSecondMileForRestaurant(10, restID);
		Thread.sleep(5000);
		Assert.assertEquals(sHelper.serviceabilityCheckListing(lat, lng, restID).toString(), "SERVICEABLE");
	}
	
	@Test(groups = { "regression", "Karunakar QE-393" }, priority = 0, dataProvider = "Listing391", description="Verify the restaurant is  serviceable  in Listing   when delivery time is more  than the max SLA")
	public void serviceabilityInListingDeliveryTimeMore(String lat, String lng, String restID, String zone_id) throws Exception {
		serviceablilityHelper.updateDeliveryTimeForZone(100, 1);
		Thread.sleep(5000);		
	    Assert.assertEquals(sHelper.serviceabilityCheckListing(lat, lng, restID).toString(),"SERVICEABLE");
	}
	
	@Test(groups = { "regression", "Karunakar QE- 392" }, priority = 0, dataProvider = "Listing391", description="Verify the restaurant is not serviceable  in Listing   when delivery time is more  than the max SLA")
	public void serviceabilityInListingDeliveryTimeLess(String lat, String lng, String restID, String zone_id) throws Exception {
		serviceablilityHelper.updateDeliveryTimeForZone(10, 1);
		Thread.sleep(5000);		
		Assert.assertNull(sHelper.serviceabilityCheckListing(lat, lng, restID));
		serviceablilityHelper.updateDeliveryTimeForZone(100, 1);

	}
	
	@Test(groups = { "regression", "Karunakar QE- 394" }, priority = 0, dataProvider = "Listing391", description="Verify the restaurant is serviceable  in Listing   when item exceeded in the cart")
	public void serviceabilityInListingItemsMore(String lat, String lng, String restID, String zone_id) throws Exception {
		serviceablilityHelper.updateMaxItemsInBatchForZone(0, 1);
		Thread.sleep(5000);		
		Assert.assertEquals(sHelper.serviceabilityCheckListing(lat, lng, restID).toString(), "SERVICEABLE");
		serviceablilityHelper.updateMaxItemsInBatchForZone(10, 1);
	}
	
	@Test(groups = { "regression", "Karunakar QE- 395" }, priority = 0, dataProvider = "Listing391", description="Verify the restaurant is serviceable  in Listing   when item count is less than the config")
	public void serviceabilityInListingItemsLess( String lat, String lng, String restID, String zone_id) throws Exception {
		serviceablilityHelper.updateMaxItemsInBatchForZone(10, 1);
		Thread.sleep(5000);		
		Assert.assertEquals(sHelper.serviceabilityCheckListing(lat, lng, restID).toString(), "SERVICEABLE");
	}
	
	//High rain mode enabled and BF has crossed the stop bf of rain mode
	@Test(groups = { "regression", "Karunakar QE- 396" }, priority = 0, dataProvider = "Listing", description="Verify the restaurant is not serviceable  in Listing when High rain mode is enabled and current bf crosses the rain mode stop bf")
	public void serviceabilityInListinginHighRainBFMore(String lat, String lng, String restID, String zone_id) throws Exception {
		serviceablilityHelper.updateRainModeTypeForZone(1, 1);
		//set BF in redis more than stop bf of rain mode
		Thread.sleep(5000);		
		Assert.assertEquals(sHelper.serviceabilityCheckListing(lat, lng, restID).toString(), "SERVICEABLE_WITH_BANNER");
	}
	//High rain mode enabled and BF has crossed the stop bf of rain mode
		@Test(groups = { "regression", "Karunakar QE- 397" }, priority = 0, dataProvider = "Listing", description="Verify restaurant is not serviceable  in Listing when Low rain mode is enabled and current bf crosses the rain mode stop bf")
		public void serviceabilityInListinginLowRainBFMore( String lat, String lng, String restID, String zone_id) throws Exception {
			serviceablilityHelper.updateRainModeTypeForZone(2, 1);
			//set BF in redis less than stop bf of rain mode
			Thread.sleep(5000);		
			Assert.assertEquals(sHelper.serviceabilityCheckListing(lat, lng, restID).toString(), "SERVICEABLE");
		}
		//High rain mode enabled and BF has crossed the stop bf of rain mode
		@Test(groups = { "regression", "Karunakar QE- 398" }, priority = 0, dataProvider = "Listing", description="Verify the restaurant is  serviceable  in Listing when High rain mode is enabled  and the current bf is less than the rain stop bf")
		public void serviceabilityInListinginHighRainBFLess( String lat, String lng, String restID, String zone_id) throws Exception {
			serviceablilityHelper.updateRainModeTypeForZone(2, 1);
			//set BF in redis more than stop bf of rain mode
			Thread.sleep(5000);		
			Assert.assertEquals(sHelper.serviceabilityCheckListing(lat, lng, restID).toString(), "SERVICEABLE");
		}
		@Test(groups = { "regression", "Karunakar QE- 401" }, priority = 0, dataProvider = "Listing", description="Verify the restaurant is  serviceable  in Listing when Low rain mode is enabled  and the current bf is less than the rain stop bf")
		public void serviceabilityInListinginLowRainBFLess( String lat, String lng, String restID, String zone_id) throws Exception {
			serviceablilityHelper.updateRainModeTypeForZone(1, 1);
			//set BF in redis more than stop bf of rain mode
			Thread.sleep(5000);		
			Assert.assertEquals(sHelper.serviceabilityCheckListing(lat, lng, restID).toString(), "SERVICEABLE");
		}
		
		
			//low rain mode enabled and rain expires
			@Test(groups = { "regression", "Karunakar QE- 399" }, priority = 0, dataProvider = "Listing", description="Verify the restaurant serviceabilty  in Listing when heavy rain mode expires")
			public void serviceabilityInListinginHighRainExpires( String lat, String lng, String restID, String zone_id) throws Exception {
				serviceablilityHelper.updateRainModeTypeForZone(1, 1);
				//Make High rain mode expire here
				Thread.sleep(5000);		
				Assert.assertEquals(sHelper.serviceabilityCheckListing(lat, lng, restID).toString(), "SERVICEABLE");
			}
			//low rain mode enabled and rain expires
				@Test(groups = { "regression", "Karunakar QE-400" }, priority = 0, dataProvider = "Listing", description="Verify the restaurant serviceabilty when rain Low rain mode expires")
				public void serviceabilityInListinginLowRainExpires( String lat, String lng, String restID, String zone_id) throws Exception {
					serviceablilityHelper.updateRainModeTypeForZone(2, 1);
					//Make Low rain mode expire here and check BF
					Thread.sleep(5000);		
					Assert.assertEquals(sHelper.serviceabilityCheckListing(lat, lng, restID).toString(), "SERVICEABLE");
				}
				
				//rain enable in rest
				@Test(groups = { "regression", "Karunakar QE-403" }, priority = 0, dataProvider = "Listing", description="Verify restaurant is not serviceable  in Listing in rain when a restaurant has rain mode desabled")
				public void serviceabilityInListingRainInVendorEnabled( String lat, String lng, String restID, String zone_id) throws Exception {
					serviceablilityHelper.updateRainModeTypeForRestaurant(3, 1);
					//set BF in redis more than stop bf of rain mode
					Thread.sleep(5000);		
					Assert.assertEquals(sHelper.serviceabilityCheckListing(lat, lng, restID).toString(), "SERVICEABLE_WITH_BANNER");
				}
				//rain disable in rest
					@Test(groups = { "regression", "Karunakar QE-404" }, priority = 0, dataProvider = "Listing", description="Verify restaurant is  serviceable  in Listing in rain when a restaurant has rain mode enabled")
					public void serviceabilityInListingRainInVendordisabled(String lat, String lng, String restID, String zone_id) throws Exception {
						serviceablilityHelper.updateRainModeTypeForRestaurant(1, 1);
						//set BF in redis less than stop bf of rain mode
						Thread.sleep(5000);		
						Assert.assertEquals(sHelper.serviceabilityCheckListing(lat, lng, restID).toString(), "SERVICEABLE");
					}		
	@Test(groups = { "regression", "Karunakar" }, priority = 0, dataProvider = "Listing391", description="Verify the restaurant is serviceable  in Listing   when zone is open")
	public void serviceabilityInListingDeliveryZoneOpen(String lat, String lng, String restID, String zone_id) throws Exception {
		serviceablilityHelper.updateEnabledForZone(1, 1);
		Thread.sleep(5000);		
	    Assert.assertEquals(sHelper.serviceabilityCheckListing(lat, lng, restID).toString(),"SERVICEABLE");
	}
	
	//Zone closed but still showing serviceable in both snd and delivery
	@Test(groups = { "regression", "Karunakar" }, priority = 0, dataProvider = "Listing391", description="Verify the restaurant is not serviceable  in Listing   when zone is close")
	public void serviceabilityInListingDeliveryZoneCloses(String lat, String lng, String restID, String zone_id) throws Exception {
		serviceablilityHelper.updateEnabledForZone(0, 1);
		Thread.sleep(5000);	
		System.out.println("Zone closed but still showing serviceable in both snd and delivery");
	    Assert.assertEquals(sHelper.serviceabilityCheckListing(lat, lng, restID).toString(),"SERVICEABLE_WITH_BANNER");
		serviceablilityHelper.updateEnabledForZone(1, 1);
	}
	
	@Test(groups = { "regression", "Karunakar" }, priority = 0, dataProvider = "Listing391", description="Verify the restaurant is not serviceable  in Listing   when the area is open")
	public void serviceabilityInListingDeliveryAreaOpen(String lat, String lng, String restID, String areaId) throws Exception {
		serviceablilityHelper.updateEnabledForArea(1, 1);
		Thread.sleep(5000);		
	    Assert.assertEquals(sHelper.serviceabilityCheckListing(lat, lng, restID).toString(),"SERVICEABLE");
	}
	
	//Closing area from delivery side doesnt affect in SND
	@Test(groups = { "regression", "Karunakar QE-509" }, priority = 0, dataProvider = "Listing391", description="Verify the restaurant is not serviceable  in Listing   when the area is close")
	public void serviceabilityInListingDeliveryAreaCloses(String lat, String lng, String restID, String areaId) throws Exception {
		serviceablilityHelper.updateEnabledForArea(0, 1);
		Thread.sleep(5000);	
	    System.out.println("\nClosing area from delivery side doesnt affect in SND\n");
	    Assert.assertEquals(sHelper.serviceabilityCheckListing(lat, lng, restID).toString(),"SERVICEABLE");
		serviceablilityHelper.updateEnabledForArea(1, 1);

	}
	
	
	
	//rain enable in rest
	@Test(groups = { "regression", "Karunakar QE-471" }, priority = 0, dataProvider = "Listing391", description="Verify restaurant is serviceable in Listing and DE gets the payout when restaurant falls under rain polygon")
	public void serviceabilityInListingRainInVendorFallsRain( String lat, String lng, String restID, String zone_id) throws Exception {
		serviceablilityHelper.updateRainModeTypeForRestaurant(1, 1);
		//set BF in redis more than stop bf of rain mode
		Thread.sleep(5000);		
		Assert.assertEquals(sHelper.serviceabilityCheckListing(lat, lng, restID).toString(), "SERVICEABLE");
	}
	
	@Test(groups = { "regression", "Karunakar QE-472" }, priority = 0, dataProvider = "Listing391", description="Verify restaurant is serviceable in Listing and DE gets the payout when restaurant falls under rain polygon")
	public void serviceabilityInListingRainInCustFallsRain( String lat, String lng, String restID, String zone_id) throws Exception {
		serviceablilityHelper.updateRainModeTypeForRestaurant(1, 1);
		//set BF in redis more than stop bf of rain mode
		Thread.sleep(5000);		
		Assert.assertEquals(sHelper.serviceabilityCheckListing(lat, lng, restID).toString(), "SERVICEABLE");
	}
	
	
	@Test(groups = { "regression", "Karunakar QE-473" }, priority = 0, dataProvider = "Listing391", description="Verify the restaurant is serviceable in cart when Temp Last Mile is  less than the config")
	public void serviceabilityInListingTempLastMileMore( String lat, String lng, String restID, String zone_id) throws Exception {
		serviceablilityHelper.updateEnableTempLastMile(1, 1);
		serviceablilityHelper.updateZoneTempLastMileForZone(0, 1);
		Thread.sleep(5000);		
		Assert.assertEquals(sHelper.serviceabilityCheckListing(lat, lng, restID).toString(), "SERVICEABLE_WITH_BANNER");
	//	serviceablilityHelper.updateZoneTempLastMileForZone(10, 1);

	}

	@Test(groups = { "regression", "Karunakar QE-474" }, priority = 0, dataProvider = "Listing391", description="Verify the restaurant is not serviceable in cart when Temp Last Mile is  greater than the config")
	public void serviceabilityInListingTempLastMile( String lat, String lng, String restID, String zone_id) throws Exception {
		serviceablilityHelper.updateEnableTempLastMile(1, 1);
		serviceablilityHelper.updateZoneTempLastMileForZone(10, 1);
		Thread.sleep(5000);		
		Assert.assertEquals(sHelper.serviceabilityCheckListing(lat, lng, restID).toString(), "SERVICEABLE");
	}
	
	
	@Test(groups = { "regression", "Karunakar QE-480" }, priority = 0, dataProvider = "Listing", description="Verify the restaurant is serviceable in Listing when BF less than the stop BF")
	public void serviceabilityInListingBFLessThanSBF( String lat, String lng, String restID, String zone_id) throws Exception {
		//Set BF Less than Stop BF
		Thread.sleep(5000);		
		Assert.assertEquals(sHelper.serviceabilityCheckListing(lat, lng, restID).toString(), "SERVICEABLE");
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//**************************Cart Testcases*********************************//
	@Test(groups = { "regression", "Karunakar QE-454" }, priority = 0, dataProvider = "Cart", description="Verify the restaurant is serviceable in Cart   when last mile is less than the config at area level")
			public void serviceabilityInCartAreaLastMileLess ( String restID, String zone_id, String area_id,String itemId, String quantity,String addressId1,String addressId2) throws Exception {
		serviceablilityHelper.updateLastMileForArea(10, 1);
		Thread.sleep(5000);
		Assert.assertEquals(sHelper.serviceabilityCheckCart(restID,  area_id, itemId,  quantity).get(addressId1).toString(), "1");
	}
	
	@Test(groups = { "regression", "Karunakar QE-455" }, priority = 0, dataProvider = "Cart", description="Verify the restaurant is not serviceable in Cart  when last mile is greater than the config at restaurant level")
	public void serviceabilityInCartRestaurantLastMileLess ( String restID, String zone_id, String area_id,String itemId, String quantity,String addressId1,String addressId2) throws Exception {
serviceablilityHelper.updateLastMileForRestaurant(1, 1);
Thread.sleep(5000);
Assert.assertEquals(sHelper.serviceabilityCheckCart(restID,  area_id, itemId,  quantity).get(addressId1).toString(), "1");
serviceablilityHelper.updateLastMileForRestaurant(10, 1);

	}
	@Test(groups = { "regression", "Karunakar QE-456" }, priority = 0, dataProvider = "Cart", description="Verify the restaurant is serviceable in Cart  when last mile is less than the config at restaurant level")
	public void serviceabilityInCartRestaurantLastMileMore ( String restID, String zone_id, String area_id,String itemId, String quantity,String addressId1,String addressId2) throws Exception {
serviceablilityHelper.updateLastMileForRestaurant(10, 1);
Thread.sleep(5000);
Assert.assertEquals(sHelper.serviceabilityCheckCart(restID,  area_id, itemId,  quantity).get(addressId1).toString(), "0");
}
	
	@Test(groups = { "regression", "Karunakar QE-457" }, priority = 0, dataProvider = "Cart", description="Verify the restaurant is not serviceable in Cart   when last mile is less than the config at area level")
	public void serviceabilityInCartAreaLastMileMore ( String restID, String zone_id, String area_id,String itemId, String quantity,String addressId1, String addressId2) throws Exception {
				
	   serviceablilityHelper.updateLastMileForArea(0, 1);
      Thread.sleep(5000);		
	  Assert.assertEquals(sHelper.serviceabilityCheckCart(restID,  area_id, itemId,  quantity).get(addressId1).toString(), "0");
	  serviceablilityHelper.updateLastMileForRestaurant(10, 1);
	   serviceablilityHelper.updateLastMileForArea(4, 1);

	}
	
	@Test(groups = { "regression", "Karunakar QE-458" }, priority = 0, dataProvider = "Cart", description="Verify the restaurant is not serviceable  in Cart   when delivery time is more  than the max SLA")
	public void serviceabilityInCartDeliveryTimeMore(String restID, String zone_id,String area_id,String itemId, String quantity,String addressId,String addressId2) throws Exception {
		serviceablilityHelper.updateLastMileForZone(10, 1);
		Thread.sleep(5000);		
	    Assert.assertEquals(sHelper.serviceabilityCheckCart( restID,  area_id, itemId,  quantity).get(addressId).toString(),"0");
	}
	
	
	
	@Test(groups = { "regression", "Karunakar QE-459" }, priority = 0, dataProvider = "Cart", description="Verify the restaurant is serviceable  in Cart   when delivery time is less than the max SLA")
	public void serviceabilityInCartDeliveryTimeLess( String restID, String zone_id,String area_id,String itemId, String quantity,String addressId, String addressId2) throws Exception {
		serviceablilityHelper.updateLastMileForZone(100, 1);
		Thread.sleep(5000);	
	  Assert.assertEquals(sHelper.serviceabilityCheckCart(  restID,  area_id, itemId,  quantity).get(addressId).toString(),"1");
		
	}
	

	@Test(groups = { "regression", "Karunakar QE-460" }, priority = 0, dataProvider = "Cart", description="Verify the restaurant is not serviceable  in Cart   when item exceeded in the cart")
	public void serviceabilityInCartItemsMore( String restID, String zone_id,String area_id,String itemId, String quantity,String addressId, String addressId2) throws Exception {
		
		serviceablilityHelper.updateMaxItemsInBatchForZone(1, 1);
		Thread.sleep(5000);		
	    Assert.assertEquals(sHelper.serviceabilityCheckCart(  restID,  area_id, itemId,  quantity).get(addressId).toString(),"0");
		
	}
	
	@Test(groups = { "regression", "Karunakar QE-461" }, priority = 0, dataProvider = "Cart", description="Verify the restaurant is serviceable  in Cart   when item count is less than the config")
	public void serviceabilityInCartItemsLess( String restID, String zone_id,String area_id,String itemId, String quantity,String addressId, String addressId2) throws Exception {
		serviceablilityHelper.updateMaxItemsInBatchForZone(10, 1);
		Thread.sleep(5000);		
	 Assert.assertEquals(sHelper.serviceabilityCheckCart(  restID,  area_id, itemId,  quantity).get(addressId).toString(),"1");
	}
	
	
	@Test(groups = { "regression", "Karunakar QE-477" }, priority = 0, dataProvider = "Cart", description="Verify the restaurant is serviceable in cart when Temp Last Mile is  less than the config")
	public void serviceabilityInCartTempLastMileMore( String restID, String zone_id,String area_id,String itemId, String quantity,String addressId, String addressId2) throws Exception {
		serviceablilityHelper.updateEnableTempLastMile(1, 1);
		serviceablilityHelper.updateZoneTempLastMileForZone(1, 1);
		Thread.sleep(5000);		
	 Assert.assertEquals(sHelper.serviceabilityCheckCart(  restID,  area_id, itemId,  quantity).get(addressId).toString(),"1");
	}

	@Test(groups = { "regression", "Karunakar QE-478" }, priority = 0, dataProvider = "Cart", description="Verify the restaurant is not serviceable in cart when Temp Last Mile is  greater than the config")
	public void serviceabilityInCartTempLastMile( String restID, String zone_id,String area_id,String itemId, String quantity,String addressId, String addressId2) throws Exception {
		serviceablilityHelper.updateEnableTempLastMile(1, 1);
		serviceablilityHelper.updateZoneTempLastMileForZone(10, 1);
		Thread.sleep(5000);		
	 Assert.assertEquals(sHelper.serviceabilityCheckCart(  restID,  area_id, itemId,  quantity).get(addressId),"0");
	}
	
	@Test(groups = { "regression", "Karunakar QE-462" }, priority = 0, dataProvider = "Cart", description="Verify the restaurant is not serviceable  in Cart   when High rain mode is enabled and current bf crosses the rain mode stop bf")
	public void serviceabilityInCartInHeavyRainModeBFMore( String restID, String zone_id,String area_id,String itemId, String quantity,String addressId, String addressId2) throws Exception {
		serviceablilityHelper.updateRainModeTypeForZone(2, 1);
		//Set BF more
				Thread.sleep(5000);		
	 Assert.assertEquals(sHelper.serviceabilityCheckCart(  restID,  area_id, itemId,  quantity).get(addressId),"0");
	 //set BF normal
	}
	
	@Test(groups = { "regression", "Karunakar QE-463" }, priority = 0, dataProvider = "Cart", description="Verify restaurant is not serviceable  in Cart  when Low rain mode is enabled and current bf crosses the rain mode stop bf")
	public void serviceabilityInCartInLowRainModeMoreBF( String restID, String zone_id,String area_id,String itemId, String quantity,String addressId, String addressId2) throws Exception {
		serviceablilityHelper.updateRainModeTypeForZone(1, 1);
		//Set BF more
		Thread.sleep(5000);		
	 Assert.assertEquals(sHelper.serviceabilityCheckCart(  restID,  area_id, itemId,  quantity).get(addressId),"0");
	 //Set BF nirmal
	}
	@Test(groups = { "regression", "Karunakar QE-464" }, priority = 0, dataProvider = "Cart", description="Verify the restaurant is  serviceable  in Cart  when High rain mode is enabled  and the current bf is less than the rain stop bf")
	public void serviceabilityInCartInHighRainLesseBF( String restID, String zone_id,String area_id,String itemId, String quantity,String addressId, String addressId2) throws Exception {
		serviceablilityHelper.updateRainModeTypeForZone(1, 1);
		//Set BF normal
		Thread.sleep(5000);		
	 Assert.assertEquals(sHelper.serviceabilityCheckCart(  restID,  area_id, itemId,  quantity).get(addressId),"1");
	}
	@Test(groups = { "regression", "Karunakar QE-468" }, priority = 0, dataProvider = "Cart", description="Verify restaurant is serviceable  in Cart   when Low rain mode enabled and the current bf is less than the rain stop bf")
	public void serviceabilityInCartInLowRainLessBF( String restID, String zone_id,String area_id,String itemId, String quantity,String addressId, String addressId2) throws Exception {
		serviceablilityHelper.updateRainModeTypeForZone(1, 1);
		//Set BF normal
		Thread.sleep(5000);		
	 Assert.assertEquals(sHelper.serviceabilityCheckCart(  restID,  area_id, itemId,  quantity).get(addressId),"1");
	}
	@Test(groups = { "regression", "Karunakar QE-465" }, priority = 0, dataProvider = "Cart", description="Verify the restaurant serviceabilty  in Cart  when rain High rain mode expires")
	public void serviceabilityInCartInLowRainExpires( String restID, String zone_id,String area_id,String itemId, String quantity,String addressId, String addressId2) throws Exception {
		serviceablilityHelper.updateRainModeTypeForZone(1, 1);
		Thread.sleep(5000);		
	 Assert.assertEquals(sHelper.serviceabilityCheckCart(  restID,  area_id, itemId,  quantity).get(addressId),"1");
	}
	@Test(groups = { "regression", "Karunakar QE-466" }, priority = 0, dataProvider = "Cart", description="Verify the restaurant serviceabilty  in Cart  when rain Low rain mode expires")
	public void serviceabilityInCartInHighRainExpires( String restID, String zone_id,String area_id,String itemId, String quantity,String addressId, String addressId2) throws Exception {
		serviceablilityHelper.updateRainModeTypeForZone(1, 1);
		Thread.sleep(5000);		
	 Assert.assertEquals(sHelper.serviceabilityCheckCart(  restID,  area_id, itemId,  quantity).get(addressId),"1");
	}
	
	@Test(groups = { "regression", "Karunakar QE-469" }, priority = 0, dataProvider = "Cart", description="Verify the restaurant is serviceable  in Cart   when item count is less than the config")
	public void serviceabilityInCartInHighRainVendor( String restID, String zone_id,String area_id,String itemId, String quantity,String addressId, String addressId2) throws Exception {
		serviceablilityHelper.updateRainModeTypeForRestaurant(1, 1);
		Thread.sleep(5000);		
	 Assert.assertEquals(sHelper.serviceabilityCheckCart(  restID,  area_id, itemId,  quantity).get(addressId),"1");
	}
	@Test(groups = { "regression", "Karunakar QE-470" }, priority = 0, dataProvider = "Cart", description="Verify the restaurant is serviceable  in Cart   when item count is less than the config")
	public void serviceabilityInCartInLowRainVendor( String restID, String zone_id,String area_id,String itemId, String quantity,String addressId, String addressId2) throws Exception {
		serviceablilityHelper.updateRainModeTypeForRestaurant(1, 1);
		Thread.sleep(5000);		
	 Assert.assertEquals(sHelper.serviceabilityCheckCart(  restID,  area_id, itemId,  quantity).get(addressId),"1");
	}
	
	@Test(groups = { "regression", "Karunakar QE-475" }, priority = 0, dataProvider = "Cart", description="Verify restaurant is serviceable in Listing and DE gets the payout when restaurant falls under rain polygon")
	public void serviceabilityInCartIncustFallsRain( String restID, String zone_id,String area_id,String itemId, String quantity,String addressId, String addressId2) throws Exception {
		serviceablilityHelper.updateRainModeTypeForRestaurant(1, 1);
		//rest address should fll under rain polygn
		Thread.sleep(5000);		
	 Assert.assertEquals(sHelper.serviceabilityCheckCart(  restID,  area_id, itemId,  quantity).get(addressId),"1");
	}
	
	@Test(groups = { "regression", "Karunakar QE-476" }, priority = 0, dataProvider = "Cart", description="Verify restaurant is serviceable in Listing and DE gets the payout when customer falls under rain polygon")
	public void serviceabilityInCartInRestFallsRain( String restID, String zone_id,String area_id,String itemId, String quantity,String addressId, String addressId2) throws Exception {
		serviceablilityHelper.updateRainModeTypeForRestaurant(1, 1);
		//customer address should fall under rain polygon
		Thread.sleep(5000);		
	 Assert.assertEquals(sHelper.serviceabilityCheckCart(  restID,  area_id, itemId,  quantity).get(addressId),"1");
	}
	
	
	@Test(groups = { "regression", "Karunakar" }, priority = 0, dataProvider = "Cart", description="Verify the restaurant is serviceable  in Cart   when item count is less than the config")
	public void serviceabilityInCartInOpenZone( String restID, String zone_id,String area_id,String itemId, String quantity,String addressId, String addressId2) throws Exception {
		serviceablilityHelper.updateEnabledForZone(1, 1);
		Thread.sleep(5000);		
	 Assert.assertEquals(sHelper.serviceabilityCheckCart(  restID,  area_id, itemId,  quantity).get(addressId),"1");
	}
	
	@Test(groups = { "regression", "Karunakar" }, priority = 0, dataProvider = "Cart", description="Verify the restaurant is serviceable  in Cart   when item count is less than the config")
	public void serviceabilityInCartInCloseZone( String restID, String zone_id,String area_id,String itemId, String quantity,String addressId, String addressId2) throws Exception {
		serviceablilityHelper.updateEnabledForZone(0, 1);
		Thread.sleep(5000);		
	 Assert.assertEquals(sHelper.serviceabilityCheckCart(  restID,  area_id, itemId,  quantity).get(addressId),"0");
	}
	
	
	@Test(groups = { "regression", "Karunakar" }, priority = 0, dataProvider = "Cart", description="Verify the restaurant is serviceable  in Cart   when item count is less than the config")
	public void serviceabilityInCartInOpenArea( String restID, String zone_id,String area_id,String itemId, String quantity,String addressId, String addressId2) throws Exception {
		serviceablilityHelper.updateEnabledForArea(1, 1);
		Thread.sleep(5000);		
	 Assert.assertEquals(sHelper.serviceabilityCheckCart(  restID,  area_id, itemId,  quantity).get(addressId),"1");
	}
	
	@Test(groups = { "regression", "Karunakar QE-510" }, priority = 0, dataProvider = "Cart", description="Verify the restaurant is serviceable  in Cart   when item count is less than the config")
	public void serviceabilityInCartInCloseArea( String restID, String zone_id,String area_id,String itemId, String quantity,String addressId, String addressId2) throws Exception {
		serviceablilityHelper.updateEnabledForArea(0, 1);
		Thread.sleep(5000);		
	 Assert.assertEquals(sHelper.serviceabilityCheckCart(  restID,  area_id, itemId,  quantity).get(addressId),"0");
	}
	
	@Test(groups = { "regression", "Karunakar QE-479" }, priority = 0, dataProvider = "Cart", description="Verify the restaurant is serviceable in Cart when BF less than the stop BF")
	public void serviceabilityInCartInBFLessThanSBF( String restID, String zone_id,String area_id,String itemId, String quantity,String addressId, String addressId2) throws Exception {
		//Set BF less than STop BF
		Thread.sleep(5000);		
	 Assert.assertEquals(sHelper.serviceabilityCheckCart(  restID,  area_id, itemId,  quantity).get(addressId),"1");
	 //Set BF normal
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//Menu Api starts here 
	@Test(groups = { "regression", "Karunakar" }, priority = 0, dataProvider = "Menu", description="LastMile is less than the config")
	public void serviceabilityInMenu (String lat, String lng, String restID, String area_id) throws Exception {
		serviceablilityHelper.updateLastMileForArea(4, 1);
		Thread.sleep(5000);		
		Assert.assertEquals(sHelper.serviceabilityCheckMenu( lat,  lng,  restID,  area_id).toString(), "SERVICEABLE");
	}
	@Test(groups = { "regression", "Karunakar" }, priority = 0, dataProvider = "Menu", description="Verify the restaurant is not serviceable in Menu when last mile is greater than the config at area level")
	public void serviceabilityInMenu2 (String lat, String lng, String restID, String area_id) throws Exception {
		serviceablilityHelper.updateLastMileForArea(1, 1);
		Thread.sleep(5000);
		Assert.assertEquals(sHelper.serviceabilityCheckMenu(lat, lng, restID,area_id).toString(),"SERVICEABLE_WITH_BANNER");
	}
	@Test(groups = { "regression", "Karunakar" }, priority = 0, dataProvider = "Menu", description="Verify the restaurant is not serviceable  in Menu   when delivery time is more  than the max SLA")
	public void serviceabilityInMenu3(String lat, String lng, String restID, String zone_id) throws Exception {
		serviceablilityHelper.updateDeliveryTimeForZone(100, 1);
		Thread.sleep(5000);		
	    Assert.assertEquals(sHelper.serviceabilityCheckListing(lat, lng, restID).toString(),"SERVICEABLE_WITH_BANNER");
	}
	@Test(groups = { "regression", "Karunakar" }, priority = 0, dataProvider = "Menu", description="Verify the restaurant is not serviceable  in Menu   when delivery time is more  than the max SLA")
	public void serviceabilityInMenu4(String lat, String lng, String restID, String zone_id) throws Exception {
		serviceablilityHelper.updateDeliveryTimeForZone(10, 1);
		Thread.sleep(5000);		
	    Assert.assertEquals(sHelper.serviceabilityCheckListing(lat, lng, restID).toString(),"SERVICEABLE_WITH_BANNER");
	}
	@Test(groups = { "regression", "Karunakar" }, priority = 0, dataProvider = "Menu", description="Verify the restaurant is not serviceable  in Menu   when delivery time is more  than the max SLA")
	public void serviceabilityInMenu5(String lat, String lng, String restID, String zone_id) throws Exception {
		serviceablilityHelper.updateEnabledForZone(1, 1);
		Thread.sleep(5000);		
	    Assert.assertEquals(sHelper.serviceabilityCheckListing(lat, lng, restID).toString(),"SERVICEABLE_WITH_BANNER");
	}
	@Test(groups = { "regression", "Karunakar" }, priority = 0, dataProvider = "Menu", description="Verify the restaurant is not serviceable  in Menu   when delivery time is more  than the max SLA")
	public void serviceabilityInMenu6(String lat, String lng, String restID, String zone_id) throws Exception {
		serviceablilityHelper.updateEnabledForZone(0, 1);
		Thread.sleep(5000);		
	    Assert.assertEquals(sHelper.serviceabilityCheckListing(lat, lng, restID).toString(),"SERVICEABLE_WITH_BANNER");
	}
	@Test(groups = { "regression", "Karunakar" }, priority = 0, dataProvider = "Menu", description="Verify the restaurant is not serviceable  in Menu   when delivery time is more  than the max SLA")
	public void serviceabilityInMenu7(String lat, String lng, String restID, String zone_id) throws Exception {
		serviceablilityHelper.updateEnabledForArea(1, 1);
		Thread.sleep(5000);		
	    Assert.assertEquals(sHelper.serviceabilityCheckListing(lat, lng, restID).toString(),"SERVICEABLE_WITH_BANNER");
	}
	@Test(groups = { "regression", "Karunakar" }, priority = 0, dataProvider = "Menu", description="Verify the restaurant is not serviceable  in Menu   when delivery time is more  than the max SLA")
	public void serviceabilityInMenu8(String lat, String lng, String restID, String zone_id) throws Exception {
		serviceablilityHelper.updateEnabledForArea(0, 1);
		Thread.sleep(5000);		
	    Assert.assertEquals(sHelper.serviceabilityCheckListing(lat, lng, restID).toString(),"SERVICEABLE_WITH_BANNER");
	}
	
	@Test(groups = { "regression", "Karunakar" }, priority = 0, dataProvider = "Menu", description="Verify the restaurant is not serviceable  in Menu   when delivery time is more  than the max SLA")
	public void serviceabilityInMenu9(String lat, String lng, String restID, String zone_id) throws Exception {
		serviceablilityHelper.updateEnabledForRestaurant(1, restID);
				Thread.sleep(5000);		
	    Assert.assertEquals(sHelper.serviceabilityCheckListing(lat, lng, restID).toString(),"SERVICEABLE_WITH_BANNER");
	}
	@Test(groups = { "regression", "Karunakar" }, priority = 0, dataProvider = "Menu", description="Verify the restaurant is not serviceable  in Menu   when delivery time is more  than the max SLA")
	public void serviceabilityInMenu10(String lat, String lng, String restID, String zone_id) throws Exception {
		serviceablilityHelper.updateEnabledForRestaurant(0, restID);
		Thread.sleep(5000);		
	    Assert.assertEquals(sHelper.serviceabilityCheckListing(lat, lng, restID).toString(),"SERVICEABLE_WITH_BANNER");
	}
	
	
	@Test(dataProvider = "Cart1", enabled = true, groups = {"regression", "Somnath"}, description="Verify the restaurant is serviceable in Cart  when last mile is less than the config at restaurant level")
	public void serviceability456(String restID, String itemId, String quantity, String area_id, String addressId) throws Exception {
		serviceablilityHelper.updateMaxSecondMileForRestaurant(10, restID);
		Thread.sleep(5000);
		Assert.assertEquals(sHelper.serviceabilityCheckCart(restID, area_id, itemId, quantity).get(addressId).toString(), "1");
	}
	//low rain mode enabled and BF has crossed the stop bf of rain mode
	@Test(groups = { "regression", "Karunakar QE-402" }, priority = 0, dataProvider = "Cart", description="Verify the restaurant is serviceable  in Listing   when item count is less than the config")
	public void serviceabilityInMenuinLowRainBFMore( String lat, String lng, String restID, String zone_id) throws Exception {
		serviceablilityHelper.updateRainModeTypeForZone(1, 1);
		//set BF in redis less than stop bf of rain mode
		Thread.sleep(5000);		
		Assert.assertEquals(sHelper.serviceabilityCheckListing(lat, lng, restID).toString(), "SERVICEABLE");
	}

				
}

	

