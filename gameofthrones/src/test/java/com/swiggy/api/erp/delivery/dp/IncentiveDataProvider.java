package com.swiggy.api.erp.delivery.dp;

import org.testng.annotations.DataProvider;

import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;

import framework.gameofthrones.JonSnow.Processor;

public class IncentiveDataProvider {		
	static CheckoutHelper helperc = new CheckoutHelper();


	@DataProvider(name = "payoutData")
	public  Object[][] firstMileData() throws Exception {
		return new Object[][] {
				{DeliveryConstant.MayurDE, "223", "12.937651", "77.585953"},
		};
	}
	
	
	@DataProvider(name = "LastMile")
	public  Object[][] LastMileData() throws Exception {
		return new Object[][] {
				//{DeliveryConstant.snded_id, "223", "12.937651", "77.585953"},
			
			{DeliveryConstant.MayurDE, "223", "12.928422", "77.593104"},

		};
	}

	@DataProvider(name = "batchRuleData")
	public  Object[][] createRef2() throws Exception {
		String orderId1=createOrder();
		String orderId2=createOrder();
		return new Object[][] {
				{orderId1,orderId2,DeliveryConstant.MayurDE},
		};
	}
	public static String createOrder() throws Exception {
		Object[][] data = cartItems();
		Processor p = helperc.placeOrder(DeliveryConstant.mobile,
				DeliveryConstant.password, data[0][0].toString(),
				data[0][1].toString(), data[0][2].toString());
		return p.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.order_id");
	}

	public static Object[][] cartItems() {

		return new Object[][] { { DeliveryConstant.alchemist_item_id, "1", DeliveryConstant.alchemist_rest_id } };

	}
}


