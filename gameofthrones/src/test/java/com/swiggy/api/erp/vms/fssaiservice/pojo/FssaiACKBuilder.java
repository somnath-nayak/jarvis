package com.swiggy.api.erp.vms.fssaiservice.pojo;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"registered_name",
"acknowledgement_reference",
"address_line_1",
"address_line_2",
"official_city_id",
"zipcode",
"owner_name",
"owner_contact_number",
"application_date",
"restaurant_id",
"user_id",
"user_type",
"document_url"
})
public class FssaiACKBuilder {

@JsonProperty("registered_name")
private String registered_name;
@JsonProperty("acknowledgement_reference")
private String acknowledgement_reference;
@JsonProperty("address_line_1")
private String address_line_1;
@JsonProperty("address_line_2")
private String address_line_2;
@JsonProperty("official_city_id")
private Long official_city_id;
@JsonProperty("zipcode")
private String zipcode;
@JsonProperty("owner_name")
private String owner_name;
@JsonProperty("owner_contact_number")
private String owner_contact_number;
@JsonProperty("application_date")
private String application_date;
@JsonProperty("restaurant_id")
private Long restaurant_id;
@JsonProperty("user_id")
private String user_id;
@JsonProperty("user_type")
private String user_type;
@JsonProperty("document_url")
private String document_url;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

/**
* No args constructor for use in serialization
* 
*/
public FssaiACKBuilder() {
}

/**
* 
* @param owner_contact_number
* @param official_city_id
* @param owner_name
* @param address_line_1
* @param user_type
* @param registered_name
* @param application_date
* @param zipcode
* @param address_line_2
* @param restaurant_id
* @param acknowledgement_reference
* @param user_id
* @param document_url
*/
public FssaiACKBuilder(String registered_name, String acknowledgement_reference, String address_line_1, String address_line_2, Long official_city_id, String zipcode, String owner_name, String owner_contact_number, String application_date, Long restaurant_id, String user_id, String user_type, String document_url) {
super();
this.registered_name = registered_name;
this.acknowledgement_reference = acknowledgement_reference;
this.address_line_1 = address_line_1;
this.address_line_2 = address_line_2;
this.official_city_id = official_city_id;
this.zipcode = zipcode;
this.owner_name = owner_name;
this.owner_contact_number = owner_contact_number;
this.application_date = application_date;
this.restaurant_id = restaurant_id;
this.user_id = user_id;
this.user_type = user_type;
this.document_url = document_url;
}

@JsonProperty("registered_name")
public String getRegistered_name() {
return registered_name;
}

@JsonProperty("registered_name")
public void setRegistered_name(String registered_name) {
this.registered_name = registered_name;
}

public FssaiACKBuilder withRegistered_name(String registered_name) {
this.registered_name = registered_name;
return this;
}

@JsonProperty("acknowledgement_reference")
public String getAcknowledgement_reference() {
return acknowledgement_reference;
}

@JsonProperty("acknowledgement_reference")
public void setAcknowledgement_reference(String acknowledgement_reference) {
this.acknowledgement_reference = acknowledgement_reference;
}

public FssaiACKBuilder withAcknowledgement_reference(String acknowledgement_reference) {
this.acknowledgement_reference = acknowledgement_reference;
return this;
}

@JsonProperty("address_line_1")
public String getAddress_line_1() {
return address_line_1;
}

@JsonProperty("address_line_1")
public void setAddress_line_1(String address_line_1) {
this.address_line_1 = address_line_1;
}

public FssaiACKBuilder withAddress_line_1(String address_line_1) {
this.address_line_1 = address_line_1;
return this;
}

@JsonProperty("address_line_2")
public String getAddress_line_2() {
return address_line_2;
}

@JsonProperty("address_line_2")
public void setAddress_line_2(String address_line_2) {
this.address_line_2 = address_line_2;
}

public FssaiACKBuilder withAddress_line_2(String address_line_2) {
this.address_line_2 = address_line_2;
return this;
}

@JsonProperty("official_city_id")
public Long getOfficial_city_id() {
return official_city_id;
}

@JsonProperty("official_city_id")
public void setOfficial_city_id(Long official_city_id) {
this.official_city_id = official_city_id;
}

public FssaiACKBuilder withOfficial_city_id(Long official_city_id) {
this.official_city_id = official_city_id;
return this;
}

@JsonProperty("zipcode")
public String getZipcode() {
return zipcode;
}

@JsonProperty("zipcode")
public void setZipcode(String zipcode) {
this.zipcode = zipcode;
}

public FssaiACKBuilder withZipcode(String zipcode) {
this.zipcode = zipcode;
return this;
}

@JsonProperty("owner_name")
public String getOwner_name() {
return owner_name;
}

@JsonProperty("owner_name")
public void setOwner_name(String owner_name) {
this.owner_name = owner_name;
}

public FssaiACKBuilder withOwner_name(String owner_name) {
this.owner_name = owner_name;
return this;
}

@JsonProperty("owner_contact_number")
public String getOwner_contact_number() {
return owner_contact_number;
}

@JsonProperty("owner_contact_number")
public void setOwner_contact_number(String owner_contact_number) {
this.owner_contact_number = owner_contact_number;
}

public FssaiACKBuilder withOwner_contact_number(String owner_contact_number) {
this.owner_contact_number = owner_contact_number;
return this;
}

@JsonProperty("application_date")
public String getApplication_date() {
return application_date;
}

@JsonProperty("application_date")
public void setApplication_date(String application_date) {
this.application_date = application_date;
}

public FssaiACKBuilder withApplication_date(String application_date) {
this.application_date = application_date;
return this;
}

@JsonProperty("restaurant_id")
public Long getRestaurant_id() {
return restaurant_id;
}

@JsonProperty("restaurant_id")
public void setRestaurant_id(Long restaurant_id) {
this.restaurant_id = restaurant_id;
}

public FssaiACKBuilder withRestaurant_id(Long restaurant_id) {
this.restaurant_id = restaurant_id;
return this;
}

@JsonProperty("user_id")
public String getUser_id() {
return user_id;
}

@JsonProperty("user_id")
public void setUser_id(String user_id) {
this.user_id = user_id;
}

public FssaiACKBuilder withUser_id(String user_id) {
this.user_id = user_id;
return this;
}

@JsonProperty("user_type")
public String getUser_type() {
return user_type;
}

@JsonProperty("user_type")
public void setUser_type(String user_type) {
this.user_type = user_type;
}

public FssaiACKBuilder withUser_type(String user_type) {
this.user_type = user_type;
return this;
}

@JsonProperty("document_url")
public String getDocument_url() {
return document_url;
}

@JsonProperty("document_url")
public void setDocument_url(String document_url) {
this.document_url = document_url;
}

public FssaiACKBuilder withDocument_url(String document_url) {
this.document_url = document_url;
return this;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

public FssaiACKBuilder withAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
return this;
}

@Override
public String toString() {
return new ToStringBuilder(this).append("registered_name", registered_name).append("acknowledgement_reference", acknowledgement_reference).append("address_line_1", address_line_1).append("address_line_2", address_line_2).append("official_city_id", official_city_id).append("zipcode", zipcode).append("owner_name", owner_name).append("owner_contact_number", owner_contact_number).append("application_date", application_date).append("restaurant_id", restaurant_id).append("user_id", user_id).append("user_type", user_type).append("document_url", document_url).append("additionalProperties", additionalProperties).toString();
}

@Override
public int hashCode() {
return new HashCodeBuilder().append(official_city_id).append(owner_name).append(user_type).append(application_date).append(zipcode).append(restaurant_id).append(acknowledgement_reference).append(owner_contact_number).append(address_line_1).append(additionalProperties).append(registered_name).append(address_line_2).append(user_id).append(document_url).toHashCode();
}

@Override
public boolean equals(Object other) {
if (other == this) {
return true;
}
if ((other instanceof FssaiACKBuilder) == false) {
return false;
}
FssaiACKBuilder rhs = ((FssaiACKBuilder) other);
return new EqualsBuilder().append(official_city_id, rhs.official_city_id).append(owner_name, rhs.owner_name).append(user_type, rhs.user_type).append(application_date, rhs.application_date).append(zipcode, rhs.zipcode).append(restaurant_id, rhs.restaurant_id).append(acknowledgement_reference, rhs.acknowledgement_reference).append(owner_contact_number, rhs.owner_contact_number).append(address_line_1, rhs.address_line_1).append(additionalProperties, rhs.additionalProperties).append(registered_name, rhs.registered_name).append(address_line_2, rhs.address_line_2).append(user_id, rhs.user_id).append(document_url, rhs.document_url).isEquals();
}

}