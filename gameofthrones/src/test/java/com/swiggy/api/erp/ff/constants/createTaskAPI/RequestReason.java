package com.swiggy.api.erp.ff.constants.createTaskAPI;

import java.util.Arrays;
import java.util.Optional;

public enum RequestReason {

    ORDER_DECLINED_BY_RESTAURANT("Order Declined By Restaurant", 1),
    MANUAL_PLACING_ORDER("Manual Placing Order", 2),
    CANCELLATION_REQUIRED("Cancellation required", 3),
    VENDOR_UNAVAILABLE("Vendor Unavailable", 5),
    PLACING_DELAY("Placing Delay", 12),
    PLACING_STATE_MISMATCH("Placing Status Mismatch", 16),
    PARTNER_CALL_REQUEST("Partner Call Request", 18),
    VENDOR_UNABLE_TO_ACCEPT_ORDER("Vendor unable to accept order", 30);

    private final String description;
    private final int id;

    RequestReason(String description, int id) {
        this.description = description;
        this.id = id;
    }

    public static Optional<RequestReason> findById(int id) {

        return Arrays.stream(RequestReason.values())
                .filter(x -> x.getId() == id)
                .findFirst();
    }

    public static Optional<RequestReason> getEnum(String reason) {

        for (RequestReason requestReason : RequestReason.values()) {
            if (requestReason.name().equals(reason)) {
                return Optional.of(requestReason);
            }
        }
        return Optional.empty();
    }

    public String toString() {
        return this.description;
    }

    public int getId() {
        return this.id;
    }
}

