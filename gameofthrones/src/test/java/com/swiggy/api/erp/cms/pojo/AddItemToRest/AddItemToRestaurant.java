package com.swiggy.api.erp.cms.pojo.AddItemToRest;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class AddItemToRestaurant {
    @JsonProperty("description")
    private String description;
    @JsonProperty("enabled")
    private Integer enabled;
    @JsonProperty("in_stock")
    private Integer inStock;
    @JsonProperty("is_perishable")
    private Integer isPerishable;
    @JsonProperty("is_spicy")
    private Integer isSpicy;
    @JsonProperty("is_veg")
    private Integer isVeg;
    @JsonProperty("name")
    private String name;
    @JsonProperty("packing_charges")
    private Integer packingCharges;
    @JsonProperty("packing_slab_count")
    private Integer packingSlabCount;
    @JsonProperty("price")
    private Integer price;
    @JsonProperty("serves_how_many")
    private Integer servesHowMany;
    @JsonProperty("service_charges")
    private String serviceCharges;
    @JsonProperty("addons_and_variant_change_request")
    private String addonsAndVariantChangeRequest;
    @JsonProperty("item_change_request")
    private String itemChangeRequest;
    @JsonProperty("s3_image_url")
    private String s3ImageUrl;
    @JsonProperty("main_category_id")
    private Integer mainCategoryId;
    @JsonProperty("category_id")
    private String categoryId;
    @JsonProperty("restaurant_id")
    private Integer restaurantId;
    @JsonProperty("vat")
    private Integer vat;
    @JsonProperty("service_tax")
    private Integer serviceTax;
    @JsonProperty("addon_free_limit")
    private Integer addon_free_limit;
    @JsonProperty("addon_limit")
    private Integer addon_limit;
    @JsonProperty("order")
    private Integer order;
    @JsonProperty("pop_usage_type")
    private String popUsageType;
    @JsonProperty("image_id")
    private String imageId;
    @JsonProperty("image_url")
    private String imageUrl;
    private Metadata metadata;
    private List<Ticket> tickets = null;
    @JsonProperty("pop_usage_type")
    private String pop_usage_type;

    @JsonProperty("type")
    private String type;

    @JsonProperty("image_id")
    private String image_id;


    /**
     * No args constructor for use in serialization
     *
     */
    public AddItemToRestaurant() {
    }


    public AddItemToRestaurant(String description, Integer enabled, Integer inStock ,Integer isPerishable,Integer isSpicy, Integer isVeg, String name, Integer packingCharges, Integer packingSlabCount, Integer price, Integer servesHowMany, String serviceCharges, String addonsAndVariantChangeRequest, String itemChangeRequest, String s3ImageUrl, Integer mainCategoryId, String categoryId,Metadata metadata, List<Ticket> tickets, Integer restaurantId, Integer vat, Integer serviceTax, Integer addon_free_limit, Integer addon_limit, Integer order, String type, String popUsageType, String imageId, String imageUrl) {
        super();
        this.description = description;
        this.enabled = enabled;
        this.inStock = inStock;
        this.isPerishable = isPerishable;
        this.isSpicy = isSpicy;
        this.isVeg = isVeg;
        this.name = name;
        this.packingCharges = packingCharges;
        this.packingSlabCount = packingSlabCount;
        this.price = price;
        this.servesHowMany = servesHowMany;
        this.serviceCharges = serviceCharges;
        this.addonsAndVariantChangeRequest = addonsAndVariantChangeRequest;
        this.itemChangeRequest = itemChangeRequest;
        this.s3ImageUrl = s3ImageUrl;
        this.mainCategoryId = mainCategoryId;
        this.categoryId = categoryId;
        this.metadata = metadata;
        this.tickets = tickets;
        this.restaurantId = restaurantId;
        this.vat = vat;
        this.serviceTax = serviceTax;
        this.addon_free_limit = addon_free_limit;
        this.addon_limit = addon_limit;
        this.order = order;
        this.type = type;
        this.popUsageType = popUsageType;
        this.imageId = imageId;
        this.imageUrl = imageUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getEnabled() {
        return enabled;
    }

    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }

    public Integer getInStock() {
        return inStock;
    }

    public void setInStock(Integer in_stock) {
        this.inStock = in_stock;
    }

    public Integer getIsPerishable() {
        return isPerishable;
    }

    public void setIsPerishable(Integer is_perishable) {
        this.isPerishable = is_perishable;
    }

    public Integer getIsSpicy() {
        return isSpicy;
    }

    public void setIsSpicy(Integer isSpicy) {
        this.isSpicy = isSpicy;
    }

    public Integer getIsVeg() {
        return isVeg;
    }

    public void setIsVeg(Integer isVeg) {
        this.isVeg = isVeg;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPackingCharges() {
        return packingCharges;
    }

    public void setPackingCharges(Integer packingCharges) {
        this.packingCharges = packingCharges;
    }

    public Integer getPackingSlabCount() {
        return packingSlabCount;
    }

    public void setPackingSlabCount(Integer packingSlabCount) {
        this.packingSlabCount = packingSlabCount;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getServesHowMany() {
        return servesHowMany;
    }

    public void setServesHowMany(Integer servesHowMany) {
        this.servesHowMany = servesHowMany;
    }

    public String getServiceCharges() {
        return serviceCharges;
    }

    public void setServiceCharges(String serviceCharges) {
        this.serviceCharges = serviceCharges;
    }

    public String getAddonsAndVariantChangeRequest() {
        return addonsAndVariantChangeRequest;
    }

    public void setAddonsAndVariantChangeRequest(String addonsAndVariantChangeRequest) {
        this.addonsAndVariantChangeRequest = addonsAndVariantChangeRequest;
    }

    public String getItemChangeRequest() {
        return itemChangeRequest;
    }

    public void setItemChangeRequest(String itemChangeRequest) {
        this.itemChangeRequest = itemChangeRequest;
    }

    public String getS3ImageUrl() {
        return s3ImageUrl;
    }

    public void setS3ImageUrl(String s3ImageUrl) {
        this.s3ImageUrl = s3ImageUrl;
    }

    public Integer getMainCategoryId() {
        return mainCategoryId;
    }

    public void setMainCategoryId(Integer mainCategoryId) {
        this.mainCategoryId = mainCategoryId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }

    public Integer getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(Integer restaurantId) {
        this.restaurantId = restaurantId;
    }

    public Integer getVat() {
        return vat;
    }

    public void setVat(Integer vat) {
        this.vat = vat;
    }

    public Integer getServiceTax() {
        return serviceTax;
    }

    public void setServiceTax(Integer serviceTax) {
        this.serviceTax = serviceTax;
    }

    public Integer getAddon_free_limit() {
        return addon_free_limit;
    }

    public void setAddon_free_limit(Integer addon_free_limit) {
        this.addon_free_limit = addon_free_limit;
    }

    public Integer getAddon_limit() {
        return addon_limit;
    }

    public void setAddon_limit(Integer addon_limit) {
        this.addon_limit = addon_limit;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public String getPopUsageType() {
        return popUsageType;
    }

    public void setPopUsageType(String popUsageType) {
        this.popUsageType = popUsageType;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("description", description).append("enabled", enabled).append("in_stock", inStock).append("is_perishable", isPerishable).append("isSpicy", isSpicy).append("isVeg", isVeg).append("name", name).append("packingCharges", packingCharges).append("packingSlabCount", packingSlabCount).append("price", price).append("servesHowMany", servesHowMany).append("serviceCharges", serviceCharges).append("addonsAndVariantChangeRequest", addonsAndVariantChangeRequest).append("itemChangeRequest", itemChangeRequest).append("s3ImageUrl", s3ImageUrl).append("mainCategoryId", mainCategoryId).append("categoryId", categoryId).append("metadata", metadata).append("tickets", tickets).append("restaurant_id",restaurantId).append("vat",vat).append("service_tax", serviceTax).append("addon_free_limit", addon_free_limit).append("addon_limit", addon_limit).append("order", order).append("type", type).append("pop_usage_type", popUsageType).append("image_id", imageId).append("image_url", imageUrl).toString();

    }


    public void setType(String type) {
        this.type = type;
    }


    public void setpop_usage_type(String pop_usage_type) {
        this.pop_usage_type = pop_usage_type;
    }

    public String getType() {
        return type;
    }


    public String getpop_type() {
        return pop_usage_type;
    }

    public void setImageId(String image_id) {
        this.image_id=image_id;

    }

    public String getImageId() {
        return image_id;
    }

}