package com.swiggy.api.erp.cms.tests;

import com.swiggy.api.erp.cms.dp.SelfServeV1Dp;
import com.swiggy.api.erp.cms.helper.SelfServeHelper;
import org.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SelfServeV1Delete extends SelfServeV1Dp {
    SelfServeHelper sshelper= new SelfServeHelper();

    @Test(dataProvider="deleteCategory", priority=2, groups={"Sanity_TC", "Smoke_TC", "Regression_TC"},
            description="delete category")
    public void deleteCategory(String restId,String catId,String responsemessage)
            throws JSONException, InterruptedException
    {

        String response = sshelper.deleteCategory(restId,catId).ResponseValidator.GetBodyAsText();
        if (responsemessage==null)
        {
            Assert.assertEquals("true",response,"Success");
            SelfServeHelper.setCategoryActive(restId,catId);
        }
        else
        {
            Assert.assertEquals(responsemessage, response, "Success");
        }

    }

}

