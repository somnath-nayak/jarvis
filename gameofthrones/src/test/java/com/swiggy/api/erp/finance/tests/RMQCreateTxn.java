package com.swiggy.api.erp.finance.tests;


import com.swiggy.api.erp.finance.helper.CashMgmtDBhelper;
import com.swiggy.api.erp.finance.helper.CashMgmtHelper;

import org.testng.Assert;

import org.testng.annotations.AfterTest;

import org.testng.annotations.Test;
import java.io.IOException;
import java.util.List;

public class RMQCreateTxn{
    CashMgmtDBhelper cmdh=new CashMgmtDBhelper();
    CashMgmtHelper cmh=new CashMgmtHelper();
    CashMgmtBulkTxnTest cmbtt=new CashMgmtBulkTxnTest();


    //Gettting DE list to validate RMQ json push
    @Test(priority = 0)
    public void getDEFromDB(){

        List<String> dlist=cmbtt.getDE();
        System.out.println(dlist);
    }

    @Test(priority = 1,invocationCount = 1)
    public void TestPushOrderstatusJsonRMQ() throws IOException {
        //getting list of DE's from DB
        List<String> dlist=cmbtt.getDE();
        for(int i=0;i<dlist.size();i++) {
            System.out.println("Adding the txn for DE =>"+dlist.get(i));
            String de_id=dlist.get(i);

            //Getting order id
            String order_id=cmh.getOrderId();

            //Passing json value to update order status( pickedup) json
            cmh.pushPickedupJsonRMQ("app",order_id,de_id, "pickedup", "50", "50", "50", "50", "50");

            //Validating that orders are getting created in DB or not
            String orderIdInDB=cmdh.transactionValidatorFromDB(de_id,order_id,"CASH_SPENDING").toString();
            Assert.assertEquals(order_id,orderIdInDB);


            // Passing json value to update order status ( delivered) json
            cmh.pushDeliveredJsonRMQ("app",order_id,de_id, "delivered", "50", "50", "50", "50", "50");

            //Validating that orders are getting created in DB or not
            String orderIdInDB2=cmdh.transactionValidatorFromDB(de_id,order_id,"CASH_INCOMING").toString();
            Assert.assertEquals(order_id,orderIdInDB2);
        }
    }

    @Test(priority = 2,invocationCount = 1)
    public void TestPushOrderstatusFFSideRMQ() throws IOException {
        //getting list of DE's from DB
        List<String> dlist=cmbtt.getDE();
        for(int i=0;i<dlist.size();i++) {
            System.out.println("Adding the txn for DE =>"+dlist.get(i));
            String de_id=dlist.get(i);

            //Getting order id
            String order_id=cmh.getOrderId();

            //Passing json value to update order status( pickedup) json
            cmh.pushPickedupJsonRMQ("dashboard",order_id,de_id, "pickedup", "50", "50", "50", "50", "50");

            //Validating that orders are getting created in DB or not
            String orderIdInDB=cmdh.transactionValidatorFromDB(de_id,order_id,"CASH_SPENDING").toString();
            Assert.assertEquals(order_id,orderIdInDB);


            // Passing json value to update order status ( delivered) json
            cmh.pushDeliveredJsonRMQ("dashboard",order_id,de_id, "delivered", "50", "50", "50", "50", "50");

            //Validating that orders are getting created in DB or not
            String orderIdInDB2=cmdh.transactionValidatorFromDB(de_id,order_id,"CASH_INCOMING").toString();
            Assert.assertEquals(order_id,orderIdInDB2);
        }
    }



    @AfterTest
    public void makeFCZeroAndCloseSessionForRMQ() throws InterruptedException {
        cmbtt.makeFCZero();
        cmbtt.closeSessionInBulk();
    }

}

