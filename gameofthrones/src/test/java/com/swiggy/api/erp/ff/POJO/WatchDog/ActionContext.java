package com.swiggy.api.erp.ff.POJO.WatchDog;

public class ActionContext {

        private String headers;

        private Body body;

        private String args;

        private String type;

        private String url;

        public String getHeaders ()
        {
            return headers;
        }

        public void setHeaders (String headers)
        {
            this.headers = headers;
        }

        public Body getBody ()
        {
            return body;
        }

        public void setBody (Body body)
        {
            this.body = body;
        }

        public String getArgs ()
        {
            return args;
        }

        public void setArgs (String args)
        {
            this.args = args;
        }

        public String getType ()
        {
            return type;
        }

        public void setType (String type)
        {
            this.type = type;
        }

        public String getUrl ()
        {
            return url;
        }

        public void setUrl (String url)
        {
            this.url = url;
        }

        @Override
        public String toString ()
        {
            return "ClassPojo [headers = " + headers + ", body = " + body + ", args = " + args + ", type = " + type + ", url = " + url + "]";
        }

    }
