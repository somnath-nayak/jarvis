package com.swiggy.api.erp.cms.pojo.FullMenu;

import com.swiggy.api.erp.cms.constants.MenuConstants;

import java.util.Arrays;

/**
 * Created by kiran.j on 2/7/18.
 */
public class Addon_Groups {

    private String id;

    private String name;

    private int order;

    private int addon_limit;

    private int addon_free_limit;

    private int addon_min_limit;

    private Addons[] addons;

    private Item_Slots[] item_slots;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public int getAddon_limit() {
        return addon_limit;
    }

    public void setAddon_limit(int addon_limit) {
        this.addon_limit = addon_limit;
    }

    public int getAddon_free_limit() {
        return addon_free_limit;
    }

    public void setAddon_free_limit(int addon_free_limit) {
        this.addon_free_limit = addon_free_limit;
    }

    public int getAddon_min_limit() {
        return addon_min_limit;
    }

    public void setAddon_min_limit(int addon_min_limit) {
        this.addon_min_limit = addon_min_limit;
    }

    public Addons[] getAddons() {
        return addons;
    }

    public void setAddons(Addons[] addons) {
        this.addons = addons;
    }

    public Item_Slots[] getItem_slots() {
        return item_slots;
    }

    public void setItem_slots(Item_Slots[] item_slots) {
        this.item_slots = item_slots;
    }

    public Addon_Groups build() {
        setDefaultValues();
        return this;
    }

    public void setDefaultValues() {
        Addons addons = new Addons();
        addons.build();
        if(this.getId() == null)
            this.setId(MenuConstants.addons_groups_id);
        if(this.getName() == null)
            this.setName(MenuConstants.addons_groups_name);
        if(this.getOrder() == 0)
            this.setOrder(MenuConstants.addons_groups_order);
        if(this.getAddon_limit() == 0)
            this.setAddon_limit(MenuConstants.addon_limit);
        if(this.getAddon_min_limit() == 0)
            this.setAddon_min_limit(MenuConstants.addon_min_limit);
        if(this.getAddon_free_limit() == 0)  
        	 this.setAddon_free_limit(MenuConstants.addon_free_limit);
        this.setAddons(new Addons[]{addons});

    }
    
    public void setAddonGroupBlank() {
        Addons addons = new Addons();
        addons.setAddonBlank();
        if(this.getId() == null)
            this.setId("");
        if(this.getName() == null)
            this.setName("");
        this.setAddons(new Addons[]{addons});

    }

    @Override
    public String toString() {
        return "{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", order=" + order +
                ", addon_limit=" + addon_limit +
                ", addon_free_limit='" + addon_free_limit + '\'' +
                ", addon_min_limit=" + addon_min_limit +
                ", addons=" + Arrays.toString(addons) +
                ", item_slots=" + Arrays.toString(item_slots) +
                '}';
    }
}
