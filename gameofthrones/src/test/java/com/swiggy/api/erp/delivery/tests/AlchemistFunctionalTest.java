package com.swiggy.api.erp.delivery.tests;

import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.erp.delivery.helper.AlchemistKafkaEventHelper;
import com.swiggy.api.erp.delivery.helper.AlchemistRuleHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryUtils;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Tyrion.KafkaHelper;
import framework.gameofthrones.Tyrion.RedisHelper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.*;

public class AlchemistFunctionalTest {

    final private String KAFKA_INCENTIVE_TOPIC_NAME = "incentive_order_data";
    final private String KAFKA_TICKETING_TOPIC_NAME = "ticketing_incentive_update";
    final private String KAFKA_INCENTIVE_SERVER_NAME = "de-kafka-alchemist";
    private int RetryCounter;

    AlchemistRuleHelper ruleHelper = null;
    KafkaHelper kafkaHelper = new KafkaHelper();
    ArrayList<Integer> rulesApplied = null;
    ArrayList<Integer> dryRunComputedRuleIds = null;
    Boolean ReadRulesFromDBFlag = false;
    Boolean SendPayoutToDE;
    AlchemistKafkaEventHelper alchemistKafkaEventHelper = null;
    HashMap<String, String> masterRuleDataTCMap = null;
    HashMap<String, String> customOrderDetailsJSONMap = null;
    HashMap<String, String> ticketingdetailsJSONMap=null;
    HashMap<String, String> createNewRuleMap = null;
    String eventJSON = null;

    @BeforeSuite
    public void DisableAllActiveRules() {
        alchemistKafkaEventHelper = new AlchemistKafkaEventHelper();
        kafkaHelper.createKafkaProducer(KAFKA_INCENTIVE_SERVER_NAME);
//        if(!ReadRulesFromDBFlag)
//            ruleHelper.disableAllActiveRulesWhichAreActivateBeforeSuiteExecution();

    }

    @AfterSuite
    public void EnableAllActiveRules() {
//        if(!ReadRulesFromDBFlag)
//            ruleHelper.enableAllRulesWhichWereDeactivatedBeforeSuiteExecution();
        alchemistKafkaEventHelper.printTestSuiteResultJSON();
    }

    @AfterMethod
    public void DisableRuleCreatedByCurrentTestCase() {
        ruleHelper.disableRulesCreatedByCurrentTestCase();
        ruleHelper = null;
        rulesApplied = null;
        eventJSON = null;
        dryRunComputedRuleIds = null;
        ReadRulesFromDBFlag = false;
        masterRuleDataTCMap = null;
    }

    @DataProvider(name = "GoldenRules")
    public Object[][] data() {
        Object[][] objectData = DeliveryUtils.getTestsInSheet("goldenset");
        LinkedHashMap<String, String> map = null;
        int numberOfTestCasesEnabled = 0;
        for (int counter = 0; counter < objectData.length; counter++) {
            map = (LinkedHashMap<String, String>) objectData[counter][1];
            if (Boolean.parseBoolean(map.get("enabled"))) {
                numberOfTestCasesEnabled++;
            }
        }
        Object[][] testCases = new Object[numberOfTestCasesEnabled][objectData[0].length];
        int index = 0;
        for (int counter = 0; counter < objectData.length; counter++) {
            map = (LinkedHashMap<String, String>) objectData[counter][1];
            if (Boolean.parseBoolean(map.get("enabled"))) {
                testCases[index][1] = objectData[counter][1];
                index++;
            }
        }
        return testCases;
    }

    @Test(dataProvider = "GoldenRules",enabled = true, description = "This test cases pulls golden set of rules from excel and executed tests on it")
    public void verifyPayoutfortheGoldenSetOfRules(String ID, LinkedHashMap<String, String> objectData) {
        String TC_ID = objectData.get("TC_ID");
        System.out.println("**************** Test Case Id " + TC_ID + " STARTS HERE****************");
        System.out.println("Test Description " + objectData.get("TC_Description"));
        String zone, setCAPRD_To_state;
        Boolean CancelOrder;
        zone = objectData.get("zoneID");
        Double a = Double.valueOf((zone));
        int zoneId = a.intValue();
        setCAPRD_To_state = objectData.get("setCAPRDToState");
        CancelOrder = Boolean.parseBoolean(objectData.get("cancelOrder"));
        Boolean RejectOrder = Boolean.parseBoolean(objectData.get("rejectOrder"));
        SendPayoutToDE = Boolean.valueOf(objectData.get("sendPayoutToDE"));
        masterRuleDataTCMap = new HashMap<>();
        ruleHelper = new AlchemistRuleHelper();

        createNewRuleMap = new HashMap<>();
        createNewRuleMap.put("zone", String.valueOf(zoneId));
        createNewRuleMap.put("computeOn", objectData.get("computeOn"));
        createNewRuleMap.put("tag", objectData.get("ruleTags"));
        createNewRuleMap.put("start", objectData.get("ruleValidityStartTime"));
        createNewRuleMap.put("end", objectData.get("ruleValidityEndTime"));
        createNewRuleMap.put("expression", objectData.get("expression"));
        createNewRuleMap.put("orderJsonValues", objectData.get("orderJsonValues"));

        masterRuleDataTCMap = ruleHelper.createRulesWithMap(createNewRuleMap);
        masterRuleDataTCMap.put("SendPayoutToDE", String.valueOf(SendPayoutToDE));
        masterRuleDataTCMap.put("TC_ID", TC_ID);

        customOrderDetailsJSONMap = new HashMap<>();
        preOrderChecks();

        alchemistKafkaEventHelper.createNewDEIDGenerator(zoneId);

        eventJSON = alchemistKafkaEventHelper.returnNewOrderJSONForAlchemistKafka(customOrderDetailsJSONMap);
        String key = alchemistKafkaEventHelper.getBatchId();
        sendEventToKafka(key, eventJSON);

        customOrderDetailsJSONMap.put("order_status", "assigned");
        eventJSON = alchemistKafkaEventHelper.returnOrderStatusUpdateJSONForAlchemistKafka(customOrderDetailsJSONMap);
        sendEventToKafka(key, eventJSON);

        eventJSON = alchemistKafkaEventHelper.returnTaskSequenceJSONForAlchemistKafka(customOrderDetailsJSONMap);
        sendEventToKafka(key, eventJSON);

//      if(verifyDryRunHappened())
        doCAPRDViaKafkaEventsPush(key, eventJSON, setCAPRD_To_state);

        if (CancelOrder) {
            eventJSON = alchemistKafkaEventHelper.returnOrderCancelJSONForAlchemistKafka();
            sendEventToKafka(key, eventJSON);
        }

        if (RejectOrder) {
            customOrderDetailsJSONMap.put("order_status", "rejected");
            eventJSON = alchemistKafkaEventHelper.returnRejectedStatusUpdateJSONForAlchemistKafka(customOrderDetailsJSONMap);
            sendEventToKafka(key, eventJSON);
        }

        verifyRuleValidationsAsPerTestData();
        alchemistKafkaEventHelper.convertTestCaseResultstoJSON(masterRuleDataTCMap);
    }

    @Test(enabled = false, description = "Test case is used during manual bug validation, where ask is to test based on custom data")
    public void verifyPayoutfortheCustomTestDateandRules() {
        String TC_ID = "1";
        String zone, expression, ComputeOn, setCAPRD_To_state, ruleValidityStartTime, ruleValidityEndTime, ruleTags, orderJsonValues;
        Boolean CancelOrder, RejectOrder, runTicketing;
        zone = "1";
        Double a = Double.valueOf((zone));
        int zoneId = a.intValue();
        expression = "batch_count < 2";
        ComputeOn = "delivered";
        setCAPRD_To_state = "delivered";
        CancelOrder = Boolean.parseBoolean("false");
        runTicketing = true;
        SendPayoutToDE = Boolean.valueOf("true");
        ruleValidityStartTime = "2018-08-17 1:1:1";
        ruleValidityEndTime = "2019-09-30 1:1:1";
        ruleTags = "Per Order";
        orderJsonValues = "batch_count=1";
        HashMap<String, String> createNewRuleMap = new HashMap<>();
        createNewRuleMap.put("zone", zone);
        createNewRuleMap.put("computeOn", ComputeOn);
        createNewRuleMap.put("tag", ruleTags);
        createNewRuleMap.put("start", ruleValidityStartTime);
        createNewRuleMap.put("end", ruleValidityEndTime);
        createNewRuleMap.put("expression", expression);
        createNewRuleMap.put("orderJsonValues", orderJsonValues);

        RejectOrder = false;
        masterRuleDataTCMap = new HashMap<>();
        ruleHelper = new AlchemistRuleHelper();

        if (ReadRulesFromDBFlag)
            masterRuleDataTCMap = ruleHelper.verifyActiveRuleInDB(expression);
        else {
            masterRuleDataTCMap = ruleHelper.createRulesWithMap(createNewRuleMap);
        }

        masterRuleDataTCMap.put("TC_ID", TC_ID);
        masterRuleDataTCMap.put("SendPayoutToDE", String.valueOf(SendPayoutToDE));
        customOrderDetailsJSONMap = new HashMap<>();
        preOrderChecks();

        alchemistKafkaEventHelper.createNewDEIDGenerator(Integer.parseInt(masterRuleDataTCMap.get("zone_id")));
        String eventJSON = alchemistKafkaEventHelper.returnNewOrderJSONForAlchemistKafka(customOrderDetailsJSONMap);
        String key = alchemistKafkaEventHelper.getBatchId();
        sendEventToKafka(key, eventJSON);

        customOrderDetailsJSONMap.put("order_status", "assigned");
        eventJSON = alchemistKafkaEventHelper.returnOrderStatusUpdateJSONForAlchemistKafka(customOrderDetailsJSONMap);
        sendEventToKafka(key, eventJSON);

        eventJSON = alchemistKafkaEventHelper.returnTaskSequenceJSONForAlchemistKafka(customOrderDetailsJSONMap);
        sendEventToKafka(key, eventJSON);

        if(verifyDryRunHappened())
        doCAPRDViaKafkaEventsPush(key, eventJSON, setCAPRD_To_state);

        if (CancelOrder) {
            eventJSON = alchemistKafkaEventHelper.returnOrderCancelJSONForAlchemistKafka();
            sendEventToKafka(key, eventJSON);
        }

        if (RejectOrder) {
            customOrderDetailsJSONMap.put("order_status", "rejected");
            eventJSON = alchemistKafkaEventHelper.returnRejectedStatusUpdateJSONForAlchemistKafka(customOrderDetailsJSONMap);
            sendEventToKafka(key, eventJSON);
        }

        verifyRuleValidationsAsPerTestData();
        verifyAggregationsInPerformanceTables();
        if (runTicketing) {

        }
        alchemistKafkaEventHelper.convertTestCaseResultstoJSON(masterRuleDataTCMap);
    }

    @Test(enabled = true, description = "Verify payout happens to correct DE when order earlier was assigned to DE who rejected and later reassigned to a new DE wo delivers the order")
    public void verifyRejectedOrderAssignedToNewDEAndPayout() {
        String TC_ID = "1";
        String zone, expression, ComputeOn, setCAPRD_To_state, ruleValidityStartTime, ruleValidityEndTime, ruleTags, orderJsonValues;
        Boolean CancelOrder, RejectOrder, runTicketing;
        zone = "1";
        Double a = Double.valueOf((zone));
        int zoneId = a.intValue();
        expression = "batch_count > 1";
        ComputeOn = "delivered";
        setCAPRD_To_state = "delivered";
        CancelOrder = Boolean.parseBoolean("false");
        runTicketing = true;

        SendPayoutToDE = Boolean.valueOf("true");
        ruleValidityStartTime = "2018-08-17 1:1:1.0";
        ruleValidityEndTime = "2019-09-30 1:1:1.0";
        ruleTags = "Per Order";
        orderJsonValues = "batch_count=2";
        HashMap<String, String> createNewRuleMap = new HashMap<>();
        createNewRuleMap.put("zone", zone);
        createNewRuleMap.put("computeOn", ComputeOn);
        createNewRuleMap.put("tag", ruleTags);
        createNewRuleMap.put("start", ruleValidityStartTime);
        createNewRuleMap.put("end", ruleValidityEndTime);
        createNewRuleMap.put("expression", expression);
        createNewRuleMap.put("orderJsonValues", orderJsonValues);

        masterRuleDataTCMap = new HashMap<>();
        ruleHelper = new AlchemistRuleHelper();

        if (ReadRulesFromDBFlag)
            masterRuleDataTCMap = ruleHelper.verifyActiveRuleInDB(expression);
        else {
            masterRuleDataTCMap = ruleHelper.createRulesWithMap(createNewRuleMap);
        }

        RejectOrder = true;
        masterRuleDataTCMap.put("TC_ID", TC_ID);
        masterRuleDataTCMap.put("SendPayoutToDE", String.valueOf(SendPayoutToDE));
        customOrderDetailsJSONMap = new HashMap<>();
        preOrderChecks();

        alchemistKafkaEventHelper.createNewDEIDGenerator(Integer.parseInt(masterRuleDataTCMap.get("zone_id")));
        String eventJSON = alchemistKafkaEventHelper.returnNewOrderJSONForAlchemistKafka(customOrderDetailsJSONMap);
        String key = alchemistKafkaEventHelper.getBatchId();
        sendEventToKafka(key, eventJSON);

        alchemistKafkaEventHelper.createNewDEIDGenerator(Integer.parseInt(masterRuleDataTCMap.get("zone_id")));
        customOrderDetailsJSONMap.put("order_status", "assigned");
        eventJSON = alchemistKafkaEventHelper.returnOrderStatusUpdateJSONForAlchemistKafka(customOrderDetailsJSONMap);
        sendEventToKafka(key, eventJSON);

        eventJSON = alchemistKafkaEventHelper.returnTaskSequenceJSONForAlchemistKafka(customOrderDetailsJSONMap);
        sendEventToKafka(key, eventJSON);

        if (RejectOrder) {
            customOrderDetailsJSONMap.put("order_status", "rejected");
            eventJSON = alchemistKafkaEventHelper.returnRejectedStatusUpdateJSONForAlchemistKafka(customOrderDetailsJSONMap);
            sendEventToKafka(key, eventJSON);
        }

        verifyAggregationsInPerformanceTables();

        alchemistKafkaEventHelper.createNewDEIDGenerator(Integer.parseInt(masterRuleDataTCMap.get("zone_id")));
        customOrderDetailsJSONMap.put("order_status", "assigned");
        eventJSON = alchemistKafkaEventHelper.returnOrderStatusUpdateJSONForAlchemistKafka(customOrderDetailsJSONMap);
        sendEventToKafka(key, eventJSON);

        eventJSON = alchemistKafkaEventHelper.returnTaskSequenceJSONForAlchemistKafka(customOrderDetailsJSONMap);
        sendEventToKafka(key, eventJSON);

        doCAPRDViaKafkaEventsPush(key, eventJSON, setCAPRD_To_state);

      if (CancelOrder) {
          eventJSON = alchemistKafkaEventHelper.returnOrderCancelJSONForAlchemistKafka();
          sendEventToKafka(key, eventJSON);
      }

      verifyRuleValidationsAsPerTestData();
      verifyAggregationsInPerformanceTables();
      if (runTicketing) {
      }

      alchemistKafkaEventHelper.convertTestCaseResultstoJSON(masterRuleDataTCMap);
    }

    @Test(enabled = true, description = "Verify Ticketing is paying out correct amount to DE when DE creates a ticket after he delivers order")
    public void verifyOrderDeliveredAndTicketingRanLater() {
        String TC_ID = "1";
        String zone, expression, ComputeOn, setCAPRD_To_state, ruleValidityStartTime, ruleValidityEndTime, ruleTags, orderJsonValues;
        Boolean CancelOrder, RejectOrder, runTicketingFlag;
        zone = "1";
        Double a = Double.valueOf((zone));
        int zoneId = a.intValue();
        expression = "batch_count=1";
        ComputeOn = "delivered";
        setCAPRD_To_state = "delivered";
        CancelOrder = Boolean.parseBoolean("false");
        runTicketingFlag = true;
        SendPayoutToDE = Boolean.valueOf("true");
        ruleValidityStartTime = "2018-08-17 1:1:1";
        ruleValidityEndTime = "2019-09-30 1:1:1";
        ruleTags = "Per Order";
        orderJsonValues = "batch_count=1";
        createNewRuleMap = new HashMap<>();
        createNewRuleMap.put("zone", zone);
        createNewRuleMap.put("computeOn", ComputeOn);
        createNewRuleMap.put("tag", ruleTags);
        createNewRuleMap.put("start", ruleValidityStartTime);
        createNewRuleMap.put("end", ruleValidityEndTime);
        createNewRuleMap.put("expression", expression);
        createNewRuleMap.put("orderJsonValues", orderJsonValues);

        RejectOrder = false;
        masterRuleDataTCMap = new HashMap<>();
        ruleHelper = new AlchemistRuleHelper();

        if (ReadRulesFromDBFlag)
            masterRuleDataTCMap = ruleHelper.verifyActiveRuleInDB(expression);
        else {
            masterRuleDataTCMap = ruleHelper.createRulesWithMap(createNewRuleMap);
        }

        masterRuleDataTCMap.put("TC_ID", TC_ID);
        masterRuleDataTCMap.put("SendPayoutToDE", String.valueOf(SendPayoutToDE));
        customOrderDetailsJSONMap = new HashMap<>();
        preOrderChecks();

        alchemistKafkaEventHelper.createNewDEIDGenerator(Integer.parseInt(masterRuleDataTCMap.get("zone_id")));
        String eventJSON = alchemistKafkaEventHelper.returnNewOrderJSONForAlchemistKafka(customOrderDetailsJSONMap);
        String key = alchemistKafkaEventHelper.getBatchId();
        sendEventToKafka(key, eventJSON);

        customOrderDetailsJSONMap.put("order_status", "assigned");
        eventJSON = alchemistKafkaEventHelper.returnOrderStatusUpdateJSONForAlchemistKafka(customOrderDetailsJSONMap);
        sendEventToKafka(key, eventJSON);
        eventJSON = alchemistKafkaEventHelper.returnTaskSequenceJSONForAlchemistKafka(customOrderDetailsJSONMap);
        sendEventToKafka(key, eventJSON);

        doCAPRDViaKafkaEventsPush(key, eventJSON, setCAPRD_To_state);

        if (CancelOrder) {
            eventJSON = alchemistKafkaEventHelper.returnOrderCancelJSONForAlchemistKafka();
            sendEventToKafka(key, eventJSON);
        }

        if (RejectOrder) {
            customOrderDetailsJSONMap.put("order_status", "rejected");
            eventJSON = alchemistKafkaEventHelper.returnRejectedStatusUpdateJSONForAlchemistKafka(customOrderDetailsJSONMap);
            sendEventToKafka(key, eventJSON);
        }

        verifyRuleValidationsAsPerTestData();
        verifyAggregationsInPerformanceTables();

        expression = "last_mile>100";
        ComputeOn = "delivered";
        SendPayoutToDE = Boolean.valueOf("true");
        ruleValidityStartTime = "2018-08-17 1:1:1";
        ruleValidityEndTime = "2019-09-30 1:1:1";
        ruleTags = "Per Order";
        orderJsonValues = "last_mile=120";
        createNewRuleMap = new HashMap<>();
        createNewRuleMap.put("zone", zone);
        createNewRuleMap.put("computeOn", ComputeOn);
        createNewRuleMap.put("tag", ruleTags);
        createNewRuleMap.put("start", ruleValidityStartTime);
        createNewRuleMap.put("end", ruleValidityEndTime);
        createNewRuleMap.put("expression", expression);
        createNewRuleMap.put("orderJsonValues", orderJsonValues);
        SendPayoutToDE = Boolean.valueOf("true");
        masterRuleDataTCMap = new HashMap<>();
        ruleHelper = new AlchemistRuleHelper();

        if (ReadRulesFromDBFlag)
            masterRuleDataTCMap = ruleHelper.verifyActiveRuleInDB(expression);
        else {
            masterRuleDataTCMap = ruleHelper.createRulesWithMap(createNewRuleMap);
        }

        String incentive_rules_ticketing_rule_query = "update incentive_rules set created_at='" + ruleValidityStartTime + "'where id = "+masterRuleDataTCMap.get("ruleid");
        SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).update(incentive_rules_ticketing_rule_query);

        masterRuleDataTCMap.put("TC_ID", TC_ID);
        masterRuleDataTCMap.put("SendPayoutToDE", String.valueOf(SendPayoutToDE));
        ticketingdetailsJSONMap = new HashMap<>();
        if (runTicketingFlag) {
            executeTicketing();
            eventJSON = alchemistKafkaEventHelper.returnTicketingJSONForAlchemistKafka(ticketingdetailsJSONMap);
            kafkaHelper.sendMessagewithKey(KAFKA_TICKETING_TOPIC_NAME, alchemistKafkaEventHelper.getBatchId(), eventJSON);
        }
        verifyRuleValidationsAsPerTestData();
        alchemistKafkaEventHelper.convertTestCaseResultstoJSON(masterRuleDataTCMap);
    }

    @Test(enabled = true, description = "Verify during EOD runs, order is getting appropiate Daily Incentives")
    public void verifyPayoutforEOD() {
        String TC_ID = "1";
        String zone, expression, ComputeOn, setCAPRD_To_state, ruleValidityStartTime, ruleValidityEndTime, ruleTags, orderJsonValues;
        Boolean CancelOrder, RejectOrder, runTicketing;
        zone = "1";
        Double a = Double.valueOf((zone));
        int zoneId = a.intValue();
        expression = "trips_count > 0";
        ComputeOn = "eod";
        setCAPRD_To_state = "delivered";
        CancelOrder = Boolean.parseBoolean("false");
        runTicketing = false;
        RejectOrder = false;
        SendPayoutToDE = Boolean.valueOf("true");
        ruleValidityStartTime = "2018-08-17 1:1:1.0";
        ruleValidityEndTime = "2019-09-30 1:1:1.0";
        ruleTags = "Daily Incentive";
        orderJsonValues = "last_mile=7";
        HashMap<String, String> createNewRuleMap = new HashMap<>();
        createNewRuleMap.put("zone", zone);
        createNewRuleMap.put("computeOn", ComputeOn);
        createNewRuleMap.put("tag", ruleTags);
        createNewRuleMap.put("start", ruleValidityStartTime);
        createNewRuleMap.put("end", ruleValidityEndTime);
        createNewRuleMap.put("expression", expression);
        createNewRuleMap.put("orderJsonValues", orderJsonValues);

        masterRuleDataTCMap = new HashMap<>();
        ruleHelper = new AlchemistRuleHelper();

        if (ReadRulesFromDBFlag)
            masterRuleDataTCMap = ruleHelper.verifyActiveRuleInDB(expression);
        else {
            masterRuleDataTCMap = ruleHelper.createRulesWithMap(createNewRuleMap);
        }

        masterRuleDataTCMap.put("TC_ID", TC_ID);
        masterRuleDataTCMap.put("SendPayoutToDE", String.valueOf(SendPayoutToDE));
        customOrderDetailsJSONMap = new HashMap<>();
        preOrderChecks();

        alchemistKafkaEventHelper.createNewDEIDGenerator(Integer.parseInt(masterRuleDataTCMap.get("zone_id")));
        String eventJSON = alchemistKafkaEventHelper.returnNewOrderJSONForAlchemistKafka(customOrderDetailsJSONMap);
        String key = alchemistKafkaEventHelper.getBatchId();
        sendEventToKafka(key, eventJSON);

        customOrderDetailsJSONMap.put("order_status", "assigned");
        eventJSON = alchemistKafkaEventHelper.returnOrderStatusUpdateJSONForAlchemistKafka(customOrderDetailsJSONMap);
        sendEventToKafka(key, eventJSON);

        eventJSON = alchemistKafkaEventHelper.returnTaskSequenceJSONForAlchemistKafka(customOrderDetailsJSONMap);
        sendEventToKafka(key, eventJSON);

//      if(verifyDryRunHappened())
        doCAPRDViaKafkaEventsPush(key, eventJSON, setCAPRD_To_state);

        if (CancelOrder) {
            eventJSON = alchemistKafkaEventHelper.returnOrderCancelJSONForAlchemistKafka();
            sendEventToKafka(key, eventJSON);
        }

        if (RejectOrder) {
            customOrderDetailsJSONMap.put("order_status", "rejected");
            eventJSON = alchemistKafkaEventHelper.returnRejectedStatusUpdateJSONForAlchemistKafka(customOrderDetailsJSONMap);
            sendEventToKafka(key, eventJSON);
        }

        ruleHelper.fireEvent(ComputeOn, alchemistKafkaEventHelper.getDEId());
        verifyPayoutsForEODEOWEOM(ComputeOn, masterRuleDataTCMap.get("ruleid"));

        if (runTicketing) {

        }
        alchemistKafkaEventHelper.convertTestCaseResultstoJSON(masterRuleDataTCMap);
    }

    @Test(enabled = true, description = "Verify during EOW runs, order is getting appropiate Weekly Incentives")
    public void verifyPayoutforEOW() {
        String TC_ID = "1";
        String zone, expression, ComputeOn, setCAPRD_To_state, ruleValidityStartTime, ruleValidityEndTime, ruleTags, orderJsonValues;
        Boolean CancelOrder, RejectOrder, runTicketing;
        zone = "1";
        Double a = Double.valueOf((zone));
        int zoneId = a.intValue();
        expression = "trips_count > 0";
        ComputeOn = "eow";
        setCAPRD_To_state = "delivered";
        CancelOrder = Boolean.parseBoolean("false");
        runTicketing = false;
        RejectOrder = false;
        SendPayoutToDE = Boolean.valueOf("true");
        ruleValidityStartTime = "2018-08-17 1:1:1.0";
        ruleValidityEndTime = "2019-09-30 1:1:1.0";
        ruleTags = "Weekly Incentive";
        orderJsonValues = "last_mile=7";
        HashMap<String, String> createNewRuleMap = new HashMap<>();
        createNewRuleMap.put("zone", zone);
        createNewRuleMap.put("computeOn", ComputeOn);
        createNewRuleMap.put("tag", ruleTags);
        createNewRuleMap.put("start", ruleValidityStartTime);
        createNewRuleMap.put("end", ruleValidityEndTime);
        createNewRuleMap.put("expression", expression);
        createNewRuleMap.put("orderJsonValues", orderJsonValues);

        masterRuleDataTCMap = new HashMap<>();
        ruleHelper = new AlchemistRuleHelper();

        if (ReadRulesFromDBFlag)
            masterRuleDataTCMap = ruleHelper.verifyActiveRuleInDB(expression);
        else {
            masterRuleDataTCMap = ruleHelper.createRulesWithMap(createNewRuleMap);
        }

        masterRuleDataTCMap.put("TC_ID", TC_ID);
        masterRuleDataTCMap.put("SendPayoutToDE", String.valueOf(SendPayoutToDE));
        customOrderDetailsJSONMap = new HashMap<>();
        preOrderChecks();

        alchemistKafkaEventHelper.createNewDEIDGenerator(Integer.parseInt(masterRuleDataTCMap.get("zone_id")));
        String eventJSON = alchemistKafkaEventHelper.returnNewOrderJSONForAlchemistKafka(customOrderDetailsJSONMap);
        String key = alchemistKafkaEventHelper.getBatchId();
        sendEventToKafka(key, eventJSON);

        customOrderDetailsJSONMap.put("order_status", "assigned");
        eventJSON = alchemistKafkaEventHelper.returnOrderStatusUpdateJSONForAlchemistKafka(customOrderDetailsJSONMap);
        sendEventToKafka(key, eventJSON);

        eventJSON = alchemistKafkaEventHelper.returnTaskSequenceJSONForAlchemistKafka(customOrderDetailsJSONMap);
        sendEventToKafka(key, eventJSON);

//      if(verifyDryRunHappened())
        doCAPRDViaKafkaEventsPush(key, eventJSON, setCAPRD_To_state);

        if (CancelOrder) {
            eventJSON = alchemistKafkaEventHelper.returnOrderCancelJSONForAlchemistKafka();
            sendEventToKafka(key, eventJSON);
        }

        if (RejectOrder) {
            customOrderDetailsJSONMap.put("order_status", "rejected");
            eventJSON = alchemistKafkaEventHelper.returnRejectedStatusUpdateJSONForAlchemistKafka(customOrderDetailsJSONMap);
            sendEventToKafka(key, eventJSON);
        }

        ruleHelper.fireEvent(ComputeOn, alchemistKafkaEventHelper.getDEId());
        verifyPayoutsForEODEOWEOM(ComputeOn, masterRuleDataTCMap.get("ruleid"));

        if (runTicketing) {

        }
        alchemistKafkaEventHelper.convertTestCaseResultstoJSON(masterRuleDataTCMap);
    }

    @Test(enabled = true, description = "Verify during EOM runs, order is getting appropiate Monthly Incentives")
    public void verifyPayoutforEOM() {
        String TC_ID = "1";
        String zone, expression, ComputeOn, setCAPRD_To_state, ruleValidityStartTime, ruleValidityEndTime, ruleTags, orderJsonValues;
        Boolean CancelOrder, RejectOrder, runTicketing;
        zone = "1";
        Double a = Double.valueOf((zone));
        int zoneId = a.intValue();
        expression = "trips_count > 0";
        ComputeOn = "eom";
        setCAPRD_To_state = "delivered";
        CancelOrder = Boolean.parseBoolean("false");
        runTicketing = false;
        RejectOrder = false;
        SendPayoutToDE = Boolean.valueOf("true");
        ruleValidityStartTime = "2018-08-17 1:1:1.0";
        ruleValidityEndTime = "2019-09-30 1:1:1.0";
        ruleTags = "Monthly Incentive";
        orderJsonValues = "last_mile=7";
        HashMap<String, String> createNewRuleMap = new HashMap<>();
        createNewRuleMap.put("zone", zone);
        createNewRuleMap.put("computeOn", ComputeOn);
        createNewRuleMap.put("tag", ruleTags);
        createNewRuleMap.put("start", ruleValidityStartTime);
        createNewRuleMap.put("end", ruleValidityEndTime);
        createNewRuleMap.put("expression", expression);
        createNewRuleMap.put("orderJsonValues", orderJsonValues);

        masterRuleDataTCMap = new HashMap<>();
        ruleHelper = new AlchemistRuleHelper();

        if (ReadRulesFromDBFlag)
            masterRuleDataTCMap = ruleHelper.verifyActiveRuleInDB(expression);
        else {
            masterRuleDataTCMap = ruleHelper.createRulesWithMap(createNewRuleMap);
        }

        masterRuleDataTCMap.put("TC_ID", TC_ID);
        masterRuleDataTCMap.put("SendPayoutToDE", String.valueOf(SendPayoutToDE));
        customOrderDetailsJSONMap = new HashMap<>();
        preOrderChecks();

        alchemistKafkaEventHelper.createNewDEIDGenerator(Integer.parseInt(masterRuleDataTCMap.get("zone_id")));
        String eventJSON = alchemistKafkaEventHelper.returnNewOrderJSONForAlchemistKafka(customOrderDetailsJSONMap);
        String key = alchemistKafkaEventHelper.getBatchId();
        sendEventToKafka(key, eventJSON);

        customOrderDetailsJSONMap.put("order_status", "assigned");
        eventJSON = alchemistKafkaEventHelper.returnOrderStatusUpdateJSONForAlchemistKafka(customOrderDetailsJSONMap);
        sendEventToKafka(key, eventJSON);

        eventJSON = alchemistKafkaEventHelper.returnTaskSequenceJSONForAlchemistKafka(customOrderDetailsJSONMap);
        sendEventToKafka(key, eventJSON);

//      if(verifyDryRunHappened())
        doCAPRDViaKafkaEventsPush(key, eventJSON, setCAPRD_To_state);

        if (CancelOrder) {
            eventJSON = alchemistKafkaEventHelper.returnOrderCancelJSONForAlchemistKafka();
            sendEventToKafka(key, eventJSON);
        }

        if (RejectOrder) {
            customOrderDetailsJSONMap.put("order_status", "rejected");
            eventJSON = alchemistKafkaEventHelper.returnRejectedStatusUpdateJSONForAlchemistKafka(customOrderDetailsJSONMap);
            sendEventToKafka(key, eventJSON);
        }

        ruleHelper.fireEvent(ComputeOn, alchemistKafkaEventHelper.getDEId());
        verifyPayoutsForEODEOWEOM(ComputeOn, masterRuleDataTCMap.get("ruleid"));

        if (runTicketing) {

        }
        alchemistKafkaEventHelper.convertTestCaseResultstoJSON(masterRuleDataTCMap);
    }

    public void sendEventToKafka(String key, String eventJSON) {
        kafkaHelper.sendMessagewithKey(KAFKA_INCENTIVE_TOPIC_NAME, key, eventJSON);
    }

    public void preOrderChecks() {
        zoneandAreaCodeCheck();
        rainModeCheck();
        batchCountCheck();
        EBPCheck();
        DEWaitTimeCheck();
        avgRatingsCheck();
        bannerFactorCheck();
        orderRejectCountCheck();
    }

    public void preTicketingRunChecks(){
        if (masterRuleDataTCMap.containsKey("first_mile")) {
            ticketingdetailsJSONMap.put("first_mile", masterRuleDataTCMap.get("first_mile"));
        }
        if (masterRuleDataTCMap.containsKey("last_mile")) {
            ticketingdetailsJSONMap.put("last_mile", masterRuleDataTCMap.get("last_mile"));
        }
        if (masterRuleDataTCMap.containsKey("fmt")) {
            int fmtInMins = Integer.parseInt(masterRuleDataTCMap.get("fmt")) * 60;
            ticketingdetailsJSONMap.put("fmt", String.valueOf(fmtInMins));
        }
        if (masterRuleDataTCMap.containsKey("lmt")) {
            int lmtInMins = Integer.parseInt(masterRuleDataTCMap.get("lmt")) * 60;
            ticketingdetailsJSONMap.put("lmt", String.valueOf(lmtInMins));
        }
        if (masterRuleDataTCMap.containsKey("de_wait_time")) {
            ticketingdetailsJSONMap.put("de_wait_time", masterRuleDataTCMap.get("de_wait_time"));
        }
    }

    private void zoneandAreaCodeCheck() {
        if (masterRuleDataTCMap.containsKey("city_id")) {
            customOrderDetailsJSONMap.put("city_id", masterRuleDataTCMap.get("city_id"));
        }
        if (masterRuleDataTCMap.containsKey("zone_id")) {
            customOrderDetailsJSONMap.put("zone_id", masterRuleDataTCMap.get("zone_id"));
            customOrderDetailsJSONMap.put("area_code", masterRuleDataTCMap.get("zone_id"));
        }
    }

    private void rainModeCheck() {
        if (masterRuleDataTCMap.containsKey("is_rain_mode")) {
            customOrderDetailsJSONMap.put("rain_mode", masterRuleDataTCMap.get("is_rain_mode"));
            if (masterRuleDataTCMap.containsKey("rain_mode_type")) {
                customOrderDetailsJSONMap.put("rain_mode_type", masterRuleDataTCMap.get("rain_mode_type"));
            } else if (masterRuleDataTCMap.containsKey("rain_mode_type")) {
                customOrderDetailsJSONMap.put("rain_mode_type", "1");
            }

        }
    }

    private void batchCountCheck() {
        if (masterRuleDataTCMap.containsKey("batch_count")) {
            customOrderDetailsJSONMap.put("batch_count", masterRuleDataTCMap.get("batch_count"));
            if (masterRuleDataTCMap.containsKey("batch_type")) {
                customOrderDetailsJSONMap.put("batch_type", masterRuleDataTCMap.get("batch_type"));
            }
        }
    }

    private void EBPCheck() {
        if (masterRuleDataTCMap.containsKey("first_mile")) {
            customOrderDetailsJSONMap.put("first_mile", masterRuleDataTCMap.get("first_mile"));
        }
        if (masterRuleDataTCMap.containsKey("last_mile")) {
            customOrderDetailsJSONMap.put("last_mile", masterRuleDataTCMap.get("last_mile"));
        }
        if (masterRuleDataTCMap.containsKey("fmt")) {
            int fmtInMins = Integer.parseInt(masterRuleDataTCMap.get("fmt")) * 60;
            customOrderDetailsJSONMap.put("fmt", String.valueOf(fmtInMins));
        }
        if (masterRuleDataTCMap.containsKey("lmt")) {
            int lmtInMins = Integer.parseInt(masterRuleDataTCMap.get("lmt")) * 60;
            customOrderDetailsJSONMap.put("lmt", String.valueOf(lmtInMins));
        }
    }

    private void DEWaitTimeCheck() {
        if (masterRuleDataTCMap.containsKey("de_wait_time")) {
            customOrderDetailsJSONMap.put("de_wait_time", masterRuleDataTCMap.get("de_wait_time"));
        }
    }

    private void bannerFactorCheck() {
        if (masterRuleDataTCMap.containsKey("banner_factor")) {
            customOrderDetailsJSONMap.put("banner_factor", masterRuleDataTCMap.get("banner_factor"));
        }
    }

    private void avgRatingsCheck() {
    }

    private void orderRejectCountCheck() {
        if (masterRuleDataTCMap.containsKey("order_reject_count")) {
            customOrderDetailsJSONMap.put("order_reject_count", masterRuleDataTCMap.get("order_reject_count"));
        }
    }

    public void doCAPRDViaKafkaEventsPush(String key, String eventJSON, String do_CAPRD_to_state) {
        List<String> CAPRDstates = new ArrayList<String>() {{
            add("confirmed");
            add("arrived");
            add("pickedup");
            add("reached");
            add("delivered");
        }};
        int toState = CAPRDstates.indexOf(do_CAPRD_to_state.toLowerCase());
        for (int i = 0; i <= toState; i++) {
            switch (CAPRDstates.get(i)) {
                case "confirmed":
                    customOrderDetailsJSONMap.put("order_status", "confirmed");
                    break;

                case "arrived":
                    customOrderDetailsJSONMap.put("order_status", "arrived");
                    break;

                case "pickedup":
                    customOrderDetailsJSONMap.put("order_status", "pickedup");
                    break;

                case "reached":
                    customOrderDetailsJSONMap.put("order_status", "reached");
                    break;

                case "delivered":
                    customOrderDetailsJSONMap.put("order_status", "delivered");
                    break;
            }
            eventJSON = alchemistKafkaEventHelper.returnOrderStatusUpdateJSONForAlchemistKafka(customOrderDetailsJSONMap);
            sendEventToKafka(key, eventJSON);
        }
    }

    public void executeTicketing(){
        ticketingdetailsJSONMap.put("order_id", alchemistKafkaEventHelper.getOrderId());
        String query="select ticket_id from ticketing_adjustment order by id desc limit 1";
        String last_ticket=SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).queryForList(query).get(0).get("ticket_id").toString();
        int last_ticket_id=Integer.parseInt(last_ticket);
        last_ticket_id++;
        ticketingdetailsJSONMap.put("ticket_id", String.valueOf(last_ticket_id));
        ticketingdetailsJSONMap.put("de_id",alchemistKafkaEventHelper.getDEId());
        preTicketingRunChecks();
    }

    private Boolean verifyDryRunHappened() {
        RetryCounter = 10;
        dryRunComputedRuleIds = new ArrayList<>();
        String Dry_Run_Response = "";
        String payput_Details_for_Dry_Run = "select * from payout_audit where type='DRYRUN_DELIVERED' and order_id ='" + alchemistKafkaEventHelper.getOrderId() + "'";
        String Dry_Run_RowCount = "select count(*) from payout_audit where type='DRYRUN_DELIVERED' and order_id ='" + alchemistKafkaEventHelper.getOrderId() + "'";

        try {
            while (RetryCounter > 0) {
                String RowCount = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).queryForList(Dry_Run_RowCount).get(0).get("count(*)").toString();
                if (Integer.parseInt(RowCount) == 1) {
                    Dry_Run_Response = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).queryForList(payput_Details_for_Dry_Run).get(0).get("payout_detail").toString();
                    String Response = "{\"Root\":" + Dry_Run_Response + "}";
                    JSONObject jsonObject = new JSONObject(Response);
                    JSONArray root = jsonObject.getJSONArray("Root");
                    for (int index = 0; index < root.length(); ++index) {
                        dryRunComputedRuleIds.add(root.getJSONObject(index).getInt("RuleId"));
                    }
                    break;
                }
                waitIntervalMili(1000);
                System.out.println("Rechecking and waiting for Dry Run event in payout_audit table");
                RetryCounter--;
            }
        } catch (IndexOutOfBoundsException ip) {
            System.out.println("Dry Run didnt happened for the order in Payout_audit table.. Please check");
            ip.printStackTrace();
        } catch (JSONException je) {
            System.out.println("No Rule Id is applicable during Dry Run for the Order");
            je.printStackTrace();
        }
        if ((Boolean.parseBoolean(masterRuleDataTCMap.get("SendPayoutToDE"))) && dryRunComputedRuleIds.contains(Integer.parseInt(masterRuleDataTCMap.get("ruleid")))) {
            System.out.println("Dry Run happened successfully for the order id " + alchemistKafkaEventHelper.getOrderId());
            return true;
        } else if (!(dryRunComputedRuleIds.contains(Integer.parseInt(masterRuleDataTCMap.get("ruleid"))))) {
            if (!(Boolean.parseBoolean(masterRuleDataTCMap.get("SendPayoutToDE")))) {
                System.out.println("Dry Run happened successfully for the order id " + alchemistKafkaEventHelper.getOrderId());
                return true;
            } else {
                System.out.println("Dry Rune didnt happened for the order id and Rule id: " + masterRuleDataTCMap.get("ruleid"));
                Assert.assertTrue(false);
            }
        }
        return false;
    }

    private void verifyRuleValidationsAsPerTestData() {
        RetryCounter = 15;
        String Payout_Detail_Response = "";
        rulesApplied = new ArrayList<>();
        String payout_audit_query = "select * from payout_audit where used_for_payout=1 and order_id ='" + alchemistKafkaEventHelper.getOrderId() + "'" + "and de_id =" + alchemistKafkaEventHelper.getDEId();
        String check_terminal_event = "select count(*) from payout_audit where used_for_payout=1 and order_id ='" + alchemistKafkaEventHelper.getOrderId() + "'" + "and de_id =" + alchemistKafkaEventHelper.getDEId();

        try {
            while (RetryCounter > 0) {
                String RowCount = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).queryForList(check_terminal_event).get(0).get("count(*)").toString();
                if (Integer.parseInt(RowCount) == 1) {
                    Payout_Detail_Response = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).queryForList(payout_audit_query).get(0).get("payout_detail").toString();
                    String Response = "{\"Root\":" + Payout_Detail_Response + "}";
                    JSONObject jsonObject = new JSONObject(Response);
                    JSONArray root = jsonObject.getJSONArray("Root");
                    for (int index = 0; index < root.length(); ++index) {
                        rulesApplied.add(root.getJSONObject(index).getInt("RuleId"));
                    }
                    break;
                }
                waitIntervalMili(1000);
                System.out.println("Rechecking and waiting for Terminal event in payout_audit table");
                RetryCounter--;
            }
        } catch (IndexOutOfBoundsException ip) {
            System.out.println("No terminal state event received for the order in Payout_audit table.. Terminal states are Cancelled/Rejected/Delivered");
            ip.printStackTrace();
        } catch (JSONException je) {
            System.out.println("No Rule got applied to the Order");
            je.printStackTrace();
        }

        System.out.println("Test Case created Rule id: " + Integer.parseInt(masterRuleDataTCMap.get("ruleid")) + " and Following rules got applied to the order after CAPRD-----" + rulesApplied);
        System.out.println("With test case data Payout be given to DE = " + masterRuleDataTCMap.get("SendPayoutToDE"));

        if (!(masterRuleDataTCMap.containsKey("SendPayoutToDE"))) {
            System.out.println("SendPayoutToDE flag not provided in Test Case, Please add and Run again");
            Assert.assertTrue(false);
        } else if (Boolean.parseBoolean(masterRuleDataTCMap.get("SendPayoutToDE"))) {
            Assert.assertTrue(rulesApplied.contains(Integer.parseInt(masterRuleDataTCMap.get("ruleid"))));
        } else if (!(Boolean.parseBoolean(masterRuleDataTCMap.get("SendPayoutToDE")))) {
            Assert.assertFalse(rulesApplied.contains(Integer.parseInt(masterRuleDataTCMap.get("ruleid"))));
        }
    }

    private void verifyPayoutsForEODEOWEOM(String event, String rule_id) {
        RetryCounter = 10;
        ArrayList<Long> rulesApplied = new ArrayList<>();
        List<Map<String, Object>> ruleIdsApplied = new ArrayList<>();
        String table = "";
        if (event.equals("eod")) {
            table = "de_rule_map_daily";
        } else if (event.equals("eow")) {
            table = "de_rule_map_weekly";
        } else if (event.equals("eom")) {
            table = "de_rule_map_monthly";
        }

        String event_payout_query = "select rule_id from " + table + " where de_id=" + alchemistKafkaEventHelper.getDEId();
        try {
            while (RetryCounter > 0) {
                ruleIdsApplied = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).queryForList(event_payout_query);
                System.out.println(ruleIdsApplied);
                if (ruleIdsApplied.size() > 0) {
                    break;
                } else {
                    waitIntervalMili(1000);
                    System.out.println("Rechecking and waiting for event in DE Rule Map table");
                    RetryCounter--;
                }
            }
        } catch (IndexOutOfBoundsException ip) {
            System.out.println("No events received for the order in DE Rule map table");
            ip.printStackTrace();
        }

        for (int i = 0; i < ruleIdsApplied.size(); i++) {
            rulesApplied.add((Long) ruleIdsApplied.get(i).get("rule_id"));
        }

        System.out.println("Test Case created Rule id: " + Integer.parseInt(masterRuleDataTCMap.get("ruleid")) + " and Following rules got applied to the order after EOD/EOW/EOM Event fired-----" + ruleIdsApplied);
        System.out.println("With test case data Payout be given to DE = " + masterRuleDataTCMap.get("SendPayoutToDE"));

        if (!(masterRuleDataTCMap.containsKey("SendPayoutToDE"))) {
            System.out.println("SendPayoutToDE flag not provided in Test Case, Please add and Run again");
            Assert.assertTrue(false);
        } else if (Boolean.parseBoolean(masterRuleDataTCMap.get("SendPayoutToDE"))) {
            Assert.assertTrue(rulesApplied.contains(Long.valueOf(masterRuleDataTCMap.get("ruleid"))));
        } else if (!(Boolean.parseBoolean(masterRuleDataTCMap.get("SendPayoutToDE")))) {
            Assert.assertFalse(rulesApplied.contains(Long.valueOf(masterRuleDataTCMap.get("ruleid"))));
        }
    }

    public void verifyAggregationsInPerformanceTables(){
        int Performance_tables_count=5;
        String order_earnings[]=new String[Performance_tables_count];
        String earnings[]=new String[Performance_tables_count];

        String perf_blob_hourly_details = "select perf_blob from de_performance_hourly where de_id="+alchemistKafkaEventHelper.getDEId()+" order by id limit 1";
        String perf_blob_daily_details = "select perf_blob from de_performance_daily where de_id="+alchemistKafkaEventHelper.getDEId()+" order by id limit 1";
        String perf_blob_weekly_details = "select perf_blob from de_performance_weekly where de_id="+alchemistKafkaEventHelper.getDEId()+" order by id limit 1";
        String perf_blob_quarter_details = "select perf_blob from de_performance_quarter where de_id="+alchemistKafkaEventHelper.getDEId()+" order by id limit 1";
        String perf_blob_monthly_details = "select perf_blob from de_performance_monthly where de_id="+alchemistKafkaEventHelper.getDEId()+" order by id limit 1";

        String Hourly_Aggregation = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).queryForList(perf_blob_hourly_details).get(0).get("perf_blob").toString();
        String Daily_Aggregation = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).queryForList(perf_blob_daily_details).get(0).get("perf_blob").toString();
        String Weekly_Aggregation = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).queryForList(perf_blob_weekly_details).get(0).get("perf_blob").toString();
        String Quarter_Aggregation = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).queryForList(perf_blob_quarter_details).get(0).get("perf_blob").toString();
        String Monthly_Aggregation = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).queryForList(perf_blob_monthly_details).get(0).get("perf_blob").toString();

        String expected_bonus_query="select bonus_total from payout_audit where used_for_payout=1 and de_id="+alchemistKafkaEventHelper.getDEId()+" order by id limit 1";
        String expected_bonus = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).queryForList(expected_bonus_query).get(0).get("bonus_total").toString();

        JSONObject jsonObject[] = new JSONObject[Performance_tables_count];
        try{
            jsonObject[0]= new JSONObject(Hourly_Aggregation);
            jsonObject[1]= new JSONObject(Daily_Aggregation);
            jsonObject[2]= new JSONObject(Weekly_Aggregation);
            jsonObject[3]= new JSONObject(Quarter_Aggregation);
            jsonObject[4]= new JSONObject(Monthly_Aggregation);
        }
        catch (JSONException e){
            e.printStackTrace();

        }

        for(int i=0;i<jsonObject.length;i++){
            try {
                order_earnings[i]=jsonObject[i].get("order_earnings").toString();
                earnings[i]=jsonObject[i].get("earnings").toString();
            } catch (JSONException e) {
            }
            Double d_oe=Double.parseDouble(order_earnings[i]);

            if(Integer.parseInt(expected_bonus)==d_oe.intValue()){
                System.out.println("Aggregation happened successfully");
                Assert.assertTrue(true);
            }
            else {
                System.out.println("Aggregations didn't happened correctly");
                Assert.assertTrue(false);
            }
        }
    }

    public void waitIntervalMili(int ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }



    @Test(enabled = false)
    public void runTicketing() {
        HashMap<String, String> ticketingdetailsJSONMap = new HashMap<>();
        ticketingdetailsJSONMap.put("order_id", "Test_Order_20181220182223");
        ticketingdetailsJSONMap.put("ticket_id", "35248");
        ticketingdetailsJSONMap.put("last_mile","171");
        eventJSON = alchemistKafkaEventHelper.returnTicketingJSONForAlchemistKafka(ticketingdetailsJSONMap);
        kafkaHelper.sendMessagewithKey(KAFKA_TICKETING_TOPIC_NAME, "20181220182223", eventJSON);
    }


    @Test(enabled = false, description = "This feature is not Currently in prod, this test case was created for validation")
    public void DPAY61() {
        Boolean flag=false;
//        for(int i=0;i<5;i++) {

            String TC_ID = "1";
            String zone, expression, ComputeOn, setCAPRD_To_state, ruleValidityStartTime, ruleValidityEndTime, ruleTags, orderJsonValues;
            Boolean CancelOrder, RejectOrder, runTicketing;
            zone = "1";
            Double a = Double.valueOf((zone));
            int zoneId = a.intValue();
            expression = "batch_count = 1";
            ComputeOn = "delivered";
            setCAPRD_To_state = "delivered";
            CancelOrder = Boolean.parseBoolean("false");
            runTicketing = true;
            SendPayoutToDE = Boolean.valueOf("true");
            ruleValidityStartTime = "2018-08-17 1:1:1";
            ruleValidityEndTime = "2019-09-30 1:1:1";
            ruleTags = "Per Order";
            orderJsonValues = "batch_count=1";
            HashMap<String, String> createNewRuleMap = new HashMap<>();
            createNewRuleMap.put("zone", zone);
            createNewRuleMap.put("computeOn", ComputeOn);
            createNewRuleMap.put("tag", ruleTags);
            createNewRuleMap.put("start", ruleValidityStartTime);
            createNewRuleMap.put("end", ruleValidityEndTime);
            createNewRuleMap.put("expression", expression);
            createNewRuleMap.put("orderJsonValues", orderJsonValues);

            RejectOrder = false;
            masterRuleDataTCMap = new HashMap<>();
            ruleHelper = new AlchemistRuleHelper();

            if (ReadRulesFromDBFlag)
                masterRuleDataTCMap = ruleHelper.verifyActiveRuleInDB(expression);
            else {
                masterRuleDataTCMap = ruleHelper.createRulesWithMap(createNewRuleMap);
            }

            masterRuleDataTCMap.put("TC_ID", TC_ID);
            masterRuleDataTCMap.put("SendPayoutToDE", String.valueOf(SendPayoutToDE));
            customOrderDetailsJSONMap = new HashMap<>();
            preOrderChecks();

            if(!flag){
                flag=true;
                alchemistKafkaEventHelper.createNewDEIDGenerator(Integer.parseInt(masterRuleDataTCMap.get("zone_id")));
            }
            String eventJSON = alchemistKafkaEventHelper.returnNewOrderJSONForAlchemistKafka(customOrderDetailsJSONMap);
            String key = alchemistKafkaEventHelper.getBatchId();
            sendEventToKafka(key, eventJSON);

            customOrderDetailsJSONMap.put("order_status", "assigned");
            eventJSON = alchemistKafkaEventHelper.returnOrderStatusUpdateJSONForAlchemistKafka(customOrderDetailsJSONMap);
            sendEventToKafka(key, eventJSON);

            eventJSON = alchemistKafkaEventHelper.returnTaskSequenceJSONForAlchemistKafka(customOrderDetailsJSONMap);
            sendEventToKafka(key, eventJSON);

//        if(verifyDryRunHappened())
            doCAPRDViaKafkaEventsPush(key, eventJSON, setCAPRD_To_state);

            if (CancelOrder) {
                eventJSON = alchemistKafkaEventHelper.returnOrderCancelJSONForAlchemistKafka();
                sendEventToKafka(key, eventJSON);
            }

            if (RejectOrder) {
                customOrderDetailsJSONMap.put("order_status", "rejected");
                eventJSON = alchemistKafkaEventHelper.returnRejectedStatusUpdateJSONForAlchemistKafka(customOrderDetailsJSONMap);
                sendEventToKafka(key, eventJSON);
            }

//        verifyRuleValidationsAsPerTestData();

            if (runTicketing) {

            }
//        alchemistKafkaEventHelper.convertTestCaseResultstoJSON(masterRuleDataTCMap);
//        }
        updateForDPAY_61(alchemistKafkaEventHelper.getOrderId(), alchemistKafkaEventHelper.getDEId(), "2018-12-50-14", "1543486546000");
    }

    public void updateForDPAY_61(String order_id, String de_id, String createdDate, String epochTime) {
        String payout_audit_query = "update de_rule_map_daily set created_on='" + createdDate + "'where de_id =";
        SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).update(payout_audit_query + de_id);
        System.out.println("success1");

        String payout_audit_query2 = "update de_rule_map_weekly set created_on='" + createdDate + "'where de_id =";
        SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).update(payout_audit_query + de_id);
        System.out.println("success2");
        String performance_daily_query = "update de_performance_daily set date='" + createdDate + "'where de_id =";
        SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).update(performance_daily_query + de_id);
        System.out.println("success3");
    }

    public void deleteKeyInRedis(String key) {
        RedisHelper redisHelper = new RedisHelper();
        redisHelper.deleteKey("deliveryredis", 0, key);
    }

    @Test(enabled = false)
    public void createNewRule() {
        String TC_ID = "1";
        String zone, expression, ComputeOn, setCAPRD_To_state, ruleValidityStartTime, ruleValidityEndTime, ruleTags, orderJsonValues;
        Boolean CancelOrder, RejectOrder, runTicketing;
        zone = "1";
        Double a = Double.valueOf((zone));
        int zoneId = a.intValue();
        expression = "last_mile>100";
        ComputeOn = "delivered";
        setCAPRD_To_state = "delivered";
        CancelOrder = Boolean.parseBoolean("false");
        runTicketing = true;
        SendPayoutToDE = Boolean.valueOf("true");
        ruleValidityStartTime = "2018-08-17 1:1:1";
        ruleValidityEndTime = "2019-09-30 1:1:1";
        ruleTags = "Per Order";
        orderJsonValues = "batch_count=2";
        HashMap<String, String> createNewRuleMap = new HashMap<>();
        createNewRuleMap.put("zone", zone);
        createNewRuleMap.put("computeOn", ComputeOn);
        createNewRuleMap.put("tag", ruleTags);
        createNewRuleMap.put("start", ruleValidityStartTime);
        createNewRuleMap.put("end", ruleValidityEndTime);
        createNewRuleMap.put("expression", expression);
        createNewRuleMap.put("orderJsonValues", orderJsonValues);

        RejectOrder = false;
        masterRuleDataTCMap = new HashMap<>();
        ruleHelper = new AlchemistRuleHelper();

        if (ReadRulesFromDBFlag)
            masterRuleDataTCMap = ruleHelper.verifyActiveRuleInDB(expression);
        else {
            masterRuleDataTCMap = ruleHelper.createRulesWithMap(createNewRuleMap);
        }

    }

    @Test(enabled = false, description = "This feature is not Currently in prod, this test case was created for validation")
    public void DelayedTaskSequeningUpdatingPayouts_DPAY109() {
        String TC_ID = "1";
        String zone, expression, ComputeOn, setCAPRD_To_state, ruleValidityStartTime, ruleValidityEndTime, ruleTags, orderJsonValues;
        Boolean CancelOrder, RejectOrder, runTicketing;
        zone = "1";
        Double a = Double.valueOf((zone));
        int zoneId = a.intValue();
        expression = "batch_count=1";
        ComputeOn = "delivered";
        setCAPRD_To_state = "delivered";
        CancelOrder = Boolean.parseBoolean("false");
        runTicketing = false;
        SendPayoutToDE = Boolean.valueOf("true");
        ruleValidityStartTime = "2018-08-17 1:1:1";
        ruleValidityEndTime = "2019-09-30 1:1:1";
        ruleTags = "Per Order";
        orderJsonValues = "batch_count=1";
        HashMap<String, String> createNewRuleMap = new HashMap<>();
        createNewRuleMap.put("zone", zone);
        createNewRuleMap.put("computeOn", ComputeOn);
        createNewRuleMap.put("tag", ruleTags);
        createNewRuleMap.put("start", ruleValidityStartTime);
        createNewRuleMap.put("end", ruleValidityEndTime);
        createNewRuleMap.put("expression", expression);
        createNewRuleMap.put("orderJsonValues", orderJsonValues);

        RejectOrder = false;
        masterRuleDataTCMap = new HashMap<>();
        ruleHelper = new AlchemistRuleHelper();

        if (ReadRulesFromDBFlag)
            masterRuleDataTCMap = ruleHelper.verifyActiveRuleInDB(expression);
        else {
            masterRuleDataTCMap = ruleHelper.createRulesWithMap(createNewRuleMap);
        }

        masterRuleDataTCMap.put("TC_ID", TC_ID);
        masterRuleDataTCMap.put("SendPayoutToDE", String.valueOf(SendPayoutToDE));
        customOrderDetailsJSONMap = new HashMap<>();
        preOrderChecks();

        alchemistKafkaEventHelper.createNewDEIDGenerator(Integer.parseInt(masterRuleDataTCMap.get("zone_id")));
        String eventJSON = alchemistKafkaEventHelper.returnNewOrderJSONForAlchemistKafka(customOrderDetailsJSONMap);
        String key = alchemistKafkaEventHelper.getBatchId();
        sendEventToKafka(key, eventJSON);

        customOrderDetailsJSONMap.put("order_status", "assigned");
        eventJSON = alchemistKafkaEventHelper.returnOrderStatusUpdateJSONForAlchemistKafka(customOrderDetailsJSONMap);
        sendEventToKafka(key, eventJSON);



//        if(verifyDryRunHappened())
        doCAPRDViaKafkaEventsPush(key, eventJSON, setCAPRD_To_state);

        if (CancelOrder) {
            eventJSON = alchemistKafkaEventHelper.returnOrderCancelJSONForAlchemistKafka();
            sendEventToKafka(key, eventJSON);
        }

        if (RejectOrder) {
            customOrderDetailsJSONMap.put("order_status", "rejected");
            eventJSON = alchemistKafkaEventHelper.returnRejectedStatusUpdateJSONForAlchemistKafka(customOrderDetailsJSONMap);
            sendEventToKafka(key, eventJSON);
        }

        eventJSON = alchemistKafkaEventHelper.returnTaskSequenceJSONForAlchemistKafka(customOrderDetailsJSONMap);
        sendEventToKafka(key, eventJSON);

        verifyRuleValidationsAsPerTestData();
        verifyAggregationsInPerformanceTables();
        if (runTicketing) {
        }
        alchemistKafkaEventHelper.convertTestCaseResultstoJSON(masterRuleDataTCMap);
    }



    @Test(enabled = false, description = "This feature is not Currently in prod, this test case was created for validation")
    public void verifyTaskSeqDelayedforDEWhoReceivesRejectedOrder() {
        String TC_ID = "1";
        String zone, expression, ComputeOn, setCAPRD_To_state, ruleValidityStartTime, ruleValidityEndTime, ruleTags, orderJsonValues;
        Boolean CancelOrder, RejectOrder, runTicketing;
        zone = "1";
        Double a = Double.valueOf((zone));
        int zoneId = a.intValue();
        expression = "batch_count > 1 and service_line_id=1";
        ComputeOn = "delivered";
        setCAPRD_To_state = "delivered";
        CancelOrder = Boolean.parseBoolean("false");
        runTicketing = true;

        SendPayoutToDE = Boolean.valueOf("true");
        ruleValidityStartTime = "2018-08-17 1:1:1.0";
        ruleValidityEndTime = "2019-09-30 1:1:1.0";
        ruleTags = "Per Order";
        orderJsonValues = "batch_count=2";
        HashMap<String, String> createNewRuleMap = new HashMap<>();
        createNewRuleMap.put("zone", zone);
        createNewRuleMap.put("computeOn", ComputeOn);
        createNewRuleMap.put("tag", ruleTags);
        createNewRuleMap.put("start", ruleValidityStartTime);
        createNewRuleMap.put("end", ruleValidityEndTime);
        createNewRuleMap.put("expression", expression);
        createNewRuleMap.put("orderJsonValues", orderJsonValues);

        masterRuleDataTCMap = new HashMap<>();
        ruleHelper = new AlchemistRuleHelper();

        if (ReadRulesFromDBFlag)
            masterRuleDataTCMap = ruleHelper.verifyActiveRuleInDB(expression);
        else {
            masterRuleDataTCMap = ruleHelper.createRulesWithMap(createNewRuleMap);
        }

        RejectOrder = true;
        masterRuleDataTCMap.put("TC_ID", TC_ID);
        masterRuleDataTCMap.put("SendPayoutToDE", String.valueOf(SendPayoutToDE));
        customOrderDetailsJSONMap = new HashMap<>();
        preOrderChecks();

        alchemistKafkaEventHelper.createNewDEIDGenerator(Integer.parseInt(masterRuleDataTCMap.get("zone_id")));
        String eventJSON = alchemistKafkaEventHelper.returnNewOrderJSONForAlchemistKafka(customOrderDetailsJSONMap);
        String key = alchemistKafkaEventHelper.getBatchId();
        sendEventToKafka(key, eventJSON);

        String de=alchemistKafkaEventHelper.getDEId();
        customOrderDetailsJSONMap.put("order_status", "assigned");
        eventJSON = alchemistKafkaEventHelper.returnOrderStatusUpdateJSONForAlchemistKafka(customOrderDetailsJSONMap);
        sendEventToKafka(key, eventJSON);

        if (RejectOrder) {
            customOrderDetailsJSONMap.put("order_status", "rejected");
            eventJSON = alchemistKafkaEventHelper.returnRejectedStatusUpdateJSONForAlchemistKafka(customOrderDetailsJSONMap);
            sendEventToKafka(key, eventJSON);
        }

        alchemistKafkaEventHelper.createNewDEIDGenerator(Integer.parseInt(masterRuleDataTCMap.get("zone_id")));

        customOrderDetailsJSONMap.put("order_status", "assigned");
        eventJSON = alchemistKafkaEventHelper.returnOrderStatusUpdateJSONForAlchemistKafka(customOrderDetailsJSONMap);
        sendEventToKafka(key, eventJSON);
        String de2=alchemistKafkaEventHelper.getDEId();

        doCAPRDViaKafkaEventsPush(key, eventJSON, setCAPRD_To_state);

        if (CancelOrder) {
            eventJSON = alchemistKafkaEventHelper.returnOrderCancelJSONForAlchemistKafka();
            sendEventToKafka(key, eventJSON);
        }

        customOrderDetailsJSONMap.put("de_id",de);
        eventJSON = alchemistKafkaEventHelper.returnTaskSequenceJSONForAlchemistKafka(customOrderDetailsJSONMap);
        sendEventToKafka(key, eventJSON);

        customOrderDetailsJSONMap.put("de_id",de2);
        eventJSON = alchemistKafkaEventHelper.returnTaskSequenceJSONForAlchemistKafka(customOrderDetailsJSONMap);
        sendEventToKafka(key, eventJSON);

        verifyRuleValidationsAsPerTestData();

        if (runTicketing) {
        }
        alchemistKafkaEventHelper.convertTestCaseResultstoJSON(masterRuleDataTCMap);
    }
}