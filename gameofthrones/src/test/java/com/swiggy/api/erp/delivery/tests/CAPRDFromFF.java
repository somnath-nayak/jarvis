package com.swiggy.api.erp.delivery.tests;

import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.erp.delivery.dp.CAPRDFromFFDataProvider;
import com.swiggy.api.erp.delivery.helper.AutoassignHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryControllerHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryHelperMethods;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceHelper;

import framework.gameofthrones.JonSnow.Processor;

public class CAPRDFromFF extends CAPRDFromFFDataProvider{
	DeliveryServiceHelper helpdel = new DeliveryServiceHelper();
	AutoassignHelper hepauto = new AutoassignHelper();
	DeliveryHelperMethods delmeth = new DeliveryHelperMethods();
	DeliveryControllerHelper delcon=new DeliveryControllerHelper();
	public CAPRDFromFF()
	{
		super(DeliveryConstant.caprdrestaurantid,DeliveryConstant.caprdrestaurantLat,DeliveryConstant.getCaprdrestaurantLon);
	}
	@BeforeTest
	public void datasetup()
	{
		delmeth.dbhelperupdate("update delivery_boys set status='FR' where id=2418");
		delmeth.dbhelperupdate("update delivery_boys set batch_id=null where id=2418");
	}

	@Test(dataProvider = "QE-334", groups = "CAPRDregressionMayur", description = "Verify OE is able to mark order assigned from FF and same is reflected in delivery")
	public void CAPRDFROMFF334(String order_id,String de_id, String version)
			throws InterruptedException {
		Assert.assertNotNull(order_id,"Order is not created through checkout helper");
		hepauto.delogin(de_id, version);
		String deotp= null;
		try {
			deotp = (new JSONObject(helpdel.getOtp(de_id.toString()).ResponseValidator.GetBodyAsText())).get("data").toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		//String deotp = delmeth.redisconnectget(DeliveryConstant.de_otp, de_id, DeliveryConstant.redisdb);
		hepauto.deotp(de_id, deotp);
		helpdel.makeDEFree(de_id);
		Thread.sleep(10000);
		Processor processor=helpdel.orderAssign(order_id, de_id);
		if((processor.ResponseValidator.GetResponseCode())==200)
		{
		String query = "Select * from trips where order_id=" + order_id + ";";
		Object assigndeid = delmeth.dbhelperget(query, "de_id");
		if ((null == assigndeid)
				|| (!de_id.equalsIgnoreCase(assigndeid.toString()))) {
			helpdel.makeDEFree(de_id);
			Assert.assertTrue(false);

		} else {
		     helpdel.makeDEFree(de_id);
		Assert.assertTrue(true);

		}
		}
	}

	@Test(dataProvider = "QE-335", groups = "CAPRDregressionMayur", description = "Verify OE is able to mark order confirmed from FF once DE is assigned to order and same is reflected in delivery")
	public void CAPRDFROMFF335(String order_id,String de_id, String version)
			throws InterruptedException {
		Assert.assertNotNull(order_id,"Order is not created through checkout helper");
		hepauto.delogin(de_id, version);
		String deotp= null;
		try {
			deotp = (new JSONObject(helpdel.getOtp(de_id.toString()).ResponseValidator.GetBodyAsText())).get("data").toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		//String deotp = delmeth.redisconnectget(DeliveryConstant.de_otp, de_id, DeliveryConstant.redisdb);
		hepauto.deotp(de_id, deotp);
		helpdel.makeDEFree(de_id);
		//Thread.sleep(10000);
	    helpdel.assignOrder(order_id, de_id);
		Thread.sleep(10000);
		delcon.orderConfirmedFromController(order_id);
		Thread.sleep(5000);
		String query = "Select * from trips where order_id=" + order_id + ";";
		Object confirmorder = delmeth.dbhelperget(query, "confirmed_time");
		if (null == confirmorder) {
			helpdel.makeDEFree(de_id);
			Assert.assertTrue(false);

		} else {
		     helpdel.makeDEFree(de_id);
		Assert.assertTrue(true);

		}
		}
				
	
	@Test(dataProvider = "QE-336", groups = "CAPRDregressionMayur", description = "Verify OE is not able to mark order confirmed from FF if DE is not assigned to order and same should not be reflected in delivery")
	public void CAPRDFROMFF336(String order_id,String version)
			throws InterruptedException {
		Assert.assertNotNull(order_id,"Order is not created through checkout helper");
		Thread.sleep(10000);
		  Processor processor=delcon.orderConfirmedFromController(order_id);
		  String resp=processor.ResponseValidator.GetBodyAsText();
		  if((processor.ResponseValidator.GetResponseCode())==200)
			{
			  Assert.assertEquals(JsonPath.read(resp, "$.statusCode").toString(),
						"1");
				Assert.assertEquals(JsonPath.read(resp, "$.statusMessage")
						.toString(),
						"can  not update the state from null to confirmed");
			  }
		 else {
		Assert.assertTrue(false);
	}
			}
	@Test(dataProvider = "QE-337", groups = "CAPRDregressionMayur", description = "Verify OE is able to mark order arrived from FF if order status is confirmed and same is reflected in delivery")
	public void CAPRDFROMFF337(String order_id,String de_id, String version)
			throws InterruptedException {
		Assert.assertNotNull(order_id,"Order is not created through checkout helper");
		hepauto.delogin(de_id, version);
		String deotp= null;
		try {
			deotp = (new JSONObject(helpdel.getOtp(de_id.toString()).ResponseValidator.GetBodyAsText())).get("data").toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		//String deotp = delmeth.redisconnectget(DeliveryConstant.de_otp, de_id, DeliveryConstant.redisdb);
		hepauto.deotp(de_id, deotp);
		helpdel.makeDEFree(de_id);
		Thread.sleep(10000);
	    helpdel.assignOrder(order_id, de_id);
		Thread.sleep(10000);
		delcon.orderConfirmedFromController(order_id);
		Thread.sleep(5000);
		delcon.orderArrivedFromController(order_id);
		Thread.sleep(5000);
		String query = "Select * from trips where order_id=" + order_id + ";";
		Object arriveorder = delmeth.dbhelperget(query, "arrived_time");
		if (null == arriveorder) {
			helpdel.makeDEFree(de_id);
			Assert.assertTrue(false);

		} else {
		     helpdel.makeDEFree(de_id);
		Assert.assertTrue(true);
		}
		}
	@Test(dataProvider = "QE-338", groups = "CAPRDregressionMayur", description = "Verify OE is not able to mark order arrived from FF if order status is assigned and same should not be reflected in delivery")
	public void CAPRDFROMFF338(String order_id,String state)
			throws InterruptedException {
		Assert.assertNotNull(order_id,"Order is not created through checkout helper");
		if (state.equalsIgnoreCase("arrived")) {
			Processor processor = delcon.orderArrivedFromController(order_id);
			String resp = processor.ResponseValidator.GetBodyAsText();
			if ((processor.ResponseValidator.GetResponseCode()) == 200) {
				Assert.assertEquals(JsonPath.read(resp, "$.statusCode")
						.toString(), "1");
				Assert.assertEquals(JsonPath.read(resp, "$.statusMessage")
						.toString(),
						"can  not update the state from assigned to arrived");

			} else {
				Assert.assertTrue(false);
			}
		} else if (state.equalsIgnoreCase("pickedup")) {
			Thread.sleep(10000);
			Processor processor = delcon.orderPickedupFromController(order_id);
			String resp = processor.ResponseValidator.GetBodyAsText();
			if ((processor.ResponseValidator.GetResponseCode()) == 200) {
				Assert.assertEquals(JsonPath.read(resp, "$.statusCode")
						.toString(), "1");
				Assert.assertEquals(JsonPath.read(resp, "$.statusMessage")
						.toString(),
						"can  not update the state from assigned to pickedup");

			} else {
				Assert.assertTrue(false);
			}
		} else if (state.equalsIgnoreCase("reached")) {
			Thread.sleep(10000);
			Processor processor = delcon.orderReachedFromController(order_id);
			String resp = processor.ResponseValidator.GetBodyAsText();
			if ((processor.ResponseValidator.GetResponseCode()) == 200) {
				Assert.assertEquals(JsonPath.read(resp, "$.statusCode")
						.toString(), "1");
				Assert.assertEquals(JsonPath.read(resp, "$.statusMessage")
						.toString(),
						"can  not update the state from assigned to reached");

			} else {
				Assert.assertTrue(false);
			}
		} else if (state.equalsIgnoreCase("delivered")) {
			Thread.sleep(10000);
			Processor processor =delcon.orderDeliveredFromController(order_id);
			String resp = processor.ResponseValidator.GetBodyAsText();
			if ((processor.ResponseValidator.GetResponseCode()) == 200) {
				Assert.assertEquals(JsonPath.read(resp, "$.statusCode")
						.toString(), "1");
				Assert.assertEquals(JsonPath.read(resp, "$.statusMessage")
						.toString(),
						"can not update the state from assigned to delivered");
			} else {
				Assert.assertTrue(false);
			}
		}
	
	}
	@Test(dataProvider = "QE-339", groups = "CAPRDregressionMayur", description = "Verify OE is able to mark order pickedup from FF if order status is arrived and same is reflected in delivery")
	public void CAPRDFROMFF339(String order_id,String de_id, String version)
			throws InterruptedException {
		Assert.assertNotNull(order_id,"Order is not created through checkout helper");
		hepauto.delogin(de_id, version);
		String deotp= null;
		try {
			deotp = (new JSONObject(helpdel.getOtp(de_id.toString()).ResponseValidator.GetBodyAsText())).get("data").toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		//String deotp = delmeth.redisconnectget(DeliveryConstant.de_otp, de_id,DeliveryConstant.redisdb);
		hepauto.deotp(de_id, deotp);
		helpdel.makeDEFree(de_id);
		Thread.sleep(10000);
	    helpdel.assignOrder(order_id, de_id);
		Thread.sleep(10000);
		delcon.orderConfirmedFromController(order_id);
		Thread.sleep(5000);
		delcon.orderArrivedFromController(order_id);
		Thread.sleep(5000);
		delcon.orderPickedupFromController(order_id);
		String query = "Select * from trips where order_id=" + order_id + ";";
		Object pickedorder = delmeth.dbhelperget(query, "pickedup_time");
		if (null == pickedorder) {
			helpdel.makeDEFree(de_id);
			Assert.assertTrue(false);

		} else {
		     helpdel.makeDEFree(de_id);
		Assert.assertTrue(true);

		}
		}
	
	@Test(dataProvider = "QE-340", groups = "CAPRDregressionMayur", description = "Verify OE is not able to mark order pickedup from FF if order status is confirmed and same should not be reflected in delivery")
	public void CAPRDFROMFF340(String order_id,String state)
			throws InterruptedException {
		Assert.assertNotNull(order_id,"Order is not created through checkout helper");
		if (state.equalsIgnoreCase("pickedup")) {
			Thread.sleep(10000);
			Processor processor = delcon.orderPickedupFromController(order_id);
			String resp = processor.ResponseValidator.GetBodyAsText();
			if ((processor.ResponseValidator.GetResponseCode()) == 200) {
				Assert.assertEquals(JsonPath.read(resp, "$.statusCode")
						.toString(), "1");
				Assert.assertEquals(JsonPath.read(resp, "$.statusMessage")
						.toString(),
						"can  not update the state from confirmed to pickedup");

			} else {
				Assert.assertTrue(false);
			}
		} else if (state.equalsIgnoreCase("reached")) {
			Thread.sleep(10000);
			Processor processor = delcon.orderReachedFromController(order_id);
			String resp = processor.ResponseValidator.GetBodyAsText();
			if ((processor.ResponseValidator.GetResponseCode()) == 200) {
				Assert.assertEquals(JsonPath.read(resp, "$.statusCode")
						.toString(), "1");
				Assert.assertEquals(JsonPath.read(resp, "$.statusMessage")
						.toString(),
						"can  not update the state from confirmed to reached");

			} else {
				Assert.assertTrue(false);
			}
		} else if (state.equalsIgnoreCase("delivered")) {
			Thread.sleep(10000);
			Processor processor =delcon.orderDeliveredFromController(order_id);
			String resp = processor.ResponseValidator.GetBodyAsText();
			if ((processor.ResponseValidator.GetResponseCode()) == 200) {
				Assert.assertEquals(JsonPath.read(resp, "$.statusCode")
						.toString(), "1");
				Assert.assertEquals(JsonPath.read(resp, "$.statusMessage")
						.toString(),
						"can  not update the state from confirmed to delivered");

			} else {
				Assert.assertTrue(false);
			}
		}
	
		}
	@Test(dataProvider = "QE-341", groups = "CAPRDregressionMayur", description = "Verify OE is able to mark order reached from FF if order status is pickedup and same is reflected in delivery")
	public void CAPRDFROMFF341(String order_id,String de_id, String version)
			throws InterruptedException {
		Assert.assertNotNull(order_id,"Order is not created through checkout helper");
		hepauto.delogin(de_id, version);
		String deotp= null;
		try {
			deotp = (new JSONObject(helpdel.getOtp(de_id.toString()).ResponseValidator.GetBodyAsText())).get("data").toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		//String deotp = delmeth.redisconnectget(DeliveryConstant.de_otp, de_id, DeliveryConstant.redisdb);
		hepauto.deotp(de_id, deotp);
		helpdel.makeDEFree(de_id);
		Thread.sleep(10000);
	    helpdel.assignOrder(order_id, de_id);
		Thread.sleep(10000);
		delcon.orderConfirmedFromController(order_id);
		Thread.sleep(5000);
		delcon.orderArrivedFromController(order_id);
		Thread.sleep(5000);
		delcon.orderPickedupFromController(order_id);
		Thread.sleep(10000);
		delcon.orderReachedFromController(order_id);
		String query = "Select * from trips where order_id=" + order_id + ";";
		Object reachedorder = delmeth.dbhelperget(query, "reached_time");
		if (null == reachedorder) {
			helpdel.makeDEFree(de_id);
			Assert.assertTrue(false);

		} else {
		     helpdel.makeDEFree(de_id);
		Assert.assertTrue(true);

		}
		}
	@Test(dataProvider = "QE-342", groups = "CAPRDregressionMayur", description = "Verify OE is not able to mark order reached from FF if order status is arrived and same should not be reflected in delivery")
	public void CAPRDFROMFF342(String order_id,String state)
			throws InterruptedException {
		Assert.assertNotNull(order_id,"Order is not created through checkout helper");
		 if (state.equalsIgnoreCase("reached")) {
			Thread.sleep(10000);
			Processor processor = delcon.orderReachedFromController(order_id);
			String resp = processor.ResponseValidator.GetBodyAsText();
			if ((processor.ResponseValidator.GetResponseCode()) == 200) {
				Assert.assertEquals(JsonPath.read(resp, "$.statusCode")
						.toString(), "1");
				Assert.assertEquals(JsonPath.read(resp, "$.statusMessage")
						.toString(),
						"can  not update the state from arrived to reached");

			} else {
				Assert.assertTrue(false);
			}
		} else if (state.equalsIgnoreCase("delivered")) {
			Thread.sleep(10000);
			Processor processor =delcon.orderDeliveredFromController(order_id);
			String resp = processor.ResponseValidator.GetBodyAsText();
			if ((processor.ResponseValidator.GetResponseCode()) == 200) {
				Assert.assertEquals(JsonPath.read(resp, "$.statusCode")
						.toString(), "1");
				Assert.assertEquals(JsonPath.read(resp, "$.statusMessage")
						.toString(),
						"can  not update the state from arrived to delivered");

			} else {
				Assert.assertTrue(false);
			}
		}
	
		}
	@Test(dataProvider = "QE-344", groups = "CAPRDregressionMayur", description = "Verify OE is able to mark order delivered from FF if order status is reached and same is reflected in delivery")
	public void CAPRDFROMFF344(String order_id,String de_id, String version)
			throws InterruptedException {
		Assert.assertNotNull(order_id,"Order is not created through checkout helper");
		hepauto.delogin(de_id, version);
		String deotp= null;
		try {
			deotp = (new JSONObject(helpdel.getOtp(de_id.toString()).ResponseValidator.GetBodyAsText())).get("data").toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		//String deotp = delmeth.redisconnectget(DeliveryConstant.de_otp, de_id,DeliveryConstant.redisdb);
		hepauto.deotp(de_id, deotp);
		helpdel.makeDEFree(de_id);
		Thread.sleep(10000);
	    helpdel.assignOrder(order_id, de_id);
		Thread.sleep(10000);
		delcon.orderConfirmedFromController(order_id);
		Thread.sleep(5000);
		delcon.orderArrivedFromController(order_id);
		Thread.sleep(5000);
		delcon.orderPickedupFromController(order_id);
		Thread.sleep(5000);
		delcon.orderReachedFromController(order_id);
		Thread.sleep(5000);
		delcon.orderDeliveredFromController(order_id);
		Thread.sleep(10000);
		String query = "Select * from trips where order_id=" + order_id + ";";
		Object reachedorder = delmeth.dbhelperget(query, "delivered_time");
		if (null == reachedorder) {
			helpdel.makeDEFree(de_id);
			Assert.assertTrue(false);

		} else {
		     helpdel.makeDEFree(de_id);
		Assert.assertTrue(true);

		}
		}
	
	@Test(dataProvider = "QE-345", groups = "CAPRDregressionMayur", description = "Verify OE is able to mark order delivered from FF if order status is pickedup and same is reflected in delivery")
	public void CAPRDFROMFF345(String order_id,String de_id, String version)
			throws InterruptedException {
		Assert.assertNotNull(order_id,"Order is not created through checkout helper");
		hepauto.delogin(de_id, version);
		String deotp= null;
		try {
			deotp = (new JSONObject(helpdel.getOtp(de_id.toString()).ResponseValidator.GetBodyAsText())).get("data").toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		//String deotp = delmeth.redisconnectget(DeliveryConstant.de_otp, de_id, DeliveryConstant.redisdb);
		hepauto.deotp(de_id, deotp);
		helpdel.makeDEFree(de_id);
		Thread.sleep(10000);
	    helpdel.assignOrder(order_id, de_id);
		Thread.sleep(10000);
		delcon.orderConfirmedFromController(order_id);
		Thread.sleep(5000);
		delcon.orderArrivedFromController(order_id);
		Thread.sleep(5000);
		delcon.orderPickedupFromController(order_id);
		Thread.sleep(5000);
		delcon.orderDeliveredFromController(order_id);
		Thread.sleep(10000);
		String query = "Select * from trips where order_id=" + order_id + ";";
		Object reachedorder = delmeth.dbhelperget(query, "delivered_time");
		if (null == reachedorder) {
			helpdel.makeDEFree(de_id);
			Assert.assertTrue(false);

		} else {
		     helpdel.makeDEFree(de_id);
		Assert.assertTrue(true);

		}
		}
	@Test(dataProvider = "QE-348", groups = "CAPRDregressionMayur", description = "Verify OE is able to mark order re-assigned and same is reflected in delivery")
	public void CAPRDFROMFF348(String order_id,String de_id, String version,String city_id)
			throws InterruptedException {
		Assert.assertNotNull(order_id,"Order is not created through checkout helper");
		hepauto.delogin(de_id, version);
		String deotp= null;
		try {
			deotp = (new JSONObject(helpdel.getOtp(de_id.toString()).ResponseValidator.GetBodyAsText())).get("data").toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		//String deotp = delmeth.redisconnectget(DeliveryConstant.de_otp, de_id, DeliveryConstant.redisdb);
		hepauto.deotp(de_id, deotp);
		helpdel.makeDEFree(de_id);
		Thread.sleep(10000);
	    helpdel.assignOrder(order_id, de_id);
		Thread.sleep(10000);
		delcon.orderConfirmedFromController(order_id);
		Thread.sleep(5000);
		delcon.reasssignFromFF(order_id, city_id);
		Thread.sleep(10000);
		String query = "Select * from trips where order_id=" + order_id + ";";
		Object reachedorder = delmeth.dbhelperget(query, "de_id");
		if (null == reachedorder) {
			helpdel.makeDEFree(de_id);
			Assert.assertTrue(true);

		} else {
		     helpdel.makeDEFree(de_id);
		Assert.assertTrue(false);

		}
		}

}
