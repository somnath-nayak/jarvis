package com.swiggy.api.erp.delivery.helper;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.delivery.Pojos.Last_miles;
import com.swiggy.api.erp.delivery.Pojos.Restaurant;
import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.erp.delivery.constants.RTSConstants;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.helper.CommonAPIHelper;
import com.swiggy.api.sf.checkout.pojo.Cart;
import com.swiggy.api.sf.checkout.pojo.CreateMenuEntry;
import com.swiggy.api.sf.checkout.pojo.CreateOrderBuilder;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Daenerys.Initializer;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class RTSHelper {

		Initialize gameofthrones;
		DeliveryHelperMethods delmeth;
		JsonHelper jsonHelper;
		CheckoutHelper checkhelp;
		 CartSLA_Helper serhelp;
		 DeliveryDataHelper deldatahelp;
		 DeliveryServiceHelper delserv;
		String tid,token;
		public RTSHelper() {
			gameofthrones =  Initializer.getInitializer();
			delmeth=new DeliveryHelperMethods();
			jsonHelper = new JsonHelper();
			checkhelp=new CheckoutHelper();
			serhelp = new CartSLA_Helper();
			deldatahelp=new DeliveryDataHelper();
			delserv=new DeliveryServiceHelper();
					}

		

		
		public Processor updateDEExecutiveLocation(String deId,String lat, String lng,String cityId,String enabled,String assignedZones)
				throws InterruptedException {
			HashMap<String, String> requestheaders = delmeth.singleheader();
			GameOfThronesService service = new GameOfThronesService(
					"deliveryrts", "rtsupdatedeliveryexeclocation", gameofthrones);
			String[] payloadparams = new String[] { deId, lat,lng,cityId,enabled,assignedZones};
			Processor processor = new Processor(service, requestheaders,
					payloadparams);
			return processor;
		}

		public Processor getAllDEWithinRadius(String cityId,String zoneId, String lat,String lng,String radius)
				throws InterruptedException {

			HashMap<String, String> requestheaders = delmeth.singleheader();
			GameOfThronesService service = new GameOfThronesService(
					"deliveryrts", "rtsgetalldeliveryexecswithinradius", gameofthrones);
			String[] payloadparams = new String[] { cityId, zoneId,lat,lng, radius};
			Processor processor = new Processor(service, requestheaders,
					payloadparams);
			return processor;
		}

		public Processor getDESchedules(String deIds)
				throws InterruptedException {

			HashMap<String, String> requestheaders = delmeth.singleheader();
			GameOfThronesService service = new GameOfThronesService(
					"deliveryrts", "rtsgetdeliveryexecschedules", gameofthrones);
			String[] payloadparams = new String[] { deIds};
			Processor processor = new Processor(service, requestheaders,
					payloadparams);
			return processor;
		}

		public Processor getAllDEWithinZone(String cityId,String zoneId)
				throws InterruptedException {

			HashMap<String, String> requestheaders = delmeth.singleheader();

			GameOfThronesService service = new GameOfThronesService(
					"deliveryrts", "rtsgetalldeliveryexecswithinzone", gameofthrones);
			String[] payloadparams = new String[] { cityId,zoneId };
			Processor processor = new Processor(service, requestheaders,
					payloadparams);
			return processor;
		}

		public Processor updateDE(String deId,String lat,String lng,String assignedZones,String enabled,String cityId)
				throws InterruptedException {

			HashMap<String, String> requestheaders = delmeth.singleheader();

			GameOfThronesService service = new GameOfThronesService(
					"deliveryrts", "rtsupdatedeliveryexec", gameofthrones);
			String[] payloadparams = new String[] { deId,lat,lng,assignedZones,enabled,cityId};
			Processor processor = new Processor(service, requestheaders,
					payloadparams);
			return processor;
		}
		
		public Processor scheduleTasks(String taskId,String picklat,String picklng,String droplat,String droplng,String predictedStartTime,String predictedTimeToComplete,String cityId,String zoneId,String createdAtTime)
				throws InterruptedException {

			HashMap<String, String> requestheaders = delmeth.singleheader();
			GameOfThronesService service = new GameOfThronesService(
					"deliveryrts", "rtsscheduletasks", gameofthrones);
			String[] payloadparams = new String[] { taskId,picklat,picklng,droplat,droplng,predictedStartTime,predictedTimeToComplete,cityId,zoneId,createdAtTime};
			Processor processor = new Processor(service, requestheaders,
					payloadparams);
			return processor;
		}
		public Processor cancelTask(String taskId)
				throws InterruptedException {
			HashMap<String, String> requestheaders = delmeth.singleheader();
			GameOfThronesService service = new GameOfThronesService(
					"deliveryrts", "rtscanceltask", gameofthrones);
			String[] payloadparams = new String[] { taskId};
			Processor processor = new Processor(service, requestheaders,
					payloadparams);
			return processor;
		}
		
		public Processor assignmentResolve(String cityId,String zoneId)
				throws InterruptedException {

			HashMap<String, String> requestheaders = delmeth.singleheader();
			GameOfThronesService service = new GameOfThronesService(
					"deliveryrts", "rtsresolvedeliveryexecschedulesafterassignment", gameofthrones);
			String[] payloadparams = new String[] { cityId, zoneId};
			Processor processor = new Processor(service, requestheaders,
					payloadparams);
			return processor;
		}
		
		public String CreateRTSOrder(int address_id,long mobile,String password,int rest) throws IOException
	    {
	        CreateMenuEntry cartpayload= place_order(address_id,mobile,password,rest);
	        String cartPayload= jsonHelper.getObjectToJSON(cartpayload.getCartItems());
	        HashMap<String, String> hashMap=checkhelp.createLogin(cartpayload.getpassword(), cartpayload.getmobile());
	        String response= checkhelp.CreateCartAddon(hashMap.get("Tid"), hashMap.get("Token"), cartPayload, cartpayload.getRestaurantId()).ResponseValidator.GetBodyAsText();
	        String restId = JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[", "").replace("]", "");
	        String menuItem = JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[", "").replace("]", "");
	        String[] menuItems= menuItem.split(",");
	        int noOfProducts = menuItems.length;
	        System.out.println("TID="+hashMap.get("Tid"));
	        System.out.println("TOKEN="+hashMap.get("Token"));
	        checkhelp.validateDataFromResponseNew(response,restId,menuItem,noOfProducts);
	        String order= checkhelp.orderPlace1(hashMap.get("Tid"), hashMap.get("Token"), cartpayload.getaddress_id(),cartpayload.getpayment_cod_method(),cartpayload.getorder_comments()).ResponseValidator.GetBodyAsText();
	       return  order;
	       
	    }

	    public CreateMenuEntry place_order(int address_id,Long mobile,String password,int rest) throws IOException
	    {
	        List<Cart> cart = new ArrayList<>();
	        cart.add(new Cart(null, null,RTSConstants.rts_item_id, 1));
	        CreateMenuEntry createOrder = new CreateOrderBuilder()
	                .password(password)
	                .mobile(mobile)
	                .cart(cart)
	                .restaurant(rest)
	                .Address(address_id)
	                .paymentMethod("Cash")
	                .orderComments("Test-Order")
	                .buildAll();
	        return createOrder;
	    }

	    
	    public Processor runassignment(String city)
	    {
			GameOfThronesService service = new GameOfThronesService(
					"assignmentrun", "runassignment", gameofthrones);
			HashMap<String, String> requestHeaders = delmeth.singleheader();
			String[] urlParams = new String[] { city};
			Processor processor = new Processor(service, requestHeaders, null, urlParams);
			return processor;
		}
    
	     public Processor getdata(String listing_count,String bill,String total_item,String cartstate,String order_id,String rest,String lat_lng,String area,String cust_lat_lng) throws IOException
		{
			Processor processor = null;
			Restaurant restaurant = new Restaurant();
			restaurant.defaultresultset();
			restaurant.setRestaurant_id(rest);
			restaurant.setLat_long(lat_lng);
			restaurant.setArea_id(area);
			Last_miles last_mile = new Last_miles();
			last_mile.setdefaultvalues();
	        last_mile.setAddress_lat_long(cust_lat_lng);
			Last_miles[] last = new Last_miles[1];
			last[0] = last_mile;
			int random = ((new Random().nextInt(10000))+1);
			if(null==order_id)
				
		{
			processor = serhelp.cartslaapi(restaurant,
					listing_count, last,
					bill,"",String.valueOf(RTSConstants.cart_id + random),
					cartstate,total_item);
		}
		else
		{
			processor = serhelp.cartslaapi(restaurant,
					listing_count, last,
					bill, order_id,
					String.valueOf(RTSConstants.cart_id + random),
					cartstate,total_item);
		}
            return processor;
		}
	    public Processor getdatadominos(String listing_count,String bill,String total_item,String cartstate,String order_id,String rest,String lat_lng,String area,String cust_lat_lng) throws IOException
	  		{ //RTSConstants.dominos_rest_id RTSConstants.dominos_rest_lat_lng RTSConstants.rts_area_id RTSConstants.dominos_cust_lat_lng
	  			Processor processor = null;
	  			Restaurant restaurant = new Restaurant();
	  			restaurant.defaultresultset();
	  			restaurant.setRestaurant_id(rest);
	  			restaurant.setLat_long(lat_lng);
	  			restaurant.setArea_id(area);
	  			Last_miles last_mile = new Last_miles();
	  			last_mile.setdefaultvalues();
	  	        last_mile.setAddress_lat_long(cust_lat_lng);
	  			Last_miles[] last = new Last_miles[1];
	  			last[0] = last_mile;
	  			int random = ((new Random().nextInt(10000))+1);
	  			if(null==order_id)
	  				
	  		{
	  			processor = serhelp.cartslaapi(restaurant,
	  					listing_count, last,
	  					bill,"",String.valueOf(RTSConstants.cart_id + random),
	  					cartstate,total_item);
	  		}
	  		else
	  		{
	  			processor = serhelp.cartslaapi(restaurant,
	  					listing_count, last,
	  					bill, order_id,
	  					String.valueOf(RTSConstants.cart_id + random),
	  					cartstate,total_item);
	  		}
                return processor;
	  		}
		public String generaterandomorderid()
		{
			CommonAPIHelper commonAPIHelper = new CommonAPIHelper();
			int random = commonAPIHelper.getRandomNo(0, 10000);
			String order_id=RTSConstants.randomorder_id+random+1;
			return order_id;
		}
		public String getsla(Processor processor)
		{
			Object ser = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),
					"$.cart..sla");
			String sla = ser.toString().replace("[", "").replace("]", "");
			return sla;
		}
		public String getserviceability(Processor processor)
		{
			Object ser = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),
					"$.cart..serviceable");
			String serviceability = ser.toString().replace("[", "").replace("]", "");
			return serviceability;
		}
		public String getnonserviceabilityreason(Processor processor)
		{
			Object non_ser = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),
					"$.cart..non_serviceable_reason");
			String non_service_reason=non_ser.toString().replace("[", "").replace("]", "");
		return non_service_reason;
		}
		
		public int checkaddressexistence(int addresss_id,String lat,String lng,String mobile,String password)
		{int flag=0;
			HashMap<String, String> map=checkhelp.TokenData(mobile, password);
			tid=map.get("Tid");
			token=map.get("Token");
			Processor processor=checkhelp.invokegetAllAddressAPI(tid, token);
			List<String> lm=JsonPath.read(processor.ResponseValidator.GetBodyAsText(),
					"$..data.addresses..id");
			for(int i=0;i<lm.size();i++)
			{
				String s=lm.get(i).toString().replace("[", "").replace("]", "");
				if(s.equalsIgnoreCase(String.valueOf(addresss_id)))
				{
					flag=1;
				}
			}
			if(flag==0)
			{
				HashMap<String,String> adressData=new HashMap<String, String>();
				adressData.put("name", "NEWADDRESS012");
				adressData.put("mobile", "1234567890");
				adressData.put("address", "4, 17th Main, Near BDA Complex, 100 Feet Road, 4th B Block,Koramangala");
				adressData.put("landmark", "Swiggy office Land Mark");
				adressData.put("area", "Swiggy Test");
				adressData.put("lat", lat);
				adressData.put("lng", lng);
				adressData.put("flat_no", "4321");
				adressData.put("city", "Bangalore");
				adressData.put("annotation", " ");
		Processor processor2=CheckoutHelper.addNewAddress(adressData, tid, token);
		String id=JsonPath.read(processor2.ResponseValidator.GetBodyAsText(),"$..data.address_id").toString();
		int address_id=Integer.parseInt(id.replace("[", "").replace("]", ""));
		return address_id;
			}
			else
			{
				return addresss_id;
			}
			}
		public void updateDERTS(String de_id,String lat,String lng,String zone,String enabled,String cityid,boolean locupdate) throws InterruptedException
		{
			if(locupdate==true)
			{
		updateDE(de_id, lat,lng,zone,enabled,cityid);
		updateDEExecutiveLocation(de_id,lat,lng,cityid,enabled,zone);
		deldatahelp.delocationupdate(lat,lng, de_id, DeliveryConstant.delivery_app_version);
			}
			else
			{
				updateDE(de_id, lat,lng,zone,enabled,cityid);
				updateDEExecutiveLocation(de_id,lat,lng,cityid,enabled,zone);
				
			}
	  	}
		
		public void checklongdistancepolygonexistence(String polygonquery,String polygonid)
		{	
			String query=polygonquery+polygonid;
			Object id=delmeth.dbhelperget(query, "id",  DeliveryConstant.databaseName);
			if(id==null)
			{
				delserv.createLongDistancePolgyon(RTSConstants.long_distance_polygon_latlongs, "true", "100", "6.0");
			}
		}
		
		public void updatstartandstopbf(String startbf,String stopbf,double redisbf)
		{
			String query1="update delivery_config set meta_value="+startbf +"where meta_key='rts_start_bfactor'";
			delmeth.dbhelperupdate(query1,  DeliveryConstant.databaseName);
			String query2="update delivery_config set meta_value="+stopbf +"where meta_key='rts_stop_bfactor'";
			delmeth.dbhelperupdate(query2,  DeliveryConstant.databaseName);
			delmeth.redisconnectsetdouble(RTSConstants.get_bf, RTSConstants.single_assignedZones, 1, redisbf, RTSConstants.delivery_redis);
	}
}
