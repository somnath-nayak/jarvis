package com.swiggy.api.erp.delivery.helper;

import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Daenerys.Initializer;
import framework.gameofthrones.Tyrion.DBHelper;
import framework.gameofthrones.Tyrion.RedisHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DeliveryHelperMethods {
	//Initialize gameofthrones = new Initialize();
	Initialize init =Initializer.getInitializer();

	public Object dbhelperget(String query, String entity) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate("deliverydb");
		List<Map<String, Object>> list = sqlTemplate.queryForList(query);
		if (list == null || list.isEmpty()) {
			return null;
		} else {
			return list.get(0).get(entity);
		}
	}
	public Object dbhelperget(String query, String entity,String dbname) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(dbname);
		List<Map<String, Object>> list = sqlTemplate.queryForList(query);
		if (list == null || list.isEmpty()) {
			return null;
		} else {
			return list.get(0).get(entity);
		}
	}
	public List<Map<String, Object>> dbhelpergetall(String query) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate("deliverydb");
		List<Map<String, Object>> list = sqlTemplate.queryForList(query);
		if (list == null || list.isEmpty()) {
			return null;
		} else {
			return list;
		}
	}
	public List<Map<String, Object>> dbhelpergetall(String query, String db_name) {
		DBHelper db = new DBHelper();
		SqlTemplate sqlTemplate = db.getMySqlTemplate(db_name);
		List<Map<String, Object>> list = sqlTemplate.queryForList(query);
		if (list == null || list.isEmpty()) {
			return null;
		} else {
			return list;
		}
	}

	public HashMap<String, String> doubleheader() {
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("content-type", "application/json");
		requestheaders.put("Authorization", "Basic R0dZU1dJOjIwMTVTVyFHR1k=");
		return requestheaders;

	}

	
	public HashMap<String, String> singleheader() {
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("content-type", "application/json");
		return requestheaders;

	}

	
	public void dbhelperupdate(String query) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate("deliverydb");
		sqlTemplate.update(query);

	}

	public void dbhelperupdate(String query,String dbname) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(dbname);
		sqlTemplate.update(query);

	}
	public Object redisconnectget(String keyinitial, String entity, int redisdb) {
		RedisHelper red = new RedisHelper();
		String s = keyinitial;
		String key = s.concat(entity);
		Object value = red.getValue("deliveryredis", redisdb, key);
		return value;

	}

	public void redisconnectsetdouble(String keyinitial, String entity,
			int redisdb, double value1,String box) {
		RedisHelper red = new RedisHelper();
		String s = keyinitial;
		String key = s.concat(entity);
		red.getRedisTemplate(box, redisdb).opsForValue().set(key, value1);
	}
	public void redisconnectsetrestid(String keyinitial,
			int redisdb, String value1,String box) {
		RedisHelper red = new RedisHelper();
		red.getRedisTemplate(box, redisdb).opsForValue().set(keyinitial, value1);
	}
	public void redisconnectsetdoubleStrinvalue(String keyinitial, String entity,
			int redisdb, String value1,String box) {
		RedisHelper red = new RedisHelper();
		String s = keyinitial;
		String key = s.concat(entity);
		red.getRedisTemplate(box, redisdb).opsForValue().set(key, value1);
	}

	public void redisconnectsetboolean(String keyinitial, String entity,
			int redisdb, boolean value1) {
		RedisHelper red = new RedisHelper();
		String s = keyinitial;
		String key = s.concat(entity);
		red.getRedisTemplate("deliveryredis", redisdb).opsForValue().set(key, value1);
	}

	public void rediskeydel(String keyinitial, String entity,
			int redisdb,String box) {
		RedisHelper red = new RedisHelper();
		String s = keyinitial;
		String key = s.concat(entity);
		red.getRedisTemplate(box, redisdb).delete(key);
	}

	public HashMap<String, String> formdataheaders() {
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("authorization", DeliveryConstant.basicauth);
		requestheaders.put("content-type", "application/form-data");
		return requestheaders;
	}

	public String getSolrUrl() {
		String environment = init.EnvironmentDetails.setup.getEnvironmentData().getName().toLowerCase();
		String solrUrl = null;
		switch (environment){
			case "stage1":
				solrUrl = DeliveryConstant.stage1_solrUrl;
				break;
			case "stage2":
				solrUrl = DeliveryConstant.stage2_solrUrl;
				break;
			case "stage3":
				solrUrl = DeliveryConstant.stage3_solrUrl;
				break;
			case "u4":
				solrUrl = DeliveryConstant.u4_listing_solrUrl;
				break;
			default:
				System.out.println("***** Environment not found *****");
		}
		return solrUrl;
	}

	public String getcurrtimehour()
	{
		LocalDateTime now = LocalDateTime.now();
		int hour=now.getHour();
			return String.valueOf(hour);
	}
	public int getweekday()
	{
	
		LocalDateTime now = LocalDateTime.now();
       	String s=now.getDayOfWeek().toString();
			int weekday = 0;
			switch(s)
			{
			case "MONDAY":
				weekday=0;
				break;
			case "TUESDAY":
				weekday=1;
				break;
			case "WEDNESDAY":
				weekday=2;
				break;
			case "THURSDAY":
				weekday=3;
				break;
			case "FRIDAY":
				weekday=4;
				break;
			case "SATURDAY":
				weekday=5;
				break;
			case "SUNDAY":
				weekday=6;
				break;
				
			}
			
			return weekday;
		
	}
	public String getslot(String hour)
	{
		int hr=Integer.parseInt(hour);
		String s = "BREAKFAST";
		if(hr>=7 && hr<11)
		{
			s="breakfast";
		}
		else if(hr>=11 && hr<15)
		{
			s="lunch";
		}
		else if(hr>=15 && hr<18)
		{
			s="snack";
		}
		else if(hr>=18 && hr<23)
		{
			s="dinner";
		}
		else if(hr>=23 && hr<4)
		{
			s="midnight";
		}
		return s;
		}
}
