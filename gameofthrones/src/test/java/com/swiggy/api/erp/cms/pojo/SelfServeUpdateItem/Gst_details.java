package com.swiggy.api.erp.cms.pojo.SelfServeUpdateItem;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.erp.cms.pojo.SelfServeUpdateItem
 **/
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "sgst",
        "cgst",
        "igst",
        "inclusive"
})
public class Gst_details {

    @JsonProperty("sgst")
    private String sgst;
    @JsonProperty("cgst")
    private String cgst;
    @JsonProperty("igst")
    private String igst;
    @JsonProperty("inclusive")
    private Boolean inclusive;

    @JsonProperty("sgst")
    public String getSgst() {
        return sgst;
    }

    @JsonProperty("sgst")
    public void setSgst(String sgst) {
        this.sgst = sgst;
    }

    @JsonProperty("cgst")
    public String getCgst() {
        return cgst;
    }

    @JsonProperty("cgst")
    public void setCgst(String cgst) {
        this.cgst = cgst;
    }

    @JsonProperty("igst")
    public String getIgst() {
        return igst;
    }

    @JsonProperty("igst")
    public void setIgst(String igst) {
        this.igst = igst;
    }

    @JsonProperty("inclusive")
    public Boolean getInclusive() {
        return inclusive;
    }

    @JsonProperty("inclusive")
    public void setInclusive(Boolean inclusive) {
        this.inclusive = inclusive;
    }

    private void setDefaultValues() {
        if(this.getIgst() == null)
            this.setIgst("[SUBTOTAL]*0.37");
        if(this.getCgst() == null)
            this.setCgst("[SUBTOTAL]*0.38");
        if(this.getSgst() == null)
            this.setSgst("[SUBTOTAL]*0.37");
        if(this.getInclusive() == null)
            this.setInclusive(true);
    }

    public Gst_details build() {
        setDefaultValues();
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("sgst", sgst).append("cgst", cgst).append("igst", igst).append("inclusive", inclusive).toString();
    }

}
