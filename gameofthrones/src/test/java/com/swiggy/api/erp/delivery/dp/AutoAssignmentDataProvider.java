package com.swiggy.api.erp.delivery.dp;

import com.swiggy.api.erp.delivery.helper.DeliveryDataHelper;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Tyrion.SqlTemplate;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;
import java.util.*;
import org.testng.annotations.DataProvider;

import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import com.swiggy.api.erp.delivery.helper.AutoassignHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryHelperMethods;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;

import framework.gameofthrones.JonSnow.Processor;

public class AutoAssignmentDataProvider {

	//static String order_id, withdeorder_id;
	AutoassignHelper autoassignHelper = new AutoassignHelper();
	DeliveryHelperMethods deliveryHelperMethods = new DeliveryHelperMethods();
	DeliveryServiceHelper deliveryServiceHelper = new DeliveryServiceHelper();
	DeliveryDataHelper deliveryDataHelper=new DeliveryDataHelper();
    Integer zone_id=34;
	String restaurantId;
	String restaurantLat;
	String restaurantLon;
	String de_id;
	String de_auth;
	String appversion="1.9";
	Integer city_id;
	Integer areaid;
	String cyclede;
	String reject_de;
	String reject_deAuth;




	public AutoAssignmentDataProvider()
	{
		String query = "select res.id from restaurant as res inner join area as ar on res.area_code=ar.id where res.enabled= 1 and ar.zone_id="+zone_id+ " order by 1 desc limit 1";
		restaurantId= SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForList(query).get(0).get("id").toString();
		String path= SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).
				queryForList("select path from zone where id = "+zone_id).get(0).get("path").toString();
		ArrayList<String> pathList = new ArrayList<>(
				Arrays.asList(path.split(" ")));
		String latlon=deliveryDataHelper.getCentroid(pathList);
		restaurantLat=latlon.split(",")[0];
		restaurantLon=latlon.split(",")[1];
		SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).execute("update restaurant set lat_long= \""+latlon+"\"");
		areaid= Integer.parseInt(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select id from area where zone_id="+zone_id).get("id").toString());
		city_id=Integer .parseInt(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select city_id from zone  where id ="+zone_id).get("city_id").toString());
		//de_id=deliveryDataHelper.CreateDE(zone_id,areaid);
		de_id="59942";
		de_auth=deliveryDataHelper.getDEAuth(de_id,"1.9");
		System.out.println("auth is " +de_auth);
		System.out.println("de_id is  " +de_id);
		//cyclede=deliveryDataHelper.createBicycleDE(zone_id,areaid,1);
		cyclede="59943";
		reject_de= deliveryDataHelper.CreateDE(zone_id);
		reject_deAuth=deliveryDataHelper.getDEAuth(reject_de,"1.9");

	}


/*	@DataProvider(name = "updatedeloc")
	public Object[][] locupdatede() throws Exception {
		return new Object[][] { { "59888", "`", "12.90612", "77.6291143" },

		};
	}*/

	public Map<String, String> getdefaultMapforOrder()
	{
		Map<String, String> orderDefaultValues= new HashMap<>();
		orderDefaultValues.put("restaurant_id",restaurantId);
		orderDefaultValues.put("restaurant_lat_lng",restaurantLat+","+restaurantLon);
		orderDefaultValues.put("restaurant_area_code",SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).
				queryForList("select id from area where zone_id ="+zone_id).get(0).get("id").toString());
		orderDefaultValues.put("order_time",deliveryDataHelper.getcurrentDateTimefororderTime());
		orderDefaultValues.put("order_type","regular");
		orderDefaultValues.put("lat",restaurantLat);
		orderDefaultValues.put("lng",restaurantLon);
		return orderDefaultValues;
	}
	@DataProvider(name = "QE-224")
	public Object[][] AutoassigntestDP224() throws Exception {
		deliveryServiceHelper.makeDEFree(de_id);
		CMSHelper cms = new CMSHelper();
		cms.setRestaurantCloseTime(restaurantId, 2300);
		Map<String, String> defMap= new HashMap<>();
		deliveryHelperMethods.dbhelperupdate(DeliveryConstant.updatebatch);
		deliveryHelperMethods.dbhelperupdate(DeliveryConstant.assigndetounassinged);
		String orderJson=deliveryDataHelper.returnOrderJson(getdefaultMapforOrder());
		boolean order_status=deliveryDataHelper.pushOrderJsonToRMQAndValidate(orderJson);
		String order_id=null;
		System.out.println(orderJson);
		if(order_status)
		{
			order_id=(new JSONObject(orderJson)).get("order_id").toString();
		}
		return new Object[][] {
		{ order_id,de_id,de_auth, appversion, restaurantLat,
				restaurantLon,orderJson }
		};
	}

	@DataProvider(name = "QE-225")
	public Object[][] AutoassigntestDP225() throws Exception {
		String orderJson=deliveryDataHelper.returnOrderJson(getdefaultMapforOrder());
		boolean order_status=deliveryDataHelper.pushOrderJsonToRMQAndValidate(orderJson);
		String order_id=null;
		System.out.println(orderJson);
		if(order_status)
		{
			order_id=(new JSONObject(orderJson)).get("order_id").toString();
		}

		SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).execute("update auto_assignment_param_values set value = .1 where zone_id ="+zone_id+" and parameter_id=5");
		String distancelatlon= deliveryDataHelper.getApproxLatLongAtADistance(Double.parseDouble(restaurantLat),Double.parseDouble(restaurantLon),.3);
		String distancelat=distancelatlon.split(",")[0];
		String distancelon=distancelatlon.split(",")[1];
		SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).execute("update auto_assignment_param_values set value = 4 where zone_id ="+zone_id+" and parameter_id=5");
		return new Object[][] {
		{ order_id, de_id	, appversion, distancelat,
				distancelon,orderJson, zone_id }
		};
	}

	@DataProvider(name = "QE-226")
	public Object[][] AutoassigntestDP226() throws Exception {
		String orderJson = deliveryDataHelper.returnOrderJson(getdefaultMapforOrder());
		boolean order_status = deliveryDataHelper.pushOrderJsonToRMQAndValidate(orderJson);
		String order_id = null;
		System.out.println(orderJson);
		if (order_status) {
            order_id = (new JSONObject(orderJson)).get("order_id").toString();
        }
			return new Object[][]{
					{order_id, de_id, appversion, restaurantLat, restaurantLon,orderJson}
			};
		}


	@DataProvider(name = "QE-227")
	public Object[][] AutoassigntestDP227() throws Exception {
		String orderJson=deliveryDataHelper.returnOrderJson(getdefaultMapforOrder());
		boolean order_status=deliveryDataHelper.pushOrderJsonToRMQAndValidate(orderJson);
		String order_id=deliveryDataHelper.getOrderId(restaurantId,restaurantLat,restaurantLon);
		return new Object[][] {
				{ order_id, de_id, appversion, restaurantLat,restaurantLon }
		};
	}

	// ########################################
	@DataProvider(name = "QE-241")
	public Object[][] AutoAssignTest_CycleDE() throws Exception {
		//SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).execute("update delivery_boys set bycycle=1 where id ="+de_id);
		//deliveryServiceHelper.makeDEBusy(DeliveryConstant.MayurDE);
		deliveryServiceHelper.makeDEBusy(de_id);
        deliveryServiceHelper.makeDEFree(cyclede) ;
        deliveryServiceHelper.makeDEActive(cyclede) ;

        autoassignHelper.updateParamAuto(city_id.toString(),zone_id.toString(),
				DeliveryConstant.Max_first_mile_Cycles,
				DeliveryConstant.Max_first_mile_Cycles_value, "test",
				DeliveryConstant.autoassign_maxfirstmileparamname);
		autoassignHelper.updateParamAuto(DeliveryConstant.city,zone_id.toString(),
				DeliveryConstant.Max_last_mile_Cycles,
				DeliveryConstant.Max_last_mile_Cycles_value, "test",
				DeliveryConstant.autoassign_maxlastmileparamname);
        String orderJson=deliveryDataHelper.returnOrderJson(getdefaultMapforOrder());
        boolean order_status=deliveryDataHelper.pushOrderJsonToRMQAndValidate(orderJson);
        String order_id=null;
        System.out.println(orderJson);
        if(order_status)
        {
            order_id=(new JSONObject(orderJson)).get("order_id").toString();
        }
		return new Object[][] { { order_id,cyclede, appversion, restaurantLat,
				restaurantLon, orderJson } };
	}

/*	public static String createOrder_CycleDE() throws Exception {
		CheckoutHelper helperc = new CheckoutHelper();
		Object[][] data = cartItem();
		Processor p = helperc.placeOrder(DeliveryConstant.mobile,
				DeliveryConstant.password, data[0][0].toString(),
				data[0][1].toString(), data[0][2].toString());
		return order_id = p.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.order_id");
	}*/

/*	public static Object[][] cartItem() {

		return new Object[][] { { "84406", "2", "620" } };
	}*/

	// ###########################################
	@DataProvider(name = "QE-242")
	public Object[][] AutoAssignTest_CycleDE1() throws Exception {
		CMSHelper cms = new CMSHelper();
		cms.setRestaurantCloseTime(restaurantId, 2300);
        autoassignHelper.updateParamAuto(city_id.toString(),zone_id.toString(),
                DeliveryConstant.Max_first_mile_Cycles,
                "0.1", "test",
                DeliveryConstant.autoassign_maxfirstmileparamname);
        String orderJson=deliveryDataHelper.returnOrderJson(getdefaultMapforOrder());
        boolean order_status=deliveryDataHelper.pushOrderJsonToRMQAndValidate(orderJson);
        String latlon=deliveryDataHelper.getApproxLatLongAtADistance(Double.parseDouble(restaurantLat),Double.parseDouble(restaurantLon),0.5);
        String distancelat=latlon.split(",")[0];
        String distancelon=latlon.split(",")[1];
        String order_id=null;
        if(order_status)
        {
            order_id=(new JSONObject(orderJson)).get("order_id").toString();
        }
		return new Object[][] { { order_id, cyclede, appversion, distancelat,
                distancelon,orderJson } };
	}

	// ###########################################
	@DataProvider(name = "QE-239")
	public Object[][] autoAssign_CycleDE_addingWeight() throws Exception {
		deliveryHelperMethods.dbhelperupdate(DeliveryConstant.updatebatch);
		deliveryHelperMethods.dbhelperupdate(DeliveryConstant.assigndetounassinged);
		autoassignHelper.updateParamAuto(DeliveryConstant.city, zone_id.toString(),
				DeliveryConstant.autoassign_weight_cycle_de_paramID,
				DeliveryConstant.autoassign_weight_cycle_de_paramValue, "test",
				DeliveryConstant.autoassign_weight_cycle_de_name);
		autoassignHelper.updateParamAuto(DeliveryConstant.city, zone_id.toString(),
				DeliveryConstant.autoassign_cyclelastmile,
				DeliveryConstant.autoassign_cyclelastmilevaluemore, "test",
				DeliveryConstant.autoassign_cyclelastmilename);
		autoassignHelper.updateParamAuto(DeliveryConstant.city, zone_id.toString(),
				DeliveryConstant.Max_first_mile_Cycles,
				DeliveryConstant.Max_first_mile_Cycles_value, "test",
				DeliveryConstant.autoassign_maxfirstmileparamname);
		autoassignHelper.updateParamAuto(DeliveryConstant.city, zone_id.toString(),
				DeliveryConstant.autoassign_cycletotal,
				DeliveryConstant.autoassign_cycletotalvaluemore, "test",
				DeliveryConstant.autoassign_cycletotalname);
		//Thread.sleep(5000);
		String order_id=deliveryDataHelper.getOrderId(restaurantId,restaurantLat,restaurantLon);
		//Thread.sleep(15000);
		return new Object[][] { { order_id, cyclede,DeliveryConstant.MayurDE, appversion,
			restaurantLat,restaurantLon} };
	}


	// ###########################################
	//confused about the other DE ID,Need to check what lat long to use
	@DataProvider(name = "QE-240")
	public Object[][] autoAssign_CycleDE_notAddingWeight() throws Exception {
		autoassignHelper.updateParamAuto(DeliveryConstant.city, zone_id.toString(),
				DeliveryConstant.autoassign_weight_cycle_de_paramID,
				DeliveryConstant.autoassign_weight_cycle_de_paramValue, "test",
				DeliveryConstant.autoassign_weight_cycle_de_name);
		autoassignHelper.updateParamAuto(DeliveryConstant.city, zone_id.toString(),
				DeliveryConstant.autoassign_cyclelastmile,
				DeliveryConstant.autoassign_cyclelastmilevaluemore, "test",
				DeliveryConstant.autoassign_cyclelastmilename);
		autoassignHelper.updateParamAuto(DeliveryConstant.city, zone_id.toString(),
				DeliveryConstant.Max_first_mile_Cycles,
				DeliveryConstant.Max_first_mile_Cycles_value, "test",
				DeliveryConstant.autoassign_maxfirstmileparamname);
		autoassignHelper.updateParamAuto(DeliveryConstant.city, zone_id.toString(),
				DeliveryConstant.autoassign_cycletotal,
				DeliveryConstant.autoassign_cycletotalvaluemore, "test",
				DeliveryConstant.autoassign_cycletotalname);
		autoassignHelper.updateParamAuto(DeliveryConstant.city, zone_id.toString(),
				DeliveryConstant.autoassigncasfirstmile,
				DeliveryConstant.autoassigncasfirstmilemore, "test",
				DeliveryConstant.autoassigncasfirstmilename);
		autoassignHelper.updateParamAuto(DeliveryConstant.city, zone_id.toString(),
				DeliveryConstant.autoassigncasweight,
				DeliveryConstant.autoassingcasweightmore, "test",
				DeliveryConstant.autoassigncasweightname);
		return new Object[][] { {de_id,cyclede, appversion,
			"13.040791","77.62250799999993"} };
	}

	// ###########################################
	@DataProvider(name = "QE-228")
	public Object[][] AutoassigntestDP228() throws Exception {
		String orderJson=deliveryDataHelper.returnOrderJson(getdefaultMapforOrder());
		boolean order_status=deliveryDataHelper.pushOrderJsonToRMQAndValidate(orderJson);
		String order_id=null;
		System.out.println(orderJson);
		if(order_status)
		{
			order_id=(new JSONObject(orderJson)).get("order_id").toString();
		}

		return new Object[][] {
		{ order_id, de_id, appversion, restaurantLat,restaurantLon,orderJson}
		};
	}

	// ***********************************************
	@DataProvider(name = "QE-244")
	public Object[][] AutoassigntestDP244() throws Exception {
		autoassignHelper.updateParamAuto(DeliveryConstant.city, zone_id.toString(),
				DeliveryConstant.autoassign_cyclelastmile,
				DeliveryConstant.autoassign_cyclelastmilevalueless, "test",
				DeliveryConstant.autoassign_cyclelastmilename);

		//Thread.sleep(15000);
        String orderJson=deliveryDataHelper.returnOrderJson(getdefaultMapforOrder());
        boolean order_status=deliveryDataHelper.pushOrderJsonToRMQAndValidate(orderJson);
        String order_id=null;
        System.out.println(orderJson);
        if(order_status)
        {
            order_id=(new JSONObject(orderJson)).get("order_id").toString();
        }
        return new Object[][] {

		{ order_id, cyclede, appversion, de_id, restaurantLat,
				restaurantLon,orderJson }
		};
	}

	// ***********************************************
	//Need to check on Lat lng again
	@DataProvider(name = "QE-245")
	public Object[][] AutoassigntestDP245() throws Exception {
		autoassignHelper.updateParamAuto(DeliveryConstant.city, zone_id.toString(),
				DeliveryConstant.autoassign_cyclelastmile,
				DeliveryConstant.autoassign_cyclelastmilevaluemore, "test",
				DeliveryConstant.autoassign_cyclelastmilename);
		autoassignHelper.updateParamAuto(DeliveryConstant.city, zone_id.toString(),
				DeliveryConstant.autoassign_cycletotal,
				DeliveryConstant.autoassign_cycletotalvalueless, "test",
				DeliveryConstant.autoassign_cycletotalname);
        String orderJson=deliveryDataHelper.returnOrderJson(getdefaultMapforOrder());
        boolean order_status=deliveryDataHelper.pushOrderJsonToRMQAndValidate(orderJson);
        String order_id=null;
        System.out.println(orderJson);
        if(order_status)
        {
            order_id=(new JSONObject(orderJson)).get("order_id").toString();
        }
        String distancelatlon= deliveryDataHelper.getApproxLatLongAtADistance(Double.parseDouble(restaurantLat),Double.parseDouble(restaurantLon),.5);
        String distancelat=distancelatlon.split(",")[0];
        String distancelon=distancelatlon.split(",")[1];
		//Thread.sleep(15000);
		return new Object[][] {

		{ order_id, cyclede,de_auth, appversion, distancelat, distancelon }

		};
	}

	// **************************************************
	@DataProvider(name = "QE-246")
	public Object[][] AutoassigntestDP246() throws Exception {
		autoassignHelper.updateParamAuto(DeliveryConstant.city, zone_id.toString(),
				DeliveryConstant.autoassign_cycletotal,
				DeliveryConstant.autoassign_cycletotalvaluemore, "test",
				DeliveryConstant.autoassign_cycletotalname);
		String order_id=deliveryDataHelper.getOrderId(restaurantId,restaurantLat,restaurantLon);
		//Thread.sleep(15000);
		return new Object[][] {
//Add distance here
		{ order_id, cyclede, appversion, "13.040791", "77.62250799999993" }

		};
	}

	//Lat long to be checked
	// ****************************************************
	@DataProvider(name = "QE-247")
	public Object[][] AutoassigntestDP247() throws Exception {
		autoassignHelper.updateParamAuto(DeliveryConstant.city, zone_id.toString(),
				DeliveryConstant.autoassign_minjitparamid,
				DeliveryConstant.autoassign_minjitvaluemore, "test",
				DeliveryConstant.autoassign_minjitparamidname);
		autoassignHelper.updateParamAuto(DeliveryConstant.city, zone_id.toString(),
				DeliveryConstant.autoassign_maxjitparamid,
				DeliveryConstant.autoassign_maxjitvaluemore, "test",
				DeliveryConstant.autoassign_maxjitparamidname);
		//Thread.sleep(15000);
        String orderJson=deliveryDataHelper.returnOrderJson(getdefaultMapforOrder());
        boolean order_status=deliveryDataHelper.pushOrderJsonToRMQAndValidate(orderJson);
        String order_id=null;
        System.out.println(orderJson);
        if(order_status)
        {
            order_id=(new JSONObject(orderJson)).get("order_id").toString();
        }
		return new Object[][] {

		{ order_id, de_id, appversion, restaurantLat,
				restaurantLon,orderJson,zone_id.toString() }

		};
	}
//Lat Long is an issue
	// *********************************************************
	@DataProvider(name = "QE-248")
	public Object[][] AutoassigntestDP248()  {

			autoassignHelper.updateParamAuto(DeliveryConstant.city, zone_id.toString(),
					DeliveryConstant.autoassigndynamicfirstmiledelay,
					DeliveryConstant.autoassigndynamicfirstmiledelaylessmin,
					"test", DeliveryConstant.autoassigndynamicfirstmiledelayname);

			autoassignHelper.updateParamAuto(DeliveryConstant.city, zone_id.toString(),
					DeliveryConstant.autoassignStepfactorDynamicfirstMile,
					DeliveryConstant.autoassignStepfactorDynamicfirstMilemore,
					"test",
					DeliveryConstant.autoassignStepfactorDynamicfirstMilename);
        String orderJson=deliveryDataHelper.returnOrderJson(getdefaultMapforOrder());
        boolean order_status=deliveryDataHelper.pushOrderJsonToRMQAndValidate(orderJson);
        String order_id=null;
        System.out.println(orderJson);
        if(order_status)
        {
            try {
                order_id=(new JSONObject(orderJson)).get("order_id").toString();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
		return new Object[][] {
		{ order_id, de_id, appversion, restaurantLat,
				restaurantLon,orderJson, zone_id.toString(), city_id.toString() }
		};
	}

	// ****************************************************
	@DataProvider(name = "QE-249")
	public Object[][] AutoassigntestDP249() throws Exception {
		autoassignHelper.updateParamAuto(DeliveryConstant.city, zone_id.toString(),
				DeliveryConstant.autoassigndynamicfirstmiledelay,
				DeliveryConstant.autoassigndynamicfirstmiledelaylessmin,
				"test", DeliveryConstant.autoassigndynamicfirstmiledelayname);
		autoassignHelper.updateParamAuto(DeliveryConstant.city, zone_id.toString(),
				DeliveryConstant.autoassignStepfactorDynamicfirstMile,
				DeliveryConstant.autoassignStepfactorDynamicfirstMilemore,
				"test",
				DeliveryConstant.autoassignStepfactorDynamicfirstMilename);
		autoassignHelper.updateParamAuto(DeliveryConstant.city, "34",
				DeliveryConstant.autoassignabsoluteDynamicfirstmile,
				DeliveryConstant.autoassignabsoluteDynamicfirstmileless,
				"test", DeliveryConstant.autoassignabsoluteDynamicfirstmilename);
		//Thread.sleep(10000);
		String order_id=deliveryDataHelper.getOrderId(restaurantId,restaurantLat,restaurantLon);
		return new Object[][] {

		{ order_id, DeliveryConstant.mayur_autoassign_de, "1.8.2.1", "13.0672901",
				"77.6028082" }

		};
	}

	// *********************************************************
	@DataProvider(name = "QE-250")
	public Object[][] AutoassigntestDP250() throws Exception {
		autoassignHelper.updateParamAuto(DeliveryConstant.city, "34",
				DeliveryConstant.autoassigncasfirstmile,
				DeliveryConstant.autoassigncasfirstmilemore, "test",
				DeliveryConstant.autoassigncasfirstmilename);
		autoassignHelper.updateParamAuto(DeliveryConstant.city, "34",
				DeliveryConstant.autoassigncasweight,
				DeliveryConstant.autoassingcasweightmore, "test",
				DeliveryConstant.autoassigncasweightname);
		deliveryServiceHelper.markDEInactive("59891", "1");
		String order_id=deliveryDataHelper.getOrderId(restaurantId,restaurantLat,restaurantLon);
		//Thread.sleep(10000);
		return new Object[][] {

		{order_id, "59891", "1.8.2.1", "13.040791", "77.62250799999993" }

		};
	}

	// ***************************************************************
	@DataProvider(name = "QE-251")
	public Object[][] AutoassigntestDP251() throws Exception {
		String order_id=deliveryDataHelper.getOrderId(restaurantId,restaurantLat,restaurantLon);
		//Thread.sleep(20000);
		deliveryServiceHelper.ordercancel(order_id);
				Thread.sleep(10000);
		return new Object[][] {

		{ order_id, DeliveryConstant.mayur_autoassign_de, "1.8.2.1", "13.040791",
				"77.62250799999993" }

		};
	}

	// ********************************************************
	@DataProvider(name = "QE-252")
	public Object[][] AutoassigntestDP252() throws Exception {
        String orderJson=deliveryDataHelper.returnOrderJson(getdefaultMapforOrder());
        boolean order_status=deliveryDataHelper.pushOrderJsonToRMQAndValidate(orderJson);
        String order_id=null;
        if(order_status)
        {
            try {
                order_id=(new JSONObject(orderJson)).get("order_id").toString();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
		deliveryServiceHelper.makeDEFree(de_id);
		deliveryServiceHelper.markDEInactive(de_id, "1");
		return new Object[][] {

		{ order_id, de_id, appversion, restaurantLat,
				restaurantLon, orderJson }
		};
	}

	// **************************************************
	@DataProvider(name = "QE-254")
	public Object[][] AutoassigntestDP254() throws Exception {
		deliveryServiceHelper.makeDEBusy(DeliveryConstant.MayurDE);
		String order_id=deliveryDataHelper.getOrderId(restaurantId,restaurantLat,restaurantLon);
		//Thread.sleep(10000);
		return new Object[][] {

		{ order_id, DeliveryConstant.mayur_autoassign_de, "1.8.2.1", "13.040791",
				"77.62250799999993" }

		};
	}

	@DataProvider(name = "QE-255")
	public Object[][] AutoassigntestDP255() throws Exception {
		String order_id=deliveryDataHelper.getOrderId(restaurantId,restaurantLat,restaurantLon);
		//Thread.sleep(10000);
		String query = "update trips set order_tags='is_new_user' where order_id="
				+ order_id + ";";
		deliveryHelperMethods.dbhelperupdate(query);
		String order_id1 = createOrder();
		Thread.sleep(10000);
		return new Object[][] {

		{ order_id, order_id1, DeliveryConstant.mayur_autoassign_de, "1.8.2.1",
				"13.040791", "77.62250799999993" }

		};
	}

	@DataProvider(name = "QE-257")
	public Object[][] AutoassigntestDP257() throws Exception {
		autoassignHelper.updateParamAuto(DeliveryConstant.city, "34",
				DeliveryConstant.autoassign_orderdelaystepfactor,
				DeliveryConstant.autoassign_orderdelaystepfactorvalueless,
				"test", DeliveryConstant.autoassign_orderdelaystepfactorname);
		autoassignHelper.updateParamAuto(DeliveryConstant.city, "34",
				DeliveryConstant.autoassign_weightorderdelayid,
				DeliveryConstant.autoassign_weightorderdelaymore, "test",
				DeliveryConstant.autoassign_weightorderdelayname);
		String order_id=deliveryDataHelper.getOrderId(restaurantId,restaurantLat,restaurantLon);
		//Thread.sleep(300000);
		String order_id1 = deliveryDataHelper.getOrderId(restaurantId,restaurantLat,restaurantLon);
		return new Object[][] {

		{ order_id, order_id1, DeliveryConstant.mayur_autoassign_de, "1.8.2.1",
				"13.040791", "77.62250799999993" }

		};
	}

	@DataProvider(name = "QE-260")
	public Object[][] AutoassigntestDP260() throws Exception {
        String orderJson=deliveryDataHelper.returnOrderJson(getdefaultMapforOrder());
        boolean order_status=deliveryDataHelper.pushOrderJsonToRMQAndValidate(orderJson);
        String order_id=null;
        if(order_status)
        {
            try {
                order_id=(new JSONObject(orderJson)).get("order_id").toString();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
		return new Object[][] {
		{ order_id, de_id, appversion, restaurantLat,
				restaurantLon, orderJson
		}

		};
	}

	// *******************************************************************
	@DataProvider(name = "QE-229")
	public Object[][] AutoassigntestDP229() throws Exception {
		String order_id=deliveryDataHelper.getOrderId(restaurantId,restaurantLat,restaurantLon);
		return new Object[][] {
		// { order_id, "2418","19866", "1.8.2.1", "13.040791",
		// "77.62250799999993" }
		{ order_id, "59005", "59802", "1.8.2.1", "13.0030624",
				"77.56429279999998" } };
	}

	@DataProvider(name = "QE-233")
	public Object[][] AutoassigntestDP233() throws Exception {
		autoassignHelper.updateParamAuto(DeliveryConstant.city, "49",
				DeliveryConstant.autoassign_orderdelaystepfactor,
				DeliveryConstant.autoassign_orderdelaystepfactorvalueless,
				"test", DeliveryConstant.autoassign_orderdelaystepfactorname);
		autoassignHelper.updateParamAuto(DeliveryConstant.city, "49",
				DeliveryConstant.autoassign_weightorderdelayid,
				DeliveryConstant.autoassign_weightorderdelaymore, "test",
				DeliveryConstant.autoassign_weightorderdelayname);
		String order_id=deliveryDataHelper.getOrderId(restaurantId,restaurantLat,restaurantLon);
		Thread.sleep(10000);
		String order_id1 =deliveryDataHelper.getOrderId(restaurantId,restaurantLat,restaurantLon);
		return new Object[][] {

		{ order_id, order_id1, "59005", "1.8.2.1", "13.0030624",
				"77.56429279999998" }

		};
	}

	@DataProvider(name = "QE-230")
	public Object[][] AutoassigntestDP230()  {
        String orderJson=deliveryDataHelper.returnOrderJson(getdefaultMapforOrder());
        boolean order_status=deliveryDataHelper.pushOrderJsonToRMQAndValidate(orderJson);
        String order_id=null;
        if(order_status)
        {
            try {
                order_id=(new JSONObject(orderJson)).get("order_id").toString();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
		return new Object[][] { { order_id, de_id, reject_de,appversion,
				restaurantLat, restaurantLon,orderJson,de_auth,reject_deAuth } };
	}

	@DataProvider(name = "QE-231")
	public Object[][] Autoassigntest231DP() throws Exception {
        String orderJson1=deliveryDataHelper.returnOrderJson(getdefaultMapforOrder());
        boolean order_status1=deliveryDataHelper.pushOrderJsonToRMQAndValidate(orderJson1);
        String orderJson2=deliveryDataHelper.returnOrderJson(getdefaultMapforOrder());
        boolean order_status2=deliveryDataHelper.pushOrderJsonToRMQAndValidate(orderJson2);
        String order_id1=null;
        String order_id2=null;
        String city_id=null;
        if(order_status1&&order_status2)
        {
            try {
                order_id1=(new JSONObject(orderJson1)).get("order_id").toString();
                order_id2=(new JSONObject(orderJson2)).get("order_id").toString();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
	    Processor processor3 = autoassignHelper.updateParamAuto("1",
				zone_id.toString(), DeliveryConstant.Param_id,
				DeliveryConstant.Param_value, DeliveryConstant.user,
				DeliveryConstant.autoasign_weightwithde);

		deliveryServiceHelper.markOrderWithDE(order_id2);
		return new Object[][] {

		{ order_id1, order_id2, de_id, appversion,
				restaurantLat, restaurantLon,orderJson2 } };
	}

	@DataProvider(name = "QE-232")
	public Object[][] Autoassigntest232DP() throws Exception {
		Processor processor3 = autoassignHelper.updateParamAuto(DeliveryConstant.city,
				DeliveryConstant.zone, DeliveryConstant.Param_id,
				DeliveryConstant.Param_value, DeliveryConstant.user,
				DeliveryConstant.autoasign_weightwithde);
		Assert.assertEquals(processor3.ResponseValidator.GetResponseCode(), 200);
		String order_id=deliveryDataHelper.getOrderId(restaurantId,restaurantLat,restaurantLon);
		Thread.sleep(15000);
		return new Object[][] { { order_id, DeliveryConstant.mayur_autoassign_de,
				"1.8.2.1", "13.040791", "77.62250799999993" } };
	}

/*	public static String createWithdeOrder() throws Exception {
		CheckoutHelper helperc = new CheckoutHelper();
		Object[][] data = cartItems2withDE();
		Processor p = helperc.placeOrder(DeliveryConstant.mobile,
				DeliveryConstant.password, data[0][0].toString(),
				data[0][1].toString(), data[0][2].toString());
		String withdeorder_id=deliveryDataHelper.getOrderId(restaurantId,restaurantLat,restaurantLon);
*//*		return withdeorder_id = p.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.order_id");*//*
		return withdeorder_id;
	}*/

	public static Object[][] cartItems2Normal() {

		return new Object[][] { { "32851", "1", "300" } };
	}

	public static Object[][] cartItems2withDE() {

		return new Object[][] { { "1118346", "2", "4852" } };
	}

	@DataProvider(name = "QE-253")
	public Object[][] AutoassigntestDP253() throws Exception {
		deliveryHelperMethods.dbhelperupdate(DeliveryConstant.updatebatch);
		deliveryHelperMethods.dbhelperupdate(DeliveryConstant.assigndetounassinged);
		autoassignHelper.updateParamAuto(DeliveryConstant.city, "34",
				DeliveryConstant.autoassigncasfirstmile,
				DeliveryConstant.autoassigncasfirstmilemore, "test",
				DeliveryConstant.autoassigncasfirstmilename);
		autoassignHelper.updateParamAuto(DeliveryConstant.city, "34",
				DeliveryConstant.autoassigncasweight,
				DeliveryConstant.autoassingcasweightmore, "test",
				DeliveryConstant.autoassigncasweightname);
		Thread.sleep(5000);
		deliveryServiceHelper.markDEInactive(DeliveryConstant.MayurDE, "1");
		return new Object[][] {

		{ "59891", "1.8.2.1", "13.040791", "77.62250799999993"}

		};
	}

	@DataProvider(name = "QE-259")
	public Object[][] AutoassigntestDP259() throws Exception {
        String orderJson=deliveryDataHelper.returnOrderJson(getdefaultMapforOrder());
        boolean order_status=deliveryDataHelper.pushOrderJsonToRMQAndValidate(orderJson);
        String order_id=null;
        if(order_status)
        {
            try {
                order_id=(new JSONObject(orderJson)).get("order_id").toString();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
		return new Object[][] { { order_id, de_id, appversion, restaurantLat,
				restaurantLon, orderJson }
		};
	}

	@DataProvider(name = "QE-261")
	public Object[][] AutoassigntestDP261() throws Exception {
        String orderJson=deliveryDataHelper.returnOrderJson(getdefaultMapforOrder());
        boolean order_status=deliveryDataHelper.pushOrderJsonToRMQAndValidate(orderJson);
        String order_id=null;
        if(order_status)
        {
            try {
                order_id=(new JSONObject(orderJson)).get("order_id").toString();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
		return new Object[][] { { order_id,de_id, appversion, restaurantLat,
				restaurantLon, orderJson}

		};
	}

	public static String createOrder() throws Exception {
		CheckoutHelper helperc = new CheckoutHelper();
		Object[][] data = cartItems();
		Processor p = helperc.placeOrder(DeliveryConstant.mobile,
				DeliveryConstant.password, data[0][0].toString(),
				data[0][1].toString(), data[0][2].toString());
		return p.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.order_id");
	}

	public static Object[][] cartItems() {

		return new Object[][] { { DeliveryConstant.auto_assign_mayur_item_id, "2", DeliveryConstant.auto_assign_mayur_rest_id } };

	}

	public String createOrder1() throws Exception {
		CheckoutHelper helperc = new CheckoutHelper();
		Object[][] data = cartItems1();
		Processor p = helperc.placeOrder(DeliveryConstant.mobile,
				DeliveryConstant.password, data[0][0].toString(),
				data[0][1].toString(), data[0][2].toString());
		String order_id1 = p.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.order_id");
		return order_id1;
	}

	public static Object[][] cartItems1() {

		return new Object[][] { { "1064227", "1", "6118" } };
	}

	@DataProvider(name = "QE-243")
	public Object[][] autoassigntest243() throws Exception {
		autoassignHelper.updateParamAuto(DeliveryConstant.city, zone_id.toString(),
				DeliveryConstant.Max_first_mile_Cycles,
				DeliveryConstant.Max_first_mile_Cycles_value, "test",
				DeliveryConstant.autoassign_maxfirstmileparamname);
		autoassignHelper.updateParamAuto(DeliveryConstant.city,zone_id.toString(),
				DeliveryConstant.Max_last_mile_Cycles,
				DeliveryConstant.Max_last_mile_Cycles_value, "test",
				DeliveryConstant.autoassign_maxlastmileparamname);
		autoassignHelper.updateParamAuto(DeliveryConstant.city, zone_id.toString(),
				DeliveryConstant.Max_trip_distance_cycle,
				DeliveryConstant.Max_trip_distance_cycle_value, "test",
				DeliveryConstant.autoassign_cycletotalname);
		autoassignHelper.updateParamAuto(DeliveryConstant.city, zone_id.toString(),
				DeliveryConstant.Min_JIT_delay,
				DeliveryConstant.Min_JIT_delay_value, "test",
				DeliveryConstant.autoassign_minjitparamidname);
        String orderJson=deliveryDataHelper.returnOrderJson(getdefaultMapforOrder());
        boolean order_status=deliveryDataHelper.pushOrderJsonToRMQAndValidate(orderJson);
        String order_id=null;
        if(order_status)
        {
            order_id=(new JSONObject(orderJson)).get("order_id").toString();
        }
		//Thread.sleep(15000);
		return new Object[][] { { order_id,cyclede,appversion, restaurantLat,restaurantLon,orderJson
				 } };
	}

	@DataProvider(name = "cyclede11")
	public Object[][] cyclede11() throws Exception {
		String order_id1 = createOrder1();
		return new Object[][] {

		{ order_id1, "27577", "1.8.1.2", "12.93509011732015",
				"77.62456053621099" }

		};
	}

	@DataProvider(name = "cyclede12")
	public Object[][] autoassigntest329() {
		autoassignHelper.updateParamAuto(DeliveryConstant.city, zone_id.toString(),
				DeliveryConstant.Max_first_mile_Cycles,
				DeliveryConstant.Max_first_mile_Cycles_value, "test",
				DeliveryConstant.autoassign_maxfirstmileparamname);
		autoassignHelper.updateParamAuto(DeliveryConstant.city, zone_id.toString(),
				DeliveryConstant.Max_last_mile_Cycles,
				DeliveryConstant.Max_last_mile_Cycles_value_less, "test",
				DeliveryConstant.autoassign_maxlastmileparamname);
		autoassignHelper.updateParamAuto(DeliveryConstant.city, zone_id.toString(),
				DeliveryConstant.Max_trip_distance_cycle,
				DeliveryConstant.Max_trip_distance_cycle_value, "test",
				DeliveryConstant.autoassign_cycletotalname);
		deliveryServiceHelper.makeDEBusy(de_id);
        String orderJson=deliveryDataHelper.returnOrderJson(getdefaultMapforOrder());
        boolean order_status=deliveryDataHelper.pushOrderJsonToRMQAndValidate(orderJson);
        String order_id=null;
        if(order_status)
        {
            try {
                order_id=(new JSONObject(orderJson)).get("order_id").toString();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
		return new Object[][] {
		{ order_id, cyclede,appversion, restaurantLat,
                restaurantLon,orderJson,zone_id.toString(),city_id.toString() }

		};
	}

	@DataProvider(name = "QE-237")
	public Object[][] AutoassigntestDP237() throws Exception {
		autoassignHelper.updateParamAuto(DeliveryConstant.city, "34",
				DeliveryConstant.autoassign_withinshiftid,
				DeliveryConstant.autoassign_withinshiftmore, "test",
				DeliveryConstant.autoassign_withinshiftname);
		String order_id = createOrder();
		Thread.sleep(10000);
		return new Object[][] {

		{ order_id,DeliveryConstant.MayurDE, "59892", "1.8.2.1", "13.040791",
				"77.62250799999993" }

		};
	}

	// *************************************************************************************************************************************************
	public static String createOrder2() throws Exception {
		CheckoutHelper helperc = new CheckoutHelper();
		Object[][] data = cartItems2();
		Processor p = helperc.placeOrder(DeliveryConstant.mobile,
				DeliveryConstant.password, data[0][0].toString(),
				data[0][1].toString(), data[0][2].toString());
		return p.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.order_id");
	}

	public static Object[][] cartItems2() {

		return new Object[][] { { "847476", "1", "5218" } };

	}

/*	public static String createWithdeOrder2() throws Exception {
		CheckoutHelper helperc = new CheckoutHelper();
		Object[][] data = cartItems2withDE2();
		Processor p = helperc.placeOrder(DeliveryConstant.mobile,
				DeliveryConstant.password, data[0][0].toString(),
				data[0][1].toString(), data[0][2].toString());
		return withdeorder_id = p.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.order_id");
	}*/

	public static Object[][] cartItems2withDE2() {

		return new Object[][] { { "847560", "1", "5218" } };
	}

}