package com.swiggy.api.erp.finance.dp;

import com.swiggy.api.erp.finance.constants.CashMgmtConstant;
import com.swiggy.api.erp.finance.helper.CashMgmtDBhelper;
import org.testng.annotations.DataProvider;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class CashDataProvider {
    CashMgmtDBhelper cmdh=new CashMgmtDBhelper();
    ArrayList<String> deList=new ArrayList<>();

    @DataProvider(name="getAllDE")
    public  Object[][] getAllDE(){
        return new Object[][]{{CashMgmtConstant.ACTIVE_DE},{CashMgmtConstant.INACTIVE_DE},{CashMgmtConstant.INVALID_DE}};
    }

    @DataProvider(name="getActiveDEId")
    public Object[][] getActiveDEId() {

       return new Object[][]{{CashMgmtConstant.ACTIVE_DE}};

    }

    @DataProvider( name= "getInactiveDEId")
    public  Object[][] getInactiveDEid(){
        return new Object[][]{{CashMgmtConstant.INACTIVE_DE}};

    }
    @DataProvider ( name="setDeId_EmploymentStatus")
    public Object[][] setDeId_EmploymentStatus(){
        return new Object[][]{{CashMgmtConstant.ACTIVE_DE,"1"},{CashMgmtConstant.INACTIVE_DE,"3"}};

    }

    @DataProvider ( name="getInvalidDEId")
    public Object[][] getInvalidDEId(){
        return new Object[][]{{CashMgmtConstant.INVALID_DE}};

    }

    @DataProvider( name= "addTxnDP")
    public Object[][] addTxnDP(){
        return new Object[][]{{CashMgmtConstant.ACTIVE_DE,"Cash Disbursed to DE","100","0",CashMgmtConstant.CASH_SESSION_DISBURSE},
                              {CashMgmtConstant.ACTIVE_DE,"Cash collect from DE","0","50",CashMgmtConstant.CASH_SESSION_COLLECT},
                              {CashMgmtConstant.ACTIVE_DE,"Cash Incoming","50","0",CashMgmtConstant.CASH_INCOMING},
                              {CashMgmtConstant.ACTIVE_DE,"Cash Outgoing","0","50",CashMgmtConstant.CASH_SPENDING},
                              {CashMgmtConstant.ACTIVE_DE,"Pocketing test","10","0",CashMgmtConstant.POCKETING_TRANSACTION},
                              {CashMgmtConstant.ACTIVE_DE,"Payout test","0","10",CashMgmtConstant.PAYOUT_ADJUSTMENT},
                              {CashMgmtConstant.ACTIVE_DE,"Manual Transactions","15","0",CashMgmtConstant.MANUAL_TRANSACTION}};
    }

    @DataProvider( name= "addTxnInvalidDEDP")
    public Object[][] addTxnInvalidDEDP(){
        return new Object[][]{

                {CashMgmtConstant.INVALID_DE,"Cash Disbursed to DE","100","0",CashMgmtConstant.CASH_SESSION_DISBURSE},
                {CashMgmtConstant.INVALID_DE,"Cash collect from DE","0","50",CashMgmtConstant.CASH_SESSION_COLLECT},
                {CashMgmtConstant.INVALID_DE,"Cash Incoming","50","0",CashMgmtConstant.CASH_INCOMING},
                {CashMgmtConstant.INVALID_DE,"Cash Outgoing","0","50",CashMgmtConstant.CASH_SPENDING},
                {CashMgmtConstant.INVALID_DE,"Pocketing test","10","0",CashMgmtConstant.POCKETING_TRANSACTION},
                {CashMgmtConstant.INVALID_DE,"Payout test","0","10",CashMgmtConstant.PAYOUT_ADJUSTMENT},
                {CashMgmtConstant.INVALID_DE,"Manual Transactions","15","0",CashMgmtConstant.MANUAL_TRANSACTION}};
    }

    @DataProvider( name="addBulkTxnDP")
    public Object [][] addBulkTransactionDP(){
        return new Object[][]{

                {"Cash Incoming", "50", "0", CashMgmtConstant.CASH_INCOMING},
                {"Cash Outgoing", "0", "50", CashMgmtConstant.CASH_SPENDING},
                {"Cash Disbursed to DE", "100", "0", CashMgmtConstant.CASH_SESSION_DISBURSE},
                {"Cash collect from DE", "0", "50", CashMgmtConstant.CASH_SESSION_COLLECT},
                {"Pocketing test", "10", "0", CashMgmtConstant.POCKETING_TRANSACTION},
                {"Manual Transactions", "15", "0", CashMgmtConstant.MANUAL_TRANSACTION}

        };

    }

    @DataProvider( name="deFromDBDP")
    public Object[][] getBulkDEFromDB(){
        List<Map<String, Object>> lst=(List<Map<String, Object>>)cmdh.getActiveDEFromDB("1");
        Iterator<Map<String, Object>> iterator = lst.iterator();
        if(deList.isEmpty()) {
            while (iterator.hasNext()) {
               // deList.add(iterator.next().get("id").toString());
                return new Object[][]{{iterator.next().get("id").toString()}};
            }
        }
        return null;


    }
    @DataProvider( name ="getInvalidAndInactiveDE")
    public  Object[][] invalidInactiveDEprovider(){
        return new Object[][]{{CashMgmtConstant.INACTIVE_DE},{CashMgmtConstant.INVALID_DE}};
    }

}
