package com.swiggy.api.erp.delivery.Pojos;

import java.util.List;
import java.util.Map;

public class CreateCluster
{
    private String entityType;
    private Polygon polygon;
    private String parentId;
    private String name;
    private Map<String, Object> attributes;
    private String type;
    private List<String> tags;

    public String getEntityType() {
        return entityType;
    }

    public Polygon getPolygon() {
        return polygon;
    }

    public String getParentId() {
        return parentId;
    }

    public String getName() {
        return name;
    }

    public Map<String, Object> getAttributes() {
        return attributes;
    }

    public String getType() {
        return type;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public void setPolygon(Polygon polygon) {
        this.polygon = polygon;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAttributes(Map<String, Object> attributes) {
        this.attributes = attributes;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }
}
