package com.swiggy.api.erp.cms.tests.Availability;

import com.swiggy.api.erp.cms.helper.CMSHelper;
import org.testng.annotations.Test;

public class AvailabilityCronRegularItem {
    CMSHelper cmsHelper=new CMSHelper();
    @Test(description = "sends regular item availability events to snd kafka")
    public void regualItemAvailability(){
        cmsHelper.availabilityRegularItem();
    }
}
