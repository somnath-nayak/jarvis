package com.swiggy.api.erp.cms.pojo.FullMenu;

import com.swiggy.api.erp.cms.constants.MenuConstants;

/**
 * Created by kiran.j on 2/2/18.
 */
public class Sub_categories
{
    private String id;

    private int order;

    private String description;

    private String name;

    private String category_id;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public int getOrder ()
    {
        return order;
    }

    public void setOrder (int order)
    {
        this.order = order;
    }

    public String getDescription ()
{
    return description;
}

    public void setDescription (String description)
    {
        this.description = description;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getCategory_id ()
    {
        return category_id;
    }

    public void setCategory_id (String category_id)
    {
        this.category_id = category_id;
    }
    

    public Sub_categories build() {
        setDefaultValues();
        return this;
    }

    public void setDefaultValues() {
        if(this.getId() == null)
            this.setId(MenuConstants.sub_category_id);
        if(this.getName() == null)
            this.setName(MenuConstants.sub_category_name);
        if(this.getDescription() == null)
            this.setDescription("Automation");
        if(this.getOrder()+"" == null)
            this.setOrder(MenuConstants.subcat_order);

    }
    

//    public Sub_categories build() {
//        setDefaultValues();
//        return this;
//    }
//
//    public void setDefaultValues() {
//        if(this.getId() == null)
//            this.setId(MenuConstants.subcat_id);
//        if(this.getName() == null)
//            this.setName(MenuConstants.subcat_name);
//        if(this.getCategory_id() == null)
//            this.setCategory_id(MenuConstants.subcat_catid);
//        if(this.getDescription() == null)
//            this.setDescription(MenuConstants.subcat_desc);
//    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", order = "+order+", description = "+description+", name = "+name+", category_id = "+category_id+"]";
    }
}