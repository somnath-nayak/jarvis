
package com.swiggy.api.erp.cms.pojo.PricingService;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.swiggy.api.erp.cms.constants.CatalogV2Constants;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "key",
    "pricing_construct",
    "meta"
})
public class Pricing {

    @JsonProperty("key")
    private String key;
    @JsonProperty("pricing_construct")
    private Pricing_construct pricing_construct;
    @JsonProperty("meta")
    private Meta meta;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();


    @JsonProperty("key")
    public String getKey() {
        return key;
    }

    @JsonProperty("key")
    public void setKey(String key) {
        this.key = key;
    }

    public Pricing withKey(String key) {
        this.key = key;
        return this;
    }

    @JsonProperty("pricing_construct")
    public Pricing_construct getPricing_construct() {
        return pricing_construct;
    }

    @JsonProperty("pricing_construct")
    public void setPricing_construct(Pricing_construct pricing_construct) {
        this.pricing_construct = pricing_construct;
    }

    public Pricing withPricing_construct(Pricing_construct pricing_construct) {
        this.pricing_construct = pricing_construct;
        return this;
    }

    @JsonProperty("meta")
    public Meta getMeta() {
        return meta;
    }

    @JsonProperty("meta")
    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public Pricing withMeta(Meta meta) {
        this.meta = meta;
        return this;
    }

    public Pricing setValue(String key,long mrp,long storeprice){
        Attributes ab=new Attributes().setDefaultValue();
        Meta m=new Meta().setDefaultValue();
        Price_list pc=new Price_list().settValue(mrp,storeprice);
        Pricing_construct pc1=new Pricing_construct().setDefaultValue(ab,pc);
        this.withKey(key).withMeta(m).withPricing_construct(pc1);
        return this;
    }



}
