package com.swiggy.api.erp.delivery.helper;

import com.swiggy.api.erp.cms.helper.CMSHelper;
import com.swiggy.api.erp.cms.helper.RestaurantHelper;
import com.swiggy.api.erp.cms.pojo.Restaurant;
import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.RabbitMQHelper;
import framework.gameofthrones.Tyrion.RedisHelper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by preetesh.sharma on 08/02/18.
 */
public class POPHelper {
    DeliveryDataHelper deliveryDataHelper;
    RestaurantHelper rsHelper;
    CandidateHelper cdHelper;
    CMSHelper cmshelper;
    RedisHelper redishelper;
    RabbitMQHelper rabhelper;
    Initialize init;
    DeliveryHelperMethods dHelper;
    AutoassignHelper autoassignHelper;
    DeliveryServiceHelper deservicehelper;
    Map<String, List<String>> allPrimarydata;
    public POPHelper()
    {
        rsHelper= new RestaurantHelper();
        cdHelper=new CandidateHelper();
        cmshelper=new CMSHelper();
        redishelper=new RedisHelper();
        rabhelper=new RabbitMQHelper();
        init=new Initialize();
        dHelper= new DeliveryHelperMethods();
        autoassignHelper=new AutoassignHelper();
        deservicehelper= new DeliveryServiceHelper();
        allPrimarydata= new HashMap<String,List<String>>();
        deliveryDataHelper= new DeliveryDataHelper();


    }

    public String createPopRestaurant(String name)
    {
        Restaurant myrestaurant= new Restaurant(null,"1",null,null,name,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);
        myrestaurant=myrestaurant.build();
        Processor processor=rsHelper.createRestaurant(myrestaurant);
        System.out.println("Response Payload"+processor.ResponseValidator.GetBodyAsText());
        String restswiggyid=processor.ResponseValidator.GetNodeValue("swiggy_id").toString();
        if(allPrimarydata.containsKey("restaurant_id")) {
            List<String> thislist=allPrimarydata.get("restaurant_id");
            thislist.add(restswiggyid);
            allPrimarydata.put("restaurant_id", thislist);
        }
        else
        {
           List<String> thislist= new ArrayList<String>();
           thislist.add(restswiggyid);
           allPrimarydata.put("restaurant_id",thislist);
        }
            return restswiggyid;

        //Poll resturnt ID to check wether it is created in delivery restaurants

    }
 /*   public Integer createDe()
    {
        Candidate candidate=new Candidate();
        candidate.build();
        Integer candidateid=0;
        try {
             candidateid = cdHelper.createCandidateAndActivate();
            }
            catch (Exception ioe)
            {
                ioe.printStackTrace();
            }
        allPrimarydata.put("de_id",candidateid.toString());
        return candidateid;

        //SHOULD BE N SERVICE ZONE CREATE LAT LONG CLOSE TO POP RESTAURANT
    }*/









    public String updatePopRestaurantInventory(List<String> restids,int inventory)
    {
        //'[{"id":10820,"inv":10},{"id":18990,"inv":10}]'
        StringBuilder sbfinal= new StringBuilder();
        List<String> tempList= new ArrayList<String>();
        for(String restid:restids)
        {
            StringBuilder sb= new StringBuilder();
            sb.append("{\"id\":"+restid+",\""+"inv\":"+inventory+"}");
            tempList.add(sb.toString());
        }
        sbfinal.append("'[");
        for(String restvalues:tempList)
        {
            sbfinal.append(restvalues+",");
        }
        sbfinal.deleteCharAt(sbfinal.length()-1).append("]'");
        System.out.println(sbfinal.toString());
        return sbfinal.toString();
    }



    public String  createInventoryRequest(Map<String, String> restInventoryMap) throws JSONException
    {
        JSONArray inventoryJsonArray=new JSONArray();
        for(String str:restInventoryMap.keySet())
        {
            String eachHotelInventoryString="{\"id\":"+str+",\"inv\":"+restInventoryMap.get(str)+"}";
            JSONObject eachHotelInventoryObject=new JSONObject(eachHotelInventoryString);
            inventoryJsonArray.put(eachHotelInventoryObject);
        }
        return inventoryJsonArray.toString();

    }

    public boolean setRedisInventory(String name, int database,String key, String request)
    {
        boolean flag= false;
        System.out.println(name+"   "+database+"   "+key+"   "+request);
        redishelper.setValue(name,database,key,request);
        String value=redishelper.getValue(name,database,key).toString();
        return request.trim().equalsIgnoreCase(value.trim());
    }

    public boolean pollPopTagging(String rest_id,String de_id) {
        boolean exitflag = false;
        long startTime = System.currentTimeMillis();
        long maxDurationInMilliseconds = 5 * 60 * 1000;
        long endtime = System.currentTimeMillis() + maxDurationInMilliseconds;
        //String status = "failure";
        do {
            try {
                Thread.sleep(15000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if(getDeTaggedStatus(rest_id,de_id))
            {
                exitflag=true;
               // break;
            }
            else
             {
                 String destatus = DeliveryConstant.destatusquery + de_id;
                 Map<String, Object> dbMap = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForList(destatus).get(0);
                 String currentstatus = dbMap.get("status").toString();
               /*  if (!currentstatus.equalsIgnoreCase("FR"))
                 {
                     SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).execute(DeliveryConstant.destatusfree + de_id);
                 }*/

                if(!currentstatus.equalsIgnoreCase("FR"))
                {
                        deservicehelper.makeDEFree(de_id);
                    deservicehelper.makeDEFree(de_id);
                }
              exitflag=false;
             }

        } while (exitflag == true & System.currentTimeMillis() < endtime);
        return exitflag;
    }


    public boolean getDeTaggedStatus(String restid,String de_id)
    {
        String hashKey="DSAA:eptags:1";
        String hashValue="swiggy_pop:"+restid;
        String deId = redishelper.getHashValue("deliveryredis",2,"DSAA:eptags:1",hashValue);
        return deId.trim().equalsIgnoreCase(de_id);
    }



    @Test
    public void maintest()
    {
       // POPHelper popHelper= new POPHelper();
        //pollTrips("7741280120");
       // System.out.println(popHelper.getDEAuth());
       //popHelper.CreateDE(1);
      // popHelper.getDEAuth("87451");
      //  System.out.println(popHelper.returnOrderJson("10820"));
        //need to make de activ
        //make thr key for no of des reserved in the zone as 1
        //need to makede free as well for this

    /*    String name="Boggota" + Math.random()*1000+Math.random()*100+Math.random()*10;
        popHelper.createPopRestaurant(name);*/
/*        List<String> restList = new ArrayList<>();
        restList.add("10820");
        restList.add("10821");
        String toput=popHelper.updatePopRestaurantInventory(restList,20);
       // redishelper.setValueJson("deliveryredis",2,"POP_RX_TEMP_1",toput);


  //      popHelper.testLatLong(12.933149, 77.585845,0.3);
        String str = redishelper.getHashValue("deliveryredis",2,"DSAA:eptags:1","swiggy_pop:18990");
        System.out.println(str);*/
     //   redishelper.setHashValue("deliveryredis",2,"X","Y","Z");


    }

}

//12.933149,77.585845
//12.941661,77.594579
/*String destatus = DeliveryConstant.destatusquery + de_id;
    Map<String, Object> dbMap = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForList(destatus).get(0);
    String currentstatus = dbMap.get("status").toString();
            if (!currentstatus.equalsIgnoreCase("FR")) {
                    SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).execute(DeliveryConstant.destatusfree + de_id);
                    } else {
                    //connect to redis with a tag name for the same
                    String ifdetagged = null;
                    if (detagged != null) {
                    continue;
                    } else {
                    break;
                    }
                    }*/