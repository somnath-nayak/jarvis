
package com.swiggy.api.erp.cms.pojo.Taxonomy;

import java.util.*;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "store_id",
    "display_name",
    "taxonomy_type",
    "enabled",
    "images",
    "priority",
    "nodes",
    "meta"
})
public class Taxonomy {

    @JsonProperty("store_id")
    private String store_id;
    @JsonProperty("display_name")
    private String display_name;
    @JsonProperty("taxonomy_type")
    private String taxonomy_type;
    @JsonProperty("enabled")
    private Boolean enabled;
    @JsonProperty("images")
    private List<String> images = null;
    @JsonProperty("priority")
    private Long priority;
    @JsonProperty("nodes")
    private List<Node> nodes = null;
    @JsonProperty("meta")
    private Meta meta;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Taxonomy() {
        setDefaultData();
    }

    /**
     * 
     * @param enabled
     * @param display_name
     * @param nodes
     * @param priority
     * @param images
     * @param store_id
     * @param taxonomy_type
     * @param meta
     */
    public Taxonomy(String store_id, String display_name, String taxonomy_type, Boolean enabled, List<String> images, Long priority, List<Node> nodes, Meta meta) {
        super();
        this.store_id = store_id;
        this.display_name = display_name;
        this.taxonomy_type = taxonomy_type;
        this.enabled = enabled;
        this.images = images;
        this.priority = priority;
        this.nodes = nodes;
        this.meta = meta;
    }

    @JsonProperty("store_id")
    public String getStore_id() {
        return store_id;
    }

    @JsonProperty("store_id")
    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    public Taxonomy withStore_id(String store_id) {
        this.store_id = store_id;
        return this;
    }

    @JsonProperty("display_name")
    public String getDisplay_name() {
        return display_name;
    }

    @JsonProperty("display_name")
    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public Taxonomy withDisplay_name(String display_name) {
        this.display_name = display_name;
        return this;
    }

    @JsonProperty("taxonomy_type")
    public String getTaxonomy_type() {
        return taxonomy_type;
    }

    @JsonProperty("taxonomy_type")
    public void setTaxonomy_type(String taxonomy_type) {
        this.taxonomy_type = taxonomy_type;
    }

    public Taxonomy withTaxonomy_type(String taxonomy_type) {
        this.taxonomy_type = taxonomy_type;
        return this;
    }

    @JsonProperty("enabled")
    public Boolean getEnabled() {
        return enabled;
    }

    @JsonProperty("enabled")
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Taxonomy withEnabled(Boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    @JsonProperty("images")
    public List<String> getImages() {
        return images;
    }

    @JsonProperty("images")
    public void setImages(List<String> images) {
        this.images = images;
    }

    public Taxonomy withImages(List<String> images) {
        this.images = images;
        return this;
    }

    @JsonProperty("priority")
    public Long getPriority() {
        return priority;
    }

    @JsonProperty("priority")
    public void setPriority(Long priority) {
        this.priority = priority;
    }

    public Taxonomy withPriority(Long priority) {
        this.priority = priority;
        return this;
    }

    @JsonProperty("nodes")
    public List<Node> getNodes() {
        return nodes;
    }

    @JsonProperty("nodes")
    public void setNodes(List<Node> nodes) {
        this.nodes = nodes;
    }

    public Taxonomy withNodes(List<Node> nodes) {
        this.nodes = nodes;
        return this;
    }

    @JsonProperty("meta")
    public Meta getMeta() {
        return meta;
    }

    @JsonProperty("meta")
    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public Taxonomy withMeta(Meta meta) {
        this.meta = meta;
        return this;
    }

    public Taxonomy setDefaultData()
    {
        this.withDisplay_name("test").withStore_id("100").withTaxonomy_type("All Listing").withEnabled(true).
                withImages(new ArrayList<>(Arrays.asList("ig4pc6hfayiogabujpc3","abcd"))).withPriority(1l).
                withMeta(new Meta()).withNodes(new ArrayList(Arrays.asList(new Rule_set(),new Rule())));
        return this;
    }


}
