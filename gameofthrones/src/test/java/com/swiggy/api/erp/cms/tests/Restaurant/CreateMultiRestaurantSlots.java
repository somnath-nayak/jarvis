package com.swiggy.api.erp.cms.tests.Restaurant;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.dp.RestaurantDP;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CreateMultiRestaurantSlots extends RestaurantDP {
    CMSHelper cmsHelper= new CMSHelper();

    @Test(dataProvider = "restslotscreate1",description = "Create multi restaurant slots and push to kafka")
    public void createMultirestSlots2(String day,String restaurant_id, String open_time, String close_time){
        String response=cmsHelper.restaurantSlotsCreate(day,restaurant_id,open_time,close_time).ResponseValidator.GetBodyAsText();
        int status=JsonPath.read(response,"$.status");
        Assert.assertEquals(status,1);
    }
}
