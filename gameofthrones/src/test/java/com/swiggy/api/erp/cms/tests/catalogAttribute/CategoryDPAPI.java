package com.swiggy.api.erp.cms.tests.catalogAttribute;

import com.swiggy.api.erp.cms.dp.itemDP;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import framework.gameofthrones.Aegon.Initialize;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CategoryDPAPI extends itemDP {
    Initialize gameofthrones = new Initialize();
    CMSHelper cmsHelper= new CMSHelper();
    @Test(dataProvider = "createitempj",description = "Category prediction DP API")
    public void dpCategory(String name){
    int responsecode=cmsHelper.dpAPICategory(name).ResponseValidator.GetResponseCode();
        Assert.assertEquals(200,responsecode,"Response code is not 200");
    }

}
