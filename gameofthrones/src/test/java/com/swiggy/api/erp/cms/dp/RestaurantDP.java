package com.swiggy.api.erp.cms.dp;

import org.testng.annotations.DataProvider;

public class RestaurantDP {


    @DataProvider(name = "restslotscreate")
    public Object[][] restSlotsCreate(){



        return new Object[][]{
                {System.getenv("day"),System.getenv("restaurant_id"),System.getenv("open_time"),System.getenv("close_time")},

        };

    }
    @DataProvider(name = "restslotscreate1")
    public Object[][] restSlotsCreate1(){



        return new Object[][]{

                {"TUE","10272","1","2359"},

                {"TUE","5271","1","2359"},
                {"TUE","757","1","2359"},
                {"TUE","535","1","2359"},
                {"TUE","532","1","2359"},
                {"TUE","223","1","2359"},
                {"TUE","9421","1","2359"},
                {"TUE","23683","1","2359"},
                {"TUE","57062","1","2359"}

        };

    }
    @DataProvider(name = "multirestslotscreate")
    public Object[][] restSlotsCreate2(){



        return new Object[][]{
                {System.getenv("day"),System.getenv("restaurant_id"),System.getenv("open_time"),System.getenv("close_time")},
                {System.getenv("day"),System.getenv("restaurant_id"),System.getenv("open_time"),System.getenv("close_time")},
                {System.getenv("day"),System.getenv("restaurant_id"),System.getenv("open_time"),System.getenv("close_time")},
                {System.getenv("day"),System.getenv("restaurant_id"),System.getenv("open_time"),System.getenv("close_time")},
                {System.getenv("day"),System.getenv("restaurant_id"),System.getenv("open_time"),System.getenv("close_time")},
                {System.getenv("day"),System.getenv("restaurant_id"),System.getenv("open_time"),System.getenv("close_time")}

        };

    }
    @DataProvider(name = "restslotscreate3")
    public Object[][] restSlotsCreate3(){



        return new Object[][]{
                {"TUE","5271","1","2359"},


        };

    }

}
