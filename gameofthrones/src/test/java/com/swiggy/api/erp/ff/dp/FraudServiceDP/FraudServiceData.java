package com.swiggy.api.erp.ff.dp.FraudServiceDP;

import org.testng.annotations.DataProvider;


public class FraudServiceData {


    @DataProvider(name = "PendingCancellationFee")
    public Object[][] getPendingFeeDataSet() {

        FraudService dataService=new FraudService();
        return dataService.getPendingCancellationFeeData();
    }

    @DataProvider(name = "CityConfigurations")
    public Object[][] getCityConfigurationDataSet() {


        FraudService dataService=new FraudService();
        return dataService.getCityConfiguration();
    }


    @DataProvider(name = "getCityConfiguration_basedTimeDataSet")
    public Object[][] getCityConfiguration_basedTimeDataSet() {


        FraudService dataService=new FraudService();
        return dataService.getCityConfigurationBasedTime();
    }

    @DataProvider(name = "PendingFeeCityConfigurations")
    public Object[][] PendingFeeCityConfigurations() {


        FraudService dataService=new FraudService();
        return dataService.getPendingFeeCityConfiguration();
    }


    @DataProvider(name = "exclustionList")
    public Object[][] getexclusionlistDataSet() {

        FraudService dataService=new FraudService();
        return dataService.getExclustionListData();
    }

    @DataProvider(name = "DSCheck")
    public Object[][] getDSCheckDataSet() {

        FraudService dataService=new FraudService();
        return dataService.getDSCheckData();
    }

    @DataProvider(name = "cityDefaultCheck")
    public Object[][] cityDefaultCheck() {

        FraudService dataService=new FraudService();
        return dataService.getCityDefaultCheck();
    }


    @DataProvider(name = "defaultDSPFailedCheck")
    public Object[][] DefaultDSPFailedCheck() {

        FraudService dataService=new FraudService();
        return dataService.getDefaultDSPData();
    }
}
