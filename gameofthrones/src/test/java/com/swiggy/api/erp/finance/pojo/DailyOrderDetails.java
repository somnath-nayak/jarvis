package com.swiggy.api.erp.finance.pojo;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.erp.finance.pojo
 **/

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "fromTime",
        "toTime",
        "deList"
})
public class DailyOrderDetails {

    @JsonProperty("fromTime")
    private String fromTime;
    @JsonProperty("toTime")
    private String toTime;
    @JsonProperty("deList")
    private List<Integer> deList = null;

    @JsonProperty("fromTime")
    public String getFromTime() {
        return fromTime;
    }

    @JsonProperty("fromTime")
    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    @JsonProperty("toTime")
    public String getToTime() {
        return toTime;
    }

    @JsonProperty("toTime")
    public void setToTime(String toTime) {
        this.toTime = toTime;
    }

    @JsonProperty("deList")
    public List<Integer> getDeList() {
        return deList;
    }

    @JsonProperty("deList")
    public void setDeList(List<Integer> deList) {
        this.deList = deList;
    }

    public void setDefaultValues(String from, String to, List<Integer> deIDs) {
        if (this.getDeList() == null)
            this.setDeList(deIDs);
        if (this.getFromTime() == null)
            this.setFromTime(from);
        if (this.getToTime() == null)
            this.setToTime(to);
    }

    public DailyOrderDetails build(String from, String to, List<Integer> deIDs) {
        setDefaultValues(from,to,deIDs);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("fromTime", fromTime).append("toTime", toTime).append("deList", deList).toString();
    }

}
