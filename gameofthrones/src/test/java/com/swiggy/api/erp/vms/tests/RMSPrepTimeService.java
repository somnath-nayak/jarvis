package com.swiggy.api.erp.vms.tests;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.swiggy.api.erp.vms.constants.RMSConstants;
import com.swiggy.api.erp.vms.dp.RMSPrepTimeDP;
import com.swiggy.api.erp.vms.helper.RMSPrepTimeHelper;
import framework.gameofthrones.Cersei.ToolBox;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.apache.commons.collections.CollectionUtils;
import org.testng.asserts.SoftAssert;

import java.io.IOException;
import java.util.List;

/**
 * Created by kiran.j on 8/2/18.
 */
public class RMSPrepTimeService extends RMSPrepTimeDP {

    RMSPrepTimeHelper helper = new RMSPrepTimeHelper();
    SchemaValidatorUtils schemaValidatorUtils = new SchemaValidatorUtils();


    @Test(dataProvider="slotWisePrepTime",groups={"sanity","smoke","regression"})
    public void SlotWisePrepTime(String prepTimeRestaurantId,String statusMessage, String auth, boolean valid) throws IOException, ProcessingException
    {
        Processor processor = helper.getPrepTimeSlotWise(prepTimeRestaurantId, auth);
        if(valid) {
            String resp = processor.ResponseValidator.GetBodyAsText();
            String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir") + "/../Data/SchemaSet/Json/VMS/daywisePreptime.txt");
            List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
            Assert.assertTrue(CollectionUtils.isEmpty(missingNodeList),missingNodeList + " Nodes Are Missing Or Not Matching For POP Aggreagtor API");
            boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
            Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
            Assert.assertTrue(processor.ResponseValidator.GetNodeValue("statusMessage").equalsIgnoreCase(statusMessage), "Status Message is Incorrect");
        } else {
            Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 404, "Invalid Restaurant is Accepted");
        }
    }


    @Test(dataProvider="dayWisePrepTime",groups={"sanity","smoke","regression"}, description = "")
    public void fetchPrepTime(String prepTimeRestaurantId,String statusMessage, String auth, boolean valid) throws IOException, ProcessingException
    {
        Processor processor = helper.fetchSlotWise(prepTimeRestaurantId, auth);
        String resp = processor.ResponseValidator.GetBodyAsText();
        String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir") + "/../Data/SchemaSet/Json/VMS/daywisePreptime.txt");
        List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
        Assert.assertTrue(CollectionUtils.isEmpty(missingNodeList),missingNodeList + " Nodes Are Missing Or Not Matching For POP Aggreagtor API");
        boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
        Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
        Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
    }

    @Test(dataProvider="deleteSlotPrepTime",groups={"sanity","smoke","regression"})
    public void DelPrepTime(String prepTimeRestaurantId,String statusMessage, String auth,  boolean valid, String restaurantId) throws IOException, ProcessingException
    {
        SoftAssert softAssert = new SoftAssert();
        Processor processor=helper.delPreptime(prepTimeRestaurantId, auth, restaurantId);
        String resp=processor.ResponseValidator.GetBodyAsText();
        String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/SchemaSet/Json/VMS/deletePreptime.txt");
        List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
        Assert.assertTrue(CollectionUtils.isEmpty(missingNodeList),missingNodeList + " Nodes Are Missing Or Not Matching For POP Aggreagtor API");
        boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
        Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
        if(valid)
            softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
        else
            softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("data"), statusMessage);
        softAssert.assertAll();
    }

    @Test(dataProvider="savePrepTime",groups={"sanity","smoke","regression"} ,description = "Creates New Prep Time for a Restaurant(SLOTWISE or GLOBAL)")
    public void SavePrepTime(String prepTimeRestaurantId,String from, String to, String prep_time, String prep_type, String days, String statusMessage, String auth, boolean check) throws IOException, ProcessingException
    {
        Processor processor=helper.savingPreptime(prepTimeRestaurantId, from, to,prep_time, prep_type, days, auth);
        SoftAssert softAssert = new SoftAssert();
        if(check) {
            String resp = processor.ResponseValidator.GetBodyAsText();
            String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir") + "/../Data/SchemaSet/Json/VMS/savePreptime.txt");
            List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
            Assert.assertTrue(CollectionUtils.isEmpty(missingNodeList),missingNodeList + " Nodes Are Missing Or Not Matching For POP Aggreagtor API");
            if(prep_type.equalsIgnoreCase(RMSConstants.PrepType.GLOBAL.toString())) {
                String[] global_slots = helper.fetchSlotWiseId(RMSConstants.restaurantId, RMSConstants.PrepType.GLOBAL.toString());
                if(global_slots.length >= 1)
                    softAssert.assertTrue(helper.verifyNegativeResponse(processor));
            }
            else {
                boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
                softAssert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
                softAssert.assertTrue(helper.verifySaveResponse(from, to, prep_time, prep_type, days, processor, false));
                softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
                String slotID = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[*].id");
                softAssert.assertNotEquals(slotID, null, "SlotID id is null");
            }
        }
//        else
//            softAssert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("status"), Integer.parseInt(statusMessage));
        softAssert.assertAll();
    }


    @Test(dataProvider="updatepreptime",groups={"sanity","smoke","regression"})
    public void updatePrepTime(String from, String to, String prep_time, String prep_type, String days, String prep_id, String statusMessage, String auth, boolean valid, String rest_id) throws IOException, ProcessingException
    {
        SoftAssert softAssert = new SoftAssert();
        Processor processor=helper.updatePrepTime(from, to,prep_time, prep_type, days, prep_id, auth, rest_id);
        String resp=processor.ResponseValidator.GetBodyAsText();
        String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/SchemaSet/Json/VMS/savePreptime.txt");;
        List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
        Assert.assertTrue(CollectionUtils.isEmpty(missingNodeList),missingNodeList + " Nodes Are Missing Or Not Matching For POP Aggreagtor API");
        boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
        if(valid) {
            softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
            softAssert.assertTrue(helper.verifySaveResponse(from, to, prep_time, prep_type, days, processor, false));
        }
        else
            softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
        softAssert.assertAll();
    }


    @Test(dataProvider="savePrepTimeMulti",groups={"sanity","smoke","regression"})
    public void SavePrepTimeMulti(String prepTimeRestaurantId,String from, String to, String prep_time, String prep_type, String days, String statusMessage, String auth, boolean ignore, boolean check) throws IOException, ProcessingException
    {
        SoftAssert softAssert = new SoftAssert();
        Processor processor=helper.savingPreptimeMulti(prepTimeRestaurantId, from, to, prep_time, prep_type, days, auth);
        if(check) {
            String resp = processor.ResponseValidator.GetBodyAsText();
            String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir") + "/../Data/SchemaSet/Json/VMS/savePreptimeMulti.txt");
            List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
            Assert.assertTrue(CollectionUtils.isEmpty(missingNodeList),missingNodeList + " Nodes Are Missing Or Not Matching For POP Aggreagtor API");
            if(prep_type.equalsIgnoreCase(RMSConstants.PrepType.GLOBAL.toString())) {
                String[] global_slots = helper.fetchSlotWiseId(RMSConstants.restaurantId, RMSConstants.PrepType.GLOBAL.toString());
                if(global_slots.length >= 1)
                    softAssert.assertTrue(helper.verifyNegativeResponse(processor));
            }
            else {
                boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
                softAssert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
                softAssert.assertTrue(helper.verifySaveResponse(from, to, prep_time, prep_type, days, processor, ignore));
                softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
            }
        } else
            softAssert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("status"), Integer.parseInt(statusMessage));

        softAssert.assertAll();
    }


}
