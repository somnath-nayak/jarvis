package com.swiggy.api.erp.ff.POJO.CouponFraudCheck;

public class Customer_details
{
    private String device_id;

    public String getDevice_id ()
    {
        return device_id;
    }

    public void setDevice_id (String device_id)
    {
        this.device_id = device_id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [device_id = "+device_id+"]";
    }
}
