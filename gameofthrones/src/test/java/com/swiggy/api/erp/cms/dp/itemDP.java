package com.swiggy.api.erp.cms.dp;

import com.swiggy.api.erp.cms.constants.CmsConstants;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import org.testng.annotations.DataProvider;

import java.time.Instant;

public class itemDP {
    CMSHelper cmshelper=new CMSHelper();

    @DataProvider(name = "createitempj")
    public Object[][] itempj() throws InterruptedException{

        String s = "chicken tandoori for catalog"+String.valueOf(Instant.now().getEpochSecond());
        Thread.sleep(1000);

        return new Object[][]{
                {
                        s}
        };
    }

    @DataProvider(name = "updateitm")
    public Object[][] updateitem() {

        String name = "Chicken shawrma"+String.valueOf(Instant.now().getEpochSecond());

        return new Object[][]{
                { "comments", "description", name,cmshelper.getActiveItem()}
        };
    }

    @DataProvider(name="itemcache")
    public Object[][] itemCache(){

            return new Object[][]{
                    {
                        System.getenv("itemId")
                    }

            };

        }



}
