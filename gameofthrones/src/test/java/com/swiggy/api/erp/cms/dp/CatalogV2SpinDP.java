package com.swiggy.api.erp.cms.dp;


import com.swiggy.api.erp.cms.helper.CatalogV2Helper;
import org.testng.annotations.DataProvider;


public class CatalogV2SpinDP {
    CatalogV2Helper catalogV2Helper=new CatalogV2Helper();

    String res=catalogV2Helper.createCategoryPojo().ResponseValidator.GetBodyAsText();


    @DataProvider(name = "cratespindp")
    public Object[][] test() throws InterruptedException{
        return new Object[][]{
                {res}};
    }

}

