package com.swiggy.api.erp.vms.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.vms.dp.PlacingServiceDP;
import com.swiggy.api.erp.vms.helper.PlacingServiceHelper;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;
import framework.gameofthrones.Cersei.ToolBox;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import org.apache.commons.collections.CollectionUtils;
import org.codehaus.jackson.JsonParseException;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.List;

public class PlacingServiceTest extends PlacingServiceDP {

	PlacingServiceHelper helper = new PlacingServiceHelper();
	SchemaValidatorUtils schemaValidatorUtils = new SchemaValidatorUtils();

	int updatedEventId;
	
	@Test(dataProvider="deleteStateTransition",groups={"sanity","smoke"},description="Delete State Transition")
	public void deleteStateTransition(int transId, String deleteTransition)
		{

		Processor processor=helper.getAllStateTransition();
		String resp=processor.ResponseValidator.GetBodyAsText();

		String data = JsonPath.read(resp, "$.data..id").toString().replace("[", "").replace("]", "");
		String[] id =data.split(",");
		System.out.println("length is "+id.length + id[0]);
		for(int i =0; i<id.length; i++)
		{
			if(id[i].isEmpty())
			{
				System.out.println("No State Transition");
				Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), "State Transition Map fetched successfully");
				Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), 0);
			}
			else
			{
			Processor P2=helper.deleteStateTransition(Integer.parseInt(id[i]));
			Assert.assertEquals(P2.ResponseValidator.GetNodeValue("statusMessage"), deleteTransition);
			Assert.assertEquals(P2.ResponseValidator.GetNodeValueAsInt("statusCode"), 0);
			i = i+3;
			}
		}

	}
	
	
	@Test(dataProvider="getAndDeleteEvent",groups={"sanity","smoke"}, description="Create Event")
	public void getAndDeleteEvent(String geteventMessage, int eventId, String deleteeventMessage, int transId, String deleteTransition) throws Exception{
		Processor processor=helper.getAllEvents();
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), geteventMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), 0);
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..type"));
		String response=processor.ResponseValidator.GetBodyAsText();
		String data1 = JsonPath.read(response, "$.data..id").toString().replace("[", "").replace("]", "");
		String[] id1 =data1.split(",");

		for(int i =0; i<id1.length;i++)
		{
			if(id1[i].isEmpty()) {
				System.out.println("No Events");
				Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), geteventMessage);
				Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), 0);

			}
			else {
			Processor P2=helper.deleteEvent(Integer.parseInt(id1[i]));
			String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/placing_delete_event");
			List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema, P2.ResponseValidator.GetBodyAsText());
			Assert.assertTrue(CollectionUtils.isEmpty(missingNodeList),missingNodeList + " Nodes Are Missing Or Not Matching For POP Aggreagtor API");
			Assert.assertEquals(P2.ResponseValidator.GetNodeValue("statusMessage"), deleteeventMessage);
			Assert.assertEquals(P2.ResponseValidator.GetNodeValueAsInt("statusCode"), 0);
			}
		}
	}

	@Test(dataProvider="createEvent",groups={"sanity","smoke"}, description="Create Event", dependsOnMethods = "getAndDeleteEvent")
	public void createEvent(String eventType, String updatedBy, String addeventMessage, String geteventMessage, int eventId, String deleteeventMessage) throws Exception {
		Processor processor = helper.createEvent(eventType, updatedBy);
		String resp = processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/placing_create_event");
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
		Assert.assertTrue(CollectionUtils.isEmpty(missingNodeList),missingNodeList + " Nodes Are Missing Or Not Matching For Create Event API");
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), addeventMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), 0);
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsInt("$.data.id"));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("$.data.type"), eventType);
	}

	@Test(dataProvider="createEventRegression1",groups={"regression"}, description="Create Event Regression")
	public void createEventRegression1(String eventType, String updatedBy, int expectedStatusCode, String addeventErrorMessage) throws Exception {
		Processor processor=helper.createEvent(eventType, updatedBy);
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/event_exist");
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema,processor.ResponseValidator.GetBodyAsText());
		Assert.assertTrue(CollectionUtils.isEmpty(missingNodeList),missingNodeList + " Nodes Are Missing Or Not Matching For Create Event API");
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), addeventErrorMessage );
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), expectedStatusCode);
	}


	@Test(dataProvider="createEventRegression2",groups={"regression"}, description="Create Event Regression")
	public void createEventRegression2(String eventType, String updatedBy, int expectedStatusCode, String addeventErrorMessage) {
		Processor processor=helper.createEvent(eventType, updatedBy);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("$.error"), addeventErrorMessage );
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("$.status"), expectedStatusCode);
	}

	@Test(dataProvider="getAndDeleteState",groups={"sanity","smoke"}, description="Create Event")
	public void getAndDeleteState(String getstatetMessage, int stateId, String deletestateMessage, int transId, String deleteTransition) throws Exception
	{
		 Processor processor=helper.getAllStates();

		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), getstatetMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), 0);
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..type"));
		String response=processor.ResponseValidator.GetBodyAsText();
		String data1 = JsonPath.read(response, "$.data..id").toString().replace("[", "").replace("]", "");

		String[] id1 =data1.split(",");

			for(int i =0; i<id1.length;i++)
			{
			if(id1[i].isEmpty())
			{
				System.out.println("No Events");
				Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), getstatetMessage);
				Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), 0);

			}
			else
			{
			System.out.println("id isssss"  +i);
			System.out.println("State isssss"  +id1[i]);
			Processor P2=helper.deleteState(Integer.parseInt(id1[i]));
			String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/placing_delete_state");
			List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,P2.ResponseValidator.GetBodyAsText());
			Assert.assertTrue(CollectionUtils.isEmpty(missingNodeList),missingNodeList + " Nodes Are Missing Or Not Matching For delete state API");
			Assert.assertEquals(P2.ResponseValidator.GetNodeValue("statusMessage"), deletestateMessage);
			Assert.assertEquals(P2.ResponseValidator.GetNodeValueAsInt("statusCode"), 0);
			}
			}
	}
		
	@Test(dataProvider="deleteEventRegression",groups={"regression"}, description="Delete Event Regression")
	public void deleteEventRegression(int eventId, int expectedStatusCode, String deleteeventErrorMessage)
	{
		Processor P2=helper.deleteEvent(eventId);
		Assert.assertEquals(P2.ResponseValidator.GetNodeValue("statusMessage"), deleteeventErrorMessage);
		Assert.assertEquals(P2.ResponseValidator.GetNodeValueAsInt("statusCode"), expectedStatusCode);
	}

		
	@Test(dataProvider="createState",groups={"sanity","smoke"}, description="Create State")
	public void createState(String stateType, String updatedBy, String addstateMessage, String getstatetMessage, int stateId, String deletestateMessage) throws Exception {
		Processor processor=helper.createState(stateType, updatedBy);
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/placing_delete_state");
		List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,processor.ResponseValidator.GetBodyAsText());
		Assert.assertTrue(CollectionUtils.isEmpty(missingNodeList),missingNodeList + " Nodes Are Missing Or Not Matching For Create State API");
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), addstateMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), 0);
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsInt("$.data.id"));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("$.data.type"), stateType);
	}
	
	@Test(dataProvider="createStateRegression1",groups={"regression"}, description="Create Event Regression")
	public void createStateRegression1(String stateType, String updatedBy, int expectedStatusCode, String addeventErrorMessage) throws Exception{
		Processor processor=helper.createState(stateType, updatedBy);
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/event_exist");
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema,processor.ResponseValidator.GetBodyAsText());
		Assert.assertTrue(CollectionUtils.isEmpty(missingNodeList),missingNodeList + " Nodes Are Missing Or Not Matching For Create Event API");
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), addeventErrorMessage );
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), expectedStatusCode);
	}
	
	@Test(dataProvider="deleteStateRegression",groups={"regression"}, description="Delete Event Regression")
    public void deleteStateRegression(int stateId, int expectedStatusCode, String deleteStateErrorMessage) {
		Processor P2=helper.deleteState(stateId);
		Assert.assertEquals(P2.ResponseValidator.GetNodeValue("statusMessage"), deleteStateErrorMessage);
		Assert.assertEquals(P2.ResponseValidator.GetNodeValueAsInt("statusCode"), expectedStatusCode);
	}

	
	@Test(dataProvider="createStateRegression",groups={"regression"}, description="Create Event Regression")
    public void createStateRegression(String stateType, String updatedBy, int expectedStatusCode, String addstateErrorMessage) {
		Processor processor=helper.createState(stateType, updatedBy);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("$.error"), addstateErrorMessage );
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("$.status"), expectedStatusCode);
	}
	

	@Test(dataProvider="addStateTransition",groups={"sanity","smoke"},description="Create Event")
	public void addStateTransition(int fromStateId, int eventId, int toStateId, String updatedBy, String addstateTransitionMessage, int transId, String deleteTransition) throws IOException, SwiggyAPIAutomationException
		{

		Processor P1=helper.getAllStates();
		String response1=P1.ResponseValidator.GetBodyAsText();				
		String data1 = JsonPath.read(response1, "$.data..id").toString().replace("[", "").replace("]", "");
				
		String[] id1 =data1.split(",");
		System.out.println("States Array Lenght is " +id1.length);
		System.out.println(id1[0]);
					
		for(int i=0; !id1[i].isEmpty()&&i<id1.length-1;i++)
	    {
			
	        System.out.println("id isssss"  +i);
	       
	        Processor P2=helper.getAllEvents();
			String response2=P2.ResponseValidator.GetBodyAsText();				
			String data2 = JsonPath.read(response2, "$.data..id").toString().replace("[", "").replace("]", "");
					
			String[] id2 =data2.split(",");
			System.out.println("Events Array Lenght is " +id2.length);		
			for(int j =0; !id2[j].isEmpty()&&j<id2.length-1; j=j+1)
		    {
		        System.out.println("id isssss"  +j);
		        
				Processor processor1=helper.addStateTransition(Integer.parseInt(id1[i]), Integer.parseInt(id2[j]), Integer.parseInt(id1[i+1]), updatedBy);
				String resp1=processor1.ResponseValidator.GetBodyAsText();
				//int Id = processor1.ResponseValidator.GetNodeValueAsInt("$.data.id");
				
				Assert.assertEquals(processor1.ResponseValidator.GetNodeValue("statusMessage"), addstateTransitionMessage);
		        Assert.assertEquals(processor1.ResponseValidator.GetNodeValueAsInt("statusCode"), 0);
		        Assert.assertNotNull(processor1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
			    Assert.assertNotNull(processor1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..type"));
		    }
			
		}
	}

	@Test(dataProvider="addStateTransition",groups={"regression"},  description="Add State Transitoin Regression 1")
	public void addStateTransitionRegression1(int fromStateId, int eventId, int toStateId, String updatedBy, String addstateTransitionMessage, int transId, String deleteTransition) throws IOException, SwiggyAPIAutomationException
		{
		
		Processor P1=helper.getAllStates();
		String response1=P1.ResponseValidator.GetBodyAsText();				
		String data1 = JsonPath.read(response1, "$.data..id").toString().replace("[", "").replace("]", "");
				
		String[] id1 =data1.split(",");
					
		for(int i =0; !id1[i].isEmpty()&&i<id1.length-1;i++)
	    {
	        System.out.println("id isssss"  +i);
	       
	        Processor P2=helper.getAllEvents();
			String response2=P2.ResponseValidator.GetBodyAsText();				
			String data2 = JsonPath.read(response2, "$.data..id").toString().replace("[", "").replace("]", "");
					
			String[] id2 =data2.split(",");
					
			for(int j =0; !id2[j].isEmpty()&&j<id2.length-1; j=j+1)
		    {
		        System.out.println("id isssss"  +j);
		        
				Processor processor1=helper.addStateTransition(Integer.parseInt(id1[i]), Integer.parseInt(id2[j]), Integer.parseInt(id1[i+1]), updatedBy);
				  
				Assert.assertEquals(processor1.ResponseValidator.GetNodeValue("statusMessage"), "Could not add the State Transition");
		        Assert.assertEquals(processor1.ResponseValidator.GetNodeValueAsInt("statusCode"), 1);
		    }
			
		}
	}

	@Test(dataProvider="addStateTransitionRegression2",groups={"regression"}, description="Delete Event Regression")
    public void addStateTransitionRegression2(int fromStateId, int eventId, int toStateId, String updatedBy, int expectedStatusCode, String addTransitionErrorMessage) {
		Processor P2=helper.addStateTransition(fromStateId, eventId, toStateId, updatedBy);
		Assert.assertEquals(P2.ResponseValidator.GetNodeValue("statusMessage"), addTransitionErrorMessage);
		Assert.assertEquals(P2.ResponseValidator.GetNodeValueAsInt("statusCode"), expectedStatusCode);
	}
	
	@Test(dataProvider="getAllStateTransition",groups={"sanity","smoke"},description="Get all State Transitions")
	public void getAllStateTransition(String getstatetTransitionMessage) throws Exception{
		Processor processor=helper.getAllStateTransition();
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/state_transition");
		List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,processor.ResponseValidator.GetBodyAsText());
		Assert.assertTrue(CollectionUtils.isEmpty(missingNodeList),missingNodeList + " Nodes Are Missing Or Not Matching For Get All State Transition API");
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), getstatetTransitionMessage);
        Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), 0);
        Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
	    Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..type"));
	}
	
	@Test(dataProvider="makeTransition",groups={"sanity","smoke"}, description="Create State")
	public void makeTransition(String eventId, String eventType, Long eventTimestamp, long orderId, String updatedBy, String restaurantId, String makeTramsitionMessage) {
		 String newVal = eventId;
		    boolean flag=false;
			int counter = 0;
		    Processor processor = null;
			while(!flag && counter < 5) {
				 processor=helper.makeTransition(newVal, eventType, eventTimestamp, orderId, updatedBy, restaurantId);
				if (processor.ResponseValidator.GetNodeValue("statusMessage").equals("Failed to capture Placing Event")) {
					flag = false;
					counter++;
					newVal = helper.getEventId(newVal);
				} else {
					flag = true;
					break;
				}
			}
		String resp=processor.ResponseValidator.GetBodyAsText();
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), makeTramsitionMessage);
        Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), 0);
		}

	@Test(dataProvider="makeTransitionRegression",groups={"sanity","smoke"}, description="Create State")
	public void makeTransitionRegression(String eventId, String eventType, Long eventTimestamp, long orderId, String updatedBy, String restaurantId, int expectedStatusCode, String makeTramsitionErrorMessage) {
		Processor processor=helper.makeTransition(eventId, eventType, eventTimestamp, orderId, updatedBy, restaurantId);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("$.error"), makeTramsitionErrorMessage );
	    Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("$.status"), expectedStatusCode);
	}

	
	@Test(dataProvider="getPlacingState",groups={"sanity","smoke"}, description="Get Placing State")
	public void getPlacingState(long order_id, String getPlacingStateMessage) throws Exception {
		Processor processor=helper.getPlacingState(order_id);
		String resp=processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/placingstate");
		List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		Assert.assertTrue(CollectionUtils.isEmpty(missingNodeList),missingNodeList + " Nodes Are Missing Or Not Matching For POP Aggreagtor API");
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), getPlacingStateMessage);
        Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), 0);
        Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.state"));
        Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.placingEvent.id"));
        Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.placingEvent.eventType"));
	}
	
	@Test(dataProvider="getPlacingStateRegression",groups={"regression"}, description="Get Placing State")
	public void getPlacingStateRegression(long order_id, int expectedStatusCode, String getPlacingStateErrorMessage) throws Exception {
		Processor processor=helper.getPlacingState(order_id);
		String resp=processor.ResponseValidator.GetBodyAsText();
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), getPlacingStateErrorMessage);
        Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), 1);
	}

	@Test(dataProvider="deleteStateTransitionRegression",groups={"regression"},description="Delete State Transition Regression")
	public void deleteStateTransitionRegression(int transId, int statusCode, String deleteTransitionErrorMessage) {
		Processor P2=helper.deleteStateTransition(transId);
		Assert.assertEquals(P2.ResponseValidator.GetNodeValue("statusMessage"), deleteTransitionErrorMessage);
		Assert.assertEquals(P2.ResponseValidator.GetNodeValueAsInt("statusCode"), 1);
	}
	
	@Test(dataProvider="getHealthCheck",groups={"sanity","smoke"},description="Get Health Check")
	public void getHealthCheck(String status) {
		Processor P=helper.healthCheck(status);
		Assert.assertEquals(P.ResponseValidator.GetNodeValue("status"), status);
	}
}
