package com.swiggy.api.erp.delivery.tests;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.erp.delivery.constants.RTSConstants;
import com.swiggy.api.erp.delivery.dp.MultiRestaurantBatchingDataProvider;
import com.swiggy.api.erp.delivery.helper.DeliveryDataHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryHelperMethods;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceHelper;
import com.swiggy.api.erp.delivery.helper.RTSHelper;

public class MultiRestaurantBatchingTest extends
		MultiRestaurantBatchingDataProvider {
	DeliveryServiceHelper helpdel = new DeliveryServiceHelper();
	DeliveryHelperMethods delmeth = new DeliveryHelperMethods();
	DeliveryDataHelper deldata= new DeliveryDataHelper();
	RTSHelper rtshelp = new RTSHelper();
	static Object de_id;
	
	@BeforeClass
        public void enablebatching() {
		String query3=DeliveryConstant.update_rest_query+"'"+DeliveryConstant.multi_rest_batching_rest1_lat_long+"'"+" where id="+DeliveryConstant.multi_rest_batching_rest1;
		delmeth.dbhelperupdate(query3);
		String query4=DeliveryConstant.update_rest_query+"'"+DeliveryConstant.multi_rest_batching_rest2_lat_long+"'"+" where id="+DeliveryConstant.multi_rest_batching_rest2;
		delmeth.dbhelperupdate(query4);
		addRedisKeyForRestDistance(DeliveryConstant.multi_rest_batching_rest1,DeliveryConstant.multi_rest_batching_rest2);
		delmeth.dbhelperupdate(DeliveryConstant.batching_enabled_query);
		delmeth.dbhelperupdate(DeliveryConstant.multi_rest_batching_enabled_query);
		String query = RTSConstants.enable_resturant_batching
				+ DeliveryConstant.multi_rest_batching_rest1;
		delmeth.dbhelperupdate(query);
		String query1=RTSConstants.enable_resturant_batching
				+ DeliveryConstant.multi_rest_batching_rest2;
		delmeth.dbhelperupdate(query1);
		delmeth.dbhelperupdate(RTSConstants.set_random_de_id);
		de_id=deldata.CreateDE(DeliveryConstant.zone_id_int);
	}
	@AfterMethod
	public void closeallbatches()
	{
		delmeth.dbhelperupdate(DeliveryConstant.updatebatch);
		delmeth.dbhelperupdate(DeliveryConstant.assigndetounassinged);
	}
	@Test(dataProvider = "QE-282",priority=20, groups = "MultiRestaurantBatching", description = "Verify whether two orders from two different restaurants under same zone are batched only if both of these orders are non with_de")
	public void MultiRestaurantBatching282(String order_id1, String order_id2)
			throws InterruptedException {
		Assert.assertNotNull(order_id1,
				"Order is not created through pushing Order Json");
		Assert.assertNotNull(order_id2,
				"Order is not created through pushing Order Json");
		String query1 = DeliveryConstant.batch_id + order_id1 + ";";
		String query2 = DeliveryConstant.batch_id + order_id2 + ";";
		Object batch_id1 = delmeth.dbhelperget(query1, "batch_id");
		Object batch_id2 = delmeth.dbhelperget(query2, "batch_id");
		rtshelp.runassignment(RTSConstants.cityid);
		if (batch_id1 != null && batch_id2!= null ) {
			Object batch_id3 = delmeth.dbhelperget(query1, "batch_id");
			Object batch_id4 = delmeth.dbhelperget(query2, "batch_id");
		Assert.assertEquals(batch_id3,batch_id4);
			}
		else {
			System.out.println("Batch id not present in Batch table");
			Assert.assertTrue(false);
		}
	}
	@Test(dataProvider = "QE-283",priority=21, groups = "MultiRestaurantBatching", description = "Verify whether two orders from two different restaurants from different zones are not batched even if both of these orders are non with_de.")
	public void MultiRestaurantBatching283(String order_id1, String order_id2)
			throws InterruptedException {
		Assert.assertNotNull(order_id1,
				"Order is not created through pushing Order Json");
		Assert.assertNotNull(order_id2,
				"Order is not created through pushing Order Json");
		String query1 = DeliveryConstant.batch_id + order_id1 + ";";
		String query2 = DeliveryConstant.batch_id + order_id2 + ";";
		Object batch_id1 = delmeth.dbhelperget(query1, "batch_id");
		Object batch_id2 = delmeth.dbhelperget(query2, "batch_id");
		rtshelp.runassignment(RTSConstants.cityid);
		if (batch_id1 != null && batch_id2!= null ) {
			Object batch_id3 = delmeth.dbhelperget(query1, "batch_id");
			Object batch_id4 = delmeth.dbhelperget(query2, "batch_id");
			Assert.assertNotEquals(batch_id3, batch_id4);
			}
		else {
			System.out.println("Batch id not present in Batch table");
			Assert.assertTrue(false);
		}
	}
	@Test(dataProvider = "QE-284",priority=22, groups = "MultiRestaurantBatching", description = "Verify whether batch is spiltted if DE has not yet arrived for the order which is marked WITH_DE")
	public void MultiRestaurantBatching284(String order_id1, String order_id2)
			throws InterruptedException {
		Assert.assertNotNull(order_id1,
				"Order is not created through pushing Order Json");
		Assert.assertNotNull(order_id2,
				"Order is not created through pushing Order Json");
		String query1 = DeliveryConstant.batch_id + order_id1 + ";";
		String query2 = DeliveryConstant.batch_id + order_id2 + ";";
		Object batch_id1 = delmeth.dbhelperget(query1, "batch_id");
		Object batch_id2 = delmeth.dbhelperget(query2, "batch_id");
		rtshelp.runassignment(RTSConstants.cityid);
          if (batch_id1 != null && batch_id2!= null ) {
        	  Object batch_id3 = delmeth.dbhelperget(query1, "batch_id");
  			Object batch_id4 = delmeth.dbhelperget(query2, "batch_id");
			if (((batch_id3.toString()).equalsIgnoreCase(batch_id4.toString())))
			{
				helpdel.markOrderWithDE(order_id1);
				rtshelp.runassignment(RTSConstants.cityid);
				Thread.sleep(5000);
				Object batch_id5 = delmeth.dbhelperget(query1, "batch_id");
				Object batch_id6 = delmeth.dbhelperget(query2, "batch_id");
				Assert.assertNotEquals(batch_id5,batch_id6);
				} 
			else
			{
				Assert.assertTrue(false);
			}
		}
		else {
			System.out.println("Batch id not present in Batch table");
			Assert.assertTrue(false);
		}
	}
	@Test(dataProvider = "QE-285",priority=23, groups = "MultiRestaurantBatching", description = "Verify whether batch is not spiltted if DE has marked arrived for the order which is marked WITH_DE")
	public void MultiRestaurantBatching285(String order_id1, String order_id2)
			throws InterruptedException {
		Assert.assertNotNull(order_id1,
				"Order is not created through pushing Order Json");
		Assert.assertNotNull(order_id2,
				"Order is not created through pushing Order Json");
		String query1 = DeliveryConstant.batch_id + order_id1 + ";";
		String query2 = DeliveryConstant.batch_id + order_id2 + ";";
		Object batch_id1 = delmeth.dbhelperget(query1, "batch_id");
		Object batch_id2 = delmeth.dbhelperget(query2, "batch_id");
		rtshelp.runassignment(RTSConstants.cityid);
		 if (batch_id1 != null && batch_id2!= null ) {
			 Object batch_id3 = delmeth.dbhelperget(query1, "batch_id");
				Object batch_id4 = delmeth.dbhelperget(query2, "batch_id");
				if (((batch_id3.toString()).equalsIgnoreCase(batch_id4.toString())))
				 {
				deldata.delocationupdate(DeliveryConstant.single_rest_cust_Dist_200m_lat, DeliveryConstant.single_rest_cust_Dist_200m_lng, de_id.toString(), DeliveryConstant.delivery_app_version);	
				helpdel.assignOrder(order_id1,de_id.toString());
				Thread.sleep(5000);
				helpdel.zipDialConfirmDE(order_id1);
				Thread.sleep(5000);
				helpdel.zipDialArrivedDE(order_id1);
				Thread.sleep(5000);
				helpdel.markOrderWithDE(order_id1);
				Thread.sleep(5000);
				Object batch_id5 = delmeth.dbhelperget(query1, "batch_id");
				Object batch_id6 = delmeth.dbhelperget(query2, "batch_id");
				Assert.assertEquals(batch_id5, batch_id6);
			} 
		}
		else {
			System.out.println("Batch id not present in Batch table");
			Assert.assertTrue(false);
		}
	}
	@Test(dataProvider = "QE-286",priority=24, groups = "MultiRestaurantBatching", description = "Verify whether batch is spiltted if DE has arrived for one of the order after which it is marked WITH_DE and not yet arrived for the other order which is also marked WITH_DE")
	public void MultiRestaurantBatching286(String order_id1, String order_id2)
			throws InterruptedException {
		Assert.assertNotNull(order_id1,
				"Order is not created through pushing Order Json");
		Assert.assertNotNull(order_id2,
				"Order is not created through pushing Order Json");
		String query1 = DeliveryConstant.batch_id + order_id1 + ";";
		String query2 = DeliveryConstant.batch_id + order_id2 + ";";
		Object batch_id1 = delmeth.dbhelperget(query1, "batch_id");
		Object batch_id2 = delmeth.dbhelperget(query2, "batch_id");
		rtshelp.runassignment(RTSConstants.cityid);
		 if (batch_id1 != null && batch_id2!= null ) {
			 Object batch_id3 = delmeth.dbhelperget(query1, "batch_id");
				Object batch_id4 = delmeth.dbhelperget(query2, "batch_id");
				if (((batch_id3.toString()).equalsIgnoreCase(batch_id4.toString())))
				 { 
				deldata.delocationupdate(DeliveryConstant.single_rest_cust_Dist_200m_lat, DeliveryConstant.single_rest_cust_Dist_200m_lng, de_id.toString(), DeliveryConstant.delivery_app_version);	
				helpdel.assignOrder(order_id1,de_id.toString());
				Thread.sleep(5000);
				helpdel.zipDialConfirmDE(order_id1);
				Thread.sleep(5000);
				helpdel.zipDialArrivedDE(order_id1);
				Thread.sleep(5000);
				helpdel.markOrderWithDE(order_id1);
				Thread.sleep(5000);
				helpdel.markOrderWithDE(order_id2);
				Thread.sleep(5000);
				rtshelp.runassignment(RTSConstants.cityid);
				Object batch_id5 = delmeth.dbhelperget(query1, "batch_id");
				Object batch_id6 = delmeth.dbhelperget(query2, "batch_id");
				Assert.assertNotEquals(batch_id5, batch_id6);
				}
				}
		else {
			System.out.println("Batch id not present in Batch table");
			Assert.assertTrue(false);
		}
	}
	@Test(dataProvider = "QE-287",priority=25, groups = "MultiRestaurantBatching", description = "Verify whether orders from two restaurants are not getting merged if only single restaurant Batching is enabled for a zone")
	public void MultiRestaurantBatching287(String order_id1, String order_id2)
			throws InterruptedException {
		Assert.assertNotNull(order_id1,
				"Order is not created through pushing Order Json");
		Assert.assertNotNull(order_id2,
				"Order is not created through pushing Order Json");
		String query1 = DeliveryConstant.batch_id + order_id1 + ";";
		String query2 = DeliveryConstant.batch_id + order_id2 + ";";
		Object batch_id1 = delmeth.dbhelperget(query1, "batch_id");
		Object batch_id2 = delmeth.dbhelperget(query2, "batch_id");
		rtshelp.runassignment(RTSConstants.cityid);
		 if (batch_id1 != null && batch_id2!= null ) {
			 Object batch_id3 = delmeth.dbhelperget(query1, "batch_id");
				Object batch_id4 = delmeth.dbhelperget(query2, "batch_id");
				Assert.assertNotEquals(batch_id3, batch_id4);
				}
		else {
			System.out.println("Batch id not present in Batch table");
			Assert.assertTrue(false);
		}
	}
	
	@Test(dataProvider = "QE-288", priority=7,groups = "MultiRestaurantBatching", description = "Verify whether two orders from two different restaurants under same zone are batched only if both of these orders are non with_de")
	public void MultiRestaurantBatching288(String order_id1, String order_id2)
			throws InterruptedException {
		Assert.assertNotNull(order_id1,
				"Order is not created through pushing Order Json");
		Assert.assertNotNull(order_id2,
				"Order is not created through pushing Order Json");
		String query1 = DeliveryConstant.batch_id + order_id1 + ";";
		String query2 = DeliveryConstant.batch_id + order_id2 + ";";
		Object batch_id1 = delmeth.dbhelperget(query1, "batch_id");
		Object batch_id2 = delmeth.dbhelperget(query2, "batch_id");
		rtshelp.runassignment(RTSConstants.cityid);
		 if (batch_id1 != null && batch_id2!= null ) {
			 Object batch_id3 = delmeth.dbhelperget(query1, "batch_id");
				Object batch_id4 = delmeth.dbhelperget(query2, "batch_id");
				Assert.assertEquals(batch_id3, batch_id4);
				}
		else {
			System.out.println("Batch id not present in Batch table");
			Assert.assertTrue(false);
		}
	}


	@Test(dataProvider = "QE-289",priority=8, groups = "MultiRestaurantBatching", description = "Verify whether orders from two restaurants are not batched if one of the restaurant is not enabled for MultiRestaurantBatching,R2R distance is within threshold limit mentioned in config and both restaurant falls under same zone")
	public void MultiRestaurantBatching289(String order_id1, String order_id2)
			throws InterruptedException {
		Assert.assertNotNull(order_id1,
				"Order is not created through pushing Order Json");
		Assert.assertNotNull(order_id2,
				"Order is not created through pushing Order Json");
		String query1 = DeliveryConstant.batch_id + order_id1 + ";";
		String query2 = DeliveryConstant.batch_id + order_id2 + ";";
		Object batch_id1 = delmeth.dbhelperget(query1, "batch_id");
		Object batch_id2 = delmeth.dbhelperget(query2, "batch_id");
		rtshelp.runassignment(RTSConstants.cityid);
		 if (batch_id1 != null && batch_id2!= null ) {
			 	 Object batch_id3 = delmeth.dbhelperget(query1, "batch_id");
				Object batch_id4 = delmeth.dbhelperget(query2, "batch_id");
				helpdel.updateDeliveryRestaurantParams(
						DeliveryConstant.restmultiid,
						DeliveryConstant.batchingenabled_true);
				Assert.assertNotEquals(batch_id3, batch_id4);
				}
		else {
			System.out.println("Batch id not present in Batch table");
			
			Assert.assertTrue(false);
		}

	}

	@Test(dataProvider = "QE-290",priority=28, groups = "MultiRestaurantBatching", description = "Verify whether orders from two restaurants are not batched if both of the restaurant are enabled for MultiRestaurantBatching,R2R distance is not within threshold limit mentioned in config and both restaurant falls under same zone")
	public void MultiRestaurantBatching290(String order_id1, String order_id2)
			throws InterruptedException {
		Assert.assertNotNull(order_id1,
				"Order is not created through pushing Order Json");
		Assert.assertNotNull(order_id2,
				"Order is not created through pushing Order Json");
		String query1 = DeliveryConstant.batch_id + order_id1 + ";";
		String query2 = DeliveryConstant.batch_id + order_id2 + ";";
		Object batch_id1 = delmeth.dbhelperget(query1, "batch_id");
		Object batch_id2 = delmeth.dbhelperget(query2, "batch_id");
		rtshelp.runassignment(RTSConstants.cityid);
		 if (batch_id1 != null && batch_id2!= null ) {
			 Object batch_id3 = delmeth.dbhelperget(query1, "batch_id");
				Object batch_id4 = delmeth.dbhelperget(query2, "batch_id");
				Assert.assertNotEquals(batch_id3, batch_id4);
				}
		else {
			System.out.println("Batch id not present in Batch table");
			Assert.assertTrue(false);
		}
	}
	@Test(dataProvider = "QE-291",priority=10, groups = "MultiRestaurantBatching", description = "Verify whether orders from two restaurants are not batched if both of the restaurant are enabled for MultiRestaurantBatching,R2R distance is within threshold limit mentioned in config and both restaurant does not falls under same zone")
	public void MultiRestaurantBatching291(String order_id1, String order_id2)
			throws InterruptedException {
		Thread.sleep(15000);
		Assert.assertNotNull(order_id1,
				"Order is not created through pushing Order Json");
		Assert.assertNotNull(order_id2,
				"Order is not created through pushing Order Json");
		String query1 = DeliveryConstant.batch_id + order_id1 + ";";
		String query2 = DeliveryConstant.batch_id + order_id2 + ";";
		Object batch_id1 = delmeth.dbhelperget(query1, "batch_id");
		Object batch_id2 = delmeth.dbhelperget(query2, "batch_id");
		rtshelp.runassignment(RTSConstants.cityid);
		 if (batch_id1 != null && batch_id2!= null ) {
			 Object batch_id3 = delmeth.dbhelperget(query1, "batch_id");
				Object batch_id4 = delmeth.dbhelperget(query2, "batch_id");
				Assert.assertNotEquals(batch_id3, batch_id4);
				}
		else {
			System.out.println("Batch id not present in Batch table");
			Assert.assertTrue(false);
		}
	}

	@Test(dataProvider = "QE-292",priority=11, groups = "MultiRestaurantBatching", description = "Verify that orders are not merged if bill value of orders together exceedes config values")
	public void MultiRestaurantBatching292(String order_id1, String order_id2)
			throws InterruptedException {
		Assert.assertNotNull(order_id1,
				"Order is not created through pushing Order Json");
		Assert.assertNotNull(order_id2,
				"Order is not created through pushing Order Json");
		String query1 = DeliveryConstant.batch_id + order_id1 + ";";
		String query2 = DeliveryConstant.batch_id + order_id2 + ";";
		Object batch_id1 = delmeth.dbhelperget(query1, "batch_id");
		Object batch_id2 = delmeth.dbhelperget(query2, "batch_id");
		rtshelp.runassignment(RTSConstants.cityid);
		 if (batch_id1 != null && batch_id2!= null ) {
			 Object batch_id3 = delmeth.dbhelperget(query1, "batch_id");
				Object batch_id4 = delmeth.dbhelperget(query2, "batch_id");
				Assert.assertNotEquals(batch_id3, batch_id4);
				}
		else {
			System.out.println("Batch id not present in Batch table");
			Assert.assertTrue(false);
		}
	}
	@Test(dataProvider = "QE-293",priority=12, groups = "MultiRestaurantBatching", description = "Verify whether orders from different restaurants are not batched if one of the orders is already marked pickedup")
	public void MultiRestaurantBatching293(String order_id1, String order_id2)
			throws InterruptedException {
		Assert.assertNotNull(order_id1,"Order is not created through checkout helper");
		Assert.assertNotNull(order_id2,"Order is not created through checkout helper");
		String query1 = DeliveryConstant.batch_id + order_id1 + ";";
		String query2 = DeliveryConstant.batch_id + order_id2 + ";";
		Object batch_id1 = delmeth.dbhelperget(query1, "batch_id");
		Object batch_id2 = delmeth.dbhelperget(query2, "batch_id");
		rtshelp.runassignment(RTSConstants.cityid);
		 if (batch_id1 != null && batch_id2!= null ) {
			 Object batch_id3 = delmeth.dbhelperget(query1, "batch_id");
				Object batch_id4 = delmeth.dbhelperget(query2, "batch_id");
				Assert.assertNotEquals(batch_id3, batch_id4);
				}
		else {
			System.out.println("Batch id not present in Batch table");
			Assert.assertTrue(false);
		}

	}

	@Test(dataProvider = "QE-294",priority=32, groups = "MultiRestaurantBatching", description = "Verify order is not getting merged with already exisiting batch consisting of two orders if Maximum Orders In Batch is configured as two at zone level")
	public void MultiRestaurantBatching294(String order_id1, String order_id2)
			throws Exception {
		Assert.assertNotNull(order_id1,
				"Order is not created through pushing Order Json");
		Assert.assertNotNull(order_id2,
				"Order is not created through pushing Order Json");
		String query1 = DeliveryConstant.batch_id + order_id1 + ";";
		String query2 = DeliveryConstant.batch_id + order_id2 + ";";
		Object batch_id1 = delmeth.dbhelperget(query1, "batch_id");
		Object batch_id2 = delmeth.dbhelperget(query2, "batch_id");
		rtshelp.runassignment(RTSConstants.cityid);
		 if (batch_id1 != null && batch_id2!= null ) {
			 Object batch_id3 = delmeth.dbhelperget(query1, "batch_id");
				Object batch_id4 = delmeth.dbhelperget(query2, "batch_id");
				if (((batch_id3.toString()).equalsIgnoreCase(batch_id4.toString())))
				 {  
					Map<String, String> defMap= new HashMap<>();
					defMap.put("restaurant_id",DeliveryConstant.multi_rest_batching_rest1);
					defMap.put("restaurant_lat_lng",DeliveryConstant.multi_rest_batching_rest1_lat_long);
					defMap.put("restaurant_customer_distance", DeliveryConstant.multi_rest_last_mile);
					defMap.put("restaurant_area_code", DeliveryConstant.multi_rest_area);
					String order_time=deldata.getcurrentDateTimefororderTime();
					defMap.put("order_time",order_time);
					String orderJson1=deldata.returnOrderJson(defMap);
					boolean order_status1=deldata.pushOrderJsonToRMQAndValidate(orderJson1);
					String order_id3=null;
					if(order_status1)
					{
						order_id3=(new JSONObject(orderJson1)).get("order_id").toString();
					}
					rtshelp.runassignment(RTSConstants.cityid);
			 String query3 = DeliveryConstant.batch_id + order_id3 + ";";
				Object batch_id5 = delmeth.dbhelperget(query3, "batch_id");
				if (((batch_id3.toString()).equalsIgnoreCase(batch_id5.toString())) || ((batch_id4.toString()).equalsIgnoreCase(batch_id5.toString())) )
				 { 
					Assert.assertTrue(false);
				}
			 else {

				Assert.assertTrue(true);
			}
		}
		 }		 
		else {
			System.out.println("Batch id not present in Batch table");
			Assert.assertTrue(false);
		}
	}
	@Test(dataProvider = "QE-295",priority=33, groups = "MultiRestaurantBatching", description = "Verify orders are not getting merged if SLA exceeds max sla of batch configured at zone level")
	public void MultiRestaurantBatching295(String order_id1, String order_id2)
			throws InterruptedException {
		Assert.assertNotNull(order_id1,
				"Order is not created through pushing Order Json");
		Assert.assertNotNull(order_id2,
				"Order is not created through pushing Order Json");
		String query1 = DeliveryConstant.batch_id + order_id1 + ";";
		String query2 = DeliveryConstant.batch_id + order_id2 + ";";
		Object batch_id1 = delmeth.dbhelperget(query1, "batch_id");
		Object batch_id2 = delmeth.dbhelperget(query2, "batch_id");
		rtshelp.runassignment(RTSConstants.cityid);
		 if (batch_id1 != null && batch_id2!= null ) {
			 Object batch_id3 = delmeth.dbhelperget(query1, "batch_id");
				Object batch_id4 = delmeth.dbhelperget(query2, "batch_id");
				Assert.assertNotEquals(batch_id3,batch_id4);
					}
		else {
			System.out.println("Batch id not present in Batch table");
			Assert.assertTrue(false);
		}

	}
	
	@Test(dataProvider = "QE-296",priority=34, groups = "MultiRestaurantBatching", description = "Verify orders are not getting merged if number of items exceeds max item in Batch confgiured at zone level")
	public void MultiRestaurantBatching296(String order_id1, String order_id2)
			throws InterruptedException {
		Assert.assertNotNull(order_id1,
				"Order is not created through pushing Order Json");
		Assert.assertNotNull(order_id2,
				"Order is not created through pushing Order Json");
		String query1 = DeliveryConstant.batch_id + order_id1 + ";";
		String query2 = DeliveryConstant.batch_id + order_id2 + ";";
		Object batch_id1 = delmeth.dbhelperget(query1, "batch_id");
		Object batch_id2 = delmeth.dbhelperget(query2, "batch_id");
		rtshelp.runassignment(RTSConstants.cityid);
		if (batch_id1 != null && batch_id2!= null ) {
			 Object batch_id3 = delmeth.dbhelperget(query1, "batch_id");
				Object batch_id4 = delmeth.dbhelperget(query2, "batch_id");
				Assert.assertNotEquals(batch_id3,batch_id4);
				}
		else {
			System.out.println("Batch id not present in Batch table");
			Assert.assertTrue(false);
		}

	}
	@Test(dataProvider = "QE-297",priority=35, groups = "MultiRestaurantBatching", description = "Verify orders are not getting merged if distance between customers exceeds max customer distance in batch V2 configured at zone level")
	public void MultiRestaurantBatching297(String order_id1, String order_id2)
			throws InterruptedException {
		Assert.assertNotNull(order_id1,
				"Order is not created through pushing Order Json");
		Assert.assertNotNull(order_id2,
				"Order is not created through pushing Order Json");
		String query1 = DeliveryConstant.batch_id + order_id1 + ";";
		String query2 = DeliveryConstant.batch_id + order_id2 + ";";
		Object batch_id1 = delmeth.dbhelperget(query1, "batch_id");
		Object batch_id2 = delmeth.dbhelperget(query2, "batch_id");
		rtshelp.runassignment(RTSConstants.cityid);
		if (batch_id1 != null && batch_id2!= null ) {
			 Object batch_id3 = delmeth.dbhelperget(query1, "batch_id");
				Object batch_id4 = delmeth.dbhelperget(query2, "batch_id");
				Assert.assertNotEquals(batch_id3,batch_id4);
				}
		else {
			System.out.println("Batch id not present in Batch table");
			Assert.assertTrue(false);
		}

	}
	@Test(dataProvider = "QE-298",priority=36, groups = "MultiRestaurantBatching", description = "Verify orders are not getting merged if max order difference between orders exceeds max orders diff In batch V2 configured at zone level")
	public void MultiRestaurantBatching298(String order_id1, String order_id2)
			throws InterruptedException {
		Assert.assertNotNull(order_id1,
				"Order is not created through pushing Order Json");
		Assert.assertNotNull(order_id2,
				"Order is not created through pushing Order Json");
		String query1 = DeliveryConstant.batch_id + order_id1 + ";";
		String query2 = DeliveryConstant.batch_id + order_id2 + ";";
		Object batch_id1 = delmeth.dbhelperget(query1, "batch_id");
		Object batch_id2 = delmeth.dbhelperget(query2, "batch_id");
		rtshelp.runassignment(RTSConstants.cityid);
		if (batch_id1 != null && batch_id2!= null ) {
			 Object batch_id3 = delmeth.dbhelperget(query1, "batch_id");
				Object batch_id4 = delmeth.dbhelperget(query2, "batch_id");
				Assert.assertNotEquals(batch_id3,batch_id4);
		}
		else {
			System.out.println("Batch id not present in Batch table");
			Assert.assertTrue(false);
		}

	}
	public void addRedisKeyForRestDistance(String rest1,String rest2)
	{
		delmeth.redisconnectsetrestid(rest1, 3, DeliveryConstant.rest1_multirest_redis_key,DeliveryConstant.deliveryRedisName);
		delmeth.redisconnectsetrestid(rest2, 3, DeliveryConstant.rest2_multirest_redis_key,DeliveryConstant.deliveryRedisName);
	}
	
}

