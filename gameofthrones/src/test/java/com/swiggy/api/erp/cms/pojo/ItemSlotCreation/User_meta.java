
package com.swiggy.api.erp.cms.pojo.ItemSlotCreation;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "source",
    "meta"
})
public class User_meta {

    @JsonProperty("source")
    private String source;
    @JsonProperty("meta")
    private Meta_ meta;


    @JsonProperty("source")
    public String getSource() {
        return source;
    }

    @JsonProperty("source")
    public void setSource(String source) {
        this.source = source;
    }

    @JsonProperty("meta")
    public Meta_ getMeta() {
        return meta;
    }

    @JsonProperty("meta")
    public void setMeta(Meta_ meta) {
        this.meta = meta;
    }

    public User_meta build() {
        setDefaultValues();
        return this;
    }

    public void setDefaultValues(){
        Meta_ metaInUserMeta = new Meta_();
        metaInUserMeta.build();
        this.setMeta(metaInUserMeta);

        if(this.source==null){
            this.source = "CMS-UserMeta";
        }

    }

}
