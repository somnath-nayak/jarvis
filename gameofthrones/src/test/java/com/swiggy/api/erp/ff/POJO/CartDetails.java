package com.swiggy.api.erp.ff.POJO;

import org.codehaus.jackson.annotate.JsonProperty;

public class CartDetails {

    @JsonProperty("restaurant_id")
    private String restaurantId;
    @JsonProperty("bill_amount")
    private Integer billAmount;
    @JsonProperty("item_quantity")
    private Integer itemQuantity;
    @JsonProperty("item_count")
    private Integer itemCount;

    /**
     * No args constructor for use in serialization
     *
     */
    public CartDetails() {
    }

    /**
     *
     * @param itemQuantity
     * @param billAmount
     * @param itemCount
     * @param restaurantId
     */
    public CartDetails(String restaurantId, Integer billAmount, Integer itemQuantity, Integer itemCount) {
        super();
        this.restaurantId = restaurantId;
        this.billAmount = billAmount;
        this.itemQuantity = itemQuantity;
        this.itemCount = itemCount;
    }

    @JsonProperty("restaurant_id")
    public String getRestaurantId() {
        return restaurantId;
    }

    @JsonProperty("restaurant_id")
    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    @JsonProperty("bill_amount")
    public Integer getBillAmount() {
        return billAmount;
    }

    @JsonProperty("bill_amount")
    public void setBillAmount(Integer billAmount) {
        this.billAmount = billAmount;
    }

    @JsonProperty("item_quantity")
    public Integer getItemQuantity() {
        return itemQuantity;
    }

    @JsonProperty("item_quantity")
    public void setItemQuantity(Integer itemQuantity) {
        this.itemQuantity = itemQuantity;
    }

    @JsonProperty("item_count")
    public Integer getItemCount() {
        return itemCount;
    }

    @JsonProperty("item_count")
    public void setItemCount(Integer itemCount) {
        this.itemCount = itemCount;
    }

}