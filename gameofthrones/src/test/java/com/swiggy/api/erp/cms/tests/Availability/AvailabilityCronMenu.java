package com.swiggy.api.erp.cms.tests.Availability;

import com.swiggy.api.erp.cms.helper.CMSHelper;
import org.testng.annotations.Test;

public class AvailabilityCronMenu {
    CMSHelper cmsHelper=new CMSHelper();
    @Test(description = "sends menu availability events to snd kafka")
    public void menuAvailability(){
        cmsHelper.availabilityMenu();
    }
}
