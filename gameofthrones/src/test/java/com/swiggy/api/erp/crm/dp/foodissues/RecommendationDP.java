package com.swiggy.api.erp.crm.dp.foodissues;

import com.swiggy.api.erp.crm.constants.CRMConstants;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceHelper;
import com.swiggy.api.erp.ff.helper.OMSHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import framework.gameofthrones.Tyrion.RedisHelper;
import org.testng.annotations.DataProvider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

public class RecommendationDP {

    public static Initialize gameofthrones = new Initialize();
    public static CheckoutHelper checkoutHelper = new CheckoutHelper();
    public static RedisHelper redisHelper = new RedisHelper();
    public static OMSHelper omsHelper = new OMSHelper();
    public static JsonHelper jsonHelper = new JsonHelper();
    public static DeliveryServiceHelper deliveryServiceHelper = new DeliveryServiceHelper();
    public static FoodIssuesRules foodIssuesRules = new FoodIssuesRules();
    
    public static Collection<String[]> rules = null;
    
    public static ArrayList<ArrayList<Object[]>> listOLists = new ArrayList<ArrayList<Object[]>>();
    public static ArrayList<Object[]> issueList = new ArrayList<Object[]>();

    public static Processor processor = null;

    public static String deliveredorderId = CRMConstants.regularPrePaidOrder;
    public static String userId = CRMConstants.userID;

    public static String fullOrder = null, entire = null, segment = null, fraud = null, issuetype = null, ordertype = null;
    public static int ruleSize;
    

    public static void getRules() throws Exception
    {
    	Object key =  "igccRules";
     	rules = FoodIssuesRules.setigccRules().get((String) key);
     	ruleSize = rules.size();
     	System.out.println("\n rule size \t" +ruleSize);
    }
    
    public static String entireItem(String entire) throws Exception
    {
     	if (entire == "Entire Item")
     		fullOrder = "true";
     	else 
     		fullOrder = "false";
     	
     	return fullOrder;
    }


    public static void fetchIssueDetails(String orderType) throws Exception
    {	
    	getRules();
    	
     	for (int i=0; i<ruleSize; i++)
     	{	
     	String[] ruleList = (String[]) rules.toArray()[i];
		issuetype = ruleList[0]; 
		entire = ruleList[1]; 
		segment = ruleList[2]; 
		fraud = ruleList[3]; 
        ordertype=ruleList[4];
        String action = ruleList[6];
        String refund = ruleList[7];
        String coupon = ruleList[8];

        if (ordertype.equalsIgnoreCase(orderType) && segment !="Segment")
		{
			entireItem(entire);

     	if (issuetype.equalsIgnoreCase("Missing Item"))
     	{
     		issuetype = "MISSING_ITEMS";
     	}
     	else if (issuetype.equalsIgnoreCase("Wrong Item"))
     	{
     		issuetype = "WRONG_ITEMS";
     	}
     	else if (issuetype.equalsIgnoreCase("Quantity"))
     	{
     		issuetype = "QUANTITY_ISSUES";
     	}
     	else if (issuetype.equalsIgnoreCase("Packaging"))
		{
			issuetype = "PACKAGING_ISSUES";
		}
		else if (issuetype.equalsIgnoreCase("Quality"))
		{
			issuetype = "QUALITY_ISSUES";
		} else if (issuetype.equalsIgnoreCase("Spl. Instruction Not Followed"))
		{
			issuetype = "SPECIAL_INSTRUCTION";
		}

		HashMap<String,String> issueData = new HashMap<>();

		issueData.put("issueType", issuetype);
        issueData.put("fullOrder", fullOrder);
        issueData.put("fullItem","true");
        issueData.put("userId", userId);
        issueData.put("segment", segment);
        issueData.put("fraud", fraud);
        issueData.put("orderType",ordertype);
        
        String[] expected = action.split(",");
        int expectedSize = expected.length;
        
        String length = Integer.toString(expectedSize);
        
        issueData.put("length", length);
        
        System.out.println("\n");
        for (int k=0; k<expectedSize; k++)
        {
        	String value = expected[k];
        	issueData.put("action["+k+"]", value);
        }
       		
        issueData.put("refund", refund);
        issueData.put("coupon", coupon);
        
		issueList.add(new Object[]{issueData});
		}
		}
    }

	@DataProvider(name = "regularPostPaidOrder")
    public static Iterator<Object[]> getPostPaidOrder() throws Exception {
		String orderType="Regular";

    	fetchIssueDetails(orderType);
    	return issueList.iterator();
    }

	@DataProvider(name = "regularPrePaidOrder")
	public static Iterator<Object[]> getPrePaidOrder() throws Exception {
		String orderType="Regular";

		fetchIssueDetails(orderType);
		return issueList.iterator();
	}

	@DataProvider(name = "regularPostPaidOrderUnserviceable")
	public static Iterator<Object[]> getPostPaidOrderUnserviceable() throws Exception {
		String orderType="Regular";

		fetchIssueDetails(orderType);
		return issueList.iterator();
	}

	@DataProvider(name = "regularPrePaidOrderUnServiceable")
	public static Iterator<Object[]> getPrePaidOrderUnServiceable() throws Exception {
		String orderType="Regular";

		fetchIssueDetails(orderType);
		return issueList.iterator();
	}

	@DataProvider(name = "regularPostPaidItemUnServiceable")
	public static Iterator<Object[]> getPostPaidItemUnServiceable() throws Exception {
		String orderType="Regular";

		fetchIssueDetails(orderType);
		return issueList.iterator();
	}

}
