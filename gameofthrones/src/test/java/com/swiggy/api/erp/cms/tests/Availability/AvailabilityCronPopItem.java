package com.swiggy.api.erp.cms.tests.Availability;

import com.swiggy.api.erp.cms.helper.CMSHelper;
import org.testng.annotations.Test;

public class AvailabilityCronPopItem {
    CMSHelper cmsHelper=new CMSHelper();
    @Test(description = "sends pop item availability events to snd kafka")
    public void popItemAvailability(){
        cmsHelper.availabilityPopItem();
    }
}
