package com.swiggy.api.erp.cms.tests;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.constants.Constants;
import com.swiggy.api.erp.cms.dp.TimeSlotDP;
import com.swiggy.api.erp.cms.helper.TimeSlotsHelper;
import com.swiggy.api.erp.cms.pojo.ItemSlot;
import com.swiggy.api.erp.cms.pojo.ItemSlotBuilder;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Cersei.ToolBox;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import framework.gameofthrones.Tyrion.JsonHelper;
import io.gatling.core.json.Json;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.*;

public class TimeSlotsTest extends TimeSlotDP {
    Initialize gameofthrones = new Initialize();
    TimeSlotsHelper timeSlotsHelper= new TimeSlotsHelper();
    SchemaValidatorUtils schemaValidatorUtils=new SchemaValidatorUtils();

    @Test(dataProvider = "addTimeSlot")
    public void createCurrentTimeSlot(String restId, String open, String close, String day)
    {
        String response= timeSlotsHelper.postTimeSlots(restId,open,close,day).ResponseValidator.GetBodyAsText();
        //Assert.assertEquals(Constants.statusCode, timeSlotsHelper.postTimeSlots(restId,open,close,day).ResponseValidator.GetResponseCode());
        String code = JsonPath.read(response, "$.code").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.code, code);
        String Day = JsonPath.read(response, "$.data.day").toString().replace("[","").replace("]","");
        Assert.assertEquals(day, Day);
        String id = JsonPath.read(response, "$.data.id").toString().replace("[","").replace("]","");
        String getTimeSlot=timeSlotsHelper.getTimeSlots(restId).ResponseValidator.GetBodyAsText();
        String getIds=JsonPath.read(getTimeSlot, "$.data..id").toString().replace("[","").replace("]","").replace("{","").replace("}","");
        Assert.assertTrue(getIds.contains(id));
    }

    @Test(dataProvider = "addTimeSlotNight")
    public void createTimeSlotNight(String restId, String open, String close, String day)
    {
        String response= timeSlotsHelper.postTimeSlots(restId,open,close,day).ResponseValidator.GetBodyAsText();
        //Assert.assertEquals(Constants.notAcceptable, timeSlotsHelper.postTimeSlots(restId,open,close,day).ResponseValidator.GetResponseCode());
        String code = JsonPath.read(response, "$.code").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.FailureCode, code);
        String message = JsonPath.read(response, "$.message").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.FailureMessage, message);
        String invalid = JsonPath.read(response, "$.data..value").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(Constants.InvalidCloseTime, invalid);
    }

    @Test(dataProvider = "updateTimeSlot")
    public void updateCurrentTimeSlot(String restId, String open, String close, String day, String updateTime)
    {
        String response= timeSlotsHelper.postTimeSlots(restId,open,close,day).ResponseValidator.GetBodyAsText();
        //Assert.assertEquals(Constants.statusCode, response.);
        String code = JsonPath.read(response, "$.code").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.code, code);
        String Day = JsonPath.read(response, "$.data.day").toString().replace("[","").replace("]","");
        Assert.assertEquals(day, Day);
        String id = JsonPath.read(response, "$.data.id").toString().replace("[","").replace("]","");
        String getTimeSlot=timeSlotsHelper.getTimeSlots(restId).ResponseValidator.GetBodyAsText();
        String getIds=JsonPath.read(getTimeSlot, "$.data..id").toString().replace("[","").replace("]","").replace("{","").replace("}","");
        Assert.assertTrue(getIds.contains(id));
        String putResponse= timeSlotsHelper.putTimeSlots(restId,open,updateTime,day,id).ResponseValidator.GetBodyAsText();
        //Assert.assertEquals(Constants.statusCode, timeSlotsHelper.putTimeSlots(restId,open,updateTime,day,id).ResponseValidator.GetResponseCode());
        String putCode = JsonPath.read(putResponse, "$.code").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.code, putCode);
        String putDay = JsonPath.read(response, "$.data.day").toString().replace("[","").replace("]","");
        Assert.assertEquals(day, putDay);
    }

    @Test(dataProvider = "updateDaySlot")
    public void updateFutureTimeSlot(String restId, String open, String close, String day, String updateTime)
    {
        String response= timeSlotsHelper.postTimeSlots(restId,open,close,day).ResponseValidator.GetBodyAsText();
        String code = JsonPath.read(response, "$.code").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.code, code);
        String Day = JsonPath.read(response, "$.data.day").toString().replace("[","").replace("]","");
        Assert.assertEquals(day, Day);
        String id = JsonPath.read(response, "$.data.id").toString().replace("[","").replace("]","");
        String getTimeSlot=timeSlotsHelper.getTimeSlots(restId).ResponseValidator.GetBodyAsText();
        String getIds=JsonPath.read(getTimeSlot, "$.data..id").toString().replace("[","").replace("]","").replace("{","").replace("}","");
        Assert.assertTrue(getIds.contains(id));
        String putResponse= timeSlotsHelper.putTimeSlots(restId,open,updateTime,day,id).ResponseValidator.GetBodyAsText();
        String putCode = JsonPath.read(putResponse, "$.code").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.FailureCode, putCode);
        String putDay = JsonPath.read(response, "$.data.day").toString().replace("[","").replace("]","");
        Assert.assertEquals(day, putDay);
    }

    @Test(dataProvider = "addTimeSlot")
    public void deleteTimeSlot(String restId, String open, String close, String day) {
        String response = timeSlotsHelper.postTimeSlots(restId, open, close, day).ResponseValidator.GetBodyAsText();
        //Assert.assertEquals(Constants.statusCode, timeSlotsHelper.postTimeSlots(restId,open,close,day).ResponseValidator.GetResponseCode());
        String code = JsonPath.read(response, "$.code").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(Constants.code, code);
        String Day = JsonPath.read(response, "$.data.day").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(day, Day);
        String id = JsonPath.read(response, "$.data.id").toString().replace("[", "").replace("]", "");
        String getTimeSlot = timeSlotsHelper.getTimeSlots(restId).ResponseValidator.GetBodyAsText();
        String getIds = JsonPath.read(getTimeSlot, "$.data..id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "");
        Assert.assertTrue(getIds.contains(id));
        String deleteTimeSlot= timeSlotsHelper.delTimeSlots(id).ResponseValidator.GetBodyAsText();
        String deleteSuccess= JsonPath.read(deleteTimeSlot,"$.code").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.code,deleteSuccess,"Slot is not deleted");
        String getTimeSlot1 = timeSlotsHelper.getTimeSlots(restId).ResponseValidator.GetBodyAsText();
        String getIds1 = JsonPath.read(getTimeSlot1, "$.data..id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "");
        Assert.assertFalse(getIds1.contains(id));
    }

    @Test(dataProvider = "deleteSlot")
    public void deleteAllTimeSlot(String restId) {
        String getTimeSlot = timeSlotsHelper.getTimeSlots(restId).ResponseValidator.GetBodyAsText();
        String getIds = JsonPath.read(getTimeSlot, "$.data..id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "");
        System.out.println(getIds);
        List<String> list= Arrays.asList(getIds.split(","));
        for(int i=0; i<list.size(); i++){
            String deleteTimeSlot= timeSlotsHelper.delTimeSlots(String.valueOf(list.get(i))).ResponseValidator.GetBodyAsText();
            String deleteSuccess= JsonPath.read(deleteTimeSlot,"$.code").toString().replace("[","").replace("]","");
            Assert.assertEquals(Constants.code,deleteSuccess,"Slot is not deleted");
        }
    }

    @Test(dataProvider = "addHolidaySlot")
    public void addHolidaySlots(String payload)
    {
        String response= timeSlotsHelper.postHolidayTimeSlots(payload).ResponseValidator.GetBodyAsText();
        String code=JsonPath.read(response,"$.code").toString().replace("[","").replace("]","");
        Assert.assertEquals(code,Constants.code);
        String restId=JsonPath.read(response,"$.data.restaurant_id").toString().replace("[","").replace("]","");
        String id=JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        String getHolidaySlot= timeSlotsHelper.getHolidayTimeSlots(restId).ResponseValidator.GetBodyAsText();
        String getId= JsonPath.read(getHolidaySlot,"$.data..id").toString().replace("[","").replace("]","");
        Assert.assertTrue(getId.contains(id));
    }

    @Test(dataProvider = "updateHolidaySlot")
    public void updateHolidaySlots(String payload, String payload1)
    {
        String response= timeSlotsHelper.postHolidayTimeSlots(payload).ResponseValidator.GetBodyAsText();
        String code=JsonPath.read(response,"$.code").toString().replace("[","").replace("]","");
        Assert.assertEquals(code,Constants.code);
        String restId=JsonPath.read(response,"$.data.restaurant_id").toString().replace("[","").replace("]","");
        String id=JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        String getHolidaySlot= timeSlotsHelper.getHolidayTimeSlots(restId).ResponseValidator.GetBodyAsText();
        String getId= JsonPath.read(getHolidaySlot,"$.data..id").toString().replace("[","").replace("]","");
        Assert.assertTrue(getId.contains(id));
        String putResponse= timeSlotsHelper.putHolidayTimeSlots(payload1,id).ResponseValidator.GetBodyAsText();
        String putCode=JsonPath.read(putResponse,"$.code").toString().replace("[","").replace("]","");
        Assert.assertEquals(putCode,Constants.code);
        String putId=JsonPath.read(putResponse,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertEquals(id,putId,"ID's are not same");
    }


    @Test(dataProvider = "addHolidaySlot")
    public void delHolidaySlots(String payload)
    {
        String response= timeSlotsHelper.postHolidayTimeSlots(payload).ResponseValidator.GetBodyAsText();
        String code=JsonPath.read(response,"$.code").toString().replace("[","").replace("]","");
        Assert.assertEquals(code,Constants.code);
        String restId=JsonPath.read(response,"$.data.restaurant_id").toString().replace("[","").replace("]","");
        String id=JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        String getHolidaySlot= timeSlotsHelper.getHolidayTimeSlots(restId).ResponseValidator.GetBodyAsText();
        String getId= JsonPath.read(getHolidaySlot,"$.data..id").toString().replace("[","").replace("]","");
        Assert.assertTrue(getId.contains(id));
        String delResponse= timeSlotsHelper.delHolidayTimeSlots(id).ResponseValidator.GetBodyAsText();
        String delCode=JsonPath.read(delResponse,"$.code").toString().replace("[","").replace("]","");
        Assert.assertEquals(delCode,Constants.code);
    }

    @Test(dataProvider = "deleteHolidaySlot")
    public void delAllHolidaySlots(String restId)
    {
        String getHolidaySlot= timeSlotsHelper.getHolidayTimeSlots(restId).ResponseValidator.GetBodyAsText();
        String getId= JsonPath.read(getHolidaySlot,"$.data..id").toString().replace("[","").replace("]","");
        List<String> list= Arrays.asList(getId.split(","));
        for(int i=0; i<list.size(); i++){
            String delResponse= timeSlotsHelper.delHolidayTimeSlots(String.valueOf(list.get(i))).ResponseValidator.GetBodyAsText();
            String delCode=JsonPath.read(delResponse,"$.code").toString().replace("[","").replace("]","");
            Assert.assertEquals(delCode,Constants.code);
        }
    }

   /* @Test(dataProvider = "addTimeSlot")
    public void availabilityTimeSlot(String restId, String open, String close, String day)
    {
        String response= timeSlotsHelper.postTimeSlots(restId,open,close,day).ResponseValidator.GetBodyAsText();
        //Assert.assertEquals(Constants.statusCode, timeSlotsHelper.postTimeSlots(restId,open,close,day).ResponseValidator.GetResponseCode());
        String code = JsonPath.read(response, "$.code").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.code, code);
        String Day = JsonPath.read(response, "$.data.day").toString().replace("[","").replace("]","");
        Assert.assertEquals(day, Day);
        String id = JsonPath.read(response, "$.data.id").toString().replace("[","").replace("]","");
        String getTimeSlot=timeSlotsHelper.getTimeSlots(restId).ResponseValidator.GetBodyAsText();
        String getIds=JsonPath.read(getTimeSlot, "$.data..id").toString().replace("[","").replace("]","").replace("{","").replace("}","");
        Assert.assertTrue(getIds.contains(id));
        String availablityResponse= timeSlotsHelper.getAvailability(restId).ResponseValidator.GetBodyAsText();
        String code1 = JsonPath.read(availablityResponse, "$.code").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.code, code1);
        String openFalse = JsonPath.read(availablityResponse, "$.data.is_open").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.isOpenFalse, openFalse);
    }

    @Test(dataProvider = "currentTimeSlot")
    public void availabilityCurrentTimeSlot(String restId, String open, String close, String day)
    {
        String response= timeSlotsHelper.postTimeSlots(restId,open,close,day).ResponseValidator.GetBodyAsText();
        //Assert.assertEquals(Constants.statusCode, timeSlotsHelper.postTimeSlots(restId,open,close,day).ResponseValidator.GetResponseCode());
        String code = JsonPath.read(response, "$.code").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.code, code);
        String Day = JsonPath.read(response, "$.data.day").toString().replace("[","").replace("]","");
        Assert.assertEquals(day, Day);
        String id = JsonPath.read(response, "$.data.id").toString().replace("[","").replace("]","");
        String getTimeSlot=timeSlotsHelper.getTimeSlots(restId).ResponseValidator.GetBodyAsText();
        String getIds=JsonPath.read(getTimeSlot, "$.data..id").toString().replace("[","").replace("]","").replace("{","").replace("}","");
        Assert.assertTrue(getIds.contains(id));
        String availablityResponse= timeSlotsHelper.getAvailability(restId).ResponseValidator.GetBodyAsText();
        String code1 = JsonPath.read(availablityResponse, "$.code").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.code, code1);
        String openTrue = JsonPath.read(availablityResponse, "$.data.is_open").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.isOpenTrue, openTrue, "Restaurant is not open on current time");
    }

    @Test(dataProvider = "addAHolidaySlot")
    public void availabilityHolidaySlot(String payload)
    {
        String response= timeSlotsHelper.postHolidayTimeSlots(payload).ResponseValidator.GetBodyAsText();
        String code=JsonPath.read(response,"$.code").toString().replace("[","").replace("]","");
        Assert.assertEquals(code,Constants.code);
        String restId=JsonPath.read(response,"$.data.restaurant_id").toString().replace("[","").replace("]","");
        String id=JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        String getHolidaySlot= timeSlotsHelper.getHolidayTimeSlots(restId).ResponseValidator.GetBodyAsText();
        String getId= JsonPath.read(getHolidaySlot,"$.data..id").toString().replace("[","").replace("]","");
        Assert.assertTrue(getId.contains(id));
        String availablityResponse= timeSlotsHelper.getAvailability(restId).ResponseValidator.GetBodyAsText();
        String code1 = JsonPath.read(availablityResponse, "$.code").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.code, code1);
        String openFalse = JsonPath.read(availablityResponse, "$.data").toString().replace("[","").replace("]","");
        Assert.assertEquals(null, openFalse, "Restaurant is open on current time");
    }*/

    @Test(dataProvider = "addItemSlot")
    public void addItemSlot(String item_id, String dayOfWeek,String open_time, String close_time )
    {
        String response= timeSlotsHelper.postItemSlots(item_id, dayOfWeek,open_time, close_time).ResponseValidator.GetBodyAsText();
        String code=JsonPath.read(response,"$.code").toString().replace("[","").replace("]","");
        Assert.assertEquals(code,Constants.code);
        String restId=JsonPath.read(response,"$.data.restaurant_id").toString().replace("[","").replace("]","");
        String id=JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        String getItemSlot= timeSlotsHelper.getItemSlots(restId).ResponseValidator.GetBodyAsText();
        String getId= JsonPath.read(getItemSlot,"$.data..id").toString().replace("[","").replace("]","");
        Assert.assertTrue(getId.contains(id));
    }

    @Test(dataProvider = "addItemSlot")
    public void deleteItemSlot(String item_id, String dayOfWeek,String open_time, String close_time) throws IOException
    {
        String response= timeSlotsHelper.postItemSlots(item_id, dayOfWeek,open_time, close_time).ResponseValidator.GetBodyAsText();
        /*String code=JsonPath.read(response,"$.code").toString().replace("[","").replace("]","");
        Assert.assertEquals(code,Constants.code);
        String restId=JsonPath.read(response,"$.data.restaurant_id").toString().replace("[","").replace("]","");
        */String id=JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        //String getItemSlot= timeSlotsHelper.getItemSlots(restId).ResponseValidator.GetBodyAsText();
        //String getId= JsonPath.read(getItemSlot,"$.data..id").toString().replace("[","").replace("]","");
        //Assert.assertTrue(getId.contains(id));
        String delResp= timeSlotsHelper.delItemSlots(id).ResponseValidator.GetBodyAsText();
    }

    @Test(dataProvider = "updateItemSlot")
    public void updateItemSlot(String item_id, String dayOfWeek,String open_time, String close_time, String next_time) throws IOException
    {
        String response= timeSlotsHelper.postItemSlots(item_id, dayOfWeek,open_time, close_time).ResponseValidator.GetBodyAsText();
        /*String code=JsonPath.read(response,"$.code").toString().replace("[","").replace("]","");
        Assert.assertEquals(code,Constants.code);
        String restId=JsonPath.read(response,"$.data.restaurant_id").toString().replace("[","").replace("]","");
        */String id=JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        //String getItemSlot= timeSlotsHelper.getItemSlots(restId).ResponseValidator.GetBodyAsText();
        //String getId= JsonPath.read(getItemSlot,"$.data..id").toString().replace("[","").replace("]","");
        //Assert.assertTrue(getId.contains(id));
        String updateResp= timeSlotsHelper.putItemSlots(id, item_id,dayOfWeek,next_time,close_time).ResponseValidator.GetBodyAsText();
    }


    @Test(dataProvider = "addItemHoliSlot")
    public void addItemHolidaySlot(String itemId, String fromTime, String toTime)
    {
        String response= timeSlotsHelper.postItemHolidaySlots(itemId,fromTime,toTime).ResponseValidator.GetBodyAsText();
        String code=JsonPath.read(response,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(code,Constants.status);
        String itemIdResp=JsonPath.read(response,"$.data.item_id").toString().replace("[","").replace("]","");
        String id=JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertEquals(itemId,itemIdResp, "Item is not matching");
        String getItemSlot= timeSlotsHelper.getItemHolidaySlots(id).ResponseValidator.GetBodyAsText();
        String getId= JsonPath.read(getItemSlot,"$.data..id").toString().replace("[","").replace("]","");
        Assert.assertTrue(getId.contains(id));
    }

    @Test(dataProvider = "updateItemHoliSlot")
    public void updateItemHolidaySlot(String itemId, String fromTime, String toTime, String updateTime)
    {
        String response= timeSlotsHelper.postItemHolidaySlots(itemId,fromTime,toTime).ResponseValidator.GetBodyAsText();
        String code=JsonPath.read(response,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(code,Constants.status);
        String itemIdResp=JsonPath.read(response,"$.data.item_id").toString().replace("[","").replace("]","");
        String id=JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertEquals(itemId,itemIdResp, "Item is not matching");
        String getItemSlot= timeSlotsHelper.getItemHolidaySlots(id).ResponseValidator.GetBodyAsText();
        String getId= JsonPath.read(getItemSlot,"$.data..id").toString().replace("[","").replace("]","");
        Assert.assertTrue(getId.contains(id));
        String updateResponse= timeSlotsHelper.putItemHolidaySlots(id,itemId,updateTime,toTime).ResponseValidator.GetBodyAsText();
        String status= JsonPath.read(updateResponse, "$.statusCode").toString().replace("[","").replace("]","");
        String updateItemId= JsonPath.read(updateResponse, "$.data.item_id").toString().replace("[","").replace("]","");
        Assert.assertEquals(itemId,updateItemId, "status is not matching");
        Assert.assertEquals(Constants.status,status, "status is not 1");

    }

    @Test (dataProvider = "addTimeSlot", groups = { "Regression", "Smoke" }, description = "1.Get Tid, token from login, 2.Create a normal cart, 3.Verify Cart validation for the same cart")
    public void CreateTimeSlotSchemaValidation(String restId, String open, String close, String day) throws IOException, ProcessingException {
        String response= timeSlotsHelper.postTimeSlots(restId,open,close,day).ResponseValidator.GetBodyAsText();
        //Assert.assertEquals(Constants.statusCode, timeSlotsHelper.postTimeSlots(restId,open,close,day).ResponseValidator.GetResponseCode());
        String code = JsonPath.read(response, "$.code").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.code, code);
        String Day = JsonPath.read(response, "$.data.day").toString().replace("[","").replace("]","");
        Assert.assertEquals(day, Day);
        String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/SchemaSet/Json/CMS/createtimeslot.txt");
        System.out.println(jsonschema);
        List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,response);
        Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For create time slot API");
        boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(response);
        System.out.println("Contain empty nodes => " + isEmpty);
        Assert.assertEquals( false,isEmpty,"Found Empty Nodes in Response");
    }

    @Test (dataProvider = "addTimeSlot", groups = { "Regression", "Smoke" }, description = "1.Get Tid, token from login, 2.Create a normal cart, 3.Verify Cart validation for the same cart")
    public void getTimeSlotSchemaValidation(String restId, String open, String close, String day) throws IOException, ProcessingException {
        String response= timeSlotsHelper.postTimeSlots(restId,open,close,day).ResponseValidator.GetBodyAsText();
        //Assert.assertEquals(Constants.statusCode, timeSlotsHelper.postTimeSlots(restId,open,close,day).ResponseValidator.GetResponseCode());
        String code = JsonPath.read(response, "$.code").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.code, code);
        String Day = JsonPath.read(response, "$.data.day").toString().replace("[","").replace("]","");
        Assert.assertEquals(day, Day);
        String getTimeSlot=timeSlotsHelper.getTimeSlots(restId).ResponseValidator.GetBodyAsText();
        String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/SchemaSet/Json/CMS/gettimeslot.txt");
        System.out.println(jsonschema);
        List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,getTimeSlot);
        Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For get time slot API");
        boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(response);
        System.out.println("Contain empty nodes => " + isEmpty);
        Assert.assertEquals( false,isEmpty,"Found Empty Nodes in Response");
    }

    @Test(dataProvider = "updateTimeSlot")
    public void updateTimeSlotSchemaValidation(String restId, String open, String close, String day, String updateTime) throws IOException, ProcessingException {
        String response= timeSlotsHelper.postTimeSlots(restId,open,close,day).ResponseValidator.GetBodyAsText();
        //Assert.assertEquals(Constants.statusCode, response.);
        String code = JsonPath.read(response, "$.code").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.code, code);
        String Day = JsonPath.read(response, "$.data.day").toString().replace("[","").replace("]","");
        Assert.assertEquals(day, Day);
        String id = JsonPath.read(response, "$.data.id").toString().replace("[","").replace("]","");
        String getTimeSlot=timeSlotsHelper.getTimeSlots(restId).ResponseValidator.GetBodyAsText();
        String getIds=JsonPath.read(getTimeSlot, "$.data..id").toString().replace("[","").replace("]","").replace("{","").replace("}","");
        Assert.assertTrue(getIds.contains(id));
        String putResponse= timeSlotsHelper.putTimeSlots(restId,open,updateTime,day,id).ResponseValidator.GetBodyAsText();
        //Assert.assertEquals(Constants.statusCode, timeSlotsHelper.putTimeSlots(restId,open,updateTime,day,id).ResponseValidator.GetResponseCode());
        String putCode = JsonPath.read(putResponse, "$.code").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.code, putCode);
        String putDay = JsonPath.read(response, "$.data.day").toString().replace("[","").replace("]","");
        Assert.assertEquals(day, putDay);
        String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/SchemaSet/Json/CMS/updatetimeslot.txt");
        System.out.println(jsonschema);
        List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,putResponse);
        Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For get time slot API");
        boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(response);
        System.out.println("Contain empty nodes => " + isEmpty);
        Assert.assertEquals( false,isEmpty,"Found Empty Nodes in Response");
    }

    @Test(dataProvider = "addTimeSlot")
    public void deleteTimeSlotSchemaValidation(String restId, String open, String close, String day) throws IOException, ProcessingException {
        String response = timeSlotsHelper.postTimeSlots(restId, open, close, day).ResponseValidator.GetBodyAsText();
        //Assert.assertEquals(Constants.statusCode, timeSlotsHelper.postTimeSlots(restId,open,close,day).ResponseValidator.GetResponseCode());
        String code = JsonPath.read(response, "$.code").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(Constants.code, code);
        String Day = JsonPath.read(response, "$.data.day").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(day, Day);
        String id = JsonPath.read(response, "$.data.id").toString().replace("[", "").replace("]", "");
        String getTimeSlot = timeSlotsHelper.getTimeSlots(restId).ResponseValidator.GetBodyAsText();
        String getIds = JsonPath.read(getTimeSlot, "$.data..id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "");
        Assert.assertTrue(getIds.contains(id));
        String deleteTimeSlot= timeSlotsHelper.delTimeSlots(id).ResponseValidator.GetBodyAsText();
        String deleteSuccess= JsonPath.read(deleteTimeSlot,"$.code").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.code,deleteSuccess,"Slot is not deleted");
        String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/SchemaSet/Json/CMS/deletetimeslot.txt");
        System.out.println(jsonschema);
        List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,deleteTimeSlot);
        Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For delete time slot API");
        boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(response);
        System.out.println("Contain empty nodes => " + isEmpty);
        Assert.assertEquals( false,isEmpty,"Found Empty Nodes in Response");
    }

    @Test(dataProvider = "addHolidaySlot")
    public void addHolidaySlotsSchemaValidation(String payload)throws IOException, ProcessingException
    {
        String response= timeSlotsHelper.postHolidayTimeSlots(payload).ResponseValidator.GetBodyAsText();
        String code=JsonPath.read(response,"$.code").toString().replace("[","").replace("]","");
        Assert.assertEquals(code,Constants.code);
        String restId=JsonPath.read(response,"$.data.restaurant_id").toString().replace("[","").replace("]","");
        String id=JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        String getHolidaySlot= timeSlotsHelper.getHolidayTimeSlots(restId).ResponseValidator.GetBodyAsText();
        String getId= JsonPath.read(getHolidaySlot,"$.data..id").toString().replace("[","").replace("]","");
        Assert.assertTrue(getId.contains(id));
        String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/SchemaSet/Json/CMS/createholidayslots.txt");
        System.out.println(jsonschema);
        List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,response);
        Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For delete time slot API");
        boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(response);
        System.out.println("Contain empty nodes => " + isEmpty);
        Assert.assertEquals( false,isEmpty,"Found Empty Nodes in Response");
    }

    @Test(dataProvider = "updateHolidaySlot")
    public void updateHolidaySlotsSchemaValidation(String payload, String payload1)throws IOException, ProcessingException
    {
        String response= timeSlotsHelper.postHolidayTimeSlots(payload).ResponseValidator.GetBodyAsText();
        String code=JsonPath.read(response,"$.code").toString().replace("[","").replace("]","");
        Assert.assertEquals(code,Constants.code);
        String restId=JsonPath.read(response,"$.data.restaurant_id").toString().replace("[","").replace("]","");
        String id=JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        String getHolidaySlot= timeSlotsHelper.getHolidayTimeSlots(restId).ResponseValidator.GetBodyAsText();
        String getId= JsonPath.read(getHolidaySlot,"$.data..id").toString().replace("[","").replace("]","");
        Assert.assertTrue(getId.contains(id));
        String putResponse= timeSlotsHelper.putHolidayTimeSlots(payload1,id).ResponseValidator.GetBodyAsText();
        String putCode=JsonPath.read(putResponse,"$.code").toString().replace("[","").replace("]","");
        Assert.assertEquals(putCode,Constants.code);
        String putId=JsonPath.read(putResponse,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertEquals(id,putId,"ID's are not same");
        String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/SchemaSet/Json/CMS/updatholidayslots.txt");
        System.out.println(jsonschema);
        List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,putResponse);
        Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For delete time slot API");
        boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(response);
        System.out.println("Contain empty nodes => " + isEmpty);
        Assert.assertEquals( false,isEmpty,"Found Empty Nodes in Response");
    }


    @Test(dataProvider = "addHolidaySlot")
    public void delHolidaySlotSchemaValidation(String payload)throws IOException, ProcessingException
    {
        String response= timeSlotsHelper.postHolidayTimeSlots(payload).ResponseValidator.GetBodyAsText();
        String code=JsonPath.read(response,"$.code").toString().replace("[","").replace("]","");
        Assert.assertEquals(code,Constants.code);
        String restId=JsonPath.read(response,"$.data.restaurant_id").toString().replace("[","").replace("]","");
        String id=JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        String getHolidaySlot= timeSlotsHelper.getHolidayTimeSlots(restId).ResponseValidator.GetBodyAsText();
        String getId= JsonPath.read(getHolidaySlot,"$.data..id").toString().replace("[","").replace("]","");
        Assert.assertTrue(getId.contains(id));
        String delResponse= timeSlotsHelper.delHolidayTimeSlots(id).ResponseValidator.GetBodyAsText();
        String delCode=JsonPath.read(delResponse,"$.code").toString().replace("[","").replace("]","");
        Assert.assertEquals(delCode,Constants.code);
        String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/SchemaSet/Json/CMS/deleteholidayslots.txt");
        System.out.println(jsonschema);
        List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,delResponse);
        Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For delete time slot API");
        boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(response);
        System.out.println("Contain empty nodes => " + isEmpty);
        Assert.assertEquals( false,isEmpty,"Found Empty Nodes in Response");
    }

    @Test(dataProvider = "addHolidaySlot")
    public void getHolidaySlotSchemaValidation(String payload)throws IOException, ProcessingException
    {
        String response= timeSlotsHelper.postHolidayTimeSlots(payload).ResponseValidator.GetBodyAsText();
        String code=JsonPath.read(response,"$.code").toString().replace("[","").replace("]","");
        Assert.assertEquals(code,Constants.code);
        String restId=JsonPath.read(response,"$.data.restaurant_id").toString().replace("[","").replace("]","");
        String id=JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        String getHolidaySlot= timeSlotsHelper.getHolidayTimeSlots(restId).ResponseValidator.GetBodyAsText();
        String getId= JsonPath.read(getHolidaySlot,"$.data..id").toString().replace("[","").replace("]","");
        Assert.assertTrue(getId.contains(id));
        String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/SchemaSet/Json/CMS/getholidayslots.txt");
        System.out.println(jsonschema);
        List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,getHolidaySlot);
        Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For delete time slot API");
        boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(response);
        System.out.println("Contain empty nodes => " + isEmpty);
        Assert.assertEquals( false,isEmpty,"Found Empty Nodes in Response");
    }


}
