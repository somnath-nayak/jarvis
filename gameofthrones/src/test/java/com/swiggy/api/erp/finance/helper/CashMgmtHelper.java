package com.swiggy.api.erp.finance.helper;

import com.jayway.jsonpath.JsonPath;
import com.rabbitmq.client.AMQP;
import com.swiggy.api.erp.finance.constants.CashMgmtConstant;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.RabbitMQHelper;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

public class CashMgmtHelper {
    Initialize gameofthrones = new Initialize();
    RabbitMQHelper rmqHelper = new RabbitMQHelper();

    public HashMap<String,String> getHeader(){
        HashMap<String,String> headers = new HashMap<String, String>();
        headers.put("Content-Type","application/json");
        headers.put("Authorization","Basic R0dZU1dJOjIwMTVTVyFHR1k=");
        return headers;

    }



    public Processor createSessionHelper(String deid){
        HashMap<String,String> headers= getHeader();
        String[] payload=new String[]{deid};
        GameOfThronesService service = new GameOfThronesService("financecashmanagement","create_session_api",gameofthrones);
        Processor p = new Processor(service, headers, payload);
        return  p;
    }


    public Processor getSessionsHelper(String deId,String offset,String limit){
        HashMap<String,String> headers= getHeader();
        GameOfThronesService service = new GameOfThronesService("financecashmanagement","get_sessions_api",gameofthrones);

        Processor p = new Processor(service,headers,null,new String[]{deId,offset,limit});
        return  p;
    }

    public Processor getSessions(String de_id,String offset, String limit) {
        HashMap<String,String> headers= getHeader();
        GameOfThronesService service = new GameOfThronesService("transactionmanagement","get_session_api",gameofthrones);
        return new Processor(service,headers,null,new String[]{de_id,offset,limit});
    }


   public Processor addCommentHelper(String de_id,String session_id){
       HashMap<String,String> headers= getHeader();
        String[] param=new String[]{de_id,session_id};
        GameOfThronesService service = new GameOfThronesService("cashmanagement","add_comment_api",gameofthrones);
        Processor p = new Processor(service,headers,param);
        return  p;

    }
    public Processor getCommentHelper(String session_id){
        HashMap<String,String> headers= getHeader();
        GameOfThronesService service = new GameOfThronesService("financecashmanagement","get_comment_api",gameofthrones);

        Processor p = new Processor(service,headers,null,new String[]{session_id});
        return  p;

    }
    public Processor getSessionDetailHelper(String session_id,String de_id,String limit , String offset){
        HashMap<String,String> headers= getHeader();
        GameOfThronesService service = new GameOfThronesService("financecashmanagement","get_session_detail_api",gameofthrones);

        Processor p = new Processor(service,headers,null,new String[]{session_id,de_id,limit,offset});
        return  p;

    }

    public Processor getSessionDetails(String de_id, String session_id){
        HashMap<String,String> headers= getHeader();
        GameOfThronesService service = new GameOfThronesService("financecashmanagement","get_session_detail",gameofthrones);
        return new Processor(service,headers,null,new String[]{session_id, de_id});
    }


    public Processor addTransactionHelper(String de_id,String description,String order_id,String de_incoming,String de_outgoing,String txntype){
        HashMap<String,String> headers= getHeader();
        String[] payload=new String[]{de_id,description,order_id,de_incoming,de_outgoing,txntype};
        GameOfThronesService service = new GameOfThronesService("cashmanagement","add_txn_api",gameofthrones);
        Processor p = new Processor(service,headers,payload);
        return  p;

    }
    public Processor addPocketingAndPayoutTxn(String de_id,String description,String order_id,String de_incoming,String de_outgoing,String txntype){
        HashMap<String,String> headers= getHeader();
        String[] payload=new String[]{de_id,description,order_id,de_incoming,de_outgoing,txntype};
        GameOfThronesService service = new GameOfThronesService("cashmanagement","add_bulk_txn_api",gameofthrones);
        Processor p = new Processor(service,headers,payload);
        return p;
    }
    public Processor closeSessionHelper(String de_id){
        HashMap<String,String> headers= getHeader();
        String[] payload=new String[]{de_id};
        GameOfThronesService service = new GameOfThronesService("financecashmanagement","close_session_api",gameofthrones);
        Processor p = new Processor(service, headers, payload);
        return  p;

    }


    public String getOrderId(){
        long s=System.currentTimeMillis();

        return s+"";
    }

    public String pushPickedupJsonRMQ(String src,String order_id,String de_id,String status,String de_outgoing,String de_bill,String system_incoming,String system_outgoing,String system_bill) throws IOException {

        File file = new File("../Data/Payloads/JSON/CashOrderPickedup.json");
        String order_pickedup_json= FileUtils.readFileToString(file);

         order_pickedup_json=order_pickedup_json.replace("${source}",src).replace("${order_id}",order_id).replace("deId",de_id).replace("${status}",status).replace("de_outgoing",de_outgoing).replace("de_bill",de_bill).replace("systemIncoming",system_incoming).replace("systemSpending",system_outgoing).replace("systemBill",system_bill);

        rmqHelper.pushMessageToExchange(CashMgmtConstant.CASH_RMQHOSTNAME, CashMgmtConstant.CASH_ORDERSTATUSQUEUE, new AMQP.BasicProperties().builder().contentType("application/json"),order_pickedup_json);
        return order_id;
    }

    public String pushDeliveredJsonRMQ(String src,String order_id,String de_id,String status,String de_incoming,String de_bill,String system_incoming,String system_outgoing,String system_bill) throws IOException {

        File file = new File("../Data/Payloads/JSON/CashOrderDelivered.json");
        String order_delivered_json= FileUtils.readFileToString(file);

        order_delivered_json=order_delivered_json.replace("${source}",src).replace("${order_id}",order_id).replace("deId",de_id).replace("${status}",status).replace("de_incoming",de_incoming).replace("de_bill",de_bill).replace("systemIncoming",system_incoming).replace("systemSpending",system_outgoing).replace("systemBill",system_bill);

        rmqHelper.pushMessageToExchange(CashMgmtConstant.CASH_RMQHOSTNAME, CashMgmtConstant.CASH_ORDERSTATUSQUEUE, new AMQP.BasicProperties().builder().contentType("application/json"),order_delivered_json);
        return order_id;
    }

    public Processor revertNormalTxnHelper(String de_id,String  txn_id){
        HashMap<String,String> headers= getHeader();
        String[] payload=new String[]{de_id,txn_id};
        GameOfThronesService service = new GameOfThronesService("cashmanagement","revert_txn_api",gameofthrones);
        Processor p = new Processor(service,headers,payload);
        return  p;

    }


    public Processor channelOptionHelper(String de_id){
        HashMap<String,String> headers= getHeader();
        GameOfThronesService service = new GameOfThronesService("financecashmanagement","cash_channel_options_api",gameofthrones);
        Processor p = new Processor(service,headers,null,new String[]{de_id});
        return  p;

    }

    public Processor channelDetailsHelper(String channel_id,String de_id){
        HashMap<String,String> headers= getHeader();
        GameOfThronesService service = new GameOfThronesService("financecashmanagement","cash_channel_detail_api",gameofthrones);
        Processor p = new Processor(service,headers,null,new String[]{channel_id,de_id});
        return  p;

    }




    public String getCurrentSessionId(Processor processor) {
        String json = processor.ResponseValidator.GetBodyAsText();
        return JsonPath.read(json, "$.data.sessions[?(@.currentSession == true)].sessionId").toString().replace("[","").replace("]","");
    }

    public String getTransactions(Processor processor) {
        String json = processor.ResponseValidator.GetBodyAsText();
        return JsonPath.read(json, "$.data.transactions[0].id").toString().replace("[","").replace("]","");
    }

    public Processor getSessions(String de_id) {
        HashMap<String,String> headers= getHeader();
        GameOfThronesService service = new GameOfThronesService("financecashmanagement","get_session_api",gameofthrones);
        return new Processor(service,headers,null,new String[]{de_id});
    }
}
