package com.swiggy.api.erp.vms.helper;

import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;
import com.swiggy.api.erp.vms.constants.RMSConstants;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Daenerys.Initializer;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.RedisHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;

import java.math.BigInteger;
import java.time.Instant;
import java.util.*;

//import edu.emory.mathcs.backport.java.util.Arrays;

public class SRSHelper extends RMSConstants {

	Initialize initializer = Initializer.getInitializer();

	public Processor createRest(String name, String vymoId, String cancelledCheque, String compId, String area, String city, String agreementType, String updatedBy, String locality, String bankCity, String ownerEmail, String ownerMobile, String address, String mou, String commissionExp, String payBySystemValue, String gstState, String invoicingName, String bankName, String bankIfscCode, String bankAccountNo, String tanNo, String panNo, String benificiaryName, String invoicingEmail, String ownerName, String smEmail, String asmEmail) {

        GameOfThronesService services = new GameOfThronesService("restaurantservice", "createRest", initializer);
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("Content-Type", "application/json");

        Processor processor = new Processor(services, requestheaders, new String[]{name, vymoId, cancelledCheque, compId, area, city, agreementType, updatedBy, locality, bankCity, ownerEmail, ownerMobile, address, mou, commissionExp, payBySystemValue, gstState, invoicingName, bankName, bankIfscCode, bankAccountNo, tanNo, panNo, benificiaryName, invoicingEmail, ownerName, smEmail, asmEmail}, null);
        return processor;

    }

    public Processor updateRest(String name, String vymoId, String cancelledCheque, String compId, String area, String city, String agreementType, String updatedBy, String locality, String bankCity, String ownerEmail, String ownerMobile, String address, String mou, String commissionExp, String payBySystemValue, String gstState, String invoicingName, String bankName, String bankIfscCode, String bankAccountNo, String tanNo, String panNo, String benificiaryName, String invoicingEmail, String ownerName, String smEmail, String asmEmail, String menuId, String restId) {

        GameOfThronesService services = new GameOfThronesService("restaurantservice", "updateRest", initializer);
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("Content-Type", "application/json");

        Processor processor = new Processor(services, requestheaders, new String[]{name, vymoId, cancelledCheque, compId, area, city, agreementType, updatedBy, locality, bankCity, ownerEmail, ownerMobile, address, mou, commissionExp, payBySystemValue, gstState, invoicingName, bankName, bankIfscCode, bankAccountNo, tanNo, panNo, benificiaryName, invoicingEmail, ownerName, smEmail, asmEmail, menuId}, new String[]{restId} );
        return processor;

    }

    public Processor getRestaurant(String restId) {

        GameOfThronesService services = new GameOfThronesService("restaurantservice", "getRestaurant", initializer);
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("Content-Type", "application/json");

        Processor processor = new Processor(services, requestheaders, null, new String[]{restId});
        return processor;

    }

    public List<Map<String, Object>> getRestaurantDetails(String restId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RMSConstants.cmsDB);
        return sqlTemplate.queryForList(RMSConstants.getRest_query + restId);
    }

    public List<Map<String, Object>> getRestaurantFinanceDetails(String restId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RMSConstants.cmsDB);
        return sqlTemplate.queryForList(RMSConstants.getRestFin_query + restId);
    }

    public String getVymoRestMap(String restId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RMSConstants.rmsVymoDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(RMSConstants.getVymoRest_query + restId);
        return list.get(0).get("vymo_id").toString();
    }

    public Processor restToggleTnC(String enableRestId, String disableRestId) {

        GameOfThronesService services = new GameOfThronesService("restaurantservice", "restToggleTnC", initializer);
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("Content-Type", "application/json");

        Processor processor = new Processor(services, requestheaders, new String[]{enableRestId, disableRestId}, null);
        return processor;

    }

    public Processor restFindBy(String restId) {

        GameOfThronesService services = new GameOfThronesService("restaurantservice", "restFindBy", initializer);
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("Content-Type", "application/json");

        Processor processor = new Processor(services, requestheaders, new String[]{restId}, null);
        return processor;

    }

    public Processor updateRestDetails(String restId, String name, String packing_charges, String tier,String veg_classifier, String halal_classifier, String cutlery, String updated_by) {

        GameOfThronesService services = new GameOfThronesService("restaurantservice", "updateRestDetails", initializer);
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("Content-Type", "application/json");

        Processor processor = new Processor(services, requestheaders, new String[]{name, packing_charges, tier, restId, veg_classifier, halal_classifier, cutlery, updated_by }, new String[]{restId});
        return processor;

    }

    public Processor getByAreaIds(String areaId1, String areaId2) {

        GameOfThronesService services = new GameOfThronesService("restaurantservice", "getByAreaIds", initializer);
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("Content-Type", "application/json");

        Processor processor = new Processor(services, requestheaders, null, new String[]{areaId1, areaId2});
        return processor;

    }

    public Processor getByRestIdsAndParentIds(String cityId, String restId, String parentId) {

        GameOfThronesService services = new GameOfThronesService("restaurantservice", "getByRestIdsAndParentIds", initializer);
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("Content-Type", "application/json");

        Processor processor = new Processor(services, requestheaders, null, new String[]{cityId, restId, parentId});
        return processor;

    }

    public Processor getRestIdsByAreaId(String areaId) {

        GameOfThronesService services = new GameOfThronesService("restaurantservice", "getRestIdsByAreaId", initializer);
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("Content-Type", "application/json");

        Processor processor = new Processor(services, requestheaders, null, new String[]{areaId});
        return processor;

    }

    public Processor getRestIdsByCityId(String cityId) {

        GameOfThronesService services = new GameOfThronesService("restaurantservice", "getRestIdsByCityId", initializer);
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("Content-Type", "application/json");

        Processor processor = new Processor(services, requestheaders, null, new String[]{cityId});
        return processor;

    }

    public Processor isRestaurantOpen(String restId) {

        GameOfThronesService services = new GameOfThronesService("restaurantservice", "isRestaurantOpen", initializer);
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("Content-Type", "application/json");

        Processor processor = new Processor(services, requestheaders, null, new String[]{restId});
        return processor;

    }

    public Map<String, Object> isRestaurantEnabled(String restId) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RMSConstants.cmsDB);
        //return sqlTemplate.queryForList(RMSConstants.getRestEnabled_query + restId).get(0).get("enabled").toString();
        return sqlTemplate.queryForMap(RMSConstants.getRestEnabled_query + restId);

    }

    public Processor getNextPageOfRestIds(String restId) {

        GameOfThronesService services = new GameOfThronesService("restaurantservice", "getNextPageOfRestIds", initializer);
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("Content-Type", "application/json");

        Processor processor = new Processor(services, requestheaders, null, new String[]{restId});
        return processor;

    }

    public Processor getRestAreaId(String restId) {

        GameOfThronesService services = new GameOfThronesService("restaurantservice", "getRestAreaId", initializer);
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("Content-Type", "application/json");

        Processor processor = new Processor(services, requestheaders, null, new String[]{restId});
        return processor;

    }

    public Processor getRestCityId(String restId) {

        GameOfThronesService services = new GameOfThronesService("restaurantservice", "getRestCityId", initializer);
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("Content-Type", "application/json");

        Processor processor = new Processor(services, requestheaders, null, new String[]{restId});
        return processor;

    }

    public Processor getRestContacts(String restId) {

        GameOfThronesService services = new GameOfThronesService("restaurantservice", "getRestContacts", initializer);
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("Content-Type", "application/json");

        Processor processor = new Processor(services, requestheaders, null, new String[]{restId});
        return processor;

    }

    public Processor getRestPartner(String restId) {

        GameOfThronesService services = new GameOfThronesService("restaurantservice", "getRestPartner", initializer);
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("Content-Type", "application/json");
        requestheaders.put("Authorization", "Basic WUdHSVNXOlNXSUdHWUAyMDE3");

        Processor processor = new Processor(services, requestheaders, null, new String[]{restId});
        return processor;

    }

    public Processor restSuggestV2(String cityId, String areaId, String partner, String apiUnderTest) {

        GameOfThronesService services = new GameOfThronesService("restaurantservice", apiUnderTest, initializer);
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("Content-Type", "application/json");
        requestheaders.put("Authorization", "Basic WUdHSVNXOlNXSUdHWUAyMDE3");

        Processor processor = new Processor(services, requestheaders, null, new String[]{"["+cityId+"]", "["+areaId+"]", "["+partner+"]"});
        return processor;

    }

    public Processor restSuggestV2(String Id, String apiUnderTest) {

        GameOfThronesService services = new GameOfThronesService("restaurantservice", apiUnderTest, initializer);
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("Content-Type", "application/json");
        requestheaders.put("Authorization", "Basic WUdHSVNXOlNXSUdHWUAyMDE3");

        Processor processor = new Processor(services, requestheaders, null, new String[]{"["+Id+"]"});
        return processor;

    }

    public Processor restSuggestV2(String Id1, String Id2, String apiUnderTest) {

        GameOfThronesService services = new GameOfThronesService("restaurantservice", apiUnderTest, initializer);
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("Content-Type", "application/json");
        requestheaders.put("Authorization", "Basic WUdHSVNXOlNXSUdHWUAyMDE3");

        Processor processor = new Processor(services, requestheaders, null, new String[]{"["+Id1+"]", "["+Id2+"]"});
        return processor;

    }


    public List<Map<String, Object>> getRestSuggestV2FromDB(String cityId, String areaId, String partner) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RMSConstants.cmsDB);
        return sqlTemplate.queryForList("select restaurants.id, restaurants.area_code, restaurants.city_code, restaurants_partner_map.type_of_partner from restaurants LEFT JOIN restaurants_partner_map on restaurants.id = restaurants_partner_map.rest_id where restaurants.area_code="+areaId+" AND restaurants.city_code="+cityId+" AND restaurants_partner_map.type_of_partner="+partner);

    }



}
