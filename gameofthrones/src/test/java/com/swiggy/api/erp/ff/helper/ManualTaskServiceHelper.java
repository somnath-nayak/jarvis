package com.swiggy.api.erp.ff.helper;

import com.swiggy.api.erp.ff.constants.ManualTaskServiceConstants;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ManualTaskServiceHelper implements ManualTaskServiceConstants {

    Initialize gameofthrones = new Initialize();
    OMSHelper omsHelper = new OMSHelper();
    VerificationServiceHelper verificationServiceHelper = new VerificationServiceHelper();

    public Processor createTask(String orderId, String reason, String subReason, String source) {
        HashMap<String,String> headers = new HashMap<>();
       omsHelper.verifyOrder(orderId, "111");
        omsHelper.clearOrdersInQueue("all");
        GameOfThronesService service = new GameOfThronesService(SERVICE_MANUAL_TASK_SERVICE, "taskcreate", gameofthrones);
        Processor processor = new Processor(service,headers, new String[] {orderId, reason, subReason, source});
        return processor;
    }

    public Processor createTask2(String orderId, String reason, String subReason, String source) {
        HashMap<String,String> headers = new HashMap<>();
       // omsHelper.verifyOrder(orderId, "111");
        omsHelper.clearOrdersInQueue("all");
        GameOfThronesService service = new GameOfThronesService(SERVICE_MANUAL_TASK_SERVICE, "taskcreate", gameofthrones);
        Processor processor = new Processor(service,headers, new String[] {orderId, reason, subReason, source});
        return processor;
    }

    public Processor nullifyTask(int unassignmentReasonId) {
        GameOfThronesService service = new GameOfThronesService(SERVICE_MANUAL_TASK_SERVICE, "taskcancel", gameofthrones);
        Processor processor = new Processor(null, new String[] {""+unassignmentReasonId}, null, null, service);
        return processor;
    }

    public Processor cancelTask(int cancellationReasonId) {
        GameOfThronesService service = new GameOfThronesService(SERVICE_MANUAL_TASK_SERVICE, "taskcancel", gameofthrones);
        Processor processor = new Processor(null, new String[] {""+cancellationReasonId}, null, null, service);
        return processor;
    }

    public Processor cancelTaskTemp(String orderId, String taskType, int cancellationReasonId) {
        GameOfThronesService service = new GameOfThronesService(SERVICE_MANUAL_TASK_SERVICE, "taskcanceltemp", gameofthrones);
        Processor processor = new Processor(null, new String[] {orderId, taskType, ""+cancellationReasonId}, null, null, service);
        return processor;
    }

    public Processor getTaskStatus(String taskId) {
        GameOfThronesService service = new GameOfThronesService(SERVICE_MANUAL_TASK_SERVICE, "gettaskstatus", gameofthrones);
        Processor processor = new Processor(null, null, new String[] {taskId}, null, service);
        return processor;
    }

    public Processor getTaskStatusTemp(String orderId, String reason) {
        GameOfThronesService service = new GameOfThronesService(SERVICE_MANUAL_TASK_SERVICE, "gettaskstatustemp", gameofthrones);
        Processor processor = new Processor(null, null, new String[] {orderId, reason}, null, service);
        return processor;
    }

    public Processor getWorklog(String orderId) {
        GameOfThronesService service = new GameOfThronesService(SERVICE_MANUAL_TASK_SERVICE, "getworklog", gameofthrones);
        Processor processor = new Processor(null, null, new String[] {orderId}, null, service);
        return processor;
    }

    public Processor getTask(String orderId) {
        GameOfThronesService service = new GameOfThronesService(SERVICE_MANUAL_TASK_SERVICE, "gettask", gameofthrones);
        Processor processor = new Processor(null, null, new String[] {orderId}, null, service);
        return processor;
    }

    private boolean checkPresenceOfTaskInDB(String taskId){
        try {
            List<Map<String, Object>> taskDetailsList = SystemConfigProvider.getTemplate(MANUAL_TASK_DB_HOST).queryForList("select * from task where task_id = '"+ taskId  +"';");
            return true;
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean checkPresenceOfAssignmentRequestIdInDB(String taskId){
        try {
            List<Map<String, Object>> taskDetailsList = SystemConfigProvider.getTemplate(MANUAL_TASK_DB_HOST).queryForList("select assignment_request_id from task where task_id = '"+ taskId  +"';");
            Assert.assertNotNull(taskDetailsList.get(0).get("assignment_request_id"),"assignment request id is null");
            return true;
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
            return false;
        }
    }

    private int getTaskTypeFromDB(String taskId){
        int task_type;
        try {
            List<Map<String, Object>> taskDetailsList = SystemConfigProvider.getTemplate(MANUAL_TASK_DB_HOST).queryForList("select task_type from task where task_id = '"+ taskId  +"';");
            task_type = (int)taskDetailsList.get(0).get("task_type");
            return task_type;
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
            return -1;
        }
    }

    private String getStatusFromDB(String taskId){
        String status = "";
        try {
            List<Map<String, Object>> taskDetailsList = SystemConfigProvider.getTemplate(MANUAL_TASK_DB_HOST).queryForList("select status from task where task_id = '"+ taskId  +"';");
            status = (String) taskDetailsList.get(0).get("status");
            return status;
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
            return status;
        }
    }

    private int getRequestReasonFromDB(String taskId){
        int request_reason;
        try {
            List<Map<String, Object>> taskDetailsList = SystemConfigProvider.getTemplate(MANUAL_TASK_DB_HOST).queryForList("select request_reason from task where task_id = '"+ taskId  +"';");
            request_reason = (int) taskDetailsList.get(0).get("request_reason");
            return request_reason;
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
            return -1;
        }
    }

    private int getRequestSubReasonFromDB(String taskId){
        int request_sub_reason;
        try {
            List<Map<String, Object>> taskDetailsList = SystemConfigProvider.getTemplate(MANUAL_TASK_DB_HOST).queryForList("select request_sub_reason from task where task_id = '"+ taskId  +"';");
            request_sub_reason = (int) taskDetailsList.get(0).get("request_sub_reason");
            return request_sub_reason;
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
            return -1;
        }
    }

    private int getRoleIdFromDB(String taskId){
        int role_id;
        try {
            List<Map<String, Object>> taskDetailsList = SystemConfigProvider.getTemplate(MANUAL_TASK_DB_HOST).queryForList("select role_id from task where task_id = '"+ taskId  +"';");
            role_id = (int)taskDetailsList.get(0).get("role_id");
            return role_id;
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
            return -1;
        }
    }

    private String getSourceFromDB(String taskId){
        String source = "";
        try {
            List<Map<String, Object>> taskDetailsList = SystemConfigProvider.getTemplate(MANUAL_TASK_DB_HOST).queryForList("select * from task where task_id = '"+ taskId  +"';");
            source = (String) taskDetailsList.get(0).get("source");
            return source;
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
            return source;
        }
    }

    public void assertCreateTaskApiResponses(Processor response, HashMap<String, String> expectedResponseMap) {
        if (response.ResponseValidator.GetResponseCode() == 200){
            SoftAssert softAssert = new SoftAssert();

            softAssert.assertEquals(response.ResponseValidator.GetResponseCode(), Integer.parseInt(expectedResponseMap.get("responseCode")), "Actual Response code did not match with expected");
            softAssert.assertEquals(response.ResponseValidator.GetNodeValueAsInt("status_code"), Integer.parseInt(expectedResponseMap.get("statusCode")), "Actual status code did not match with expected");
            softAssert.assertEquals(response.ResponseValidator.GetNodeValue("status_message"), expectedResponseMap.get("statusMessage"), "Actual status message did not match with expected");
            softAssert.assertNotNull(response.ResponseValidator.GetNodeValue("data.task_id"), "taskId is null in the response");
            softAssert.assertEquals(response.ResponseValidator.GetNodeValue("data.status"), expectedResponseMap.get("taskStatus"), "Actual status inside data message did not match with expected");
            softAssert.assertAll();
        } else {
            Assert.assertEquals(response.ResponseValidator.GetResponseCode(), expectedResponseMap.get("responseCode"), "Actual Response code did not match with expected ");
        }

    }

    public void assertDataAfterTaskCreation(String orderId, String expectedSource, String taskId, int expectedReasonId, int expectedSubReasonId, String reasonOE, String subReasonOE) {
        SoftAssert softAssert = new SoftAssert();
        if(!subReasonOE.contains("null")){
            if(subReasonOE.equals("L1")){
                softAssert.assertTrue(verificationServiceHelper.queryOrdersInQueue(orderId, "L1Placer"), "order is not present waiting for L1 in orders in queue "+orderId);
                softAssert.assertEquals(getRoleIdFromDB(taskId), 1, "Expected role ID did not match with actual for task_id = "+taskId);
            }else if (subReasonOE.equals("L2")){
                softAssert.assertTrue(verificationServiceHelper.queryOrdersInQueue(orderId, "L2Placer"), "order is not present waiting for L2 in orders in queue "+orderId);
                softAssert.assertEquals(getRoleIdFromDB(taskId), 2, "Expected role ID did not match with actual for task_id = "+taskId);
                softAssert.assertEquals(getRequestSubReasonFromDB(taskId), expectedSubReasonId, "Expected request sub-reason did not match in DB " + orderId);
            }
        }
        softAssert.assertEquals(getTaskTypeFromDB(taskId), 1 , "Expected task type was placing but did not match in DB " + orderId);
        softAssert.assertTrue(checkPresenceOfTaskInDB(taskId), "Entry has not been created in task table for order_id = "+orderId);
        softAssert.assertEquals(getStatusFromDB(taskId), "active", "Expected status was active but did not match in DB " + orderId);
        softAssert.assertEquals(getRequestReasonFromDB(taskId), expectedReasonId, "Expected request reason did not match in DB " + orderId);
        softAssert.assertEquals(getSourceFromDB(taskId), expectedSource, "Expected source did not match in DB " + orderId);
        softAssert.assertAll();
    }

    public void assertDataAfterNullifyTask(String orderId, String expectedSource, String taskId, int expectedReasonId, int expectedSubReasonId, String reasonOE, String subReasonOE) {
        SoftAssert softAssert = new SoftAssert();
        if(!subReasonOE.contains("null")){
            if(subReasonOE.equals("L1")){
                softAssert.assertTrue(verificationServiceHelper.queryOrdersInQueue(orderId, "L1Placer"), "order is not present waiting for L1 in orders in queue "+orderId);
                softAssert.assertEquals(getRoleIdFromDB(taskId), 1, "Expected role ID did not match with actual for task_id = "+taskId);
            }else if (subReasonOE.equals("L2")){
                softAssert.assertTrue(verificationServiceHelper.queryOrdersInQueue(orderId, "L2Placer"), "order is not present waiting for L2 in orders in queue "+orderId);
                softAssert.assertEquals(getRoleIdFromDB(taskId), 2, "Expected role ID did not match with actual for task_id = "+taskId);
                softAssert.assertEquals(getRequestSubReasonFromDB(taskId), expectedSubReasonId, "Expected request sub-reason did not match in DB " + orderId);
            }else if (subReasonOE.equals("DAILY_AGENT")){
                softAssert.assertTrue(verificationServiceHelper.queryOrdersInQueue(orderId, "DAILY_AGENT"), "order is not present waiting for L2 in orders in queue "+orderId);
                softAssert.assertEquals(getRoleIdFromDB(taskId), 2, "Expected role ID did not match with actual for task_id = "+taskId);
                softAssert.assertEquals(getRequestSubReasonFromDB(taskId), expectedSubReasonId, "Expected request sub-reason did not match in DB " + orderId);
            }
        }
        softAssert.assertEquals(getTaskTypeFromDB(taskId), 1 , "Expected task type was placing but did not match in DB " + orderId);
        softAssert.assertTrue(checkPresenceOfTaskInDB(taskId), "Entry has not been created in task table for order_id = "+orderId);
        softAssert.assertTrue(checkPresenceOfAssignmentRequestIdInDB(taskId), "Entry has not been created in task table for order_id = "+orderId);
        softAssert.assertEquals(getStatusFromDB(taskId), "COMPLETED", "Expected status was COMPLETED but did not match in DB " + orderId);
        softAssert.assertEquals(getRequestReasonFromDB(taskId), expectedReasonId, "Expected request reason did not match in DB " + orderId);
        softAssert.assertEquals(getSourceFromDB(taskId), expectedSource, "Expected source did not match in DB " + orderId);
        softAssert.assertAll();
    }
}
