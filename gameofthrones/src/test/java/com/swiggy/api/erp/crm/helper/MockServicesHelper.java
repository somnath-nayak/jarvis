package com.swiggy.api.erp.crm.helper;

import com.swiggy.api.erp.crm.constants.CRMConstants;
import com.swiggy.api.erp.delivery.helper.OrderStatus;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Daenerys.Initializer;
import framework.gameofthrones.Tyrion.SqlTemplate;
import framework.gameofthrones.Tyrion.WireMockHelper;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class MockServicesHelper {

    WireMockHelper wireMockHelper = new WireMockHelper();
    Initialize Env = new Initialize();

    public void stubOrderDetailAPIResponse (String orderId,String restId,String itemId,String
            cost,String orderType, String order_status, String restaurant_type) throws IOException, InterruptedException

    {
        File file = new File("../Data/MockAPI/ResponseBody/crm/OrderDetailApiMockResponse");
        String body = FileUtils.readFileToString(file);
        body = body.replace("${order_id}", orderId).replace("${restaurant_id}",restId).
                replace("${item_id}",itemId).replace("${sub_total}",cost).
                replace("${order_status}",order_status).replace("${type}",orderType).
                replace("${restaurant_type}",restaurant_type);
        System.out.println(body);
        wireMockHelper.setupStub("get_order_from_id/"+orderId+"/formatted", 200, "application/json", body,0);

    }

    public void stubGetUserSegmentApiResponse(String userId, String valueSegment, String igccSeg ){
        File file = new File("../Data/MockAPI/ResponseBody/crm/RnGSegmentMockResponse");
        String body = null;
        try {
            body = FileUtils.readFileToString(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        body = body.replace("${user_id}", userId).
                replace("${valueSegment}",valueSegment).replace("${IGCCSegment}",igccSeg);
        System.out.println(body);
        wireMockHelper.setupStub("/api/v1/get/"+userId, 200, "application/json", body,0);

    }

    public void stubOrderDetailAPIResponse2 (String orderId,String restId,String itemId,String
            cost,String orderType,String order_status, String restaurant_type) throws IOException, InterruptedException

    {
        File file = new File("../Data/MockAPI/ResponseBody/crm/CheckoutOrderDetailApiMockResponse2");
        String body = FileUtils.readFileToString(file);
        body = body.replace("${order_id}", orderId).replace("${restaurant_id}",restId).
                replace("${item_id}",itemId).replace("${sub_total}",cost).
                replace("${order_status}",order_status).replace("${type}",orderType).
                replace("${restaurant_type}",restaurant_type);
        System.out.println(body);
        wireMockHelper.setupStubEdit("api/v1/order/get_order_details?order_id="+orderId, 200, "application/json", body,0);

    }

    public void stubDashOrderDetails(String orderId, String deliveryStatus){
        File file = new File("../Data/MockAPI/ResponseBody/crm/OrderDetailApiMockResponse");
        String body =null ;//= FileUtils.readFileToString(file);
        body = body.replace("${order_id}", orderId).replace("${delivery_status}",deliveryStatus);
        System.out.println(body);
        wireMockHelper.setupStub("get_order_from_id/"+orderId+"/formatted", 200, "application/json", body,0);

    }

    public void stubOrderDetailAPIRefundPayment (String orderId,String restId,String itemId,String
            cost,String orderType, String order_status, String paymentMethod) throws IOException, InterruptedException

    {
        File file = new File("../Data/MockAPI/ResponseBody/crm/OrderDetailPaymentApiMockResponse");
        String body = FileUtils.readFileToString(file);
        body = body.replace("${order_id}", orderId).replace("${restaurant_id}",restId).
                replace("${item_id}",itemId).replace("${sub_total}",cost).
                replace("${order_status}",order_status).replace("${type}",orderType).replace("${paymentMethod}",paymentMethod);
        System.out.println(body);
        wireMockHelper.setupStub("get_order_from_id/"+orderId+"/formatted", 200, "application/json", body,0);

    }

    public void stubOrderDetailsCafeAPIResponse (String orderId,String restId,String itemId,String
            cost,String orderType, String order_status, String placed_status) throws IOException, InterruptedException

    {
        File file = new File("../Data/MockAPI/ResponseBody/crm/OrderDetailCafeApiMockResponse");
        String body = FileUtils.readFileToString(file);
        body = body.replace("${order_id}", orderId).replace("${restaurant_id}",restId).
                replace("${item_id}",itemId).replace("${sub_total}",cost).replace("${placed_status}",placed_status).
                replace("${order_status}",order_status).replace("${type}",orderType);
        System.out.println(body);
        wireMockHelper.setupStub("get_order_from_id/"+orderId+"/formatted", 200, "application/json", body,0);

    }

    public void stubOrderDetailsDominosAPIResponse (String orderId,String restId,String itemId,String
            cost,String orderType, String order_status, String placed_status) throws IOException, InterruptedException

    {
        File file = new File("../Data/MockAPI/ResponseBody/crm/OrderDetailDominosApiMockResponse");
        String body = FileUtils.readFileToString(file);
        body = body.replace("${order_id}", orderId).replace("${restaurant_id}",restId).
                replace("${item_id}",itemId).replace("${sub_total}",cost).replace("${placed_status}",placed_status).
                replace("${order_status}",order_status).replace("${type}",orderType);
        System.out.println(body);
        wireMockHelper.setupStub("get_order_from_id/"+orderId+"/formatted", 200, "application/json", body,0);

    }

    public void stubDeliveryTrackAPIResponse(String orderId, String deliveryStatus, String assignedPredTime, String assignedActualTime, String confirmedPredTime,
                                             String confirmedActualTime, String arrivedPredTime, String arrivedActualTime, String pickedupPredTime,
                                             String pickedupActualTime, String reachedPredTime, String reachedActualTime, String deliveredPredTime,
                                             String deliveredActualTime) throws IOException, InterruptedException {
        File file = new File("../Data/MockAPI/ResponseBody/crm/OrderTrackApiMockResponse");
        String body = FileUtils.readFileToString(file);
        body = body.replace("${order_id}", orderId).
                replace("${delivery_status}", deliveryStatus).
                replace("${assigned_pred_time}", assignedPredTime).
                replace("${assigned_actual_time}", assignedActualTime).
                replace("${confirmed_pred_time}", confirmedPredTime).
                replace("${confirmed_actual_time}", confirmedActualTime).
                replace("${arrived_pred_time}", arrivedPredTime).
                replace("${arrived_actual_time}", arrivedActualTime).
                replace("${pickedup_pred_time}", pickedupPredTime).
                replace("${pickedup_actual_time}", pickedupActualTime).
                replace("${reached_pred_time}", reachedPredTime).
                replace("${reached_actual_time}", reachedActualTime).
                replace("${delivered_pred_time}", deliveredPredTime).
                replace("${delivered_actual_time}", deliveredActualTime);
        System.out.println(body);
        wireMockHelper.setupStub("api/order/track/" + orderId, 200, "application/json", body, 0);
    }

    public void stubFetchAllDispositionIDs(String orderId,String dispositionId, String subDispositionId) throws IOException
    {
        File file = new File("../Data/MockAPI/ResponseBody/crm/FetchAllDispositionIdsMockResponse");
        String body = FileUtils.readFileToString(file);
        body=body.replace("${dispositionId}",dispositionId).replace("${subDispositionId}",subDispositionId);
        System.out.println(body);
        wireMockHelper.setupStub("v1/order/"+orderId+"/order-cancellation-disposition", 200, "application/json", body,0);
    }

    public void stubGetCancellationFee(String orderId) throws IOException
    {
        File file = new File("../Data/MockAPI/ResponseBody/crm/CancellationFeeMockResponse");
        String body = FileUtils.readFileToString(file);
        System.out.println(body);
        wireMockHelper.setupStub("v1/order/"+orderId+"/cancellation-fee/", 200, "application/json", body,0);
    }

    public void stubFFcancellation(String orderId) throws IOException
    {
        File file = new File("../Data/MockAPI/ResponseBody/crm/FFCancellationAPIMockResponse");
        String body = FileUtils.readFileToString(file);
        System.out.println(body);
        wireMockHelper.setupStubPut("v1/order/"+orderId+"/", 200, "application/json", body,0);
    }

    public void stubRefundPGFlag(String orderId) throws IOException
    {
        File file = new File("../Data/MockAPI/ResponseBody/crm/CheckRefundable");
        String body = FileUtils.readFileToString(file);
        System.out.println(body);
        wireMockHelper.setupStub(MessageFormat.format("api/v1/refund/byOrderId/toPg/checkRefundable\\?orderId={0}", orderId), 200, "application/json", body,0);
    }

    public void stubCloneCheckAPI(String orderId,String restId,String itemId,String
            cost,String orderType) throws IOException
    {
        File file = new File("../Data/MockAPI/ResponseBody/CloneCheckMockResponse");
        String body = FileUtils.readFileToString(file);
        body = body.replace("${order_id}", orderId).replace("${restaurant_id}",restId).
                replace("${item_id}",itemId).replace("${sub_total}",cost).replace("${type}",orderType);
        System.out.println(body);
        wireMockHelper.setupStubPost("api/v1/order/update/cloning_check", 200, "application/json", body,2000);
    }

    public void stubCloneConfirmAPI(String orderId,String restId,String itemId,String
            cost,String orderType,String baseOrder) throws IOException
    {
        File file = new File("../Data/MockAPI/ResponseBody/CloneConfirmMockResponse");
        String body = FileUtils.readFileToString(file);
        body = body.replace("${order_id}", orderId).replace("${restaurant_id}",restId).
                replace("${item_id}",itemId).replace("${sub_total}",cost).replace("${type}",orderType).replace("${base_order}",baseOrder);
        System.out.println(body);
        wireMockHelper.setupStubPost("api/v1/order/update/cloning_confirm", 200, "application/json", body,2000);
    }

    public void stubFraudServiceFF() throws IOException
    {
        File file = new File("../Data/MockAPI/ResponseBody/FraudServiceFFMockResponse");
        String body = FileUtils.readFileToString(file);
        System.out.println(body);
        wireMockHelper.setupStub("api/v1/fraud-detection-service/cod/status", 200, "application/json", body,0);
    }

    public void stubRNGCouponFetch() throws IOException
    {
        File file = new File("../Data/MockAPI/ResponseBody/CouponFetchMockResponse");
        String body = FileUtils.readFileToString(file);
        System.out.println(body);
        wireMockHelper.setupStubPost("/coupon/fetch/", 200, "application/json", body,2000);
    }

    public void stubCouponGrant() throws IOException
    {
        File file = new File("../Data/MockAPI/ResponseBody/CouponGrantMockResponse");
        String body = FileUtils.readFileToString(file);
        System.out.println(body);
        wireMockHelper.setupStubPost("/coupon/fetch/", 200, "application/json", body,2000);
    }

    public void stubGetUserSegmentation() throws IOException
    {
        File file = new File("../Data/MockAPI/ResponseBody/UserSegmentationMockResponse");
        String body = FileUtils.readFileToString(file);
        System.out.println(body);
        wireMockHelper.setupStub("", 200, "application/json", body,0);
    }

    public void stubCheckoutPartialRefund() throws IOException
    {
        File file = new File("../Data/MockAPI/ResponseBody/RefundPartialMockResponse");
        String body = FileUtils.readFileToString(file);
        System.out.println(body);
        wireMockHelper.setupStubPost("api/v1/get/{userid}", 200, "application/json", body,2000);
    }


    public void stubCheckCODStatusFF (String reason, String customerId) throws IOException, InterruptedException {
        File file = new File("../Data/MockAPI/ResponseBody/crm/CodStatusFFResponse");
        String body = FileUtils.readFileToString(file);
        body = body.replace("${reason}", reason);
        System.out.println(body);
        wireMockHelper.setupStub("api/v1/fraud-detection-service/cod/status\\?customerId="+customerId, 200, "application/json", body,0);

    }

    public void stubCodStatusFF (String customerId, String pendingCancellationFees){

    }

    public void stubRefundStatus(String orderId, String paymentMode) throws IOException {
        File file = new File("../Data/MockAPI/ResponseBody/crm/CheckoutServiceRefundStatusResponse");
        String body = FileUtils.readFileToString(file);
        body = body.replace("${paymentmode}", paymentMode);
        System.out.println(body);
        wireMockHelper.setupStub("/api/v1/order/"+orderId+"/refundStatus", 200, "application/json", body,0);
    }

    public void stubRefundDetails(String userId, String paymentMode) throws IOException {
        File file = new File("../Data/MockAPI/ResponseBody/crm/CheckoutServiceRefundDetailsResponse");
        String body = FileUtils.readFileToString(file);
        body = body.replace("${paymentmode}", paymentMode);
        System.out.println(body);
        wireMockHelper.setupStub("api/v1/user/"+userId+"/orderRefundDetails", 200, "application/json", body,0);
    }


    public void connectDB(String name, String patch){
        String queries [] =null;
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(name);
        ArrayList<String> dbPatch = runDBPatch(name, patch);
        queries = dbPatch.toArray(queries);
        sqlTemplate.executeBatch(queries);
    }

    public  ArrayList<String> runDBPatch(String dbname, String patch){
        ArrayList<String> queryList = new ArrayList<String>();
        String urls = null;
        String query = null;
        switch (dbname){
            case "helpcenter" :
                urls = CRMConstants.HP_CHECKOUT_SERVICE+","+CRMConstants.HP_DELIVERY_HEIMDALL+","+CRMConstants.HP_DELIVERY_SERVICE+","+
                    CRMConstants.HP_FF_COD+","+CRMConstants.HP_VENDOR_SERVICE+","+CRMConstants.HP_FF_SERVICE;
                if(patch == "mock")
                    query = "update external_client_config set value="+CRMConstants.MOCK_SERVER+" where name in ("+urls+")";
                queryList.add(query);
                break;
            case "swiggy_chat_middleware" :
                break;
            case "post_order_services" :
                break;
            case "swiggy_cc_services":
                urls = CRMConstants.CC_CHECKOUT_URL+","+CRMConstants.CC_FF_URL+","+CRMConstants.CC_DATA_SCIENCE_URL+","+
                        CRMConstants.CC_RNG_URL+","+CRMConstants.CC_RNG_USER_SEG_URL;
                if(patch == "mock")
                    query = "update external_client_config set value="+CRMConstants.MOCK_SERVER+" where name in ("+urls+")";
                queryList.add(query);
                break;
        }
        List<String> urlList = Arrays.asList(urls.split(","));
        if(patch == "integrated") {
            for (int i = 0; i < urlList.size(); i++) {
                String value = null;
                if(urlList.get(i).contains("checkout"))
                    value = "checkout";
                else if(urlList.get(i).contains("delivery.heimdall"))
                    value = "";
                else if(urlList.get(i).contains("delivery.service"))
                    value = "deliveryservice";
                else if(urlList.get(i).contains("fulfillment.cod.service"))
                    value = "fraudservice";
                else if(urlList.get(i).contains("fulfillment.service") || urlList.get(i).contains("ff.service."))
                    value = "oms";
                else if(urlList.get(i).contains("vendor.service"))
                    value = "edittimeservice";
                else if(urlList.get(i).contains("data.science.service"))
                    value = "http://10.0.13.231:8082";
                else if(urlList.get(i).contains("rng.service"))
                    value = "cs";
                else if(urlList.get(i).contains("rng.user.segment"))
                    value = "http://seg-u-cuat-03.swiggy";
                value = value.replace("swiggyint.in","swiggyops.de");
                query = "update external_client_config set value='"+value+"'"+" where name=" + urlList.get(i);
                queryList.add(query);
            }
        }
        return queryList;
        }
}
