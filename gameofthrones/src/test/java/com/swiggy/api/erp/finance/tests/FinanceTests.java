package com.swiggy.api.erp.finance.tests;
import com.swiggy.api.erp.ff.helper.LOSHelper;
import com.swiggy.api.erp.finance.dp.FinanceData;
import com.swiggy.api.erp.finance.helper.FinanceHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.HashMap;

public class FinanceTests extends FinanceData{

    LOSHelper losHelper = new LOSHelper();

    FinanceHelper helper = new FinanceHelper();
    Initialize gameofthrones = new Initialize();


    @Test (priority = 1, dataProvider = "createOrderData", groups = {"regression"}, threadPoolSize = 1, invocationCount = 1)
    public void createOrderTests(String restType,  String scenario, String[] orderstatus ) throws IOException, InterruptedException {
       System.out.println("#################### "+scenario+" started ##################################");

       // Creating Order from Selected Restaurant
        String orderId = helper.createOrderFromSelectedRestaurant(restType);
        System.out.println(orderId);

        for(int i=0;i<orderstatus.length;i++){
            //Update the created order for the given status via central rmq
            losHelper.statusUpdate(orderId, helper.getCurrentEpochtime(), orderstatus[i]);

            //Get recon order api and validate the created order and its status
            GameOfThronesService reconOrder = new GameOfThronesService("fin","getReconOrder",gameofthrones);
            HashMap<String,String> headers = new HashMap<String, String>();
            headers.put("Content-Type","application/json");
            Processor reconOrderresponse = new Processor(reconOrder,headers,null,new String[]{orderId});
            Assert.assertEquals(reconOrderresponse.ResponseValidator.GetNodeValue("order_id"),orderId);
            Assert.assertEquals(reconOrderresponse.ResponseValidator.GetNodeValue("order_status"),orderstatus[i]);
        }

    }

    @Test (priority = 2, dataProvider = "cancelOrderData", groups = {"regression"})
    public void cancelOrderTests(String restType, String foodprepared, String responsible, String scenario) throws IOException, InterruptedException {

        System.out.println("#################### "+scenario+" started ##################################");

        //Create Order from selected restaurant using RMQ
        String orderId = helper.createOrderFromSelectedRestaurant(restType);

        //Cancellation of  created order
        helper.cancelOrderTests(orderId, foodprepared, responsible );


        System.out.println("*********************** "+scenario+" completed ********************");
        }

}
