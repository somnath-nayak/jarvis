
package com.swiggy.api.erp.cms.pojo.PricingService;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.swiggy.api.erp.cms.constants.CatalogV2Constants;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "mrp",
    "store"
})
public class Price_list {

    @JsonProperty("mrp")
    private Long mrp;
    @JsonProperty("store")
    private Long store;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Price_list() {

    }

    /**
     * 
     * @param store
     * @param mrp
     */
    public Price_list(Long mrp, Long store) {
        super();
        this.mrp = mrp;
        this.store = store;
    }

    @JsonProperty("mrp")
    public Long getMrp() {
        return mrp;
    }

    @JsonProperty("mrp")
    public void setMrp(Long mrp) {
        this.mrp = mrp;
    }

    public Price_list withMrp(Long mrp) {
        this.mrp = mrp;
        return this;
    }

    @JsonProperty("store")
    public Long getStore() {
        return store;
    }

    @JsonProperty("store")
    public void setStore(Long store) {
        this.store = store;
    }

    public Price_list withStore(Long store) {
        this.store = store;
        return this;
    }

    public Price_list settValue(long mrp,long storeprice){
        this.withMrp(mrp).withStore(storeprice);
        return this;
    }


}
