package com.swiggy.api.erp.ff.tests.placingService;

import com.swiggy.api.erp.ff.dp.placingService.PlacingServiceConsumerTestData;
import com.swiggy.api.erp.ff.helper.OMSHelper;
import com.swiggy.api.erp.ff.helper.PlacingServiceHelper;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.concurrent.TimeoutException;


public class PlacingServiceConsumerTest extends PlacingServiceConsumerTestData {

    PlacingServiceHelper helper = new PlacingServiceHelper();
    OMSHelper omsHelper = new OMSHelper();

    @Test(dataProvider = "placingConsumerTestdata", description = "Test placing service consumers by publishing updates in exchange")
    public void placingConsumerTests(String timeStamp, String orderId, String toState, String scenario) throws IOException, TimeoutException {
        System.out.println("***************************************** placingConsumerTests "+scenario+" started *****************************************");

        omsHelper.verifyOrder(orderId, "111");
        helper.publishPlacingEvent(timeStamp, orderId, toState);
        Assert.assertTrue(helper.verifyPlacingStatusAndTimeInDB(orderId, timeStamp, scenario));

        System.out.println("######################################### placingConsumerTests "+scenario+" compleated #########################################");
    }

    @Test(dataProvider = "placingConsumerDeadLetterTestdata", description = "Test placing service dead letter by publishing wrong message")
    public void placingConsumerTestsWithWrongMessageDeadLetter(String timeStamp, String orderId, String toState, String scenario) throws IOException, TimeoutException, InterruptedException {
        System.out.println("***************************************** placingConsumerTestsWithWrongMessageDeadLetter "+scenario+" started *****************************************");

//        omsHelper.verifyOrder(orderId, "111");
//        helper.publishPlacingEvent(timeStamp, orderId, toState);
        helper.publishWrongMesagePlacingEvent();
        Thread.sleep(30000);
        Assert.assertTrue(helper.isWrongMessagePresentInDeadLetters());

        System.out.println("######################################### placingConsumerTestsWithWrongMessageDeadLetter "+scenario+" compleated #########################################");
    }
}
