package com.swiggy.api.erp.finance.pojo;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.erp.finance.pojo
 **/

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "deId",
        "ops",
        "description",
        "orderId",
        "deIncoming",
        "deOutgoing",
        "txType"
})

public class TransactionList {

    @JsonProperty("deId")
    private Integer deId;
    @JsonProperty("ops")
    private Ops ops;
    @JsonProperty("description")
    private String description;
    @JsonProperty("orderId")
    private String orderId;
    @JsonProperty("deIncoming")
    private Integer deIncoming;
    @JsonProperty("deOutgoing")
    private Integer deOutgoing;
    @JsonProperty("txType")
    private String txType;

    @JsonProperty("deId")
    public Integer getDeId() {
        return deId;
    }

    @JsonProperty("deId")
    public void setDeId(Integer deId) {
        this.deId = deId;
    }

    @JsonProperty("ops")
    public Ops getOps() {
        return ops;
    }

    @JsonProperty("ops")
    public void setOps(Ops ops) {
        this.ops = ops;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("orderId")
    public String getOrderId() {
        return orderId;
    }

    @JsonProperty("orderId")
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    @JsonProperty("deIncoming")
    public Integer getDeIncoming() {
        return deIncoming;
    }

    @JsonProperty("deIncoming")
    public void setDeIncoming(Integer deIncoming) {
        this.deIncoming = deIncoming;
    }

    @JsonProperty("deOutgoing")
    public Integer getDeOutgoing() {
        return deOutgoing;
    }

    @JsonProperty("deOutgoing")
    public void setDeOutgoing(Integer deOutgoing) {
        this.deOutgoing = deOutgoing;
    }

    @JsonProperty("txType")
    public String getTxType() {
        return txType;
    }

    @JsonProperty("txType")
    public void setTxType(String txType) {
        this.txType = txType;
    }

    private void setDefaultValues(int de_id, int de_incoming, int de_outgoing, String orderID, String txnType ) {
        Ops ops = new Ops();
        ops.build(de_id);
        if (this.getDeId() == null)
            this.setDeId(de_id);
        if (this.getDeIncoming() == null)
            this.setDeIncoming(de_incoming);
        if (this.getDeOutgoing() == null)
            this.setDeOutgoing(de_outgoing);
        if (this.getDescription() == null)
            this.setDescription("DE cash automation test");
        if (this.getOrderId() == null)
            this.setOrderId(orderID);
        if (this.getTxType() == null)
            this.setTxType(txnType);
        if (this.getOps() == null)
            this.setOps(ops);
    }

    public TransactionList build(int de_id, int de_incoming, int de_outgoing, String orderID,String txnType) {
        setDefaultValues(de_id,de_incoming,de_outgoing,orderID,txnType);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("deId", deId).append("ops", ops).append("description", description).append("orderId", orderId).append("deIncoming", deIncoming).append("deOutgoing", deOutgoing).append("txType", txType).toString();
    }

}
