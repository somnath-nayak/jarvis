package com.swiggy.api.erp.cms.pojo.SelfServeV1Item;

public class Variants_v2 {
	 private Variant_groups[] variant_groups;

	    private Pricing_models[] pricing_models;

	    public Variant_groups[] getVariant_groups ()
	    {
	        return variant_groups;
	    }

	    public void setVariant_groups (Variant_groups[] variant_groups)
	    {
	        this.variant_groups = variant_groups;
	    }

	    public Pricing_models[] getPricing_models ()
	    {
	        return pricing_models;
	    }

	    public void setPricing_models (Pricing_models[] pricing_models)
	    {
	        this.pricing_models = pricing_models;
	    }

	    @Override
	    public String toString()
	    {
	        return "ClassPojo [variant_groups = "+variant_groups+", pricing_models = "+pricing_models+"]";
	    }
	}
				
				