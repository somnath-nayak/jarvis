package com.swiggy.api.erp.vms.fssaiservice.pojo;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"id",
"user_id",
"user_type",
"user_meta",
"restaurant_id",
"registered_name",
"address_line_1",
"address_line_2",
"official_city_id",
"official_city",
"zipcode",
"document_url",
"status",
"fssai_licence_number",
"fssai_licence_validity",
"last_updated_at",
"acknowledgement_reference",
"application_date",
"fssai_status",
"fssailicenceValidity",
"owner_contact_number",
"owner_name"
})
public class FssaiLicenseUpdateBuilder {

@JsonProperty("id")
private Long id;
@JsonProperty("user_id")
private String user_id;
@JsonProperty("user_type")
private String user_type;
@JsonProperty("user_meta")
private String user_meta;
@JsonProperty("restaurant_id")
private Long restaurant_id;
@JsonProperty("registered_name")
private String registered_name;
@JsonProperty("address_line_1")
private String address_line_1;
@JsonProperty("address_line_2")
private String address_line_2;
@JsonProperty("official_city_id")
private Long official_city_id;
@JsonProperty("official_city")
private String official_city;
@JsonProperty("zipcode")
private String zipcode;
@JsonProperty("document_url")
private String document_url;
@JsonProperty("status")
private Long status;
@JsonProperty("fssai_licence_number")
private String fssai_licence_number;
@JsonProperty("fssai_licence_validity")
private String fssai_licence_validity;
@JsonProperty("last_updated_at")
private String last_updated_at;
@JsonProperty("acknowledgement_reference")
private String acknowledgement_reference;
@JsonProperty("application_date")
private String application_date;
@JsonProperty("fssai_status")
private String fssai_status;
@JsonProperty("fssailicenceValidity")
private String fssailicenceValidity;
@JsonProperty("owner_contact_number")
private String owner_contact_number;
@JsonProperty("owner_name")
private String owner_name;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

/**
* No args constructor for use in serialization
* 
*/
public FssaiLicenseUpdateBuilder() {
}

/**
* 
* @param official_city_id
* @param owner_name
* @param status
* @param user_type
* @param user_meta
* @param application_date
* @param zipcode
* @param restaurant_id
* @param acknowledgement_reference
* @param owner_contact_number
* @param id
* @param fssai_licence_number
* @param last_updated_at
* @param address_line_1
* @param fssailicenceValidity
* @param fssai_licence_validity
* @param registered_name
* @param address_line_2
* @param fssai_status
* @param user_id
* @param official_city
* @param document_url
*/
public FssaiLicenseUpdateBuilder(Long id, String user_id, String user_type, String user_meta, Long restaurant_id, String registered_name, String address_line_1, String address_line_2, Long official_city_id, String official_city, String zipcode, String document_url, Long status, String fssai_licence_number, String fssai_licence_validity, String last_updated_at, String acknowledgement_reference, String application_date, String fssai_status, String fssailicenceValidity, String owner_contact_number, String owner_name) {
super();
this.id = id;
this.user_id = user_id;
this.user_type = user_type;
this.user_meta = user_meta;
this.restaurant_id = restaurant_id;
this.registered_name = registered_name;
this.address_line_1 = address_line_1;
this.address_line_2 = address_line_2;
this.official_city_id = official_city_id;
this.official_city = official_city;
this.zipcode = zipcode;
this.document_url = document_url;
this.status = status;
this.fssai_licence_number = fssai_licence_number;
this.fssai_licence_validity = fssai_licence_validity;
this.last_updated_at = last_updated_at;
this.acknowledgement_reference = acknowledgement_reference;
this.application_date = application_date;
this.fssai_status = fssai_status;
this.fssailicenceValidity = fssailicenceValidity;
this.owner_contact_number = owner_contact_number;
this.owner_name = owner_name;
}

@JsonProperty("id")
public Long getId() {
return id;
}

@JsonProperty("id")
public void setId(Long id) {
this.id = id;
}

public FssaiLicenseUpdateBuilder withId(Long id) {
this.id = id;
return this;
}

@JsonProperty("user_id")
public String getUser_id() {
return user_id;
}

@JsonProperty("user_id")
public void setUser_id(String user_id) {
this.user_id = user_id;
}

public FssaiLicenseUpdateBuilder withUser_id(String user_id) {
this.user_id = user_id;
return this;
}

@JsonProperty("user_type")
public String getUser_type() {
return user_type;
}

@JsonProperty("user_type")
public void setUser_type(String user_type) {
this.user_type = user_type;
}

public FssaiLicenseUpdateBuilder withUser_type(String user_type) {
this.user_type = user_type;
return this;
}

@JsonProperty("user_meta")
public String getUser_meta() {
return user_meta;
}

@JsonProperty("user_meta")
public void setUser_meta(String user_meta) {
this.user_meta = user_meta;
}

public FssaiLicenseUpdateBuilder withUser_meta(String user_meta) {
this.user_meta = user_meta;
return this;
}

@JsonProperty("restaurant_id")
public Long getRestaurant_id() {
return restaurant_id;
}

@JsonProperty("restaurant_id")
public void setRestaurant_id(Long restaurant_id) {
this.restaurant_id = restaurant_id;
}

public FssaiLicenseUpdateBuilder withRestaurant_id(Long restaurant_id) {
this.restaurant_id = restaurant_id;
return this;
}

@JsonProperty("registered_name")
public String getRegistered_name() {
return registered_name;
}

@JsonProperty("registered_name")
public void setRegistered_name(String registered_name) {
this.registered_name = registered_name;
}

public FssaiLicenseUpdateBuilder withRegistered_name(String registered_name) {
this.registered_name = registered_name;
return this;
}

@JsonProperty("address_line_1")
public String getAddress_line_1() {
return address_line_1;
}

@JsonProperty("address_line_1")
public void setAddress_line_1(String address_line_1) {
this.address_line_1 = address_line_1;
}

public FssaiLicenseUpdateBuilder withAddress_line_1(String address_line_1) {
this.address_line_1 = address_line_1;
return this;
}

@JsonProperty("address_line_2")
public String getAddress_line_2() {
return address_line_2;
}

@JsonProperty("address_line_2")
public void setAddress_line_2(String address_line_2) {
this.address_line_2 = address_line_2;
}

public FssaiLicenseUpdateBuilder withAddress_line_2(String address_line_2) {
this.address_line_2 = address_line_2;
return this;
}

@JsonProperty("official_city_id")
public Long getOfficial_city_id() {
return official_city_id;
}

@JsonProperty("official_city_id")
public void setOfficial_city_id(Long official_city_id) {
this.official_city_id = official_city_id;
}

public FssaiLicenseUpdateBuilder withOfficial_city_id(Long official_city_id) {
this.official_city_id = official_city_id;
return this;
}

@JsonProperty("official_city")
public String getOfficial_city() {
return official_city;
}

@JsonProperty("official_city")
public void setOfficial_city(String official_city) {
this.official_city = official_city;
}

public FssaiLicenseUpdateBuilder withOfficial_city(String official_city) {
this.official_city = official_city;
return this;
}

@JsonProperty("zipcode")
public String getZipcode() {
return zipcode;
}

@JsonProperty("zipcode")
public void setZipcode(String zipcode) {
this.zipcode = zipcode;
}

public FssaiLicenseUpdateBuilder withZipcode(String zipcode) {
this.zipcode = zipcode;
return this;
}

@JsonProperty("document_url")
public String getDocument_url() {
return document_url;
}

@JsonProperty("document_url")
public void setDocument_url(String document_url) {
this.document_url = document_url;
}

public FssaiLicenseUpdateBuilder withDocument_url(String document_url) {
this.document_url = document_url;
return this;
}

@JsonProperty("status")
public Long getStatus() {
return status;
}

@JsonProperty("status")
public void setStatus(Long status) {
this.status = status;
}

public FssaiLicenseUpdateBuilder withStatus(Long status) {
this.status = status;
return this;
}

@JsonProperty("fssai_licence_number")
public String getFssai_licence_number() {
return fssai_licence_number;
}

@JsonProperty("fssai_licence_number")
public void setFssai_licence_number(String fssai_licence_number) {
this.fssai_licence_number = fssai_licence_number;
}

public FssaiLicenseUpdateBuilder withFssai_licence_number(String fssai_licence_number) {
this.fssai_licence_number = fssai_licence_number;
return this;
}

@JsonProperty("fssai_licence_validity")
public String getFssai_licence_validity() {
return fssai_licence_validity;
}

@JsonProperty("fssai_licence_validity")
public void setFssai_licence_validity(String fssai_licence_validity) {
this.fssai_licence_validity = fssai_licence_validity;
}

public FssaiLicenseUpdateBuilder withFssai_licence_validity(String fssai_licence_validity) {
this.fssai_licence_validity = fssai_licence_validity;
return this;
}

@JsonProperty("last_updated_at")
public String getLast_updated_at() {
return last_updated_at;
}

@JsonProperty("last_updated_at")
public void setLast_updated_at(String last_updated_at) {
this.last_updated_at = last_updated_at;
}

public FssaiLicenseUpdateBuilder withLast_updated_at(String last_updated_at) {
this.last_updated_at = last_updated_at;
return this;
}

@JsonProperty("acknowledgement_reference")
public String getAcknowledgement_reference() {
return acknowledgement_reference;
}

@JsonProperty("acknowledgement_reference")
public void setAcknowledgement_reference(String acknowledgement_reference) {
this.acknowledgement_reference = acknowledgement_reference;
}

public FssaiLicenseUpdateBuilder withAcknowledgement_reference(String acknowledgement_reference) {
this.acknowledgement_reference = acknowledgement_reference;
return this;
}

@JsonProperty("application_date")
public String getApplication_date() {
return application_date;
}

@JsonProperty("application_date")
public void setApplication_date(String application_date) {
this.application_date = application_date;
}

public FssaiLicenseUpdateBuilder withApplication_date(String application_date) {
this.application_date = application_date;
return this;
}

@JsonProperty("fssai_status")
public String getFssai_status() {
return fssai_status;
}

@JsonProperty("fssai_status")
public void setFssai_status(String fssai_status) {
this.fssai_status = fssai_status;
}

public FssaiLicenseUpdateBuilder withFssai_status(String fssai_status) {
this.fssai_status = fssai_status;
return this;
}

@JsonProperty("fssailicenceValidity")
public String getFssailicenceValidity() {
return fssailicenceValidity;
}

@JsonProperty("fssailicenceValidity")
public void setFssailicenceValidity(String fssailicenceValidity) {
this.fssailicenceValidity = fssailicenceValidity;
}

public FssaiLicenseUpdateBuilder withFssailicenceValidity(String fssailicenceValidity) {
this.fssailicenceValidity = fssailicenceValidity;
return this;
}

@JsonProperty("owner_contact_number")
public String getOwner_contact_number() {
return owner_contact_number;
}

@JsonProperty("owner_contact_number")
public void setOwner_contact_number(String owner_contact_number) {
this.owner_contact_number = owner_contact_number;
}

public FssaiLicenseUpdateBuilder withOwner_contact_number(String owner_contact_number) {
this.owner_contact_number = owner_contact_number;
return this;
}

@JsonProperty("owner_name")
public String getOwner_name() {
return owner_name;
}

@JsonProperty("owner_name")
public void setOwner_name(String owner_name) {
this.owner_name = owner_name;
}

public FssaiLicenseUpdateBuilder withOwner_name(String owner_name) {
this.owner_name = owner_name;
return this;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

public FssaiLicenseUpdateBuilder withAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
return this;
}

@Override
public String toString() {
return new ToStringBuilder(this).append("id", id).append("user_id", user_id).append("user_type", user_type).append("user_meta", user_meta).append("restaurant_id", restaurant_id).append("registered_name", registered_name).append("address_line_1", address_line_1).append("address_line_2", address_line_2).append("official_city_id", official_city_id).append("official_city", official_city).append("zipcode", zipcode).append("document_url", document_url).append("status", status).append("fssai_licence_number", fssai_licence_number).append("fssai_licence_validity", fssai_licence_validity).append("last_updated_at", last_updated_at).append("acknowledgement_reference", acknowledgement_reference).append("application_date", application_date).append("fssai_status", fssai_status).append("fssailicenceValidity", fssailicenceValidity).append("owner_contact_number", owner_contact_number).append("owner_name", owner_name).append("additionalProperties", additionalProperties).toString();
}

@Override
public int hashCode() {
return new HashCodeBuilder().append(official_city_id).append(owner_name).append(status).append(user_type).append(user_meta).append(application_date).append(zipcode).append(restaurant_id).append(acknowledgement_reference).append(owner_contact_number).append(id).append(fssai_licence_number).append(last_updated_at).append(address_line_1).append(additionalProperties).append(fssailicenceValidity).append(fssai_licence_validity).append(registered_name).append(address_line_2).append(fssai_status).append(official_city).append(user_id).append(document_url).toHashCode();
}

@Override
public boolean equals(Object other) {
if (other == this) {
return true;
}
if ((other instanceof FssaiLicenseUpdateBuilder) == false) {
return false;
}
FssaiLicenseUpdateBuilder rhs = ((FssaiLicenseUpdateBuilder) other);
return new EqualsBuilder().append(official_city_id, rhs.official_city_id).append(owner_name, rhs.owner_name).append(status, rhs.status).append(user_type, rhs.user_type).append(user_meta, rhs.user_meta).append(application_date, rhs.application_date).append(zipcode, rhs.zipcode).append(restaurant_id, rhs.restaurant_id).append(acknowledgement_reference, rhs.acknowledgement_reference).append(owner_contact_number, rhs.owner_contact_number).append(id, rhs.id).append(fssai_licence_number, rhs.fssai_licence_number).append(last_updated_at, rhs.last_updated_at).append(address_line_1, rhs.address_line_1).append(additionalProperties, rhs.additionalProperties).append(fssailicenceValidity, rhs.fssailicenceValidity).append(fssai_licence_validity, rhs.fssai_licence_validity).append(registered_name, rhs.registered_name).append(address_line_2, rhs.address_line_2).append(fssai_status, rhs.fssai_status).append(official_city, rhs.official_city).append(user_id, rhs.user_id).append(document_url, rhs.document_url).isEquals();
}

}
