package com.swiggy.api.erp.cms.dp;

import com.swiggy.api.erp.cms.constants.MenuConstants;
import com.swiggy.api.erp.cms.pojo.BulkVariantGroup.BulkCreateVariantGroup;
import com.swiggy.api.erp.cms.pojo.BulkVariantGroup.Entities;
import com.swiggy.api.erp.cms.pojo.VariantGroups.Entity;
import com.swiggy.api.erp.cms.pojo.VariantGroups.VariantGroup;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by kiran.j on 2/19/18.
 */
public class VariantGroupDP {

    @DataProvider(name = "variantGroup")
    public Iterator<Object[]> createItems() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        VariantGroup variantGroup = new VariantGroup();
        variantGroup.build();
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(variantGroup), new String[]{}});

        Entity entity = variantGroup.getEntity();
        entity.setItem_id("");
        variantGroup.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(variantGroup), MenuConstants.variant_group_itemid});

        variantGroup = new VariantGroup();
        variantGroup.build();
        entity = variantGroup.getEntity();
        entity.setName("");
        variantGroup.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(variantGroup), MenuConstants.variant_group_name});

        variantGroup = new VariantGroup();
        variantGroup.build();
        entity = variantGroup.getEntity();
        entity.setId("");
        variantGroup.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(variantGroup), MenuConstants.variant_group_id});

        variantGroup = new VariantGroup();
        variantGroup.build();
        entity = variantGroup.getEntity();
        entity.setName(null);
        variantGroup.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(variantGroup), new String[]{}});

        variantGroup = new VariantGroup();
        variantGroup.build();
        entity = variantGroup.getEntity();
        entity.setOrder(0);
        variantGroup.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(variantGroup), new String[]{}});

        variantGroup = new VariantGroup();
        variantGroup.build();
        entity = variantGroup.getEntity();
        entity.setOrder(1);
        variantGroup.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(variantGroup), new String[]{}});

        variantGroup = new VariantGroup();
        variantGroup.build();
        entity = variantGroup.getEntity();
        entity.setItem_id(null);
        variantGroup.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(variantGroup), MenuConstants.variant_group_itemid});

        variantGroup = new VariantGroup();
        variantGroup.build();
        entity = variantGroup.getEntity();
        entity.setId(null);
        variantGroup.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(variantGroup), MenuConstants.variant_group_id});

        return obj.iterator();
    }

    @DataProvider(name = "updatevariantGroup")
    public Iterator<Object[]> updatevariantGroup() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        VariantGroup variantGroup = new VariantGroup();
        variantGroup.build();
        obj.add(new Object[]{MenuConstants.rest_id, variantGroup, new String[]{}});

        variantGroup = new VariantGroup();
        variantGroup.build();
        Entity entity = variantGroup.getEntity();
        entity.setItem_id("");
        variantGroup.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id,variantGroup, MenuConstants.variant_group_itemid});

        variantGroup = new VariantGroup();
        variantGroup.build();
        entity = variantGroup.getEntity();
        entity.setName("");
        variantGroup.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, variantGroup, MenuConstants.variant_group_name});

        variantGroup = new VariantGroup();
        variantGroup.build();
        entity = variantGroup.getEntity();
        entity.setName(null);
        variantGroup.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, variantGroup, new String[]{}});

        variantGroup = new VariantGroup();
        variantGroup.build();
        entity = variantGroup.getEntity();
        entity.setOrder(0);
        variantGroup.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, variantGroup, new String[]{}});

        variantGroup = new VariantGroup();
        variantGroup.build();
        entity = variantGroup.getEntity();
        entity.setOrder(1);
        variantGroup.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, variantGroup, new String[]{}});

        variantGroup = new VariantGroup();
        variantGroup.build();
        entity = variantGroup.getEntity();
        entity.setItem_id(null);
        variantGroup.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, variantGroup, MenuConstants.variant_group_itemid});

        return obj.iterator();
    }

    @DataProvider(name = "bulkvariantGroup")
    public Iterator<Object[]> bulkvariantGroup() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        BulkCreateVariantGroup variantGroup = new BulkCreateVariantGroup();
        variantGroup.build();
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variantGroup), new String[]{}, MenuConstants.token_id3});

        Entities entities = variantGroup.getEntities()[0];
        entities.setId(null);
        variantGroup.setEntities(new Entities[]{entities});
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variantGroup), MenuConstants.bulkaddvariantgroup_id, MenuConstants.token_id3});

        entities = variantGroup.getEntities()[0];
        entities.build();
        entities.setItem_id(null);
        variantGroup.setEntities(new Entities[]{entities});
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variantGroup), MenuConstants.getBulkaddvariantgroup_itemid, MenuConstants.token_id3});

        entities = variantGroup.getEntities()[0];
        entities.build();
        entities.setName(null);
        variantGroup.setEntities(new Entities[]{entities});
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variantGroup), new String[]{}, MenuConstants.token_id3});

        entities = variantGroup.getEntities()[0];
        entities.build();
        entities.setOrder(0);
        variantGroup.setEntities(new Entities[]{entities});
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variantGroup), new String[]{}, MenuConstants.token_id3});

        entities = variantGroup.getEntities()[0];
        entities.build();
        entities.setOrder(1);
        variantGroup.setEntities(new Entities[]{entities});
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variantGroup), new String[]{}, MenuConstants.token_id3});

        return obj.iterator();
    }

    @DataProvider(name = "bulkupdatevariantGroup")
    public Iterator<Object[]> bulkupdatevariantGroup() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        BulkCreateVariantGroup variantGroup = new BulkCreateVariantGroup();
        variantGroup.build();
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variantGroup), new String[]{}, MenuConstants.token_id3});

        Entities entities = variantGroup.getEntities()[0];
        entities.setId(null);
        variantGroup.setEntities(new Entities[]{entities});
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variantGroup), MenuConstants.bulkaddvariantgroup_id, MenuConstants.token_id3});

        entities = variantGroup.getEntities()[0];
        entities.build();
        entities.setItem_id(null);
        variantGroup.setEntities(new Entities[]{entities});
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variantGroup), MenuConstants.getBulkaddvariantgroup_itemid, MenuConstants.token_id3});

        entities = variantGroup.getEntities()[0];
        entities.build();
        entities.setName(null);
        variantGroup.setEntities(new Entities[]{entities});
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variantGroup), new String[]{}, MenuConstants.token_id3});

        entities = variantGroup.getEntities()[0];
        entities.build();
        entities.setOrder(0);
        variantGroup.setEntities(new Entities[]{entities});
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variantGroup), new String[]{}, MenuConstants.token_id3});

        entities = variantGroup.getEntities()[0];
        entities.build();
        entities.setOrder(1);
        variantGroup.setEntities(new Entities[]{entities});
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variantGroup), new String[]{}, MenuConstants.token_id3});

        return obj.iterator();
    }
}
