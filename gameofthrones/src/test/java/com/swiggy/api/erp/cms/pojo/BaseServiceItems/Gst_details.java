package com.swiggy.api.erp.cms.pojo.BaseServiceItems;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.erp.cms.pojo.BaseServiceItems
 **/
public class Gst_details {
    private String inclusive;

    private String igst;

    private String cgst;

    private String sgst;

    public String getInclusive ()
    {
        return inclusive;
    }

    public void setInclusive (String inclusive)
    {
        this.inclusive = inclusive;
    }

    public String getIgst ()
    {
        return igst;
    }

    public void setIgst (String igst)
    {
        this.igst = igst;
    }

    public String getCgst ()
    {
        return cgst;
    }

    public void setCgst (String cgst)
    {
        this.cgst = cgst;
    }

    public String getSgst ()
    {
        return sgst;
    }

    public void setSgst (String sgst)
    {
        this.sgst = sgst;
    }


    public void setDefaultValues() {
        if(this.getIgst() == null)
            this.setIgst("[QUANTITY]*0.2");
        if(this.getCgst() == null)
            this.setCgst("[QUANTITY]*0.2");
        if(this.getSgst() == null)
            this.setSgst("[QUANTITY]*0.2");
        if(this.getInclusive() == null)
            this.setInclusive("true");
    }

    public Gst_details build() {
        setDefaultValues();
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("cgst", cgst).append("igst", igst).append("inclusive", inclusive).append("sgst", sgst).toString();
    }
}
