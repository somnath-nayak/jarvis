package com.swiggy.api.erp.vms.helper;


import com.swiggy.api.erp.vms.constants.RMSConstants;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import java.util.HashMap;


public class VendorInsightsHelper extends RMSConstants{

	Initialize initializer=new Initialize();

	public Processor getRestaurants(String accessToken, String username_VI,String rest_ids,String start_date,String end_date,String group_by){


		GameOfThronesService services = new GameOfThronesService("vendorinsights", "getrestaurants_VI", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("accesstoken", accessToken);
		Processor processor = new Processor(services, requestheaders,
				new String[] { username_VI, rest_ids, start_date, end_date, group_by });

		return processor;

	}

	public Processor orderForRest(String user_id, String rest_id, String start_date, String end_date, String group_by,String accessToken) {

		GameOfThronesService services = new GameOfThronesService("vendorinsights", "orderForRest_VI", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("accesstoken", accessToken);	
		Processor processor = new Processor(services, requestheaders,
				new String[] { user_id, rest_id, start_date, end_date, group_by });
		return processor;
	}

	public Processor helpContext(String accessToken, String rest_id, String user_id) {

		GameOfThronesService services = new GameOfThronesService("vendorinsights", "helpContext_VI", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");

		requestheaders.put("accesstoken", accessToken);
		Processor processor = new Processor(services, requestheaders, new String[] { rest_id, user_id });

		return processor;

	}

	public Processor orderFeedback(String accessToken, String order_id, String rest_id, String user_id) {

		GameOfThronesService services = new GameOfThronesService("vendorinsights", "orderFeedback_VI", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");

		requestheaders.put("accesstoken", accessToken);
		Processor processor = new Processor(services, requestheaders, new String[] {order_id, rest_id, user_id });

		return processor;

	}

	public Processor lastUpdatedTime(String accessToken, String rest_id) {

		GameOfThronesService services = new GameOfThronesService("vendorinsights", "lastUpdatedTime_VI", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");

		requestheaders.put("accesstoken", accessToken);
		Processor processor = new Processor(services, requestheaders, new String[] { rest_id });

		return processor;

	}

	public Processor confirmationTime(String accessToken, String user_id, String rest_id, String start_date, String end_date,
			String group_by1) {

		GameOfThronesService services = new GameOfThronesService("vendorinsights", "confirmationTime_VI", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");

		requestheaders.put("accesstoken", accessToken);
		Processor processor = new Processor(services, requestheaders,
				new String[] { user_id, rest_id, start_date, end_date, group_by1 });

		return processor;
	}

	public Processor orderForRestWeekly(String accessToken, String user_id, String rest_id2, String start_date_weekly,
			String end_date_weekly, String group_by_weekly) {

		GameOfThronesService services = new GameOfThronesService("vendorinsights", "orderForRest_VI", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("accesstoken", accessToken);
		Processor processor = new Processor(services, requestheaders,
				new String[] { user_id, rest_id2, start_date_weekly, end_date_weekly, group_by_weekly });
		return processor;
	}

	public Processor orderForRestMonthly(String accessToken, String user_id, String rest_id, String start_date_monthly,
			String end_date_monthly, String group_by_mothly) {

		GameOfThronesService services = new GameOfThronesService("vendorinsights", "orderForRest_VI", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("accesstoken", accessToken);
		Processor processor = new Processor(services, requestheaders,
				new String[] { user_id, rest_id, start_date, end_date, group_by });
		return processor;
	}

	public Processor metricsForRest(String user_id, String rest_id, String start_date, String end_date,String group_by,String accessToken) {

		GameOfThronesService services = new GameOfThronesService("vendorinsights", "metricsForRest_VI", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("accesstoken", accessToken);
		Processor processor = new Processor(services, requestheaders,new String[] { user_id, rest_id, start_date, end_date, group_by });
		return processor;
	}

	public Processor ratingsForRest(String start_date, String end_date, String rest_id,String accessToken) {

		GameOfThronesService services = new GameOfThronesService("vendorinsights", "ratingsForRest_VI", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("accesstoken", accessToken);
		Processor processor = new Processor(services, requestheaders, new String[] { start_date, end_date, rest_id });
		return processor;
	}

	public Processor ordersForRatings(String rest_id, String in_ratings, String user_id,String accessToken) {

		GameOfThronesService services = new GameOfThronesService("vendorinsights", "getordersforRating_VI",
				initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("accesstoken", accessToken);
		Processor processor = new Processor(services, requestheaders, null,new String[] { rest_id, in_ratings, user_id });
		return processor;
	}


	public Processor ordersForItems(String start_date,String end_date,String rest_id,String user_id,String accessToken){

		GameOfThronesService services = new GameOfThronesService("vendorinsights", "ordersForItems_VI", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("accesstoken", accessToken);
		Processor processor = new Processor(services, requestheaders,new String[] { start_date, end_date, rest_id, user_id });
		return processor;
	}


	public Processor ordersForCategories(String start_date,String end_date,String rest_id,String user_id,String accessToken){

		GameOfThronesService services = new GameOfThronesService("vendorinsights", "ordersForCategories_VI",
				initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("accesstoken", accessToken);
		Processor processor = new Processor(services, requestheaders,new String[] { start_date, end_date, rest_id, user_id });
		return processor;
	}

	public Processor cancellationReasons(String start_date,String end_date,String rest_id,String user_id,String accessToken){

		GameOfThronesService services = new GameOfThronesService("vendorinsights", "cancellationReasons_VI",
				initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("accesstoken", accessToken);
		Processor processor = new Processor(services, requestheaders,new String[] { start_date, end_date, rest_id, user_id });
		return processor;
	}


	public Processor oosOrdersAndReasons(String start_date,String end_date,String rest_id,String user_id,String group_by_weekly,String accessToken){

		GameOfThronesService services = new GameOfThronesService("vendorinsights", "oosOrdersAndReasons_VI",initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("accesstoken", accessToken);
		Processor processor = new Processor(services, requestheaders,new String[] { start_date, end_date, rest_id, user_id, group_by_weekly });
		return processor;
	}

	public Processor sellerTieringVI(String start_date, String end_date, String rest_id, String user_id, String accessToken){

		GameOfThronesService services = new GameOfThronesService("vendorinsights", "sellerTieringVI",initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("accesstoken", accessToken);
		Processor processor = new Processor(services, requestheaders,new String[] { start_date, end_date, rest_id, user_id });
		return processor;
	}

	public Processor tdMetricsVI(String start_date, String end_date, String rest_id, String type, String offerId, String accessToken){

		GameOfThronesService services = new GameOfThronesService("vendorinsights", "tdMetricsVI",initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("accesstoken", accessToken);
		Processor processor = new Processor(services, requestheaders,new String[] { start_date, end_date, rest_id, type, offerId });
		return processor;
	}

	public Processor getOverview(String accessToken, String rest_id_overview1) {
		GameOfThronesService services = new GameOfThronesService("vendorinsights", "getOverview", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		// requestheaders.put("Content-Type","application/json");
		Processor processor = new Processor(services, requestheaders, null, new String[] { rest_id_overview1 });
		return processor;

	}

	public Processor getRevenue(String accessToken, String rest_id_overview, String startdate, String enddate,
								String group_by_weekly) {
		GameOfThronesService services = new GameOfThronesService("vendorinsights", "getRevenue", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		// requestheaders.put("Content-Type","application/json");
		Processor processor = new Processor(services, requestheaders, null,
				new String[] { rest_id_overview, startdate, enddate, group_by_weekly });
		return processor;

	}

	public Processor getOperations(String accessToken, String rest_id_overview, String startdate, String enddate,
								   String group_by_weekly) {
		GameOfThronesService services = new GameOfThronesService("vendorinsights", "getOperations", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		// requestheaders.put("Content-Type","application/json");
		Processor processor = new Processor(services, requestheaders, null,
				new String[] { rest_id_overview, startdate, enddate, group_by_weekly });
		return processor;

	}

	public Processor getRatings(String accessToken, String rest_id_overview, String startdate, String enddate,
								String group_by_weekly) {
		GameOfThronesService services = new GameOfThronesService("vendorinsights", "getRatings", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		// requestheaders.put("Content-Type","application/json");
		Processor processor = new Processor(services, requestheaders, null,
				new String[] { rest_id_overview, startdate, enddate, group_by_weekly });
		return processor;

	}

	public Processor getMfrReport(String rest_id, String startdate, String enddate) {
		GameOfThronesService services = new GameOfThronesService("vendorinsights", "getMfrReport", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		// requestheaders.put("Content-Type","application/json");
		Processor processor = new Processor(services, requestheaders, null,
				new String[] {rest_id, startdate, enddate});
		return processor;

	}

}
	
	
	