package com.swiggy.api.erp.delivery.helper;

import framework.gameofthrones.JonSnow.Processor;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author preetesh
 * @project swiggy_test
 * @createdon 11/09/18
 */
public class BatchingBuilder
{
    private final String zone_name;
    private final String zone_id;
    private final String city;
    private final String norainmode;
    private final String maxOrdersInBatch;
    private final String maxSlaOfBatch;
    private final String batchingVersion;
    private final String maxOrderDiff;
    private final String maxCustEdge;
    private final String maxBatchElapsedTime;
    private final String maxItemsInBatch;
    private final String maxCustEdgeV2;
    private final String maxOrderDiffV2;
    private final String maxRestEdge;
    private final String maxBill;
    private String updateZoneStatusCode;
    DeliveryServiceHelper deliveryServiceHelper=new DeliveryServiceHelper();

    public String getUpdateZoneStatusCode() {
        return updateZoneStatusCode;
    }

    public static class Builder
    {
        private final String zone_name;
        private final String zone_id;
        private final String city;
        private String norainmode = "0";
        private String maxOrdersInBatch="2";
        private String maxSlaOfBatch="100";
        private String batchingVersion="1";
        private String maxOrderDiff="500";
        private String maxCustEdge="100";
        private String maxBatchElapsedTime="20";
        private String maxItemsInBatch="2";
        private String maxCustEdgeV2="4000";
        private String maxOrderDiffV2="20";
        private String maxRestEdge="5000";
        private String maxBill="5000";

        public Builder maxItemsInBatch(String val)
        {
            maxItemsInBatch=val;
            return this;
        }
        public Builder(String zone_name,String zone_id,String city)
        {
            this.zone_id=zone_id;
            this.zone_name=zone_name;
            this.city=city;
        }

        public Builder norainmode(String val)
        {
            norainmode=val;
            return this;
        }

        public Builder maxOrdersInBatch(String val)
        {
            maxOrdersInBatch=val;
            return this;
        }
        public Builder maxSlaOfBatch(String val)
        {
            maxSlaOfBatch=val;
            return this;
        }
        public Builder batchingVersion(String val)
        {
            batchingVersion=val;
            return this;
        }

        public Builder maxOrderDiff(String val)
        {
            maxOrderDiff=val;
            return this;
        }

        public Builder maxCustEdge(String val)
        {
            maxCustEdge=val;
            return this;
        }

        public Builder maxBatchElapsedTime(String val)
        {
            maxBatchElapsedTime=val;
            return this;
        }

        public Builder maxOrderDiffV2(String val)
        {
            maxOrderDiffV2=val;
            return this;
        }

        public Builder maxRestEdge(String val)
        {
            maxRestEdge=val;
            return this;
        }

        public Builder maxCustEdgeV2(String val)
        {
            maxCustEdgeV2=val;
            return this;
        }

        public Builder maxBill(String val)
        {
            maxBill=val;
            return this;
        }

        public BatchingBuilder build()
        {
            return new BatchingBuilder(this);
        }
    }

    private BatchingBuilder(Builder builder)
    {
        this.zone_id=builder.zone_id;
        this.zone_name=builder.zone_name;
        this.city=builder.city;
        this.norainmode=builder.norainmode;
        this.maxOrdersInBatch=builder.maxOrdersInBatch;
        this.maxSlaOfBatch=builder.maxSlaOfBatch;
        this.batchingVersion=builder.batchingVersion;
        this.maxOrderDiff=builder.maxOrderDiff;
        this.maxCustEdge=builder.maxCustEdge;
        this.maxBatchElapsedTime=builder.maxBatchElapsedTime;
        this.maxItemsInBatch=builder.maxItemsInBatch;
        this.maxCustEdgeV2=builder.maxCustEdgeV2;
        this.maxOrderDiffV2=builder.maxOrderDiffV2;
        this.maxRestEdge=builder.maxRestEdge;
        this.maxBill=builder.maxBill;
        Processor processor=deliveryServiceHelper.updateZoneBatchingParams(zone_id,zone_name,city,norainmode,maxOrdersInBatch,
                maxSlaOfBatch,batchingVersion,maxOrderDiff,maxCustEdge,maxBatchElapsedTime,maxItemsInBatch,
                maxCustEdgeV2,maxOrderDiffV2,maxRestEdge,maxBill);
        try {
            updateZoneStatusCode=new JSONObject(processor.ResponseValidator.GetBodyAsText()).get("statusMessage").toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public static void main(String[] args) {
        BatchingBuilder batchingBuilder=new Builder("BLR_HSR","6","1").batchingVersion("2").build();
    }

}

