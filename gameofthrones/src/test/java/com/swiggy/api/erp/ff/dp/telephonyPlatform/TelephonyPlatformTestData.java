package com.swiggy.api.erp.ff.dp.telephonyPlatform;

import com.swiggy.api.erp.ff.helper.TelephonyPlatformHelper;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.util.HashMap;

public class TelephonyPlatformTestData {

    TelephonyPlatformHelper helper = new TelephonyPlatformHelper();

    @DataProvider(name = "ivrsRequestData")
    public Object[][] ivrsRequestData() throws IOException {

        HashMap<String, String> successCase = new HashMap<>();
        successCase.put("responseCode", "200");
//        successCase.put("id", "generated in run time");
        successCase.put("code", "SUCCESS");
        successCase.put("message", "Success");

        HashMap<String, String> badRequestCase = new HashMap<>();
        badRequestCase.put("responseCode", "400");
        badRequestCase.put("errors.error_field", "some error String");
        badRequestCase.put("errors.rejected_value", "some rejected value String");
        badRequestCase.put("errors.message", "some String message");

        return new Object[][]{
//                {clientId, requestId, caller_region_id, mpbile, calleeId, appId, caseHashmap}
                {"1", helper.getUniqueRequestId(), "1", "1234567899", "1", successCase},
                {" ", helper.getUniqueRequestId(), "2", "1234567898", "1", successCase}
        };
    }

    @DataProvider(name = "ivrsRequestHysterixData")
    public Object[][] ivrsRequestHysterixData() {

        HashMap<String, String> successCase = new HashMap<>();
        successCase.put("responseCode", "200");
//        successCase.put("id", "generated in run time");
        successCase.put("code", "SUCCESS");
        successCase.put("message", "Successfully submitted Ivrs Call Request");

        HashMap<String, String> badRequestCase = new HashMap<>();
        badRequestCase.put("responseCode", "400");
        badRequestCase.put("errors.error_field", "some error String");
        badRequestCase.put("errors.rejected_value", "some rejected value String");
        badRequestCase.put("errors.message", "some String message");

        return new Object[][]{
//                {clientId, requestId, caller_region_id, mpbile, calleeId, appId, caseHashmap}
                {"1", helper.getUniqueRequestId(), "1", "1234567899", "1", successCase},
//                {header, "requestId", "callerRegionId", "calleeId", "appId", badRequestCase}
        };
    }

    @DataProvider(name = "callStatusData")
    public Object[][] callStatusData(){
        HashMap<String, String> successCase = new HashMap<>();
        successCase.put("responseCode", "200");
        successCase.put("code", "SUCCESS");
        successCase.put("message", "Success");

        return new Object[][]{
//                {callId, clientId,  caseHashmap, scenarion, exotelResponseToBeStubbed}
                {helper.getCallID("1", helper.getUniqueRequestId(), "1", "1234567899", "1"), "1", successCase, "with valid header and valid call_id", "queued"},
                {helper.getCallID("1", helper.getUniqueRequestId(), "1", "1234567899", "1"), "1", successCase, "with valid header and valid call_id", "in-progress"},
                {helper.getCallID("1", helper.getUniqueRequestId(), "1", "1234567899", "1"), "1", successCase, "with valid header and valid call_id", "completed"},
                {helper.getCallID("1", helper.getUniqueRequestId(), "1", "1234567899", "1"), "1", successCase, "with valid header and valid call_id", "failed"},
                {helper.getCallID("1", helper.getUniqueRequestId(), "1", "1234567899", "1"), "1", successCase, "with valid header and valid call_id", "busy"},
                {helper.getCallID("1", helper.getUniqueRequestId(), "1", "1234567899", "1"), "1", successCase, "with valid header and valid call_id", "no-answer"}
        };
    }

    @DataProvider(name = "createIvrsAppData")
    public Object[][] createIvrsAppData() throws IOException {

        HashMap<String, String> validHeader = new HashMap<>();
        validHeader.put("AuthKey", "tpadmin@2018");
        validHeader.put("Content-Type", "application/json");

        HashMap<String, String> invalidHeader = new HashMap<>();
        invalidHeader.put("AuthKey", "invalid");
        invalidHeader.put("Content-Type", "application/json");


        HashMap<String, String> successCase = new HashMap<>();
        successCase.put("responseCode", "200");
        successCase.put("createdBy", "test.automation@swiggy.in");
        successCase.put("appName", "Automation-App-01");

        HashMap<String, String> badRequestCase = new HashMap<>();
        badRequestCase.put("responseCode", "400");
        badRequestCase.put("errors.error_field", "some error String");
        badRequestCase.put("errors.rejected_value", "some rejected value String");
        badRequestCase.put("errors.message", "some String message");

        HashMap<String, String> unauthorizedCase = new HashMap<>();
        unauthorizedCase.put("responseCode", "401");
        unauthorizedCase.put("errors.error_field", "some error String");
        unauthorizedCase.put("errors.rejected_value", "some rejected value String");
        unauthorizedCase.put("errors.message", "some String message");

        return new Object[][]{
//                {header, appName, podName, appUrl, completedCallbackUrl, failedCallbackUrl, keyPressCallbackUrl, expectedResponseMap}
                {validHeader, "Automation-App-01", "FF", "http://my.exotel.in/exoml/start/124586", "http://localhost:5000/completed", "http://localhost:5000/failed", "http://localhost:5000/keypress", successCase},
                {invalidHeader, "Automation-App-01", "FF", "http://my.exotel.in/exoml/start/124586", "http://localhost:5000/completed", "http://localhost:5000/failed", "http://localhost:5000/keypress", unauthorizedCase},
                {validHeader, "Automation-App-01", "FF", "http://my.exotel.in/exoml/start/124586", "", "", "", successCase},
                {validHeader, "Automation-App-01", "FF", "", "", "", "", badRequestCase},
                {validHeader, "", "FF", "http://my.exotel.in/exoml/start/124586", "", "", "", badRequestCase},
                {validHeader, "Automation-App-01", "", "http://my.exotel.in/exoml/start/124586", "", "", "", badRequestCase},
                {validHeader, "", "", "", "", "", "", badRequestCase},

        };
    }

    @DataProvider(name = "getIvrsAppByIdData")
    public Object[][] getIvrsAppByIdData() throws IOException {


        HashMap<String, String> successCase = new HashMap<>();
        successCase.put("responseCode", "200");
        successCase.put("appName", "arvind-test-0");
        successCase.put("appId", "3");
        successCase.put("clientId", "2");
        successCase.put("createdBy", "arvind.k@swiggy.in");

        HashMap<String, String> badRequestCase = new HashMap<>();
        badRequestCase.put("responseCode", "400");
        badRequestCase.put("errors.error_field", "some error String");
        badRequestCase.put("errors.rejected_value", "some rejected value String");
        badRequestCase.put("errors.message", "some String message");

        return new Object[][]{
//                {appId, expectedResponseMap}
                {"3", successCase},
                {"a", badRequestCase},
                {"@#", badRequestCase},
                {" ", badRequestCase}
        };
    }

    @DataProvider(name = "exotelKeypressCallbackData")
    public Object[][] exotelKeypressCallbackData() throws IOException {

        return new Object[][]{
//                {callSid, digits}
                {"9704f6aa6272f87d51eeb34f740dbdbb", "1"},
                {"9704f6aa6272f87d51eeb34f740dbdbb", ""},
                {"9704f6aa6272f87d51eeb34f740dbdbb", "%20"},
                {"", "1"},
                {"%20", "1"},
                {"%20", "%20"},
        };
    }

    @DataProvider(name = "exotelStatusCallbackData")
    public Object[][] exotelStatusCallbackData() throws IOException {

        return new Object[][]{
//                {callSid, status}
                {"9704f6aa6272f87d51eeb34f740dbdbb", "completed"},
                {"9704f6aa6272f87d51eeb34f740dbdbb", ""},
                {"9704f6aa6272f87d51eeb34f740dbdbb", " "},
                {"", "completed"},
                {" ", "completed"},
                {" ", " "},
        };
    }

    @DataProvider(name = "ubonaResponseData")
    public Object[][] ubonaResponseData() throws IOException {

        HashMap<String, String> successCase = new HashMap<>();
        successCase.put("responseCode", "200");

        HashMap<String, String> badRequestCase = new HashMap<>();
        badRequestCase.put("responseCode", "400");
        badRequestCase.put("error", "Bad Request");
        badRequestCase.put("message", "value not one of declared Enum instance names");

        return new Object[][]{
                // {eventName, status}
                {"CUSTOMER_ANSWERED", "CUSTOMER_INPUT", successCase},
                {"EXPIRED", "INCOMPLETE_INPUT", successCase},
                {"CUSTOMER_ANSWERED", "INVALID_INPUT", successCase},
                {"CUSTOMER_NOT_ANSWERED", "NO_INPUT", successCase},
                {"CUSTOMER_ANSWERED", "CUSTOMER_DISCONNECTED", successCase},
                {"CUSTOMER_ANSWERED", "INVALID_INPUT", successCase},
                {"CUSTOMER_ANSWERED", "", badRequestCase},
                {"", "CUSTOMER_INPUT", badRequestCase},
                {"INVALID_EVENT ", "CUSTOMER_INPUT", badRequestCase},
                {"CUSTOMER_ANSWERED ", "INVALID_CUSTOMER_INPUT", badRequestCase},
        };
    }



}
