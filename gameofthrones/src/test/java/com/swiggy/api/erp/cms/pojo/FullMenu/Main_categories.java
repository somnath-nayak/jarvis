package com.swiggy.api.erp.cms.pojo.FullMenu;

import com.swiggy.api.erp.cms.constants.MenuConstants;

/**
 * Created by kiran.j on 2/2/18.
 */
public class Main_categories
{
    private String id;

    private Sub_categories[] sub_categories;

    private String order;

    private String description;

    private String name;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public Sub_categories[] getSub_categories ()
    {
        return sub_categories;
    }

    public void setSub_categories (Sub_categories[] sub_categories)
    {
        this.sub_categories = sub_categories;
    }

    public String getOrder ()
    {
        return order;
    }

    public void setOrder (String order)
    {
        this.order = order;
    }

    public String getDescription ()
{
    return description;
}

    public void setDescription (String description)
    {
        this.description = description;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public Main_categories build() {
        setDefaultValues();
        return this;
    }

    public void setDefaultValues() {
        Sub_categories sub_categories = new Sub_categories();
        sub_categories.build();

        if(this.getId() == null)
            this.setId(MenuConstants.main_category_id);
        if(this.getName() == null)
            this.setName(MenuConstants.main_category_name);
        if(this.getDescription() == null)
            this.setDescription("");
        if(this.getOrder() == null)
            this.setOrder(MenuConstants.main_category_order);
        if(this.getSub_categories() == null)
            this.setSub_categories(new Sub_categories[]{sub_categories});

    }

    @Override
    public String toString()
    {
        return "{id = "+id+", sub_categories = "+sub_categories+", order = "+order+", description = "+description+", name = "+name+"}";
    }
}