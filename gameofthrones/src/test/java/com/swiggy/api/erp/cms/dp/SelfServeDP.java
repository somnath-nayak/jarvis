package com.swiggy.api.erp.cms.dp;

import com.swiggy.api.erp.cms.helper.BaseServiceHelper;
import com.swiggy.api.erp.cms.helper.SelfServeHelper;
import com.swiggy.api.erp.cms.helper.SelfServiceHelper;
import com.swiggy.api.erp.cms.pojo.SelfServeAddNewItem.AddNewItemForRestaurant;
import com.swiggy.api.erp.cms.pojo.SelfServeTriggerEventForTicketAssignment.Ticket;
import com.swiggy.api.erp.cms.pojo.SelfServeTriggerEventForTicketAssignment.TriggerEventForTicketAssignment;
import com.swiggy.api.erp.cms.pojo.SelfServeUpdateTicketStatus.UpdateTicketStatus;
import edu.emory.mathcs.backport.java.util.Arrays;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.erp.cms.dp
 **/
public class SelfServeDP {

    SelfServeHelper selfServeHelper = new SelfServeHelper();
    BaseServiceHelper baseServiceHelper = new BaseServiceHelper();
    String g_cat_id, g_sub_cat_id, g_item_id;
    int g_rest_id = baseServiceHelper.getRestId(false);
   // int g_rest_id= baseServiceHelper.createRestaurantAndReturnID();


    @DataProvider(name="storeAvailability")
    public  Object[][] storeAvailability()
    {

        DateTimeFormatter formatters = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime date = LocalDateTime.now();

        Object[][] data = {{3,"9990",true, null, null,"positive"},
                {3,"9990",false, date.plusDays(2).format(formatters), date.plusDays(4).format(formatters),"positive"},
                {0,"9990",true, null, null,"negative"},
                {3,"0",false, date.plusDays(2).format(formatters), date.plusDays(4).format(formatters),"negative"},
                {3,"9990",false, date.plusDays(4).format(formatters), date.plusDays(2).format(formatters),"negative"},
                {3,"9990",false, date.minusDays(4).format(formatters), date.plusDays(2).format(formatters),"negative"},
                {3,"9990",false, date.plusHours(2).format(formatters), date.plusHours(10).format(formatters),"positive"}
        } ;
        return data;
    }

/*
    Returns: String json, String state, String name, String restID, boolean hitAddItem, boolean hitTriggerEvent, boolean hitEnableAgent, boolean enableAgentAction, String description
*/
    @DataProvider(name = "addItemDP")
    public Iterator<Object[]> addItemDP() throws IOException {

        List<Object[]> obj = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        System.out.println("\n--REST_ID ::: " + g_rest_id );
        int cat = baseServiceHelper.getCategoryId(g_rest_id);
        int subcat = baseServiceHelper.getSubCategoryId(g_rest_id,cat);
        String name = selfServeHelper.generateName();
        AddNewItemForRestaurant addNewItemForRestaurant = new AddNewItemForRestaurant();
        addNewItemForRestaurant.build(String.valueOf(cat),String.valueOf(subcat), name);
        selfServeHelper.unavailableAllAvailableAgents();
        obj.add(new Object[] {jsonHelper.getObjectToJSON(addNewItemForRestaurant),"NEW", name, String.valueOf(g_rest_id), true, true, false, false,"NA", "--New Ticket State"});

        jsonHelper = new JsonHelper();
        selfServeHelper.unavailableAllAvailableAgents();
        obj.add(new Object[] {jsonHelper.getObjectToJSON(addNewItemForRestaurant), "SUBMITTED", name, String.valueOf(g_rest_id),false, false, true, false, "NA","--Submitted Ticket State"});

        jsonHelper = new JsonHelper();
        obj.add(new Object[] {jsonHelper.getObjectToJSON(addNewItemForRestaurant), "ASSIGNED", name, String.valueOf(g_rest_id),false,false,false, false, "NA","--Assigned Ticket State"});
        //duplicate add item
        obj.add(new Object[] {jsonHelper.getObjectToJSON(addNewItemForRestaurant), "NEW", name, String.valueOf(g_rest_id),true, true, false, false, "NA","--Duplicate Item Add"});
        //submit duplicate item
        obj.add(new Object[] {jsonHelper.getObjectToJSON(addNewItemForRestaurant), "ASSIGNED", name, String.valueOf(g_rest_id),false, false, false, false, "NA","--Duplicate Item Ticket Submit"});
        //Create new and generate ticket
        addNewItemForRestaurant = new AddNewItemForRestaurant();
        addNewItemForRestaurant.build(String.valueOf(cat),String.valueOf(subcat), name);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(addNewItemForRestaurant),"NEW", name, String.valueOf(g_rest_id), true, true, true, false, "NA", "--Create new item and generate ticket"});

        UpdateTicketStatus updateTicketStatus = new UpdateTicketStatus();
//        updateTicketStatus.setData(jsonHelper.getObjectToJSON(addNewItemForRestaurant));
//        updateTicketStatus.setRestaurantId(g_rest_id);
//        updateTicketStatus.setType("ITEM_ADDITION");
//        updateTicketStatus.setStatus("ASSIGNED");
        //String ticket_id = selfServeHelper.getAssignedTicketDetailsFromDB(String.valueOf(g_rest_id),name).get(0).get("id").toString();
       // updateTicketStatus.setId(Integer.parseInt(ticket_id));
        updateTicketStatus.setRestaurantId(g_rest_id);
       // updateTicketStatus.setGroup_id(selfServeHelper.getGroupIDFromDB(String.valueOf(g_rest_id),name));
        obj.add(new Object[] {jsonHelper.getObjectToJSON(updateTicketStatus),"ASSIGNED", name, String.valueOf(g_rest_id), false, false, false, true, "approved", "--Assign and approve"});
        return obj.iterator();
    }

    //(String agentID, String isAvailable, String isEnabled, String description)
    @DataProvider(name = "updateAgentAvailabilityDP")
    public Object[][] UpdateAgentAvailabilityDP() {
        String agent =  selfServeHelper.getAgentIDFromDB();
        String authkey = selfServeHelper.getAuthKeyOfAgentFromDB(agent);
        String authuserid = selfServeHelper.getAuthUserIDFromDB(agent);
        Processor p = selfServeHelper.findAgentByAuthUserID(authkey,authuserid);
        boolean get_isAvailable = p.ResponseValidator.GetNodeValueAsBool("$.data.isAvailable");
        boolean get_isEnabled = p.ResponseValidator.GetNodeValueAsBool("$.data.isEnabled");
        System.out.println("--Agent (get_isAvailable) ::: "+get_isAvailable);
        System.out.println("--Agent (get_isEnabled) ::: " +get_isEnabled);
        if (!get_isEnabled) {
            selfServeHelper.makeSingleAgentEnabled(agent);
        }
        if (!get_isAvailable) {
            selfServeHelper.makeSingleAgentAvailable(agent);
        }

        return new Object[][] {
                {agent, "true", "true", "--Available Agent"},
                {agent, "false", "true", "--Unavailable Agent"},
                {agent, "false", "false","--Unavailable already unavailable agent"},
                {"0","true","true", "--Invalid Agent"},
                {agent, "true", "false", "--Disabled Agent"}
        };
    }

    @DataProvider(name = "agentEnableDisableDP")
    public Object[][] agentEnableDisableDP() {
        String agent =  selfServeHelper.getAgentIDFromDB();
        String authkey = selfServeHelper.getAuthKeyOfAgentFromDB(agent);
        String authuserid = selfServeHelper.getAuthUserIDFromDB(agent);
        Processor p = selfServeHelper.findAgentByAuthUserID(authkey,authuserid);
        boolean get_isEnabled = p.ResponseValidator.GetNodeValueAsBool("$.data.isEnabled");
        System.out.println("--Agent (get_isEnabled) ::: " +get_isEnabled);
        if (!get_isEnabled) {
            selfServeHelper.makeSingleAgentEnabled(agent);
        }
        return new Object[][] {
                {agent, "true", "--Enabled Agent"},
                {agent, "false", "--Disable Agent"},
                {agent, "false","--Disable already disabled agent"},
                {"0","true", "--Invalid Agent"}
        };
    }

    @DataProvider(name = "getAgentDetailsDP")
    public Object[][] getAgentDetailsDP() {
        String agent =  selfServeHelper.getAgentIDFromDB();
        String authkey = selfServeHelper.getAuthKeyOfAgentFromDB(agent);
        String authuserid = selfServeHelper.getAuthUserIDFromDB(agent);
        return new Object[][]{
                {agent,authkey,authuserid,"SELFSERVE","--Valid getAgentDetails"},
                {"0",authkey,authuserid,"SELFSERVE","--Invalid getAgentDetails"},
        };
    }

    //return: String json, String name, String validCase, String submitTicket, String restID, String description
    @DataProvider(name = "addItemToRestDP")
    public Iterator<Object[]> addItemToRestDP() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        int cat = baseServiceHelper.getCategoryId(g_rest_id);
        int subcat = baseServiceHelper.getSubCategoryId(g_rest_id,cat);
        String name = selfServeHelper.generateName();
        AddNewItemForRestaurant addNewItemForRestaurant = new AddNewItemForRestaurant();
        addNewItemForRestaurant.build(String.valueOf(cat),String.valueOf(subcat), name);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(addNewItemForRestaurant),name, "true","false", String.valueOf(g_rest_id), "--Add New Item To Restaurant Valid"});

        addNewItemForRestaurant = new AddNewItemForRestaurant();
        addNewItemForRestaurant.build(String.valueOf(0),String.valueOf(0),name);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(addNewItemForRestaurant),name, "false", "false", String.valueOf(0), "--Add New Item To Restaurant Invalid"});

        addNewItemForRestaurant = new AddNewItemForRestaurant();
        addNewItemForRestaurant.build(String.valueOf(cat),String.valueOf(subcat), name);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(addNewItemForRestaurant),name, "true","true", String.valueOf(g_rest_id), "--Add New Item & Submit To Restaurant Valid"});

        addNewItemForRestaurant = new AddNewItemForRestaurant();
        addNewItemForRestaurant.build(String.valueOf(0),String.valueOf(0),name);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(addNewItemForRestaurant),name, "false", "true", String.valueOf(0), "--Add New Item & submit To Restaurant Invalid"});

        return obj.iterator();
    }

    @DataProvider(name = "getTicketDetailsDP")
    public Object[][] getTicketDetailsDP() throws IOException {
        JsonHelper jsonHelper = new JsonHelper();
        List<Map<String, Object>> DBlist = new ArrayList<>();
        int cat = baseServiceHelper.getCategoryId(g_rest_id);
        int subcat = baseServiceHelper.getSubCategoryId(g_rest_id,cat);
        String name = selfServeHelper.generateName();
        AddNewItemForRestaurant addNewItemForRestaurant = new AddNewItemForRestaurant();
        addNewItemForRestaurant.build(String.valueOf(cat),String.valueOf(subcat), name);
        Processor p = selfServeHelper.addItemHelper(jsonHelper.getObjectToJSON(addNewItemForRestaurant),String.valueOf(g_rest_id));
        int get_status1 = p.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        System.out.println("--addItemHelper in getTicketDetailsDP status ::: "+get_status1);
        DBlist = selfServeHelper.getCreatedTicketDetailsFromDB(String.valueOf(g_rest_id),name);
        String ticketID = DBlist.get(0).get("id").toString();
        return new Object[][] {
                {ticketID, name, String.valueOf(g_rest_id), "ITEM_ADDITION", "--get TicketDetails Valid TicketID"},
                {"0",name,"0", "ITEM_ADDITION", "--get TicketDetails invalid TicketID"}
        };
    }

    @DataProvider(name="restaurantId")
    public Object[][] getRestId(){
        return new Object[][]{{"532"},{"535"},{"757"}};
    }

}
