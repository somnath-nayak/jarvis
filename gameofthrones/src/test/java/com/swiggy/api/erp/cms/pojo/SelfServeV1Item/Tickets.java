package com.swiggy.api.erp.cms.pojo.SelfServeV1Item;

import com.swiggy.api.erp.cms.constants.SelfServeConstants;

public class Tickets
{
	private String id;

	public String getId ()
	{
		return id;
	}

	public void setId (String id)
	{
		this.id = id;
	}

	public Tickets build() {
		setDefaultValues();
		return this;
	}

	private void setDefaultValues() {
		if(this.getId()==null)
			this.setId("0");
	}
	@Override
	public String toString()
	{
		return "ClassPojo [id = "+id+"]";
	}
}

