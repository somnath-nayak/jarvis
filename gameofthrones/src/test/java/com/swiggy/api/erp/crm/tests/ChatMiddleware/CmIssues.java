package com.swiggy.api.erp.crm.tests.ChatMiddleware;

import com.swiggy.api.erp.crm.dp.cancellationflows.CreateData;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.util.HashSet;
import java.util.Set;
import java.util.Arrays;
import java.util.HashMap;

//import framework.gameofthrones.JonSnow.XMLValidator;

public class CmIssues {

	Initialize gameofthrones = new Initialize();

	@Test( dataProviderClass = CreateData.class, dataProvider = "user data", enabled = true,description = "Test for the first level nodes")
	public void firstLevelIssues(HashMap<String, String[]> data, HashMap<String, HashMap<String, String[]>> nodes) {
		String[] userdata;
		HashMap<String, String> requestheaders_issues = new HashMap<String, String>();

		for (int i = 1; i <= data.size(); i++) {
			userdata = data.get("user" + i);
			requestheaders_issues.put("Content-Type", "application/json");
			requestheaders_issues.put("userId", userdata[0]);
			System.out.println(userdata[0] + "  --------" + userdata[1]);
			requestheaders_issues.put("username", userdata[1]);

			GameOfThronesService firstlevel_issuenodes = new GameOfThronesService("chatmiddlewear", "firstLevelIssueNodes", gameofthrones);
			Processor firslevelIssueNodes_response = new Processor(firstlevel_issuenodes, requestheaders_issues);

			System.out.println("Response " + firslevelIssueNodes_response);

			String[] actaul = new String[4];
			actaul[0] = firslevelIssueNodes_response.ResponseValidator.GetNodeValue("data.issuetypes.data[0].title");
			actaul[1] = firslevelIssueNodes_response.ResponseValidator.GetNodeValue("data.issuetypes.data[1].title");
			actaul[2] = firslevelIssueNodes_response.ResponseValidator.GetNodeValue("data.issuetypes.data[2].title");
			actaul[3] = firslevelIssueNodes_response.ResponseValidator.GetNodeValue("data.issuetypes.data[3].title");

			String[] expected = {"Order specific issues", "General Queries", "Legal","FAQs"};
			Assert.assertEquals(actaul,expected,"first level nodes present");
		}
	}

	@Test( dataProviderClass = CreateData.class, dataProvider = "user data", enabled = true,description = "Test for the order specific nodes")
	public void issueTypeOrderSpecific(HashMap<String, String[]> data, HashMap<String, HashMap<String, String[]>> nodes) {
		String[] userdata;
		String[] queryparam = new String[1];
		String[] payload = new String[1];
		String Actaultitle, ActualnodeId, general;
		int generalIssueNodeCount;
		HashMap<String, String[]> data2 = nodes.get(3); // orderspecific issues

		HashMap<String, String> requestheaders = new HashMap<String, String>();

		// checking for each user
		for (int i = 1; i <= data.size(); i++) {
			userdata = data.get("user" + i);
			requestheaders.put("Content-Type", "application/json");
			requestheaders.put("userId", userdata[0]);
			requestheaders.put("username", userdata[1]);

			queryparam[0] = userdata[2];
			payload[0] = null;

			GameOfThronesService isuuetype_general = new GameOfThronesService("chatmiddlewear", "issueTypeOrderSpecific", gameofthrones);
			Processor response = new Processor(isuuetype_general, requestheaders, payload, queryparam);

			int http_status = response.ResponseValidator.GetResponseCode();
			Assert.assertEquals(http_status, 200);

			String[] actaul = new String[6];
			actaul[0] = response.ResponseValidator.GetNodeValue("data.issues.data[0].title");
			actaul[1] = response.ResponseValidator.GetNodeValue("data.issues.data[1].title");
			actaul[2] = response.ResponseValidator.GetNodeValue("data.issues.data[2].title");
			actaul[3] = response.ResponseValidator.GetNodeValue("data.issues.data[3].title");
			actaul[4] = response.ResponseValidator.GetNodeValue("data.issues.data[4].title");
			actaul[5] = response.ResponseValidator.GetNodeValue("data.issues.data[5].title");

			String[] expected = { "I want to cancel my order", "I want to modify items in this order","Where is my order?",
					 "I have payment & refund related queries for this order", "I have coupon related queries for this order",
					"I want to provide special instructions for this order"};

			Set<String> setOfExpectedIssues = new HashSet<>(Arrays.asList(expected));
			Set<String> setOfActualIssues = new HashSet<>(Arrays.asList(actaul));

			Assert.assertEquals(actaul.length,expected.length);
			Assert.assertEquals(setOfActualIssues,setOfExpectedIssues);


		}

	}

	@Test( dataProviderClass = CreateData.class, dataProvider = "user data", enabled = true,description = "Test for the general nodes")
	public void issueTypeGeneral(HashMap<String, String[]> data, HashMap<String, HashMap<String, String[]>> nodes) {
		String[] userdata;
		String Actaultitle, ActualnodeId, general;
		int generalIssueNodeCount;
		HashMap<String, String[]> data2 = nodes.get(1);// general issues

		HashMap<String, String> requestheaders = new HashMap<String, String>();

		// checking for each user
		for (int i = 1; i <= data.size(); i++) {
			userdata = data.get("user" + i);
			requestheaders.put("Content-Type", "application/json");
			requestheaders.put("userId", userdata[0]);
			requestheaders.put("username", userdata[1]);

			GameOfThronesService isuuetype_general = new GameOfThronesService("chatmiddlewear", "issueTypeGeneral", gameofthrones);
			Processor response = new Processor(isuuetype_general, requestheaders);

			int http_status = response.ResponseValidator.GetResponseCode();
			Assert.assertEquals(http_status, 200);


			String[] actaul = new String[5];
			actaul[0] = response.ResponseValidator.GetNodeValue("data.issues.data[0].title");
			actaul[1] = response.ResponseValidator.GetNodeValue("data.issues.data[1].title");
			actaul[2] = response.ResponseValidator.GetNodeValue("data.issues.data[2].title");
			actaul[3] = response.ResponseValidator.GetNodeValue("data.issues.data[3].title");
			actaul[4] = response.ResponseValidator.GetNodeValue("data.issues.data[4].title");

			String[] expected = {"I have a query related to placing an order", "I am unable to log in on Swiggy",
					"I have a payment or refund related query","I have a coupon related query", "I want to unsubscribe from Swiggy communications"};
			Assert.assertEquals(actaul,expected,"All the general nodes are available");
		}
	}

	@Test(dataProviderClass = CreateData.class, dataProvider = "user data", enabled = true,description = "Test for the legal nodes")
	public void issueTypeLegal(HashMap<String, String[]> data, HashMap<String, HashMap<String, String[]>> nodes) {
		String[] userdata;
		String Actaultitle, ActualnodeId, general;
		int generalIssueNodeCount;
		HashMap<String, String[]> data2 = nodes.get(2);// legal issues

		HashMap<String, String> requestheaders = new HashMap<String, String>();

		// checking for each user
		for (int i = 1; i <= data.size(); i++) {
			userdata = data.get("user" + i);
			requestheaders.put("Content-Type", "application/json");
			requestheaders.put("userId", userdata[0]);
			requestheaders.put("username", userdata[1]);

			GameOfThronesService isuuetype_legal = new GameOfThronesService("chatmiddlewear", "issueTypeLegal", gameofthrones);
			Processor response = new Processor(isuuetype_legal, requestheaders);

			int http_status = response.ResponseValidator.GetResponseCode();
			Assert.assertEquals(http_status, 200);

			String[] actaul = new String[4];
			actaul[0] = response.ResponseValidator.GetNodeValue("data.issues.data[0].title");
			actaul[1] = response.ResponseValidator.GetNodeValue("data.issues.data[1].title");
			actaul[2] = response.ResponseValidator.GetNodeValue("data.issues.data[2].title");
			actaul[3] = response.ResponseValidator.GetNodeValue("data.issues.data[3].title");

			String[] expected = {"Terms of Use", "Privacy Policy",
					"Cancellations and Refunds", "Terms of Use for Swiggy ON-TIME / Assured"};
			Assert.assertEquals(actaul,expected,"All the legal nodes are available");
		}
	}
}
