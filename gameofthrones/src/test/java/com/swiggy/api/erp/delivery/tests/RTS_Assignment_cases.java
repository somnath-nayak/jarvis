package com.swiggy.api.erp.delivery.tests;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.swiggy.api.erp.delivery.constants.RTSConstants;
import com.swiggy.api.erp.delivery.dp.RTS_Assignment_Data_Provider;
import com.swiggy.api.erp.delivery.helper.AutoassignHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryDataHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryHelperMethods;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceHelper;
import com.swiggy.api.erp.delivery.helper.RTSHelper;

public class RTS_Assignment_cases extends RTS_Assignment_Data_Provider {
	DeliveryHelperMethods delmeth = new DeliveryHelperMethods();
	DeliveryDataHelper deldata = new DeliveryDataHelper();
	DeliveryServiceHelper delserhel=new DeliveryServiceHelper();
	RTSHelper rtshelp = new RTSHelper();
	AutoassignHelper hepauto=new AutoassignHelper();
	String de_id1,de_id2,de_id3,de_id4;
	//static Object de_id1,de_id2,de_id3,de_id4;
	@BeforeClass
	public void enableRTSassignment() {
		delmeth.dbhelperupdate(RTSConstants.enable_CAS_city);
		delmeth.dbhelperupdate(RTSConstants.enable_RTS_CAS_featue);
		delmeth.dbhelperupdate(RTSConstants.disable_RTS_Assignment);
	   de_id1=deldata.CreateDE(RTSConstants.Cas_de_area_int);
	   de_id2=deldata.CreateDE(RTSConstants.single_assignedZones_int);
	   de_id3=deldata.CreateDE(RTSConstants.single_assignedZones_int);
	   de_id4=deldata.CreateDE(RTSConstants.single_assignedZones_int);
	System.out.println("DE ids"+de_id1+" "+de_id2+" "+de_id3+" "+de_id4);
	deldata.addDEzonemap(de_id1,  RTSConstants.Cas_de_area_int);
	deldata.addDEzonemap(de_id2,  RTSConstants.cas_De_rest_Area);
	deldata.addDEzonemap(de_id3,RTSConstants.long_distance_area_code);
	deldata.addDEzonemap(de_id4,  RTSConstants.long_distance_area_code);
	}
	@AfterMethod
	public void setothersorderstorandomDE() {
		delmeth.dbhelperupdate(RTSConstants.closeallexisitingbatches);
		delmeth.dbhelperupdate(RTSConstants.set_random_de_id);	
	}
	@Test(dataProvider = "RTS_CAS_1",priority=1, groups = "RTSCrossareaassignment", description = "CAS order should assign to customer zone DE when feature flag is on and no entry is made in Cross zone assignment exclude mappings")
	public void RTSCrossArea1(String order_id,String version, String lat, String lng) throws InterruptedException {
		executetestcrossarea(order_id, lat, lng, de_id1,de_id2, version);
		}
	@Test(dataProvider = "RTS_CAS_2",priority=2, groups = "RTSCrossareaassignment", description = "CAS order should not assign to customer zone DE when feature flag is on and entry is made in  Cross zone assignment exclude mappings")
	public void RTSCrossArea2(String order_id,String version, String lat, String lng) throws InterruptedException {
		executetestwithoutcrossarea(order_id, lat, lng, de_id1, de_id2, version);
	}
	@Test(dataProvider = "RTS_CAS_3",priority=3, groups = "RTSCrossareaassignment", description = "CAS order should assign to customer zone DE when feature flag is off and entry is made in  Cross zone assignment exclude mappings(It will fallback to city flag)")
	public void RTSCrossArea3(String order_id,String version, String lat, String lng) throws InterruptedException {
		executetestcrossarea(order_id, lat, lng, de_id1,de_id2, version);
		}
	@Test(dataProvider = "RTS_CAS_4",priority=4, groups = "RTSCrossareaassignment", description = "CAS order should assign to customer zone DE when feature flag is on and entry is made in  Cross zone assignment exclude mappings but is not active")
	public void RTSCrossArea4(String order_id,String version, String lat, String lng) throws InterruptedException {
		executetestcrossarea(order_id, lat, lng, de_id1,de_id2, version);
	}
	@Test(dataProvider = "RTS_CostAssign_5",priority=5, groups = "RTSCrossareaassignment", description = "Verify order is getting assigned to DE with edge having minimum cost when order is placed from restaurant which falls under RTS zone")
	public void RTSCostAssignment5(String order_id,String version) throws InterruptedException {
	Assert.assertNotNull(order_id,
				"Order is not created through order json push");
		delserhel.makeDEFree(de_id3);
		delserhel.makeDEActive(de_id3);
		String auth1=deldata.delocationupdate(RTSConstants.RTS_Assignment_DE1_lat, RTSConstants.RTS_Assignment_DE1_lng, de_id3, version);
		delserhel.makeDEFree(de_id4);
		delserhel.makeDEActive(de_id4);
		String auth2=deldata.delocationupdate(RTSConstants.RTS_Assignment_DE2_lat, RTSConstants.RTS_Assignment_DE2_lng, de_id4, version);
		rtshelp.runassignment(RTSConstants.cityid);
		String query = "Select * from trips where order_id=" + order_id + ";";
		Object assigndeid = delmeth.dbhelperget(query, "de_id");
		if ((null == assigndeid)
				|| (!de_id3.equalsIgnoreCase(assigndeid.toString()))) {
			delserhel.makeDEFree(de_id3);
			delserhel.deLogout(auth1);
			delserhel.makeDEFree(de_id4);
			delserhel.deLogout(auth2);
			Assert.assertTrue(false);
		} else {
			delserhel.makeDEFree(de_id3);
			delserhel.deLogout(auth1);
			delserhel.makeDEFree(de_id4);
			delserhel.deLogout(auth2);
	    	Assert.assertTrue(true);
			}
	}
	@Test(dataProvider = "RTS_CostAssign_6",priority=6, groups = "RTSCrossareaassignment", description = "Verify order is getting assigned to DE with edge having minimum cost when order is placed from restaurant which falls under RTS zone")
	public void RTSCostAssignment6(String order_id,String version) throws InterruptedException {
		Assert.assertNotNull(order_id,
				"Order is not created through order json push");
		delserhel.makeDEFree(de_id3.toString());
		delserhel.makeDEActive(de_id3.toString());
		String auth1=deldata.delocationupdate(RTSConstants.RTS_Assignment_DE1_lat, RTSConstants.RTS_Assignment_DE1_lng, de_id3.toString(), version);
		delserhel.makeDEFree(de_id4.toString());
		delserhel.makeDEActive(de_id4.toString());
		String auth2=deldata.delocationupdate(RTSConstants.RTS_Assignment_DE2_lat, RTSConstants.RTS_Assignment_DE2_lng, de_id4.toString(), version);rtshelp.runassignment(RTSConstants.cityid);
		String query = "Select * from trips where order_id=" + order_id + ";";
		Object assigndeid = delmeth.dbhelperget(query, "de_id");
		if ((null == assigndeid)
				|| (!de_id3.toString().equalsIgnoreCase(assigndeid.toString()))) {
			delserhel.makeDEFree(de_id3.toString());
			delserhel.deLogout(auth1);
			delserhel.makeDEFree(de_id4.toString());
			delserhel.deLogout(auth2);
			Assert.assertTrue(false);
		} else {
			delserhel.makeDEFree(de_id3.toString());
			delserhel.deLogout(auth1);
			delserhel.makeDEFree(de_id4.toString());
			delserhel.deLogout(auth2);
	    	Assert.assertTrue(true);
			}
	}
	@Test(dataProvider = "RTS_CostAssign_7",priority=7, groups = "RTSCrossareaassignment", description = "Verify order is getting assigned to DE with edge having minimum cost  according to greedy approach when order is placed from restaurant which falls under RTS zone")
	public void RTSCostAssignment7(String order_id,String version) throws InterruptedException {
		Assert.assertNotNull(order_id,
				"Order is not created through order json push");
		delserhel.makeDEFree(de_id3.toString());
		delserhel.makeDEActive(de_id3.toString());
		String auth1=deldata.delocationupdate(RTSConstants.RTS_Assignment_DE1_lat, RTSConstants.RTS_Assignment_DE1_lng, de_id3.toString(), version);
		delserhel.makeDEFree(de_id4.toString());
		delserhel.makeDEActive(de_id4.toString());
		String auth2=deldata.delocationupdate(RTSConstants.RTS_Assignment_DE2_lat, RTSConstants.RTS_Assignment_DE2_lng, de_id4.toString(), version);
		rtshelp.runassignment(RTSConstants.cityid);
		String query = "Select * from trips where order_id=" + order_id + ";";
		Object assigndeid = delmeth.dbhelperget(query, "de_id");
		if ((null == assigndeid)
				|| (!de_id3.toString().equalsIgnoreCase(assigndeid.toString()))) {
			delserhel.makeDEFree(de_id3.toString());
			delserhel.deLogout(auth1);
			delserhel.makeDEFree(de_id4.toString());
			delserhel.deLogout(auth2);
			Assert.assertTrue(false);
		} else {
			delserhel.makeDEFree(de_id3.toString());
			delserhel.deLogout(auth1);
			delserhel.makeDEFree(de_id4.toString());
			delserhel.deLogout(auth2);
	    	Assert.assertTrue(true);
			}
	}
	@Test(dataProvider = "RTS_CostAssign_8",priority=8, groups = "RTSCrossareaassignment", description = "Verify order is getting assigned to DE with edge having minimum cost  according to hungarian approach when order is placed from restaurant which falls under RTS zone")
	public void RTSCostAssignment8(String order_id,String version) throws InterruptedException {
		Assert.assertNotNull(order_id,
				"Order is not created through order json push");
		delserhel.makeDEFree(de_id3.toString());
		delserhel.makeDEActive(de_id3.toString());
		String auth1=deldata.delocationupdate(RTSConstants.RTS_Assignment_DE1_lat, RTSConstants.RTS_Assignment_DE1_lng, de_id3.toString(), version);
		delserhel.makeDEFree(de_id4.toString());
		delserhel.makeDEActive(de_id4.toString());
		String auth2=deldata.delocationupdate(RTSConstants.RTS_Assignment_DE2_lat, RTSConstants.RTS_Assignment_DE2_lng, de_id4.toString(), version);
		rtshelp.runassignment(RTSConstants.cityid);
		String query = "Select * from trips where order_id=" + order_id + ";";
		Object assigndeid = delmeth.dbhelperget(query, "de_id");
		if ((null == assigndeid)
				|| (!de_id3.toString().equalsIgnoreCase(assigndeid.toString()))) {
			delserhel.makeDEFree(de_id3.toString());
			delserhel.deLogout(auth1);
			delserhel.makeDEFree(de_id4.toString());
			delserhel.deLogout(auth2);
			Assert.assertTrue(false);
		} else {
			delserhel.makeDEFree(de_id3.toString());
			delserhel.deLogout(auth1);
			delserhel.makeDEFree(de_id4.toString());
			delserhel.deLogout(auth2);
	    	Assert.assertTrue(true);
			}
	}
	@Test(dataProvider = "RTS_CostAssign_9",priority=8, groups = "RTSCrossareaassignment", description = "Verify order is getting assigned to busy DE accoridng to cost calculated")
	public void RTSCostAssignment9(String order_id,String version) throws InterruptedException {
		Assert.assertNotNull(order_id,
				"Order is not created through order json push");
		delserhel.makeDEFree(de_id3.toString());
		delserhel.makeDEActive(de_id3.toString());
		String auth1=deldata.delocationupdate(RTSConstants.RTS_Assignment_DE1_lat, RTSConstants.RTS_Assignment_DE1_lng, de_id3.toString(), version);
		delserhel.makeDEFree(de_id4.toString());
		delserhel.makeDEActive(de_id4.toString());
		String auth2=deldata.delocationupdate(RTSConstants.RTS_Assignment_DE2_lat, RTSConstants.RTS_Assignment_DE2_lng, de_id4.toString(), version);
		rtshelp.runassignment(RTSConstants.cityid);
		String query = "Select * from trips where order_id=" + order_id + ";";
		Object assigndeid = delmeth.dbhelperget(query, "de_id");
		if ((null == assigndeid)
				|| (!de_id3.toString().equalsIgnoreCase(assigndeid.toString()))) {
			delserhel.makeDEFree(de_id3.toString());
			delserhel.deLogout(auth1);
			delserhel.makeDEFree(de_id4.toString());
			delserhel.deLogout(auth2);
			Assert.assertTrue(false);
		} else {
			delserhel.zipDialConfirmDE(order_id);
			Thread.sleep(5000);
			delserhel.zipDialArrivedDE(order_id);
			Thread.sleep(5000);
			delserhel.zipDialPickedUpDE(order_id);
			Assert.assertTrue(true);
			}
	}
	public void executetestwithoutcrossarea(String order_id,String lat,String lng,String deid1,String deid2,String version) throws InterruptedException
	{
	Assert.assertNotNull(order_id,
			"Order is not created through order json push");
	delserhel.makeDEFree(deid1);
	delserhel.makeDEActive(deid1);
	String auth1=deldata.delocationupdate(lat, lng, deid1, version);
	delserhel.makeDEFree(deid2);
	delserhel.makeDEActive(deid2);
	String auth2=deldata.delocationupdate(lat, lng, deid2, version);
	rtshelp.runassignment(RTSConstants.cityid);
	String query = "Select * from trips where order_id=" + order_id + ";";
	Object assigndeid = delmeth.dbhelperget(query, "de_id");
	if ((null == assigndeid)
			|| (!deid1.equalsIgnoreCase(assigndeid.toString()))) {
		delserhel.makeDEFree(deid1);
		delserhel.deLogout(auth1);
		delserhel.makeDEFree(deid2);
		delserhel.deLogout(auth2);
		Assert.assertTrue(true);
	} else {
		delserhel.makeDEFree(deid1);
		delserhel.deLogout(auth1);
		delserhel.makeDEFree(deid2);
		delserhel.deLogout(auth2);
    	Assert.assertTrue(false);
		}
	}
	public void executetestcrossarea(String order_id,String lat,String lng,String deid1,String deid2,String version) throws InterruptedException
	{
		Assert.assertNotNull(order_id,
				"Order is not created through order json push");
		delserhel.makeDEFree(deid1);
		delserhel.makeDEActive(deid1);
		String auth1=deldata.delocationupdate(lat, lng, deid1, version);
		delserhel.makeDEFree(deid2);
		delserhel.makeDEActive(deid2);
		String auth2=deldata.delocationupdate(lat, lng, deid2, version);
		rtshelp.runassignment(RTSConstants.cityid);
		String query = "Select * from trips where order_id=" + order_id + ";";
		Object assigndeid = delmeth.dbhelperget(query, "de_id");
		if ((null == assigndeid)
				|| (!deid1.equalsIgnoreCase(assigndeid.toString()))) {
			delserhel.makeDEFree(deid1);
			delserhel.deLogout(auth1);
			delserhel.makeDEFree(deid2 );
			delserhel.deLogout(auth2);
			Assert.assertTrue(false);
		} else {
			delserhel.makeDEFree(deid1);
			delserhel.deLogout(auth1);
			delserhel.makeDEFree(deid2);
			delserhel.deLogout(auth2);
	    	Assert.assertTrue(true);
			}
	}
		
		@AfterClass
	public void disableRTSassignment() {
		delmeth.dbhelperupdate(RTSConstants.closeallexisitingbatches);
		delmeth.dbhelperupdate(RTSConstants.set_random_de_id);
		delmeth.dbhelperupdate(RTSConstants.disable_CAS_city);
		delmeth.dbhelperupdate(RTSConstants.disable_RTS_CAS_featue);
		delmeth.dbhelperupdate(RTSConstants.disable_RTS_Assignment);
		delmeth.dbhelperupdate(RTSConstants.disable_RTS_Assignment_for_BTM_zone);
		}
	
}
