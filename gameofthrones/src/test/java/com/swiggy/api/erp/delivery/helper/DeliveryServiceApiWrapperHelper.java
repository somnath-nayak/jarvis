package com.swiggy.api.erp.delivery.helper;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.delivery.constants.DEAlchemistConstants;
import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Daenerys.Initializer;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;

import javax.ws.rs.ProcessingException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DeliveryServiceApiWrapperHelper {
    Processor processor=null;
    GameOfThronesService service = null;
    DeliveryHelperMethods del=new DeliveryHelperMethods();
    Initialize gameofthrones = Initializer.getInitializer();
    public int RETRYCOUNTER=10;
    List<String> CAPRDstates=new ArrayList<String>(){{
        add("confirmed");
        add("arrived");
        add("pickedup");
        add("reached");
        add("delivered");
    }};

    public Processor rejectOrder(String order_id){
        HashMap<String, String> requestheaders = del.singleheader();
        service = new GameOfThronesService("deliveryservice", "zipdialorderrejected", gameofthrones);
        String[] payloadparams = new String[] {order_id};
        processor = new Processor(service, requestheaders, payloadparams);
        return processor;
    }

    public Processor statusUpdate(String order_id,String do_CAPRD_to_state,String auth) {
        service = new GameOfThronesService("deliveryservice", "deliveryboystatusupdate", gameofthrones);
        HashMap<String, String> requestheaders = del.doubleheader();
        requestheaders.put("Authorization", auth);
        HashMap<String, String> formcontent = new HashMap<>();
        formcontent.put("order_id", order_id);
        HashMap<String, String> orderDetails = new HashMap<>();
        String billQuery="select pay,bill,collect from trips where order_id='";
        Map<String, Object> bill = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForList(billQuery+order_id+"'").get(0);
        orderDetails.put("bill", String.valueOf(bill.get("bill")));
        orderDetails.put("pay",String.valueOf(bill.get("pay")));
        orderDetails.put("collect", String.valueOf(bill.get("collect")));
        orderDetails.put("order_id", order_id);
        orderDetails.put("latlng", "12.932530,77.602380");
        String json = null;
        try {
            json = new ObjectMapper().writeValueAsString(orderDetails);
        } catch (IOException e) {
            e.printStackTrace();
        }
        formcontent.put("data", json);
        int toState = CAPRDstates.indexOf(do_CAPRD_to_state.toLowerCase());
        for (int i = 0; i <= toState; i++) {
            RETRYCOUNTER=10;
            switch (CAPRDstates.get(i)) {
                case "confirmed":
                    formcontent.put("status", CAPRDstates.get(i));
                    while(RETRYCOUNTER>0){
                        processor = new Processor(service, requestheaders, null, null, formcontent);
                        String resp=processor.ResponseValidator.GetBodyAsText();
                        String statusCode = JsonPath.read(resp, "$.statusCode").toString();
                        if((statusCode.equals("0"))) {
                            break;
                        }
                        waitIntervalMili(1000);
                        RETRYCOUNTER--;
                    }
                    break;

                case "arrived":
                    formcontent.put("status", CAPRDstates.get(i));
                    while(RETRYCOUNTER>0){
                        processor = new Processor(service, requestheaders, null, null, formcontent);
                        String resp=processor.ResponseValidator.GetBodyAsText();
                        String statusCode = JsonPath.read(resp, "$.statusCode").toString();
                        if((statusCode.equals("0"))) {
                            break;
                        }
                        waitIntervalMili(1000);
                        RETRYCOUNTER--;
                    }
                    break;

                case "pickedup":
                    formcontent.put("status", CAPRDstates.get(i));
                    while(RETRYCOUNTER>0){
                        processor = new Processor(service, requestheaders, null, null, formcontent);
                        String resp=processor.ResponseValidator.GetBodyAsText();
                        String statusCode = JsonPath.read(resp, "$.statusCode").toString();
                        if((statusCode.equals("0"))) {
                            break;
                        }
                        waitIntervalMili(1000);
                        RETRYCOUNTER--;
                    }
                    break;

                case "reached":
                    formcontent.put("status", CAPRDstates.get(i));
                    while(RETRYCOUNTER>0){
                        processor = new Processor(service, requestheaders, null, null, formcontent);
                        String resp=processor.ResponseValidator.GetBodyAsText();
                        String statusCode = JsonPath.read(resp, "$.statusCode").toString();
                        if((statusCode.equals("0"))) {
                            break;
                        }
                        waitIntervalMili(1000);
                        RETRYCOUNTER--;
                    }
                    break;

                case "delivered":
                    formcontent.put("status", CAPRDstates.get(i));
                    while(RETRYCOUNTER>0){
                        processor = new Processor(service, requestheaders, null, null, formcontent);
                        String resp=processor.ResponseValidator.GetBodyAsText();
                        String statusCode = JsonPath.read(resp, "$.statusCode").toString();
                        if((statusCode.equals("0"))) {
                            System.out.println("Order ID "+order_id+ " is marked Delivered in Delivery");
                            break;
                        }
                        waitIntervalMili(1000);
                        RETRYCOUNTER--;
                    }
                    break;
            }
        }
        return processor;
    }

    public Processor mergeAndBatch(String batch_id1, String batch_id2){
        HashMap<String, String> requestheaders = del.doubleheader();
        service = new GameOfThronesService("deliveryservice", "mergeorders", gameofthrones);
        String[] payloadparams = new String[] { batch_id1,batch_id1,batch_id2 };
        while(RETRYCOUNTER>0){
            processor = new Processor(service, requestheaders, payloadparams);
            if((processor.ResponseValidator.GetResponseCode()==200)) {
                waitIntervalMili(1000);
                break;
            }
            waitIntervalMili(1000);
            RETRYCOUNTER--;
        }
        return processor;
    }

    public Processor DeAssignment(String batch_id,String de_id) {
        JSONObject responseJson=null;
        String status_code="";
        HashMap<String, String> requestheaders = del.doubleheader();
        service = new GameOfThronesService("deliveryservice", "assignorder", gameofthrones);
        String[] payloadparams = new String[]{batch_id, de_id};
        waitIntervalMili(1000);
        while(RETRYCOUNTER>0){
            processor = new Processor(service, requestheaders, payloadparams);
            try {
                responseJson = new JSONObject(processor.ResponseValidator.GetBodyAsText());
                status_code=responseJson.get("statusCode").toString();
                if(status_code.equals("0")) {
                    return processor;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            waitIntervalMili(1000);
            RETRYCOUNTER--;
        }
        return processor;
    }

    public Processor markDEFree(String de_id) {
        service = new GameOfThronesService("deliveryservice", "makeDEFree", gameofthrones);
        HashMap<String, String> requestheaders = del.doubleheader();
        String[] payloadparams = new String[]{de_id};
        while(RETRYCOUNTER>0){
            processor = new Processor(service, requestheaders, payloadparams);
            if((processor.ResponseValidator.GetResponseCode()==200)) {
                break;
            }
            waitIntervalMili(1000);
            RETRYCOUNTER--;
        }
        return processor;
    }


    public Processor createDE(String de_json) {
        service = new GameOfThronesService("deliveryservice", "make-de", gameofthrones);
        HashMap<String, String> headers = del.doubleheader();
        processor = new Processor(service, headers, new String[]{de_json});
        return processor;
    }

    public Processor cancelOrder(String order_id) {
        service = new GameOfThronesService("deliveryservice", "deliverycancelorder", gameofthrones);
        HashMap<String, String> headers = del.doubleheader();
        processor = new Processor(service, headers, new String[]{order_id});
        //Wait to make sure event gets sent to Kafka and gets reflected same in Alchemist DB
        return processor;
    }

    public Processor createRule(String ruleName, String bonus, String incentive_group, String zone,String expression, String comeputeOn) {
        RETRYCOUNTER=10;
        service = new GameOfThronesService("deliveryalchemist", "createRule", gameofthrones);
        HashMap<String, String> headers = del.doubleheader();
        while(RETRYCOUNTER>0) {
            try{
                processor = new Processor(service, headers, new String[]{ruleName, bonus, incentive_group, zone, expression, comeputeOn});
                if((processor.ResponseValidator.GetResponseCode()==200)) {
                    break;
                }
            }
            catch (ProcessingException pe){
            }
            waitIntervalMili(1000);
            RETRYCOUNTER--;
        }
        return processor;
    }

    public Processor createRuleWithMap(HashMap<String,String> createRuleMap) {
        RETRYCOUNTER=5;
        service = new GameOfThronesService("deliveryalchemist", "createRule", gameofthrones);
        HashMap<String, String> headers = del.doubleheader();
        while(RETRYCOUNTER>0) {
            try{
                processor = new Processor(service, headers, new String[]{createRuleMap.get("name"), createRuleMap.get("bonus"),
                        createRuleMap.get("group"), createRuleMap.get("zoneName"), createRuleMap.get("expression"),
                        createRuleMap.get("computeOn"),createRuleMap.get("tag"),createRuleMap.get("start"),createRuleMap.get("end")});
                if((processor.ResponseValidator.GetResponseCode()==200)) {
                    break;
                }
            }
            catch (ProcessingException pe){
            }
            waitIntervalMili(1000);
            RETRYCOUNTER--;
        }
        return processor;
    }

    public Processor SystemReject(String de_id,String order_id) {
        service = new GameOfThronesService("deliveryservice", "SystemReject", gameofthrones);
        HashMap<String, String> headers = del.doubleheader();
        processor = new Processor(service, headers, new String[]{de_id,order_id});
        return processor;
    }

    public Processor fireAlchemistAggregateEvents(String event,String time,String de_id) {
        service = new GameOfThronesService("deliveryalchemist", "computeevent", gameofthrones);
        HashMap<String, String> headers = del.doubleheader();
        processor = new Processor(service, headers, new String[]{event,time,de_id});
        return processor;
    }

    public String returnAuthForDE(String de_id){
        service = new GameOfThronesService(
                "deliveryservice", "delogin", gameofthrones);
        HashMap<String, String> headers = del.formdataheaders();
        HashMap<String, String> formcontent = new HashMap<String, String>();
        formcontent.put("id", de_id);
        formcontent.put("version", DeliveryConstant.delivery_app_version);
        processor = new Processor(service, headers, null, null,
                formcontent);

        service = new GameOfThronesService(
                "deliveryservice", "deotp", gameofthrones);
        HashMap<String, String> formcontentforOtp = new HashMap<>();
        formcontentforOtp.put("id", de_id);
        formcontentforOtp.put("otp", "547698");
        processor = new Processor(service, headers, null, null,
                formcontentforOtp);

        String resp = processor.ResponseValidator.GetBodyAsText();
        String auth = JsonPath.read(resp, "$.data.Authorization").toString();
        return auth;
    }

    public void waitIntervalMili(int pollinterval)
    {
        try {
            Thread.sleep(pollinterval);
        }
        catch(InterruptedException e) {
            e.printStackTrace();
        }
    }
}
