package com.swiggy.api.erp.cms.tests;

import com.swiggy.api.erp.cms.dp.SubCategoryDp;
import com.swiggy.api.erp.cms.helper.FullMenuHelper;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

/**
 * Created by kiran.j on 2/19/18.
 */
public class SubCategoryTests extends SubCategoryDp{

    @Test(dataProvider = "subcategory", description = "Verifies Different Scenarios for Creating Sub - Categories")
    public void subCategoryTests(String rest_id, String json, String[] errors, String token_id) {
        SoftAssert softAssert = new SoftAssert();
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        Processor processor = fullMenuHelper.createSubCategory(rest_id, json, token_id);
        if(errors.length == 0)
            softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive(processor));
        else if(errors.length == 1)
            softAssert.assertTrue(processor.ResponseValidator.GetResponseCode() == Integer.parseInt(errors[0]));
        else
            softAssert.assertTrue(fullMenuHelper.validateErrors(processor, errors));
        softAssert.assertAll();
    }

    @Test(dataProvider = "updatesubcategory", description = "Verifies Different Scenarios for Updating Sub - Categories")
    public void updateSubCategoryTests(String rest_id, String json, String[] errors, String token_id, String cat_id ) {
        SoftAssert softAssert = new SoftAssert();
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        Processor processor = fullMenuHelper.updateSubCategory(rest_id, json, token_id, cat_id);
        if(errors.length == 0)
            softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive(processor));
        else if(errors.length == 1)
            softAssert.assertTrue(processor.ResponseValidator.GetResponseCode() == Integer.parseInt(errors[0]));
        else
            softAssert.assertTrue(fullMenuHelper.validateErrors(processor, errors));
        softAssert.assertAll();
    }

    @Test(dataProvider = "createbulksubcategory", description = "Verifies Different Scenarios for Creating Bulk Sub - Categories")
    public void createBulkSubCategoryTests(String rest_id, String json, String[] errors, String token_id) {
        System.out.println(json);
        SoftAssert softAssert = new SoftAssert();
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        Processor processor = fullMenuHelper.createBulkSubcategory(rest_id, json, token_id);
        if(errors.length == 0)
            softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive(processor));
        else if(errors.length == 1)
            softAssert.assertTrue(processor.ResponseValidator.GetResponseCode() == Integer.parseInt(errors[0]));
        else
            softAssert.assertTrue(fullMenuHelper.validateErrors(processor, errors));
        softAssert.assertAll();
    }

    @Test(dataProvider = "updatebulksubcategory", description = "Verifies Different Scenarios for Updating Bulk Sub - Categories")
    public void updateBulkSubCategoryTests(String rest_id, String json, String[] errors, String token_id) {
        SoftAssert softAssert = new SoftAssert();
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        Processor processor = fullMenuHelper.updateBulkSubcategory(rest_id, json, token_id);
        if(errors.length == 0)
            softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive(processor));
        else if(errors.length == 1)
            softAssert.assertTrue(processor.ResponseValidator.GetResponseCode() == Integer.parseInt(errors[0]));
        else
            softAssert.assertTrue(fullMenuHelper.validateErrors(processor, errors));
        softAssert.assertAll();
    }
}
