package com.swiggy.api.erp.delivery.constants;

public interface DEAlchemistConstants {
    String DE_Latitude_more_than_5="12.9554712";
    String DE_longitude_more_than_5="77.5850857";
    String DE_Latitude_less_than_5="12.9270034";
    String DE_longitude_less_than_5="77.609277";
    String Cust_Latitude_more_than_5="12.8883177";
    String Cust_longitude_more_than_5="77.5876703";
    String Cust_Latitude_less_than_5="12.918621";
    String Cust_longitude_less_than_5="77.610493";
    String App_Version="1.9";
    String IncentiveGroup="Bangalore-FT1";
    String RestLat="12.915772";
    String RestLong="77.61001699999997";

}
