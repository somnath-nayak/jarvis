package com.swiggy.api.erp.ff.helper;

import com.rabbitmq.client.AMQP;
import com.swiggy.api.erp.ff.constants.LosConstants;
import com.swiggy.api.erp.ff.constants.OMSConstants;
import com.swiggy.api.erp.ff.constants.VerificationServiceConstants;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.RabbitMQHelper;
import org.apache.commons.io.FileUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VerificationServiceHelper implements VerificationServiceConstants {
	
	Initialize gameofthrones = new Initialize();
	LOSHelper losHelper = new LOSHelper();
	RabbitMQHelper rmqHelper = new RabbitMQHelper();
	OMSHelper omsHelper = new OMSHelper();
	
	public long getExpectedCustomerId(long order_id) throws JSONException{
		List<Map<String, Object>> orderDetailsList = SystemConfigProvider.getTemplate(omsDBHost).queryForList("select * from oms_rawdata where order_id = "+ order_id  +";");
		String payload = (String)orderDetailsList.get(0).get("payload");
		JSONObject jsonObj = new JSONObject(payload.toString());
		System.out.println("Customer id is = "+jsonObj.get("customer_id"));
		return jsonObj.getLong("customer_id");
	}
	
	public long getActualCustomerid(long order_id){
		List<Map<String, Object>> orderDetailsList = SystemConfigProvider.getTemplate(verificationDBHost).queryForList("select customer_id from fact_order where order_id = "+ order_id  +";");
		long customer_id = (long)orderDetailsList.get(0).get("customer_id");
		return customer_id;
	}
	
	public String getCurrentState(long orderId){
		String state = "";
		try {
			List<Map<String, Object>> orderDetailsList = SystemConfigProvider.getTemplate(verificationDBHost).queryForList("select state from verification_status where order_id = "+ orderId  +";");
			state = (String)orderDetailsList.get(0).get("state");
			return state;
		} catch (IndexOutOfBoundsException e) {
			e.printStackTrace();
			return "Exception, Order not in DB";
		}
	}
	
	public String getMetaData(long orderId){
		String metaData = "";
		try {
			List<Map<String, Object>> orderDetailsList = SystemConfigProvider.getTemplate(verificationDBHost).queryForList("select verification_info from verification_info where order_id = "+ orderId  +";");
			metaData = (String) orderDetailsList.get(0).get("verification_info");
			System.out.println("meta data ==============="+metaData);
			return metaData;
		} catch (IndexOutOfBoundsException e) {
			e.printStackTrace();
			return "Exception, Order not in DB";
		}
	}
	
	public String getVerificationChannel(long orderId){
		String channel = "";
		try {
			List<Map<String, Object>> orderDetailsList = SystemConfigProvider.getTemplate(verificationDBHost).queryForList("select channel from verification_status where order_id = "+ orderId  +";");
			channel = (String)orderDetailsList.get(0).get("channel");
			return channel;
		} catch (IndexOutOfBoundsException e) {
			e.printStackTrace();
			return "Exception, order not in DB";
		}
	}
	
	public boolean isOrderIdPresentInFactTable(long orderId){
		boolean flag = false;
		try {
			List<Map<String, Object>> orderDetailsList = SystemConfigProvider.getTemplate(verificationDBHost).queryForList("select order_id from fact_order where order_id = "+ orderId  +";");
			long actualOrder_id = (long)orderDetailsList.get(0).get("order_id");
			return actualOrder_id == orderId;
		} catch (IndexOutOfBoundsException e) {
			return flag;
		}
	}
	
	//verification BCP start
	public boolean isOrderIdPresentInVerificationTable(long order_id){
		boolean flag = false;
		try {
			List<Map<String, Object>> orderDetailsList = SystemConfigProvider.getTemplate(verificationDBHost).queryForList("SELECT order_id FROM verification.verification_status where order_id = "+ order_id  +";");
			long actualOrder_id = (long)orderDetailsList.get(0).get("order_id");
			return actualOrder_id == order_id;
		} catch (IndexOutOfBoundsException e) {
			return flag;
		}
	}
	
	public boolean StateOfOrderInVerificationTable(long order_id,int state){
		boolean flag = false;
		try {
			List<Map<String, Object>> orderDetailsList = SystemConfigProvider.getTemplate(verificationDBHost).queryForList("SELECT state FROM verification.verification_status where order_id = "+ order_id  +";");
			String actual_state = (String) orderDetailsList.get(0).get("state");
			int a=Integer.parseInt(actual_state);
			return a == state;
		} catch (IndexOutOfBoundsException e) {
			return flag;
		}
	}
	
	public static String StateOfOrderInVerificationTableInt(long order_id,int state){
		String flag = "0";
		try {
			List<Map<String, Object>> orderDetailsList = SystemConfigProvider.getTemplate(verificationDBHost).queryForList("SELECT state FROM verification.verification_status where order_id = "+ order_id  +";");
			String actual_state = (String) orderDetailsList.get(0).get("state");
			String ab=(actual_state);
			return ab;
		} catch (IndexOutOfBoundsException e) {
			return flag;
		}
	}
	
	
	
	
	
	//verification BCP end

	public boolean isVerificationRequired(long orderId){
		boolean flag = false;
		String metaData = "";
		try {
			List<Map<String, Object>> orderDetailsList = SystemConfigProvider.getTemplate(verificationDBHost).queryForList("select verification_info from verification_info where order_id = "+ orderId  +";");
			metaData = (String) orderDetailsList.get(0).get("verification_info");
			if(metaData.contains("")){
				flag = true;
			}
			return flag;
			
		} catch (IndexOutOfBoundsException e) {
			return flag;
		}
	}
	
	public void clearFactTable(String customerId){
		try {
			SystemConfigProvider.getTemplate(verificationDBHost).execute("delete from fact_order where customer_id = "+ customerId  +";");
		} catch (IndexOutOfBoundsException e) {
			System.out.println("There is no entry for customer_id = "+customerId+" in the fact_order table of verification DB");
		}
	}
	
	public void markOrderAsDelivered(String order_id) throws IOException, InterruptedException{
		omsHelper.verifyOrder(order_id, verifyAction);
		Thread.sleep(100);
        File file = new File("../Data/Payloads/JSON/losOrderStatusUpdate.json");
        for(int i=0;i<status.length;i++){
        	String statusUpdate = FileUtils.readFileToString(file);
        	System.out.println(status.length+ "  "+ status[i]);
        	statusUpdate = statusUpdate.replace("${order_id}", order_id).replace("${batch_id}",order_id.substring(8)).replace("${time}", String.valueOf(Instant.now().getEpochSecond())).replace("${status}", status[i]);
        	rmqHelper.pushMessageToExchange(LosConstants.hostName, LosConstants.statusUpdateQueue, new AMQP.BasicProperties().builder().contentType("application/json"), statusUpdate);
        }
	}
	
	public boolean queryOrdersInQueue(String order_id, String oeRole){
		try {
			List<Map<String, Object>> isOrderInQueue = SystemConfigProvider.getTemplate(omsDBHost).queryForList(queryForOrdersInqueue(order_id, oeRole));
			return (String.valueOf(isOrderInQueue.get(0).get("order_id")).equals(order_id));
		} catch (IndexOutOfBoundsException e) {
			// TODO: handle exception
			return false;
		}
	}

	public boolean queryOrdersInQueue(String order_id){
		try {
			List<Map<String, Object>> isOrderInQueue = SystemConfigProvider.getTemplate(omsDBHost).queryForList(queryForOrdersInqueue(order_id));
			return (String.valueOf(isOrderInQueue.get(0).get("order_id")).equals(order_id));
		} catch (IndexOutOfBoundsException e) {
			// TODO: handle exception
			return false;
		}
	}
	
	public boolean isOrderRelayedToRMS(long order_id){
		List<Map<String, Object>> isOrderRelayedToRMS = SystemConfigProvider.getTemplate(omsDBHost).queryForList(queryToCheckRelayToRMS(order_id));
		if(isOrderRelayedToRMS.get(0).get("relay_to_rms")!=null || isOrderRelayedToRMS.get(0).get("relay_to_rms")!="")
			return true;
		else
			return false;
	}
	
	private String convertTrueFalseToZeroOne(String valueToConvert){
		String convetedString = null;
		if(valueToConvert.equalsIgnoreCase("true")){
			convetedString = "1";
		}else if (valueToConvert.equalsIgnoreCase("false")) {
			convetedString = "0";
		}
		return convetedString;
	}
	
	public String convertCurrentOrderAaction(String userValidation, String distanceValidation, String duplicateValidation){
		String convetedString = null;
		convetedString = convertTrueFalseToZeroOne(userValidation)+convertTrueFalseToZeroOne(distanceValidation)+convertTrueFalseToZeroOne(distanceValidation);
		return convetedString;
	}
	
	public Processor queueForAssigningVerifier(String orderType, String channel, String userValidation, String distanceValidation, String duplicateValidation) throws IOException, InterruptedException{
		HashMap<String, String> requestheaders = new HashMap<>();
		requestheaders.put("content-type", "application/json");
		String orderId = losHelper.getAnOrder(orderType);
		Thread.sleep(3000);
    	String [] payloadParams = {orderId, channel, userValidation, distanceValidation, duplicateValidation};
    	GameOfThronesService service = new GameOfThronesService(OMSConstants.SERVICE, "queueforassigningverifier", gameofthrones);
    	Processor processor = new Processor(requestheaders, payloadParams, null, null, service);
    	List<Map<String, Object>> currentOrderAction = SystemConfigProvider.getTemplate(LosConstants.hostName).queryForList("select current_order_action from oms_order where order_id = "+orderId+";");
    	Assert.assertEquals(currentOrderAction.get(0).get("current_order_action"), convertTrueFalseToZeroOne(userValidation)+convertTrueFalseToZeroOne(distanceValidation)+convertTrueFalseToZeroOne(duplicateValidation), "Current order Action in DB did not match with the expected current order action");
		return processor;
	}
	
	public String queueForAssigningVerifierAndReturnOrderId(String orderType, String channel, String userValidation, String distanceValidation, String duplicateValidation) throws IOException, InterruptedException{
		HashMap<String, String> requestheaders = new HashMap<>();
		requestheaders.put("content-type", "application/json");
		String orderId = losHelper.getAnOrder(orderType);
		Thread.sleep(3000);
    	String [] payloadParams = {orderId, channel, userValidation, distanceValidation, duplicateValidation};
    	GameOfThronesService service = new GameOfThronesService(OMSConstants.SERVICE, "queueforassigningverifier", gameofthrones);
    	Processor processor = new Processor(requestheaders, payloadParams, null, null, service);
    	List<Map<String, Object>> currentOrderAction = SystemConfigProvider.getTemplate(LosConstants.hostName).queryForList("select current_order_action from oms_order where order_id = "+orderId+";");
    	Assert.assertEquals(currentOrderAction.get(0).get("current_order_action"), convertTrueFalseToZeroOne(userValidation)+convertTrueFalseToZeroOne(distanceValidation)+convertTrueFalseToZeroOne(duplicateValidation), "Current order Action in DB did not match with the expected current order action");
		return orderId;
	}
	
	public Processor relayOrderToVendorAndDelivery(String orderId){
		GameOfThronesService service = new GameOfThronesService(OMSConstants.SERVICE, "relayordertovendoranddelivery", gameofthrones);

		HashMap<String, String> requestheaders = new HashMap<>();
		requestheaders.put("content-type", "application/json");
    	String[] payloadParams = {orderId,"123"};
    	Processor processor = new Processor(service,requestheaders,payloadParams);
    	System.out.println(processor.RequestValidator.GetBodyAsText()+"=============================================");
		return processor;
	}
	
	public Processor hitRelayOrderToVendorAndDeliveryAPIAsGET(){
		HashMap<String, String> requestheaders = new HashMap<>();
		requestheaders.put("content-type", "application/json");
//    	String[] payloadParams = {orderId};
    	GameOfThronesService service = new GameOfThronesService(OMSConstants.SERVICE, "relayordertovendoranddeliveryasget", gameofthrones);
    	Processor processor = new Processor(requestheaders, null, null, null, service);
		return processor;
	}
	
	public Processor hitQueueForAssigningVerifierIAsGET(){
		HashMap<String, String> requestheaders = new HashMap<>();
		requestheaders.put("content-type", "application/json");
//    	String[] payloadParams = {orderId};
    	GameOfThronesService service = new GameOfThronesService(OMSConstants.SERVICE, "queueforassigningverifierasget", gameofthrones);
    	Processor processor = new Processor(requestheaders, null, null, null, service);
		return processor;
	}
	

	public String createOrderForVerificationService(boolean newUser, boolean longDistance, boolean crossAreaVerification, boolean isOrderReplicated, String orderType) throws IOException{
		Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat order_idFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        String orderId = order_idFormat.format(date);
        String orderTime = dateFormat.format(date);
        String epochTime = String.valueOf(Instant.now().getEpochSecond());
        String customerId = customerIdForVerificationService;
        String isLongDistance = "false";
        String isFirstOrderDelivered = "true";
        String isPartnerEnabled = "false";
        String typeOfPartner = "0";
        String restaurantCustomerDistance = "5.0";
        String isReplicated = "false";
        String orderIncoming = defaultOrderIncoming;
        
        if(isOrderReplicated)
        	isReplicated = "true";
        
        if(!longDistance && crossAreaVerification)
        	restaurantCustomerDistance = "8.0";
        
        if(longDistance && !crossAreaVerification)
        	restaurantCustomerDistance = "8.0";
        
        if(longDistance && crossAreaVerification)
        	restaurantCustomerDistance = "12.0";
        
        if(orderType.contains("partner")){
        	isPartnerEnabled = "true";
        	typeOfPartner = "3";
        }
        
        if(newUser){
        	customerId = String.valueOf(Instant.now().getEpochSecond());
			orderIncoming = orderIncomingForNewUser;
        	System.out.println(customerId+"!!!!@@@@@@@$%^^^^^^^^^&&&****((@@@@@@@");
        	isFirstOrderDelivered = "false";
        }
        if(longDistance)
        	isLongDistance = "true";
		losHelper.createOrderForVerificationService(orderId, orderTime, epochTime, customerId, isLongDistance, isFirstOrderDelivered, isPartnerEnabled, typeOfPartner, restaurantCustomerDistance, isReplicated, orderIncoming);
		
		return orderId;
	}
	
	public String getVerifyAction(boolean newUser, boolean crossAreaVerification, boolean duplicateCheck){
		String action ="", user = "0", distance = "0", duplicate = "0";
		if(newUser)
			user = "1";
		if(crossAreaVerification)
			distance = "1";
		if(duplicateCheck)
			duplicate = "1";
		action = user+distance+duplicate;
		
		return action;
	}
	
	public void assertCurrentStateInVerificationDBAfterVerification(Long orderId){
		Assert.assertEquals(getCurrentState(orderId), "1", "Expected current state was accepted but did not match with actual, orderId = "+orderId);
	}

	public void assertNewUserCases(Long orderId, boolean longDistance, boolean crossAreaVerification, boolean isOrderReplicated) throws InterruptedException{

		String scenario = String.valueOf(longDistance)+String.valueOf(crossAreaVerification)+String.valueOf(isOrderReplicated);
		String metaData = "";
		SoftAssert softAssertion= new SoftAssert();
		switch (scenario){
			case "falsefalsefalse":
				softAssertion.assertEquals(getVerificationChannel(orderId), "0", "Expected channel of verification was IVRS but did not match with actual. "+orderId);
				softAssertion.assertEquals(getCurrentState(orderId), "1", "expected current state was pending but did not match with actual "+orderId);
				metaData = getMetaData(orderId);
				softAssertion.assertTrue(metaData.contains("\"verificationRequired\":false") && metaData.contains("\"userValidation\":false") && metaData.contains("\"distanceValidation\":false") && metaData.contains("\"duplicateValidation\":false"), "Expected validation flags did no match in DB "+orderId);
				softAssertion.assertTrue(metaData.contains("\"live_cod_order_count\":0"), "Live COD orderCount should be zero in DB but found more than Zero "+orderId);
				softAssertion.assertTrue(metaData.contains("\"replicated\":false"), "value of replicated should be false in DB but found true "+orderId);
				softAssertion.assertTrue(metaData.contains("is_first_order_delivered\":false"), "value of is_first_order_delivered should be false in DB but found true "+orderId);
				softAssertion.assertAll();
				break;

			case "falsefalsetrue":
				softAssertion.assertEquals(getVerificationChannel(orderId), "0", "Expected channel of verification was None but did not match with actual. "+orderId);
				softAssertion.assertEquals(getCurrentState(orderId), "1", "expected current state was Accepted but did not match with actual "+orderId);
				metaData = getMetaData(orderId);
				softAssertion.assertTrue(metaData.contains("\"verificationRequired\":false") && metaData.contains("\"userValidation\":false")&& metaData.contains("\"distanceValidation\":false")&& metaData.contains("\"duplicateValidation\":false"), "Expected validation flags did no match in DB "+orderId);
				softAssertion.assertTrue(metaData.contains("\"live_cod_order_count\":0"), "Live COD orderCount should be zero in DB but found more than Zero "+orderId);
				softAssertion.assertTrue(metaData.contains("\"replicated\":true"), "value of replicated should be true in DB but found false "+orderId);
				softAssertion.assertTrue(metaData.contains("is_first_order_delivered\":false"), "value of is_first_order_delivered should be false in DB but found true "+orderId);
				softAssertion.assertTrue(!queryOrdersInQueue(String.valueOf(orderId), getOeRole("Verifier")), "Replicated Order is present in waiting for verifier queue "+orderId);
				softAssertion.assertAll();
				break;

			case "falsetruefalse":
				softAssertion.assertEquals(getVerificationChannel(orderId), "2", "Expected channel of verification was Manual but did not match with actual. "+orderId);
				softAssertion.assertEquals(getCurrentState(orderId), "0", "expected current state was pending but did not match with actual "+orderId);
				metaData = getMetaData(orderId);
				softAssertion.assertTrue(metaData.contains("\"verificationRequired\":true")&& metaData.contains("\"userValidation\":false")&& metaData.contains("\"distanceValidation\":true")&& metaData.contains("\"duplicateValidation\":false"), "Expected validation flags did no match in DB order ID is = "+orderId);
				softAssertion.assertTrue(metaData.contains("\"live_cod_order_count\":0"), "Live COD orderCount should be zero in DB but found more than Zero "+orderId);
				softAssertion.assertTrue(metaData.contains("\"replicated\":false"), "value of replicated should be false in DB but found true "+orderId);
				softAssertion.assertTrue(metaData.contains("is_first_order_delivered\":false"), "value of is_first_order_delivered should be false in DB but found true "+orderId);
				softAssertion.assertTrue(queryOrdersInQueue(String.valueOf(orderId), getOeRole("Verifier")), "Order is not present in waiting for verifier queue "+orderId);
				softAssertion.assertAll();
				break;

			case "falsetruetrue":
				softAssertion.assertEquals(getVerificationChannel(orderId), "0", "Expected channel of verification was None but did not match with actual. "+orderId);
				softAssertion.assertEquals(getCurrentState(orderId), "1", "expected current state was Accepted but did not match with actual "+orderId);
				metaData = getMetaData(orderId);
				softAssertion.assertTrue(metaData.contains("\"verificationRequired\":false")&& metaData.contains("\"userValidation\":false")&& metaData.contains("\"distanceValidation\":false")&& metaData.contains("\"duplicateValidation\":false"), "Expected validation flags did no match in DB "+orderId);
				softAssertion.assertTrue(metaData.contains("\"live_cod_order_count\":0"), "Live COD orderCount should be zero in DB but found more than Zero "+orderId);
				softAssertion.assertTrue(metaData.contains("\"replicated\":true"), "value of replicated should be true in DB but found false "+orderId);
				softAssertion.assertTrue(metaData.contains("is_first_order_delivered\":false"), "value of is_first_order_delivered should be false in DB but found true "+orderId);
				softAssertion.assertTrue(!queryOrdersInQueue(String.valueOf(orderId), getOeRole("Verifier")), "Replicated Order is present in waiting for verifier queue "+orderId);
				softAssertion.assertAll();
				break;

			case "truefalsefalse":
				softAssertion.assertEquals(getVerificationChannel(orderId), "0", "Expected channel of verification was IVRS but did not match with actual. " + orderId);
				softAssertion.assertEquals(getCurrentState(orderId), "1", "expected current state was pending but did not match with actual " + orderId);
				metaData = getMetaData(orderId);
				softAssertion.assertTrue(metaData.contains("\"verificationRequired\":false") && metaData.contains("\"userValidation\":false") && metaData.contains("\"distanceValidation\":false") && metaData.contains("\"duplicateValidation\":false"), "Expected validation flags did no match in DB " + orderId);
				softAssertion.assertTrue(metaData.contains("\"live_cod_order_count\":0"), "Live COD orderCount should be zero in DB but found more than Zero " + orderId);
				softAssertion.assertTrue(metaData.contains("\"replicated\":false"), "value of replicated should be false in DB but found true " + orderId);
				softAssertion.assertTrue(metaData.contains("is_first_order_delivered\":false"), "value of is_first_order_delivered should be false in DB but found true " + orderId);
				softAssertion.assertAll();
				break;

			case "truefalsetrue":
				softAssertion.assertEquals(getVerificationChannel(orderId), "0", "Expected channel of verification was None but did not match with actual. " + orderId);
				softAssertion.assertEquals(getCurrentState(orderId), "1", "expected current state was Accepted but did not match with actual " + orderId);
				metaData = getMetaData(orderId);
				softAssertion.assertTrue(metaData.contains("\"verificationRequired\":false") && metaData.contains("\"userValidation\":false") && metaData.contains("\"distanceValidation\":false") && metaData.contains("\"duplicateValidation\":false"), "Expected validation flags did no match in DB " + orderId);
				softAssertion.assertTrue(metaData.contains("\"live_cod_order_count\":0"), "Live COD orderCount should be zero in DB but found more than Zero " + orderId);
				softAssertion.assertTrue(metaData.contains("\"replicated\":true"), "value of replicated should be true in DB but found false " + orderId);
				softAssertion.assertTrue(metaData.contains("is_first_order_delivered\":false"), "value of is_first_order_delivered should be false in DB but found true " + orderId);
				softAssertion.assertTrue(!queryOrdersInQueue(String.valueOf(orderId), getOeRole("Verifier")), "Replicated Order is present in waiting for verifier queue " + orderId);
				softAssertion.assertAll();
				break;

			case "truetruefalse":
				softAssertion.assertEquals(getVerificationChannel(orderId), "2", "Expected channel of verification was IVRS but did not match with actual. order ID is = " + orderId);
				softAssertion.assertEquals(getCurrentState(orderId), "0", "expected current state was pending but did not match with actual " + orderId);
				metaData = getMetaData(orderId);
				softAssertion.assertTrue(metaData.contains("\"verificationRequired\":true") && metaData.contains("\"userValidation\":false") && metaData.contains("\"distanceValidation\":true") && metaData.contains("\"duplicateValidation\":false"), "Expected validation flags did no match in DB. order ID is = " + orderId);
				softAssertion.assertTrue(metaData.contains("\"live_cod_order_count\":0"), "Live COD orderCount should be zero in DB but found more than Zero " + orderId);
				softAssertion.assertTrue(metaData.contains("\"replicated\":false"), "value of replicated should be false in DB but found true " + orderId);
				softAssertion.assertTrue(metaData.contains("is_first_order_delivered\":false"), "value of is_first_order_delivered should be false in DB but found true " + orderId);
				softAssertion.assertTrue(queryOrdersInQueue(String.valueOf(orderId), getOeRole("Verifier")), "Order is not present in waiting for verifier queue " + orderId);
				softAssertion.assertAll();
				break;

			case "truetruetrue":
				softAssertion.assertEquals(getVerificationChannel(orderId), "0", "Expected channel of verification was None but did not match with actual. " + orderId);
				softAssertion.assertEquals(getCurrentState(orderId), "1", "expected current state was Accepted but did not match with actual " + orderId);
				metaData = getMetaData(orderId);
				softAssertion.assertTrue(metaData.contains("\"verificationRequired\":false") && metaData.contains("\"userValidation\":false") && metaData.contains("\"distanceValidation\":false") && metaData.contains("\"duplicateValidation\":false"), "Expected validation flags did no match in DB " + orderId);
				softAssertion.assertTrue(metaData.contains("\"live_cod_order_count\":0"), "Live COD orderCount should be zero in DB but found more than Zero " + orderId);
				softAssertion.assertTrue(metaData.contains("\"replicated\":true"), "value of replicated should be true in DB but found false " + orderId);
				softAssertion.assertTrue(metaData.contains("is_first_order_delivered\":false"), "value of is_first_order_delivered should be false in DB but found true " + orderId);
				softAssertion.assertTrue(!queryOrdersInQueue(String.valueOf(orderId), getOeRole("Verifier")), "Replicated Order is present in waiting for verifier queue " + orderId);
				softAssertion.assertAll();
				break;
		}
	}

    public void assertCrossAreaCases(Long orderId, boolean longDistance, boolean crossAreaVerification, boolean isOrderReplicated) throws InterruptedException{

        String scenario = String.valueOf(longDistance)+String.valueOf(crossAreaVerification)+String.valueOf(isOrderReplicated);
        String metaData = "";
		SoftAssert softAssertion= new SoftAssert();
        switch (scenario){
            case "falsefalsefalse":
                softAssertion.assertEquals(getVerificationChannel(orderId), "0", "Expected channel of verification was NONE but did not match with actual. "+orderId);
                softAssertion.assertEquals(getCurrentState(orderId), "1", "expected current state was pending but did not match with actual "+orderId);
                metaData = getMetaData(orderId);
                softAssertion.assertTrue(metaData.contains("\"verificationRequired\":false") && metaData.contains("\"userValidation\":false") && metaData.contains("\"distanceValidation\":false") && metaData.contains("\"duplicateValidation\":false"), "Expected validation flags did no match in DB "+orderId);
                softAssertion.assertTrue(metaData.contains("\"live_cod_order_count\":0"), "Live COD orderCount should be zero in DB but found more than Zero "+orderId);
                softAssertion.assertTrue(metaData.contains("\"replicated\":false"), "value of replicated should be false in DB but found true "+orderId);
                softAssertion.assertTrue(metaData.contains("is_first_order_delivered\":true"), "value of is_first_order_delivered should be true in DB but found false "+orderId);
                softAssertion.assertTrue(!queryOrdersInQueue(String.valueOf(orderId), getOeRole("Verifier")), "Order is present in waiting for verifier queue "+orderId);
                softAssertion.assertAll();
                break;
            case "falsefalsetrue":
                softAssertion.assertEquals(getVerificationChannel(orderId), "0", "Expected channel of verification was None but did not match with actual. "+orderId);
                softAssertion.assertEquals(getCurrentState(orderId), "1", "expected current state was Accepted but did not match with actual "+orderId);
                metaData = getMetaData(orderId);
                softAssertion.assertTrue(metaData.contains("\"verificationRequired\":false") && metaData.contains("\"userValidation\":false") && metaData.contains("\"distanceValidation\":false") && metaData.contains("\"duplicateValidation\":false"), "Expected validation flags did no match in DB "+orderId);
                softAssertion.assertTrue(metaData.contains("\"live_cod_order_count\":0"), "Live COD orderCount should be zero in DB but found more than Zero "+orderId);
                softAssertion.assertTrue(metaData.contains("\"replicated\":true"), "value of replicated should be true in DB but found false "+orderId);
                softAssertion.assertTrue(metaData.contains("is_first_order_delivered\":true"), "value of is_first_order_delivered should be true in DB but found false "+orderId);
                softAssertion.assertTrue(!queryOrdersInQueue(String.valueOf(orderId), getOeRole("Verifier")), "Replicated Order is present in waiting for verifier queue "+orderId);
                softAssertion.assertAll();
                break;
            case "falsetruefalse":
                softAssertion.assertEquals(getVerificationChannel(orderId), "2", "Expected channel of verification was Manual but did not match with actual. "+orderId);
                softAssertion.assertEquals(getCurrentState(orderId), "0", "expected current state was pending but did not match with actual "+orderId);
                metaData = getMetaData(orderId);
                softAssertion.assertTrue(metaData.contains("\"verificationRequired\":true") && metaData.contains("\"userValidation\":false") && metaData.contains("\"distanceValidation\":true") && metaData.contains("\"duplicateValidation\":false"), "Expected validation flags did no match in DB order ID is = "+orderId);
                softAssertion.assertTrue(metaData.contains("\"live_cod_order_count\":0"), "Live COD orderCount should be zero in DB but found more than Zero "+orderId);
                softAssertion.assertTrue(metaData.contains("\"replicated\":false"), "value of replicated should be false in DB but found true "+orderId);
                softAssertion.assertTrue(metaData.contains("is_first_order_delivered\":true"), "value of is_first_order_delivered should be true in DB but found false "+orderId);
                softAssertion.assertTrue(queryOrdersInQueue(String.valueOf(orderId), getOeRole("Verifier")), "Order is not present in waiting for verifier queue "+orderId);
                softAssertion.assertAll();
                break;
            case "falsetruetrue":
                softAssertion.assertEquals(getVerificationChannel(orderId), "0", "Expected channel of verification was None but did not match with actual. "+orderId);
                softAssertion.assertEquals(getCurrentState(orderId), "1", "expected current state was Accepted but did not match with actual "+orderId);
                metaData = getMetaData(orderId);
                softAssertion.assertTrue(metaData.contains("\"verificationRequired\":false") && metaData.contains("\"userValidation\":false") && metaData.contains("\"distanceValidation\":false") && metaData.contains("\"duplicateValidation\":false"), "Expected validation flags did no match in DB "+orderId);
                softAssertion.assertTrue(metaData.contains("\"live_cod_order_count\":0"), "Live COD orderCount should be zero in DB but found more than Zero "+orderId);
                softAssertion.assertTrue(metaData.contains("\"replicated\":true"), "value of replicated should be true in DB but found false "+orderId);
                softAssertion.assertTrue(metaData.contains("is_first_order_delivered\":true"), "value of is_first_order_delivered should be true in DB but found false "+orderId);
                softAssertion.assertTrue(!queryOrdersInQueue(String.valueOf(orderId), getOeRole("Verifier")), "Replicated Order is present in waiting for verifier queue "+orderId);
                softAssertion.assertAll();
                break;

            case "truefalsefalse":
                softAssertion.assertEquals(getVerificationChannel(orderId), "0", "Expected channel of verification was NONE but did not match with actual. "+orderId);
                softAssertion.assertEquals(getCurrentState(orderId), "1", "expected current state was Accepted but did not match with actual "+orderId);
                metaData = getMetaData(orderId);
                softAssertion.assertTrue(metaData.contains("\"verificationRequired\":false") &&metaData.contains("\"userValidation\":false") &&metaData.contains("\"distanceValidation\":false") &&metaData.contains("\"duplicateValidation\":false"), "Expected validation flags did no match in DB "+orderId);
                softAssertion.assertTrue(metaData.contains("\"live_cod_order_count\":0"), "Live COD orderCount should be zero in DB but found more than Zero "+orderId);
                softAssertion.assertTrue(metaData.contains("\"replicated\":false"), "value of replicated should be false in DB but found true "+orderId);
                softAssertion.assertTrue(metaData.contains("is_first_order_delivered\":true"), "value of is_first_order_delivered should be true in DB but found false "+orderId);
                softAssertion.assertTrue(!queryOrdersInQueue(String.valueOf(orderId), getOeRole("Verifier")), "Order is present in waiting for verifier queue "+orderId);
				softAssertion.assertAll();
                break;

            case "truefalsetrue":
                softAssertion.assertEquals(getVerificationChannel(orderId), "0", "Expected channel of verification was None but did not match with actual. "+orderId);
                softAssertion.assertEquals(getCurrentState(orderId), "1", "expected current state was Accepted but did not match with actual "+orderId);
                metaData = getMetaData(orderId);
                softAssertion.assertTrue(metaData.contains("\"verificationRequired\":false") &&metaData.contains("\"userValidation\":false") &&metaData.contains("\"distanceValidation\":false") &&metaData.contains("\"duplicateValidation\":false"), "Expected validation flags did no match in DB "+orderId);
                softAssertion.assertTrue(metaData.contains("\"live_cod_order_count\":0"), "Live COD orderCount should be zero in DB but found more than Zero "+orderId);
                softAssertion.assertTrue(metaData.contains("\"replicated\":true"), "value of replicated should be true in DB but found false "+orderId);
                softAssertion.assertTrue(metaData.contains("is_first_order_delivered\":true"), "value of is_first_order_delivered should be true in DB but found false "+orderId);
                softAssertion.assertTrue(!queryOrdersInQueue(String.valueOf(orderId), getOeRole("Verifier")), "Replicated Order is present in waiting for verifier queue "+orderId);
				softAssertion.assertAll();
                break;

            case "truetruefalse":
                softAssertion.assertEquals(getVerificationChannel(orderId), "2", "Expected channel of verification was IVRS but did not match with actual. order ID is = "+orderId);
                softAssertion.assertEquals(getCurrentState(orderId), "0", "expected current state was pending but did not match with actual "+orderId);
                metaData = getMetaData(orderId);
                softAssertion.assertTrue(metaData.contains("\"verificationRequired\":true") &&metaData.contains("\"userValidation\":false") &&metaData.contains("\"distanceValidation\":true") &&metaData.contains("\"duplicateValidation\":false"), "Expected validation flags did no match in DB. order ID is = "+orderId);
                softAssertion.assertTrue(metaData.contains("\"live_cod_order_count\":0"), "Live COD orderCount should be zero in DB but found more than Zero "+orderId);
                softAssertion.assertTrue(metaData.contains("\"replicated\":false"), "value of replicated should be false in DB but found true "+orderId);
                softAssertion.assertTrue(metaData.contains("is_first_order_delivered\":true"), "value of is_first_order_delivered should be true in DB but found false "+orderId);
                softAssertion.assertTrue(queryOrdersInQueue(String.valueOf(orderId), getOeRole("Verifier")), "Order is not present in waiting for verifier queue "+orderId);
				softAssertion.assertAll();
                break;

            case "truetruetrue":
                softAssertion.assertEquals(getVerificationChannel(orderId), "0", "Expected channel of verification was None but did not match with actual. "+orderId);
                softAssertion.assertEquals(getCurrentState(orderId), "1", "expected current state was Accepted but did not match with actual "+orderId);
                metaData = getMetaData(orderId);
                softAssertion.assertTrue(metaData.contains("\"verificationRequired\":false") &&metaData.contains("\"userValidation\":false") &&metaData.contains("\"distanceValidation\":false") &&metaData.contains("\"duplicateValidation\":false"), "Expected validation flags did no match in DB "+orderId);
                softAssertion.assertTrue(metaData.contains("\"live_cod_order_count\":0"), "Live COD orderCount should be zero in DB but found more than Zero "+orderId);
                softAssertion.assertTrue(metaData.contains("\"replicated\":true"), "value of replicated should be true in DB but found false "+orderId);
                softAssertion.assertTrue(metaData.contains("is_first_order_delivered\":true"), "value of is_first_order_delivered should be true in DB but found false "+orderId);
                softAssertion.assertTrue(!queryOrdersInQueue(String.valueOf(orderId), getOeRole("Verifier")), "Replicated Order is present in waiting for verifier queue "+orderId);
				softAssertion.assertAll();
                break;
        }
    }

}
