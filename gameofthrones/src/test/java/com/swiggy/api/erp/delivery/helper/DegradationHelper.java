package com.swiggy.api.erp.delivery.helper;

import com.swiggy.api.erp.delivery.constants.DegradationConstant;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.annotations.Test;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by preetesh.sharma on 23/04/18.
 */


public class DegradationHelper {

    Initialize init;
    GameOfThronesService gameOfThronesService;
    Map<String, String> paramDefaultMap;


    public DegradationHelper() {
        init = new Initialize();
        paramDefaultMap = new HashMap<>();
        paramDefaultMap.put("MAX_SLA", "60");
        paramDefaultMap.put("LAST_MILE", "4");
    }

    public Boolean createEntity(String[] payload) {
        gameOfThronesService = new GameOfThronesService("deliverydegradation", "entitycreationrequest", init);
        HashMap<String, String> headerMap = new HashMap<String, String>();
        headerMap.put("Content-type", "application/json");
        Processor processor = new Processor(gameOfThronesService, headerMap, payload);
        Integer entityid = processor.ResponseValidator.GetNodeValueAsInt("id");
        String name = processor.ResponseValidator.GetNodeValue("name");
        Processor processor1 = getEntityById(entityid.toString());
        Integer entityidfromget = processor1.ResponseValidator.GetNodeValueAsInt("id");
        String namefromget = processor1.ResponseValidator.GetNodeValue("name");
        return (entityid == entityidfromget) & (namefromget.equalsIgnoreCase(name));
    }

    public Processor getEntityById(String entityid) {
        gameOfThronesService = new GameOfThronesService("deliverydegradation", "getentityrequest", init);
        Processor processor = new Processor(gameOfThronesService, DegradationConstant.singleHeader(), null, new String[]{entityid});
        return processor;
    }

    //add states to entity type
    public Boolean createState(String[] payload) {
        gameOfThronesService = new GameOfThronesService("deliverydegradation", "statecreationrequest", init);
        Processor processor = new Processor(gameOfThronesService, DegradationConstant.singleHeader(), payload);
        String stateid = processor.ResponseValidator.GetNodeValue("id");
        String name = processor.ResponseValidator.GetNodeValue("name");
        Processor processor1 = getStateById(stateid);
        String namefromget = processor1.ResponseValidator.GetNodeValue("name");
        return name == namefromget;
    }

    public Processor getRuleOnEntityEdge(String entityId, String edgeId) {
        gameOfThronesService = new GameOfThronesService("deliverydegradation", "getentityedgerule", init);
        Processor processor = new Processor(gameOfThronesService, DegradationConstant.singleHeader(), new String[]{entityId, edgeId});
        return processor;
    }
    
    
    
    

    public String getBannerFactorRule(String from_state, String to_state, Integer zone_id, String supply_state) {
        String bfQuery = "select rule from rules r, edges e where  r.edge_id=e.id and from_state=(select id from states where lower(name) =lower('" + from_state + "')) " +
                " and to_state=(select id from states where lower(name) =lower('" + to_state + "')) " +
                " and entity_id=(select id from entities where reference_id=" + zone_id + " and status='ACTIVE') and machine_id=" +
                "(select id from machines where lower(name) =lower('" + supply_state + "')) order by 1 desc limit 1";
        String rule = SystemConfigProvider.getTemplate("degradation").queryForMap(bfQuery).get("rule").toString();
        Pattern p = Pattern.compile("([0-9](\\.[0-9]{1,3})?)|(\\.[0-9]{1,3})");
        Pattern p1 = Pattern.compile("[><]");
        String bfvalue = null;
        String expression = null;
        System.out.println("rule is "+rule);
        String bfrule[] = rule.split("&&");
        for (String str : bfrule)
        {
        	System.out.println("First Split is "+ str);
            if (str.contains("bf") || str.toLowerCase().contains("bannerfactor")) {
                Matcher m = p.matcher(str);
                System.out.println(m.find());
                bfvalue = m.group();
               Matcher m1 = p1.matcher(str);
                System.out.println(m1.find());
               expression = m1.group();
            }
        }
        System.out.println("********* BF VALUE ********** : " + bfvalue);
        return bfvalue + "__" + expression;
    }

    public String createState(String[] payload, String test) {
        gameOfThronesService = new GameOfThronesService("deliverydegradation", "statecreationrequest", init);
        HashMap<String, String> headerMap = new HashMap<String, String>();
        headerMap.put("Content-type", "application/json");
        Processor processor = new Processor(gameOfThronesService, headerMap, payload);
        Integer stateid = processor.ResponseValidator.GetNodeValueAsInt("id");
        String name = processor.ResponseValidator.GetNodeValue("name");
        return name + "__" + stateid;
    }

    public Processor getStateById(String stateid) {
        gameOfThronesService = new GameOfThronesService("deliverydegradation", "getstaterequest", init);
        Processor processor = new Processor(gameOfThronesService, DegradationConstant.singleHeader(), null, new String[]{stateid});
        return processor;
    }

    //add params to state, not sure this will run or not

    public Processor createParamsForState(String[] payload, String[] urlparams) {
        gameOfThronesService = new GameOfThronesService("deliverydegradation", "stateparamcreationrequest", init);
        Processor processor = new Processor(gameOfThronesService, DegradationConstant.singleHeader(), payload, urlparams);
        return processor;
    }

    public void addParamsToState(Map<String, String> parammap, Integer zone_id, String state) {
        for (String str : paramDefaultMap.keySet()) {
            if (!parammap.containsKey(str)) {
                parammap.put(str, paramDefaultMap.get(str));
            }
        }
        Integer entityId = getEntityIdFromReferenceId(zone_id);
        Integer stateId = getStateIdFromName(state);
        for (String param : parammap.keySet()) {
            Processor processor = createParamsForState(new String[]{param, parammap.get(param)}, new String[]{entityId.toString(), stateId.toString()});
        }

    }

    public Processor addWebhook(String endPoint, String name) {
        gameOfThronesService = new GameOfThronesService("deliverydegradation", "webHookClientCreationRequest", init);
        Processor processor = new Processor(gameOfThronesService, DegradationConstant.singleHeader(), new String[]{endPoint, name});
        return processor;

    }

    public Processor subscribeEntityToWebhook(String entityType, String machineType, String client_id) {
        gameOfThronesService = new GameOfThronesService("deliverydegradation", "webHookSubscriptionRequest", init);
        Processor processor = new Processor(gameOfThronesService, DegradationConstant.singleHeader(), new String[]{entityType, machineType}, new String[]{client_id});
        return processor;
    }

    public Boolean addWebhookAndSubscribe(String endPoint, String name, String entityType, String machineType) {
        String client_id_query = "select id from webhook_clients where lower( name)='" + name + "' and lower(status)='active'";
        Processor processor_addclient = addWebhook(endPoint, name);
        Map<String, Object> webMap = SystemConfigProvider.getTemplate("degradation").queryForMap(client_id_query);
        if (webMap == null) {
            return false;
        }
        String client_id = webMap.get("id").toString();
        Processor processor_subscribe = subscribeEntityToWebhook(entityType, machineType, client_id);
        // Boolean subscription_status=processor_subscribe.ResponseValidator.GetNodeValueAsBool("success");
        String subscribedclientId = SystemConfigProvider.getTemplate("degradation").queryForMap("select client_id from webhook_subscriptions where client_id=" + client_id).get("client_id").toString();
        return subscribedclientId == client_id;
    }

    public Processor getStateParams(String entityid, String stateid) {
        gameOfThronesService = new GameOfThronesService("deliverydegradation", "getstateparamrequest", init);
        Processor processor = new Processor(gameOfThronesService, DegradationConstant.singleHeader(), null, new String[]{entityid, stateid});
        return processor;
    }


    //add machine
    public Boolean createMachine(String[] payload) {
        gameOfThronesService = new GameOfThronesService("deliverydegradation", "machinecreationrequest", init);
        HashMap<String, String> headerMap = new HashMap<String, String>();
        headerMap.put("Content-type", "application/json");
        Processor processor = new Processor(gameOfThronesService, headerMap, payload);
        String machineid = processor.ResponseValidator.GetNodeValue("id");
        String machinename = processor.ResponseValidator.GetNodeValue("name");
        Processor processor1 = getMachineById(machineid);
        String machinenamefromget = processor1.RequestValidator.GetNodeValue("name");
        return machinename == machinenamefromget;
    }

    public String createMachine(String[] payload, String test) {
        gameOfThronesService = new GameOfThronesService("deliverydegradation", "machinecreationrequest", init);
        HashMap<String, String> headerMap = new HashMap<String, String>();
        headerMap.put("Content-type", "application/json");
        Processor processor = new Processor(gameOfThronesService, headerMap, payload);
        Integer machineid = processor.ResponseValidator.GetNodeValueAsInt("id");
        return machineid.toString();
    }

    public Processor getMachineById(String machineid) {
        gameOfThronesService = new GameOfThronesService("deliverydegradation", "getmachinerequest", init);
        Processor processor = new Processor(gameOfThronesService, DegradationConstant.singleHeader(), null, new String[]{machineid});
        return processor;
    }

    public Processor getActiveMachinesForEntity(String entityid) {
        gameOfThronesService = new GameOfThronesService("deliverydegradation", "getactivemachinerequest", init);
        Processor processor = new Processor(gameOfThronesService, DegradationConstant.singleHeader(), null, new String[]{entityid});
        return processor;
    }

    public Processor getEntityCurrentState(String entityid) {
        gameOfThronesService = new GameOfThronesService("deliverydegradation", "getentitycurrentstate", init);
        Processor processor = new Processor(gameOfThronesService, DegradationConstant.singleHeader(), null, new String[]{entityid});
        return processor;
    }

    public String getSupplyState(String entityId) {
        Processor processor = getActiveMachinesForEntity(entityId);
        String supplystate = processor.ResponseValidator.GetBodyAsText();
        String machineId = null;
        if (!supplystate.equals("[]")) {
            try {
                machineId = new JSONObject(new JSONArray(processor.ResponseValidator.GetBodyAsText()).get(0).toString()).get("machineId").toString();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else
            return null;
        String machinename = SystemConfigProvider.getTemplate("degradation").queryForMap("select name from machines where id=" + machineId).get("name").toString();
        return machinename;
    }

    public String get_current_degradation_state(String entityId) {
        Processor processor = getEntityCurrentState(entityId);
        // Integer stateId=processor.ResponseValidator.GetNodeValueAsInt("stateId");
        String degradationstate = processor.ResponseValidator.GetBodyAsText();
        String stateId = null;
        if (!degradationstate.equals("[]")) {
            try {
                stateId = new JSONObject(new JSONArray(processor.ResponseValidator.GetBodyAsText()).get(0).toString()).get("stateId").toString();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else
            return null;
        String statename = SystemConfigProvider.getTemplate("degradation").queryForMap("select name from states where id=" + stateId).get("name").toString();
        return statename;

    }


    public String getActiveMachinesForEntity(Integer entityid) {
        gameOfThronesService = new GameOfThronesService("deliverydegradation", "getactivemachinerequest", init);
        Processor processor = new Processor(gameOfThronesService, DegradationConstant.singleHeader(), null, new String[]{entityid.toString()});
        Integer machineId = processor.ResponseValidator.GetNodeValueAsInt("machineId");
        return machineId.toString();
    }


    public Processor activateMachine(Integer entityid, Integer machineid) {
        gameOfThronesService = new GameOfThronesService("deliverydegradation", "activatemachine", init);
        Processor processor = new Processor(gameOfThronesService, DegradationConstant.singleHeader(), new String[]{machineid.toString()}, new String[]{entityid.toString()});
        return processor;
    }

    public Processor changeMachineState(Integer entityid, Integer machineid, Integer stateid) {
        gameOfThronesService = new GameOfThronesService("deliverydegradation", "changestate", init);
        Processor processor = new Processor(gameOfThronesService, DegradationConstant.singleHeader(), new String[]{entityid.toString(), machineid.toString(), stateid.toString()});
        return processor;
    }

    public Boolean changeStateAndValidate(Integer zone_id, String supplystate, String todegradationstate) {

        Integer entity_id = getEntityIdFromReferenceId(zone_id);
        if (entity_id == 0)
            return false;
        Integer machineid = getSupplyStateIdFromName(supplystate, entity_id);
        if (machineid == 0)
            return false;
        Integer stateid = getStateIdFromName(todegradationstate);
        if (stateid == 0)
            return false;
        String current_ds = get_current_degradation_state(entity_id.toString());
        Processor processor = changeMachineState(entity_id, machineid, stateid);
        String changedstate = get_current_degradation_state(entity_id.toString());
        return changedstate == current_ds;
        //get current state
        //call change state,
        // get state again and validate it is changes ot not

    }

    public boolean changeMachineAndValidate(Integer zone_id, String supplystate, String todegradationstate) {
        Integer entity_id = getEntityIdFromReferenceId(zone_id);
        if (entity_id == 0)
            return false;
        Integer machineid = getSupplyStateIdFromName(supplystate, entity_id);
        if (machineid == 0)
            return false;
        Integer stateid = getStateIdFromName(todegradationstate);
        if (stateid == 0)
            return false;
        String current_ss = getSupplyState(entity_id.toString());
        if (current_ss == null || !current_ss.toLowerCase().equalsIgnoreCase(supplystate)) {
            activateMachine(entity_id, machineid);
        }
        Processor processor = changeMachineState(entity_id, machineid, stateid);
        String changedstate = getSupplyState(entity_id.toString());
        return supplystate.toLowerCase() == changedstate.toLowerCase();

    }

    public Integer getEntityIdFromReferenceId(Integer referenceId) {
        Map<String, Object> entityMap = SystemConfigProvider.getTemplate("degradation").queryForMap("select id from entities where reference_id=" + referenceId);
        Integer entity_id = 0;
        if (entityMap != null) {
            entity_id = Integer.parseInt(entityMap.get("id").toString());
        }
        return entity_id;

    }
    
    

    public Integer getSupplyStateIdFromName(String supplystate, Integer entityid) {
        Integer machineId = 0;
        Map<String, Object> machineMap = SystemConfigProvider.getTemplate("degradation").queryForMap("select id from machines where lower(name)='" + supplystate.toLowerCase() +
                "' and entity_type=(select entity_type from entities where id=" + entityid + ")");
        if (machineMap != null) {
            machineId = Integer.parseInt(machineMap.get("id").toString());
        }
        return machineId;
    }
    
    public String getSupplyStateNameFromId(String machineId) {
        String machineName = null;
        Map<String, Object> machineMap = SystemConfigProvider.getTemplate("degradation").queryForMap("select name from machines where id='" + machineId + "'");
        if (machineMap != null) {
        	machineName = machineMap.get("name").toString();
        }
        return machineName;
    }
    
    public String getEntityNameFromId(String entityId) 
    {
        String entityName = SystemConfigProvider.getTemplate("degradation").queryForMap("select name from entities where id='" + entityId + "'").get("name").toString();
        return entityName;
    }


    public Integer getStateIdFromName(String statename) {
        Integer stateId = 0;
        Map<String, Object> stateMap = SystemConfigProvider.getTemplate("degradation").queryForMap("select id from states where lower(name)='" + statename.toLowerCase() + "'");
        if (stateMap != null) {
            stateId = Integer.parseInt(stateMap.get("id").toString());
        }
        return stateId;
    }

    //add edge between states

    public Boolean createEdge(String[] payload) {
        gameOfThronesService = new GameOfThronesService("deliverydegradation", "edgecreationrequest", init);
        Processor processor = new Processor(gameOfThronesService, DegradationConstant.singleHeader(), payload);
        String edgename = processor.ResponseValidator.GetNodeValue("name");
        Integer edgeid = processor.ResponseValidator.GetNodeValueAsInt("id");
        Processor processor1 = getEdgeDetails(edgeid.toString());
        String edgenameasget = processor1.ResponseValidator.GetNodeValue("name");
        return edgename == edgenameasget;
    }
    
    public Processor createEdgeBetweenStates(String[] payload) {
        gameOfThronesService = new GameOfThronesService("deliverydegradation", "edgecreationrequest", init);
        Processor processor = new Processor(gameOfThronesService, DegradationConstant.singleHeader(), payload);
        return processor;
    }
    
    public Processor deleteEdgeById(String edgeId) {
        gameOfThronesService = new GameOfThronesService("deliverydegradation", "deleteEdgeById", init);
        Processor processor = new Processor(gameOfThronesService, DegradationConstant.singleHeader(), null, new String[] {edgeId});
        return processor;
    }

   /* public Boolean createEdge(JSONObject payload) {
        gameOfThronesService = new GameOfThronesService("deliverydegradation", "edgecreationrequest", init);
        HashMap<String, String> headerMap = new HashMap<String, String>();
        headerMap.put("Content-type", "application/json");
        Processor processor = new Processor(gameOfThronesService, headerMap, payload.toString());
        String edgename = processor.ResponseValidator.GetNodeValue("name");
        Integer edgeid = processor.ResponseValidator.GetNodeValueAsInt("id");
        Processor processor1 = getEdgeDetails(edgeid.toString());
        String edgenameasget = processor1.ResponseValidator.GetNodeValue("name");
        return edgename == edgenameasget;
    }*/

    public Processor ListMachineEdges(String machineid) {
        gameOfThronesService = new GameOfThronesService("deliverydegradation", "listedgepermachinerequest", init);
        Processor processor = new Processor(gameOfThronesService, DegradationConstant.singleHeader(), null, new String[]{machineid});
        return processor;

    }

    public Processor getEdgeDetails(String edgeid) {
        gameOfThronesService = new GameOfThronesService("deliverydegradation", "getedgedetailsrequest", init);
        Processor processor = new Processor(gameOfThronesService, DegradationConstant.singleHeader(), null, new String[]{edgeid});
        return processor;
    }
    
    
    public Processor updateEntity(String[] payload, String entityId)
    {
    	 gameOfThronesService = new GameOfThronesService("deliverydegradation", "updateEntity", init);
         Processor processor = new Processor(gameOfThronesService, DegradationConstant.singleHeader(), payload, new String[]{entityId});
         return processor;
    }


    public boolean putZoneInDegradation(Integer zone_id, String degradation_state, String supply_state) {

        Integer entityId = getEntityIdFromReferenceId(zone_id);
        boolean zoneInDegradation = false;
        if (entityId == 0)
            return false;
        String current_ss = getSupplyState(entityId.toString());
        String current_ds = get_current_degradation_state(entityId.toString());
        if (supply_state == null) {
            supply_state = current_ss;
        }
        if (((current_ss == null) && (current_ds == null)) || (!(current_ss.toLowerCase().equalsIgnoreCase(supply_state)))) {
            return changeMachineAndValidate(zone_id, supply_state, degradation_state);

        } else if (current_ss.toLowerCase().equalsIgnoreCase(supply_state) && !current_ds.toLowerCase().equalsIgnoreCase(degradation_state)) {
            return changeStateAndValidate(zone_id, supply_state, degradation_state);
        } else {
            return true;
        }

    }

    //Method to create data
    public LinkedHashMap<String, LinkedHashMap<String, String>> createPreData(String[][] dataarray) {
        LinkedHashMap<String, LinkedHashMap<String, String>> bfmap = new LinkedHashMap<>();
        List<String> statelist = new ArrayList<>();
        statelist.add("normal");
        statelist.add("light");
        statelist.add("heavy");
        statelist.add("extreme");
        List<String> MachineMap = new ArrayList<String>();
        MachineMap.add("BAU".toLowerCase());
        MachineMap.add("SlightlyStressed".toLowerCase());
        MachineMap.add("HeavilyStressed".toLowerCase());
        MachineMap.add("ExtremelyStressed".toLowerCase());

        for (int j = 0; j < statelist.size(); j++) {
            LinkedHashMap<String, String> tempmap = new LinkedHashMap<>();
            for (int i = 0; i < dataarray.length; i++) {

                tempmap.put(statelist.get(i), dataarray[j][i]);
            }
            bfmap.put(MachineMap.get(j), tempmap);
        }
        return bfmap;

    }

    public Map<String, String> zIndexMapper() {
        Map<String, String> zindexMap = new HashMap<>();
        zindexMap.put("NL", "1");
        zindexMap.put("NH", "2");
        zindexMap.put("NE", "3");
        zindexMap.put("LH", "1");
        zindexMap.put("LE", "2");
        zindexMap.put("LN", "4");
        zindexMap.put("HE", "1");
        zindexMap.put("HL", "4");
        zindexMap.put("HN", "5");
        zindexMap.put("EH", "4");
        zindexMap.put("EL", "5");
        zindexMap.put("EN", "6");
        return zindexMap;
    }


    public String[][] getAllBfValues() {
        String[][] str = new String[4][4];
        str[0][0] = "0.8";
        str[0][1] = "1.1";
        str[0][2] = "1.2";
        str[0][3] = "1.4";
        str[1][0] = "0.8";
        str[1][1] = "1.1";
        str[1][2] = "1.2";
        str[1][3] = "1.4";
        str[2][0] = "0.8";
        str[2][1] = "1.1";
        str[2][2] = "1.2";
        str[2][3] = "1.4";
        str[3][0] = "0.8";
        str[3][1] = "1.1";
        str[3][2] = "1.2";
        str[3][3] = "1.4";
        return str;
    }

    public void createMachinesWithEdges(String entityid, String initialState, String machineName, String machineType) {
        //create machine with machine name, machine type and initial state
        //once done, create edges with bf values
        String[][] bfarray = getAllBfValues();
        Map<String, String> zIndexMap = zIndexMapper();
        LinkedHashMap<String, LinkedHashMap<String, String>> allmachineData = createPreData(bfarray);
        LinkedHashMap<String, String> thismachineMap = allmachineData.get(machineName.toLowerCase());
        //take the state name in a list
        //make all cominations put this in a map with value 1 and -1 depending on if first state is having lesser index than other
        List<String> statetemp = new ArrayList<>();
        statetemp.addAll(thismachineMap.keySet());
        List<String> statelist = new ArrayList<>();
        for (String state : statetemp) {
            int j = statetemp.indexOf(state);
            for (int i = 0; i < statetemp.size(); i++) {
                if (j == i) {
                    continue;
                } else {
                    statelist.add(statetemp.get(j) + "-" + statetemp.get(j));
                }

            }
        }

    }

@Test
    public void createminimal() {
        //create fake entity
        Map<Character, String> edgeStatemapper = new HashMap<>();
        Map<String, String> idStateMap = new HashMap<>();
        List<String> machineids = new ArrayList<>();
        edgeStatemapper.put('N', "normal");
        edgeStatemapper.put('L', "light");
        edgeStatemapper.put('H', "heavy");
        edgeStatemapper.put('E', "extreme");
        List<String> states = new ArrayList<String>() {{
            add("normal");
            add("light");
            add("heavy");
            add("extreme");
        }};
        List<String> machines = new ArrayList<String>() {{
            add("BusinessAsUsual");
            add("SlightlyStrained");
            add("HeavilyStrained");
            add("ExtremelyStrained");
        }};
        String entityType = "zone";
        String machineType = "degradation";
        Boolean createentity = createEntity(new String[]{"fake", "10000", "15", entityType});
        if (createentity) {
            for (String state : states) {
                String statemap = createState(new String[]{entityType, state}, "");
                idStateMap.put(statemap.split("__")[0], statemap.split("__")[1]);
            }

            for (String machine : machines) {
                String id = createMachine(new String[]{entityType, idStateMap.get("normal"), machine, machineType}, "");
                machineids.add(id);
            }

            Map<String, String> edgeMap = zIndexMapper();
            int i = 0;
            for (String id : machineids) {
                for (String edge : edgeMap.keySet()) {
                    Character fromEdge = edge.toCharArray()[0];
                    Character toEdge = edge.toCharArray()[1];
                    JSONObject edgeobject = null;
                    try {
                        edgeobject = new JSONObject();
                        edgeobject.put("fromState", idStateMap.get(edgeStatemapper.get(fromEdge)));
                        edgeobject.put("machineId", id);
                        edgeobject.put("name", edge);
                        edgeobject.put("toState", idStateMap.get(edgeStatemapper.get(toEdge)));
                        edgeobject.put("zIndex", zIndexMapper().get(edge));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    createEdge(new String[]{idStateMap.get(edgeStatemapper.get(fromEdge)), id, edge, idStateMap.get(edgeStatemapper.get(toEdge)), zIndexMapper().get(edge)});
                    //createEdge(edgeobject);
                }

            }
            Map<String, Object> refMap = SystemConfigProvider.getTemplate("degradation").queryForMap("select reference_id from entities order by 1 desc limit 1");
            Integer zone_id = Integer.parseInt(refMap.get("reference_id").toString());
            Map<String, String> paramMap = new HashMap<String, String>();
            //List<String> statelist= new ArrayList<>();
            for (String state : states)
                addParamsToState(paramMap, zone_id, state);
        } else {
            String url = init.EnvironmentDetails.setup.getEnvironmentData().getServices().GetServiceDetails("deliveryservice").getBaseurl();
            addWebhookAndSubscribe(url + "/api/esme/events", "DeliveryService3", "zone", "degradation");
        }
        }

    public void waitInterval(int pollintervalinseconds) {
        try {
            Thread.sleep(pollintervalinseconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

   @Test(enabled = false)
    public void test() {
	   //createminimal();
	 // getBannerFactorRule("normal", "heavy", 4, "slightlystrained"); 
        //putZoneInDegradation(15, "light", "slightlystrained");
        /*String url = init.EnvironmentDetails.setup.getEnvironmentData().getServices().GetServiceDetails("deliveryservice").getBaseurl();
        addWebhookAndSubscribe(url + "/api/esme/events", "DeliveryService3", "zone", "degradation");*/
    }

}

