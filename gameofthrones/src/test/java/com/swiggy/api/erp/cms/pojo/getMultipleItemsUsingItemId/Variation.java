package com.swiggy.api.erp.cms.pojo.getMultipleItemsUsingItemId;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "id",
        "name",
        "price",
        "variantGroupId",
        "order",
        "inStock",
        "isVeg",
        "externalVariantId",
        "uniqueId",
        "default",
        "gst_details",
        "catalog_attributes"
})
public class Variation {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("price")
    private Integer price;
    @JsonProperty("variantGroupId")
    private Integer variantGroupId;
    @JsonProperty("order")
    private Integer order;
    @JsonProperty("inStock")
    private Integer inStock;
    @JsonProperty("isVeg")
    private Integer isVeg;
    @JsonProperty("externalVariantId")
    private String externalVariantId;
    @JsonProperty("uniqueId")
    private String uniqueId;
    @JsonProperty("default")
    private Integer _default;
    @JsonProperty("gst_details")
    private GstDetails gstDetails;
    @JsonProperty("catalog_attributes")
    private CatalogAttributes catalogAttributes;

    /**
     * No args constructor for use in serialization
     *
     */
    public Variation() {
    }

    /**
     *
     * @param id
     * @param gstDetails
     * @param isVeg
     * @param variantGroupId
     * @param price
     * @param order
     * @param _default
     * @param name
     * @param externalVariantId
     * @param catalogAttributes
     * @param inStock
     * @param uniqueId
     */
    public Variation(Integer id, String name, Integer price, Integer variantGroupId, Integer order, Integer inStock, Integer isVeg, String externalVariantId, String uniqueId, Integer _default, GstDetails gstDetails, CatalogAttributes catalogAttributes) {
        super();
        this.id = id;
        this.name = name;
        this.price = price;
        this.variantGroupId = variantGroupId;
        this.order = order;
        this.inStock = inStock;
        this.isVeg = isVeg;
        this.externalVariantId = externalVariantId;
        this.uniqueId = uniqueId;
        this._default = _default;
        this.gstDetails = gstDetails;
        this.catalogAttributes = catalogAttributes;
    }

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("price")
    public Integer getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(Integer price) {
        this.price = price;
    }

    @JsonProperty("variantGroupId")
    public Integer getVariantGroupId() {
        return variantGroupId;
    }

    @JsonProperty("variantGroupId")
    public void setVariantGroupId(Integer variantGroupId) {
        this.variantGroupId = variantGroupId;
    }

    @JsonProperty("order")
    public Integer getOrder() {
        return order;
    }

    @JsonProperty("order")
    public void setOrder(Integer order) {
        this.order = order;
    }

    @JsonProperty("inStock")
    public Integer getInStock() {
        return inStock;
    }

    @JsonProperty("inStock")
    public void setInStock(Integer inStock) {
        this.inStock = inStock;
    }

    @JsonProperty("isVeg")
    public Integer getIsVeg() {
        return isVeg;
    }

    @JsonProperty("isVeg")
    public void setIsVeg(Integer isVeg) {
        this.isVeg = isVeg;
    }

    @JsonProperty("externalVariantId")
    public String getExternalVariantId() {
        return externalVariantId;
    }

    @JsonProperty("externalVariantId")
    public void setExternalVariantId(String externalVariantId) {
        this.externalVariantId = externalVariantId;
    }

    @JsonProperty("uniqueId")
    public String getUniqueId() {
        return uniqueId;
    }

    @JsonProperty("uniqueId")
    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    @JsonProperty("default")
    public Integer getDefault() {
        return _default;
    }

    @JsonProperty("default")
    public void setDefault(Integer _default) {
        this._default = _default;
    }

    @JsonProperty("gst_details")
    public GstDetails getGstDetails() {
        return gstDetails;
    }

    @JsonProperty("gst_details")
    public void setGstDetails(GstDetails gstDetails) {
        this.gstDetails = gstDetails;
    }

    @JsonProperty("catalog_attributes")
    public CatalogAttributes getCatalogAttributes() {
        return catalogAttributes;
    }

    @JsonProperty("catalog_attributes")
    public void setCatalogAttributes(CatalogAttributes catalogAttributes) {
        this.catalogAttributes = catalogAttributes;
    }

}