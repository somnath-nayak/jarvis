package com.swiggy.api.erp.cms.tests.CriticalAPI;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.dp.Critical_API_dataProvider;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class VariantOutOfStock extends Critical_API_dataProvider {
    CMSHelper cms=new CMSHelper();
    @Test(dataProvider = "itemoutofstock", description = "Verify Variant Out Of Stock")
    public void variantOutOfStock(String var_id, String var_gid,String msg) {
        String response=cms.variantOutOfStock(var_id,var_gid).ResponseValidator.GetBodyAsText();
        String statusmsg= JsonPath.read(response,"$.statusMessage").toString().replace("[","").replace("]","");
        String statuscode= JsonPath.read(response,"$.statusCode").toString().replace("[","").replace("]","");
        if(statuscode.equals("0")){
            Assert.assertEquals(statusmsg,msg);
            Assert.assertEquals(Integer.parseInt(statuscode),0);}
        else{
            Assert.assertEquals(statusmsg,msg);
            Assert.assertEquals(Integer.parseInt(statuscode),1);
        }

    }
}
