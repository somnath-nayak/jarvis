package com.swiggy.api.erp.delivery.helper;

import com.rabbitmq.client.AMQP;
import com.swiggy.api.erp.delivery.Pojos.DE;
import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.erp.delivery.constants.RTSConstants;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Daenerys.Initializer;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.RabbitMQHelper;
import framework.gameofthrones.Tyrion.RedisHelper;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.text.StrSubstitutor;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.noggit.JSONUtil;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.rabbitmq.client.AMQP;
import com.swiggy.api.erp.delivery.Pojos.DE;
import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.erp.delivery.constants.RTSConstants;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Daenerys.Initializer;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.RabbitMQHelper;
import framework.gameofthrones.Tyrion.RedisHelper;

/**
 * Created by preetesh.sharma on 01/03/18.
 * Contains methods that can be used for Data generation w r t Delivery and otherwise as well
 * Would also contain generic method to poll db, connect to redis or pushing to RMQ as well
 */
public class DeliveryDataHelper
{
    DeliveryControllerHelper dc=new DeliveryControllerHelper();
    Initialize init;
    RabbitMQHelper rabhelper;
    AutoassignHelper autoassignHelper;
    DeliveryServiceHelper deliveryServiceHelper;
    CandidateHelper cdHelper;
    DeliveryHelperMethods dHelper;
    RedisHelper redisHelper;
    public DeliveryDataHelper()
    {

        init=Initializer.getInitializer();
        rabhelper= new RabbitMQHelper();
        autoassignHelper= new AutoassignHelper();
        deliveryServiceHelper=new DeliveryServiceHelper();
        cdHelper=new CandidateHelper();
        dHelper= new DeliveryHelperMethods();
        redisHelper = new RedisHelper();

    }


    public Integer CheckIfPointIsNotInTags(Double lat,Double lon,String tags)
    {
        String solrUrl = dHelper.getSolrUrl();
        SolrDocumentList solrDocuments = getSolrOutput(solrUrl,"polygon","polygon:\"Intersects("+lat+" "+lon+")\"",100);
        String jsonDoc = JSONUtil.toJSON(solrDocuments);
        JSONArray polygonArray = null;
        String polygon_id = null;
        try
        {
            polygonArray = new JSONArray(JSONUtil.toJSON(solrDocuments));
            System.out.println(polygonArray.toString());
            Integer len = polygonArray.length();
            for(int i = 0; i < len; i++) {
                JSONObject polygonObject = polygonArray.getJSONObject(i);
                String polygonid = polygonObject.getString("id").toString();
                String maxLastMile = null;
                System.out.println(polygonObject.get("tags")+"  Getting Tags");
                System.out.println(polygonObject.get("enabled").toString().contains("true"));
                if (!polygonObject.get("tags").toString().contains(tags)&&polygonObject.get("enabled").toString().contains("true"))
                {
                    return Integer.parseInt(polygonObject.get("id").toString());
                }
            }}
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        return 0;
    }

    public Map<String,String> latLonNotInGivenTagsButInZone(Double lat, Double lon, Double distance,String tags)
    {
        Integer polygonId;
        List<Integer> polygonIdList=new ArrayList<>();
        Map<String,String> zonemap= new HashMap<>();
        Double angle=0.0;
        for(int i = 0; i < 72; i++)
        {

            Double[] latlonarray = getLatLng(lat,lon,angle,distance);
            Integer id = CheckIfPointIsNotInTags(latlonarray[0],latlonarray[1],tags);
            String customerZoneId=getCustomerZoneFromLatLon(latlonarray[0],latlonarray[1]);
            if(id != 0 && customerZoneId!=null)
            {
                zonemap.put("lat",latlonarray[0].toString());
                zonemap.put("lon",latlonarray[1].toString());
                zonemap.put("zone_id",customerZoneId);
                return zonemap;
                /*polygonId = id;
                polygonIdList.add(id);
                System.out.println("found Polygon ID " + id);*/
            }
            angle = angle+5.0;

        }
        return  zonemap;
    }




    ////////get customer lat long which lies in a zone.. check..
    public Map<String,String> getCustomerLatLongLiesInZone(Double lat, Double lon, Double distance)
    {
        // Integer polygonId;
        //List<Integer> polygonIdList=new ArrayList<>();
        Map<String,String> zonemap= new HashMap<>();
        Double angle=0.0;
        for(int i = 0; i < 72; i++)
        {

            Double[] latlonarray = getLatLng(lat,lon,angle,distance);
            //Integer id = CheckIfPointIsNotInTags(latlonarray[0],latlonarray[1],tags);
            String customerZoneId=getCustomerZoneFromLatLon(latlonarray[0],latlonarray[1]);
            if(customerZoneId!=null)
            {
                zonemap.put("lat",latlonarray[0].toString());
                zonemap.put("lon",latlonarray[1].toString());
                zonemap.put("zone_id",customerZoneId);
                return zonemap;
                /*polygonId = id;
                polygonIdList.add(id);
                System.out.println("found Polygon ID " + id);*/
            }
            angle = angle+5.0;
        }
        return  zonemap;
    }

    public String getCustomerZoneFromLatLon(Double lat,Double lon)
    {
        String solrUrl=dHelper.getSolrUrl();
        SolrDocumentList solrDocuments = getSolrOutput(solrUrl,"user_zone","polygon:\"Intersects(" + lat.toString() + "  " + lon.toString() + ")\"");
        String jsonDoc = org.apache.noggit.JSONUtil.toJSON(solrDocuments);
        System.out.println("**** json doc **** " + jsonDoc);
        String customerZoneId=null;
        try{
            customerZoneId =((JSONObject)((new JSONArray(jsonDoc)).get(0))).get("id").toString();
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        return customerZoneId;
    }


    public Double[] getLatLng(double lat, double lng, double angleInDegree, double distance)
    {
        distance = distance / 6371;
        double angleInRadian = toRadian(angleInDegree);

        double lat1 = toRadian(lat);
        double lng1 = toRadian(lng);

        double newLat = Math.asin(Math.sin(lat1) * Math.cos(distance) +
                Math.cos(lat1) * Math.sin(distance) * Math.cos(angleInRadian));
        double newLng = lng1 + Math.atan2(Math.sin(angleInRadian) * Math.sin(distance) * Math.cos(lat1),
                Math.cos(distance) - Math.sin(lat1) * Math.sin(newLat));

        if (Double.isNaN(newLat) || Double.isNaN(newLng)) {
            return null;
        }
        Double[] vNewLatLng = {toDegree(newLat), toDegree(newLng)};
        return vNewLatLng;
    }

    public  double toRadian(double lat_lng) {
        return lat_lng * Math.PI / 180;
    }

    public  double toDegree(double lat_lng) {
        return lat_lng * 180 / Math.PI;
    }

    public static int getDistanceBtwnTwoLatLongs(double originLat, double originLng, double destinationLat, double destinationLng)
    {
        final double R = 6372.8;
        double dLat = Math.toRadians(destinationLat - originLat);
        double dLon = Math.toRadians(destinationLng - originLng);
        double lat1 = Math.toRadians(originLat);
        double lat2 = Math.toRadians(destinationLat);
        double a = Math.pow(Math.sin(dLat / 2), 2) + Math.pow(Math.sin(dLon / 2), 2) * Math.cos(lat1) * Math.cos(lat2);
        double c = 2 * Math.asin(Math.sqrt(a));
        return (int) Math.round(R * c * 1000);
    }




    public  boolean  polldb(String dbname,String Query,String searchforparam, String tosearchvalue, int pollint, int maxpollinsec)
    {
        long current=System.currentTimeMillis();
        long finaltime=current+maxpollinsec*1000;
        Boolean pollflag= false;
        if(pollint<100)
        {
            pollint*=1000;
        }
        int numtimespolled =0;
        do
        {
            try {
                Map<String, Object> testMap = SystemConfigProvider.getTemplate(dbname).queryForMap(Query);
                if(testMap.get(searchforparam).toString().equalsIgnoreCase(tosearchvalue))
                {
                    pollflag=true;
                    break;
                }
                else{
                    numtimespolled++;
                    waitIntervalMili(pollint);
                }
            }
            catch (Exception e)
            {
                System.out.println("In Exception");
                numtimespolled++;
                waitIntervalMili(pollint);
            }
        }while(!(System.currentTimeMillis()>finaltime));
        System.out.println("numtimespolled  "+numtimespolled+"  num seconds we polled for in total " +(System.currentTimeMillis()-current)+ "final flag that went out " +pollflag);
        return pollflag;
    }



    public  boolean  polldbWithRegex(String dbname,String Query,String tosearchparam, String tosearchregex, int pollint, int maxpollinsec)
    {
        long current=System.currentTimeMillis();
        long finaltime=current+maxpollinsec*1000;
        Boolean pollflag= false;
        int numtimespolled =0;
        if(pollint<100)
        {
            pollint*=1000;
        }
        Pattern p = Pattern.compile(tosearchregex);
        Matcher m=null;
        do
        {
            try {
                Map<String, Object> testMap = SystemConfigProvider.getTemplate(dbname).queryForMap(Query);
                m = p.matcher(testMap.get(tosearchparam).toString());
                if(m.find())
                {
                    pollflag=true;
                    break;
                }
                else{
                    numtimespolled++;
                    waitIntervalMili(pollint);
                }
            }
            catch (Exception e)
            {
                System.out.println("In Exception");
                numtimespolled++;
                waitIntervalMili(pollint);
            }
        }while(!(System.currentTimeMillis()>finaltime));

        return pollflag;
    }

    public void waitInterval(int pollintervalinseconds)
    {
        try
        {
            Thread.sleep(pollintervalinseconds*1000);
        }
        catch(InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    public void waitIntervalMili(int pollinterval)
    {
        try
        {
            Thread.sleep(pollinterval);
        }
        catch(InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    public String  getOrderId(String restid, String restlat, String restlon)
    {
        String generatedOrder=null;
        String order_id=null;
        String orderJson=returnOrderJson(restid,restlat,restlon);
        try {
            JSONObject order = new JSONObject(orderJson);
            generatedOrder =order.get("order_id").toString();
        } catch (JSONException je) {
            je.printStackTrace();
        }
        if(pushOrderJsonToRMQAndValidate(orderJson))
        {
            order_id=generatedOrder;
            System.out.println("PRINTING ORDER ID final "+order_id);
        }
        return order_id;
    }

    public String returnOrderJson(Map<String, String> defaulOverrideMap)
    {
        //  StaticData staticData=new StaticData();
        //String filePath=staticData.getPayloadfolder()+"/"+"JSON/"+OrderJsonForDelivery"";
        // String filepath=staticData.getPayloadfolder()+"/"+"JSON/OrderJsonForDelivery";
        //String path=new File("").getAbsolutePath();
        //int endIndex = path.lastIndexOf("/");
        //String newpath = path.substring(0, endIndex);
        String path=new File("").getAbsolutePath();
/*        String path=new File("").getAbsolutePath();
       System.out.println("path" +path);
        int endIndex = path.lastIndexOf("\\");
        String newpath = path.substring(0, endIndex);*/
        //String finalPath=newpath+"..//Data/Payloads/JSON/OrderJsonFordelivery";
        String finalPath="../Data/Payloads/JSON/OrderJsonFordeliverySLD";
        // System.out.println(path);
        File file = new File(finalPath);
        //File file = new File("../Data/Payloads/JSON/OrderJsonForDelivery");
        Map<String,String> defaultMap=setOrderJsonValues();
        if(defaulOverrideMap.size()>0) {
            for (String str : defaulOverrideMap.keySet()) {
                if (defaultMap.containsKey(str))
                    defaultMap.put(str, defaulOverrideMap.get(str));
            }
        }

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String lastorderquery= DeliveryConstant.lastorderId;
        System.out.println(lastorderquery);
        List<Map<String, Object>> order_idlist= SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForList(lastorderquery);
        BigInteger order=(new BigInteger(order_idlist.get(0).get("order_id").toString()).add(new BigInteger(new Integer((int)(Math.random()*10+5)).toString())));
        defaultMap.put("order_id",order.toString());
        String contents=null;
        try {
            contents = FileUtils.readFileToString(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        StrSubstitutor sub = new StrSubstitutor(defaultMap);
        String resolvedString = sub.replace(contents);
        return resolvedString;
    }
    public String returnOrderJsonSLD(Map<String, String> defaulOverrideMap)
    {
        //  StaticData staticData=new StaticData();
        //String filePath=staticData.getPayloadfolder()+"/"+"JSON/"+OrderJsonForDelivery"";
        // String filepath=staticData.getPayloadfolder()+"/"+"JSON/OrderJsonForDelivery";
        //String path=new File("").getAbsolutePath();
        //int endIndex = path.lastIndexOf("/");
        //String newpath = path.substring(0, endIndex);
        String path=new File("").getAbsolutePath();
/*        String path=new File("").getAbsolutePath();
       System.out.println("path" +path);
        int endIndex = path.lastIndexOf("\\");
        String newpath = path.substring(0, endIndex);*/
        //String finalPath=newpath+"..//Data/Payloads/JSON/OrderJsonFordelivery";
        String finalPath="../Data/Payloads/JSON/OrderJsonFordelivery";
        // System.out.println(path);
        File file = new File(finalPath);
        //File file = new File("../Data/Payloads/JSON/OrderJsonForDelivery");
        Map<String,String> defaultMap=setOrderJsonValues();
        if(defaulOverrideMap.size()>0) {
            for (String str : defaulOverrideMap.keySet()) {
                if (defaultMap.containsKey(str))
                    defaultMap.put(str, defaulOverrideMap.get(str));
            }
        }

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        /*String lastorderquery= DeliveryConstant.lastorderId;
        System.out.println(lastorderquery);
        List<Map<String, Object>> order_idlist= SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForList(lastorderquery);
        BigInteger order=(new BigInteger(order_idlist.get(0).get("order_id").toString()).add(new BigInteger(new Integer((int)(Math.random()*10+5)).toString())));*/

        Date date = new Date();
        DateFormat order_idFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        String order_id = order_idFormat.format(date);
        defaultMap.put("order_id",order_id);
        //defaultMap.put("order_id",order.toString());
        String contents=null;
        try {
            contents = FileUtils.readFileToString(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        StrSubstitutor sub = new StrSubstitutor(defaultMap);
        String resolvedString = sub.replace(contents);
        return resolvedString;
    }

    public String  returnOrderJson(String restid, String lat, String lon)
    {
        List<String> orderList=new ArrayList<>();
        String lastorderquery= DeliveryConstant.lastorderId;
        List<Map<String, Object>> order_idlist= SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForList(lastorderquery);
        BigInteger order=(new BigInteger(order_idlist.get(0).get("order_id").toString()).add(new BigInteger(new Integer((int)(Math.random()*10+1)).toString())));
        orderList.add(order.toString());
        System.out.println(order);
        String orderjson="{\n" +
                "\"payment_method\": \"Ok its cash Cash\",\n" +
                "\"GST_on_commission\": {\n" +
                "    \"SGST\": 0.92,\n" +
                "    \"CGST\": 0.92,\n" +
                "    \"IGST\": 0.92\n" +
                "},\n" +
                "\"restaurant_id\": \""+restid+"\",\n" +
                "\"swiggy_discount_hit\": 0.0,\n" +
                "\"customer_ip\": \"10.0.15.223\",\n" +
                "\"billing_lng\": \"77.603578\",\n" +
                "\"payment_txn_id\": \"\",\n" +
                "\"cancellation_fee_collected\": 0.0,\n" +
                "\"post_status\": \"processing\",\n" +
                "\"pg_response_time\": \"2017-12-06 18:43:06\",\n" +
                "\"restaurant_city_name\": \"Bangalore\",\n" +
                "\"coupon_applied\": \"\",\n" +
                "\"pay_by_system_value\": false,\n" +
                "\"with_de\": true,\n" +
                "\"order_notes\": \"\",\n" +
                "\"order_time\": \"2018-02-05 16:19:00\",\n" +
                "\"cust_lat_lng\": {\n" +
                "    \"lat\": \"12.918621\",\n" +
                "    \"lng\": \"77.610493\"\n" +
                "},\n" +
                "\"de_pickedup_refund\": 0,\n" +
                "\"restaurant_coverage_area\": \"jayanagar\",\n" +
                "\"cancellation_order_summary_link\": \"/invoice/download?token=64900184_null\",\n" +
                "\"order_incoming\": \"1\",\n" +
                "\"restaurant_type\": \"F\",\n" +
                "\"commission_on_full_bill\": true,\n" +
                "\"swuid\": \"44accebe-d38e-47bb-8a5e-cd7468af7be6\",\n" +
                "\"restaurant_area_name\": \"koramangala\",\n" +
                "\"payment_confirmation_channel\": \"api\",\n" +
                "\"agreement_type\": \"1\",\n" +
                "\"partner_id\": \"\",\n" +
                "\"free_shipping\": \"0\",\n" +
                "\"restaurant_new_slug\": \"shanmukh-9th-block-jayanagar\",\n" +
                "\"distance_calc_method\": \"GDMA Cache\",\n" +
                "\"customer_id\": \"705262\",\n" +
                "\"delivered_time_in_seconds\": \"0\",\n" +
                "\"restaurant_taxation_type\": \"VAT\",\n" +
                "\"is_assured\": 0,\n" +
                "\"order_id\": "+order.toString()+",\n" +
                "\"has_rating\": \"0\",\n" +
                "\"order_total\": 246,\n" +
                "\"type_of_partner\": 0,\n" +
                "\"order_discount\": 0.0,\n" +
                "\"TCS_on_bill\": {\n" +
                "    \"STCS\": 5.58,\n" +
                "    \"CTCS\": 5.58,\n" +
                "    \"ITCS\": 5.58\n" +
                "},\n" +
                "\"key\": \"OBPH62\",\n" +
                "\"delivery_address\": {\n" +
                "    \"address_line1\": \"Test IBC Knowledge Park Bhavani Nagar Suddagunte Palya Bengaluru Karnataka 560029 India\",\n" +
                "    \"name\": \"Shashank\",\n" +
                "    \"area\": \"Suddagunte Palya\",\n" +
                "    \"mobile\": \"8892028093\",\n" +
                "    \"reverse_geo_code_failed\": false,\n" +
                "    \"email\": \"shashank.shekhar@swiggy.in\",\n" +
                "    \"lat\": \"12.932559\",\n" +
                "    \"flat_no\": \"Test\",\n" +
                "    \"address\": \"Test IBC Knowledge Park Bhavani Nagar Suddagunte Palya Bengaluru Karnataka 560029 India\",\n" +
                "    \"landmark\": \"IBC\",\n" +
                "    \"lng\": \"77.603578\",\n" +
                "    \"id\": \"781738\"\n" +
                "},\n" +
                "\"original_order_total\": 246,\n" +
                "\"is_partner_enable\": false,\n" +
                "\"is_refund_initiated\": 0,\n" +
                "\"post_name\": \"\",\n" +
                "\"is_first_order_delivered\": true,\n" +
                "\"tax_expressions\": {\n" +
                "    \"Service Charges\": \"[CART_SUBTOTAL]*0\",\n" +
                "    \"Service Tax\": \"[CART_SUBTOTAL]*0.058\",\n" +
                "    \"Vat\": \"[CART_SUBTOTAL]*0\",\n" +
                "    \"service_charge_GST_inclusive\": false,\n" +
                "    \"packaging_GST_inclusive\": false,\n" +
                "    \"item_GST_inclusive\": false\n" +
                "},\n" +
                "\"prep_time\": \"22\",\n" +
                "\"is_replicated\": false,\n" +
                "\"restaurant_phone_numbers\": \"08041735566,08041745566\",\n" +
                "\"order_placement_status\": \"0\",\n" +
                "\"restaurant_commission_exp\": \"[bill_without_taxes]*0.131004366812227\",\n" +
                "\"order_spending\": \"0\",\n" +
                "\"cart_id\": 1433,\n" +
                "\"customer_user_agent\": \"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.10 Safari/537.36\",\n" +
                "\"cancellation_fee_collected_total\": 0.0,\n" +
                "\"payment_txn_status\": \"success\",\n" +
                "\"commission\": \"23.03\",\n" +
                "\"rendering_details\": [{\n" +
                "    \"display_text\": \"Bill Total\",\n" +
                "    \"intermediateText\": \"\",\n" +
                "    \"is_negative\": 0,\n" +
                "    \"info_text\": \"\",\n" +
                "    \"hierarchy\": 1,\n" +
                "    \"value\": \"186.21\",\n" +
                "    \"is_collapsible\": 1,\n" +
                "    \"currency\": \"rupees\",\n" +
                "    \"meta\": null,\n" +
                "    \"sub_details\": [{\n" +
                "        \"display_text\": \"Item Total\",\n" +
                "        \"is_negative\": 0,\n" +
                "        \"info_text\": null,\n" +
                "        \"hierarchy\": 1,\n" +
                "        \"value\": \"\",\n" +
                "        \"is_collapsible\": 0,\n" +
                "        \"currency\": \"rupees\",\n" +
                "        \"meta\": null,\n" +
                "        \"key\": \"item_total\",\n" +
                "        \"type\": \"display\",\n" +
                "        \"visibilityExpression\": \"true\"\n" +
                "    }, {\n" +
                "        \"display_text\": \"Packing Charges\",\n" +
                "        \"is_negative\": 0,\n" +
                "        \"info_text\": \"\",\n" +
                "        \"hierarchy\": 1,\n" +
                "        \"value\": \"7.00\",\n" +
                "        \"is_collapsible\": 0,\n" +
                "        \"currency\": \"rupees\",\n" +
                "        \"meta\": null,\n" +
                "        \"key\": \"packing_charges\",\n" +
                "        \"type\": \"display\",\n" +
                "        \"visibilityExpression\": \"true\"\n" +
                "    }, {\n" +
                "        \"display_text\": \"VAT and Service Tax\",\n" +
                "        \"is_negative\": 0,\n" +
                "        \"info_text\": \"\",\n" +
                "        \"hierarchy\": 1,\n" +
                "        \"value\": \"10.21\",\n" +
                "        \"is_collapsible\": 0,\n" +
                "        \"currency\": \"rupees\",\n" +
                "        \"meta\": null,\n" +
                "        \"key\": \"vat_and_service_tax\",\n" +
                "        \"type\": \"display\",\n" +
                "        \"visibilityExpression\": \"true\"\n" +
                "    }, {\n" +
                "        \"display_text\": \"GST\",\n" +
                "        \"is_negative\": 0,\n" +
                "        \"info_text\": null,\n" +
                "        \"hierarchy\": 1,\n" +
                "        \"value\": \"0.00\",\n" +
                "        \"is_collapsible\": 0,\n" +
                "        \"currency\": \"rupees\",\n" +
                "        \"meta\": null,\n" +
                "        \"key\": \"GST\",\n" +
                "        \"type\": \"display\",\n" +
                "        \"visibilityExpression\": \"true\"\n" +
                "    }],\n" +
                "    \"key\": \"bill_total\",\n" +
                "    \"type\": \"display\",\n" +
                "    \"visibilityExpression\": \"true\"\n" +
                "}, {\n" +
                "    \"display_text\": \"Line Separator\",\n" +
                "    \"intermediateText\": \"\",\n" +
                "    \"is_negative\": 0,\n" +
                "    \"info_text\": null,\n" +
                "    \"hierarchy\": 1,\n" +
                "    \"value\": \"\",\n" +
                "    \"is_collapsible\": 0,\n" +
                "    \"currency\": \"rupees\",\n" +
                "    \"meta\": null,\n" +
                "    \"key\": \"line_separator\",\n" +
                "    \"type\": \"separator\",\n" +
                "    \"visibilityExpression\": \"true\"\n" +
                "}, {\n" +
                "    \"display_text\": \"Coupon Discount\",\n" +
                "    \"intermediateText\": \"\",\n" +
                "    \"is_negative\": 1,\n" +
                "    \"info_text\": null,\n" +
                "    \"hierarchy\": 1,\n" +
                "    \"value\": \"Apply\",\n" +
                "    \"is_collapsible\": 0,\n" +
                "    \"currency\": \"rupees\",\n" +
                "    \"meta\": null,\n" +
                "    \"key\": \"apply_coupon\",\n" +
                "    \"type\": \"button\",\n" +
                "    \"visibilityExpression\": \"#is_coupon_valid == 0\"\n" +
                "}, {\n" +
                "    \"display_text\": \"Delivery Charges\",\n" +
                "    \"intermediateText\": \"\",\n" +
                "    \"is_negative\": 0,\n" +
                "    \"info_text\": \"\",\n" +
                "    \"hierarchy\": 1,\n" +
                "    \"value\": \"40.00\",\n" +
                "    \"is_collapsible\": 1,\n" +
                "    \"currency\": \"rupees\",\n" +
                "    \"meta\": null,\n" +
                "    \"key\": \"delivery_charges\",\n" +
                "    \"type\": \"display\",\n" +
                "    \"visibilityExpression\": \"(#delivery_charges > #threshold_fee && #delivery_charges > #distance_fee && #delivery_charges > #time_fee) || #delivery_charges == 0\"\n" +
                "}, {\n" +
                "    \"display_text\": \"Surge Fee\",\n" +
                "    \"intermediateText\": \"\",\n" +
                "    \"is_negative\": 0,\n" +
                "    \"info_text\": \"testing_Due to operational reasons an additional convenience fee is applicable on this order, to be passed on to our delivery executives. Thank you for understanding._Testing\",\n" +
                "    \"hierarchy\": 1,\n" +
                "    \"value\": \"20.00\",\n" +
                "    \"is_collapsible\": 0,\n" +
                "    \"currency\": \"rupees\",\n" +
                "    \"meta\": null,\n" +
                "    \"key\": \"surge_fee\",\n" +
                "    \"type\": \"display\",\n" +
                "    \"visibilityExpression\": \"true\"\n" +
                "}, {\n" +
                "    \"display_text\": \"Instruction Text\",\n" +
                "    \"intermediateText\": \"\",\n" +
                "    \"is_negative\": 0,\n" +
                "    \"info_text\": null,\n" +
                "    \"hierarchy\": 1,\n" +
                "    \"value\": \"\",\n" +
                "    \"is_collapsible\": 0,\n" +
                "    \"currency\": \"rupees\",\n" +
                "    \"meta\": null,\n" +
                "    \"key\": \"instruction_text\",\n" +
                "    \"type\": \"free_text_input\",\n" +
                "    \"visibilityExpression\": \"true\"\n" +
                "}, {\n" +
                "    \"display_text\": \"Line Separator\",\n" +
                "    \"intermediateText\": \"\",\n" +
                "    \"is_negative\": 0,\n" +
                "    \"info_text\": null,\n" +
                "    \"hierarchy\": 1,\n" +
                "    \"value\": \"\",\n" +
                "    \"is_collapsible\": 0,\n" +
                "    \"currency\": \"rupees\",\n" +
                "    \"meta\": null,\n" +
                "    \"key\": \"line_separator\",\n" +
                "    \"type\": \"separator\",\n" +
                "    \"visibilityExpression\": \"true\"\n" +
                "}, {\n" +
                "    \"display_text\": \"To Pay\",\n" +
                "    \"intermediateText\": \"\",\n" +
                "    \"is_negative\": 0,\n" +
                "    \"info_text\": \"\",\n" +
                "    \"hierarchy\": 1,\n" +
                "    \"value\": \"246.00\",\n" +
                "    \"is_collapsible\": 0,\n" +
                "    \"currency\": \"rupees\",\n" +
                "    \"meta\": {\n" +
                "        \"align\": \"right\",\n" +
                "        \"bold\": true\n" +
                "    },\n" +
                "    \"key\": \"grand_total\",\n" +
                "    \"type\": \"display\",\n" +
                "    \"visibilityExpression\": \"true\"\n" +
                "}],\n" +
                "\"order_status\": \"processing\",\n" +
                "\"restaurant_customer_distance\": \"3.6\",\n" +
                "\"billing_lat\": \"12.932559\",\n" +
                "\"coupon_discount\": 0.0,\n" +
                "\"order_payment_method\": \"Cash\",\n" +
                "\"restaurant_address\": \"No 1313, 25th Main Road, Jayanagara 9th Block, Jayanagar\",\n" +
                "\"convenience_fee\": \"20\",\n" +
                "\"order_delivery_charge\": 40.0,\n" +
                "\"listing_version_shown\": \"2-default\",\n" +
                "\"TCS_on_bill_expressions\": {\n" +
                "    \"STCS\": \"0.03\",\n" +
                "    \"CTCS\": \"0.03\",\n" +
                "    \"ITCS\": \"0.03\"\n" +
                "},\n" +
                "\"order_restaurant_bill\": \"610\",\n" +
                "\"restaurant_email\": \"sawan.choubisa@swiggy.in\",\n" +
                "\"sla_time\": \"40\",\n" +
                "\"order_delivery_status\": \"\",\n" +
                "\"converted_to_cod\": false,\n" +
                "\"sla_difference\": \"0\",\n" +
                "\"GST_on_commission_expressions\": {\n" +
                "    \"SGST\": \"0.04\",\n" +
                "    \"CGST\": \"0.04\",\n" +
                "    \"IGST\": \"0.04\"\n" +
                "},\n" +
                "\"delayed_placing\": 1,\n" +
                "\"restaurant_discount_hit\": 0.0,\n" +
                "\"restaurant_has_inventory\": \"0\",\n" +
                "\"restaurant_mobile\": \"\",\n" +
                "\"is_assured_restaurant\": false,\n" +
                "\"sla\": \"40\",\n" +
                "\"last_failed_order_id\": 0,\n" +
                "\"nodal_spending\": \"143.47\",\n" +
                "\"post_type\": \"\",\n" +
                "\"restaurant_name\": \"California Burrito\",\n" +
                "\"delivery_time_in_seconds\": \"0\",\n" +
                "\"restaurant_payment_mode\": {\n" +
                "    \"agreement_type_string\": \"Postpaid\",\n" +
                "    \"agreement_type\": \"1\"\n" +
                "},\n" +
                "\"actual_sla_time\": \"0\",\n" +
                "\"order_type\": \"pop\",\n" +
                "\"on_time\": false,\n" +
                "\"restaurant_lat_lng\": \""+lat+","+lon+"\",\n" +
                "\"order_tax\": 10.208,\n" +
                "\"menu_shown\": {\n" +
                "    \"variant_id\": 1\n" +
                "},\n" +
                "\"sid\": \"4df319d7-d598-4c97-8b37-bd37b4a210d4\",\n" +
                "\"tid\": \"b8c7128e-a5bf-4d06-851c-b3c58ed02828\",\n" +
                "\"billing_address_id\": \"781738\",\n" +
                "\"restaurant_city_code\": \"1\",\n" +
                "\"overbooking\": \"0\",\n" +
                "\"ordered_time_in_seconds\": 1516105969,\n" +
                "\"restaurant_cover_image\": \"exojagrnia7n8s3fryvj\",\n" +
                "\"order_items\": [{\n" +
                "    \"packing_charges\": \"7\",\n" +
                "    \"total\": \"176\",\n" +
                "    \"global_sub_category\": \"sample\",\n" +
                "    \"item_charges\": {\n" +
                "        \"Service Charges\": \"0\",\n" +
                "        \"GST\": \"0\",\n" +
                "        \"Vat\": \"0\",\n" +
                "        \"Service Tax\": \"0\"\n" +
                "    },\n" +
                "    \"item_swiggy_discount_hit\": 0.0,\n" +
                "    \"name\": \"Corn Chilli\",\n" +
                "    \"global_main_category\": \"sample main category\",\n" +
                "    \"category_details\": {\n" +
                "        \"category\": \"Starters\",\n" +
                "        \"sub_category\": \"Chilli & Manchurian\"\n" +
                "    },\n" +
                "    \"is_veg\": \"1\",\n" +
                "    \"image_id\": \"tlcyf50ehkzouv9hj5qj\",\n" +
                "    \"item_tax_expressions\": {\n" +
                "        \"GST_inclusive\": false,\n" +
                "        \"Service Charges\": \"0\",\n" +
                "        \"Vat\": \"0\",\n" +
                "        \"Service Tax\": \"0\"\n" +
                "    },\n" +
                "    \"item_id\": \"79427\",\n" +
                "    \"variants\": [],\n" +
                "    \"addons\": [],\n" +
                "    \"subtotal\": \"169\",\n" +
                "    \"quantity\": \"1\",\n" +
                "    \"item_restaurant_discount_hit\": 0.0\n" +
                "}],\n" +
                "\"free_gifts\": [],\n" +
                "\"is_select\": false,\n" +
                "\"is_ivr_enabled\": \"0\",\n" +
                "\"restaurant_area_code\": \"1\",\n" +
                "\"swiggy_money\": 0.0,\n" +
                "\"payment\": \"successful\",\n" +
                "\"device_id\": \"44accebe-d38e-47bb-8a5e-cd7468af7be6\",\n" +
                "\"restaurant_locality\": \"9th Block\",\n" +
                "\"configurations\": {\n" +
                "    \"self_delivery\": false\n" +
                "},\n" +
                "\"charges\": {\n" +
                "    \"GST\": \"0\",\n" +
                "    \"Delivery Charges\": \"40\",\n" +
                "    \"Service Charges\": \"0\",\n" +
                "    \"Service Tax\": \"10.21\",\n" +
                "    \"Packing Charges\": \"7\",\n" +
                "    \"Convenience Fee\": \"20\",\n" +
                "    \"Cancellation Fee\": \"0\",\n" +
                "    \"Vat\": \"0\"\n" +
                "},\n" +
                "\"cancellation_fee_applied\": 0.0,\n" +
                "\"coupon_code\": \"\",\n" +
                "\"service_tax_on_commission\": \"0\"\n" +
                "}\n";
        return orderjson;
    }
    public boolean pushOrderJsonToRMQAndValidate(String orderjson)
    {
        rabhelper.pushMessageToExchange(DeliveryConstant.rmq_hostname, DeliveryConstant.push_order_queue, new AMQP.BasicProperties().builder().contentType("application/json"), orderjson);
        JSONObject order= null;
        String orderid=null;
        try {
            order = new JSONObject(orderjson);
            orderid=order.get("order_id").toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String orderQuery="select order_id from trips where  order_id="+orderid;
        boolean orderstatusindelivery= polldb(DeliveryConstant.databaseName,orderQuery,"order_id",orderid,2,30);
        return orderstatusindelivery;
    }

    public boolean pushOrderJsonToExchangeAndValidate(String orderjson) {
        rabhelper.pushMessageToExchange(DeliveryConstant.rmq_hostname, DeliveryConstant.push_order_exchange, new AMQP.BasicProperties().builder().contentType("application/json"), orderjson);
        JSONObject order = null;
        String orderid = null;
        try {
            order = new JSONObject(orderjson);
            orderid = order.get("order_id").toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String orderQuery = "select order_id from trips where  order_id=" + orderid;
        boolean orderstatusindelivery = polldb(DeliveryConstant.databaseName, orderQuery, "order_id", orderid, 2, 30);
        return orderstatusindelivery;
    }

    public String CreateDE(int zoneid)
    {
        String areaid=SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForList("select id from area where zone_id="+zoneid).get(0).get("id").toString();
        DE de= new DE(zoneid,Integer.parseInt(areaid));
        de.build();
        ObjectMapper om = new ObjectMapper();
        String json=null;
        try
        {
            json = om.writeValueAsString(de);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.print(json);
        GameOfThronesService service = new GameOfThronesService("deliveryservice", "make-de", init);
        HashMap<String,String> headers= dHelper.doubleheader();
        Processor processor = new Processor(service, headers, new String[] {json});
        String response= processor.ResponseValidator.GetBodyAsText();
        System.out.println(response);
        JSONObject responseJson = null;
        String de_id=null;
        try {
            responseJson = new JSONObject(response);
            de_id=responseJson.get("data").toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update("update delivery_boys set enabled =1 where id= "+de_id);
        return de_id;
    }

    public String CreateDE(int zoneid, int areaid)
    {
        DE de= new DE(zoneid,areaid);
        de.build();
        ObjectMapper om = new ObjectMapper();
        String json=null;
        try
        {
            json = om.writeValueAsString(de);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.print(json);
        GameOfThronesService service = new GameOfThronesService("deliveryservice", "make-de", init);
        HashMap<String,String> headers= dHelper.doubleheader();
        Processor processor = new Processor(service, headers, new String[] {json});
        String response= processor.ResponseValidator.GetBodyAsText();
        System.out.println(response);
        JSONObject responseJson = null;
        String de_id=null;
        try {
            responseJson = new JSONObject(response);
            de_id=responseJson.get("data").toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return de_id;
    }

    public String CreateDE(DE de, int zoneid, int areaid)
    {
        de.setAreacode(areaid);
        de.setZone_id(zoneid);
        de.build();
        ObjectMapper om = new ObjectMapper();
        String json=null;
        try
        {
            json = om.writeValueAsString(de);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.print(json);
        GameOfThronesService service = new GameOfThronesService("deliveryservice", "make-de", init);
        HashMap<String,String> headers= dHelper.doubleheader();
        Processor processor = new Processor(service, headers, new String[] {json});
        String response= processor.ResponseValidator.GetBodyAsText();
        System.out.println(response);
        JSONObject responseJson = null;
        String de_id=null;
        try {
            responseJson = new JSONObject(response);
            de_id=responseJson.get("data").toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return de_id;
    }

    public String getDEAuth(String de_id,String version)
    {
        System.out.println(de_id);
        JSONObject loginresponse=null;
        JSONObject otpresponse=null;
        String otp=null;
        String deauth=null;
        boolean loginresponseresult=false;
        try {
            loginresponse = new JSONObject(autoassignHelper.delogin(de_id,version).ResponseValidator.GetBodyAsText());
            otpresponse = new JSONObject(deliveryServiceHelper.getOtp(de_id.toString()).ResponseValidator.GetBodyAsText());

            System.out.println("loginresponse is "+loginresponse);
            System.out.println("otpresponse is "+otpresponse);
            loginresponseresult=loginresponse.get("statusMessage").toString().trim().equalsIgnoreCase("OTP Sent");
            System.out.println("Login Response is "+loginresponseresult);
            otp=otpresponse.get("data").toString();
            System.out.println("otp is "+otp);
            deauth=autoassignHelper.deotp(de_id,otp).ResponseValidator.GetNodeValue("data.Authorization");
            System.out.println("deauth is " +deauth);
        }
        catch (JSONException je)
        {
            je.printStackTrace();
        }
        return  deauth;
    }


    public String delocationupdate(String lat, String lon, String de_id,String version)
    {
        String auth=getDEAuth(de_id,version);
        Processor processor=null;
        processor=autoassignHelper.locupdate(lat,lon,auth);
        return auth;
    }


    public String getApproxLatLongAtADistance(Double lat, Double lon, Double distance)
    {
        String text = Double.toString(Math.abs(lat));
        int integerPlaces = text.indexOf('.');
        int decimalPlaces = text.length() - integerPlaces - 1;
        Double r_earth=6731.0;
        Double newLat=lat +( distance / r_earth) * (180 / Math.PI);
        Double newLon=lon+(distance/r_earth)*(180/Math.PI)/Math.cos(lat* Math.PI/180);
        BigDecimal bigDecimalLat=new BigDecimal(newLat);
        BigDecimal roundedWithScale1=bigDecimalLat.setScale(decimalPlaces,BigDecimal.ROUND_HALF_EVEN);
        BigDecimal bigDecimalLon=new BigDecimal(newLon);
        BigDecimal roundedWithScale2=bigDecimalLon.setScale(decimalPlaces,BigDecimal.ROUND_HALF_EVEN);
        System.out.println(roundedWithScale1+","+roundedWithScale2);
        return roundedWithScale1+","+roundedWithScale2;
    }

    public int getDistance(double originLat, double originLng, double destinationLat, double destinationLng) {
        double R = 6372.8;
        double dLat = Math.toRadians(destinationLat - originLat);
        double dLon = Math.toRadians(destinationLng - originLng);
        double lat1 = Math.toRadians(originLat);
        double lat2 = Math.toRadians(destinationLat);
        double a = Math.pow(Math.sin(dLat / 2), 2) + Math.pow(Math.sin(dLon / 2), 2) * Math.cos(lat1) * Math.cos(lat2);
        double c = 2 * Math.asin(Math.sqrt(a));
        return (int) Math.round(R * c * 1000);
    }


    public boolean pushOrderToDelivery_Orders(String orderjson)
    {
        rabhelper.pushMessageToExchange(DeliveryConstant.rmq_hostname, DeliveryConstant.push_order_queue_for_ff, new AMQP.BasicProperties().builder().contentType("application/json"), orderjson);
        JSONObject order= null;
        String orderid=null;
        try {
            order = new JSONObject(orderjson);
            orderid=order.get("order_id").toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String orderQuery="select order_id from trips where  order_id="+orderid;
        boolean orderstatusindelivery= polldb(DeliveryConstant.databaseName,orderQuery,"order_id",orderid,2,30);
        String partener_delivery_Query="select order_id from partner_order_mapping where order_id="+orderid;
        boolean orderstatuspartner= polldb(DeliveryConstant.databaseName,orderQuery,"order_id",orderid,2,30);
        return orderstatusindelivery&orderstatuspartner;
    }

    public String  getOrderIdForCAPRDFF(String restid, String restlat, String restlon)
    {
        String generatedOrder=null;
        String order_id=null;
        String orderJson=returnOrderJson(restid,restlat,restlon);
        try {
            JSONObject order = new JSONObject(orderJson);
            generatedOrder =order.get("order_id").toString();
        } catch (JSONException je) {
            je.printStackTrace();
        }
        if(pushOrderToDelivery_Orders(orderJson))
        {
            order_id=generatedOrder;
            System.out.println("PRINTING ORDER ID final "+order_id);
        }
        return order_id;
    }


    public String randomlatlongGenerator(Double lat, Double lon)
    {
        String text = Double.toString(Math.abs(lat));
        int integerPlaces = text.indexOf('.');
        int decimalPlaces = text.length() - integerPlaces - 1;
        Double rand=Math.round(Math.random()*100D)/100D;
        Double newlat= lat+rand/1000;
        System.out.println(newlat);
        Double newlon= lat+rand/1000;
        System.out.println(newlon);
        Double roundvalue=Math.pow(10,decimalPlaces);
        newlat=Math.round(newlat*roundvalue)/roundvalue;
        newlon=Math.round(newlon*roundvalue)/roundvalue;
        return newlat.toString()+','+newlon.toString();

    }
    public String generateDiscardedLocation(Double lat, Double lon,int max)
    {
        int loopcount=(int) (Math.random()*max);
        JSONArray finalArray= new JSONArray();
        for(int i=0;i<loopcount;i++)
        {
            JSONObject test= new JSONObject();
            String latlon[]=randomlatlongGenerator(lat,lon).split(",");
            try {
                test.put("lat",latlon[0]);
                test.put("lng",latlon[0]);
                test.put("timestamp",System.currentTimeMillis());
                test.put("batch_id","");
                finalArray.put(test);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return finalArray.toString();
    }

    public Map<String, String> setOrderJsonValues()
    {
        Map<String, String> orderDefaultValues= new HashMap<>();
        orderDefaultValues.put("restaurant_id","8171");
        orderDefaultValues.put("prep_time","361122");
        orderDefaultValues.put("restaurant_lat_lng","12.915772,77.61001699999997");
        orderDefaultValues.put("restaurant_city_code", "7");
        orderDefaultValues.put("restaurant_customer_distance","3.5");
        orderDefaultValues.put("is_long_distance","false");
        orderDefaultValues.put("restaurant_area_code","4");
        //orderDefaultValues.put("order_time","2018-02-05 16:19:00");
        orderDefaultValues.put("ordered_time_in_seconds","1516105969");
        orderDefaultValues.put("order_time",getcurrentDateTimefororderTime());
        orderDefaultValues.put("order_type","regular");
        orderDefaultValues.put("lat","12.918621");
        orderDefaultValues.put("lng","77.610493");
        orderDefaultValues.put("is_replicated","false");


        return orderDefaultValues;
    }
    public String getcurrentDateTimefororderTime()
    {
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now = new Date();
        String strDate = sdfDate.format(now);
        return strDate;
    }


    public Integer getcurrentDateTimeInMinutes()
    {
        SimpleDateFormat sdfDate = new SimpleDateFormat("HHmm");
        Date now = new Date();
        String strDate = sdfDate.format(now);
        System.out.println(strDate);
        return Integer.parseInt(strDate);
    }


    public String getOrderIdFromOrderJson(String orderJson)
    {
        String generatedOrder=null;
        try {
            JSONObject order = new JSONObject(orderJson);
            generatedOrder =order.get("order_id").toString();
        } catch (JSONException je) {
            je.printStackTrace();
        }
        return generatedOrder;
    }


    public void addDEzonemap(String de_id,int zone)
    {int flag=0;
        String query1= RTSConstants.get_DE_zone_map_query+de_id;
        List<Map<String, Object>> ls=dHelper.dbhelpergetall(query1, DeliveryConstant.databaseName);
        for(Map<String, Object> l:ls)
        {
            String s=l.get("zone_id").toString();
            if(s.equalsIgnoreCase(String.valueOf(zone)))
            {
                flag=1;
            }
        }
        if(flag==0)
        {
            Object id=dHelper.dbhelperget(RTSConstants.getLastidin_De_Zone_Map_query, "id",  DeliveryConstant.databaseName);
            int i=Integer.parseInt(id.toString());
            String query="insert into de_zone_map values ("+(i+1) +","+ de_id + "," + zone + ")";
            dHelper.dbhelperupdate(query, DeliveryConstant.databaseName);
        }
    }

    public void deleteDEzonemap(String de_id,int zone)
    {int flag=0;
        String idd=null;
        String query1= RTSConstants.get_DE_zone_map_query+de_id;
        List<Map<String, Object>> ls=dHelper.dbhelpergetall(query1, DeliveryConstant.databaseName);
        for(Map<String, Object> l:ls)
        {
            String s=l.get("zone_id").toString();
            if(s.equalsIgnoreCase(String.valueOf(zone)))
            {
                flag=1;
                idd=l.get("id").toString();
            }
        }
        if(flag==0)
        {
            String query="delete from de_zone_map values where id="+idd;
            dHelper.dbhelperupdate(query,  DeliveryConstant.databaseName);
        }
    }

    public  String getCentroid(List<String> path) {

        double sumX = 0, sumY = 0, sumZ = 0;
        int counts = 0;
        for (String latLong : path) {
            sumX += (Math.cos(Math.toRadians(Double.parseDouble(latLong.split(",")[0]))) * Math.cos(
                    Math.toRadians(Double.parseDouble(latLong.split(",")[1]))));
            sumY += (Math.cos(Math.toRadians(Double.parseDouble(latLong.split(",")[0])))) * Math.sin(
                    Math.toRadians(Double.parseDouble(latLong.split(",")[1])));
            sumZ += Math.sin(Math.toRadians(Double.parseDouble(latLong.split(",")[0])));
            counts++;
        }
        double avgX = sumX / counts, avgY = sumY / counts, avgZ = sumZ / counts;

        double lng = Math.atan2(avgY, avgX);
        double hyp = Math.sqrt(avgX * avgX + avgY * avgY);
        double lat = Math.atan2(avgZ, hyp);
        lng = lng * (180 / Math.PI);
        lat = lat * (180 / Math.PI);
        lng = (double) Math.round(lng * 10000000d) / 10000000d;
        lat = (double) Math.round(lat * 10000000d) / 10000000d;
        return lat+","+lng;
    }

    /*set rain_mode_type value to the given value for given zone id,
     * rain mode value 1 corresponds to light
     * rain mode value 2 corresponds to heavy
     * rain mode value 3 corresponds to normal
     * rain mode value 4 corresponds to com.swiggy.poc.FSM enabled
     * currently not handling com.swiggy.poc.FSM here
     *Will return a boolean value, validating whether rain_mode_type is modified or not
     */
    public Boolean enableRainModeWithoutFSM(Integer rain_mode_type, Integer zoneid)
    {
        List<Integer> rain_mode_types=new ArrayList<Integer>(){{add(1);add(2);add(3);}};
        if (!rain_mode_types.contains(rain_mode_type))
        {
            return false;
        }
        else
        {
            SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update("update zone set rain_mode_type ="+rain_mode_type+"  where id="+zoneid);
            String updatedRainMode=SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select rain_mode_type from zone where id = "+zoneid).get("rain_mode_type").toString();
            return Integer.parseInt(updatedRainMode)==rain_mode_type;
        }
    }

    //Method with switch the current zone state, will make it open if it is close and close it if it is open, Will return a boolean value as the status of switch
    public boolean zoneOpenCloseSwitch(Integer zoneid)
    {
        List<Map<String, Object>>zoneMap=SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForList("select is_open from zone where id="+zoneid);
        String current_state=null;
        if(zoneMap.size()>0)
        {
            current_state=zoneMap.get(0).get("is_open").toString();
        }
        else
        {
            return false;
        }
        if(current_state=="1"|| current_state=="true")
        {
            SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).execute("update zone set is_open=0 where id="+zoneid);
            String temp_state=SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select is_open from zone where id="+zoneid).get("is_open").toString();
            return !(temp_state==current_state);
        }
        else if (current_state=="0"|| current_state=="false")
        {
            SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).execute("update zone set is_open=1 where id="+zoneid);
            String temp_state=SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select is_open from zone where id="+zoneid).get("is_open").toString();
            return !(temp_state==current_state);
        }
        else
        {
            return false;
        }

    }


    public Processor createPolygon(String polygonTag)
    {
        GameOfThronesService service = new GameOfThronesService("deliveryservice", "createPolygon", init);
        HashMap<String,String> headers= dHelper.doubleheader();
        String name = "polygon_automation_vj";
        String lat_lng = "12.919176658002104 77.64754737567136,12.915746656192992 77.6567741746826,12.905728271581637 77.65985335063169,12.899307091642587 77.65194619845579,12.899286175477677 77.63804162692259,12.909419852505232 77.63012374591062,12.913655168564498 77.63568128298948,12.919176658002104 77.64754737567136";
        String cityId = "1";
        String maxSla = "100";
        String lastMile = "6.0";
        String[] payload = {name, lat_lng, polygonTag, cityId, maxSla, lastMile};
        Processor processor = new Processor(service, headers, payload);
        return processor;
    }


    @Test
    public void loginDEandUpdateLocation(String de_id,String app_version, String restlat, String restlon) {
        autoassignHelper.delogin(de_id, app_version);
        Integer deotp = deliveryServiceHelper.getOtp(de_id, app_version);
        String response = autoassignHelper.deotp(de_id, deotp.toString()).ResponseValidator.GetBodyAsText();
        String de_auth=null;
        try {
            de_auth = ((JSONObject) ((new JSONObject(response)).get("data"))).getString("Authorization");
            System.out.println("de Auth is " + de_auth);
        } catch (Exception e) {
            e.printStackTrace();
        }
        autoassignHelper.locupdate(restlat, restlon, de_auth);

    }


    public Boolean checkAssignmentStatus(Processor processor, String order_id, String de_id) {
        if ((processor.ResponseValidator.GetResponseCode()) == 200) {
            String query = "Select de_id from trips where order_id=" + order_id;
            String deIdAssigned = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap(query).get("de_id").toString();
            return de_id.equalsIgnoreCase(deIdAssigned);
        }
        return false;
    }


    public SolrDocumentList getSolrOutput(String url, String core, String query)
    {
        //SolrClient client = new HttpSolrClient(url);
        SolrClient client = new HttpSolrClient.Builder(url).build();
        SolrDocumentList solrDocuments=null;
        try{
            SolrQuery query1=new SolrQuery(query);
            query1.setParam("wt", "json");
            solrDocuments= (client.query(core, query1).getResults());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return solrDocuments;
    }

    public String getRestLatLongFromSolr(String url, String core, String query ) {
        SolrDocumentList solrDocuments = getSolrOutput( url,  core,  query);
        Iterator<SolrDocument> iterator=solrDocuments.iterator();
        while(iterator.hasNext())
        {
            System.out.println(iterator.next().toString());
        }
        Pattern pattern= Pattern.compile("\\bplace\\W([0-9]*[.])?[0-9]+[,]([0-9]*[.])?[0-9]+");
        Matcher matcher= pattern.matcher(solrDocuments.toString());
        matcher.find();
        String latlon=matcher.group();
        System.out.println(latlon);
        return latlon.split("=")[1];
    }

    //given a zone Id Take active pop polygons from DB
    //take each polygon Id and go to solar if the polygon in enabled in solr, take the path and give the centroid

    public String getPolygonPathFromSolr(SolrDocumentList solrDocuments)
    {
        Pattern pattern= Pattern.compile("latLongs=\\[(.*?)\\]");
        Matcher matcher= pattern.matcher(solrDocuments.toString());
        String polygonPath=null;
        matcher.find();
        try {
            StringBuffer latlonstring = new StringBuffer(matcher.group().split("=")[1]);
            latlonstring.deleteCharAt(0).deleteCharAt(latlonstring.length()-1);
            polygonPath=latlonstring.toString();
        }
        catch (Exception e)
        {
            //e.printStackTrace();
            System.out.println("In Exception");
            return polygonPath;
        }
        return polygonPath;
    }

    public Boolean isPolygonEnabledInSolr(SolrDocumentList solrDocuments)
    {
        Pattern pattern= Pattern.compile("enabled=true");
        Matcher matcher= pattern.matcher(solrDocuments.toString());
        return matcher.find();
    }

    /*Method to find a lat long of a pop polygon in a given zone:
    Input is zone Id,
    The method finds all the pop polygons mapped to a given zone,
    then checks wether the polygon is enabled inpolygon table on by one,
    If the polygon is enabled, It checks the wether a polygon is enabled in solr,
    If the polygon is enabled in solr it will fetch its path and give the centroid of the polygon
    ie a point inside the polygon.
    if any of the condition fails, null is returned.
    */
    public String getPopLatLongFromZone(Integer zone_id)
    {
        String env=init.EnvironmentDetails.setup.getEnvironmentData().getName().toLowerCase();
        String solrUrl=null;
        if(env.equals("stage1"))
        {
            solrUrl="http://solr-u-cuat-01.swiggyops.de:8983/solr/";
        }
        if(env.equals("stage2"))
        {
            solrUrl="http://solr-u-cuat-02.swiggyops.de:8983/solr/";
        }
        if(env.equals("stage3"))
        {
            solrUrl="http://solr-u-cuat-03.swiggyops.de:8983/solr/";
        }
        String popLatlon=null;
        //checkin db that zone has pop polygon select polygon_id from pop_polygon where zone_id=4;
        List<Map<String,Object>> polygonList=SystemConfigProvider.getTemplate("deliverydb").queryForList("select polygon_id from pop_polygon where zone_id="+zone_id);
        if(polygonList.size()>0)
        {
            for(Map<String,Object> pid:polygonList)
            {
                String polygonId=pid.get("polygon_id").toString();
                Map<String,Object> enabledPolygon=SystemConfigProvider.getTemplate("deliverydb").queryForMap("select enabled from polygon where id="+polygonId);

                if(enabledPolygon.size()==1 && enabledPolygon.get("enabled").toString()=="true")
                {
                    SolrDocumentList solrDocuments=getSolrOutput(solrUrl,"polygon","id:"+polygonId);
                    if(isPolygonEnabledInSolr(solrDocuments))
                    {
                        String popPath=getPolygonPathFromSolr(solrDocuments);
                        if(popPath !=null)
                        {
                            List<String> allCoordinates=new ArrayList<>();
                            String[] pathArray=popPath.split(",");
                            for(String str:pathArray)
                            {
                                allCoordinates.add(str.split(" ")[0]+","+str.split(" ")[1]);
                            }
                            popLatlon=getCentroid(allCoordinates);
                            if(popLatlon!=null)
                            {
                                break;
                            }
                        }
                    }
                }
            }
            return popLatlon;
        }
        else
        {
            return null;
        }
    }



    public Boolean doAssignment(String order_id, String de_id)
    {
        //deliveryServiceHelper.makeDEFree(de_id);
        HashMap<String, String> requestheaders = dHelper.doubleheader();
        String batch_id= SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select batch_id from trips where order_id ="+order_id).get("batch_id").toString();
        GameOfThronesService service = new GameOfThronesService("deliveryservice", "assignorder", init);
        String[] payloadparams = new String[] { batch_id, de_id };
        Processor processor = new Processor(service, requestheaders, payloadparams);
        boolean assignStatus=checkAssignmentStatus(processor,order_id,de_id);
        return assignStatus;

    }
    public boolean doCAPRD(String order_id, String de_id ,String from_state,String to_state)
    {
//        if(!doAssignment(order_id,de_id))
//        {
//            return false;
//        }
        if(from_state==null)
            from_state="confirmed";
        if(to_state==null)
            to_state="delivered";
        waitIntervalMili(2000);

        List<String> states=new ArrayList<String>(){{
            add("confirmed");
            add("arrived");
            add("pickedup");
            add("reached");
            add("delivered");
        }};
        /*int startIndex=0;
        int endIndex=0;*/
        boolean changedState=false;
        int startIndex=states.indexOf(from_state.toLowerCase());
        int endIndex=states.indexOf(to_state.toLowerCase());

        /*if(!(from_state==null||from_state=="")) {
            startIndex=states.indexOf(from_state.toLowerCase());
        }
        if(!(to_state==null||to_state==""))
        {
            endIndex=states.indexOf(to_state.toLowerCase());
        }
        else
        {
            endIndex=states.size()-1;
        }*/
        System.out.println("Start Index is "+startIndex);
        System.out.println("End Index is "+endIndex);

        for(int i=startIndex;i<=endIndex;i++)
        {
            String tochangeState=states.get(i);
            changedState=changeStatefromController(tochangeState,order_id);
            if(!changedState)
            {
                break;
            }

        }
        return changedState;
    }

    public boolean changeStatefromController(String to_state, String order_id)
    {
        String regex = "\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}";
        String query = null;
        boolean orderstatus=false;
        switch (to_state)
        {
            case "confirmed":

               /*Boolean conStat=deliveryServiceHelper.zipDialConfirm(order_id);
                    deliveryServiceHelper.zipDialConfirm(order_id);*/
                query = DeliveryConstant.zipconfirmquery + order_id;
                zip confirmzip= new zipConfirm();
                retryZipAPI(10,500,"statusCode","0",confirmzip,order_id);
                orderstatus = polldbWithRegex(DeliveryConstant.databaseName, query, "confirmed_time", regex, 300, 10);
                System.out.println("order satus for confirmed "+ orderstatus);
                break;

            case "arrived":
                /*deliveryServiceHelper.zipDialArrivedDE(order_id);*/
                query = DeliveryConstant.ziparrivequery + order_id;
                retryZipAPI(10,500,"statusCode","0",new zipArrived(),order_id);
                orderstatus = polldbWithRegex(DeliveryConstant.databaseName, query, "arrived_time", regex, 300, 10);
                System.out.println("order satus for arrived "+ orderstatus);
                break;

            case "pickedup":
                // deliveryServiceHelper.zipDialArrivedDE(order_id);
                query = DeliveryConstant.zippickedupquery + order_id;
                retryZipAPI(10,500,"statusCode","0",new zipPickedUp(),order_id);
                orderstatus = polldbWithRegex(DeliveryConstant.databaseName, query, "pickedup_time", regex, 300, 10);
                System.out.println("order satus for pickedup "+ orderstatus);
                break;

            case "reached":
                //deliveryServiceHelper.zipDialReached(order_id);
                query = DeliveryConstant.zipreachedquery + order_id;
                retryZipAPI(10,500,"statusCode","0",new zipReached(),order_id);
                orderstatus = polldbWithRegex(DeliveryConstant.databaseName, query, "reached_time", regex, 300, 10);
                System.out.println("order satus for reached "+ orderstatus);
                break;

            case "delivered":
                //deliveryServiceHelper.zipDialDelivered(order_id);
                query = DeliveryConstant.zipdeliveredquery + order_id;
                retryZipAPI(10,500,"statusCode","0",new zipDelivered(),order_id);
                orderstatus = polldbWithRegex(DeliveryConstant.databaseName, query, "delivered_time", regex, 300, 10);
                System.out.println("order satus for delivered "+ orderstatus);
                break;
        }

        return orderstatus;
    }


    public Integer checkIFPointIsInLDPloygon(Double lat,Double lon)
    {
        String solrUrl = dHelper.getSolrUrl();
        SolrDocumentList solrDocuments = getSolrOutput(solrUrl,"polygon","polygon:\"Intersects("+lat+" "+lon+")\"",100);
        String jsonDoc = JSONUtil.toJSON(solrDocuments);
        System.out.println(jsonDoc);
        JSONArray polygonArray = null;
        String polygon_id = null;

        try
        {
            polygonArray = new JSONArray(JSONUtil.toJSON(solrDocuments));
            System.out.println(polygonArray.toString());
            Integer len = polygonArray.length();
            for(int i = 0; i < len; i++) {
                JSONObject polygonObject = polygonArray.getJSONObject(i);
                String polygonid = polygonObject.getString("id").toString();
                String maxLastMile = null;
                System.out.println(polygonObject.get("tags")+"  Getting Tags");
                System.out.println(polygonObject.get("tags").toString().contains("LONG_DISTANCE"));
                System.out.println(polygonObject.get("enabled").toString().contains("true"));

                if (polygonObject.get("tags").toString().contains("LONG_DISTANCE")&&polygonObject.get("enabled").toString().contains("true"))
                {
                    String polygonQuery = "select polygon_id from long_distance where polygon_id=" + polygonid;
                    List<Map<String,Object>> polygonIdList = SystemConfigProvider.getTemplate(DeliveryConstant.dbname).queryForList(polygonQuery);
                    if(polygonIdList.size() > 0)
                    {
                        return Integer.parseInt(polygonObject.get("id").toString());
                    }


                }
            }}
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        return 0;
    }

    public SolrDocumentList getSolrOutput(String url, String core, String query,Integer numrows)
    {
        //SolrClient client = new HttpSolrClient(url);
        SolrClient client = new HttpSolrClient.Builder(url).build();
        SolrDocumentList solrDocuments=null;
        try{
            SolrQuery query1=new SolrQuery(query);
            query1.setParam("wt", "json");
            query1.setRows(numrows);
            solrDocuments= (client.query(core, query1).getResults());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return solrDocuments;
    }


    @Test
    public void newOrderbuilder()
    {
        //OrderBuilder ob=new OrderBuilder.Builder("10").build();
        //System.out.println(ob.getOrder_id());
        //System.out.println(ob.getRestaurant_id());

        //OrderBuilder ob1=new OrderBuilder.Builder("3").is_batching_enabled("true").
        //      is_long_distance("true").order_type("pop").restaurant_customer_distance("1").build();

        BatchingBuilder batchingBuilder=new BatchingBuilder.Builder("BLR_HSR","6","1").batchingVersion("2").build();
    }



/*
* //write a method to poll db

//retry the maincode sfter every x seconds, if value is found break else,loop again

for (;;)
{
		System.out.println("hello");

}

//write a method to do retries of an api till a certain code in body

//


* */

    public void pollerNew(int pollint, int maxpollinsec,String searchforparam,String valuefound , String query)
    {
        long current=System.currentTimeMillis();
        long finaltime=current+maxpollinsec*1000;
        Boolean pollflag= false;
        if(pollint<100)
        {
            pollint*=1000;
        }
        int numtimespolled =0;
        do
        {
            try {
                Map<String, Object> testMap = SystemConfigProvider.getTemplate("deliverydb").queryForMap(query);
                if(testMap.get(searchforparam).toString().equalsIgnoreCase(valuefound))
                {
                    pollflag=true;
                    break;
                }
                else{
                    numtimespolled++;
                    waitIntervalMili(pollint);
                }
            }
            catch (Exception e)
            {
                System.out.println("In Exception");
                numtimespolled++;
                waitIntervalMili(pollint);
            }
        }while(!(System.currentTimeMillis()>finaltime));
        System.out.println("numtimespolled  "+numtimespolled+"  num seconds we polled for in total " +(System.currentTimeMillis()-current)+ "final flag that went out " +pollflag);
    }

    public Boolean retryZipAPI(int numretries,int pollint,String jsonnode, String expectedvalue, zip invokezipoperation, String order_id)
    {
        Boolean pollflag=false;
        while (numretries>0)
        {
            Processor processor=invokezipoperation.executeZip(order_id);
            System.out.println(processor.ResponseValidator.GetBodyAsText());
            Integer statuscode=processor.ResponseValidator.GetNodeValueAsInt("statusCode");
            System.out.println("num retries PreeteshS"+numretries);
            if(statuscode.toString().equalsIgnoreCase(expectedvalue))
            //if(String.valueOf(processor.ResponseValidator.GetNodeValue(jsonnode)).equalsIgnoreCase(expectedvalue))
            {
                pollflag=true;
                break;
            }
            long curtime=System.currentTimeMillis();
            waitIntervalMili(pollint);
            long fintime=System.currentTimeMillis();
            System.out.println(fintime-curtime +" WAITTIME");

            numretries--;
        }
        System.out.println("pollFlag value "+pollflag);
        return pollflag;

    }

    @Test(enabled = false)
    public void maintest()
    {
        //zip confirmzip= new zipConfirm();
        //retryZipAPI(5,200,"statusCode","2",confirmzip,"20180818115620");

       /* Map<String, String> order_defaultMap= new HashMap<>();
        String orderJson=returnOrderJson(order_defaultMap);
        Boolean orderstatus=pushOrderJsonToRMQAndValidate(orderJson);
        String order_id=getOrderIdFromOrderJson(orderJson);
        boolean status=doCAPRD(order_id,de_id,"confirmed","delivered");/*
       // String query="select order_id from trips where order_id="+order_id;
       // pollerNew(200,30,"order_id",order_id,query);*/

       /*try {
           Map<String, Object> testMap = SystemConfigProvider.getTemplate("deliverydb").queryForMap("select batch_id, order_id from trips where order_id=20180818115601");
           System.out.println(testMap.size());
           System.out.println(StringUtils.join(testMap.keySet(),","));
           System.out.println(StringUtils.join(testMap.values(),","));
       }
       catch (Exception e)
       {
           System.out.println("In Exception");
           waitIntervalMili(100);
       }*/

        //Need this for CAPRD
        String de_id=CreateDE(4);
        System.out.println("Preetesh  "+de_id);
        // getDEAuth(de_id,"1.9");
        loginDEandUpdateLocation(de_id,"1.9","12.982620", "77.685820");
        Map<String, String> order_defaultMap= new HashMap<>();
        String orderJson=returnOrderJson(order_defaultMap);
        Boolean orderstatus=pushOrderJsonToRMQAndValidate(orderJson);
        String order_id=getOrderIdFromOrderJson(orderJson);
        boolean status=doCAPRD(order_id,de_id,"confirmed","delivered");
        // boolean status=doCAPRD("20180818115634","89848","confirmed","delivered");
        System.out.println(status+ "Preetesh");

    }
    @Test(enabled = false)
    public void tablesearch()
    {
        List<Map<String,Object>> alldbMapList= SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForList("show tables");
        List<String> alltableList= new ArrayList<>();
        for(Map<String,Object> tbname:alldbMapList)
        {
            alltableList.add(tbname.get("Tables_in_delivery").toString());
        }
        Map<String, String> alltableData= new HashMap<>();
        for(String str:alltableList)
        {
            String createcofig=SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForList("show create table "+str).get(0).get("Create Table").toString();
            alltableData.put(str,createcofig);
            System.out.println(str+"     "+ createcofig);
            if(createcofig.contains("de_image"))
            {
                System.out.printf("preetesh"+str);
            }
        }

    }

    @Test(enabled = false)
    public void test()
    {
        Object[][] test=DeliveryUtils.getTestsInSheet("goldenset");
        for(int i=0;i<test.length;i++)
        {
            //System.out.println(test.length +" Preetesh");
            LinkedHashMap<String,String> parent = (LinkedHashMap<String, String>) test[i][1];
            System.out.println("Printing each row");
            for(String str1:parent.keySet())
            {
                System.out.println("  "+str1+"   "+parent.get(str1));
            }

        }
    }


    @Test(enabled = false)
    public void setAllRedis()
    {
        RedisHelper redisHelper= new RedisHelper();
        Set<String> allkeys=redisHelper.getAllKeysLike("deliveryredis",1,"BL_BANNER_FACTOR_ZONE*");
        Iterator it =((Set) allkeys).iterator();
        while(it.hasNext())
        {
            String str=it.next().toString();
            System.out.println(str);
            redisHelper.setValue("deliveryredis",1,str,".5");
        }

    }

    /*    @Test
        public void checkRedisKeyExists()
        {
            RedisHelper redisHelper= new RedisHelper();
            System.out.println(redisHelper.checkKeyExists("deliveryredis",1,"BL_BANNER_FACTOR_ZONE_10"));

        }*/
    @Test
    public void setAllRedisZoneBF()
    {
        List<String> allzoneList=SystemConfigProvider.getTemplate("deliverydb").queryForListFromSingleRow("select id from zone where enabled =1","id");
        for(String str:allzoneList)
        {
            RedisHelper redisHelper= new RedisHelper();
            String key="BL_BANNER_FACTOR_ZONE_"+str;
            redisHelper.setValue("deliveryredis",1,key,".5");

        }
    }

    @Test
    public void parseJson() throws JSONException
    {
        //String cart="{\\\"cart\\\":[{\"address_id\\\":1,\"last_mile_distance\\\":0,\\\"distance_method\\\":\\\"HAVERSINE\\\",\\\"sla\\\":11,\\\"sla_range_min\\\":10,\\\"sla_range_max\\\":15,\\\"prep_time_pred\\\":7.69,\\\"first_mile_time_pred\\\":5.61,\\\"last_mile_time_pred\\\":0,\\\"placement_delay_pred\\\":2.95,\\\"assignement_delay_pred\\\":3.84,\\\"serviceable\\\":0,\\\"non_serviceable_reason\\\":12,\\\"batchable\\\":false,\\\"item_count_sla_beef_up\\\":-1.02,\\\"is_long_distance\\\":false,\\\"surge_mode\\\":{\\\"rain_mode\\\":0,\\\"long_distance\\\":0},\\\"banner_factor\\\":2,\\\"start_banner_factor\\\":1.5,\\\"stop_banner_factor\\\":3,\\\"zone_beefup\\\":0,\\\"rain_beefup\\\":0,\\\"dontOverrideSLAComponents\\\":false,\\\"longDistance\\\":false,\\\"lmtime\\\":0,\\\"fmtime\\\":5.61}]}";
        // String cart="{\"cart\\\":[{\\\"address_id\\\":1,\\\"last_mile_distance\\\":0,\\\"distance_method\\\":\\\"HAVERSINE\\\",\\\"sla\\\":11,\\\"sla_range_min\\\":10,\\\"sla_range_max\\\":15,\\\"prep_time_pred\\\":7.69,\\\"first_mile_time_pred\\\":5.61,\\\"last_mile_time_pred\\\":0,\\\"placement_delay_pred\\\":2.95,\\\"assignement_delay_pred\\\":3.84,\\\"serviceable\\\":0,\\\"non_serviceable_reason\\\":12,\\\"batchable\\\":false,\\\"item_count_sla_beef_up\\\":-1.02,\\\"is_long_distance\\\":false,\\\"surge_mode\\\":{\\\"rain_mode\\\":0,\\\"long_distance\\\":0},\\\"banner_factor\\\":2,\\\"start_banner_factor\\\":1.5,\\\"stop_banner_factor\\\":3,\\\"zone_beefup\\\":0,\\\"rain_beefup\\\":0,\"dontOverrideSLAComponents\\\":false,\\\"longDistance\\\":false,\\\"lmtime\\\":0,\\\"fmtime\\\":5.61}]}";
        String cart ="{\n" +
                "    \"cart\": [\n" +
                "        {\n" +
                "            \"address_id\": 1,\n" +
                "            \"restaurant_id\": 6118,\n" +
                "            \"last_mile_distance\": 0,\n" +
                "            \"distance_method\": \"HAVERSINE_LUCIFER\",\n" +
                "            \"sla\": 44,\n" +
                "            \"slaUsingDS\": 44,\n" +
                "            \"sla_range_min\": 40,\n" +
                "            \"sla_range_max\": 45,\n" +
                "            \"prep_time_pred\": 23,\n" +
                "            \"first_mile_time_pred\": 7,\n" +
                "            \"last_mile_time_pred\": 8,\n" +
                "            \"placement_delay_pred\": 3,\n" +
                "            \"assignement_delay_pred\": 3,\n" +
                "            \"serviceable\": 2,\n" +
                "            \"batchable\": false,\n" +
                "            \"item_count_sla_beef_up\": 0,\n" +
                "            \"is_long_distance\": false,\n" +
                "            \"surge_mode\": {\n" +
                "                \"rain_mode\": 2,\n" +
                "                \"long_distance\": 0\n" +
                "            },\n" +
                "            \"banner_factor\": 2,\n" +
                "            \"start_banner_factor\": 1,\n" +
                "            \"stop_banner_factor\": 2,\n" +
                "            \"demand_shaping_factor\": 1.1,\n" +
                "            \"zone_beefup\": 0,\n" +
                "            \"rain_beefup\": 0,\n" +
                "            \"bannerFactorDiff\": 0,\n" +
                "            \"degradation\": {\n" +
                "                \"mode\": 1,\n" +
                "                \"supply_state\": 0,\n" +
                "                \"cause\": \"rain\"\n" +
                "            },\n" +
                "            \"thirty_mins_or_free\": false,\n" +
                "            \"dontOverrideSLAComponents\": false,\n" +
                "            \"longDistance\": false,\n" +
                "            \"lmtime\": 8,\n" +
                "            \"fmtime\": 7\n" +
                "        }\n" +
                "    ]\n" +
                "}";

        JSONObject obj=new JSONObject(cart);
        JSONObject mappedResponse=((JSONArray)obj.getJSONArray("cart")).getJSONObject(0);
        Map<String,String> responseMap=new HashMap<>();
        Iterator<?> it = mappedResponse.keys();
        JSONObject mappedResponseSurge=null;
        JSONObject mappedResponseDegradation=null;
        while (it.hasNext())
        {
            String key = (String)it.next();
            responseMap.put(key,mappedResponse.getString(key));
        }

        if(mappedResponse.has("surge_mode"))//&&(mappedResponse.getJSONObject("surge_mode")!=null || mappedResponse.getJSONObject("surge_mode").toString()!=""))
        //if(resObject.has("surge_mode"))
        {
            mappedResponseSurge = ((JSONObject)(mappedResponse.get("surge_mode")));
            Iterator<?> its = mappedResponseSurge.keys();
            while (its.hasNext())
            {
                String key = (String)its.next();
                responseMap.put(key,mappedResponseSurge.getString(key));
            }
        }

        if(mappedResponse.has("degradation"))//&&(mappedResponse.getJSONObject("degradation")!=null || mappedResponse.getJSONObject("degradation").toString()!=""))
        {
            mappedResponseDegradation = ((JSONObject)(mappedResponse.get("degradation")));
            Iterator<?> itd = mappedResponseDegradation.keys();
            while (itd.hasNext())
            {
                String key = (String)itd.next();
                responseMap.put(key,mappedResponseDegradation.getString(key));
            }
        }

        for(String str:responseMap.keySet())
        {
            System.out.println(str+"   "+ responseMap.get(str));
        }
    }

    public void updateDELocationInRedis(String de_id){
        RedisHelper redisHelper= new RedisHelper();
        String toset="{\"trackable\":\"1\",\"lng\":\"77.60467362\",\"image_url\":\"https://de-docs.s3.amazonaws.com/photographs/216.vnr\",\"name\":\"Karunakar\",\"mobile\":\"7877777777\",\"last_seen_at\":\"2018-07-26 00:16:51\",\"lat\":\"12.93384525\"}";
        String key="DE_Location_"+de_id;
        redisHelper.setValueJson("deliveryredis", 1, key, toset);
        String key2="DLS:DE_CURRENT_LOC_"+de_id;
        redisHelper.setValueJson("deliveryredis", 1, key2, toset);

    }

    public void DeleteDE(String de_id, String area_id) {
        SystemConfigProvider.getTemplate("deliverydb").update("delete from de_documents where de_id ="+ de_id);
        SystemConfigProvider.getTemplate("deliverydb").update("delete from de_info where de_id ="+ de_id);
        try {
            SystemConfigProvider.getTemplate("deliverydb").update("delete from de_service_map where de_id ="+ de_id);
            SystemConfigProvider.getTemplate("deliverydb").update("delete from de_cash_sessions where de_id ="+ de_id);
            SystemConfigProvider.getTemplate("deliverydb").update("delete from de_cash_session_audit where de_id ="+ de_id);
            SystemConfigProvider.getTemplate("deliverydb").update("delete from de_cash_order_txn_session where de_id ="+ de_id);

            String referral_id = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForList("select id from `ref_applicant_info` where de_id ="+ de_id).get(0).get("id").toString();
            SystemConfigProvider.getTemplate("deliverydb").update("delete from sno_ref_mapping where referral_id ="+ referral_id);
            SystemConfigProvider.getTemplate("deliverydb").update("delete from ref_applicant_info where de_id ="+ de_id);
        }catch (Exception e){ e.getMessage();}
        SystemConfigProvider.getTemplate("deliverydb").update("delete from delivery_boys where id ="+de_id);
        redisHelper.deleteKey("deliveryredis",1,"DE_Location_"+de_id);
        redisHelper.deleteKey("deliveryredis",1,"DE_BOY_"+de_id);
        redisHelper.deleteKey("deliveryredis",1,"DE_AREA_CODES_"+de_id);
        redisHelper.deleteKey("deliveryredis",1,"DE_AREA_NAMES_"+de_id);
        redisHelper.deleteKey("deliveryredis",1,"DE_DAILY_"+area_id+"_"+de_id);
    }

    public void updateDELocationInRedis(String de_id,String lat,String lng){
        RedisHelper redisHelper= new RedisHelper();
        String toset="{\"trackable\":\"1\",\"lng\":\""+lng+"\",\"image_url\":\"https://de-docs.s3.amazonaws.com/photographs/216.vnr\",\"name\":\"Karunakar\",\"mobile\":\"7877777777\",\"last_seen_at\":\"2018-07-26 00:16:51\",\"lat\":\""+lat+"\"}";
        System.out.println("**********************************************"+ toset);
        String key="DE_Location_"+de_id;
        redisHelper.setValueJson("deliveryredis", 1, key, toset);
    }

}