
package com.swiggy.api.erp.cms.pojo.ItemSlotCreation;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "close_time",
    "created_by",
    "day_of_week",
    "item_id",
    "open_time"
})
public class Data {

    @JsonProperty("close_time")
    private Integer close_time;
    @JsonProperty("created_by")
    private Created_by created_by;
    @JsonProperty("day_of_week")
    private Integer day_of_week;
    @JsonProperty("item_id")
    private Integer item_id;
    @JsonProperty("open_time")
    private Integer open_time;


    @JsonProperty("close_time")
    public Integer getClose_time() {
        return close_time;
    }

    @JsonProperty("close_time")
    public void setClose_time(Integer close_time) {
        this.close_time = close_time;
    }

    @JsonProperty("created_by")
    public Created_by getCreated_by() {
        return created_by;
    }

    @JsonProperty("created_by")
    public void setCreated_by(Created_by created_by) {
        this.created_by = created_by;
    }

    @JsonProperty("day_of_week")
    public Integer getDay_of_week() {
        return day_of_week;
    }

    @JsonProperty("day_of_week")
    public void setDay_of_week(Integer day_of_week) {
        this.day_of_week = day_of_week;
    }

    @JsonProperty("item_id")
    public Integer getItem_id() {
        return item_id;
    }

    @JsonProperty("item_id")
    public void setItem_id(Integer item_id) {
        this.item_id = item_id;
    }

    @JsonProperty("open_time")
    public Integer getOpen_time() {
        return open_time;
    }

    @JsonProperty("open_time")
    public void setOpen_time(Integer open_time) {
        this.open_time = open_time;
    }

    public Data build(int itemId) {
        setDefaultValues(itemId);
        return this;
    }

    public void setDefaultValues(int itemId) {
        Created_by created_by=new Created_by();
        created_by.build();


        if(this.close_time==null){
            this.close_time =2300;
        }
        if(this.day_of_week==null){
            this.day_of_week=1;
        }
        if(this.item_id == null){
            this.item_id = itemId;
        }
        if(this.open_time==null){
            this.open_time=1000;
        }

        this.setCreated_by(created_by);

    }

}
