
package com.swiggy.api.erp.cms.pojo.Taxonomy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "a",
    "c"
})
public class Meta {

    @JsonProperty("a")
    private String a;
    @JsonProperty("c")
    private String c;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Meta() {
        setDefaultData();
    }

    /**
     * 
     * @param c
     * @param a
     */
    public Meta(String a, String c) {
        super();
        this.a = a;
        this.c = c;
    }

    @JsonProperty("a")
    public String getA() {
        return a;
    }

    @JsonProperty("a")
    public void setA(String a) {
        this.a = a;
    }

    public Meta withA(String a) {
        this.a = a;
        return this;
    }

    @JsonProperty("c")
    public String getC() {
        return c;
    }

    @JsonProperty("c")
    public void setC(String c) {
        this.c = c;
    }

    public Meta withC(String c) {
        this.c = c;
        return this;
    }

    public Meta setDefaultData()
    {
        this.withA("b").withC("d");
        return this;
    }

}
