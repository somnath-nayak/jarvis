package com.swiggy.api.erp.cms.pojo.BaseServiceBulkVariant;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.erp.cms.pojo.BaseServiceBulkVariant
 **/

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "delete_by_ids",
        "delete_by_third_party_ids",
        "update_items",
        "variants"
})
public class BulkVariants {

    @JsonProperty("delete_by_ids")
    private List<Integer> delete_by_ids = null;
    @JsonProperty("delete_by_third_party_ids")
    private List<String> delete_by_third_party_ids = null;
    @JsonProperty("update_items")
    private Boolean update_items;
    @JsonProperty("variants")
    private List<Variant> variants = null;

    @JsonProperty("delete_by_ids")
    public List<Integer> getDelete_by_ids() {
        return delete_by_ids;
    }

    @JsonProperty("delete_by_ids")
    public void setDelete_by_ids(List<Integer> delete_by_ids) {
        this.delete_by_ids = delete_by_ids;
    }

    @JsonProperty("delete_by_third_party_ids")
    public List<String> getDelete_by_third_party_ids() {
        return delete_by_third_party_ids;
    }

    @JsonProperty("delete_by_third_party_ids")
    public void setDelete_by_third_party_ids(List<String> delete_by_third_party_ids) {
        this.delete_by_third_party_ids = delete_by_third_party_ids;
    }

    @JsonProperty("update_items")
    public Boolean getUpdate_items() {
        return update_items;
    }

    @JsonProperty("update_items")
    public void setUpdate_items(Boolean update_items) {
        this.update_items = update_items;
    }

    @JsonProperty("variants")
    public List<Variant> getVariants() {
        return variants;
    }

    @JsonProperty("variants")
    public void setVariants(List<Variant> variants) {
        this.variants = variants;
    }

    public void setDefaultValues() {
        List<Variant> list = new ArrayList<>();
        if(this.getUpdate_items() == null)
            this.setUpdate_items(true);
        if(this.getVariants() == null)
            this.setVariants(list);
        if(this.getDelete_by_ids() == null)
            this.setDelete_by_ids(new ArrayList<>());
        if(this.getDelete_by_third_party_ids() == null)
            this.setDelete_by_third_party_ids(new ArrayList<>());
    }

    public BulkVariants build() {
        setDefaultValues();
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("delete_by_ids", delete_by_ids).append("delete_by_third_party_ids", delete_by_third_party_ids).append("update_items", update_items).append("variants", variants).toString();
    }

}

