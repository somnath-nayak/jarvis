package com.swiggy.api.erp.cms.dp;

import com.swiggy.api.daily.cms.helper.CMSDailyContants;
import com.swiggy.api.erp.cms.constants.CmsConstants;
import com.swiggy.api.erp.cms.helper.CatalogV2Helper;
import com.swiggy.api.erp.cms.pojo.Inventory.Inventory;
import com.swiggy.api.erp.cms.pojo.PricingService.Pricing;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.Reporter;
import org.testng.annotations.DataProvider;

public class CatalogV2PricingInventoryDP {
    CatalogV2Helper helper=new CatalogV2Helper();


    @DataProvider(name = "createpricingdp")
    public Object[][] pricing(){

        return new Object[][]{
                {"100-101",100,101},
                {"100-102",12,13},
                {"100-102",112,100},
        };
    }

    @DataProvider(name = "createinventorydp")
    public Object[][] inventory(){
        return new Object[][]{
                {"100-101",100},
                {"100-102",12},
                {"100-103",112},
        };
    }

    @DataProvider(name = "clearinventorynegativedp")
    public Object[][] inventory1(){
        return new Object[][]{
                {"998828282-101"},
                {"2878282-102"},
                {"286227722-103"},
        };
    }


    @DataProvider(name = "getpricingdpnegative")
    public Object[][] prcingNegative(){
        return new Object[][]{{"-1"},{"1"},{"100-11111"},{" "},{"Something"}};
    }

    @DataProvider(name = "getinventorydpnegative")
    public Object[][] inventoryNegative(){
        return new Object[][]{{"-1"},{"1"},{"99999-99999"},{" "},{"Something"}};
    }
}
