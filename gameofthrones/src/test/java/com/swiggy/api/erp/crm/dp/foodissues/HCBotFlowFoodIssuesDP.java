package com.swiggy.api.erp.crm.dp.foodissues;

import com.swiggy.api.erp.ff.helper.LOSHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import com.swiggy.api.erp.crm.constants.CRMConstants;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

public class HCBotFlowFoodIssuesDP {

    static Initialize gameofthrones = new Initialize();
    static LOSHelper losHelper = new LOSHelper();
    public static String orderId = null;


    public static HashMap<String, String> order_details(String orderId) throws Exception {

        String[] orderArray = new String[]{orderId};
        String[] queryparam = orderArray;

        HashMap<String, String> requestheaders_getOrder = new HashMap<String, String>();
        requestheaders_getOrder.put("Content-Type", "application/json");
        GameOfThronesService getOrder = new GameOfThronesService("oms", "getOrder", gameofthrones);
        Processor getOrder_response = new Processor(getOrder, requestheaders_getOrder, null, queryparam);

        Assert.assertEquals(getOrder_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("objects..order_id"), "[\"" + orderId + "\"]");

        String order_id = getOrder_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("objects..order_id");
        System.out.println("order_id   .. " + order_id);

        String order_status = getOrder_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("objects..order_status");
        System.out.println("order_status    .. " + order_status);

        String restaurant_id = getOrder_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("objects..restaurant_details.restaurant_id").toString().replace("[", "").replace("]", "");
        String restaurant_name = getOrder_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("objects..restaurant_details.name").toString().replace("[", "").replace("]", "");
        String item_id = getOrder_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("objects..items..id").toString().replace("[", "").replace("]", "");
        String item_name = getOrder_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("objects..items..name").toString().replace("[", "").replace("]", "");
        String is_veg = getOrder_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("objects..items..is_veg").toString().replace("[", "").replace("]", "");
        Boolean isVeg = Boolean.parseBoolean(is_veg);

        String quantity = getOrder_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("objects..items..quantity").toString().replace("[", "").replace("]", "");
        String cost = getOrder_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("objects..items..sub_total").toString().replace("[", "").replace("]", "");

        //String addons_id = getOrder_response.ResponseValidator.GetNodeValue("objects..items..addons..choice_id");
        //String addons_name = getOrder_response.ResponseValidator.GetNodeValue("objects..items..addons..name");
        //String addons_groupId = getOrder_response.ResponseValidator.GetNodeValue("objects..items..addons..group_id");
        //int addons_cost = getOrder_response.ResponseValidator.GetNodeValueAsInt("objects..items..addons..price");

        //String variants_id = getOrder_response.ResponseValidator.GetNodeValue("objects..items..variants..variation_id");
        //String variants_name = getOrder_response.ResponseValidator.GetNodeValue("objects..items..variants..name");
        //String variants_groupId = getOrder_response.ResponseValidator.GetNodeValue("objects..items..variants..group_id");
        //int variants_cost = getOrder_response.ResponseValidator.GetNodeValueAsInt("objects..items..variants..price");

        if(is_veg=="0")
        {
            is_veg="false";
        }
        else
        {
            is_veg="true";
        }


        HashMap<String, String> order_details = new HashMap<String, String>();
        order_details.put("restaurant_id", restaurant_id);
        order_details.put("restaurant_name", restaurant_name);
        order_details.put("item_id", item_id);
        order_details.put("is_veg", is_veg);
        order_details.put("quantity", quantity);
        order_details.put("cost", cost);
        order_details.put("item_name", item_name);
        order_details.put("orderId", order_id);


        return order_details;

    }

    @DataProvider(name = "orderDetailsManual")
    public static Object[][] orderDetailsManual() throws Exception {

        String orderType = "manual";
        orderId = CRMConstants.orderManualRecommendation;

        //Generate a conversation id
        String conversationId = UUID.randomUUID().toString();

        System.out.println("order id for "+orderType+" " + orderId);
        return new Object[][]{
                {"foodIssues1",order_details(orderId),conversationId},
                {"foodIssues2",order_details(orderId),conversationId},
                {"foodIssues3",order_details(orderId),conversationId},
                {"foodIssues4",order_details(orderId),conversationId},
                {"foodIssues5",order_details(orderId),conversationId},
                {"foodIssues6",order_details(orderId),conversationId},
                {"foodIssues7",order_details(orderId),conversationId},
                {"foodIssues8",order_details(orderId),conversationId},
                {"foodIssues9",order_details(orderId),conversationId},
                {"foodIssues10",order_details(orderId),conversationId},
                {"foodIssues11",order_details(orderId),conversationId},
                {"foodIssues12",order_details(orderId),conversationId},
                {"foodIssues13",order_details(orderId),conversationId},
                {"foodIssues14",order_details(orderId),conversationId},
                {"foodIssues15",order_details(orderId),conversationId},
                {"foodIssues16",order_details(orderId),conversationId},
                {"foodIssues17",order_details(orderId),conversationId},
                {"foodIssues18",order_details(orderId),conversationId}
        };
    }
}
