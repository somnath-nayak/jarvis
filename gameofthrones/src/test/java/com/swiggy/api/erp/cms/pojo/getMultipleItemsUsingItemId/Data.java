package com.swiggy.api.erp.cms.pojo.getMultipleItemsUsingItemId;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.math.BigInteger;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "category_id",
        "packing_charges",
        "service_charges",
        "type",
        "crop_choices",
        "description",
        "third_party_sub_category_name",
        "id",
        "event_timestamp",
        "in_stock",
        "sub_category_desc",
        "addons",
        "serves_how_many",
        "sub_category",
        "service_tax",
        "category_order",
        "is_spicy",
        "gst_details",
        "vat",
        "menu_id",
        "enabled",
        "third_party_id",
        "category_desc",
        "s3_image_url",
        "image_id",
        "sub_category_order",
        "version",
        "is_veg",
        "order",
        "recommended",
        "variants",
        "addon_free_limit",
        "addon_limit",
        "is_perishable",
        "restaurant_id",
        "sub_category_id",
        "eligible_for_long_distance",
        "catalog_attributes",
        "third_party_category_name",
        "packing_slab_count",
        "category",
        "price",
        "preparation_style",
        "is_discoverable",
        "name",
        "timestamp",
        "available"
})
public class Data {

    @JsonProperty("category_id")
    private Integer categoryId;
    @JsonProperty("packing_charges")
    private Integer packingCharges;
    @JsonProperty("service_charges")
    private String serviceCharges;
    @JsonProperty("type")
    private String type;
    @JsonProperty("crop_choices")
    private Integer cropChoices;
    @JsonProperty("description")
    private String description;
    @JsonProperty("third_party_sub_category_name")
    private String thirdPartySubCategoryName;
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("event_timestamp")
    private BigInteger eventTimestamp;
    @JsonProperty("in_stock")
    private Integer inStock;
    @JsonProperty("sub_category_desc")
    private String subCategoryDesc;
    @JsonProperty("addons")
    private Map<String, Addon> addonMap;
    @JsonProperty("serves_how_many")
    private Integer servesHowMany;
    @JsonProperty("sub_category")
    private String subCategory;
    @JsonProperty("service_tax")
    private String serviceTax;
    @JsonProperty("category_order")
    private Integer categoryOrder;
    @JsonProperty("is_spicy")
    private Integer isSpicy;
    @JsonProperty("gst_details")
    private GstDetails gstDetails;
    @JsonProperty("vat")
    private String vat;
    @JsonProperty("menu_id")
    private Integer menuId;
    @JsonProperty("enabled")
    private Integer enabled;
    @JsonProperty("third_party_id")
    private String thirdPartyId;
    @JsonProperty("category_desc")
    private String categoryDesc;
    @JsonProperty("s3_image_url")
    private String s3ImageUrl;
    @JsonProperty("image_id")
    private String imageId;
    @JsonProperty("sub_category_order")
    private Integer subCategoryOrder;
    @JsonProperty("version")
    private Integer version;
    @JsonProperty("is_veg")
    private Integer isVeg;
    @JsonProperty("order")
    private Integer order;
    @JsonProperty("recommended")
    private Boolean recommended;
    @JsonProperty("variants")
    private Variants variants;
    @JsonProperty("addon_free_limit")
    private Integer addonFreeLimit;
    @JsonProperty("addon_limit")
    private Integer addonLimit;
    @JsonProperty("is_perishable")
    private Integer isPerishable;
    @JsonProperty("restaurant_id")
    private Integer restaurantId;
    @JsonProperty("sub_category_id")
    private Integer subCategoryId;
    @JsonProperty("eligible_for_long_distance")
    private Integer eligibleForLongDistance;
    @JsonProperty("catalog_attributes")
    private CatalogAttributes catalogAttributes;
    @JsonProperty("third_party_category_name")
    private String thirdPartyCategoryName;
    @JsonProperty("packing_slab_count")
    private Integer packingSlabCount;
    @JsonProperty("category")
    private String category;
    @JsonProperty("price")
    private Integer price;
    @JsonProperty("preparation_style")
    private String preparationStyle;
    @JsonProperty("is_discoverable")
    private Boolean isDiscoverable;
    @JsonProperty("name")
    private String name;
    @JsonProperty("timestamp")
    private BigInteger timestamp;
    @JsonProperty("available")
    private Boolean available;

    /**
     * No args constructor for use in serialization
     *
     */
    public Data() {
    }

    /**
     *
     * @param serviceTax
     * @param subCategoryOrder
     * @param vat
     * @param recommended
     * @param servesHowMany
     * @param subCategoryDesc
     * @param type
     * @param preparationStyle
     * @param version
     * @param timestamp
     * @param id
     * @param isVeg
     * @param imageId
     * @param order
     * @param description
     * @param name
     * @param isPerishable
     * @param variants
     * @param thirdPartyId
     * @param restaurantId
     * @param s3ImageUrl
     * @param cropChoices
     * @param serviceCharges
     * @param enabled
     * @param eventTimestamp
     * @param subCategory
     * @param isSpicy
     * @param addonMap
     * @param categoryId
     * @param thirdPartyCategoryName
     * @param thirdPartySubCategoryName
     * @param menuId
     * @param catalogAttributes
     * @param available
     * @param categoryDesc
     * @param isDiscoverable
     * @param subCategoryId
     * @param packingCharges
     * @param gstDetails
     * @param category
     * @param price
     * @param categoryOrder
     * @param eligibleForLongDistance
     * @param addonFreeLimit
     * @param addonLimit
     * @param inStock
     * @param packingSlabCount
     */
    public Data(Integer categoryId, Integer packingCharges, String serviceCharges, String type, Integer cropChoices, String description, String thirdPartySubCategoryName, Integer id, BigInteger eventTimestamp, Integer inStock, String subCategoryDesc, Map<String, Addon> addonMap, Integer servesHowMany, String subCategory, String serviceTax, Integer categoryOrder, Integer isSpicy, GstDetails gstDetails, String vat, Integer menuId, Integer enabled, String thirdPartyId, String categoryDesc, String s3ImageUrl, String imageId, Integer subCategoryOrder, Integer version, Integer isVeg, Integer order, Boolean recommended, Variants variants, Integer addonFreeLimit, Integer addonLimit, Integer isPerishable, Integer restaurantId, Integer subCategoryId, Integer eligibleForLongDistance, CatalogAttributes catalogAttributes, String thirdPartyCategoryName, Integer packingSlabCount, String category, Integer price, String preparationStyle, Boolean isDiscoverable, String name, BigInteger timestamp, Boolean available) {
        super();
        this.categoryId = categoryId;
        this.packingCharges = packingCharges;
        this.serviceCharges = serviceCharges;
        this.type = type;
        this.cropChoices = cropChoices;
        this.description = description;
        this.thirdPartySubCategoryName = thirdPartySubCategoryName;
        this.id = id;
        this.eventTimestamp = eventTimestamp;
        this.inStock = inStock;
        this.subCategoryDesc = subCategoryDesc;
        this.addonMap = addonMap;
        this.servesHowMany = servesHowMany;
        this.subCategory = subCategory;
        this.serviceTax = serviceTax;
        this.categoryOrder = categoryOrder;
        this.isSpicy = isSpicy;
        this.gstDetails = gstDetails;
        this.vat = vat;
        this.menuId = menuId;
        this.enabled = enabled;
        this.thirdPartyId = thirdPartyId;
        this.categoryDesc = categoryDesc;
        this.s3ImageUrl = s3ImageUrl;
        this.imageId = imageId;
        this.subCategoryOrder = subCategoryOrder;
        this.version = version;
        this.isVeg = isVeg;
        this.order = order;
        this.recommended = recommended;
        this.variants = variants;
        this.addonFreeLimit = addonFreeLimit;
        this.addonLimit = addonLimit;
        this.isPerishable = isPerishable;
        this.restaurantId = restaurantId;
        this.subCategoryId = subCategoryId;
        this.eligibleForLongDistance = eligibleForLongDistance;
        this.catalogAttributes = catalogAttributes;
        this.thirdPartyCategoryName = thirdPartyCategoryName;
        this.packingSlabCount = packingSlabCount;
        this.category = category;
        this.price = price;
        this.preparationStyle = preparationStyle;
        this.isDiscoverable = isDiscoverable;
        this.name = name;
        this.timestamp = timestamp;
        this.available = available;
    }

    @JsonProperty("category_id")
    public Integer getCategoryId() {
        return categoryId;
    }

    @JsonProperty("category_id")
    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    @JsonProperty("packing_charges")
    public Integer getPackingCharges() {
        return packingCharges;
    }

    @JsonProperty("packing_charges")
    public void setPackingCharges(Integer packingCharges) {
        this.packingCharges = packingCharges;
    }

    @JsonProperty("service_charges")
    public String getServiceCharges() {
        return serviceCharges;
    }

    @JsonProperty("service_charges")
    public void setServiceCharges(String serviceCharges) {
        this.serviceCharges = serviceCharges;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("crop_choices")
    public Integer getCropChoices() {
        return cropChoices;
    }

    @JsonProperty("crop_choices")
    public void setCropChoices(Integer cropChoices) {
        this.cropChoices = cropChoices;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("third_party_sub_category_name")
    public String getThirdPartySubCategoryName() {
        return thirdPartySubCategoryName;
    }

    @JsonProperty("third_party_sub_category_name")
    public void setThirdPartySubCategoryName(String thirdPartySubCategoryName) {
        this.thirdPartySubCategoryName = thirdPartySubCategoryName;
    }

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("event_timestamp")
    public BigInteger getEventTimestamp() {
        return eventTimestamp;
    }

    @JsonProperty("event_timestamp")
    public void setEventTimestamp(BigInteger eventTimestamp) {
        this.eventTimestamp = eventTimestamp;
    }

    @JsonProperty("in_stock")
    public Integer getInStock() {
        return inStock;
    }

    @JsonProperty("in_stock")
    public void setInStock(Integer inStock) {
        this.inStock = inStock;
    }

    @JsonProperty("sub_category_desc")
    public String getSubCategoryDesc() {
        return subCategoryDesc;
    }

    @JsonProperty("sub_category_desc")
    public void setSubCategoryDesc(String subCategoryDesc) {
        this.subCategoryDesc = subCategoryDesc;
    }

    @JsonProperty("addons")
    public Map<String, Addon> getAddonMap() {
        return addonMap;
    }

    @JsonProperty("addons")
    public void setAddons(Map<String, Addon> addonMap) {
        this.addonMap = addonMap;
    }

    @JsonProperty("serves_how_many")
    public Integer getServesHowMany() {
        return servesHowMany;
    }

    @JsonProperty("serves_how_many")
    public void setServesHowMany(Integer servesHowMany) {
        this.servesHowMany = servesHowMany;
    }

    @JsonProperty("sub_category")
    public String getSubCategory() {
        return subCategory;
    }

    @JsonProperty("sub_category")
    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    @JsonProperty("service_tax")
    public String getServiceTax() {
        return serviceTax;
    }

    @JsonProperty("service_tax")
    public void setServiceTax(String serviceTax) {
        this.serviceTax = serviceTax;
    }

    @JsonProperty("category_order")
    public Integer getCategoryOrder() {
        return categoryOrder;
    }

    @JsonProperty("category_order")
    public void setCategoryOrder(Integer categoryOrder) {
        this.categoryOrder = categoryOrder;
    }

    @JsonProperty("is_spicy")
    public Integer getIsSpicy() {
        return isSpicy;
    }

    @JsonProperty("is_spicy")
    public void setIsSpicy(Integer isSpicy) {
        this.isSpicy = isSpicy;
    }

    @JsonProperty("gst_details")
    public GstDetails getGstDetails() {
        return gstDetails;
    }

    @JsonProperty("gst_details")
    public void setGstDetails(GstDetails gstDetails) {
        this.gstDetails = gstDetails;
    }

    @JsonProperty("vat")
    public String getVat() {
        return vat;
    }

    @JsonProperty("vat")
    public void setVat(String vat) {
        this.vat = vat;
    }

    @JsonProperty("menu_id")
    public Integer getMenuId() {
        return menuId;
    }

    @JsonProperty("menu_id")
    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    @JsonProperty("enabled")
    public Integer getEnabled() {
        return enabled;
    }

    @JsonProperty("enabled")
    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }

    @JsonProperty("third_party_id")
    public String getThirdPartyId() {
        return thirdPartyId;
    }

    @JsonProperty("third_party_id")
    public void setThirdPartyId(String thirdPartyId) {
        this.thirdPartyId = thirdPartyId;
    }

    @JsonProperty("category_desc")
    public String getCategoryDesc() {
        return categoryDesc;
    }

    @JsonProperty("category_desc")
    public void setCategoryDesc(String categoryDesc) {
        this.categoryDesc = categoryDesc;
    }

    @JsonProperty("s3_image_url")
    public String getS3ImageUrl() {
        return s3ImageUrl;
    }

    @JsonProperty("s3_image_url")
    public void setS3ImageUrl(String s3ImageUrl) {
        this.s3ImageUrl = s3ImageUrl;
    }

    @JsonProperty("image_id")
    public String getImageId() {
        return imageId;
    }

    @JsonProperty("image_id")
    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    @JsonProperty("sub_category_order")
    public Integer getSubCategoryOrder() {
        return subCategoryOrder;
    }

    @JsonProperty("sub_category_order")
    public void setSubCategoryOrder(Integer subCategoryOrder) {
        this.subCategoryOrder = subCategoryOrder;
    }

    @JsonProperty("version")
    public Integer getVersion() {
        return version;
    }

    @JsonProperty("version")
    public void setVersion(Integer version) {
        this.version = version;
    }

    @JsonProperty("is_veg")
    public Integer getIsVeg() {
        return isVeg;
    }

    @JsonProperty("is_veg")
    public void setIsVeg(Integer isVeg) {
        this.isVeg = isVeg;
    }

    @JsonProperty("order")
    public Integer getOrder() {
        return order;
    }

    @JsonProperty("order")
    public void setOrder(Integer order) {
        this.order = order;
    }

    @JsonProperty("recommended")
    public Boolean getRecommended() {
        return recommended;
    }

    @JsonProperty("recommended")
    public void setRecommended(Boolean recommended) {
        this.recommended = recommended;
    }

    @JsonProperty("variants")
    public Variants getVariants() {
        return variants;
    }

    @JsonProperty("variants")
    public void setVariants(Variants variants) {
        this.variants = variants;
    }

    @JsonProperty("addon_free_limit")
    public Integer getAddonFreeLimit() {
        return addonFreeLimit;
    }

    @JsonProperty("addon_free_limit")
    public void setAddonFreeLimit(Integer addonFreeLimit) {
        this.addonFreeLimit = addonFreeLimit;
    }

    @JsonProperty("addon_limit")
    public Integer getAddonLimit() {
        return addonLimit;
    }

    @JsonProperty("addon_limit")
    public void setAddonLimit(Integer addonLimit) {
        this.addonLimit = addonLimit;
    }

    @JsonProperty("is_perishable")
    public Integer getIsPerishable() {
        return isPerishable;
    }

    @JsonProperty("is_perishable")
    public void setIsPerishable(Integer isPerishable) {
        this.isPerishable = isPerishable;
    }

    @JsonProperty("restaurant_id")
    public Integer getRestaurantId() {
        return restaurantId;
    }

    @JsonProperty("restaurant_id")
    public void setRestaurantId(Integer restaurantId) {
        this.restaurantId = restaurantId;
    }

    @JsonProperty("sub_category_id")
    public Integer getSubCategoryId() {
        return subCategoryId;
    }

    @JsonProperty("sub_category_id")
    public void setSubCategoryId(Integer subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    @JsonProperty("eligible_for_long_distance")
    public Integer getEligibleForLongDistance() {
        return eligibleForLongDistance;
    }

    @JsonProperty("eligible_for_long_distance")
    public void setEligibleForLongDistance(Integer eligibleForLongDistance) {
        this.eligibleForLongDistance = eligibleForLongDistance;
    }

    @JsonProperty("catalog_attributes")
    public CatalogAttributes getCatalogAttributes() {
        return catalogAttributes;
    }

    @JsonProperty("catalog_attributes")
    public void setCatalogAttributes(CatalogAttributes catalogAttributes) {
        this.catalogAttributes = catalogAttributes;
    }

    @JsonProperty("third_party_category_name")
    public String getThirdPartyCategoryName() {
        return thirdPartyCategoryName;
    }

    @JsonProperty("third_party_category_name")
    public void setThirdPartyCategoryName(String thirdPartyCategoryName) {
        this.thirdPartyCategoryName = thirdPartyCategoryName;
    }

    @JsonProperty("packing_slab_count")
    public Integer getPackingSlabCount() {
        return packingSlabCount;
    }

    @JsonProperty("packing_slab_count")
    public void setPackingSlabCount(Integer packingSlabCount) {
        this.packingSlabCount = packingSlabCount;
    }

    @JsonProperty("category")
    public String getCategory() {
        return category;
    }

    @JsonProperty("category")
    public void setCategory(String category) {
        this.category = category;
    }

    @JsonProperty("price")
    public Integer getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(Integer price) {
        this.price = price;
    }

    @JsonProperty("preparation_style")
    public String getPreparationStyle() {
        return preparationStyle;
    }

    @JsonProperty("preparation_style")
    public void setPreparationStyle(String preparationStyle) {
        this.preparationStyle = preparationStyle;
    }

    @JsonProperty("is_discoverable")
    public Boolean getIsDiscoverable() {
        return isDiscoverable;
    }

    @JsonProperty("is_discoverable")
    public void setIsDiscoverable(Boolean isDiscoverable) {
        this.isDiscoverable = isDiscoverable;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("timestamp")
    public BigInteger getTimestamp() {
        return timestamp;
    }

    @JsonProperty("timestamp")
    public void setTimestamp(BigInteger timestamp) {
        this.timestamp = timestamp;
    }

    @JsonProperty("available")
    public Boolean getAvailable() {
        return available;
    }

    @JsonProperty("available")
    public void setAvailable(Boolean available) {
        this.available = available;
    }

}