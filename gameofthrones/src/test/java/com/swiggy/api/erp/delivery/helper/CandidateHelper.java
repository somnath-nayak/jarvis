package com.swiggy.api.erp.delivery.helper;

import com.swiggy.api.erp.delivery.Pojos.Candidate;
import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Supplier;

/**
 * Created by kiran.j on 1/31/18.
 */
public class CandidateHelper {

    Initialize gameofthrones = new Initialize();
    public static Supplier<String> mobileSupplier = () -> String.format("98123%05d", ThreadLocalRandom.current().nextInt(99999));

    public Processor createCandidate(Candidate candidate) throws IOException {
        JsonHelper jsonHelper = new JsonHelper();
        DeliveryHelperMethods del = new DeliveryHelperMethods();
        GameOfThronesService service = new GameOfThronesService(
                DeliveryConstant.delivery_Service, "addcandidate", gameofthrones);
        HashMap<String, String> requestheaders = del.doubleheader();
        Processor processor = new Processor(service, requestheaders, new String[] {ObjectToJSON(candidate)});
        return processor;
    }

    public int createCandidateAndActivate() throws Exception {
        Candidate candidate = new Candidate();
        DeliveryServiceHelper deliveryServiceHelper = new DeliveryServiceHelper();
        candidate.build();
        Processor processor = createCandidate(candidate);
//        int de_id = ;
//        deliveryServiceHelper.makeDEActive();
        return 0;
    }

    public String ObjectToJSON(Object className) throws IOException {
        String json = null;
        ObjectMapper mapper = new ObjectMapper();
        json = mapper.writeValueAsString(className);
        return json;
    }

}
