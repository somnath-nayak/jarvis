package com.swiggy.api.erp.vms.editservice.pojo;


import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"is_item_oos",
"alternate_items",
"oos_addons",
"oos_variants"
})
public class Oos_and_alternates {

@JsonProperty("is_item_oos")
private boolean is_item_oos;
@JsonProperty("alternate_items")
private List<Long> alternate_items = null;
@JsonProperty("oos_addons")
private List<Oos_addon> oos_addons = null;
@JsonProperty("oos_variants")
private List<Oos_variant> oos_variants = null;

/**
* No args constructor for use in serialization
* 
*/
public Oos_and_alternates() {
}

/**
* 
* @param oos_addons
* @param is_item_oos
* @param alternate_items
* @param oos_variants
*/
public Oos_and_alternates(boolean is_item_oos, List<Long> alternate_items, List<Oos_addon> oos_addons, List<Oos_variant> oos_variants) {
super();
this.is_item_oos = is_item_oos;
this.alternate_items = alternate_items;
this.oos_addons = oos_addons;
this.oos_variants = oos_variants;
}

@JsonProperty("is_item_oos")
public boolean isIs_item_oos() {
return is_item_oos;
}

@JsonProperty("is_item_oos")
public void setIs_item_oos(boolean is_item_oos) {
this.is_item_oos = is_item_oos;
}

public Oos_and_alternates withIs_item_oos(boolean is_item_oos) {
this.is_item_oos = is_item_oos;
return this;
}

@JsonProperty("alternate_items")
public List<Long> getAlternate_items() {
return alternate_items;
}

@JsonProperty("alternate_items")
public void setAlternate_items(List<Long> alternate_items) {
this.alternate_items = alternate_items;
}

public Oos_and_alternates withAlternate_items(List<Long> alternate_items) {
this.alternate_items = alternate_items;
return this;
}

@JsonProperty("oos_addons")
public List<Oos_addon> getOos_addons() {
return oos_addons;
}

@JsonProperty("oos_addons")
public void setOos_addons(List<Oos_addon> oos_addons) {
this.oos_addons = oos_addons;
}

public Oos_and_alternates withOos_addons(List<Oos_addon> oos_addons) {
this.oos_addons = oos_addons;
return this;
}

@JsonProperty("oos_variants")
public List<Oos_variant> getOos_variants() {
return oos_variants;
}

@JsonProperty("oos_variants")
public void setOos_variants(List<Oos_variant> oos_variants) {
this.oos_variants = oos_variants;
}

public Oos_and_alternates withOos_variants(List<Oos_variant> oos_variants) {
this.oos_variants = oos_variants;
return this;
}

@Override
public String toString() {
return new ToStringBuilder(this).append("is_item_oos", is_item_oos).append("alternate_items", alternate_items).append("oos_addons", oos_addons).append("oos_variants", oos_variants).toString();
}

@Override
public int hashCode() {
return new HashCodeBuilder().append(oos_addons).append(is_item_oos).append(alternate_items).append(oos_variants).toHashCode();
}

@Override
public boolean equals(Object other) {
if (other == this) {
return true;
}
if ((other instanceof Oos_and_alternates) == false) {
return false;
}
Oos_and_alternates rhs = ((Oos_and_alternates) other);
return new EqualsBuilder().append(oos_addons, rhs.oos_addons).append(is_item_oos, rhs.is_item_oos).append(alternate_items, rhs.alternate_items).append(oos_variants, rhs.oos_variants).isEquals();
}

}