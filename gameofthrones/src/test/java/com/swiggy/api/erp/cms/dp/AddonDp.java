package com.swiggy.api.erp.cms.dp;

import com.swiggy.api.erp.cms.constants.MenuConstants;
import com.swiggy.api.erp.cms.helper.FullMenuHelper;
import com.swiggy.api.erp.cms.pojo.Addons.Addons;
import com.swiggy.api.erp.cms.pojo.Addons.Entity;
import com.swiggy.api.erp.cms.pojo.BulkAddons.BulkAddons;
import com.swiggy.api.erp.cms.pojo.BulkAddons.Entities;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by kiran.j on 2/22/18.
 */
public class AddonDp {

    @DataProvider(name = "createaddon")
    public Iterator<Object[]> createaddon() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        Addons addons = new Addons();
        addons.build();

        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(addons), new String[]{}, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2)});

        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(addons), new String[]{"401"}, "123"});

        obj.add(new Object[]{"", jsonHelper.getObjectToJSON(addons), new String[]{"404"}, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2)});

        Entity entity = addons.getEntity();
        entity.build();
        entity.setGst_details(null);
        addons.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSONallowNullValues(addons), new String[]{}, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2)});

        entity = addons.getEntity();
        entity.build();
        entity.setAddon_group_id("string");
        addons.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(addons), new String[]{}, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2)});

        entity = addons.getEntity();
        entity.build();
        entity.setAddon_group_id(null);
        addons.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSONallowNullValues(addons), MenuConstants.addon_groupid, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2)});

        entity = addons.getEntity();
        entity.build();
        entity.setId(null);
        addons.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSONallowNullValues(addons), MenuConstants.createaddon_id, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2)});

        entity = addons.getEntity();
        entity.build();
        entity.setName("");
        addons.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(addons), MenuConstants.createaddon_name, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2)});

        entity = new Entity();
        entity.build();
        entity.setPrice(-10);
        addons.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(addons), MenuConstants.createaddon_price, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2)});

        return obj.iterator();
    }

    @DataProvider(name = "updateaddon")
    public Iterator<Object[]> updateaddon() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        Addons addons = new Addons();
        addons.build();

        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(addons), new String[]{}, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2), addons.getEntity().getId()});

        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(addons), new String[]{"401"}, "123", addons.getEntity().getId()});

        obj.add(new Object[]{"", jsonHelper.getObjectToJSON(addons), new String[]{"404"}, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2), addons.getEntity().getId()});

        Entity entity = addons.getEntity();
        entity.build();
        entity.setGst_details(null);
        addons.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSONallowNullValues(addons), new String[]{}, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2), addons.getEntity().getId()});

        entity = addons.getEntity();
        entity.build();
        entity.setAddon_group_id(null);
        addons.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSONallowNullValues(addons), MenuConstants.addon_groupid, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2), addons.getEntity().getId()});

        entity = addons.getEntity();
        entity.build();
        entity.setId(null);
        addons.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSONallowNullValues(addons), MenuConstants.createaddon_id, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2), "123"});

        entity = addons.getEntity();
        entity.build();
        entity.setName("");
        addons.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(addons), new String[]{}, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2), addons.getEntity().getId()});

        entity = new Entity();
        entity.build();
        entity.setPrice(-10);
        addons.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(addons), MenuConstants.createaddon_price, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2), addons.getEntity().getId()});

        return obj.iterator();
    }

    @DataProvider(name = "createbulkaddon")
    public Iterator<Object[]> createbulkaddon() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        Entities entity1 = new Entities();
        BulkAddons addons = new BulkAddons();
        addons.build();

        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(addons), new String[]{}, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2)});

        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(addons), new String[]{"401"}, "123"});

        obj.add(new Object[]{"", jsonHelper.getObjectToJSON(addons), new String[]{"404"}, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2)});

        Entities entity = new Entities();
        entity.build();
        entity.setId(null);
        addons.setEntities(new Entities[]{entity});
        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSONallowNullValues(addons), MenuConstants.bulkaddon_id, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2)});

        entity = addons.getEntities()[0];
        entity.build();
        entity1.build();
        entity.setGst_details(null);
        addons.setEntities(new Entities[]{entity});
        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSONallowNullValues(addons), new String[]{}, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2)});

        entity = new Entities();
        entity.build();
        entity1.build();
        entity.setName("");
        addons.setEntities(new Entities[]{entity1, entity});
        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(addons), MenuConstants.bulkaddon_name, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2)});

        entity = new Entities();
        entity.build();
        entity1.build();
        entity.setAddon_group_id(null);
        addons.setEntities(new Entities[]{entity1, entity});
        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(addons), MenuConstants.bulkaddon_group_id, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2)});

        entity = new Entities();
        entity.build();
        entity1.build();
        entity.setPrice(-10);
        addons.setEntities(new Entities[]{entity1, entity});
        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(addons), MenuConstants.bulkaddon_price, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2)});

        return obj.iterator();
    }

    @DataProvider(name = "updatebulkaddon")
    public Iterator<Object[]> updatebulkaddon() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        Entities entity1 = new Entities();
        BulkAddons addons = new BulkAddons();
        addons.build();

        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(addons), new String[]{}, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2)});

        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(addons), new String[]{"401"}, "123"});

        obj.add(new Object[]{"", jsonHelper.getObjectToJSON(addons), new String[]{"404"}, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2)});

        Entities entity = new Entities();
        entity.build();
        entity.setId(null);
        addons.setEntities(new Entities[]{entity});
        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSONallowNullValues(addons), MenuConstants.bulkaddon_id, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2)});

        entity = addons.getEntities()[0];
        entity.build();
        entity1.build();
        entity.setGst_details(null);
        addons.setEntities(new Entities[]{entity});
        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSONallowNullValues(addons), new String[]{}, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2)});

        entity = new Entities();
        entity.build();
        entity1.build();
        entity.setName("");
        addons.setEntities(new Entities[]{entity1, entity});
        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(addons),new String[]{}, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2)});

        entity = new Entities();
        entity.build();
        entity1.build();
        entity.setAddon_group_id(null);
        addons.setEntities(new Entities[]{entity1, entity});
        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(addons), MenuConstants.bulkaddon_group_id, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2)});

        entity = new Entities();
        entity.build();
        entity1.build();
        entity.setPrice(-10);
        addons.setEntities(new Entities[]{entity1, entity});
        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(addons), MenuConstants.bulkaddon_price, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2)});

        return obj.iterator();
    }
}
