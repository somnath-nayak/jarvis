package com.swiggy.api.erp.ff.dp.verificationService;

import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.api.erp.delivery.helper.EndToEndHelp;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.pojo.Cart;
import com.swiggy.api.sf.checkout.pojo.CreateMenuEntry;
import com.swiggy.api.sf.checkout.pojo.CreateOrderBuilder;
import com.swiggy.api.sf.snd.helper.SANDHelper;

public class VerificationBCP_Data {
	CheckoutHelper checkouthelper= new CheckoutHelper();
	SANDHelper sandHelper = new SANDHelper();
	
	@DataProvider (name = "orderCancellationDataUsingOMSAPI")
	public static Object[][] orderCancellationDataUsingOMSAPI(){
		return new Object[][]{
			{"29", "26", "0", "false"} // {dispoition_id, subDisposition_id, Cancellation_Fee, Cancellation_Fee_Applicability}
		};
	}
	
	@DataProvider(name = "orderFromBackend")
    public Object[][] orderFromBackend() throws Exception {
        EndToEndHelp endToEndHelp = new EndToEndHelp();
        List<Cart> cart = new ArrayList<>();
        cart.add(new Cart(null, null, "8428266", 1));
        CreateMenuEntry createOrder = new CreateOrderBuilder()
                .password("swiggy")
                .mobile(7899772315l)
                .cart(cart)
                .restaurant(9990)
                .paymentMethod("Cash")
                .orderComments("Test-Order")
                .buildAll();

        return new Object[][] {{createOrder, "29", "26", "0", "false"}};
    }
	

	
	

}
