package com.swiggy.api.erp.finance.dp;

import com.swiggy.api.erp.finance.constants.FinanceConstants;
import org.testng.annotations.DataProvider;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;

public class FinanceData implements FinanceConstants{

    @DataProvider(name="createOrderData")
    public static Object[][] getOrderId() {

        return new Object[][]{
                {"newmouprepaid", "scenario",new String[]{"reached","pickedup","delivered"}}
//                {"newmoupostpaid", "scenario","delivered"},
//                {"oldmouprepaid", "scenario","delivered"},
//                {"oldmoupostpaid", "scenario","delivered"},
        };
    }

    @DataProvider(name="cancelOrderData")
    public static Object[][] cancelOrder() {
        
        return new Object[][]{

                {"newmouprepaid", food_prepared_t, responsible_id_swiggy ,  "scenario"},
                {"newmouprepaid", food_prepared_t, responsible_id_customer,  "scenario"},
                {"newmouprepaid", food_prepared_t, responsible_id_restaurant,  "scenario"},
                {"newmouprepaid", food_prepared_f, responsible_id_swiggy,  "scenario"},
                {"newmouprepaid", food_prepared_f, responsible_id_customer,  "scenario"},
                {"newmouprepaid", food_prepared_f, responsible_id_restaurant,  "scenario"},
                {"newmoupostpaid", food_prepared_t, responsible_id_swiggy,  "scenario"},
                {"newmoupostpaid", food_prepared_t, responsible_id_customer,  "scenario"},
                {"newmoupostpaid", food_prepared_t, responsible_id_restaurant,  "scenario"},
                {"newmoupostpaid", food_prepared_f, responsible_id_swiggy,  "scenario"},
                {"newmoupostpaid", food_prepared_f, responsible_id_customer,  "scenario"},
                {"newmoupostpaid", food_prepared_f, responsible_id_restaurant,  "scenario"},
                {"oldmouprepaid", food_prepared_t, responsible_id_swiggy,  "scenario"},
                {"oldmouprepaid", food_prepared_t, responsible_id_customer,  "scenario"},
                {"oldmouprepaid", food_prepared_t, responsible_id_restaurant,  "scenario"},
                {"oldmouprepaid", food_prepared_f, responsible_id_swiggy,  "scenario"},
                {"oldmouprepaid", food_prepared_f, responsible_id_customer,  "scenario"},
                {"oldmouprepaid", food_prepared_f, responsible_id_restaurant,  "scenario"},
                {"oldmoupostpaid", food_prepared_t, responsible_id_swiggy,  "scenario"},
                {"oldmoupostpaid", food_prepared_t, responsible_id_customer,  "scenario"},
                {"oldmoupostpaid", food_prepared_t, responsible_id_restaurant,  "scenario"},
                {"oldmoupostpaid", food_prepared_f, responsible_id_swiggy,  "scenario"},
                {"oldmoupostpaid", food_prepared_f, responsible_id_customer,  "scenario"},
                {"oldmoupostpaid", food_prepared_f, responsible_id_restaurant,  "scenario"},
        };
    }
}
