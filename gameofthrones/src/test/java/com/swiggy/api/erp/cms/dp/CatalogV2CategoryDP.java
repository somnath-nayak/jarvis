package com.swiggy.api.erp.cms.dp;

import com.swiggy.api.erp.cms.helper.CatalogV2Helper;
import org.testng.annotations.DataProvider;

import java.time.Instant;

public class CatalogV2CategoryDP {
    CatalogV2Helper catalogV2Helper=new CatalogV2Helper();
    @DataProvider(name = "createcategory")
    public Object[][] category() throws InterruptedException{

        String s = "Category name"+String.valueOf(Instant.now().getEpochSecond());
        Thread.sleep(1000);

        return new Object[][]{
                {
                        s}
        };
    }
}
