package com.swiggy.api.erp.delivery.tests;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.EmptyResultDataAccessException;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;
import com.swiggy.api.erp.delivery.constants.ClusterConstants;
import com.swiggy.api.erp.delivery.dp.ClusterServiceDataProvider;
import com.swiggy.api.erp.delivery.helper.ClusterServiceHelper;

import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.Validator;

public class ClusterServiceTest extends ClusterServiceDataProvider
{
	ClusterServiceHelper clusterHelper = new ClusterServiceHelper();
	ClusterServiceDataProvider clusterdp = new ClusterServiceDataProvider();
	
	public static String cg_id_Test = null;
	public static String connection_id_Test = null;
	
	static String[] con_gr, con_gr1;
	static String[] con;
	static String[] con1;
	static String ItemId, selfDelItemId, item_refid, selfDelRefId;
	static String day_of_week, peak;
	static int hour_of_day;
	
	
	public void disableExisting_Connection_andcg()
	{
		List<Map<String,Object>> enabled_id = SystemConfigProvider.getTemplate("deliveryclusterdb")
																  .queryForList("select id from connection_groups where enabled='1'");
		List<String> arr=new ArrayList<>();
		for(Map<String,Object> test:enabled_id)
		{
			arr.add(test.get("id").toString());
		}
		con_gr = arr.toArray(new String[arr.size()]);
		
		List<Map<String,Object>> enabled_con_id = SystemConfigProvider.getTemplate("deliveryclusterdb")
																	  .queryForList("select id from connections where enabled='1'");
		List<String> arr1=new ArrayList<>();
		for(Map<String,Object> test:enabled_con_id)
		{
			arr1.add(test.get("id").toString());
		}
		con = arr1.toArray(new String[arr1.size()]);
		
		clusterHelper.updateMultipleTuplesQuery("connection_groups", "enabled", "0", "id", con_gr);
		clusterHelper.updateMultipleTuplesQuery("connections", "enabled", "0", "id", con);
	}

	public void enableExisting_Connection_andcg() 
	{
		clusterHelper.updateMultipleTuplesQuery("connection_groups", "enabled", "1", "id", con_gr);
		clusterHelper.updateMultipleTuplesQuery("connections", "enabled", "1", "id", con);
		
		Object[] enabled_id = SystemConfigProvider.getTemplate("deliveryclusterdb")
												  .queryForList("select id from connection_groups where enabled='1'").toArray();
		con_gr1 = Arrays.toString(enabled_id).replace("{", "").replace("}", "").replace("[", "{").replace("]", "}").split(",");
		System.out.println("con_gr1 --------1-------" + Arrays.toString(con_gr1));
		
		Object[] enabled_cgid = SystemConfigProvider.getTemplate("deliveryclusterdb")
													.queryForList("select id from connections where enabled='1'").toArray();
		con1 = Arrays.toString(enabled_cgid).replace("{", "").replace("}", "").replace("[", "{").replace("]", "}").split(",");
		System.out.println("con1 --------1-------" + Arrays.toString(con1));
	}
	
	
	public void deleteConnection_andcg_createdInTest(String connection_id, String cg_id)
	{
		clusterHelper.deleteQuery("connections", "id", connection_id);
		clusterHelper.deleteQuery("connection_groups", "id", cg_id);
	}
	
	
	public void connectionUpdate(String max_sla)
	{
		clusterHelper.updateConnection(ClusterConstants.from_cluster_id, ClusterConstants.to_cluster_id, 
									   ClusterConstants.last_mile, max_sla, ClusterConstants.openBF, 
									   ClusterConstants.closeBF, ClusterConstants.connection_type, 
									   ClusterConstants.is_connection_enabled, ClusterConstants.degradedState, ClusterServiceTest.connection_id_Test);
	}
	
	
	public String createExclusionConnection_getId(String from_cluster_id, String cg_id)
	{
		Processor response = clusterHelper.createExclusionTrueConnection(from_cluster_id, ClusterConstants.to_cluster_id, 
							 ClusterConstants.connection_position, ClusterConstants.is_connection_enabled, cg_id);
		String id = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.id");
		return id;
	}
	
	
	public String createExclusionConnection_getId(String cg_id)
	{
		Processor response = clusterHelper.createExclusionTrueConnection(ClusterConstants.from_cluster_id, ClusterConstants.to_cluster_id, 
							 ClusterConstants.connection_position, ClusterConstants.is_connection_enabled, cg_id);
		String id = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.id");
		return id;
	}
	
	
	public String createExclusionConnection_getId(int connectionPosition, String cg_id)
	{
		Processor response = clusterHelper.createExclusionTrueConnection(ClusterConstants.from_cluster_id, ClusterConstants.to_cluster_id, 
							 String.valueOf(connectionPosition), ClusterConstants.is_connection_enabled, cg_id);
		String id = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.id");
		return id;
	}
	
	
	
	public String createInclusionConnection_getId(String cg_id, String connectionPriority)
	{
		Processor inclusion_connection_created = clusterHelper.createConnection(cg_id, ClusterConstants.from_cluster_id, ClusterConstants.to_cluster_id, ClusterConstants.last_mile,
				ClusterConstants.max_sla, ClusterConstants.openBF, ClusterConstants.closeBF, ClusterConstants.time_slot, connectionPriority, 
				ClusterConstants.connection_type, ClusterConstants.is_connection_enabled, ClusterConstants.degradedState);

		String id = inclusion_connection_created.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.id");
		return id;
	}
	
	
	public String createInclusionConnection_getId(String cg_id, String closeBF, String connectionPriority)
	{
		Processor inclusion_connection_created = clusterHelper.createConnection(cg_id, ClusterConstants.from_cluster_id, ClusterConstants.to_cluster_id, ClusterConstants.last_mile,
				ClusterConstants.max_sla, ClusterConstants.openBF, closeBF, ClusterConstants.time_slot, connectionPriority, 
				ClusterConstants.connection_type, ClusterConstants.is_connection_enabled, ClusterConstants.degradedState);

		String id = inclusion_connection_created.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.id");
		return id;
	}
	
	
	public String createInclusionConnection_getId(String cg_id, String from_cluster_id, String last_mile, String max_sla)
	{
		Processor inclusion_connection_created = clusterHelper.createConnection(cg_id, from_cluster_id, ClusterConstants.to_cluster_id, last_mile,
				max_sla, ClusterConstants.openBF, ClusterConstants.closeBF, ClusterConstants.time_slot, ClusterConstants.connection_position, 
				ClusterConstants.connection_type, ClusterConstants.is_connection_enabled, ClusterConstants.degradedState);

		String id = inclusion_connection_created.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.id");
		return id;
	}
	
	
	
	public String createDistanceBasedInclusionConnection_getId(String cg_id)
	{
		Processor distanceBasedConnection = clusterHelper.createConnectionWithDistance(ClusterConstants.from_cluster_id, ClusterConstants.to_cluster_id, 
																					   ClusterConstants.last_mile, ClusterConstants.connection_position, 
																					   ClusterConstants.connection_type, ClusterConstants.is_connection_enabled, cg_id);
		String id = distanceBasedConnection.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.id");
		return id;
	}
	
	
	public String createSlaBasedInclusionConnection_getId(String connectionPriority, String cg_id)
	{
		Processor slaBasedConnection = clusterHelper.createConnectionWithSla(ClusterConstants.from_cluster_id, ClusterConstants.to_cluster_id, 
																			 ClusterConstants.max_sla, connectionPriority, 
																			 ClusterConstants.is_connection_enabled, cg_id);
		String id = slaBasedConnection.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.id");
		return id;
	}
	
	
	public String createCluster_getId(String[] payload)							//for fetching id of cluster after creating it..
	{
		Processor cluster_create = clusterHelper.createCluster(payload);
		String cluster_id = cluster_create.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.id");
		return cluster_id;
	}
	
	
	public String createItem_andGetId(String item_lat, String item_lng, String item_refId)
	{
		String[] payload = clusterdp.getCreateItemElements(item_lat, item_lng, item_refId, ClusterConstants.area_id, ClusterConstants.zone_id);
		Processor item_create = clusterHelper.create_Item(payload);
		String item_id = item_create.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.id");
		return item_id;
	}
	
	public String createItem_andGetId(String item_refId)
	{
		String[] payload = clusterdp.getCreateItemElements(ClusterConstants.item_lat, ClusterConstants.item_lng, item_refId, ClusterConstants.area_id, ClusterConstants.zone_id);
		Processor item_create = clusterHelper.create_Item(payload);
		String item_id = item_create.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.id");
		return item_id;
	}
	
	
	public String createSelfDelItem_andGetId(String lat, String lng, String item_refId, LinkedHashMap<String, String> map)
	{
		String[] payload = getSelfDelItemElements(lat, lng, item_refId, map);
		Processor item_create = clusterHelper.createSelfDeliveryItem(payload);
		String item_id = item_create.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.id");
		return item_id;
	}
	
	
	public String createItem_andGetId(String item_lat, String item_lng, String item_refId, String area_id, String zone_id)
	{
		String[] payload = clusterdp.getCreateItemElements(item_lat, item_lng, item_refId, area_id, zone_id);
		Processor item_create = clusterHelper.create_Item(payload);
		String item_id = item_create.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.id");
		return item_id;
	}
	
	
	public String createcg_andGetId(String cg_name)
	{
		clusterHelper.createConnectionGroup(ClusterConstants.cluster_id, ClusterConstants.time_slot, cg_name, ClusterConstants.connection_position, 
											ClusterConstants.rule_group_type, ClusterConstants.is_connection_enabled);

		String cg_id=SystemConfigProvider.getTemplate("deliveryclusterdb").queryForList("select id from connection_groups where name='" + cg_name + "'").get(0).get("id").toString();
		System.out.println("-----Test connection group id--------" + cg_id);
		return cg_id;
	}
	
	
	
	public String createcg_andGetId(String cg_name, String time_slot)
	{
		clusterHelper.createConnectionGroup(ClusterConstants.cluster_id, time_slot, cg_name, ClusterConstants.connection_position, 
											ClusterConstants.rule_group_type, ClusterConstants.is_connection_enabled);
		String cg_id=SystemConfigProvider.getTemplate("deliveryclusterdb")
										 .queryForList("select id from connection_groups where name='" + cg_name + "'").get(0).get("id").toString();
		System.out.println("-----Test connection group id--------" + cg_id);
		return cg_id;
	}
	
	
	public String createcg_andGetId(String cg_name, int connection_position)
	{
		clusterHelper.createConnectionGroup(ClusterConstants.cluster_id, ClusterConstants.time_slot, cg_name, String.valueOf(connection_position), 
											ClusterConstants.rule_group_type, ClusterConstants.is_connection_enabled);
		String cg_id=SystemConfigProvider.getTemplate("deliveryclusterdb")
										 .queryForList("select id from connection_groups where name='" + cg_name + "'").get(0).get("id").toString();
		System.out.println("-----Test connection group id--------" + cg_id);
		return cg_id;
	}
	
	
	
	
	public String createConnection_andGetId(String cg_id)
	{
		Processor createdConnection = clusterHelper.createConnection(cg_id, ClusterConstants.from_cluster_id, ClusterConstants.to_cluster_id, ClusterConstants.last_mile,
																	 ClusterConstants.max_sla, ClusterConstants.openBF, ClusterConstants.closeBF, 
																	 ClusterConstants.time_slot, ClusterConstants.connection_position, 
																	 ClusterConstants.connection_type, ClusterConstants.is_connection_enabled, ClusterConstants.degradedState);
		
		String connection_id = createdConnection.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.id");
		return connection_id;
	}
	
	public String createConnection_andGetId(String cg_id, String customerCluster_id, String RestaurantCluster_id, String last_mile)
	{
		Processor createdConnection = clusterHelper.createConnection(cg_id, customerCluster_id, RestaurantCluster_id, last_mile,
																	 ClusterConstants.max_sla, ClusterConstants.openBF, ClusterConstants.closeBF, 
																	 ClusterConstants.time_slot, ClusterConstants.connection_position, 
																	 ClusterConstants.connection_type, ClusterConstants.is_connection_enabled, ClusterConstants.degradedState);
		
		String connection_id = createdConnection.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.id");
		return connection_id;
	}
	
	
	public String fetchItemList_fromItem_Check_response(String customer_lat, String customer_lng)
	{
		Processor processor = clusterHelper.Item_Check(customer_lat, customer_lng, ClusterConstants.item_count, ClusterConstants.zone_id, ClusterConstants.currentBF, 
													   ClusterConstants.sla_extra_time, ClusterConstants.is_cluster_zone_open, item_refid, 
													   ClusterConstants.rule_group_type);
		
		String item_list = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.item_list");
		return item_list;
	}
	
	
	public String fetchItemList_fromItem_Check_response(String customer_lat, String customer_lng, String item_ref_id)
	{
		Processor processor = clusterHelper.Item_Check(customer_lat, customer_lng, ClusterConstants.item_count, clusterdp.getZoneId(item_ref_id), ClusterConstants.currentBF, 
													   ClusterConstants.sla_extra_time, ClusterConstants.is_cluster_zone_open, item_ref_id, 
													   ClusterConstants.rule_group_type);
		
		String item_list = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.item_list");
		return item_list;
	}
	
	
	public String fetchItemList_fromItem_Check_response(String customer_lat, String customer_lng, int currentBF)
	{
		Processor processor = clusterHelper.Item_Check(customer_lat, customer_lng, ClusterConstants.item_count, ClusterConstants.zone_id, String.valueOf(currentBF), 
													   ClusterConstants.sla_extra_time, ClusterConstants.is_cluster_zone_open, item_refid, 
													   ClusterConstants.rule_group_type);
		
		String item_list = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.item_list");
		return item_list;
	}

	
	public Processor item_check_response(String customer_lat, String customer_lng)
	{
		Processor response = clusterHelper.Item_Check(customer_lat, customer_lng, ClusterConstants.item_count, ClusterConstants.zone_id, 
				ClusterConstants.currentBF, ClusterConstants.sla_extra_time, 
				ClusterConstants.is_cluster_zone_open, item_refid, ClusterConstants.rule_group_type);
		return response;
	}
	
	
	public Processor item_check_response(String customer_lat, String customer_lng, String currentBF, 
										 String sla_extra_time, String item_ref_id)
	{
		Processor response = clusterHelper.Item_Check(customer_lat, customer_lng, ClusterConstants.item_count, clusterdp.getZoneId(item_ref_id), currentBF, sla_extra_time, 
													  ClusterConstants.is_cluster_zone_open, item_ref_id, ClusterConstants.rule_group_type);
		return response;
	}
	
	
	public String fetchIgnoredItemList_fromItem_Check_response(String customer_lat, String customer_lng, String currentBF, String isZoneOpen)
	{
		Processor processor = clusterHelper.Item_Check(customer_lat, customer_lng, ClusterConstants.item_count, clusterdp.getZoneId(item_refid), currentBF, 
													   ClusterConstants.sla_extra_time, isZoneOpen, item_refid, ClusterConstants.rule_group_type);
		
		String item_list = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.ignored_item_list");
		return item_list;
	}
	
	
	public String fetchId_fromItem_Search_response(String customer_lat, String customer_lng, String zone_id)
	{
		Processor itemSearch_response = clusterHelper.Item_Search(customer_lat, customer_lng, ClusterConstants.entity_type, ClusterConstants.item_count, 
																  zone_id, ClusterConstants.is_cluster_zone_open, ClusterConstants.sla_extra_time, 
																  ClusterConstants.currentBF, ClusterConstants.rule_group_type);
		
		String id = itemSearch_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.item_list[*].item.id");
		return id;
	}
	
	
	public String getSLA_fromItemCheckResponse(String customer_lat, String customer_lng, String item_count,
											   String sla_extra_time, String item_refId)
	{
		Processor itemCheckResponse = clusterHelper.Item_Check(customer_lat, customer_lng, item_count, clusterdp.getZoneId(item_refId), 
				ClusterConstants.currentBF, sla_extra_time, ClusterConstants.is_cluster_zone_open, item_refId, ClusterConstants.rule_group_type);

		String sla = itemCheckResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.item_list..local_attributes.sla")
					.replace("[", "").replace("]", "");
		return sla;
	}
	
	
	public Processor item_search_response(String customer_lat, String customer_lng, String entity_type, String rule_type)
	{
		Processor response = clusterHelper.Item_Search(customer_lat, customer_lng, entity_type, ClusterConstants.item_count, clusterdp.getZoneId(item_refid), 
													   ClusterConstants.is_cluster_zone_open, ClusterConstants.sla_extra_time, ClusterConstants.currentBF, rule_type);
		return response;
	}
	
	
	public void deleteMultipleTuplesQuery(String tableName, String reference_fieldName, String[] reference_fieldValue)
	{
		//String query = "delete from clusters where id in ('" + RestaurantCluster1_id + "', '" + RestaurantCluster2_id + "', '" + customerCluster_id + "')";
		String attributes = "";
		for(int i = 0; i < reference_fieldValue.length; i++)
		{
			if(i == (reference_fieldValue.length - 1))
				attributes = attributes + reference_fieldValue[i];
			else
				attributes = attributes + reference_fieldValue[i] + "', '";
		}
		String query = "delete from " + tableName + " where " + reference_fieldName + " in ('" + attributes + "')";
		SystemConfigProvider.getTemplate("deliveryclusterdb").execute(query);
	}
	
	
	public void createCg_andConnectionFor_Test(String cg_name)
	{
		cg_id_Test = createcg_andGetId(cg_name);
		connection_id_Test = createConnection_andGetId(cg_id_Test);
	}
	
	
	public boolean getCurrentDay()
	{
		day_of_week = LocalDate.now().getDayOfWeek().name();
		System.out.println("**CURRENT-DAY**" + day_of_week);
		boolean flag = day_of_week != "SUNDAY" && day_of_week != "SATURDAY";
		return flag;
	}
	
	
	public boolean getCurrentHour()
	{
		hour_of_day = LocalTime.now().getHour();
		System.out.println("**CURRENT-HOUR**" + hour_of_day);
		boolean flag = (12 < hour_of_day && hour_of_day < 16) || (19 < hour_of_day && hour_of_day < 23);
		System.out.println("**hour-flag**" + flag);
		return flag;
	}
	
	
	public String getCurrentPeak()
	{
		int hour_of_day = LocalTime.now().getHour();
		System.out.println("**CURRENT-HOUR**" + hour_of_day);
		peak = ((12 < hour_of_day) && (hour_of_day < 16)) ? "LUNCH" : ((19 < hour_of_day && hour_of_day < 23) ? "DINNER" : "REST");
		System.out.println("**PEAK**" + peak);
		return peak;
	}
	
	
	boolean ifItemExists(String ref_Id)
	{
		List<Map<String, Object>> query = SystemConfigProvider.getTemplate("deliveryclusterdb")
										  .queryForList("select * from items where reference_id='" + ref_Id + "'");
		boolean flag = query.size() != 0;
		return flag;
	}
	
	
	@BeforeTest
	public void beforeClass()
	{
		disableExisting_Connection_andcg();
		if(clusterHelper.ifTupleExistsInDB("connection_groups", "name", ClusterConstants.cgName))
		{
			try
			{
				String cgId = SystemConfigProvider.getTemplate("deliveryclusterdb").queryForMap("select id from connection_groups where name='" + ClusterConstants.cgName + "'").get("id").toString();
				clusterHelper.deleteQuery("connection_groups", "name", ClusterConstants.cgName);
				if(cgId != null)
					clusterHelper.deleteQuery("connections", "group_id", cgId);
			}
			catch (EmptyResultDataAccessException e) {}
		}
		createCg_andConnectionFor_Test(ClusterConstants.cgName);
		item_refid = ClusterConstants.ref_id;
		ItemId = createItem_andGetId(item_refid);
		clusterHelper.addItem_toCluster(ClusterConstants.cluster_id, ItemId);
		
		selfDelRefId = ClusterConstants.selfDelItem_ref_id;
		
		while(ifItemExists(selfDelRefId))
		{
			selfDelRefId = String.valueOf(Integer.sum(Integer.parseInt(selfDelRefId), 1000));
		}
		
		selfDelItemId = createSelfDelItem_andGetId(selfDelRefId);
		clusterHelper.addItem_toCluster(ClusterConstants.cluster_id, selfDelItemId);
		System.out.println("---------------before------------------");
	}
	
	
	@AfterTest
	public void afterClass()
	{
		System.out.println("---------------after------------------");
		deleteConnection_andcg_createdInTest(connection_id_Test, cg_id_Test);
		enableExisting_Connection_andcg();
		clusterHelper.deleteQuery("items", "id", ItemId);
		clusterHelper.deleteQuery("cluster_items", "item_id", ItemId);
		clusterHelper.deleteQuery("items", "id", selfDelItemId);
		clusterHelper.deleteQuery("cluster_items", "item_id", selfDelItemId);
	}
	
	
	@Test(dataProvider = "CSTC_01", description="CSTC_01 :- Verify items are serviceable when distance is less than the config at connection")
	public void distanceltCheck(String customer_lat, String customer_lng) 
	{
		Processor item_check_response = item_check_response(customer_lat, customer_lng);
		
		String itemResponseAsText = item_check_response.ResponseValidator.GetBodyAsText();
		String itemId= JsonPath.read(itemResponseAsText, "$.item_list[*].item.id")
					   .toString().replace("[", "").replace("]", "");
		
		String distance_check = JsonPath.read(itemResponseAsText, "$.execution_audit.audits." + itemId.replace("\"", "") + "[0].reason")
								.toString().replace("[", "").replace("]", "");
		String reason = JsonPath.read(itemResponseAsText, "$.execution_audit.audits." + itemId.replace("\"", "") + "[0].included")
					    .toString().replace("[", "").replace("]", "");
		
		Assert.assertEquals(distance_check, "DISTANCE_CHECK");
		Assert.assertEquals(reason, "true");
	}

	
	@Test(dataProvider = "CSTC_02", description="CSTC_02 :- Verify items are not serviceable when distance is greater than the config at connection")
	public void distancegtCheck(String customerLat, String customerLng) 
	{
		String item_list = fetchItemList_fromItem_Check_response(customerLat, customerLng);
		Assert.assertEquals(item_list, "[]");
	}

	
	@Test(dataProvider = "CSTC_07", description="CSTC_07 :- Verify items are serviceable when current BF is less than the closeBF config at connection")
	public void bannerltCheck(String customerLat, String customerLng) 
	{
		Processor item_check_response = item_check_response(customerLat, customerLng);
		
		String itemResponseAsText = item_check_response.ResponseValidator.GetBodyAsText();
		String itemId= JsonPath.read(itemResponseAsText, "$.item_list[*].item.id")
					   .toString().replace("[", "").replace("]", "");
		
		String distance_check = JsonPath.read(itemResponseAsText, "$.execution_audit.audits." + itemId.replace("\"", "") + "[2].reason")
								.toString().replace("[", "").replace("]", "");
		String reason = JsonPath.read(itemResponseAsText, "$.execution_audit.audits." + itemId.replace("\"", "") + "[2].included")
						.toString().replace("[", "").replace("]", "");
		
		Assert.assertEquals(distance_check, "BANNER_CHECK");
		Assert.assertEquals(reason, "true");
	}
	
	
	@Test(dataProvider = "CSTC_08", description="Verify items are not serviceable when current BF is greater than the closeBF config at connection")
	public void bannergtCheck(String customerLat, String customerLng, String current_banner_factor) 
	{
		Processor item_check_response = item_check_response(customerLat, customerLng, current_banner_factor, 
															ClusterConstants.sla_extra_time, item_refid);
		
		String itemResponseAsText = item_check_response.ResponseValidator.GetBodyAsText();
		String itemId= JsonPath.read(itemResponseAsText, "$.ignored_item_list..item.id")
					   .toString().replace("[", "").replace("]", "");
		
		String distance_check = JsonPath.read(itemResponseAsText, "$.execution_audit.audits." + itemId.replace("\"", "") + "[2].reason")
								.toString().replace("[", "").replace("]", "");
		String reason = JsonPath.read(itemResponseAsText, "$.execution_audit.audits." + itemId.replace("\"", "") + "[2].included")
						.toString().replace("[", "").replace("]", "");
		
		Assert.assertEquals(distance_check, "BANNER_CHECK");
		Assert.assertEquals(reason, "false");
	}
	
	
	@Test(dataProvider = "CSTC_11", description="CSTC_11 :- Verify items are not serviceable if zone is closed at cluster level")
	public void zoneClosedAtClusterLevel(String customerLat, String customerLng, String isClusterZoneOpen) 
	{
		String ignored_item_list = fetchIgnoredItemList_fromItem_Check_response(customerLat, customerLng, ClusterConstants.currentBF, 
																				isClusterZoneOpen);
		Assert.assertNotEquals(ignored_item_list, "[]");
	}
	
	
	/*@Test(dataProvider = "CSTC_12", description="CSTC_12 :- Verify items are seviceable if isZoneOpen:false at global cluster but at child cluster it is true")
	public void zoneClosedAtGlobalCluster(String customerLat, String customerLng, String isGlobalCluster_ZoneOpen) 
	{
		Processor clusterUpdateResponse = clusterHelper.updateCluster(isGlobalCluster_ZoneOpen, ClusterConstants.cluster_id);								//Cluster update => closing the zone of global cluster
		String isZoneOpenFalse = clusterUpdateResponse.ResponseValidator.
								 GetNodeValueAsStringFromJsonArray("$.attributes.isZoneOpen").replace("[", "").replace("]", "");
		String item_list = null;
		if(isGlobalCluster_ZoneOpen.equals(isZoneOpenFalse))
		{
			item_list = fetchItemList_fromItem_Check_response(customerLat, customerLng);
			
		}
		clusterHelper.updateCluster("true", ClusterConstants.cluster_id);	
		Assert.assertNotEquals(item_list, "[]");
		Assert.assertEquals(isZoneOpenFalse, isGlobalCluster_ZoneOpen);
	}*/
	
	
	@Test(dataProvider = "CSTC_13", description="CSTC_13 :- Verify items are serviceable if sla is less than the max_sla config at connection")
	public void slaltCheck(String customerLat, String customerLng) throws InterruptedException 				
	{
		connectionUpdate(ClusterConstants.max_sla);														//setting max_sla=70..
		List<Map<String, Object>> query = SystemConfigProvider.getTemplate("deliveryclusterdb")
										  .queryForList("select filter_group from connections where id='" + connection_id_Test + "'");

		String  maxSla = (JsonPath.read(query.get(0).get("filter_group").toString(), "$..filters..max_sla"))
						 .toString().replace("[", "").replace("]", "");
		String item_list = null;
		if(maxSla.equals(ClusterConstants.max_sla))
		{
			item_list = fetchItemList_fromItem_Check_response(customerLat, customerLng);
			
		}
		Assert.assertEquals(maxSla, ClusterConstants.max_sla);
		Assert.assertNotEquals(item_list, "[]");
	}
	
	
	@Test(dataProvider = "CSTC_14", description="CSTC_14 :- Verify items are not serviceable if sla is greater than the max_sla config at connection")
	public void slagtCheck(String customerLat, String customerLng, String max_sla) throws InterruptedException 
	{
		connectionUpdate(max_sla);									//setting max-sla=10..
		List<Map<String, Object>> query = SystemConfigProvider.getTemplate("deliveryclusterdb")
										  .queryForList("select filter_group from connections where id='" + connection_id_Test + "'");
		
	    String  maxSla = (JsonPath.read(query.get(0).get("filter_group").toString(), "$..filters..max_sla"))
	    				 .toString().replace("[", "").replace("]", "");
	    String ignored_item_list = null;
		if(maxSla.equals(max_sla))
		{
			ignored_item_list = fetchIgnoredItemList_fromItem_Check_response(customerLat, customerLng, 
																			 ClusterConstants.currentBF, ClusterConstants.is_cluster_zone_open);
		}
		
		connectionUpdate(ClusterConstants.max_sla);	
		Assert.assertEquals(maxSla, max_sla);
		Assert.assertNotEquals(ignored_item_list, "[]");
	}

	@Test(dataProvider = "CSTC_17", description="CSTC_17 :- Verify that only enabled connections are executing")
	public void enabledConnectionExecution(String customerLat, String customerLng, String connection_priority)
	{
		clusterHelper.updateQuery("connections", "enabled", "0", "group_id", cg_id_Test);
		
		String exclusionConnectionId = createExclusionConnection_getId(cg_id_Test);
		String inclusionConnectionId = createInclusionConnection_getId(cg_id_Test, connection_priority);
		
		String item_list = fetchItemList_fromItem_Check_response(customerLat, customerLng);
		
		String item_list_empty = null;
		if(!item_list.equals("[]"))
		{
			clusterHelper.updateQuery("connections", "enabled", "0", "id", inclusionConnectionId);
			item_list_empty = fetchItemList_fromItem_Check_response(customerLat, customerLng);
		}
		
		clusterHelper.updateQuery("connections", "enabled", "1", "id", connection_id_Test);
		deleteMultipleTuplesQuery("connections", "id", new String[] {inclusionConnectionId, exclusionConnectionId});
		Assert.assertNotEquals(item_list, "[]");
		Assert.assertEquals(item_list_empty, "[]");
	}
	
	
	@Test(dataProvider = "CSTC_18", description="CSTC_18 :- Verify enabled connections are executing as per their priority")
	public void enabledConnectionExecutingAsPriority(String customer_lat, String customer_lng, String slaBasedConnection_priority)
	{
		clusterHelper.updateQuery("connections", "enabled", "0", "group_id", cg_id_Test);
		String distanceBasedConnectionId = createDistanceBasedInclusionConnection_getId(cg_id_Test);
		String slaBasedConnectionId = createSlaBasedInclusionConnection_getId(slaBasedConnection_priority, cg_id_Test);
		
		Processor item_check_response = item_check_response(customer_lat, customer_lng);
		
		String itemResponseAsText = item_check_response.ResponseValidator.GetBodyAsText();
		String itemId= JsonPath.read(itemResponseAsText, "$.item_list[*].item.id")
					   .toString().replace("[", "").replace("]", "");
		String[] auditReasons= JsonPath.read(itemResponseAsText, 
							   "$.execution_audit.audits."+itemId.replace("\"", "")+".[*].reason")
							   .toString().replace("[", "").replace("]", "").split(",");
		
		clusterHelper.updateQuery("connections", "enabled", "1", "id", connection_id_Test);
		deleteMultipleTuplesQuery("connections", "id",
								  new String[] {distanceBasedConnectionId, slaBasedConnectionId});
		Assert.assertEquals(auditReasons[0], "\"DISTANCE_CHECK\"");
		Assert.assertEquals(auditReasons[1], "\"SLA_CHECK\"");
	}

	
	@Test(dataProvider = "CSTC_20", description="CSTC_20 :- Verify that mapped items are serviceable if connection is of inclusion type")
	public void connectionExecutionBasedOnInclusion(String customerLat, String customerLng)
	{
		clusterHelper.updateQuery("connections", "enabled", "0", "group_id", cg_id_Test);
		String connectionId = createDistanceBasedInclusionConnection_getId(cg_id_Test);
		String item_list= fetchItemList_fromItem_Check_response(customerLat, customerLng);
	
		clusterHelper.updateQuery("connections", "enabled", "1", "id", connection_id_Test);
		clusterHelper.deleteQuery("connections", "id", connectionId);
		Assert.assertNotEquals(item_list, "[]");
	}
	
	
	@Test(dataProvider = "CSTC_21", description="CSTC_21 :- Verify that mapped items are not serviceable if connection is of exclusion type")
	public void connectionExecutionBasedOnExclusion(String connectionType, String customerLat, String customerLng)
	{
		clusterHelper.updateQuery("connections", "enabled", "0", "group_id", cg_id_Test);
		String connectionId = createExclusionConnection_getId(cg_id_Test);
		
		Processor itemCheckResponse = item_check_response(customerLat, customerLng);
		
		String itemCheckResponseAsText = itemCheckResponse.ResponseValidator.GetBodyAsText();
		String itemId = JsonPath.read(itemCheckResponseAsText, "$.ignored_item_list[*].item.id")
						.toString().replace("[", "").replace("]", "");
		
		String auditReason= JsonPath.read(itemCheckResponseAsText, 
							"$.execution_audit.audits." + itemId.replace("\"", "") + "[-1:].reason")
							.toString().replace("[", "").replace("]", "").replace("\"", "");
		String isIncluded = JsonPath.read(itemCheckResponseAsText, 
							"$.execution_audit.audits."+itemId.replace("\"", "") +".[-1:].included")
							.toString().replace("[", "").replace("]", "");
		
		clusterHelper.updateQuery("connections", "enabled", "1", "id", connection_id_Test);
		clusterHelper.deleteQuery("connections", "id", connectionId);
		Assert.assertEquals(auditReason, "exclusion");
		Assert.assertEquals(isIncluded, "false");
	}
	
	
	@Test(dataProvider="CSTC_22", description="CSTC_22 :- Verify if 1st connection includes some items then 2nd connection (type: inclusion) won't be able to exclude unsatisfied included items")
	public void itemServiceable_When2ndConnection_InclusionType(String closeBF, String priority, String customerLat, String customerLng)
	{
		SystemConfigProvider.getTemplate("deliveryclusterdb").update("update connections set enabled='0' where id<>'" + connection_id_Test + "' and group_id='" + cg_id_Test + "'");
		String connection_id = createInclusionConnection_getId(cg_id_Test, closeBF, priority);
		
		String item_list = fetchItemList_fromItem_Check_response(customerLat, customerLng);
		
		clusterHelper.deleteQuery("connections", "id", connection_id);
		Assert.assertNotEquals(item_list, "[]");
	}

	
	@Test(dataProvider="CSTC_23", description="CSTC_23 :- Verify if 1st connection includes some items then 2nd connection (type: exclusion) can exclude unsatisfied included items")
	public void ItemUnserviceable_When2ndConnection_ExclusionType(int connectionPosition, String customerLat, String customerLng, String current_banner_factor)
	{
		SystemConfigProvider.getTemplate("deliveryclusterdb").update("update connections set enabled='0' where id<>'" + connection_id_Test + "' and group_id='" + cg_id_Test + "'");
		String connection_id = createExclusionConnection_getId(connectionPosition, cg_id_Test);
		
		Processor item_check_response = item_check_response(customerLat, customerLng);
		
		String itemCheckResponse_AsText = item_check_response.ResponseValidator.GetBodyAsText();
		String item_id = JsonPath.read(itemCheckResponse_AsText, "$.ignored_item_list[*].item.id")
						 .toString().replace("[", "").replace("]", "");
		
		String auditReason_Exclusion = JsonPath.read(itemCheckResponse_AsText, 
									   "$.execution_audit.audits." + item_id.replace("\"", "") + ".[-1:].reason")
									   .toString().replace("[", "").replace("]", "").replace("\"", "");

		clusterHelper.deleteQuery("connections", "id", connection_id);
		Assert.assertEquals(auditReason_Exclusion, "exclusion");
	}
	
	
	@Test(dataProvider = "CSTC_24-&-CSTC_25", description="23-Verify items are serviceable only if satisfy all multiple filters in a connection / 24-Verify items are not serviceable if can't satisfy all multiple filters in a connection")
	public void serviceabilityBasedOnFilters(String customerLat, String customerLng, int banner_factor)
	{
		if(banner_factor < 3)
		{
			String item_list = fetchItemList_fromItem_Check_response(customerLat, customerLng, banner_factor);
			Assert.assertNotEquals(item_list, "[]");
		}
		else
		{
			String ignored_item_list = fetchIgnoredItemList_fromItem_Check_response(customerLat, customerLng, String.valueOf(banner_factor), ClusterConstants.is_cluster_zone_open);
			Assert.assertNotEquals(ignored_item_list, "[]");
		}
	}
	
	
	@Test(dataProvider = "CSTC_26", description="CSTC_26 :- Verify that only enabled connection_groups are executing")
	public void onlyEnabled_ConnectionGroupExcecuting(String customerLat, String customerLng)
	{
		if(clusterHelper.ifTupleExistsInDB("connection_groups", "name", ClusterConstants.exclusionCG))
		{
			try
			{
				String cgId = SystemConfigProvider.getTemplate("deliveryclusterdb").queryForMap("select id from connection_groups where name='" + ClusterConstants.exclusionCG + "'").get("id").toString();
				clusterHelper.deleteQuery("connection_groups", "name", ClusterConstants.exclusionCG);
				if(cgId != null)
					clusterHelper.deleteQuery("connections", "group_id", cgId);
			}
			catch (EmptyResultDataAccessException e) {}
		}
		String cgId = createcg_andGetId(ClusterConstants.exclusionCG);
		String connectionId = createExclusionConnection_getId(cgId);
		
		String ignored_item_list = fetchIgnoredItemList_fromItem_Check_response(customerLat, customerLng, 
																				ClusterConstants.currentBF, ClusterConstants.is_cluster_zone_open);
		
		String item_list = null;
		if(!ignored_item_list.equals("[]"))
		{
			clusterHelper.updateQuery("connection_groups", "enabled", "0", "name", "ExclusionTest");
			item_list = fetchItemList_fromItem_Check_response(customerLat, customerLng);
		}
		clusterHelper.deleteQuery("connections", "id", connectionId);
		clusterHelper.deleteQuery("connection_groups", "id", cgId);
		Assert.assertNotEquals(ignored_item_list, "[]");
		Assert.assertNotEquals(item_list, "[]");
	}
	
	
	@Test(dataProvider = "CSTC_27", description="CSTC_27 :- Verify enabled connection_groups are executing as per their priority")
	public void ConnectionGroupExcecution_asPriority(String customerLat, String customerLng, int cg_position)
	{
		if(clusterHelper.ifTupleExistsInDB("connection_groups", "name", ClusterConstants.cgForPriorityTest))
		{
			try
			{
				String cgId = SystemConfigProvider.getTemplate("deliveryclusterdb").queryForMap("select id from connection_groups where name='" + ClusterConstants.cgForPriorityTest + "'").get("id").toString();
				clusterHelper.deleteQuery("connection_groups", "name", ClusterConstants.cgForPriorityTest);
				if(cgId != null)
					clusterHelper.deleteQuery("connections", "group_id", cgId);
			}
			catch (EmptyResultDataAccessException e) {}
		}
		String cg_id = createcg_andGetId("PriorityTest", cg_position);
		String connection_id = createExclusionConnection_getId(cg_id);
		
		Processor item_check_response = item_check_response(customerLat, customerLng);
		String itemCheckResponse_AsText = item_check_response.ResponseValidator.GetBodyAsText();
		String itemId = JsonPath.read(itemCheckResponse_AsText, "$.ignored_item_list[*].item.id")
						.toString().replace("[", "").replace("]", "");
		
		String auditReason= JsonPath.read(itemCheckResponse_AsText, 
							"$.execution_audit.audits." + itemId.replace("\"", "") + "[-1:].reason")
							.toString().replace("[", "").replace("]", "").replace("\"", "");
		String isIncluded = JsonPath.read(itemCheckResponse_AsText, 
							"$.execution_audit.audits."+itemId.replace("\"", "") +".[-1:].included")
							.toString().replace("[", "").replace("]", "");
		
		clusterHelper.deleteQuery("connections", "id", connection_id);
		clusterHelper.deleteQuery("connection_groups", "id", cg_id);
		Assert.assertEquals(auditReason, "exclusion");
		Assert.assertEquals(isIncluded, "false");
	}
	
	
	@Test(dataProvider = "CSTC_29", description="CSTC_29 :- Verify connection_groups are executing as per their time-slots")
	public void CgExecution_asPerTimeSlot(String customerLat, String customerLng)
	{
		clusterHelper.updateQuery("connection_groups", "enabled", "0", "id", cg_id_Test);
		if(clusterHelper.ifTupleExistsInDB("connection_groups", "name", ClusterConstants.cgForTimeSlotCheck[0]) || clusterHelper.ifTupleExistsInDB("connection_groups", "name", ClusterConstants.cgForTimeSlotCheck[1]))
		{
			try
			{
				String cgId = SystemConfigProvider.getTemplate("deliveryclusterdb").queryForMap("select id from connection_groups where name='" + ClusterConstants.cgForTimeSlotCheck[0] + "'").get("id").toString();
				if(cgId != null)
					clusterHelper.deleteQuery("connections", "group_id", cgId);
				String cgId1 = SystemConfigProvider.getTemplate("deliveryclusterdb").queryForMap("select id from connection_groups where name='" + ClusterConstants.cgForTimeSlotCheck[1] + "'").get("id").toString();
				if(cgId1 != null)
					clusterHelper.deleteQuery("connections", "group_id", cgId1);
				deleteMultipleTuplesQuery("connection_groups", "name", new String[] {  ClusterConstants.cgForTimeSlotCheck[0], ClusterConstants.cgForTimeSlotCheck[1]});
				deleteMultipleTuplesQuery("connections", "group_id", new String[] {cgId, cgId1});
			}
			catch (EmptyResultDataAccessException e) {}
		}
		String cg_id_mrng = createcg_andGetId(ClusterConstants.cgForTimeSlotCheck[0], "0000-1200");
		String connection_id_mrng = createConnection_andGetId(cg_id_mrng);
		
		String cg_id_evng = createcg_andGetId(ClusterConstants.cgForTimeSlotCheck[1], "1201-2359");
		String connection_id_evng = createExclusionConnection_getId(cg_id_evng);
		
		String item_list = fetchItemList_fromItem_Check_response(customerLat, customerLng);
		Calendar cal = Calendar.getInstance();
		 
		// Set time of calendar to 12:00
		cal.set(Calendar.HOUR_OF_DAY, 12);
		cal.set(Calendar.MINUTE, 0);
		 
		boolean afterNoon = Calendar.getInstance().after(cal);
		clusterHelper.updateQuery("connection_groups", "enabled", "1", "id", cg_id_Test);
		deleteMultipleTuplesQuery("connections", "id", new String[] {connection_id_mrng, connection_id_evng});
		deleteMultipleTuplesQuery("connection_groups", "id", new String[] {cg_id_mrng, cg_id_evng});
		if (afterNoon) {
		    Assert.assertEquals(item_list, "[]");
		}
		else {
		    Assert.assertNotEquals(item_list, "[]");
		}
	}
	
	
	@Test(dataProvider = "CSTC_30", description="CSTC_30 :- Verify time-slot should not be NULL in a connection_groups")
	public void CgTimeSlot_Null(String customerLat, String customerLng)
	{
		if(clusterHelper.ifTupleExistsInDB("connection_groups", "name", ClusterConstants.nullTimeSlotCG))
		{
			try
			{
				String cgId = SystemConfigProvider.getTemplate("deliveryclusterdb").queryForMap("select id from connection_groups where name='" + ClusterConstants.nullTimeSlotCG + "'").get("id").toString();
				clusterHelper.deleteQuery("connection_groups", "name", ClusterConstants.nullTimeSlotCG);
				if(cgId != null)
					clusterHelper.deleteQuery("connections", "group_id", cgId);
			}
			catch (EmptyResultDataAccessException e) {}
		}
		clusterHelper.updateQuery("connection_groups", "enabled", "0", "id", cg_id_Test);
		clusterHelper.createConnectionGroup_NullTimeSlot(ClusterConstants.cluster_id, ClusterConstants.nullTimeSlotCG);
		
		String cg_id = SystemConfigProvider.getTemplate("deliveryclusterdb").queryForList("select id from connection_groups where name='" + ClusterConstants.nullTimeSlotCG + "'").get(0).get("id").toString();
		String connection_id = createConnection_andGetId(cg_id);
		
		Processor item_check_response = item_check_response(customerLat, customerLng);
		
		int itemCheckResponse_ResponseCode = item_check_response.ResponseValidator.GetResponseCode();
		clusterHelper.updateQuery("connection_groups", "enabled", "1", "id", cg_id_Test);
		clusterHelper.deleteQuery("connection_groups", "id", cg_id);
		clusterHelper.deleteQuery("connections", "id", connection_id);
		Assert.assertEquals(itemCheckResponse_ResponseCode, 500);
	}
	
	
	@Test(dataProvider = "CSTC_31",description="CSTC_31 :- Verify only enabled mapped items of a cluster are showing in item_search api response")
	public void enabledMappedItem_toCluster_serviceable(String customer_lat, String customer_lng)		
	{
		clusterHelper.enableItem_inCluster(ClusterConstants.cluster_id, 
										   ClusterConstants.getRestaurantId(item_refid));
		
		String itemsId = fetchId_fromItem_Search_response(customer_lat, customer_lng, 
														  clusterdp.getZoneId(item_refid));
		Assert.assertTrue(itemsId.contains(ClusterConstants.getRestaurantId(item_refid)));
	}
	
	
	@Test(dataProvider = "CSTC_32",description="CSTC_32 :- Verify disabled items should not be shown in item_seach api response")
	public void disabledMappedItem_toCluster_unServiceable(String customer_lat, String customer_lng)	
	{
		clusterHelper.ItemDisable_inCluster(ClusterConstants.cluster_id, 
											ClusterConstants.getRestaurantId(item_refid));
		
		String itemsId = fetchId_fromItem_Search_response(customer_lat, customer_lng, 
														  clusterdp.getZoneId(item_refid));
		
		clusterHelper.enableItem_inCluster(ClusterConstants.cluster_id, 
										   ClusterConstants.getRestaurantId(item_refid));
		Assert.assertFalse(itemsId.contains(ClusterConstants.getRestaurantId(item_refid)));
		
	}
	
	
	@Test(dataProvider = "CSTC_33", description="CSTC_32 :- Verify deleted items should not be shown in item_seach api response")
	public void mappedItem_toCluster_deletedUnserviceable(String customer_lat, String customer_lng, String item_ref_id)
	{
		while(ifItemExists(item_ref_id))
		{
			item_ref_id = String.valueOf(Math.incrementExact(Integer.parseInt(item_ref_id)));
		}
		String itemId = createItem_andGetId(item_ref_id);
		clusterHelper.addItem_toCluster(ClusterConstants.cluster_id, itemId);
		clusterHelper.deleteItem_fromCluster(ClusterConstants.cluster_id, itemId);
		
		String itemsId = fetchId_fromItem_Search_response(customer_lat, customer_lng, ClusterConstants.zone_id);
		clusterHelper.enableItem_inCluster(ClusterConstants.cluster_id, itemId);
		clusterHelper.updateQuery("cluster_items", "deleted", "0", "item_id", itemId);
		clusterHelper.deleteQuery("items", "id", itemId);
		clusterHelper.deleteQuery("cluster_items", "item_id", itemId);
		
		Assert.assertFalse(itemsId.contains(itemId));
	}
	
	
	@Test(dataProvider = "CSTC_34", description="CSTC_34 :- Verify if any item is disabled from items table then it should not be shown in item_search api response even though that item is marked as enabled in cluster_items table")
	public void disabledItem_mappedtoCluster_UnServiceable(String customer_lat, String customer_lng)
	{
		clusterHelper.disable_Item(ItemId);		
		clusterHelper.enableItem_inCluster(ClusterConstants.cluster_id, ItemId);
		
		String itemsId = fetchId_fromItem_Search_response(customer_lat, customer_lng, 
														  clusterdp.getZoneId(item_refid));
		clusterHelper.enable_Item(ItemId);	
		
		Assert.assertFalse(itemsId.contains(ItemId));
	}
	
	
	@Test(dataProvider = "CSTC_36", description="CSTC_36 :- Verify item should be shown in item_search api response even though that item is disabled in another cluster")
	public void ItemServiceable_disabledInAnotherCluster(String[] payload_restaurantCluster1, String[] payload_restaurantCluster2, 
														 String[] payload_customerCluster, String item_lat, String item_lng, String item_refId, 
														 String last_mile, String customer_lat, String customer_lng)
	{
		clusterHelper.updateQuery("connection_groups", "enabled", "0", "id", cg_id_Test);
		clusterHelper.updateQuery("connections", "enabled", "0", "group_id", cg_id_Test);
		
		if(clusterHelper.ifTupleExistsInDB("clusters", "name", ClusterConstants.clustersName[0]) || clusterHelper.ifTupleExistsInDB("clusters", "name", ClusterConstants.clustersName[1]) || clusterHelper.ifTupleExistsInDB("clusters", "name", ClusterConstants.clustersName[2]) || clusterHelper.ifTupleExistsInDB("connection_groups", "name", ClusterConstants.multiClusterCG))
		{
			
			List<Map<String, Object>> clusterIds = SystemConfigProvider.getTemplate("deliveryclusterdb").queryForList("select id from clusters where name in ('" + ClusterConstants.clustersName[0] + "', '" + ClusterConstants.clustersName[1] + "', '" + ClusterConstants.clustersName[2] + "')");
			String[] ids = new String[clusterIds.size()];
			for (int i = 0; i < clusterIds.size(); i++) {
				ids[i] = clusterIds.get(i).get("id").toString();
			}
			
			if(ids != null)
				deleteMultipleTuplesQuery("cluster_items", "cluster_id", ids);
			
			deleteMultipleTuplesQuery("clusters", "name", ids);
			try
			{
				String cgId = SystemConfigProvider.getTemplate("deliveryclusterdb").queryForMap("select id from connection_groups where name='" + ClusterConstants.multiClusterCG + "'").get("id").toString();
				if(cgId != null)
					clusterHelper.deleteQuery("connections", "group_id", cgId);
				clusterHelper.deleteQuery("connection_groups", "name", ClusterConstants.multiClusterCG);
			}
			catch (EmptyResultDataAccessException e) {}
			
		}
		String RestaurantCluster1_id = createCluster_getId(payload_restaurantCluster1);											
		String RestaurantCluster2_id = createCluster_getId(payload_restaurantCluster2);											
		String customerCluster_id = createCluster_getId(payload_customerCluster);													
		
		String item_id = createItem_andGetId(item_lat, item_lng, item_refId);	
		
		clusterHelper.addItem_toCluster(RestaurantCluster1_id, item_id);
		clusterHelper.addItem_toCluster(RestaurantCluster2_id, item_id);
		
		String cg_id = createcg_andGetId(ClusterConstants.multiClusterCG);																				//created connection group..
		
		String connection1_id = createConnection_andGetId(cg_id, customerCluster_id, RestaurantCluster1_id, last_mile);			//from customer cluster to first restaurant cluster
		String connection2_id = createConnection_andGetId(cg_id, customerCluster_id, RestaurantCluster2_id, last_mile);			//from customer cluster to second restaurant cluster
		
		String items_Id = fetchId_fromItem_Search_response(customer_lat, customer_lng, ClusterConstants.zone_id);
		String itemsId = null;
		if(items_Id.contains(item_id))
		{
			clusterHelper.updateQuery("cluster_items", "enabled", "0", "cluster_id", RestaurantCluster1_id);										//disabled the first restaurant cluster mapping..
			itemsId = fetchId_fromItem_Search_response(customer_lat, customer_lng, ClusterConstants.zone_id);
			clusterHelper.updateQuery("cluster_items", "enabled", "1", "cluster_id", RestaurantCluster1_id);
		}

		deleteMultipleTuplesQuery("clusters", "id", 
								  new String[] {RestaurantCluster1_id,RestaurantCluster2_id,customerCluster_id});																//deletes the clusters created in this test case..
		clusterHelper.deleteQuery("connection_groups", "id", cg_id);																				//deletes the connection group created in this test case..
		deleteMultipleTuplesQuery("connections", "id", new String[] {connection1_id, connection2_id});								//deletes the connections created in this test case..
		clusterHelper.deleteQuery("items", "id", item_id);																						//deletes the restaurant created in this test case..
		clusterHelper.deleteQuery("cluster_items", "item_id", item_id);																			//deletes the restaurant mapping created in this test case..
				
		clusterHelper.updateQuery("connection_groups", "enabled", "1", "id", cg_id_Test);
		clusterHelper.updateQuery("connections", "enabled", "1", "id", connection_id_Test);
		
		Assert.assertTrue(items_Id.contains(item_id));
		Assert.assertTrue(itemsId.contains(item_id));
		
	}
	
	
	@Test(dataProvider  = "CSTC_39", description="CSTC_38 :- Verify items are showing in item_search api response based on rule_type and entity")
	public void itemSearchBasedOnRuleTypeAndEntity(String customerLat, String customerLng)			
	{
		String entity_type=SystemConfigProvider.getTemplate("deliveryclusterdb")
											   .queryForList("select entity_type from clusters where id='" + ClusterConstants.cluster_id + "'").get(0).get("entity_type").toString();
		String rule_type=SystemConfigProvider.getTemplate("deliveryclusterdb")
											 .queryForList("select type from connection_groups where id='" + cg_id_Test + "'").get(0).get("type").toString();
		
		Processor item_search_response = item_search_response(customerLat, customerLng, entity_type, rule_type);
		String response_as_text = item_search_response.ResponseValidator.GetBodyAsText();
		String[] type = JsonPath.read(response_as_text, "$.item_list..item.type")
						.toString().replace("[", "").replace("]", "").replace("\"", "").split(",");
		for(String ent_type : type)
		{
			Assert.assertEquals(ent_type, "restaurant");
		}
	}
	
	
	@Test(dataProvider  = "CSTC_40", description="CSTC_40 :- Verify items should be shown in ignored_item_list if not serviceable according to connections")
	public void itemUnserviceable_shownIn_ignoredItemList(String customerLat, String customerLng, String current_factor)
	{
		String ignored_item_list = fetchIgnoredItemList_fromItem_Check_response(customerLat, customerLng, 
																				current_factor, ClusterConstants.is_cluster_zone_open);
		Assert.assertNotEquals(ignored_item_list, "[]");
	}
	
	
	@Test(dataProvider = "CSTC_41/42/43/44", description="CSTC_41 :- Verify avgPrepTimeWEP/WENP/WDP/WDNP is adding with sla calculation (sla filter connection should be enabled)")
	public void avgPrepTimeWEP_WENP_WDP_WDNP_AddingWithSLA(String[] item_attributes, String customer_lat, String customer_lng, String item_ref_id)
	{
		if(!getCurrentDay() & getCurrentHour())
		{
			System.out.println("****WEP**** => " + "DAY_OF_WEEK -> " + day_of_week + " & HOUR_OF_DAY -> " + hour_of_day);
			double add = 10.0;
			clusterHelper.updateItem(item_attributes, ClusterConstants.getRestaurantId(item_ref_id));
			String initial_sla = getSLA_fromItemCheckResponse(customer_lat, customer_lng, ClusterConstants.item_count,
															  ClusterConstants.sla_extra_time, item_ref_id);
			
			String initial_avgPrepTimeWEP = item_attributes[10];
			item_attributes[10] = String.valueOf(Double.parseDouble(initial_avgPrepTimeWEP)+add);
			clusterHelper.updateItem(item_attributes, ClusterConstants.getRestaurantId(item_ref_id));
			
			String final_sla = getSLA_fromItemCheckResponse(customer_lat, customer_lng, 
															ClusterConstants.item_count, ClusterConstants.sla_extra_time, item_ref_id);
			
			item_attributes[10] = initial_avgPrepTimeWEP;
			clusterHelper.updateItem(item_attributes, ClusterConstants.getRestaurantId(item_ref_id));
			Assert.assertEquals(Double.parseDouble(final_sla), (Double.parseDouble(initial_sla)+add));
		}
		else if(!getCurrentDay() & !getCurrentHour())
		{
			System.out.println("**WENP** => " + "DAY_OF_WEEK -> " + day_of_week + " & HOUR_OF_DAY -> " + hour_of_day);
			double add = 10.0;
			clusterHelper.updateItem(item_attributes, ClusterConstants.getRestaurantId(item_ref_id));
			String initial_sla = getSLA_fromItemCheckResponse(customer_lat, customer_lng, 
															  ClusterConstants.item_count, ClusterConstants.sla_extra_time, item_ref_id);
			
			String initial_avgPrepTimeWENP = item_attributes[12];
			item_attributes[12] = String.valueOf(Double.parseDouble(initial_avgPrepTimeWENP)+add);
			clusterHelper.updateItem(item_attributes, ClusterConstants.getRestaurantId(item_ref_id));
			
			String final_sla = getSLA_fromItemCheckResponse(customer_lat, customer_lng, 
															ClusterConstants.item_count, ClusterConstants.sla_extra_time, item_ref_id);
			
			item_attributes[12] = initial_avgPrepTimeWENP;
			clusterHelper.updateItem(item_attributes, ClusterConstants.getRestaurantId(item_ref_id));
			Assert.assertEquals(Double.parseDouble(final_sla), (Double.parseDouble(initial_sla)+add));
		}
		else if(getCurrentDay() && getCurrentHour())
		{
			System.out.println("****WDP**** => " + "DAY_OF_WEEK -> " + day_of_week + " & HOUR_OF_DAY -> " + hour_of_day);
			double add = 10.0;
			clusterHelper.updateItem(item_attributes, ClusterConstants.getRestaurantId(item_ref_id));
			String initial_sla = getSLA_fromItemCheckResponse(customer_lat, customer_lng, 
															  ClusterConstants.item_count, ClusterConstants.sla_extra_time, item_ref_id);
			
			String initial_avgPrepTimeWDP = item_attributes[9];
			item_attributes[9] = String.valueOf(Double.parseDouble(initial_avgPrepTimeWDP)+add);
			clusterHelper.updateItem(item_attributes, ClusterConstants.getRestaurantId(item_ref_id));
			
			String final_sla = getSLA_fromItemCheckResponse(customer_lat, customer_lng, 
															ClusterConstants.item_count, ClusterConstants.sla_extra_time, item_ref_id);
			
			item_attributes[9] = initial_avgPrepTimeWDP;
			clusterHelper.updateItem(item_attributes, ClusterConstants.getRestaurantId(item_ref_id));
			Assert.assertEquals(Double.parseDouble(final_sla), (Double.parseDouble(initial_sla)+add));
		}
		else if(getCurrentDay() && !getCurrentHour())
		{
			System.out.println("****WDNP**** => " + "DAY_OF_WEEK -> " + day_of_week + " & HOUR_OF_DAY -> " + hour_of_day);
			double add = 10.0;
			clusterHelper.updateItem(item_attributes, ClusterConstants.getRestaurantId(item_ref_id));
			String initial_sla = getSLA_fromItemCheckResponse(customer_lat, customer_lng, 
															  ClusterConstants.item_count, ClusterConstants.sla_extra_time, item_ref_id);
			
			String initial_avgPrepTimeWDNP = item_attributes[11];
			item_attributes[11] = String.valueOf(Double.parseDouble(initial_avgPrepTimeWDNP)+add);
			clusterHelper.updateItem(item_attributes, ClusterConstants.getRestaurantId(item_ref_id));
			
			String final_sla = getSLA_fromItemCheckResponse(customer_lat, customer_lng, 
															ClusterConstants.item_count, ClusterConstants.sla_extra_time, item_ref_id);
			
			item_attributes[11] = initial_avgPrepTimeWDNP;
			clusterHelper.updateItem(item_attributes, ClusterConstants.getRestaurantId(item_ref_id));
			Assert.assertEquals(Double.parseDouble(final_sla), (Double.parseDouble(initial_sla)+add));
		}
		else
		{
			Assert.fail();
		}
	}
	
	
	
	@Test(dataProvider = "CSTC_45", description="CSTC_45 :- Verify zoneSlaExtraTime is adding with sla calculation")
	public void zoneSlaExtraTime_addingIn_CurrentSla(String customerLat, String customerLng, String zoneSlaExtraTime)
	{
		String initial_sla = getSLA_fromItemCheckResponse(customerLat, customerLng,
														  ClusterConstants.item_count, zoneSlaExtraTime, item_refid);
		
		String final_sla = getSLA_fromItemCheckResponse(customerLat, customerLng, 
														ClusterConstants.item_count, ClusterConstants.sla_extra_time, item_refid);
		Assert.assertEquals(Integer.parseInt(final_sla), (Integer.parseInt(initial_sla) + Integer.parseInt(ClusterConstants.sla_extra_time)));
	}
	
	
	/*@Test(dataProvider="CSTC_46", description="Verify avgFirstMileTime is adding with sla calculation")			==========> avgFirstMileTime not adding in sla..
	public void avgFirstMileTimeAddingWithSLA(String[] item_attributes, String customerLat, String customerLng, String item_ref_id)
	{
		clusterHelper.updateItem(item_attributes, ClusterConstants.getRestaurantId(item_ref_id));
		String initial_sla = getSLA_fromItemCheckResponse(customerLat, customerLng, 
														  ClusterConstants.item_count, clusterdp.getZoneId(item_ref_id), 
														  ClusterConstants.sla_extra_time, item_ref_id);

		String initial_avgFirstMileTime = item_attributes[26];
		Double add = 10.0;
		item_attributes[26] = String.valueOf(Double.parseDouble(initial_avgFirstMileTime) + add);
		clusterHelper.updateItem(item_attributes, ClusterConstants.getRestaurantId(item_ref_id));
		String final_sla = getSLA_fromItemCheckResponse(customerLat, customerLng, ClusterConstants.item_count, 
														clusterdp.getZoneId(item_ref_id), ClusterConstants.sla_extra_time, item_ref_id)

		item_attributes[26] = initial_avgFirstMileTime;
		clusterHelper.updateItem(item_attributes, ClusterConstants.getRestaurantId(item_ref_id));
		Assert.assertEquals(Double.parseDouble(final_sla), (Double.parseDouble(initial_sla)+add));
	}	*/
	
	
	@Test(dataProvider="CSTC_47/48/49", description="CSTC_47 :- Verify avgAssignmentDelayLunch/Dinner/Rest is adding with sla calculation at lunch time")
	public void avgAssignmentDelayLunch_dinner_restAddingWithSLA(String[] item_attributes, String customer_lat, String customer_lng, String item_ref_id)
	{
		if(getCurrentPeak() == "LUNCH")
		{
			System.out.println("**avgAssignmentDelayLunch** => CURRENT_PEAK -> " + peak);
			clusterHelper.updateItem(item_attributes, ClusterConstants.getRestaurantId(item_ref_id));
			String initial_sla = getSLA_fromItemCheckResponse(customer_lat, customer_lng, 
															  ClusterConstants.item_count, ClusterConstants.sla_extra_time, item_ref_id);
			
			String initial = item_attributes[21];
			Double add = 20.0;
			item_attributes[21] = String.valueOf(Double.parseDouble(initial)+add);
			clusterHelper.updateItem(item_attributes, ClusterConstants.getRestaurantId(item_ref_id));
			
			String final_sla = getSLA_fromItemCheckResponse(customer_lat, customer_lng, 
															ClusterConstants.item_count, ClusterConstants.sla_extra_time, item_ref_id);
			
			item_attributes[21] = initial;
			clusterHelper.updateItem(item_attributes, ClusterConstants.getRestaurantId(item_ref_id));
			Assert.assertNotEquals(initial_sla, final_sla);
		}
		else if(getCurrentPeak() == "DINNER")
		{
			System.out.println("****AvgAssignmentDelayDinner**** => CURRENT_PEAK -> " + peak);
			clusterHelper.updateItem(item_attributes, ClusterConstants.getRestaurantId(item_ref_id));
			String initial_sla = getSLA_fromItemCheckResponse(customer_lat, customer_lng, 
															  ClusterConstants.item_count, ClusterConstants.sla_extra_time, item_ref_id);
			
			String initial = item_attributes[23];
			Double add = 20.0;
			item_attributes[23] = String.valueOf(Double.parseDouble(initial)+add);
			clusterHelper.updateItem(item_attributes, ClusterConstants.getRestaurantId(item_ref_id));
			
			String final_sla = getSLA_fromItemCheckResponse(customer_lat, customer_lng, 
															ClusterConstants.item_count, ClusterConstants.sla_extra_time, item_ref_id);
			
			item_attributes[23] = initial;
			clusterHelper.updateItem(item_attributes, ClusterConstants.getRestaurantId(item_ref_id));
			Assert.assertNotEquals(initial_sla, final_sla);
		}
		else if(getCurrentPeak() == "REST")
		{
			System.out.println("****AvgAssignmentDelayRest**** => CURRENT_PEAK -> " + peak);
			clusterHelper.updateItem(item_attributes, ClusterConstants.getRestaurantId(item_ref_id));
			String initial_sla = getSLA_fromItemCheckResponse(customer_lat, customer_lng, 
															  ClusterConstants.item_count, ClusterConstants.sla_extra_time, item_ref_id);
			String initial = item_attributes[20];
			Double add = 20.0;
			item_attributes[20] = String.valueOf(Double.parseDouble(initial)+add);
			clusterHelper.updateItem(item_attributes, ClusterConstants.getRestaurantId(item_ref_id));
			
			String final_sla = getSLA_fromItemCheckResponse(customer_lat, customer_lng, 
														 	ClusterConstants.item_count, ClusterConstants.sla_extra_time, item_ref_id);
			
			item_attributes[20] = initial;
			clusterHelper.updateItem(item_attributes, ClusterConstants.getRestaurantId(item_ref_id));
			Assert.assertNotEquals(initial_sla, final_sla);
		}
		else
		{
			Assert.fail();
		}
	}
	
	
	
	@Test(dataProvider = "CSTC_50", description="CSTC_50 :- Verify avgPlacementDelay is adding with sla calculation")
	public void avgPlacementDelay_addingInSla(String customer_lat, String customer_lng, String restaurant_refId, String avgPlacementDelay, String[] item_attributes)
	{
		clusterHelper.updateItem(item_attributes, ClusterConstants.getRestaurantId(restaurant_refId));
		
		String initial_sla = getSLA_fromItemCheckResponse(customer_lat, customer_lng, 
														  ClusterConstants.item_count, ClusterConstants.sla_extra_time, restaurant_refId);
		String initial_avgPlacementDelay = item_attributes[16];
		item_attributes[16] = avgPlacementDelay;
		clusterHelper.updateItem(item_attributes, ClusterConstants.getRestaurantId(restaurant_refId));
		
		String final_sla = getSLA_fromItemCheckResponse(customer_lat, customer_lng, 
														ClusterConstants.item_count, ClusterConstants.sla_extra_time, restaurant_refId);
		item_attributes[16] = initial_avgPlacementDelay;
		clusterHelper.updateItem(item_attributes, ClusterConstants.getRestaurantId(restaurant_refId));
		
		Assert.assertEquals(Integer.parseInt(final_sla), (Integer.parseInt(initial_sla) - Integer.parseInt(clusterdp.itemHashMap().get("avgPlacementDelay"))));
	}
	
	
	@Test(dataProvider = "CSTC_51", description="CSTC_51 :- Verify slaRangeWidth is adding with slaMin and showing as slaMax")
	public void slaRangeWidth_addingWith_Sla(String customer_lat, String customer_lng)
	{
		Processor item_check_response = item_check_response(customer_lat, customer_lng, 
															ClusterConstants.currentBF, 
															ClusterConstants.sla_extra_time, item_refid);
		String item_check_response_asText = item_check_response.ResponseValidator.GetBodyAsText();
		
		String min_sla = JsonPath.read(item_check_response_asText, "$.item_list..local_attributes.minSla")
						 .toString().replace("[", "").replace("]", "");
		String max_sla = JsonPath.read(item_check_response_asText, "$.item_list..local_attributes.maxSla")
						 .toString().replace("[", "").replace("]", "");
		
		Assert.assertEquals(Integer.parseInt(max_sla), (Integer.parseInt(min_sla) + Integer.parseInt(ClusterConstants.sla_range_width)));
	}
	
	
	@Test(dataProvider = "CSTC_52", description="CSTC_52 :- Verify sla is getting increased (set itemCountSlaBeefUp) if itemCount increases")
	public void itemCountSlaBeefUp_addingWithSla(String customer_lat, String customer_lng, String item_refId)
	{
		int count = Integer.parseInt(ClusterConstants.item_count);
		String item_count = String.valueOf(++count);
		
		String initialSla = getSLA_fromItemCheckResponse(customer_lat, customer_lng, 
														 ClusterConstants.item_count, ClusterConstants.sla_extra_time, item_refId);
		String finalSla = getSLA_fromItemCheckResponse(customer_lat, customer_lng, item_count,
													   ClusterConstants.sla_extra_time, item_refId);
		
		Assert.assertEquals(Integer.parseInt(finalSla), (Integer.parseInt(initialSla) + Integer.parseInt(ClusterConstants.item_count_sla_beefUp)));
	}
	
	
	@Test(dataProvider="CSTC_53/54/55/56", description="CSTC_53 :- Verify if avgPrepTimeWDP is 0 or null and slaPrepTimeCategoryFlag is true, then prepTimePeak is adding with sla.")
	public void avgPrepTimeWDPZero_prepTimePeakAddingWithSLA(String[] item_attributes, String item_refId, String customer_lat, String customer_lng)
	{
		if(getCurrentDay() && getCurrentHour())
		{
			String initial_avgPrepTimeWDP = item_attributes[9];
			item_attributes[9] = "0";
			clusterHelper.updateItem(item_attributes, ClusterConstants.getRestaurantId(item_refId));
			
			String intial_sla = getSLA_fromItemCheckResponse(customer_lat, customer_lng, 
															 ClusterConstants.item_count, ClusterConstants.sla_extra_time, item_refId);
			int initial_prepTimePeak = Integer.parseInt(item_attributes[7]);
			int add = 10;
			item_attributes[7] = String.valueOf(initial_prepTimePeak + add);
			clusterHelper.updateItem(item_attributes, ClusterConstants.getRestaurantId(item_refId));
			
			String final_sla = getSLA_fromItemCheckResponse(customer_lat, customer_lng, 
															ClusterConstants.item_count, ClusterConstants.sla_extra_time, item_refId);
			item_attributes[9] = initial_avgPrepTimeWDP;
			item_attributes[7] = String.valueOf(initial_prepTimePeak);
			clusterHelper.updateItem(item_attributes, ClusterConstants.getRestaurantId(item_refId));

			Assert.assertEquals(Integer.parseInt(final_sla), (Integer.parseInt(intial_sla)+add));
		}
		else
			System.out.println("*****WDNP and/or Weekend*****");
	}
	
	
	@Test(dataProvider="CSTC_53/54/55/56", description="CSTC_54 :- Verify if avgPrepTimeWDNP is 0 or null and slaPrepTimeCategoryFlag is true, then prepTimePeak is adding with sla.")
	public void avgPrepTimeWDNPZero_prepTimePeakAddingWithSLA(String[] item_attributes, String item_refId, String customer_lat, String customer_lng)
	{
		if(getCurrentDay() && !getCurrentHour())
		{
			String initial_avgPrepTimeWDNP = item_attributes[11];
			item_attributes[11] = "0";
			clusterHelper.updateItem(item_attributes, ClusterConstants.getRestaurantId(item_refId));
			
			String intial_sla = getSLA_fromItemCheckResponse(customer_lat, customer_lng, 
															 ClusterConstants.item_count, ClusterConstants.sla_extra_time, item_refId);
			int initial_prepTimePeak = Integer.parseInt(item_attributes[7]);
			int add = 10;
			item_attributes[7] = String.valueOf(initial_prepTimePeak + add);
			clusterHelper.updateItem(item_attributes, ClusterConstants.getRestaurantId(item_refId));
			
			String final_sla = getSLA_fromItemCheckResponse(customer_lat, customer_lng, 
															ClusterConstants.item_count, ClusterConstants.sla_extra_time, item_refId);
			item_attributes[11] = initial_avgPrepTimeWDNP;
			item_attributes[7] = String.valueOf(initial_prepTimePeak);
			clusterHelper.updateItem(item_attributes, ClusterConstants.getRestaurantId(item_refId));

			Assert.assertEquals(Integer.parseInt(final_sla), (Integer.parseInt(intial_sla)+add));
		}
		else
			System.out.println("*****WDP and/or Weekend*****");
	}
	
	
	@Test(dataProvider="CSTC_53/54/55/56", description="CSTC_55 :- Verify if avgPrepTimeWENP is 0 or null and slaPrepTimeCategoryFlag is true, then prepTimePeak is adding with sla.")
	public void avgPrepTimeWENPZero_prepTimePeakAddingWithSLA(String[] item_attributes, String item_refId, String customer_lat, String customer_lng)
	{
		if(!getCurrentDay() && !getCurrentHour())
		{
			String initial_avgPrepTimeWENP = item_attributes[12];
			item_attributes[12] = "0";
			clusterHelper.updateItem(item_attributes, ClusterConstants.getRestaurantId(item_refId));
			
			String intial_sla = getSLA_fromItemCheckResponse(customer_lat, customer_lng, 
															 ClusterConstants.item_count, ClusterConstants.sla_extra_time, item_refId);
			int initial_prepTimePeak = Integer.parseInt(item_attributes[7]);
			int add = 10;
			item_attributes[7] = String.valueOf(initial_prepTimePeak + add);
			clusterHelper.updateItem(item_attributes, ClusterConstants.getRestaurantId(item_refId));
			
			String final_sla = getSLA_fromItemCheckResponse(customer_lat, customer_lng, 
															ClusterConstants.item_count, ClusterConstants.sla_extra_time, item_refId);
			
			item_attributes[12] = initial_avgPrepTimeWENP;
			item_attributes[7] = String.valueOf(initial_prepTimePeak);
			clusterHelper.updateItem(item_attributes, ClusterConstants.getRestaurantId(item_refId));

			Assert.assertEquals(Integer.parseInt(final_sla), (Integer.parseInt(intial_sla)+add));
		}
		else
			System.out.println("*****WEP and/or WeekDay*****");
	}
	
	
	@Test(dataProvider="CSTC_53/54/55/56", description="CSTC_56 :- Verify if avgPrepTimeWEP is 0 or null and slaPrepTimeCategoryFlag is true, then prepTimePeak is adding with sla.")
	public void avgPrepTimeWEPZero_prepTimePeakAddingWithSLA(String[] item_attributes, String item_refId, String customer_lat, String customer_lng)
	{
		if(!getCurrentDay() && getCurrentHour())
		{
			String initial_avgPrepTimeWDP = item_attributes[10];
			item_attributes[10] = "0";
			clusterHelper.updateItem(item_attributes, ClusterConstants.getRestaurantId(item_refId));
			
			String intial_sla = getSLA_fromItemCheckResponse(customer_lat, customer_lng, 
															 ClusterConstants.item_count, ClusterConstants.sla_extra_time, item_refId);

			int initial_prepTimePeak = Integer.parseInt(item_attributes[7]);
			int add = 10;
			item_attributes[7] = String.valueOf(initial_prepTimePeak + add);
			clusterHelper.updateItem(item_attributes, ClusterConstants.getRestaurantId(item_refId));
			
			String final_sla = getSLA_fromItemCheckResponse(customer_lat, customer_lng, 
															ClusterConstants.item_count, ClusterConstants.sla_extra_time, item_refId);
			
			item_attributes[10] = initial_avgPrepTimeWDP;
			item_attributes[7] = String.valueOf(initial_prepTimePeak);
			clusterHelper.updateItem(item_attributes, ClusterConstants.getRestaurantId(item_refId));

			Assert.assertEquals(Integer.parseInt(final_sla), (Integer.parseInt(intial_sla)+add));
		}
		else
			System.out.println("*****WENP and/or WeekDay*****");
	}
	
	
	@Test(dataProvider="CSTC_57/58", description="CSTC_57 :- Verify if slaPrepTimeCategoryFlag is false, then avgPrepTime is adding with sla.")
	public void slaFlagFalse_avgPrepTime_addingWithSLA(String[] item_payload, String item_refId, String customer_lat, String customer_lng)
	{
		item_payload[26] = "false";
		clusterHelper.updateItem(item_payload, ClusterConstants.getRestaurantId(item_refId));
		String intial_sla = getSLA_fromItemCheckResponse(customer_lat, customer_lng, 
														 ClusterConstants.item_count, ClusterConstants.sla_extra_time, item_refId);
		
		double add = 10.0;
		String initial_avgPrepTime = item_payload[4];
		item_payload[4] = String.valueOf(Double.parseDouble(item_payload[4]) + add);
		clusterHelper.updateItem(item_payload, ClusterConstants.getRestaurantId(item_refId));
		
		String final_sla = getSLA_fromItemCheckResponse(customer_lat, customer_lng, 
														ClusterConstants.item_count, ClusterConstants.sla_extra_time, item_refId);
		item_payload[4] = initial_avgPrepTime;
		item_payload[26] = "true";
		clusterHelper.updateItem(item_payload, ClusterConstants.getRestaurantId(item_refId));
		
		Assert.assertEquals(Double.parseDouble(final_sla), (Double.parseDouble(intial_sla) + add));
	}
	
	
	@Test(dataProvider="CSTC_57/58", description="CSTC_58 :- Verify if slaPrepTimeCategoryFlag is false, then avgPrepTime is 0, then prepTimePeak is adding with sla.")
	public void slaFlagFalse_avgPrepTimeZero_avgPrepTimePeakAddingWithSLA(String[] item_payload, String item_refId, String customer_lat, String customer_lng)
	{
		item_payload[26] = "false";
		String initial_avgPrepTime = item_payload[4];
		item_payload[4] = "0";
		String initial_prepTimePeak = item_payload[7];
		clusterHelper.updateItem(item_payload, ClusterConstants.getRestaurantId(item_refId));
		
		String intial_sla = getSLA_fromItemCheckResponse(customer_lat, customer_lng, 
														 ClusterConstants.item_count, ClusterConstants.sla_extra_time, item_refId);
		double add = 10.0;
		item_payload[7] = String.valueOf(Double.parseDouble(item_payload[7]) + add);

		clusterHelper.updateItem(item_payload, ClusterConstants.getRestaurantId(item_refId));
		
		String final_sla = getSLA_fromItemCheckResponse(customer_lat, customer_lng, 
														ClusterConstants.item_count, ClusterConstants.sla_extra_time, item_refId);
		item_payload[7] = initial_prepTimePeak;
		item_payload[26] = "true";
		item_payload[4] = initial_avgPrepTime;
		clusterHelper.updateItem(item_payload, ClusterConstants.getRestaurantId(item_refId));
		
		Assert.assertEquals(Double.parseDouble(final_sla), (Double.parseDouble(intial_sla) + add));
	}
	
	
	@Test(dataProvider="CSTC_59/60", description="CSTC_59 :- Verify if resturantFirstMile is true, then AvgRestFirstMile is adding as firstMilePredicted in sla calculation.")
	public void restaurantFlagTrue_AvgRestFirstMile_addingWithSLA(String[] item_payload, String item_refId, String customer_lat, String customer_lng)
	{
		clusterHelper.updateItem(item_payload, ClusterConstants.getRestaurantId(item_refId));
		String initial_AvgRestFirstMile = item_payload[15];
		
		Processor item_check_response = item_check_response(customer_lat, customer_lng, 
															ClusterConstants.currentBF, ClusterConstants.sla_extra_time, item_refId);
		String initial_firstMilePredicted = item_check_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.item_list..local_attributes.firstMilePredicted")
											.replace("[", "").replace("]", "");
		
		double add = 10.0;
		item_payload[15] = String.valueOf(Double.parseDouble(item_payload[15]) + add);
		clusterHelper.updateItem(item_payload, ClusterConstants.getRestaurantId(item_refId));
		
		Processor item_checkResponse = item_check_response(customer_lat, customer_lng, 
														   ClusterConstants.currentBF, ClusterConstants.sla_extra_time, item_refId);
		String final_firstMilePredicted = item_checkResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.item_list..local_attributes.firstMilePredicted")
										  .replace("[", "").replace("]", "");
		
		item_payload[15] = initial_AvgRestFirstMile;
		clusterHelper.updateItem(item_payload, ClusterConstants.getRestaurantId(item_refId));
		Assert.assertEquals(Double.parseDouble(final_firstMilePredicted), (Double.parseDouble(initial_firstMilePredicted) + add));
	}
	
	
	@Test(dataProvider="CSTC_59/60", description="CSTC_60 :- Verify if resturantFirstMile is true, then AvgAreaFirstMile is adding as firstMilePredicted in sla calculation.")
	public void restaurantFlagFalse_AvgAreaFirstMile_addingWithSLA(String[] item_payload, String item_refId, String customer_lat, String customer_lng)
	{
		item_payload[27] = "false";
		clusterHelper.updateItem(item_payload, ClusterConstants.getRestaurantId(item_refId));
		double initial_AvgAreaFirstMile = Double.parseDouble(item_payload[14]);
		
		Processor item_check_response = item_check_response(customer_lat, customer_lng, 
										ClusterConstants.currentBF, ClusterConstants.sla_extra_time, item_refId);
		String initial_firstMilePredicted = item_check_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.item_list..local_attributes.firstMilePredicted")
											.replace("[", "").replace("]", "");
		
		double add = 10.0;
		Double add2 = Double.sum(add, Double.parseDouble(item_payload[14]));
		item_payload[14] = Double.toString(add2);
		
		clusterHelper.updateItem(item_payload, ClusterConstants.getRestaurantId(item_refId));
		
		Processor item_checkResponse = item_check_response(customer_lat, customer_lng, 
														   ClusterConstants.currentBF, ClusterConstants.sla_extra_time, item_refId);
		String final_firstMilePredicted = item_checkResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.item_list..local_attributes.firstMilePredicted")
										  .replace("[", "").replace("]", "");
		
		item_payload[14] = String.valueOf(initial_AvgAreaFirstMile);
		item_payload[27] = "true";
		clusterHelper.updateItem(item_payload, ClusterConstants.getRestaurantId(item_refId));
		Double value =  Double.sum(Double.parseDouble(initial_firstMilePredicted), add);
		Assert.assertEquals(Double.parseDouble(final_firstMilePredicted),Double.parseDouble(String.format("%.2f", value)));
		
	}
	
	public void makeSDserviceable()
	{
		LinkedHashMap<String, String> itemMap = selfDelItemAttributes();
		itemMap.put("selfDeliveryOverriden", "0");
		itemMap.put("deliveryPartnerActive", "1");
		itemMap.put("serviceable", "1");
		itemMap.put("partnerId", partnerId);
		
		String[] payload = getSelfDelItemElements(ClusterConstants.selfDelItemLat, ClusterConstants.selfDelItemLng, selfDelRefId, itemMap);
		clusterHelper.updateSelfDeliveryItem(payload, selfDelItemId);
	}
	String partnerId = ClusterConstants.partnerId;
	
	public String createSelfDelItem_andGetId(String item_refId)
	{
		String[] payload = getSelfDelItemElements(ClusterConstants.selfDelItemLat, ClusterConstants.selfDelItemLng, item_refId, selfDelItemAttributes());
		Processor item_create = clusterHelper.createSelfDeliveryItem(payload);
		String item_id = item_create.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.id");
		return item_id;
	}
	
	@Test(dataProvider="CSTC_61-69", description="CSTC_61 :- Verify items are serviceable if \"deliveryPartnerActive\" and \"isServiceable\" both flags are true")
	public void selfDeliveryServiceabileWithTrueFlag(String customerLat, String customerLng)
	{
		LinkedHashMap<String, String> itemMap = selfDelItemAttributes();
		itemMap.put("selfDeliveryOverriden", "0");
		itemMap.put("deliveryPartnerActive", "1");
		itemMap.put("serviceable", "1");
		itemMap.put("partnerId", partnerId);
		
		String[] payload = getSelfDelItemElements(String.valueOf(restaurantLat(selfDelRefId)), String.valueOf(restaurantLng(selfDelRefId)), selfDelRefId, itemMap);
		clusterHelper.updateSelfDeliveryItem(payload, selfDelItemId);
		Validator response = clusterHelper.Item_Check(clusterHelper.getSelfDelItemCheckPayload(customerLat, customerLng, selfDelRefId)).ResponseValidator;
		String response1 = response.GetBodyAsText();
		int responseCode = response.GetResponseCode();
		String itemList = JsonPath.read(response1, "$.item_list").toString();
		String minSla = JsonPath.read(response1, "$.item_list..local_attributes.minSla").toString().replace("[", "").replace("]", "");
		String maxSla = JsonPath.read(response1, "$.item_list..local_attributes.maxSla").toString().replace("[", "").replace("]", "");
		makeSDserviceable();
		
		Assert.assertEquals(responseCode, 200);
		Assert.assertNotEquals(itemList, "[]");
		Assert.assertEquals(minSla, maxSla);
	}
	
	
	@Test(dataProvider="CSTC_61-69", description="CSTC_62 :- Verify items should not be serviceable if \"deliveryPartnerActive\" and \"isServiceable\" both flags are false")
	public void selfDeliveryUnserviceabileWithFalseFlag(String customerLat, String customerLng)
	{
		LinkedHashMap<String, String> itemMap = selfDelItemAttributes();
		itemMap.put("selfDeliveryOverriden", "0");
		itemMap.put("deliveryPartnerActive", "0");
		itemMap.put("serviceable", "0");
		itemMap.put("partnerId", partnerId);
		
		String[] payload = getSelfDelItemElements(String.valueOf(restaurantLat(selfDelRefId)), String.valueOf(restaurantLng(selfDelRefId)), selfDelRefId, itemMap);
		clusterHelper.updateSelfDeliveryItem(payload, selfDelItemId);
		Validator response = clusterHelper.Item_Check(clusterHelper.getSelfDelItemCheckPayload(customerLat, customerLng, selfDelRefId)).ResponseValidator;
		String response1 = response.GetBodyAsText();
		int responseCode = response.GetResponseCode();
		String ignoredItemList = JsonPath.read(response1, "$.ignored_item_list").toString();
		makeSDserviceable();
		
		Assert.assertEquals(responseCode, 200);
		Assert.assertNotEquals(ignoredItemList, "[]");
	}
	
	
	@Test(dataProvider="CSTC_61-69", description="CSTC_63 :- Verify items should not be serviceable if \"deliveryPartnerActive\" is true and \"isServiceable\" is false")
	public void selfDeliveryUnserviceabilityCheck(String customerLat, String customerLng)
	{
		LinkedHashMap<String, String> itemMap = selfDelItemAttributes();
		itemMap.put("selfDeliveryOverriden", "0");
		itemMap.put("deliveryPartnerActive", "1");
		itemMap.put("serviceable", "0");
		itemMap.put("partnerId", partnerId);
		
		String[] payload = getSelfDelItemElements(String.valueOf(restaurantLat(selfDelRefId)), String.valueOf(restaurantLng(selfDelRefId)), selfDelRefId, itemMap);
		clusterHelper.updateSelfDeliveryItem(payload, selfDelItemId);
		Validator response = clusterHelper.Item_Check(clusterHelper.getSelfDelItemCheckPayload(customerLat, customerLng, selfDelRefId)).ResponseValidator;
		String response1 = response.GetBodyAsText();
		int responseCode = response.GetResponseCode();
		String ignoredItemList = JsonPath.read(response1, "$.ignored_item_list").toString();
		makeSDserviceable();
		
		Assert.assertEquals(responseCode, 200);
		Assert.assertNotEquals(ignoredItemList, "[]");
	}
	
	
	@Test(dataProvider="CSTC_61-69", description="CSTC_64 :- Verify items should not be serviceable if \"deliveryPartnerActive\" is false and \"isServiceable\" is true")
	public void selfDeliveryServiceabilityCheck(String customerLat, String customerLng)
	{
		LinkedHashMap<String, String> itemMap = selfDelItemAttributes();
		itemMap.put("selfDeliveryOverriden", "0");
		itemMap.put("deliveryPartnerActive", "0");
		itemMap.put("serviceable", "1");
		itemMap.put("partnerId", partnerId);
		
		String[] payload = getSelfDelItemElements(String.valueOf(restaurantLat(selfDelRefId)), String.valueOf(restaurantLng(selfDelRefId)), selfDelRefId, itemMap);
		clusterHelper.updateSelfDeliveryItem(payload, selfDelItemId);
		Validator response = clusterHelper.Item_Check(clusterHelper.getSelfDelItemCheckPayload(customerLat, customerLng, selfDelRefId)).ResponseValidator;
		String response1 = response.GetBodyAsText();
		int responseCode = response.GetResponseCode();
		String ignoredItemList = JsonPath.read(response1, "$.ignored_item_list").toString();
		makeSDserviceable();
		
		Assert.assertEquals(responseCode, 200);
		Assert.assertNotEquals(ignoredItemList, "[]");
	}
	
	
	@Test(dataProvider="CSTC_61-69", description="CSTC_65 :- Verify self delivery is applicable for a item if \"selfDeliveryOverriden\" flag is false")
	public void selfDeliveryOverridenFalseCheck(String customerLat, String customerLng)
	{
		LinkedHashMap<String, String> itemMap = selfDelItemAttributes();
		itemMap.put("selfDeliveryOverriden", "0");
		itemMap.put("deliveryPartnerActive", "1");
		itemMap.put("serviceable", "1");
		itemMap.put("partnerId", partnerId);
		
		String[] payload = getSelfDelItemElements(String.valueOf(restaurantLat(selfDelRefId)), String.valueOf(restaurantLng(selfDelRefId)), selfDelRefId, itemMap);
		clusterHelper.updateSelfDeliveryItem(payload, selfDelItemId);
		Validator response = clusterHelper.Item_Check(clusterHelper.getSelfDelItemCheckPayload(customerLat, customerLng, selfDelRefId)).ResponseValidator;
		int responseCode = response.GetResponseCode();
		String response1 = response.GetBodyAsText();
		
		String sla = JsonPath.read(response1, "$.item_list..sla").toString().replace("[", "").replace("]", "");
		String minSla = JsonPath.read(response1, "$.item_list..minSla").toString().replace("[", "").replace("]", "");
		String maxSla = JsonPath.read(response1, "$.item_list..maxSla").toString().replace("[", "").replace("]", "");
		String prepTimePredicted = JsonPath.read(response1, "$.item_list..local_attributes.prepTimePred").toString();
		Boolean slaCheck = sla.equals(minSla) && sla.equals(maxSla);
		makeSDserviceable();
		
		Assert.assertEquals(responseCode, 200);
		Assert.assertTrue(slaCheck);
		Assert.assertEquals(prepTimePredicted, "[]");
	}
	
	
	@Test(dataProvider="CSTC_61-69", description="CSTC_66 :- Verify self delivery is not applicable for a item if \"selfDeliveryOverriden\" flag is true")
	public void selfDeliveryOverridenTrueCheck(String customerLat, String customerLng)
	{
		LinkedHashMap<String, String> itemMap = selfDelItemAttributes();
		itemMap.put("selfDeliveryOverriden", "1");
		itemMap.put("deliveryPartnerActive", "1");
		itemMap.put("serviceable", "1");
		itemMap.put("partnerId", partnerId);
		
		String[] payload = getSelfDelItemElements(String.valueOf(restaurantLat(selfDelRefId)), String.valueOf(restaurantLng(selfDelRefId)), selfDelRefId, itemMap);
		clusterHelper.updateSelfDeliveryItem(payload, selfDelItemId);
		Validator response = clusterHelper.Item_Check(clusterHelper.getSelfDelItemCheckPayload(customerLat, customerLng, selfDelRefId)).ResponseValidator;
		int responseCode = response.GetResponseCode();
		String response1 = response.GetBodyAsText();
		String prepTimePredicted = JsonPath.read(response1, "$.item_list..local_attributes.prepTimePred").toString();
		makeSDserviceable();
		
		Assert.assertEquals(responseCode, 200);
		Assert.assertNotEquals(prepTimePredicted, "[]");
	}
	
	
	@Test(dataProvider="CSTC_61-69", description = "CSTC_67_1 / CSTC_67_2 / CSTC_68 / CSTC_69")
	public void selfDeliverySlaCheck(String lat, String lng)
	{
		Map<String, Object> map = SystemConfigProvider.getTemplate("deliveryclusterdb").queryForMap("select attributes from clusters where id='0'");
		makeSDserviceable();
		ObjectMapper mapper = new ObjectMapper();
		String[] startTime, endTime;
		String json = "", sla = null, json1 = "";
		try {
			
				try {
					json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(map.get("attributes")).replace("\\", "").replace("\"{", "{");
				} catch (JsonProcessingException e) {
					e.printStackTrace();
				}
				try
				{
			startTime = JsonPath.read(json, "$.deliveryPartnerTimeBasedSla.12..startTime").toString().replace("[", "").replace("]", "").split(",");
			endTime = JsonPath.read(json, "$.deliveryPartnerTimeBasedSla.12..endTime").toString().replace("[", "").replace("]", "").split(",");
			int[] st = new int[startTime.length];
			int[] et = new int[endTime.length];
			
			for(int i = 0; i < startTime.length; i++)
			{
				st[i] = Integer.parseInt(startTime[i]);
			}
			for(int i = 0; i < endTime.length; i++)
			{
				et[i] = Integer.parseInt(endTime[i]);
			}
			SimpleDateFormat sdf = new SimpleDateFormat("HHmm");
		      int s = Integer.parseInt(sdf.format(new Date()));
		      System.out.println("***** TIME **** " + s);
		      for(int i = 0; i < startTime.length; i++)
		      {
		    	  if(s >= st[i])
		    	  {
		    		  if(s <= et[i])
		    		  {
		    			  sla = JsonPath.read(json, "$.deliveryPartnerTimeBasedSla.12[" + i + "]..sla").toString().replace("[", "").replace("]", "");
		    			 System.out.println("**** TIME SLOT **** " + st[i] + " - " + et[i]);
		    			 break;
		    		  }
		    	  }
		      }
				}
				catch(PathNotFoundException e)
				{}
		      if(sla == null)
		      {
		    	  System.out.println("****** SLA NULL ****** " + sla);
		    	  Map<String, Object> map1 = SystemConfigProvider.getTemplate("deliveryclusterdb").queryForMap("select attributes from items where id='" + selfDelItemId + "'");
		    	  try {
						json1 = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(map1.get("attributes")).replace("\\", "").replace("\"{", "{");
					} catch (JsonProcessingException e) {
						e.printStackTrace();
					}
		    	  try 
		    	  {
		    		  sla = JsonPath.read(json1, "$.fixedSla").toString().replace("[", "").replace("]", "");
		    	  }
		    	  catch (PathNotFoundException e) {
		 			 sla = JsonPath.read(json1, "$.deliveryPartnerFixedSla").toString().replace("[", "").replace("]", "");
		 		}
		    	  
		      }
		      System.out.println("**** sla **** " + sla);
		} catch (PathNotFoundException e) {
			e.printStackTrace();
			 sla = "0";
		}
		System.out.println("****** FINAL SLA ***** " +  sla);
		Validator response = clusterHelper.Item_Check(clusterHelper.getSelfDelItemCheckPayload(lat, lng, selfDelRefId)).ResponseValidator;
		String response1 = response.GetBodyAsText();
		String sla1 = JsonPath.read(response1, "$.item_list..sla").toString().replace("[", "").replace("]", "");
		Assert.assertEquals(sla, sla1);
	}
	
	
	/*@Test(dataProvider="CSTC_61-69", description="CSTC_67 :- Verify for self delivery \"fixedSla\" is reflected as real time sla")
	public void selfDeliveryFixedSlaCheck(String customerLat, String customerLng)
	{
		LinkedHashMap<String, String> itemMap = selfDelItemAttributes();
		itemMap.put("selfDeliveryOverriden", "0");
		itemMap.put("deliveryPartnerActive", "1");
		itemMap.put("serviceable", "1");
		itemMap.put("partnerId", partnerId);
		itemMap.put("fixedSla", ClusterConstants.fixedSla);
		
		String fixedSla = itemMap.get("fixedSla");
		if(fixedSla.equals("0") || fixedSla == null)
			itemMap.put("fixedSla", ClusterConstants.fixedSla);
		String[] payload = getSelfDelItemElements(String.valueOf(restaurantLat(selfDelRefId)), String.valueOf(restaurantLng(selfDelRefId)), selfDelRefId, itemMap);
		clusterHelper.updateSelfDeliveryItem(payload, selfDelItemId);
		Validator response = clusterHelper.Item_Check(clusterHelper.getSelfDelItemCheckPayload(customerLat, customerLng, selfDelRefId)).ResponseValidator;
		int responseCode = response.GetResponseCode();
		String response1 = response.GetBodyAsText();
		String sla = JsonPath.read(response1, "$.item_list..sla").toString().replace("[", "").replace("]", "");
		String slaCheck = ClusterConstants.fixedSla;
		makeSDserviceable();
		
		Assert.assertEquals(responseCode, 200);
		Assert.assertEquals(sla, slaCheck);
	}
	
	
	@Test(dataProvider="CSTC_61-69", description="CSTC_68 :- Verify for self delivery if \"fixedSla\" is NULL then \"deliveryPartnerFixedSla\" will be reflected as real time sla")
	public void selfDeliveryPartnerFixedSlaCheck(String customerLat, String customerLng)
	{
		LinkedHashMap<String, String> itemMap = selfDelItemAttributes();
		itemMap.put("selfDeliveryOverriden", "0");
		itemMap.put("deliveryPartnerActive", "1");
		itemMap.put("serviceable", "1");
		itemMap.put("partnerId", partnerId);
		
		selfDelItemAttributes().put("selfDeliveryOverriden", "0");
		selfDelItemAttributes().put("deliveryPartnerActive", "1");
		selfDelItemAttributes().put("serviceable", "1");
		String fixedSla = selfDelItemAttributes().get("fixedSla");
		if(!fixedSla.equals("0") || fixedSla != null)
		{
			itemMap.put("fixedSla", "0");
			if(itemMap.get("deliveryPartnerFixedSla").equals("0") ||itemMap.get("deliveryPartnerFixedSla") == null)
				itemMap.put("deliveryPartnerFixedSla", ClusterConstants.deliveryPartnerFixedSla);
		}
		String[] payload = getSelfDelItemElements(String.valueOf(restaurantLat(selfDelRefId)), String.valueOf(restaurantLng(selfDelRefId)), selfDelRefId, itemMap);
		clusterHelper.updateSelfDeliveryItem(payload, selfDelItemId);
		Validator response = clusterHelper.Item_Check(clusterHelper.getSelfDelItemCheckPayload(customerLat, customerLng, selfDelRefId)).ResponseValidator;
		int responseCode = response.GetResponseCode();
		String response1 = response.GetBodyAsText();
		String sla = JsonPath.read(response1, "$.item_list..sla").toString().replace("[", "").replace("]", "");
		String slaCheck = ClusterConstants.deliveryPartnerFixedSla;
		makeSDserviceable();
		
		Assert.assertEquals(responseCode, 200);
		Assert.assertEquals(sla, slaCheck);
	}
	
	
	@Test(dataProvider="CSTC_61-69", description="CSTC_69 :- Verify for self delivery if \"fixedSla\" and \"deliveryPartnerFixedSla\"both are NULL then real time sla will be reflected as zero")
	public void selfDeliveryZeroSlaCheck(String customerLat, String customerLng)
	{
		LinkedHashMap<String, String> itemMap = selfDelItemAttributes();
		itemMap.put("selfDeliveryOverriden", "0");
		itemMap.put("deliveryPartnerActive", "1");
		itemMap.put("serviceable", "1");
		itemMap.put("partnerId", partnerId);
		
		String fixedSla = itemMap.get("fixedSla");
		if(!fixedSla.equals("0") || fixedSla != null)
		{
			itemMap.put("fixedSla", "0");
			if(!itemMap.get("deliveryPartnerFixedSla").equals("0") || itemMap.get("deliveryPartnerFixedSla") != null)
				itemMap.put("deliveryPartnerFixedSla", "0");
		}
		String[] payload = getSelfDelItemElements(String.valueOf(restaurantLat(selfDelRefId)), String.valueOf(restaurantLng(selfDelRefId)), selfDelRefId, itemMap);
		clusterHelper.updateSelfDeliveryItem(payload, selfDelItemId);
		Validator response = clusterHelper.Item_Check(clusterHelper.getSelfDelItemCheckPayload(customerLat, customerLng, selfDelRefId)).ResponseValidator;
		int responseCode = response.GetResponseCode();
		String response1 = response.GetBodyAsText();
		String sla = JsonPath.read(response1, "$.item_list..sla").toString().replace("[", "").replace("]", "");
		makeSDserviceable();
		
		Assert.assertEquals(responseCode, 200);
		Assert.assertEquals(sla, "0");
	}*/
	
	
	@Test(dataProvider="CSTC_70-71_2", description = "CSTC_70 :- Verify for self delivery \"slaRangeWidth\" won't be adding with maxSla")
	public void slaRangeWidthNotAddingWithSla(String customerLat, String customerLng)
	{
		LinkedHashMap<String, String> itemMap = selfDelItemAttributes();
		itemMap.put("selfDeliveryOverriden", "0");
		itemMap.put("deliveryPartnerActive", "1");
		itemMap.put("serviceable", "1");
		itemMap.put("partnerId", partnerId);
		
		String[] payload = getSelfDelItemElements(String.valueOf(restaurantLat(selfDelRefId)), String.valueOf(restaurantLng(selfDelRefId)), selfDelRefId, itemMap);
		clusterHelper.updateSelfDeliveryItem(payload, selfDelItemId);
		Validator response = clusterHelper.Item_Check(clusterHelper.getSelfDelItemCheckPayload(customerLat, customerLng, selfDelRefId)).ResponseValidator;
		int responseCode = response.GetResponseCode();
		String response1 = response.GetBodyAsText();
		String minSla = JsonPath.read(response1, "$.item_list..local_attributes.minSla").toString().replace("[", "").replace("]", "");
		String maxSla = JsonPath.read(response1, "$.item_list..local_attributes.maxSla").toString().replace("[", "").replace("]", "");
		makeSDserviceable();
		
		Assert.assertEquals(responseCode, 200);
		Assert.assertEquals(minSla, maxSla);
	}
	
	
	@Test(dataProvider="CSTC_70-71_2", description = "CSTC_71 :- Verify if \"partnerId\" exists under item attributes and is equivalent to one of the selfDeliveryPartnersId's defined in global cluster, then only that item is applicable for self delivery")
	public void partnerIdCheck(String customerLat, String customerLng)
	{
		LinkedHashMap<String, String> itemMap = selfDelItemAttributes();
		itemMap.put("selfDeliveryOverriden", "0");
		itemMap.put("deliveryPartnerActive", "1");
		itemMap.put("serviceable", "1");
		itemMap.put("partnerId", partnerId);
		
		String[] payload = getSelfDelItemElements(String.valueOf(restaurantLat(selfDelRefId)), String.valueOf(restaurantLng(selfDelRefId)), selfDelRefId, itemMap);
		clusterHelper.updateSelfDeliveryItem(payload, selfDelItemId);
		Validator response = clusterHelper.Item_Check(clusterHelper.getSelfDelItemCheckPayload(customerLat, customerLng, selfDelRefId)).ResponseValidator;
		int responseCode = response.GetResponseCode();
		String response1 = response.GetBodyAsText();
		String prepTimePredictedBefore = JsonPath.read(response1, "$.item_list..local_attributes.prepTimePred").toString();
		
		itemMap.put("partnerId", "0");
		
		String[] payload1 = getSelfDelItemElements(String.valueOf(restaurantLat(selfDelRefId)), String.valueOf(restaurantLng(selfDelRefId)), selfDelRefId, itemMap);
		clusterHelper.updateSelfDeliveryItem(payload1, selfDelItemId);
		String response2 = clusterHelper.Item_Check(clusterHelper.getSelfDelItemCheckPayload(customerLat, customerLng, selfDelRefId)).ResponseValidator.GetBodyAsText();
		String prepTimePredictedAfter = JsonPath.read(response2, "$.item_list..local_attributes.prepTimePred").toString();
		makeSDserviceable();
		
		Assert.assertEquals(responseCode, 200);
		Assert.assertEquals(prepTimePredictedBefore, "[]");
		Assert.assertNotEquals(prepTimePredictedAfter, "[]");
	}
	
	
	@Test(dataProvider="CSTC_70-71_2", description = "CSTC_71_1 :- Verify if \"partnerId\" does not exist under item attributes then that item is not applicable for self delivery")
	public void partnerIdSelfDeliveryCheck(String customerLat, String customerLng)
	{
		LinkedHashMap<String, String> itemMap = selfDelItemAttributes();
		itemMap.put("selfDeliveryOverriden", "0");
		itemMap.put("deliveryPartnerActive", "1");
		itemMap.put("serviceable", "1");
		itemMap.put("partnerId", partnerId);
		
		String[] payload = getSelfDelItemElements(ClusterConstants.selfDelItemLat, ClusterConstants.selfDelItemLng, selfDelRefId, itemMap);
		clusterHelper.updateSelfDeliveryItem(payload, selfDelItemId);
		Validator response = clusterHelper.Item_Check(clusterHelper.getSelfDelItemCheckPayload(customerLat, customerLng, selfDelRefId)).ResponseValidator;
		int responseCode = response.GetResponseCode();
		String response1 = response.GetBodyAsText();
		String prepTimePredictedBefore = JsonPath.read(response1, "$.item_list..local_attributes.prepTimePred").toString();
		
		itemMap.remove("partnerId");
		String[] payload1 = getSelfDelItemElements(ClusterConstants.selfDelItemLat, ClusterConstants.selfDelItemLng, selfDelRefId, itemMap);
		clusterHelper.updateSelfDeliveryItemWithoutPartnerId(payload1, selfDelItemId);
		String response2 = clusterHelper.Item_Check(clusterHelper.getSelfDelItemCheckPayload(customerLat, customerLng, selfDelRefId)).ResponseValidator.GetBodyAsText();
		String prepTimePredictedAfter = JsonPath.read(response2, "$.item_list..local_attributes.prepTimePred").toString();
		makeSDserviceable();
		
		Assert.assertEquals(responseCode, 200);
		Assert.assertEquals(prepTimePredictedBefore, "[]");
		Assert.assertNotEquals(prepTimePredictedAfter, "[]");
	}
	
	
	@Test(dataProvider="CSTC_70-71_2", description="CSTC_71_2 :- Verify if a Restaurant is self delivery, is serviceable and there are no enabled connection groups, then empty response will be shown")
	public void selfDeliveryCGCheck(String customerLat, String customerLng)
	{
		LinkedHashMap<String, String> itemMap = selfDelItemAttributes();
		itemMap.put("selfDeliveryOverriden", "0");
		itemMap.put("deliveryPartnerActive", "1");
		itemMap.put("serviceable", "1");
		itemMap.put("partnerId", partnerId);
		
		String[] payload = getSelfDelItemElements(String.valueOf(restaurantLat(selfDelRefId)), String.valueOf(restaurantLng(selfDelRefId)), selfDelRefId, itemMap);
		clusterHelper.updateSelfDeliveryItem(payload, selfDelItemId);
		Validator response = clusterHelper.Item_Check(clusterHelper.getSelfDelItemCheckPayload(customerLat, customerLng, selfDelRefId)).ResponseValidator;
		String response1 = response.GetBodyAsText();
		int responseCode = response.GetResponseCode();
		String itemListBefore = JsonPath.read(response1, "$.item_list").toString();
		
		clusterHelper.updateQuery("connection_groups", "enabled", "0", "id", cg_id_Test);
		String response2 = clusterHelper.Item_Check(clusterHelper.getSelfDelItemCheckPayload(customerLat, customerLng, selfDelRefId)).ResponseValidator.GetBodyAsText();
		String itemListAfter = JsonPath.read(response2, "$.item_list").toString();
		clusterHelper.updateQuery("connection_groups", "enabled", "1", "id", cg_id_Test);
		makeSDserviceable();
		
		Assert.assertEquals(responseCode, 200);
		Assert.assertNotEquals(itemListBefore, "[]");
		Assert.assertEquals(itemListAfter, "[]");
	}

	
	
	@Test(dataProvider = "CSTC_72", description="CSTC_72 :- Assume there are 3 overlapping clusters, from the outermost and innermost cluster items are serviceable but the mid one is a blackzone. Item should be serviceable for innermost and outermost cluster, but should be unserviceable for mid cluster(blackzone)")
	public void overlapping_cluster_withMidClusterAs_Blackzone(String[] inner_cluster_creation_payload, String[] mid_cluster_creation_payload, 
															   String[] outer_cluster_creation_payload, String restaurant_lat, String restaurant_lng, 
															   String item_refId, String item_areadId, String item_zoneId, 
															   String last_mile, String max_sla, String inner_cluster_customer_lat, String inner_cluster_customer_lng, 
															   String mid_cluster_customer_lat, String mid_cluster_customer_lng, String outer_cluster_customer_lat, 
															   String outer_cluster_customer_lng) 
	{
		if(clusterHelper.ifTupleExistsInDB("clusters", "name", ClusterConstants.overlappingClusterNames[0]) || clusterHelper.ifTupleExistsInDB("clusters", "name", ClusterConstants.overlappingClusterNames[1]) || clusterHelper.ifTupleExistsInDB("clusters", "name", ClusterConstants.overlappingClusterNames[2]) || clusterHelper.ifTupleExistsInDB("connection_groups", "name", ClusterConstants.overlappingCG))
		{
			List<Map<String, Object>> clusterIds = SystemConfigProvider.getTemplate("deliveryclusterdb").queryForList("select id from clusters where name in ('" + ClusterConstants.overlappingClusterNames[0] + "', '" + ClusterConstants.overlappingClusterNames[1] + "', '" + ClusterConstants.overlappingClusterNames[2] + "')");
			String[] ids = new String[clusterIds.size()];
			for (int i = 0; i < clusterIds.size(); i++) {
				ids[i] = clusterIds.get(i).get("id").toString();
			}
			
			if(ids != null)
				deleteMultipleTuplesQuery("cluster_items", "cluster_id", ids);
			
			deleteMultipleTuplesQuery("clusters", "id", ids);
			
			try
			{
				String cgId = SystemConfigProvider.getTemplate("deliveryclusterdb").queryForMap("select id from connection_groups where name='" + ClusterConstants.overlappingCG + "'").get("id").toString();
				if(cgId != null)
					clusterHelper.deleteQuery("connections", "group_id", cgId);

				clusterHelper.deleteQuery("connection_groups", "id", cgId);
			}
			catch (EmptyResultDataAccessException e) {}
		}
		String inner_cluster_id = createCluster_getId(inner_cluster_creation_payload);
		String mid_cluster_id = createCluster_getId(mid_cluster_creation_payload);
		String outer_cluster_id = createCluster_getId(outer_cluster_creation_payload);
		
		String item_id = createItem_andGetId(restaurant_lat, restaurant_lng, item_refId, item_areadId, item_zoneId);
		clusterHelper.addItem_toCluster(ClusterConstants.cluster_id, item_id);
		
		clusterHelper.updateQuery("connection_groups", "enabled", "0", "id", cg_id_Test);
		String connection_group_id = createcg_andGetId(ClusterConstants.overlappingCG);
		clusterHelper.updateQuery("connections", "enabled", "0", "group_id", cg_id_Test);
		
		String innerCluster_inclusionConnection_id = createInclusionConnection_getId(connection_group_id, inner_cluster_id, last_mile, max_sla);
				
		String innerCluster_itemList = fetchItemList_fromItem_Check_response(inner_cluster_customer_lat, inner_cluster_customer_lng, item_refId);
		clusterHelper.updateQuery("connections", "enabled", "0", "group_id", connection_group_id);
		String outerCluster_inclusionConnection_id = createInclusionConnection_getId(connection_group_id, outer_cluster_id, last_mile, max_sla);
		String outerCluster_itemList = null;
		
		if(!innerCluster_itemList.equals("[]"))
		{
			outerCluster_itemList = fetchItemList_fromItem_Check_response(outer_cluster_customer_lat,
					outer_cluster_customer_lng, item_refId);
		}
		clusterHelper.updateQuery("connections", "enabled", "0", "group_id", connection_group_id);
		String midCluster_exclusionConnection_id = createExclusionConnection_getId(mid_cluster_id, connection_group_id);
		
		String midCluster_itemList = fetchItemList_fromItem_Check_response(mid_cluster_customer_lat, mid_cluster_customer_lng, item_refId);
		
		deleteMultipleTuplesQuery("clusters", "id", new String[] {inner_cluster_id, mid_cluster_id, outer_cluster_id});
		clusterHelper.deleteQuery("connection_groups", "id", connection_group_id);
		deleteMultipleTuplesQuery("connections", "id", new String[] {innerCluster_inclusionConnection_id, outerCluster_inclusionConnection_id, 
																	 midCluster_exclusionConnection_id});
		clusterHelper.deleteQuery("items", "id", item_id);
		clusterHelper.deleteQuery("cluster_items", "item_id", item_id);
		clusterHelper.updateQuery("connections", "enabled", "1", "id", connection_id_Test);
		clusterHelper.updateQuery("connection_groups", "enabled", "1", "id", cg_id_Test);
		
		Assert.assertNotEquals(innerCluster_itemList, "[]");
		Assert.assertNotEquals(outerCluster_itemList, "[]");
		Assert.assertEquals(midCluster_itemList, "[]");
	}
	
	
	@Test(dataProvider="CSTC_73", description="CSTC_73 :- Verify a particular restaurent cluster is serviceable from a particular customer cluster")
	public void itemServiceable_fromCluster_toCluster(String[] restaurant_cluster_payload, String[] customer_cluster_payload, String item_lat, String item_lng, 
													  String item_refId, String customer_lat, String customer_lng, String last_mile)
	{
		if(clusterHelper.ifTupleExistsInDB("clusters", "name", ClusterConstants.serviceabilityCheckClusterNames[0]) || clusterHelper.ifTupleExistsInDB("clusters", "name", ClusterConstants.serviceabilityCheckClusterNames[1]) || clusterHelper.ifTupleExistsInDB("connection_groups", "name", ClusterConstants.serviceabilityCGName))
		{
			List<Map<String, Object>> clusterIds = SystemConfigProvider.getTemplate("deliveryclusterdb").queryForList("select id from clusters where name in ('" + ClusterConstants.serviceabilityCheckClusterNames[0] + "', '" + ClusterConstants.serviceabilityCheckClusterNames[1] + "')");
			String[] ids = new String[clusterIds.size()];
			for (int i = 0; i < clusterIds.size(); i++) {
				ids[i] = clusterIds.get(i).get("id").toString();
			}
			
			if(ids != null)
				deleteMultipleTuplesQuery("cluster_items", "cluster_id", ids);
			
			deleteMultipleTuplesQuery("clusters", "name", ids);
			try
			{
				String cgId = SystemConfigProvider.getTemplate("deliveryclusterdb").queryForMap("select id from connection_groups where name='" + ClusterConstants.serviceabilityCGName + "'").get("id").toString();
				if(cgId != null)
					clusterHelper.deleteQuery("connections", "group_id", cgId);

				clusterHelper.deleteQuery("connection_groups", "id", cgId);
			}
			catch (EmptyResultDataAccessException e) {}
		}
		String restaurant_cluster_id = createCluster_getId(restaurant_cluster_payload);
		String customer_cluster_id = createCluster_getId(customer_cluster_payload);
		String item_id = createItem_andGetId(item_lat, item_lng, item_refId);
		
		clusterHelper.updateQuery("connection_groups", "enabled", "0", "id", cg_id_Test);
		String cg_id = createcg_andGetId(ClusterConstants.serviceabilityCGName);
		clusterHelper.updateQuery("connections", "enabled", "0", "id", connection_id_Test);
		String connection_id = createConnection_andGetId(cg_id, customer_cluster_id, restaurant_cluster_id, last_mile);
		clusterHelper.addItem_toCluster(restaurant_cluster_id, item_id);
		
		String item_list = fetchItemList_fromItem_Check_response(customer_lat, customer_lng, item_refId);
		
		deleteMultipleTuplesQuery("clusters", "id", new String[] {restaurant_cluster_id, customer_cluster_id});
		clusterHelper.deleteQuery("items", "id", item_id);
		clusterHelper.deleteQuery("cluster_items", "item_id", item_id);
		clusterHelper.deleteQuery("connections", "id", connection_id);
		clusterHelper.deleteQuery("connection_groups", "id", cg_id);
		clusterHelper.updateQuery("connections", "enabled", "1", "id", connection_id_Test);
		clusterHelper.updateQuery("connection_groups", "enabled", "1", "id", cg_id_Test);
		
		Assert.assertNotEquals(item_list, "[]");
	}
	
	
}

