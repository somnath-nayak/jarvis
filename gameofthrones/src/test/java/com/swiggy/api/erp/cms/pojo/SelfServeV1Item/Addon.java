package com.swiggy.api.erp.cms.pojo.SelfServeV1Item;

public class Addon {
	 private String restaurant_id;

	    private Gst_details gst_details;

	    private String active;

	    private String third_party_id;

	    private String in_stock;

	    private String created_by;

	    private String is_veg;

	    private String default1;

	    private Catalog_attributes catalog_attributes;

	    private String temp_addon_id;

	    private String price;

	    private String[] addon_group_ids;

	    private String name;

	    private String updated_by;

	    private Catalog_multi_value_attributes[] catalog_multi_value_attributes;

	    private String id;

	    private String uniqueId;

	    private String inc_price;

	    private String order;

	    public String getRestaurant_id ()
	    {
	        return restaurant_id;
	    }

	    public void setRestaurant_id (String restaurant_id)
	    {
	        this.restaurant_id = restaurant_id;
	    }

	    public Gst_details getGst_details ()
	    {
	        return gst_details;
	    }

	    public void setGst_details (Gst_details gst_details)
	    {
	        this.gst_details = gst_details;
	    }

	    public String getActive ()
	    {
	        return active;
	    }

	    public void setActive (String active)
	    {
	        this.active = active;
	    }

	    public String getThird_party_id ()
	    {
	        return third_party_id;
	    }

	    public void setThird_party_id (String third_party_id)
	    {
	        this.third_party_id = third_party_id;
	    }

	    public String getIn_stock ()
	    {
	        return in_stock;
	    }

	    public void setIn_stock (String in_stock)
	    {
	        this.in_stock = in_stock;
	    }

	    public String getCreated_by ()
	    {
	        return created_by;
	    }

	    public void setCreated_by (String created_by)
	    {
	        this.created_by = created_by;
	    }

	    public String getIs_veg ()
	    {
	        return is_veg;
	    }

	    public void setIs_veg (String is_veg)
	    {
	        this.is_veg = is_veg;
	    }

	    public String getDefault ()
	    {
	        return default1;
	    }

	    public void setDefault (String default1)
	    {
	        this.default1 = default1;
	    }

	    public Catalog_attributes getCatalog_attributes ()
	    {
	        return catalog_attributes;
	    }

	    public void setCatalog_attributes (Catalog_attributes catalog_attributes)
	    {
	        this.catalog_attributes = catalog_attributes;
	    }

	    public String getTemp_addon_id ()
	    {
	        return temp_addon_id;
	    }

	    public void setTemp_addon_id (String temp_addon_id)
	    {
	        this.temp_addon_id = temp_addon_id;
	    }

	    public String getPrice ()
	    {
	        return price;
	    }

	    public void setPrice (String price)
	    {
	        this.price = price;
	    }

	    public String[] getAddon_group_ids ()
	    {
	        return addon_group_ids;
	    }

	    public void setAddon_group_ids (String[] addon_group_ids)
	    {
	        this.addon_group_ids = addon_group_ids;
	    }

	    public String getName ()
	    {
	        return name;
	    }

	    public void setName (String name)
	    {
	        this.name = name;
	    }

	    public String getUpdated_by ()
	    {
	        return updated_by;
	    }

	    public void setUpdated_by (String updated_by)
	    {
	        this.updated_by = updated_by;
	    }

	    public Catalog_multi_value_attributes[] getCatalog_multi_value_attributes ()
	    {
	        return catalog_multi_value_attributes;
	    }

	    public void setCatalog_multi_value_attributes (Catalog_multi_value_attributes[] catalog_multi_value_attributes)
	    {
	        this.catalog_multi_value_attributes = catalog_multi_value_attributes;
	    }

	    public String getId ()
	    {
	        return id;
	    }

	    public void setId (String id)
	    {
	        this.id = id;
	    }

	    public String getUniqueId ()
	    {
	        return uniqueId;
	    }

	    public void setUniqueId (String uniqueId)
	    {
	        this.uniqueId = uniqueId;
	    }

	    public String getInc_price ()
	    {
	        return inc_price;
	    }

	    public void setInc_price (String inc_price)
	    {
	        this.inc_price = inc_price;
	    }

	    public String getOrder ()
	    {
	        return order;
	    }

	    public void setOrder (String order)
	    {
	        this.order = order;
	    }

	    @Override
	    public String toString()
	    {
	        return "ClassPojo [restaurant_id = "+restaurant_id+", gst_details = "+gst_details+", active = "+active+", third_party_id = "+third_party_id+", in_stock = "+in_stock+", created_by = "+created_by+", is_veg = "+is_veg+", default = "+default1+", catalog_attributes = "+catalog_attributes+", temp_addon_id = "+temp_addon_id+", price = "+price+", addon_group_ids = "+addon_group_ids+", name = "+name+", updated_by = "+updated_by+", catalog_multi_value_attributes = "+catalog_multi_value_attributes+", id = "+id+", uniqueId = "+uniqueId+", inc_price = "+inc_price+", order = "+order+"]";
	    }
	}
				
				