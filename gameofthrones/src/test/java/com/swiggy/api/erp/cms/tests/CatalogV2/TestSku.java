package com.swiggy.api.erp.cms.tests.CatalogV2;

import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.constants.CatalogV2Constants;
import com.swiggy.api.erp.cms.dp.CatalogV2SkuDP;
import com.swiggy.api.erp.cms.helper.CatalogV2Helper;

import framework.gameofthrones.JonSnow.Processor;


/**
 * Created by HemaGovindaraj on 24/12/18.
 */

public class TestSku extends CatalogV2SkuDP {
	CatalogV2Helper cmsCatalogHelper=new CatalogV2Helper();
	 String expectedresponse;
			 
	@Test(dataProvider = "CreateSKU", priority = 0, description = "Creation of SKU")
	public void CreateSKu(String json_payload,String ExpectedskuId,boolean state,String spinId)
			throws Exception {

		SoftAssert softAssert = new SoftAssert();
		Processor processor= cmsCatalogHelper.skuCreationPojo(json_payload);
		String actualResponse=processor.ResponseValidator.GetBodyAsText();
		if(state==true)
		{	
			String actualSkuId =processor.ResponseValidator.GetNodeValue("sku_id").toString().replace("[","").replace("]","").toString();
			softAssert.assertEquals(ExpectedskuId,actualSkuId,"Success");
		}
		else  
		{
			if(spinId!=null)
			{
			  
			  expectedresponse = CatalogV2Constants.ExpectGetSpinResult1+spinId+CatalogV2Constants.ExpectGetSpinResult2;
			  Assert.assertEquals(actualResponse,expectedresponse,"Success");
			}
			  
		}

		

	}

	@Test(dataProvider = "getSkuByIds", priority = 1, description = "Get Sku by SKUID")
	public void getSkuByIds(String storeId,String expectedid,String categoryId,boolean state)
			throws Exception {
		Processor p = cmsCatalogHelper.getSkuByIds(storeId, expectedid, categoryId);
		String res=p.ResponseValidator.GetBodyAsText();
		if(state==true)
		{ 
			String actualSkuId =JsonPath.read(res, "$.[0].sku_id").toString().replace("[","").replace("]","").toString();
			Assert.assertEquals(actualSkuId,expectedid, "Success");
		}

	}

	@Test(dataProvider = "getSkuByStoreId", priority = 2, description = "Get sku by store id")
	public void getSkuByStoreId(String storeId,boolean state)
			throws Exception {
		String response = cmsCatalogHelper.getSkuByStore(storeId).ResponseValidator.GetBodyAsText();
		if(state==true)
		{
			String actualStoreid=JsonPath.read(response, "$.[0].store_id").toString().replace("[","").replace("]","").toString();
			Assert.assertEquals(actualStoreid,storeId, "Success");
		}

	}

	@Test(dataProvider = "disableSkuId", priority = 3, description = "Disable SKU")
	public void disableSkuId(String storeId,String SkuIds,boolean state)
			throws Exception {
		String response = cmsCatalogHelper.disableSku(storeId,SkuIds).ResponseValidator.GetBodyAsText();
		if(state==true)
		{
			String actualStoreid=JsonPath.read(response, "$.[0].store_id").toString().replace("[","").replace("]","").toString();
			Assert.assertEquals(actualStoreid,storeId, "Success");
		}
		else {
			String Expectedresponse =CatalogV2Constants.ExpectGetSkuResult1+SkuIds+CatalogV2Constants.ExpectGetSkuResult2;
			Assert.assertEquals(response,Expectedresponse, "Success");
		}

	}
	@Test(dataProvider = "enableSkuId", priority = 4, description = "Enable SKU")
	public void enableSkuId(String storeId,String SkuIds,boolean state)
			throws Exception {
		String response = cmsCatalogHelper.enableSku(storeId,SkuIds).ResponseValidator.GetBodyAsText();
		if(state==true)
		{
			String actualStoreid=JsonPath.read(response, "$.[0].store_id").toString().replace("[","").replace("]","").toString();
			Assert.assertEquals(actualStoreid,storeId, "Success");
		}
		else {
			String Expectedresponse =CatalogV2Constants.ExpectGetSkuResult1+storeId+CatalogV2Constants.ExpectGetSkuResult2;
			Assert.assertEquals(response,Expectedresponse, "Success");
		}
	}

	@Test(dataProvider = "getSkuBySKuId", priority = 5, description = "Get Sku by SKU ids")
	public void getSkuBySKuId(String storeId,String skuId,boolean state)
			throws Exception {
		String response = cmsCatalogHelper.getSkuById(storeId,skuId).ResponseValidator.GetBodyAsText();
		if(state==true){
			String actualSkuId = JsonPath.read(response, "$.sku_id").toString().replace("[","").replace("]","").toString();
			Assert.assertEquals(actualSkuId,skuId, "Success");
		}
		else {
			String Expectedresponse =CatalogV2Constants.ExpectGetSkuResult1+skuId+CatalogV2Constants.ExpectGetSkuResult2;
			Assert.assertEquals(response,Expectedresponse, "Success");
		}
	}


}
