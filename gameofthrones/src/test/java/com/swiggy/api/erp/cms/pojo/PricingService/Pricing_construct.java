
package com.swiggy.api.erp.cms.pojo.PricingService;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "price_list",
    "attributes"
})
public class Pricing_construct {

    @JsonProperty("price_list")
    private Price_list price_list;
    @JsonProperty("attributes")
    private Attributes attributes;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();



    @JsonProperty("price_list")
    public Price_list getPrice_list() {
        return price_list;
    }

    @JsonProperty("price_list")
    public void setPrice_list(Price_list price_list) {
        this.price_list = price_list;
    }

    public Pricing_construct withPrice_list(Price_list price_list) {
        this.price_list = price_list;
        return this;
    }

    @JsonProperty("attributes")
    public Attributes getAttributes() {
        return attributes;
    }

    @JsonProperty("attributes")
    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }

    public Pricing_construct withAttributes(Attributes attributes) {
        this.attributes = attributes;
        return this;
    }

    public Pricing_construct setDefaultValue(Attributes ab,Price_list pc){
        this.withAttributes(ab).withPrice_list(pc);
        return this;
    }

}
