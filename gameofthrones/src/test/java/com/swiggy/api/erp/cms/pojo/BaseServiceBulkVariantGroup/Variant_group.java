package com.swiggy.api.erp.cms.pojo.BaseServiceBulkVariantGroup;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.erp.cms.pojo.BaseServiceBulkVariantGroup
 **/
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.swiggy.api.erp.cms.helper.BaseServiceHelper;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "active",
        "created_at",
        "created_by",
        "id",
        "item_id",
        "name",
        "order",
        "third_party_id",
        "updated_at",
        "updated_by"
})

public class Variant_group {

    BaseServiceHelper baseServiceHelper = new BaseServiceHelper();

    @JsonProperty("active")
    private Boolean active;
    @JsonProperty("created_at")
    private String created_at;
    @JsonProperty("created_by")
    private String created_by;
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("item_id")
    private Integer item_id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("order")
    private Integer order;
    @JsonProperty("third_party_id")
    private String third_party_id;
    @JsonProperty("updated_at")
    private String updated_at;
    @JsonProperty("updated_by")
    private String updated_by;

    @JsonProperty("active")
    public Boolean getActive() {
        return active;
    }

    @JsonProperty("active")
    public void setActive(Boolean active) {
        this.active = active;
    }

    @JsonProperty("created_at")
    public String getCreated_at() {
        return created_at;
    }

    @JsonProperty("created_at")
    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    @JsonProperty("created_by")
    public String getCreated_by() {
        return created_by;
    }

    @JsonProperty("created_by")
    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("item_id")
    public Integer getItem_id() {
        return item_id;
    }

    @JsonProperty("item_id")
    public void setItem_id(Integer item_id) {
        this.item_id = item_id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("order")
    public Integer getOrder() {
        return order;
    }

    @JsonProperty("order")
    public void setOrder(Integer order) {
        this.order = order;
    }

    @JsonProperty("third_party_id")
    public String getThird_party_id() {
        return third_party_id;
    }

    @JsonProperty("third_party_id")
    public void setThird_party_id(String third_party_id) {
        this.third_party_id = third_party_id;
    }

    @JsonProperty("updated_at")
    public String getUpdated_at() {
        return updated_at;
    }

    @JsonProperty("updated_at")
    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    @JsonProperty("updated_by")
    public String getUpdated_by() {
        return updated_by;
    }

    @JsonProperty("updated_by")
    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    public void setDefaultValues(int restID) {
        String timestamp = baseServiceHelper.getTimeStampRandomised();
        int catID = baseServiceHelper.createCategoryIdAndReturnInt(restID);
        int subcatID = baseServiceHelper.createSubCategoryAndReturnInt(restID,catID);
        int itemID = Integer.parseInt(baseServiceHelper.createItemIdAndReturnString(restID,catID,subcatID));
        if(getActive() == null)
            this.setActive(true);
        if(this.getCreated_at() == null)
            this.setCreated_by("Automation_BaseService_Suite");
        if(this.getCreated_at() == null)
            this.setCreated_at("2018-04-23T08:02:21.654Z");
        if(this.getItem_id() == null)
            this.setItem_id(itemID);
        if(this.getName() == null)
            this.setName("Automation_BaseService_"+timestamp);
        if(this.getOrder() == null)
            this.setOrder(0);
        if(this.getThird_party_id() == null)
            this.setThird_party_id("Automation_"+timestamp);
        if(this.getUpdated_by() == null)
            this.setUpdated_by("Automation_BaseService");
        if(this.getUpdated_at() == null)
            this.setUpdated_at("2018-04-23T08:02:21.654Z");
    }

    public Variant_group build(int restID) {
        setDefaultValues(restID);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("active", active).append("created_at", created_at).append("created_by", created_by).append("id", id).append("item_id", item_id).append("name", name).append("order", order).append("third_party_id", third_party_id).append("updated_at", updated_at).append("updated_by", updated_by).toString();
    }

}
