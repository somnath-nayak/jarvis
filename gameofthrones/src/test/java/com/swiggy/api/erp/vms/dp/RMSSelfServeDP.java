package com.swiggy.api.erp.vms.dp;

import com.swiggy.api.erp.vms.constants.RMSConstants;
import com.swiggy.api.erp.vms.helper.RMSCommonHelper;
import com.swiggy.api.erp.vms.helper.RMSHelper;
import com.swiggy.api.erp.vms.helper.RMSSelfServeHelper;
import com.swiggy.api.erp.vms.tests.RMSSelfServeTest;
import framework.gameofthrones.JonSnow.DateHelper;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.annotations.DataProvider;
import java.util.*;

public class RMSSelfServeDP extends RMSCommonHelper {

    static HashMap<String, String> m = new HashMap<>();

    static String restaurantId = null;
    public static String VENDOR_PASSWORD = null;
    public static String VENDOR_USERNAME = null;
    public static String SSMIT_ticketId = null;
    public static String SSMDI_item_id = null;
    public static String SSMEI_item_Id = null;
    public static String SSMANI_item_Id = "5897902";
    public static String SSMANI_category_id = "254882";
    public static String SSMANI_main_category_id = "254880";
    public static String SSMCT_ticketId = "5404";
    public static String SSMDCT_imageId = "549";

    static {
        try {
            if (getEnv().equalsIgnoreCase("stage1")) {
                VENDOR_PASSWORD = "El3ment@ry";
                VENDOR_USERNAME = "5271";
                restaurantId = "9990";
                SSMDI_item_id = "17495079";
                SSMIT_ticketId ="2171";
                SSMEI_item_Id = "17495079";
            } else if (getEnv().equalsIgnoreCase("stage2")) {
                VENDOR_PASSWORD = "El3ment@ry";
                VENDOR_USERNAME = "9990";
                restaurantId = "9990";
                SSMDI_item_id = "20365383";
                SSMIT_ticketId ="3002";
                SSMEI_item_Id = "20365387";
            } else if (getEnv().equalsIgnoreCase("stage3")) {
                VENDOR_PASSWORD = "El3ment@ry";
                VENDOR_USERNAME = "5271";
                restaurantId = "9990";
                SSMDI_item_id = "17495079";
                SSMIT_ticketId ="2171";
                SSMEI_item_Id = "17495079";
            } else if (getEnv().equalsIgnoreCase("prod")) {
                VENDOR_PASSWORD = "$w199y@zolb";
                VENDOR_USERNAME = "9738948943";
                restaurantId = "9990";
                SSMDI_item_id = "17495079";
                SSMIT_ticketId ="2171";
                SSMEI_item_Id = "17495079";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @DataProvider(name = "self_Serve_Menu_GET_MENU_DP")
    public static Object[][] self_Serve_Menu_GET_MENU_DP() {
        m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
        return new Object[][] {{m.get("accessToken"), restaurantId, RMSConstants.SSMM_statusMessage }};
    }

    @DataProvider(name = "self_Serve_Menu_GET_MENU_DP1")
    public static Object[][] self_Serve_Menu_GET_MENU_DP1() {
        m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
        return new Object[][] {
                { m.get("accessToken"), "null", -3, "Invalid Session" },
                { m.get("accessToken"), "8997687996576786879", -3, "Invalid Session" },
                { m.get("accessToken"), "-1", -3, "Invalid Session" },
                { m.get("accessToken"), "0", -3, "Invalid Session" }
        };
    }

    @DataProvider(name = "self_Serve_Menu_Calculate_Service_DP")
    public static Object[][] self_Serve_Menu_Calculate_Service_DP() {
        m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
        return new Object[][] {{ m.get("accessToken"), RMSConstants.SSMCS_packagingCharges, RMSConstants.SSMCS_price,
                restaurantId, RMSConstants.SSMCS_statusMessage }};
    }

    @DataProvider(name = "self_Serve_Menu_Calculate_Service_DP1")
    public static Object[][] self_Serve_Menu_Calculate_Service_DP1() {
        m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
        return new Object[][] {
                { m.get("accessToken"), "10", "10", "", -1, "Invalid Input" },
                { m.get("accessToken"), "10", "10", "8997687996576786879", -3, "Invalid Session" },
                { m.get("accessToken"), "10", "10", "-1", -3, "Invalid Session" },
                { m.get("accessToken"), "10", "10", "0", -3, "Invalid Session" },
                { m.get("accessToken"), "10", "", "", -1, "Invalid Input" },
                { m.get("accessToken"), "10", "8997687996576786879", "8997687996576786879", -3, "Invalid Session" },
                { m.get("accessToken"), "10", "-1", "-1", -3, "Invalid Session" },
                { m.get("accessToken"), "10", "0", "0", -3, "Invalid Session" },
                { m.get("accessToken"), "", "10", "", -1, "Invalid Input" },
                { m.get("accessToken"), "8997687996576786879", "10", "8997687996576786879", -3, "Invalid Session" },
                { m.get("accessToken"), "-1", "10", "-1", -3, "Invalid Session" },
                { m.get("accessToken"), "0", "10", "0", -3, "Invalid Session" }
        };
    }

    @DataProvider(name = "self_Serve_Menu_Revison_History_DP")
    public static Object[][] self_Serve_Menu_Revison_History_DP() {
        m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
        return new Object[][] {{ m.get("accessToken"), restaurantId, RMSConstants.SSMRH_statusMessage }};
    }

    @DataProvider(name = "self_Serve_Menu_Revison_History_DP1")
    public static Object[][] self_Serve_Menu_Revison_History_DP1() {
        m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
        return new Object[][] {
                { m.get("accessToken"), "", -1, "Invalid Input" },
                { m.get("accessToken"), "8997687996576786879", -3, "Invalid Session" },
                { m.get("accessToken"), "-1", -3, "Invalid Session" },
                { m.get("accessToken"), "0", -3, "Invalid Session" }
        };
    }

    @DataProvider(name = "self_Serve_Menu_Item_Ticket_DP")
    public static Object[][] self_Serve_Menu_Item_Ticket_DP() {
        m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
        return new Object[][] {{ m.get("accessToken"), restaurantId, SSMIT_ticketId, RMSConstants.SSMIT_statusMessage }};
    }

    @DataProvider(name = "self_Serve_Menu_Item_Ticket_DP1")
    public static Object[][] self_Serve_Menu_Item_Ticket_DP1() {
        m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
        return new Object[][] {
                { m.get("accessToken"), "9990", "2176661666", 1, "Item Ticket Fetch failed" },
                { m.get("accessToken"), "9990", "-1", 1, "Item Ticket Fetch failed" },
                { m.get("accessToken"), "9990", "0", 1, "Item Ticket Fetch failed" },
                { m.get("accessToken"), "9990", "null", -1, "Action Failed" },
                { m.get("accessToken"),  "", "2171", -3, "Invalid Session" },
                { m.get("accessToken"), "2176661666", "2171", -3, "Invalid Session" },
                { m.get("accessToken"), "-1", "2171", -3, "Invalid Session" },
                { m.get("accessToken"), "0", "2171", -3, "Invalid Session" },
                { m.get("accessToken"), "null", "null", -3, "Invalid Session" },
                { m.get("accessToken"), "2176661666", "2176661666", -3, "Invalid Session" },
                { m.get("accessToken"), "-1", "-1", -3, "Invalid Session" },
                { m.get("accessToken"), "0", "0", -3, "Invalid Session" }
        };
    }

    @DataProvider(name = "Self_Serve_Menu_add_NEW_TEM_DP")
    public static Object[][] Self_Serve_Menu_add_NEW_TEM_DP() {
        m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
        return new Object[][] { { m.get("accessToken"), RMSConstants.SSMANI_name, RMSConstants.SSMANI_is_veg,
                RMSConstants.SSMANI_packing_charges, RMSConstants.SSMANI_description,
                RMSConstants.SSMANI_item_change_request, RMSConstants.SSMANI_addons_and_variant_change_request,
                restaurantId, SSMANI_item_Id, RMSConstants.SSMANI_markInStock,
                RMSConstants.SSMANI_s3_image_url, RMSConstants.SSMANI_price, SSMANI_category_id,
                SSMANI_main_category_id, RMSConstants.SSMANI_statusMessage }};
    }

    @DataProvider(name = "Self_Serve_Menu_add_NEW_TEM_DP1")
    public static Object[][] Self_Serve_Menu_add_NEW_TEM_DP1() {
        m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
        return new Object[][] {
                { m.get("accessToken"), RMSConstants.SSMANI_name, RMSConstants.SSMANI_is_veg, RMSConstants.SSMANI_packing_charges,
                        RMSConstants.SSMANI_description, RMSConstants.SSMANI_item_change_request,
                        RMSConstants.SSMANI_addons_and_variant_change_request, "0", SSMANI_item_Id,
                        RMSConstants.SSMANI_markInStock, RMSConstants.SSMANI_s3_image_url, RMSConstants.SSMANI_price,
                        SSMANI_category_id, SSMANI_main_category_id, -3, "Invalid Session" },
                { m.get("accessToken"), RMSConstants.SSMANI_name, RMSConstants.SSMANI_is_veg, RMSConstants.SSMANI_packing_charges,
                        RMSConstants.SSMANI_description, RMSConstants.SSMANI_item_change_request,
                        RMSConstants.SSMANI_addons_and_variant_change_request, "-1", SSMANI_item_Id,
                        RMSConstants.SSMANI_markInStock, RMSConstants.SSMANI_s3_image_url, RMSConstants.SSMANI_price,
                        SSMANI_category_id, SSMANI_main_category_id, -3, "Invalid Session" },
                { m.get("accessToken"), RMSConstants.SSMANI_name, RMSConstants.SSMANI_is_veg, RMSConstants.SSMANI_packing_charges,
                        RMSConstants.SSMANI_description, RMSConstants.SSMANI_item_change_request,
                        RMSConstants.SSMANI_addons_and_variant_change_request, "null", SSMANI_item_Id,
                        RMSConstants.SSMANI_markInStock, RMSConstants.SSMANI_s3_image_url, RMSConstants.SSMANI_price,
                        SSMANI_category_id, SSMANI_main_category_id, -3, "Invalid Session" },
                { m.get("accessToken"), RMSConstants.SSMANI_name, RMSConstants.SSMANI_is_veg, RMSConstants.SSMANI_packing_charges,
                        RMSConstants.SSMANI_description, RMSConstants.SSMANI_item_change_request,
                        RMSConstants.SSMANI_addons_and_variant_change_request, "7868768786",
                        SSMANI_item_Id, RMSConstants.SSMANI_markInStock, RMSConstants.SSMANI_s3_image_url,
                        RMSConstants.SSMANI_price, SSMANI_category_id,
                        SSMANI_main_category_id, -3, "Invalid Session" }
        };
    }

    @DataProvider(name = "self_Serve_Menu_Edit_ITEM_DP")
    public static Object[][] self_Serve_Menu_Edit_ITEM_DP() {
        m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
        return new Object[][] { { m.get("accessToken"), SSMEI_item_Id, restaurantId,
                RMSConstants.SSMEI_description, RMSConstants.SSMEI_seller_tier, RMSConstants.SSMEI_statusMessage }};
    }

    @DataProvider(name = "self_Serve_Menu_Edit_ITEM_DP1")
    public static Object[][] self_Serve_Menu_Edit_ITEM_DP1() {
        m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
        return new Object[][] {
                { m.get("accessToken"), SSMEI_item_Id, "12345", RMSConstants.SSMEI_description, RMSConstants.SSMEI_seller_tier,
                        -3, "Invalid Session" },
                { m.get("accessToken"), SSMEI_item_Id, "-1232", RMSConstants.SSMEI_description, RMSConstants.SSMEI_seller_tier,
                        -3, "Invalid Session" },
                { m.get("accessToken"), "8787897989", restaurantId, RMSConstants.SSMEI_description,
                        RMSConstants.SSMEI_seller_tier, 1, "Item edit failed" }
        };
    }

    @DataProvider(name = "self_Serve_Menu_Cancel_Ticket_DP")
    public static Object[][] self_Serve_Menu_Cancel_Ticket_DP() {
        m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
        return new Object[][] {{ m.get("accessToken"), restaurantId, RMSConstants.SSMCT_statusMessage }};
    }

    @DataProvider(name = "self_Serve_Menu_Cancel_Ticket_DP1")
    public static Object[][] self_Serve_Menu_Cancel_Ticket_DP1() {
        m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
        return new Object[][] {
                { m.get("accessToken"), "null", SSMCT_ticketId, -3, "Invalid Session" },
                { m.get("accessToken"), "-1", SSMCT_ticketId, -3, "Invalid Session" },
                { m.get("accessToken"), "0", SSMCT_ticketId, -3, "Invalid Session" },
                { m.get("accessToken"), "78676878", SSMCT_ticketId, -3, "Invalid Session" },
                { m.get("accessToken"), restaurantId, "0", 1, "Cancel ticket failed" },
                { m.get("accessToken"), restaurantId, "0", 1, "Cancel ticket failed" },
                { m.get("accessToken"), restaurantId, "0", 1, "Cancel ticket failed" },
                { m.get("accessToken"), restaurantId, "0", 1, "Cancel ticket failed" },
                { m.get("accessToken"), "8879879", "8879879", -3, "Invalid Session" },
                { m.get("accessToken"), "null", "null", -3, "Invalid Session" },
                { m.get("accessToken"), "-1", "-1", -3, "Invalid Session" },
                { m.get("accessToken"), "0", "0", -3, "Invalid Session" }
        };
    }

    @DataProvider(name = "self_Serve_Menu_delete_Cart_Image_DP")
    public static Object[][] self_Serve_Menu_delete_Cart_Image_DP() {
        m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
        return new Object[][] {{ m.get("accessToken"), restaurantId, SSMDCT_imageId, RMSConstants.SSMDCI_statusMessage }};
    }

    @DataProvider(name = "self_Serve_Menu_delete_Cart_Image_DP1")
    public static Object[][] self_Serve_Menu_delete_Cart_Image_DP1() {
        m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
        return new Object[][] {
                { m.get("accessToken"), "null", SSMDCT_imageId, -3, "Invalid Session" },
                { m.get("accessToken"), "0", SSMDCT_imageId, -3, "Invalid Session" },
                { m.get("accessToken"), "-1", SSMDCT_imageId, -3, "Invalid Session" },
                { m.get("accessToken"), "9879879877", SSMDCT_imageId, -3, "Invalid Session" },
                { m.get("accessToken"), restaurantId, "", -1, "Action Failed" },
                { m.get("accessToken"), "null", "null", -3, "Invalid Session" },
                { m.get("accessToken"), "0", "0", -3, "Invalid Session" },
                { m.get("accessToken"), "-1", "-1", -3, "Invalid Session" },
                { m.get("accessToken"), "9879879877", "9879879877", -3, "Invalid Session" }
        };
    }

    @DataProvider(name = "self_Serve_Menu_Delete_Item_DP")
    public static Object[][] self_Serve_Menu_DeletE_item_DP() {
        m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
        return new Object[][] {{ m.get("accessToken"), restaurantId, SSMDI_item_id, RMSConstants.SSMDI_statusMessage }};
    }

    @DataProvider(name = "self_Serve_Menu_Delete_Item_DP1")
    public static Object[][] self_Serve_Menu_DeletE_item_DP1() {
        m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
        return new Object[][] {
                { m.get("accessToken"), "null", SSMDI_item_id, -3, "Invalid Session" },
                { m.get("accessToken"), "0", SSMDI_item_id, -3, "Invalid Session" },
                { m.get("accessToken"), "-1", SSMDI_item_id, -3, "Invalid Session" },
                { m.get("accessToken"), "2133333", SSMDI_item_id, -3, "Invalid Session" },
                { m.get("accessToken"), restaurantId, "", -1, "Action Failed" },
                { m.get("accessToken"), restaurantId, "0", 1, "Item Delete failed" },
                { m.get("accessToken"), restaurantId, "-1", 1, "Item Delete failed" },
                { m.get("accessToken"), "null", "null", -3, "Invalid Session" },
                { m.get("accessToken"), "0", "0", -3, "Invalid Session" },
                { m.get("accessToken"), "-1", "-1", -3, "Invalid Session" },
                { m.get("accessToken"), "2133333", "2133333", -3, "Invalid Session" }
        };
    }

    @DataProvider(name = "self_Serve_Menu_Submit_Pending_Tickets_DP")
    public static Object[][] self_Serve_Menu_Submit_Pending_Tickets_DP() {
        m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
        return new Object[][] {{m.get("accessToken"), restaurantId, RMSConstants.SSMSPT_statusMessage}};
    }

    @DataProvider(name = "self_Serve_Menu_Submit_Pending_Tickets_DP1")
    public static Object[][] self_Serve_Menu_Submit_Pending_Tickets_DP1() {
        m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
        return new Object[][] {
                { m.get("accessToken"), restaurantId, "null", 1, "Pending Tickets Submission failed" },
                { m.get("accessToken"), "null", "null", -1, "Invalid Input" },
                { m.get("accessToken"), "0", "0", -1, "Invalid Input" },
                { m.get("accessToken"), "-1", "-1", -3, "Invalid Session" },
                { m.get("accessToken"), "67585454", "67585454", -3, "Invalid Session" }
        };
    }

    @DataProvider(name = "self_Serve_Item_Image_Upload_DP")
    public static Object[][] self_Serve_Item_Image_Upload_DP() {
        m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
        return new Object[][] {{m.get("accessToken"), System.getProperty("user.dir") + "/src/test/testResources/VMS/testImage.png", restaurantId,
                "Item Image upload Successfully", "1", "success", "testImage.png"}};
    }

    @DataProvider(name = "self_Serve_Item_Image_Upload_Regression")
    public static Object[][] self_Serve_Item_Image_Upload_Regression() {
        m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
        return new Object[][] {
                {m.get("accessToken"), System.getProperty("user.dir") + "/src/test/testResources/VMS/testImage.png", "",
                "Invalid Session", "0", "0"},
                {m.get("accessToken"), System.getProperty("user.dir") + "/src/test/testResources/VMS/testImage.png", "-1",
                        "Invalid Session", "0", "0"},
                {"Yghsdcqhew=72378", System.getProperty("user.dir") + "/src/test/testResources/VMS/testImage.png", restaurantId,
                        "Invalid Session", "0", "0"},
                {m.get("accessToken"), System.getProperty("user.dir") + "/src/test/testResources/VMS/test.pdf", restaurantId,
                        "Item Image upload Successfully", "0", "improper size"}
        };
    }

    @DataProvider(name = "self_Serve_Item_Catlog_ImageDP")
    public static Object[][] self_Serve_Item_Catlog_ImageDP() {
        m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
        return new Object[][] {{m.get("accessToken"), System.getProperty("user.dir") + "/src/test/testResources/VMS/testImage.png", restaurantId,
                "Item Catalogue Image upload Successfully"}};
    }

    @DataProvider(name = "self_Serve_Item_Catlog_ImageRegression")
    public static Object[][] self_Serve_Item_Catlog_ImageRegression() {
        m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
        return new Object[][] {
                {m.get("accessToken"), System.getProperty("user.dir") + "/src/test/testResources/VMS/testImage.png", "",
                "Invalid Input"},
                {m.get("accessToken"), System.getProperty("user.dir") + "/src/test/testResources/VMS/testImage.png", "-1",
                        "Invalid Session"},
                {"Yvdduin=eedbh", System.getProperty("user.dir") + "/src/test/testResources/VMS/testImage.png", restaurantId,
                        "Invalid Session"}
        };
    }

    @DataProvider(name = "selfServeCategoryOOSInStock")
    public static Object[][] selfServeCategoryOOSInStock() throws Exception {
        List<Integer> subCategoryList = null;
        List<Integer> variantList = null;
        m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
        RMSSelfServeHelper helper = new RMSSelfServeHelper();
        Processor p = helper.fetchItems(m.get("accessToken"), restaurantId);
        HashMap<String, List<Integer>> map = RMSCommonHelper.getInStockCategoryFromFetchItems(p);
        if(map.containsKey("subCategoryList"))
            subCategoryList = map.get("subCategoryList");
        if(map.containsKey("variantList"))
            variantList = map.get("variantList");
        return new Object[][] {
                {m.get("accessToken"), restaurantId, map.get("categoryList").get(0).toString(),
                        subCategoryList, map.get("itemList"), variantList, "Marked category out of stock", "Marked category in stock"}
        };
    }

    @DataProvider(name = "selfServeCategoryOOS")
    public static Object[][] selfServeCategoryOOS() throws Exception {

        m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
        RMSSelfServeHelper helper = new RMSSelfServeHelper();
        DateHelper date = new DateHelper();
        Processor p = helper.fetchItems(m.get("accessToken"), restaurantId);
        HashMap<String, List<Integer>> map = RMSCommonHelper.getInStockCategoryFromFetchItems(p);

        return new Object[][] {
                {m.get("accessToken"), restaurantId, map.get("categoryList").get(0).toString(),
                        date.getDateTimePlusOrMinusCurrentDate_yyyy_mm_dd_hh_mm_ss(2),
                        date.getDateTimePlusOrMinusCurrentDate_yyyy_mm_dd_hh_mm_ss(4), "Marked category out of stock"},
                {m.get("accessToken"), restaurantId, map.get("categoryList").get(0).toString(),
                        date.getDateTimePlusOrMinusCurrentDate_yyyy_mm_dd_hh_mm_ss(2),
                        RMSConstants.startDate(4), "Invalid to time"},
                {"Ybjwdjw=qyu2dh", restaurantId, map.get("categoryList").get(0).toString(),
                        date.getDateTimePlusOrMinusCurrentDate_yyyy_mm_dd_hh_mm_ss(2),
                        date.getDateTimePlusOrMinusCurrentDate_yyyy_mm_dd_hh_mm_ss(4), "Invalid Session"}
        };
    }

    @DataProvider(name = "selfServeCategoryInStockRegression")
    public static Object[][] selfServeCategoryInStockRegression() throws Exception {

        m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
        return new Object[][] {
                {"Yvhwebd=wccbhw", restaurantId, "1234", "Invalid Session"},
                {m.get("accessToken"), restaurantId, "1234", "No Category/SubCategory Found For Id : 1234"}
        };
    }
}
