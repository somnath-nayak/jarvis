package com.swiggy.api.erp.cms.tests.catalogAttribute;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.dp.itemDP;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Tyrion.RabbitMQHelper;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class UpdateItemVerifyRmq extends itemDP {
    Initialize gameofthrones = new Initialize();
    RabbitMQHelper helper = new RabbitMQHelper();
    CMSHelper cmsHelper = new CMSHelper();

    @BeforeClass
    public void purgeRmq() {
        try {
            helper.purgeQueue("oms", "item.nam.change.queue");
        } catch (IOException e) {
        } catch (TimeoutException e) {
        }
    }

    String itemName;
    int itmId;


    @Test(dataProvider = "updateitm",description ="Update Item API" )
    public void updateItem(String comment, String description, String name, String itemId) {
        String response = cmsHelper.updateItem(comment, description, name, itemId).ResponseValidator.GetBodyAsText();
        itemName=JsonPath.read(response,"$.data.name");
        Assert.assertNotNull(itemName, "Item Name is Null");
    }

    @Test(dependsOnMethods = "updateItem",description = "Verify item in the rabbitMQ")
    public void rmqItemNameChange(){
        if (itemName != null) {
            String str = helper.getMessage("oms", "item.nam.change.queue");
            String newNameRmq = JsonPath.read(str, "$.new_item.name");
            itmId = JsonPath.read(str, "$.new_item.id");
            Assert.assertEquals(itemName, newNameRmq);

        }

    }


}
