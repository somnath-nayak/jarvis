package com.swiggy.api.erp.vms.td.pojo;

import java.util.ArrayList;
import java.util.List;

/***
 * 
 * @author ramzi
 *
 */
public class RMSEditTDBuilder {

	private RMSUpdateTDEntry rmsUpdateTDEntity;

	public RMSEditTDBuilder() {
		rmsUpdateTDEntity = new RMSUpdateTDEntry();
	}

	public RMSEditTDBuilder setEnabled(String enabled) {
		rmsUpdateTDEntity.setEnabled(enabled);
		return this;
	}

	public RMSEditTDBuilder setRestaurantIds(List<Integer> restaurantIds) {
		rmsUpdateTDEntity.setRestaurantIds(restaurantIds);
		return this;
	}

	public RMSEditTDBuilder setId(int id) {
		rmsUpdateTDEntity.setId(id);
		return this;
	}

	public RMSEditTDBuilder setItemName(String itemName) {
		rmsUpdateTDEntity.setitemName(itemName);
		return this;

	}

	public RMSEditTDBuilder setValidFrom(Long validFrom) {
		rmsUpdateTDEntity.setValidFrom(validFrom);
		return this;
	}

	public RMSEditTDBuilder setValidTill(Long validTill) {
		rmsUpdateTDEntity.setValidTill(validTill);
		return this;
	}

	public RMSEditTDBuilder setCampaignType(String campaignType) {
		rmsUpdateTDEntity.setCampaignType(campaignType);
		return this;
	}

	public RMSEditTDBuilder setRuleDiscountType(String ruleDiscountType) {
		rmsUpdateTDEntity.setRuleDiscountType(ruleDiscountType);
		return this;
	}

	public RMSEditTDBuilder setCreatedBy(Long createdBy) {
		rmsUpdateTDEntity.setCreatedBy(createdBy);
		return this;
	}

	public RMSEditTDBuilder setUpdatedBy(Long updatedBy) {
		rmsUpdateTDEntity.setUpdatedBy(updatedBy);
		return this;
	}

	public RMSEditTDBuilder setDiscountLevel(String discountLevel) {
		rmsUpdateTDEntity.setDiscountLevel(discountLevel);
		return this;
	}

	public RMSEditTDBuilder setRestaurantList(List<Restaurant> restaurantList) {
		rmsUpdateTDEntity.setRestaurantList(restaurantList);
		return this;
	}

	public RMSEditTDBuilder setSlots(List<Slots> slots) {
		rmsUpdateTDEntity.setSlots(slots);
		return this;
	}

	public RMSEditTDBuilder setFirstOrderRestriction(String firstOrderRestriction) {
		rmsUpdateTDEntity.setFirstOrderRestriction(firstOrderRestriction);
		return this;
	}

	public RMSEditTDBuilder setUserRestriction(String userRestriction) {
		rmsUpdateTDEntity.setUserRestriction(userRestriction);
		return this;
	}

	public RMSEditTDBuilder setRestaurantFirstOrder(String restaurantFirstOrder) {
		rmsUpdateTDEntity.setRestaurantFirstOrder(restaurantFirstOrder);
		return this;
	}

	public RMSEditTDBuilder setRuleDiscount(RuleDiscount ruleDiscount) {
		rmsUpdateTDEntity.setRuleDiscount(ruleDiscount);
		return this;
	}

	public RMSEditTDBuilder setRestaurantHit(Integer restaurantHit) {
		rmsUpdateTDEntity.setRestaurantHit(restaurantHit);
		return this;
	}

	public RMSEditTDBuilder setSwiggyHit(Integer swiggyHit) {
		rmsUpdateTDEntity.setSwiggyHit(swiggyHit);
		return this;
	}

	public RMSEditTDBuilder setTaxesOnDiscountedBill(String taxesOnDiscountedBill) {
		rmsUpdateTDEntity.setTaxesOnDiscountedBill(taxesOnDiscountedBill);

		return this;
	}

	public RMSEditTDBuilder setCommissionOnFullBill(String commissionOnFullBill) {
		rmsUpdateTDEntity.setCommissionOnFullBill(commissionOnFullBill);

		return this;
	}

	public RMSEditTDBuilder setTimeSlotRestriction(String timeSlotRestriction) {
		rmsUpdateTDEntity.setTimeSlotRestriction(timeSlotRestriction);
		return this;
	}

	public RMSEditTDBuilder setDiscountId(String timeSlotRestriction) {
		rmsUpdateTDEntity.setTimeSlotRestriction(timeSlotRestriction);
		return this;
	}

	public RMSUpdateTDEntry build() {
		defaultData();
		return rmsUpdateTDEntity;
	}

	private void defaultData() {
		if (rmsUpdateTDEntity.getEnabled() == null) {
			rmsUpdateTDEntity.setEnabled("true");
		}
		if (rmsUpdateTDEntity.getCampaignType() == null) {
			rmsUpdateTDEntity.setCampaignType("Percentage");
        }

		if (rmsUpdateTDEntity.getCreatedBy() == null) {
			rmsUpdateTDEntity.setCreatedBy(9886379321l);
		}
		if (rmsUpdateTDEntity.getFirstOrderRestriction() == null) {
			rmsUpdateTDEntity.setFirstOrderRestriction("false");
        }
		if (rmsUpdateTDEntity.getRestaurantFirstOrder() == null) {
			rmsUpdateTDEntity.setRestaurantFirstOrder("false");
        }

		if (rmsUpdateTDEntity.getUserRestriction() == null) {
			rmsUpdateTDEntity.setUserRestriction("false");
        }

		if (rmsUpdateTDEntity.getSlots() == null) {
			rmsUpdateTDEntity.setSlots(new ArrayList<>());
		}

		if (rmsUpdateTDEntity.getTimeSlotRestriction() == null) {
			rmsUpdateTDEntity.setTimeSlotRestriction("false");
        }
		if (rmsUpdateTDEntity.getCommissionOnFullBill() == null) {
			rmsUpdateTDEntity.setCommissionOnFullBill("false");
        }
		if (rmsUpdateTDEntity.getTaxesOnDiscountedBill() == null) {
			rmsUpdateTDEntity.setTaxesOnDiscountedBill("false");
		}
		if (rmsUpdateTDEntity.getSwiggyHit() == null) {
			rmsUpdateTDEntity.setSwiggyHit(0);
		}
		if (rmsUpdateTDEntity.getRestaurantHit() == null) {
			rmsUpdateTDEntity.setRestaurantHit(100);
		}
		if (rmsUpdateTDEntity.getitemName() == null) {
			rmsUpdateTDEntity.setitemName("");
		}

	}

}
