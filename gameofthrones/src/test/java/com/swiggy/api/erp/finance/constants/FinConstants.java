package com.swiggy.api.erp.finance.constants;

/**
 * Created by kiran.j on 11/9/17.
 */
public interface FinConstants {
    String undelivered_schema = "/Data/SchemaSet/Json/Finance/undelivered";
    String cancel_schema = "/Data/Payloads/JSON/finance_cancel";
    String submit_schema = "/Data/SchemaSet/Json/Finance/submitrecon";
    String reconhealth = "/Data/SchemaSet/Json/Finance/reconhealth";
    String reconversion = "/Data/SchemaSet/Json/Finance/reconversion";
    String restcurrentpayout = "/Data/SchemaSet/Json/Finance/finance_currentpayout";
    String reconlogout = "/Data/SchemaSet/Json/Finance/finance_logout";
    String quickrecon = "/Data/SchemaSet/Json/Finance/finance_quickrecon";
    String autoassign = "/Data/SchemaSet/Json/Finance/finance_autoassignment";
    String addannexure = "/Data/SchemaSet/Json/Finance/finance_addanexure";
    String restpendingapproval = "/Data/SchemaSet/Json/Finance/finance_restaurantpendingapproval";
    String restidpendingapproval = "/Data/SchemaSet/Json/Finance/finance_restaurantidpendingapproval";
    String createreports = "/Data/SchemaSet/Json/Finance/finance_createreports";
    String deliveredstatus = "delivered";
    String[] food_prepared = {"false","true"};
    String autoreconciled = "AUTORECONCILED";
    String unreconciled = "UNRECONCILED";
    String restaurants[] = new String[]{"7","7","7"};
    String cancelled = "CANCELLED";
    String reconciled = "RECONCILED";
    String packing_charges = "3.0";
    Double packe_charges = 3.0;
    String packaging_reason = "4";
    String item_total = "4";
    String mouquery = "SELECT restaurant_id FROM finance.restaurants where mou_type=";
    String oldmou = "0000";
    String newmou = "0001";
    String finance_db = "financeDB";
    String restid = "restaurant_id";
    int status = 200;
    String invoice_job_check = "select * from recon_invoice_jobs where to_date='";
    String endstring = "'";
    String from_time = "2017-01-01 00:00:00";
    String from_time_formatted = "2017-01-01";
    String user = "test";
    String testemail = "test@test.com";
    String task_id = "165";
    String key = "key";
    String value = "value";
    String countmanualjob = "select count(*) from manual_invoice_jobs";
    String fileurl = "test/test";
    String pageno = "10";
    String pagesize = "0";
    String restaurantId = "270";
    String type = "12";
    String excludelist = "11";
    String cityid = "12";
}
