package com.swiggy.api.erp.cms.tests.Items;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.dp.itemDP;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import org.junit.Assert;
import org.testng.annotations.Test;

public class BuildItemCache extends itemDP {
    CMSHelper cmsHelper=new CMSHelper();
    @Test(dataProvider = "itemcache",description = "Building cache for item")
    public void buildItemCache(String itemId){
        String response=cmsHelper.buildItemCache(itemId).ResponseValidator.GetBodyAsText();
        int status=JsonPath.read(response,"$.statusCode");
     Assert.assertEquals(status,1);
    }

}
