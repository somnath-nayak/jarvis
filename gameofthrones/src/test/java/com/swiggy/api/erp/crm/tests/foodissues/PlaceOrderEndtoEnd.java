package com.swiggy.api.erp.crm.tests.foodissues;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.crm.constants.CRMConstants;
import com.swiggy.api.erp.crm.constants.FlowMapper;
import com.swiggy.api.erp.crm.constants.NodeIDs;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceHelper;
import com.swiggy.api.erp.ff.helper.OMSHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.DBHelper;
import framework.gameofthrones.Tyrion.RedisHelper;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

/**
 * Created by sumit.m on 25/06/18.
 */

public class PlaceOrderEndtoEnd {

    Initialize gameofthrones = new Initialize();
    DBHelper dbHelper = new DBHelper();
    FlowMapper flowMapperTitle = new FlowMapper();
    CheckoutHelper checkoutHelper = new CheckoutHelper();
    DeliveryServiceHelper deliveryServiceHelper = new DeliveryServiceHelper();
    NodeIDs nodeIDs = new NodeIDs();
    OMSHelper omsHelper = new OMSHelper();
    RedisHelper redisHelper = new RedisHelper();
    public String orderId, deliveryStatus, ffStatus;
    public String ff_status, delivery_status;

    public Processor getOrder_response;

    public Processor getOrderDetails(String OrderId)
    {
        String[] orderArray = new String[]{orderId};
        String[] queryparam = orderArray;

        HashMap<String, String> requestheaders_getOrder = new HashMap<String, String>();
        requestheaders_getOrder.put("Content-Type", "application/json");
        GameOfThronesService getOrder = new GameOfThronesService("oms", "getOrder", gameofthrones);
        getOrder_response = new Processor(getOrder, requestheaders_getOrder, null, queryparam);

        return getOrder_response;

    }

    public String getDeliveryStatus(String orderId) {

        getOrderDetails(orderId);

        deliveryStatus = getOrder_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("objects..status.delivery_status");
        System.out.println("order_status is ##############   .. " + deliveryStatus);

        deliveryStatus = deliveryStatus.replace("[\"", "").replace("\"]", "");
        return deliveryStatus;

    }

    public String getOrderId(String OrderId)
    {
        getOrderDetails(OrderId);

        String order_id = getOrder_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("objects..order_id").replace("[\"", "").replace("\"]", "");
        return order_id;
    }

    public String getFFStatus(String orderId)
    {
        getOrderDetails(orderId);

        ffStatus = getOrder_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("objects..status.status");

        ffStatus = ffStatus.replace("[\"", "").replace("[\"", "");

        return ffStatus;
    }

    @Test(priority = 0, description = "Order END to END - Unplaced/CAPRD")
    public void unplacedCAPRD() throws Exception {

        Processor processor = null;
        String status;
        String order_id;

        String userId = CRMConstants.userMobile;
        String userPassword = CRMConstants.userPassword;
        String itemId = CRMConstants.itemId;
        String quantity = CRMConstants.quantity;
        String restId = CRMConstants.restId;

        processor = checkoutHelper.placeOrder(userId, userPassword, itemId, quantity, restId);
        String body = processor.ResponseValidator.GetBodyAsText();

        orderId = JsonPath.read(body, "$.data.order_id").toString();
        System.out.println("orderId" + orderId);

        String deID = CRMConstants.deID;

        omsHelper.verifyOrder(orderId, "001");
        order_id = getOrderId(orderId);
        Assert.assertEquals(order_id, orderId, "verify order in oms, which is placed through checkout");

        deliveryServiceHelper.assignOrder(orderId, deID);
        TimeUnit.SECONDS.sleep(5);
        delivery_status = getDeliveryStatus(orderId);
        Assert.assertEquals(delivery_status, "assigned", "delivery DE Assigned");

        deliveryServiceHelper.zipDialConfirm(orderId);
        TimeUnit.SECONDS.sleep(5);
        delivery_status = getDeliveryStatus(orderId);
        Assert.assertEquals(delivery_status, "confirmed", "delivery DE Confirmed");

        deliveryServiceHelper.zipDialArrived(orderId);
        TimeUnit.SECONDS.sleep(5);
        delivery_status = getDeliveryStatus(orderId);
        Assert.assertEquals(delivery_status, "arrived", "delivery DE Arrived");


        deliveryServiceHelper.zipDialPickedUp(orderId);
        TimeUnit.SECONDS.sleep(5);
        delivery_status = getDeliveryStatus(orderId);
        Assert.assertEquals(delivery_status, "pickedup", "delivery DE Pickedup");


        deliveryServiceHelper.zipDialReached(orderId);
        TimeUnit.SECONDS.sleep(5);
        delivery_status = getDeliveryStatus(orderId);
        Assert.assertEquals(delivery_status, "reached", "delivery DE Reached");


        deliveryServiceHelper.zipDialDelivered(orderId);
        TimeUnit.SECONDS.sleep(5);
        delivery_status = getDeliveryStatus(orderId);
        Assert.assertEquals(delivery_status, "delivered", "delivery order Delivered");
    }

    @Test (priority = 1, description = "Order END to END - Placed/CAPRD")
        public void placedCAPRD() throws Exception {

        Processor processor = null;
        String order_id;

        String userId = CRMConstants.userMobile;
        String userPassword = CRMConstants.userPassword;
        String itemId = CRMConstants.itemId;
        String quantity = CRMConstants.quantity;
        String restId = CRMConstants.restId;

        processor = checkoutHelper.placeOrder(userId, userPassword, itemId, quantity, restId);
        String body = processor.ResponseValidator.GetBodyAsText();

        orderId = JsonPath.read(body, "$.data.order_id").toString();
        System.out.println("orderId" + orderId);

        String deID = CRMConstants.deID;

        omsHelper.verifyOrder(orderId, "001");
        order_id = getOrderId(orderId);
        Assert.assertEquals(order_id, orderId, "verify order in oms, which is placed through checkout");

        omsHelper.changeOrderStatusToPlaced(orderId);
        ff_status = getFFStatus(order_id);
        Assert.assertEquals(ff_status, "placed", "oms Order Placed");

        deliveryServiceHelper.assignOrder(orderId, deID);
        TimeUnit.SECONDS.sleep(5);
        delivery_status = getDeliveryStatus(orderId);
        Assert.assertEquals(delivery_status, "assigned", "delivery DE Assigned");

        deliveryServiceHelper.zipDialConfirm(orderId);
        TimeUnit.SECONDS.sleep(5);
        delivery_status = getDeliveryStatus(orderId);
        Assert.assertEquals(delivery_status, "confirmed", "delivery DE Confirmed");

        deliveryServiceHelper.zipDialArrived(orderId);
        TimeUnit.SECONDS.sleep(5);
        delivery_status = getDeliveryStatus(orderId);
        Assert.assertEquals(delivery_status, "arrived", "delivery DE Arrived");

        deliveryServiceHelper.zipDialPickedUp(orderId);
        TimeUnit.SECONDS.sleep(5);
        delivery_status = getDeliveryStatus(orderId);
        Assert.assertEquals(delivery_status, "pickedup", "delivery DE Pickedup");

        deliveryServiceHelper.zipDialReached(orderId);
        TimeUnit.SECONDS.sleep(5);
        delivery_status = getDeliveryStatus(orderId);
        Assert.assertEquals(delivery_status, "reached", "delivery DE Reached");

        deliveryServiceHelper.zipDialDelivered(orderId);
        TimeUnit.SECONDS.sleep(5);
        delivery_status = getDeliveryStatus(orderId);
        Assert.assertEquals(delivery_status, "delivered", "delivery order Delivered");
    }
}

