package com.swiggy.api.erp.delivery.dp;

import com.swiggy.api.erp.delivery.helper.*;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.annotations.DataProvider;

import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;

import framework.gameofthrones.JonSnow.Processor;

public class CAPRDFromFFDataProvider {
	POPHelper popHelper = new POPHelper();
	CAPRDflowsDataProvider caprd=new CAPRDflowsDataProvider();
	DeliveryServiceHelper helpdel = new DeliveryServiceHelper();
	AutoassignHelper hepauto=new AutoassignHelper();
	DeliveryHelperMethods delmeth=new DeliveryHelperMethods();
	DeliveryControllerHelper delcon=new DeliveryControllerHelper();
	DeliveryDataHelper deliveryDataHelper= new DeliveryDataHelper();
	String restaurantId;
	String restaurantLat;
	String restaurantLon;

	public CAPRDFromFFDataProvider(String restaurantId,String restaurantLat, String restaurantLon)
	{
		this.restaurantId=restaurantId;
		this.restaurantLat=restaurantLat;
		this.restaurantLon=restaurantLon;
	}
	
/*	public static String createOrder() throws Exception {
		CheckoutHelper helperc = new CheckoutHelper();
		Object[][] data = cartItems();
		Processor p = helperc.placeOrder(DeliveryConstant.mobile,
				DeliveryConstant.password, data[0][0].toString(),
				data[0][1].toString(), data[0][2].toString());
		return order_id = p.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.order_id");
	}*/

	public static Object[][] cartItems() {

		return new Object[][] { { "8805487", "2", "4977" } };

	}
	@DataProvider(name = "QE-334")
	public Object[][] CAPRDDP334() throws Exception {
		String order_id=deliveryDataHelper.getOrderIdForCAPRDFF(restaurantId,restaurantLat,restaurantLon);
		//order_id=createOrder();
		return new Object[][] {
			
		{order_id,DeliveryConstant.MayurDE,"1.8.2.1"}

		};
	}

	@DataProvider(name = "QE-335")
	public Object[][] CAPRDDP335() throws Exception {
		String order_id=deliveryDataHelper.getOrderIdForCAPRDFF(restaurantId,restaurantLat,restaurantLon);
		//order_id=createOrder();
		return new Object[][] {
			
		{order_id,DeliveryConstant.MayurDE,"1.8.2.1"}

		};
	}
	@DataProvider(name = "QE-336")
	public Object[][] CAPRDDP336() throws Exception {
		String order_id=deliveryDataHelper.getOrderIdForCAPRDFF(restaurantId,restaurantLat,restaurantLon);
		//order_id=createOrder();
		return new Object[][] {
		{order_id,"1.8.2.1"}
		};
	}
	@DataProvider(name = "QE-337")
	public Object[][] CAPRDDP337() throws Exception {
		String order_id=deliveryDataHelper.getOrderIdForCAPRDFF(restaurantId,restaurantLat,restaurantLon);
		//order_id=createOrder();
		return new Object[][] {
		{order_id,DeliveryConstant.MayurDE,"1.8.2.1"}
		};
	}
	
	@DataProvider(name = "QE-338")
	public Object[][] CAPRDDP338() throws Exception {
		String order_id=deliveryDataHelper.getOrderIdForCAPRDFF(restaurantId,restaurantLat,restaurantLon);
		//order_id=createOrder();
		//Thread.sleep(15000);
		hepauto.delogin(DeliveryConstant.MayurDE, "1.8.2.1");
		String de_id= DeliveryConstant.MayurDE;
		String deotp= null;
		try {
			deotp = (new JSONObject(helpdel.getOtp(de_id.toString()).ResponseValidator.GetBodyAsText())).get("data").toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		//String deotp = delmeth.redisconnectget(DeliveryConstant.de_otp, DeliveryConstant.MayurDE, DeliveryConstant.redisdb);
		hepauto.deotp(DeliveryConstant.MayurDE, deotp);
		helpdel.makeDEFree(DeliveryConstant.MayurDE);
		Thread.sleep(10000);
	    helpdel.assignOrder(order_id, DeliveryConstant.MayurDE);
		Thread.sleep(10000);
		
		return new Object[][] {
			
				{order_id,"arrived"},
				{order_id,"pickedup"},
				{order_id,"reached"},
				{order_id,"delivered"}

		};
	}
	@DataProvider(name = "QE-339")
	public Object[][] CAPRDDP339() throws Exception {
		String order_id=deliveryDataHelper.getOrderIdForCAPRDFF(restaurantId,restaurantLat,restaurantLon);
		//order_id=createOrder();
		return new Object[][] {
			
		{order_id,DeliveryConstant.MayurDE,"1.8.2.1"}

		};
	}
	
	@DataProvider(name = "QE-340")
	public Object[][] CAPRDDP340() throws Exception {
		String order_id=deliveryDataHelper.getOrderIdForCAPRDFF(restaurantId,restaurantLat,restaurantLon);
		hepauto.delogin(DeliveryConstant.MayurDE, "1.8.2.1");
		String de_id= DeliveryConstant.MayurDE;
		String deotp= null;
		try {
			deotp = (new JSONObject(helpdel.getOtp(de_id.toString()).ResponseValidator.GetBodyAsText())).get("data").toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		//String deotp = delmeth.redisconnectget(DeliveryConstant.de_otp, DeliveryConstant.MayurDE, DeliveryConstant.redisdb);
		hepauto.deotp(DeliveryConstant.MayurDE, deotp);
		helpdel.makeDEFree(DeliveryConstant.MayurDE);
		Thread.sleep(10000);
	    helpdel.assignOrder(order_id, DeliveryConstant.MayurDE);
		Thread.sleep(10000);
		delcon.orderConfirmedFromController(order_id);
		Thread.sleep(10000);
		return new Object[][] {
			
				{order_id,"pickedup"},
				{order_id,"reached"},
				{order_id,"delivered"}

		};
	}
	@DataProvider(name = "QE-341")
	public Object[][] CAPRDDP341() throws Exception {
		String order_id=deliveryDataHelper.getOrderIdForCAPRDFF(restaurantId,restaurantLat,restaurantLon);
		return new Object[][] {
			
		{order_id,DeliveryConstant.MayurDE,"1.8.2.1"}

		};
	}
	@DataProvider(name = "QE-342")
	public Object[][] CAPRDDP342() throws Exception {
		String order_id=deliveryDataHelper.getOrderIdForCAPRDFF(restaurantId,restaurantLat,restaurantLon);
		hepauto.delogin(DeliveryConstant.MayurDE, "1.8.2.1");
		String de_id= DeliveryConstant.MayurDE;
		String deotp= null;
		try {
			deotp = (new JSONObject(helpdel.getOtp(de_id.toString()).ResponseValidator.GetBodyAsText())).get("data").toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	//	String deotp = delmeth.redisconnectget(DeliveryConstant.de_otp, DeliveryConstant.MayurDE, DeliveryConstant.redisdb);
		hepauto.deotp(DeliveryConstant.MayurDE, deotp);
		helpdel.makeDEFree(DeliveryConstant.MayurDE);
		Thread.sleep(10000);
	    helpdel.assignOrder(order_id, DeliveryConstant.MayurDE);
		Thread.sleep(10000);
		delcon.orderConfirmedFromController(order_id);
		Thread.sleep(10000);
		delcon.orderArrivedFromController(order_id);
		return new Object[][] {
			
				{order_id,"reached"},
				{order_id,"delivered"}

		};
	}
	@DataProvider(name = "QE-344")
	public Object[][] CAPRDDP344() throws Exception {
		String order_id=deliveryDataHelper.getOrderIdForCAPRDFF(restaurantId,restaurantLat,restaurantLon);
		return new Object[][] {
		{order_id,DeliveryConstant.MayurDE,"1.8.2.1"}

		};
	}
	@DataProvider(name = "QE-345")
	public Object[][] CAPRDDP345() throws Exception {
		String order_id=deliveryDataHelper.getOrderIdForCAPRDFF(restaurantId,restaurantLat,restaurantLon);
		return new Object[][] {
			
		{order_id,DeliveryConstant.MayurDE,"1.8.2.1"}

		};
	}
	@DataProvider(name = "QE-348")
	public Object[][] CAPRDDP348() throws Exception {
		String order_id=deliveryDataHelper.getOrderIdForCAPRDFF(restaurantId,restaurantLat,restaurantLon);
		return new Object[][] {
		{order_id,DeliveryConstant.MayurDE,"1.8.2.1","1"}

		};
	}


}

