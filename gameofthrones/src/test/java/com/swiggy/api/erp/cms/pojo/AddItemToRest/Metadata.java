package com.swiggy.api.erp.cms.pojo.AddItemToRest;

import org.apache.commons.lang.builder.ToStringBuilder;

public class Metadata {

    private String phoneNumbers;

    /**
     * No args constructor for use in serialization
     *
     */
    public Metadata() {
    }

    /**
     *
     * @param phoneNumbers
     */
    public Metadata(String phoneNumbers) {
        super();
        this.phoneNumbers = phoneNumbers;
    }

    public String getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(String phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("phoneNumbers", phoneNumbers).toString();
    }

}