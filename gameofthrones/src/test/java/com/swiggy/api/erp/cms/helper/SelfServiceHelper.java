package com.swiggy.api.erp.cms.helper;

import com.swiggy.api.erp.cms.constants.CmsConstants;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.DBHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SelfServiceHelper {
    Initialize gameofthrones = new Initialize();
   public static String ticketId="", group_id="", parent_resc_id="", assigned_to="", state="", resc_id="", priority="", ticket_type="", 
		active="", eta="";
    
    
    public HashMap<String, String> setHeaders() throws JSONException {
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		JSONObject json = new JSONObject();
		json.put("source", "cms");
		
		JSONObject jsonUserData = new JSONObject();
		jsonUserData.put("user", "CMS-user");
		json.put("meta",jsonUserData);
		
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("user-meta", json.toString());
		requestheaders.put("Authorization", "Basic dmVuZG9yLXN5c3RlbTpzZWxmc2VydmUtc3lzdGVt");
		
		return requestheaders;
	}
    
  
    public String[] setPayload() throws JSONException {
    	return new String[] {"Test Automation Item1", "1", "1", "1", "1", "1", "Automation Item2", "40", "5", "200", "2", "20", "New Test Item2",
    			"item change resquest1", "https://s3-ap-southeast-1.amazonaws.com/cms-file-uploads-cuat/images/items56502780-e275-4060-923e-aed5b2cf2c16-index.jpg",
    			"412903", "461898"};
	}

    
    
    public Processor createTicket(String restaurantId) throws JSONException {
		GameOfThronesService service1 = new GameOfThronesService("cmsselfserve", "addnewitem", gameofthrones);
		 Processor p = new Processor(service1, 
				 					setHeaders(), 
				 					setPayload(), 
				 					new String[] {restaurantId});	
		 return p;	
	}
   
  // Validate Newly created ticket details from DB
  	 public void getTicketDetailsFromDB() {
  	        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
  	        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.getTicketDetails_query);
  	        
  	        Map<String,Object> rowNum = list.get(0);
  	        ticketId = rowNum.get("id").toString();
  	        group_id = rowNum.get("group_id").toString();
  	        parent_resc_id = rowNum.get(parent_resc_id).toString();
  	        assigned_to = rowNum.get("assigned_to").toString();
  	        state = rowNum.get("state").toString();
  	        ticket_type = rowNum.get("ticket_type").toString();
  	        active = rowNum.get("active").toString();
	        eta = rowNum.get("eta").toString();
  	        
  	        Assert.assertNotNull(ticketId);
  	        Assert.assertNotNull(group_id);
  	        Assert.assertNotNull(assigned_to);
  	        Assert.assertNotNull(eta);
	        
  	        Assert.assertEquals(parent_resc_id, CmsConstants.restaurantId_selfserve);
  	        Assert.assertTrue(state.equals("NEW") || state.equals("ASSIGNED") || state.equals("SUBMITTED") );
  	        Assert.assertTrue(ticket_type.equals("SELFSERVE") || ticket_type.equals("PRICE_PARITY_MANUAL_MAPPING"));
  	        Assert.assertEquals(active, 1);
  	    }
    
    public Processor addItemToRest(String payload) {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("cmsselfserve", "additemrestaurant", gameofthrones);
        String[] payloadparams = {payload};
        Processor processor = new Processor(service, requestheaders, payloadparams);
        return processor;
    }

    /*@Test
    public void t() throws IOException {
        AddItemToRestaurant addItemToRestaurant= new RestBuilder().buildItemSlot();
        System.out.println(addItemToRestaurant);

    }*/

    public Processor getHolidayTimeSlots(String restId) {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("cmsselfserve", "triggerevent", gameofthrones);
        String[] urlParams={restId};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }



    /**
     * Update Agent Params
     * @param isEnabled
     * @param isAvailable
     * @param currentLoad
     * @param isActive
     * @param agentIds
     */
    public void updateAgentParams(int isEnabled, int isAvailable, int currentLoad, int isActive, String agentIds){
        DBHelper dbHelper = new DBHelper();
        dbHelper.getMySqlTemplate("cms").update("update agents set is_enabled=" + isEnabled + ", is_available=" +isAvailable+ ", current_load="+ currentLoad +", active=" + isActive + " where id in ("+agentIds+")");
    }

    /**
     * Update Ticket Status
     * @param ticketId
     * @param restaurantId
     * @param status
     * @param assignedTo
     * @param groupId
     * @return
     */
    public Processor updateTicket(long ticketId, long restaurantId, String status, long assignedTo, String groupId, String toStatus, String authKey){
        HashMap<String, String>  hm = new HashMap<>();
        hm.put("Content-Type", "application/json");
        hm.put("Authorization", "Basic YWdlbnQtZGFzaGJvYXJkOnNlbGZzZXJ2ZS1zeXN0ZW0=");
        hm.put("auth-key", authKey);
        GameOfThronesService service = new GameOfThronesService("cmsselfserve", "update_ticket", gameofthrones);
        return new Processor(service, hm, new String[]{""+ticketId, ""+restaurantId, status, ""+assignedTo, groupId}, new String[]{toStatus});
    }


}
