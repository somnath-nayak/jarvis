package com.swiggy.api.erp.ff.helper;

import com.rabbitmq.client.AMQP;
import com.swiggy.api.erp.ff.constants.AssignmentServiceConstants;
import com.swiggy.api.erp.ff.constants.LosConstants;
import com.swiggy.api.erp.ff.constants.OMSConstants;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.DBHelper;
import framework.gameofthrones.Tyrion.RabbitMQHelper;
import framework.gameofthrones.Tyrion.RedisHelper;
import org.testng.Assert;

import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AssignmentServiceHelper {
	
	Initialize gameofthrones = new Initialize();
	
    RabbitMQHelper rabbitMQHelper = new RabbitMQHelper();
    RedisHelper redisHelper = new RedisHelper();
    DBHelper dbHelper = new DBHelper();
    /**
     * @param oeId
     * @param roleId
     * @return
     */
    public String oeLoginRMQ(int oeId, int roleId) {
        System.out.println("++++++++++++++++++++======================Login++++++++++++++++++++++++++++++++++++++++++++++++++++++");

        String message = "{\"new_state_id\": 3, \"city_list\": \"[1, 2, 3, 4, 5, 6, 7]\", \"oe_id\": " + oeId + ", \"event_time\": \"" + Instant.now().toString() + "\", \"last_free_time\": \"" + Instant.now().toString() + "\", \"message_type\": \"login_start_duty\", \"current_order_count\": 0, \"role_id\": " + roleId + "}";
        rabbitMQHelper.pushMessage(OMSConstants.SERVICE, OMSConstants.ASSIGNMENT_LOGIN_QUEUE, new AMQP.BasicProperties().builder().contentType("application/json"), message);
        return message;

    }
    
    public String oeLogoutRMQ(int oeId, int roleId, int new_state_id) {
        System.out.println("++++++++++++++++++++======================Logout++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        String message = "{\"new_state_id\": "+new_state_id+", \"city_list\": \"[1, 2, 3, 4, 5, 6, 7]\", \"unassignment_type_id\": 1, \"oe_id\": " + oeId + ", \"event_time\": \"" + Instant.now().toString() + "\", \"unassignment_reason_id\": 20, \"message_type\": \"logout_stop_duty\", \"current_order_count\": 0, \"role_id\": " + roleId + "}";
        rabbitMQHelper.pushMessage(OMSConstants.SERVICE, OMSConstants.ASSIGNMENT_LOGOUT_QUEUE, new AMQP.BasicProperties().builder().contentType("application/json"), message);
        return message;
    }
    
    public String orderStatusRMQ(long orderId, int status_id) {
        String message = "{ \"message_type\": \"order_status_change\",\"order_id\": "+orderId+",\"new_status_id\": "+status_id+",\"unassignment_reason_id\": 1,\"unassignment_type_id\":1,\"event_time\": \""+Instant.now().toString()+" }";
        rabbitMQHelper.pushMessage(OMSConstants.SERVICE, OMSConstants.ASSIGNMENT_ORDER_STATUS, new AMQP.BasicProperties().builder().contentType("application/json"), message);
        return message;
    }
    
    public String oeFreeRMQ(long orderId,int oeId,int roleId,int uid) {
        String message = "{\"message_type\": \"oe_work_complete_event\",\"oe_id\": " + oeId + ",\"order_id\":"+orderId+",\"role_id\": "+roleId+",\"last_free_time\":\""+Instant.now().toString()+",\"unassignment_reason_id\": "+uid+",\"unassignment_type_id\": 1,\"current_order_count\":0,\"city_list\": \"[1, 2, 3, 4, 5, 6, 7]\",\"event_time\": "+Instant.now().toString()+"}"	;  
        rabbitMQHelper.pushMessage(OMSConstants.SERVICE, OMSConstants.ASSIGNMENT_ORDER_STATUS, new AMQP.BasicProperties().builder().contentType("application/json"), message);
        return message;
    }

    public String manualOrderRMQ(int assignment_reason_id, long orderId, int roleId, int orderPk) {
        String message = "{\"assignment_reason_id\": " + assignment_reason_id + ",\"order_pk\": "+ orderPk +", \"event_time\": \"" + Instant.now().toString() + "\", \"order_id\": " + orderId + ", \"message_type\": \"manual_order\", \"role_id\": " + roleId + ", \"assignment_type_id\": 1}";
        rabbitMQHelper.pushMessage(OMSConstants.SERVICE, OMSConstants.MANUAL_ORDER_QUEUE, new AMQP.BasicProperties().builder().contentType("application/json"), message);
        return message;


    }

    public String ivrsNotEnabled(int assignment_reason_id, long orderId, int roleId, int orderPk) {
        String message = "{\"assignment_reason_id\": " + assignment_reason_id + ", \"order_pk\": "+ orderPk +", \"event_time\": \"" + Instant.now().toString() + "\", \"order_id\": " + orderId + ", \"message_type\": \"manual_order\", \"role_id\": " + roleId + ", \"assignment_type_id\": 1}";
        rabbitMQHelper.pushMessage(OMSConstants.SERVICE, OMSConstants.MANUAL_ORDER_QUEUE, new AMQP.BasicProperties().builder().contentType("application/json"), message);
        return message;


    }

    public String exceptionRms(int assignment_reason_id, int orderId, int roleId) {
        String message = "{\"assignment_reason_id\": " + assignment_reason_id + ", \"event_time\": \"" + Instant.now().toString() + "\", \"order_id\": " + orderId + ", \"message_type\": \"manual_order\", \"role_id\": " + roleId + ", \"assignment_type_id\": 1}";
        rabbitMQHelper.pushMessage(OMSConstants.SERVICE, OMSConstants.MANUAL_ORDER_QUEUE, new AMQP.BasicProperties().builder().contentType("application/json"), message);
        return message;

    }

    public String rNr(int assignment_reason_id, long orderId, int roleId, int orderPk) {
        String message = "{\"assignment_reason_id\": " + assignment_reason_id +", \"order_pk\": "+ orderPk +", \"event_time\": \"" + Instant.now().toString() + "\", \"order_id\": " + orderId + ", \"message_type\": \"manual_order\", \"role_id\": " + roleId + ", \"assignment_type_id\": 1}";
        System.out.println(message);
        rabbitMQHelper.pushMessage(OMSConstants.SERVICE, OMSConstants.MANUAL_ORDER_QUEUE, new AMQP.BasicProperties().builder().contentType("application/json"), message);
        return message;

    }

    public String cNr(int assignment_reason_id, int orderId, int roleId) {
        String message = "{\"assignment_reason_id\": " + assignment_reason_id + ", \"event_time\": \"" + Instant.now().toString() + "\", \"order_id\": " + orderId + ", \"message_type\": \"manual_order\", \"role_id\": " + roleId + ", \"assignment_type_id\": 1}";
        rabbitMQHelper.pushMessage(OMSConstants.SERVICE, OMSConstants.MANUAL_ORDER_QUEUE, new AMQP.BasicProperties().builder().contentType("application/json"), message);
        return message;

    }

    public String deAssistance(int assignment_reason_id, int orderId, int roleId) {
        String message = "{\"assignment_reason_id\": " + assignment_reason_id + ", \"event_time\": \"" + Instant.now().toString() + "\", \"order_id\": " + orderId + ", \"message_type\": \"manual_order\", \"role_id\": " + roleId + ", \"assignment_type_id\": 1}";
        rabbitMQHelper.pushMessage(OMSConstants.SERVICE, OMSConstants.MANUAL_ORDER_QUEUE, new AMQP.BasicProperties().builder().contentType("application/json"), message);
        return message;

    }

    public String updatedRMSresponse(int assignment_reason_id, int orderId, int roleId) {
        String message = "{\"assignment_reason_id\": " + assignment_reason_id + ", \"event_time\": \"" + Instant.now().toString() + "\", \"order_id\": " + orderId + ", \"message_type\": \"manual_order\", \"role_id\": " + roleId + ", \"assignment_type_id\": 1}";
        rabbitMQHelper.pushMessage(OMSConstants.SERVICE, OMSConstants.MANUAL_ORDER_QUEUE, new AMQP.BasicProperties().builder().contentType("application/json"), message);
        return message;

    }

    public String rmsDelay(int assignment_reason_id, int orderId, int roleId) {
        String message = "{\"assignment_reason_id\": " + assignment_reason_id + ", \"event_time\": \"" + Instant.now().toString() + "\", \"order_id\": " + orderId + ", \"message_type\": \"manual_order\", \"role_id\": " + roleId + ", \"assignment_type_id\": 1}";
        rabbitMQHelper.pushMessage(OMSConstants.SERVICE, OMSConstants.MANUAL_ORDER_QUEUE, new AMQP.BasicProperties().builder().contentType("application/json"), message);
        return message;

    }

    public String manualVerification(int assignment_reason_id, int orderId, int roleId) {
        String message = "{\"assignment_reason_id\": " + assignment_reason_id + ", \"event_time\": \"" + Instant.now().toString() + "\", \"order_id\": " + orderId + ", \"message_type\": \"manual_order\", \"role_id\": " + roleId + ", \"assignment_type_id\": 1}";
        rabbitMQHelper.pushMessage(OMSConstants.SERVICE, OMSConstants.MANUAL_ORDER_QUEUE, new AMQP.BasicProperties().builder().contentType("application/json"), message);
        return message;

    }

    public String cnrRetry(int assignment_reason_id, int orderId, int roleId) {
        String message = "{\"assignment_reason_id\": " + assignment_reason_id + ", \"event_time\": \"" + Instant.now().toString() + "\", \"order_id\": " + orderId + ", \"message_type\": \"manual_order\", \"role_id\": " + roleId + ", \"assignment_type_id\": 1}";
        rabbitMQHelper.pushMessage(OMSConstants.SERVICE, OMSConstants.MANUAL_ORDER_QUEUE, new AMQP.BasicProperties().builder().contentType("application/json"), message);
        return message;

    }

    public String placingDelay(int assignment_reason_id, int orderId, int roleId) {
        String message = "{\"assignment_reason_id\": " + assignment_reason_id + ", \"event_time\": \"" + Instant.now().toString() + "\", \"order_id\": " + orderId + ", \"message_type\": \"manual_order\", \"role_id\": " + roleId + ", \"assignment_type_id\": 1}";
        rabbitMQHelper.pushMessage(OMSConstants.SERVICE, OMSConstants.MANUAL_ORDER_QUEUE, new AMQP.BasicProperties().builder().contentType("application/json"), message);
        return message;

    }

    public String ivrsPlacing(int assignment_reason_id, int orderId, int roleId) {
        String message = "{\"assignment_reason_id\": " + assignment_reason_id + ", \"event_time\": \"" + Instant.now().toString() + "\", \"order_id\": " + orderId + ", \"message_type\": \"manual_order\", \"role_id\": " + roleId + ", \"assignment_type_id\": 1}";
        rabbitMQHelper.pushMessage(OMSConstants.SERVICE, OMSConstants.MANUAL_ORDER_QUEUE, new AMQP.BasicProperties().builder().contentType("application/json"), message);
        return message;

    }

    public String rnrRetryFailure(int assignment_reason_id, int orderId, int roleId) {
        String message = "{\"assignment_reason_id\": " + assignment_reason_id + ", \"event_time\": \"" + Instant.now().toString() + "\", \"order_id\": " + orderId + ", \"message_type\": \"manual_order\", \"role_id\": " + roleId + ", \"assignment_type_id\": 1}";
        rabbitMQHelper.pushMessage(OMSConstants.SERVICE, OMSConstants.MANUAL_ORDER_QUEUE, new AMQP.BasicProperties().builder().contentType("application/json"), message);
        return message;

    }

    public String cnrRetryFailure(int assignment_reason_id, int orderId, int roleId) {
        String message = "{\"assignment_reason_id\": " + assignment_reason_id + ", \"event_time\": \"" + Instant.now().toString() + "\", \"order_id\": " + orderId + ", \"message_type\": \"manual_order\", \"role_id\": " + roleId + ", \"assignment_type_id\": 1}";
        rabbitMQHelper.pushMessage(OMSConstants.SERVICE, OMSConstants.MANUAL_ORDER_QUEUE, new AMQP.BasicProperties().builder().contentType("application/json"), message);
        return message;

    }

    public String cacheFailure(int assignment_reason_id, int orderId, int roleId) {
        String message = "{\"assignment_reason_id\": " + assignment_reason_id + ", \"event_time\": \"" + Instant.now().toString() + "\", \"order_id\": " + orderId + ", \"message_type\": \"manual_order\", \"role_id\": " + roleId + ", \"assignment_type_id\": 1}";
        rabbitMQHelper.pushMessage(OMSConstants.SERVICE, OMSConstants.MANUAL_ORDER_QUEUE, new AMQP.BasicProperties().builder().contentType("application/json"), message);
        return message;

    }

    public String partnerCallMe(int assignment_reason_id, int orderId, int roleId) {
        String message = "{\"assignment_reason_id\": " + assignment_reason_id + ", \"event_time\": \"" + Instant.now().toString() + "\", \"order_id\": " + orderId + ", \"message_type\": \"manual_order\", \"role_id\": " + roleId + ", \"assignment_type_id\": 1}";
        rabbitMQHelper.pushMessage(OMSConstants.SERVICE, OMSConstants.MANUAL_ORDER_QUEUE, new AMQP.BasicProperties().builder().contentType("application/json"), message);
        return message;

    }

    public String partnerCallMeRms(int assignment_reason_id, int orderId, int roleId) {
        String message = "{\"assignment_reason_id\": " + assignment_reason_id + ", \"event_time\": \"" + Instant.now().toString() + "\", \"order_id\": " + orderId + ", \"message_type\": \"manual_order\", \"role_id\": " + roleId + ", \"assignment_type_id\": 1}";
        rabbitMQHelper.pushMessage(OMSConstants.SERVICE, OMSConstants.MANUAL_ORDER_QUEUE, new AMQP.BasicProperties().builder().contentType("application/json"), message);
        return message;

    }

    public int getOrderPk(long orderId){
        List<Map<String, Object>> OrderPK = SystemConfigProvider.getTemplate(LosConstants.hostName).queryForList("select id from oms_order where order_id = "+ orderId  +";");
        listCheck(OrderPK);
        System.out.println ("order pk = "+(int)OrderPK.get(0).get("id"));
        return (int)OrderPK.get(0).get("id");
    }

    public HashMap<String, String> getOrderIdAndWaitingFor(long order_id, String waiting_for){
        HashMap<String, String> hm = new HashMap<String, String>();
            List<Map<String, Object>> list = SystemConfigProvider.getTemplate(LosConstants.hostName).queryForList("select order_id,waiting_for from oms_ordersinqueue where order_id = "+ order_id  +" and waiting_for = '"+waiting_for+"';");
            listCheck(list);
            long order = (long)list.get(0).get("order_id");
            hm.put("orderId", String.valueOf(order));
            hm.put("waiting_for",waiting_for);
            return hm;

    }

    public Processor getCurrentOrdesForOE(String oeId){
    	HashMap<String, String> requestheaders = new HashMap<>();
		requestheaders.put("content-type", "application/json");
    	String [] urlParams = {oeId};
    	GameOfThronesService service = new GameOfThronesService(AssignmentServiceConstants.SERVICE, "getcurrentordesforoe", gameofthrones);
    	Processor processor = new Processor(requestheaders, null, urlParams, null, service);
		return processor;
    }
    
    public Processor assignOEToOrder(String orderId, String roleId){
    	HashMap<String, String> requestheaders = new HashMap<>();
		requestheaders.put("content-type", "application/json");
		GameOfThronesService service = new GameOfThronesService(AssignmentServiceConstants.SERVICE, "assignoetoorder", gameofthrones);
    	Processor processor = new Processor(requestheaders, new String[]{orderId,roleId}, null, null, service);
		return processor;
    }

    public void deleteRedisAndMySql(String oe){
        int city_id = 0, role_id = 0;
        if(oe.equals("L1Placer")){
            city_id = AssignmentServiceConstants.l1_city_id;
            role_id = AssignmentServiceConstants.l1_role_id;
        }else if (oe.equals("L2Placer")) {
            city_id = AssignmentServiceConstants.l2_city_id;
            role_id = AssignmentServiceConstants.l2_role_id;
        }else if (oe.equals("Verifier")) {
            city_id = AssignmentServiceConstants.verifier_city_id;
            role_id = AssignmentServiceConstants.verifier_role_id;
        }else if (oe.equals("CallDe")) {
            city_id = AssignmentServiceConstants.callde_city_id;
            role_id = AssignmentServiceConstants.callde_role_id;
        }
        //deleting redis entry for order and oe
        redisHelper.deleteKey(OMSConstants.REDIS, 0, "ORDER_SORTED_SET_" + city_id + "_" + role_id);
        redisHelper.deleteKey(OMSConstants.REDIS, 0, "OE_SORTED_SET_" + role_id);

        //deleting mysql entries in assignment table

        SystemConfigProvider.getTemplate(OMSConstants.ASSIGNMENT_DB).execute("delete FROM ff_assignment.assignment;");

        System.out.println("########  Deleted redis and DB for ==="+city_id+","+ role_id+" and oe ==="+ oe);
    }


    public void listCheck( List mysqlOrders2)
    {

        if(mysqlOrders2.size()==0) {
            Assert.assertTrue(false, "there is no order assigened to verifier2");
        }
    }


    public boolean checkInOrdersInQueueTable(long orderId,String oe)
    {
        List<Map<String, Object>> list = SystemConfigProvider.getTemplate(LosConstants.hostName).queryForList("select order_id,waiting_for from oms_ordersinqueue where order_id = "+ orderId  +" and waiting_for = '"+oe+"';");

        return list.size() != 0;
    }



    public List<Map<String, Object>> checkInAssignment_RequestTable(long orderId,int role_id)
    {
        List<Map<String, Object>> list = SystemConfigProvider.getTemplate(OMSConstants.ASSIGNMENT_DB).queryForList("select * from db_assignment.assignment_request where order_id = "+ orderId  +" and oe_role = '"+role_id+"';");

        listCheck(list);
        return list;

    }

    public String checkInAssignmentDB(long orderId,int oe_id)
    {
        List<Map<String, Object>> list = SystemConfigProvider.getTemplate(OMSConstants.ASSIGNMENT_DB).queryForList("SELECT * FROM db_assignment.assignment where order_id ="+ orderId+ " and oe_id = " +oe_id+";");

        listCheck(list);
        String assignmentId = list.get(0).get("id").toString();
        return assignmentId;

    }

    public String checkInAssignment_Request_MapTable(String assignment_id,String request_id)
    {
        List<Map<String, Object>> requestMapObjects2 = SystemConfigProvider.getTemplate(OMSConstants.ASSIGNMENT_DB).queryForList("select is_active from db_assignment.request_assignment_map where assignment_id = "+ assignment_id+" and request_id ="+"\'"+request_id+"\'"+";");
        listCheck(requestMapObjects2);
        System.out.println("==========================================="+requestMapObjects2.get(0).get("is_active").toString());
       return requestMapObjects2.get(0).get("is_active").toString();



    }

}
