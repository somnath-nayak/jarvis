package com.swiggy.api.erp.delivery.dp;

import org.testng.annotations.DataProvider;

import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.erp.delivery.constants.RTSConstants;
import com.swiggy.api.erp.delivery.helper.DeliveryHelperMethods;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceHelper;
import com.swiggy.api.erp.delivery.helper.RTSHelper;

import framework.gameofthrones.Tyrion.SqlTemplate;

public abstract class RTSDataProvider {
	RTSHelper rtshelp = new RTSHelper();
	static DeliveryHelperMethods delmeth = new DeliveryHelperMethods();
	DeliveryServiceHelper delhelp=new DeliveryServiceHelper();
	@DataProvider(name = "RTS_3")
	public Object[][] RTS_3() throws Exception {
		delmeth.dbhelperupdate("update zone set is_open=1 where id=3");
		delmeth.dbhelperupdate("update delivery_config set meta_value=1 where meta_key='rts_zones'");
		delmeth.dbhelperupdate("update delivery_config set meta_value=1 where meta_key='rts_areas'");
		Thread.sleep(10000);
		return new Object[][] {
		{}
		};
	}
	@DataProvider(name = "RTS_4")
	public Object[][] RTS_4() throws Exception {
		delmeth.dbhelperupdate("update delivery_config set meta_value=3 where meta_key='rts_zones'");
		delmeth.dbhelperupdate("update delivery_config set meta_value=3 where meta_key='rts_areas'");
		Thread.sleep(10000);
		return new Object[][] {
		{}
		};
	}
	@DataProvider(name = "RTS_5")
	public Object[][] RTS_5() throws Exception {
		delmeth.dbhelperupdate("update delivery_config set meta_value=3 where meta_key='rts_zones'");
		delmeth.dbhelperupdate("update delivery_config set meta_value=3 where meta_key='rts_areas'");
		delmeth.dbhelperupdate("update zone_config set max_sla=10 where zone_id=3");
		Thread.sleep(10000);
		return new Object[][] {
		{}
		};
	}
	@DataProvider(name = "RTS_6")
	public Object[][] RTS_6() throws Exception {
		delmeth.dbhelperupdate("update delivery_config set meta_value=3 where meta_key='rts_zones'");
		delmeth.dbhelperupdate("update delivery_config set meta_value=3 where meta_key='rts_areas'");
		delmeth.dbhelperupdate("update zone_config set max_sla=3600 where zone_id=3");
		Thread.sleep(10000);
		return new Object[][] {
		{}
		};
	}
	@DataProvider(name = "RTS_12")
	public Object[][] RTS_12() throws Exception {
		delmeth.dbhelperupdate("update zone_config set max_sla=3600 where zone_id=3");
		delmeth.dbhelperupdate("update zone_config set max_first_mile=10 where zone_id=3");
		Thread.sleep(10000);
		return new Object[][] {
		{}
		};
	}
	@DataProvider(name = "RTS_13")
	public Object[][] RTS_13() throws Exception {
		delmeth.dbhelperupdate("update zone_config set max_sla=3600 where zone_id=3");
		delmeth.dbhelperupdate("update zone_config set max_first_mile=2500 where zone_id=3");
		Thread.sleep(10000);
		return new Object[][] {
		{}
		};
	}
	@DataProvider(name = "RTS_14")
	public Object[][] RTS_14() throws Exception {
		delmeth.dbhelperupdate("update area set last_mile_cap=0.5 where id=3");
		delmeth.dbhelperupdate("update zone_config set max_sla=100 where zone_id=3");
		Thread.sleep(10000);
		return new Object[][] {
		{}
		};
	}
	@DataProvider(name = "RTS_15")
	public Object[][] RTS_15() throws Exception {
		delmeth.dbhelperupdate("update area set last_mile_cap=0.5 where id=3");
		delmeth.dbhelperupdate("update zone_config set max_sla=200 where zone_id=3");
		delmeth.dbhelperupdate("update zone_config set max_first_mile=10 where zone_id=3");
		Thread.sleep(10000);
		return new Object[][] {
		{}
		};
	}
	@DataProvider(name = "RTS_16")
	public Object[][] RTS_16() throws Exception {
		delmeth.dbhelperupdate("update area set last_mile_cap=0.5 where id=3");
		delmeth.dbhelperupdate("update delivery_config set meta_value=4 where meta_key='rts_zones'");
		delmeth.dbhelperupdate("update delivery_config set meta_value=4 where meta_key='rts_areas'");
		delmeth.dbhelperupdate("update zone_config set max_sla=3600 where zone_id=3");
		delmeth.dbhelperupdate("update zone_config set max_first_mile=2500 where zone_id=3");
		Thread.sleep(10000);
		return new Object[][] {
		{}
		};
	}
	@DataProvider(name = "RTS_17")
	public Object[][] RTS_17() throws Exception {
		delmeth.dbhelperupdate("update area set last_mile_cap=4 where id=3");
		delmeth.dbhelperupdate("update delivery_config set meta_value=3 where meta_key='rts_zones'");
		delmeth.dbhelperupdate("update delivery_config set meta_value=3 where meta_key='rts_areas'");
		delmeth.dbhelperupdate("update zone_config set max_sla=4000 where zone_id=3");
		delmeth.dbhelperupdate("update zone_config set max_first_mile=2500 where zone_id=3");
		delmeth.dbhelperupdate("update zone set is_open=0 where id=3");
		Thread.sleep(10000);
		return new Object[][] {
		{}
		};
	}
	@DataProvider(name = "RTS_18")
	public Object[][] RTS_18() throws Exception {
		delmeth.dbhelperupdate("update area set last_mile_cap=4 where id=3");
		delmeth.dbhelperupdate("update zone_config set max_sla=3600 where zone_id=3");
		delmeth.dbhelperupdate("update zone_config set max_first_mile=2500 where zone_id=3");
		delmeth.dbhelperupdate("update zone set is_open=0 where id=3");
		delmeth.dbhelperupdate(RTSConstants.stale_time_less);
		Thread.sleep(10000);
		return new Object[][] {
		{}
		};
	}
	@DataProvider(name = "RTS_19")
	public Object[][] RTS_19() throws Exception {
		delmeth.dbhelperupdate(RTSConstants.stale_time_more);
		Thread.sleep(10000);
		return new Object[][] {
		{}
		};
	}
	@DataProvider(name = "RTS_20")
	public Object[][] RTS_20() throws Exception {
		delmeth.dbhelperupdate(RTSConstants.stale_time_more);
		Thread.sleep(6000);
		return new Object[][] {
		{}
		};
	}
	@DataProvider(name = "RTS_21")
	public Object[][] RTS_21() throws Exception {
		delmeth.dbhelperupdate(RTSConstants.stale_time_more);
		Thread.sleep(6000);
		return new Object[][] {
		{}
		};
	}
	@DataProvider(name = "RTS_24")
	public Object[][] RTS_24() throws Exception {
		delmeth.dbhelperupdate(RTSConstants.stale_time_more);
		rtshelp.updatstartandstopbf(RTSConstants.startbf, RTSConstants.stopbf, RTSConstants.bannerfactor_more);
		Thread.sleep(10000);
		return new Object[][] {
		{}
		};
	}
	@DataProvider(name = "RTS_25")
	public Object[][] RTS_25() throws Exception {
    	delmeth.dbhelperupdate(RTSConstants.stale_time_more);
    	delmeth.dbhelperupdate("update zone set is_open=1 where id=3");
		rtshelp.updatstartandstopbf(RTSConstants.startbf, RTSConstants.stopbf, RTSConstants.bannerfactor_less);
		Thread.sleep(10000);
		return new Object[][] {
		{}
		};
	}
	
	
	

}
