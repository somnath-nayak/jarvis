package com.swiggy.api.erp.cms.tests.CatalogV2;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.helper.CatalogV2Helper;
import framework.gameofthrones.Cersei.ToolBox;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class GetSpinsByCategory {

    CatalogV2Helper clh = new CatalogV2Helper();
    SchemaValidatorUtils schemaValidatorUtils = new SchemaValidatorUtils();
@Test(description = "This test is to download spins by category id")
public void getSpinsByCategory() throws Exception{
    String jsonSchema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/Schemaset/Json/CMS/spinsbycategory.txt");
    String response = clh.getSpinsByCategory("9683ac81-ed76-4146-a429-c8d770425c94").ResponseValidator.GetBodyAsText();
    System.out.println(response);
    List<String> missingAddedNodeList = schemaValidatorUtils.validateServiceSchema(jsonSchema,response);
    Assert.assertTrue(missingAddedNodeList.isEmpty(),missingAddedNodeList + "Nodes are missing or data type of the object is mismatching");
    String status = JsonPath.read(response,"$.response_status");
    Assert.assertEquals(status,"SUCCESS");

}
}
