package com.swiggy.api.erp.crm.tests.foodissues;

import static org.testng.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import com.swiggy.api.erp.crm.constants.HCDataCollectionFlowMapper;
import com.swiggy.api.erp.crm.dp.foodissues.HCDataCollectionDP;

public class HCDataFullOrder {

    Initialize gameofthrones = new Initialize();
    HCDataCollectionFlowMapper flowMap = new HCDataCollectionFlowMapper();
    HCDataCollectionDP dp = new HCDataCollectionDP();

    @Test (dataProviderClass = HCDataCollectionDP.class,dataProvider = "orderDetailsManualFullOrder", description = "Data Collection - Manual Full Order")
    public void manualHCDataCollectionFullOrder(String flow,HashMap<String,String> data,String conversationId, String status) throws Exception
    {
        HashMap<String, String> requestheaders_getData = new HashMap<String, String>();
        requestheaders_getData.put("Content-Type", "application/json");

        // Set the status of the created according to the data provider input
        dp.statusUpdate(status);

        //Fetch the flow map from the flow name
        String[] flowToExecute= flowMap.defineFlow().get(flow);
        int lengthofflow = flowToExecute.length;

        //Number of nodes to visit
        System.out.println("Number of nodes to visit "+ flowToExecute.length);

        String orderid = HCDataCollectionDP.orderId;

        for(int i=0;i<lengthofflow;i++)
        {
            //Set the apiName by fetching the required name from the decide payload function
            String apiName = flowMap.decidePayload(flowToExecute[i]);

            if(apiName=="orderSelectionOverlayInput")
            {
                apiName="validChild";
            }

            String[] payload = new String[]{data.get("orderId").replace("[","").replace("]",""),conversationId,data.get("restaurant_id"),data.get("restaurant_name"),data.get("item_id"),data.get("item_name"), data.get("is_veg"),data.get("quantity"),data.get("cost")};
            String[] uri = new String[]{flowToExecute[i]};

            GameOfThronesService collectFoodIssueData = new GameOfThronesService("crm",apiName,gameofthrones);
            Processor dataCollection_response = new Processor(collectFoodIssueData,requestheaders_getData, payload, uri);

            String addOnGroupList = dataCollection_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data..id").toString().replace("[", "").replace("]", "");
            List<String> addOnlist= Arrays.asList(addOnGroupList.split(","));

            System.out.println("addOnGroupList: " +addOnGroupList);
            String payloadkey = flowMap.decidePayload(flowToExecute[i]);

            if (i < (lengthofflow - 1))
            {
                int index = addOnlist.indexOf(flowToExecute[i+1]);
                System.out.println("flowTOExecute: " +flowToExecute[i+1]);
                Assert.assertEquals(dataCollection_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("status"), "SUCCESS");
                assertTrue(addOnlist.contains(flowToExecute[i+1]));
            } else {
                Assert.assertEquals(dataCollection_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("status"), "SUCCESS");
            }

            if (payloadkey == "orderSelectionOverlayInput")
            {
                String[] queryparam = new String[] {orderid};

                // Get Food Issues based on Order Id
                HashMap<String, String> requestheaders_ccgetfiOrderId = new HashMap<String, String>();
                requestheaders_ccgetfiOrderId.put("Content-Type", "application/json");
                GameOfThronesService ccgetfiOrderId = new GameOfThronesService("crm", "ccgetfiOrderId", gameofthrones);

                Processor ccgetfiOrderId_response = new Processor(ccgetfiOrderId, requestheaders_ccgetfiOrderId, null, queryparam);

                System.out.println(ccgetfiOrderId_response.toString());

                Assert.assertEquals(ccgetfiOrderId_response.ResponseValidator.GetNodeValue("status"), "SUCCESS");
                Assert.assertNotNull(ccgetfiOrderId_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data[0].orderId"));
                Assert.assertNotNull(ccgetfiOrderId_response.ResponseValidator.GetNodeValueAsInt("data[0].id"));

                Assert.assertEquals(ccgetfiOrderId_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data[0].orderId"), orderid);
                Assert.assertEquals(ccgetfiOrderId_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data[0].fullOrder"), "true");

                int foodIssueId = ccgetfiOrderId_response.ResponseValidator.GetNodeValueAsInt("data[0].id");

                String foodId = Integer.toString(foodIssueId);

                // Get Food Issues based on Food Issues Id

                String[] foodArray = new String[] { foodId };
                String[] foodqueryparam = foodArray;

                HashMap<String, String> requestheaders_ccgetfiId = new HashMap<String, String>();
                requestheaders_ccgetfiId.put("Content-Type", "application/json");
                GameOfThronesService ccgetfiId = new GameOfThronesService("crm", "ccgetfiId", gameofthrones);

                Processor ccgetfiId_response = new Processor(ccgetfiId, requestheaders_ccgetfiId, null, foodqueryparam);

                Assert.assertEquals(ccgetfiId_response.ResponseValidator.GetNodeValue("status"), "SUCCESS");

                Assert.assertEquals(ccgetfiId_response.ResponseValidator.GetNodeValueAsInt("data.id"), foodIssueId);
                Assert.assertEquals(ccgetfiId_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.orderId"), orderid);
            }
        }
    }
}
