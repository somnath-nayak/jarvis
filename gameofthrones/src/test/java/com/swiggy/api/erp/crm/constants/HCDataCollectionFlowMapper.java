package com.swiggy.api.erp.crm.constants;
import java.util.HashMap;


public class HCDataCollectionFlowMapper {

    //Deciding which payload to be used for the different nodes
    public String decidePayload(String nodeId){

        String payload=null;

        //Payload for select items
        if(nodeId== "268" ||nodeId=="269"||nodeId=="272"||nodeId=="273") {
            payload="orderSelectionOverlayInput";
            }

        //Payload for comments
        else if (nodeId== "274" ||nodeId=="276"||nodeId=="277"){
            payload="userComment";
        }

        //Payload for image uploads
        else if (nodeId== "279" ||nodeId=="281"){
            payload="imageUploads";
        }

        //Payload for container count
        else if (nodeId== "275"){
            payload="containerOverlay";
        }
        else {
            payload="validChild";
        }

        return payload;
    }


    //The class is a fixed mapper of the flows for the HC data collection APIs

    public HashMap<String,String[]> defineFlow(){

        HashMap<String,String[]> flowMap = new HashMap<String, String[]>();

        //The order is delivered, items are missing from my order
         flowMap.put("foodIssues1",new String[]{CRMConstants.Help,CRMConstants.OrderIssues, CRMConstants.itemsMissing,CRMConstants.itemsMissingContainersNo});
         flowMap.put("foodIssues2",new String[]{CRMConstants.Help,CRMConstants.OrderIssues,CRMConstants.itemsMissing,CRMConstants.itemsMissingContainersNo});

        //The order is delivered, Items are different from what I order
        flowMap.put("foodIssues3",new String[]{CRMConstants.Help,CRMConstants.OrderIssues,CRMConstants.itemsDifferent,CRMConstants.itemsDifferentSelectItems,CRMConstants.itemsDifferentAddComments});
        flowMap.put("foodIssues4",new String[]{CRMConstants.Help,CRMConstants.OrderIssues,CRMConstants.itemsDifferent,CRMConstants.itemsDifferentSelectItems,CRMConstants.itemsDifferentAddComments});

        //Order is delivered, I have packaging or spillage issues
        flowMap.put("foodIssues5",new String[]{CRMConstants.Help,CRMConstants.OrderIssues,CRMConstants.packagingORspillageIssue,CRMConstants.packagingORspillageIssueSomeItems,CRMConstants.packagingORspillageSelectIssue,CRMConstants.packagingORspillageAddPhotos});
        flowMap.put("foodIssues6",new String[]{CRMConstants.Help,CRMConstants.OrderIssues,CRMConstants.packagingORspillageIssue,CRMConstants.packagingORspillageIssueSomeItems,CRMConstants.packagingORspillageSelectIssue,CRMConstants.packagingORspillageAddPhotos});
        flowMap.put("foodIssues7",new String[]{CRMConstants.Help,CRMConstants.OrderIssues,CRMConstants.packagingORspillageIssue,CRMConstants.packagingORspillageIssueSomeItems,CRMConstants.packagingORspillageSelectIssue,CRMConstants.packagingORspillageSKIP});
        flowMap.put("foodIssues8",new String[]{CRMConstants.Help,CRMConstants.OrderIssues,CRMConstants.packagingORspillageIssue,CRMConstants.packagingORspillageIssueSomeItems,CRMConstants.packagingORspillageSelectIssue,CRMConstants.packagingORspillageSKIP});
        flowMap.put("foodIssues9",new String[]{CRMConstants.Help,CRMConstants.OrderIssues,CRMConstants.packagingORspillageIssue,CRMConstants.packagingORspillageEntireItem,CRMConstants.packagingORspillageEntireItemAddPhotos});
        flowMap.put("foodIssues10",new String[]{CRMConstants.Help,CRMConstants.OrderIssues,CRMConstants.packagingORspillageIssue,CRMConstants.packagingORspillageEntireItem,CRMConstants.packagingORspillageEntireItemAddPhotos});
        flowMap.put("foodIssues11",new String[]{CRMConstants.Help,CRMConstants.OrderIssues,CRMConstants.packagingORspillageIssue,CRMConstants.packagingORspillageEntireItem,CRMConstants.packagingORspillageEntireItemSKIP});
        flowMap.put("foodIssues12",new String[]{CRMConstants.Help,CRMConstants.OrderIssues,CRMConstants.packagingORspillageIssue,CRMConstants.packagingORspillageEntireItem,CRMConstants.packagingORspillageEntireItemSKIP});

        // Order is delivered,I have an issue with food quantity
        flowMap.put("foodIssues13",new String[]{CRMConstants.Help,CRMConstants.OrderIssues,CRMConstants.receivedBadQuality,CRMConstants.receivedBadQualitySelectItems});
        flowMap.put("foodIssues14",new String[]{CRMConstants.Help,CRMConstants.OrderIssues,CRMConstants.receivedBadQuality,CRMConstants.receivedBadQualitySelectItems});

        //Order is delivered,I have an issue with food quality
        flowMap.put("foodIssues15",new String[]{CRMConstants.Help,CRMConstants.OrderIssues,CRMConstants.foodQuantity,CRMConstants.foodQuantitySelectItems});
        flowMap.put("foodIssues16",new String[]{CRMConstants.Help,CRMConstants.OrderIssues,CRMConstants.foodQuantity,CRMConstants.foodQuantitySelectItems});
        flowMap.put("foodIssues17",new String[]{CRMConstants.Help,CRMConstants.OrderIssues,CRMConstants.splInstructionsNotFollowed,CRMConstants.splInstructionsNotFollowedAddComments,CRMConstants.splInstructionsNotFollowedSubmitComments});
        flowMap.put("foodIssues18",new String[]{CRMConstants.Help,CRMConstants.OrderIssues,CRMConstants.splInstructionsNotFollowed,CRMConstants.splInstructionsNotFollowedAddComments,CRMConstants.splInstructionsNotFollowedSubmitComments});

        //Cafe Flow
        flowMap.put("foodIssues19",new String[]{CRMConstants.Help,CRMConstants.OrderIssues,CRMConstants.cafeCancel});
        flowMap.put("foodIssues20",new String[]{CRMConstants.Help,CRMConstants.OrderIssues,CRMConstants.cafeEdit});
        flowMap.put("foodIssues21",new String[]{CRMConstants.Help,CRMConstants.OrderIssues,CRMConstants.cafeItemsMissing});
        flowMap.put("foodIssues22",new String[]{CRMConstants.Help,CRMConstants.OrderIssues,CRMConstants.cafeBadQuality});
        flowMap.put("foodIssues23",new String[]{CRMConstants.Help,CRMConstants.OrderIssues,CRMConstants.cafeBuffetCancel});
        flowMap.put("foodIssues24",new String[]{CRMConstants.Help,CRMConstants.OrderIssues,CRMConstants.cafeNotRcvdRefund});
        flowMap.put("foodIssues25",new String[]{CRMConstants.Help,CRMConstants.OrderIssues,CRMConstants.cafeDifferentIssue});
        flowMap.put("foodIssues26",new String[]{CRMConstants.Help,CRMConstants.OrderIssues,CRMConstants.cafePaymentRefundQueries});

        return flowMap;
    }
}
