package com.swiggy.api.erp.cms.pojo.SelfServeV1Item;

public class Batch_info {
	 private String batch_size;

	    private String prep_time;

	    private String batch_reduction_ratio;

	    private String expiry_time;

	    public String getBatch_size ()
	    {
	        return batch_size;
	    }

	    public void setBatch_size (String batch_size)
	    {
	        this.batch_size = batch_size;
	    }

	    public String getPrep_time ()
	    {
	        return prep_time;
	    }

	    public void setPrep_time (String prep_time)
	    {
	        this.prep_time = prep_time;
	    }

	    public String getBatch_reduction_ratio ()
	    {
	        return batch_reduction_ratio;
	    }

	    public void setBatch_reduction_ratio (String batch_reduction_ratio)
	    {
	        this.batch_reduction_ratio = batch_reduction_ratio;
	    }

	    public String getExpiry_time ()
	    {
	        return expiry_time;
	    }

	    public void setExpiry_time (String expiry_time)
	    {
	        this.expiry_time = expiry_time;
	    }

	    @Override
	    public String toString()
	    {
	        return "ClassPojo [batch_size = "+batch_size+", prep_time = "+prep_time+", batch_reduction_ratio = "+batch_reduction_ratio+", expiry_time = "+expiry_time+"]";
	    }
	}
				
				