package com.swiggy.api.erp.ff.tests.los;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import com.swiggy.api.erp.ff.constants.LosConstants;
import com.swiggy.api.erp.ff.dp.losDataProvider;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.swiggy.api.erp.ff.helper.LOSHelper;

import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Tyrion.RabbitMQHelper;

public class RMSUpdateTests extends losDataProvider implements LosConstants{
	
	RabbitMQHelper helper=new RabbitMQHelper();
	LOSHelper losHelper = new LOSHelper();
	Logger log = Logger.getLogger(RMSUpdateTests.class);
	
	@Test(dataProvider = "orderCreationData", groups = {"sanity","regression"},description = "Create a partner order, push the rms verification message to the queue and assert the status in the DB")
	public void testRMSUpdateConsumer(String order_id,String order_time,String epochTime) throws IOException, InterruptedException {
		log.info("***************************************** testRmsUpdate started *****************************************");
		//Send the correct verification JSON data
		losHelper.rmsUpdate("correctJson", order_id, "true", "true", LosConstants.external_order_id, order_time, epochTime);

		Thread.sleep(5000);

		//Get the rms ack value from the db and assert it against the expected value
		List<Map<String, Object>> rms_ack = SystemConfigProvider.getTemplate(LosConstants.hostName).queryForList("select rms_ack from oms_orderstatus where id in (select status_id from oms_order where order_id = " + order_id + ");");
		Boolean db_rms_ack = (Boolean) rms_ack.get(0).get("rms_ack");
		Assert.assertEquals(true, db_rms_ack.booleanValue(), "Order is not not verified by the rms");
		log.info("######################################### testRmsUpdate completed #########################################");
	}

    @Test(dataProvider = "orderCreationData", groups = {"sanity","regression"},description = "Create a partner order, push the rms verification message to the queue with invalid external ID and assert the message in the dead letter queue")
    public void testRMSUpdateForDeadLetterQueue(String order_id,String order_time,String epochTime) throws IOException, InterruptedException, TimeoutException {
        log.info("***************************************** testRmsUpdate started *****************************************");

        //Purger the rms update dead letter queue
      
        Thread.sleep(10000);
        helper.purgeQueue(LosConstants.hostName,LosConstants.rmsUpdateDeadLetterQueue);
        Thread.sleep(2000);

        //Send the correct verification JSON data and collect it
        String pushedMessage=losHelper.rmsUpdate("correctJson", order_id, "true", "true", LosConstants.wrong_external_order_id, order_time, epochTime);
        Thread.sleep(12000);

        //Assert the pushed message is same as from the dead letter queue
        String messgaeFromQueue=helper.getMessage(LosConstants.hostName,LosConstants.rmsUpdateDeadLetterQueue);
        Thread.sleep(100);
        Assert.assertEquals(messgaeFromQueue,pushedMessage,"The comparision of the messages failed");

        log.info("######################################### testRmsUpdate completed #########################################");
    }
	
	@Test(groups = {"sanity","regression"})
	public void testRMSUpdateConsumerWithWrongJson() throws IOException{
		log.info("***************************************** testStatusUpdateToAssignedForManualOrder started *****************************************");
		
		Date date = new Date();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		DateFormat order_idFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		String order_id = order_idFormat.format(date);
		String order_time = dateFormat.format(date);
		log.info("Order time = "+order_time);
		String epochTime = String.valueOf(Instant.now().getEpochSecond());
		log.info("Epoch time = " +epochTime);
		losHelper.rmsUpdate("wrongJson", order_id, "true", "true", "16", order_time, epochTime);
		
		log.info("######################################### testStatusUpdateToAssignedForManualOrder completed #########################################");
	}

}
