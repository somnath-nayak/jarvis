package com.swiggy.api.erp.cms.tests;

import com.swiggy.api.erp.cms.dp.CreateItemDp;
import com.swiggy.api.erp.cms.helper.FullMenuHelper;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

/**
 * Created by kiran.j on 2/19/18.
 */
public class CreateItemTest extends CreateItemDp {

    @Test(dataProvider = "createItems", description = "Verifies Different Scenarios for Creating Items")
    public void createItemsTest(String rest_id, String json, String[] errors, String token_id) {
        SoftAssert softAssert = new SoftAssert();
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        Processor processor = fullMenuHelper.createItem(rest_id, json, token_id);
        if(errors.length == 0)
            softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive(processor));
        else if(errors.length == 1)
            softAssert.assertTrue(processor.ResponseValidator.GetResponseCode() == Integer.parseInt(errors[0]));
        else
            softAssert.assertTrue(fullMenuHelper.validateErrors(processor, errors));
        softAssert.assertAll();
    }

    @Test(dataProvider = "updateItems", description = "Verifies Different Scenarios for Updating Items")
    public void updateItemsTest(String rest_id, String json, String[] errors, String token_id, String id) {
        SoftAssert softAssert = new SoftAssert();
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        Processor processor = fullMenuHelper.updateItem(rest_id, json, token_id, id);
        if(errors.length == 0)
            softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive(processor));
        else if(errors.length == 1)
            softAssert.assertTrue(processor.ResponseValidator.GetResponseCode() == Integer.parseInt(errors[0]));
        else
            softAssert.assertTrue(fullMenuHelper.validateErrors(processor, errors));
        softAssert.assertAll();
    }

    @Test(dataProvider = "createbulkItems", description = "Verifies Different Scenarios for Creating Bulk Items")
    public void createBulkItemsTest(String rest_id, String json, String[] errors, String token_id) {
        System.out.println(json);
        SoftAssert softAssert = new SoftAssert();
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        Processor processor = fullMenuHelper.createBulkItem(rest_id, json, token_id);
        if(errors.length == 0)
            softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive(processor));
        else if(errors.length == 1)
            softAssert.assertTrue(processor.ResponseValidator.GetResponseCode() == Integer.parseInt(errors[0]));
        else
            softAssert.assertTrue(fullMenuHelper.validateErrors(processor, errors));
        softAssert.assertAll();
    }

    @Test(dataProvider = "updatebulkItems", description = "Verifies Different Scenarios for Updating Bulk Items")
    public void updateBulkItemsTest(String rest_id, String json, String[] errors, String token_id) {
        SoftAssert softAssert = new SoftAssert();
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        Processor processor = fullMenuHelper.updateBulkItem(rest_id, json, token_id);
        if(errors.length == 0)
            softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive(processor));
        else if(errors.length == 1)
            softAssert.assertTrue(processor.ResponseValidator.GetResponseCode() == Integer.parseInt(errors[0]));
        else
            softAssert.assertTrue(fullMenuHelper.validateErrors(processor, errors));
        softAssert.assertAll();
    }
}
