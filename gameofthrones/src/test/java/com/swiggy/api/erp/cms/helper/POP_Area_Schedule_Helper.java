package com.swiggy.api.erp.cms.helper;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/*
 * Area scheduling can be created from this class and same details can be updated for area-code=3 (HSR)
 */

public class POP_Area_Schedule_Helper {
	public static int id, areaScheduleid=0;
	Initialize gameofthrones = new Initialize();

	public HashMap<String, String> setHeaders() throws JSONException {
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		JSONObject json = new JSONObject();
		json.put("source", "cms");
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("user_meta", json.toString());
		return requestheaders;
	}

	public String[] setPayload1(String slot_type, int area_id, int openTime, int closeTime, String day,
			String menu_type) throws JSONException {
		return new String[] { slot_type, Integer.toString(area_id), Integer.toString(openTime),
				Integer.toString(closeTime), day, menu_type };
	}

	public Processor createArea_schedule_helper(String slot_type, int area_id, 
			int openTime, int closeTime, String day, String menu_type) throws JSONException {
		GameOfThronesService service1 = new GameOfThronesService("cmsrestaurantservice", "areaschedulecreate", gameofthrones);
		 Processor p = new Processor(service1, 
				 					setHeaders(), 
				 					setPayload1(slot_type, area_id, openTime, closeTime, day, menu_type), 
				 					null);	
		 return p;	
	}
	
	
	public Processor updateArea_schedule_helper(String slot_type, int area_id, int openTime, int closeTime, String day,
			String menu_type, int areaScheduleid) throws JSONException {
		//String getId = Long.toString(id);
		GameOfThronesService service1 = new GameOfThronesService("cmsrestaurantservice", "areascheduleupdate", gameofthrones);
		Processor p1 = new Processor(service1, 
									setHeaders(), 
									setPayload1(slot_type, area_id, openTime, closeTime, day, menu_type),
									new String[] { ""+ areaScheduleid });
		return p1;
	}

	public Processor getArea_schedule_helper(int areaScheduleid) throws JSONException {
		//String getId = Long.toString(id);
		GameOfThronesService service1 = new GameOfThronesService("cmsrestaurantservice", "areascheduleget", gameofthrones);
		Processor p1 = new Processor(service1, 
									setHeaders(), 
									null, 
									new String[] { ""+ areaScheduleid });
		return p1;
	}

	public Processor deleteArea_schedule_helper(int areaScheduleid) throws JSONException {
		GameOfThronesService service1 = new GameOfThronesService("cmsrestaurantservice", "areascheduledelete", gameofthrones);
		Processor p1 = new Processor(service1, 
									setHeaders(), 
									null, 
									new String[] {""+ areaScheduleid });
		return p1;
	}

	public Processor create_InvalidArea_schedule_helper(String slot_type, int area_id, int openTime, int closeTime, String day,
			String menu_type) throws JSONException {
		GameOfThronesService service1 = new GameOfThronesService("cmsrestaurantservice", "areaschedulecreate", gameofthrones);
		Processor p1 = new Processor(service1, 
									setHeaders(), 
									setPayload1(slot_type, area_id, openTime, closeTime, day, menu_type), 
									null);
		return p1;
	}

	public Processor create_Area_schedule_For_ExistingSlot_helper(String slot_type, int area_id, int openTime, int closeTime,
			String day, String menu_type) throws JSONException {
		GameOfThronesService service1 = new GameOfThronesService("cmsrestaurantservice", "areaschedulecreate", gameofthrones);
		Processor p1 = new Processor(service1, 
									setHeaders(), 
									setPayload1(slot_type, area_id, openTime, closeTime, day, menu_type), 
									null);
		return p1;
	}

	public Processor create_Area_schedule_With_Invalid_OR_Empty_DayName_helper(String slot_type, int area_id, int openTime,
			int closeTime, String day, String menu_type) throws JSONException {
		GameOfThronesService service1 = new GameOfThronesService("cmsrestaurantservice", "areaschedulecreate", gameofthrones);
		Processor p1 = new Processor(service1, 
									setHeaders(),
									setPayload1(slot_type, area_id, openTime, closeTime, day, menu_type), 
									null);
		return p1;
	}

}
