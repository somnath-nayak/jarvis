package com.swiggy.api.erp.delivery.dp;

import org.testng.annotations.DataProvider;
import java.text.DateFormat;

import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.erp.delivery.helper.DeliveryDataHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryHelperMethods;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceHelper;
import com.swiggy.api.erp.ff.helper.LOSHelper;
import com.swiggy.api.erp.ff.helper.OMSHelper;

public class DeliveryServiceAssiatanceDataProvider {
	
	

	static String order_id;

	static String order_id1;
	static String order_id2;
	static String order_id3;
	  static LOSHelper loshelper = new LOSHelper();
	DeliveryDataHelper deliveryDataHelper= new DeliveryDataHelper();
	DeliveryHelperMethods del = new DeliveryHelperMethods();
	DeliveryServiceHelper helpdel=new DeliveryServiceHelper();
	OMSHelper omhelper =new OMSHelper();
	
	
	
	
	
	
	@DataProvider(name = "DeAssistance")
	public static Object[][] reassign() throws Exception {

		 order_id1 = loshelper.getAnOrder("pop");

		return new Object[][] {

		{ DeliveryConstant.snded_id, DeliveryConstant.delivery_app_version,order_id1, "INCORRECT_CUSTOMER_ADDRESS", "Incorrect customer address", 0 } };
	}

}
