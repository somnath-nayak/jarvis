package com.swiggy.api.erp.ff.dp;

import com.swiggy.api.erp.ff.constants.ContextualCancellationConstants;
import com.swiggy.api.erp.ff.helper.ContextualCancellationHelper;
import com.swiggy.api.erp.ff.helper.LOSHelper;

import org.primefaces.component.message.Message;
import org.testng.annotations.DataProvider;

import com.swiggy.api.erp.ff.helper.OMSHelper;
import com.swiggy.api.erp.ff.tests.ContextualCancellationTest;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;

import framework.gameofthrones.Tyrion.DBHelper;

import java.io.IOException;
import java.sql.Array;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

public class ContextualCancellationData implements ContextualCancellationConstants {

	static LOSHelper losHelper = new LOSHelper();
	OMSHelper omsHelper = new OMSHelper();
	DBHelper dbHelper = new DBHelper();
	static ContextualCancellationHelper helper = new ContextualCancellationHelper();

	CheckoutHelper CheckoutHelper=new CheckoutHelper();
	
	
	
	@DataProvider (name = "fetchMetaInfoForGroups")
	public Object[][] fetchMetaInfoForGroups() throws IOException, InterruptedException {
		
	//	HashMap <String, String> success = new HashMap <String, String>();
	//	success.put("httpStatusCode");

		return new Object[][] {
			{"1"},
			{"3"}
			
		
		};
	}
	
	
	@DataProvider(name = "fetchMetaInfoForGroupsDbValidate")
    public static Object[][] fetchmetainfoforgroupsDbValidate() throws SQLException, IOException, InterruptedException {
		HashMap<String, Integer> deliveryStatusMap = ContextualCancellationHelper.getDeliveryStatusConfig();
		HashMap<String, Integer> placingStatusMap = ContextualCancellationHelper.getPlacingStatusConfig();
		int rows = groups.length*verificationStatus.length*delivery_status.length*placing_status.length;
		Object[][] status = new Object[rows][5];
		int a = 0;
		while(a < rows) {
			for(int i=0;i<groups.length;i++) {
       		 for(int j=0;j<verificationStatus.length;j++) {
       			for(int k=0;k<delivery_status.length;k++) {
       				for(int l=0;l<placing_status.length;l++)
                     	{
       					status[a][0]= losHelper.getAnOrder("manual");
   						status[a][1]= ContextualCancellationHelper.getAllDispositionsByGroup(verificationStatus[j], deliveryStatusMap.get(delivery_status[k]), placingStatusMap.get(placing_status[l]), groups[i]);
   						status[a][2] = String.valueOf(groups[i]);
   						status[a][3] = verification_status_in_string[j]+" "+ delivery_status[k]+" "+placing_status[l];
   						status[a][4] = groups[i];
   						a++;
                     	}
       			}
       		 }
       	  }
		}
         	
    return status;
    }


	
	
	@DataProvider (name = "fetchMetaInfoForGroupsBadCases")
	public Object[][] fetchMetaInfoForGroupsBadCases() {
		
		HashMap <String, String> badrequest = new HashMap <String, String>();
		badrequest.put("httpStatusCode", "400");
		
		HashMap <String, String> notfound = new HashMap <String, String>();
		notfound.put("httpStatusCode", "404");
		
		return new Object[][] {
			{"10000",badrequest},
			{"test",notfound}
			
		
		};
	}
	
	@DataProvider(name = "fetchAllDispositionIds")
    public Object[][] fetchAllDispositionIds() throws Exception {

	    //Long orderId=omsHelper.getMeAnOrder();

	    HashMap<String, String> successCase = new HashMap<String, String>();
	    successCase.put("httpStatusCode","200");
	    successCase.put("statusMessage","Success");

        HashMap<String, String> badRequestCase = new HashMap<String, String>();
        badRequestCase.put("httpStatusCode","400");
        badRequestCase.put("statusMessage","Error with your submission");
        
        HashMap<String, String> notfound = new HashMap<String, String>();
        notfound.put("httpStatusCode","404");
        

        return new Object[][] {
                //{String.valueOf(orderId), "1",successCase},
				{String.valueOf(768576661), "1",successCase},
                {String.valueOf(768576661), "2",successCase},
				{String.valueOf(768576661), "3",successCase},
				{String.valueOf(768576661), "5",successCase},
                {"000111000", "1",badRequestCase},
                {"0000", "10000",badRequestCase},
                {"0", "",badRequestCase},
                {"0", "2",badRequestCase},
                {"0", "0",badRequestCase},
                {"00000", "0",badRequestCase},

        };
    }
	
	@DataProvider(name = "fetchAllDispositionIdsSchemaValidator")
    public Object[][] fetchAllDispositionIdsSchemaValidator() throws Exception {

	    //Long orderId=omsHelper.getMeAnOrder();

        return new Object[][] {
                //{String.valueOf(orderId), "1"},
                {String.valueOf(768576661), "2"}

        };
    }
	
	@DataProvider(name = "cancellationApi")
	public Object[][] cancellationApi() throws Exception {
		String orderIdInvalid = "76857662";
	//	String message=null;
		HashMap<String, String> successCase = new HashMap<String, String>();
		successCase.put("Testname","successCase");
	    successCase.put("httpStatusCode","200");
	    successCase.put("statusMessage","Success");
	    successCase.put("cancellation_initiated","true");

	    HashMap<String, String> invalidOrderId = new HashMap<String, String>();
        invalidOrderId.put("Testname","invalidOrderId");
        invalidOrderId.put("message","Invalid order");
        invalidOrderId.put("error_field","order");
        invalidOrderId.put("rejected_value",orderIdInvalid);
        invalidOrderId.put("httpStatusCode","400");
        invalidOrderId.put("statusMessage","Errors with your submission");
        invalidOrderId.put("cancellation_initiated","false");
        
     
        HashMap<String, String> invalidAction = new HashMap<String, String>();
        invalidAction.put("Testname","invalidAction");
       
        invalidAction.put("message","Invalid action");
        
        invalidAction.put("error_field","action");
        invalidAction.put("rejected_value","Test");
        invalidAction.put("httpStatusCode","400");
        invalidAction.put("statusMessage","Errors with your submission");
        invalidAction.put("cancellation_initiated","false");
        
        HashMap<String, String> invaliddispositionId = new HashMap<String, String>();
        invaliddispositionId.put("Testname","invaliddispositionId");
        invaliddispositionId.put("error_field","disposition_id");
        invaliddispositionId.put("message","Disposition no longer valid");
        invaliddispositionId.put("rejected_value","200");
        invaliddispositionId.put("httpStatusCode","400");
        invaliddispositionId.put("statusMessage","Errors with your submission");
        invaliddispositionId.put("cancellation_initiated","false");
        
        HashMap<String, String> invalidSubDispositionId = new HashMap<String, String>();
        invalidSubDispositionId.put("Testname","invalidSubDispositionId");
        invalidSubDispositionId.put("error_field","sub_disposition_id");
        invalidSubDispositionId.put("message","Sub Disposition Does not Exist");
        invalidSubDispositionId.put("rejected_value","200");
        invalidSubDispositionId.put("httpStatusCode","400");
        invalidSubDispositionId.put("statusMessage","Errors with your submission");
        invalidSubDispositionId.put("cancellation_initiated","false");
        
        HashMap<String, String> invalidinvalidCancellationFeeApplicability = new HashMap<String, String>();
        invalidinvalidCancellationFeeApplicability.put("Testname","invalidinvalidCancellationFeeApplicability");
        invalidinvalidCancellationFeeApplicability.put("httpStatusCode","400");
        invalidinvalidCancellationFeeApplicability.put("statusMessage","Errors with your submission");
        invalidinvalidCancellationFeeApplicability.put("cancellation_initiated","false");
        
        HashMap<String, String> invalidCancellationFee = new HashMap<String, String>();
        invalidCancellationFee.put("Testname","invalidCancellationFee");
        invalidCancellationFee.put("httpStatusCode","400");
        invalidCancellationFee.put("statusMessage","Errors with your submission");
        invalidCancellationFee.put("cancellation_initiated","false");
        
        
        return new Object[][] {
			{"cancel","14","10","0","false",losHelper.getAnOrder("manual"),successCase},
			{"cancel","14","10","0","false", orderIdInvalid, invalidOrderId},
			{"Test","14","10","0","false",losHelper.getAnOrder("manual"),invalidAction},
			{"cancel","200","10","0","false",losHelper.getAnOrder("manual"),invaliddispositionId},
			{"cancel","14","200","0","false",losHelper.getAnOrder("manual"),invalidSubDispositionId},
			{"cancel","14","10","0","Test",losHelper.getAnOrder("manual"),invalidinvalidCancellationFeeApplicability},
			{"cancel","14","10","-10","false",losHelper.getAnOrder("manual"),invalidCancellationFee}

			
		};
	}
	
	@DataProvider(name = "fetchCancellationFee")  // ranga
	public Object[][] fetchCancellationFee() throws Exception {
		
		HashMap <String, String> successCase = new HashMap <String, String>();
		successCase.put("httpStatusCode", "200");
		successCase.put("statusMessage", "Success");
		
		
		HashMap <String, String> badCase = new HashMap <String, String>();
		badCase.put("httpStatusCode", "400");
		badCase.put("statusMessage", "Errors with your submission");
		
		return new Object[][] {
	     	{"768576627","15","14",successCase},
			{"768576627","14","10",badCase}
		};
	}
	
	@DataProvider(name = "cancellationApiDbValidate")
	public Object[][] cancellationApiDbValidate() throws Exception {
		
		return new Object[][] {
			{"cancel","15","14","0","true", ""+CheckoutHelper.placeOrder("9916400860","Admin@123","132335","1","219").ResponseValidator.GetNodeValueAsInt("$.data.order_id")}  //checkout helper placeorder

		};
	}

}
	
