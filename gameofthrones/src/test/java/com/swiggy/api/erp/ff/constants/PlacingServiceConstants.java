package com.swiggy.api.erp.ff.constants;

public interface PlacingServiceConstants {
    String hostName = "oms";
    String placingServiceExchange = "vendor.placing-fsm.state-transitions";
    String dead_letters_queue = "ff-vendor-placing-service-fsm-queue-exchange-dead-letters";

    default String orderStatusQuery(String orderId, String scenario){
        String query = null;
        if(scenario.contains("PLACED")){
            query = "SELECT placed_status, placed_time FROM oms_orderstatus WHERE id IN (SELECT status_id FROM oms_order WHERE order_id = "+orderId+");";
        }else if (scenario.contains("PARTNER")){
            query = "SELECT placed_status, with_partner_time FROM oms_orderstatus WHERE id IN (SELECT status_id FROM oms_order WHERE order_id = "+orderId+");";
        }else if (scenario.contains("DE")){
            query = "SELECT placed_status, with_de_time FROM oms_orderstatus WHERE id IN (SELECT status_id FROM oms_order WHERE order_id = "+orderId+");";
        }
        return query;
    }
}
