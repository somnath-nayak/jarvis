package com.swiggy.api.erp.cms.pojo.BaseServiceBulkAddonGroup;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.erp.cms.pojo.BaseServiceBulkAddonGroup
 **/

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "addon_groups",
        "delete_by_ids",
        "delete_by_third_party_ids",
        "update_items"
})
public class BulkAddonGroups {

    @JsonProperty("addon_groups")
    private List<Addon_group> addon_groups = null;
    @JsonProperty("delete_by_ids")
    private List<Integer> delete_by_ids = null;
    @JsonProperty("delete_by_third_party_ids")
    private List<String> delete_by_third_party_ids = null;
    @JsonProperty("update_items")
    private Boolean update_items;

    @JsonProperty("addon_groups")
    public List<Addon_group> getAddon_groups() {
        return addon_groups;
    }

    @JsonProperty("addon_groups")
    public void setAddon_groups(List<Addon_group> addon_groups) {
        this.addon_groups = addon_groups;
    }

    @JsonProperty("delete_by_ids")
    public List<Integer> getDelete_by_ids() {
        return delete_by_ids;
    }

    @JsonProperty("delete_by_ids")
    public void setDelete_by_ids(List<Integer> delete_by_ids) {
        this.delete_by_ids = delete_by_ids;
    }

    @JsonProperty("delete_by_third_party_ids")
    public List<String> getDelete_by_third_party_ids() {
        return delete_by_third_party_ids;
    }

    @JsonProperty("delete_by_third_party_ids")
    public void setDelete_by_third_party_ids(List<String> delete_by_third_party_ids) {
        this.delete_by_third_party_ids = delete_by_third_party_ids;
    }

    @JsonProperty("update_items")
    public Boolean getUpdate_items() {
        return update_items;
    }

    @JsonProperty("update_items")
    public void setUpdate_items(Boolean update_items) {
        this.update_items = update_items;
    }

    public BulkAddonGroups build() {
        setDefaultValues();
        return this;
    }

    public void setDefaultValues() {
        //Addon_group addon_group = new Addon_group();
        List<Addon_group> list = new ArrayList<>();
        //list.add(addon_group.build());
        if(this.getUpdate_items() == null)
            this.setUpdate_items(true);
        if(this.getAddon_groups() == null)
            this.setAddon_groups(list);
        if(this.getDelete_by_ids() == null)
            this.setDelete_by_ids(new ArrayList<>());
        if(this.getDelete_by_third_party_ids() == null)
            this.setDelete_by_third_party_ids(new ArrayList<>());
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("addon_groups", addon_groups).append("delete_by_ids", delete_by_ids).append("delete_by_third_party_ids", delete_by_third_party_ids).append("update_items", update_items).toString();
    }

}
