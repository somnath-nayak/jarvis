package com.swiggy.api.erp.ff.tests.verificationService;

import com.swiggy.api.erp.ff.constants.VerificationServiceConstants;
import com.swiggy.api.erp.ff.helper.LOSHelper;
import com.swiggy.api.erp.ff.helper.OMSHelper;
import com.swiggy.api.erp.ff.helper.VerificationServiceHelper;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;

public class DuplicateCheckForLiveOrders {

    VerificationServiceHelper helper = new VerificationServiceHelper();
    OMSHelper omsHelper = new OMSHelper();
    LOSHelper losHelper = new LOSHelper();

    Date date = new Date();
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat order_idFormat = new SimpleDateFormat("yyyyMMddHHmmss");
    String order_id = order_idFormat.format(date);
    String order_time = dateFormat.format(date);
    String epochTime = String.valueOf(Instant.now().getEpochSecond());

    @Test(groups = {"sanity", "regression"},description = "Create a duplicateManualOrderTestfor Different Item Idsa manual order and check for it's availability in waiting for verifier queue")
    public void duplicateCheckForOrderWithDifferentItemId() throws Exception{
        System.out.println("***************************************** duplicateManualOrderTestfor Different Item Ids started *****************************************");
        helper.clearFactTable(VerificationServiceConstants.customerIds);
        String order_id = losHelper.getAnOrder("manual");
        String duplicateWithDifferentItemOrder_id = losHelper.getAnOrder("manual2");
        Thread.sleep(1000);
        String metadata = helper.getMetaData(Long.parseLong(duplicateWithDifferentItemOrder_id));
        Assert.assertTrue(metadata.contains("\"verificationRequired\":false")&& metadata.contains("\"duplicateValidation\":false"));
        System.out.println("######################################### duplicateManualOrderTestfor Different Item Ids compleated #########################################");
    }

    @Test(groups = {"sanity", "regression"},description = "dublicateCheckForOrderWithMoreTimeColleapseInbitween")
    public void duplicateCheckForOrderWithMoreTimeColleapseInbitween() throws Exception{
        System.out.println("***************************************** duplicateManualOrderTest for more than configure Time started *****************************************");
        helper.clearFactTable(VerificationServiceConstants.customerIds);
        String order_id = losHelper.getAnOrder("manual");
        Thread.sleep(100);
        String duplicateOrder_id = losHelper.getAnOrder("manual");
        Thread.sleep(2000);
        String metadata = helper.getMetaData(Long.parseLong(duplicateOrder_id));

        Assert.assertTrue(metadata.contains("\"verificationRequired\":false")&& metadata.contains("\"duplicateValidation\":false"));
        System.out.println("######################################### duplicateManualOrderTest for more than configure Time compleated #########################################");
    }

    @Test(groups = {"sanity", "regression"},description = "dublicateCheckForOrderWithMoreTimeColleapseInbitween")
    public void duplicateCheckForOrderWithSameAmount() throws IOException, InterruptedException {
        System.out.println("***************************************** duplicateManualOrderTest for more than configure Time started *****************************************");
        helper.clearFactTable(VerificationServiceConstants.customerIds);
        losHelper.getAnOrder("212","79427","67414","4");
        String duplicateOrder_id =  losHelper.getAnOrder("212","79427","67414","40");
        Thread.sleep(2000);
        String metadata = helper.getMetaData(Long.parseLong(duplicateOrder_id));
        Assert.assertTrue(metadata.contains("\"verificationRequired\":false")&& metadata.contains("\"duplicateValidation\":false"));
        System.out.println("######################################### duplicateManualOrderTest for more than configure Time compleated #########################################");
    }


}
