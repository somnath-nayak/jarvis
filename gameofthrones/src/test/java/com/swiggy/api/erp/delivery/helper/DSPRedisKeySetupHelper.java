package com.swiggy.api.erp.delivery.helper;

import com.swiggy.api.erp.delivery.constants.DSPrediskeyConstant;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Daenerys.Initializer;

public class DSPRedisKeySetupHelper {
	Initialize init =Initializer.getInitializer();
	DeliveryHelperMethods delmeth=new DeliveryHelperMethods();

	public void addredisDSPkeysforPrepTime(String restaurant_id,String zone_id)
	{
		delmeth.redisconnectsetdoubleStrinvalue(DSPrediskeyConstant.key_prep_time_avg_hour, restaurant_id, 0,DSPrediskeyConstant.set_prep_time_avg_hour_more, DSPrediskeyConstant.redis_box_key);
		delmeth.redisconnectsetdoubleStrinvalue(DSPrediskeyConstant.key_rest_place_order, restaurant_id, 0,DSPrediskeyConstant.set_rest_place_order_more, DSPrediskeyConstant.redis_box_key);
		delmeth.redisconnectsetdoubleStrinvalue(DSPrediskeyConstant.key_rest_delivered_orders, restaurant_id, 0,DSPrediskeyConstant.set_rest_delivered_orders_more, DSPrediskeyConstant.redis_box_key);
		delmeth.redisconnectsetdoubleStrinvalue(DSPrediskeyConstant.key_rest_active_orders, restaurant_id, 0,DSPrediskeyConstant.set_rest_active_orders_more, DSPrediskeyConstant.redis_box_key);
		delmeth.redisconnectsetdoubleStrinvalue(DSPrediskeyConstant.key_rest_de_ratings, restaurant_id, 0,DSPrediskeyConstant.set_rest_de_ratings_more, DSPrediskeyConstant.redis_box_key);
		delmeth.redisconnectsetdoubleStrinvalue(DSPrediskeyConstant.key_prep_time_avg_7_days, restaurant_id, 0,DSPrediskeyConstant.set_prep_time_avg_7_days_more, DSPrediskeyConstant.redis_box_key);
		delmeth.redisconnectsetdoubleStrinvalue(DSPrediskeyConstant.key_prep_time_avg_21_days, restaurant_id, 0,DSPrediskeyConstant.set_prep_time_avg_21_days_more, DSPrediskeyConstant.redis_box_key);
		delmeth.redisconnectsetdoubleStrinvalue(DSPrediskeyConstant.key_rest_wait_time_avg_hour, restaurant_id, 0,DSPrediskeyConstant.set_rest_wait_time_avg_hour_more, DSPrediskeyConstant.redis_box_key);
		delmeth.redisconnectsetdoubleStrinvalue(DSPrediskeyConstant.key_zone_undelivered_orders, zone_id, 0,DSPrediskeyConstant.set_zone_undelivered_orders_less, DSPrediskeyConstant.redis_box_key);
	}
	
	public void deleteredisDSPkeysforPrepTime(String restaurant_id,String zone_id)
	{
		delmeth.rediskeydel(DSPrediskeyConstant.key_prep_time_avg_hour, restaurant_id, 0,DSPrediskeyConstant.redis_box_key);
		delmeth.rediskeydel(DSPrediskeyConstant.key_rest_place_order, restaurant_id, 0,DSPrediskeyConstant.redis_box_key);
		delmeth.rediskeydel(DSPrediskeyConstant.key_rest_delivered_orders, restaurant_id, 0,DSPrediskeyConstant.redis_box_key);
		delmeth.rediskeydel(DSPrediskeyConstant.key_rest_active_orders, restaurant_id, 0,DSPrediskeyConstant.redis_box_key);
		delmeth.rediskeydel(DSPrediskeyConstant.key_rest_de_ratings, restaurant_id, 0,DSPrediskeyConstant.redis_box_key);
		delmeth.rediskeydel(DSPrediskeyConstant.key_prep_time_avg_7_days, restaurant_id, 0,DSPrediskeyConstant.redis_box_key);
		delmeth.rediskeydel(DSPrediskeyConstant.key_prep_time_avg_21_days, restaurant_id, 0,DSPrediskeyConstant.redis_box_key);
		delmeth.rediskeydel(DSPrediskeyConstant.key_rest_wait_time_avg_hour, restaurant_id, 0,DSPrediskeyConstant.redis_box_key);
		delmeth.rediskeydel(DSPrediskeyConstant.key_zone_undelivered_orders, zone_id, 0,DSPrediskeyConstant.redis_box_key);
	}
	
	
	
}	
	
	
	
	
