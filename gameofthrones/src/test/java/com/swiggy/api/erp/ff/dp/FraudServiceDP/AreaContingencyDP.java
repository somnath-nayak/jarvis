package com.swiggy.api.erp.ff.dp.FraudServiceDP;

import com.swiggy.api.erp.ff.POJO.CartDetails;
import com.swiggy.api.erp.ff.POJO.CustomerDetails;
import com.swiggy.api.erp.ff.POJO.DeliveryDetails;
import com.swiggy.api.erp.ff.POJO.SessionDetails;
import org.testng.annotations.DataProvider;

import java.util.ArrayList;
import java.util.List;

public class AreaContingencyDP {

    CustomerDetails cd;
    SessionDetails sd;
    DeliveryDetails dd1, dd2, dd3, dd4;
    CartDetails cartd1, cartd2, cartd3,cartd4;

    @DataProvider(name = "getAreaLevelData")
    public Object[][] getAreaLevelData() {

        String rId = "Testing";
        List<String> requestIds = new ArrayList<>();

        requestIds.add(rId);

        cd = new CustomerDetails();
        cd.setCustomerId(12345);
        cd.setPendingCancellationFee(0);
        cd.setSwiggyMoneyUsed(0.0);
        cd.setMobileNo(123456789);


        List<CustomerDetails> customerDetails = new ArrayList<>();
        customerDetails.add(cd);

        sd = new SessionDetails();
        sd.setDeviceId("abcde-fghij-klmno");
        sd.setTimestamp("2018-07-03 13:56:00");
        sd.setUserAgent("Swiggy-Android");

        List<SessionDetails> sessionDetails = new ArrayList<>();
        sessionDetails.add(sd);

        dd1 = new DeliveryDetails();
        dd1.setAreaId(1);
        dd1.setCityId(1);
        dd1.setCustomerGeohash("abcde");
        dd1.setRestaurantCustomerDistanceKms("1.45");

        dd2 = new DeliveryDetails();
        dd2.setAreaId(2);
        dd2.setCityId(1);
        dd2.setCustomerGeohash("abcde");
        dd2.setRestaurantCustomerDistanceKms("1.45");


        dd3 = new DeliveryDetails();
        dd3.setAreaId(3);
        dd3.setCityId(1);
        dd3.setCustomerGeohash("abcde");
        dd3.setRestaurantCustomerDistanceKms("1.45");

        dd4 = new DeliveryDetails();
        dd4.setAreaId(4);
        dd4.setCityId(4);
        dd4.setCustomerGeohash("abcde");
        dd4.setRestaurantCustomerDistanceKms("1.45");


        List<DeliveryDetails> deliveryDetails = new ArrayList<>();
        deliveryDetails.add(dd1);
        deliveryDetails.add(dd2);
        deliveryDetails.add(dd3);
        deliveryDetails.add(dd4);

        cartd1 = new CartDetails();
        cartd1.setBillAmount(150);
        cartd1.setItemCount(3);
        cartd1.setItemQuantity(10);
        cartd1.setRestaurantId("12345");

        cartd2 = new CartDetails();
        cartd2.setBillAmount(200);
        cartd2.setItemCount(3);
        cartd2.setItemQuantity(10);
        cartd2.setRestaurantId("12345");

        cartd3 = new CartDetails();
        cartd3.setBillAmount(250);
        cartd3.setItemCount(3);
        cartd3.setItemQuantity(10);
        cartd3.setRestaurantId("12345");

        cartd4 = new CartDetails();
        cartd4.setBillAmount(50);
        cartd4.setItemCount(3);
        cartd4.setItemQuantity(10);
        cartd4.setRestaurantId("12345");

        List<CartDetails> cartDetails = new ArrayList<>();
        cartDetails.add(cartd1);
        cartDetails.add(cartd2);
        cartDetails.add(cartd3);
        cartDetails.add(cartd4);

        return dataProviderGenerator.generatevariants2(requestIds, customerDetails, sessionDetails, deliveryDetails, cartDetails);

    }

}
