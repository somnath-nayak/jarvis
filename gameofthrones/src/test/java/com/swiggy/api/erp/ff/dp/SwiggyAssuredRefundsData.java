package com.swiggy.api.erp.ff.dp;
import com.swiggy.api.erp.ff.helper.LOSHelper;
import com.swiggy.api.erp.ff.helper.SwiggyAssuredRefundsHelper;
import org.json.JSONException;
import org.testng.annotations.DataProvider;
import java.io.IOException;
import java.util.HashMap;
import java.text.ParseException;

public class SwiggyAssuredRefundsData {
	
	LOSHelper losHelper = new LOSHelper();
	SwiggyAssuredRefundsHelper swiggyAssuredHelper = new SwiggyAssuredRefundsHelper();

	@DataProvider(name = "getSwiggyAssuredOrderSLABreached")
	public Object[][] getSwiggyAssuredOrderSLABreached() throws IOException, InterruptedException, ParseException, NumberFormatException, JSONException {
	
		String orderTimeEarlier = null;	
		orderTimeEarlier = swiggyAssuredHelper.getOrderTimeEarlier();
		
		String orderTimeNow = null;
		orderTimeNow = swiggyAssuredHelper.getOrderTimeNow();
		
		String epochTime = null;
		epochTime = swiggyAssuredHelper.getEpochTime();
		
		String orderId = null;
		orderId = swiggyAssuredHelper.getOrderId();
		
		/*
		 * To test SLA breached order case create order time one hour before
		 */
			
		losHelper.createSwiggyAssuredOrder(orderId, orderTimeEarlier, epochTime);
		Thread.sleep(2000);
		
		losHelper.deliveryStatusUpdate(orderId, orderTimeNow, "10", "10", "10");
			
		HashMap<String, Integer> responseCodeMap = new HashMap<String, Integer>();
		responseCodeMap.put("validSwiggyAssuredRefundsOrder", 200);
		responseCodeMap.put("isAssuredTrue", 1);
			
		HashMap<String, String> responseCodeMap1 = new HashMap<String, String>();
		responseCodeMap1.put("edited_status", null);
		responseCodeMap1.put("pg_response_time", swiggyAssuredHelper.getPgResponseTime(Long.parseLong(orderId)));
		responseCodeMap1.put("order_delivered_time",swiggyAssuredHelper.getDeliveredTime(Long.parseLong(orderId)));
		responseCodeMap1.put("refund_status",swiggyAssuredHelper.getRefundStatus(Long.parseLong(orderId)));
	
		return new Object[][] {
			{ orderId, "Valid Swiggy Assured Refunds Order SLA breached",responseCodeMap, responseCodeMap1}
		};	
	}
	
	@DataProvider(name = "getSwiggyAssuredOrderSLANotBreached")
	public Object[][] getSwiggyAssuredOrderSLANotBreached() throws IOException, InterruptedException, ParseException, NumberFormatException, JSONException {
		
		String orderTimeEarlier = null;	
		orderTimeEarlier = swiggyAssuredHelper.getOrderTimeEarlier();
		
		String orderTimeNow = null;
		orderTimeNow = swiggyAssuredHelper.getOrderTimeNow();
		
		String epochTime = null;
		epochTime = swiggyAssuredHelper.getEpochTime();
		
		String orderId = null;
		orderId = swiggyAssuredHelper.getOrderId();
		
		losHelper.createSwiggyAssuredOrder(orderId, orderTimeEarlier, epochTime);
		Thread.sleep(2000);
		
		losHelper.deliveryStatusUpdate(orderId, orderTimeNow, "10", "10", "10");

		Thread.sleep(1000);
		
		HashMap<String, Integer> responseCodeMap = new HashMap<String, Integer>();
		HashMap<String, String> responseCodeMap1 = new HashMap<String, String>();
		
		responseCodeMap1.put("edited_status", null);
		responseCodeMap1.put("pg_response_time", swiggyAssuredHelper.getPgResponseTime(Long.parseLong(orderId)));
		responseCodeMap1.put("order_delivered_time",swiggyAssuredHelper.getDeliveredTime(Long.parseLong(orderId)));
		responseCodeMap1.put("refund_status",swiggyAssuredHelper.getRefundStatus(Long.parseLong(orderId)));
	
		return new Object[][] {
			{ orderId, "Valid Swiggy Assured Refunds Order SLA not breached",responseCodeMap, responseCodeMap1}
		};	
	}
	
	@DataProvider(name = "getSwiggyAssuredOrderSLABreachedOrderEdited")
	public Object[][] getSwiggyAssuredOrderSLABreachedOrderEdited() throws IOException, InterruptedException, ParseException, NumberFormatException, JSONException {
				
		String orderTimeEarlier = null;	
		orderTimeEarlier = swiggyAssuredHelper.getOrderTimeEarlier();
		
		String epochTime = null;
		epochTime = swiggyAssuredHelper.getEpochTime();
		
		String orderId = null;
		orderId = swiggyAssuredHelper.getOrderId();
		
		losHelper.createSwiggyAssuredOrder(orderId, orderTimeEarlier, epochTime);
		Thread.sleep(1000);
	
		/*
		 * Set the order status as edited in DB
		 */
		swiggyAssuredHelper.setOrderAsEdited(Long.parseLong(orderId));
			
		Thread.sleep(2000);
		
		losHelper.deliveryStatusUpdate(orderId, orderTimeEarlier, "10", "10", "10");
		
		HashMap<String, Integer> responseCodeMap = new HashMap<String, Integer>();
		responseCodeMap.put("validSwiggyAssuredRefundsOrder", 200);
		responseCodeMap.put("isAssuredTrue", 1);
		
		HashMap<String, String> responseCodeMap1 = new HashMap<String, String>();
		responseCodeMap1.put("edited_status",swiggyAssuredHelper.setOrderAsEdited(Long.parseLong(orderId))); 
		responseCodeMap1.put("pg_response_time", swiggyAssuredHelper.getPgResponseTime(Long.parseLong(orderId)));
		responseCodeMap1.put("order_delivered_time",swiggyAssuredHelper.getDeliveredTime(Long.parseLong(orderId)));
		responseCodeMap1.put("refund_status",swiggyAssuredHelper.getRefundStatus(Long.parseLong(orderId)));
		
		return new Object[][] {
			{ orderId, "Valid Swiggy Assured Refunds Order SLA breached with edited order",responseCodeMap, responseCodeMap1}
		};	
	}
	
	
	@DataProvider(name = "getNonSwiggyAssuredOrder")
	public Object[][] getNonSwiggyAssuredOrder() throws IOException, InterruptedException, ParseException, JSONException {
		
		String orderTimeEarlier = null;	
		orderTimeEarlier = swiggyAssuredHelper.getOrderTimeEarlier();
		
		String orderTimeNow = null;
		orderTimeNow = swiggyAssuredHelper.getOrderTimeNow();
		
		String epochTime = null;
		epochTime = swiggyAssuredHelper.getEpochTime();
		
		String orderId = null;
		orderId = swiggyAssuredHelper.getOrderId();

		losHelper.createOrderWithPartner(orderId, orderTimeEarlier, epochTime);

		Thread.sleep(1000);

		losHelper.deliveryStatusUpdate(orderId, orderTimeNow, "10", "10", "10");

		HashMap<String, Integer> responseCodeMap = new HashMap<String, Integer>();
		responseCodeMap.put("nonSwiggyAssuredOrder", 200);
		responseCodeMap.put("isAssuredTrue", 0);
					
		return new Object[][] {

				{ orderId, "Non Swiggy Assured Refunds Order", responseCodeMap}

		};
	}
	
}
