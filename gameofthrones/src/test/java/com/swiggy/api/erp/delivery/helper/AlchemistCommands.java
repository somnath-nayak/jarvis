package com.swiggy.api.erp.delivery.helper;

import com.swiggy.api.erp.delivery.constants.DEAlchemistConstants;
import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AlchemistCommands {

    List<String> order_ids = null;
    Map<String, String> configMap;
    DeliveryServiceApiHelper delservice = new DeliveryServiceApiHelper();
    DeliveryDataHelper DH = new DeliveryDataHelper();
    AlchemistRuleHelper alchemistRuleHelper = new AlchemistRuleHelper();
    String from_state;
    String to_state;
    int numberOfOrders = 1;
    int RetryCounter = 5;
    public Map<String, Command> AllAlchemistCommandMap = new HashMap<>();
    public void setCAPRD_From_state(String from_state) {
        this.from_state = from_state;
    }
    public void setCAPRD_To_state(String to_state) {
        this.to_state = to_state;
    }
    HashMap<String, String> orderJsonMap = new HashMap<>();

    public AlchemistCommands(HashMap<String, String> ruleMap) {
        order_ids = new ArrayList<>();
        configMap = alchemistRuleHelper.Beforerule(ruleMap);
    }

    public void setOrdermapper() {
        AllAlchemistCommandMap.put("pre_order_checks", new Command() {
            public void runCommand() {
                rainModeCheck();
                deWaitTimeMock();
                batch_Count();
                rejectCount();
            }
        });

        AllAlchemistCommandMap.put("create_new_de", new Command() {
            public void runCommand() {
                createNewDE();
            }
        });

        AllAlchemistCommandMap.put("cancelOrder", new Command() {
            public void runCommand() {
                delservice.cancelOrder(configMap.get("order_id"));
            }
        });

        AllAlchemistCommandMap.put("docaprd", new Command() {
            public void runCommand() {
                if (numberOfOrders > 1) {
                    orderIdCheckforBatchedOrders();
                } else if (numberOfOrders == 1) {
                    configMap.put("order_id", order_ids.get(0));
                }
                DH.doCAPRD(configMap.get("order_id"),configMap.get("de_id"),from_state,to_state);
            }
        });
        AllAlchemistCommandMap.put("assign", new Command() {
            public void runCommand() {
                assignDE();
            }
        });
        AllAlchemistCommandMap.put("doValidate", new Command() {
            public void runCommand() {
                ruleValidate();
            }
        });
    }

    public void createOrderIds() {
        String order_id;
        if (!(configMap.containsKey("batch_count"))) {
            numberOfOrders = 1;
        } else if (configMap.containsKey("batch_count")) {
            numberOfOrders = Integer.parseInt(configMap.get("batch_count"));
        }

//        if((configMap.containsKey("last_mile"))){
//            String LatLong=delservice.getApproxLatLongAtADistance(Double.parseDouble(DEAlchemistConstants.RestLat),Double.parseDouble(DEAlchemistConstants.RestLong),(Double.parseDouble(configMap.get("last_mile"))*2));
//            String loc[]= LatLong.split(",");
//            orderJsonMap.put("lat",loc[0]);
//            orderJsonMap.put("lng",loc[1]);
//        }
        if ((!(configMap.containsKey("last_mile")) && (!(configMap.containsKey("lmt"))))) {
            orderJsonMap.put("lat", DEAlchemistConstants.Cust_Latitude_less_than_5);
            orderJsonMap.put("lng", DEAlchemistConstants.Cust_longitude_less_than_5);
        } else if (configMap.containsKey("lmt") && (Integer.parseInt(configMap.get("lmt")) > 15)) {
            orderJsonMap.put("lat", DEAlchemistConstants.Cust_Latitude_more_than_5);
            orderJsonMap.put("lng", DEAlchemistConstants.Cust_longitude_more_than_5);
        } else if (configMap.containsKey("lmt") && (Integer.parseInt(configMap.get("lmt")) < 15)) {
            orderJsonMap.put("lat", DEAlchemistConstants.Cust_Latitude_less_than_5);
            orderJsonMap.put("lng", DEAlchemistConstants.Cust_longitude_less_than_5);
        } else if (configMap.containsKey("last_mile") && (Integer.parseInt(configMap.get("last_mile")) > 5)) {
            orderJsonMap.put("lat", DEAlchemistConstants.Cust_Latitude_more_than_5);
            orderJsonMap.put("lng", DEAlchemistConstants.Cust_longitude_more_than_5);
        } else if (configMap.containsKey("last_mile") && (Integer.parseInt(configMap.get("last_mile")) < 5)) {
            orderJsonMap.put("lat", DEAlchemistConstants.Cust_Latitude_less_than_5);
            orderJsonMap.put("lng", DEAlchemistConstants.Cust_longitude_less_than_5);
        } else {
            orderJsonMap.put("lat", DEAlchemistConstants.Cust_Latitude_less_than_5);
            orderJsonMap.put("lng", DEAlchemistConstants.Cust_longitude_less_than_5);
        }
        int MultiRestaurantflag = 0;
        for (int i = 0; i < numberOfOrders; i++) {
            if (configMap.containsKey("batch_type") && MultiRestaurantflag == 1) {
                orderJsonMap.put("restaurant_id", "8171");
                orderJsonMap.put("restaurant_lat_lng", "12.915772,77.61001699999997");

            } else if (configMap.containsKey("batch_type") && ((Integer.parseInt(configMap.get("batch_count"))) > 1) && MultiRestaurantflag == 0) {
                orderJsonMap.put("restaurant_id", "8030");
                orderJsonMap.put("restaurant_lat_lng", "12.924476,77.613515");
                if (Integer.parseInt(configMap.get("batch_type")) == 2) {
                    MultiRestaurantflag = 1;
                }
            } else {
                orderJsonMap.put("restaurant_id", "8171");
                orderJsonMap.put("restaurant_lat_lng", "12.915772,77.61001699999997");
            }
            orderJsonMap.put("restaurant_area_code", configMap.get("zone"));
            order_id = delservice.createNewOrder(orderJsonMap);
            order_ids.add(order_id);
        }
    }

    public void createNewDE() {
        String de_id = delservice.createDEandmakeDEActive(Integer.parseInt(configMap.get("zone")));
//        if((configMap.containsKey("first_mile"))){
//            String LatLong=delservice.getApproxLatLongAtADistance(Double.parseDouble(DEAlchemistConstants.RestLat),Double.parseDouble(DEAlchemistConstants.RestLong),(Double.parseDouble(configMap.get("first_mile"))*2));
//            String loc[]= LatLong.split(",");
//            DH.loginDEandUpdateLocation(de_id, DEAlchemistConstants.App_Version,loc[0],loc[1]);
//        }
//        else{
//            DH.loginDEandUpdateLocation(de_id, DEAlchemistConstants.App_Version, DEAlchemistConstants.DE_Latitude_less_than_5, DEAlchemistConstants.DE_longitude_less_than_5);
//
//        }
        if ((!(configMap.containsKey("first_mile")) && (!(configMap.containsKey("fmt"))))) {
            DH.loginDEandUpdateLocation(de_id, DEAlchemistConstants.App_Version, DEAlchemistConstants.DE_Latitude_less_than_5, DEAlchemistConstants.DE_longitude_less_than_5);
        } else if (configMap.containsKey("fmt") && (Integer.parseInt(configMap.get("fmt")) > 15)) {
            DH.loginDEandUpdateLocation(de_id, DEAlchemistConstants.App_Version, DEAlchemistConstants.DE_Latitude_more_than_5, DEAlchemistConstants.DE_longitude_more_than_5);
        } else if (configMap.containsKey("fmt") && (Integer.parseInt(configMap.get("fmt")) < 15)) {
            DH.loginDEandUpdateLocation(de_id, DEAlchemistConstants.App_Version, DEAlchemistConstants.DE_Latitude_less_than_5, DEAlchemistConstants.DE_longitude_less_than_5);
        } else if (configMap.containsKey("first_mile") && (Integer.parseInt(configMap.get("first_mile")) > 5)) {
            DH.loginDEandUpdateLocation(de_id, DEAlchemistConstants.App_Version, DEAlchemistConstants.DE_Latitude_more_than_5, DEAlchemistConstants.DE_longitude_more_than_5);
        } else if (configMap.containsKey("first_mile") && (Integer.parseInt(configMap.get("first_mile")) < 5)) {
            DH.loginDEandUpdateLocation(de_id, DEAlchemistConstants.App_Version, DEAlchemistConstants.DE_Latitude_less_than_5, DEAlchemistConstants.DE_longitude_less_than_5);
        } else {
            DH.loginDEandUpdateLocation(de_id, DEAlchemistConstants.App_Version, DEAlchemistConstants.DE_Latitude_less_than_5, DEAlchemistConstants.DE_longitude_less_than_5);
        }
        configMap.put("de_id", de_id);
    }

    public void batch_Count() {
        createOrderIds();
        if (numberOfOrders > 1) {
            doBatching();
        }
    }

    public void doBatching() {
        if (numberOfOrders > 1) {
            delservice.batchCountDBUpdates(configMap.get("batch_count"), delservice.getBatchID(order_ids.get(0)), delservice.getBatchID(order_ids.get(1)));
        }
        delservice.mergeOrders(order_ids.get(0), order_ids.get(1));
    }

    public void assignDE() {
        delservice.markDEAvailable(configMap.get("de_id"));
        delservice.assignDE(order_ids.get(0), configMap.get("de_id"));
    }

    public void deWaitTimeMock (){
        if(configMap.containsKey("de_wait_time")){
            int prep_time=0;
            Double expected_de_wait_time=Double.parseDouble(configMap.get("de_wait_time"));
            String expression=configMap.get("expression").replaceAll("\\s","");
            int symbolIndex= expression.indexOf("de_wait_time");
            symbolIndex=symbolIndex+ "de_wait_time".length();
            char symbol=expression.charAt(symbolIndex);
            if(symbol=='>'){
                if((configMap.containsKey("fmt") && Integer.parseInt(configMap.get("fmt"))>15) || (configMap.containsKey("first_mile") && Integer.parseInt(configMap.get("first_mile"))>5)){
                    prep_time= (int) ((expected_de_wait_time)*10);
                }
                else{
                    prep_time= (int) ((expected_de_wait_time)*10);
                }
            }
            else if(symbol=='<'){
                if((configMap.containsKey("fmt") && Integer.parseInt(configMap.get("fmt"))>15) || (configMap.containsKey("first_mile") && Integer.parseInt(configMap.get("first_mile"))>5)){
                    prep_time= (int) ((expected_de_wait_time)/2);
                }
                else{
                    prep_time= (int) ((expected_de_wait_time)/2);
                }
            }
            orderJsonMap.put("prep_time",String.valueOf(prep_time));
        }
    }

    public void orderIdCheckforBatchedOrders() {
        RetryCounter = 6;
        String dry_run_order_query = "select * from payout_audit where type='DRYRUN_DELIVERED' and payout_detail like '%RuleId%' and order_id IN (" + order_ids.get(0) + "," + order_ids.get(1) + ")";
        String dry_run_row_count_order_query = "select count(*) from payout_audit where type='DRYRUN_DELIVERED' and payout_detail like '%RuleId%' and order_id IN (" + order_ids.get(0) + "," + order_ids.get(1) + ")";
        String order_id = "";
        while (RetryCounter > 0) {
            String RowCount = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).queryForList(dry_run_row_count_order_query).get(0).get("count(*)").toString();
            if (Integer.parseInt(RowCount) == 1) {
                break;
            }
            System.out.println("Rechecking and waiting for Terminal event in payout_audit table");
            delservice.waitIntervalMili(2000);
            RetryCounter--;
        }
            try {
                order_id = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).queryForList(dry_run_order_query).get(0).get("order_id").toString();
            }
            catch (IndexOutOfBoundsException np) {
                System.out.println("Dry Run didn't not happen successfully for any of the batched orders mentioned. Please check");
                np.printStackTrace();
            }
            System.out.println("Dry Run happened for following order in batch" + order_id);
            configMap.put("order_id", order_id);
        }

        public void rejectCount () {
            if (configMap.containsKey("reject_count")) {
                delservice.rejectOrder(order_ids.get(0), configMap.get("de_id"), Integer.parseInt(configMap.get("reject_count")));
            }
        }

        public void rainModeCheck () {
            if (configMap.containsKey("is_rain_mode") && (Integer.parseInt(configMap.get("is_rain_mode")) == 1)) {
                delservice.enableRainMode(configMap.get("zone"));
            } else if (configMap.containsKey("is_rain_mode") && (Integer.parseInt(configMap.get("is_rain_mode")) == 0)) {
                delservice.disableRainMode(configMap.get("zone"));
            }
        }



        public void ruleValidate () {
            RetryCounter = 6;
            String Payout_Detail_Response = "";
            ArrayList<Integer> rulesApplied = new ArrayList<>();
            String payout_audit_query = "select * from payout_audit where used_for_payout=1 and order_id =" + configMap.get("order_id");
            String check_terminal_event = "select count(*) from payout_audit where used_for_payout=1 and order_id =" + configMap.get("order_id");

            try {
                while (RetryCounter > 0) {
                    String RowCount = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).queryForList(check_terminal_event).get(0).get("count(*)").toString();
                    if (Integer.parseInt(RowCount) == 1) {
                        break;
                    }
                    System.out.println("Rechecking and waiting for Terminal event in payout_audit table");
                    delservice.waitIntervalMili(2000);
                    RetryCounter--;
                }
                Payout_Detail_Response = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).queryForList(payout_audit_query).get(0).get("payout_detail").toString();
                String Response = "{\"Root\":" + Payout_Detail_Response + "}";
                JSONObject jsonObject = new JSONObject(Response);
                JSONArray root = jsonObject.getJSONArray("Root");
                for (int index = 0; index < root.length(); ++index) {
                    rulesApplied.add(root.getJSONObject(index).getInt("RuleId"));
                }
                System.out.println("Test Case created Rule id: " + Integer.parseInt(configMap.get("ruleid")) + " and Following rules got applied to the order after CAPRD-----" + rulesApplied);
            } catch (IndexOutOfBoundsException ip) {
                System.out.println("No terminal state event received for the order in Payout_audit table.. Terminal states are Cancelled/Rejected/Delivered");
                ip.printStackTrace();
            } catch (JSONException je) {
                System.out.println("No Rule got applied to the Order");
                je.printStackTrace();
            }
            System.out.println("With test case data Payout be given to DE = " + configMap.get("SendPayoutToDE"));

            if (!(configMap.containsKey("SendPayoutToDE"))) {
                System.out.println("SendPayoutToDE flag not provided in Test Case, Please add and Run again");
                Assert.assertTrue(false);
            } else if (Boolean.parseBoolean(configMap.get("SendPayoutToDE"))) {
                Assert.assertTrue(rulesApplied.contains(Integer.parseInt(configMap.get("ruleid"))));
            } else if (!(Boolean.parseBoolean(configMap.get("SendPayoutToDE")))) {
                Assert.assertFalse(rulesApplied.contains(Integer.parseInt(configMap.get("ruleid"))));
            }
        }
    }