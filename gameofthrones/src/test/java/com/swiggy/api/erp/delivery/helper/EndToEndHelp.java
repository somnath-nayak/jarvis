package com.swiggy.api.erp.delivery.helper;

import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;
import com.swiggy.api.castleblack.CastleBlackConstants;
import com.swiggy.api.castleblack.CastleBlackManager;
import com.swiggy.api.castleblack.Run;
import com.swiggy.api.castleblack.SlackMessenger;
import com.swiggy.api.erp.delivery.constants.EndToEndConstants;
import com.swiggy.api.sf.checkout.helper.AddressHelper;
import com.swiggy.api.sf.checkout.pojo.CreateMenuEntry;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Daenerys.Initializer;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.DBHelper;
import framework.gameofthrones.Tyrion.JsonHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;
import org.json.simple.JSONObject;
import org.testng.Assert;

import java.io.IOException;
import java.util.*;

public class EndToEndHelp {

   // Initialize gameofthrones = new Initialize();
    Initialize gameofthrones =Initializer.getInitializer();
    JsonHelper jsonHelper= new JsonHelper();
    SANDHelper sandHelper= new SANDHelper();
    CastleBlackManager castleBlackManager = new CastleBlackManager();
    AddressHelper addressHelper= new AddressHelper();
    Random random= new Random();
    DBHelper dbHelper=  new DBHelper();

    Run runId= Run.getInstance();

    public String createOrder(CreateMenuEntry payload) throws IOException {
        String cartPayload= jsonHelper.getObjectToJSON(payload.getCartItems());
        HashMap<String, String> hashMapLogin=createLogin(payload.getpassword(), payload.getmobile());
        Assert.assertEquals(Integer.parseInt(hashMapLogin.get("rc")), 200, "login failed");
        SlackMessenger.webHook(">>>" + "*Environment*" + "--" + "`" + hashMapLogin.get("environment") + "`"+"\n"+"*Service Name*" + "--" + "`" + hashMapLogin.get("pageName") + "`" + "\n" + "*Service URL*" + "--" + "`" +  hashMapLogin.get("url")  + "`" + "\n" + "*Status Code*" + "--" + "`" + hashMapLogin.get("rc") + "`"+"\n"+"*Request Type*"+ "--" + "`"+hashMapLogin.get("methodType")+"`"+"\n"+"*Request Body*"+ "--" + "```"+hashMapLogin.get("payloadBody")+"```"+"\n"+"*Response Body*"+ "--" + "```"+hashMapLogin.get("response")+"```", Integer.parseInt(hashMapLogin.get("rc")));

        HashMap<String, String> cartHashMap = CreateCartAddon(hashMapLogin.get("Tid"), hashMapLogin.get("Token"), cartPayload, payload.getRestaurantId());

        SlackMessenger.webHook(">>>" + "*Environment*" + "--" + "`" + cartHashMap.get("environment") + "`"+"\n"+"*Service Name*" + "--" + "`" + cartHashMap.get("pageName") + "`" + "\n" + "*Service URL*" + "--" + "`" +  cartHashMap.get("url")  + "`" + "\n" + "*Status Code*" + "--" + "`" + cartHashMap.get("rc") + "`"+"\n"+"*Request Type*"+ "--" + "`"+cartHashMap.get("methodType")+"`"+"\n"+"*Request Body*"+ "--" + "```"+cartHashMap.get("payloadBody")+"```"+"\n"+"*Response Body*"+ "--" + "```"+cartHashMap.get("response")+"```", Integer.parseInt(cartHashMap.get("rc")));
        HashMap<String, String> order = orderPlace(hashMapLogin.get("Tid"), hashMapLogin.get("Token"), Integer.parseInt(cartHashMap.get("addressId")), payload.getpayment_cod_method(), payload.getorder_comments());
        SlackMessenger.webHook(">>>" + "*Environment*" + "--" + "`" + order.get("environment") + "`"+"\n"+"*Service Name*" + "--" + "`" + order.get("pageName") + "`" + "\n" + "*Service URL*" + "--" + "`" +  order.get("url")  + "`" + "\n" + "*Status Code*" + "--" + "`" + order.get("rc") + "`"+"\n"+"*Request Type*"+ "--" + "`"+order.get("methodType")+"`"+"\n"+"*Request Body*"+ "--" + "```"+order.get("payloadBody")+"```"+"\n"+"*Response Body*"+ "--" + "```"+order.get("response")+"```", Integer.parseInt(order.get("rc")));
        Assert.assertNotNull(order.get("order_id"), "Order generation failed");
        return order.get("order_id");
    }

    public HashMap<String, String> createLogin(String password, Long mobile) throws IOException
    {
        HashMap<String, String> hashMap= new HashMap<>();
        try {
            HashMap<String, String> requestheaders = new HashMap<>();
            requestheaders.put("content-type", "application/json");
            requestheaders.put("deviceId", "bd6a6df1-a77f-4c69-baa6-5e378dad49a5");
            JSONObject jsonHeader = new JSONObject();
            jsonHeader.putAll(requestheaders);
            GameOfThronesService service = new GameOfThronesService("checkout", "login", gameofthrones);
            String[] payloadParam = {password, String.valueOf(mobile)};
            Processor processor = new Processor(service, requestheaders, payloadParam);
            hashMap = apiDataHashMap(service, processor);
            hashMap.put("pageName", CastleBlackConstants.loginPage);
            String resp = processor.ResponseValidator.GetBodyAsText();
            String stat= JsonPath.read(resp,"$.statusCode").toString().replace("[","").replace("]","");
            Assert.assertEquals(stat, "0", "status code is not zero");
            Assert.assertEquals(processor.ResponseValidator.GetResponseCode(),200,"ResponseCode is not 200");
            insideCastleBlack(hashMap,jsonHeader);
            String r= JsonPath.read(resp, "$.statusCode").toString().replace("[","").replace("]","");
            Assert.assertEquals(EndToEndConstants.statusCode,r,"Status code is not 0");
            //System.out.println("----"+processor.ResponseValidator.GetResponseCode());
            String tid = JsonPath.read(resp, "$.tid").toString().replace("[", "").replace("]", "");
            String token = JsonPath.read(resp, "$.data.token").toString().replace("[", "").replace("]", "");
            hashMap.put("Tid", tid);
            hashMap.put("Token", token);
        }
        catch(Exception e)
        {
            castleBlackManager.insideCastleBlack(runId.s, hashMap.get("pageName"), hashMap.get("url"), hashMap.get("rc"), hashMap.get("Desc"));
            SlackMessenger.webHook1(">>>" + "*Environment*" + "--" + "`" + hashMap.get("environment") + "`" + "\n" + "*Service Name*" + "--" + "`" + hashMap.get("pageName") + "`" + "\n" + "*Service URL*" + "--" + "`" + hashMap.get("url") + "`" + "\n" + "*Status Code*" + "--" + "`" + hashMap.get("rc") + "`" + "\n" + "*Request Type*" + "--" + "`" + hashMap.get("methodType") + "`" + "\n" + "*Request Body*" + "--" + "```" + hashMap.get("payloadBody") +"```" + "\n" + "*Response*" + "--" + "```" + hashMap.get("response") + "```","200");
        }
        catch(AssertionError e)
        {
            castleBlackManager.insideCastleBlack(runId.s, hashMap.get("pageName"), hashMap.get("url"), hashMap.get("rc"), hashMap.get("Desc"));
            SlackMessenger.webHook1(">>>" + "*Environment*" + "--" + "`" + hashMap.get("environment") + "`" + "\n" + "*Service Name*" + "--" + "`" + hashMap.get("pageName") + "`" + "\n" + "*Service URL*" + "--" + "`" + hashMap.get("url") + "`" + "\n" + "*Status Code*" + "--" + "`" + hashMap.get("rc") + "`" + "\n" + "*Request Type*" + "--" + "`" + hashMap.get("methodType") + "`" + "\n" + "*Request Body*" + "--" + "```" + hashMap.get("payloadBody") +"```" + "\n" + "*Response*" + "--" + "```" + hashMap.get("response") + "```","200");
        }
        return hashMap;
    }


    public Processor DeleteCart(String tid, String Token) {
        HashMap<String, String> requestheaders = requestHeader(tid, Token);
        GameOfThronesService service = new GameOfThronesService("checkout", "deletecart", gameofthrones);
        HashMap<String, String> headers = new HashMap<String, String>();
        Processor processor = new Processor(service, requestheaders, null, null);
        return processor;
    }

    public HashMap<String, String> apiDataHashMap(GameOfThronesService service,Processor processor)
    {
        String url=service.APIDetails.Baseurl+"/"+service.APIDetails.APIPath;
        String methodType= service.APIDetails.RequestMethod;
        String environment = service.EnvironmentData.getName();
        System.out.println("----->"+environment);
        String response = processor.ResponseValidator.GetBodyAsText();
        HashMap<String, String> apiData = new HashMap<>();
        int rc = processor.ResponseValidator.GetResponseCode();
        apiData.put("url", url);
        apiData.put("rc", String.valueOf(rc));
        apiData.put("Desc", "OK");
        apiData.put("response", response);
        apiData.put("methodType", methodType);
        apiData.put("environment", environment);
        if(methodType.equalsIgnoreCase("POST")) {
            String payload = processor.RequestValidator.GetBodyAsText();
            apiData.put("payloadBody", payload);
        }
        return apiData;
    }

    public HashMap<String, String> CreateCartAddon(String tid, String token, String cart, Integer rest) throws IOException
    {
        HashMap<String, String> hashMap= new HashMap<>();
        try {
            HashMap<String, String> requestheaders = requestHeader(tid, token);
            JSONObject jsonHeader = new JSONObject();
            jsonHeader.putAll(requestheaders);
            DeleteCart(tid, token);
            GameOfThronesService service = new GameOfThronesService("checkout", "createcartv2nitems", gameofthrones);
            String[] payloadparams = {cart, String.valueOf(rest)};
            Processor processor = new Processor(service, requestheaders, payloadparams);
            String response = processor.ResponseValidator.GetBodyAsText();
            String statusCode = JsonPath.read(response, "$.statusCode").toString().replace("[", "").replace("]", "");
            hashMap = apiDataHashMap(service, processor);
            hashMap.put("pageName", CastleBlackConstants.cart);
            String msg= JsonPath.read(response,"$.statusMessage").toString().replace("[","").replace("]","");
            hashMap.put("msg", msg);
            Assert.assertEquals(EndToEndConstants.statusCode,statusCode,"Status code is not 0");
            Assert.assertEquals(processor.ResponseValidator.GetResponseCode(),200,"ResponseCode is not 200");
            insideCastleBlack(hashMap,jsonHeader);
            //int rc = processor.ResponseValidator.GetResponseCode();
            String lat= JsonPath.read(response,"$.data.restaurant_details.lat").toString().replace("[","").replace("]","");
            String lng= JsonPath.read(response,"$.data.restaurant_details.lng").toString().replace("[","").replace("]","");
            Assert.assertEquals(EndToEndConstants.statusCode,statusCode,"Status code is not 0");
            Assert.assertEquals(processor.ResponseValidator.GetResponseCode(),200,"ResponseCode is not 200");

            hashMap.put("addressId", checkAddressServicibility(response,tid, token, lat, lng));
        }
        catch(Exception e)
        {
            castleBlackManager.insideCastleBlack(runId.s, hashMap.get("pageName"), hashMap.get("url"), "500", hashMap.get("Desc"));
            SlackMessenger.webHook1(">>>" + "*Environment*" + "--" + "`" + hashMap.get("environment") + "`" + "\n"
                    + "*Service Name*" + "--" + "`" + hashMap.get("pageName") + "`" + "\n" + "*Service URL*" + "--" + "`" + hashMap.get("url")
                    + "`" + "\n" + "*Status Code*" + "--" + "`" + "500" + "`" + "\n" + "*Request Type*" + "--" + "`" + hashMap.get("methodType")
                    + "`" + "\n" + "*Request Body*" + "--" + "```" + hashMap.get("payloadBody")
                    + "```"+"\n"+ "*Response*" + "--" + "```" + hashMap.get("msg") + "```", hashMap.get("rc"));
        }
        catch(AssertionError e)
        {
            castleBlackManager.insideCastleBlack(runId.s, hashMap.get("pageName"), hashMap.get("url"), "500", hashMap.get("Desc"));
            SlackMessenger.webHook1(">>>" + "*Environment*" + "--" + "`" + hashMap.get("environment") + "`" + "\n" + "*Service Name*" + "--" + "`" + hashMap.get("pageName") + "`" + "\n" + "*Service URL*" + "--" + "`" + hashMap.get("url") + "`" + "\n" + "*Status Code*" + "--" + "`" + "500" + "`" + "\n" + "*Request Type*" + "--" + "`" + hashMap.get("methodType") + "`" + "\n" + "*Request Body*" + "--" + "```" + hashMap.get("payloadBody") + "```"+"\n"+ "*Response*" + "--" + "```" + hashMap.get("msg") + "```", hashMap.get("rc"));
        }
        return hashMap;
    }

    public void insideCastleBlack(HashMap<String, String> hashMap, JSONObject jsonHeader ) throws IOException {
        castleBlackManager.insideCastleBlack(runId.s, hashMap.get("pageName"), hashMap.get("url"), hashMap.get("rc"), hashMap.get("Desc"));
        SlackMessenger.webHook(">>>" + "*Environment*" + "--" + "`" + hashMap.get("environment") + "`" + "\n" + "*Service Name*" + "--" + "`" + hashMap.get("pageName")
                + "`" + "\n" + "*Service URL*" + "--" + "`" + hashMap.get("url") + "`" + "\n" + "*Status Code*" + "--" + "`" + hashMap.get("rc") + "`" + "\n" + "*Request Type*" + "--" + "`" +
                hashMap.get("methodType") + "`" + "\n" + "*Request Body*" + "--" + "```" + hashMap.get("payloadBody") + "```" + "\n" + "*Response*" + "--" + "```" + hashMap.get("response")
                + "```" + "\n" + "*Request header*" + "--" + jsonHeader.toString(), Integer.parseInt(hashMap.get("rc")));
    }

    public String checkAddressServicibility(String  cartResponse, String tid, String token, String lat, String lng){
        Object AddressId = JsonPath.read(cartResponse, "$.data.addresses..id").toString().replace("[", "").replace("]", "").replace("\"", "");
        String[] AddressIdArray = (AddressId.toString().split(","));
        //String deliveryValid=processor.ResponseValidator.GetNodeValue("data..addresses..delivery_valid");
        String deliveryValid = JsonPath.read(cartResponse, "$.data..addresses..delivery_valid").toString().replace("[", "").replace("]", "");
        String[] delivery = deliveryValid.split(",");
        List<String> addressIdList = new ArrayList<>();
        try {
            for (int i = 0; i < delivery.length; i++) {
                if (delivery[i].equals("1")) {
                    addressIdList.add(AddressIdArray[i]);
                } else {
                    String valid = addressHelper.NewAddress(tid, token, "name", "7406734416", "near budhwal", "234", "area", lat, lng, "223", "ggn", "WORK").ResponseValidator.GetBodyAsText();
                    String j = JsonPath.read(valid, "data.address_id").toString().replace("[", "").replace("]", "");
                    addressIdList.add(j);
                    break;
                }
            }
        }
        catch(Exception e)
        {
            String valid=addressHelper.NewAddress(tid,token,"name","7406734416","near budhwal", "234","area",lat,lng,"223","ggn","WORK").ResponseValidator.GetBodyAsText();
            String j= JsonPath.read(valid,"data.address_id").toString().replace("[","").replace("]","");
            addressIdList.add(j);
        }
        System.out.println(addressIdList.get(0));
        return addressIdList.get(0);
    }

    public List<String> Aggregator(String[] payload) throws IOException {
        HashMap<String, String> hashMap= new HashMap();
        List<String> openRestList = new ArrayList<>();
        try {
            HashMap<String, String> requestheaders = new HashMap<>();
            requestheaders.put("content-type", "application/json");
            JSONObject jsonHeader = new JSONObject();
            jsonHeader.putAll(requestheaders);
            GameOfThronesService service = new GameOfThronesService("sand", "aggregator", gameofthrones);
            Processor processor = new Processor(service, requestheaders, payload);
            String response = processor.ResponseValidator.GetBodyAsText();
            hashMap = apiDataHashMap(service, processor);
            hashMap.put("pageName", CastleBlackConstants.aggregator);
            String statusCode= JsonPath.read(response, "$.statusCode").toString().replace("[","").replace("]","");
            Assert.assertEquals(processor.ResponseValidator.GetResponseCode(),200,"ResponseCode is not 200");
            Assert.assertEquals(EndToEndConstants.statusCode,statusCode,"Status code is not 0");

            List<String> restIds = Arrays.asList(JsonPath.read(response, "$.data.restaurants[*].id").toString().replace("[", "").replace("]", "")
                    .replace("\"", "").split(","));
            List<String> open = Arrays.asList(JsonPath.read(response, "$.data.restaurants[*].availability.opened").toString().replace("[", "")
                    .replace("]", "").replace("\"", "").split(","));
            List<String> restaurantServicibility = Arrays.asList(JsonPath.read(response, "$.data.restaurants.[*].sla.serviceability").toString().
                    replace("[", "").replace("]", "").replace("\"", "").split(","));
            for (int i = 0; i < restIds.size(); i++) {
                if (open.get(i).equalsIgnoreCase("true") && restaurantServicibility.get(i).equalsIgnoreCase("SERVICEABLE")) {
                    openRestList.add(restIds.get(i));
                }
            }
            Assert.assertTrue(openRestList.size()>0, "Restaurants are not serviceable or closed ");
            insideCastleBlack(hashMap, jsonHeader);
        }
         catch(Exception e)
        {
            castleBlackManager.insideCastleBlack(runId.s, hashMap.get("pageName"), hashMap.get("url"), "500", hashMap.get("Desc"));
            SlackMessenger.webHook1(">>>" + "*Environment*" + "--" + "`" + hashMap.get("environment") + "`" + "\n" + "*Service Name*" + "--" + "`" + hashMap.get("pageName") + "`" + "\n" + "*Service URL*" + "--" + "`" + hashMap.get("url") + "`" + "\n" + "*Status Code*" + "--" + "`" + "500" + "`" + "\n" + "*Request Type*" + "--" + "`" + hashMap.get("methodType") + "`" + "\n" + "*Request Body*" + "--" + "```" + hashMap.get("payloadBody") + "```" + "\n" + "*Response*" + "--" + "```" + hashMap.get("response") + "```" ,"200");
        }
        catch(AssertionError e)
        {
            if(openRestList.size()<=0){
                castleBlackManager.insideCastleBlack(runId.s, hashMap.get("pageName"), hashMap.get("url"), "500", hashMap.get("Desc"));
                SlackMessenger.webHook1(">>>" + "*Environment*" + "--" + "`" + hashMap.get("environment") + "`" + "\n" + "*Service Name*" + "--" + "`" + hashMap.get("pageName") + "`" + "\n" + "*Service URL*" + "--" + "`" + hashMap.get("url") + "`" + "\n" + "*Status Code*" + "--" + "`" + "500" + "`" + "\n" + "*Request Type*" + "--" + "`" + hashMap.get("methodType") + "`" + "\n" + "*Request Body*" + "--" + "```" + hashMap.get("payloadBody") + "```" + "\n" + "*Response*" + "--" + "```" + "Restaurants are not serviceable or closed " + "```" ,"200");
            }else{
            castleBlackManager.insideCastleBlack(runId.s, hashMap.get("pageName"), hashMap.get("url"), "500", hashMap.get("Desc"));
            SlackMessenger.webHook1(">>>" + "*Environment*" + "--" + "`" + hashMap.get("environment") + "`" + "\n" + "*Service Name*" + "--" + "`" + hashMap.get("pageName") + "`" + "\n" + "*Service URL*" + "--" + "`" + hashMap.get("url") + "`" + "\n" + "*Status Code*" + "--" + "`" + "500" + "`" + "\n" + "*Request Type*" + "--" + "`" + hashMap.get("methodType") + "`" + "\n" + "*Request Body*" + "--" + "```" + hashMap.get("payloadBody") + "```" + "\n" + "*Response*" + "--" + "```" + hashMap.get("response") + "```" ,"200");}
        }
        return openRestList;
    }

    public int menuItemsList(String restID) {
        String rest = sandHelper.getRestaurantMenu(restID).ResponseValidator.GetBodyAsText();
        String MenuItems = JsonPath.read(rest, "$.data..subCategories..menu[0].id").toString().replace("[", "")
                .replace("]", "");
        String[] menuLists = MenuItems.split(",");
        return menuLists.length;
    }

    public HashMap<String, String> orderPlace(String tid, String token,Integer addressId, String payment, String comments) throws IOException
    {
        HashMap<String, String> hashMap= new HashMap<>();
        try {
            HashMap<String, String> requestheaders = requestHeader(tid, token);
            JSONObject jsonHeader = new JSONObject();
            jsonHeader.putAll(requestheaders);
            GameOfThronesService service = new GameOfThronesService("checkout", "orderplacev1", gameofthrones);
            String[] payloadparams = {String.valueOf(addressId), payment, comments};
            Processor processor = new Processor(service, requestheaders, payloadparams);
            String order = processor.ResponseValidator.GetBodyAsText();
            hashMap = apiDataHashMap(service, processor);
            hashMap.put("pageName", CastleBlackConstants.order);
            String statusCode= JsonPath.read(order,"$.statusCode").toString().replace("[","").replace("]","");
            Assert.assertTrue(processor.ResponseValidator.DoesNodeExists("data.order_id"),"order is not generated");
            String orderId = JsonPath.read(order, "$.data.order_id").toString().replace("[", "").replace("]", "");
            hashMap.put("order_id", orderId);
            Assert.assertEquals(EndToEndConstants.statusCode,statusCode, "statusCode is not 0");
            Assert.assertEquals(processor.ResponseValidator.GetResponseCode(),200,"ResponseCode is not 200");
            insideCastleBlack(hashMap,jsonHeader);

        }
        catch(Exception e)
        {
            castleBlackManager.insideCastleBlack(runId.s, hashMap.get("pageName"), hashMap.get("url"), "500", hashMap.get("Desc"));
            SlackMessenger.webHook1(">>>" + "*Environment*" + "--" + "`" + hashMap.get("environment") + "`" + "\n" + "*Service Name*" + "--" + "`" + hashMap.get("pageName") + "`" + "\n" + "*Service URL*" + "--" + "`" + hashMap.get("url") + "`" + "\n" + "*Status Code*" + "--" + "`" + "500" + "`" + "\n" + "*Request Type*" + "--" + "`" + hashMap.get("methodType") + "`" + "\n" + "*Request Body*" + "--" + "```" + hashMap.get("payloadBody") + "```" + "\n" + "*Response*" + "--" + "```" + hashMap.get("response") + "```" ,"200");
        }

        catch(AssertionError e)
        {
            castleBlackManager.insideCastleBlack(runId.s, hashMap.get("pageName"), hashMap.get("url"), "500", hashMap.get("Desc"));
            SlackMessenger.webHook1(">>>" + "*Environment*" + "--" + "`" + hashMap.get("environment") + "`" + "\n" + "*Service Name*" + "--" + "`" + hashMap.get("pageName") + "`" + "\n" + "*Service URL*" + "--" + "`" + hashMap.get("url") + "`" + "\n" + "*Status Code*" + "--" + "`" + "500" + "`" + "\n" + "*Request Type*" + "--" + "`" + hashMap.get("methodType") + "`" + "\n" + "*Request Body*" + "--" + "```" + hashMap.get("payloadBody") + "```" + "\n" + "*Response*" + "--" + "```" + hashMap.get("response") + "```" ,"200");
        }
        return hashMap;
    }

    public HashMap<String, String> requestHeader(String tid, String token) {
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("content-type", "application/json");
        requestheaders.put("tid", tid);
        requestheaders.put("token", token);
        return requestheaders;
    }

    public String menuItemsList1(String restID) {
        String rest = sandHelper.getRestaurantMenu(restID).ResponseValidator.GetBodyAsText();
        String MenuItems = JsonPath.read(rest, "$.data.categories..id").toString().replace("[", "")
                .replace("]", "");
        String[] menuLists = MenuItems.split(",");
        System.out.println("menu length--->"+menuLists.length);
        return menuLists[1];
    }

    public String getRestaurantMenu(String restID) throws IOException {
        String[] queryParam = {restID};
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        HashMap<String,String> hashMap= new HashMap<>();
        List<String> itemIds= new ArrayList<>();
        try{
        requestHeader.put("Content-Type", "application/json");
        JSONObject jsonHeader = new JSONObject();
        jsonHeader.putAll(requestHeader);
        GameOfThronesService service = new GameOfThronesService("sand1", "menuv3RestId", gameofthrones);
        Processor processor = new Processor(service, requestHeader, null, queryParam);
        String rest= processor.ResponseValidator.GetBodyAsText();
        String stat= JsonPath.read(rest, "$.statusCode").toString().replace("[","").replace("]","");
        hashMap= apiDataHashMap(service,processor);
        hashMap.put("pageName", CastleBlackConstants.menu);
        insideCastleBlack(hashMap,jsonHeader);
        Assert.assertEquals(stat, "0", "status code is not 1");
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(),200, "Response code is Not 200");
        String MenuItems = JsonPath.read(rest, "$.data.menu.items[*].id").toString().replace("[", "").replace("]", "").replace("\"","");
        String stock = JsonPath.read(rest, "$.data.menu.items[*].inStock").toString().replace("[","").replace("]","").replace("\"","");
        String enabled = JsonPath.read(rest, "$.data.menu.items[*].enabled").toString().replace("[","").replace("]","").replace("\"","");
        List<String> menuLists = Arrays.asList(MenuItems.split(","));
        List<String> stockList= Arrays.asList(stock.split(","));
        List<String> enabledList= Arrays.asList(enabled.split(","));
        for(int i=0; i<menuLists.size(); i++) {
            if (stockList.get(i).equals("1") && enabledList.get(i).equals("1"))
            {
                itemIds.add(menuLists.get(i));
                //System.out.println(menuLists.get(i));
            }
        }
        }
        catch(Exception e)
        {
            castleBlackManager.insideCastleBlack(runId.s, hashMap.get("pageName"), hashMap.get("url"), "500", hashMap.get("Desc"));
            SlackMessenger.webHook1(">>>" + "*Environment*" + "--" + "`" + hashMap.get("environment") + "`" + "\n" + "*Service Name*" + "--" + "`" + hashMap.get("pageName") + "`" + "\n" + "*Service URL*" + "--" + "`" + hashMap.get("url") + "`" + "\n" + "*Status Code*" + "--" + "`" + "500" + "`" + "\n" + "*Request Type*" + "--" + "`" + hashMap.get("methodType") + "`" + "\n" + "*Request Body*" + "--" + "```" + hashMap.get("payloadBody") + "```" + "\n" + "*Response*" + "--" + "```" + hashMap.get("response") + "```" ,"200");
        }
        catch(AssertionError e)
        {
            castleBlackManager.insideCastleBlack(runId.s, hashMap.get("pageName"), hashMap.get("url"), "500", hashMap.get("Desc"));
            SlackMessenger.webHook1(">>>" + "*Environment*" + "--" + "`" + hashMap.get("environment") + "`" + "\n" + "*Service Name*" + "--" + "`" + hashMap.get("pageName") + "`" + "\n" + "*Service URL*" + "--" + "`" + hashMap.get("url") + "`" + "\n" + "*Status Code*" + "--" + "`" + "500" + "`" + "\n" + "*Request Type*" + "--" + "`" + hashMap.get("methodType") + "`" + "\n" + "*Request Body*" + "--" + "```" + hashMap.get("payloadBody") + "```" + "\n" + "*Response*" + "--" + "```" + hashMap.get("response") + "```" ,"200");
        }
        int n=random.nextInt(itemIds.size());
        System.out.println("item a gya"+itemIds.get(n));
        return itemIds.get(n);
    }

    public Object getDEId() {
        SqlTemplate sqlTemplate = dbHelper.getMySqlTemplate(CastleBlackConstants.deDb);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CastleBlackConstants.deNameQuery);
        return list.get(0).get("id");
    }

    public String getMenuItemWithoutVariant(String restroMenuList, Integer minPrice) {
        System.out.println(restroMenuList);
        LinkedHashMap<String,Object> menuItems = JsonPath.read(restroMenuList, "$.data.menu.items");
        List<String> items = new ArrayList<String>();
        List<Object> itemVariation = null;
        for(Map.Entry<String,Object> entry : menuItems.entrySet()) {

            try {
                itemVariation = JsonPath.read(entry.getValue(), "$.variants_new.variant_groups.*.variations");
            }catch(PathNotFoundException e) {
            }
            if((null== itemVariation || itemVariation.size()==0)&&(((int)JsonPath.read(entry.getValue(),"$.price")/100)>= minPrice)){

                items.add(entry.getKey());
            }
        }
        if(items.size()!=0) {
            Random random = new Random();
            return items.get(random.nextInt(items.size()));
        } else {
            return "";
        }
    }


    public String getMenuItemWithoutVariantWithMinAmt(String restId, Integer minAmt) {
        String menuItem;
        HashMap<String, String> menuItemAndPrice = new HashMap<>();
       // for (String rest : restList) {
            String restroMenuResponse = sandHelper.getRestaurantMenu(restId).ResponseValidator.GetBodyAsText();
            // select a menu item
            menuItem = getMenuItemWithoutVariant(restroMenuResponse, minAmt);
            if (menuItem != "") {
                menuItemAndPrice.put("menuItemID", menuItem);
                menuItemAndPrice.put("restID", restId);
                menuItemAndPrice.put("itemPrice", String.valueOf(
                        (int) JsonPath.read(restroMenuResponse, "$.data.menu.items." + menuItem + ".price") / 100));

            }
        //}
        return menuItemAndPrice.get("menuItemID");
    }

}
