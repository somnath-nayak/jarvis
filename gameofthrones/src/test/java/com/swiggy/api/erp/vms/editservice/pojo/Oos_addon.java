package com.swiggy.api.erp.vms.editservice.pojo;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"oos_group_id",
"oos_choices",
"alternate_choices"
})
public class Oos_addon {

@JsonProperty("oos_group_id")
private long oos_group_id;
@JsonProperty("oos_choices")
private List<Long> oos_choices = null;
@JsonProperty("alternate_choices")
private List<Long> alternate_choices = null;

/**
* No args constructor for use in serialization
* 
*/
public Oos_addon() {
}

/**
* 
* @param alternate_choices
* @param oos_group_id
* @param oos_choices
*/
public Oos_addon(long oos_group_id, List<Long> oos_choices, List<Long> alternate_choices) {
super();
this.oos_group_id = oos_group_id;
this.oos_choices = oos_choices;
this.alternate_choices = alternate_choices;
}

@JsonProperty("oos_group_id")
public long getOos_group_id() {
return oos_group_id;
}

@JsonProperty("oos_group_id")
public void setOos_group_id(long oos_group_id) {
this.oos_group_id = oos_group_id;
}

public Oos_addon withOos_group_id(long oos_group_id) {
this.oos_group_id = oos_group_id;
return this;
}

@JsonProperty("oos_choices")
public List<Long> getOos_choices() {
return oos_choices;
}

@JsonProperty("oos_choices")
public void setOos_choices(List<Long> oos_choices) {
this.oos_choices = oos_choices;
}

public Oos_addon withOos_choices(List<Long> oos_choices) {
this.oos_choices = oos_choices;
return this;
}

@JsonProperty("alternate_choices")
public List<Long> getAlternate_choices() {
return alternate_choices;
}

@JsonProperty("alternate_choices")
public void setAlternate_choices(List<Long> alternate_choices) {
this.alternate_choices = alternate_choices;
}

public Oos_addon withAlternate_choices(List<Long> alternate_choices) {
this.alternate_choices = alternate_choices;
return this;
}

@Override
public String toString() {
return new ToStringBuilder(this).append("oos_group_id", oos_group_id).append("oos_choices", oos_choices).append("alternate_choices", alternate_choices).toString();
}

@Override
public int hashCode() {
return new HashCodeBuilder().append(alternate_choices).append(oos_group_id).append(oos_choices).toHashCode();
}

@Override
public boolean equals(Object other) {
if (other == this) {
return true;
}
if ((other instanceof Oos_addon) == false) {
return false;
}
Oos_addon rhs = ((Oos_addon) other);
return new EqualsBuilder().append(alternate_choices, rhs.alternate_choices).append(oos_group_id, rhs.oos_group_id).append(oos_choices, rhs.oos_choices).isEquals();
}

}