package com.swiggy.api.erp.ff.constants.createTaskAPI;

public enum TaskType {

    PLACING("placing", 1),
    VERIFICATION("verification", 2),
    DE_ASSISTANCE("de_assistance", 3),
    CUSTOMER_ASSISTANCE("customer_assistance", 4);

    private final String description;

    private final int id;

    TaskType(String description, int id) {
        this.description = description;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String toString() {
        return this.description;
    }
}
