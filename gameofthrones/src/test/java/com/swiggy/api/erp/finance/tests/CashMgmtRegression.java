package com.swiggy.api.erp.finance.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.finance.constants.CashMgmtConstant;
import com.swiggy.api.erp.finance.dp.CashMgmtRegressionDP;
import com.swiggy.api.erp.finance.helper.CashMgmtHelper;
import com.swiggy.api.erp.finance.helper.CashMgmtRegressionHelper;
import com.swiggy.api.erp.finance.pojo.DailyOrderDetails;
import framework.gameofthrones.JonSnow.DateHelper;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.apache.xpath.operations.Bool;
import org.mortbay.log.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;

import java.io.*;
import java.time.Instant;
import java.util.*;


/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.erp.finance.tests
 **/
public class CashMgmtRegression extends CashMgmtRegressionDP {

    CashMgmtRegressionHelper helper = new CashMgmtRegressionHelper();
    private static Logger log = LoggerFactory.getLogger(CashMgmtRegression.class);
    CashMgmtHelper cashMgmtHelper = new CashMgmtHelper();


    DateHelper dateHelper = new DateHelper();

    String g_zone_id, g_de_id;

    @BeforeClass
    public void createAndGetZoneIdAndDEId() throws IOException {
//        String[] getVals = createZoneAndDE();
//        g_zone_id = getVals[0];
//        g_de_id = getVals[1];
        createZoneAndDE();
        log.info("--ZONE-ID ::: "+g_zoneID);
        log.info("--DE-ID ::: "+g_deID);
        cashMgmtHelper.createSessionHelper(g_deID);
    }


    @Test(dataProvider = "getFloatingCashDP",groups = {"CashMgmt", "CashMgmtRegression"},description="validate Floating Cash",enabled = true)
    public void validateFloatingCash(String deID, String fc, String validCase) {
        SoftAssert softAssert = new SoftAssert();
        Processor p = helper.getFloatingCashDetailsHelper(deID);
        String get_msg = p.ResponseValidator.GetNodeValue("$.statusMessage");
        int get_status = p.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        if (Boolean.parseBoolean(validCase)){
            String get_fc = p.ResponseValidator.GetNodeValue("$.data.floatingCash");
            //assert
            softAssert.assertEquals(get_msg,CashMgmtConstant.msg_success, "Msg not success");
            softAssert.assertEquals(get_status,CashMgmtConstant.statusZero, "status not zero");
            softAssert.assertEquals(get_fc,fc,"Floating cash not same");
        } else {
            String get_data = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data").replace("{","").replace("}","");
            //assert
            softAssert.assertEquals(get_msg,CashMgmtConstant.msg_deID_not_found, "deid not found msg not same");
            softAssert.assertEquals(get_status,CashMgmtConstant.statusOne, "status not 1");
            softAssert.assertEquals(get_data,"", "data not empty string");
        }
        softAssert.assertAll();
    }

    @Test(dataProvider = "zoneLevelFloatingCashDP",groups = {"CashMgmt", "CashMgmtRegression"},description="validate Zone Level Floating Cash",enabled = true)
    public void validateZoneLevelFloatingCash(String zoneID, String fc,String[] deIDs, String validCase) {
        SoftAssert softAssert = new SoftAssert();
        int fc_total =0;
        Processor p = helper.getZoneLevelFloatingCashHelper(zoneID);
        String get_msg = p.ResponseValidator.GetNodeValue("$.statusMessage");
        int get_status = p.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        if (Boolean.parseBoolean(validCase)){
            int get_fc = p.ResponseValidator.GetNodeValueAsInt("$.data.floatingCash");
            String[] get_DEs = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..deliveryBoys[*]..deId").replace("}","").replace("[","").replace("{","").replace("]","").replace("\"","").trim().split(",");
            String[] individual_FCs =p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..deliveryBoys[*]..floatingCash").replace("}","").replace("[","").replace("{","").replace("]","").replace("\"","").trim().split(",");
            //Arrays.sort(get_DEs);
            Arrays.sort(deIDs);
            for (String de:get_DEs) {
                softAssert.assertTrue((Arrays.binarySearch(deIDs,de)>=0), "de ID not found in response from api.Not found de:"+de);
            }
            for (int i = 0; i < individual_FCs.length; i++) {
                fc_total = fc_total + Integer.parseInt(individual_FCs[i]);
            }
            softAssert.assertEquals(get_DEs.length,deIDs.length,"length not same. API response len:"+get_DEs.length);
            softAssert.assertEquals(fc_total,Integer.parseInt(fc), "sum of individual fc is not same");
            softAssert.assertEquals(get_msg,CashMgmtConstant.msg_success, "Msg not success");
            softAssert.assertEquals(get_status,CashMgmtConstant.status200, "status not 200");
            softAssert.assertEquals(String.valueOf(get_fc),fc,"Floating cash not same");
        } else {
            String get_data = p.ResponseValidator.GetNodeValue("$.data");
            //assert
            softAssert.assertEquals(get_msg,CashMgmtConstant.msg_zoneID_not_found, "zone not found msg not same");
            softAssert.assertEquals(get_status,CashMgmtConstant.status400, "status not 1");
            softAssert.assertNull(get_data,"data not null");
        }
        softAssert.assertAll();
    }

    @Test(dataProvider = "zoneCashOverviewDP",groups = {"CashMgmt", "CashMgmtRegression"},description="validate Zone Cash Overview",enabled = true)
    public void validateZoneCashOverview(String zoneID, String[] zoneCashID,String[] openingBal, String[] closingBal, String[] issuedToHub, String[] depositByHub, String validCase) {
        SoftAssert softAssert = new SoftAssert();
        Processor p = helper.getZoneCashOverviewHelper(zoneID);
        String get_msg = p.ResponseValidator.GetNodeValue("$.statusMessage");
        int get_status = p.ResponseValidator.GetNodeValueAsInt("$.statusCode");

        if (Boolean.parseBoolean(validCase)){
            String[] get_zoneCashId = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..zoneCashDetails[*]..zoneCashId").replace("}","").replace("[","").replace("{","").replace("]","").replace("\"","").trim().split(",");
            String[] get_openingBal = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..zoneCashDetails[*]..openingBalance").replace("}","").replace("[","").replace("{","").replace("]","").replace("\"","").trim().split(",");
            String[] get_closingBal = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..zoneCashDetails[*]..closingBalance").replace("}","").replace("[","").replace("{","").replace("]","").replace("\"","").trim().split(",");
            String[] get_issuedToHub = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..zoneCashDetails[*]..issuedToHub").replace("}","").replace("[","").replace("{","").replace("]","").replace("\"","").trim().split(",");
            String[] get_depositByHub= p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..zoneCashDetails[*]..depositByHub").replace("}","").replace("[","").replace("{","").replace("]","").replace("\"","").trim().split(",");
            Arrays.sort(zoneCashID);
            Arrays.sort(openingBal);
            Arrays.sort(closingBal);
            Arrays.sort(issuedToHub);
            Arrays.sort(depositByHub);
            //asserts
            softAssert.assertEquals(get_msg,CashMgmtConstant.msg_success, "Msg not success");
            softAssert.assertEquals(get_status,CashMgmtConstant.status200, "status not 200");
            for (int i=0; i<get_zoneCashId.length; i++) {
                softAssert.assertTrue((Arrays.binarySearch(zoneCashID,get_zoneCashId[i])>=0), "zoneCashID not found in response from api.Missing ID:"+get_zoneCashId[i]);
                softAssert.assertTrue((Arrays.binarySearch(openingBal,get_openingBal[i])>=0), "get_openingBal not found in response from api");
                softAssert.assertTrue((Arrays.binarySearch(closingBal,get_closingBal[i])>=0), "get_closingBal not found in response from api");
                softAssert.assertTrue((Arrays.binarySearch(depositByHub,get_depositByHub[i])>=0), "get_openingBal not found in response from api");
                softAssert.assertTrue((Arrays.binarySearch(issuedToHub,get_issuedToHub[i])>=0), "get_issuedToHub not found in response from api");
            }

        } else {
            String get_data = p.ResponseValidator.GetNodeValue("$.data");
            //assert
            softAssert.assertEquals(get_msg,CashMgmtConstant.msg_zoneID_not_found, "zone not found msg not same");
            softAssert.assertEquals(get_status,CashMgmtConstant.status400, "status not 400");
            softAssert.assertNull(get_data,"data not null");
        }
        softAssert.assertAll();
    }

    @Test(dataProvider = "zoneCashLogDP",groups = {"CashMgmt", "CashMgmtRegression"},description="validate Zone Cash Log",enabled = true)
    public void validateZoneCashLog(String zoneID, String[] auditValue,String[] userEmail,String[] userID,String[] timestamp, String validCase) {
        SoftAssert softAssert = new SoftAssert();
        Processor p = helper.getZoneCashLogHelper(zoneID);
        String get_msg = p.ResponseValidator.GetNodeValue("$.statusMessage");
        int get_status = p.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        //TODO: Some other logic in DB query as response is excluding DB values
        if (Boolean.parseBoolean(validCase)){
            String[] get_auditValue = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.logs[*]..value").replace("}","").replace("[","").replace("{","").replace("]","").replace("\"","").trim().split(",");
            String[] get_userEmail = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.logs[*]..userEmail").replace("}","").replace("[","").replace("{","").replace("]","").replace("\"","").trim().split(",");
            String[] get_userID = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.logs[*]..userId").replace("}","").replace("[","").replace("{","").replace("]","").replace("\"","").trim().split(",");
            String[] get_timestamp = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.logs[*]..timestamp").replace("}","").replace("[","").replace("{","").replace("]","").replace("\"","").trim().split(",");
            Arrays.sort(auditValue);
            Arrays.sort(userEmail);
            Arrays.sort(userID);
            Arrays.sort(timestamp);
            softAssert.assertEquals(get_auditValue.length,auditValue.length,"length not same");
            softAssert.assertEquals(get_msg,CashMgmtConstant.msg_success, "Msg not success");
            softAssert.assertEquals(get_status,CashMgmtConstant.status200, "status not 200");
            for (int i = 0; i < auditValue.length; i++) {
                    softAssert.assertTrue((Arrays.binarySearch(auditValue,get_auditValue[i])>=0), "get_auditValue not same");
                    softAssert.assertTrue((Arrays.binarySearch(userEmail,get_userEmail[i])>=0),"get_userEmail not same");
                    softAssert.assertTrue((Arrays.binarySearch(userID,get_userID[i])>=0),"get_userID not same");
                    softAssert.assertTrue((Arrays.binarySearch(timestamp,get_timestamp[i])>=0),"get_timestamp not same");
            }
        } else {
            String get_logs = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.logs").replace("{","").replace("}","").replace("[","").replace("]","");
            //assert
            softAssert.assertEquals(get_msg,CashMgmtConstant.msg_success, "zone not found msg not same");
            softAssert.assertEquals(get_status,CashMgmtConstant.status200, "status not 200 in else");
            softAssert.assertEquals(get_logs,"", "logs not empty string");
        }
        softAssert.assertAll();
    }

    @Test(dataProvider = "zoneCashHubTransactionDP",groups = {"CashMgmt", "CashMgmtRegression"},description="validate Zone Cash Hub Transaction",enabled = true)
    public void validateZoneCashHubTransaction(String json, String zoneID, String amount, String txnType , String validCase) {
        SoftAssert softAssert = new SoftAssert();
        Processor p = helper.addHubTransactionHelper(json);
        int responseCode = p.ResponseValidator.GetResponseCode();
        if (Boolean.parseBoolean(validCase)){
            softAssert.assertEquals(responseCode,200, "response code not 200");
        } else {
            String get_msg = p.ResponseValidator.GetNodeValue("$.statusMessage");
            int get_status = p.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            String get_data = p.ResponseValidator.GetNodeValue("$.data");
            softAssert.assertEquals(get_msg,CashMgmtConstant.msg_invalid_request_params, "msg_invalid_request_params msg not same");
            softAssert.assertEquals(get_status,CashMgmtConstant.statusOne, "status not 1");
            softAssert.assertNull(get_data,"data not null");
        }
        softAssert.assertAll();
    }

    @Test(dataProvider = "reportCityHubDP",groups = {"CashMgmt", "CashMgmtRegression"},description="validate City Hub Report",enabled = true)
    public void validateReportCityHub(String date,String cityID,List<Map<String, Object>> allZones,String validCase) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        HashMap<String,String[]> hm;
        Processor p = helper.getCityHubReportHelper(date,cityID);
        softAssert.assertEquals(p.ResponseValidator.GetResponseCode(),200, "Response code not 200");
        log.info("--===>Response -- "+p.ResponseValidator.GetBodyAsText());
        log.info("--===>Path to File -- "+CashMgmtConstant.path_txt);
        helper.readCSVResponseAndCreateTxt(p,CashMgmtConstant.path_txt);
        hm = helper.parseTxtFileToHashMap(CashMgmtConstant.path_txt);
        if (Boolean.parseBoolean(validCase)) {
            for (int i = 0; i < allZones.size(); i++) {
                String zoneName = allZones.get(i).get("name").toString();
                String zoneId = allZones.get(i).get("id").toString();
                List<Map<String, Object>> list = helper.getCityHubReportFromDB(zoneId, date);
                String[] data = hm.get(zoneName); // get string data from csv response to gotten after api hit
                Log.info("--Asserting data for ::: " + zoneName);
                if (list.size() > 0) {
                    String get_date = list.get(0).get("date").toString();
                    String get_openingBal = list.get(0).get("opening_balance").toString();
                    String get_closingBal = list.get(0).get("closing_balance").toString();
                    String get_issued_to_hub = list.get(0).get("issued_to_hub").toString();
                    String get_deposit_by_hub = list.get(0).get("deposit_by_hub").toString();
                    String get_disbursed_to_de = list.get(0).get("disbursed_to_des").toString();
                    String get_collected_From_de = list.get(0).get("collected_from_des").toString();
                    //asserts
                    softAssert.assertEquals(data[0], get_date, "date not same: " + get_date);
                    softAssert.assertEquals(data[1], zoneName, "zoneName not same: " + zoneName);
                    softAssert.assertEquals(data[2], get_openingBal, "get_openingBal not same: " + get_openingBal);
                    softAssert.assertEquals(data[3], get_issued_to_hub, "get_issued_to_hub not same: " + get_issued_to_hub);
                    softAssert.assertEquals(data[4], get_deposit_by_hub, "get_deposit_by_hub not same: " + get_deposit_by_hub);
                    softAssert.assertEquals(data[5], get_disbursed_to_de, "get_disbursed_to_de not same: " + get_disbursed_to_de);
                    softAssert.assertEquals(data[6], get_collected_From_de, "get_collected_From_de not same: " + get_collected_From_de);
                    softAssert.assertEquals(data[7], get_closingBal, "get_closingBal not same: " + get_closingBal);
                } else {
                    for (int j = 0; j < data.length; j++) {
                        if (j == 1)
                            softAssert.assertEquals(data[j], zoneName);
                        else if (j == 0)
                            softAssert.assertEquals(data[j], date, "date not same");
                        else
                            softAssert.assertEquals(data[j], "0", "data assert 0 has issue");
                    }
                }
            }
            helper.deleteAllCreatedTmpFiles(CashMgmtConstant.path_txt);
        }
        softAssert.assertAll();
    }

    @Test(dataProvider = "pocketingDP",groups = {"CashMgmt", "CashMgmtRegression"},description="validate DE Pocketing",enabled = true)
    public void validatePocketing(String json, String deID, String incoming, String outgoing, String orderID, String fc, String validCase) {
        SoftAssert softAssert = new SoftAssert();
        Processor p = helper.addBulkTransactionHelper(json);
        log.info("--DE_ID ::: "+deID);
        log.info("--FC ::: "+fc);
        List<Map<String, Object>> list;
        list = helper.getDeCashTransactionDetailsFromDB(deID,orderID);
        if (!Boolean.parseBoolean(validCase)) {
            String get_deID = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..deId").replace("}","").replace("[","").replace("{","").replace("]","").replace("\"","").trim();
            String get_incoming = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..deIncoming").replace("}","").replace("[","").replace("{","").replace("]","").replace("\"","").trim();
            String get_outgoing = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..deOutgoing").replace("}","").replace("[","").replace("{","").replace("]","").replace("\"","").trim();
            softAssert.assertEquals(get_deID,deID, "deID not same");
            softAssert.assertEquals(get_incoming,incoming,"incoming not same");
            softAssert.assertEquals(get_outgoing,outgoing,"outgoing not same");
            softAssert.assertTrue((list.size()==0), "list size in de cash transaction is not empty");
        } else {
            softAssert.assertTrue((list.size() > 0), "list size in de cash transaction is empty");
            log.info("--de_cash_transactions incoming ::: " + list.get(0).get("de_incoming"));
            List<Map<String, Object>> list1;
            list1 = helper.getDECashSessionDetailsFromDB(deID);
            String get_fc = list1.get(0).get("floating_cash").toString();
            System.out.println("--get_fc ::: " + get_fc);
            softAssert.assertEquals(Integer.parseInt(get_fc), (Integer.parseInt(fc) + Integer.parseInt(incoming)), "floating cash not as per required in db");
        }
        softAssert.assertAll();
    }

    @Test(dataProvider = "dynamicFloatingCashDP",groups = {"CashMgmt", "CashMgmtRegression"},description="validate Dynamic Floating Cash",enabled = true)
    public void validateDynamicFloatingCash(String fromTime, String toTime, String deID, String zoneID, String attendance) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        JsonHelper jsonHelper = new JsonHelper();
        String get_fc;
        DailyOrderDetails dailyOrderDetails = new DailyOrderDetails();
        dailyOrderDetails.build(fromTime,toTime, Arrays.asList(Integer.parseInt(deID)));
        //to find attendance
        Processor p = helper.getDailyOrderDetailsHelper(jsonHelper.getObjectToJSON(dailyOrderDetails));
        int get_status = p.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String get_data = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data").replace("{","").replace("[","").replace("}","").replace("]","").replace("\"","").trim();
        String get_msg = p.ResponseValidator.GetNodeValue("$.statusMessage");
        softAssert.assertEquals(get_msg,CashMgmtConstant.msg_success, "msg not success");
        softAssert.assertEquals(get_status,CashMgmtConstant.statusZero, "status not 0");
        if (Integer.parseInt(attendance) == 0) {
            softAssert.assertEquals(get_data,"", "data not empty string");
            String[] ufcl_lflc = helper.getUpperLowerFCLimitFromDB(zoneID);
            get_fc = ufcl_lflc[1];
            log.info("--In attendance 0 LFCL = "+get_fc);
            Processor p1 = helper.getFloatingCashDetailsHelper(deID);
            String fc_limit = p1.ResponseValidator.GetNodeValue("$.data.floatingCashLimit");
            log.info("--FC LIMIT from cash_details api = "+fc_limit);
            softAssert.assertEquals(get_fc,fc_limit, "In attendance 0 fc Limit not same");
        } else {
            String get_de = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[*].deId").replace("{", "").replace("}", "").replace("[", "").replace("]", "").trim();
            softAssert.assertEquals(get_de, deID, "DE ID not found or same in attendance 1");
            String[] currentDate = toTime.split("\\s+");
            String fromDate = helper.CalculateStartDateForUnpaidOrderCount(currentDate[0]);
            String fromDateTime = fromDate + " " + currentDate[1];
            log.info("--Calculated From Time ::: " + fromDateTime);
            dailyOrderDetails = new DailyOrderDetails();
            dailyOrderDetails.build(fromDateTime, toTime, Arrays.asList(Integer.parseInt(deID)));
            Processor p1 = helper.getDailyOrderDetailsHelper(jsonHelper.getObjectToJSON(dailyOrderDetails));
            String get_pickedupOrderCount = p1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[*].pickedupOrderCount").replace("{", "").replace("}", "").replace("[", "").replace("]", "").trim();
            String get_ppo = helper.getPayPerOrderFromDB(zoneID);
            log.info("--pickedupOrderCount ::: " + get_pickedupOrderCount);
            int calculate_dynamic_fc = Integer.parseInt(get_pickedupOrderCount) * Integer.parseInt(get_ppo);
            log.info("--Calculated_Dynamic_FC ::: " + calculate_dynamic_fc);
            Processor p2 = helper.getFloatingCashDetailsHelper(deID);
            get_fc = p2.ResponseValidator.GetNodeValue("$.data.floatingCash");
            String get_fc_limit = p2.ResponseValidator.GetNodeValue("$.data.floatingCashLimit");
            String[] ufcl_lflc = helper.getUpperLowerFCLimitFromDB(zoneID);
            if (Integer.parseInt(get_fc) > calculate_dynamic_fc) {
                softAssert.assertEquals(get_fc_limit, ufcl_lflc[1], "lfcl not same as get_fc_limit:" + get_fc_limit);
            } else {
                if (Integer.parseInt(ufcl_lflc[0]) < calculate_dynamic_fc)
                    softAssert.assertEquals(get_fc_limit, ufcl_lflc[0], "ufcl not same as get_fc_limit:" + get_fc_limit);
                else
                    softAssert.assertEquals(get_fc_limit, calculate_dynamic_fc, "calculate_dynamic_fc not same as get_fc:" + get_fc_limit);
            }
        }
        softAssert.assertAll();
    }

    @Test(dataProvider = "dePayoutAdjustmentDP",groups = {"CashMgmt", "CashMgmtRegression"},description="validate DE Payout Adjustment",enabled = true)
    public void validateDePayoutAdjustment(String json, String deID, String incoming, String outgoing, String fc, String validCase) {
        SoftAssert softAssert = new SoftAssert();
        log.info("--DE_ID ::: "+deID);
        log.info("--FC ::: "+fc);
        List<Map<String, Object>> list;
        Processor p = helper.dePayoutAdjustmentHelper(json);
        String get_deID = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..deId").replace("}","").replace("[","").replace("{","").replace("]","").replace("\"","").trim();
        String get_old_fc = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..old_fc").replace("}","").replace("[","").replace("{","").replace("]","").replace("\"","").trim();
        String get_new_fc = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..new_fc").replace("}","").replace("[","").replace("{","").replace("]","").replace("\"","").trim();
        String get_amount_to_adjust = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..amount_to_adjust").replace("}","").replace("[","").replace("{","").replace("]","").replace("\"","").trim();
        String get_de_status = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..de_status").replace("}","").replace("[","").replace("{","").replace("]","").replace("\"","").trim();
        softAssert.assertEquals(get_deID,deID, "de id not same");
        softAssert.assertEquals(Integer.parseInt(get_amount_to_adjust),Integer.parseInt(outgoing), "incoming not equal to adjust value");
        if (Boolean.parseBoolean(validCase)) {
            softAssert.assertEquals(Integer.parseInt(get_old_fc),Integer.parseInt(fc),"get_old_fc not same");
            softAssert.assertEquals(Integer.parseInt(get_new_fc),(Integer.parseInt(fc) + Integer.parseInt(outgoing)),"new fc is not same to total of old + outgoing");
            softAssert.assertEquals(get_de_status, "WORKING", "de status not same");
        } else {
            String get_comment = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..comment").replace("}","").replace("[","").replace("{","").replace("]","").replace("\"","").trim();
            softAssert.assertNull(get_old_fc);
            softAssert.assertNull(get_new_fc);
            softAssert.assertNull(get_de_status);
            softAssert.assertEquals(get_comment,CashMgmtConstant.msg_de_id_not_found, "comment not same");
        }
    }

    /*

    @Test(dataProvider = "blockUnblockDeDP",priority = 10,groups = {"CashMgmt", "CashMgmtRegression"},description="validate Block Unblock DE",enabled = true)
    public void validateBlockUnblockDe(String json, String deID, String fc_limit, String validationType,String block_unblock, String validCase) {
        SoftAssert softAssert = new SoftAssert();
        log.info("--FC_LIMIT ::: "+fc_limit);
        log.info("--DE_ID ::: "+deID);
        if(Boolean.parseBoolean(validCase)) {
            String get_current_fc = helper.getFloatingCashDetailsHelper(deID).ResponseValidator.GetNodeValue("$.data.floatingCash");
            if(validationType.equals("fc less-than equal")) {
                softAssert.assertTrue((Integer.parseInt(get_current_fc) <= Integer.parseInt(fc_limit)));
                if(block_unblock.equals("unblock")) {
                    Processor p1 = helper.unblockDeHelper(json);
                    String get_unblock_msg = p1.ResponseValidator.GetNodeValue("$.statusMessage");
                    int get_unblock_status = p1.ResponseValidator.GetNodeValueAsInt("$.statusCode");
                    softAssert.assertEquals(get_unblock_msg, CashMgmtConstant.msg_success, "Msg not success in unblock");
                    softAssert.assertEquals(get_unblock_status, CashMgmtConstant.status200, "status not 200 in unblock");
                    String deStatus = helper.getDeForceLogoutStatusFromDB(deID);
                    softAssert.assertEquals(Boolean.parseBoolean(deStatus),false, "de status not false in unblock");
                } else {
                    Processor p = helper.blockDeHelper(json);
                    String get_block_msg = p.ResponseValidator.GetNodeValue("$.statusMessage");
                    int get_block_status = p.ResponseValidator.GetNodeValueAsInt("$.statusCode");
                    softAssert.assertEquals(get_block_msg, CashMgmtConstant.msg_success, "Msg not success");
                    softAssert.assertEquals(get_block_status, CashMgmtConstant.status200, "status not 200");
                    String de_status = helper.getDeForceLogoutStatusFromDB(deID);
                    softAssert.assertEquals(Boolean.parseBoolean(de_status),false, "de status not false in block");
                }

            } else if (validationType.equals("fc greater-than")) {
                int delta_txn = Integer.parseInt(fc_limit) - Integer.parseInt(get_current_fc);
                Processor p = cashMgmtHelper.addTransactionHelper(deID,"Adding txn for block-unblock",String.valueOf(dateHelper.getCurrentEpochTimeInSeconds()),String.valueOf(++delta_txn),"0","disburse_cash_tx");
                int get_addtxn_responseCode = p.ResponseValidator.GetResponseCode();
                softAssert.assertEquals(get_addtxn_responseCode,200, "get_addtxn_responseCode not 200");
                String get_new_fc = helper.getFloatingCashDetailsHelper(deID).ResponseValidator.GetNodeValue("$.data.floatingCash");
                softAssert.assertTrue((Integer.parseInt(get_new_fc) > Integer.parseInt(fc_limit)));
                if(block_unblock.equals("unblock")) {
                    Processor p1 = helper.unblockDeHelper(json);
                    String get_unblock_msg = p1.ResponseValidator.GetNodeValue("$.statusMessage");
                    int get_unblock_status = p1.ResponseValidator.GetNodeValueAsInt("$.statusCode");
                    softAssert.assertEquals(get_unblock_msg, CashMgmtConstant.msg_success, "Msg not success in unblock");
                    softAssert.assertEquals(get_unblock_status, CashMgmtConstant.status200, "status not 200 in unblock");
                    String deStatus = helper.getDeForceLogoutStatusFromDB(deID);
                    softAssert.assertEquals(Boolean.parseBoolean(deStatus),true, "de status not true in block");
                } else {
                    Processor p0 = helper.blockDeHelper(json);
                    String get_block_msg = p0.ResponseValidator.GetNodeValue("$.statusMessage");
                    int get_block_status = p0.ResponseValidator.GetNodeValueAsInt("$.statusCode");
                    softAssert.assertEquals(get_block_msg, CashMgmtConstant.msg_success, "Msg not success");
                    softAssert.assertEquals(get_block_status, CashMgmtConstant.status200, "status not 200");
                    String de_status = helper.getDeForceLogoutStatusFromDB(deID);
                    softAssert.assertEquals(Boolean.parseBoolean(de_status),true, "de status not true in block");
                }
            }
        } else {
            if(block_unblock.equals("unblock")) {
                Processor p1 = helper.unblockDeHelper(json);
                String get_unblock_msg = p1.ResponseValidator.GetNodeValue("$.statusMessage");
                int get_unblock_status = p1.ResponseValidator.GetNodeValueAsInt("$.statusCode");
                softAssert.assertEquals(get_unblock_msg, CashMgmtConstant.msg_success, "Msg not success in unblock");
                softAssert.assertEquals(get_unblock_status, CashMgmtConstant.status200, "status not 200 in unblock");
            } else {
                Processor p = helper.blockDeHelper(json);
                String get_block_msg = p.ResponseValidator.GetNodeValue("$.statusMessage");
                int get_block_status = p.ResponseValidator.GetNodeValueAsInt("$.statusCode");
                softAssert.assertEquals(get_block_msg, CashMgmtConstant.msg_success, "Msg not success");
                softAssert.assertEquals(get_block_status, CashMgmtConstant.status200, "status not 200");
            }
        }
        softAssert.assertAll();
    }


*/
    @Test(dataProvider = "closeSessionDP",priority = 11,groups = {"CashMgmt", "CashMgmtRegression"},description="validate Close Session",enabled = true)
    public void validateCloseSession(String deID, String validCase) {
        SoftAssert softAssert = new SoftAssert();
        log.info("--DE_ID ::: "+deID);
        Processor p = cashMgmtHelper.closeSessionHelper(deID);
        if(Boolean.parseBoolean(validCase)){
            int responseCode = p.ResponseValidator.GetResponseCode();
            softAssert.assertEquals(responseCode,200, "response code is not 200");
        } else {
            int responseCode = p.ResponseValidator.GetResponseCode();
            softAssert.assertEquals(responseCode,400, "response code is not 400");
            String get_msg = p.ResponseValidator.GetNodeValue("$.statusMessage");
            int get_status = p.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            String get_data = p.ResponseValidator.GetNodeValue("$.data");
            softAssert.assertEquals(get_msg,CashMgmtConstant.msg_no_open_session, "msg_no_open_session msg not same");
            softAssert.assertEquals(get_status,CashMgmtConstant.statusOne, "status not 1");
            softAssert.assertNull(get_data,"data not null");

        }
    }

    @AfterClass
    public void deleteCreatedFiles() {
        helper.deleteAllCreatedTmpFiles(CashMgmtConstant.path_txt);
    }


}
