package com.swiggy.api.erp.cms.tests.PriceParity;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.constants.PriceparityConstants;
import com.swiggy.api.erp.cms.helper.BaseServiceHelper;
import com.swiggy.api.erp.cms.helper.PriceParityHelper;
import com.swiggy.api.erp.cms.helper.SelfServiceHelper;
import com.swiggy.api.erp.cms.pojo.BaseServiceItems.Item;
import com.swiggy.api.erp.cms.pojo.BaseServiceItems.Items;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.JsonValidator;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.DBHelper;
import framework.gameofthrones.Tyrion.ExcelHelper;
import framework.gameofthrones.Tyrion.JsonHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;
import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PriceParityTest {

    private static Logger log = LoggerFactory.getLogger(PriceParityTest.class);

    PriceParityHelper priceParityHelper = new PriceParityHelper();
    JsonHelper jsonHelper = new JsonHelper();
    BaseServiceHelper baseServiceHelper = new BaseServiceHelper();
    SelfServiceHelper selfServiceHelper = new SelfServiceHelper();
    static String scrapeData = "";
    SqlTemplate sqlTemplateCI = null;
    final int agentIdMapping = PriceparityConstants.agentIdMapping;
    final int agentIdVerification = PriceparityConstants.agentIdVerification;

    @BeforeClass
    public void dataSetupForPriceParity() throws IOException {
        log.debug("Setting Up Data For Price Parity.. ");
        sqlTemplateCI = SystemConfigProvider.getTemplate("competitive_intelligence");
        priceParityHelper.resetPriceData();
        File file = new File(this.getClass().getClassLoader().getResource("cms/scrapedata.json").getFile());
        scrapeData = FileUtils.readFileToString(file);
    }

    @Test(description = "Update Price More than the competitor Price and check E2E Flow", priority = 1)
    public void testOneItemUpdatePriceMoreThanCompetitorE2EFlow() throws IOException, InterruptedException, ParseException, SQLException {
        int restaurantId = PriceparityConstants.restaurantId_pp_basic;
        int compRestaurantId = PriceparityConstants.compRestaurantId_basic;
        priceParityHelper.resetPriceData();
        Items items = new Items();
        List<Item> itemsList = new ArrayList<>();
        Item item = new Item(5875585, "Babycorn Pudina Dry", restaurantId, 220.0, null);
        itemsList.add(item);
        items.setItems(itemsList);
        String itemsString = jsonHelper.getObjectToJSON(items);
        BaseServiceHelper baseServiceHelper = new BaseServiceHelper();
        Processor processor = baseServiceHelper.bulkUpdateItems(itemsString);
        Thread.sleep(1000);
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200, "Response Code Should be 200");

        String scheduleTime = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm")
                .withZone(ZoneId.systemDefault()).format(Instant.now().minusSeconds(120));
        selfServiceHelper.updateAgentParams(1, 1, 0, 1, agentIdMapping + "," + agentIdVerification);
        String workflowId = priceParityHelper.scheduleMapping(scheduleTime, restaurantId);

        List<Map<String, Object>> itemList = sqlTemplateCI.queryForList("select distinct(item_id) from event_queue where id in (select event_queue_id from event_workflow_map where workflow_id='" + workflowId + "')");
        List itemIds = priceParityHelper.listMapToArray(itemList, "item_id");
        Assert.assertTrue(DBHelper.pollDB("competitive_intelligence",
                "select * from workflow where workflow_id='" + workflowId + "' and " +
                        "workflow_stage='MANUAL_MAPPING'", "workflow_status", "INITIATED", 10, 600), "Status Should change to Manual Mapping");

        sqlTemplateCI.update("update scrape_data set data='" + scrapeData + "' where workflow_id='" + workflowId + "'");
        long ticketId = priceParityHelper.getAgentTicketId(workflowId, "PRICE_PARITY_MANUAL_MAPPING");
        Processor processorCreateMap = priceParityHelper.createMapping(workflowId, itemIds.toString(), restaurantId, compRestaurantId);
        Assert.assertEquals(processorCreateMap.ResponseValidator.GetResponseCode(), 200);
        Map<String, Object> ticketAssignmentDetails = initiateManualMapping(ticketId, 1);

        long verificationTicketId = initiateVerification(workflowId, ticketId, restaurantId, compRestaurantId, agentIdMapping, ticketAssignmentDetails.get("ticket_group_id").toString());

        ExcelHelper excelHelper = new ExcelHelper();
        List<Map<String, String>> verificationSheet = excelHelper.getExcelList("/tmp/verified.xlsx", "Menu_Item_Verification");
        Assert.assertTrue(verificationSheet.size() == 1, "1 Rows should be there in Menu Item Mapping Sheet");

        Map<String, String> itemsInSheet = verificationSheet.iterator().next();
        SoftAssert sft = new SoftAssert();
        sft.assertEquals(itemsInSheet.get("entityType"), itemsInSheet.get("cEntityType"));
        sft.assertEquals(itemsInSheet.get("itemName"), itemsInSheet.get("cItemName"));
        sft.assertEquals(itemsInSheet.get("isDisparate"), "1");
        sft.assertEquals(itemsInSheet.get("disparityMargin"), "26.25");
        sft.assertEquals(itemsInSheet.get("subtotal"), "231.0");
        sft.assertAll();
        uploadAndApproveVerificationTicket(workflowId, restaurantId, compRestaurantId, verificationTicketId, agentIdVerification, ticketAssignmentDetails.get("ticket_group_id").toString());

        Assert.assertTrue(DBHelper.pollDB("competitive_intelligence",
                "select * from restaurant_price_disparity where workflow_id='"+workflowId+"'", "workflow_id", workflowId, 10, 600), "Status Should change to Manual Mapping");

        Map<String, Object> priceDisparityMap = sqlTemplateCI.queryForMap("select * from restaurant_price_disparity where workflow_id='" + workflowId + "'");
        Map<String, Object> degradationMap    = sqlTemplateCI.queryForMap("select * from  restaurant_degradation where rest_id=" + restaurantId);
        byte[] blob = (byte[]) priceDisparityMap.get("disparity_data");
        String disparityData = new String(blob);

        System.out.println("Disparity Data : " + disparityData);
        //Validating Restaurant Disparity Data
        sft = new SoftAssert();
        JsonValidator js = new JsonValidator(disparityData);

        sft.assertEquals(js.GetNodeValue("$.[0].restId"), ""+restaurantId);
        sft.assertEquals(js.GetNodeValue("$.[0].itemName"), "Babycorn Pudina Dry");
        sft.assertEquals(js.GetNodeValue("$.[0].price"), "220.0");
        sft.assertEquals(js.GetNodeValue("$.[0].tax"), "11.0");
        sft.assertEquals(js.GetNodeValue("$.[0].packagingCharge"), "0.0");

        sft.assertEquals(degradationMap.get("action_status").toString(), "NA");
        sft.assertEquals(degradationMap.get("disparity_count").toString(), "1");

        sqlTemplateCI.update("update restaurant_price_disparity set email_count=3 where workflow_id='" + workflowId + "'");
        Assert.assertTrue(DBHelper.pollDB("competitive_intelligence",
                "select * from restaurant_price_disparity where workflow_id='" + workflowId + "'", "email_count", "4", 20, 600), "Check for 4 Email Sent");

        degradationMap = sqlTemplateCI.queryForMap("select * from  restaurant_degradation where rest_id=" + restaurantId);
        sft.assertEquals(degradationMap.get("action_status").toString(), "PENALIZE");
        sft.assertEquals(degradationMap.get("penalized_count").toString(), "1");
        sft.assertAll();
    }

    @Test(description = "Update Multiple Item Both Price More/Less/Same than the competitor Price and check E2E Flow", priority = 2)
    public void testMultipleItemUpdateMultiPriceThanCompetitorE2EFlow() throws IOException, InterruptedException {
        int restaurantId = PriceparityConstants.restaurantId_pp_basic;
        int compRestaurantId = PriceparityConstants.compRestaurantId_basic;
        Items items = new Items();
        List<Item> itemsList = new ArrayList<>();
        itemsList.add(new Item(5875589, "Paneer Manchurian", restaurantId, 230.0, null));
        itemsList.add(new Item(5875606, "Mutton Fry", restaurantId, 250.0, 20.0));
        itemsList.add(new Item(5875608, "Fish Fry", restaurantId, 350.0, null));
        itemsList.add(new Item(5875620, "Dal Fry", restaurantId, 170.0, null));

        items.setItems(itemsList);
        String itemsString = jsonHelper.getObjectToJSON(items);
        Processor processor = baseServiceHelper.bulkUpdateItems(itemsString);
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200, "Response Code Should be 200");

        String scheduleTime = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm")
                .withZone(ZoneId.systemDefault()).format(Instant.now().minusSeconds(120));
        selfServiceHelper.updateAgentParams(1, 1, 0, 1, agentIdMapping + "," + agentIdVerification);
        String workflowId = priceParityHelper.scheduleMapping(scheduleTime, restaurantId);
        log.debug("WorkFlow ID: " + workflowId);

        List<Map<String, Object>> itemList = sqlTemplateCI.queryForList("select distinct(item_id) from event_queue where id in (select event_queue_id from event_workflow_map where workflow_id='" + workflowId + "')");
        List itemIds = priceParityHelper.listMapToArray(itemList, "item_id");
        Assert.assertTrue(DBHelper.pollDB("competitive_intelligence",
                "select * from workflow where workflow_id='" + workflowId + "' and " +
                        "workflow_stage='MANUAL_MAPPING'", "workflow_status", "INITIATED", 10, 600), "Status Should change to Manual Mapping");

        sqlTemplateCI.update("update scrape_data set data='" + scrapeData + "' where workflow_id='" + workflowId + "'");
        long ticketId = priceParityHelper.getAgentTicketId(workflowId, "PRICE_PARITY_MANUAL_MAPPING");
        Processor processorCreateMap = priceParityHelper.createMapping(workflowId, itemIds.toString(), restaurantId, compRestaurantId);
        Assert.assertEquals(processorCreateMap.ResponseValidator.GetResponseCode(), 200);
        Map<String, Object> ticketAssignmentDetails = initiateManualMapping(ticketId, 3);

        long ticketIdVerification = initiateVerification(workflowId, ticketId, restaurantId, compRestaurantId, agentIdMapping, ticketAssignmentDetails.get("ticket_group_id").toString());
        ExcelHelper excelHelper = new ExcelHelper();
        List<Map<String, String>> verificationSheet = excelHelper.getExcelList("/tmp/verified.xlsx", "Menu_Item_Verification");
        Assert.assertTrue(verificationSheet.size() == 3, "3 Rows should be there in Menu Item Mapping Sheet");

        SoftAssert sft = new SoftAssert();
        sft.assertEquals(verificationSheet.get(0).get("entityType"), verificationSheet.get(0).get("cEntityType"));
        sft.assertEquals(verificationSheet.get(1).get("entityType"), verificationSheet.get(1).get("cEntityType"));
        sft.assertEquals(verificationSheet.get(2).get("entityType"), verificationSheet.get(2).get("cEntityType"));

        sft.assertEquals(verificationSheet.get(0).get("itemName"), verificationSheet.get(0).get("cItemName"));
        sft.assertEquals(verificationSheet.get(1).get("itemName"), verificationSheet.get(1).get("cItemName"));
        sft.assertEquals(verificationSheet.get(2).get("itemName"), verificationSheet.get(2).get("cItemName"));

        sft.assertEquals(verificationSheet.get(0).get("isDisparate"), "1");
        sft.assertEquals(verificationSheet.get(1).get("isDisparate"), "1");
        sft.assertEquals(verificationSheet.get(2).get("isDisparate"), "0");


        sft.assertEquals(verificationSheet.get(0).get("disparityMargin"), "10.5");
        sft.assertEquals(verificationSheet.get(1).get("disparityMargin"), "20.0");
        sft.assertEquals(verificationSheet.get(2).get("disparityMargin"), "-11.58");
        sft.assertAll();
        uploadAndApproveVerificationTicket(workflowId, restaurantId, compRestaurantId, ticketIdVerification, agentIdVerification, ticketAssignmentDetails.get("ticket_group_id").toString());

        Assert.assertTrue(DBHelper.pollDB("competitive_intelligence",
                "select * from restaurant_price_disparity where workflow_id='"+workflowId+"'", "workflow_id", workflowId, 10, 600), "Status Should change to Manual Mapping");


        Map<String, Object> priceDisparity = sqlTemplateCI.queryForMap("select * from  restaurant_price_disparity where workflow_id='"+workflowId+"'");
        byte[] blob = (byte[]) priceDisparity.get("disparity_data");
        String disparityData = new String(blob);
        Assert.assertEquals("[{\"restId\":\"30648\",\"restName\":\"Nandhana Palace\",\"itemName\":\"Babycorn Pudina Dry\",\"variantGroupName\":\"\",\"variantName\":\"\",\"addonGroupName\":\"\",\"addonName\":\"\",\"price\":\"220.0\",\"tax\":\"11.0\",\"packagingCharge\":\"0.0\"},{\"restId\":\"30648\",\"restName\":\"Nandhana Palace\",\"itemName\":\"Paneer Manchurian\",\"variantGroupName\":\"\",\"variantName\":\"\",\"addonGroupName\":\"\",\"addonName\":\"\",\"price\":\"230.0\",\"tax\":\"11.5\",\"packagingCharge\":\"0.0\"},{\"restId\":\"30648\",\"restName\":\"Nandhana Palace\",\"itemName\":\"Mutton Fry\",\"variantGroupName\":\"\",\"variantName\":\"\",\"addonGroupName\":\"\",\"addonName\":\"\",\"price\":\"250.0\",\"tax\":\"12.5\",\"packagingCharge\":\"20.0\"}]", disparityData);
    }

    @Test(description = "Price Correction should change the status to NO-Disparity", priority = 3)
    public void testCorrectItemPrice() throws IOException, InterruptedException {
        int restaurantId = PriceparityConstants.restaurantId_pp_basic;
        int compRestaurantId = PriceparityConstants.compRestaurantId_basic;
        Items items = new Items();
        List<Item> itemsList = new ArrayList<>();
        itemsList.add(new Item(5875585, "Babycorn Pudina Dry", restaurantId, 195.0, null));
        itemsList.add(new Item(5875589, "Paneer Manchurian", restaurantId, 220.0, null));
        itemsList.add(new Item(5875606, "Mutton Fry", restaurantId, 250.0, 0.0));
        itemsList.add(new Item(5875608, "Fish Fry", restaurantId, 360.0, 21.6));
        itemsList.add(new Item(5875620, "Dal Fry", restaurantId, 170.0, null));

        items.setItems(itemsList);
        String itemsString = jsonHelper.getObjectToJSON(items);
        Processor processor = baseServiceHelper.bulkUpdateItems(itemsString);
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200, "Response Code Should be 200");

        String scheduleTime = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm")
                .withZone(ZoneId.systemDefault()).format(Instant.now().minusSeconds(120));
        selfServiceHelper.updateAgentParams(1, 1, 0, 1, agentIdMapping + "," + agentIdVerification);
        String workflowId = priceParityHelper.scheduleMapping(scheduleTime, restaurantId);

        List<Map<String, Object>> itemList = sqlTemplateCI.queryForList("select distinct(item_id) from event_queue where id in (select event_queue_id from event_workflow_map where workflow_id='" + workflowId + "')");
        List itemIds = priceParityHelper.listMapToArray(itemList, "item_id");
        Assert.assertTrue(DBHelper.pollDB("competitive_intelligence",
                "select * from workflow where workflow_id='" + workflowId + "' and " +
                        "workflow_stage='MANUAL_MAPPING'", "workflow_status", "INITIATED", 10, 600), "Status Should change to Manual Mapping");

        sqlTemplateCI.update("update scrape_data set data='" + scrapeData + "' where workflow_id='" + workflowId + "'");
        long ticketId = priceParityHelper.getAgentTicketId(workflowId, "PRICE_PARITY_MANUAL_MAPPING");
        Processor processorCreateMap = priceParityHelper.createMapping(workflowId, itemIds.toString(), restaurantId, compRestaurantId);
        Assert.assertEquals(processorCreateMap.ResponseValidator.GetResponseCode(), 200);
        Map<String, Object> ticketAssignmentDetails = initiateManualMapping(ticketId, 4);

        long ticketIdVerification = initiateVerification(workflowId, ticketId, restaurantId, compRestaurantId, agentIdMapping, ticketAssignmentDetails.get("ticket_group_id").toString());
        ExcelHelper excelHelper = new ExcelHelper();
        List<Map<String, String>> verificationSheet = excelHelper.getExcelList("/tmp/verified.xlsx", "Menu_Item_Verification");
        Assert.assertTrue(verificationSheet.size() == 4, "4 Rows should be there in Menu Item Mapping Sheet");

        SoftAssert sft = new SoftAssert();
        sft.assertEquals(verificationSheet.get(0).get("isDisparate"), "0");
        sft.assertEquals(verificationSheet.get(1).get("isDisparate"), "0");
        sft.assertEquals(verificationSheet.get(2).get("isDisparate"), "0");


        sft.assertEquals(verificationSheet.get(0).get("disparityMargin"), "0.0");
        sft.assertEquals(verificationSheet.get(1).get("disparityMargin"), "0.0");
        sft.assertEquals(verificationSheet.get(2).get("disparityMargin"), "0.0");
        sft.assertAll();
        uploadAndApproveVerificationTicket(workflowId, restaurantId, compRestaurantId, ticketIdVerification, agentIdVerification, ticketAssignmentDetails.get("ticket_group_id").toString());
        Assert.assertTrue(DBHelper.pollDB("competitive_intelligence",
                "select * from restaurant_price_disparity where workflow_id='"+workflowId+"'", "workflow_id", workflowId, 10, 200), "restaurant_price_disparity table should be populated with value");

        sft = new SoftAssert();
        Map<String, Object> degradationMap = sqlTemplateCI.queryForMap("select * from restaurant_degradation where rest_id=" + restaurantId);
        sft.assertEquals(degradationMap.get("action_status").toString(), "RESOLVED");
        sft.assertEquals(degradationMap.get("disparity_count").toString(), "2");
        sft.assertAll();

        //No Mail Should Sent After resolved the ticket
        int emailCount = (Integer) sqlTemplateCI.queryForMap("select * from restaurant_price_disparity where workflow_id='"+workflowId+"'").get("email_count");
        Assert.assertFalse(DBHelper.pollDB("competitive_intelligence",
                "select * from restaurant_price_disparity where workflow_id='"+workflowId+"'", "email_count", ""+(emailCount+1), 20, 200), "Email Count Should not increase");
    }


    @Test(description = "Create Manual Mapping...", priority = 4)
    public void testManualMappingUpdateE2EFlow() throws IOException, InterruptedException {
        int restaurantId = PriceparityConstants.restaurantId_pp_basic;
        int compRestaurantId = PriceparityConstants.compRestaurantId_basic;

        priceParityHelper.resetPriceData();
        Items items = new Items();
        List<Item> itemsList = new ArrayList<>();
        Item item = new Item(5875585, "BabyCorn (Special Dry)", restaurantId, 220.0, null);
        itemsList.add(item);
        items.setItems(itemsList);
        String itemsString = jsonHelper.getObjectToJSON(items);
        BaseServiceHelper baseServiceHelper = new BaseServiceHelper();
        Processor processor = baseServiceHelper.bulkUpdateItems(itemsString);
        Thread.sleep(1000);
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200, "Response Code Should be 200");

        String scheduleTime = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm")
                .withZone(ZoneId.systemDefault()).format(Instant.now().minusSeconds(120));
        selfServiceHelper.updateAgentParams(1, 1, 0, 1, agentIdMapping + "," + agentIdVerification);
        String workflowId = priceParityHelper.scheduleMapping(scheduleTime, restaurantId);

        List<Map<String, Object>> itemList = sqlTemplateCI.queryForList("select distinct(item_id) from event_queue where id in (select event_queue_id from event_workflow_map where workflow_id='" + workflowId + "')");
        List itemIds = priceParityHelper.listMapToArray(itemList, "item_id");
        Assert.assertTrue(DBHelper.pollDB("competitive_intelligence",
                "select * from workflow where workflow_id='" + workflowId + "' and " +
                        "workflow_stage='MANUAL_MAPPING'", "workflow_status", "INITIATED", 10, 600), "Status Should change to Manual Mapping");

        sqlTemplateCI.update("update scrape_data set data='" + scrapeData + "' where workflow_id='" + workflowId + "'");
        long ticketId = priceParityHelper.getAgentTicketId(workflowId, "PRICE_PARITY_MANUAL_MAPPING");
        Processor processorCreateMap = priceParityHelper.createMapping(workflowId, itemIds.toString(), restaurantId, compRestaurantId);
        Assert.assertEquals(processorCreateMap.ResponseValidator.GetResponseCode(), 200);

        selfServiceHelper.updateAgentParams(1, 1, 0, 1, "" + agentIdMapping + "," + agentIdVerification);
        Assert.assertTrue(DBHelper.pollDB("cms", "select * from tickets where id=" + ticketId, "state", "ASSIGNED", 5, 180), "Ticket state should change to Assigned");
        Map<String, Object> ticketAssignmentDetails = priceParityHelper.getTicketAssignmentDetails(ticketId);
        Processor processorAssignmentDetails = priceParityHelper.fetchTicketDetails(ticketId);
        Assert.assertEquals(processorAssignmentDetails.ResponseValidator.GetResponseCode(), 200);
        String s3Data = processorAssignmentDetails.ResponseValidator.GetNodeValue("$.data.data");
        String s3MappingURL = JsonPath.read(s3Data, "$.s3_url");
        Assert.assertTrue(priceParityHelper.downLoadFromS3(s3MappingURL, "/tmp/mapping.xlsx"), "Mapping File should be Downloaded");

        XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(new File("/tmp/mapping.xlsx")));
        String[] arr = {"ITEM", "59812", "Nandhana Palace", "160804312", "Babycorn Pudina Dry"};
        Row row = wb.getSheet("Menu_Item_Mapping").getRow(1);
        for (int i = 0; i < arr.length; i++) {
            Cell cell = row.createCell(i + 13);
            cell.setCellType(Cell.CELL_TYPE_STRING);
            cell.setCellValue(arr[i]);
        }
        FileOutputStream fos = new FileOutputStream(new File("/tmp/mapping.xlsx"));
        wb.write(fos);
        fos.close();

        long verificationTicketId = initiateVerification(workflowId, ticketId, restaurantId, compRestaurantId, agentIdMapping, ticketAssignmentDetails.get("ticket_group_id").toString());

        ExcelHelper excelHelper = new ExcelHelper();
        List<Map<String, String>> verificationSheet = excelHelper.getExcelList("/tmp/verified.xlsx", "Menu_Item_Verification");
        Assert.assertTrue(verificationSheet.size() == 1, "1 Rows should be there in Menu Item Mapping Sheet");

        Map<String, String> itemsInSheet = verificationSheet.iterator().next();
        SoftAssert sft = new SoftAssert();
        sft.assertEquals(itemsInSheet.get("isDisparate"), "1");
        sft.assertEquals(itemsInSheet.get("disparityMargin"), "26.25");
        sft.assertEquals(itemsInSheet.get("subtotal"), "231.0");
        sft.assertAll();
        wb = new XSSFWorkbook(new FileInputStream(new File("/tmp/verified.xlsx")));
        row = wb.getSheet("Menu_Item_Verification").getRow(1);
        Cell cell = row.createCell(45);
        cell.setCellType(Cell.CELL_TYPE_STRING);
        cell.setCellValue("0");
        fos = new FileOutputStream(new File("/tmp/verified.xlsx"));
        wb.write(fos);
        fos.close();
        uploadAndApproveVerificationTicket(workflowId, restaurantId, compRestaurantId, verificationTicketId, agentIdVerification, ticketAssignmentDetails.get("ticket_group_id").toString());
        Assert.assertTrue(DBHelper.pollDB("competitive_intelligence",
                "select * from restaurant_price_disparity where workflow_id='"+workflowId+"'", "workflow_id", workflowId, 10, 600), "Status Should change to Manual Mapping");

        Map<String, Object> degradationMap = sqlTemplateCI.queryForMap("select * from  restaurant_degradation where rest_id=" + restaurantId);
        sft = new SoftAssert();
        sft.assertEquals(degradationMap.get("action_status").toString(), "RESOLVED");
        sft.assertEquals(degradationMap.get("disparity_count").toString(), "0");
        sft.assertAll();
    }

    @Test(description = "Check Price Parity Feature For New Item Addition to Old Restaurant", priority = 5)
    public void testNewItemCreatedToOldRestaurant() throws IOException {
        int restaurantId = PriceparityConstants.restaurantId_pp_basic;
        int compRestaurantId = PriceparityConstants.compRestaurantId_basic;

        Long itemId = baseServiceHelper.getItemId(restaurantId, "Boneless Amaravathi Chicken Fry");
        if (itemId != null) {
            Assert.assertEquals(baseServiceHelper.deleteItemByItemId(String.valueOf(itemId)).ResponseValidator.GetResponseCode(), 200);
        }

        Item item = new Item(null, "Boneless Amaravathi Chicken Fry", restaurantId, 275.0, 20.0);
        item.setCategory_id(885331);
        item.setDefaultValuesBase(restaurantId);
        String itemString = jsonHelper.getObjectToJSON(item);
        Processor processor = baseServiceHelper.createItemHelper(itemString);
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);

        String scheduleTime = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm")
                .withZone(ZoneId.systemDefault()).format(Instant.now().minusSeconds(120));
        String workflowId = priceParityHelper.scheduleMapping(scheduleTime, restaurantId);

        List<Map<String, Object>> itemList = sqlTemplateCI.queryForList("select distinct(item_id) from event_queue where id in (select event_queue_id from event_workflow_map where workflow_id='" + workflowId + "')");
        List itemIds = priceParityHelper.listMapToArray(itemList, "item_id");
        Assert.assertTrue(DBHelper.pollDB("competitive_intelligence",
                "select * from workflow where workflow_id='" + workflowId + "' and " +
                        "workflow_stage='MANUAL_MAPPING'", "workflow_status", "INITIATED", 30, 600), "Status Should change to Manual Mapping");

        sqlTemplateCI.update("update scrape_data set data='" + scrapeData + "' where workflow_id='" + workflowId + "'");
        long ticketId = priceParityHelper.getAgentTicketId(workflowId, "PRICE_PARITY_MANUAL_MAPPING");
        Processor processorCreateMap = priceParityHelper.createMapping(workflowId, itemIds.toString(), restaurantId, compRestaurantId);
        Assert.assertEquals(processorCreateMap.ResponseValidator.GetResponseCode(), 200);

        Map<String, Object> ticketAssignmentDetails = initiateManualMapping(ticketId, 1);

        ticketId = initiateVerification(workflowId, ticketId, restaurantId, compRestaurantId, agentIdMapping, ticketAssignmentDetails.get("ticket_group_id").toString());

        ExcelHelper excelHelper = new ExcelHelper();
        List<Map<String, String>> verificationSheet = excelHelper.getExcelList("/tmp/verified.xlsx", "Menu_Item_Verification");
        Assert.assertTrue(verificationSheet.size() == 1, "1 Rows should be there in Menu Item Mapping Sheet");

        Map<String, String> itemsInSheet = verificationSheet.iterator().next();
        SoftAssert sft = new SoftAssert();
        sft.assertEquals(itemsInSheet.get("entityType"), itemsInSheet.get("cEntityType"));
        sft.assertEquals(itemsInSheet.get("itemName"), itemsInSheet.get("cItemName"));
        sft.assertEquals(itemsInSheet.get("isDisparate"), "1");
        sft.assertEquals(itemsInSheet.get("disparityMargin"), "8.24");
        sft.assertAll();

        uploadAndApproveVerificationTicket(workflowId, restaurantId, compRestaurantId, ticketId, agentIdVerification, ticketAssignmentDetails.get("ticket_group_id").toString());
        Assert.assertTrue(DBHelper.pollDB("competitive_intelligence",
                "select * from restaurant_price_disparity where workflow_id='"+workflowId+"'", "email_count", "1", 10, 100), "Status Should change to Manual Mapping");

        sqlTemplateCI.update("update restaurant_degradation set action_status='EXCEPTION' where rest_id=" + restaurantId);

        //No Mail Should Sent After resolved the ticket
        int emailCount = (Integer) sqlTemplateCI.queryForMap("select * from restaurant_price_disparity where workflow_id='"+workflowId+"'").get("email_count");
        Assert.assertFalse(DBHelper.pollDB("competitive_intelligence",
                "select * from restaurant_price_disparity where workflow_id='"+workflowId+"'", "email_count", ""+(emailCount+1), 20, 100), "Email Count Should not increase");
    }


    private Map<String, Object> initiateManualMapping(long ticketId, int size) throws IOException {
        selfServiceHelper.updateAgentParams(1, 1, 0, 1, "" + agentIdMapping + "," + agentIdVerification);
        Assert.assertTrue(DBHelper.pollDB("cms", "select * from tickets where id=" + ticketId, "state", "ASSIGNED", 5, 180), "Ticket state should change to Assigned");
        Map<String, Object> ticketAssignmentDetails = priceParityHelper.getTicketAssignmentDetails(ticketId);
        Processor processorAssignmentDetails = priceParityHelper.fetchTicketDetails(ticketId);
        Assert.assertEquals(processorAssignmentDetails.ResponseValidator.GetResponseCode(), 200);
        String s3Data = processorAssignmentDetails.ResponseValidator.GetNodeValue("$.data.data");
        String s3MappingURL = JsonPath.read(s3Data, "$.s3_url");
        Assert.assertTrue(priceParityHelper.downLoadFromS3(s3MappingURL, "/tmp/mapping.xlsx"), "Mapping File should be Downloaded");
        ExcelHelper excelHelper = new ExcelHelper();
        List<Map<String, String>> mappingSheet = excelHelper.getExcelList("/tmp/mapping.xlsx", "Menu_Item_Mapping");
        Assert.assertEquals(mappingSheet.size(), size, size + " Rows should be there in Menu Item Mapping Sheet");

        for (Map<String, String> itemsInSheet : mappingSheet) {
            SoftAssert sft = new SoftAssert();
            sft.assertEquals(itemsInSheet.get("entityType"), itemsInSheet.get("cEntityType"));
            sft.assertEquals(itemsInSheet.get("itemName"), itemsInSheet.get("cItemName"));
            sft.assertAll();
        }
        return ticketAssignmentDetails;
    }


    private long initiateVerification(String workflowId, long ticketId, long restaurantId, long compRestaurantId, long agentId, String groupId) throws IOException {
        Processor processorUploadMapping = priceParityHelper.uploadMappingFile(workflowId, "/tmp/mapping.xlsx", restaurantId, compRestaurantId);
        selfServiceHelper.updateAgentParams(1, 1, 0, 1, "" + agentIdVerification);
        Assert.assertEquals(processorUploadMapping.ResponseValidator.GetResponseCode(), 200);
        Assert.assertEquals(processorUploadMapping.ResponseValidator.GetNodeValue("$.statusMessage"), "Success");
        Assert.assertEquals(priceParityHelper.completeMapping(workflowId, "" + restaurantId).ResponseValidator.GetResponseCode(), 200);


        Assert.assertEquals(selfServiceHelper.updateTicket(ticketId, restaurantId, "ASSIGNED", agentId,
                groupId, "APPROVED", "c293820176a2c1f012cbf62175ec9704522101a8").ResponseValidator.GetNodeValue("$.statusMessage"), "Success");
        Assert.assertTrue(DBHelper.pollDB("competitive_intelligence",
                "select * from competitive_intelligence.workflow where workflow_id='" + workflowId + "' and " +
                        "workflow_stage='VERIFICATION';", "workflow_status", "INITIATED", 30, 300), "Status Should change to AUTO_VERIFICATION=COMPLETED");

        ticketId = priceParityHelper.getAgentTicketId(workflowId, "PRICE_PARITY_MAPPING_VERIFICATION");
        Assert.assertTrue(DBHelper.pollDB("cms", "select * from tickets where id=" + ticketId, "state", "ASSIGNED", 5, 180), "Ticket state should change to Assigned");

        Processor processorAssignmentDetails = priceParityHelper.fetchTicketDetails(ticketId);
        Assert.assertEquals(processorAssignmentDetails.ResponseValidator.GetResponseCode(), 200);
        String s3Data = processorAssignmentDetails.ResponseValidator.GetNodeValue("$.data.data");
        String s3VerificationURL = JsonPath.read(s3Data, "$.s3_url");
        Assert.assertTrue(priceParityHelper.downLoadFromS3(s3VerificationURL, "/tmp/verified.xlsx"), "Verification File should be Downloaded");
        return ticketId;
    }


    private void uploadAndApproveVerificationTicket(String workflowId, long restaurantId, long compRestaurantId, long ticketId, long agentId, String groupId) {
        Processor processorUploadVer = priceParityHelper.uploadVerifiedFile(workflowId, "/tmp/verified.xlsx", restaurantId, compRestaurantId);
        Assert.assertEquals(processorUploadVer.ResponseValidator.GetResponseCode(), 200);
        Assert.assertEquals(processorUploadVer.ResponseValidator.GetNodeValue("$.statusMessage"), "Success");
        Assert.assertEquals(priceParityHelper.completeVerification(workflowId, "" + compRestaurantId, "Z", "" + restaurantId).ResponseValidator.GetResponseCode(), 200);
        Assert.assertEquals(selfServiceHelper.updateTicket(ticketId, restaurantId, "ASSIGNED", agentId,
                groupId, "APPROVED", "417a029fa7d014af819aee986fdab5884c1d8298").ResponseValidator.GetNodeValue("$.statusMessage"), "Success");
    }

}
