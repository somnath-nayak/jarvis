package com.swiggy.api.erp.delivery.Pojos;

import java.util.Map;

public class ClusterItem {
    private String referenceId;
    private Map<String, Object> attributes;
    private String type;
    private int enabled;
    private Coordinate coordinate;

    public String getReferenceId() {
        return referenceId;
    }

    public Map<String, Object> getAttributes() {
        return attributes;
    }

    public String getType() {
        return type;
    }

    public int getEnabled() {
        return enabled;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public void setAttributes(Map<String, Object> attributes) {
        this.attributes = attributes;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setEnabled(int enabled) {
        this.enabled = enabled;
    }

    public void setCoordinate(Coordinate coordinate) {
        this.coordinate = coordinate;
    }
}
