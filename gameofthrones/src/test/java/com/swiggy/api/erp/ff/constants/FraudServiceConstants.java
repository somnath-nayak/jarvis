package com.swiggy.api.erp.ff.constants;

public interface FraudServiceConstants {



	String hostName = "oms";
	String userID = "6789";
	String userID1 = "292929";
	String startTime = "00";
	String endTime = "23";
	String startTime_8 = "10";
	String endTime_8 = "15";
	String startTime_9 = "22";
	String endTime_9 = "5";
	String Cityfee_300 = "300";
	String Cityfee_100 = "100";
	String Cityfee_Default = "1000";
	String CityId_5 = "5";
	String CityId_6 = "6";
	String CityId_7 = "7";
	String CityId_8 = "8";
	String CityId_9 = "9";
	String Cityfee_0 = "0";
	String Cityfee_8 = "10";
	String Cityfee_9 = "10";
	String CityId_1 = "1";
	String CityId_2 = "2";
	String CityId_3 = "3";
	String CityId_4 = "3";
	String Default = "default";
	int mockPort = 6668;
	int statusCode = 200;
	String status_0 = "0";
	String status_1 = "1";
	String status_2 = "2";
	String predication_1 = "1.0";
	String predication_2 = "2.0";
	String predication_0 = "0.0";
	int delay = 5000;

	int CityFee_1_300 = 300;
	int CityFee_2_100 = 100;
	int CityFee_3_0 = 0;
	int default_1000 = 1000;

	String CODExpectedFailureMessage = "cod_fallback_dsp_failure";
	String CODExpectedLowConfidenceMessage = "cod_fallback_dsp_low_confidence";
	String CODModelFailure = "model_failure";
	String category1 = "CAT1";
	String category2 = "CAT2";
	String category3 = "CAT3";
	String category4 = "CAT4";
	String category5 = "CAT5";
	String category6 = "CAT6";
	String category7 = "CAT7";
	String category8 = "CAT8";
    String category9 = "CAT9";
    String category10 = "CAT10";
    String category11 = "CAT11";
    String category12 = "CAT12";
    String category13 = "CAT13";
    String category14 = "CAT14";
    String category15 = "CAT15";
    String category16 = "CAT16";

	String value2 = "3";
	String value0 = "0";
	String value3 = "3";
	String value4 = "4";
	String value6 = "6";
	String value7 = "7";
	String value8 = "8";
    String value12 = "12";
    String value13 = "13";
    String value14 = "14";
    String value15 = "0";
    String value16 = "16";

	String deviceId1 = "1324adfe-eaf32dcf-123daaed-adefaea3";
	String deviceId2 = "1324adfe-eaf32dcf";
	String cat_dev_count_1 = "1";
	String cat_dev_count_3 = "3";
	String cat_dev_count_5 = "5";


	String coupon_device_count = "coupon_category_count_map_for_device_id";

	String category_count_01 = "2";
	String category_count_03 = "3";
	String category_count_04 = "5";
	String category_count_06 = "5";
	String category_count_07 = "7";
	String category_count_08 = "9";
    String category_count_0 = "0";
    String category_count_10 = "10";
    String category_count_11 = "11";
    String category_count_12 = "11";
    String category_count_13 = "13";
    String category_count_16 = "16";
	String fraudExchagne = "fds-transaction-updates-queue";
	String afterPaymentConfirmationStaus="TRANSACTION_COMPLETED";
	String afterCancelOrderStatus = "TRANSACTION_REVERTED";
	int database=0;

	String areaId_Kormanagala="1";
	String areaId_BTM="3";
	String areaId_HSR="2";
	String areaId_Ameerpet="20";
	String areaId_Thane="37";


	String areaFee_200="200";
	String areaFee_0="0";
}
