package com.swiggy.api.erp.vms.tests;

import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.swiggy.api.erp.vms.dp.FssaiDP;
import com.swiggy.api.erp.vms.helper.FssaiServicesHelper;

import framework.gameofthrones.Cersei.ToolBox;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;

public class FssaiServicesTest extends FssaiDP {
	SchemaValidatorUtils schemaValidatorUtils = new SchemaValidatorUtils();
	
	@Test(dataProvider="fssaicompliance")
	public void testFssaiCompliance(String accessToken,String restaurantId,int statusCode,String statusMessage ) throws Exception
	{
		FssaiServicesHelper fsssaiObject=new FssaiServicesHelper();
		Processor p=fsssaiObject.fssaiCompliance(accessToken, restaurantId);
		String resp = p.ResponseValidator.GetBodyAsText();
		System.out.println(p.ResponseValidator.GetBodyAsText());
			
		if(0==statusCode) {
			String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/fssaicompliance");
			List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
			Assert.assertTrue(missingNodeList.isEmpty(),
					missingNodeList + " Nodes Are Missing Or Not Matching For LoginApi API");
			boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
			Assert.assertTrue(!isEmpty);
			Assert.assertEquals(p.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
			Assert.assertEquals(p.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
		}
		else {
			Assert.assertEquals(p.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
			Assert.assertEquals(p.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);								
		}
		
	}
	

	
	
	@Test(dataProvider="getFssaiAlert")
	public void testgetFssaiAlert(String accessToken,int statusCode,String statusMessage) throws Exception
	{
		FssaiServicesHelper fsssaiObject=new FssaiServicesHelper();
		Processor p=fsssaiObject.getFssaiAlert(accessToken);
		String resp=p.ResponseValidator.GetBodyAsText();
		if(0==statusCode) {
			String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/fssaialert");
			List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
			Assert.assertTrue(missingNodeList.isEmpty(),
					missingNodeList + " Nodes Are Missing Or Not Matching For LoginApi API");
			boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
			Assert.assertTrue(!isEmpty);
			Assert.assertEquals(p.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
			Assert.assertEquals(p.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
		}
		else {
			Assert.assertEquals(p.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
			Assert.assertEquals(p.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);								
		}
	}
	
	
	//API postman payloads received not working. I will update the collection once I get full working posyman collection from Karthik Kulkarni
	@Test(dataProvider="getFssaiInfo")
	public void testgetFssaiInfo(String accessToken,String restaurantId )
	{
		FssaiServicesHelper fsssaiObject=new FssaiServicesHelper();
		Processor p=fsssaiObject.getFssaiInfo(accessToken, restaurantId);
		System.out.println(p.ResponseValidator.GetBodyAsText());
	}
	
	//API postman payloads received not working. I will update the collection once I get full working posyman collection from Karthik Kulkarni
	@Test(dataProvider="fssaiackUpdate")
	public void testgetFssaiAckUpdate(String accessToken,String payLoad )
	{
		FssaiServicesHelper fsssaiObject=new FssaiServicesHelper();
		Processor p=fsssaiObject.fssaiackUpdate(accessToken, payLoad);
		System.out.println(p.ResponseValidator.GetBodyAsText());
	}
	
	//API postman payloads received not working. I will update the collection once I get full working posyman collection from Karthik Kulkarni
	@Test(dataProvider="fssailicenseUpdate")
	public void testgetFssaiLicenseUpdate(String accessToken,String payLoad )
	{
		FssaiServicesHelper fsssaiObject=new FssaiServicesHelper();
		Processor p=fsssaiObject.updateFssaiLicense(accessToken, payLoad);
		System.out.println(p.ResponseValidator.GetBodyAsText());
	}
	
	
	
	
}
