package com.swiggy.api.erp.cms.pojo.EntityTag;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class EntityTagMapBulkUploadBuilder {
   private EntityTagMapBulk entityTagMapBulk;

   public EntityTagMapBulkUploadBuilder()
   {
       entityTagMapBulk=new EntityTagMapBulk();
   }

   public EntityTagMapBulkUploadBuilder entityTagMap(List<EntityTagMap> entityTagMap)
   {
       ArrayList<EntityTagMap> entityTagMapsEntries = new ArrayList<>();
       for (EntityTagMap entity : entityTagMap) {
           entityTagMapsEntries.add(entity);
       }
       entityTagMapBulk.setEntityTagMap(entityTagMapsEntries);
       return this;
   }

    public EntityTagMapBulkUploadBuilder deleteById(List<DeleteById> deleteByIds)
    {
        ArrayList<DeleteById> deleteByIdEntry = new ArrayList<>();
        for (DeleteById deleteById : deleteByIds) {
            deleteByIdEntry.add(deleteById);
        }
        entityTagMapBulk.setDeleteByIds(deleteByIdEntry);
        return this;
    }

    private void defaultEntityBulkUpload() throws IOException {
        if(null == entityTagMapBulk.getEntityTagMap()){
            List <EntityTagMap> entityTagList=new ArrayList<>();
            EntityTagMap entityTagMap=new EntityTagMap();
            if(entityTagMap.getCategory()==null)
                entityTagMap.setCategory("qq");
            if(entityTagMap.getEntityId()==null)
                entityTagMap.setEntityId(12);
            if(entityTagMap.getEntityType()==null)
                entityTagMap.setEntityType("item");
            if(entityTagMap.getPartitionId()==null)
                entityTagMap.setPartitionId(1);
            if(entityTagMap.getPosition()==null)
                entityTagMap.setPosition(0);
            if(entityTagMap.getTagId()==null)
                entityTagMap.setTagId(12);
            /*if(entityTagMap.getTagName()==null)
                entityTagMap.setTagName("rko");*/
            if(entityTagMap.getUpdatedBy()==null)
                entityTagMap.setUpdatedBy("jitendra");
            entityTagList.add(entityTagMap);
            entityTagMapBulk.setEntityTagMap(entityTagList);
        }
    }

    private void defaultDeleteBulkUpload() throws IOException {
        if(null == entityTagMapBulk.getDeleteByIds()){
            List <DeleteById> deleteByIdsList=new ArrayList<>();
            DeleteById deleteBy=new DeleteById();
            if(deleteBy.getCategory()==null)
                deleteBy.setCategory("qq");
            if(deleteBy.getEntityId()==null)
                deleteBy.setEntityId(12);
            if(deleteBy.getEntityType()==null)
                deleteBy.setEntityType("item");
            if(deleteBy.getPartitionId()==null)
                deleteBy.setPartitionId(1);
            if(deleteBy.getPosition()==null)
                deleteBy.setPosition(0);
            if(deleteBy.getTagId()==null)
                deleteBy.setTagId(12);
            /*if(deleteBy.getTagName()==null)
                deleteBy.setTagName("rko");*/
            if(deleteBy.getUpdatedBy()==null)
                deleteBy.setUpdatedBy("jitendra");
            deleteByIdsList.add(deleteBy);
            entityTagMapBulk.setDeleteByIds(deleteByIdsList);
        }
    }

    public EntityTagMapBulk buildEntityTagMap() throws IOException{
        defaultEntityBulkUpload();
        return entityTagMapBulk;
   }

    public EntityTagMapBulk buildDeleteById() throws IOException{
        defaultDeleteBulkUpload();
        return entityTagMapBulk;
    }
    public EntityTagMapBulk buildAll() throws IOException{
        defaultEntityBulkUpload();
        defaultDeleteBulkUpload();
        return entityTagMapBulk;
    }

}
