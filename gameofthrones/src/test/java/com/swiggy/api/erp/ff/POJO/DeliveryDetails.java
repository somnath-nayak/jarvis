package com.swiggy.api.erp.ff.POJO;


import org.codehaus.jackson.annotate.JsonProperty;

public class DeliveryDetails {

    @JsonProperty("customer_geohash")
    private String customerGeohash;
    @JsonProperty("restaurant_customer_distance_kms")
    private String restaurantCustomerDistanceKms;
    @JsonProperty("city_id")
    private Integer cityId;
    @JsonProperty("area_id")
    private Integer areaId;

    /**
     * No args constructor for use in serialization
     *
     */
    public DeliveryDetails() {
    }

    /**
     *
     * @param cityId
     * @param customerGeohash
     * @param restaurantCustomerDistanceKms
     * @param areaId
     */
    public DeliveryDetails(String customerGeohash, String restaurantCustomerDistanceKms, Integer cityId, Integer areaId) {
        super();
        this.customerGeohash = customerGeohash;
        this.restaurantCustomerDistanceKms = restaurantCustomerDistanceKms;
        this.cityId = cityId;
        this.areaId = areaId;
    }

    @JsonProperty("customer_geohash")
    public String getCustomerGeohash() {
        return customerGeohash;
    }

    @JsonProperty("customer_geohash")
    public void setCustomerGeohash(String customerGeohash) {
        this.customerGeohash = customerGeohash;
    }

    @JsonProperty("restaurant_customer_distance_kms")
    public String getRestaurantCustomerDistanceKms() {
        return restaurantCustomerDistanceKms;
    }

    @JsonProperty("restaurant_customer_distance_kms")
    public void setRestaurantCustomerDistanceKms(String restaurantCustomerDistanceKms) {
        this.restaurantCustomerDistanceKms = restaurantCustomerDistanceKms;
    }

    @JsonProperty("city_id")
    public Integer getCityId() {
        return cityId;
    }

    @JsonProperty("city_id")
    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    @JsonProperty("area_id")
    public Integer getAreaId() {
        return areaId;
    }

    @JsonProperty("area_id")
    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

}