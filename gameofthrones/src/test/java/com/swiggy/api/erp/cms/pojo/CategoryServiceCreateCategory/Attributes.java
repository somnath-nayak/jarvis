package com.swiggy.api.erp.cms.pojo.CategoryServiceCreateCategory;

import com.swiggy.api.erp.cms.constants.CatalogV2Constants;
import java.util.Arrays;

public class Attributes {
    private String[] allowed_values;

    private String name;

    private String display_name;

    private String is_filterable;

    private String[] themes;

    private String type;

    private String is_mandatory;

    private Rules[] rules;

    public String[] getAllowed_values ()
    {
        return allowed_values;
    }

    public void setAllowed_values (String[] allowed_values)
    {
        this.allowed_values = allowed_values;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public String getIs_filterable ()
    {
        return is_filterable;
    }

    public void setIs_filterable (String is_filterable)
    {
        this.is_filterable = is_filterable;
    }

    public String[] getThemes ()
    {
        return themes;
    }

    public void setThemes (String[] themes)
    {
        this.themes = themes;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    public String getIs_mandatory ()
    {
        return is_mandatory;
    }

    public void setIs_mandatory (String is_mandatory)
    {
        this.is_mandatory = is_mandatory;
    }

    public Rules[] getRules ()
    {
        return rules;
    }

    public void setRules (Rules[] rules)
    {
        this.rules = rules;
    }

    public Attributes build(){
        setDefaultValues();
        return this;
    }

    public void setDefaultValues() {
        Rules rules=new Rules();
        rules.build();
    if(this.getName()==null){
        this.setName(CatalogV2Constants.attribute_name);
    }
    if(this.getDisplay_name()==null){
        this.setDisplay_name(CatalogV2Constants.attribute_name);
    }
    if(this.getType()==null){
        this.setType(CatalogV2Constants.type);
    }

    if(this.getAllowed_values()==null){
        this.setAllowed_values(CatalogV2Constants.allowedValues);
    }

    if(this.getThemes()==null){
        this.setThemes(CatalogV2Constants.themes);
    }
    if(this.getRules()==null){
        this.setRules(new Rules[]{rules});
    }

    }


    @Override
   public String toString(){
       return "ClassPojo [allowed_values = "+allowed_values+", name = "+name+", is_filterable = "+is_filterable+", themes = "+themes+", type = "+type+", is_mandatory = "+is_mandatory+", rules = "+rules+"]";
    }
}
