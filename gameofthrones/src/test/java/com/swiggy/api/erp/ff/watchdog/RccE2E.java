package com.swiggy.api.erp.ff.watchdog;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import framework.gameofthrones.Tyrion.DBHelper;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.swiggy.api.erp.ff.constants.OMSConstants;
import com.swiggy.api.erp.ff.dp.RCCDP;
import com.swiggy.api.erp.ff.helper.LOSHelper;
import com.swiggy.api.erp.ff.helper.OMSHelper;
import com.swiggy.api.erp.ff.helper.WatchdogHelper;

import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Tyrion.KafkaHelper;
import framework.gameofthrones.Tyrion.RabbitMQHelper;
import framework.gameofthrones.Tyrion.RedisHelper;



public class RccE2E extends RCCDP {
	
	KafkaHelper kafka = new KafkaHelper();
	OMSHelper omshelper = new OMSHelper();
	RedisHelper redisHelper = new RedisHelper();
	RabbitMQHelper rmqhelper  = new RabbitMQHelper();
	WatchdogHelper wdhelper = new WatchdogHelper();
	DBHelper db = new DBHelper();

	@BeforeClass
	public void disableONP(){
		System.out.println("Disable ONP Rule====================================");
		SystemConfigProvider.getTemplate(OMSConstants.WATCHDOG_DB).execute("UPDATE watchdog.eca SET enabled=0 WHERE name = 'DelayedOrderToDeliveryWF';");
		SystemConfigProvider.getTemplate(OMSConstants.WATCHDOG_DB).execute("UPDATE watchdog.eca SET enabled=0 WHERE name = 'WatchdogRelayOrderToDeliveryWorkflow';");
	}

	@AfterClass
	public void enableONP(){
		System.out.println("Enable ONP Rule====================================");
		SystemConfigProvider.getTemplate(OMSConstants.WATCHDOG_DB).execute("UPDATE watchdog.eca SET enabled=1 WHERE name = 'DelayedOrderToDeliveryWF';");
		SystemConfigProvider.getTemplate(OMSConstants.WATCHDOG_DB).execute("UPDATE watchdog.eca SET enabled=1 WHERE name = 'WatchdogRelayOrderToDeliveryWorkflow';");
	}

	
	@Test(priority = 1, groups = { "sanity", "regression" }, dataProvider="orderAtRiskPlacedOrder", description = "orders at risk")
	public void orderAtRiskPlacedOrder(String message,String order_id) throws IOException, InterruptedException {
		
		//Already placed order watchdog to do nothing
		kafka.sendMessage(OMSConstants.OMSKAFKA, OMSConstants.ORDER_AT_RISK, message);
		Thread.sleep(3000);
        List<Map<String, Object>> instanceId = SystemConfigProvider.getTemplate(OMSConstants.WATCHDOG_DB).queryForList("SELECT instance_id FROM watchdog.eca_logs where instance_id = "+ order_id  +";");
        Assert.assertTrue(instanceId.isEmpty());
			
	}
	
	@Test(priority = 2, groups = { "sanity", "regression" }, dataProvider="orderAtRiskNotPlaced", description = "orders at risk")
	public void orderAtRiskNotPlaced(String message,String order_id,String city_id,String role_id) throws IOException, InterruptedException {
		
		//Unplaced order watchdog to assign agent
		omshelper.redisFlushAll(OMSConstants.REDIS);
		SystemConfigProvider.getTemplate(OMSConstants.SERVICE).execute("delete from oms_ordersinqueue where order_id="+order_id+";");
		kafka.sendMessage(OMSConstants.OMSKAFKA, OMSConstants.ORDER_AT_RISK, message);
		Thread.sleep(3000);
		//checking order is present in watchdog_eca_logs table
		List<Map<String, Object>> instanceId = SystemConfigProvider.getTemplate(OMSConstants.WATCHDOG_DB).queryForList("SELECT instance_id,state FROM watchdog.eca_logs where instance_id = "+ order_id  +";");
        Assert.assertEquals(instanceId.get(0).get("instance_id").toString(),order_id);
        Assert.assertEquals(instanceId.get(0).get("state").toString(),"COMPLETED");
        Thread.sleep(3000);
        //checking order is present in ordersinqueue table
        List<Map<String, Object>> postgresOrders = SystemConfigProvider.getTemplate(OMSConstants.SERVICE).queryForList("select order_id,waiting_for from oms_ordersinqueue where order_id = "+order_id+";");
        Assert.assertEquals(String.valueOf(postgresOrders.get(0).get("order_id")), ""+order_id);
        Assert.assertEquals(String.valueOf(postgresOrders.get(0).get("waiting_for")), "BAD_ORDER_FOLLOWUP");
		Thread.sleep(3000);
        //checking whether order entry has created in redis
        String sortedListOrder = redisHelper.Zrange(OMSConstants.REDIS,0,"ORDER_SORTED_SET_"+city_id+"_"+role_id,0,1000).toString();
        System.out.println(sortedListOrder);
        Assert.assertEquals(sortedListOrder.contains(""+order_id), true);
        System.out.println("orderId===================================="+order_id);
			
	}

	@Test(priority = 3, groups = { "sanity", "regression" }, dataProvider="rccCancelledOrder", description = "RCC")
	public void rccCancelledOrder(String order_id_1,String order_id_2,String order_id_3) throws IOException, InterruptedException, TimeoutException {
		
		//purging TestAssignment Queue
		wdhelper.cancelledOrder("123");
		try {
        rmqhelper.purgeQueue("oms","ff-watchdog-cancel-order");
		} catch(NullPointerException e){
			e.printStackTrace();
		}
        //Deleting entries in orderinqueue table
        SystemConfigProvider.getTemplate(OMSConstants.SERVICE).execute("delete from oms_ordersinqueue where order_id="+order_id_1+";");
        SystemConfigProvider.getTemplate(OMSConstants.SERVICE).execute("delete from oms_ordersinqueue where order_id="+order_id_2+";");
        SystemConfigProvider.getTemplate(OMSConstants.SERVICE).execute("delete from oms_ordersinqueue where order_id="+order_id_3+";");
		Thread.sleep(3000);
		//push message to watchdog queue
		wdhelper.cancelledOrder(order_id_1);
		//checking orderId exist in watchdog_eca_logs table
		Boolean instanceId = DBHelper.pollDB(OMSConstants.WATCHDOG_DB,"SELECT instance_id,state FROM watchdog.eca_logs where instance_id = "+ order_id_1,"state","COMPLETED",15,45);
		Assert.assertTrue(instanceId);
			
	}

}
