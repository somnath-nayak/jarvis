package com.swiggy.api.erp.cms.pojo.Items;

import com.swiggy.api.erp.cms.constants.MenuConstants;

/**
 * Created by kiran.j on 2/9/18.
 */
public class Entity
{
    private boolean in_stock;

    private int is_veg;

    private int addon_free_limit;

    private boolean enable;

    private String category_id;

    private String id;

    private double price;

    private String image_url;

    private int addon_limit;

    private String description;

    private String sub_category_id;

    private String name;

    private Gst_details gst_details;

    private double packing_charges;

    private boolean has_variant;

    public boolean getIn_stock ()
    {
        return in_stock;
    }

    public void setIn_stock (boolean in_stock)
    {
        this.in_stock = in_stock;
    }

    public int getIs_veg ()
    {
        return is_veg;
    }

    public void setIs_veg (int is_veg)
    {
        this.is_veg = is_veg;
    }

    public int getAddon_free_limit ()
    {
        return addon_free_limit;
    }

    public void setAddon_free_limit (int addon_free_limit)
    {
        this.addon_free_limit = addon_free_limit;
    }

    public boolean getEnable ()
    {
        return enable;
    }

    public void setEnable (boolean enable)
    {
        this.enable = enable;
    }

    public String getCategory_id ()
    {
        return category_id;
    }

    public void setCategory_id (String category_id)
    {
        this.category_id = category_id;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public double getPrice ()
    {
        return price;
    }

    public void setPrice (double price)
    {
        this.price = price;
    }

    public String getImage_url ()
    {
        return image_url;
    }

    public void setImage_url (String image_url)
    {
        this.image_url = image_url;
    }

    public int getAddon_limit ()
    {
        return addon_limit;
    }

    public void setAddon_limit (int addon_limit)
    {
        this.addon_limit = addon_limit;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public String getSub_category_id ()
    {
        return sub_category_id;
    }

    public void setSub_category_id (String sub_category_id)
    {
        this.sub_category_id = sub_category_id;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public Gst_details getGst_details ()
    {
        return gst_details;
    }

    public void setGst_details (Gst_details gst_details)
    {
        this.gst_details = gst_details;
    }

    public double getPacking_charges ()
    {
        return packing_charges;
    }

    public void setPacking_charges (double packing_charges)
    {
        this.packing_charges = packing_charges;
    }

    public boolean getHas_variant ()
    {
        return has_variant;
    }

    public void setHas_variant (boolean has_variant)
    {
        this.has_variant = has_variant;
    }

    public Entity build() {
        setDefaultValues();
        return this;
    }

    public void setDefaultValues() {
        Gst_details gst_details = new Gst_details();
        if(this.getCategory_id() == null)
            this.setCategory_id(MenuConstants.items_category_id);
        if(this.getDescription() == null)
            this.setDescription(MenuConstants.items_description);
        if(this.getId() == null)
            this.setId(MenuConstants.items_id);
        if(this.getImage_url() == null)
            this.setImage_url(MenuConstants.items_image_url);
        if(this.getName() == null)
            this.setName(MenuConstants.items_name);
        if(this.getSub_category_id() == null)
            this.setSub_category_id(MenuConstants.items_subcat_id);

        this.setGst_details(gst_details);
        this.setPrice(MenuConstants.default_price);
    }

    @Override
    public String toString()
    {
        return "ClassPojo [in_stock = "+in_stock+", is_veg = "+is_veg+", addon_free_limit = "+addon_free_limit+", enable = "+enable+", category_id = "+category_id+", id = "+id+", price = "+price+", image_url = "+image_url+", addon_limit = "+addon_limit+", description = "+description+", sub_category_id = "+sub_category_id+", name = "+name+", gst_details = "+gst_details+", packing_charges = "+packing_charges+", has_variant = "+has_variant+"]";
    }
}
