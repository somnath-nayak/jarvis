package com.swiggy.api.erp.cms.pojo.CategoryServiceCreateCategory;

import com.swiggy.api.erp.cms.constants.CatalogV2Constants;

public class Rules {
    private String template_id;

    private Params params;

    public String getTemplate_id ()
    {
        return template_id;
    }

    public void setTemplate_id (String template_id)
    {
        this.template_id = template_id;
    }

    public Params getParams ()
    {
        return params;
    }

    public void setParams (Params params)
    {
        this.params = params;
    }

    public Rules build(){
        setDefaultValues();
        return this;
    }

    public void setDefaultValues() {
        Params params=new Params();
        params.build();
        if(this.getTemplate_id()==null){
            this.setTemplate_id(CatalogV2Constants.templateId);
        }
        if(this.getParams()==null){
            this.setParams(params);
        }
    }

      @Override
   public String toString()
   {
       return "ClassPojo [template_id = "+template_id+", params = "+params+"]";
    }
}
