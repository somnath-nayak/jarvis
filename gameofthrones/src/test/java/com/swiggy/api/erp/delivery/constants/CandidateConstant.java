package com.swiggy.api.erp.delivery.constants;

/**
 * Created by kiran.j on 1/30/18.
 */
public interface CandidateConstant {

    String aadhar_no = "123412341234";
    String account_name = "testaccountname";
    String account_no = "10000001756893";
    String qr_code = "123123";
    String bank_name = "HDFC";
    String bicycle_name = "Bike DE";
    String bike_model = "Test Model";
    String bike_reg_no = "123123123";
    String blood_group = "";
    String c_city = "C Test City";
    String c_pincode = "560012";
    String c_state = "C Test State";
    String c_street = "C Street";
    String city = "Test City";
    String contact_no = "8456123408";
    String dl_address = "";
    String dl_issuedon = "";
    String dl_no = "";
    String dl_state = "";
    String dl_type = "";
    String dl_valid = "";
    String dob = "17/09/1991";
    String education_level = "Postgrad";
    String emergency_no = "8456123408";
    String fathers_name = "Test Name";
    String first_name = "TestDE";
    String idfy_ref_id = "212323";
    String ifsc_code = "12312312311";
    String jacket_quantity = "1";
    String jacket_size = "L";
    String languages_known = "Hindi|English";
    String last_name = "TestLastName";
    String martial_status = "Single";
    String mobile_device_type = "";
    String p_city = "Banglore";
    String p_pincode = "569012";
    String p_state = "karanataka";
    String p_street_address = "tata tower 123 7th main";
    String pan_no = "";
    String pan_type = "Permanent";
    String photograph = "";
    String primary_area = "banaswadi";
    String primary_source = "Online Adverts";
    String proof_address = "";
    String proof_dl = "";
    String proof_pan = "";
    String proof_rc = "";
    String raincoat_quantity = "1";
    String raincoat_size = "L";
    String rc_number = "";
    String rc_type = "";
    String referrer_id = "";
    String referrer_name = "";
    String relationwith_emergency = "asdfghjkwe";
    String secondary_area = "";
    String security_deposit = "10000";
    String security_deposit_receiptno = "1";
    String shift_type = "Part Time";
    String shifts = "Bangalore-FT1,Bangalore-Night,Gurgaon-Night";
    String simno = "";
    String tshirt_quantity = "1";
    String tshirt_size = "L";
    String voterid_no = "";
}
