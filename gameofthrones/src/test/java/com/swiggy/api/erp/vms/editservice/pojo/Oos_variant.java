package com.swiggy.api.erp.vms.editservice.pojo;


import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"oos_group_id",
"oos_variations",
"alternate_variations"
})
public class Oos_variant {

@JsonProperty("oos_group_id")
private long oos_group_id;
@JsonProperty("oos_variations")
private List<Long> oos_variations = null;
@JsonProperty("alternate_variations")
private List<Long> alternate_variations = null;

/**
* No args constructor for use in serialization
* 
*/
public Oos_variant() {
}

/**
* 
* @param oos_variations
* @param alternate_variations
* @param oos_group_id
*/
public Oos_variant(long oos_group_id, List<Long> oos_variations, List<Long> alternate_variations) {
super();
this.oos_group_id = oos_group_id;
this.oos_variations = oos_variations;
this.alternate_variations = alternate_variations;
}

@JsonProperty("oos_group_id")
public long getOos_group_id() {
return oos_group_id;
}

@JsonProperty("oos_group_id")
public void setOos_group_id(long oos_group_id) {
this.oos_group_id = oos_group_id;
}

public Oos_variant withOos_group_id(long oos_group_id) {
this.oos_group_id = oos_group_id;
return this;
}

@JsonProperty("oos_variations")
public List<Long> getOos_variations() {
return oos_variations;
}

@JsonProperty("oos_variations")
public void setOos_variations(List<Long> oos_variations) {
this.oos_variations = oos_variations;
}

public Oos_variant withOos_variations(List<Long> oos_variations) {
this.oos_variations = oos_variations;
return this;
}

@JsonProperty("alternate_variations")
public List<Long> getAlternate_variations() {
return alternate_variations;
}

@JsonProperty("alternate_variations")
public void setAlternate_variations(List<Long> alternate_variations) {
this.alternate_variations = alternate_variations;
}

public Oos_variant withAlternate_variations(List<Long> alternate_variations) {
this.alternate_variations = alternate_variations;
return this;
}

@Override
public String toString() {
return new ToStringBuilder(this).append("oos_group_id", oos_group_id).append("oos_variations", oos_variations).append("alternate_variations", alternate_variations).toString();
}

@Override
public int hashCode() {
return new HashCodeBuilder().append(oos_variations).append(alternate_variations).append(oos_group_id).toHashCode();
}

@Override
public boolean equals(Object other) {
if (other == this) {
return true;
}
if ((other instanceof Oos_variant) == false) {
return false;
}
Oos_variant rhs = ((Oos_variant) other);
return new EqualsBuilder().append(oos_variations, rhs.oos_variations).append(alternate_variations, rhs.alternate_variations).append(oos_group_id, rhs.oos_group_id).isEquals();
}

}