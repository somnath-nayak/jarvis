package com.swiggy.api.erp.cms.tests;

import java.util.List;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.swiggy.api.erp.cms.dp.FullMenuPositiveDp;
import com.swiggy.api.erp.cms.helper.FullMenuHelper;
import com.swiggy.api.erp.cms.pojo.FullMenu.Items;

import framework.gameofthrones.JonSnow.Processor;

/**
 * Created by HemaGovinadaraj on 14/2/18.
 */
public class FullMenuAdditionalTestCases extends FullMenuPositiveDp{
	@Test(dataProvider = "Cat_SubCat_cases", description = "Test cases for categories adn subcategoreirs")
	public void CatSubCatCases(String rest_id, String json_payload, String[] errors, List<String> ids,Items[] items,boolean ItemPassOrFail)  throws Exception {
		FullMenuHelper fullMenuHelper = new FullMenuHelper();
		SoftAssert softAssert = new SoftAssert();
		Processor processor = fullMenuHelper.createFullMenu(rest_id, json_payload);
		if(errors.length == 0)
			softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive1(rest_id, processor, ids,items,ItemPassOrFail));
		else
			softAssert.assertTrue(fullMenuHelper.validateErrors(processor, errors));

		softAssert.assertAll();
	}


	@Test(dataProvider = "Item_related_multiplecase",description = "Items with multi cases")
	public void Item_related_multiplecase(String rest_id, String json_payload, String[] errors, List<String> ids,Items[] items,boolean ItemPassOrFail)throws Exception {
		FullMenuHelper fullMenuHelper = new FullMenuHelper();
		SoftAssert softAssert = new SoftAssert();
		Processor processor = fullMenuHelper.createFullMenu(rest_id, json_payload);
		if(errors.length == 0)
			softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive1(rest_id, processor, ids,items,ItemPassOrFail));
		else
			softAssert.assertTrue(fullMenuHelper.validateErrors(processor, errors));
		softAssert.assertAll();
	}

	@Test(dataProvider = "categorySwitch", description = "Test cases for categories adn subcategoreirs")
	public void CategorySwitch(String rest_id, String json_payload, String[] errors, List<String> ids,Items[] items,boolean ItemPassOrFail)  throws Exception {
		FullMenuHelper fullMenuHelper = new FullMenuHelper();
		SoftAssert softAssert = new SoftAssert();
		Processor processor = fullMenuHelper.createFullMenu(rest_id, json_payload);
		if(errors.length == 0)
			softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive1(rest_id, processor, ids,items,ItemPassOrFail));
		else
			softAssert.assertTrue(fullMenuHelper.validateErrors(processor, errors));

		softAssert.assertAll();
	}


}
