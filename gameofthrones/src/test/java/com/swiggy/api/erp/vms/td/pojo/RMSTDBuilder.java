package com.swiggy.api.erp.vms.td.pojo;

import java.util.ArrayList;
import java.util.List;

/***
 * 
 * @author ramzi
 *
 */
public class RMSTDBuilder {

	private RMSCreateTDEntry rmsCreateTDEntity;

	public RMSTDBuilder()
	{
		rmsCreateTDEntity=new RMSCreateTDEntry();
	}

public RMSTDBuilder setEnabled(String enabled)
{
	rmsCreateTDEntity.setEnabled(enabled);
	return this;
}

public RMSTDBuilder setRestaurantIds(List<Integer>restaurantIds)
{
	rmsCreateTDEntity.setRestaurantIds(restaurantIds);
	return this;
}

public RMSTDBuilder setId(int id)
{
	//rmsCreateTDEntity.setId(id);
	return this;
}
	public RMSTDBuilder setItemName(String itemName){
	rmsCreateTDEntity.setitemName(itemName);
	return this;
	
}

public RMSTDBuilder setValidFrom(Long validFrom)
{
	rmsCreateTDEntity.setValidFrom(validFrom);
	return this;
}

public RMSTDBuilder setValidTill(Long validTill)
{
	rmsCreateTDEntity.setValidTill(validTill);
	return this;
}

public RMSTDBuilder setCampaignType(String campaignType)
{
	rmsCreateTDEntity.setCampaignType(campaignType);
	return this;
}

public RMSTDBuilder setRuleDiscountType(String ruleDiscountType)
{
	rmsCreateTDEntity.setRuleDiscountType(ruleDiscountType);
	return this;
}



public RMSTDBuilder setCreatedBy(Long createdBy)
{
	rmsCreateTDEntity.setCreatedBy(createdBy);
	return this;
}

public RMSTDBuilder setUpdatedBy(Long updatedBy)
{
	rmsCreateTDEntity.setUpdatedBy(updatedBy);
	return this;
}

public RMSTDBuilder setDiscountLevel(String discountLevel)
{
	rmsCreateTDEntity.setDiscountLevel(discountLevel);
	return this;
}

public RMSTDBuilder setRestaurantList(List<Restaurant>restaurantList)
{
	rmsCreateTDEntity.setRestaurantList(restaurantList);
	return this;
}

public RMSTDBuilder setSlots(List<Slots> slots)
{
	rmsCreateTDEntity.setSlots(slots);
	return this;
}

public RMSTDBuilder setFirstOrderRestriction(String firstOrderRestriction)
{
	rmsCreateTDEntity.setFirstOrderRestriction(firstOrderRestriction);
	return this;
}





public RMSTDBuilder setUserRestriction(String userRestriction)
{
	rmsCreateTDEntity.setUserRestriction(userRestriction);
	return this;
}

public RMSTDBuilder setRestaurantFirstOrder(String restaurantFirstOrder)
{
	rmsCreateTDEntity.setRestaurantFirstOrder(restaurantFirstOrder);
	return this;
}

public RMSTDBuilder setRuleDiscount(RuleDiscount ruleDiscount)
{
	rmsCreateTDEntity.setRuleDiscount(ruleDiscount);
	return this;
}

public RMSTDBuilder setRestaurantHit(Integer restaurantHit)
{
	rmsCreateTDEntity.setRestaurantHit(restaurantHit);
	return this;
}

public RMSTDBuilder setSwiggyHit(Integer swiggyHit)
{
	rmsCreateTDEntity.setSwiggyHit(swiggyHit);
	return this;
}

public RMSTDBuilder setTaxesOnDiscountedBill(String taxesOnDiscountedBill)
{
	rmsCreateTDEntity.setTaxesOnDiscountedBill(taxesOnDiscountedBill);

	return this;
}

public RMSTDBuilder setCommissionOnFullBill(String commissionOnFullBill)
{
	rmsCreateTDEntity.setCommissionOnFullBill(commissionOnFullBill);

	return this;
}

public RMSTDBuilder setTimeSlotRestriction(String timeSlotRestriction)
{
	rmsCreateTDEntity.setTimeSlotRestriction(timeSlotRestriction);
	return this;
}

public RMSTDBuilder setDiscountId(String timeSlotRestriction)
{
	rmsCreateTDEntity.setTimeSlotRestriction(timeSlotRestriction);
	return this;
}


public RMSCreateTDEntry build()
{
	defaultData();
	return rmsCreateTDEntity;
}


private void defaultData()
{
	if(rmsCreateTDEntity.getEnabled()==null) {
		rmsCreateTDEntity.setEnabled("true");
	}
	if(rmsCreateTDEntity.getCampaignType()==null) {
		rmsCreateTDEntity.setCampaignType("Percentage");
    }
	
	if(rmsCreateTDEntity.getCreatedBy()==null) {
		rmsCreateTDEntity.setCreatedBy(9886379321l);
	}
	if(rmsCreateTDEntity.getFirstOrderRestriction()==null) {
		rmsCreateTDEntity.setFirstOrderRestriction("false");
    }
	if(rmsCreateTDEntity.getRestaurantFirstOrder()==null) {
		rmsCreateTDEntity.setRestaurantFirstOrder("false");
    }
	
	if(rmsCreateTDEntity.getUserRestriction()==null) {
		rmsCreateTDEntity.setUserRestriction("false");
    }
	
	if(rmsCreateTDEntity.getSlots()==null) {
		rmsCreateTDEntity.setSlots(new ArrayList<>());
	}
	
	if(rmsCreateTDEntity.getTimeSlotRestriction()==null) {
		rmsCreateTDEntity.setTimeSlotRestriction("false");
    }
	if(rmsCreateTDEntity.getCommissionOnFullBill()==null) {
		rmsCreateTDEntity.setCommissionOnFullBill("false");
    }
	if(rmsCreateTDEntity.getTaxesOnDiscountedBill()==null) {
		rmsCreateTDEntity.setTaxesOnDiscountedBill("false");
	}
	if(rmsCreateTDEntity.getSwiggyHit()==null) {
		rmsCreateTDEntity.setSwiggyHit(0);
	}
	if(rmsCreateTDEntity.getRestaurantHit()==null) {
		rmsCreateTDEntity.setRestaurantHit(100);
	}
	if(rmsCreateTDEntity.getitemName()==null) {
		rmsCreateTDEntity.setitemName("");
	}
	
	
	
	
	
}





}
