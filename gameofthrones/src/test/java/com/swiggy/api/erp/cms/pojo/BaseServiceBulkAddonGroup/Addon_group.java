package com.swiggy.api.erp.cms.pojo.BaseServiceBulkAddonGroup;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.erp.cms.pojo.BaseServiceBulkAddonGroup
 **/

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.swiggy.api.erp.cms.helper.BaseServiceHelper;
import org.apache.commons.lang3.builder.ToStringBuilder;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "active",
        "addon_free_limit",
        "addon_limit",
        "addon_min_limit",
        "created_by",
        "item_id",
        "name",
        "order",
        "third_party_id"
})
public class Addon_group {
    BaseServiceHelper baseServiceHelper = new BaseServiceHelper();

    @JsonProperty("active")
    private Boolean active;
    @JsonProperty("addon_free_limit")
    private Integer addon_free_limit;
    @JsonProperty("addon_limit")
    private Integer addon_limit;
    @JsonProperty("addon_min_limit")
    private Integer addon_min_limit;
    @JsonProperty("created_by")
    private String created_by;
    @JsonProperty("item_id")
    private Integer item_id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("order")
    private Integer order;
    @JsonProperty("third_party_id")
    private String third_party_id;
    @JsonProperty("id")
    private String id;

    @JsonProperty("active")
    public Boolean getActive() {
        return active;
    }

    @JsonProperty("active")
    public void setActive(Boolean active) {
        this.active = active;
    }

    @JsonProperty("addon_free_limit")
    public Integer getAddon_free_limit() {
        return addon_free_limit;
    }

    @JsonProperty("addon_free_limit")
    public void setAddon_free_limit(Integer addon_free_limit) {
        this.addon_free_limit = addon_free_limit;
    }

    @JsonProperty("addon_limit")
    public Integer getAddon_limit() {
        return addon_limit;
    }

    @JsonProperty("addon_limit")
    public void setAddon_limit(Integer addon_limit) {
        this.addon_limit = addon_limit;
    }

    @JsonProperty("addon_min_limit")
    public Integer getAddon_min_limit() {
        return addon_min_limit;
    }

    @JsonProperty("addon_min_limit")
    public void setAddon_min_limit(Integer addon_min_limit) {
        this.addon_min_limit = addon_min_limit;
    }

    @JsonProperty("created_by")
    public String getCreated_by() {
        return created_by;
    }

    @JsonProperty("created_by")
    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    @JsonProperty("item_id")
    public Integer getItem_id() {
        return item_id;
    }

    @JsonProperty("item_id")
    public void setItem_id(Integer item_id) {
        this.item_id = item_id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("order")
    public Integer getOrder() {
        return order;
    }

    @JsonProperty("order")
    public void setOrder(Integer order) {
        this.order = order;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("third_party_id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("third_party_id")
    public String getThird_party_id() {
        return third_party_id;
    }

    @JsonProperty("third_party_id")
    public void setThird_party_id(String third_party_id) {
        this.third_party_id = third_party_id;
    }

    public void setDefaultValues(int restID) {
        String timestamp = baseServiceHelper.getTimeStampRandomised();
        int catID = baseServiceHelper.createCategoryIdAndReturnInt(restID);
        int subcatID = baseServiceHelper.createSubCategoryAndReturnInt(restID,catID);
        int itemID = Integer.parseInt(baseServiceHelper.createItemIdAndReturnString(restID,catID,subcatID));
        if(this.getThird_party_id() == null)
            this.setThird_party_id("Automation_"+timestamp);
        if(this.getName() == null)
            this.setName("Automation_BaseService_"+timestamp);
        if(this.getActive() == null)
            this.setActive(true);
        if(this.getOrder() == null)
            this.setOrder(0);
        if(this.getAddon_free_limit() == null)
            this.setAddon_free_limit(2);
        if(this.getAddon_limit() == null)
            this.setAddon_limit(10);
        if(this.getAddon_min_limit() == null)
            this.setAddon_min_limit(0);
        if(this.getItem_id() == null)
            this.setItem_id(itemID);
        if(this.getCreated_by() == null )
            this.setCreated_by("Automation_BaseService_Suite");
    }

    public Addon_group build(int restID) {
        this.setDefaultValues(restID);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("active", active).append("addon_free_limit", addon_free_limit).append("addon_limit", addon_limit).append("addon_min_limit", addon_min_limit).append("created_by", created_by).append("item_id", item_id).append("name", name).append("order", order).append("third_party_id", third_party_id).append("id", id).toString();
    }

}
