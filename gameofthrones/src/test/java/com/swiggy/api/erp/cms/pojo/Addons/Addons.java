package com.swiggy.api.erp.cms.pojo.Addons;

/**
 * Created by kiran.j on 2/8/18.
 */
public class Addons
{
    private Entity entity;

    public Entity getEntity ()
    {
        return entity;
    }

    public void setEntity (Entity entity)
    {
        this.entity = entity;
    }

    public Addons build() {
        Entity entity = new Entity();
        entity.build();
        if(this.getEntity() == null)
            this.setEntity(entity);

        return this;
    }

    @Override
    public String toString()
    {
        return "{ entity = "+entity+"}";
    }
}
