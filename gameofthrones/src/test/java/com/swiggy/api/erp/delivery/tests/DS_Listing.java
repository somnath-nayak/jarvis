package com.swiggy.api.erp.delivery.tests;

import java.io.IOException;

import framework.gameofthrones.Daenerys.Initializer;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.delivery.constants.DSListingConstant;
import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.erp.delivery.constants.RTSConstants;
import com.swiggy.api.erp.delivery.dp.DS_Listing_DataProvider;
import com.swiggy.api.erp.delivery.helper.DeliveryHelperMethods;
import com.swiggy.api.erp.delivery.helper.RTSHelper;
import com.swiggy.api.erp.delivery.helper.ServiceablilityHelper;

import framework.gameofthrones.Aegon.Initialize;

public class DS_Listing extends DS_Listing_DataProvider {

	DeliveryHelperMethods delmeth=new DeliveryHelperMethods();
	ServiceablilityHelper serhelp=new ServiceablilityHelper();
	RTSHelper rtshelp = new RTSHelper();
	//Initialize init = new Initialize();
	Initialize init =Initializer.getInitializer();
	
	@Test(dataProvider = "DS_1",priority=27, groups = "DS_Listing", description = "Verify cerebro is returing serviceable when DS send serviceable even when BF exceedes stop BF if restaturant falls under zone where DS listing is enabled")
	public void DSTC_1() throws InterruptedException {
	serhelp.DSserviceable(DSListingConstant.DS_listing_lat, DSListingConstant.DS_listing_lng, DSListingConstant.DS_listing_city_id, DSListingConstant.DS_listing_rest_id).ResponseValidator.GetBodyAsText();
	String processor=serhelp.DSserviceable(DSListingConstant.DS_listing_lat, DSListingConstant.DS_listing_lng, DSListingConstant.DS_listing_city_id, DSListingConstant.DS_listing_rest_id).ResponseValidator.GetBodyAsText();
	String serviceability=checkserviceablility(processor);
    Assert.assertEquals(serviceability, "2","Serviceablility was equal to "+serviceability);
	}
	@Test(dataProvider = "DS_2",priority=28, groups = "DS_Listing", description = "Verify cerebro is returing unserviceable when DS send unserviceable even when BF did not cross stop banner factor if restaurant falls under zone where DS lisiting is enabled")
	public void DSTC_2() throws InterruptedException {
	serhelp.DSserviceable(DSListingConstant.DS_listing_lat, DSListingConstant.DS_listing_lng, DSListingConstant.DS_listing_city_id, DSListingConstant.DS_listing_rest_id).ResponseValidator.GetBodyAsText();
	String processor=serhelp.DSserviceable(DSListingConstant.DS_listing_lat, DSListingConstant.DS_listing_lng, DSListingConstant.DS_listing_city_id, DSListingConstant.DS_listing_rest_id).ResponseValidator.GetBodyAsText();
	String serviceability=checkserviceablility(processor);
	String nonserreason=checknonserviceablilityreason(processor);
	Assert.assertEquals(serviceability.toString(), "1","Serviceablility was equal to "+serviceability.toString());
	Assert.assertEquals(nonserreason.toString(), "7","Non serviceable reason was= "+nonserreason.toString());
			}
	@Test(dataProvider = "DS_3",priority=29, groups = "DS_Listing", description = "Verify Cerebro is returning serviceablility according to fallback logic if cerebro does not get response from DS api")
	public void DSTC_3() throws InterruptedException {
	serhelp.DSserviceable(DSListingConstant.DS_non_listing_lat, DSListingConstant.DS_non_listing_lng, DSListingConstant.DS_listing_city_id, DSListingConstant.DS_non_listing_rest_id).ResponseValidator.GetBodyAsText();
	String processor=serhelp.DSserviceable(DSListingConstant.DS_non_listing_lat, DSListingConstant.DS_non_listing_lng, DSListingConstant.DS_listing_city_id, DSListingConstant.DS_non_listing_rest_id).ResponseValidator.GetBodyAsText();
	String serviceability=checkserviceablility(processor);
	Assert.assertEquals(serviceability.toString(), "2","Serviceablility was equal to "+serviceability.toString());
	}
	@Test(dataProvider = "DS_4",priority=30, groups = "DS_Listing", description = "Verify Cerbro is sending serviceablility according to current logic if rest is of zone which does not fall under DS listing zone")
	public void DSTC_4() throws InterruptedException {
	serhelp.DSserviceable(DSListingConstant.DS_non_listing_lat, DSListingConstant.DS_non_listing_lng, DSListingConstant.DS_listing_city_id, DSListingConstant.DS_non_listing_rest_id).ResponseValidator.GetBodyAsText();
	String processor=serhelp.DSserviceable(DSListingConstant.DS_non_listing_lat, DSListingConstant.DS_non_listing_lng, DSListingConstant.DS_listing_city_id, DSListingConstant.DS_non_listing_rest_id).ResponseValidator.GetBodyAsText();
	String serviceability=checkserviceablility(processor);
	Assert.assertEquals(serviceability.toString(), "2","Serviceablility was equal to "+serviceability.toString());
}
	@Test(dataProvider = "DS_5",priority=31, groups = "DS_Listing", description = "Verify Cerebro response is always serviceable when avgA2DTimeInMins*bannerFactor is less than max sla 65")
	public void DSTC_5() throws InterruptedException {
	serhelp.DSserviceable(DSListingConstant.DS_listing_lat, DSListingConstant.DS_listing_lng, DSListingConstant.DS_listing_city_id, DSListingConstant.DS_listing_rest_id).ResponseValidator.GetBodyAsText();
	String processor=serhelp.DSserviceable(DSListingConstant.DS_listing_lat, DSListingConstant.DS_listing_lng, DSListingConstant.DS_listing_city_id, DSListingConstant.DS_listing_rest_id).ResponseValidator.GetBodyAsText();
	String serviceability=checkserviceablility(processor);
	Assert.assertEquals(serviceability, "2","Serviceablility was equal to "+serviceability);
		}
	@Test(dataProvider = "DS_6",priority=32, groups = "DS_Listing", description = "Verify Cerebro response when avgA2DTimeInMins*bannerFactor is more than max sla(65)")
	public void DSTC_6() throws InterruptedException {
	serhelp.DSserviceable(DSListingConstant.DS_listing_lat, DSListingConstant.DS_listing_lng, DSListingConstant.DS_listing_city_id, DSListingConstant.DS_listing_rest_id).ResponseValidator.GetBodyAsText();
    String processor=serhelp.DSserviceable(DSListingConstant.DS_listing_lat, DSListingConstant.DS_listing_lng, DSListingConstant.DS_listing_city_id, DSListingConstant.DS_listing_rest_id).ResponseValidator.GetBodyAsText();
	String serviceability=checkserviceablility(processor);
	String nonserreason=checknonserviceablilityreason(processor);
	Assert.assertEquals(serviceability.toString(), "1","Serviceablility was equal to "+serviceability.toString());
	Assert.assertEquals(nonserreason.toString(), "7","Non serviceable reason was= "+nonserreason.toString());
				}
	@Test(dataProvider = "DS_7",priority=7,enabled=false, groups = "DS_Listing", description = "Verify BF is handled when no active DE are there (BF is 1234)")
	public void DSTC_7() throws InterruptedException {
	Thread.sleep(10000);
	String processor=serhelp.DSserviceable(DSListingConstant.DS_listing_lat, DSListingConstant.DS_listing_lng, DSListingConstant.DS_listing_city_id, DSListingConstant.DS_listing_rest_id).ResponseValidator.GetBodyAsText();
	Object ser=JsonPath.read(processor,"$.serviceableRestaurants..restaurantId");
	if(null==ser)
		{
			System.out.println("Restaurant not appearing in listing");
			Assert.assertTrue(false);
		}
		else
		{
			Object serviceability=JsonPath.read(processor,"$.serviceableRestaurants..serviceability");
			Object nonserreason=JsonPath.read(processor,"$.serviceableRestaurants..non_serviceable_reason");
			Assert.assertEquals(serviceability.toString(), "1","Serviceablility was equal to "+serviceability.toString());
			Assert.assertEquals(nonserreason.toString(), "7","Non serviceable reason was= "+nonserreason.toString());
			}
		
	}
	@Test(dataProvider = "DS_8",priority=8,enabled=false, groups = "DS_Listing", description = "Verify whether cerebro sends restaurants as unserviceable when no of orders placed are equal to max capacity volume(max order value+active orders) send by DS until Next capacity max volume cron runs again")
	public void DSTC_8() throws InterruptedException {
	Thread.sleep(10000);
	String processor=serhelp.DSserviceable(DSListingConstant.DS_listing_lat, DSListingConstant.DS_listing_lng, DSListingConstant.DS_listing_city_id, DSListingConstant.DS_listing_rest_id).ResponseValidator.GetBodyAsText();
	Object ser=JsonPath.read(processor,"$.serviceableRestaurants..restaurantId");
	if(null==ser)
		{
			System.out.println("Restaurant not appearing in listing");
			Assert.assertTrue(false);
		}
		else
		{
			Object serviceability=JsonPath.read(processor,"$.serviceableRestaurants..serviceability");
			Object nonserreason=JsonPath.read(processor,"$.serviceableRestaurants..non_serviceable_reason");
			Assert.assertEquals(serviceability.toString(), "1","Serviceablility was equal to "+serviceability.toString());
			Assert.assertEquals(nonserreason.toString(), "7","Non serviceable reason was= "+nonserreason.toString());
			}
		
	}
	@Test(dataProvider = "DS_20",priority=20,enabled=false, groups = "DS_Listing", description = "Verify max order volume count is decreased accordingly when a order is placed and serviceability is also updated accordingly")
	public void DSTC_20() throws InterruptedException, IOException {
	
	String d=(String) delmeth.redisconnectget(DSListingConstant.max_order_key, "4", 1);
	int no_of_orders=Integer.parseInt(d);
	String orderid = rtshelp.CreateRTSOrder(RTSConstants.address_id, RTSConstants.mobile, DeliveryConstant.password, RTSConstants.rts_rest_id_int);
	Object order_id=JsonPath.read(orderid,"$.data.order_id").toString().replace("[","").replace("]","");
	if (order_id == null) {
		System.out.println("Order is not created");
		Assert.assertTrue(false);
	}
	else
	{
		int new_max_order_count=no_of_orders-1;
		if(new_max_order_count==0)
		{
			String processor=serhelp.DSserviceable(DSListingConstant.DS_listing_lat, DSListingConstant.DS_listing_lng, DSListingConstant.DS_listing_city_id, DSListingConstant.DS_listing_rest_id).ResponseValidator.GetBodyAsText();
			Object ser=JsonPath.read(processor,"$.serviceableRestaurants..restaurantId");
			if(null==ser)
			{
				System.out.println("Restaurant not appearing in listing");
				Assert.assertTrue(false);
			}
			else
			{
				Object serviceability=JsonPath.read(processor,"$.serviceableRestaurants..serviceability");
				Object nonserreason=JsonPath.read(processor,"$.serviceableRestaurants..non_serviceable_reason");
				Assert.assertEquals(serviceability.toString(), "1","Serviceablility was equal to "+serviceability.toString());
				Assert.assertEquals(nonserreason.toString(), "7","Non serviceable reason was= "+nonserreason.toString());
				}
		
		}
		else
		{
			String processor=serhelp.DSserviceable(DSListingConstant.DS_listing_lat, DSListingConstant.DS_listing_lng, DSListingConstant.DS_listing_city_id, DSListingConstant.DS_listing_rest_id).ResponseValidator.GetBodyAsText();
			Object ser=JsonPath.read(processor,"$.serviceableRestaurants..restaurantId");
			if(null==ser)
			{
				System.out.println("Restaurant not appearing in listing");
				Assert.assertTrue(false);
			}
			else
			{
				Object serviceability=JsonPath.read(processor,"$.serviceableRestaurants..serviceability");
				Object nonserreason=JsonPath.read(processor,"$.serviceableRestaurants..non_serviceable_reason");
				Assert.assertEquals(serviceability.toString(), "2","Serviceablility was equal to "+serviceability.toString());
				Assert.assertEquals(nonserreason, null,"Non serviceable reason was= "+nonserreason.toString());
			}
		
			
		}
	}
	}
	
public String checkserviceablility(String processor)
{
	String serviceability = null;
	Object ser=JsonPath.read(processor,"$.serviceableRestaurants..restaurantId");
	if(null==ser)
		{
			System.out.println("Restaurant not appearing in listing");
			Assert.assertTrue(false);
		}
		else
		{
			Object serv=JsonPath.read(processor,"$.serviceableRestaurants..serviceability");
			serviceability=serv.toString().replace("[", "").replace("]","");
		}
	return serviceability;
}
public String checknonserviceablilityreason(String processor)
{
	String nonserviceability = null;
	Object ser=JsonPath.read(processor,"$.serviceableRestaurants..restaurantId");
	if(null==ser)
		{
			System.out.println("Restaurant not appearing in listing");
			Assert.assertTrue(false);
		}
		else
		{
			Object nonserreason=JsonPath.read(processor,"$.serviceableRestaurants..non_serviceable_reason");
			nonserviceability=nonserreason.toString().replace("[", "").replace("]", "");
	}
	return nonserviceability;	
	}
	
	
}	 
	

