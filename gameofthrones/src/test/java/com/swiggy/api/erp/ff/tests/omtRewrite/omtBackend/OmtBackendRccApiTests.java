package com.swiggy.api.erp.ff.tests.omtRewrite.omtBackend;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.swiggy.api.erp.ff.dp.omtRewrite.OmtBackendData;
import com.swiggy.api.erp.ff.helper.OmtBackendHelper;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import framework.gameofthrones.Tyrion.KafkaHelper;

public class OmtBackendRccApiTests extends OmtBackendData {
	
	Initialize gameofthrones = new Initialize();
	SchemaValidatorUtils schemaValidatorUtils = new SchemaValidatorUtils();
	OmtBackendHelper helper = new OmtBackendHelper();
	
	@Test(priority = 1, groups = { "sanity", "regression" }, dataProvider="filterorder", description = "Filter Rcc orders")
	public void filterOrders(String restaurantId, String usecase) {

		Processor filterOrdersResponse = helper.filterOrders(restaurantId);
		if(usecase.equalsIgnoreCase("validcase")) {
		Assert.assertEquals(String.valueOf(filterOrdersResponse.ResponseValidator.GetResponseCode()), "200",
				"Response code did not match");
		}
		else if ((usecase.equalsIgnoreCase("badcase"))) {
			Assert.assertEquals(String.valueOf(filterOrdersResponse.ResponseValidator.GetResponseCode()), "400",
					"Response code did not match");
		}
			

	}
	
}
