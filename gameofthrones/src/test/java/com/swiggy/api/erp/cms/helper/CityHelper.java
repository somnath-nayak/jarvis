package com.swiggy.api.erp.cms.helper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.rabbitmq.client.AMQP;
import com.swiggy.api.erp.cms.constants.CityConstants;
import com.swiggy.api.erp.cms.constants.CmsConstants;
import com.swiggy.api.erp.cms.pojo.City;
import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.DBHelper;
import framework.gameofthrones.Tyrion.JsonHelper;
import framework.gameofthrones.Tyrion.RabbitMQHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by kiran.j on 3/7/18.
 */
public class CityHelper {

    RabbitMQHelper rmqHelper = new RabbitMQHelper();
    Initialize gameofthrones = new Initialize();
    private static int max_counter = 5;


    public City getDefaultCityPojo() {
        City city = new City();
        city.build();
        return city;
    }

    public String[] buildQuery(HashMap<String, Object> map) {
        String column = "(", value = "(";
        for(Map.Entry<String, Object> entry: map.entrySet()) {
            column += "`" + entry.getKey()+ "`,";
            value += "'"+ entry.getValue()+ "',";
        }
        column = column.substring(0, column.length()-1);
        value = value.substring(0, value.length()-1);
        column += ")";
        value += ")";
        System.out.println(column + value);
        return new String[]{column, value};
    }

    public HashMap<String, Object> getCityMap(City city) {
        Gson gson = new GsonBuilder().create();
        String json = gson.toJson(city);
        HashMap<String,Object> map = new Gson().fromJson(json, HashMap.class);
        return map;
    }

    public int createCity(City city) throws IOException {
        String[] query_params = buildQuery(getCityMap(city));
        int counter = 0;
        String query = "";
        JsonHelper jsonHelper = new JsonHelper();
        query = CityConstants.insert_into + CityConstants.city_table + query_params[0] +CityConstants.values+ query_params[1];
        System.out.println("final query is "+query);
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.execute(query);
        int city_id = getCityId(city.getName());
        city.setId(city_id);
        File file = new File("../Data/Payloads/JSON/createCityArea");
        String queue = FileUtils.readFileToString(file);
        queue = queue.replace("${action}", CityConstants.insert).replace("${data}", jsonHelper.getObjectToJSON(city)).replace("${model}", CityConstants.city);
        System.out.println(queue);
//        rmqHelper.pushMessageToExchange("oms", "swiggy.catalog_changes_exchange", new AMQP.BasicProperties().builder().contentType("application/json"), queue);
        createCitySlots(city_id);
        while(!waitFoDelivery(city_id) && counter < max_counter) {
            rmqHelper.pushMessageToExchange("oms", "swiggy.catalog_changes_exchange", new AMQP.BasicProperties().builder().contentType("application/json"), queue);
            counter++;
        }

        return city_id;
    }

    public int getCityId(String name) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CityConstants.city_id + name + CityConstants.like_end);
        return (list.size() > 0) ? (int) (list.get(0).get(CityConstants.id)) : 0;
    }

    public void createCitySlots(int city_id) {
        String days[] = CmsConstants.days;
        for(String day: days)
            citySlot(Integer.toString(city_id), CmsConstants.open_time, CmsConstants.close_time, day);
    }

    public Processor citySlot(String city_id, String openTime, String closeTime, String day) {
        HashMap<String, String> requestheaders = requestHeaders();
        GameOfThronesService service = new GameOfThronesService("cmsrestaurantservice", "cityslots", gameofthrones);
        Processor processor = new Processor(service, requestheaders, new String[]{city_id, closeTime, day, openTime});
        return processor;
    }

    public HashMap<String, String> requestHeaders()
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        requestheaders.put("user_meta","{\"source\": \"CMS\", \"user\":\"cms-tester\"}");
        return requestheaders;
    }

    public void pushCityUpdateEvent(String city_id) throws IOException {
        Gson gson = new Gson();
        CMSHelper cmsHelper = new CMSHelper();
        JsonHelper jsonHelper = new JsonHelper();
        String json = gson.toJson(cmsHelper.getCityDetails(city_id));
        File file = new File("../Data/Payloads/JSON/createCityArea");
        String queue = FileUtils.readFileToString(file);
        queue = queue.replace("${action}", CityConstants.update).replace("${data}", json).replace("${model}", CityConstants.city_name);
        System.out.println(queue);
        rmqHelper.pushMessageToExchange("oms", "swiggy.catalog_changes_exchange", new AMQP.BasicProperties().builder().contentType("application/json"), queue);
    }

    public boolean waitFoDelivery(int city_id) {
        String query = DeliveryConstant.city_present + city_id;
        boolean status = DBHelper.pollDB(DeliveryConstant.dbname, query, "id", Integer.toString(city_id), 10, 600);
        return status;
    }

}
