package com.swiggy.api.erp.vms.helper;

import com.swiggy.api.erp.vms.constants.PrepTimeConstants;
import com.swiggy.api.erp.vms.constants.RMSConstants;
import com.swiggy.api.sf.checkout.helper.CommonAPIHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import org.codehaus.jackson.JsonNode;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by kiran.j on 8/2/18.
 */
public class RMSPrepTimeHelper {

    Initialize initializer = new Initialize();
    RMSHelper rmsHelper = new RMSHelper();
    String slotIDs;

    public Processor getPrepTimeSlotWise(String prepTimeRestaurantId, String auth) {
        GameOfThronesService services = new GameOfThronesService("rms", "slotwisepreptime", initializer);
        HashMap<String, String> requestheaders =  getDefaultHeaders();
        requestheaders.put("Cookie", "Swiggy_Session-alpha="+auth);
        Processor processor = new Processor(services, requestheaders, null, new String[] { prepTimeRestaurantId });
        return processor;
    }

    public Processor getPrepTimeDayWise(String prepTimeRestaurantId, String auth){
        GameOfThronesService services=new GameOfThronesService("rms", "daywisepreptime", initializer);
        HashMap<String, String> requestheaders =  getDefaultHeaders();
        requestheaders.put("Cookie", "Swiggy_Session-alpha="+auth);
        Processor processor = new Processor(services, requestheaders, null, new String[] { prepTimeRestaurantId });
        return processor;
    }

    public Processor savingPreptime(String prepTimeRestaurantId, String from, String to, String prep_time, String prep_type, String days, String auth){
        GameOfThronesService services=new GameOfThronesService("rms", "savepreptime", initializer);
        HashMap<String, String> requestheaders =  getDefaultHeaders();
        requestheaders.put("Cookie", "Swiggy_Session-alpha="+auth);
        Processor processor = new Processor(services, requestheaders,
                new String[] { from, to, prep_time, prep_type, days }, new String[] { prepTimeRestaurantId });
        return processor;
    }

    public HashMap<String, String> getDefaultHeaders() {
        HashMap<String, String> header = new HashMap<String, String>();
        header.put("Content-Type", "application/json");
        return header;
    }

    public boolean aggregateResponseAndStatusCodes(Processor response) {
        boolean success = false;

        System.out.println("SuccessresponseCode :" + response.ResponseValidator.GetResponseCode());
        System.out.println("SuccessstatusCode : " + response.ResponseValidator.GetNodeValueAsInt("statusCode"));

        if ((RMSConstants.successresponseCode == response.ResponseValidator.GetResponseCode())
                && RMSConstants.successstatusCode == response.ResponseValidator.GetNodeValueAsInt("statusCode")) {
            System.out.println("1..inside response");
            success = true;
        } else if ((RMSConstants.successresponseCode == response.ResponseValidator.GetResponseCode())
                && RMSConstants.failedstatusCode == response.ResponseValidator.GetNodeValueAsInt("statusCode")) {
            System.out.println("2..inside response");
            System.out.println("SuccessresponseCode: " + response.ResponseValidator.GetResponseCode());
            System.out.println("SuccessstatusCode :  " + response.ResponseValidator.GetNodeValueAsInt("statusCode"));
            success = true;
        } else if ((RMSConstants.successresponseCode == response.ResponseValidator.GetResponseCode())
                && RMSConstants.failedstatusCode_invalidSession == response.ResponseValidator
                .GetNodeValueAsInt("statusCode")) {
            System.out.println("3..inside response");
            System.out.println("SuccessresponseCode: " + response.ResponseValidator.GetResponseCode());
            System.out.println("SuccessstatusCode :  " + response.ResponseValidator.GetNodeValueAsInt("statusCode"));
            success = true;
        }

        else if((RMSConstants.successresponseCode == response.ResponseValidator.GetResponseCode()) &&
                RMSConstants.failedstatusCode_alreadyExist == response.ResponseValidator.GetNodeValueAsInt("statusCode")){
            System.out.println("4..inside response");
            System.out.println("SuccessresponseCode: " + response.ResponseValidator.GetResponseCode());
            System.out.println("SuccessstatusCode :  " + response.ResponseValidator.GetNodeValueAsInt("statusCode"));
            success = true;
        }
        return success;
    }

    public String[] fetchSlotWiseId(String restId, String auth) {
        String[] slotidarray = null;
        try {
            GameOfThronesService services = new GameOfThronesService("rms", "fetchpreptimemultirest", initializer);
            HashMap<String, String> requestheaders =  getPrepTimeHeaders();
            requestheaders.put("Cookie", "Swiggy_Session-alpha="+auth);
            Processor processor = new Processor(services, requestheaders, new String[] { restId }, null);
            JsonNode json = CommonAPIHelper.convertStringtoJSON(
                    processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("data..prepTimes"));
            System.out.println(json.size());
            slotIDs = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..[?(@.type=='SLOTWISE')].id");

            System.out.println("***************"+slotIDs);
            slotIDs= slotIDs.replaceAll("\\[", "").replaceAll("\\]", "");

            System.out.println(slotIDs);
            slotidarray = slotIDs.split(",");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return slotidarray;
    }

    public Processor fetchSlotWise(String restId, String auth) {
        GameOfThronesService services = new GameOfThronesService("rms", "fetchpreptimemultirest", initializer);
        HashMap<String, String> requestheaders =  getPrepTimeHeaders();
        requestheaders.put("Cookie", "Swiggy_Session-alpha="+auth);
        Processor processor = new Processor(services, requestheaders, new String[] { restId }, null);
        return processor;
    }

    public HashMap<String, String> getPrepTimeHeaders() {
        HashMap<String, String> header = new HashMap<String, String>();
        header.put("Content-Type", "application/json");
        header.put("Authorization", RMSConstants.prep_auth);
        return header;
    }

    public boolean verifySaveResponse(String from, String to, String preptime, String type, String days, Processor processor, boolean ignore) throws IOException {
        boolean success = false;
        success = (processor.ResponseValidator.GetNodeValue("data."+ PrepTimeConstants.from_time).toString().equalsIgnoreCase(from)) &&
                (processor.ResponseValidator.GetNodeValue("data."+PrepTimeConstants.end_time).toString().equalsIgnoreCase(to)) &&
                (processor.ResponseValidator.GetNodeValueAsInt("data."+PrepTimeConstants.prep_time) == Integer.parseInt(preptime)) &&
                (processor.ResponseValidator.GetNodeValue("data."+PrepTimeConstants.type).toString().equalsIgnoreCase(type));
        if(ignore)
            success &= (processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("data."+PrepTimeConstants.days).replace("[","").replace("]","").equalsIgnoreCase(days));
        return success;
    }

    public boolean verifyNegativeResponse(Processor processor) {
        boolean success = false;
        success = (processor.ResponseValidator.GetNodeValueAsInt("statusCode") == PrepTimeConstants.negative_status) &&
                (processor.ResponseValidator.GetNodeValue("statusMessage").equalsIgnoreCase(PrepTimeConstants.negative_message));
        return success;
    }

    public Processor delPreptime(String prepTimeRestaurantId, String auth, String restaurantId){
        GameOfThronesService services=new GameOfThronesService("rms", "deletepreptime", initializer);
        HashMap<String, String> requestheaders =  new HashMap<>();
        requestheaders.put("Cookie", "Swiggy_Session-alpha="+auth);
        Processor processor = new Processor(services, requestheaders, new String[]{restaurantId}, new String[] { prepTimeRestaurantId });
        return processor;
    }

    public void createPrepTimeForRest(String RestId, String auth) {
        String slots[] = fetchSlotWiseId(RestId, auth);
        if(slots.length <= 1) {
            String fromDate, toDate;
            Date date = new Date();
            DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
            fromDate = dateFormat.format(-1);
            toDate = dateFormat.format(date);
            savingPreptime(RestId,fromDate, toDate, RMSConstants.prep_time, RMSConstants.PrepType.GLOBAL.toString(), "\"" + RMSConstants.days[0] + "\"", auth);
            savingPreptime(RestId,fromDate, toDate, RMSConstants.prep_time, RMSConstants.PrepType.SLOTWISE.toString(), "\"" + RMSConstants.days[0] + "\"", auth);
        }
    }

    public Processor updatePrepTime(String from, String to, String prep_time, String prep_type, String days,
                                    String prep_id, String auth, String rest_id) {
        HashMap<String, String> requestheaders =  new HashMap<String, String>();
        requestheaders.put("Cookie", "Swiggy_Session-alpha="+auth);
        GameOfThronesService services = new GameOfThronesService("rms", "updatepreptime", initializer);
        return new Processor(services, requestheaders, new String[] { from, to, prep_time, prep_type, days, rest_id},
                new String[] { prep_id });
    }

    public Processor savingPreptimeMulti(String prepTimeRestaurantId, String from, String to, String prep_time,
                                         String prep_type, String days, String auth) {
        GameOfThronesService services = new GameOfThronesService("rms", "savepreptimemulti", initializer);
        HashMap<String, String> requestheaders =  getDefaultHeaders();
        requestheaders.put("Cookie", "Swiggy_Session-alpha="+auth);
        Processor processor = new Processor(services, requestheaders,
                new String[] { prepTimeRestaurantId, from, to, prep_time, prep_type, days },
                new String[] { prepTimeRestaurantId });
        return processor;
    }
}
