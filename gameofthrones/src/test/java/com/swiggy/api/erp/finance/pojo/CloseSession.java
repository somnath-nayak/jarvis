package com.swiggy.api.erp.finance.pojo;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.erp.finance.pojo
 **/
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "deId",
        "ops"
})
public class CloseSession {

    @JsonProperty("deId")
    private Integer deId;
    @JsonProperty("ops")
    private Ops ops;

    @JsonProperty("deId")
    public Integer getDeId() {
        return deId;
    }

    @JsonProperty("deId")
    public void setDeId(Integer deId) {
        this.deId = deId;
    }

    @JsonProperty("ops")
    public Ops getOps() {
        return ops;
    }

    @JsonProperty("ops")
    public void setOps(Ops ops) {
        this.ops = ops;
    }

    private void setDefaultValues(String deID) {
        Ops ops = new Ops();
        ops.build(Integer.parseInt(deID));
        if (this.getDeId() == null)
            this.setDeId(Integer.parseInt(deID));
        if (this.getOps() == null)
            this.setOps(ops);
    }

    public CloseSession build(String deID) {
        setDefaultValues(deID);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("deId", deId).append("ops", ops).toString();
    }

}
