package com.swiggy.api.erp.cms.constants;

import com.swiggy.api.erp.cms.helper.CMSHelper;

/**
 * Created by kiran.j on 11/3/17.
 */
public class CmsConstants {
    CMSHelper cmshelper=new CMSHelper();
    public static String cmsDB = "cms";
    public static int  restaurantId = 9990; 
    public static String restaurantdIdByName = "SELECT id FROM swiggy.restaurants where name like '%";
    public static String restaurantdIdByNameExact = "SELECT id FROM swiggy.restaurants where name like '";
    public static String endquotes = "'";
    public static String rest = "${restaurant}";
    public static String category = "${category}";
    public static String subcategory = "${subcategory}";
    public static String andId = "and id=";
    public static String and = "and ";
    public static String endMySqlLike = "%'";
    public static String opentime = "open_time";
    public static String closetime = "close_time";
    public static String enabled = "enabled";
    public static String bannermessage = "banner_message";
    public static String isopen = "is_open";
    public static String isveg = "is_veg";
    public static String instock = "in_stock";
    public static String assured = "is_assured";
    public static String swiggyselect = "is_select";
    public static String recommended = "recommended";
    public static String poptype = "POP_MENU";
    public static String optionvalue = "option_value";
    public static String eligiblelong = "eligible_for_long_distance";
    public static String longdistance = "is_long_distance_enabled";
    public static String cityCloseTime = "SELECT close_time FROM swiggy.city where id=";
    public static String cityOpenTime = "SELECT open_time FROM swiggy.city where id=";
    public static String areaOpenTime = "SELECT open_time FROM swiggy.area where id=";
    public static String areaCloseTime = "SELECT close_time FROM swiggy.area where id=";
    public static String restaurantOpenTime = "SELECT open_time FROM swiggy.restaurant_slots where id=";
    public static String restaurantClosedTime = "SELECT close_time FROM swiggy.restaurant_slots where id=";
    public static String areaEnabled = "SELECT enabled FROM swiggy.area where id=";
    public static String cityEnabled = "SELECT enabled FROM swiggy.city where id=";
    public static String restaurantEnabled = "SELECT enabled FROM swiggy.restaurants where id=";
    public static String cityBanner = "SELECT banner_message FROM swiggy.city where id=";
    public static String areaBanner = "SELECT banner_message FROM swiggy.area where id=";
    public static String areaOpen = "SELECT  is_open from swiggy.area where id=";
    public static String cityOpen = "SELECT  is_open from swiggy.city where id=";
    public static String updateAreaOpenTime = "UPDATE swiggy.area set open_time= ";
    public static String updateAreaCloseTime = "UPDATE swiggy.area set close_time= ";
    public static String updateCityOpenTime = "UPDATE swiggy.city set open_time= ";
    public static String updateCityCloseTime = "UPDATE swiggy.city set close_time= ";
    public static String updateRestaurantOpenTime = "UPDATE swiggy.restaurant_slots set open_time= ";
    public static String updateRestaurantCloseTime = "UPDATE swiggy.restaurant_slots set close_time= ";
    public static String updateRestaurantEnabled = "UPDATE swiggy.restaurants set enabled=";
    public static String updateCityEnabled = "UPDATE swiggy.city set enabled=";
    public static String updateAreaEnabled = "UPDATE swiggy.area set enabled=";
    public static String updateAreaBanner = "UPDATE swiggy.area set banner_message='";
    public static String updateCityBanner = "UPDATE swiggy.city set banner_message='";
    public static String whereIdString = "' where id=";
    public static String whereId = " where id=";
    public static String updateAreaOpen ="UPDATE swiggy.area set is_open=";
    public static String updateCityOpen = "UPDATE swiggy.city set is_open=";
    public static String areaslotsOpenHour = "SELECT open_time from swiggy.area_slots where id=";
    public static String areaslotsCloseHour = "SELECT close_time from swiggy.area_slots where id=";
    public static String cityslotsOpenHour = "SELECT open_time from swiggy.city_slots where id=";
    public static String cityslotCloseHour = "SELECT close_time from swiggy.city_slots where id=";
    public static String updateAreaslotOpenHour = "UPDATE swiggy.area_slots set open_time=";
    public static String updateAreaslotCloseHour = "UPDATE swiggy.area_slots set close_time=";
    public static String updateCityslotOpenHour = "UPDATE swiggy.city_slots set open_time=";
    public static String updateCityslotCloseHour = "UPDATE swiggy.city_slots set close_time=";
    public static String getisveg = "SELECT "+ isveg +" from swiggy.items where id=";
    public static String updateisveg = "UPDATE swiggy.items set is_veg=";
    public static String getinstock = "SELECT in_stock from swiggy.items where id=";
    public static String updateinstock = "UPDATE swiggy.items set in_stock=";
    public static String islongdistance = "SELECT is_long_distance_enabled FROM swiggy.restaurants where id=";
    public static String isselect = "SELECT  is_select from swiggy.restaurants where id=";
    public static String isswiggyassured = "SELECT  is_assured from swiggy.restaurants where id=";
    public static String setisselect = "UPDATE swiggy.restaurants set is_select=";
    public static String updatelongdistance = "UPDATE swiggy.restaurants set is_long_distance_enabled=";
    public static String setisassured = "UPDATE swiggy.restaurants set is_assured=";
    public static String itemenable = "SELECT enabled from swiggy.items where id=";
    public static String updateitemenable = "UPDATE swiggy.items set enabled=";
    public static String itemrecommend = "SELECT recommended from swiggy.items where id=";
    public static String updaterecommend = "UPDATE swiggy.items set recommended=";
    public static String eligiblelongdistance = "SELECT eligible_for_long_distance from swiggy.items where id=";
    public static String updateeligiblelongdistance = "UPDATE swiggy.items set eligible_for_long_distance=";
    public static String usermetavalue = "{\"source\": \"CMS\", \"user\":\"cms-tester\"}";
    public static String usermetavalue2 = "{\"source\":\"cms\",\"meta\": {\"user\": \"cms-tester\"}}";
    public static String usermetakey = "user_meta";
    public static String usermetakey2  = "user-meta";
    public static String areaholiday = "SELECT from_time, to_time, area_id FROM swiggy.area_holiday_slots where id=";
    public static String cityholiday = "SELECT from_time, to_time, city_id FROM swiggy.city_holiday_slots where id=";
    public static String deleteUserWPUsers="delete from wp_users where id=";
    public static String deleteUserWPUsersHistory="delete from wp_users_history where user_id=";
    public static String deleteAddresses="delete from addresses where user_id=";
    public static String getUserId="select * from wp_users where mobile=";
    public static String getwpoptions = "SELECT * FROM swiggy.wp_options where option_name='";

    public static String getExclusiveRestaurants = "select id, lat_long from restaurants where is_exclusive = 1 and enabled = 1 limit 5";
    public static String getItems = "select r.id as \"rest_id\", i.id as \"item_id\", i.recommended as \"is_recommended\", i.name as \"item_name\", mt1.name as \"category_name\", mt2.name as \"subcat_name\", mt1.id as \"cat_id\", mt2.id as \"subcat_id\"\n" +
            "from restaurants r\n" +
            "inner join menu_taxonomy as mt on r.menu_id=mt.id\n" +
            "inner join menu_taxonomy as mt1 on mt.id=mt1.parent\n" +
            "inner join menu_taxonomy as mt2 on mt1.id=mt2.parent\n" +
            "inner join item_menu_map as itm on mt2.id=itm.menu_id\n" +
            "inner join items as i on i.id =  itm.item_id\n" +
            "where r.id =";

    public static String getPrice = "select price from swiggy.items where id =23802";


    public static String getCategories = "select DISTINCT mt1.id as  \"cat_id\"\n"+
            "from restaurants r\n" +
            "inner join menu_taxonomy as mt on r.menu_id=mt.id\n" +
            "inner join menu_taxonomy as mt1 on mt.id=mt1.parent\n" +
            "inner join menu_taxonomy as mt2 on mt1.id=mt2.parent\n" +
            "inner join item_menu_map as itm on mt2.id=itm.menu_id\n" +
            "inner join items as i on i.id =  itm.item_id\n" +
            "where r.id =";
    public static String getcatandsubcat = "select  distinct  r.id as \"restaurant_id\", mt1.id as \"cat_id\",mt2.id as \"subcat_id\"\n" +
            "from restaurants r\n" +
            "inner join menu_taxonomy as mt on r.menu_id=mt.id\n" +
            "inner join menu_taxonomy as mt1 on mt.id=mt1.parent\n" +
            "inner join menu_taxonomy as mt2 on mt1.id=mt2.parent\n" +
            "inner join item_menu_map as itm on mt2.id=itm.menu_id\n" +
            "inner join items as i on i.id =  itm.item_id\n" +
            "where i.enabled = 1 and i.active = 1 and mt1.active = 1 and mt2.active = 1 and mt.active = 1\n" +
            "ORDER by mt1.order desc, mt2.order desc, mt1.id, mt2.id, i.id limit 20";

    public static String getrestlist = "select  distinct  r.id as \"restaurant_id\"\n" +
            "from restaurants r\n" +
            "inner join menu_taxonomy as mt on r.menu_id=mt.id\n" +
            "inner join menu_taxonomy as mt1 on mt.id=mt1.parent\n" +
            "inner join menu_taxonomy as mt2 on mt1.id=mt2.parent\n" +
            "inner join item_menu_map as itm on mt2.id=itm.menu_id\n" +
            "inner join items as i on i.id =  itm.item_id\n" +
            "where i.enabled = 1 and i.active = 1 and mt1.active = 1 and mt2.active = 1 and mt.active = 1\n" +
            "ORDER by mt1.order desc, mt2.order desc, mt1.id, mt2.id, i.id limit 100";

    public static String getcategories = "select  distinct  mt1.id as \"cat_id\", mt1.name as \"cat_name\"" +
            "from restaurants r\n" +
            "inner join menu_taxonomy as mt on r.menu_id=mt.id\n" +
            "inner join menu_taxonomy as mt1 on mt.id=mt1.parent\n" +
            "inner join menu_taxonomy as mt2 on mt1.id=mt2.parent\n" +
            "inner join item_menu_map as itm on mt2.id=itm.menu_id\n" +
            "inner join items as i on i.id =  itm.item_id\n" +
            "where i.enabled = 1 and i.active = 1 and mt1.active = 1 and mt2.active = 1 and mt.active = 1 and r.id= '";
    public static String getcategoriesend = "' ORDER by mt1.order desc, mt2.order desc, mt1.id, mt2.id, i.id limit 20";

    public static String getsubcategory = "select  distinct  mt2.id as \"subcat_id\", mt2.name as \"subcat_name\"" +
            "from restaurants r\n" +
            "inner join menu_taxonomy as mt on r.menu_id=mt.id\n" +
            "inner join menu_taxonomy as mt1 on mt.id=mt1.parent\n" +
            "inner join menu_taxonomy as mt2 on mt1.id=mt2.parent\n" +
            "inner join item_menu_map as itm on mt2.id=itm.menu_id\n" +
            "inner join items as i on i.id =  itm.item_id\n" +
            "where i.enabled = 1 and i.active = 1 and mt1.active = 1 and mt2.active = 1 and mt.active = 1 and r.id=";
    public static String categoryid = " and mt1.id= ";
    public static String getsubcategoryend = " ORDER by mt1.order desc, mt2.order desc, mt1.id, mt2.id, i.id limit 20";

    public static String getitem = "select  distinct  i.id as \"item_id\", i.name as \"item_name\"" +
            "from restaurants r\n" +
            "inner join menu_taxonomy as mt on r.menu_id=mt.id\n" +
            "inner join menu_taxonomy as mt1 on mt.id=mt1.parent\n" +
            "inner join menu_taxonomy as mt2 on mt1.id=mt2.parent\n" +
            "inner join item_menu_map as itm on mt2.id=itm.menu_id\n" +
            "inner join items as i on i.id =  itm.item_id\n" +
            "where i.enabled = 1 and i.active = 1 and mt1.active = 1 and mt2.active = 1 and mt.active = 1 and r.id = ${restaurant} and mt1.id= ${category} and mt2.id= ${subcategory}\n" +
            "ORDER by mt1.order desc, mt2.order desc, mt1.id, mt2.id, i.id limit 10\n" +
            "\n";


    public static String getitemdetails = "select  distinct  mt1.id as \"cat_id\", mt2.id as \"subcat_id\", i.id as \"item_id\"\n" +
            "from restaurants r\n" +
            "inner join menu_taxonomy as mt on r.menu_id=mt.id\n" +
            "inner join menu_taxonomy as mt1 on mt.id=mt1.parent\n" +
            "inner join menu_taxonomy as mt2 on mt1.id=mt2.parent\n" +
            "inner join item_menu_map as itm on mt2.id=itm.menu_id\n" +
            "inner join items as i on i.id =  itm.item_id\n" +
            "where i.enabled = 1 and i.active = 1 and mt1.active = 1 and mt2.active = 1 and mt.active = 1 and r.id=";
    public static String enditemdetails = " ORDER by mt1.order desc, mt2.order desc, mt1.id, mt2.id, i.id";

    public static String getItemsEnd = " and i.enabled = 1 and i.active = 1 and mt1.active = 1 and mt2.active = 1 and mt.active = 1\n" +
            "ORDER by mt1.order desc, mt2.order desc, mt1.id, mt2.id, i.id;";

    public static String getNewArrivalRestaurants = "select id, lat_long from restaurants where created_on > '2017-11-02 01:31:46' and enabled = 1  and city_code = 1 limit 5";
    public static String getItemsInfo="select * from items where id in ";
    public static String getExclusiveRestaurants1 = "select id, lat_long from restaurants where is_exclusive = ";
    public static String getExclusiveRestaurants2 = " and enabled = 1 limit 5";
    public static String getNewArrivalRestaurants1 = "select id, lat_long from restaurants where created_on > '";
    public static String getNewArrivalRestaurants2 = "' and enabled = 1  and city_code = 1 limit 5";
    public static String getNonNewArrivalRestaurants1 = "select id, lat_long from restaurants where created_on < '";
    public static String getNonNewArrivalRestaurants2 = "' and enabled = 1  and city_code = 1 limit 5";
    public static String getLatLongByRest = "select lat_long from restaurants where id = ";
    public static String getValidRestItem= "select restaurant_id, item_id from valid_restaurant_item where restaurant_id= ";

    public static String aggrement_type = "post_paid";
    public static String pay_by_system_value = "1";
    public static String default_rest_name = "Kritunga-";
    public static String city_code = "1";
    public static String city = "bangalore";
    public static String area_code = "1";
    public static String default_address = "1st Floor, KHB Colony, Koramangala, 5th Block, Bangalore";
    public static String email_id = "automationtest@swiggy.in";
    public static String default_locality = "5th Block";
    public static String bank_name = "HDFC BANK";
    public static String beneficiary_name = "test";
    public static String commission_exp = "[bill]*0.18";
    public static String url = "https://restaurant-mou.com/mou/box_mou.pdf";
    public static String formdata_contenttype = "application/form-data";
    public static String wf_cms_key = "xyPD592!!!_Abn9b";
    public static int rest_status_code = 0;
    public static String code = "code";
    public static String swiggy_id = "swiggy_id";
    public static Double lat = 12.9352156;
    public static Double longi = 77.618562;
    public static String[] days = {"MON","TUE","WED","THU","FRI","SAT","SUN"};
    public static String open_time = "0";
    public static String close_time = "2360";

    public static String menumap_query = "Select * from swiggy.restaurant_menu_map where menu_type = 'REGULAR_MENU' and restaurant_id=";
    public static String popmap_query = "Select * from swiggy.restaurant_menu_map where menu_type = 'POP_MENU' and restaurant_id=";
    public static String menutaxonomy_query = "Select * from swiggy.menu_taxonomy where parent=";
    public static String item_menu_map = "select * from swiggy.item_menu_map where menu_id=";
    public static String menuid = "menu_id";
    public static String testcategory_name = "TestCategory";
    public static String testsubcategory_name = "TestSubCategory";
    public static String cat = "Cat";
    public static String subcat = "SubCat";
    public static String id = "id";
    public static String updatecuisine = "UPDATE swiggy.restaurants set cuisine='";
    public static String cuisine = "North Indian,Chinese";

    // Item-Deletion constants
    public static String FILE_PATH="C://Users/manjunath.d/Downloads/ItemUploadWithVariant.csv";
    public static String getFileId_query = "Select * from swiggy.grasshooper_file_upload where id=";
    public static String deleteFileId_query = "delete from swiggy.grasshooper_file_upload where id=";

    public static String getMenuId_query = "Select * from swiggy.restaurant_menu_map where restaurant_id=";

    public static String restMenuMap_query = "Select * from swiggy.restaurant_menu_map where restaurant_id=";
    public static String getCatSubcat_query = "Select * from swiggy.menu_taxonomy where parent=";
    public static String getItemMenuMap_query = "Select * from swiggy.item_menu_map where menu_id=";
    public static String getItems_query = "Select * from swiggy.items where id=";
    public static String getMultipleItems_query = "Select * from swiggy.items where id IN ";
    public static String getMultipleItemMenuMap_query = "Select * from swiggy.item_menu_map where menu_id IN ";
    public static String getMultipleCatSubcat_query = "Select * from swiggy.menu_taxonomy where parent IN ";


    public static String deleteItems_query = "Delete from swiggy.items where id IN ";
    public static String deleteItemMenuMap_query = "Delete from swiggy.item_menu_map where menu_id IN ";
    public static String deleteCatSubcat_query = "Delete from swiggy.menu_taxonomy where parent IN ";


    // SelfServe ticketing constants
    public static String restaurantId_selfserve = "9990";
    public static String getTicketDetails_query = "Select * from swiggy.tickets order by id desc";


    public static String VALID_FILE_ID="283";
    public static String INVALID_FILE_ID="abc";
    public static String NONEXIST_FILE_ID="-123";

    public static String VALID_USER_ID="1354";
    public static String INVALID_USER_ID="test123";
    public static String NONEXIST_USER_ID="-123";

    public static String USER_ID="1354";

    public static String order = "1";

    //EDVO Constants

    public static String dominosRestaurant = "Select id from swiggy.restaurants_partner_map where type_of_partner =";
    public static String mealitemsbyrestId = "select distinct item_id as itemId from group_item g\n" +
            "inner join group_definition as gd on g.group_id=gd.id\n" +
            "inner join meal_definition as md on gd.meal_id=md.id\n" +
            "where md.rest_id=";
    public static String mealItemsByMealId = "Select distinct item_id as itemId from group_item g\n"+
            "inner join group_definition as gd on g.group_id=gd.id\n" +
            "inner join meal_definition as md on gd.meal_id=md.id\n" +
            "where md.id=";
    public static String mealIdsbyRestId= "Select id from meals.meal_definition where rest_id=";
    public static String EDVOImage = "Select image_id from meals.meal_definition where id= ";
    public static String EDVOText = "Select text from meals.meal_definition where id= ";
    public static String EDVOSubText = "Select sub_text from meals.meal_definition where id= ";
    public static String launchMainText = "select main_text from meals.launch_page where meal_id =";
    public static String launchSubText = "select sub_text from meals.launch_page where meal_id =";
    public static String exitMainText = "select main_text from meals.exit_page where meal_id = ";
    public static String exitSubText = "select sub_text from meals.exit_page where meal_id =  ";
    public static String screenNumber = "select screen_no from meals.screen_entity where group_id = ";
    public static String screenTitle = "select screen_title from meals.screen_entity where group_id =";
    public static String screenDescription = "select screen_description from meals.screen_entity where group_id =";
    public static String bgColor = "select bg_color from meals.launch_page where meal_id = ";

    public static String updateEDVOImage = "Update meals.meal_definition set image_id=";
    public static String updateEDVOText = "Update meals.meal_definition set text=";
    public static String updatelaunchMainText = "Update meals.launch_page set main_text =";
    public static String updatelaunchSubText = "Update meals.launch_page set sub_text = ";
    public static String updateExitMainText = "Update meals.exit_page set main_text = ";
    public static String updateExitSubText = "Update meals.exit_page set sub_text = ";
    public static String updateScreenNumber = "Update meals.screen_entity set screen_no =";
    public static String updateScreenTitle = "Update meals.screen_entity set screen_title =";
    public static String updateScreenDescription = "Update meals.screen_entity set screen_description =";
    public static String updateBGColor = "Update meals.launch_page set bg_color = ";
    public static String updateEDVOmeal = " where id = ";
    public static String updateEDVOMealId = " where meal_id =";
    public static String updateEDVOGroup = " where group_id = ";
    public static String updateMinChoice = "update meals.group_definition set min_choices = ";
    public static String updateMinChoice1 = " and min_total = ";
    public static String updateMaxChoice = "update meals.group_definition set max_choices = ";
    public static String updateMaxChoice1 = " and max_total = ";
    public static String updateMinTotal = "update meals.group_definition set min_total = ";
    public static String updateMaxTotal = "update meals.group_definition set max_total = ";
    public static String updateMaxTotal1 = " and max_choices = ";


    public static String live_date_query = "update swiggy.restaurants set restaurants.live_date = restaurants.created_on where id='";
    public static String update_latlng = "update swiggy.restaurants set restaurants.lat_long ='";

    public static String getMinChoice = "select min_choices from meals.group_definition ";
    public static String getMaxChoice = "select max_choices from meals.group_definition ";
    public static String getMinTotal = "select min_total from meals.group_definition ";
    public static String getMaxTotal = "select max_total from meals.group_definition ";
    public static String getItemMaxQuantity = "select max_quantity from meals.group_item " ;
    public static String getItemMaxQuantity1 = " and item_id = ";
    public static String setItemMaxQuantity = "update meals.group_item set max_quantity =  " ;
    public static String setItemMaxQuantity1 = " and item_id = ";
    public static String insert_cuisine = "INSERT INTO `swiggy`.`restaurants_cuisines`(`restaurants_id`,`cuisines_id`) VALUES(";
    public static String close_insert = ")";
    public static String comma = ",";
    public static int cuisinse_id = 97;
    public static String rest_details = "SELECT * from swiggy.restaurants where id = ";
    public static String update_assured = "update swiggy.restaurants set assured=";
    public static String city_present = "select * from swiggy.city where name = '";
    public static String area_present = "select * from swiggy.area where name ='";

    public static String polygon_id = "select id from swiggy.city_polygon where city_id= ";
    public static String partner_map_data = "select restaurants.id as restaurant_id, restaurants.restaurant_class, restaurants.enabled as is_enabled, restaurants.name as restaurant_name ,\n" +
            "restaurants_partner_map.partner_id, restaurants_partner_map.id, city.name as city_name, city.id as city_id, \n" +
            "area.name as area_name, area.id as area_id, restaurants_partner_map.type_of_partner, restaurants_partner_map.otp from restaurants \n" +
            "inner join restaurants_partner_map on restaurants_partner_map.rest_id = restaurants.id\n" +
            "inner join city on restaurants.city_code = city.id\n" +
            "inner join area on restaurants.area_code = area.id\n" +
            "where restaurants.id =";
    public static String getCategory_storeId_start = "select * from menu_taxonomy where parent=(select menu_id from restaurants where id =(select rest_id from cloud_partner_map where store_id='";
    public static String getCategory_storeId_end = "')) and active=1 and type='Cat'";
    public static String getCategory_wo_active = "')) and type='Cat' ";
    public static String getCatActive = "and external_id ='";
    public static String getfullmenu_items = "select external_item_id from items where  id in (select item_id from item_menu_map where menu_id in (select id from menu_taxonomy where parent in\n" +

            "(select id from menu_taxonomy where name<>\"nota\" and parent=(select menu_id from restaurants where id=(select rest_id from cloud_partner_map where store_id= '";

    public static String close_braces = "')))))";
    public static String getSubcategory_fullmenu = "select external_id from menu_taxonomy where id in (select menu_id from item_menu_map where item_id in (select id from items where  \n" +
            "external_item_id= '";
    public static String getSubCategory_middle = "' and restaurant_id in (select rest_id from cloud_partner_map where store_id='";
    public static String getSubcategory_end = "')))";

    public static String getCategory_fullmenu = "select external_id from menu_taxonomy where id in (select parent from menu_taxonomy where id in (select menu_id from item_menu_map where item_id in\n" +
            "(select id from items where  external_item_id='";
    public static String getCategory_middle = "' and restaurant_id in (select rest_id from cloud_partner_map where store_id= '";
    public static String getCategory_end = "' ))))";
    public static String getItems_fullmenu = "select enabled, active, external_item_id from items where restaurant_id=(select rest_id from cloud_partner_map where store_id='";
    public static String getItems_end = "')";
    public static String token_query = "select swiggy.cloud_partner_info.api_token from swiggy.cloud_partner_map \n" +
            "INNER JOIN  swiggy.cloud_partner_info on swiggy.cloud_partner_map.user_id = swiggy.cloud_partner_info.user_id\n" +
            "where swiggy.cloud_partner_map.store_id='";

    public static String polygon_name = "select id from swiggy.polygon where name= ";
    public static String getItemsforRest = "select id from items where restaurant_id = ";
    public static String check_user_exists = "select * from wp_users where mobile = ";
    public static String item_details = "select * from swiggy.items where restaurant_id=";
    public static String api_token = "api_token";
    public static String getItems_fullmenu1 = "select * from items where  external_item_id='";
    public static String getItems_fullmenuMiddle1 = "'and restaurant_id=(select rest_id from cloud_partner_map where store_id='";


    public static String getItems_VariantGroup = "select * from variant_groups where active=1 and item_id=(select id from items where external_item_id='";
    public static String getItems_VariantGroup1 = "'and restaurant_id=(select rest_id from cloud_partner_map where store_id='";
    public static String getItems_VariantGroup2 = "'))";

    public static String getItems_Variant="select * from variants where active=1 and variant_group_id =(select id from variant_groups where item_id=(select id from items where external_item_id='";
    public static String getItems_Variant1=	"'and restaurant_id=(select rest_id from cloud_partner_map where store_id='";
    public static String getItems_Variant2=	"')) and external_id='";
    public static String getItems_Variant3=	"')";


    public static String regular_items = "select * from swiggy.items where (addons = ''  or addons = '{}')and variants = '' and variants_v2 is NULL and restaurant_id =";

    public static String getItems_AddonGroup = "select * from addon_groups where  active=1 and item_id=(select id from items where external_item_id='";
    public static String getItems_AddonGroup1 = "'and restaurant_id=(select rest_id from cloud_partner_map where store_id='";
    public static String getItems_AddonGroup2 = "'))";

    public static String getItems_addons="select * from addons_new where external_addon_id='";
    public static String getItems_addons1=	"' and  id in (select addon_id  from addons_addon_group_map where addon_group_id=(select ag.id from addon_groups ag INNER JOIN addons_addon_group_map agm on   \r\n" +
            "ag.id=agm.addon_group_id and item_id=(select id from items where external_item_id='";
    public static String getItems_addons2= "'and restaurant_id=(select rest_id from cloud_partner_map where store_id='";
    public static String getItems_addons3="')) and  ag.external_id ='";
    public static String getItems_addons4="')) ";

    public static String live_date_query_date = "update swiggy.restaurants set restaurants.live_date = '";
    public static String live_date_cond =  "' where id='";
    public static String set_exclusive = "update restaurants set is_exclusive='";
    public static String set_exclusive_end = "' where id=";



    public static String insert_wp_options = "INSERT INTO `swiggy`.`wp_options` (`option_name`,`option_value`,`autoload`) VALUES('";
    public static String wp_options_end = "')";

    public static String update_wp_options = "update `swiggy`.`wp_options` set option_value='";
    public static String update_wp_options2 = "',autoload='";
    public static String update_wp_options3 = "' where option_name='";
    public static String update_wp_options_end = "'";

    public static String delete_wp_options = "delete from `swiggy`.`wp_options` where option_name='";
    public static String getcity_id = "select id from swiggy.city where name = '";
    public static String getarea_id = "select id from swiggy.area where name = '";
    public static String city_details = "select * from swiggy.city where id = ";
    public static String area_details = "select * from swiggy.area where id = ";

    public static String getItemsSlot = "select * from item_slots where  active=1 and item_id in (select id from items where external_item_id='";
    public static String getItemsSlot1 = "'and restaurant_id=(select rest_id from cloud_partner_map where store_id='";
    public static String getItemsSlot2 = "'))";

    public static String getCatlogGst = "select * from catalog_gst_details where  active=1 and entity_id ='";
    public static String getCatlogGst1 = "'";

    public static String item_id = "9179233";
    public static String gst_state = "karnataka";
    public static String gstin = "";
    public static String gst_business_name = "";
    public static String gst_registration_doc = "";

    public static String pop_item = "POP_ITEM";
    public static String popRainOnly = "RAIN_ONLY";
    public static String popOnly = "POP_ONLY";
    public static String popAndRainBoth = "RAIN_AND_POP";



    public static String AreaIdSchedule="select * from area_schedule where area_id='";
    public static String AreaIdSchedule1 = "'and day='";

    public static String CategoryIdforPop="select * from menu_taxonomy where parent='";
    public static String CategoryIdforPop1 = "'";

    public static String ItemSchedulePriority="select * from item_schedule_mapping where area_Schedule_id='";
    public static String ItemSchedulePriority1="' and cuisine='";
    public static String ItemSchedulePriority2="'and dish_type='";


    public static String MenuIdforPop="select * from restaurant_menu_map where restaurant_id='";
    public static String MenuIdforPop1 = "' and menu_type=\"POP_MENU\"";

    public static String areaIdBasedOnItem="select area_Code from restaurants inner join items on restaurants.id=items.restaurant_id and items.id='";
    public static String areaIdBasedOnItem1="'";
    public static String menuType="POP_MENU",restType="Rest",catType="Cat",subCatType="SubCat",restMenuName="PopMenuForId",catName="AutomationCat",subCatName="AutomationCat";
    public static int display_sequence=10,inventory=900,openTime = 0000, closeTime = 2330;
    public static String menu_type = "POP_MENU",slotType ="FullDaySLot",cusine="NorthIndian",dish_type="BreakFast";
    public static String fetchTestRestaurants ="select r.id rest_id, r.name rest_name,c.name city_name,r.`updated_by`,r.`updated_on` from restaurants r inner join city c on (r.`city_code`=c.id) where r.`enabled`=1 and r.`name` like '%test%'";
    public   String itemSuggestion="select suggestion from `item_suggestion` where `item_id`="+cmshelper.getSmartItem();

    public static String getSubcategoryId0 = "select menu_id from item_menu_map where item_id=";
    public static String getCategoryId0 = "select parent from menu_taxonomy where id in (select menu_id from item_menu_map where item_id=";
    public static String getCategoryId1 = ")";
    public static String ItemSuggestion=null;
    public static String getItem_id_query="select * from items where enabled=1 and active=1 and restaurant_id=";
    
    public static  String ExecuteQuery(String itemId)
    {
  
    	return "INSERT INTO `item_suggestion` (`created_by`, `created_on`, `updated_by`, `updated_on`, `item_id`, `suggestion`) VALUES('{\"source\":\"item-suggestion-scheduler\",\"meta\":null}', SYSDATE(), '{\"source\":\"item-suggestion-scheduler\",\"meta\":null}', SYSDATE(), "+itemId+", '[{\"item_id\":24911,\"score\":0.5123},{\"item_id\":24912,\"scoreâ€�:0.8},{â€œitem_id\":24913,\"score\":0.8}]')";

    }

    public static String source = "CMS";
    public static String metaUser = "Hemamalini.g@swiggy.in Automation";
    
    public static String catInactiveitemHolidayslot=" select * from  item_holiday_slots ihs inner join item_menu_map imm where ihs.item_id=imm.item_id and imm.menu_id in(select id from  menu_taxonomy where parent'";
    public static String catInactiveitemHolidayslot1="') and ihs.created_on='";
    public static String catInactiveitemHolidayslot2="'";

    public static String VerifyItemCountimHolidayslotTab=" select count(*) from  item_holiday_slots ihs inner join item_menu_map imm where ihs.item_id=imm.item_id and  imm.menu_id in(select id from  menu_taxonomy where parent='";
    public static String VerifyItemCountimHolidayslotTab1="') and ihs.created_on='";
    public static String VerifyItemCountimHolidayslotTab2="'";
    
    public static String VerifyItemMenuMapTab=" select count(*) from item_menu_map where menu_id in(select id from  menu_taxonomy where parent='";
    public static String VerifyItemMenuMapTab1="'";
    
    public static String activeItemId="select id from items where enabled=1 and `in_stock`=1 and active=1 limit 1";
    public static String suggItem="select item_id from item_suggestion where item_id=545197";
    public static String authUserId=" select auth_user_id from agents where auth_user_id in(select id from auth_user where is_active=1) and is_enabled=1)";
    public static String authKeyEnableDisable="select auth_key,auth_user_id from agents where auth_user_id='${authUserId}'";
    public static String catId="select id from menu_taxonomy where parent =(select menu_id from restaurants where id='";
    public static String catId1="') and active=1 and type=\"Cat\" limit 1";
    public static String subCatId="select id from menu_taxonomy where type=\"SubCat\" and parent=(select id from menu_taxonomy where parent =(select menu_id from restaurants where id='";
    public static String subCatId1="') and active=1 and type=\"Cat\" limit 1) limit 1";
    public static String catdetails="select * from menu_taxonomy where id='";
    public static String catdetails1="'";
    public static String catandsubcatforItem="select parent,id from menu_taxonomy where type=\"SubCat\" and id in(select menu_id from item_menu_map where item_id='";
    public static String catandsubcatforItem1="')";
    public static String differentsubcat="select id from menu_taxonomy where parent='";
    public static String differentsubcat1="'";

}
