package com.swiggy.api.erp.cms.pojo.BaseServiceBulkAddon;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.erp.cms.pojo.BaseServiceBulkAddon
 **/

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "addons",
        "delete_by_ids",
        "delete_by_third_party_ids",
        "update_items"
})
public class BulkAddons {

    @JsonProperty("addons")
    private List<Addon_Bulk> addons = null;
    @JsonProperty("delete_by_ids")
    private List<Integer> delete_by_ids = null;
    @JsonProperty("delete_by_third_party_ids")
    private List<String> delete_by_third_party_ids = null;
    @JsonProperty("update_items")
    private Boolean update_items;

    @JsonProperty("addons")
    public List<Addon_Bulk> getAddons() {
        return addons;
    }

    @JsonProperty("addons")
    public void setAddons(List<Addon_Bulk> addons) {
        this.addons = addons;
    }

    @JsonProperty("delete_by_ids")
    public List<Integer> getDelete_by_ids() {
        return delete_by_ids;
    }

    @JsonProperty("delete_by_ids")
    public void setDelete_by_ids(List<Integer> delete_by_ids) {
        this.delete_by_ids = delete_by_ids;
    }

    @JsonProperty("delete_by_third_party_ids")
    public List<String> getDelete_by_third_party_ids() {
        return delete_by_third_party_ids;
    }

    @JsonProperty("delete_by_third_party_ids")
    public void setDelete_by_third_party_ids(List<String> delete_by_third_party_ids) {
        this.delete_by_third_party_ids = delete_by_third_party_ids;
    }

    @JsonProperty("update_items")
    public Boolean getUpdate_items() {
        return update_items;
    }

    @JsonProperty("update_items")
    public void setUpdate_items(Boolean update_items) {
        this.update_items = update_items;
    }

    public void setDefaultValues() {
        List<Addon_Bulk> list = new ArrayList<>();
        if(this.getUpdate_items() == null)
            this.setUpdate_items(true);
        if(this.getAddons() == null)
            this.setAddons(list);
        if(this.getDelete_by_ids() == null)
            this.setDelete_by_ids(new ArrayList<>());
        if(this.getDelete_by_third_party_ids() == null)
            this.setDelete_by_third_party_ids(new ArrayList<>());
    }

    public BulkAddons build() {
        setDefaultValues();
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("addons", addons).append("delete_by_ids", delete_by_ids).append("delete_by_third_party_ids", delete_by_third_party_ids).append("update_items", update_items).toString();
    }

}

