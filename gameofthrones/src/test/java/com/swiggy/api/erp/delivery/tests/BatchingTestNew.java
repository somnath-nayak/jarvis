package com.swiggy.api.erp.delivery.tests;

import com.swiggy.api.erp.delivery.helper.AutoassignHelper;
import com.swiggy.api.erp.delivery.helper.BatchingBuilder;
import com.swiggy.api.erp.delivery.helper.DeliveryDataBaseUtils;
import com.swiggy.api.erp.delivery.helper.OrderBuilder;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 * @author preetesh
 * @project swiggy_test
 * @createdon 11/09/18
 */



public class BatchingTestNew {

    Integer zone_id=10;
    Integer cityId;
    String zoneName;
    Integer resturantId;
    AutoassignHelper autoassignHelper= new AutoassignHelper();




    @BeforeClass
    public void beforeBatching()
    {
        cityId=DeliveryDataBaseUtils.getCityFromZone(zone_id.toString());
        // restaurant Id in zone
        //Create DE
        // zoneName=DeliveryDataBaseUtils.
        //get city Id from zoneId
        //get zone name from zoneid
        //get random restaurant from zone id
        //get random restaurant from
        //need to create orders---no that is required in the test

        /*
         * Problem statement for order creation
         * we want to create orders with
         * Two restaurants from same zone are need
         * */
    }

    @Test(enabled = true, groups = {"batchingregression"}, description = "Verify that if the order is a replicated order than it's not considered for batching and also, there is no delay added for this order")
    public void batchingtest256() {
        BatchingBuilder b1=  new BatchingBuilder.Builder("BLR_HSR","3","1").build();
        OrderBuilder o1=new OrderBuilder.Builder("3").is_replicated("true").is_batching_enabled("1").build();
        OrderBuilder o2=new OrderBuilder.Builder("3").is_replicated("true").is_batching_enabled("1").build();
        autoassignHelper.runAutoAssign("1");
        String batch_id1= DeliveryDataBaseUtils.getBatchIdFromOrderId(o1.getOrder_id());
        String batch_id2= DeliveryDataBaseUtils.getBatchIdFromOrderId(o2.getOrder_id());
        Assert.assertNotEquals(batch_id1,batch_id2);
    }


}
