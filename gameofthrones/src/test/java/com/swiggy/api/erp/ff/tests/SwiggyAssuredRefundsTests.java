package com.swiggy.api.erp.ff.tests;
import java.util.HashMap;
import org.json.JSONException;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.swiggy.api.erp.ff.dp.SwiggyAssuredRefundsData;
import com.swiggy.api.erp.ff.helper.LOSHelper;
import com.swiggy.api.erp.ff.helper.SwiggyAssuredRefundsHelper;


public class SwiggyAssuredRefundsTests extends SwiggyAssuredRefundsData {
	
	SwiggyAssuredRefundsHelper SwiggyAssuredsRefundHelper = new SwiggyAssuredRefundsHelper();
	LOSHelper losHelper = new LOSHelper();

	@Test(dataProvider = "getSwiggyAssuredOrderSLABreached", groups = { "sanity","regression" }, description = "Swiggy Assured Refunds SLA Breached")
	public void getSwiggyAssuredRefundsTestsSLABreached(String orderId, String scenario, HashMap<String,Integer> expectedResponse, HashMap<String, String> expectedResponse1) throws  NumberFormatException, JSONException, InterruptedException {

		System.out.println("Swiggy Assured refunds tests SLA breached.& order not edited.........\n." + scenario + " started");
		
		SoftAssert softAssertion = new SoftAssert();
		softAssertion.assertEquals(SwiggyAssuredsRefundHelper.getSwiggyAssuredFlag(Long.parseLong(orderId)), 1, "This order is not an assured order");
		softAssertion.assertEquals(SwiggyAssuredsRefundHelper.getCustomerEditFlag(Long.parseLong(orderId)), null,"The order is edited so not subject to refund");
		softAssertion.assertEquals(SwiggyAssuredsRefundHelper.getPgResponseTime(Long.parseLong(orderId)), expectedResponse1.get("pg_response_time"), "pg response time did not match with expected");
		softAssertion.assertEquals(SwiggyAssuredsRefundHelper.getDeliveredTime(Long.parseLong(orderId)), expectedResponse1.get("order_delivered_time"), "delivered time did not match with expected");	
		softAssertion.assertEquals(SwiggyAssuredsRefundHelper.getRefundStatus(Long.parseLong(orderId)), expectedResponse1.get("refund_status"),"Refund status did not match with expected");
		softAssertion.assertAll();
		
		System.out.println("Swiggy Assured refunds tests SLA breached & order not edited ........\n." + scenario + " completed");
	}
	
	@Test(dataProvider = "getSwiggyAssuredOrderSLANotBreached", groups = { "sanity","regression" }, description = "Swiggy Assured Refunds SLA Breached")
	public void getSwiggyAssuredOrderSLANotBreached(String orderId, String scenario, HashMap<String,Integer> expectedResponse,HashMap<String,String> expectedResponse1) throws  NumberFormatException, JSONException, InterruptedException {

		System.out.println("Swiggy Assured refunds tests SLA not breached & order not edited ........\n." + scenario + " started");
					
		SoftAssert softAssertion = new SoftAssert();
		softAssertion.assertEquals(SwiggyAssuredsRefundHelper.getSwiggyAssuredFlag(Long.parseLong(orderId)), 1, "This order is not an assured order");
		softAssertion.assertEquals(SwiggyAssuredsRefundHelper.getCustomerEditFlag(Long.parseLong(orderId)), null,"The order is edited so not subject to refund");
		softAssertion.assertEquals(SwiggyAssuredsRefundHelper.getPgResponseTime(Long.parseLong(orderId)), expectedResponse1.get("pg_response_time"), "pg response time did not match with expected");
		softAssertion.assertEquals(SwiggyAssuredsRefundHelper.getDeliveredTime(Long.parseLong(orderId)), expectedResponse1.get("order_delivered_time"), "delivered time did not match with expected");	
		softAssertion.assertEquals(SwiggyAssuredsRefundHelper.getRefundStatus(Long.parseLong(orderId)), expectedResponse1.get("refund_status"),"Refund status did not match with expected");
		softAssertion.assertAll();
		System.out.println("Swiggy Assured refunds tests SLA breached.& order not edited.........\n." + scenario + " completed");

	}
	
	@Test(dataProvider = "getSwiggyAssuredOrderSLABreachedOrderEdited", groups = { "sanity","regression" }, description = "Swiggy Assured Refunds SLA Breached")
	public void getSwiggyAssuredOrderSLABreachedOrderEdited(String orderId, String scenario,HashMap<String,Integer> expectedResponse, HashMap<String,String> expectedResponse1) throws  NumberFormatException, JSONException, InterruptedException {

		System.out.println("Swiggy Assured refunds tests SLA breached & order edited..........\n." + scenario + " started");
		
		SoftAssert softAssertion = new SoftAssert();
		softAssertion.assertEquals(SwiggyAssuredsRefundHelper.getSwiggyAssuredFlag(Long.parseLong(orderId)), 1, "This order is not an assured order");
		softAssertion.assertEquals(SwiggyAssuredsRefundHelper.getCustomerEditFlag(Long.parseLong(orderId)), null,"The order is edited so not subject to refund");
		softAssertion.assertEquals(SwiggyAssuredsRefundHelper.getPgResponseTime(Long.parseLong(orderId)), expectedResponse1.get("pg_response_time"), "pg response time did not match with expected");
		softAssertion.assertEquals(SwiggyAssuredsRefundHelper.getDeliveredTime(Long.parseLong(orderId)), expectedResponse1.get("order_delivered_time"), "delivered time did not match with expected");	
		softAssertion.assertEquals(SwiggyAssuredsRefundHelper.getRefundStatus(Long.parseLong(orderId)), expectedResponse1.get("refund_status"),"Refund status did not match with expected");
		softAssertion.assertAll();
		System.out.println("Swiggy Assured refunds tests SLA breached & order edited.......\n." + scenario + " completed");

	}
	
	@Test(dataProvider = "getNonSwiggyAssuredOrder", groups = { "sanity","regression" }, description = "Swiggy Assured Refunds")
	public void getNonSwiggyAssuredRefundsTests(String orderId, String scenario,HashMap<String,Integer> expectedResponse) throws  NumberFormatException, JSONException, InterruptedException {

		System.out.println("Non Swiggy Assured refunds tests ..........\n." + scenario + " started");

		SoftAssert softAssertion = new SoftAssert();		
		softAssertion.assertEquals(SwiggyAssuredsRefundHelper.getSwiggyAssuredFlag(Long.parseLong(orderId)), 0, "This order is not an assured order");
		softAssertion.assertAll();
		System.out.println("Non Swiggy Assured refunds tests ..........\n." + scenario + " completed");

			
	}
	
}
