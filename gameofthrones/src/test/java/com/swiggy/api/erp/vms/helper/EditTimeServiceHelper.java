package com.swiggy.api.erp.vms.helper;

import java.util.HashMap;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Daenerys.Initializer;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;

public class EditTimeServiceHelper {

	static Initialize initializer = Initializer.getInitializer();

	public static Processor editOrderRequest(String payload, String accessToken) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		// header.put("Authorization",basicAuth);
		GameOfThronesService service = new GameOfThronesService("edittimeservice", "orderEditRequest", initializer);
		String[] payloadparams = new String[] { payload };
		Processor processor = new Processor(service, requestHeaders, payloadparams);
		return processor;
	}
	
	public static Processor orderEditRequestStatus(String basicAuth, String orderId) {
		HashMap<String, String> header = new HashMap<String, String>();
		GameOfThronesService services = new GameOfThronesService("edittimeservice", "orderEditRequestStatus", initializer);
		header.put("Content-Type", "application/json");
		// header.put("Authorization",basicAuth);
		Processor processor = new Processor(services, header, null, new String[] { orderId });
		return processor;

	}

	public static Processor orderEditTableCheck(String basicAuth, String orderId) {
		HashMap<String, String> header = new HashMap<String, String>();
		GameOfThronesService services = new GameOfThronesService("edittimeservice", "orderEditRequestCheck", initializer);
		header.put("Content-Type", "application/json");
		// header.put("Authorization",basicAuth);
		Processor processor = new Processor(services, header, null, new String[] { orderId });
		return processor;

	}



	public static Processor ping(String basicAuth) {
		HashMap<String, String> header = new HashMap<String, String>();
		GameOfThronesService services = new GameOfThronesService("edittimeservice", "orderEditServiceHealth", initializer);
		header.put("Content-Type", "application/json");
		// header.put("Authorization",basicAuth);
		Processor processor = new Processor(services, header);
		return processor;

	}

}
