package com.swiggy.api.erp.cms.helper;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


public class CriticalAPI_Helper {
	Initialize gameofthrones = new Initialize();

	public HashMap<String, String> setHeaders() throws JSONException {
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		JSONObject json = new JSONObject();
		json.put("source", "cms");
		
		JSONObject jsonUserData = new JSONObject();
		jsonUserData.put("user", "CMS-user");
		json.put("meta",jsonUserData);
		
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("user-meta", json.toString());
		return requestheaders;
	}

	public String[] setPayload(int menu_id, String from_time, String to_time) throws JSONException {
		return new String[] {Integer.toString(menu_id), from_time, to_time };
	}

	public String[] setPayload1(String external_item_ids, int restaurant_id) throws JSONException {
		return new String[] {external_item_ids, Integer.toString(restaurant_id) };
	}
	public String[] setPayload2(String external_item_ids, String restaurant_id) throws JSONException {
		return new String[] {external_item_ids, restaurant_id };
	}
	

	public Processor createMenuHolidaySlotHelper(int menuId, String fromTime, String toTime) throws JSONException {
		GameOfThronesService service1 = new GameOfThronesService("cmsbaseservice", "menuholidayslotcreate", gameofthrones);
		 Processor p = new Processor(service1, 
				 					setHeaders(), 
				 					setPayload(menuId, fromTime, toTime), 
				 					null);	
		 return p;	
	}
	
	public Processor updateMenuHolidaySlotHelper(int menuId, String fromTime, String toTime, int newlyCreatedMenuID) throws JSONException {
		GameOfThronesService service1 = new GameOfThronesService("cmsbaseservice", "menuholidayslotupdate", gameofthrones);
		 Processor p = new Processor(service1, 
				 					setHeaders(), 
				 					setPayload(menuId, fromTime, toTime), 
				 					new String[] { ""+ newlyCreatedMenuID });	
		 return p;	
	}
	
	public Processor getMenuHolidaySlotHelper(int menu_id, int newlyCreatedMenuID) throws JSONException {
		GameOfThronesService service1 = new GameOfThronesService("cmsbaseservice", "menuholidayslotget", gameofthrones);
		 Processor p = new Processor(service1, 
				 					setHeaders(), 
				 					new String[]{""}, 
				 					new String[] { ""+ newlyCreatedMenuID });	
		 return p;	
	}
	
	public Processor deleteMenuHolidaySlotHelper(int newlyCreatedMenuID) throws JSONException {
		GameOfThronesService service1 = new GameOfThronesService("cmsbaseservice", "menuholidayslotdelete", gameofthrones);
		 Processor p = new Processor(service1, 
				 					setHeaders(), 
				 					new String[]{""}, 
				 					new String[] { ""+ newlyCreatedMenuID });	
		 return p;	
	}
	
	public Processor getItemIdsWithexternalIds(String external_item_ids, int restaurant_id) throws JSONException {
		GameOfThronesService service1 = new GameOfThronesService("cmsbaseservice", "thirdpartyreversemap", gameofthrones);
		 Processor p = new Processor(service1, 
				 					setHeaders(), 
				 					setPayload1(external_item_ids, restaurant_id),
				 					null);	
		 return p;	
	}
	
	public Processor getItemIdswithoutRestId(String external_item_ids, String restaurant_id) throws JSONException {
		GameOfThronesService service1 = new GameOfThronesService("cmsbaseservice", "thirdpartyreversemap", gameofthrones);
		 Processor p = new Processor(service1, 
				 					setHeaders(), 
				 					setPayload2(external_item_ids, restaurant_id),
				 					null);	
		 return p;	
	}
	
    // Hema  Starts
    
    public Processor getRestaurantMenu(String restaurantId) {
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        String[] urlParams = {restaurantId};
        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "GetMenu", gameofthrones);
        Processor processor = new Processor(gots,requestHeader,null, urlParams);
        System.out.println("test"+processor);
        return processor;
    }
    
    public Processor itemOutOfStock(String item_id,String from_time,String to_time) {
    	HashMap<String, String> requestHeader = new HashMap<String, String>();
    	requestHeader.put("Content-Type", "application/json");
        String[] payload = {item_id,from_time,to_time};
        String[] querypaylaod= {item_id};
        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "itemoutofstock", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, payload,querypaylaod);
        return processor;
    }
    
//    public boolean VerifyGetRestaurantMenu(Processor processor) {
//    	       boolean success = false;
//    //	       String body = CriticalAPI_Helper.getRestaurantMenu().ResponseValidator.GetBodyAsText();
//    	       int status = JsonPath.read(body,"$.statusCode");
//    	      
//    	       String data_field = JsonPath.read(body, "$..data").toString().replace("[","").replace("]","").replace("{","").replace("}","").trim();
//    	       
//    	        return success;
//    	    }
    
}

