package com.swiggy.api.erp.cms.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.dp.SelfServeV1Dp;
import com.swiggy.api.erp.cms.helper.SelfServeHelper;
import framework.gameofthrones.JonSnow.Processor;
import org.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SelfServeV1CancleTicketTest extends SelfServeV1Dp {

    @Test(dataProvider = "cancleTicket", priority = 0, groups = {"Sanity_TC", "Smoke_TC", "Regression_TC"},
            description = "cancleTicket")
    public void cancleTicket(String restId,String tickeId,boolean message,int status)
            throws JSONException, InterruptedException
    {
        SelfServeHelper sshelper = new SelfServeHelper();
        Processor p;
        p= sshelper.cancleTicket(restId,tickeId);
        int statuscode=p.ResponseValidator.GetResponseCode();
        Assert.assertEquals(status, statuscode, "Success");

    }
}
