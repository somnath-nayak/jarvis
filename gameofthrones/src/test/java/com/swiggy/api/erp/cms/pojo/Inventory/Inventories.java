
package com.swiggy.api.erp.cms.pojo.Inventory;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "quantity"
})
public class Inventories {

    @JsonProperty("quantity")
    private Long quantity;

    @JsonProperty("quantity")
    public Long getQuantity() {
        return quantity;
    }

    @JsonProperty("quantity")
    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Inventories withQuantity(Long quantity) {
        this.quantity = quantity;
        return this;
    }

    public Inventories setValue(long quan){
        this.withQuantity(quan);
        return this;
    }

}
