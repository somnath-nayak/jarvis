package com.swiggy.api.erp.finance.constants;

public interface PgConstants {

    String finance_DB = "financeDB";

    String freecharge_pg="Freecharge";
    String  Mobikwik_pg="Mobikwik";
    String payU_pg="PayU";
    String PaytmApi_pg="PaytmApi";
    String lazypay_pg="Lazypay";
    String citrus_pg="Citrus";
    String razorpay_pg="Razorpay";

    String freecharge_mid="QSEV80O5nBjPQh";
    String Mobikwik_mid1="MBK6868";
    String Mobikwik_mid2="MBK3451";
    String payU_mid="Kw8o8g";
    String PaytmApi_mid1="swiggy58513599375770";
    String PaytmApi_mid2="swiggy97751185767429";
    String lazypay_mid="180";
    String citrus_mid="42857";
    String razorpay_mid="8S0i1kWYyF2woQ";

    String txn_type_forward="Forward";
    String txn_type_refund="Refund";

    String txn_id_refund_prefix="SwiggyRefund";
    String txn_id_forward_prefix="SwiggyForward";

    String refund_id_prefix_for_refund_txn="rfnd";
    String refund_id_for_forward_txn="0";

    String utr_id_prefix="UTRI";

    String txn_details_for_refund_txn="{\"paymentMode\":\"debitcard\",\"refundDate\":\"2019-02-05T11:03:03.000\",\"refundAmount\":444.0,\"cardIssuer\":\"ICIC\",\"errors\":null,\"errorsDate\":null,\"createdBy\":\"API\"}";
    String txn_details_for_forward_txn="{\"paymentMode\":\"debitcard\",\"refundDate\":null,\"refundAmount\":null,\"cardIssuer\":\"ICIC\",\"errors\":null,\"errorsDate\":null,\"createdBy\":\"API\"}";

    String get_order_total_from_orders="select * from orders where order_id= \"${order_id}\"";
    String get_order_status_from_orders_tinyint = "SELECT cast(${*} as char) as ${*} FROM finance.orders where order_id=\"${order_id}\"";

    String insert_into_pg_txn_new="INSERT INTO `pg_transactions_new` (`pg`, `mid`, `order_id`, `txn_id`, `txn_type`, `txn_date`, `settlement_date`, `gross_txn_value`, `pg_commission`, `cgst`, `sgst`, `igst`, `amount_transferred_to_nodal`, `utr`, `refund_id`, `created_at`, `txn_details`)\n" +
            "VALUES" +
            "('${pg}', '${mid}', '${order_id}', '${txn_id}', '${txn_type}', '${txn_date}', '${settlement_date}', ${gross_txn_value},${pg_commission},${cgst}, ${sgst}, ${igst},${amount_transferred_to_nodal}, '${utr}', '${refund_id}', '${created_at}', '${txn_details}');";


    String get_entity_from_pg_txn_table="select * from pg_transactions_new where order_id=\"${order_id}\" and txn_type= \"${entity}\"";

    double pg_commission_percent=0.05;
    Integer settlement_threshold=1;
}
