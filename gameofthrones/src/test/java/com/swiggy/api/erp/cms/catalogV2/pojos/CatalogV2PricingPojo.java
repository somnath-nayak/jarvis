package com.swiggy.api.erp.cms.catalogV2.pojos;

import com.swiggy.api.erp.cms.constants.CatalogV2Constants;


/**
 * Created by HemaGovindaraj on 24/12/18.
 */

public class CatalogV2PricingPojo
{ 
	private boolean inclusive;

	private String price;

	private String sgst_expression;

	private String packaging_expression;

	private String mrp;

	private String cgst_expression;

	private String igst_expression;

	public boolean getInclusive ()
	{
		return inclusive;
	}

	public void setInclusive (boolean inclusive)
	{
		this.inclusive = inclusive;
	}

	public String getPrice ()
	{
		return price;
	}

	public void setPrice (String price)
	{
		this.price = price;
	}

	public String getSgst_expression ()
	{
		return sgst_expression;
	}

	public void setSgst_expression (String sgst_expression)
	{
		this.sgst_expression = sgst_expression;
	}

	public String getPackaging_expression ()
	{
		return packaging_expression;
	}

	public void setPackaging_expression (String packaging_expression)
	{
		this.packaging_expression = packaging_expression;
	}

	public String getMrp ()
	{
		return mrp;
	}

	public void setMrp (String mrp)
	{
		this.mrp = mrp;
	}

	public String getCgst_expression ()
	{
		return cgst_expression;
	}

	public void setCgst_expression (String cgst_expression)
	{
		this.cgst_expression = cgst_expression;
	}

	public String getIgst_expression ()
	{
		return igst_expression;
	}

	public void setIgst_expression (String igst_expression)
	{
		this.igst_expression = igst_expression;
	}

	public CatalogV2PricingPojo build() {
		setDefaultValues();
		return this;
	}


	private void setDefaultValues() {

		if(this.getCgst_expression()==null){
			this.setCgst_expression(CatalogV2Constants.cgst_expression);
		}
		if(this.getIgst_expression()==null){
			this.setIgst_expression(CatalogV2Constants.igst_expression);
		}
		if(this.getSgst_expression()==null){
			this.setSgst_expression(CatalogV2Constants.sgst_expression);
		}
		if(!this.getInclusive()){
			this.setInclusive(CatalogV2Constants.inclusive);
		}
		if(this.getPackaging_expression()==null){
			this.setPackaging_expression(CatalogV2Constants.packaging_expression);
		}
		if(this.getPrice()==null){
			this.setPrice(CatalogV2Constants.price);
		}
		if(this.getMrp()==null){
			this.setMrp(CatalogV2Constants.mrp);;
		}

	}


	@Override
	public String toString()
	{
		return "ClassPojo [inclusive = "+inclusive+", price = "+price+", sgst_expression = "+sgst_expression+", packaging_expression = "+packaging_expression+", mrp = "+mrp+", cgst_expression = "+cgst_expression+", igst_expression = "+igst_expression+"]";
	}


}

