package com.swiggy.api.erp.cms.pojo.SelfServeUpdateTicketStatus;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.erp.cms.pojo.SelfServeUpdateTicketStatus
 **/

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "key",
        "newValue",
        "oldValue"
})
public class Field_updated {

    @JsonProperty("key")
    private String key;
    @JsonProperty("newValue")
    private String newValue;
    @JsonProperty("oldValue")
    private String oldValue;

    @JsonProperty("key")
    public String getKey() {
        return key;
    }

    @JsonProperty("key")
    public void setKey(String key) {
        this.key = key;
    }

    @JsonProperty("newValue")
    public String getNewValue() {
        return newValue;
    }

    @JsonProperty("newValue")
    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    @JsonProperty("oldValue")
    public String getOldValue() {
        return oldValue;
    }

    @JsonProperty("oldValue")
    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("key", key).append("newValue", newValue).append("oldValue", oldValue).toString();
    }

}
