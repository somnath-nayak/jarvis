package com.swiggy.api.erp.crm.tests.IntegrationServicesTests;

import com.swiggy.api.erp.crm.constants.CRMConstants;
import com.swiggy.api.erp.crm.constants.FlowMapper;
import com.swiggy.api.erp.crm.dp.MockServiceDP;
import com.swiggy.api.erp.crm.helper.CRMUpstreamValidation;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.RedisHelper;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static org.testng.Assert.assertTrue;

public class IntegrationValidWorkflow extends MockServiceDP {
    FlowMapper flowMapper = new FlowMapper();
    Initialize gameofthrones = new Initialize();
    RedisHelper redisHelper = new RedisHelper();
    CRMUpstreamValidation crmUpstreamValidation = new CRMUpstreamValidation();

    @Test(dataProvider = "fetchWorFlowsMT", groups = {"regression"}, description = "End to end tests for cancellation flow")
    public void integrationValidWorkflow(HashMap<String, String> flowDetails) throws IOException, InterruptedException{

        String ffStatus = flowDetails.get("ffStatus").toString();
        String deliveryStatus = flowDetails.get("delivery_status").toString();
        String workFlowType = flowDetails.get("workflow_type").toString();
        String flowName = flowDetails.get("flow").toString();
        String[] flow = flowMapper.setFlow().get(flowName);
        List<String> expectedFlowList = Arrays.asList(flow);
        String apiCall = null;
        String queryparam[] = new String[2];
        String userId = "7191663";
        String orderId = null;
        if(workFlowType == CRMConstants.ORDERISSUETYPE) {
            int lengthofflow = flow.length;
            System.out.println("lengthofflow \t" + lengthofflow);

            redisHelper.setValueJson("checkoutredis", 0, "user_credit_7191663", "{\"userId\":7191663,\"swiggyMoney\":0.0,\"cancellationFee\":0.0}");
            String getValue = (String) redisHelper.getValue("checkoutredis", 0, "user_credit_7191663");
            System.out.println("key value" + getValue);

            HashMap<String, String> map = crmUpstreamValidation.e2e("7507220659", "Test@2211", "757", "613982", "1", deliveryStatus, ffStatus);
            System.out.print("map returned \t " + map);

            orderId = map.get("order_id");
            Assert.assertNotNull(orderId);
            queryparam[0] = orderId;
            queryparam[1] = userId;
            apiCall = "validOrderWorkFlow";
        } else {
            apiCall = "validWorkFlow";
            queryparam[0] = workFlowType;
            queryparam[1] = userId;
        }
        HashMap<String, String> requestheaders_workflowinbound = new HashMap<String, String>();
        // Negative Test Check with invalid tenantId
        requestheaders_workflowinbound.put("tenantId", "efdeac62db1ef01652053dd85f474ef626f3710277a63e77e109fc35cdaff6e");

        GameOfThronesService workflowInvalidinbound = new GameOfThronesService("crm", apiCall, gameofthrones);
        Processor workflowInvalidinbound_response = new Processor(workflowInvalidinbound, requestheaders_workflowinbound, null, queryparam);

        Assert.assertEquals(workflowInvalidinbound_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("status"), "FAILURE");
        String expectedErrorMessage = "is not registered with us. Please contact support to register the tenant with regression.";
        expectedErrorMessage = expectedErrorMessage.replace("\"","");
        String actualErrorMessage = workflowInvalidinbound_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("exception..errorMessage").toString().replace("[", "").replace("]", "");
        System.out.println("Actual Error Message  " + actualErrorMessage);
        assertTrue(actualErrorMessage.contains(expectedErrorMessage),"Actual error message doesn't match with expected ");
        requestheaders_workflowinbound.remove("tenantId");

        // Positive Test Check with valid tenantId
        requestheaders_workflowinbound.put("Content-Type", "application/json");
        requestheaders_workflowinbound.put("tenantId", "eefdeac62db1ef01652053dd85f474ef626f3710277a63e77e109fc35cdaff6e");

        GameOfThronesService workflowinbound = new GameOfThronesService("crm", apiCall, gameofthrones);
        Processor workflowinbound_response = new Processor(workflowinbound, requestheaders_workflowinbound, null, queryparam);

        Assert.assertEquals(workflowinbound_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("status"), "SUCCESS");
        String addOnGroupList = workflowinbound_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.childrenNodes..id").toString().replace("[", "").replace("]", "");

        List<String> actualWorkflowList = Arrays.asList(addOnGroupList.split(","));
        System.out.println("actualWorkflowList "+actualWorkflowList);
        System.out.println("expectedFlowList "+expectedFlowList);
        assertTrue(actualWorkflowList.containsAll(expectedFlowList),"Actual result doesn't match with expected ");
    }
}

