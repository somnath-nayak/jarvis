package com.swiggy.api.erp.vms.editservice.pojo;



import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"order_id",
"edit_reason",
"edit_data",
"meta_data"
})
public class EditServiceEntry {

@JsonProperty("order_id")
private long order_id;
@JsonProperty("edit_reason")
private String edit_reason;
@JsonProperty("edit_data")
private Edit_data edit_data;
@JsonProperty("meta_data")
private Meta_data meta_data;

/**
* No args constructor for use in serialization
* 
*/
public EditServiceEntry() {
}

/**
* 
* @param edit_reason
* @param edit_data
* @param order_id
* @param meta_data
*/
public EditServiceEntry(long order_id, String edit_reason, Edit_data edit_data, Meta_data meta_data) {
super();
this.order_id = order_id;
this.edit_reason = edit_reason;
this.edit_data = edit_data;
this.meta_data = meta_data;
}

@JsonProperty("order_id")
public long getOrder_id() {
return order_id;
}

@JsonProperty("order_id")
public void setOrder_id(long order_id) {
this.order_id = order_id;
}

public EditServiceEntry withOrder_id(long order_id) {
this.order_id = order_id;
return this;
}

@JsonProperty("edit_reason")
public String getEdit_reason() {
return edit_reason;
}

@JsonProperty("edit_reason")
public void setEdit_reason(String edit_reason) {
this.edit_reason = edit_reason;
}

public EditServiceEntry withEdit_reason(String edit_reason) {
this.edit_reason = edit_reason;
return this;
}

@JsonProperty("edit_data")
public Edit_data getEdit_data() {
return edit_data;
}

@JsonProperty("edit_data")
public void setEdit_data(Edit_data edit_data) {
this.edit_data = edit_data;
}

public EditServiceEntry withEdit_data(Edit_data edit_data) {
this.edit_data = edit_data;
return this;
}

@JsonProperty("meta_data")
public Meta_data getMeta_data() {
return meta_data;
}

@JsonProperty("meta_data")
public void setMeta_data(Meta_data meta_data) {
this.meta_data = meta_data;
}

public EditServiceEntry withMeta_data(Meta_data meta_data) {
this.meta_data = meta_data;
return this;
}

@Override
public String toString() {
return new ToStringBuilder(this).append("order_id", order_id).append("edit_reason", edit_reason).append("edit_data", edit_data).append("meta_data", meta_data).toString();
}

@Override
public int hashCode() {
return new HashCodeBuilder().append(edit_reason).append(edit_data).append(order_id).append(meta_data).toHashCode();
}

@Override
public boolean equals(Object other) {
if (other == this) {
return true;
}
if ((other instanceof EditServiceEntry) == false) {
return false;
}
EditServiceEntry rhs = ((EditServiceEntry) other);
return new EqualsBuilder().append(edit_reason, rhs.edit_reason).append(edit_data, rhs.edit_data).append(order_id, rhs.order_id).append(meta_data, rhs.meta_data).isEquals();
}

}