package com.swiggy.api.erp.cms.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.dp.SolvingForRainsDP;
import com.swiggy.api.erp.cms.helper.Popv2Helper;
import framework.gameofthrones.JonSnow.Processor;
import org.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

public class PopItemCreationUsingRestId extends SolvingForRainsDP {
    String itemId;
    Popv2Helper v2 = new Popv2Helper();

    @Test(dataProvider = "PopItem", description = "Creating pop item using restaurant id")
    public void popItemCreation(String rest_id, String popUsageType)
            throws IOException, JSONException, InterruptedException {

        Processor p = v2.PopItemCreation(Integer.parseInt(rest_id), popUsageType);
        String response = p.ResponseValidator.GetBodyAsText();
        Thread.sleep(5000);
        itemId = JsonPath.read(response, "$.data.uniqueId").toString();
        Assert.assertEquals(1, p.ResponseValidator.GetNodeValueAsInt("statusCode"),"Unable to Create Item");
        Processor p1 = v2.ItemSchedule(Integer.parseInt(itemId));
        Assert.assertEquals(1, p1.ResponseValidator.GetNodeValueAsInt("statusCode"),"Unable to Item Schedule");
        System.out.println(
                "***********************************created item" + itemId + "***********************************");

    }


}
