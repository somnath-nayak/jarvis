package com.swiggy.api.erp.cms.pojo.AddItemToRest;

import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.List;

public class Trigger {

    private Metadata metadata;
    private List<Ticket> tickets = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public Trigger() {
    }

    /**
     *
     * @param tickets
     * @param metadata
     */
    public Trigger(Metadata metadata, List<Ticket> tickets) {
        super();
        this.metadata = metadata;
        this.tickets = tickets;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("metadata", metadata).append("tickets", tickets).toString();
    }

}
