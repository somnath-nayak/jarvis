package com.swiggy.api.erp.vms.tests;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.minidev.json.JSONArray;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.vms.constants.RMSConstants;
import com.swiggy.api.erp.vms.dp.RMSTDServicesTestDP;
import com.swiggy.api.erp.vms.helper.RMSHelper;
import com.swiggy.api.erp.vms.helper.RMSTDHelper;
import com.swiggy.api.sf.rng.helper.RngHelper;

import framework.gameofthrones.Cersei.ToolBox;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;

import javax.validation.constraints.AssertFalse;


public class RMSTDServicesTest extends RMSTDServicesTestDP {
	
	RMSTDHelper helper=new RMSTDHelper();
	RMSHelper helper1 = new RMSHelper();
	ArrayList<Long> list = new ArrayList<>();
    public static String discountId;
    
	SchemaValidatorUtils schemaValidatorUtils=new SchemaValidatorUtils();
	
	@Test(dataProvider = "createTD", description="Create Trade Discount")
	public void testCreateTradeDiscount(String payLoad, String accessToken,boolean isTrue) {
		 try {
       System.out.println("Payloadddddddddd:::" +payLoad);
		int count = 1;
		RMSTDHelper rmsTDHelper = new RMSTDHelper();
		Processor p = null;
		do {
			p = rmsTDHelper.createTD(payLoad, accessToken);
			System.out.println("-------------->"+p.ResponseValidator.GetBodyAsText());
			if (p.ResponseValidator.GetNodeValueAsInt("statusCode") == -5) {
				String[] res = p.ResponseValidator.GetNodeValue("data.dataRest.data").split(" ");
				RngHelper.disableEnableTDFromMarketingDashboard(res[5], "false");
				RngHelper.disableEnableTDFromMarketingDashboard(res[6], "false");
			} else {
				break;
			}
		} while (count++ < 5);
		
		if(isTrue) {
		String resp=p.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/createTD");
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		Assert.assertTrue(missingNodeList.isEmpty(),
				missingNodeList + " Nodes Are Missing Or Not Matching For CREATE TD API");
		boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
		Assert.assertTrue(!isEmpty);
		}
	
		
		
		Assert.assertEquals(0, p.ResponseValidator.GetNodeValueAsInt("statusCode"));
		Assert.assertEquals("Successfully created Restaurant Level discount",
				p.ResponseValidator.GetNodeValue("statusMessage"));
		discountId = JsonPath.read(p.ResponseValidator.GetBodyAsText(), "$.data.dataRest.data").toString();
		Assert.assertNotNull(JsonPath.read(p.ResponseValidator.GetBodyAsText(), "$.data").toString(), "td id not is null");
		System.out.println(JsonPath.read(p.ResponseValidator.GetBodyAsText(), "$.data.dataRest.data").toString());
		Assert.assertTrue(Long
				.parseLong(JsonPath.read(p.ResponseValidator.GetBodyAsText(), "$.data.dataRest.data").toString()) > 0);
		list.add(Long.parseLong(JsonPath.read(p.ResponseValidator.GetBodyAsText(), "$.data.dataRest.data").toString()));
		
			} catch (Exception e) {
				e.printStackTrace();
			}
			  finally{
				  System.out.println("asfasfasfafafas: "+discountId);
				  Processor processor =RngHelper.disableEnableTDFromMarketingDashboard(discountId, "false");
				  Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), "done");
			      Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), 0);
			  }
		
	}
	
	@Test(dataProvider = "createTD1", description="Create Trade Discount with discount Percentage less than 1 or greater than 50")
	public void testCreateWithPercentageDiscount(String payLoad, String accessToken) throws IOException, ProcessingException {
		RMSTDHelper rmsTDHelper = new RMSTDHelper();
		Processor p = null;
			p = rmsTDHelper.createTD(payLoad, accessToken);
		/*	
			String resp=p.ResponseValidator.GetBodyAsText();
			String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/SchemaSet/JSON/VMS/createTD1.txt");
			List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
			Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For POP Aggreagtor API");
			boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
			System.out.println("Contain empty nodes => " + isEmpty);
		*/
		Assert.assertEquals(-1, p.ResponseValidator.GetNodeValueAsInt("statusCode"));
		Assert.assertEquals("Percentage value cannot be less than 1 or greater than 50. Please ensure that Percentage value lies within 1 and 50",
				p.ResponseValidator.GetNodeValue("statusMessage"));
		}
	
	@Test(dataProvider = "createTD2", description="Create Trade Discount with Flat discount less than 10 or greater than 300")
	public void testCreateWithFlatDiscount(String payLoad, String accessToken) throws IOException, ProcessingException {
		RMSTDHelper rmsTDHelper = new RMSTDHelper();
		Processor p = null;
			p = rmsTDHelper.createTD(payLoad, accessToken);
		/*	
			String resp=p.ResponseValidator.GetBodyAsText();
			String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/SchemaSet/JSON/VMS/createTD2.txt");
			List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
			Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For POP Aggreagtor API");
			boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
			System.out.println("Contain empty nodes => " + isEmpty);
		*/
		Assert.assertEquals(-1, p.ResponseValidator.GetNodeValueAsInt("statusCode"));
		Assert.assertEquals("Flat value cannot be less than 10 or greater than 300. Please ensure that Flat value lies within 10 and 300",
				p.ResponseValidator.GetNodeValue("statusMessage"));
		}

	@Test(dataProvider = "createTD3", description="Create Trade Discount with invalid minimum cart amount")
	public void testCreateWithInValidMinCart(String payLoad, String accessToken) throws IOException, ProcessingException {
		RMSTDHelper rmsTDHelper = new RMSTDHelper();
		Processor p = null;
			p = rmsTDHelper.createTD(payLoad, accessToken);
		/*	
			String resp=p.ResponseValidator.GetBodyAsText();
			String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/SchemaSet/JSON/VMS/createTD3.txt");
			List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
			Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For POP Aggreagtor API");
			boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
			System.out.println("Contain empty nodes => " + isEmpty);
		*/
		Assert.assertEquals(-5, p.ResponseValidator.GetNodeValueAsInt("statusCode"));
		Assert.assertEquals("Failed in creating Restaurant Level discount",
				p.ResponseValidator.GetNodeValue("statusMessage"));
		}
	
	@Test(dataProvider = "createTD4", description="Create Trade Discount for non accessable restaurants")
	public void testCreateWithNonAccessibleRestaurants(String payLoad, String accessToken) throws IOException, ProcessingException {
		int count = 1;
		RMSTDHelper rmsTDHelper = new RMSTDHelper();
		Processor p = null;
			p = rmsTDHelper.createTD(payLoad, accessToken);
		/*	
			String resp=p.ResponseValidator.GetBodyAsText();
			String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/SchemaSet/JSON/VMS/createTD4.txt");
			List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
			Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For POP Aggreagtor API");
			boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
			System.out.println("Contain empty nodes => " + isEmpty);
		*/
		Assert.assertEquals(-3, p.ResponseValidator.GetNodeValueAsInt("statusCode"));
		Assert.assertEquals("Invalid Session",
				p.ResponseValidator.GetNodeValue("statusMessage"));
		}
	
	@Test(dataProvider = "createTD5", description="Create invalid free_delivery Discount")
	public void testCreateInvalidFreeDeliveryTD(String payLoad, String accessToken) throws IOException, ProcessingException {
		RMSTDHelper rmsTDHelper = new RMSTDHelper();
		Processor p = null;
			p = rmsTDHelper.createTD(payLoad, accessToken);
		/*	
			String resp=p.ResponseValidator.GetBodyAsText();
			String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/SchemaSet/JSON/VMS/createTD5.txt");
			List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
			Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For POP Aggreagtor API");
			boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
			System.out.println("Contain empty nodes => " + isEmpty);
		*/
		Assert.assertEquals(-5, p.ResponseValidator.GetNodeValueAsInt("statusCode"));
		Assert.assertEquals("Failed in creating Restaurant Level discount",
				p.ResponseValidator.GetNodeValue("statusMessage"));
		}

	@Test(dataProvider = "createTD6", description="Create inavlid Discount")
	public void testCreateWithInvalidDiscountData(String payLoad, String accessToken) throws IOException, ProcessingException {
		RMSTDHelper rmsTDHelper = new RMSTDHelper();
		Processor p = null;
			p = rmsTDHelper.createTD(payLoad, accessToken);
		/*	
			String resp=p.ResponseValidator.GetBodyAsText();
			String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/SchemaSet/JSON/VMS/createTD6.txt");
			List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
			Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For POP Aggreagtor API");
			boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
			System.out.println("Contain empty nodes => " + isEmpty);
		*/
		Assert.assertEquals(-5, p.ResponseValidator.GetNodeValueAsInt("statusCode"));
		Assert.assertEquals("Failed to create discount",
				p.ResponseValidator.GetNodeValue("statusMessage"));
		}
	
	@Test(dataProvider = "editTD", description="Edit discount amount")
	public void testEditTD(String payLoad, String accessToken,boolean isTrue) {
		try {
		       System.out.println("Payloadddddddddd:::" +payLoad);
				int count = 1;
				RMSTDHelper rmsTDHelper = new RMSTDHelper();
				Processor p = null;
				do {
					p = rmsTDHelper.editTD(payLoad, accessToken);
					System.out.println("-------------->"+p.ResponseValidator.GetBodyAsText());
					if (p.ResponseValidator.GetNodeValueAsInt("statusCode") == -5) {
						String[] res = p.ResponseValidator.GetNodeValue("data.dataRest.data").split(" ");
						RngHelper.disableEnableTDFromMarketingDashboard(res[9], "false");
						RngHelper.disableEnableTDFromMarketingDashboard(res[10], "false");
					} else {
						break;
					}
				} while (count++ < 5);
				
				if(isTrue) {
					String resp=p.ResponseValidator.GetBodyAsText();
					String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/editTD");
					List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
					Assert.assertTrue(missingNodeList.isEmpty(),
							missingNodeList + " Nodes Are Missing Or Not Matching For CREATE TD API");
					boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
					Assert.assertTrue(!isEmpty);
					}
				
		Assert.assertEquals(0, p.ResponseValidator.GetNodeValueAsInt("statusCode"));
		Assert.assertEquals("Successfully Edited Restaurant Level discount",
				p.ResponseValidator.GetNodeValue("statusMessage"));
		Assert.assertNotNull(JsonPath.read(p.ResponseValidator.GetBodyAsText(), "$.data").toString(), "td id is null");

		 }
		catch (Exception e) {
			e.printStackTrace();
		}

	}

		
	@Test(dataProvider="getTDbyRest",groups={"sanity","smoke"},description="Get Trade Discount of a restaurant")
		public void getTDbyRestSanity(ArrayList<String> restaurantId, String getDiscountbyRestMessage,boolean isTrue) throws IOException, ProcessingException
			{

			    String sessionid=helper.getLoginToken(RMSTDServicesTestDP.VENDOR_USERNAME, RMSTDServicesTestDP.VENDOR_PASSWORD);	
			    Processor processor=helper.getTDbyRest(sessionid, restaurantId);
			    String resp=processor.ResponseValidator.GetBodyAsText();
			    System.out.println(resp);
				if(isTrue) {
		
					String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/getTDbyRest");
					List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
					Assert.assertTrue(missingNodeList.isEmpty(),
							missingNodeList + " Nodes Are Missing Or Not Matching For CREATE TD API");
					boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
					Assert.assertTrue(!isEmpty);
					}
				Assert.assertTrue(helper1.aggregateResponseAndStatusCodes(processor));
	            Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), getDiscountbyRestMessage);
	            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..restaurantId"));
	            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id"));
	            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..valid_from"));
	            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..valid_till"));
	            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..campaign_type"));
	            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..restaurantList"));
			            
			}

	@Test(dataProvider="getTDbyRest1",groups={"regression"}, description="Get Trade Discount of a restaurant")
	public void getTDbyRestRegression(ArrayList<String> restaurantId, int expectedStatusCode, String expectedStatusMessage) throws IOException, ProcessingException
		{

		    String sessionid=helper.getLoginToken(RMSTDServicesTestDP.VENDOR_USERNAME, RMSTDServicesTestDP.VENDOR_PASSWORD);	
		    Processor processor=helper.getTDbyRest(sessionid, restaurantId);
		    String resp=processor.ResponseValidator.GetBodyAsText();
		/*
		    String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/Data/SchemaSet/Json/VMS/getTDbyRest1.txt");;
			List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
			Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For POP Aggreagtor API");
			boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
			System.out.println("Contain empty nodes => " + isEmpty);
		*/	
			Assert.assertTrue(helper1.aggregateResponseAndStatusCodes(processor));
            Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), expectedStatusMessage); 
		}
	  @Test(dataProvider="getTDdetails",groups={"sanity","smoke"},description="Get Trade Discount details")
			public void getTDdetails(String restaurantId, int discountId, String getDiscountDetailsMessage) throws IOException, ProcessingException
				{

				    String sessionid=helper.getLoginToken(RMSTDServicesTestDP.VENDOR_USERNAME, RMSTDServicesTestDP.VENDOR_PASSWORD);	  
				    Processor processor=helper.getTDdetails(sessionid, restaurantId, discountId);
				    String resp=processor.ResponseValidator.GetBodyAsText();
				    
					String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/getTDdetails");
					List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
					Assert.assertTrue(missingNodeList.isEmpty(),
							missingNodeList + " Nodes Are Missing Or Not Matching For getTDDetails API");
					boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
					Assert.assertTrue(!isEmpty);
				    
				    
		            Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), getDiscountDetailsMessage); 
		            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
		            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..valid_from"));
		            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..valid_till"));
		            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..campaign_type"));
		            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..restaurantList"));
				   
					
				}
	  
	  @Test(dataProvider="getTDdetails1",groups={"regression"} ,description="Get Trade Discount details")
		public void getTDdetailsRegression(String restaurantId, int discountId, int expectedStatusCode, String expectedStatusMessage) throws IOException, ProcessingException
			{

			    String sessionid=helper.getLoginToken(RMSTDServicesTestDP.VENDOR_USERNAME, RMSTDServicesTestDP.VENDOR_PASSWORD);	
			    Processor processor=helper.getTDdetails(sessionid, restaurantId, discountId);
//				String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/Data/SchemaSet/Json/VMS/getTDdetails1.txt");;
//				List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
//				Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For POP Aggreagtor API");
//				boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
//				System.out.println("Contain empty nodes => " + isEmpty);
				Assert.assertTrue(helper1.aggregateResponseAndStatusCodes(processor));
	            Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), expectedStatusMessage); 				
			}


	  @Test(dataProvider="metaData",groups={"sanity","smoke"},description="Get Trade Discount meta data")
			public void testTDmetaData(String restaurantId,String getMetaMessage) throws IOException, ProcessingException
				{

				    String sessionid=helper.getLoginToken(RMSTDServicesTestDP.VENDOR_USERNAME, RMSTDServicesTestDP.VENDOR_PASSWORD);	
				    Processor processor=helper.metaData(sessionid, restaurantId);
   
				  
				    	String resp=processor.ResponseValidator.GetBodyAsText();
						String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/getTDmetaData");
						List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
						Assert.assertTrue(missingNodeList.isEmpty(),
								missingNodeList + " Nodes Are Missing Or Not Matching For metaData  API");
						boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
						Assert.assertTrue(!isEmpty);
				    
				    
					Assert.assertTrue(helper1.aggregateResponseAndStatusCodes(processor));
		            Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), getMetaMessage); 
		            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..type"));
		            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..restrictionKey"));
		             
				}


	  @Test(dataProvider="metaData1",groups={"regression"}, description="Get Trade Discount meta data with invalid data")
		public void testTDmetaDataRegression(String restaurantId,int expectedStatusCode, String expectedStatusMessage) throws IOException, ProcessingException
			{

			    String sessionid=helper.getLoginToken(RMSTDServicesTestDP.VENDOR_USERNAME, RMSTDServicesTestDP.VENDOR_PASSWORD);	
			    Processor processor=helper.metaData(sessionid, restaurantId);
//				String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/Data/SchemaSet/Json/VMS/getTDmetaData1.txt");;
//				List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
//				Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For POP Aggreagtor API");
//				boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
//				System.out.println("Contain empty nodes => " + isEmpty);
				Assert.assertTrue(helper1.aggregateResponseAndStatusCodes(processor));
	            Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), expectedStatusMessage); 
	             
			}

	
	  
	  @Test(dataProvider = "createCategoryTD", description="Create Category level Trade Discount")
		public void createCategoryTD(String payLoad, String accessToken) {
			 try {
	       System.out.println("Payloadddddddddd:::" +payLoad);
			int count = 1;
			RMSTDHelper rmsTDHelper = new RMSTDHelper();
			Processor p = null;
			do {
				p = rmsTDHelper.createTD(payLoad, accessToken);
				if (p.ResponseValidator.GetNodeValueAsInt("statusCode") == -5) {
					String[] res = p.ResponseValidator.GetNodeValue("data.dataCat.data").split(" ");
					RngHelper.disableEnableTDFromMarketingDashboard(res[5], "false");
					RngHelper.disableEnableTDFromMarketingDashboard(res[6], "false");
				} else {
					break;
				}
			} while (count++ < 5);
			Assert.assertEquals(0, p.ResponseValidator.GetNodeValueAsInt("statusCode"));
			Assert.assertEquals("Successfully created Category Level discount",
					p.ResponseValidator.GetNodeValue("statusMessage"));
			discountId = JsonPath.read(p.ResponseValidator.GetBodyAsText(), "$.data.dataCat.data").toString();
			Assert.assertNotNull(JsonPath.read(p.ResponseValidator.GetBodyAsText(), "$.data").toString(), "td id not is null");
			System.out.println(JsonPath.read(p.ResponseValidator.GetBodyAsText(), "$.data.dataCat.data").toString());
			Assert.assertTrue(Long
					.parseLong(JsonPath.read(p.ResponseValidator.GetBodyAsText(), "$.data.dataCat.data").toString()) > 0);
			list.add(Long.parseLong(JsonPath.read(p.ResponseValidator.GetBodyAsText(), "$.data.dataCat.data").toString()));
			
				} catch (Exception e) {
					e.printStackTrace();
				}
//				  finally{
//					  System.out.println("asfasfasfafafas: "+discountId);
//					  Processor processor =RngHelper.disableEnableTDFromMarketingDashboard(discountId, "false");
//					  Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), "done");
//				      Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), 0);
//				  }
//			
		}

	  
		@Test(dataProvider = "createCatTD1", description="Create Category level Trade Discount with discount Percentage less than 1 or greater than 50")
		public void createCatTDWithPercentageDiscount(String payLoad, String accessToken) throws IOException, ProcessingException {
			RMSTDHelper rmsTDHelper = new RMSTDHelper();
			Processor p = null;
				p = rmsTDHelper.createTD(payLoad, accessToken);
			/*	
				String resp=p.ResponseValidator.GetBodyAsText();
				String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/SchemaSet/JSON/VMS/createTD1.txt");
				List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
				Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For POP Aggreagtor API");
				boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
				System.out.println("Contain empty nodes => " + isEmpty);
			*/	
			Assert.assertEquals(-1, p.ResponseValidator.GetNodeValueAsInt("statusCode"));
			Assert.assertEquals("Percentage value cannot be less than 1 or greater than 50. Please ensure that Percentage value lies within 1 and 50",
					p.ResponseValidator.GetNodeValue("statusMessage"));
			}
		
		@Test(dataProvider = "createCatTD2", description="Create Category Trade Discount with Flat discount less than 10 or greater than 300")
		public void createCatTDWithFlatDiscount(String payLoad, String accessToken) throws IOException, ProcessingException {
			RMSTDHelper rmsTDHelper = new RMSTDHelper();
			Processor p = null;
				p = rmsTDHelper.createTD(payLoad, accessToken);
			/*	
				String resp=p.ResponseValidator.GetBodyAsText();
				String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/SchemaSet/JSON/VMS/createTD2.txt");
				List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
				Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For POP Aggreagtor API");
				boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
				System.out.println("Contain empty nodes => " + isEmpty);
            */		
			Assert.assertEquals(-1, p.ResponseValidator.GetNodeValueAsInt("statusCode"));
			Assert.assertEquals("Flat value cannot be less than 10 or greater than 300. Please ensure that Flat value lies within 10 and 300",
					p.ResponseValidator.GetNodeValue("statusMessage"));
			}

		@Test(dataProvider = "createCatTD3", description="Create Category Trade Discount with invalid minimum cart amount")
		public void createCatTDWithInvalidMinCartAmount(String payLoad, String accessToken) throws ProcessingException, IOException {
			RMSTDHelper rmsTDHelper = new RMSTDHelper();
			Processor p = null;
				p = rmsTDHelper.createTD(payLoad, accessToken);
            /*
				String resp=p.ResponseValidator.GetBodyAsText();
				String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/SchemaSet/JSON/VMS/createCatTD3.txt");
				List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
				Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For POP Aggreagtor API");
				boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
				System.out.println("Contain empty nodes => " + isEmpty);
	        */
			Assert.assertEquals(-5, p.ResponseValidator.GetNodeValueAsInt("statusCode"));
			Assert.assertEquals("Failed in creating Category Level discount",
					p.ResponseValidator.GetNodeValue("statusMessage"));
			}
	
		
		@Test(dataProvider = "editCatTD", description="Edit discount amount")
		public void editCatTD(String payLoad, String accessToken) {
			
			try {
			       System.out.println("Payloadddddddddd:::" +payLoad);
					int count = 1;
					RMSTDHelper rmsTDHelper = new RMSTDHelper();
					Processor p = null;
					do {
						p = rmsTDHelper.editTD(payLoad, accessToken);
						if (p.ResponseValidator.GetNodeValueAsInt("statusCode") == -5) {
							String[] res = p.ResponseValidator.GetNodeValue("data.dataRest.data").split(" ");
							RngHelper.disableEnableTDFromMarketingDashboard(res[9], "false");
							RngHelper.disableEnableTDFromMarketingDashboard(res[10], "false");
						} else {
							break;
						}
					} while (count++ < 5);
					
			System.out.println("Edit TDDDDDD" +p.ResponseValidator.GetBodyAsText());
			Assert.assertEquals(0, p.ResponseValidator.GetNodeValueAsInt("statusCode"));
			Assert.assertEquals("Successfully Edited Category Level discount",
					p.ResponseValidator.GetNodeValue("statusMessage"));
			Assert.assertNotNull(JsonPath.read(p.ResponseValidator.GetBodyAsText(), "$.data").toString(), "td id is null");
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		}

		@Test(dataProvider = "editCatTD3", description="Edit restaurant")
		public void editCatTD3(String payLoad, String accessToken) {
			 try {
			       System.out.println("Payloadddddddddd:::" +payLoad);
					int count = 1;
					RMSTDHelper rmsTDHelper = new RMSTDHelper();
					Processor p = null;
					do {
						p = rmsTDHelper.editTD(payLoad, accessToken);
						if (p.ResponseValidator.GetNodeValueAsInt("statusCode") == -5) {
							String[] res = p.ResponseValidator.GetNodeValue("data.dataRest.data").split(" ");
							RngHelper.disableEnableTDFromMarketingDashboard(res[9], "false");
							RngHelper.disableEnableTDFromMarketingDashboard(res[10], "false");
						} else {
							break;
						}
					} while (count++ < 5);
					
			Assert.assertEquals(0, p.ResponseValidator.GetNodeValueAsInt("statusCode"));
			Assert.assertEquals("Successfully Edited Restaurant Level discount",
					p.ResponseValidator.GetNodeValue("statusMessage"));
			Assert.assertNotNull(JsonPath.read(p.ResponseValidator.GetBodyAsText(), "$.data").toString(), "td id is null");

			 }
			catch (Exception e) {
				e.printStackTrace();
			}

		}

	@Test(dataProvider="getTDMenuData",groups={"sanity","smoke"},description="Get Trade Discount of a restaurant")
	public void getTDMenuData(String accessToken, String restaurantId, String statusMessage) throws IOException, ProcessingException
	{
		Processor processor=helper.getTDMenuData(accessToken, restaurantId);
		Assert.assertTrue(helper1.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		if(processor.ResponseValidator.DoesNodeExists("$.data.id")) {
			Assert.assertEquals(processor.ResponseValidator.GetNodeValue("$.data.id"), restaurantId);
			Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.name"));
			Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.categories.[0].id"));
			Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.categories.[0].name"));
			Assert.assertTrue(processor.ResponseValidator.DoesNodeExists("$.data.categories.[0].menu"));
			Assert.assertTrue(processor.ResponseValidator.DoesNodeExists("$.data.categories.[0].subCategories"));
		}

	}

	@Test(dataProvider="getTDMenuData",groups={"sanity","smoke"},description="Get Trade Discount of a restaurant")
	public void getTDMetrics(String accessToken, String restaurantId, String statusMessage) throws IOException, ProcessingException
	{
		String discountId = null;
		ArrayList<String> lst= new ArrayList<String>();
		lst.add(restaurantId);
		JSONArray ja = helper.getTDbyRest(accessToken, lst).ResponseValidator.GetNodeValueAsJsonArray("$.data..id");
		if(ja.size()>0) {
			discountId = ja.get(0).toString();
		}

		Processor processor=helper.getTDMetrics(accessToken, restaurantId, discountId);
		Assert.assertTrue(helper1.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		if(processor.ResponseValidator.DoesNodeExists("$.data.display_flag")) {
			Assert.assertNotNull(processor.ResponseValidator.GetNodeValue("$.data.display_flag"));
			Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.display_flag_vms"));
			Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.number_orders"));
			Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.aov"));
			Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurant_first_order"));
			Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discount"));
			Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restrictedDate"));

		}

	}

	@Test(dataProvider="getTDMenuDataRegression",groups={"regression"},description="Get Trade Discount of a restaurant")
	public void getTDMetricsRegression(String accessToken, String restaurantId, String discountId, String statusMessage, int statusCode) throws IOException, ProcessingException
	{
		Processor processor=helper.getTDMetrics(accessToken, restaurantId, discountId);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);

	}
		
			  
//	@AfterClass
	public void a1() {
		for (Long l : list) {
			RngHelper.disableEnableTD(String.valueOf(l), "false");
		}
	}

}
