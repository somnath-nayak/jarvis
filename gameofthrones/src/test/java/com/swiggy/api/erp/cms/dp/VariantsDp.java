package com.swiggy.api.erp.cms.dp;

import com.swiggy.api.erp.cms.constants.MenuConstants;
import com.swiggy.api.erp.cms.helper.FullMenuHelper;
import com.swiggy.api.erp.cms.pojo.BulkVariant.BulkVariant;
import com.swiggy.api.erp.cms.pojo.BulkVariant.Entities;
import com.swiggy.api.erp.cms.pojo.Items.Gst_details;
import com.swiggy.api.erp.cms.pojo.Variants.Entity;
import com.swiggy.api.erp.cms.pojo.Variants.Variants;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by kiran.j on 2/21/18.
 */
public class VariantsDp {

    @DataProvider(name = "createVariant")
    public Iterator<Object[]> createVariant() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        Variants variants = new Variants();
        variants.build();

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), new String[]{}, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3)});

        Entity entity = new Entity();
        entity.build();
        entity.setId("");
        variants.setEntity(entity);

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), MenuConstants.addvariant_id, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3)});

        entity = new Entity();
        entity.build();
        entity.setName("");
        variants.setEntity(entity);

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), MenuConstants.addvariant_name, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3)});

        entity = new Entity();
        entity.build();
        entity.setVariant_group_id("");
        variants.setEntity(entity);

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), MenuConstants.addvariant_groupid, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3)});

        entity = new Entity();
        entity.build();
        entity.setPrice(-10);
        variants.setEntity(entity);

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), MenuConstants.addvariant_price, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3)});

        entity = new Entity();
        entity.build();
        Gst_details gst_details = entity.getGst_details();
        gst_details.setCgst(MenuConstants.negative_cgst);
        entity.setGst_details(gst_details);
        variants.setEntity(entity);

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), MenuConstants.variant_cgst, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3)});

        entity = new Entity();
        entity.build();
        gst_details = entity.getGst_details();
        gst_details.setIgst(MenuConstants.negative_igst);
        entity.setGst_details(gst_details);
        variants.setEntity(entity);

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), MenuConstants.variant_igst, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3)});

        entity = new Entity();
        entity.build();
        gst_details = entity.getGst_details();
        gst_details.setSgst(MenuConstants.negative_sgst);
        entity.setGst_details(gst_details);
        variants.setEntity(entity);

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), MenuConstants.variant_sgst, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3)});

        entity = new Entity();
        entity.build();
        entity.setId(null);
        variants.setEntity(entity);

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), MenuConstants.addvariant_id, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3)});

        entity = new Entity();
        entity.build();
        entity.setName(null);
        variants.setEntity(entity);

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), MenuConstants.addvariant_name, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3)});

        entity = new Entity();
        entity.build();
        entity.setVariant_group_id(null);
        variants.setEntity(entity);

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), MenuConstants.addvariant_groupid, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3)});

        entity = new Entity();
        entity.build();
        entity.setGst_details(null);
        variants.setEntity(entity);

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), new String[]{}, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3)});

        return obj.iterator();
    }

    @DataProvider(name = "updateVariant")
    public Iterator<Object[]> updateVariant() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        Variants variants = new Variants();
        variants.build();

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), new String[]{}, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3), variants.getEntity().getId()});

        Entity entity = new Entity();
        entity.build();
        entity.setId("");
        variants.setEntity(entity);

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), MenuConstants.updatetvariant_id, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3), "123"});

        entity = new Entity();
        entity.build();
        entity.setName("");
        variants.setEntity(entity);

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), MenuConstants.updatevariant_name, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3), variants.getEntity().getId()});

        entity = new Entity();
        entity.build();
        entity.setVariant_group_id("");
        variants.setEntity(entity);

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), MenuConstants.addvariant_groupid, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3), variants.getEntity().getId()});

        entity = new Entity();
        entity.build();
        entity.setPrice(-10);
        variants.setEntity(entity);

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), MenuConstants.addvariant_price, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3), variants.getEntity().getId()});

        entity = new Entity();
        entity.build();
        Gst_details gst_details = entity.getGst_details();
        gst_details.setCgst(MenuConstants.negative_cgst);
        entity.setGst_details(gst_details);
        variants.setEntity(entity);

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), MenuConstants.variant_cgst, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3), variants.getEntity().getId()});

        entity = new Entity();
        entity.build();
        gst_details = entity.getGst_details();
        gst_details.setIgst(MenuConstants.negative_igst);
        entity.setGst_details(gst_details);
        variants.setEntity(entity);

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), MenuConstants.variant_igst, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3), variants.getEntity().getId()});

        entity = new Entity();
        entity.build();
        gst_details = entity.getGst_details();
        gst_details.setSgst(MenuConstants.negative_sgst);
        entity.setGst_details(gst_details);
        variants.setEntity(entity);

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), MenuConstants.variant_sgst, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3), variants.getEntity().getId()});

        entity = new Entity();
        entity.build();
        entity.setId(null);
        variants.setEntity(entity);

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), MenuConstants.addvariant_id, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3),"123"});

        entity = new Entity();
        entity.build();
        entity.setName(null);
        variants.setEntity(entity);

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), new String[]{}, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3), variants.getEntity().getId()});

        entity = new Entity();
        entity.build();
        entity.setVariant_group_id(null);
        variants.setEntity(entity);

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), MenuConstants.addvariant_groupid, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3), variants.getEntity().getId()});

        entity = new Entity();
        entity.build();
        entity.setGst_details(null);
        variants.setEntity(entity);

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), new String[]{}, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3), variants.getEntity().getId()});

        return obj.iterator();
    }

    @DataProvider(name = "createbulkVariant")
    public Iterator<Object[]> createbulkVariant() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        BulkVariant variants = new BulkVariant();
        variants.build();

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), new String[]{}, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3)});

        Entities entity = new Entities();
        entity.build();
        entity.setId("");
        variants.setEntities(new Entities[]{entity});

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), MenuConstants.bulkvariant_id, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3)});

        entity = new Entities();
        entity.build();
        entity.setName("");
        variants.setEntities(new Entities[]{entity});

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), MenuConstants.bulkvariant_name, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3)});

        entity = new Entities();
        entity.build();
        entity.setVariant_group_id("");
        variants.setEntities(new Entities[]{entity});

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), MenuConstants.bulkvariant_groupdid, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3)});

        entity = new Entities();
        entity.build();
        entity.setPrice(-10);
        variants.setEntities(new Entities[]{entity});

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), MenuConstants.bulkvariant_price, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3)});

        entity = new Entities();
        entity.build();
        Gst_details gst_details = entity.getGst_details();
        gst_details.setCgst(MenuConstants.negative_cgst);
        entity.setGst_details(gst_details);
        variants.setEntities(new Entities[]{entity});

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), MenuConstants.bulkvariant_cgst, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3)});

        entity = new Entities();
        entity.build();
        gst_details = entity.getGst_details();
        gst_details.setIgst(MenuConstants.negative_igst);
        entity.setGst_details(gst_details);
        variants.setEntities(new Entities[]{entity});

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), MenuConstants.bulkvariant_igst, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3)});

        entity = new Entities();
        entity.build();
        gst_details = entity.getGst_details();
        gst_details.setSgst(MenuConstants.negative_sgst);
        entity.setGst_details(gst_details);
        variants.setEntities(new Entities[]{entity});

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), MenuConstants.bulkvariant_sgst, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3)});

        entity = new Entities();
        entity.build();
        entity.setId(null);
        variants.setEntities(new Entities[]{entity});

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), MenuConstants.bulkvariant_id, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3)});

        entity = new Entities();
        entity.build();
        entity.setName(null);
        variants.setEntities(new Entities[]{entity});

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), MenuConstants.bulkvariant_name, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3)});

        entity = new Entities();
        entity.build();
        entity.setVariant_group_id(null);
        variants.setEntities(new Entities[]{entity});

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), MenuConstants.bulkvariant_groupdid, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3)});

        entity = new Entities();
        entity.build();
        entity.setGst_details(null);
        variants.setEntities(new Entities[]{entity});

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), new String[]{}, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3)});

        return obj.iterator();
    }

    @DataProvider(name = "updatebulkVariant")
    public Iterator<Object[]> updatebulkVariant() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        BulkVariant variants = new BulkVariant();
        variants.build();

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), new String[]{}, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3)});

        Entities entity = new Entities();
        entity.build();
        entity.setId("");
        variants.setEntities(new Entities[]{entity});

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), MenuConstants.bulkvariant_id, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3)});

        entity = new Entities();
        entity.build();
        entity.setName("");
        variants.setEntities(new Entities[]{entity});

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), MenuConstants.updatebulkvariant_name, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3)});

        entity = new Entities();
        entity.build();
        entity.setVariant_group_id("");
        variants.setEntities(new Entities[]{entity});

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), MenuConstants.bulkvariant_groupdid, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3)});

        entity = new Entities();
        entity.build();
        entity.setPrice(-10);
        variants.setEntities(new Entities[]{entity});

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), MenuConstants.bulkvariant_price, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3)});

        entity = new Entities();
        entity.build();
        Gst_details gst_details = entity.getGst_details();
        gst_details.setCgst(MenuConstants.negative_cgst);
        entity.setGst_details(gst_details);
        variants.setEntities(new Entities[]{entity});

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), MenuConstants.bulkvariant_cgst, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3)});

        entity = new Entities();
        entity.build();
        gst_details = entity.getGst_details();
        gst_details.setIgst(MenuConstants.negative_igst);
        entity.setGst_details(gst_details);
        variants.setEntities(new Entities[]{entity});

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), MenuConstants.bulkvariant_igst, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3)});

        entity = new Entities();
        entity.build();
        gst_details = entity.getGst_details();
        gst_details.setSgst(MenuConstants.negative_sgst);
        entity.setGst_details(gst_details);
        variants.setEntities(new Entities[]{entity});

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), MenuConstants.bulkvariant_sgst, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3)});

        entity = new Entities();
        entity.build();
        entity.setId(null);
        variants.setEntities(new Entities[]{entity});

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), MenuConstants.bulkvariant_id, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3)});

        entity = new Entities();
        entity.build();
        entity.setName(null);
        variants.setEntities(new Entities[]{entity});

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), new String[]{}, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3)});

        entity = new Entities();
        entity.build();
        entity.setVariant_group_id(null);
        variants.setEntities(new Entities[]{entity});

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), MenuConstants.bulkvariant_groupdid, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3)});

        entity = new Entities();
        entity.build();
        entity.setGst_details(null);
        variants.setEntities(new Entities[]{entity});

        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(variants), new String[]{}, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id3)});

        return obj.iterator();
    }
}
