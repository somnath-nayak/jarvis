package com.swiggy.api.erp.ff.dp.FraudServiceDP;

import com.swiggy.api.erp.ff.POJO.CouponFraudCheck.Coupon_details;
import com.swiggy.api.erp.ff.POJO.CouponFraudCheck.Customer_details;
import com.swiggy.api.erp.ff.constants.FraudServiceConstants;

import java.util.ArrayList;
import java.util.List;

public class CouponAbusesData {


    Customer_details cu1,cu2,cu3,cu4;
    Coupon_details cou1,cou2,cou3,cou4,cou5;

    List<Coupon_details> couList = new ArrayList<>();
    List<Customer_details> cusList = new ArrayList<>();

    public Object[][] getData()
    {

        cu1 = new Customer_details();
        cu2 = new Customer_details();

        cu1.setDevice_id(FraudServiceConstants.deviceId1);
        cu2.setDevice_id(FraudServiceConstants.deviceId2);


        cou1 = new Coupon_details();
        cou2 = new Coupon_details();
        cou3 = new Coupon_details();
        cou4 = new Coupon_details();
        cou5 = new Coupon_details();

        String [] coupons1 = new String[]{"CAT1"};
        String [] coupons2 = new String[]{"CAT2"};
        String [] coupons3 = new String[]{"CAT3"};
        String [] coupons4 = new String[]{"CAT4"};
        String [] coupons5 = new String[]{"CAT5"};

        cou1.setCoupon_categories(coupons1);
        cou2.setCoupon_categories(coupons2);
        cou3.setCoupon_categories(coupons3);
        cou4.setCoupon_categories(coupons4);
        cou5.setCoupon_categories(coupons5);




        cusList.add(cu1);
        cusList.add(cu2);
        couList.add(cou1);
        couList.add(cou2);
        couList.add(cou3);
        couList.add(cou4);
        couList.add(cou5);

        return dataProviderGenerator.generatevariants2(cusList,couList);


    }

    public Object[][] getWithoutCouponDetailsData()
    {

        cu1 = new Customer_details();
//        cou1 = new Coupon_details();
//        cou2 = new Coupon_details();

        cu1.setDevice_id(FraudServiceConstants.deviceId1);

        cusList.add(cu1);
        //String[] coupons1 = new String[]{};
//        cou1.setCoupon_categories(coupons1);
//        couList.add(cou1);
//        couList.add(cou2);

        return dataProviderGenerator.generatevariants2(cusList);


    }

    public Object[][] getFieldLevelData()
    {

        cu1 = new Customer_details();
        cu2 = new Customer_details();
        cu3 = new Customer_details();
        cu4 = new Customer_details();

        cu1.setDevice_id(null);
        cu2.setDevice_id("");
        // cu3.setDevice_id("####");



        cou1 = new Coupon_details();
        cou2 = new Coupon_details();
        cou3 = new Coupon_details();
        cou4 = new Coupon_details();



        String [] coupons1 = new String[]{"CAT1"};
        //  String [] coupons2 = new String[]{null};
//        String [] coupons3 = new String[]{""};
//        String [] coupons4 = new String[]{"0000000000000000000"};


        cou1.setCoupon_categories(coupons1);
//        cou2.setCoupon_categories(coupons2);
//        cou3.setCoupon_categories(coupons3);
//        cou4.setCoupon_categories(coupons4);






        cusList.add(cu1);
        cusList.add(cu2);
        //    cusList.add(cu3);


        couList.add(cou1);
//        couList.add(cou2);
//        couList.add(cou3);
//        couList.add(cou4);

        return dataProviderGenerator.generatevariants2(cusList,couList);






    }

    public Object[][] getRmqPaymentConfirmationData()
    {
        return new Object[][]{
                {FraudServiceConstants.deviceId1,FraudServiceConstants.category5},
                {FraudServiceConstants.deviceId1,FraudServiceConstants.category6},
                {FraudServiceConstants.deviceId1,FraudServiceConstants.category7},
                {FraudServiceConstants.deviceId1,FraudServiceConstants.category9},
                {FraudServiceConstants.deviceId2,FraudServiceConstants.category6},
                {FraudServiceConstants.deviceId2,FraudServiceConstants.category7},
                {FraudServiceConstants.deviceId2,FraudServiceConstants.category8}
        };
    }

    public Object[][] getRmqOrderCancellationData()
    {
        return new Object[][]{
                {FraudServiceConstants.deviceId1,FraudServiceConstants.category5},
                {FraudServiceConstants.deviceId1,FraudServiceConstants.category6},
                {FraudServiceConstants.deviceId1,FraudServiceConstants.category7},
                {FraudServiceConstants.deviceId1,FraudServiceConstants.category8},
                {FraudServiceConstants.deviceId1,FraudServiceConstants.category9},

        };
    }

    public Object[][] getMultipleCouponCategoriesData()
    {


        cu1 = new Customer_details();
        cu2 = new Customer_details();

        cu1.setDevice_id(FraudServiceConstants.deviceId1);
        cu2.setDevice_id(FraudServiceConstants.deviceId2);


        cou1 = new Coupon_details();
        cou2 = new Coupon_details();
        cou3 = new Coupon_details();



        String [] coupons1 = new String[]{"CAT1","CAT3"};
        String [] coupons2 = new String[]{"CAT2","CAT3"};
        String [] coupons3 = new String[]{"CAT1","CAT4","CAT5"};
        cou1.setCoupon_categories(coupons1);
        cou2.setCoupon_categories(coupons2);
        cou3.setCoupon_categories(coupons3);





        cusList.add(cu1);
        cusList.add(cu2);
        couList.add(cou1);
        couList.add(cou2);
        couList.add(cou3);


        return dataProviderGenerator.generatevariants2(cusList,couList);


    }

    public Object[][]getRmqOrderCancellationPaymentConfirmation()
    {
        return new Object[][]{
                {FraudServiceConstants.deviceId1,FraudServiceConstants.category10},
                {FraudServiceConstants.deviceId1,FraudServiceConstants.category11},

        };
    }



    public Object[][] getAPIPaymentConfirmationData()
    {

        return new Object[][]{
                {FraudServiceConstants.deviceId1,FraudServiceConstants.category5},
                {FraudServiceConstants.deviceId1,FraudServiceConstants.category6},
                {FraudServiceConstants.deviceId1,FraudServiceConstants.category7},
                {FraudServiceConstants.deviceId1,FraudServiceConstants.category9},
                {FraudServiceConstants.deviceId1,FraudServiceConstants.category2}
        };

    }

    public Object[][] getAPIPaymentCancellationDataData()
    {

        return new Object[][]{
                {FraudServiceConstants.deviceId1,FraudServiceConstants.category16},
                {FraudServiceConstants.deviceId1,FraudServiceConstants.category13},
                {FraudServiceConstants.deviceId1,FraudServiceConstants.category14},
                {FraudServiceConstants.deviceId1,FraudServiceConstants.category15}
        };

    }

}
