package com.swiggy.api.erp.cms.helper;
import com.redis.E;
import com.swiggy.api.erp.cms.pojo.CategoryServiceCreateCategory.Attributes;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.pojo.CategoryServiceCreateCategory.CreateProductCategory;
import com.swiggy.api.erp.cms.pojo.Inventory.Inventory;
import com.swiggy.api.erp.cms.pojo.PricingService.Pricing;
import com.swiggy.api.erp.cms.pojo.SpinService.Bar_code;
import com.swiggy.api.erp.cms.pojo.SpinService.MainPojo;
import com.swiggy.api.erp.cms.pojo.SpinService.SpinSearch;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Daenerys.Initializer;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import framework.gameofthrones.Tyrion.RabbitMQHelper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.testng.Reporter;

import java.time.Instant;
import java.util.HashMap;
import java.util.Iterator;

public class CatalogV2Helper {
	Initialize gameofthrones = Initializer.getInitializer();
	RabbitMQHelper rmqHelper = new RabbitMQHelper();
	Pricing p=new Pricing();
	Inventory inv=new Inventory();
	ObjectMapper mp=new ObjectMapper();

	public Processor createCategory(String name) {
		Processor processor = null;
		try {
			HashMap<String, String> headers=new HashMap<>();
			headers.put("Content-Type", "application/json");
			String[] payloadparams = new String[]{name};
			GameOfThronesService service = new GameOfThronesService("cmscategoryservice", "createcategory", gameofthrones);
			processor = new Processor(service, headers, payloadparams, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return processor;
	}



	public Processor createSpin(String res) {
		Processor processor1 = null;
		HashMap<String, String> headers = new HashMap<>();
		headers.put("Content-Type", "application/json");
		headers.put("updated-by", "1234");
		headers.put("updated-by", "ashiwani");
		//headers.put("user-id", "100");
		ObjectMapper mapper =new ObjectMapper();
		Reporter.log(res,true);
		String cid= JsonPath.read(res,"$.id").toString().replace("[","").replace("]","");
		String allowedvalue=JsonPath.read(res,"$.attributes..allowed_values[0]").toString().replace("[","").replace("]","").replace("\"","");
		com.swiggy.api.erp.cms.pojo.SpinService.Attributes at=new com.swiggy.api.erp.cms.pojo.SpinService.Attributes(allowedvalue,"","",100);

		Bar_code br=new Bar_code(true,"12345","UPC");
		MainPojo mj=new MainPojo(at,br,cid,null);
		GameOfThronesService go = new GameOfThronesService("cmsproductservice", "createspin", gameofthrones);
		try{
			processor1 = new Processor(go, headers, new String[]{mapper.writeValueAsString(mj)});}
		catch (Exception e){e.printStackTrace();}
		return processor1;
	}

	public Processor getSpin(String spinid) {
		Processor processor = null;
		HashMap<String, String> headers = new HashMap<>();
		headers.put("Content-Type", "application/json");
		GameOfThronesService got = new GameOfThronesService("cmsproductservice", "getsinglespin", gameofthrones);
		String[] urlparams = {spinid};
		processor = new Processor(got, headers, null, urlparams);
		return processor;
	}

	public Processor getMulSpin(String spinids) {
		Processor processor = null;
		HashMap<String, String> headers = new HashMap<>();
		headers.put("Content-Type", "application/json");
		GameOfThronesService got = new GameOfThronesService("cmsproductservice", "getmultiplespin", gameofthrones);
		String[] queryParams = {spinids};
		processor = new Processor(got, headers, null, queryParams);
		return processor;
	}

	public Processor updateSpin(String spinid,String mname) {
		Processor processor = null;
		HashMap<String, String> headers = new HashMap<>();
		headers.put("Content-Type", "application/json");
		GameOfThronesService got = new GameOfThronesService("cmsproductservice", "updatespin", gameofthrones);
		String[] urlparams = {spinid};
		String[] payload={mname};
		processor = new Processor(got, headers, payload, urlparams);
		return processor;
	}

	public Processor searchSpin(String brand) throws Exception{
		Processor processor = null;
		HashMap<String, String> headers = new HashMap<>();
		SpinSearch sc=new SpinSearch(null,null,brand,null,null);
		ObjectMapper mapper =new ObjectMapper();
		String s1=mapper.writeValueAsString(sc);
		String payload[]={s1};
		headers.put("Content-Type", "application/json");
		GameOfThronesService got = new GameOfThronesService("cmsproductservice", "searhspin", gameofthrones);
		processor = new Processor(got, headers, payload);
		return processor;}


	public Processor createCategoryPojo() {
		Processor processor = null;
		try {
			HashMap<String, String> headers=new HashMap<>();
			JsonHelper jsonHelper = new JsonHelper();
			headers.put("Content-Type", "application/json");
			CreateProductCategory productCategory=new CreateProductCategory();
			productCategory.build();
			GameOfThronesService service = new GameOfThronesService("cmscategoryservice", "createcategory", gameofthrones);
			processor = new Processor(service, headers, new String[]{jsonHelper.getObjectToJSON(productCategory)}, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return processor;
	}

	public Processor createPojoForAddAttributesToACategory(String nameOfTheAttribute,String categoryId){
		Processor processor = null;
		String [] urlParams = {categoryId};
		try {
			HashMap<String, String> headers=new HashMap<>();
			JsonHelper jsonHelper = new JsonHelper();
			headers.put("Content-Type", "application/json");
			CreateProductCategory prdCategory [] = new CreateProductCategory[1];
			CreateProductCategory productCategory=new CreateProductCategory();
			productCategory.setName(nameOfTheAttribute);
			productCategory.build();
			prdCategory[0]=productCategory;
			GameOfThronesService service = new GameOfThronesService("cmscategoryservice", "addattributetoacategory", gameofthrones);
			processor = new Processor(service, headers, new String[]{jsonHelper.getObjectToJSON(prdCategory)}, urlParams);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return processor;
	}

	public Processor createCategoryWithRule() {
		Processor processor = null;
		try {
			HashMap<String, String> headers=new HashMap<>();
			JsonHelper jsonHelper = new JsonHelper();
			headers.put("Content-Type", "application/json");
			CreateProductCategory productCategory=new CreateProductCategory();
			productCategory.build();
			Attributes attributes=new Attributes();
			attributes.build();
			attributes.setAllowed_values(null);
			productCategory.setAttributes(new Attributes[]{attributes});
			Thread.sleep(1000);
			GameOfThronesService service = new GameOfThronesService("cmscategoryservice", "createcategory", gameofthrones);
			processor = new Processor(service, headers, new String[]{jsonHelper.getObjectToJSON(productCategory)}, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return processor;
	}

	public Processor createCategoryAttributeType() {
		Processor processor = null;
		try {
			HashMap<String, String> headers=new HashMap<>();
			JsonHelper jsonHelper = new JsonHelper();
			headers.put("Content-Type", "application/json");
			CreateProductCategory productCategory=new CreateProductCategory();
			productCategory.build();
			Attributes attributes=new Attributes();
			attributes.build();
			attributes.setType("INTEGER");
			productCategory.setAttributes(new Attributes[]{attributes});
			Thread.sleep(1000);
			GameOfThronesService service = new GameOfThronesService("cmscategoryservice", "createcategory", gameofthrones);
			processor = new Processor(service, headers, new String[]{jsonHelper.getObjectToJSON(productCategory)}, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return processor;
	}

	public Processor getCategoryById(String catId){
		Processor processor=null;
		HashMap<String,String> headers=new HashMap<>();
		headers.put("Content-Type", "application/json");
		String[] urlparams = {catId};
		GameOfThronesService service=new GameOfThronesService("cmscategoryservice", "getcategorybyid", gameofthrones);
		processor=new Processor(service,headers,null,urlparams);
		return processor;
	}
	public Processor getCategoryByName(String catName){
		Processor processor=null;
		HashMap<String,String> headers=new HashMap<>();
		headers.put("Content-Type", "application/json");
		String[] urlparams = {catName};
		GameOfThronesService service=new GameOfThronesService("cmscategoryservice", "getcategorybyname", gameofthrones);
		processor=new Processor(service,headers,null,urlparams);
		return processor;
	}

	public Processor createCategoryWithoutRule() {
		Processor processor = null;
		try {
			HashMap<String, String> headers=new HashMap<>();
			JsonHelper jsonHelper = new JsonHelper();
			headers.put("Content-Type", "application/json");
			CreateProductCategory productCategory=new CreateProductCategory();
			productCategory.build();
			Attributes attributes=new Attributes();
			attributes.build();
			attributes.setRules(null);
			productCategory.setAttributes(new Attributes[]{attributes});
			Thread.sleep(1000);
			GameOfThronesService service = new GameOfThronesService("cmscategoryservice", "createcategory", gameofthrones);
			processor = new Processor(service, headers, new String[]{jsonHelper.getObjectToJSON(productCategory)}, null);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return processor;
	}


	public Processor ingestFile(String url){
		Processor processor = null;
		HashMap<String, String> headers=new HashMap<>();
		headers.put("Content-Type", "application/json");
		headers.put("user-id", "1234");
		GameOfThronesService got= new GameOfThronesService("cmsingestionservice", "ingestfileusingurl", gameofthrones);
		String[] queryParams = {url};
		processor = new Processor(got,headers,null,queryParams);
		return processor;

	}

	public Processor createTaxonomy(String payolad,String storeid){
		Processor processor = null;
		HashMap<String, String> headers=new HashMap<>();
		headers.put("Content-Type", "application/json");
		GameOfThronesService got= new GameOfThronesService("cmstaxonmyservice", "createtaxonomy", gameofthrones);
		String[] queryParams = {storeid};
		processor = new Processor(got,headers,new String[]{payolad},queryParams);
		return processor;

	}


	public Processor getTaxonomy(String storeid,String taxid){
		Processor processor = null;
		HashMap<String, String> headers=new HashMap<>();
		headers.put("Content-Type", "application/json");
		GameOfThronesService got= new GameOfThronesService("cmstaxonmyservice", "gettaxonomy", gameofthrones);
		String[] queryParams = {storeid,taxid};
		processor = new Processor(got,headers,null,queryParams);
		return processor;

	}

	public Processor updateTaxonomy(String storeid,String taxid,String payload){
		Processor processor = null;
		HashMap<String, String> headers=new HashMap<>();
		headers.put("Content-Type", "application/json");
		GameOfThronesService got= new GameOfThronesService("cmstaxonmyservice", "updatetaxonomy", gameofthrones);
		String[] queryParams = {storeid,taxid};
		processor = new Processor(got,headers,new String[]{payload},queryParams);
		return processor;

	}

	public Processor publishTaxonomy(String storeid,String taxid){
		Processor processor = null;
		HashMap<String, String> headers=new HashMap<>();
		headers.put("Content-Type", "application/json");
		GameOfThronesService got= new GameOfThronesService("cmstaxonmyservice", "publishtaxonomy", gameofthrones);
		String[] queryParams = {storeid,taxid};
		processor = new Processor(got,headers,null,queryParams);
		return processor;

	}


	public String categoryName(){
		String CatName="TestCategoryName"+String.valueOf(Instant.now().getEpochSecond());
		return CatName;
	}


	public Processor getAllTaxonomy(String storeid){
		Processor processor = null;
		HashMap<String, String> headers=new HashMap<>();
		headers.put("Content-Type", "application/json");
		GameOfThronesService got= new GameOfThronesService("cmstaxonmyservice", "getalltaxonomy", gameofthrones);
		String[] queryParams = {storeid};
		processor = new Processor(got,headers,null,queryParams);
		return processor;

	}

	public Processor createPricing(String key,long mrp,long storeprice) throws Exception{
		Processor processor = null;
		p.setValue(key,mrp,storeprice);
		HashMap<String, String> headers=new HashMap<>();
		headers.put("Content-Type", "application/json");
		headers.put("service-line","dash_catalog");
		GameOfThronesService got= new GameOfThronesService("cmspricingservice", "createprice", gameofthrones);
		processor = new Processor(got,headers,new String[]{"["+mp.writeValueAsString(p)+"]"});
		return processor;

	}

	public Processor createPricingNegative() throws Exception{
		Processor processor = null;
		HashMap<String, String> headers=new HashMap<>();
		headers.put("Content-Type", "application/json");
		headers.put("service-line","dash_catalog");
		GameOfThronesService got= new GameOfThronesService("cmspricingservice", "createprice", gameofthrones);
		processor = new Processor(got,headers,new String[]{mp.writeValueAsString(p)});
		return processor;

	}

	public Processor getPricing(String store_sku){
		Processor processor = null;
		HashMap<String, String> headers=new HashMap<>();
		headers.put("Content-Type", "application/json");
		headers.put("service-line","dash_catalog");
		GameOfThronesService got= new GameOfThronesService("cmspricingservice", "getprice", gameofthrones);
		String[] queryParams = {store_sku};
		processor = new Processor(got,headers,null,queryParams);
		return processor;

	}

	public Processor createInventory(String key,long quantity) throws Exception {
		Processor processor = null;
		inv.setValue(key,quantity);
		HashMap<String, String> headers=new HashMap<>();
		headers.put("Content-Type", "application/json");
		headers.put("service-line","dash_catalog");
		GameOfThronesService got= new GameOfThronesService("cmsinventoryservice", "createinventory", gameofthrones);
		processor = new Processor(got,headers,new String[]{"["+mp.writeValueAsString(inv)+"]"});
		return processor;

	}

	public Processor createInventoryNegative() throws Exception {
		Processor processor = null;
		HashMap<String, String> headers=new HashMap<>();
		headers.put("Content-Type", "application/json");
		headers.put("service-line","dash_catalog");
		GameOfThronesService got= new GameOfThronesService("cmsinventoryservice", "createinventory", gameofthrones);
		processor = new Processor(got,headers,new String[]{mp.writeValueAsString(inv)});
		return processor;

	}


	public Processor getInventory(String store_sku){
		Processor processor = null;
		HashMap<String, String> headers=new HashMap<>();
		headers.put("Content-Type", "application/json");
		headers.put("service-line","dash_catalog");
		GameOfThronesService got= new GameOfThronesService("cmsinventoryservice", "getinventory", gameofthrones);
		String[] queryParams = {store_sku};
		processor = new Processor(got,headers,null,queryParams);
		return processor;

	}

	public Processor clearInventory(String key) throws Exception {
		Processor processor = null;
		HashMap<String, String> headers=new HashMap<>();
		headers.put("Content-Type", "application/json");
		headers.put("service-line","dash_catalog");
		GameOfThronesService got= new GameOfThronesService("cmsinventoryservice", "clearinventory", gameofthrones);
		processor = new Processor(got,headers,new String[]{"["+key+"]"});
		return processor;

	}
	public Processor clearInventoryNegative(String key) throws Exception {
		Processor processor = null;
		HashMap<String, String> headers=new HashMap<>();
		headers.put("Content-Type", "application/json");
		headers.put("service-line","dash_catalog");
		GameOfThronesService got= new GameOfThronesService("cmsinventoryservice", "clearinventory", gameofthrones);
		processor = new Processor(got,headers,new String[]{"["+key+"]"});
		return processor;

	}

	public Processor getListing(String store_id){
		Processor processor = null;
		HashMap<String, String> headers=new HashMap<>();
		headers.put("Content-Type", "application/json");
		GameOfThronesService got= new GameOfThronesService("cmslistingservice", "getlisting", gameofthrones);
		String[] queryParams = {store_id};
		processor = new Processor(got,headers,null,queryParams);
		return processor;

	}

	public HashMap<String, String> defaultHeaders() {
		HashMap<String, String> headers=new HashMap<>();
		headers.put("Content-Type", "application/json");
		headers.put("updated-by", "Automation-Hema");
		return headers;
	}

	public Processor skuCreationPojo(String SkuPojo) {
		Processor processor = null;
		HashMap<String, String> headers = defaultHeaders();
		GameOfThronesService service = new GameOfThronesService("cmsskuservice", "createSku", gameofthrones);
		System.out.println("service"+service.toString());
		processor = new Processor(service, headers, new String[]{SkuPojo}, null);

		return processor;
	}

	public Processor getSkuByIds(String storeId,String ids,String categoryId) throws Exception {
		HashMap<String, String> requestheaders = defaultHeaders();
		GameOfThronesService service = new GameOfThronesService("cmsskuservice", "getSkuByIds", gameofthrones);
		String[] urlParams={storeId,ids,categoryId};
		Processor processor = new Processor(service, requestheaders, null, urlParams);
		return processor;
	}
	public Processor getSkuByStore(String StoreID) throws Exception {
		HashMap<String, String> requestheaders = defaultHeaders();
		GameOfThronesService service = new GameOfThronesService("cmsskuservice", "getSkuByStore", gameofthrones);
		String[] urlParams= {StoreID};
		Processor processor = new Processor(service, requestheaders, null, urlParams);
		return processor;
	}
	public Processor disableSku(String storeID,String skuIds) throws Exception {
		HashMap<String, String> headers = defaultHeaders();
		GameOfThronesService service = new GameOfThronesService("cmsskuservice", "disableSku", gameofthrones);
		String[] urlParams= {storeID};
		String[] payloadParams = {skuIds};
		Processor processor =null;
		processor = new Processor(service, headers, payloadParams, urlParams);

		return processor;
	}
	public Processor enableSku(String skuIds,String storeID) throws Exception {
		HashMap<String, String> requestheaders = defaultHeaders();
		GameOfThronesService service = new GameOfThronesService("cmsskuservice", "enableSku", gameofthrones);
		String[] urlParams= {storeID};
		String[] payloadParams = {skuIds};
		Processor processor = new Processor(service, requestheaders, payloadParams, urlParams);
		return processor;
	}

	public Processor getSkuById(String skuIds,String storeID) throws Exception {
		HashMap<String, String> requestheaders = defaultHeaders();
		GameOfThronesService service = new GameOfThronesService("cmsskuservice", "getSkuById", gameofthrones);
		String[] urlParams={skuIds, storeID};
		Processor processor = new Processor(service, requestheaders, null, urlParams);
		return processor;
	}


	public Processor getSpinsByCategory(String categoryId){
		Processor processor=null;
		try {
			HashMap<String,String> headers = new HashMap<>();
			headers.put("Content-Type", "application/json");
			GameOfThronesService service = new GameOfThronesService("cmsingestionservice", "getspinsbycategory", gameofthrones);
			String[] urlParams={categoryId};
			processor = new Processor(service,headers,null,urlParams);
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		return processor;
	}

	public boolean isNameChangedByAddingAttribute(String responseAsString,String attributeInRequest) throws Exception{
		boolean doesNameAttributeMatch=false;
		JSONArray array = new JSONArray(responseAsString);

		for( int i =0; i < array.length(); i++){
			JSONObject object = array.getJSONObject(i);
			String nameOfTheAttribute = (String)object.get("name");
			doesNameAttributeMatch = nameOfTheAttribute.equalsIgnoreCase(attributeInRequest);
if(doesNameAttributeMatch){
	break;
}
		}


		return doesNameAttributeMatch;

	}

}
