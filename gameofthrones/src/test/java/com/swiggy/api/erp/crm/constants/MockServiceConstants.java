package com.swiggy.api.erp.crm.constants;

import framework.gameofthrones.JonSnow.DateHelper;

import java.util.HashMap;

public class MockServiceConstants {


    DateHelper dateHelper = new DateHelper();
    int restId=123;
    int itemId=12;

    public String getUniqueOrderId() {

        System.out.println("Generated Order ID:: "+ System.currentTimeMillis());
        return String.valueOf(System.currentTimeMillis());

    }

    public String getUniquerestaurantId() {

        restId++;
        return String.valueOf(restId);

    }

    public String getUniqueItemId() {

        itemId++;
        return String.valueOf(itemId);


    }

    public HashMap<String, String> getOrderDetails(String orderType) {

        if (orderType.equalsIgnoreCase("regular")) {
            HashMap<String, String> regularOrderDetails = new HashMap<>();
            regularOrderDetails.put("restId", getUniquerestaurantId());
            regularOrderDetails.put("itemId", getUniqueItemId());
            regularOrderDetails.put("cost", "1");
            regularOrderDetails.put("orderType", orderType);
            regularOrderDetails.put("orderId", getUniqueOrderId());
            return regularOrderDetails;
        } else if (orderType.equalsIgnoreCase("self-delivery")) {
            HashMap<String, String> dominosOrderDetails = new HashMap<>();
            dominosOrderDetails.put("restId", getUniquerestaurantId());
            dominosOrderDetails.put("itemId", getUniqueItemId());
            dominosOrderDetails.put("cost", "1");
            dominosOrderDetails.put("orderType", orderType);
            dominosOrderDetails.put("orderId", getUniqueOrderId());
            return dominosOrderDetails;
        } else if (orderType.equalsIgnoreCase("pop")) {
            HashMap<String, String> popOrderDetails = new HashMap<>();
            popOrderDetails.put("restId", getUniquerestaurantId());
            popOrderDetails.put("itemId", getUniqueItemId());
            popOrderDetails.put("cost", "1");
            popOrderDetails.put("orderType", orderType);
            popOrderDetails.put("orderId", getUniqueOrderId());
            return popOrderDetails;
        } else{
            HashMap<String, String> cafeOrderDetails = new HashMap<>();
            cafeOrderDetails.put("restId", getUniquerestaurantId());
            cafeOrderDetails.put("itemId", getUniqueItemId());
            cafeOrderDetails.put("cost", "1");
            cafeOrderDetails.put("orderType", orderType);
            cafeOrderDetails.put("orderId", getUniqueOrderId());
            return cafeOrderDetails;
        }
    }

    public HashMap<String, String> getDeliveryDetails(String disposition, String deliveryStatus) {

        String strDate = dateHelper.getCurrentDateTimeIn_yyyy_mm_dd_hh_mm_ss();
        System.out.println(strDate);

        String plus60 = dateHelper.getMinutesPlusOrMinusCurrentDateTime_yyyy_mm_dd_hh_mm_ss(60);
        System.out.println(plus60);

        String plus120 = dateHelper.getMinutesPlusOrMinusCurrentDateTime_yyyy_mm_dd_hh_mm_ss(120);
        System.out.println(plus120);

        HashMap<String, String> assignedOnTrack = new HashMap<>();

        assignedOnTrack.put("deliveryStatus", deliveryStatus);

        if (disposition.equalsIgnoreCase("onTrack")) {
            assignedOnTrack.put("assignedPredTime", plus60);
            assignedOnTrack.put("assignedActualTime", strDate);
            assignedOnTrack.put("confirmedPredTime", plus60);
            assignedOnTrack.put("confirmedActualTime", strDate);
            assignedOnTrack.put("arrivedPredTime", plus60);
            assignedOnTrack.put("arrivedActualTime", strDate);
            assignedOnTrack.put("pickedupPredTime", plus60);
            assignedOnTrack.put("pickedupActualTime", strDate);
            assignedOnTrack.put("reachedPredTime", plus60);
            assignedOnTrack.put("reachedActualTime", strDate);
            assignedOnTrack.put("deliveredPredTime", plus60);
            assignedOnTrack.put("deliveredActualTime", strDate);
        } else if (disposition.equalsIgnoreCase("notOnTrack"))
        {
            assignedOnTrack.put("assignedPredTime", plus60);
            assignedOnTrack.put("assignedActualTime", plus120);
            assignedOnTrack.put("confirmedPredTime", plus60);
            assignedOnTrack.put("confirmedActualTime", plus120);
            assignedOnTrack.put("arrivedPredTime", plus60);
            assignedOnTrack.put("arrivedActualTime", plus120);
            assignedOnTrack.put("pickedupPredTime", plus60);
            assignedOnTrack.put("pickedupActualTime", plus120);
            assignedOnTrack.put("reachedPredTime", plus60);
            assignedOnTrack.put("reachedActualTime", plus120);
            assignedOnTrack.put("deliveredPredTime", plus60);
            assignedOnTrack.put("deliveredActualTime", plus120);
        } else {
            System.out.println("Did not match the expected input");
        }

        return assignedOnTrack;
    }

        public HashMap<String,String[]> getDispositionDetails()
    {
        HashMap<String,String[]> dispositionDetails = new HashMap<>();

        dispositionDetails.put("ChangedMyMindOrderPlacedNotOnTrack",new String[]{"17","13"});
        dispositionDetails.put("PlacedByMistakeOrderPlacedNotOnTrack",new String[]{"21","17"});
        dispositionDetails.put("IncorrectAddressOrderPlacedNotOnTrack",new String[]{"25","21"});
        dispositionDetails.put("OrderDelayedOrderPlacedNotOnTrack",new String[]{"16","12"});
        dispositionDetails.put("ExpectedFasterOrderPlacedOnTrack",new String[]{"14","10"});
        dispositionDetails.put("ChangedMyMindOrderNotPlacedNotOnTrack",new String[]{"19","15"});
        dispositionDetails.put("PlacedByMistakeOrderNotPlacedNotOnTrack",new String[]{"23","19"});
        dispositionDetails.put("IncorrectAddressOrderNotPlacedNotOnTrack",new String[]{"27","23"});
        dispositionDetails.put("OrderDelayedOrderNotPlacedNotOnTrack",new String[]{"15","11"});
        dispositionDetails.put("ExpectedFasterOrderNotPlacedOnTrack",new String[]{"13","8"});
        dispositionDetails.put("ChangedMyMindOrderNotPlacedOnTrack",new String[]{"20","16"});
        dispositionDetails.put("PlacedByMistakeOrderNotPlacedOnTrack",new String[]{"24","20"});
        dispositionDetails.put("IncorrectAddressOrderNotPlacedOnTrack",new String[]{"28","24"});
        dispositionDetails.put("ChangedMyMindOrderPlacedOnTrack",new String[]{"18","14"});
        dispositionDetails.put("PlacedByMistakeOrderPlacedOnTrack",new String[]{"22","18"});
        dispositionDetails.put("IncorrectAddressOrderPlacedOnTrack",new String[]{"26","22"});
        dispositionDetails.put("ChangedMyMindOrderPickedupOnTrack",new String[]{"32","29"});
        dispositionDetails.put("ChangedMyMindOrderPickedupNotOnTrack",new String[]{"33","30"});
        dispositionDetails.put("IncorrectAddressOrderPickedupNotOnTrack",new String[]{"37","34"});
        dispositionDetails.put("IncorrectAddressOrderPickedupOnTrack",new String[]{"36","33"});
        dispositionDetails.put("OrderDelayedOrderPickedupNotOnTrack",new String[]{"31","28"});
        dispositionDetails.put("PlacedByMistakeOrderPickedupNotOnTrack",new String[]{"35","32"});
        dispositionDetails.put("PlacedByMistakeOrderPickedupOnTrack",new String[]{"34","31"});
        dispositionDetails.put("ExpectedFasterOrderPickedupOnTrack",new String[]{"30","27"});
        dispositionDetails.put("TakeawayChangedMyMind", new String[]{"13", "8"});
        dispositionDetails.put("TakeawayUnableToPickup", new String[]{"13", "8"});
        dispositionDetails.put("TakeawayPlacedByMistake", new String[]{"13", "8"});
        dispositionDetails.put("TakeawayOrderDelayed", new String[]{"13", "8"});

        return dispositionDetails;
    }

}
