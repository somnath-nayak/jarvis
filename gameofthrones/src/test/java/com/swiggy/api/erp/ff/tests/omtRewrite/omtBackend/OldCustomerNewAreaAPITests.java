package com.swiggy.api.erp.ff.tests.omtRewrite.omtBackend;

import com.swiggy.api.erp.ff.dp.omtRewrite.OmtBackendData;
import com.swiggy.api.erp.ff.helper.OmtBackendHelper;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class OldCustomerNewAreaAPITests extends OmtBackendData{

    OmtBackendHelper helper = new OmtBackendHelper();

    @Test(priority = 1, dataProvider = "oldCustomerNewAreaData", groups = {"sanity", "regression"},description = "tests for OMS-Backend API to ckeck old customer new area used in verification service")
    public void oldCustomerNewAreaAPITests(String email, String mobile, String area, String scenario, int expectedResponse, int expectedStatusCode, String expectedStatusMessage, boolean expectedIs_old_customer_new_area){
        System.out.println("***************************************** oldCustomerNewAreaAPITests with " + scenario + " started *****************************************");

        Processor response =  helper.oldCustomerNewArea(email, mobile, area);
        SoftAssert softAssertion = new SoftAssert();
        softAssertion.assertEquals(response.ResponseValidator.GetResponseCode(), expectedResponse, "Actual respponse code did not match with the expected response code for <baseurl>:8084/api/v1/verification-fact?"+email+"&mobile="+mobile+"&area="+area);
        softAssertion.assertEquals(response.ResponseValidator.GetNodeValueAsInt("statusCode"), expectedStatusCode, "Actual statusCode did not match with the expected statusCode for <baseurl>:8084/api/v1/verification-fact?"+email+"&mobile="+mobile+"&area="+area);
        softAssertion.assertEquals(response.ResponseValidator.GetNodeValue("statusMessage"), expectedStatusMessage, "Actual statusMessage did not match with the expected statusMessage for <baseurl>:8084/api/v1/verification-fact?"+email+"&mobile="+mobile+"&area="+area);
        if(expectedIs_old_customer_new_area){
            softAssertion.assertTrue(response.ResponseValidator.GetNodeValueAsBool("data.is_old_customer_new_area"), "Actual statusMessage did not match with the expected statusMessage for <baseurl>:8084/api/v1/verification-fact?"+email+"&mobile="+mobile+"&area="+area);
        }else {
            softAssertion.assertFalse(response.ResponseValidator.GetNodeValueAsBool("data.is_old_customer_new_area"), "Actual statusMessage did not match with the expected statusMessage for <baseurl>:8084/api/v1/verification-fact?"+email+"&mobile="+mobile+"&area="+area);
        }
        softAssertion.assertAll();

        System.out.println("######################################### oldCustomerNewAreaAPITests " + scenario + " compleated #########################################");
    }
}
