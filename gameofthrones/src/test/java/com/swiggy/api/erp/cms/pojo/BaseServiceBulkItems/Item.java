package com.swiggy.api.erp.cms.pojo.BaseServiceBulkItems;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.erp.cms.pojo.BaseServiceBulkItems
 **/
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.swiggy.api.erp.cms.helper.BaseServiceHelper;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "active",
        "addon_description",
        "addon_free_limit",
        "addon_limit",
        "addons",
        "category_id",
        "comment",
        "commission",
        "created_at",
        "created_by",
        "created_on",
        "crop_choices",
        "description",
        "eligible_for_long_distance",
        "enabled",
        "gst_details",
        "id",
        "image_id",
        "image_url",
        "in_stock",
        "is_discoverable",
        "is_perishable",
        "is_spicy",
        "is_veg",
        "name",
        "order",
        "packing_charges",
        "packing_slab_count",
        "preparation_style",
        "price",
        "recommended",
        "restaurant_id",
        "s3_image_url",
        "serves_how_many",
        "service_charges",
        "service_tax",
        "sub_category_id",
        "third_party_category_name",
        "third_party_category_order",
        "third_party_id",
        "third_party_sub_category_name",
        "third_party_sub_category_order",
        "type",
        "updated_at",
        "updated_by",
        "updated_on",
        "variant_description",
        "variants",
        "variants_v2",
        "vat"
})
public class Item {

    BaseServiceHelper baseServiceHelper = new BaseServiceHelper();
    private String timeStamp = baseServiceHelper.getTimeStampRandomised();

    @JsonProperty("active")
    private Boolean active;
    @JsonProperty("addon_description")
    private String addon_description;
    @JsonProperty("addon_free_limit")
    private Integer addon_free_limit;
    @JsonProperty("addon_limit")
    private Integer addon_limit;
    @JsonProperty("addons")
    private Addons addons;
    @JsonProperty("category_id")
    private Integer category_id;
    @JsonProperty("comment")
    private String comment;
    @JsonProperty("commission")
    private String commission;
    @JsonProperty("created_at")
    private String created_at;
    @JsonProperty("created_by")
    private String created_by;
    @JsonProperty("created_on")
    private String created_on;
    @JsonProperty("crop_choices")
    private Integer crop_choices;
    @JsonProperty("description")
    private String description;
    @JsonProperty("eligible_for_long_distance")
    private Integer eligible_for_long_distance;
    @JsonProperty("enabled")
    private Integer enabled;
    @JsonProperty("gst_details")
    private Gst_details gst_details;
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("image_id")
    private String image_id;
    @JsonProperty("image_url")
    private String image_url;
    @JsonProperty("in_stock")
    private Integer in_stock;
    @JsonProperty("is_discoverable")
    private Boolean is_discoverable;
    @JsonProperty("is_perishable")
    private Integer is_perishable;
    @JsonProperty("is_spicy")
    private Integer is_spicy;
    @JsonProperty("is_veg")
    private Integer is_veg;
    @JsonProperty("name")
    private String name;
    @JsonProperty("order")
    private Integer order;
    @JsonProperty("packing_charges")
    private Double packing_charges;
    @JsonProperty("packing_slab_count")
    private Integer packing_slab_count;
    @JsonProperty("preparation_style")
    private String preparation_style;
    @JsonProperty("price")
    private Double price;
    @JsonProperty("recommended")
    private Boolean recommended;
    @JsonProperty("restaurant_id")
    private Integer restaurant_id;
    @JsonProperty("s3_image_url")
    private String s3_image_url;
    @JsonProperty("serves_how_many")
    private Integer serves_how_many;
    @JsonProperty("service_charges")
    private String service_charges;
    @JsonProperty("service_tax")
    private String service_tax;
    @JsonProperty("sub_category_id")
    private Integer sub_category_id;
    @JsonProperty("third_party_category_name")
    private String third_party_category_name;
    @JsonProperty("third_party_category_order")
    private Integer third_party_category_order;
    @JsonProperty("third_party_id")
    private String third_party_id;
    @JsonProperty("third_party_sub_category_name")
    private String third_party_sub_category_name;
    @JsonProperty("third_party_sub_category_order")
    private Integer third_party_sub_category_order;
    @JsonProperty("type")
    private String type;
    @JsonProperty("updated_at")
    private String updated_at;
    @JsonProperty("updated_by")
    private String updated_by;
    @JsonProperty("updated_on")
    private String updated_on;
    @JsonProperty("variant_description")
    private String variant_description;
    @JsonProperty("variants")
    private Variants variants;
    @JsonProperty("variants_v2")
    private Variants_v2 variants_v2;
    @JsonProperty("vat")
    private String vat;

    @JsonProperty("active")
    public Boolean getActive() {
        return active;
    }

    @JsonProperty("active")
    public void setActive(Boolean active) {
        this.active = active;
    }

    @JsonProperty("addon_description")
    public String getAddon_description() {
        return addon_description;
    }

    @JsonProperty("addon_description")
    public void setAddon_description(String addon_description) {
        this.addon_description = addon_description;
    }

    @JsonProperty("addon_free_limit")
    public Integer getAddon_free_limit() {
        return addon_free_limit;
    }

    @JsonProperty("addon_free_limit")
    public void setAddon_free_limit(Integer addon_free_limit) {
        this.addon_free_limit = addon_free_limit;
    }

    @JsonProperty("addon_limit")
    public Integer getAddon_limit() {
        return addon_limit;
    }

    @JsonProperty("addon_limit")
    public void setAddon_limit(Integer addon_limit) {
        this.addon_limit = addon_limit;
    }

    @JsonProperty("addons")
    public Addons getAddons() {
        return addons;
    }

    @JsonProperty("addons")
    public void setAddons(Addons addons) {
        this.addons = addons;
    }

    @JsonProperty("category_id")
    public Integer getCategory_id() {
        return category_id;
    }

    @JsonProperty("category_id")
    public void setCategory_id(Integer category_id) {
        this.category_id = category_id;
    }

    @JsonProperty("comment")
    public String getComment() {
        return comment;
    }

    @JsonProperty("comment")
    public void setComment(String comment) {
        this.comment = comment;
    }

    @JsonProperty("commission")
    public String getCommission() {
        return commission;
    }

    @JsonProperty("commission")
    public void setCommission(String commission) {
        this.commission = commission;
    }

    @JsonProperty("created_at")
    public String getCreated_at() {
        return created_at;
    }

    @JsonProperty("created_at")
    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    @JsonProperty("created_by")
    public String getCreated_by() {
        return created_by;
    }

    @JsonProperty("created_by")
    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    @JsonProperty("created_on")
    public String getCreated_on() {
        return created_on;
    }

    @JsonProperty("created_on")
    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }

    @JsonProperty("crop_choices")
    public Integer getCrop_choices() {
        return crop_choices;
    }

    @JsonProperty("crop_choices")
    public void setCrop_choices(Integer crop_choices) {
        this.crop_choices = crop_choices;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("eligible_for_long_distance")
    public Integer getEligible_for_long_distance() {
        return eligible_for_long_distance;
    }

    @JsonProperty("eligible_for_long_distance")
    public void setEligible_for_long_distance(Integer eligible_for_long_distance) {
        this.eligible_for_long_distance = eligible_for_long_distance;
    }

    @JsonProperty("enabled")
    public Integer getEnabled() {
        return enabled;
    }

    @JsonProperty("enabled")
    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }

    @JsonProperty("gst_details")
    public Gst_details getGst_details() {
        return gst_details;
    }

    @JsonProperty("gst_details")
    public void setGst_details(Gst_details gst_details) {
        this.gst_details = gst_details;
    }

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("image_id")
    public String getImage_id() {
        return image_id;
    }

    @JsonProperty("image_id")
    public void setImage_id(String image_id) {
        this.image_id = image_id;
    }

    @JsonProperty("image_url")
    public String getImage_url() {
        return image_url;
    }

    @JsonProperty("image_url")
    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    @JsonProperty("in_stock")
    public Integer getIn_stock() {
        return in_stock;
    }

    @JsonProperty("in_stock")
    public void setIn_stock(Integer in_stock) {
        this.in_stock = in_stock;
    }

    @JsonProperty("is_discoverable")
    public Boolean getIs_discoverable() {
        return is_discoverable;
    }

    @JsonProperty("is_discoverable")
    public void setIs_discoverable(Boolean is_discoverable) {
        this.is_discoverable = is_discoverable;
    }

    @JsonProperty("is_perishable")
    public Integer getIs_perishable() {
        return is_perishable;
    }

    @JsonProperty("is_perishable")
    public void setIs_perishable(Integer is_perishable) {
        this.is_perishable = is_perishable;
    }

    @JsonProperty("is_spicy")
    public Integer getIs_spicy() {
        return is_spicy;
    }

    @JsonProperty("is_spicy")
    public void setIs_spicy(Integer is_spicy) {
        this.is_spicy = is_spicy;
    }

    @JsonProperty("is_veg")
    public Integer getIs_veg() {
        return is_veg;
    }

    @JsonProperty("is_veg")
    public void setIs_veg(Integer is_veg) {
        this.is_veg = is_veg;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("order")
    public Integer getOrder() {
        return order;
    }

    @JsonProperty("order")
    public void setOrder(Integer order) {
        this.order = order;
    }

    @JsonProperty("packing_charges")
    public Double getPacking_charges() {
        return packing_charges;
    }

    @JsonProperty("packing_charges")
    public void setPacking_charges(Double packing_charges) {
        this.packing_charges = packing_charges;
    }

    @JsonProperty("packing_slab_count")
    public Integer getPacking_slab_count() {
        return packing_slab_count;
    }

    @JsonProperty("packing_slab_count")
    public void setPacking_slab_count(Integer packing_slab_count) {
        this.packing_slab_count = packing_slab_count;
    }

    @JsonProperty("preparation_style")
    public String getPreparation_style() {
        return preparation_style;
    }

    @JsonProperty("preparation_style")
    public void setPreparation_style(String preparation_style) {
        this.preparation_style = preparation_style;
    }

    @JsonProperty("price")
    public Double getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(Double price) {
        this.price = price;
    }

    @JsonProperty("recommended")
    public Boolean getRecommended() {
        return recommended;
    }

    @JsonProperty("recommended")
    public void setRecommended(Boolean recommended) {
        this.recommended = recommended;
    }

    @JsonProperty("restaurant_id")
    public Integer getRestaurant_id() {
        return restaurant_id;
    }

    @JsonProperty("restaurant_id")
    public void setRestaurant_id(Integer restaurant_id) {
        this.restaurant_id = restaurant_id;
    }

    @JsonProperty("s3_image_url")
    public String getS3_image_url() {
        return s3_image_url;
    }

    @JsonProperty("s3_image_url")
    public void setS3_image_url(String s3_image_url) {
        this.s3_image_url = s3_image_url;
    }

    @JsonProperty("serves_how_many")
    public Integer getServes_how_many() {
        return serves_how_many;
    }

    @JsonProperty("serves_how_many")
    public void setServes_how_many(Integer serves_how_many) {
        this.serves_how_many = serves_how_many;
    }

    @JsonProperty("service_charges")
    public String getService_charges() {
        return service_charges;
    }

    @JsonProperty("service_charges")
    public void setService_charges(String service_charges) {
        this.service_charges = service_charges;
    }

    @JsonProperty("service_tax")
    public String getService_tax() {
        return service_tax;
    }

    @JsonProperty("service_tax")
    public void setService_tax(String service_tax) {
        this.service_tax = service_tax;
    }

    @JsonProperty("sub_category_id")
    public Integer getSub_category_id() {
        return sub_category_id;
    }

    @JsonProperty("sub_category_id")
    public void setSub_category_id(Integer sub_category_id) {
        this.sub_category_id = sub_category_id;
    }

    @JsonProperty("third_party_category_name")
    public String getThird_party_category_name() {
        return third_party_category_name;
    }

    @JsonProperty("third_party_category_name")
    public void setThird_party_category_name(String third_party_category_name) {
        this.third_party_category_name = third_party_category_name;
    }

    @JsonProperty("third_party_category_order")
    public Integer getThird_party_category_order() {
        return third_party_category_order;
    }

    @JsonProperty("third_party_category_order")
    public void setThird_party_category_order(Integer third_party_category_order) {
        this.third_party_category_order = third_party_category_order;
    }

    @JsonProperty("third_party_id")
    public String getThird_party_id() {
        return third_party_id;
    }

    @JsonProperty("third_party_id")
    public void setThird_party_id(String third_party_id) {
        this.third_party_id = third_party_id;
    }

    @JsonProperty("third_party_sub_category_name")
    public String getThird_party_sub_category_name() {
        return third_party_sub_category_name;
    }

    @JsonProperty("third_party_sub_category_name")
    public void setThird_party_sub_category_name(String third_party_sub_category_name) {
        this.third_party_sub_category_name = third_party_sub_category_name;
    }

    @JsonProperty("third_party_sub_category_order")
    public Integer getThird_party_sub_category_order() {
        return third_party_sub_category_order;
    }

    @JsonProperty("third_party_sub_category_order")
    public void setThird_party_sub_category_order(Integer third_party_sub_category_order) {
        this.third_party_sub_category_order = third_party_sub_category_order;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("updated_at")
    public String getUpdated_at() {
        return updated_at;
    }

    @JsonProperty("updated_at")
    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    @JsonProperty("updated_by")
    public String getUpdated_by() {
        return updated_by;
    }

    @JsonProperty("updated_by")
    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    @JsonProperty("updated_on")
    public String getUpdated_on() {
        return updated_on;
    }

    @JsonProperty("updated_on")
    public void setUpdated_on(String updated_on) {
        this.updated_on = updated_on;
    }

    @JsonProperty("variant_description")
    public String getVariant_description() {
        return variant_description;
    }

    @JsonProperty("variant_description")
    public void setVariant_description(String variant_description) {
        this.variant_description = variant_description;
    }

    @JsonProperty("variants")
    public Variants getVariants() {
        return variants;
    }

    @JsonProperty("variants")
    public void setVariants(Variants variants) {
        this.variants = variants;
    }

    @JsonProperty("variants_v2")
    public Variants_v2 getVariants_v2() {
        return variants_v2;
    }

    @JsonProperty("variants_v2")
    public void setVariants_v2(Variants_v2 variants_v2) {
        this.variants_v2 = variants_v2;
    }

    @JsonProperty("vat")
    public String getVat() {
        return vat;
    }

    @JsonProperty("vat")
    public void setVat(String vat) {
        this.vat = vat;
    }

    public void setDefaultValues(int restID) {
        Gst_details gst_details = new Gst_details();
        int catID = baseServiceHelper.createCategoryIdAndReturnInt(restID);
        int subCatID = baseServiceHelper.createSubCategoryAndReturnInt(restID,catID);
        if(this.getActive() == null)
            this.setActive(true);
        if(this.getIs_discoverable() == null)
            this.setIs_discoverable(true);
        if(this.getAddon_free_limit() == null)
            this.setAddon_free_limit(50);
        if(this.getAddon_limit() == null)
            this.setAddon_limit(50);
        if(this.getCategory_id() == null)
            this.setCategory_id(catID);
        if(this.getCreated_by() == null)
            this.setCreated_by("Automation_BaseService");
        if(this.getIn_stock() == null)
            this.setIn_stock(1);
        if(this.getDescription() == null)
            this.setDescription("Item Testing Suite");
        if(this.getEligible_for_long_distance() == null)
            this.setEligible_for_long_distance(1);
        if(this.getEnabled() == null)
            this.setEnabled(1);
        if(this.getIs_perishable() == null)
            this.setIs_perishable(1);
        if(this.getIs_veg() == null)
            this.setIs_veg(1);
        if(this.getIs_spicy() == null)
            this.setIs_spicy(1);
        if(this.getName() == null)
            this.setName("Automation_BaseService_Suite_"+timeStamp);
        if(this.getOrder() == null)
            this.setOrder(0);
        if(this.getPrice() == null)
            this.setPrice(100.0);
        if(this.getPacking_charges() == null)
            this.setPacking_charges(0.0);
        if(this.getPacking_slab_count() == null)
            this.setPacking_slab_count(0);
        if(this.getPreparation_style() == null)
            this.setPreparation_style("baked");
        if(this.getRecommended() == null)
            this.setRecommended(true);
        if(this.getRestaurant_id() == null)
            this.setRestaurant_id(restID);
        if(this.getServes_how_many() == null)
            this.setServes_how_many(1);
        if(this.getService_charges() == null)
            this.setService_charges("string");
        if(this.getService_tax() == null)
            this.setService_tax("string");
        if(this.getSub_category_id() == null)
            this.setSub_category_id(subCatID);
        if(this.getThird_party_id() == null)
            this.setThird_party_id("Automation_"+timeStamp);
        if(this.getType() == null)
            this.setType("REGULAR_ITEM");
        if(this.getUpdated_at() == null)
            this.setUpdated_at("2018-03-23T07:15:17.697Z");
        if(this.getUpdated_by() == null)
            this.setUpdated_by("Automation_baseService");
        if(this.getUpdated_on() == null)
            this.setUpdated_on("2018-03-23T07:15:17.697Z");
        if(this.getVariant_description() == null)
            this.setVariant_description("string");
        if(this.getVat() == null)
            this.setVat("string");
        if(this.getImage_id() == null)
            this.setImage_id("");
        if(this.getAddon_description() == null)
            this.setAddon_description("Testing Addon");
        this.setGst_details(gst_details.build());
    }

    public Item build(int restID)  {
        setDefaultValues(restID);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("active", active).append("addon_description", addon_description).append("addon_free_limit", addon_free_limit).append("addon_limit", addon_limit).append("addons", addons).append("category_id", category_id).append("comment", comment).append("commission", commission).append("created_at", created_at).append("created_by", created_by).append("created_on", created_on).append("crop_choices", crop_choices).append("description", description).append("eligible_for_long_distance", eligible_for_long_distance).append("enabled", enabled).append("gst_details", gst_details).append("id", id).append("image_id", image_id).append("image_url", image_url).append("in_stock", in_stock).append("is_discoverable", is_discoverable).append("is_perishable", is_perishable).append("is_spicy", is_spicy).append("is_veg", is_veg).append("name", name).append("order", order).append("packing_charges", packing_charges).append("packing_slab_count", packing_slab_count).append("preparation_style", preparation_style).append("price", price).append("recommended", recommended).append("restaurant_id", restaurant_id).append("s3_image_url", s3_image_url).append("serves_how_many", serves_how_many).append("service_charges", service_charges).append("service_tax", service_tax).append("sub_category_id", sub_category_id).append("third_party_category_name", third_party_category_name).append("third_party_category_order", third_party_category_order).append("third_party_id", third_party_id).append("third_party_sub_category_name", third_party_sub_category_name).append("third_party_sub_category_order", third_party_sub_category_order).append("type", type).append("updated_at", updated_at).append("updated_by", updated_by).append("updated_on", updated_on).append("variant_description", variant_description).append("variants", variants).append("variants_v2", variants_v2).append("vat", vat).toString();
    }

}
