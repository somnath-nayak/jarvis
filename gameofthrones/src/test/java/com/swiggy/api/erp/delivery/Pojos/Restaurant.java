package com.swiggy.api.erp.delivery.Pojos;

import com.swiggy.api.erp.delivery.constants.RTSConstants;

public class Restaurant {

	private String city_id;

    private String is_long_distance_enabled;

    private String lat_long;

    private String area_id;

    private String zone_id;

    private String restaurant_id;

    private String restaurant_type;

    public String getCity_id ()
    {
        return city_id;
    }

    public void setCity_id (String city_id)
    {
        this.city_id = city_id;
    }

    public String getIs_long_distance_enabled ()
    {
        return is_long_distance_enabled;
    }

    public void setIs_long_distance_enabled (String is_long_distance_enabled)
    {
        this.is_long_distance_enabled = is_long_distance_enabled;
    }

    public String getLat_long ()
    {
        return lat_long;
    }

    public void setLat_long (String lat_long)
    {
        this.lat_long = lat_long;
    }

    public String getArea_id ()
    {
        return area_id;
    }

    public void setArea_id (String area_id)
    {
        this.area_id = area_id;
    }

    public String getZone_id ()
    {
        return zone_id;
    }

    public void setZone_id (String zone_id)
    {
        this.zone_id = zone_id;
    }

    public String getRestaurant_id ()
    {
        return restaurant_id;
    }

    public void setRestaurant_id (String restaurant_id)
    {
        this.restaurant_id = restaurant_id;
    }

    public String getRestaurant_type ()
    {
        return restaurant_type;
    }

    public void setRestaurant_type (String restaurant_type)
    {
        this.restaurant_type = restaurant_type;
    }
    
    public void defaultresultset()
    {
    	if(this.getArea_id()==null)
    	{
    		this.setArea_id(RTSConstants.default_area_id);
    	}
    	if(this.getCity_id()==null)
    	{
    		this.setCity_id(RTSConstants.default_city_id);
    	}
    	if(this.getIs_long_distance_enabled()==null)
    	{
    		this.setIs_long_distance_enabled(RTSConstants.default_is_long_distance_enabled);
    	}
    	if(this.getLat_long()==null)
    	{
    		this.setLat_long(RTSConstants.default_restaurant_lat_long);
    	}
    	if(this.getRestaurant_id()==null)
    	{
    		this.setRestaurant_id(RTSConstants.default_restaurant_id);
    	}
    	if(this.getRestaurant_type()==null)
    	{
    		this.setRestaurant_type(RTSConstants.default_restaurant_type);
    	}
    	if(this.getZone_id()==null)
    	{
    		this.setZone_id(RTSConstants.default_zone_id);
    	}
    }
   
    
    @Override
    public String toString()
    {
        return "ClassPojo [city_id = "+city_id+", is_long_distance_enabled = "+is_long_distance_enabled+", lat_long = "+lat_long+", area_id = "+area_id+", zone_id = "+zone_id+", restaurant_id = "+restaurant_id+", restaurant_type = "+restaurant_type+"]";
    }
}