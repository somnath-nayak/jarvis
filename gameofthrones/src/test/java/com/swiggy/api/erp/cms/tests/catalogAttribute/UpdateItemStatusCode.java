package com.swiggy.api.erp.cms.tests.catalogAttribute;

import com.swiggy.api.erp.cms.dp.itemDP;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Tyrion.RabbitMQHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;
import org.testng.Assert;
import org.testng.annotations.Test;

public class UpdateItemStatusCode extends itemDP {

    Initialize gameofthrones = new Initialize();
    RabbitMQHelper helper = new RabbitMQHelper();
    CMSHelper cmsHelper = new CMSHelper();
    SqlTemplate sqlTemplateCI = SystemConfigProvider.getTemplate("cms");
    String itemName;

    @Test(dataProvider = "updateitm",description = "Update item API")
    public void updateItem(String comment, String description, String name, String itemId) {
        int responsecode=cmsHelper.updateItem(comment, description, name, itemId).ResponseValidator.GetResponseCode();
        Assert.assertEquals(200,responsecode,"Status code is not 200");
    }
}