package com.swiggy.api.erp.cms.dp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.swiggy.api.erp.cms.tests.SelfServeV1CategoryTest;
import com.swiggy.api.erp.cms.tests.SelfServeV1ItemApproveWithEdit;
import com.swiggy.api.erp.cms.tests.SelfServeV1ItemReject;
import org.testng.annotations.DataProvider;

import com.swiggy.api.erp.cms.constants.MenuConstants;
import com.swiggy.api.erp.cms.constants.SelfServeConstants;
import com.swiggy.api.erp.cms.helper.SelfServeHelper;
import com.swiggy.api.erp.cms.pojo.SelfServeV1Item.Item;
import com.swiggy.api.erp.cms.pojo.SelfServeV1Item.Item_slot;
import com.swiggy.api.erp.cms.pojo.SelfServeV1Item.Item_vo;
import com.swiggy.api.erp.cms.pojo.SelfServeV1Item.Metadata;
import com.swiggy.api.erp.cms.pojo.SelfServeV1Item.SelfServeV1ItemsPojo;
import com.swiggy.api.erp.cms.pojo.SelfServeV1Item.SubmitTicket;
import com.swiggy.api.erp.cms.pojo.SelfServeV1Item.Tickets;
import com.swiggy.api.erp.cms.tests.SelfServeV1ItemTest;

import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.jayway.jsonpath.JsonPath;
/**
 * Created by HemaGovinadaraj on 14/2/18.
 */
public class SelfServeV1Dp {
	SelfServeHelper sshelper= new SelfServeHelper();
	String[] restId=SelfServeConstants.restId;
	String catId=SelfServeConstants.catId;
	String subCatId=SelfServeConstants.subCatId;
	String[] itemId=SelfServeHelper.getItemId(restId[0]);

	@DataProvider(name = "updateCategory")
	public Object[][] updatecategory() throws Exception {
		return new Object[][] {
				//restId,SubcatId,type,SubcatName,status)
				{restId[0],subCatId,SelfServeConstants.type[1],SelfServeConstants.subCatUpdateName,null,true},
				//SubcatId type passed as cat
				{restId[0],subCatId,SelfServeConstants.type[0],SelfServeConstants.subCatUpdateName,SelfServeConstants.errormessage[0],false},
				//missmatch with SubcatId and RestId
				{restId[1],subCatId,SelfServeConstants.type[1],SelfServeConstants.subCatUpdateName,SelfServeConstants.catNotFound,false},
				//restId,catId,type,catName,status)
				{restId[0],catId,SelfServeConstants.type[0],SelfServeConstants.catUpdateName,null,true},
				//catid type passed as subcat
				{restId[0],catId,SelfServeConstants.type[1],SelfServeConstants.catUpdateName,SelfServeConstants.errormessage[0],false},
				//missmatch with catId and RestId
				{restId[1],catId,SelfServeConstants.type[1],SelfServeConstants.catUpdateName,SelfServeConstants.catNotFound,false},

		};

	}

	@DataProvider(name = "getCategoryTicketByTicketID")
	public Object[][] getCategoryTcketsByid() throws Exception {

		return new Object[][] {
				//ticket id updated in testclass by response of categoryupdate ticket
				{"21",SelfServeConstants.catUpdateName,true},
				{"0",SelfServeConstants.catTicketNoFuond+"0",false},
				{"19",SelfServeConstants.typeMissmatch,false},
		};


	}


	@DataProvider(name = "deleteCategory")
	public Object[][] deleteCategory() throws Exception {

		return new Object[][] {

				{restId[0],SelfServeHelper.getSubCatId(restId[0]),null},
				{restId[0],SelfServeHelper.getCatId(restId[0]),null},
				{"0",SelfServeHelper.getCatId(restId[0]),SelfServeConstants.catNotFound},
		};
	}


	// item level creat  Test Data
	@DataProvider(name = "createItem")
	public Iterator<Object[]> createItem() throws IOException, InterruptedException {
		String itemName;
		List<Object[]> obj = new ArrayList<>();
		JsonHelper jsonHelper = new JsonHelper();
		SelfServeHelper sshelper=new  SelfServeHelper();
		SelfServeV1ItemsPojo  selfservepojo= sshelper.createDefaultSelfServePojo();
		Item_vo item_vo=selfservepojo.getItem_vo();
		Item item=new Item();

		//item with new category and subcategory
		item_vo=new Item_vo();
		item_vo.build();
		item.build();
		item.setCategory_id(null);
		item.setSub_category_id(null);
		item_vo.setMain_category_name(SelfServeConstants.catUpdateName);
		item_vo.setSub_category_name(SelfServeConstants.subCatUpdateName);
		item_vo.setItem(item);
		selfservepojo.setItem_vo(item_vo);
		itemName=item.getName();
		obj.add(new Object[]{jsonHelper.getObjectToJSON(selfservepojo),restId[0], itemName,200,true});

		//item with only category no subcategory
		item_vo=new Item_vo();
		item_vo.build();
		item.build();
		item.setName(item.getName()+1);
		item.setCategory_id(null);
		item.setSub_category_id(null);
		item_vo.setMain_category_name(SelfServeConstants.catUpdateName);
		item_vo.setItem(item);
		selfservepojo.setItem_vo(item_vo);
		itemName=item.getName();
		obj.add(new Object[]{jsonHelper.getObjectToJSON(selfservepojo),restId[0], itemName,200,true});

		//item with only subcategory-->throw errro while approval of ticket
		item_vo=new Item_vo();
		item_vo.build();
		item.build();
		item.setName(item.getName()+2);
		item.setCategory_id(null);
		item.setSub_category_id(null);
		item_vo.setSub_category_name(SelfServeConstants.subCatUpdateName);
		item_vo.setItem(item);
		selfservepojo.setItem_vo(item_vo);
		itemName=item.getName();
		obj.add(new Object[]{jsonHelper.getObjectToJSON(selfservepojo),restId[0], itemName,200,true});

		//item add for existing category but new subcategory
		item_vo=new Item_vo();
		item_vo.build();
		item.build();
		item.setName(item.getName()+3);
		item.setSub_category_id(null);
		item_vo.setSub_category_name(SelfServeConstants.subCatUpdateName);
		item_vo.setItem(item);
		selfservepojo.setItem_vo(item_vo);
		itemName=item.getName();
		obj.add(new Object[]{jsonHelper.getObjectToJSON(selfservepojo),restId[0], itemName,200,true});


		//item without categoryid and subcategory id ,cat subcat name==>invalid
		item_vo=new Item_vo();
		item_vo.build();
		item.build();
		item.setName(item.getName()+4);
		item.setCategory_id(null);
		item.setSub_category_id(null);
		item_vo.setSub_category_name(null);
		item_vo.setMain_category_name(null);
		item_vo.setMain_category_id(null);
		item_vo.setSub_category_id(null);
		item_vo.setItem(item);
		itemName=item.getName();
		obj.add(new Object[]{jsonHelper.getObjectToJSON(selfservepojo),restId[0], itemName,200,true});

		//item price negative
		item_vo=new Item_vo();
		item_vo.build();
		item.build();
		item.setName(item.getName()+5);
		item.setPrice(MenuConstants.zero_price);
		item_vo.setItem(item);
		itemName=item.getName();
		obj.add(new Object[]{jsonHelper.getObjectToJSON(selfservepojo),restId[0], itemName,200,true});

		//item is veg noveg
		item_vo=new Item_vo();
		item_vo.build();
		item.build();
		item.setName(item.getName()+6);
		item.setIs_veg(SelfServeConstants.isVeg[1]);
		item.setPrice(MenuConstants.zero_price);
		item_vo.setItem(item);
		itemName=item.getName();
		obj.add(new Object[]{jsonHelper.getObjectToJSON(selfservepojo),restId[0], itemName,200,true});

		//item is veg egg
		item_vo=new Item_vo();
		item_vo.build();
		item.build();
		item.setName(item.getName()+7);
		item.setIs_veg(SelfServeConstants.isVeg[2]);
		item.setPrice(MenuConstants.zero_price);
		item_vo.setItem(item);
		itemName=item.getName();
		obj.add(new Object[]{jsonHelper.getObjectToJSON(selfservepojo),restId[0], itemName,200,true});


		//item create  description
		item_vo=new Item_vo();
		item_vo.build();
		item.build();
		item.setDescription("Automation item");
		item_vo.setItem(item);
		item.setName(item.getName()+8);
		selfservepojo.setItem_vo(item_vo);
		itemName=item.getName();
		SelfServeHelper.getdiffCatIdforItem(item_vo.getMain_category_id());
		obj.add(new Object[]{jsonHelper.getObjectToJSON(selfservepojo),restId[0], itemName,200,true});

		//item create with image
		item_vo=new Item_vo();
		item_vo.build();
		item.build();
		item.setName(item.getName()+9);
		item.setImage_url(SelfServeConstants.items_image_url);
		item_vo.setItem(item);
		selfservepojo.setItem_vo(item_vo);
		itemName=item.getName();
		SelfServeHelper.getdiffCatIdforItem(item_vo.getMain_category_id());
		obj.add(new Object[]{jsonHelper.getObjectToJSON(selfservepojo),restId[0], itemName,	200,true});

		//item add for existing subcategory without passing category id
		item_vo=new Item_vo();
		item_vo.build();
		item.build();
		item.setName(item.getName()+10);
		item.setCategory_id(null);
		item_vo.setItem(item);
		selfservepojo.setItem_vo(item_vo);
		itemName=item.getName();
		obj.add(new Object[]{jsonHelper.getObjectToJSON(selfservepojo),restId[0], itemName,200,true});

		//mismatch with category id and subcategory
		item_vo=new Item_vo();
		item_vo.build();
		item.build();
		item.setName(item.getName()+11);
		item.setCategory_id(SelfServeHelper.getCatOrSubCatIdHelper(restId[0],"Cat"));
		String categoryIdOfDiffResto=SelfServeHelper.getCatOrSubCatIdHelper(restId[1],"Cat");
		item.setSub_category_id(SelfServeHelper.getCatOrSubCatIdHelper(categoryIdOfDiffResto,"SubCat"));
		item_vo.setItem(item);
		itemName=item.getName();
		obj.add(new Object[]{jsonHelper.getObjectToJSON(selfservepojo),restId[0], itemName,200,true});

		//item with instock 0
		item_vo=new Item_vo();
		item_vo.build();
		item.build();
		item.setName(item.getName()+12);
		item.setIn_stock(0);
		item_vo.setItem(item);
		itemName=item.getName();
		obj.add(new Object[]{jsonHelper.getObjectToJSON(selfservepojo),restId[0], itemName,200,true});

		//item with instock 0
		item_vo=new Item_vo();
		item_vo.build();
		item.build();
		item.setName(item.getName()+13);
		item.setIs_veg(0);
		item_vo.setItem(item);
		itemName=item.getName();
		obj.add(new Object[]{jsonHelper.getObjectToJSON(selfservepojo),restId[0], itemName,200,true});

		//add item to existing category and existing subcategory
		item_vo=new Item_vo();
		item_vo.build();
		item.build();
		item.setName(item.getName()+14);
		item_vo.setItem(item);
		itemName=item.getName();
		obj.add(new Object[]{jsonHelper.getObjectToJSON(selfservepojo),restId[0], itemName,200,true});


		//valid item slots
		Item_slot itemSlot=new Item_slot();
		itemSlot.build();
		item.setName(item.getName()+15);
		item_vo.setItem(item);
		item_vo.setItem_slot(new Item_slot[] {itemSlot});
		selfservepojo.setItem_vo(item_vo);
		itemName=item.getName();
		obj.add(new Object[]{jsonHelper.getObjectToJSON(selfservepojo),restId[0], itemName,200,true});

		// opentime<closstime
		itemSlot=new Item_slot();
		itemSlot.build();
		item.setName(item.getName()+16);
		itemSlot.setOpen_time(MenuConstants.open_close[0]);
		itemSlot.setClose_time(MenuConstants.open_close[1]);
		item_vo.setItem(item);
		item_vo.setItem_slot(new Item_slot[] {itemSlot});
		selfservepojo.setItem_vo(item_vo);
		itemName=item.getName();
		obj.add(new Object[]{jsonHelper.getObjectToJSON(selfservepojo),restId[0], itemName,200,true});

		// open time> close time
		itemSlot=new Item_slot();
		itemSlot.build();
		item.setName(item.getName()+17);
		itemSlot.setOpen_time(MenuConstants.open_close[2]);
		itemSlot.setClose_time(MenuConstants.open_close[0]);
		item_vo.setItem(item);
		item_vo.setItem_slot(new Item_slot[] {itemSlot});
		selfservepojo.setItem_vo(item_vo);
		itemName=item.getName();
		obj.add(new Object[]{jsonHelper.getObjectToJSON(selfservepojo),restId[0], itemName,200,false});

		// open time =close time
		itemSlot=new Item_slot();
		itemSlot.build();
		item.setName(item.getName()+18);
		itemSlot.setOpen_time(MenuConstants.open_close[2]);
		itemSlot.setClose_time(MenuConstants.open_close[2]);
		item_vo.setItem(item);
		item_vo.setItem_slot(new Item_slot[] {itemSlot});
		selfservepojo.setItem_vo(item_vo);
		itemName=item.getName();
		obj.add(new Object[]{jsonHelper.getObjectToJSON(selfservepojo),restId[0], itemName,200,false});

		// invalid day of a week
		itemSlot=new Item_slot();
		itemSlot.build();
		item.setName(item.getName()+19);
		itemSlot.setDay_of_week("abc");
		item_vo.setItem(item);
		item_vo.setItem_slot(new Item_slot[] {itemSlot});
		selfservepojo.setItem_vo(item_vo);
		itemName=item.getName();
		obj.add(new Object[]{jsonHelper.getObjectToJSON(selfservepojo),restId[0],null,400,false});

		return obj.iterator();
	}

	@DataProvider(name = "createItemForCategoryticket")
	public Iterator<Object[]> createItemForCategoryticket() throws IOException, InterruptedException {
		String itemName;
		List<Object[]> obj = new ArrayList<>();
		JsonHelper jsonHelper = new JsonHelper();
		SelfServeHelper sshelper=new  SelfServeHelper();
		SelfServeV1ItemsPojo  selfservepojo= sshelper.createDefaultSelfServePojo();
		Item_vo item_vo=selfservepojo.getItem_vo();
		Item item=new Item();
		item.build();
		item_vo.setItem(item);
		itemName=item.getName();
		obj.add(new Object[]{jsonHelper.getObjectToJSON(selfservepojo),restId[0], itemName});

		return obj.iterator();
	}


	// item level Test Data
	@DataProvider(name = "updateItem")
	public Iterator<Object[]> updateItem() throws IOException, InterruptedException {
		String itemName;
		List<Object[]> obj = new ArrayList<>();
		JsonHelper jsonHelper = new JsonHelper();
		SelfServeHelper sshelper=new  SelfServeHelper();
		SelfServeV1ItemsPojo  selfservepojo= sshelper.createDefaultSelfServePojo();
		Item_vo item_vo=selfservepojo.getItem_vo();
		Item item=new Item();

		//item with new category and subcategory
		item_vo=new Item_vo();
		item_vo.build(itemId[1]);
		item.build(itemId[1]);
		item.setId(itemId[1]);
		item.setCategory_id(null);
		item.setSub_category_id(null);
		item_vo.setMain_category_name(SelfServeConstants.catUpdateName);
		item_vo.setSub_category_name(SelfServeConstants.subCatUpdateName);
		item_vo.setItem(item);
		selfservepojo.setItem_vo(item_vo);
		itemName=item.getName();
		obj.add(new Object[]{jsonHelper.getObjectToJSON(selfservepojo),restId[0], itemName,itemId[1],200,true});

		//item with only category nota subcategory
		item_vo=new Item_vo();
		item_vo.build(itemId[2]);
		item.build(itemId[2]);
		item.setId(itemId[2]);
		item.setCategory_id(null);
		item.setSub_category_id(null);
		item_vo.setMain_category_name(SelfServeConstants.catUpdateName+1);
		item_vo.setSub_category_name(SelfServeConstants.notaSubCat);
		item_vo.setItem(item);
		selfservepojo.setItem_vo(item_vo);
		itemName=item.getName();
		obj.add(new Object[]{jsonHelper.getObjectToJSON(selfservepojo),restId[0], itemName,itemId[2],200,true});

		//item with only newcategory no subcategory
		item_vo=new Item_vo();
		item_vo.build(itemId[3]);
		item.build(itemId[3]);
		item.setId(itemId[3]);
		item.setCategory_id(null);
		item.setSub_category_id(null);
		item_vo.setMain_category_name(SelfServeConstants.catUpdateName+2);
		item_vo.setItem(item);
		selfservepojo.setItem_vo(item_vo);
		itemName=item.getName();
		obj.add(new Object[]{jsonHelper.getObjectToJSON(selfservepojo),restId[0], itemName,itemId[3],400,false});

		//item with only subcategory
		item_vo=new Item_vo();
		item_vo.build(itemId[4]);
		item.build(itemId[4]);
		item.setId(itemId[4]);
		item.setCategory_id(null);
		item.setSub_category_id(null);
		item_vo.setSub_category_name(SelfServeConstants.subCatUpdateName+2);
		item_vo.setItem(item);
		selfservepojo.setItem_vo(item_vo);
		itemName=item.getName();
		obj.add(new Object[]{jsonHelper.getObjectToJSON(selfservepojo),restId[0], itemName,itemId[4],400,false});


		//item update for existing subcat but new cat invalid scenario
		item_vo=new Item_vo();
		item_vo.build(itemId[5]);
		item.build(itemId[5]);
		item.setId(itemId[5]);
		item_vo.setSub_category_name(SelfServeConstants.subCatUpdateName+2);
		item_vo.setItem(item);
		selfservepojo.setItem_vo(item_vo);
		itemName=item.getName();
		obj.add(new Object[]{jsonHelper.getObjectToJSON(selfservepojo),restId[0], itemName,itemId[5],200,true});

		//item price negative
		item_vo=new Item_vo();
		item_vo.build(itemId[6]);
		item.build(itemId[6]);
		item.setId(itemId[6]);
		item_vo.setItem(item);
		itemName=item.getName();
		obj.add(new Object[]{jsonHelper.getObjectToJSON(selfservepojo),restId[0], itemName,itemId[6],200,true});

		//item is_veg=1
		item_vo=new Item_vo();
		item_vo.build(itemId[7]);
		item_vo.build(itemId[7]);
		item.build(itemId[7]);
		item.setIs_veg(SelfServeConstants.isVeg[1]);
		item.setId(itemId[7]);
		item_vo.setItem(item);
		itemName=item.getName();
		obj.add(new Object[]{jsonHelper.getObjectToJSON(selfservepojo),restId[0], itemName,itemId[7],200,true});

		//item is_veg=2
		item_vo=new Item_vo();
		item_vo.build(itemId[8]);
		item.build(itemId[8]);
		item.setIs_veg(SelfServeConstants.isVeg[2]);
		item.setId(itemId[8]);
		item_vo.setItem(item);
		itemName=item.getName();
		obj.add(new Object[]{jsonHelper.getObjectToJSON(selfservepojo),restId[0], itemName,itemId[8],200,true});


		//item update  same category but different category
		item_vo=new Item_vo();
		item_vo.build(itemId[9]);
		item.build(itemId[9]);
		item_vo.setItem(item);
		selfservepojo.setItem_vo(item_vo);
		itemName=item.getName();
		SelfServeHelper.getdiffCatIdforItem(item_vo.getMain_category_id());
		obj.add(new Object[]{jsonHelper.getObjectToJSON(selfservepojo),restId[0], itemName,itemId[9],200,true});

		//item update  description
		item_vo=new Item_vo();
		item_vo.build(itemId[10]);
		item.build(itemId[10]);
		item.setDescription("Automation item");
		item_vo.setItem(item);
		selfservepojo.setItem_vo(item_vo);
		itemName=item.getName();
		SelfServeHelper.getdiffCatIdforItem(item_vo.getMain_category_id());
		obj.add(new Object[]{jsonHelper.getObjectToJSON(selfservepojo),restId[0], itemName,itemId[10],200,true});

		//item update  description
		item_vo=new Item_vo();
		item_vo.build(itemId[11]);
		item.build(itemId[11]);
		item.setDescription("Automation item");
		item.setImage_url(SelfServeConstants.items_image_url);
		item_vo.setItem(item);
		selfservepojo.setItem_vo(item_vo);
		itemName=item.getName();
		SelfServeHelper.getdiffCatIdforItem(item_vo.getMain_category_id());
		obj.add(new Object[]{jsonHelper.getObjectToJSON(selfservepojo),restId[0], itemName,itemId[11],200,true});

		//mismatch with category id and subcategory
		item_vo=new Item_vo();
		item_vo.build(itemId[12]);
		item.build(itemId[12]);
		item_vo.setMain_category_id(SelfServeHelper.getCatOrSubCatIdHelper(restId[0],"Cat"));
		String categoryIdOfDiffResto=SelfServeHelper.getCatOrSubCatIdHelper(restId[1],"Cat");
		item_vo.setSub_category_id(SelfServeHelper.getCatOrSubCatIdHelper(categoryIdOfDiffResto,"SubCat"));
		item_vo.setItem(item);
		itemName=item.getName();
		obj.add(new Object[]{jsonHelper.getObjectToJSON(selfservepojo),restId[0], itemName,itemId[12],200,true});

		//valid item slots
		item_vo.build(itemId[13]);
		Item_slot itemSlot=new Item_slot();
		itemSlot.build();
		item_vo.setItem(item);
		item_vo.setItem_slot(new Item_slot[] {itemSlot});
		selfservepojo.setItem_vo(item_vo);
		itemName=item.getName();
		obj.add(new Object[]{jsonHelper.getObjectToJSON(selfservepojo),restId[0], itemName,itemId[13],200,true});

		// open time> close time
		item_vo.build(itemId[14]);
		itemSlot=new Item_slot();
		itemSlot.build();
		itemSlot.setOpen_time(MenuConstants.open_close[2]);
		itemSlot.setClose_time(MenuConstants.open_close[0]);
		item_vo.setItem(item);
		item_vo.setItem_slot(new Item_slot[] {itemSlot});
		selfservepojo.setItem_vo(item_vo);
		itemName=item.getName();
		obj.add(new Object[]{jsonHelper.getObjectToJSON(selfservepojo),restId[0], itemName,itemId[14],200,true});

		// open time =close time
		item_vo.build(itemId[15]);
		itemSlot=new Item_slot();
		itemSlot.build();
		itemSlot.setOpen_time(MenuConstants.open_close[2]);
		itemSlot.setClose_time(MenuConstants.open_close[2]);
		item_vo.setItem(item);
		item_vo.setItem_slot(new Item_slot[] {itemSlot});
		selfservepojo.setItem_vo(item_vo);
		itemName=item.getName();
		obj.add(new Object[]{jsonHelper.getObjectToJSON(selfservepojo),restId[0], itemName,itemId[15],200,true});

		// invalid day of a week
		item_vo.build(itemId[15]);
		itemSlot=new Item_slot();
		itemSlot.build();
		itemSlot.setDay_of_week("8");
		item_vo.setItem(item);
		item_vo.setItem_slot(new Item_slot[] {itemSlot});
		selfservepojo.setItem_vo(item_vo);
		itemName=item.getName();
		obj.add(new Object[]{jsonHelper.getObjectToJSON(selfservepojo),restId[0],null,itemId[15],400,false});


		//item update for existing same category but new subcategory
		item_vo=new Item_vo();
		item_vo.build(itemId[16]);
		item_vo.setSub_category_id(null);
		item_vo.setSub_category_name("newsubcat");
		item.build(itemId[16]);
		item_vo.setItem(item);
		selfservepojo.setItem_vo(item_vo);
		itemName=item.getName();
		obj.add(new Object[]{jsonHelper.getObjectToJSON(selfservepojo),restId[0], itemName,itemId[16],200,true});


		return obj.iterator();
	}


	@DataProvider(name = "submitticket")
	public Iterator<Object[]> submitTicket() throws IOException {

		List<Object[]> obj = new ArrayList<>();
		SubmitTicket st=new  SubmitTicket();
		st.build();
		Metadata md=new Metadata();
		md.build();
		JsonHelper jsonHelper = new JsonHelper();
		List<String> createdId=SelfServeV1ItemTest.createticketId;
		//createdId.add("1");
		List<Tickets> ticks = new ArrayList<Tickets>();
		int j =0;
		if(createdId.size()>0)
		{
			for (int i=0  ;i<createdId.size();i++)
			{
				Tickets t=new Tickets();
				t.setId(createdId.get(i));
				ticks.add(t);
			}
			st.setTickets(ticks.toArray(new Tickets[ticks.size()]));
			st.setMetadata(md);
			obj.add(new Object[]{jsonHelper.getObjectToJSON(st),restId[0],"ITEM_ADDITION",200});
		}
		List<String> updatedId=SelfServeV1ItemTest.updateticketId;
		ticks = new ArrayList<Tickets>();
		j =0;
		if(updatedId.size()>0)
		{
			for (int i=0  ;i<updatedId.size();i++)
			{
				Tickets t=new Tickets();
				t.setId(updatedId.get(i));
				ticks.add(t);
			}
			st.setTickets(ticks.toArray(new Tickets[ticks.size()]));
			st.setMetadata(md);
			obj.add(new Object[]{jsonHelper.getObjectToJSON(st),restId[0],"ITEM_EDIT",200});
		}
		return  obj.iterator();
	}

	@DataProvider(name = "submitTicketforApproveWithEdit")
	public Iterator<Object[]> submitTicketforApproveWithEdit() throws IOException {

		List<Object[]> obj = new ArrayList<>();
		SubmitTicket st=new  SubmitTicket();
		st.build();
		Metadata md=new Metadata();
		md.build();
		JsonHelper jsonHelper = new JsonHelper();
		List<String> createdId= SelfServeV1ItemApproveWithEdit.createticketId;
		//createdId.add("1");
		List<Tickets> ticks = new ArrayList<Tickets>();
		int j =0;
		if(createdId.size()>0)
		{
			for (int i=0  ;i<createdId.size();i++)
			{
				Tickets t=new Tickets();
				t.setId(createdId.get(i));
				ticks.add(t);
			}
			st.setTickets(ticks.toArray(new Tickets[ticks.size()]));
			st.setMetadata(md);
			obj.add(new Object[]{jsonHelper.getObjectToJSON(st),restId[0],"ITEM_ADDITION",200});
		}
		List<String> updatedId=SelfServeV1ItemApproveWithEdit.updateticketId;
		ticks = new ArrayList<Tickets>();
		j =0;
		if(updatedId.size()>0)
		{
			for (int i=0  ;i<updatedId.size();i++)
			{
				Tickets t=new Tickets();
				t.setId(updatedId.get(i));
				ticks.add(t);
			}
			st.setTickets(ticks.toArray(new Tickets[ticks.size()]));
			st.setMetadata(md);
			obj.add(new Object[]{jsonHelper.getObjectToJSON(st),restId[0],"ITEM_EDIT",200});
		}
		return  obj.iterator();
	}

	@DataProvider(name = "submitTicketforReject")
	public Iterator<Object[]> submitTicketforReject() throws IOException {

		List<Object[]> obj = new ArrayList<>();
		SubmitTicket st=new  SubmitTicket();
		st.build();
		Metadata md=new Metadata();
		md.build();
		JsonHelper jsonHelper = new JsonHelper();
		List<String> createdId= SelfServeV1ItemReject.createticketId;
		//createdId.add("1");
		List<Tickets> ticks = new ArrayList<Tickets>();
		int j =0;
		if(createdId.size()>0)
		{
			for (int i=0  ;i<createdId.size();i++)
			{
				Tickets t=new Tickets();
				t.setId(createdId.get(i));
				ticks.add(t);
			}
			st.setTickets(ticks.toArray(new Tickets[ticks.size()]));
			st.setMetadata(md);
			obj.add(new Object[]{jsonHelper.getObjectToJSON(st),restId[0],"ITEM_ADDITION",200});
		}
		List<String> updatedId=SelfServeV1ItemReject.updateticketId;
		ticks = new ArrayList<Tickets>();
		j =0;
		if(updatedId.size()>0)
		{
			for (int i=0  ;i<updatedId.size();i++)
			{
				Tickets t=new Tickets();
				t.setId(updatedId.get(i));
				ticks.add(t);
			}
			st.setTickets(ticks.toArray(new Tickets[ticks.size()]));
			st.setMetadata(md);
			obj.add(new Object[]{jsonHelper.getObjectToJSON(st),restId[0],"ITEM_EDIT",200});
		}
		return  obj.iterator();
	}

	//approveTicket

	@DataProvider(name = "approveTicket")
	public Iterator<Object[]> approveTicket() throws Exception{
		//	Thread.sleep(5000);
		int count=5;
		List<String> createdId=SelfServeV1ItemTest.createticketId,ticketslistnotassigned=SelfServeV1ItemTest.ticketslistnotassigned;
		//createdId.add("1");
		List<Processor> plist = new ArrayList<Processor>();
		List<Object[]> obj = new ArrayList<>();
		List<Boolean> expectedstatuscreate=SelfServeV1ItemTest.expectedstatuscreate,expectedstatusupdate=SelfServeV1ItemTest.expectedstatusupdate;
		ObjectMapper mapper = new ObjectMapper();
		String agentid=null,ticketresponse=null,getticketState,ticketState="APPROVED",restid,tickettype="SELFSERVE_V1",ticket_reason,assigned="ASSIGNED";
		for(int i=0;i<createdId.size();i++)
		{
			plist.add(sshelper.getticketdetailsbyTicketId(createdId.get(i)));
			ticketresponse=plist.get(i).ResponseValidator.GetBodyAsText();
			getticketState=JsonPath.read(ticketresponse, "$.data.state").toString();
			restid=JsonPath.read(ticketresponse,"$.data.parent_resc_id").toString();
			ticket_reason=JsonPath.read(ticketresponse,"$.data.ticket_reason").toString();
			if(getticketState.equals(assigned))
			{
				agentid=JsonPath.read(ticketresponse,"$.data.assigned_to").toString();
				Processor p1=sshelper.pollingforAgentAssignemett(count,agentid,tickettype);
				if(p1!=null) {
					if (ticket_reason == "ITEM_ADDITION") ;
					{
						JsonNode node = mapper.readTree(p1.ResponseValidator.GetBodyAsText());
						node = node.get("search_data").get(0).get("item_tickets").get(i);
						System.out.println("payload is" + node);
						if (expectedstatuscreate.get(i) == true)
							obj.add(new Object[]{node.toString(), createdId.get(i), ticketState, agentid, 1, 200});
						else
							obj.add(new Object[]{node.toString(), ticketState, createdId.get(i), agentid, 0, 200});
					}
				}
			}
			else
				ticketslistnotassigned.add(createdId.get(i));

		}

		return  obj.iterator();
	}

	@DataProvider(name = "approveTicketUpdate")
	public Iterator<Object[]> approveTicketUpdate() throws Exception {
		List<String> updateticketId=SelfServeV1ItemTest.updateticketId,ticketslistnotassigned=SelfServeV1ItemTest.ticketslistnotassigned;
		int count=5;
		List<Processor> plist = new ArrayList<Processor>();
		List<Object[]> obj = new ArrayList<>();
		List<Boolean> expectedstatuscreate=SelfServeV1ItemTest.expectedstatuscreate,expectedstatusupdate=SelfServeV1ItemTest.expectedstatusupdate;
		ObjectMapper mapper = new ObjectMapper();
		String agentid=null,ticketresponse=null,getticketState,ticketState="APPROVED",restid,tickettype="SELFSERVE_V1",ticket_reason,assigned="ASSIGNED";
		for(int i=0;i<updateticketId.size();i++)
		{
			plist.add(sshelper.getticketdetailsbyTicketId(updateticketId.get(i)));
			ticketresponse=plist.get(i).ResponseValidator.GetBodyAsText();
			getticketState=JsonPath.read(ticketresponse, "$.data.state").toString();
			restid=JsonPath.read(ticketresponse,"$.data.parent_resc_id").toString();
			ticket_reason=JsonPath.read(ticketresponse,"$.data.ticket_reason").toString();
			if(getticketState.equals(assigned))
			{
				agentid=JsonPath.read(ticketresponse,"$.data.assigned_to").toString();
				Processor p1=sshelper.pollingforAgentAssignemett(count,agentid,tickettype);
				if(p1!=null) {
					if (ticket_reason == "ITEM_ADDITION") ;
					{
						JsonNode node = mapper.readTree(p1.ResponseValidator.GetBodyAsText());
						node = node.get("search_data").get(0).get("item_tickets").get(i);
						((ObjectNode) node).put("reason", "automation reason");
						((ObjectNode) node).put("reason_category", "automation reason");
						System.out.println("payload is" + node);
						if (expectedstatuscreate.get(i) == true)
							obj.add(new Object[]{node.toString(), updateticketId.get(i), ticketState, agentid, 1, 200});
						else
							obj.add(new Object[]{node.toString(), ticketState, updateticketId.get(i), agentid, 0, 200});
					}
				}
			}
			else
				ticketslistnotassigned.add(updateticketId.get(i));

		}

		return  obj.iterator();
	}

	@DataProvider(name = "approveWithEditTicket")
	public Iterator<Object[]> approveWithEditTicket() throws Exception {

	List<String> createdId=SelfServeV1ItemApproveWithEdit.updateticketId,ticketslistnotassigned=SelfServeV1ItemApproveWithEdit.ticketslistnotassigned;
	int count=5;
	List<Processor> plist = new ArrayList<Processor>();
	List<Object[]> obj = new ArrayList<>();
		List<Boolean> expectedstatuscreate=SelfServeV1ItemApproveWithEdit.expectedstatuscreate,expectedstatusupdate=SelfServeV1ItemApproveWithEdit.expectedstatusupdate;
		ObjectMapper mapper = new ObjectMapper();
		String agentid=null,ticketresponse=null,getticketState,ticketState="APPROVED_WITH_EDITS",restid,tickettype="SELFSERVE_V1",ticket_reason,assigned="ASSIGNED";
		for(int i=0;i<createdId.size();i++)
		{
			plist.add(sshelper.getticketdetailsbyTicketId(createdId.get(i)));
			ticketresponse=plist.get(i).ResponseValidator.GetBodyAsText();
			getticketState=JsonPath.read(ticketresponse, "$.data.state").toString();
			restid=JsonPath.read(ticketresponse,"$.data.parent_resc_id").toString();
			ticket_reason=JsonPath.read(ticketresponse,"$.data.ticket_reason").toString();
			if(getticketState.equals(assigned))
			{
				agentid=JsonPath.read(ticketresponse,"$.data.assigned_to").toString();
				Processor p1=sshelper.pollingforAgentAssignemett(count,agentid,tickettype);
				if(p1!=null) {
					if (ticket_reason == "ITEM_ADDITION") ;
					{
						JsonNode node = mapper.readTree(p1.ResponseValidator.GetBodyAsText());
						node = node.get("search_data").get(0).get("item_tickets").get(i);
						((ObjectNode) node).put("reason", "automation reason");
						((ObjectNode) node).put("reason_category", "automation reason");
						System.out.println("payload is" + node);
						if (expectedstatuscreate.get(i) == true)
							obj.add(new Object[]{node.toString(), createdId.get(i), ticketState, agentid, 1, 200});
						else
							obj.add(new Object[]{node.toString(), ticketState, createdId.get(i), agentid, 0, 200});
					}
				}
			}
			else
				ticketslistnotassigned.add(createdId.get(i));

		}

		return  obj.iterator();
	}

	@DataProvider(name = "approvewithEditUpdate")
	public Iterator<Object[]> approvewithEditUpdate() throws Exception {
		List<String> updateticketId=SelfServeV1ItemApproveWithEdit.updateticketId,ticketslistnotassigned=SelfServeV1ItemApproveWithEdit.ticketslistnotassigned;
		int count=5;
		List<Processor> plist = new ArrayList<Processor>();
		List<Object[]> obj = new ArrayList<>();
		List<Boolean> expectedstatuscreate=SelfServeV1ItemApproveWithEdit.expectedstatuscreate,expectedstatusupdate=SelfServeV1ItemApproveWithEdit.expectedstatusupdate;
		ObjectMapper mapper = new ObjectMapper();
		String agentid=null,ticketresponse=null,getticketState,ticketState="APPROVED_WITH_EDITS",restid,tickettype="SELFSERVE_V1",ticket_reason,assigned="ASSIGNED";
		for(int i=0;i<updateticketId.size();i++)
		{
			plist.add(sshelper.getticketdetailsbyTicketId(updateticketId.get(i)));
			ticketresponse=plist.get(i).ResponseValidator.GetBodyAsText();
			getticketState=JsonPath.read(ticketresponse, "$.data.state").toString();
			restid=JsonPath.read(ticketresponse,"$.data.parent_resc_id").toString();
			ticket_reason=JsonPath.read(ticketresponse,"$.data.ticket_reason").toString();
			if(getticketState.equals(assigned))
			{
				agentid=JsonPath.read(ticketresponse,"$.data.assigned_to").toString();
				Processor p1=sshelper.pollingforAgentAssignemett(count,agentid,tickettype);
				if(p1!=null) {
					if (ticket_reason == "ITEM_ADDITION") ;
					{
						JsonNode node = mapper.readTree(p1.ResponseValidator.GetBodyAsText());
						node = node.get("search_data").get(0).get("item_tickets").get(i);
						((ObjectNode) node).put("reason", "automation reason");
						((ObjectNode) node).put("reason_category", "automation reason");
						System.out.println("payload is" + node);
						if (expectedstatuscreate.get(i) == true)
							obj.add(new Object[]{node.toString(), updateticketId.get(i), ticketState, agentid, 1, 200});
						else
							obj.add(new Object[]{node.toString(), ticketState, updateticketId.get(i), agentid, 0, 200});
					}
				}
			}
			else
				ticketslistnotassigned.add(updateticketId.get(i));

		}

		return  obj.iterator();
	}

	@DataProvider(name = "rejectTicket")
	public Iterator<Object[]> rejectTicket() throws Exception{
		//	Thread.sleep(5000);
		int count=5;
		List<String> createdId=SelfServeV1ItemReject.createticketId,ticketslistnotassigned=SelfServeV1ItemReject.ticketslistnotassigned;
		//createdId.add("1");
		List<Processor> plist = new ArrayList<Processor>();
		List<Object[]> obj = new ArrayList<>();
		List<Boolean> expectedstatuscreate=SelfServeV1ItemReject.expectedstatuscreate,expectedstatusupdate=SelfServeV1ItemReject.expectedstatusupdate;
		ObjectMapper mapper = new ObjectMapper();
		String agentid=null,ticketresponse=null,getticketState,ticketState="REJECTED",restid,tickettype="SELFSERVE_V1",ticket_reason,assigned="ASSIGNED",submit="SUBMITTED";
		for(int i=0;i<createdId.size();i++)
		{
			plist.add(sshelper.getticketdetailsbyTicketId(createdId.get(i)));
			ticketresponse=plist.get(i).ResponseValidator.GetBodyAsText();
			getticketState=JsonPath.read(ticketresponse, "$.data.state").toString();
			restid=JsonPath.read(ticketresponse,"$.data.parent_resc_id").toString();
			ticket_reason=JsonPath.read(ticketresponse,"$.data.ticket_reason").toString();
			if(getticketState.equals(submit))
			{
				Processor p1=sshelper.pollingforticketAssignment(count,createdId.get(i));

			}

			if(getticketState.equals(assigned))
			{
				agentid=JsonPath.read(ticketresponse,"$.data.assigned_to").toString();
				Processor p1=sshelper.pollingforAgentAssignemett(count,agentid,tickettype);
				if(p1!=null) {
					if (ticket_reason == "ITEM_ADDITION") ;
					{
						JsonNode node = mapper.readTree(p1.ResponseValidator.GetBodyAsText());
						node = node.get("search_data").get(0).get("item_tickets").get(i);
						((ObjectNode) node).put("reason", "automation reason");
						((ObjectNode) node).put("reason_category", "automation reason");
						System.out.println("payload is" + node);
						if (expectedstatuscreate.get(i) == true)
							obj.add(new Object[]{node.toString(), createdId.get(i), ticketState, agentid, 1, 200});
						else
							obj.add(new Object[]{node.toString(), ticketState, createdId.get(i), agentid, 0, 200});
					}
				}
			}
			else
				ticketslistnotassigned.add(createdId.get(i));

		}

		return  obj.iterator();
	}

	@DataProvider(name = "rejectUpdate")
	public Iterator<Object[]> rejectUpdate() throws Exception {
		List<String> updateticketId=SelfServeV1ItemReject.updateticketId,ticketslistnotassigned=SelfServeV1ItemReject.ticketslistnotassigned;
		int count=5;
		List<Processor> plist = new ArrayList<Processor>();
		List<Object[]> obj = new ArrayList<>();
		List<Boolean> expectedstatuscreate=SelfServeV1ItemReject.expectedstatuscreate,expectedstatusupdate=SelfServeV1ItemReject.expectedstatusupdate;
		ObjectMapper mapper = new ObjectMapper();
		String agentid=null,ticketresponse=null,getticketState,ticketState="REJECTED",restid,tickettype="SELFSERVE_V1",ticket_reason,assigned="ASSIGNED";
		for(int i=0;i<updateticketId.size();i++)
		{
			plist.add(sshelper.getticketdetailsbyTicketId(updateticketId.get(i)));
			ticketresponse=plist.get(i).ResponseValidator.GetBodyAsText();
			getticketState=JsonPath.read(ticketresponse, "$.data.state").toString();
			restid=JsonPath.read(ticketresponse,"$.data.parent_resc_id").toString();
			ticket_reason=JsonPath.read(ticketresponse,"$.data.ticket_reason").toString();
			if(getticketState.equals(assigned))
			{
				agentid=JsonPath.read(ticketresponse,"$.data.assigned_to").toString();
				Processor p1=sshelper.pollingforAgentAssignemett(count,agentid,tickettype);
				if(p1!=null) {
					if (ticket_reason == "ITEM_ADDITION") ;
					{
						JsonNode node = mapper.readTree(p1.ResponseValidator.GetBodyAsText());
						node = node.get("search_data").get(0).get("item_tickets").get(i);
						((ObjectNode) node).put("reason", "automation reason");
						((ObjectNode) node).put("reason_category", "automation reason");
						System.out.println("payload is" + node);
						if (expectedstatuscreate.get(i) == true)
							obj.add(new Object[]{node.toString(), updateticketId.get(i), ticketState, agentid, 1, 200});
						else
							obj.add(new Object[]{node.toString(), ticketState, updateticketId.get(i), agentid, 0, 200});
					}
				}
			}
			else
				ticketslistnotassigned.add(updateticketId.get(i));

		}

		return  obj.iterator();
	}


//	approveTicketCategory

	@DataProvider(name = "approveTicketCategory")
	public Iterator<Object[]> approveTicketCategory() throws Exception{
		//	Thread.sleep(5000);
		int count=5;
		List<String> createdId=SelfServeV1CategoryTest.createticketId,ticketslistnotassigned=SelfServeV1CategoryTest.ticketslistnotassigned;
		//createdId.add("725");
		List<Processor> plist = new ArrayList<Processor>();
		List<Object[]> obj = new ArrayList<>();
		List<Boolean> expectedstatuscreate=SelfServeV1CategoryTest.expectedstatuscreate;
		//expectedstatuscreate.add(true);
		ObjectMapper mapper = new ObjectMapper();
		String agentid=null,ticketresponse=null,getticketState,ticketState="APPROVED",restid,tickettype="SELFSERVE_V1",ticket_reason,assigned="ASSIGNED";
		for(int i=0;i<createdId.size();i++)
		{
			plist.add(sshelper.getticketdetailsbyTicketId(createdId.get(i)));
			ticketresponse=plist.get(i).ResponseValidator.GetBodyAsText();
			getticketState=JsonPath.read(ticketresponse, "$.data.state").toString();
			restid=JsonPath.read(ticketresponse,"$.data.parent_resc_id").toString();
			ticket_reason=JsonPath.read(ticketresponse,"$.data.ticket_reason").toString();
			if(getticketState.equals(assigned))
			{
				agentid=JsonPath.read(ticketresponse,"$.data.assigned_to").toString();
				Processor p1=sshelper.pollingforAgentAssignemett(count,agentid,tickettype);
				if(p1!=null) {
					JsonNode node = mapper.readTree(p1.ResponseValidator.GetBodyAsText());
						node = node.get("search_data").get(0).get("category_tickets").get(i);
						System.out.println("payload is" + node);
						if (expectedstatuscreate.get(i) == true)
							obj.add(new Object[]{node.toString(), createdId.get(i), ticketState, agentid, 1, 200});
						else
							obj.add(new Object[]{node.toString(), createdId.get(i), ticketState,agentid, 0, 200});

				}
			}
			else
				ticketslistnotassigned.add(createdId.get(i));

		}

		return  obj.iterator();
	}

	@DataProvider(name = "approveTicketCategoryReject")
	public Iterator<Object[]> approveTicapproveTicketCategoryRejectketCategory() throws Exception{
		//	Thread.sleep(5000);
		int count=5;
		List<String> createdId=SelfServeV1CategoryTest.createticketId,ticketslistnotassigned=SelfServeV1CategoryTest.ticketslistnotassigned;
		//createdId.add("1");
		List<Processor> plist = new ArrayList<Processor>();
		List<Object[]> obj = new ArrayList<>();
		List<Boolean> expectedstatuscreate=SelfServeV1CategoryTest.expectedstatuscreate;
		ObjectMapper mapper = new ObjectMapper();
		String agentid=null,ticketresponse=null,getticketState,ticketState="APPROVED",restid,tickettype="SELFSERVE_V1",ticket_reason,assigned="ASSIGNED";
		for(int i=0;i<createdId.size();i++)
		{
			plist.add(sshelper.getticketdetailsbyTicketId(createdId.get(i)));
			ticketresponse=plist.get(i).ResponseValidator.GetBodyAsText();
			getticketState=JsonPath.read(ticketresponse, "$.data.state").toString();
			restid=JsonPath.read(ticketresponse,"$.data.parent_resc_id").toString();
			ticket_reason=JsonPath.read(ticketresponse,"$.data.ticket_reason").toString();
			if(getticketState.equals(assigned))
			{
				agentid=JsonPath.read(ticketresponse,"$.data.assigned_to").toString();
				Processor p1=sshelper.pollingforAgentAssignemett(count,agentid,tickettype);
				if(p1!=null) {
					JsonNode node = mapper.readTree(p1.ResponseValidator.GetBodyAsText());
					node = node.get("search_data").get(0).get("category_tickets").get(i);
					((ObjectNode) node).put("reason", "automation reason");
					((ObjectNode) node).put("reason_category", "automation reason");
					System.out.println("payload is" + node);
					if (expectedstatuscreate.get(i) == true)
						obj.add(new Object[]{node.toString(), createdId.get(i), ticketState, agentid, 1, 200});
					else
						obj.add(new Object[]{node.toString(), ticketState, createdId.get(i), agentid, 0, 200});

				}
			}
			else
				ticketslistnotassigned.add(createdId.get(i));

		}

		return  obj.iterator();
	}

	@DataProvider(name = "approveTicketCategoryApprovewithedit")
	public Iterator<Object[]> approveTicketCategoryApprovewithedit() throws Exception{
		//	Thread.sleep(5000);
		int count=5;
		List<String> createdId=SelfServeV1ItemTest.createticketId,ticketslistnotassigned=SelfServeV1ItemTest.ticketslistnotassigned;
		//createdId.add("1");
		List<Processor> plist = new ArrayList<Processor>();
		List<Object[]> obj = new ArrayList<>();
		List<Boolean> expectedstatuscreate=SelfServeV1ItemTest.expectedstatuscreate,expectedstatusupdate=SelfServeV1ItemTest.expectedstatusupdate;
		ObjectMapper mapper = new ObjectMapper();
		String agentid=null,ticketresponse=null,getticketState,ticketState="APPROVED",restid,tickettype="SELFSERVE_V1",ticket_reason,assigned="ASSIGNED";
		for(int i=0;i<createdId.size();i++)
		{
			plist.add(sshelper.getticketdetailsbyTicketId(createdId.get(i)));
			ticketresponse=plist.get(i).ResponseValidator.GetBodyAsText();
			getticketState=JsonPath.read(ticketresponse, "$.data.state").toString();
			restid=JsonPath.read(ticketresponse,"$.data.parent_resc_id").toString();
			ticket_reason=JsonPath.read(ticketresponse,"$.data.ticket_reason").toString();
			if(getticketState.equals(assigned))
			{
				agentid=JsonPath.read(ticketresponse,"$.data.assigned_to").toString();
				Processor p1=sshelper.pollingforAgentAssignemett(count,agentid,tickettype);
				if(p1!=null) {
					JsonNode node = mapper.readTree(p1.ResponseValidator.GetBodyAsText());
					node = node.get("search_data").get(0).get("category_tickets").get(i);
					((ObjectNode) node).put("reason", "automation reason");
					((ObjectNode) node).put("reason_category", "automation reason");
					System.out.println("payload is" + node);
					if (expectedstatuscreate.get(i) == true)
						obj.add(new Object[]{node.toString(), createdId.get(i), ticketState, agentid, 1, 200});
					else
						obj.add(new Object[]{node.toString(), ticketState, createdId.get(i), agentid, 0, 200});

				}
			}
			else
				ticketslistnotassigned.add(createdId.get(i));

		}

		return  obj.iterator();
	}

	@DataProvider(name = "submitCategoryticket")
	public Iterator<Object[]> submitCategoryticket() throws IOException {

		List<Object[]> obj = new ArrayList<>();
		SubmitTicket st=new  SubmitTicket();
		st.build();
		Metadata md=new Metadata();
		md.build();
		JsonHelper jsonHelper = new JsonHelper();
		List<String> createdId= SelfServeV1CategoryTest.createticketId;
		createdId.add("695");
		List<Tickets> ticks = new ArrayList<Tickets>();
		int j =0;
		if(createdId.size()>0)
		{
			for (int i=0  ;i<createdId.size();i++)
			{
				Tickets t=new Tickets();
				t.setId(createdId.get(i));
				ticks.add(t);
			}
			st.setTickets(ticks.toArray(new Tickets[ticks.size()]));
			st.setMetadata(md);
			obj.add(new Object[]{jsonHelper.getObjectToJSON(st),restId[0],1,200});
		}
		return  obj.iterator();
	}


//cancleTicket
	@DataProvider(name = "cancleTicketnonApproved")
	public Iterator<Object[]> cancleTicketnonApproved() throws Exception {
		List<String> nonassignedTicket= SelfServeV1CategoryTest.ticketslistnotassigned;
		List<Object[]> obj = new ArrayList<>();
		for(int i=1;i<nonassignedTicket.size();i++) {
			obj.add(new Object[]{restId[0],nonassignedTicket.get(i),true,200});
		}
		return  obj.iterator();
	}

	@DataProvider(name = "cancleTicketnonApprovedCat")
	public Iterator<Object[]> cancleTicketnonApprovedCat() throws Exception {
		List<String> nonassignedTicket= SelfServeV1ItemTest.ticketslistnotassigned;
		List<Object[]> obj = new ArrayList<>();
		for(int i=1;i<nonassignedTicket.size();i++) {
			obj.add(new Object[]{restId[0],nonassignedTicket.get(i),true,200});
		}
		return  obj.iterator();
	}

	@DataProvider(name = "cancleTicketnonApprovedItem")
	public Iterator<Object[]> cancleTicketnonApprovedItem() throws Exception {
		List<String> nonassignedTicket= SelfServeV1CategoryTest.ticketslistnotassigned;
		List<Object[]> obj = new ArrayList<>();
		for(int i=1;i<nonassignedTicket.size();i++) {
			obj.add(new Object[]{restId[0],nonassignedTicket.get(i),true,200});
		}
		return  obj.iterator();
	}

	@DataProvider(name = "cancleTicketnonApprovedItemReject")
	public Iterator<Object[]> cancleTicketnonApprovedItemReject() throws Exception {
		List<String> nonassignedTicket= SelfServeV1ItemReject.ticketslistnotassigned;
		List<Object[]> obj = new ArrayList<>();
		for(int i=1;i<nonassignedTicket.size();i++) {
			obj.add(new Object[]{restId[0],nonassignedTicket.get(i),true,200});
		}
		return  obj.iterator();
	}

	@DataProvider(name = "cancleTicketnonApprovedItemApproveWithEdit")
	public Iterator<Object[]> cancleTicketnonApprovedItemApproveWithEdit() throws Exception {
		List<String> nonassignedTicket= SelfServeV1ItemApproveWithEdit.ticketslistnotassigned;
		List<Object[]> obj = new ArrayList<>();
		for(int i=1;i<nonassignedTicket.size();i++) {
			obj.add(new Object[]{restId[0],nonassignedTicket.get(i),true,200});
		}
		return  obj.iterator();
	}

	//cancleTicket
	@DataProvider(name = "cancleTicket")
	public Iterator<Object[]> cancleTicket() throws Exception {
		List<String> nonassignedTicket= SelfServeV1CategoryTest.ticketslistnotassigned;
		List<Object[]> obj = new ArrayList<>();
		for(int i=1;i<803;i++) {
			obj.add(new Object[]{restId[0],i,true,200});
		}
		return  obj.iterator();
	}

}