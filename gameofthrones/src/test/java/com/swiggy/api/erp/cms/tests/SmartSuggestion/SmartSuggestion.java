package com.swiggy.api.erp.cms.tests.SmartSuggestion;

import com.swiggy.api.erp.cms.constants.CmsConstants;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import com.swiggy.automation.common.utils.APIUtils;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Tyrion.RedisHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;
import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SmartSuggestion {
    Initialize gameofthrones = new Initialize();
    CMSHelper cmsHelper=new CMSHelper();
    SqlTemplate sqlTemplateCI = SystemConfigProvider.getTemplate("cms");
    ArrayList<String> dbItems=new ArrayList<>();
    ArrayList<String> redisItems=new ArrayList<>();
    @Test(description = "Verify smart suggestion in redis with DB")
   public void dbRedisMatch() throws Exception{
        CmsConstants cmsconstants=new CmsConstants();
        Map<String, Object> items = sqlTemplateCI.queryForMap(cmsconstants.itemSuggestion);
        String sugg=items.get("suggestion").toString();
        JsonNode node2=APIUtils.convertStringtoJSON(sugg);
       for (int i=0;i<node2.size();i++){
           JsonNode node3=(node2.get(i).get("item_id"));
           dbItems.add(node3.toString());
       }

      RedisHelper rh=new RedisHelper();
        HashMap<String,String> suggestions=rh.hKeysValues("cmsredis",0,"item-"+cmsHelper.getSmartItem());
        String itemSugg=suggestions.get("item_suggestion");
        System.out.println(itemSugg);
        JsonNode node=APIUtils.convertStringtoJSON(itemSugg);
        for (int i=0;i<node.get(1).size();i++){
            JsonNode node1=node.get(1).get(i).get("item_id");
           redisItems.add(node1.toString());
        }
        Assert.assertEquals(dbItems,redisItems);

    }

}
