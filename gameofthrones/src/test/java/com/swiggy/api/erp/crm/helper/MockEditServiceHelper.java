package com.swiggy.api.erp.crm.helper;

import framework.gameofthrones.Tyrion.WireMockHelper;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

public class MockEditServiceHelper {
    WireMockHelper wireMockHelper = new WireMockHelper();

    public void stubOrderDetailAPIResponse (String orderId,String restId,String itemId,String
            cost,String orderType, String order_status) throws IOException, InterruptedException

    {
        File file = new File("../Data/MockAPI/ResponseBody/crm/OrderDetailApiMockResponse");
        String body = FileUtils.readFileToString(file);
        body = body.replace("${order_id}", orderId).replace("${restaurant_id}",restId).
                replace("${item_id}",itemId).replace("${sub_total}",cost).
                replace("${order_status}",order_status).replace("${type}",orderType);
        System.out.println(body);
        wireMockHelper.setupStubEdit("get_order_from_id/"+orderId+"/formatted/", 200, "application/json", body,0);

    }

    public void stubOrderDetailAPIResponse2(String orderId,String restId,String itemId,String
            cost,String orderType, String order_status, String restaurant_type) throws IOException, InterruptedException

    {
        File file = new File("../Data/MockAPI/ResponseBody/crm/OrderDetailApiMockResponse");
        String body = FileUtils.readFileToString(file);
        body = body.replace("${order_id}", orderId).replace("${restaurant_id}",restId).
                replace("${item_id}",itemId).replace("${sub_total}",cost).
                replace("${order_status}",order_status).replace("${type}",orderType).
                replace("${restaurant_type}",restaurant_type);
        System.out.println(body);
        wireMockHelper.setupStub("get_order_from_id/"+orderId+"/formatted", 200, "application/json", body,0);

    }

    public void stubDeliveryTrackAPIResponse(String orderId, String deliveryStatus, String assignedPredTime, String assignedActualTime, String confirmedPredTime,
                                             String confirmedActualTime, String arrivedPredTime, String arrivedActualTime, String pickedupPredTime,
                                             String pickedupActualTime, String reachedPredTime, String reachedActualTime, String deliveredPredTime,
                                             String deliveredActualTime) throws IOException, InterruptedException {
        File file = new File("../Data/MockAPI/ResponseBody/crm/OrderTrackApiMockResponse");
        String body = FileUtils.readFileToString(file);
        body = body.replace("${order_id}", orderId).
                replace("${delivery_status}", deliveryStatus).
                replace("${assigned_pred_time}", assignedPredTime).
                replace("${assigned_actual_time}", assignedActualTime).
                replace("${confirmed_pred_time}", confirmedPredTime).
                replace("${confirmed_actual_time}", confirmedActualTime).
                replace("${arrived_pred_time}", arrivedPredTime).
                replace("${arrived_actual_time}", arrivedActualTime).
                replace("${pickedup_pred_time}", pickedupPredTime).
                replace("${pickedup_actual_time}", pickedupActualTime).
                replace("${reached_pred_time}", reachedPredTime).
                replace("${reached_actual_time}", reachedActualTime).
                replace("${delivered_pred_time}", deliveredPredTime).
                replace("${delivered_actual_time}", deliveredActualTime);
        System.out.println(body);
        wireMockHelper.setupStubEdit("api/order/track/" + orderId, 200, "application/json", body, 0);
    }

    public void stubFetchAllDispositionIDs(String orderId,String dispositionId, String subDispositionId) throws IOException
    {
        File file = new File("../Data/MockAPI/ResponseBody/crm/FetchAllDispositionIdsMockResponse");
        String body = FileUtils.readFileToString(file);
        body=body.replace("${dispositionId}",dispositionId).replace("${subDispositionId}",subDispositionId);
        System.out.println(body);
        wireMockHelper.setupStub("v1/order/"+orderId+"/order-cancellation-disposition", 200, "application/json", body,0);
    }

    public void stubGetCancellationFee(String orderId) throws IOException
    {
        File file = new File("../Data/MockAPI/ResponseBody/crm/CancellationFeeMockResponse");
        String body = FileUtils.readFileToString(file);
        System.out.println(body);
        wireMockHelper.setupStub("v1/order/"+orderId+"/cancellation-fee/", 200, "application/json", body,0);
    }

    public void stubFFcancellation(String orderId) throws IOException
    {
        File file = new File("../Data/MockAPI/ResponseBody/crm/FFCancellationAPIMockResponse");
        String body = FileUtils.readFileToString(file);
        System.out.println(body);
        wireMockHelper.setupStubPut("v1/order/"+orderId+"/", 200, "application/json", body,0);
    }


    public void stubEditCheckAPI(String itemId,String cost) throws IOException
    {
        File file = new File("../Data/MockAPI/ResponseBody/crm/EditCheckApiMockResponse");
        String body = FileUtils.readFileToString(file);
        body = body.replace("${itemId}",itemId).replace("${cost}",cost);
        System.out.println(body);
        wireMockHelper.setupStubPost("api/v1/order/update/check", 200, "application/json", body,2000);
    }

    public void stubEditConfirmAPI(String orderId,String itemId,String cost) throws IOException
    {
        File file = new File("../Data/MockAPI/ResponseBody/crm/EditConfirmApiMockResponse");
        String body = FileUtils.readFileToString(file);
        body = body.replace("${orderId}", orderId).replace("${itemId}",itemId).replace("${cost}",cost);
        System.out.println(body);
        wireMockHelper.setupStubPost("api/v1/order/update/confirm", 200, "application/json", body,2000);
    }

    public void stubCheckoutOrderDetailAPIResponse (String orderId,String restId,String itemId,String
            cost,String orderType) throws IOException, InterruptedException

    {
        File file = new File("../Data/MockAPI/ResponseBody/crm/CheckoutOrderDetailApiMockResponse");
        String body = FileUtils.readFileToString(file);
        body = body.replace("${orderId}", orderId).replace("${restId}",restId).
                replace("${itemId}",itemId).replace("${cost}",cost).replace("${type}",orderType);
        System.out.println(body);
        wireMockHelper.setupStubEdit("api/v1/order/get_order_details?order_id="+orderId, 200, "application/json", body,0);

    }

    public void stubVendorOrderEditableAPIResponse (String orderId,String orderEditable, String itemAdditionAllowed) throws IOException, InterruptedException

    {
        File file = new File("../Data/MockAPI/ResponseBody/crm/VendorOrderEditableApiMockResponse");
        String body = FileUtils.readFileToString(file);
        body=body.replace("${0}",orderEditable).replace("${1}",itemAdditionAllowed);
        System.out.println(body);
        wireMockHelper.setupStubEdit("api/v1/order-editable-check/"+orderId, 200, "application/json", body,0);

    }

}
