package com.swiggy.api.erp.ff.constants;

public interface TelephonyPlatformConstants {

    String SERVICE_TELEPHONY_PLATFORM = "telephonyplatform";
    String WIREMOCK = "wiremocktelephony";
    String TELEPHONY_DB_HOST = "telephony";
    String testAppName = "Automation_App_01";
    String testPodName = "TestAutomation";
    String testAppUrl = "http://my.exotel.in/exoml/start/124586";
    String testCompletedCallbackUrl = "http://wiremock.swiggyint.in/completed";
    String testFailedCallbackUrl = "http://wiremock.swiggyint.in/failed";
    String testKeyPressCallbackUrl = "http://wiremock.swiggyint.in/keypress";
    String testCallerRegionId = "1";
    String testCalleeId = "8892028093";
    String digit = "2";
    String statusCompleted = "completed";
    String statusFailed = "failed";
}
