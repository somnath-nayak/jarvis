package com.swiggy.api.erp.finance.pojo;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.erp.finance.pojo
 **/

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "transactionList"
})
public class AddBulkTransactionWithErrorReport {

    @JsonProperty("transactionList")
    private List<TransactionList> transactionList = null;

    @JsonProperty("transactionList")
    public List<TransactionList> getTransactionList() {
        return transactionList;
    }

    @JsonProperty("transactionList")
    public void setTransactionList(List<TransactionList> transactionList) {
        this.transactionList = transactionList;
    }

    private void setDefaultValues(int de_id, int de_incoming, int de_outgoing, String orderID,String txnType) {
        List<TransactionList> list = new ArrayList<>();
        TransactionList transactionList = new TransactionList();
        transactionList.build(de_id,de_incoming,de_outgoing,orderID,txnType);
        list.add(transactionList);
        if (this.getTransactionList() == null)
            this.setTransactionList(list);
    }

    public AddBulkTransactionWithErrorReport build(int DE_Id, int de_incoming, int de_outgoing, String orderID,String txnType) {
        setDefaultValues(DE_Id,de_incoming,de_outgoing,orderID,txnType);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("transactionList", transactionList).toString();
    }

}
