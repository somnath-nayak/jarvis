package com.swiggy.api.erp.cms.tests.Restaurant;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.dp.RestaurantDP;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import org.testng.Assert;
import org.testng.annotations.Test;

public class DeleteRestaurantSlots extends RestaurantDP {

    CMSHelper cmsHelper= new CMSHelper();

    @Test(dataProvider = "restslotscreate1",description = "Create the restaurant slots and push to kafka")
    public void deleteRestaurant(String day,String restaurant_id, String open_time, String close_time){
        String response=cmsHelper.restaurantSlotsCreate(day,restaurant_id,open_time,close_time).ResponseValidator.GetBodyAsText();
        System.out.println(response);
        int status= JsonPath.read(response,"$.status");
        int slotId = JsonPath.read(response,"$.data.id");
        Assert.assertEquals(status,1);

        String responseOfDeleteRestaurantSlot = cmsHelper.restaurantSlotsDelete(Integer.toString(slotId)).ResponseValidator.GetBodyAsText();
        int statusOfResponseOfDeleteRestaurantSlot= JsonPath.read(responseOfDeleteRestaurantSlot,"$.status");
        Assert.assertEquals(statusOfResponseOfDeleteRestaurantSlot,1);
        System.out.println(responseOfDeleteRestaurantSlot);
    }
}
