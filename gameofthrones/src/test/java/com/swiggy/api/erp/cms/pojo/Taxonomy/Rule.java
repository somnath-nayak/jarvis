
package com.swiggy.api.erp.cms.pojo.Taxonomy;

import java.util.*;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.swiggy.api.daily.preorder.pojo.DeliveryDetails;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "enabled",
    "key",
    "operator",
    "value"
})
public class Rule {

    @JsonProperty("enabled")
    private Boolean enabled;
    @JsonProperty("key")
    private String key;
    @JsonProperty("operator")
    private String operator;
    @JsonProperty("value")
    private List<String> value = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Rule() {
        setDefaultData();
    }

    /**
     * 
     * @param enabled
     * @param value
     * @param operator
     * @param key
     */
    public Rule(Boolean enabled, String key, String operator, List<String> value) {
        super();
        this.enabled = enabled;
        this.key = key;
        this.operator = operator;
        this.value = value;
    }

    @JsonProperty("enabled")
    public Boolean getEnabled() {
        return enabled;
    }

    @JsonProperty("enabled")
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Rule withEnabled(Boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    @JsonProperty("key")
    public String getKey() {
        return key;
    }

    @JsonProperty("key")
    public void setKey(String key) {
        this.key = key;
    }

    public Rule withKey(String key) {
        this.key = key;
        return this;
    }

    @JsonProperty("operator")
    public String getOperator() {
        return operator;
    }

    @JsonProperty("operator")
    public void setOperator(String operator) {
        this.operator = operator;
    }

    public Rule withOperator(String operator) {
        this.operator = operator;
        return this;
    }

    @JsonProperty("value")
    public List<String> getValue() {
        return value;
    }

    @JsonProperty("value")
    public void setValue(List<String> value) {
        this.value = value;
    }

    public Rule withValue(List<String> value) {
        this.value = value;
        return this;
    }

    public Rule setDefaultData()
    {
        this.withEnabled(true).withKey("category").withOperator("EQ").withValue(new ArrayList<>(Arrays.asList("5c7fafe9-9057-42b9-a406-15a8151b7f9c")));
        return this;
    }

}
