package com.swiggy.api.erp.cms.tests.CatalogV2;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.dp.CatalogV2PricingInventoryDP;
import com.swiggy.api.erp.cms.helper.CatalogV2Helper;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created-By:Ashiwani
 */

public class TestPricing extends CatalogV2PricingInventoryDP {

    CatalogV2Helper cms=new CatalogV2Helper();

    @Test(dataProvider = "createpricingdp",description = "This test will create a Price")
    public void createPrice(String key1,long mrp,long storeprice) throws Exception{
        String response=cms.createPricing(key1,mrp,storeprice).ResponseValidator.GetBodyAsText();
        String key= JsonPath.read(response,"$.data..key").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertNotNull(JsonPath.read(response,"$.data"));
        Assert.assertEquals(key,key1,"Something Wrong");
    }

    @Test(description = "This test will verify non creation of Price")
    public void createPriceNegative() throws Exception{
        String response=cms.createPricingNegative().ResponseValidator.GetBodyAsText();
        String statusmsg=JsonPath.read(response,"$.status_message").toString().replace("[","").replace("]","");
        Assert.assertEquals(statusmsg,"Malformed Request");
    }

    @Test(dataProvider = "createpricingdp",description = "This test will get a Price for Based on given key")
    public void getPriceing(String key1,long mrp,long storeprice) throws Exception{
        String responsecreate=cms.createPricing(key1,mrp,storeprice).ResponseValidator.GetBodyAsText();
        String key= JsonPath.read(responsecreate,"$.data..key").toString().replace("[","").replace("]","").replace("\"","");
        String response = cms.getPricing(key).ResponseValidator.GetBodyAsText();
        String keyact = JsonPath.read(response, "$.data..key").toString().replace("[", "").replace("]", "").replace("\"", "");
        Assert.assertEquals(keyact, key, "Some thing wrong");
    }

    @Test(dataProvider = "getpricingdpnegative")
    public void getPriceingNegativeCase(String store_sku){
        String response=cms.getPricing(store_sku).ResponseValidator.GetBodyAsText();
        List<String> keyact= JsonPath.read(response,"$.data");
        Assert.assertEquals(keyact.size(),0,"Size should be zero");
    }
}
