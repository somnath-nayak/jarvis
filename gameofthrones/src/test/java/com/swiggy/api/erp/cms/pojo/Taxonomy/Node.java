
package com.swiggy.api.erp.cms.pojo.Taxonomy;

import java.util.*;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "node_type",
    "node_name",
    "enabled",
    "images",
    "rule_sets"
})
public class Node {

    @JsonProperty("node_type")
    private String node_type;
    @JsonProperty("node_name")
    private String node_name;
    @JsonProperty("enabled")
    private Boolean enabled;
    @JsonProperty("images")
    private List<String> images = null;
    @JsonProperty("rule_sets")
    private List<Rule_set> rule_sets = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     */
    public Node() {
        setDefaultData();
    }

    /**
     * @param enabled
     * @param images
     * @param node_name
     * @param node_type
     * @param rule_sets
     */
    public Node(String node_type, String node_name, Boolean enabled, List<String> images, List<Rule_set> rule_sets) {
        super();
        this.node_type = node_type;
        this.node_name = node_name;
        this.enabled = enabled;
        this.images = images;
        this.rule_sets = rule_sets;
    }

    @JsonProperty("node_type")
    public String getNode_type() {
        return node_type;
    }

    @JsonProperty("node_type")
    public void setNode_type(String node_type) {
        this.node_type = node_type;
    }

    public Node withNode_type(String node_type) {
        this.node_type = node_type;
        return this;
    }

    @JsonProperty("node_name")
    public String getNode_name() {
        return node_name;
    }

    @JsonProperty("node_name")
    public void setNode_name(String node_name) {
        this.node_name = node_name;
    }

    public Node withNode_name(String node_name) {
        this.node_name = node_name;
        return this;
    }

    @JsonProperty("enabled")
    public Boolean getEnabled() {
        return enabled;
    }

    @JsonProperty("enabled")
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Node withEnabled(Boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    @JsonProperty("images")
    public List<String> getImages() {
        return images;
    }

    @JsonProperty("images")
    public void setImages(List<String> images) {
        this.images = images;
    }

    public Node withImages(List<String> images) {
        this.images = images;
        return this;
    }

    @JsonProperty("rule_sets")
    public List<Rule_set> getRule_sets() {
        return rule_sets;
    }

    @JsonProperty("rule_sets")
    public void setRule_sets(List<Rule_set> rule_sets) {
        this.rule_sets = rule_sets;
    }

    public Node withRule_sets(List<Rule_set> rule_sets) {
        this.rule_sets = rule_sets;
        return this;
    }

    public Node setDefaultData() {
        this.withEnabled(true).withNode_name("test").withNode_type("1").withImages(new ArrayList<>(Arrays.asList("abcd", "wd")));
        return this;
    }


}
