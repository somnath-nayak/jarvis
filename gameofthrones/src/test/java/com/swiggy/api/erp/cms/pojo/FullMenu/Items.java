package com.swiggy.api.erp.cms.pojo.FullMenu;

import com.swiggy.api.erp.cms.constants.MenuConstants;

/**
 * Created by kiran.j on 2/2/18.
 */
public class Items
{
    private boolean in_stock;

    private int is_veg;

    private boolean enable;

    private int addon_free_limit;

    private String category_id;

    private Addon_Groups[] addon_groups;

    private Pricing_combinations[] pricing_combinations;

    private String id;

    private String veg;

    private double price;

    private Variant_groups[] variant_groups;

    private String image_url;

    private int addon_limit;

    private String description;

    private String sub_category_id;

    private String name;

    private Gst_Details gst_details;

    private String packing_charges;

    private Item_Slots[] item_slots;

    public boolean getIn_stock ()
    {
        return in_stock;
    }

    public void setIn_stock (boolean in_stock)
    {
        this.in_stock = in_stock;
    }

    public int getIs_veg ()
    {
        return is_veg;
    }

    public void setIs_veg (int is_veg)
    {
        this.is_veg = is_veg;
    }

    public boolean getEnable ()
    {
        return enable;
    }

    public void setEnable (boolean enable)
    {
        this.enable = enable;
    }

    public int getAddon_free_limit ()
{
    return addon_free_limit;
}

    public void setAddon_free_limit (int addon_free_limit)
    {
        this.addon_free_limit = addon_free_limit;
    }

    public String getCategory_id ()
    {
        return category_id;
    }

    public void setCategory_id (String category_id)
    {
        this.category_id = category_id;
    }

    public Addon_Groups[] getAddon_groups ()
    {
        return addon_groups;
    }

    public void setAddon_groups (Addon_Groups[] addon_groups)
    {
        this.addon_groups = addon_groups;
    }

    public Pricing_combinations[] getPricing_combinations ()
    {
        return pricing_combinations;
    }

    public void setPricing_combinations (Pricing_combinations[] pricing_combinations)
    {
        this.pricing_combinations = pricing_combinations;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getVeg ()
    {
        return veg;
    }

    public void setVeg (String veg)
    {
        this.veg = veg;
    }

    public double getPrice ()
    {
        return price;
    }

    public void setPrice (double price)
    {
        this.price = price;
    }

    public Variant_groups[] getVariant_groups ()
    {
        return variant_groups;
    }

    public void setVariant_groups (Variant_groups[] variant_groups)
    {
        this.variant_groups = variant_groups;
    }

    public String getImage_url ()
{
    return image_url;
}

    public void setImage_url (String image_url)
    {
        this.image_url = image_url;
    }

    public int getAddon_limit ()
{
    return addon_limit;
}

    public void setAddon_limit (int addon_limit)
    {
        this.addon_limit = addon_limit;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public String getSub_category_id ()
    {
        return sub_category_id;
    }

    public void setSub_category_id (String sub_category_id)
    {
        this.sub_category_id = sub_category_id;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public Gst_Details getGst_details ()
{
    return gst_details;
}

    public void setGst_details (Gst_Details gst_details)
    {
        this.gst_details = gst_details;
    }

    public String getPacking_charges ()
{
    return packing_charges;
}

    public void setPacking_charges (String packing_charges)
    {
        this.packing_charges = packing_charges;
    }

    public Item_Slots[] getItem_slots ()
{
    return item_slots;
}

    public void setItem_slots (Item_Slots[] item_slots)
    {
        this.item_slots = item_slots;
    }

    public Items build() {
        setDefaultValues();
        return this;
    }

    public void setDefaultValues() {
        Pricing_combinations pricing_combinations = new Pricing_combinations();
        pricing_combinations.build();
        Variant_groups variant_groups = new Variant_groups();
        variant_groups.build();
        Addon_Groups addon_group= new Addon_Groups();
        addon_group.build();
        Item_Slots item_slots = new Item_Slots();
        item_slots.build();
        Gst_Details gst_details = new Gst_Details();
        gst_details.build();
        if(this.getId() == null)
            this.setId(MenuConstants.items_id);
        if(this.getCategory_id() == null)
            this.setCategory_id(MenuConstants.items_category_id);
        if(this.getSub_category_id() == null)
            this.setSub_category_id(MenuConstants.items_subcat_id);
        if(this.getName() == null)
            this.setName(MenuConstants.items_name);
        if(this.getDescription() == null)
            this.setDescription(MenuConstants.items_description);
        if(this.getImage_url() == null)
            this.setImage_url(MenuConstants.items_image_url); 
        if(this.getVariant_groups() == null)
            this.setVariant_groups(new Variant_groups[0]);
        if(this.getAddon_groups() == null)
            this.setAddon_groups(new Addon_Groups[0]);
        if(this.getPricing_combinations() == null)
            this.setPricing_combinations(new Pricing_combinations[0]);
            this.setPricing_combinations(new Pricing_combinations[]{pricing_combinations});
        if(this.getAddon_free_limit() ==0)
            this.setAddon_free_limit(MenuConstants.itemLevelAddon_free_limit);
        if(this.getAddon_limit() ==0)
            this.setAddon_limit(MenuConstants.itemLevelAddon_free_limit);
        
        this.setVariant_groups(new Variant_groups[]{variant_groups});
        this.setAddon_groups(new Addon_Groups[] {addon_group});
        this.setIn_stock(MenuConstants.items_instock);
        this.setEnable(MenuConstants.items_enable);
        this.setPrice(MenuConstants.default_price);
       // this.setPrice(MenuConstants.zero_price);
        this.setGst_details(gst_details);
  }

    @Override
    public String toString()
    {
        return "{in_stock = "+in_stock+", is_veg = "+is_veg+", enable = "+enable+", addon_free_limit = "+addon_free_limit+", category_id = "+category_id+", addon_groups = "+addon_groups+", pricing_combinations = "+pricing_combinations+", id = "+id+", veg = "+veg+", price = "+price+", variant_groups = "+variant_groups+", image_url = "+image_url+", addon_limit = "+addon_limit+", description = "+description+", sub_category_id = "+sub_category_id+", name = "+name+", gst_details = "+gst_details+", packing_charges = "+packing_charges+", item_slots = "+item_slots+"}";
    }
}
