package com.swiggy.api.erp.cms.catalogV2.pojos;

import com.swiggy.api.erp.cms.constants.CatalogV2Constants;
import com.swiggy.api.erp.cms.pojo.CategoryServiceCreateCategory.Attributes;
import com.swiggy.api.erp.cms.pojo.CategoryServiceCreateCategory.Rules;

public class SkuAttributes {
	
	   public SkuAttributes(){
	    }

	private String brand;

	public String getBrand()
	{
		return brand;
	}

	public void setBrand (String brand)
	{
		this.brand = brand;
	}

	public SkuAttributes build(){
		setDefaultValues();
		return this;
	}

	public void setDefaultValues() {
		if(this.getBrand()==null){
			this.setBrand(CatalogV2Constants.allowedValues[0]);
		}
	}
}



