package com.swiggy.api.erp.finance.helper;

import com.swiggy.api.erp.finance.constants.CashMgmtConstant;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Tyrion.SqlTemplate;

import java.util.List;
import java.util.Map;

public class CashMgmtDBhelper {
    SqlTemplate sqlTemplate;


    Initialize gameofthrones = new Initialize();

    public CashMgmtDBhelper() {
        sqlTemplate = SystemConfigProvider.getTemplate("finance_cash");
    }

    public void initializeDE(String de_id,String emp_status) throws  InterruptedException{

        int update_success_or_fail = sqlTemplate.update("update delivery_boys set employment_status="+emp_status +" where id="+de_id+";");
        System.out.println("##############################################################");
        System.out.println("update delivery_boys set employment_status="+emp_status +" where id="+de_id+";");
        System.out.println("###############################################################");
        System.out.println(update_success_or_fail);



    }
    public Object getSessionIdFromDB(String de_id) throws InterruptedException {


        List<Map<String, Object>> list = sqlTemplate.queryForList("select * from de_cash_sessions where de_id=" + de_id + " order by id desc limit 1");

        System.out.println("Data received from DB is ==>");
        System.out.println("###########################");
        System.out.println(list);
        if (list.isEmpty())
            System.out.println("No data for DE id " + de_id);
        else if(list!=null){
            System.out.println("Found DE's latest Session id  from DB");
            return (list.get(0).get("id"));

        }
        return 0;
    }

    public Object getSessionStateFromDB(String de_id) throws InterruptedException,ArrayIndexOutOfBoundsException {

        List<Map<String, Object>> list = sqlTemplate.queryForList("select * from de_cash_sessions where de_id=" + de_id +" order by id desc limit 1;");

        System.out.println("Data received from DB is ==>");
        System.out.println("###########################");
        System.out.println(list);
        if (list.isEmpty()) System.out.println("No data for DE id " + de_id);
        else if(list!=null){
            System.out.println("Found DE session data from DB");
            return list.get(0).get("state");


        }
        return 0;

    }

    public Object getFloatingCashFromDB(String de_id) throws InterruptedException {

        List<Map<String, Object>> list = sqlTemplate.queryForList("select * from de_cash_sessions where de_id=" + de_id + " order by id desc limit 1");

        System.out.println("Data received from DB is ==>");
        System.out.println("###########################");
        System.out.println(list);
        if (list.isEmpty())
            System.out.println("No data for DE id " + de_id);
        else if (list!=null){
            System.out.println("Found DE Floating cash from DB");
            return (list.get(0).get("floating_cash"));

        }
        return 0;
    }

    public Object getOrderCountFromDB(String de_id,String session_id) {

        List<Map<String, Object>> list = sqlTemplate.queryForList("select count(distinct order_id) as order_count from de_cash_transactions where de_id=" + de_id + " and session_id=" + session_id + " and order_id is not null;");

        return list.get(list.size()-1).get("order_count");
    }

    public Object getSessionCount(String de_id){

        List<Map<String, Object>> list = sqlTemplate.queryForList("select count(*) as session_count from de_cash_sessions where de_id="+de_id);

        return list.get(0).get("session_count");



    }
    public Object getTransactionDB(String de_id,String session_id){


        List<Map<String, Object>> list = sqlTemplate.queryForList("select * from de_cash_transactions where de_id="+de_id+" and session_id="+session_id+" order by id desc limit 1");
        return list;

    }

    public Object transactionValidatorFromDB(String de_id,String order_id,String txn_type) {

        List<Map<String, Object>> orderExist = sqlTemplate.queryForList("select * from de_cash_transactions where de_id=" + de_id + " and order_id='" + order_id + "' and transaction_type='" + txn_type+"'");
        System.out.println(orderExist);
       // System.out.println("size is ="+orderExist.size());

        return orderExist.get(0).get("order_id");

    }

    public Object getActiveDEFromDB(String emp_status){

        List<Map<String, Object>> list = sqlTemplate.queryForList("select * from delivery_boys where employment_status="+emp_status+ " limit "+CashMgmtConstant.DE_COUNT);
        return list;


    }

    public int setDEEmpStatus(String emp_status){

        int updatestatus = sqlTemplate.update("update delivery_boys set employment_status="+emp_status+ " where id<2000;");
        return updatestatus;
    }

}
