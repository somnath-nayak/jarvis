package com.swiggy.api.erp.crm.constants;

import java.util.HashMap;

/**
 * Created by sumit.m on 05/06/18.
 */
public class CafeFlowMapper {

    public HashMap<String,String[]> defineFlow(){

        HashMap<String,String[]> flowMap = new HashMap<String, String[]>();

        //Complete cafe flows
        flowMap.put("foodIssues1",new String[]{CRMConstants.Help,CRMConstants.OrderIssues, CRMConstants.cafeCancel, CRMConstants.cafeChangedMyMind, CRMConstants.cafeYesCancelChangedMind, CRMConstants.cafeNeedInfoChangedMindYes});
        flowMap.put("foodIssues2",new String[]{CRMConstants.Help,CRMConstants.OrderIssues, CRMConstants.cafeCancel, CRMConstants.cafeChangedMyMind, CRMConstants.cafeYesCancelChangedMind, CRMConstants.cafeDoneChangedYes});

        flowMap.put("foodIssues3",new String[]{CRMConstants.Help,CRMConstants.OrderIssues, CRMConstants.cafeCancel, CRMConstants.cafeChangedMyMind, CRMConstants.cafeNotCancelChangedMind, CRMConstants.cafeNeedInfoChangedMindNot});
        flowMap.put("foodIssues4",new String[]{CRMConstants.Help,CRMConstants.OrderIssues, CRMConstants.cafeCancel, CRMConstants.cafeChangedMyMind, CRMConstants.cafeNotCancelChangedMind, CRMConstants.cafeDoneChangedNot});

        flowMap.put("foodIssues5",new String[]{CRMConstants.Help,CRMConstants.OrderIssues, CRMConstants.cafeCancel, CRMConstants.cafeItemsNotAvailable});
        flowMap.put("foodIssues6",new String[]{CRMConstants.Help,CRMConstants.OrderIssues, CRMConstants.cafeCancel, CRMConstants.cafeOtherIssues});

        flowMap.put("foodIssues7",new String[]{CRMConstants.Help,CRMConstants.OrderIssues, CRMConstants.cafeEdit});
        flowMap.put("foodIssues8",new String[]{CRMConstants.Help,CRMConstants.OrderIssues, CRMConstants.cafeItemsMissing});
        flowMap.put("foodIssues9",new String[]{CRMConstants.Help,CRMConstants.OrderIssues, CRMConstants.cafeBadQuality});
        flowMap.put("foodIssues10",new String[]{CRMConstants.Help,CRMConstants.OrderIssues, CRMConstants.cafeNotRcvdRefund});
        flowMap.put("foodIssues11",new String[]{CRMConstants.Help,CRMConstants.OrderIssues, CRMConstants.cafePaymentRefundQueries});
        flowMap.put("foodIssues12",new String[]{CRMConstants.Help,CRMConstants.OrderIssues, CRMConstants.cafeBuffetCancel});
        flowMap.put("foodIssues13",new String[]{CRMConstants.Help,CRMConstants.OrderIssues, CRMConstants.cafeDifferentIssue});


        return flowMap;

    }
}
