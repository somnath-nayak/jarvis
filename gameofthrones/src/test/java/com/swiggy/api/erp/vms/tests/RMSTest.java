package com.swiggy.api.erp.vms.tests;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;
import com.swiggy.api.erp.vms.dp.RMSDp;
import com.swiggy.api.erp.vms.helper.RMSCommonHelper;
import com.swiggy.api.erp.vms.helper.RMSHelper;
import com.swiggy.automation.common.utils.APIUtils;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;
import framework.gameofthrones.Cersei.ToolBox;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import net.minidev.json.JSONArray;
import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class RMSTest extends RMSDp {

	RMSHelper helper = new RMSHelper();
	SchemaValidatorUtils schemaValidatorUtils = new SchemaValidatorUtils();
	public static List<String> user_ids = new ArrayList<>();

	// DOne
	@Test(dataProvider = "getUserNameAndPassword", groups = { "sanity", "smoke" }, description = "User Login to RMS")
	public void LoginApi(String userName, String password, String statusMessage)
			throws IOException, ProcessingException {
		Processor processor = helper.getLogin(userName, password);
		String resp = processor.ResponseValidator.GetBodyAsText();
		Assert.assertTrue(Pattern.matches("\\d{4,10}", processor.ResponseValidator.GetNodeValue("$.data.mobile")),
				"Wrong MobileNumber");
		Assert.assertTrue(
				Pattern.matches("\\d{4,}", String.valueOf(processor.ResponseValidator.GetNodeValueAsInt("$.data.rid"))),
				"Wrong RestaurantId");
		Assert.assertTrue(Pattern.matches("[a-zA-Z0-9\\s]+", processor.ResponseValidator.GetNodeValue("$.data.name")),
				"Wrong Name");
		Assert.assertTrue(Pattern.matches("[a-zA-Z]+", processor.ResponseValidator.GetNodeValue("$.data.city")),
				"Wrong city");
		Assert.assertTrue(
				Pattern.matches("[a-z0-9-]+", processor.ResponseValidator.GetNodeValue("$.data.access_token")),
				"Wrong access_token");
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/login");
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		Assert.assertTrue(missingNodeList.isEmpty(),
				missingNodeList + " Nodes Are Missing Or Not Matching For LoginApi API");
		boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
		Assert.assertTrue(!isEmpty);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
	}

	@Test(dataProvider = "logout", groups = { "sanity", "smoke" }, description = "User Login to RMS")
	public void verifyLogOutApi(String userName, String password, int statusCode, String statusMessage)
			throws IOException, ProcessingException {
		Processor processor = helper.getLogin(userName, password);
		if (processor.ResponseValidator.GetNodeValue("statusMessage")
				.equals(processor.ResponseValidator.GetNodeValue("statusMessage"))) {
			Processor p = helper.logout(userName, processor.ResponseValidator.GetNodeValue("$.data.access_token"));
			String resp = p.ResponseValidator.GetBodyAsText();
			System.out.println(resp);
			Assert.assertEquals(p.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
			Assert.assertEquals(p.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
		} else {
			Assert.fail("login Failed..Cannot Checkout logout now");
		}
	}

	@Test(dataProvider = "logoutRegression", groups = { "sanity", "smoke" }, description = "User Login to RMS")
	public void verifyLogOutApiRegression(String userName, String accessToken, int statusCode, String statusMessage)
			throws IOException, ProcessingException {
		Processor p = helper.logout(userName, accessToken);
		String resp = p.ResponseValidator.GetBodyAsText();
		System.out.println(resp);
		Assert.assertEquals(p.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertEquals(p.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
	}

	@Test(groups = { "smoke", "sanity", "regression" }, description = "Checks health of RMS service")
	public void gethealthCheck() throws IOException, ProcessingException {
		Processor processor = helper.getHealthCheck();
		String resp = processor.ResponseValidator.GetBodyAsText();

		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/gethealthCheck");
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		Assert.assertTrue(missingNodeList.isEmpty(),
				missingNodeList + " Nodes Are Missing Or Not Matching For LoginApi API");
		boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
		Assert.assertTrue(!isEmpty);

		Assert.assertTrue(Boolean.parseBoolean(
				processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.healths.postgres.healthy")));
		Assert.assertTrue(Boolean.parseBoolean(
				(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.healths.redis.healthy"))));
		Assert.assertTrue(Boolean.parseBoolean(
				(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.healths.rabbitmq.healthy"))));
	}

	@Test(dataProvider = "getContactReasons_sanity", groups = { "smoke", "sanity",
			"regression" }, description = "Gets Contact Reasons")
	public void getContactReasons(String accessToken) throws IOException, ProcessingException {
		Processor processor = helper.getContactReasons(accessToken);
		String resp = processor.ResponseValidator.GetBodyAsText();
		String keys = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..key");
		System.out.println("keyyyyyy : " + keys);
		String values = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..value");
		System.out.println("valuessssssssss : " + values);
		Assert.assertNull(processor.ResponseValidator.GetNodeValue("statusMessage"));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), 0);
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..key"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..value"));

	}

	@Test(dataProvider = "contactUsTicket", groups = { "smoke", "sanity" }, description = "Create Freshdesk ticket")
	public void contactUsTicket(String accessToken, String restaurantId, String subject, String message,
			String category, String city, int statusCode, String statusMessage)
			throws IOException, ProcessingException {
		Processor processor = helper.contactUsTicket(accessToken, restaurantId, subject, message, category, city);
		String resp = processor.ResponseValidator.GetBodyAsText();
		
		
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/contactUsTicket");
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		Assert.assertTrue(missingNodeList.isEmpty(),
				missingNodeList + " Nodes Are Missing Or Not Matching For contactUsTicket API");
		boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
		Assert.assertTrue(!isEmpty);

		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.ticket.requester_id"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.ticket.id"));

	}

	@Test(dataProvider = "contactUsTicket_regression", groups = {
			"regression" }, description = "Regresion test cases to Create Freshdesk ticket")
	public void contactUsTicket_regression(String accessToken, String restaurantId, String subject, String message,
			String category, String city, int statusCode, String statusMessage)
			throws IOException, ProcessingException {
		Processor processor = helper.contactUsTicket(accessToken, restaurantId, subject, message, category, city);
		String resp = processor.ResponseValidator.GetBodyAsText();
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
	}

	@Test(dataProvider = "restauranttoggleonoff", groups = { "smoke",
			"sanity" }, description = "POST/restaurant/slots/v1/toggleStatus: To switch On or Off restaurant for getting orders")
	public void restaurantOnOff(String accessToken, String rest_id11, String isRestaurantOnOff, String source,
			int statusCode, String statusMessage) throws IOException, ProcessingException

	{
		Processor processor = helper.restaurantOnOff(accessToken, rest_id11, isRestaurantOnOff, source);
		String resp = processor.ResponseValidator.GetBodyAsText();
		System.out.println("---------------------------------->" + resp);
		String jsonschema = new ToolBox()
				.readFileAsString(System.getProperty("user.dir") + "/../Data/SchemaSet/Json/VMS/restaurantOnOff");
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		Assert.assertTrue(missingNodeList.isEmpty(),
				missingNodeList + " Nodes Are Missing Or Not Matching For POP Aggreagtor API");
		boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.['restaurantId']"),
				rest_id11);
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.['isOpen']"));
	}

	@Test(dataProvider = "restauranttoggleonoff1", groups = { "smoke",
			"sanity" }, description = "POST/restaurant/slots/v1/toggleStatus: To switch On or Off restaurant for getting orders")
	public void restaurantOnOff(String accessToken, String rest_id11, String isRestaurantOnOff, String source,
			String endDate, int statusCode, String statusMessage) throws IOException, ProcessingException {
		Processor processor = helper.restaurantOnOff(accessToken, rest_id11, isRestaurantOnOff, source, endDate);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.['restaurantId']"),
				rest_id11);
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.['isOpen']"));
		/* Turning restaurant On back again */
		helper.restaurantOnOff(accessToken, rest_id11, "true", source);
	}

	@Test(dataProvider = "restaurantOnOff_regression", groups = {
			"regression" }, description = "POST/restaurant/slots/v1/toggleStatus: Regression TestCases to switch On or Off restaurant for getting orders")
	public void restaurantOnOff_regression(String accessToken, String rest_id11, String isRestaurantOnOff,
			String source, int statusCode, String statusMessage) throws IOException, ProcessingException {
		Processor processor = helper.restaurantOnOff(accessToken, rest_id11, isRestaurantOnOff, source);
		String resp = processor.ResponseValidator.GetBodyAsText();
		System.out.println("---------------------------------->" + resp);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
	}

	@Test(dataProvider = "getRestaurantHolidaySlots", groups = { "sanity",
			"smoke" }, description = "GET/restaurant/slots/v1/holiday: gets restaurant holiday slot for given rest id")
	public void getRestaurantHolidaySlots(String accessToken, String restaurantId, int statusCode, String statusMessage)
			throws Exception {
		if (!RMSCommonHelper.getEnv().equalsIgnoreCase("prod")) {
			/* Switching off the restaurant to set holiday slot */
			helper.restaurantOnOff(accessToken, restaurantId, "false", "VMS");
			/* Switching On the restaurant to revert to original state */
			Processor processor = helper.getRestaurantHolidaySlots(accessToken, restaurantId);
			String resp = processor.ResponseValidator.GetBodyAsText();
			helper.restaurantOnOff(accessToken, restaurantId, "true", "VMS");
			String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/getRestaurantHolidaySlots");
			List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
			Assert.assertTrue(missingNodeList.isEmpty(),
					missingNodeList + " Nodes Are Missing Or Not Matching For getrestaurantHolidaySlot API");
			boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
			Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
			Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
			Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
			Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].restaurantId").toString().replace("[","").replace("]",""),
					restaurantId);
			Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..updatedBy"));
			Assert.assertNotNull(
					processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..updatedBy.meta"));
		} else {
			System.out.println("This test will not run on prod");
		}
	}

	@Test(dataProvider = "getRestaurantHolidaySlots_regression", groups = {
			"regression" }, description = "GET/restaurant/slots/v1/holiday: Regression test cases to get restaurant holiday slot for given rest id")
	public void getRestaurantHolidaySlots_regression(String accessToken, String restaurantId, int statusCode,
			String statusMessage) throws IOException, ProcessingException

	{
		Processor processor = helper.getRestaurantHolidaySlots(accessToken, restaurantId);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
	}

	@Test(dataProvider = "createRestaurantHolidaySlots", groups = { "sanity",
			"smoke" }, description = "creates holiday slots for given restaurant")
	public void createRestaurantHolidaySlots(String accessToken, String restaurantId, String endDate,
			String isRequestedTo, int statusCode, String statusMessage) throws IOException, ProcessingException

	{
		Processor processor = helper.createRestaurantHolidaySlots(accessToken, restaurantId, endDate, isRequestedTo);
		String resp = processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/createRestaurantHolidaySlots");
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		Assert.assertTrue(missingNodeList.isEmpty(),
				missingNodeList + " Nodes Are Missing Or Not Matching For POP Aggreagtor API");
		boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);

	}

	@Test(dataProvider = "createRestaurantHolidaySlots_regression", groups = {
			"regression" }, description = "Regression Test cases to create holiday slots for given restaurant ")
	public void createRestaurantHolidaySlots_regression(String accessToken, String restaurantId, String endDate,
			String isRequestedTo, int statusCode, String statusMessage) throws IOException, ProcessingException

	{
		Processor processor = helper.createRestaurantHolidaySlots(accessToken, restaurantId, endDate, isRequestedTo);

		String resp = processor.ResponseValidator.GetBodyAsText();
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
	}


	@Test(dataProvider = "fetchFeatureGate", groups = { "smoke",
			"sanity" }, description = "POSTfeature/v1/gates/: Fetch Feature gate for given restuarant id")
	public void fetchFeatureGate(String accessToken, String restId, int statusCode, String statusMessage)
			throws IOException, ProcessingException {
		Processor processor = helper.fetchFeatureGate(accessToken, restId);
		String resp = processor.ResponseValidator.GetBodyAsText();
		System.out.println("---------------------------------->" + resp);
		String jsonschema = new ToolBox()
				.readFileAsString(System.getProperty("user.dir") + "/../Data/SchemaSet/Json/VMS/fetchFeatureGate");
        List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		Assert.assertTrue(missingNodeList.isEmpty(),
				missingNodeList + " Nodes Are Missing Or Not Matching For fetchFeatureGate API");
		boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.order_history"));
		Assert.assertNotNull(
				processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.self_serve_menu_edit"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.user_info"));
	}

	@Test(dataProvider = "fetchFeatureGate_regression", groups = {
			"regression" }, description = "POSTfeature/v1/gates/: Regression Test cases to Fetch Feature gate for given restuarant id")
	public void fetchFeatureGate_regression(String accessToken, String restId, int statusCode, String statusMessage)
			throws IOException, ProcessingException {
		Processor processor = helper.fetchFeatureGate(accessToken, restId);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
	}

	// @Test(dataProvider="itemInstock",groups={"sanity","smoke","regression"},
	// description="Mark an item to InStock")
	// public void itemInstock(String accessToken,String markInStock, String itemId,
	// String restaurantId, String hours, String statusMessage) throws IOException,
	// ProcessingException
	// {
	//
	// Processor processor=helper.itemInstock(accessToken,markInStock, itemId,
	// restaurantId, hours);
	// String resp=processor.ResponseValidator.GetBodyAsText();
	//// String jsonschema = new
	// ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/itemInStock.txt");;
	//// List<String>
	// missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
	//// Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are
	// Missing Or Not Matching For POP Aggreagtor API");
	//// boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
	//// System.out.println("Contain empty nodes => " + isEmpty);
	// //String statusCode= JsonPath.read(resp, "$.data.statusCode").toString();
	// //Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
	// Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"),
	// statusMessage);
	// //Assert.assertEquals(statusCode, "1" );
	//
	// }

    /*V1 api is deprecated*/
	@Test(dataProvider = "toggleitemsoos", groups = { "sanity",
			"smoke" }, description = "Toggle On/Off to mark Item OOS", enabled = false)
	public void toggleitemsoos(String accessToken, String markInStock, String item_id, String restaurantId,
			String fromTime, String toTime, String EOD, String hours, int statusCode, String statusMessage)
			throws NumberFormatException, IOException, ProcessingException {
		Processor processor = helper.toggleitemsoos(accessToken, markInStock, item_id, restaurantId, fromTime, toTime,
				EOD, hours);
		String resp = processor.ResponseValidator.GetBodyAsText();
		System.out.println("responseeeeeeeeeeeeee :" + resp);
		String markInStock2 = "1";
		itemInstock(accessToken, markInStock2, item_id, restaurantId, hours, statusMessage);
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/variantInStock");
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		Assert.assertTrue(missingNodeList.isEmpty(),
				missingNodeList + " Nodes Are Missing Or Not Matching For POP Aggreagtor API");
		boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
		// itemInstock1(Integer.parseInt(markInStock2), item_id, restaurantId,
		// Integer.parseInt(hours), statusCode, statusMessage);
	}

	/*V1 api is deprecated*/
	@Test(dataProvider = "toggleitemsoos_regression", groups = {
			"Regression" }, description = "Regression test cases to update order status to - Dispatch", enabled = false)
	public void toggleitemsoos_regression(String accessToken, String markInStock, String item_id, String restaurantId,
			String fromTime, String toTime, String EOD, String hours, int statusCode, String statusMessage) {
		Processor processor = helper.toggleitemsoos(accessToken, markInStock, item_id, restaurantId, fromTime, toTime,
				EOD, hours);
		String resp = processor.ResponseValidator.GetBodyAsText();
		System.out.println("responseeeeeeeeeeeeee :" + resp);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);

	}

	@Test(dataProvider = "getRestUsers", groups = { "sanity",
			"smoke" }, description = "GET/profile/v1/user: Get users of a Restaurant")
	public void getRestUsers(String accessToken, String rest_id, String restUsersInfoMessage)
			throws IOException, ProcessingException {
		Processor processor = helper.getRestUsers(accessToken, rest_id);
		String resp = processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/getRestUsers");
        List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		Assert.assertTrue(missingNodeList.isEmpty(),
				missingNodeList + " Nodes Are Missing Or Not Matching For Get Restaurant Users API");
		boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), restUsersInfoMessage);
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("userDetails"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("userDetails..role_id"));
		Assert.assertNotNull(
				processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("userDetails..restaurants..user_id"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("userDetails..permissions"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("userDetails..is_active"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("userDetails..is_editable"));

	}

	@Test(dataProvider = "getRestUsers1", groups = {
			"regression" }, description = "GET/profile/v1/user: Get users of a Restaurant")
	public void getRestUsersRegression(String accessToken, String rest_id, int expectedStatusCode,
			String expectedStatusMessage) throws IOException, ProcessingException {
		Processor processor = helper.getRestUsers(accessToken, rest_id);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), expectedStatusMessage);
	}

	@Test(dataProvider = "getUserInfo", groups = { "sanity",
			"smoke" }, description = "GET/authentication/v1/userInfo: Get users of a Restaurant")
	public void getUserInfo(String accessToken, String userInfoMessage) throws IOException, ProcessingException {
		Processor processor = helper.getUserInfo(accessToken);
		String resp = processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/getUserInfo");
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		Assert.assertTrue(missingNodeList.isEmpty(),
				missingNodeList + " Nodes Are Missing Or Not Matching For getUserInfo API");
		boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), userInfoMessage);
		Assert.assertNotNull(
				processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("data..restaurants..rest_id"));
		Assert.assertNotNull(
				processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("data..restaurants..rest_name"));
		Assert.assertNotNull(
				processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("data..restaurants..area_id"));
		Assert.assertNotNull(
				processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("data..restaurants..area_name"));
		Assert.assertNotNull(
				processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("data..restaurants..city_id"));
		Assert.assertNotNull(
				processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("data..restaurants..city_name"));
		Assert.assertNotNull(
				processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("data..restaurants..locality"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("data..restaurants..tier"));
		Assert.assertNotNull(
				processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("data..restaurants..assured"));
		Assert.assertNotNull(
				processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("data..restaurants..enabled"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.userRole"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.mobile"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.permissions"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.isLiveOrder"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.isDotIn"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.isCallPartner"));
		Assert.assertNotNull(
				processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.isBillMaskingRestaurant"));

	}

	@Test(dataProvider = "getUserInfoRegression", groups = { "sanity",
			"smoke" }, description = "GET/authentication/v1/userInfo: Regression for Get users of a Restaurant")
	public void getUserInfoRegression(String accessToken, String userInfoMessage)
			throws IOException, ProcessingException {
		Processor processor = helper.getUserInfo(accessToken);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), userInfoMessage);
	}

	@Test(dataProvider = "getRestInfo", groups = { "sanity",
			"smoke" }, description = "GET/profile/v1/restaurantInfo: Get details of a Restaurant")
	public void getRestInfo(String accessToken, String rest_id, String restInfoMessage)
			throws IOException, ProcessingException {

		Processor processor = helper.getRestInfo(accessToken, rest_id);
		String resp = processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/getRestInfo");
        List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		Assert.assertTrue(missingNodeList.isEmpty(),
				missingNodeList + " Nodes Are Missing Or Not Matching For Get Restaurant Info API");
		boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), restInfoMessage);
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("restaurantDetails.name"));
		Assert.assertNotNull(
				processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("restaurantDetails.address"));
		Assert.assertNotNull(
				processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("restaurantDetails.contacts"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("restaurantDetails.active"));

	}

	@Test(dataProvider = "getRestInfo1", groups = {
			"regression" }, description = "GET/profile/v1/restaurantInfo: Get details of a Restaurant")
	public void getRestInfoRegression(String accessToken, String rest_id, int expectedStatusCode,
			String expectedStatusMessage) throws IOException, ProcessingException {

		Processor processor = helper.getRestInfo(accessToken, rest_id);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), expectedStatusMessage);

	}

	@Test(dataProvider = "createUser", groups = { "sanity",
			"smoke" }, description = "POST/profile/v1/user: Add user to a Restaurant")
	public void createUser(String accessToken, int role_id, String userPhone_no, String name, String email,
			String[] permissions, String[] restaurantId, String createUserMessage)
			throws Exception {

		String env = RMSCommonHelper.getEnv();
		int counter = 0;
		boolean flag = false;
		Processor P1 = null;
		if (env.toLowerCase().contains("stage")) {
			while (counter < 5) {
				P1 = helper.createUser(accessToken, role_id, userPhone_no, name, userPhone_no, permissions, restaurantId);
				if (P1.ResponseValidator.GetNodeValue("statusMessage").equals("Phone/email already exists")) {
					counter++;
					userPhone_no = helper.getNextPhoneNumber(userPhone_no);
				} else {
					user_ids.add(helper.getUserByName(name));
					String resp1 = P1.ResponseValidator.GetBodyAsText();
					System.out.println("responseeeeeeeeeeeeee :" + resp1);
					break;
				}
			}
			String resp = P1.RequestValidator.GetBodyAsText();
			String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/createUser");
			List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
			Assert.assertTrue(missingNodeList.isEmpty(),
					missingNodeList + " Nodes Are Missing Or Not Matching For Create User API");
			boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
			System.out.println("Contain empty nodes => " + isEmpty);
			Assert.assertTrue(helper.aggregateResponseAndStatusCodes(P1));
			Assert.assertEquals(P1.ResponseValidator.GetNodeValue("statusMessage"), createUserMessage);

		} else
			System.out.println("This test case will only run on stage");
	}

	@Test(dataProvider = "createUser1", groups = {
			"regression" }, description = "POST/profile/v1/user: Regression for Add user to a Restaurant")
	public void createUserRegression1(String accessToken, int role_id, String phone_no, String name, String email,
			String[] permissions, String[] restaurantId, int expectedStatusCode, String expectedStatusMessage)
			throws IOException, ProcessingException {

		Processor processor = helper.createUser(accessToken, role_id, phone_no, name, email, permissions, restaurantId);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), expectedStatusMessage);

	}

	@Test(dataProvider = "createUser2", groups = {
			"regression" }, description = "POST/profile/v1/user: Regression for Add user to a Restaurant")
	public void createUserRegression2(String accessToken, int role_id, String phone_no, String name, String email,
			String[] permissions, String[] restaurantId, int expectedStatusCode, String expectedStatusMessage)
			throws IOException, ProcessingException {

		Processor processor = helper.createUser2(accessToken, role_id, phone_no, name, email, permissions,
				restaurantId);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), expectedStatusMessage);

	}

	@Test(dataProvider = "updateUser", groups = { "sanity",
			"smoke" }, description = "PUT/profile/v1/user: Update user details of a Restaurant")
	public void updateUser(String accessToken, int updatedRole_id, String role_name, String phone_no,
			String updatedEmail, String updatedName, String[] permissions, String[] restaurantId, int old_role_id,
			String old_email, String[] old_permissions, String[] old_restaurants, String registration_complete,
			String isActive, String updateUserMessage) throws IOException, ProcessingException {
		Processor processor = helper.updateUser(accessToken, updatedRole_id, role_name, phone_no, updatedEmail,
				updatedName, permissions, restaurantId, old_role_id, old_email, old_permissions, old_restaurants,
				registration_complete, isActive);
		String resp = processor.ResponseValidator.GetBodyAsText();
		System.out.println("responseeeeeee: " + resp);
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/updateUser");
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		Assert.assertTrue(missingNodeList.isEmpty(),
				missingNodeList + " Nodes Are Missing Or Not Matching For POP Aggreagtor API");
		boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), updateUserMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("success"), "userDB updated/no sessions");

	}

	@Test(dataProvider = "updateUser1", groups = {
			"regression" }, description = "PUT/profile/v1/user: Update user details of a Restaurant")
	public void updateUserRegression(String accessToken, int updatedRole_id, String role_name, String phone_no,
			String updatedEmail, String updatedName, String[] permissions, String[] restaurantId, int old_role_id,
			String old_email, String[] old_permissions, String[] old_restaurants, String registration_complete,
			String isActive, int expectedStatusCode, String expectedStatusMessage)
			throws IOException, ProcessingException {
		Processor processor = helper.updateUser(accessToken, updatedRole_id, role_name, phone_no, updatedEmail,
				updatedName, permissions, restaurantId, old_role_id, old_email, old_permissions, old_restaurants,
				registration_complete, isActive);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), expectedStatusMessage);
	}

	@Test(dataProvider = "disableEnableUser", groups = { "sanity",
			"smoke" }, description = "PUT/profile/v1/disable: Disable user of a Restaurant")
	public void disableEnableUser(String accessToken, String phone_no, String is_enable, String disableUserMessage)
			throws IOException, ProcessingException {
		Processor processor = helper.disableUser(accessToken, phone_no, is_enable);
		String resp = processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/disableUser");
        List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		Assert.assertTrue(missingNodeList.isEmpty(),
				missingNodeList + " Nodes Are Missing Or Not Matching Disable User API");
		boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), disableUserMessage);
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.success..id"));
	}

	@Test(dataProvider = "disableEnableUserRegression", groups = {
			"regression" }, description = "PUT/profile/v1/disable: Disable user of a Restaurant")
	public void disableEnableUserRegression(String accessToken, String phone_no, String is_enable,
			int expectedStatusCode, String expectedStatusMessage, String expectedNodeValue)
			throws IOException, ProcessingException {
		Processor processor = helper.disableUser(accessToken, phone_no, is_enable);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), expectedStatusMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.success"),
				expectedNodeValue);
	}

	@Test(dataProvider = "fetchItems", groups = { "sanity", "smoke" }, description = "Get details of a Restaurant")
	public void fetchItems(String accessToken, String restaurantId, String fetchItemsMessage, boolean check)
			throws IOException, ProcessingException, SwiggyAPIAutomationException {
		Processor processor = helper.fetchItems(accessToken, restaurantId);
		String resp = processor.ResponseValidator.GetBodyAsText();
		if (check) {
			JsonNode node = APIUtils.convertStringtoJSON(resp);
			String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/fecthItems");
			List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
			Assert.assertTrue(missingNodeList.isEmpty(),
					missingNodeList + " Nodes Are Missing Or Not Matching For POP Aggreagtor API");
			boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
			System.out.println("Contain empty nodes => " + isEmpty);
			String actualRestId = JsonPath.read(resp, "$.data.id").toString();
			String categoriesId = JsonPath.read(resp, "$.data.categories..id").toString();
			Assert.assertNotNull(actualRestId);
			Assert.assertEquals(actualRestId, restaurantId);
			Assert.assertNotNull(categoriesId);
		}
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), fetchItemsMessage);

	}

	@Test(dataProvider = "fetchItems1", groups = { "regression" }, description = "Get menu details of a Restaurant")
	public void fetchItems1(String accessToken, String restaurantId, int expectedStatusCode,
			String expectedStatusMessage) throws IOException, ProcessingException, SwiggyAPIAutomationException {
		Processor processor = helper.fetchItems(accessToken, restaurantId);
		String resp = processor.ResponseValidator.GetBodyAsText();
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), expectedStatusMessage);

	}

	/*v1 api is deprecated*/
	@Test(dataProvider = "itemOOS", groups = { "sanity", "smoke" }, description = "Mark an item to OOS", enabled=false)
	public void itemOOS(String accessToken, int markOOStock, String itemId, String restaurantId, String hours,
			String itemOOSMessage) throws Exception {
		Processor processor = helper.itemOOS(accessToken, markOOStock, itemId, restaurantId, hours);
		String resp = processor.ResponseValidator.GetBodyAsText();
		helper.itemInstock(accessToken, "1", itemId, restaurantId, hours);
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/itemOOS");
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		Assert.assertTrue(missingNodeList.isEmpty(),
				missingNodeList + " Nodes Are Missing Or Not Matching For POP Aggreagtor API");
		String actulaItemId = JsonPath.read(resp, "$.data.item_id").toString();
		String updatedSource = JsonPath.read(resp, "$.data.updated_by.source").toString();
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), itemOOSMessage);
		Assert.assertEquals(actulaItemId, itemId);
		Assert.assertEquals(updatedSource, "VENDOR");
		Assert.assertNotNull(JsonPath.read(resp, "$.data.id"));
		Assert.assertNotNull(JsonPath.read(resp, "$.data.uniqueId"));
	}

	@Test(dataProvider = "itemOOS1", groups = { "regression" }, description = "Mark an item to OOS")
	public void itemOOS1(String accessToken, int markOOStock, String itemId, String restaurantId, String hours,
			int expectedStatusCode, String expectedStatusMessage) throws Exception {
		Processor processor = helper.itemOOS(accessToken, markOOStock, itemId, restaurantId, hours);
		String resp = processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/itemOOS");
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		Assert.assertTrue(missingNodeList.isEmpty(),
				missingNodeList + " Nodes Are Missing Or Not Matching For POP Aggreagtor API");
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), expectedStatusMessage);
	}

	/*V1 api is deprecated*/
	@Test(dataProvider = "itemInstock", groups = { "sanity", "smoke" }, description = "Mark an item to InStock", enabled=false)
	public void itemInstock(String accessToken, String markInStock, String itemId, String restaurantId, String hours,
			String itemInstockMessage) throws IOException, ProcessingException {

		Processor processor = helper.itemInstock(accessToken, markInStock, itemId, restaurantId, hours);
		String resp = processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/itemInStock");
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		Assert.assertTrue(missingNodeList.isEmpty(),
				missingNodeList + " Nodes Are Missing Or Not Matching For POP Aggreagtor API");
		String statusCode = JsonPath.read(resp, "$.data.statusCode").toString();
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), itemInstockMessage);
		Assert.assertEquals(statusCode, "1");

	}

	@Test(dataProvider = "itemInstock1", groups = { "regression" }, description = "Mark an item to InStock")
	public void itemInstock1(String accessToken, String markInStock, String itemId, String restaurantId, String hours,
			int expectedStatusCode, String expectedStatusMessage) throws IOException, ProcessingException {

		Processor processor = helper.itemInstock(accessToken, markInStock, itemId, restaurantId, hours);
		String resp = processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/itemInStock");
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		Assert.assertTrue(missingNodeList.isEmpty(),
				missingNodeList + " Nodes Are Missing Or Not Matching For POP Aggreagtor API");
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), expectedStatusMessage);
	}

	@Test(dataProvider = "itemOOSV2", groups = { "sanity", "smoke" }, description = "Mark an item to OOS")
	public void itemOOSV2(String accessToken, int markOOStock, String itemId, String restaurantId, String hours,
						String itemOOSMessage) throws Exception {
		Processor processor = helper.itemOOSV2(accessToken, markOOStock, itemId, restaurantId, hours);
		String resp = processor.ResponseValidator.GetBodyAsText();
		helper.itemOOSV2(accessToken, 1, itemId, restaurantId, hours);
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/itemOOSV2");
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		Assert.assertTrue(missingNodeList.isEmpty(),
				missingNodeList + " Nodes Are Missing Or Not Matching For POP Aggreagtor API");
		String actulaItemId = JsonPath.read(resp, "$.data.data.item_id").toString();
		String updatedSource = JsonPath.read(resp, "$.data.data.updated_by.source").toString();
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), itemOOSMessage);
		Assert.assertEquals(actulaItemId, itemId);
		Assert.assertEquals(updatedSource, "VENDOR");
		Assert.assertNotNull(JsonPath.read(resp, "$.data.data.id"));
		Assert.assertNotNull(JsonPath.read(resp, "$.data.data.uniqueId"));
	}

	@Test(dataProvider = "itemOOS1V2", groups = { "regression" }, description = "Mark an item to OOS")
	public void itemOOS1V2(String accessToken, int markOOStock, String itemId, String restaurantId, String hours,
						 int expectedStatusCode, String expectedStatusMessage) throws Exception {
		Processor processor = helper.itemOOSV2(accessToken, markOOStock, itemId, restaurantId, hours);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), expectedStatusMessage);
		if(processor.ResponseValidator.DoesNodeExists("$.data")) {
			Assert.assertEquals(processor.ResponseValidator.GetNodeValue("$.data.data.statusMessage"), "Invalid item holiday slot request!");
		}
	}

	/*V1 api is deprecated*/
	@Test(dataProvider = "variantOOS", groups = { "sanity", "smoke" }, description = "Mark Variant to OOS", enabled = false)
	public void variantOOS(String accessToken, int markOOStock, String variantItemIdOos, String variantId,
			String variantGroupId, String restaurantId, String variantOOSMessage)
			throws IOException, ProcessingException {

		Processor processor = helper.variantOOS(accessToken, markOOStock, variantItemIdOos, variantId, variantGroupId,
				restaurantId);
		String resp = processor.ResponseValidator.GetBodyAsText();
		helper.variantInStock(accessToken, "1", variantItemIdOos, variantId, variantGroupId,
				restaurantId);
		 String jsonschema = new
		 ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/variantOOStock");
        List<String>
		 missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
		 Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For POP Aggreagtor API");
		 boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
		 System.out.println("Contain empty nodes => " + isEmpty);
		 Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		 Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), variantOOSMessage);

	}

	@Test(dataProvider = "variantOOS1", groups = { "regression" }, description = "Mark Variant to OOS", enabled = true)
	public void variantOOS1(String accessToken, int markOOStock, String variantItemIdOos, String variantId,
			String variantGroupId, String restaurantId, int expectedStatusCode, String expectedStatusMessage)
			throws IOException, ProcessingException {

		Processor processor = helper.variantOOS(accessToken, markOOStock, variantItemIdOos, variantId, variantGroupId,
				restaurantId);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), expectedStatusMessage);

	}

	@Test(dataProvider = "variantOOSV2", groups = { "sanity", "smoke" }, description = "Mark Variant to OOS")
	public void variantOOSV2(String accessToken, int markOOStock, String variantItemIdOos, String variantId,
						   String variantGroupId, String restaurantId, String variantOOSMessage)
			throws IOException, ProcessingException {

		Processor processor = helper.variantOOSV2(accessToken, markOOStock, variantItemIdOos, variantId, variantGroupId,
				restaurantId);
		String resp = processor.ResponseValidator.GetBodyAsText();
		helper.variantOOSV2(accessToken, 1, variantItemIdOos, variantId, variantGroupId,
				restaurantId);
		String jsonschema = new
				ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/variantOOStockV2");
        List<String>
				missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
		Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For POP Aggreagtor API");
		boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), variantOOSMessage);

	}

	@Test(dataProvider = "variantOOS1V2", groups = { "regression" }, description = "Mark Variant to OOS")
	public void variantOOS1V2(String accessToken, int markOOStock, String variantItemIdOos, String variantId,
							String variantGroupId, String restaurantId, int expectedStatusCode, String expectedStatusMessage)
			throws IOException, ProcessingException {

		Processor processor = helper.variantOOSV2(accessToken, markOOStock, variantItemIdOos, variantId, variantGroupId,
				restaurantId);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), expectedStatusMessage);

	}

	/*V1 api is deprecated*/
	@Test(dataProvider = "variantInStock", groups = { "sanity", "smoke" }, description = "Mark Variant to InStock", enabled = false)
	public void variantInStock(String accessToken, String markInStock, String variantItemIdOos, String variantId,
			String variantGroupId, String restaurantId, String variantInStockMessage)
			throws IOException, ProcessingException {

		Processor processor = helper.variantInStock(accessToken, markInStock, variantItemIdOos, variantId,
				variantGroupId, restaurantId);
		String resp = processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/variantInStock");
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		Assert.assertTrue(missingNodeList.isEmpty(),
				missingNodeList + " Nodes Are Missing Or Not Matching For POP Aggreagtor API");
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), variantInStockMessage);

	}

	/*V1 api is deprecated*/
	@Test(dataProvider = "variantInStock1", groups = { "regression" }, description = "Mark Variant to InStock", enabled = true)
	public void variantInStock1(String accessToken, String markInStock, String variantItemIdOos, String variantId,
			String variantGroupId, String restaurantId, int expectedStatusCode, String expectedStatusMessage)
			throws IOException, ProcessingException {

		Processor processor = helper.variantInStock(accessToken, markInStock, variantItemIdOos, variantId,
				variantGroupId, restaurantId);
		String resp = processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/variantInStock");
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		Assert.assertTrue(missingNodeList.isEmpty(),
				missingNodeList + " Nodes Are Missing Or Not Matching For POP Aggreagtor API");
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), expectedStatusMessage);

	}


	@Test(dataProvider = "fetch_Broadcast_Info_DP", groups = { "sanity",
			"smoke", }, description = "GET/broadcast/v1/data: fetch_Broadcast_Info")
	public void fetch_Broadcast_Info(String accessToken, String restaurantId, String FBI_statusMessage)
			throws IOException, ProcessingException {

		RMSHelper helper = new RMSHelper();
		SchemaValidatorUtils schemaValidatorUtils = new SchemaValidatorUtils();
		Processor processor = helper.fetchBroadcastInfo(accessToken, restaurantId);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), FBI_statusMessage);

	}

	@Test(dataProvider = "fetch_Broadcast_Info_DP1", groups = {
			"regression" }, description = "GET/broadcast/v1/data: fetch_Broadcast_Info_DP regression use cases")
	public void fetch_Broadcast_Info__Regression(String accessToken, String restaurantId, int expectedStatusCode,
												 String expectedStatusMessage) throws IOException, ProcessingException {

		RMSHelper helper = new RMSHelper();

		Processor processor = helper.fetchBroadcastInfo(accessToken, restaurantId);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), expectedStatusCode);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), expectedStatusMessage);
	}

	// run in prod
	@Test(dataProvider = "restaurant_Tier_mertics_of_Restaurant_DP", groups = { "sanity",
			"smoke" },  description = "GETsellerTier/v1/metric: get restaurant Tier mertics")
	public void restaurantTierMerticsOfRestaurant(String accessToken, String restaurantId, String RTMR_statusMessage)
			throws IOException, ProcessingException {

		RMSHelper helper = new RMSHelper();
		SchemaValidatorUtils schemaValidatorUtils = new SchemaValidatorUtils();
		Processor processor = helper.restaurantTierMerticsOfRestaurant(accessToken, restaurantId);
		String resp = processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/restaurantTierMatrix");
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		Assert.assertTrue(missingNodeList.isEmpty(),
				missingNodeList + " Nodes Are Missing Or Not Matching For restaurantTier API");
		boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), RTMR_statusMessage);
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.rating"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tier"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.assured"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cityCode"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restName"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.acceptanceRate"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cancellationRate"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.editRate"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.totalRevenue"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.startDate"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.endDate"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurantId"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.goLiveDate"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.refreshDate"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.salesCutoff"));

	}

	@Test(dataProvider = "restaurant_Tier_mertics_of_Restaurant_DP1", groups = {
			"regression" },  description = "GETsellerTier/v1/metric: restaurant_Tier_mertics_of_Restaurant regression use cases")
	public void restaurantTierMerticsOfRestaurant1(String accessToken, String restaurantId, int expectedStatusCode,
			String expectedStatusMessage) {

		RMSHelper helper = new RMSHelper();

		Processor processor = helper.restaurantTierMerticsOfRestaurant(accessToken, restaurantId);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), expectedStatusCode);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), expectedStatusMessage);
	}

	@Test(dataProvider = "restaurant_Tier_Benifits_For_All_Tiers_DP", groups = { "sanity",
			"smoke" },  description = "GETsellerTier/v1/tiers: restaurant Tier Benifits For All Tiers")
	public void restaurantTierBenifitsForAllTiers(String accessToken, String cityId, String restId, String RTMR_statusMessage)
			throws IOException, ProcessingException {

		RMSHelper helper = new RMSHelper();

		Processor processor = helper.restaurantTierBenifitsForAllTiers(accessToken, cityId, restId);
		String resp = processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/restaurantTierInfo");
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		Assert.assertTrue(missingNodeList.isEmpty(),
				missingNodeList + " Nodes Are Missing Or Not Matching For restaurantTierInfo API");
		boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), RTMR_statusMessage);
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].tier.name"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].tier.key"));
		Assert.assertNotNull(
				processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].tier.displayName"));
		Assert.assertNotNull(processor.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data[0].metrics.[0].displayableValue"));
		Assert.assertNotNull(
				processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].metrics.[0].key"));
		Assert.assertNotNull(
				processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].metrics.[0].name"));
		Assert.assertNotNull(
				processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].metrics.[0].type"));
		Assert.assertNotNull(
				processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].metrics.[0].displayOrder"));
		Assert.assertNotNull(
				processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].metrics.[0].isDisplayable"));

		Assert.assertNotNull(
				processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].benefits.[0].imageUrl"));
		Assert.assertNotNull(
				processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].benefits.[0].attachment"));
		Assert.assertNotNull(
				processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].benefits.[0].key"));
		Assert.assertNotNull(
				processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].benefits.[0].name"));
		Assert.assertNotNull(
				processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].benefits.[0].type"));
		Assert.assertNotNull(
				processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].benefits.[0].displayOrder"));
		Assert.assertNotNull(
				processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].benefits.[0].isDisplayable"));
		Assert.assertNotNull(
				processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].benefits.[0].active"));
		Assert.assertNotNull(
				processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].benefits.[0].description"));

	}

	@Test(dataProvider = "restaurant_Tier_Benifits_For_All_Tiers_DP1", groups = {
			"regression" },  description = "GETsellerTier/v1/tiers: restaurant_Tier_Benifits_For_All_Tiers_DP regression use cases")
	public void restaurantTierBenifitsForAllTiers1(String accessToken, String cityId, String restId, int expectedStatusCode,
			String expectedStatusMessage) {

		RMSHelper helper = new RMSHelper();

		Processor processor = helper.restaurantTierBenifitsForAllTiers(accessToken, cityId, restId);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), expectedStatusCode);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), expectedStatusMessage);
	}


	@Test(dataProvider = "registerAndMapUser", groups = { "sanity",
			"smoke" }, description = "Register a new user and map to a created restaurant")
	public void registerAndMapUser(String accessToken, String userPhone_no, String email, String name, String password,
			String confirmPassword, int role_id, String[] permissions, String[] restaurantId, String statusMessage,
			int statusCode, String signUpMessage) throws Exception {

		boolean flag = false;
		int counter = 0;
		while (!flag && counter < 5) {
			Processor P1 = helper.createUser(accessToken, role_id, userPhone_no, name, email, permissions,
					restaurantId);
			if (P1.ResponseValidator.GetNodeValue("statusMessage").equals("Phone Number already exists")) {
				flag = false;
				counter++;
				userPhone_no = helper.getNextPhoneNumber(userPhone_no);
			} else {
				flag = true;
				String resp1 = P1.ResponseValidator.GetBodyAsText();
				System.out.println("responseeeeeeeeeeeeee :" + resp1);
				break;
			}
		}

		Thread.sleep(5000);
		Processor processor = helper.registerUser(accessToken, userPhone_no, email, name, password,
				confirmPassword);
		String resp = processor.ResponseValidator.GetBodyAsText();
		System.out.println("responseeeeeeeeeeeeee :" + resp);

		String P2 = helper.getOTP(userPhone_no);
		System.out.println("responseeeeeeeeeeeeee :" + P2);

		// Processor P3 = helper.confirmOTP(userPhone_no, "123456");
		Processor P3 = helper.confirmOTP(userPhone_no, helper.getOTP(userPhone_no));

		Assert.assertEquals(P3.ResponseValidator.GetNodeValue("statusMessage"), signUpMessage);
		Assert.assertEquals(P3.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
	}

	@Test(dataProvider="registrationStatus",groups={"sanity","smoke","regression"},description="User can registered")
	public void registration_Status(String mobileno,String statusMessage) throws IOException, ProcessingException
	{

		Processor processor = helper.registration(mobileno);
		String resp=processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/registration_VI");
		List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
		Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching  API");
		boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		System.out.println("Valid Message: "+processor.ResponseValidator.GetNodeValue("statusMessage"));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
	}


	@Test(dataProvider = "billMaskingDP", groups = { "sanity", "smoke" }, description = "Bill_Masking")
	public void billMasking(String accessToken) {

		Processor processor = helper.getBillMasking(accessToken);

		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), "success");
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.billMasking"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.liveOrder"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.dotIn"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.callPartner"));

	}

	@Test(dataProvider = "changePasswordRegression", groups = {
			"regression" }, description = "POST/authentication/v1/changepassword: change Password")
	public void changePasswordRegression(String accessToken, String username, String oldpwd, String newpwd,
			int statusCode, String statusMessage) {

		/* No need for schema validation */
		Processor processor = helper.changepassword(accessToken, username, oldpwd, newpwd);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
	}

	@Test(dataProvider = "getDispositionsDp")
	public void getDispositionsTest(String cities, String restaurantId, String token, boolean check) {
		Processor processor = helper.getDispositions(token, cities, restaurantId);
		if (check)
			Assert.assertTrue(schemaValidatorUtils.validateSchema(processor, "/../Data/SchemaSet/Json/VMS/dispositions"), "Nodes Are Missing Or Not Matching For Slotwise Prep Time API");
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
	}

	@Test(dataProvider = "getVariantStatusV2", groups = {
			"smoke" , "sanity" }, description = "GET get /variants/v2/toggleStatus")
	public void getVariantStatusV2(String accessToken, String variantId, String variantGroupId, String itemId,
										 String restaurantId, String statusMessage) throws IOException, ProcessingException{

		Processor processor = helper.getVariantV2(accessToken, variantId, variantGroupId, itemId, restaurantId);
		String resp=processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/getVariantV2");
		List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
		Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching  API");
		boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.variant_in_stock_status"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.item_in_stock_status"));

	}

	@Test(dataProvider = "getVariantStatusV2Regression", groups = {
			"regression" }, description = "GET get /variants/v2/toggleStatus")
	public void getVariantStatusV2Regression(String accessToken, String variantId, String variantGroupId, String itemId,
								   String restaurantId, String statusMessage) throws IOException, ProcessingException{

		Processor processor = helper.getVariantV2(accessToken, variantId, variantGroupId, itemId, restaurantId);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);

	}

	/*V1 is deprecated*/
	@Test(dataProvider = "getVariantStatus", groups = {
			"smoke" , "sanity" }, description = "GET get /variants/v1/toggleStatus", enabled = false)
	public void getVariantStatus(String accessToken, String variantId, String variantGroupId, String itemId,
								   String restaurantId, String statusMessage) throws IOException, ProcessingException{

		Processor processor = helper.getVariant(accessToken, variantId, variantGroupId, itemId, restaurantId);
		String resp=processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/getVariant");
		List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
		Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching  API");
		boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.variant_in_stock_status"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.item_in_stock_status"));

	}

	/*V1 is deprecated*/
	@Test(dataProvider = "getVariantStatusRegression", groups = {
			"regression" }, description = "GET get /variants/v1/toggleStatus", enabled = true)
	public void getVariantStatusRegression(String accessToken, String variantId, String variantGroupId, String itemId,
											 String restaurantId, String statusMessage) throws IOException, ProcessingException{

		Processor processor = helper.getVariant(accessToken, variantId, variantGroupId, itemId, restaurantId);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);

	}

	@Test(dataProvider = "fetchAlternatives", groups = {
			"smoke" , "sanity" }, description = "GET /restaurant/v1/fetchAlternatives")
	public void fetchAlternatives(String accessToken, String restaurantId,
								  String statusMessage) throws IOException, ProcessingException{
		Processor p= helper.fetchItems(accessToken, restaurantId);
		List<Integer> list = RMSCommonHelper.getItemListFromFetchItems(p);
		if(list.size() == 0)
			Assert.fail("Menu is empty");
		Processor processor = helper.getAlternatives(accessToken, list.get(0).toString(), restaurantId);
		String resp=processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/getAlternatives");
		List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
		Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching  API");
		boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].name"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].id"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].is_veg"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].price"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].inStock"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].categoryID"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].restaurantId"));

	}

	@Test(dataProvider = "fetchAlternativesRegression", groups = {
			"regression"}, description = "GET /restaurant/v1/fetchAlternatives")
	public void fetchAlternativesRegression(String accessToken, String itemId, String restaurantId,
											String statusMessage) {

		Processor processor = helper.getAlternatives(accessToken, itemId, restaurantId);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);

	}

	@Test(dataProvider = "fetchAlternativesV2", groups = {
			"smoke" , "sanity" }, description = "GET /restaurant/v2/fetchAlternatives")
	public void fetchAlternativesV2(String accessToken, String restaurantId,
									String statusMessage) throws IOException, ProcessingException{

		Processor processor = helper.getAlternativesV2(accessToken, restaurantId);
		String resp=processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/getAlternativesV2");
		List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
		Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching  API");
		boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.name"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.categories"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.enabled"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.menuId"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.area"));

	}

	@Test(dataProvider = "fetchAlternativesV2Regression", groups = {
			"regression" }, description = "GET /restaurant/v2/fetchAlternatives")
	public void fetchAlternativesV2Regression(String accessToken, String restaurantId,
											  String statusMessage) {

		Processor processor = helper.getAlternativesV2(accessToken, restaurantId);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
	}


	@Test(dataProvider = "getInvoiceDocument", groups = {
			"smoke" , "sanity" }, description = "GET /v1/invoice/documents")
	public void getInvoiceDocument(String accessToken, String restaurantId, String fromDate, String toDate,
								   String statusMessage) throws IOException, ProcessingException{

		Processor processor = helper.getInvoiceDocument(accessToken, restaurantId, fromDate, toDate);
		String resp=processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/getInvoiceDocument");
		List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
		Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching  API");
		boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.documents.[0].invoice_id"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.documents.[0].invoice_url"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.documents.[0].annexure_url"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.documents.[0].from_date"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.documents.[0].to_date"));

	}

	@Test(dataProvider = "getInvoiceDocumentRegression", groups = {
			"regression" }, description = "GET v1/invoice/documents")
	public void getInvoiceDocumentRegression(String accessToken, String restaurantId, String fromDate, String toDate,
								   String statusMessage) throws IOException, ProcessingException{

		Processor processor = helper.getInvoiceDocument(accessToken, restaurantId, fromDate, toDate);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);

	}

	@Test(dataProvider = "getNonPollingRestaurants", groups = {
            "smoke" , "sanity" }, description = "GET restaurant/v1/fetchNonPollingRestaurants")
    public void getNonPollingRestaurants(String interval, String statusMessage) {

        Processor processor = helper.getNonPollingRestaurants(interval);
        Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
        Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);

    }

    @Test(dataProvider = "getTDEnabledCities", groups = {
            "smoke" , "sanity" }, description = "POST /td/v1/cities")
    public void getTDEnabledCities(String accessToken, String restaurantId, String statusMessage) {

        Processor processor = helper.getTDEnabledCities(accessToken, restaurantId);
        Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
        Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
        if(processor.ResponseValidator.DoesNodeExists("$.data.GetNodeValue")) {
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValue("$.data.GetNodeValue.[0]"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValue("$.data.cityMap.[1]"));
        }

    }

	@Test(dataProvider = "getRestInfoById", groups = {
			"smoke" , "sanity" }, description = "POST /restaurant/v1/getRestaurants/")
	public void getRestInfoById(String accessToken, String restaurantId, String statusMessage) {

		Processor processor = helper.getRestInfoById(accessToken, restaurantId);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].rest_id"), restaurantId);
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].enabled"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValue("$.data.[0].rest_name"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].area_id"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValue("$.data.[0].area_name"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].city_id"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValue("$.data.[0].city_name"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValue("$.data.[0].locality"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValue("$.data.[0].tier"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].assured"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].rating"));

	}

	@Test(dataProvider = "getRestInfoByIdRegression", groups = {
			"regression"}, description = "POST /restaurant/v1/getRestaurants/")
	public void getRestInfoByIdRegression(String accessToken, String restaurantId, String statusMessage) {

		Processor processor = helper.getRestInfoById(accessToken, restaurantId);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);

	}

    @Test(dataProvider = "getGSTFeatureFlag", groups = {
            "smoke" , "sanity" }, description = "GET /v1/invoice/documents")
    public void getGSTFeatureFlag(String accessToken, int statusCode, String statusMessage) {

        Processor processor = helper.getGSTFeatureFlag(accessToken);
        Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
        Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
        if(processor.ResponseValidator.DoesNodeExists("$.data.is_gst_notification_on"))
        {
            if(processor.ResponseValidator.GetNodeValueAsBool("$.data.is_gst_notification_on"))
            {
                Assert.assertEquals(processor.ResponseValidator.GetNodeValue("$.data.lastDate"),
                        "Last date to enroll for GST is 30/06/2017");
                Assert.assertEquals(processor.ResponseValidator.GetNodeValue("$.data.gst_sw_info_link"),
                        "https://drive.google.com/file/d/0B_ctaMgW_SrUQ1FkMVFHeUlEc1U/view?usp=sharing");
                Assert.assertEquals(processor.ResponseValidator.GetNodeValue("$.data.notification_content"),
                        "Please update your password every 90 days to keep your account secure");
                Assert.assertEquals(processor.ResponseValidator.GetNodeValue("$.data.compostite_tax_help"),
                        "Restaurants whose aggregate turnover does not exceed Rs. 1 crore at any time during the " +
                                "Financial Year and has opted to be registered under Composition Tax Scheme with the govt. , " +
                                "paying GST @5%.");
                Assert.assertEquals(processor.ResponseValidator.GetNodeValue("$.data.title"),
                        "IMPORTANT UPDATE");
                String message = processor.ResponseValidator.GetNodeValue("$.data.message");
                message.equals("As per Govt regulations, e-commerce operators such as Swiggy are required to comply " +
                                "with 'Tax Collected at Source' ('TCS') provisions under GST laws, effective October 1, " +
                                "2018.\\n\\nTo continue doing business with Swiggy, A Restaurant is required to either " +
                                "submitting their GST details or confirm that their turnover is less than GST " +
                                "threshold  (Rs. 20 lakhs).\\n\\nLast date to submit your GST details is 30 Sept 2018.");
            }
        }

    }

    @Test(dataProvider = "getGSTStaticInfo", groups = {
            "smoke" , "sanity" }, description = "GET /v1/invoice/documents")
    public void getGSTStaticInfo(String accessToken, int statusCode, String statusMessage) {

        Processor processor = helper.getGSTStaticInfo(accessToken);
        Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
        Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
        Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.is_gst_notification_on"));
        Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.is_gst_on_for_web"));
        Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.is_gst_on_for_mobile"));

    }

    @Test(dataProvider = "getGSTDetails", groups = {
            "smoke" , "sanity" }, description = "GET /v1/invoice/documents", enabled = false)
    public void getGSTDetails(String accessToken, String restaurantId ,int statusCode, String statusMessage) {

        Processor processor = helper.getGSTDetails(accessToken, restaurantId);
        Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
        Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
        Assert.assertEquals(processor.ResponseValidator.GetNodeValue("$.data.restaurant_id"), restaurantId);
        if(processor.ResponseValidator.DoesNodeExists("$.data.is_gst_notification_on"))
        {
            if(processor.ResponseValidator.GetNodeValueAsBool("$.data.is_gst_notification_on"))
            {
                Assert.assertEquals(processor.ResponseValidator.GetNodeValue("$.data.lastDate"),
                        "Last date to enroll for GST is 30/06/2017");
                Assert.assertEquals(processor.ResponseValidator.GetNodeValue("$.data.gst_sw_info_link"),
                        "https://drive.google.com/file/d/0B_ctaMgW_SrUQ1FkMVFHeUlEc1U/view?usp=sharing");
                Assert.assertEquals(processor.ResponseValidator.GetNodeValue("$.data.notification_content"),
                        "Please update your password every 90 days to keep your account secure");
                Assert.assertEquals(processor.ResponseValidator.GetNodeValue("$.data.compostite_tax_help"),
                        "Restaurants whose aggregate turnover does not exceed Rs. 1 crore at any time during the " +
                                "Financial Year and has opted to be registered under Composition Tax Scheme with the govt. , " +
                                "paying GST @5%.");
                Assert.assertEquals(processor.ResponseValidator.GetNodeValue("$.data.title"),
                        "IMPORTANT UPDATE");
                String message = processor.ResponseValidator.GetNodeValue("$.data.message");
                message.equals("As per Govt regulations, e-commerce operators such as Swiggy are required to comply " +
                        "with 'Tax Collected at Source' ('TCS') provisions under GST laws, effective October 1, " +
                        "2018.\\n\\nTo continue doing business with Swiggy, A Restaurant is required to either " +
                        "submitting their GST details or confirm that their turnover is less than GST " +
                        "threshold  (Rs. 20 lakhs).\\n\\nLast date to submit your GST details is 30 Sept 2018.");
            }
        }

    }

	@Test(dataProvider="registrationStatusV2",groups={"sanity","smoke","regression"},description="User can registered")
	public void registration_StatusV2(String userId, String statusMessage) throws IOException, ProcessingException
	{

		Processor processor = helper.registrationV2(userId);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
	}

	@Test(dataProvider = "getRestaurantInfo", groups = {
			"smoke" , "sanity" }, description = "POST /restaurant/v1/getRestaurant/")
	public void getRestaurantInfo(String accessToken, String restaurantId, String statusMessage) {

		Processor processor = helper.getRestaurantInfo(accessToken, restaurantId);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);

	}

	@Test(dataProvider = "getRestaurantInfoRegression", groups = {
			"regression"}, description = "POST /restaurant/v1/getRestaurants/")
	public void getRestaurantInfoRegression(String accessToken, String restaurantId, String statusMessage) {

		Processor processor = helper.getRestaurantInfo(accessToken, restaurantId);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);

	}

	@Test(dataProvider = "getUsers", groups = {
			"smoke" , "sanity" }, description = "GET /profile/v1/allUsers/")
	public void getUsers(String rest_id, String user_id, String email,
						 String mobile_no, String name, String limit, String statusMessage) {

		Processor processor = helper.getUsers(rest_id, user_id, email, mobile_no, name, limit);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), 1);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		List<Integer> list = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.users..user_id");
		System.out.println(list);
		boolean isUserId = false;
		for(int i=0; i<list.size(); i++) {
			System.out.println(list.get(i));
			if(list.get(i) == null)
				i++;
			if(list.get(i) == Integer.parseInt(rest_id)) {
				isUserId = true;
				break;
			}
		}
		Assert.assertTrue(isUserId , "User returned are for same restaurant");
	}

	@Test(dataProvider = "getUsersLimit", groups = {
			"smoke" , "sanity" }, description = "GET /profile/v1/allUsers/")
	public void getUsersLimit(String rest_id, String user_id, String email,
						 String mobile_no, String name, String limit, String statusMessage) {

		Processor processor = helper.getUsers(rest_id, user_id, email, mobile_no, name, limit);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), 1);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("$.data.meta.limit"), "1");
		List<Integer> list = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.users..id");
		Assert.assertEquals(list.size() , 1);

	}

	@Test(dataProvider = "getUsersbyUserId", groups = {
			"smoke" , "sanity" }, description = "GET /profile/v1/allUsers/")
	public void getUsersbyUserId(String rest_id, String user_id, String email,
							  String mobile_no, String name, String limit, String statusMessage) {

		Processor processor = helper.getUsers(rest_id, user_id, email, mobile_no, name, limit);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), 1);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("$.data.users.[0].user_id"), 5271);

	}

	@Test(dataProvider = "getUsersbyEmail", groups = {
			"smoke" , "sanity" }, description = "GET /profile/v1/allUsers/")
	public void getUsersbyEmail(String rest_id, String user_id, String email,
								 String mobile_no, String name, String limit, String statusMessage) {

		Processor processor = helper.getUsers(rest_id, user_id, email, mobile_no, name, limit);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), 1);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("$.data.users.[0].email"), email);
	}

	@Test(dataProvider = "getUsersbyMobile", groups = {
			"smoke" , "sanity" }, description = "GET /profile/v1/allUsers/")
	public void getUsersbyMobile(String rest_id, String user_id, String email,
								String mobile_no, String name, String limit, String statusMessage) {

		Processor processor = helper.getUsers(rest_id, user_id, email, mobile_no, name, limit);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), 1);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("$.data.users.[0].mobile_no"), mobile_no);
	}

	@Test(dataProvider = "getUsersbyName", groups = {
			"smoke" , "sanity" }, description = "GET /profile/v1/allUsers/")
	public void getUsersbyName(String rest_id, String user_id, String email,
								 String mobile_no, String name, String limit, String statusMessage) {

		Processor processor = helper.getUsers(rest_id, user_id, email, mobile_no, name, limit);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), 1);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("$.data.users.[0].name"), name);

	}

	@Test(dataProvider = "getUserByRest", groups = {
			"smoke" , "sanity" }, description = "POST /userService/v1/userByRestaurant/")
	public void getUserByRest(String rest_id, String role_id, String partner_type, String mapping_type, String statusMessage) throws Exception{

		Processor processor = helper.getUserByRest(rest_id, role_id, partner_type, mapping_type);

		String resp=processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/getUserByRest");
		List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
		Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching  API");
		boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);

		List<Integer> rest = JsonPath.read(resp, "$.userDetails.[0].users..restaurant_id");
		System.out.println("Rest IDDDDDD" + rest);

		if(rest.size()>0) {
			for (int i = 0; i < rest.size(); i++) {
				Assert.assertEquals(rest.get(i),  Integer.valueOf(rest_id));
			}
		} else
			System.out.println("No users found with this role");

	}


	@Test(dataProvider = "getUserByRestWithRole", groups = {
			"smoke" , "sanity" }, description = "POST /userService/v1/userByRestaurant/")
	public void getUserByRestWithRole(String rest_id, String role_id, String partner_type, String mapping_type, String statusMessage) throws Exception{

		Processor processor = helper.getUserByRest(rest_id, role_id, partner_type, mapping_type);

		String resp=processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/getUserByRest");
		List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
		Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching  API");
		boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);

		List<Integer> roles = JsonPath.read(resp, "$.userDetails.[0].users..role_id");
		List<Integer> rest = JsonPath.read(resp, "$.userDetails.[0].users..restaurant_id");

		System.out.println("Role IDDDDDD" + roles);
		System.out.println("Rest IDDDDDD" + rest);

		if(roles.size()>0) {
			for (int i = 0; i < roles.size(); i++) {
				Assert.assertEquals(roles.get(i),  Integer.valueOf(role_id));
				Assert.assertEquals(rest.get(i),  Integer.valueOf(rest_id));
			}
		} else
			System.out.println("No users found with this role");

	}

	@Test(dataProvider = "getUserByRestWithPartnerType", groups = {
			"smoke" , "sanity" }, description = "POST /userService/v1/userByRestaurant/")
	public void getUserByRestWithPartnerType(String rest_id, String role_id, String partner_type, String mapping_type, String statusMessage) throws Exception{

		Processor processor = helper.getUserByRest(rest_id, role_id, partner_type, mapping_type);

		String resp=processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/getUserByRest");
		List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
		Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching  API");
		boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);

		List<Integer> roles = JsonPath.read(resp, "$.userDetails.[0].users..role_id");
		List<Integer> rest = JsonPath.read(resp, "$.userDetails.[0].users..restaurant_id");

		System.out.println("Role IDDDDDD" + roles);
		System.out.println("Rest IDDDDDD" + rest);

		if(roles.size()>0) {
			Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.userDetails.[0].partner_type"), partner_type);
			for (int i = 0; i < roles.size(); i++) {
				try {
					Assert.assertEquals(roles.get(i), Integer.valueOf(role_id));
				} catch (NumberFormatException e){}
				Assert.assertEquals(rest.get(i),  Integer.valueOf(rest_id));
			}
		} else
			System.out.println("No users found with this role and partner type");

	}

	@Test(dataProvider = "getUserByRestMultiMapping", groups = {
			"smoke" , "sanity" }, description = "POST /userService/v1/userByRestaurant/")
	public void getUserByRestMultiMapping(List<String> rest_id, String role_id, String partner_type, String mapping_type, String statusMessage) throws Exception{

		Processor processor = helper.getUserByRest(rest_id.toString(), role_id, partner_type, mapping_type);

		String resp=processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/getUserByRestMultiMapping");
		List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
		Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching  API");
		boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);

		List<Integer> roles = JsonPath.read(resp, "$.userDetails..role_id");
		List<JSONArray> rest = JsonPath.read(resp, "$.userDetails..restaurantArray");

		System.out.println("Role IDDDDDD" + roles);
		System.out.println("Rest IDDDDDD" + rest);

		if(roles.size()>0) {
			for (int i = 0; i < roles.size(); i++) {
				Assert.assertEquals(roles.get(i), Integer.valueOf(role_id));
				Assert.assertEquals(rest.get(i).get(0).toString(), rest_id.get(0));
				Assert.assertEquals(rest.get(i).get(1).toString(), rest_id.get(1));
			}
		} else
			System.out.println("No users found with this role and partner type");

	}

	@Test(dataProvider = "getUserByRestSingleMapping", groups = {
			"smoke" , "sanity" }, description = "POST /userService/v1/userByRestaurant/")
	public void getUserByRestSingleMapping(String rest_id, String role_id, String partner_type, String mapping_type, String statusMessage) throws Exception{

		Processor processor = helper.getUserByRest(rest_id, role_id, partner_type, mapping_type);

		String resp=processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/getUserByRestSingleMapping");
		List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
		Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching  API");
		boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertFalse(processor.ResponseValidator.DoesNodeExists("$.userDetails..restaurantArray"));
	}


	@Test(groups = {"smoke" , "sanity" }, description = "GET /order/v1/ff/dispositions?orderId")
	public void getDispFromFF() throws Exception {
		String orderId = helper.orderWithDisp();
		Processor processor = helper.getDispFromFF(orderId);
		String resp=processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/dispFromFF");
		List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
		Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching  API");
		boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), "success");
		if(!orderId.equals("1111")) {
			Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].orderId"), orderId);
		}
	}

	@Test(dataProvider = "getDispFromFFRegression", groups = {"regression"}, description = "GET /order/v1/ff/dispositions?orderId")
	public void getDispFromFFRegression(String orderId, String statusMessage) throws Exception {
		Processor processor = helper.getDispFromFF(orderId);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
	}

	@Test(dataProvider = "findPartnerType", groups = { "smoke",
			"sanity" }, description = "GET/order/v1/decision: find partner type")
	public void findPartnerType(String accessToken, String orderId, String restId, int statusCode, String statusMessage,
								String comment) throws Exception {

		Processor processor = helper.findPartnerType(accessToken, orderId, restId);
		String resp = processor.ResponseValidator.GetBodyAsText();
		System.out.println(resp);
		String path = System.getProperty("user.dir");
		String jsonschema = new ToolBox().readFileAsString(path + "/../Data/SchemaSet/Json/VMS/findPartnerType");
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		Assert.assertTrue(missingNodeList.isEmpty(),
				missingNodeList + " Nodes Are Missing Or Not Matching For POP Aggreagtor API");
		boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("comment"), comment);
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValue("$.['partner_id']"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.['partner_type']"));

	}

	@Test(dataProvider = "findPartnerType_regression", groups = {
			"regression" }, description = "GET/order/v1/decision: Regression test cases to find partner type")
	public void findPartnerType_regression(String accessToken, String orderId, String restId, int statusCode,
										   String statusMessage) {

		Processor processor = helper.findPartnerType(accessToken, orderId, restId);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);

	}

	@Test(dataProvider = "findPartnerType", groups = { "smoke",
			"sanity" }, description = "GET/restaurant/decision/: find partner type")
	public void getPartnerType(String accessToken, String orderId, String restId, int statusCode, String statusMessage,
							   String comment) throws Exception {

		Processor processor = helper.getPartnerType(accessToken, orderId, restId);
		String resp = processor.ResponseValidator.GetBodyAsText();
		System.out.println(resp);
		String path = System.getProperty("user.dir");
		String jsonschema = new ToolBox().readFileAsString(path + "/../Data/SchemaSet/Json/VMS/findPartnerType");
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		Assert.assertTrue(missingNodeList.isEmpty(),
				missingNodeList + " Nodes Are Missing Or Not Matching For POP Aggreagtor API");
		boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("comment"), comment);
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValue("$.['partner_id']"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.['partner_type']"));


	}

	@Test(groups = { "smoke", "sanity" }, description = "GET/v1/roles/: get roles")
	public void getRoles() throws Exception {

		Processor processor = helper.getRoles();
		String resp = processor.ResponseValidator.GetBodyAsText();
		String path = System.getProperty("user.dir");
		String jsonschema = new ToolBox().readFileAsString(path + "/../Data/SchemaSet/Json/VMS/getRoles");
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		Assert.assertTrue(missingNodeList.isEmpty(),
				missingNodeList + " Nodes Are Missing Or Not Matching For API");
		boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), 1);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), "Fetched Roles Successfully");
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.roles.[0].name"),"Owner");
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.roles.[1].name"),"Store Manager");
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.roles.[2].name"),"Order Manager");

	}

	@Test(dataProvider = "getRoleByID", groups = { "smoke", "sanity" }, description = "GET/v1/roles/: get roles")
	public void getRoleByID(String Id, String name) throws Exception {

		Processor processor = helper.getRoleByID(Id);
		String resp = processor.ResponseValidator.GetBodyAsText();
		String path = System.getProperty("user.dir");
		String jsonschema = new ToolBox().readFileAsString(path + "/../Data/SchemaSet/Json/VMS/getRoleByID");
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		Assert.assertTrue(missingNodeList.isEmpty(),
				missingNodeList + " Nodes Are Missing Or Not Matching For API");
		boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), 0);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), "Role Details Fetched");
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.roleDetails.[0].name"),name);
	}

	@Test(dataProvider = "updateUserFromROP", groups = { "smoke",
			"sanity" }, description = "POST contactability/updateUser: add users from bulk upload script inn ROP")
	public void updateUserFromROP(List<String> mobile, String name, String email, String restaurantId,
								  String role, String isPrimary, String action, String updatedBy, String source,
								  String successMessage, String conflictMessage) throws Exception {

		String is_primary = null;
		SoftAssert softAssert = new SoftAssert();
		Processor case1 = helper.updateUserFromROP(name, mobile.get(0), email, restaurantId, role, isPrimary, action, updatedBy, source);
		softAssert.assertEquals(case1.ResponseValidator.GetNodeValueAsInt("statusCode"), 0, "status Code doesn't match");
		softAssert.assertEquals(case1.ResponseValidator.GetNodeValue("statusMessage"), successMessage, "status Message doesn't match");
		try {
			is_primary = helper.getIsPrimaryFromUserMap("", "", "", mobile.get(0), "", "1");
		} catch (NullPointerException e) {
			System.out.println(e);
		}
		softAssert.assertEquals(is_primary, "true", "primary user flag is not as expected");

		Processor case2 = helper.updateUserFromROP(name, mobile.get(1), email, restaurantId, role, isPrimary, action, updatedBy, source);
		softAssert.assertEquals(case2.ResponseValidator.GetNodeValueAsInt("statusCode"), 0, "status Code doesn't match");
		softAssert.assertEquals(case2.ResponseValidator.GetNodeValue("statusMessage"), successMessage, "status Message doesn't match");
		try {
			is_primary = helper.getIsPrimaryFromUserMap("", "", "", mobile.get(1), "", "1");
		} catch (NullPointerException e) {
			System.out.println(e);
		}
		softAssert.assertEquals(is_primary, "true", "primary user flag is not as expected");
		try {
			is_primary = helper.getIsPrimaryFromUserMap("", "", "", mobile.get(0), "", "1");
		} catch (NullPointerException e) {
			System.out.println(e);
		}
		softAssert.assertNull (is_primary, "Existing primary user is not made secondary");

		helper.setRegisterdFlagForUser("", "", "", mobile.get(1), "", "1");

		Processor case3 = helper.updateUserFromROP(name, mobile.get(2), email, restaurantId, role, isPrimary, action, updatedBy, source);
		softAssert.assertEquals(case3.ResponseValidator.GetNodeValueAsInt("statusCode"), 1, "status Code doesn't match");
		softAssert.assertEquals(case3.ResponseValidator.GetNodeValue("statusMessage"), conflictMessage, "status Message doesn't match");

		String resp = case3.ResponseValidator.GetBodyAsText();
		String path = System.getProperty("user.dir");
		String jsonschema = new ToolBox().readFileAsString(path + "/../Data/SchemaSet/Json/VMS/updateUserFromROP");
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		softAssert.assertTrue(missingNodeList.isEmpty(),
				missingNodeList + " Nodes Are Missing Or Not Matching For API");
		boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);

		helper.deleteUserDataFromUsersAndMap("","", "", mobile.get(0), "","1");
		helper.deleteUserDataFromUsersAndMap("","", "", mobile.get(1), "","1");

		softAssert.assertAll();
	}

	@Test(dataProvider = "updateUserFromROPRegression", groups = { "regression"},
			description = "POST contactability/updateUser: add users from bulk upload script inn ROP")
	public void updateUserFromROPRegression(String name, String mobile, String email, String restaurantId,
								  String role, String isPrimary, String action, String updatedBy, String source,
								  String statusMessage, int statusCode) throws Exception {

		Processor case1 = helper.updateUserFromROP(name, mobile, email, restaurantId, role, isPrimary, action, updatedBy, source);
		Assert.assertEquals(case1.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
		Assert.assertEquals(case1.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);

	}

	@Test(dataProvider = "updateUserROPSecondary", groups = { "smoke",
			"sanity" }, description = "POST contactability/updateUser: add users from ROP UI as secondary")
	public void updateUserROPSecondary(String mobile, String name, String email, String restaurantId,
								  String role, String isPrimary, String action, String updatedBy, String source,
								  String successMessage) throws PathNotFoundException {

		String is_primary = null;
		SoftAssert softAssert = new SoftAssert();
		Processor case1 = helper.updateUserFromROP(name, mobile, email, restaurantId, role, isPrimary, action, updatedBy, source);
		softAssert.assertEquals(case1.ResponseValidator.GetNodeValueAsInt("statusCode"), 0, "status Code doesn't match");
		softAssert.assertEquals(case1.ResponseValidator.GetNodeValue("statusMessage"), successMessage, "status Message doesn't match");
		try {
			is_primary = helper.getIsPrimaryFromUserMap("", "", "", mobile, "", "1");
		} catch(NullPointerException e) {
			System.out.println(e);
		}
		softAssert.assertEquals(is_primary, "false", "primary user flag is not as expected");

		helper.deleteUserDataFromUsersAndMap("","", "", mobile, "","1");

		softAssert.assertAll();
	}

	@Test(dataProvider = "updateExistingUserROPSecondary", groups = { "regression"},
			description = "POST contactability/updateUser: update existing user details from ROP UI")
	public void updateExistingUserROPSecondary(String mobile, String name, String email, String restaurantId,
									   String role, String isPrimary, String action, String updatedBy, String source,
									   String successMessage) throws PathNotFoundException {

		String is_primary = null;
		SoftAssert softAssert = new SoftAssert();
		Processor case1 = helper.updateUserFromROP(name, mobile, email, restaurantId, role, isPrimary, action, updatedBy, source);
		softAssert.assertEquals(case1.ResponseValidator.GetNodeValueAsInt("statusCode"), 0, "status Code doesn't match");
		softAssert.assertEquals(case1.ResponseValidator.GetNodeValue("statusMessage"), successMessage, "status Message doesn't match");

		Processor case2 = helper.updateUserFromROP("editedName", mobile, "edited@email.com", restaurantId, role, "true", action, updatedBy, source);
		softAssert.assertEquals(case2.ResponseValidator.GetNodeValueAsInt("statusCode"), 0, "status Code doesn't match");
		softAssert.assertEquals(case2.ResponseValidator.GetNodeValue("statusMessage"), successMessage, "status Message doesn't match");
		try {
			is_primary = helper.getIsPrimaryFromUserMap("", "", "", mobile, "", "1");
		} catch(NullPointerException e) {
			System.out.println(e);
		}
		softAssert.assertEquals (is_primary, "true", "Existing secondary user is not updated to primary");

		Processor p = helper.getUsers("", "", "", mobile, "", "1");
		softAssert.assertEquals(p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.users.[0].name"), "editedName");
		softAssert.assertEquals(p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.users.[0].email"), "edited@email.com");

		helper.deleteUserDataFromUsersAndMap("","", "", mobile, "","1");

		softAssert.assertAll();
	}

	@Test(dataProvider = "updateUserROPPrimaryThenSecondary", groups = { "regression"},
			description = "POST contactability/updateUser: add primary user and then secondary from ROP UI")
	public void updateUserROPPrimaryThenSecondary(List<String> mobile, String name, String email, String restaurantId,
											 String role, String isPrimary, String action, String updatedBy, String source,
											 String successMessage) throws PathNotFoundException {

		String is_primary = null;
		SoftAssert softAssert = new SoftAssert();
		Processor case1 = helper.updateUserFromROP(name, mobile.get(0), email, restaurantId, role, isPrimary, action, updatedBy, source);
		softAssert.assertEquals(case1.ResponseValidator.GetNodeValueAsInt("statusCode"), 0, "status Code doesn't match");
		softAssert.assertEquals(case1.ResponseValidator.GetNodeValue("statusMessage"), successMessage, "status Message doesn't match");
		try {
			is_primary = helper.getIsPrimaryFromUserMap("", "", "", mobile.get(0), "", "1");
		} catch(NullPointerException e) {
			System.out.println(e);
		}
		softAssert.assertEquals(is_primary, "true", "primary user flag is not as expected");

		Processor case2 = helper.updateUserFromROP(name, mobile.get(1), email, restaurantId, role, "false", action, updatedBy, source);
		softAssert.assertEquals(case2.ResponseValidator.GetNodeValueAsInt("statusCode"), 0, "status Code doesn't match");
		softAssert.assertEquals(case2.ResponseValidator.GetNodeValue("statusMessage"), successMessage, "status Message doesn't match");
		try {
			is_primary = helper.getIsPrimaryFromUserMap("", "", "", mobile.get(1), "", "1");
		} catch(NullPointerException e) {
			System.out.println(e);
		}
		softAssert.assertEquals(is_primary, "false", "primary user flag is not as expected");
		try {
			is_primary = helper.getIsPrimaryFromUserMap("", "", "", mobile.get(0), "", "1");
		} catch(NullPointerException e) {
			System.out.println(e);
		}
		softAssert.assertEquals (is_primary, "true", "Existing primary user changed to secondary");

		helper.deleteUserDataFromUsersAndMap("","", "", mobile.get(0), "","1");
		helper.deleteUserDataFromUsersAndMap("","", "", mobile.get(1), "","1");

		softAssert.assertAll();
	}

	@Test(dataProvider = "updateSecondaryToPrimaryActionNA", groups = { "regression"},
			description = "POST contactability/updateUser: update existing secondary user details and make" +
			"him primary with action NA when primary user exists")
	public void updateSecondaryToPrimaryActionNA(List<String> mobile, String name, String email, String restaurantId,
												  String role, String isPrimary, String action, String updatedBy, String source,
												  String successMessage, String conflictMessage) throws PathNotFoundException {

		String is_primary = null;
		SoftAssert softAssert = new SoftAssert();
		Processor case1 = helper.updateUserFromROP(name, mobile.get(0), email, restaurantId, role, isPrimary, action, updatedBy, source);
		softAssert.assertEquals(case1.ResponseValidator.GetNodeValueAsInt("statusCode"), 0, "status Code doesn't match");
		softAssert.assertEquals(case1.ResponseValidator.GetNodeValue("statusMessage"), successMessage, "status Message doesn't match");

		Processor case2 = helper.updateUserFromROP(name, mobile.get(1), email, restaurantId, role, "false", action, updatedBy, source);
		softAssert.assertEquals(case2.ResponseValidator.GetNodeValueAsInt("statusCode"), 0, "status Code doesn't match");
		softAssert.assertEquals(case2.ResponseValidator.GetNodeValue("statusMessage"), successMessage, "status Message doesn't match");

		Processor case3 = helper.updateUserFromROP("editedName", mobile.get(1), "edited@email.com", restaurantId, role, "true", action, updatedBy, source);
		softAssert.assertEquals(case3.ResponseValidator.GetNodeValueAsInt("statusCode"), 1, "status Code doesn't match");
		softAssert.assertEquals(case3.ResponseValidator.GetNodeValue("statusMessage"), conflictMessage, "status Message doesn't match");

		try {
			is_primary = helper.getIsPrimaryFromUserMap("", "", "", mobile.get(1), "", "1");
		} catch(NullPointerException e) {
			System.out.println(e);
		}
		softAssert.assertEquals(is_primary, "false", "primary user flag is not as expected");
		try {
			is_primary = helper.getIsPrimaryFromUserMap("", "", "", mobile.get(0), "", "1");
		} catch(NullPointerException e) {
			System.out.println(e);
		}
		softAssert.assertEquals (is_primary, "true", "Existing primary user changed to secondary");


		Processor p = helper.getUsers("", "", "", mobile.get(1), "", "1");
		softAssert.assertEquals(p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.users.[0].name"), name);
		softAssert.assertEquals(p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.users.[0].email"), email);

		helper.deleteUserDataFromUsersAndMap("","", "", mobile.get(0), "","1");
		helper.deleteUserDataFromUsersAndMap("","", "", mobile.get(1), "","1");

		softAssert.assertAll();
	}

	@Test(dataProvider = "updateSecondaryToPrimaryActionAdd", groups = { "regression"},
			description = "POST contactability/updateUser: update existing secondary user details and make " +
			"him primary with action Add when primary user exists")
	public void updateSecondaryToPrimaryActionAdd(List<String> mobile, String name, String email, String restaurantId,
												 String role, String isPrimary, String action, String updatedBy, String source,
												 String successMessage) throws PathNotFoundException {

		String is_primary = null;
		SoftAssert softAssert = new SoftAssert();
		Processor case1 = helper.updateUserFromROP(name, mobile.get(0), email, restaurantId, role, isPrimary, action, updatedBy, source);
		softAssert.assertEquals(case1.ResponseValidator.GetNodeValueAsInt("statusCode"), 0, "status Code doesn't match");
		softAssert.assertEquals(case1.ResponseValidator.GetNodeValue("statusMessage"), successMessage, "status Message doesn't match");

		Processor case2 = helper.updateUserFromROP(name, mobile.get(1), email, restaurantId, role, "false", action, updatedBy, source);
		softAssert.assertEquals(case2.ResponseValidator.GetNodeValueAsInt("statusCode"), 0, "status Code doesn't match");
		softAssert.assertEquals(case2.ResponseValidator.GetNodeValue("statusMessage"), successMessage, "status Message doesn't match");

		Processor case3 = helper.updateUserFromROP("editedName", mobile.get(1), "edited@email.com", restaurantId, role, "true", action, updatedBy, source);
		softAssert.assertEquals(case3.ResponseValidator.GetNodeValueAsInt("statusCode"), 0, "status Code doesn't match");
		softAssert.assertEquals(case3.ResponseValidator.GetNodeValue("statusMessage"), successMessage, "status Message doesn't match");

		try {
			is_primary = helper.getIsPrimaryFromUserMap("", "", "", mobile.get(1), "", "1");
		} catch(NullPointerException e) {
			System.out.println(e);
		}
		softAssert.assertEquals(is_primary, "false", "primary user flag is not as expected");
		try {
			is_primary = helper.getIsPrimaryFromUserMap("", "", "", mobile.get(0), "", "1");
		} catch(NullPointerException e) {
			System.out.println(e);
		}
		softAssert.assertEquals (is_primary, "true", "Existing primary user changed to secondary");


		Processor p = helper.getUsers("", "", "", mobile.get(1), "", "1");
		softAssert.assertEquals(p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.users.[0].name"), "editedName");
		softAssert.assertEquals(p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.users.[0].email"), "edited@email.com");

		helper.deleteUserDataFromUsersAndMap("","", "", mobile.get(0), "","1");
		helper.deleteUserDataFromUsersAndMap("","", "", mobile.get(1), "","1");

		softAssert.assertAll();
	}

	@Test(dataProvider = "updateUserROPPrimaryExistingPrimaryActionNA",
			expectedExceptions = PathNotFoundException.class ,
					groups = { "smoke", "sanity" },
			description = "POST contactability/updateUser: add primary users when existing primary user with action NA")
	public void updateUserROPPrimaryExistingPrimaryActionNA(List<String> mobile, String name, String email, String restaurantId,
														String role, String isPrimary, String action, String updatedBy, String source,
														String successMessage, String conflictMessage) throws PathNotFoundException {

		String is_primary = null;
		SoftAssert softAssert = new SoftAssert();
		Processor case1 = helper.updateUserFromROP(name, mobile.get(0), email, restaurantId, role, isPrimary, action, updatedBy, source);
		softAssert.assertEquals(case1.ResponseValidator.GetNodeValueAsInt("statusCode"), 0, "status Code doesn't match");
		softAssert.assertEquals(case1.ResponseValidator.GetNodeValue("statusMessage"), successMessage, "status Message doesn't match");
		try {
			is_primary = helper.getIsPrimaryFromUserMap("", "", "", mobile.get(0), "", "1");
		} catch(NullPointerException e) {
			System.out.println(e);
		}
		softAssert.assertEquals(is_primary, "true", "primary user flag is not as expected");

		Processor case2 = helper.updateUserFromROP(name, mobile.get(1), email, restaurantId, role, isPrimary, action, updatedBy, source);
		softAssert.assertEquals(case2.ResponseValidator.GetNodeValueAsInt("statusCode"), 1, "status Code doesn't match");
		softAssert.assertEquals(case2.ResponseValidator.GetNodeValue("statusMessage"), conflictMessage, "status Message doesn't match");

		softAssert.assertEquals (is_primary, "true", "Existing primary user changed to secondary");

		helper.deleteUserDataFromUsersAndMap("","", "", mobile.get(0), "","1");
		helper.deleteUserDataFromUsersAndMap("","", "", mobile.get(1), "","1");

		softAssert.assertAll();
	}

	@Test(dataProvider = "updateUserROPPrimaryExistingPrimaryActionAdd",groups = { "smoke", "sanity" },
			description = "POST contactability/updateUser: add primary user when existing primary user with action add from ROP UI")
	public void updateUserROPPrimaryExistingPrimaryActionAdd(List<String> mobile, String name, String email, String restaurantId,
															String role, String isPrimary, String action, String updatedBy, String source,
															String successMessage) throws PathNotFoundException {

		String is_primary = null;
		SoftAssert softAssert = new SoftAssert();
		Processor case1 = helper.updateUserFromROP(name, mobile.get(0), email, restaurantId, role, isPrimary, action, updatedBy, source);
		softAssert.assertEquals(case1.ResponseValidator.GetNodeValueAsInt("statusCode"), 0, "status Code doesn't match");
		softAssert.assertEquals(case1.ResponseValidator.GetNodeValue("statusMessage"), successMessage, "status Message doesn't match");
		try {
			is_primary = helper.getIsPrimaryFromUserMap("", "", "", mobile.get(0), "", "1");
		} catch(NullPointerException e) {
			System.out.println(e);
		}
		softAssert.assertEquals(is_primary, "true", "primary user flag is not as expected");

		Processor case2 = helper.updateUserFromROP(name, mobile.get(1), email, restaurantId, role, isPrimary, action, updatedBy, source);
		softAssert.assertEquals(case2.ResponseValidator.GetNodeValueAsInt("statusCode"), 0, "status Code doesn't match");
		softAssert.assertEquals(case2.ResponseValidator.GetNodeValue("statusMessage"), successMessage, "status Message doesn't match");
		try {
			is_primary = helper.getIsPrimaryFromUserMap("", "", "", mobile.get(1), "", "1");
		} catch(NullPointerException e) {
			System.out.println(e);
		}
		softAssert.assertEquals(is_primary, "false", "primary user flag is not as expected");
		try {
			is_primary = helper.getIsPrimaryFromUserMap("", "", "", mobile.get(0), "", "1");
		} catch(NullPointerException e) {
			System.out.println(e);
		}
		softAssert.assertEquals (is_primary,"true", "Existing primary user changed to secondary");

		helper.deleteUserDataFromUsersAndMap("","", "", mobile.get(0), "","1");
		helper.deleteUserDataFromUsersAndMap("","", "", mobile.get(1), "","1");

		softAssert.assertAll();
	}

	@Test(dataProvider = "updateUserROPPrimaryExistingPrimaryActionReplace", groups = { "smoke", "sanity" },
			description = "POST contactability/updateUser: add primary user when existing primary with action replace from ROP UI")
	public void updateUserROPPrimaryExistingPrimaryActionReplace(List<String> mobile, String name, String email, String restaurantId,
															 String role, String isPrimary, String action, String updatedBy, String source,
															 String successMessage) throws PathNotFoundException {

		String is_primary = null;
		SoftAssert softAssert = new SoftAssert();
		Processor case1 = helper.updateUserFromROP(name, mobile.get(0), email, restaurantId, role, isPrimary, "NA", updatedBy, source);
		softAssert.assertEquals(case1.ResponseValidator.GetNodeValueAsInt("statusCode"), 0, "status Code doesn't match");
		softAssert.assertEquals(case1.ResponseValidator.GetNodeValue("statusMessage"), successMessage, "status Message doesn't match");
		try {
			is_primary = helper.getIsPrimaryFromUserMap("", "", "", mobile.get(0), "", "1");
		} catch(NullPointerException e) {
			System.out.println(e);
		}
		softAssert.assertEquals(is_primary, "true", "primary user flag is not as expected");

		String userToReplaceId = helper.getUserId("", "", "", mobile.get(0), "", "1");

		Processor case2 = helper.replaceUserFromROP(name, mobile.get(1), email, restaurantId, role, isPrimary, action, updatedBy, userToReplaceId);
		softAssert.assertEquals(case2.ResponseValidator.GetNodeValueAsInt("statusCode"), 0, "status Code doesn't match");
		softAssert.assertEquals(case2.ResponseValidator.GetNodeValue("statusMessage"), successMessage, "status Message doesn't match");
		try {
			is_primary = helper.getIsPrimaryFromUserMap("", "", "", mobile.get(1), "", "1");
		} catch(NullPointerException e) {
			System.out.println(e);
		}
		softAssert.assertEquals(is_primary, "true", "primary user flag is not as expected");
		try {
			is_primary = helper.getIsPrimaryFromUserMap("", "", "", mobile.get(0), "", "1");
		} catch(NullPointerException e) {
			System.out.println(e);
		}
		softAssert.assertNull (is_primary, "Existing primary user not changed to secondary");

		helper.deleteUserDataFromUsersAndMap("","", "", mobile.get(0), "","1");
		helper.deleteUserDataFromUsersAndMap("","", "", mobile.get(1), "","1");

		softAssert.assertAll();
	}

	@Test(dataProvider = "updateUserROPReplaceRegression", groups = { "regression"},
			description = "POST contactability/updateUser: add primary user when existing primary with action replace from ROP UI")
	public void updateUserROPReplaceRegression(String mobile, String name, String email, String restaurantId,
											   String role, String isPrimary, String action, String updatedBy, String source,
											   String successMessage, int statusCode, String restaurantId1) throws PathNotFoundException {

		SoftAssert softAssert = new SoftAssert();
		Processor case1 = helper.updateUserFromROP(name, mobile, email, restaurantId, role, isPrimary, "NA", updatedBy, source);
		softAssert.assertEquals(case1.ResponseValidator.GetNodeValueAsInt("statusCode"), 0, "status Code doesn't match");
		softAssert.assertEquals(case1.ResponseValidator.GetNodeValue("statusMessage"), "Processed successfully", "status Message doesn't match");

		String userToReplaceId = helper.getUserId("", "", "", mobile, "", "1");

		Processor case2 = helper.replaceUserFromROP(name, mobile, email, restaurantId1, role, isPrimary, action, updatedBy, userToReplaceId);
		softAssert.assertEquals(case2.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode, "status Code doesn't match");
		softAssert.assertEquals(case2.ResponseValidator.GetNodeValue("statusMessage"), successMessage, "status Message doesn't match");

		helper.deleteUserDataFromUsersAndMap("","", "", mobile, "","1");

		softAssert.assertAll();
	}

	@Test(dataProvider = "getAllCities", groups = { "smoke", "sanity"}, description = "GET v1/cities/all?restaurantId= get All cities")
	public void getAllCities(String accessToken, String restaurantId, int statusCode, String statusMessage) throws Exception {

		Processor processor = helper.getAllCities(accessToken, restaurantId);
		String resp = processor.ResponseValidator.GetBodyAsText();
		String path = System.getProperty("user.dir");
		String jsonschema = new ToolBox().readFileAsString(path + "/../Data/SchemaSet/Json/VMS/getAllCities");
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		Assert.assertTrue(missingNodeList.isEmpty(),
				missingNodeList + " Nodes Are Missing Or Not Matching For API");
		boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);

	}

	@Test(dataProvider = "getAllCitiesRegression", groups = { "regression"}, description = "GET v1/cities/all?restaurantId=: get all cities")
	public void getAllCitiesRegression(String accessToken, String restaurantId, int statusCode, String statusMessage) throws Exception {

		Processor processor = helper.getAllCities(accessToken, restaurantId);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);

	}

	@Test(dataProvider = "getCities", groups = { "smoke", "sanity"}, description = "GET v1/cities?restaurantId= get All cities")
	public void getCities(String accessToken, String restaurantId, String cityId, int statusCode, String statusMessage) throws Exception {

		Processor processor = helper.getCities(accessToken, restaurantId, cityId);
		String resp = processor.ResponseValidator.GetBodyAsText();
		String path = System.getProperty("user.dir");
		String jsonschema = new ToolBox().readFileAsString(path + "/../Data/SchemaSet/Json/VMS/getCities");
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		Assert.assertTrue(missingNodeList.isEmpty(),
				missingNodeList + " Nodes Are Missing Or Not Matching For API");
		boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);

	}

	@Test(dataProvider = "getCitiesRegression", groups = { "regression"}, description = "GET v1/cities?restaurantId= get All cities")
	public void getCitiesRegression(String accessToken, String restaurantId, String cityId, int statusCode, String statusMessage) throws Exception {

		Processor processor = helper.getCities(accessToken, restaurantId, cityId);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);

	}

	@Test(dataProvider = "getTC", groups = { "smoke", "sanity"}, description = "GET v1/terms-and-conditions?restaurantId= get terms and conditions")
	public void getTC(String accessToken, String restaurantId, int statusCode, String statusMessage) throws Exception {

		Processor processor = helper.getTC(accessToken, restaurantId);
		String resp = processor.ResponseValidator.GetBodyAsText();
		String path = System.getProperty("user.dir");
		String jsonschema = new ToolBox().readFileAsString(path + "/../Data/SchemaSet/Json/VMS/getTC");
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		Assert.assertTrue(missingNodeList.isEmpty(),
				missingNodeList + " Nodes Are Missing Or Not Matching For API");
		boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);

	}

	@Test(dataProvider = "getTCRegression", groups = { "regression"}, description = "GET v1/terms-and-conditions?restaurantId= get terms and conditions")
	public void getTCRegression(String accessToken, String restaurantId, int statusCode, String statusMessage) throws Exception {

		Processor processor = helper.getTC(accessToken, restaurantId);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);

	}

	@Test(dataProvider = "getTCAcceptance", groups = { "smoke", "sanity"}, description = "GET v1/terms-and-conditions?restaurantId= get terms and conditions")
	public void getTCAcceptance(String accessToken, String restaurantId, int statusCode, String statusMessage) throws Exception {

		Processor processor = helper.getTCAcceptance(accessToken, restaurantId);
		String resp = processor.ResponseValidator.GetBodyAsText();
		String path = System.getProperty("user.dir");
		String jsonschema = new ToolBox().readFileAsString(path + "/../Data/SchemaSet/Json/VMS/getTCAcceptance");
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		Assert.assertTrue(missingNodeList.isEmpty(),
				missingNodeList + " Nodes Are Missing Or Not Matching For API");
		boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);

	}

	@Test(dataProvider = "getTCAcceptanceRegression", groups = { "regression"}, description = "GET v1/terms-and-conditions?restaurantId= get terms and conditions")
	public void getTCAcceptanceRegression(String accessToken, String restaurantId, int statusCode, String statusMessage) throws Exception {

		Processor processor = helper.getTCAcceptance(accessToken, restaurantId);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);

	}

	@Test(dataProvider = "getTCLastAcceptance", groups = { "smoke", "sanity"}, description = "GET v1/terms-and-conditions/last-accepted?restaurantId= get terms and conditions")
	public void getTCLastAcceptance(String accessToken, String restaurantId, int statusCode, String statusMessage) throws Exception {

		Processor processor = helper.getTCLastAcceptance(accessToken, restaurantId);
		String resp = processor.ResponseValidator.GetBodyAsText();
		String path = System.getProperty("user.dir");
		String jsonschema = new ToolBox().readFileAsString(path + "/../Data/SchemaSet/Json/VMS/getTCLastAcceptance");
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		Assert.assertTrue(missingNodeList.isEmpty(),
				missingNodeList + " Nodes Are Missing Or Not Matching For API");
		boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);

	}

	@Test(dataProvider = "getTCLastAcceptanceRegression", groups = { "regression"}, description = "GET v1/terms-and-conditions/last-accepted?restaurantId= get terms and conditions")
	public void getTCLastAcceptanceRegression(String accessToken, String restaurantId, int statusCode, String statusMessage) throws Exception {

		Processor processor = helper.getTCLastAcceptance(accessToken, restaurantId);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);

	}

	@Test(dataProvider = "acceptTC", groups = { "smoke", "sanity"}, description = "POST v1/terms-and-conditions/accept accept terms and conditions")
	public void acceptTC(String accessToken, String restaurantId, int statusCode, String statusMessage) throws Exception {

		Processor processor = helper.acceptTC(accessToken, restaurantId);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);

	}

	@Test(dataProvider = "enabledWebviewTC", groups = { "smoke", "sanity"}, description = "POST v1/terms-and-conditions/enabled/webview ")
	public void enabledWebviewTC(String accessToken, String restaurantId, int statusCode, String statusMessage) throws Exception {

		Processor processor = helper.enabledWebviewTC(accessToken, restaurantId);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.details.tcEnabled"));

	}

	@Test(dataProvider = "acceptWebviewTC", groups = { "smoke", "sanity"}, description = "POST v1/terms-and-conditions/enabled/webview ")
	public void acceptWebviewTC(String accessToken, String restaurantId, int statusCode, String statusMessage) throws Exception {

		Processor processor = helper.acceptWebviewTC(accessToken, restaurantId);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.details.showPopup"));

	}

	@Test(dataProvider="getMfrReport",groups={"sanity"},
			description="Get MFR daily report for a given restaurant id")
	public void getMfrReport(String accessToken, String restId, String startdate, String enddate, int statusCode, String statusMessage) {
		Processor processor = helper.getMfrReport(accessToken, restId, startdate, enddate);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsInt("$.data.mfr_pressed_correctly"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsInt("$.data.mfr_not_pressed"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsInt("$.data.mfr_pressed_early"));
	}

	@Test(dataProvider="getMfrReportRegression",groups={"regression"},
			description="Get MFR daily report for a given restaurant id")
	public void getMfrReportRegression(String accessToken, String restId, String startdate, String enddate, int statusCode, String statusMessage) {
		Processor processor = helper.getMfrReport(accessToken, restId, startdate, enddate);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);

	}

	@AfterClass
	public void deleteIds() {
		for(int i=0;i<user_ids.size();i++)
			if(user_ids.get(i).toString() != null && !user_ids.get(i).isEmpty())
			helper.deleteUserIds(user_ids.get(i));
	}
}