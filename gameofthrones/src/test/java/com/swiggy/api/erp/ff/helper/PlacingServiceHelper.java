package com.swiggy.api.erp.ff.helper;

import com.rabbitmq.client.AMQP;
import com.swiggy.api.erp.ff.constants.PlacingServiceConstants;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Tyrion.RabbitMQHelper;
import org.apache.commons.io.FileUtils;
import org.jbehave.core.context.ContextView;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;

public class PlacingServiceHelper implements PlacingServiceConstants{

    Initialize gameofthrones = new Initialize();
    RabbitMQHelper rabbitMQHelper = new RabbitMQHelper();

    public void publishPlacingEvent(String timeStamp, String orderId, String toState) throws IOException, TimeoutException {
        File file = new File("../Data/Payloads/JSON/messageFromPlacingService.json");
        String message = FileUtils.readFileToString(file);
        message = message.replace("${timeStamp}", timeStamp).replace("${orderId}", orderId).replace("${toState}", toState);
        rabbitMQHelper.purgeQueue(hostName, dead_letters_queue);
        rabbitMQHelper.pushMessageToExchange(hostName, placingServiceExchange, new AMQP.BasicProperties().builder().contentType("application/json"), message);
    }

    public boolean isWrongMessagePresentInDeadLetters(){
        return rabbitMQHelper.getMessage(hostName, dead_letters_queue) != null && rabbitMQHelper.getMessage(hostName, dead_letters_queue) != "";
    }

    public void publishWrongMesagePlacingEvent() throws IOException {
        rabbitMQHelper.pushMessageToExchange(hostName, placingServiceExchange, new AMQP.BasicProperties().builder().contentType("application/json"), "{\"Wrong Message\":\"This message should go in dead letter\"}");
    }

    public String getTimeStampForPlacingServiceMessage(){
        Date date = new Date();
        date.setSeconds(date.getSeconds()+30);
        DateFormat currentDate = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss.SSS");
        return currentDate.format(date).replace('_', 'T');
    }

    private List<Map<String, Object>> getStatusAndTimeFromDB(String orderId, String scenario){
        try {
            return SystemConfigProvider.getTemplate(hostName).queryForList(orderStatusQuery(orderId, scenario));

        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
            System.out.println("No Entry for given order in DB");
            return null;
        }
    }

    public Boolean verifyPlacingStatusAndTimeInDB(String orderId, String expectedTime, String scenario){
        String actualPlacingStatus = null;
        String actualTimeStamp = null;
        String expectedStatus = null;
        List<Map<String, Object>> result = getStatusAndTimeFromDB(orderId, scenario);
        actualPlacingStatus = (String) result.get(0).get("placed_status");
        if(scenario.contains("PLACED")){
            actualTimeStamp = result.get(0).get("placed_time").toString();
            expectedStatus = "placed";
        }else if (scenario.contains("PARTNER")){
            actualTimeStamp = result.get(0).get("with_partner_time").toString();
            expectedStatus = "with_partner";
        }else if (scenario.contains("DE")){
            actualTimeStamp = result.get(0).get("with_de_time").toString();
            expectedStatus = "with_de";
        }
        System.out.println("Actual Status = " + actualPlacingStatus + ", Expected Status = " + expectedStatus +"\nActual TimeStamp = " + actualTimeStamp + ", Expected TimeStamp = " + expectedTime.replace("T", " "));
        return expectedStatus.equalsIgnoreCase(actualPlacingStatus) && expectedTime.replace("T", " ").equals(actualTimeStamp);
    }
}
