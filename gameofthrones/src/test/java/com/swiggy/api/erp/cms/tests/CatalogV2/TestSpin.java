package com.swiggy.api.erp.cms.tests.CatalogV2;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.dp.CatalogV2SpinDP;
import com.swiggy.api.erp.cms.helper.CatalogV2Helper;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created-By:Ashiwani
 */

public class TestSpin extends CatalogV2SpinDP {
    CatalogV2Helper cms=new CatalogV2Helper();
    String responsecreate;
    String spinid;
    String brand;


    @Test(dataProvider = "cratespindp",description = "This test will create a SPIN",priority = 0)
    public void createSpin(String res){
        responsecreate=cms.createSpin(res).ResponseValidator.GetBodyAsText();
        spinid=JsonPath.read(responsecreate,"$.spin");
        Assert.assertNotNull(spinid);
    }

    @Test(description = "This test will update a SPIN")
    public void updateSpin(){
        spinid=JsonPath.read(responsecreate,"$.spin");
        brand= JsonPath.read(responsecreate,"$..attributes.brand").toString().replace("[","").replace("]","");
        String response=cms.updateSpin(spinid,brand).ResponseValidator.GetBodyAsText();
        System.out.println(response);


    }

    @Test(description = "This test will search a SPIN using attributes")
    public void searchSpin() throws Exception{
        brand= JsonPath.read(responsecreate,"$..attributes.brand").toString().replace("[","").replace("]","").replace("\"","");;
        String response=cms.searchSpin(brand).ResponseValidator.GetBodyAsText();
        String brandact= JsonPath.read(response,"$..attributes.brand").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(brandact,brand);
    }

    @Test(description = "This test will get a single SPIN")
    public void getSingleSpin(){
        String response=cms.getSpin(spinid).ResponseValidator.GetBodyAsText();
        String spinidact= JsonPath.read(response,"$.spin").toString().replace("[","").replace("]","");
        Assert.assertEquals(spinidact,spinid,"ID not matching");

    }
    @Test(description = "This test will get a multiple SPIN")
    public void getMultipleSpin(){
        String response=cms.getMulSpin(spinid).ResponseValidator.GetBodyAsText();
        String spinidact= JsonPath.read(response,"$..spin").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(spinidact,spinid,"ID's not matching");
    }
}
