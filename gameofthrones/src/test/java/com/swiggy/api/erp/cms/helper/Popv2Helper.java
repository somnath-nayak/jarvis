package com.swiggy.api.erp.cms.helper;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.constants.CmsConstants;
import com.swiggy.api.erp.cms.constants.MenuConstants;
import com.swiggy.api.erp.cms.pojo.AddItemToRest.AddItemToRestaurant;
import com.swiggy.api.erp.cms.pojo.AddItemToRest.RestBuilder;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.Processor;
import org.json.JSONException;

import java.io.IOException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

public class Popv2Helper {
	CMSHelper cmsHelper = new CMSHelper();
	Initialize gameofthrones = new Initialize();
	POP_Item_Schedule_Mapping_By_Area_Helper obj1 = new POP_Item_Schedule_Mapping_By_Area_Helper();

	public Integer[] PopItemCreate(int restId) throws IOException, JSONException {
		POP_Restaurant_Menu_Map_Helper obj = new POP_Restaurant_Menu_Map_Helper();
		int MenuId = 0, catId, sutId, parent_id = 0;
		List<Map<String, Object>> map = cmsHelper.getMenuIdForPop(restId);
		map.size();
		if (map.size() != 0) {
			MenuId = Integer.parseInt(map.get(0).get("menu_id").toString());
		} else {
			System.out.println("no category Found");

			Processor p1 = obj.createRestaurantMenuMapPop1(CmsConstants.menuType, parent_id, restId,
					CmsConstants.restType, CmsConstants.restMenuName + restId);

			parent_id = JsonPath.read(p1.ResponseValidator.GetBodyAsText(), "$.data.id");
			p1 = obj.createRestaurantMenuMapPop1(CmsConstants.menuType, parent_id, restId, CmsConstants.catType,
					CmsConstants.catName + restId);
			parent_id = JsonPath.read(p1.ResponseValidator.GetBodyAsText(), "$.data.id");
			obj.createRestaurantMenuMapPop1(CmsConstants.menuType, parent_id, restId, CmsConstants.subCatType,
					CmsConstants.subCatName + restId);

			map = cmsHelper.getMenuIdForPop(restId);
			MenuId = Integer.parseInt(map.get(0).get("menu_id").toString());

		}
		List<Map<String, Object>> map1 = cmsHelper.getcatIdForPop(MenuId);
		catId = Integer.parseInt(map1.get(0).get("id").toString());
		List<Map<String, Object>> map2 = cmsHelper.getcatIdForPop(catId);
		sutId = Integer.parseInt(map2.get(0).get("id").toString());

		Integer[] ids = new Integer[3];
		ids[0] = restId;
		ids[1] = catId;
		ids[2] = sutId;
		return ids;

	}

	public Object[] createItemScheduledataByNewAreaSchedule(int ItemId) throws JSONException {
		POP_Area_Schedule_Helper obj = new POP_Area_Schedule_Helper();
		String date, day;
		int areaScheduleId = 0, priority = 1, area_id;
		date = LocalDate.now().toString();
		day = DayOfWeek.from(LocalDate.now()).toString().substring(0, 3);

		Map<String, Object> map = cmsHelper.getAreaIdbasedOnItem(ItemId + "");
		area_id = Integer.parseInt(map.get("area_Code").toString());
		List<Map<String, Object>> map2 = cmsHelper.areaSlotForPop(area_id, day);

		if (map2.size() > 0) {
			areaScheduleId = Integer.parseInt(map2.get(0).get("id").toString());
			int length = map2.size();
			System.out.println("areaScheduleID" + areaScheduleId);
			System.out.println("length" + length);
			for (int i = 1; i < length; i++)
				obj.deleteArea_schedule_helper(Integer.parseInt(map2.get(i).get("id").toString()));

			obj.updateArea_schedule_helper(CmsConstants.slotType, area_id, CmsConstants.openTime,
					CmsConstants.closeTime, day, CmsConstants.menu_type, areaScheduleId);
		} else
			obj.createArea_schedule_helper(CmsConstants.slotType, area_id, CmsConstants.openTime,
					CmsConstants.closeTime, day, CmsConstants.menu_type);

		List<Map<String, Object>> map3 = cmsHelper.scheduledItem(areaScheduleId, CmsConstants.cusine,
				CmsConstants.dish_type);
		if (map3.size() > 0) {
			int length = map3.size() - 1;
			priority = Integer.parseInt(map3.get(length).get("priority").toString()) + 1;
			System.out.println("length" + length + "priority" + priority);
		}

		return new Object[] { areaScheduleId, date, CmsConstants.display_sequence, CmsConstants.inventory,
				CmsConstants.cusine, CmsConstants.dish_type, priority };
	}

	public Processor PopItemCreation(int restId, String PopType) throws IOException, JSONException {
		System.out.println(
				"***************************************** Restaurant slot creation started *****************************************");
		CMSHelper cmsHelper = new CMSHelper();
		RestBuilder restBuilder = new RestBuilder();
		Integer[] ids = PopItemCreate(restId);
		AddItemToRestaurant itemToRestaurant = restBuilder.buildItemSlot();
		itemToRestaurant.setCategoryId(ids[2] + "");
		itemToRestaurant.setMainCategoryId(ids[1]);
		itemToRestaurant.setRestaurantId(restId);
		itemToRestaurant.setS3ImageUrl(MenuConstants.items_image_url);
		itemToRestaurant.setImageId(MenuConstants.image_id);
		itemToRestaurant.setType(CmsConstants.pop_item);

		if (PopType == "RAIN_ONLY")
			itemToRestaurant.setpop_usage_type(CmsConstants.popRainOnly);
		else if (PopType == "RAIN_AND_POP")
			itemToRestaurant.setpop_usage_type(CmsConstants.popAndRainBoth);
		else
			itemToRestaurant.setpop_usage_type(CmsConstants.popOnly);
		Processor p = cmsHelper.createItem(itemToRestaurant);

		return p;

	}

	public Processor ItemSchedule(int itemId) throws IOException, JSONException {
		System.out.println(
				"***************************************** Restaurant slot creation started *****************************************");
		Object[] obj = createItemScheduledataByNewAreaSchedule(itemId);
		// {ItemId,
		// areaScheduleId,date,display_sequence,inventory,cusine,dish_type,priority}
		Processor p1 = obj1.createItemScheduleByExistingArea_helper_PopV2(itemId, Integer.parseInt(obj[0].toString()),
				obj[1].toString(), Integer.parseInt(obj[2].toString()), Integer.parseInt(obj[3].toString()),
				obj[4].toString(), obj[5].toString(), obj[6].toString());
		String response = p1.ResponseValidator.GetBodyAsText();
		String ScheduleId = JsonPath.read(response, "$.data.id").toString();

        System.out.println("***********************************Created SCHEDULEID="+ScheduleId+"***********************************");

		return p1;

	}

}
