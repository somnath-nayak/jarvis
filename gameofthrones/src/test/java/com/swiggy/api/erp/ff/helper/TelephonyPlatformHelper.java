package com.swiggy.api.erp.ff.helper;

import com.swiggy.api.erp.ff.constants.TelephonyPlatformConstants;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.WireMockHelper;
import org.apache.commons.io.FileUtils;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TelephonyPlatformHelper implements TelephonyPlatformConstants{

    Initialize gameofthrones = new Initialize();

    WireMockHelper wireMockHelper = new WireMockHelper();

    public String getUniqueRequestId(){
        return String.valueOf(Instant.now().getEpochSecond());
    }

    public Processor ivrsRequest(String clientId, String requestId, String callerReasonId, String calleeId, String appId) {
        HashMap<String, String> header = new HashMap<String, String>();
        header.put("Content-Type", "application/json");
        header.put("client_id", clientId);

        GameOfThronesService service = new GameOfThronesService(SERVICE_TELEPHONY_PLATFORM, "ivrsrequest", gameofthrones);
        Processor processor = new Processor(header, new String[] {requestId, callerReasonId, calleeId, appId}, null, null, service);
        return processor;
    }

    public Processor getCallStatus(String client_id, String call_id) {
        HashMap<String, String> header = new HashMap<>();
        header.put("client_id",client_id);
        GameOfThronesService service = new GameOfThronesService(SERVICE_TELEPHONY_PLATFORM, "getcallstatus", gameofthrones);
        Processor processor = new Processor(header, null, new String[] {call_id}, null, service);
        return processor;
    }

    public Processor createIvrsApp(HashMap<String, String> header, String appName, String podName, String appUrl, String completedCallbackUrl, String failedCallbackUrl, String keyPressCallbackUrl) {
        String[] payload = {appName, podName, appUrl, completedCallbackUrl, failedCallbackUrl, keyPressCallbackUrl};
        GameOfThronesService service = new GameOfThronesService(SERVICE_TELEPHONY_PLATFORM, "createivrsapp", gameofthrones);
        Processor processor = new Processor(header, payload, null, null, service);
        return processor;
    }

    public Processor getIvrsAppById(String appId) {
        GameOfThronesService service = new GameOfThronesService(SERVICE_TELEPHONY_PLATFORM, "getivrsappbyid", gameofthrones);
        Processor processor = new Processor(null, null, new String[] {appId}, null, service);
        return processor;
    }

    public Processor exotelKeypressCallback(String callSid, String digits) {
        GameOfThronesService service = new GameOfThronesService(SERVICE_TELEPHONY_PLATFORM, "exotelkeypresscallback", gameofthrones);
        Processor processor = new Processor(null, null, new String[] {callSid, digits}, null, service);
        return processor;
    }

    public Processor exotelStatusCallback(String callSid, String status) {
        HashMap<String, String> formData = new HashMap<>();
        formData.put("CallSid", callSid);
        formData.put("Status", status);
        GameOfThronesService service = new GameOfThronesService(SERVICE_TELEPHONY_PLATFORM, "exotelstatuscallback", gameofthrones);
        Processor processor = new Processor(service, null, null, null, formData);
        return processor;
    }

    public Processor ubonaIvrsResponse(String eventName, String status) {

        HashMap<String, String> header = new HashMap<>();
        header.put("Content-Type", "application/json");

        String[] payload = {eventName, status};
        GameOfThronesService service = new GameOfThronesService(SERVICE_TELEPHONY_PLATFORM, "ubonaivrsresponse", gameofthrones);
        Processor processor = new Processor(header, payload, null, null, service);

        return processor;
    }

    private boolean verifyIdInDB(int id){
        boolean flag = false;
        try {
            List<Map<String, Object>> callRequests = SystemConfigProvider.getTemplate(TELEPHONY_DB_HOST).queryForList("select * from ivrs_call_request where id = "+ id  +";");
            return true;
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
            return flag;
        }
    }

    private boolean verifyAppIdInDB(int appId){
        boolean flag = false;
        try {
            List<Map<String, Object>> ivrsapp = SystemConfigProvider.getTemplate(TELEPHONY_DB_HOST).queryForList("select * from ivrs_app where id = "+ appId  +";");
            return true;
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
            return flag;
        }
    }

    private int getClientIdFromDB(int appId){
        int client_id = 0;
        try {
            List<Map<String, Object>> ivrsapp = SystemConfigProvider.getTemplate(TELEPHONY_DB_HOST).queryForList("select client_id from ivrs_app where id = "+ appId  +";");
            client_id = (Integer) ivrsapp.get(0).get("client_id");
            return client_id;
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
            return client_id;
        }
    }

    private int getStatusIdFromDB(int call_id){
        int status_id = 0;
        try {
            List<Map<String, Object>> call_status = SystemConfigProvider.getTemplate(TELEPHONY_DB_HOST).queryForList("select status_id from call_status where call_id = "+ call_id  +";");
            status_id = (Integer) call_status.get(0).get("status_id");
            return status_id;
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
            return status_id;
        }
    }

    public String getCallSidFromDB(int call_id){
        String provider_request_id = null;
        try {
            List<Map<String, Object>> call_status = SystemConfigProvider.getTemplate(TELEPHONY_DB_HOST).queryForList("select provider_request_id from ivrs_provider_request_mapping where call_id = "+ call_id  +";");
            provider_request_id = (String) call_status.get(0).get("provider_request_id");
            return provider_request_id;
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
            return provider_request_id;
        }
    }

    public void assertAPIResponses(Processor response, HashMap<String, String> expectedResponseMap) {
        SoftAssert softAssert = new SoftAssert();

        softAssert.assertEquals(response.ResponseValidator.GetResponseCode(), Integer.parseInt(expectedResponseMap.get("responseCode")), "Expected Response code did not match with actual");

        if (response.ResponseValidator.GetResponseCode() == 200){
            softAssert.assertEquals(response.ResponseValidator.GetNodeValue("code"), expectedResponseMap.get("code"), "Expected code did not match with the actual");
            softAssert.assertEquals(response.ResponseValidator.GetNodeValue("message"), expectedResponseMap.get("message"), "Expected message did not match with Actual");
            softAssert.assertAll();
        }
//
//        if(response.ResponseValidator.GetResponseCode() != 200){
//            softAssert.assertEquals(response.ResponseValidator.GetNodeValue("errors.error-field"), expectedResponseMap.get("errors.error_field"), "Expected error field did not match with Actual");
//            softAssert.assertEquals(response.ResponseValidator.GetNodeValue("errors.rejected_value"), expectedResponseMap.get("errors.rejected_value"), "Expected Rejected Value did not match with Actual");
//            softAssert.assertEquals(response.ResponseValidator.GetNodeValue("errors.message"), expectedResponseMap.get("errors.message"), "Expected errors.message did not match with actual");
//            softAssert.assertAll();
//        }
    }

    public void assertRegisterIvrsAppAPIResponses(Processor response, HashMap<String, String> expectedResponseMap){
        Assert.assertEquals(response.ResponseValidator.GetResponseCode(), Integer.parseInt(expectedResponseMap.get("responseCode")), "Actual responseCode did not match with expected");
        if(response.ResponseValidator.GetResponseCode() == 200){
            Assert.assertEquals(response.ResponseValidator.GetNodeValue("appName"), expectedResponseMap.get("appName"), "Actual appName did not match with expected");
            Assert.assertEquals(response.ResponseValidator.GetNodeValue("createdBy"), expectedResponseMap.get("createdBy"), "Actual appName did not match with expected");
        }
    }

    public void assertCallStatusAPIResponses(Processor response, HashMap<String, String> expectedResponseMap) {
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(response.ResponseValidator.GetResponseCode(), Integer.parseInt(expectedResponseMap.get("responseCode")), "Actual responseCode did not match with expected");
        softAssert.assertEquals(response.ResponseValidator.GetNodeValue("code"), expectedResponseMap.get("code"), "Actual code did not match with expected");
        softAssert.assertEquals(response.ResponseValidator.GetNodeValue("message"), expectedResponseMap.get("message"), "Actual message did not match with expected");
        softAssert.assertAll();
    }

    public void assertDataInDBAfterHittingAPI(int id){
        Assert.assertTrue(verifyIdInDB(id), "ID returned in the response is not present in call_requests table in DB " + id);
//        Assert.assertEquals(getStatusIdFromDB(id), 1, "Expected status was queued but did not match in DB " + id);
    }

    public void assertRegisterIvrsAppDataInDBAfterHittingAPI(int appId, int clientId){
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(verifyAppIdInDB(appId), "App id is not present in ivrs_app table of Telephony DB");
        softAssert.assertEquals(getClientIdFromDB(appId), clientId, "Client ID in API response did not match with the client_id in DB for appId = " + appId);
        softAssert.assertAll();
    }

    public String getCallID(String clientId, String requestId, String callerReasonId, String calleeId, String appId){
        return String.valueOf(ivrsRequest(clientId, requestId, callerReasonId, calleeId, appId).ResponseValidator.GetNodeValueAsInt("id"));
    }

    public void stubExotelCallStatusAPIResponse (String status) throws IOException, InterruptedException {
        System.out.println("reading file \n");
        File file = new File("../Data/MockAPI/ResponseBody/stubExotelStatusAPIResponse.xml");
        String body = FileUtils.readFileToString(file);
        body = body.replace("${status}", status);
        System.out.println("file read \n");
        wireMockHelper.setupStub("v1/Accounts/bundl/Calls/", 200, "application/xml", body,2980);
        GameOfThronesService service = new GameOfThronesService(WIREMOCK, "stubexotelstatusapi", gameofthrones);
        Processor processor = new Processor(null, null, null, null, service);
        System.out.println(processor.ResponseValidator.GetBodyAsText());
        Thread.sleep(1000);
    }

    public void stubExotelCreateIVRSAPIResponse() throws IOException, InterruptedException {
        File file = new File("../Data/MockAPI/ResponseBody/stubExotelCreateIVRSAPIResponse.xml");
        String resBody = FileUtils.readFileToString(file);
        resBody = resBody.replace("${callSid}", getUniqueRequestId());
        wireMockHelper.setupStubPost("v1/Accounts/bundl/Calls/connect", 200, "application/xml", resBody);
        GameOfThronesService service = new GameOfThronesService(WIREMOCK, "stubexotelcallapi", gameofthrones);
        Processor processor = new Processor(null, null, null, null, service);
        System.out.println(processor.ResponseValidator.GetBodyAsText());
    }

    public void stubClientAPIForCompletedCallback(String callId) throws IOException, InterruptedException {
        File file = new File("../Data/MockAPI/RequestBody/stubWebhookCompleted.json");
        String reqBody = FileUtils.readFileToString(file);
        reqBody = reqBody.replace("${callId}", callId);
        String resBody = reqBody;
        wireMockHelper.setupStubPost("completed", 200, "application/json",reqBody, resBody);
//        Thread.sleep(10000000);
    }

    public void stubClientAPIForFailedCallback(String callId) throws IOException, InterruptedException {
        File file = new File("../Data/MockAPI/RequestBody/stubWebhookFailed.json");
        String reqBody = FileUtils.readFileToString(file);
        reqBody = reqBody.replace("${callId}", callId);
        String resBody = reqBody;
        wireMockHelper.setupStubPost("failed", 200, "application/json", resBody);
//        Thread.sleep(10000000);
    }

    public void stubClientAPIForKeypressCallback(String callId, String digit) throws IOException, InterruptedException {
        File file = new File("../Data/MockAPI/RequestBody/stubWebhookKeypress.json");
        String reqBody = FileUtils.readFileToString(file);
        reqBody = reqBody.replace("${callId}", callId).replace("${digit}", digit);
        String resBody = reqBody;
        wireMockHelper.setupStubPost("keypress", 200, "application/json", resBody);
//        Thread.sleep(10000000);
    }
}
