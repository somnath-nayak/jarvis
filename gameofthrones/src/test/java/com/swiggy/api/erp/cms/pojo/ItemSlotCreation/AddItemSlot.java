
package com.swiggy.api.erp.cms.pojo.ItemSlotCreation;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "data",
    "user_meta"
})
public class AddItemSlot {

    @JsonProperty("data")
    private Data data;
    @JsonProperty("user_meta")
    private User_meta user_meta;


    @JsonProperty("data")
    public Data getData() {
        return data;
    }

    @JsonProperty("data")
    public void setData(Data data) {
        this.data = data;
    }

    @JsonProperty("user_meta")
    public User_meta getUser_meta() {
        return user_meta;
    }

    @JsonProperty("user_meta")
    public void setUser_meta(User_meta user_meta) {
        this.user_meta = user_meta;
    }


    public AddItemSlot build(int itemId) {
        setDefaultValues(itemId);
        return this;
    }

    public void setDefaultValues(int itemId) {
        Data data=new Data();
        data.build(itemId);

        User_meta user_meta = new User_meta();
        user_meta.build();
        this.setData(data);
        this.setUser_meta(user_meta);

    }

}
