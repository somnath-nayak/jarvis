package com.swiggy.api.erp.cms.pojo.ItemHolidaySlot;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;

public class Data1 {

    private String id;
    @JsonProperty("item_id")
    private Integer itemId;
    @JsonProperty("from_time")
    private String fromTime;
    @JsonProperty("to_time")
    private String toTime;

    /**
     * No args constructor for use in serialization
     *
     */
    public Data1() {
    }

    /**
     * @param id
     * @param fromTime
     * @param itemId
     * @param toTime
     */
    public Data1( String id,Integer itemId, String fromTime, String toTime) {
        super();
        this.id=id;
        this.itemId = itemId;
        this.fromTime = fromTime;
        this.toTime = toTime;
    }

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String getToTime() {
        return toTime;
    }

    public void setToTime(String toTime) {
        this.toTime = toTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("itemId", itemId).append("fromTime", fromTime).append("toTime", toTime).toString();
    }

}
