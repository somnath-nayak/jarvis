package com.swiggy.api.erp.vms.editservice.pojo;


import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"group_id",
"variation"
})
public class Variant {

@JsonProperty("group_id")
private long group_id;
@JsonProperty("variation")
private List<Long> variation = null;

/**
* No args constructor for use in serialization
* 
*/
public Variant() {
}

/**
* 
* @param variation
* @param group_id
*/
public Variant(long group_id, List<Long> variation) {
super();
this.group_id = group_id;
this.variation = variation;
}

@JsonProperty("group_id")
public long getGroup_id() {
return group_id;
}

@JsonProperty("group_id")
public void setGroup_id(long group_id) {
this.group_id = group_id;
}

public Variant withGroup_id(long group_id) {
this.group_id = group_id;
return this;
}

@JsonProperty("variation")
public List<Long> getVariation() {
return variation;
}

@JsonProperty("variation")
public void setVariation(List<Long> variation) {
this.variation = variation;
}

public Variant withVariation(List<Long> variation) {
this.variation = variation;
return this;
}

@Override
public String toString() {
return new ToStringBuilder(this).append("group_id", group_id).append("variation", variation).toString();
}

@Override
public int hashCode() {
return new HashCodeBuilder().append(variation).append(group_id).toHashCode();
}

@Override
public boolean equals(Object other) {
if (other == this) {
return true;
}
if ((other instanceof Variant) == false) {
return false;
}
Variant rhs = ((Variant) other);
return new EqualsBuilder().append(variation, rhs.variation).append(group_id, rhs.group_id).isEquals();
}

}