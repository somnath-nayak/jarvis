
package com.swiggy.api.erp.cms.pojo.ItemSlotCreation;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "meta",
    "source"
})
public class Created_by {

    @JsonProperty("meta")
    private Meta meta;
    @JsonProperty("source")
    private String source;


    @JsonProperty("meta")
    public Meta getMeta() {
        return meta;
    }

    @JsonProperty("meta")
    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    @JsonProperty("source")
    public String getSource() {
        return source;
    }

    @JsonProperty("source")
    public void setSource(String source) {
        this.source = source;
    }

    public Created_by build() {
        setDefaultValues();
        return this;
    }

    public void setDefaultValues() {
        Meta meta=new Meta();
        meta.build();

        if(this.source==null){
            this.source= "CMS";
        }
        setMeta(meta);
    }


}
