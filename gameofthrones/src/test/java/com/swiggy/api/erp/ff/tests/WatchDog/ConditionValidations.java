package com.swiggy.api.erp.ff.tests.WatchDog;


import com.google.gson.JsonParseException;
import com.jayway.jsonpath.JsonPath;
import com.rabbitmq.client.AMQP;
import com.swiggy.api.erp.ff.POJO.WatchDog.ActionContext;
import com.swiggy.api.erp.ff.constants.LosConstants;
import com.swiggy.api.erp.ff.constants.WatchDogConstants;
import com.swiggy.api.erp.ff.dp.WatchDog.ConditionData;
import com.swiggy.api.erp.ff.helper.ConditionValidationHelper;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import framework.gameofthrones.Tyrion.RabbitMQHelper;
import jodd.io.FileUtil;
import org.apache.commons.io.FileUtils;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class ConditionValidations {

JsonHelper j = new JsonHelper();
Processor processor;
ConditionValidationHelper cvh = new ConditionValidationHelper();
RabbitMQHelper rmqHelper = new RabbitMQHelper();

    @Test(dataProvider="registerCondtion",dataProviderClass = ConditionData.class,description="registering a condition")
    public void conditionRegistration(String condition)
    {
      processor = cvh.getRegisterCondtionProcessor(condition);

      String response = processor.ResponseValidator.GetBodyAsText();

      String code = JsonPath.read(response,"$.code");

      String conditionId = JsonPath.read(response,"$.id");

      int stausCode = processor.RequestValidator.GetResponseCode();

      Assert.assertEquals(stausCode,200);

      Assert.assertEquals(code,"success");

      Assert.assertNotNull(conditionId);

      Assert.assertEquals(cvh.dbValidtion(conditionId),true);


    }

    @Test(dataProvider="evlauateCondtion",dataProviderClass = ConditionData.class,description="evaluating a condition")
    public void conditionEvalution(String conditionId,String condition)
    {
        processor = cvh.getEvaluateCondtionProcessor(conditionId,condition);
        String response = processor.ResponseValidator.GetBodyAsText();

        String code = JsonPath.read(response,"$.code");
        String data = JsonPath.read(response,"$.data");


        int stausCode = processor.RequestValidator.GetResponseCode();

        Assert.assertEquals(stausCode,200);

        Assert.assertEquals(code,"success");

        Assert.assertNotNull(data);


    }

    @Test(dataProvider="ecaData",dataProviderClass = ConditionData.class,description="ECA Creation")
    public void ecaCreateion(String groupname,String state,String conditionId, String Condition, ActionContext Action) throws IOException
    {

       String ac = j.getObjectToJSON(Action);
       processor = cvh.getECAProcessor(groupname,state,conditionId,Condition,ac);
       String response = processor.ResponseValidator.GetBodyAsText();

        String code = JsonPath.read(response,"$.code");
        int stausCode = processor.RequestValidator.GetResponseCode();

        Assert.assertEquals(stausCode,200);

        Assert.assertEquals(code,"success");

    }

    @Test(dataProvider="ecaData",dataProviderClass = ConditionData.class,description="ECA ValidationN")
    public void ecaValidation(String orderId,String groupname,String state,String conditionId, String Condition, ActionContext Action) throws IOException {
        /** creating a ECA **/
        ecaCreateion(groupname,state,conditionId,Condition,Action);


        File file = new File("../Data/Payloads/JSON/losCreateManualOrder.json");
        String message = FileUtils.readFileToString(file).replace("${orderID}",orderId);
        rmqHelper.pushMessageToExchange(WatchDogConstants.hostName, WatchDogConstants.queueName, new AMQP.BasicProperties().builder().contentType("application/json"), message);

        /** checking in table status **/

          List<Map<String,Object>> l = cvh.getWatchDogDB(orderId);
          Assert.assertEquals(sizeValidation(l),true);
         String WatchDogState = l.get(0).get("state").toString();
         Assert.assertEquals(WatchDogState,"COMPLETED");



    }

    public boolean sizeValidation(List l)
    {
        return l.size() != 0;
    }






}
