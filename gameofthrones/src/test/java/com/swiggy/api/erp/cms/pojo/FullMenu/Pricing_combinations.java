package com.swiggy.api.erp.cms.pojo.FullMenu;

import com.swiggy.api.erp.cms.constants.MenuConstants;

/**
 * Created by kiran.j on 2/2/18.
 */
public class Pricing_combinations
{
    private Variant_combination[] variant_combination;

    private double price;

    private Addon_combination[] addon_combination;

    public Variant_combination[] getVariant_combination ()
    {
        return variant_combination;
    }

    public void setVariant_combination (Variant_combination[] variant_combination)
    {
        this.variant_combination = variant_combination;
    }

    public double getPrice ()
    {
        return price;
    }

    public void setPrice (double price)
    {
        this.price = price;
    }

    public Addon_combination[] getAddon_combination ()
    {
        return addon_combination;
    }

    public void setAddon_combination (Addon_combination[] addon_combination)
    {
        this.addon_combination = addon_combination;
    }

    public Pricing_combinations build() {
        setDefaultValues();
        return this;
    }

    public void setDefaultValues() {
        Addon_combination addon_combination = new Addon_combination();
        addon_combination.build();
        Variant_combination variant_combination = new Variant_combination();
        variant_combination.build();
        if(this.getVariant_combination() == null)
            this.setVariant_combination(new Variant_combination[]{variant_combination});
        if(this.getAddon_combination() == null)
            this.setAddon_combination(new Addon_combination[]{addon_combination});

        this.setPrice(MenuConstants.default_price);
    }

    

    @Override
    public String toString()
    {
        return "{ variant_combination = "+variant_combination+", price = "+price+", addon_combination = "+addon_combination+"}";
    }
}
