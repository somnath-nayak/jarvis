package com.swiggy.api.erp.delivery.dp;

import com.swiggy.api.erp.delivery.helper.DeliveryDataHelper;
import org.testng.annotations.DataProvider;

import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;

import framework.gameofthrones.JonSnow.Processor;

public class CAPRDFromExotelDataProvider {
	
	DeliveryDataHelper deliveryDataHelper=new DeliveryDataHelper();
	String restaurantId;
	String restaurantLat;
	String restaurantLon;
	public CAPRDFromExotelDataProvider(String restaurantId,String restaurantLat, String restaurantLon)
	{
		this.restaurantId=restaurantId;
		this.restaurantLat=restaurantLat;
		this.restaurantLon=restaurantLon;
	}
	public CAPRDFromExotelDataProvider()
	{

	}

/*	public static String createOrder() throws Exception {
		CheckoutHelper helperc = new CheckoutHelper();
		Object[][] data = cartItems();
		Processor p = helperc.placeOrder(DeliveryConstant.mobile,
				DeliveryConstant.password, data[0][0].toString(),
				data[0][1].toString(), data[0][2].toString());
		return order_id = p.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.order_id");
	}*/

	public static Object[][] cartItems() {

		return new Object[][] { { "815791", "2", "4977" } };

	}
	@DataProvider(name = "QE-349")
	public Object[][] CAPRDExotel349() throws Exception {
		String order_id=deliveryDataHelper.getOrderIdForCAPRDFF(restaurantId,restaurantLat,restaurantLon);
		return new Object[][] {
			
		{order_id,"2418","1.8.2.1"}

		};
	}
	@DataProvider(name = "QE-350")
	public Object[][] CAPRDExotel350() throws Exception {
		String order_id=deliveryDataHelper.getOrderIdForCAPRDFF(restaurantId,restaurantLat,restaurantLon);
		return new Object[][] {
			
		{order_id,"2418","1.8.2.1"}

		};
	}
	@DataProvider(name = "QE-351")
	public Object[][] CAPRDExotel351() throws Exception {
		String order_id=deliveryDataHelper.getOrderIdForCAPRDFF(restaurantId,restaurantLat,restaurantLon);
		return new Object[][] {
			
		{order_id,"2418","1.8.2.1"}

		};
	}
	@DataProvider(name = "QE-352")
	public Object[][] CAPRDExotel352() throws Exception {
		String order_id=deliveryDataHelper.getOrderIdForCAPRDFF(restaurantId,restaurantLat,restaurantLon);
		return new Object[][] {
		{order_id,"2418","1.8.2.1"}

		};
	}
}
