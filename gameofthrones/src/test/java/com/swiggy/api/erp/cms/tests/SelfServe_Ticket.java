package com.swiggy.api.erp.cms.tests;

import org.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.constants.CmsConstants;
import com.swiggy.api.erp.cms.dp.AgentTicketingDP;
import com.swiggy.api.erp.cms.helper.SelfServiceHelper;

import framework.gameofthrones.JonSnow.Processor;

/*
 *   Create menu-holiday slots for a given menu-id for a restaurant
 */

public class SelfServe_Ticket extends AgentTicketingDP {
	
	SelfServiceHelper obj =  new SelfServiceHelper();
	String restaurant_id = CmsConstants.restaurantId_selfserve;
	
	
	
	@Test(dataProvider="selfserveTicket", priority=0, groups={"Sanity_TC", "Smoke_TC", "Regression_TC"}, 
			description="Creates a self serve ticket")
	public void createSelfServeTicket(String restaurantId)
			throws JSONException, InterruptedException
	{
		  restaurantId = restaurant_id;
		  Processor p1 = obj.createTicket(restaurantId);
		  
		  String response = p1.ResponseValidator.GetBodyAsText();
		  int statusCode = JsonPath.read(response, "$.statusCode");
		  Assert.assertEquals(statusCode, 1);
		  String statusMessage = JsonPath.read(response, "$.statusMessage");
		  Assert.assertEquals(statusMessage, "success");
		  
		  boolean data =  JsonPath.read(response, "$.data");
		  Assert.assertEquals(data, true);
	}

	@Test(priority=1, groups={"Sanity_TC", "Smoke_TC", "Regression_TC"}, 
			description="Creates a self serve ticket")
	public void validateTicketDetails()
			throws JSONException, InterruptedException
	{
		  obj.getTicketDetailsFromDB();
	}

	
	
	
	
}
