package com.swiggy.api.erp.delivery.constants;

public interface DSListingConstant{
	
	String DS_listing_lat="12.9818";
	String DS_listing_lng="77.63819999999998";
	String DS_listing_city_id="1";
	String DS_listing_rest_id="244";
	String DS_non_listing_rest_id="291";
	String DS_non_listing_lat="12.926815";
	String DS_non_listing_lng="77.62947099999997";
	String DS_zone="2";
	String DS_zone_2="1";
	String DS_avgA2D_key="AveargeArrivalToDEPerZone#";
	String max_order_key="DS:ZONE_MAX_ORDER_VOLUME_";
	String max_order_capacity="DS:ZONE_MAX_ORDER_CAPACITY_";
	String is_zone_serviveable="DS:ZONE_IS_SERVICEABLE_";
	String get_DE_inside_zone="WR_ZONE_ACTIVE_DE_INSIDE_V2_";
	String DS_set_lessA2D="{\"value\":20.0,\"window\":{\"start\":1516622983353,\"end\":1516622983353}}";
	String DS_set_moreA2D="{\"value\":70.0,\"window\":{\"start\":1516622983353,\"end\":1516622983353}}";
	String DS_normal_restid="8241";
	String DS_normal_lat="12.9738842";
	String DS_normal_lng="77.6382973";
	
	
	}


