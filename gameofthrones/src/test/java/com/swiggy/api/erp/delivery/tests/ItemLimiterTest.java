package com.swiggy.api.erp.delivery.tests;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.delivery.Pojos.Last_miles;
import com.swiggy.api.erp.delivery.Pojos.Restaurant;
import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.erp.delivery.constants.RTSConstants;
import com.swiggy.api.erp.delivery.dp.ItemLimiter_DataProvider;
import com.swiggy.api.erp.delivery.helper.CartSLA_Helper;
import com.swiggy.api.erp.delivery.helper.DeliveryHelperMethods;

import framework.gameofthrones.JonSnow.Processor;

public class ItemLimiterTest extends ItemLimiter_DataProvider {

	DeliveryHelperMethods delmeth = new DeliveryHelperMethods();
	CartSLA_Helper serhelp = new CartSLA_Helper();

	@BeforeClass
	public void set() {
		delmeth.dbhelperupdate(DeliveryConstant.lastmile_more_query+ "4");
		delmeth.dbhelperupdate(DeliveryConstant.max_sla_more_query+"4");
		Object item_idd=delmeth.dbhelperget(RTSConstants.get_max_items_cart_key_query,"id");
		if(null==item_idd)
		{
		Object id=delmeth.dbhelperget(RTSConstants.last_id_in_delivery_config, "id");
		String query1=RTSConstants.insert_max_item_cart_key+(Integer.parseInt(id.toString())+1)+",'"+RTSConstants.max_items_cart_key+"',"+RTSConstants.item_max_count+","+"'test'"+")";
        delmeth.dbhelperupdate(query1);
		}
		Object total_item_idd=delmeth.dbhelperget(RTSConstants.get_max_total_items_cart_query,"id");
		if(null==total_item_idd)
		{
		Object id=delmeth.dbhelperget(RTSConstants.last_id_in_delivery_config, "id");
        String query2=RTSConstants.insert_max_item_cart_key+(Integer.parseInt(id.toString())+1)+",'"+RTSConstants.max_total_items_cart_key+"',"+RTSConstants.item_max_count+","+"'test'"+")";
        delmeth.dbhelperupdate(query2);
		}
        delmeth.dbhelperupdate(RTSConstants.set_max_item_cart_more);
		delmeth.dbhelperupdate(RTSConstants.set_max_total_item_cart_more);
	}

	@Test(priority = 1, groups = "Item_Limiter", description = "Verify user able to create cart if max_total_items_cart and max_items_cart is less than configured limit")
	public void IL_1() throws InterruptedException, IOException {
		Processor processor = validaterequest(RTSConstants.default_listing_item_count_more, RTSConstants.total_item_count_less);
		String serviceability = getserviceability(processor);
		Assert.assertEquals(serviceability, "2");	
		}
	@Test(dataProvider ="Item_limit_2",priority = 2, groups = "Item_Limiter", description = "Verify user is not able to create cart if max_total_items_cart is less than configured limit and max_items_cart is more than configured limit")
	public void IL_2() throws InterruptedException, IOException {
		Thread.sleep(10000);
		Processor processor = validaterequest(RTSConstants.max_item_count_value_more,RTSConstants.total_item_count_less);
		String serviceability = getserviceability(processor);
		String non_service_reason=getnonserviceabilityreason(processor);
		Assert.assertEquals(serviceability, "0");	
		Assert.assertEquals(non_service_reason, "2");
		}
	@Test(dataProvider ="Item_limit_3",priority =3, groups = "Item_Limiter", description = "Verify user is not able to create cart if max_total_items_cart is more than configured limit and max_items_cart is less than configured limit")
	public void IL_3() throws InterruptedException, IOException {
		Thread.sleep(10000);
		Processor processor = validaterequest(RTSConstants.max_item_count_value_less,RTSConstants.total_item_count_more);
		String serviceability = getserviceability(processor);
		String non_service_reason=getnonserviceabilityreason(processor);
		Assert.assertEquals(serviceability, "0");	
		Assert.assertEquals(non_service_reason, "2");
		}
	@Test(dataProvider ="Item_limit_4",priority =4, groups = "Item_Limiter", description = "Verify user is not able to create cart if both max_total_items_cart and max_items_cart are more than configured limit")
	public void IL_4() throws InterruptedException, IOException {
		Thread.sleep(10000);
		Processor processor = validaterequest(RTSConstants.max_item_count_value_more,RTSConstants.total_item_count_more);
		String serviceability = getserviceability(processor);
		String non_service_reason=getnonserviceabilityreason(processor);
		Assert.assertEquals(serviceability, "0");	
		Assert.assertEquals(non_service_reason, "2");
		}
	@Test(dataProvider ="Item_limit_5",priority =5, groups = "Item_Limiter", description = "Verify user is able to create cart succesfully even if no of items are more if max_items_cart and max_total_items_cart keys are not there")
	public void IL_5() throws InterruptedException, IOException {
		Thread.sleep(10000);
		validaterequest(RTSConstants.max_item_count_value_more,RTSConstants.total_item_count_more);
	    Processor processor = validaterequest(RTSConstants.max_item_count_value_more,RTSConstants.total_item_count_more);
		String serviceability = getserviceability(processor);
		Assert.assertEquals(serviceability, "2");	
		}
	public Processor validaterequest(String max_item_count,String total_item_count_less) throws IOException
	{
		Restaurant restaurant = new Restaurant();
		restaurant.defaultresultset();
		Last_miles last_mile = new Last_miles();
		last_mile.setdefaultvalues();
		Last_miles[] last = new Last_miles[1];
		last[0] = last_mile;
		serhelp.cartslaapi(restaurant,
				max_item_count, last,
				RTSConstants.bill_amount_less, "",
				String.valueOf(RTSConstants.cart_id),
				RTSConstants.cart_state_create,
				total_item_count_less);
		Processor processor = serhelp.cartslaapi(restaurant,
				max_item_count, last,
				RTSConstants.bill_amount_less, "",
				String.valueOf(RTSConstants.cart_id),
				RTSConstants.cart_state_create,
				total_item_count_less);
		return processor;
	}
		public String getserviceability(Processor processor)
		{
		Object ser = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),
				"$.cart..serviceable");
		String serviceability = ser.toString().replace("[", "").replace("]", "");
		return serviceability;
		}
		public String getnonserviceabilityreason(Processor processor)
		{
			Object non_ser = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),
					"$.cart..non_serviceable_reason");
			String non_service_reason=non_ser.toString().replace("[", "").replace("]", "");
		return non_service_reason;
		}
		}
		


