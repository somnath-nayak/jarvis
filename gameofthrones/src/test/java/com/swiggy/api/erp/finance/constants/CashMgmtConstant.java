package com.swiggy.api.erp.finance.constants;

public interface CashMgmtConstant {

    String ACTIVE_DE="1";
    String INACTIVE_DE= "2";
    String INVALID_DE="0";
    int DE_COUNT=3;
    String INVALID_SESSION="0";
    String CASH_SESSION_DISBURSE="disburse_cash_tx";
    String CASH_SESSION_COLLECT="collect_cash_tx";
    String PAYOUT_ADJUSTMENT="payout_adjustment_tx";
    String MANUAL_TRANSACTION="order_tx";
    String CASH_INCOMING="cash_incoming";
    String CASH_SPENDING="cash_spending";
    String POCKETING_TRANSACTION="pocket_tx";
    String CASH_RMQHOSTNAME="oms";
    String CASH_ORDERSTATUSQUEUE="cash.order_status_2";
    String get_LFCL_UFLC = "SELECT LFCL,UFCL FROM cash.zone WHERE id=";

    String get_de_cash_session_details = "SELECT * FROM cash.de_cash_sessions WHERE de_id=${DE_ID} AND STATE IN (\"OPEN\")";
    String cashDB = "finance_cash_db";
    String deliveryDB = "deliverydb";
    String oms_queue = "oms";
    String zone_exchange = "swiggy.zone_update";
    String zone_queue = "fin_zone_update";
    String get_inactive_de_id = "SELECT * FROM cash.delivery_boys WHERE employment_status=3 LIMIT 1";
    String get_zone_level_fc_de_id = "SELECT de_id,floating_cash FROM cash.de_cash_sessions WHERE de_id in(SELECT id FROM delivery_boys WHERE zone_id=${ZONEID}) AND state=\'OPEN\'";
    String msg_success = "Success";
    int statusOne = 1;
    int statusZero = 0;
    int status200 = 200;
    int status400 = 400;
    String msg_deID_not_found = "DE Id not found";
    String msg_zoneID_not_found ="Invalid zone id, it doesn't exist";
    String get_zone_cash_overview = "SELECT * FROM cash.zone_cash WHERE zone_id=";
    String get_zone_cash_log_from_zoneCashAudit = "SELECT * FROM cash.zone_cash_audit WHERE zone_cash_id=${ZONEID}";
    String msg_invalid_request_params = "Invalid/missing request parameters";
    String get_force_logout_status_of_de = "SELECT force_logout FROM cash.delivery_boys WHERE id=";
    String get_de_cash_transactions = "SELECT * FROM cash.de_cash_transactions WHERE de_id=${DEID} AND order_id=${ORDERID}";
    String msg_de_id_not_found="de id not found";
    String get_floating_cash_limit_zone = "SELECT floating_cash_limit FROM cash.zone WHERE id=";
    String msg_no_open_session = "No open session found for delivery boy.";
    String path_txt = System.getProperty("user.dir") + "/Data/validateReportCityHub.txt";
    String get_city_id = "SELECT city_id FROM cash.zone WHERE id=";
    String get_zone_name = "SELECT name FROM cash.zone WHERE id=";
    String get_all_zones_for_city_id = "SELECT id,name FROM cash.zone WHERE city_id=";
    String get_hub_city_report = "SELECT * FROM cash.zone_cash WHERE zone_id=${ZONEID} AND date=\"${DATE}\"";
    String update_timestamps_in_trips_table = "UPDATE delivery.trips SET ordered_time=\"${ORDEREDTIME}\", received_time=\"${RECEIVEDTIME}\", placed_time=\"${PLACEDTIME}\", assigned_time=\"${ASSIGNEDTIME}\", confirmed_time=\"${CONFIRMEDTIME}\", arrived_time=\"${ARRIVEDTIME}\", pickedup_time=\"${PICKEDUPTIME}\",delivered_time=\"${DELIVEREDTIME}\", de_id=\"${DEID}\" where order_id=\"${ORDERID}\";";
    String get_pay_per_order="SELECT payout_per_order from cash.city WHERE id=";
    String INVALID_CHANNELID="73737";
    String VALID_CHANNELID="1";

}
