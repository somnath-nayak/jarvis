
package com.swiggy.api.erp.delivery.Pojos;

import com.swiggy.api.erp.delivery.constants.CandidateConstant;
import com.swiggy.api.erp.delivery.helper.CandidateHelper;

public class Candidate {

    private String idfy_reference_id;

    public String getidfy_reference_id() { return this.idfy_reference_id; }

    public void setidfy_reference_id(String idfy_reference_id) { this.idfy_reference_id = idfy_reference_id; }

    private String first_name;

    public String getfirst_name() { return this.first_name; }

    public void setfirst_name(String first_name) { this.first_name = first_name; }

    private String last_name;

    public String getlast_name() { return this.last_name; }

    public void setlast_name(String last_name) { this.last_name = last_name; }

    private String dob;

    public String getDob() { return this.dob; }

    public void setDob(String dob) { this.dob = dob; }

    private String contact_number;

    public String getcontact_number() { return this.contact_number; }

    public void setcontact_number(String contact_number) { this.contact_number = contact_number; }

    private String fathers_name;

    public String getfathers_name() { return this.fathers_name; }

    public void setfathers_name(String fathers_name) { this.fathers_name = fathers_name; }

    private String pan_no;

    public String getpan_no() { return this.pan_no; }

    public void setpan_no(String pan_no) { this.pan_no = pan_no; }

    private String aadhaar_no;

    public String getaadhaar_no() { return this.aadhaar_no; }

    public void setaadhaar_no(String aadhaar_no) { this.aadhaar_no = aadhaar_no; }

    private String voter_id_no;

    public String getvoter_id_no() { return this.voter_id_no; }

    public void setvoter_id_no(String voter_id_no) { this.voter_id_no = voter_id_no; }

    private String p_street_address;

    public String getp_street_address() { return this.p_street_address; }

    public void setp_street_address(String p_street_address) { this.p_street_address = p_street_address; }

    private String p_city;

    public String getp_city() { return this.p_city; }

    public void setp_city(String p_city) { this.p_city = p_city; }

    private String p_state;

    public String getp_state() { return this.p_state; }

    public void setp_state(String p_state) { this.p_state = p_state; }

    private String p_pin_code;

    public String getp_pin_code() { return this.p_pin_code; }

    public void setp_pin_code(String p_pin_code) { this.p_pin_code = p_pin_code; }

    private String c_street_address;

    public String getc_street_address() { return this.c_street_address; }

    public void setc_street_address(String c_street_address) { this.c_street_address = c_street_address; }

    private String c_city;

    public String getC_city() { return this.c_city; }

    public void setC_city(String c_city) { this.c_city = c_city; }

    private String c_state;

    public String getc_state() { return this.c_state; }

    public void setc_state(String c_state) { this.c_state = c_state; }

    private String c_pin_code;

    public String getc_pin_code() { return this.c_pin_code; }

    public void setc_pin_code(String c_pin_code) { this.c_pin_code = c_pin_code; }

    private String mobile_device_type;

    public String getmobile_device_type() { return this.mobile_device_type; }

    public void setmobile_device_type(String mobile_device_type) { this.mobile_device_type = mobile_device_type; }

    private String languages_known;

    public String getlanguages_known() { return this.languages_known; }

    public void setlanguages_known(String languages_known) { this.languages_known = languages_known; }

    private String education_level;

    public String geteducation_level() { return this.education_level; }

    public void seteducation_level(String education_level) { this.education_level = education_level; }

    private String primary_source;

    public String getprimary_source() { return this.primary_source; }

    public void setprimary_source(String primary_source) { this.primary_source = primary_source; }

    private String agency_name;

    public String getagency_name() { return this.agency_name; }

    public void setagency_name(String agency_name) { this.agency_name = agency_name; }

    private String referrer_name;

    public String getreferrer_name() { return this.referrer_name; }

    public void setreferrer_name(String referrer_name) { this.referrer_name = referrer_name; }

    private String referrer_de_id;

    public String getreferrer_de_id() { return this.referrer_de_id; }

    public void setreferrer_de_id(String referrer_de_id) { this.referrer_de_id = referrer_de_id; }

    private String shift_type;

    public String getshift_type() { return this.shift_type; }

    public void setshift_type(String shift_type) { this.shift_type = shift_type; }

    private String primary_area;

    public String getprimary_area() { return this.primary_area; }

    public void setprimary_area(String primary_area) { this.primary_area = primary_area; }

    private String secondary_area;

    public String getsecondary_area() { return this.secondary_area; }

    public void setsecondary_area(String secondary_area) { this.secondary_area = secondary_area; }

    private String emergency_contact_number;

    public String getemergency_contact_number() { return this.emergency_contact_number; }

    public void setemergency_contact_number(String emergency_contact_number) { this.emergency_contact_number = emergency_contact_number; }

    private String relation_with_emergency_contact;

    public String getrelation_with_emergency_contact() { return this.relation_with_emergency_contact; }

    public void setrelation_with_emergency_contact(String relation_with_emergency_contact) { this.relation_with_emergency_contact = relation_with_emergency_contact; }

    private String blood_group;

    public String getblood_group() { return this.blood_group; }

    public void setblood_group(String blood_group) { this.blood_group = blood_group; }

    private String marital_status;

    public String getmarital_status() { return this.marital_status; }

    public void setmarital_status(String marital_status) { this.marital_status = marital_status; }

    private String bag_qr_code;

    public String getbag_qr_code() { return this.bag_qr_code; }

    public void setbag_qr_code(String bag_qr_code) { this.bag_qr_code = bag_qr_code; }

    private String account_holder_name;

    public String getaccount_holder_name() { return this.account_holder_name; }

    public void setaccount_holder_name(String account_holder_name) { this.account_holder_name = account_holder_name; }

    private String account_number;

    public String getaccount_number() { return this.account_number; }

    public void setaccount_number(String account_number) { this.account_number = account_number; }

    private String ifsc_code;

    public String getifsc_code() { return this.ifsc_code; }

    public void setifsc_code(String ifsc_code) { this.ifsc_code = ifsc_code; }

    private String bank_name;

    public String getbank_name() { return this.bank_name; }

    public void setbank_name(String bank_name) { this.bank_name = bank_name; }

    private String security_deposit;

    public String getsecurity_deposit() { return this.security_deposit; }

    public void setsecurity_deposit(String security_deposit) { this.security_deposit = security_deposit; }

    private String rc_number;

    public String getrc_number() { return this.rc_number; }

    public void setrc_number(String rc_number) { this.rc_number = rc_number; }

    private String rc_type;

    public String getrc_type() { return this.rc_type; }

    public void setrc_type(String rc_type) { this.rc_type = rc_type; }

    private String bike_model;

    public String getbike_model() { return this.bike_model; }

    public void setbike_model(String bike_model) { this.bike_model = bike_model; }

    private String bike_registration_number;

    public String getbike_registration_number() { return this.bike_registration_number; }

    public void setbike_registration_number(String bike_registration_number) { this.bike_registration_number = bike_registration_number; }

    private String dl_number;

    public String getdl_number() { return this.dl_number; }

    public void setdl_number(String dl_number) { this.dl_number = dl_number; }

    private String dl_type;

    public String getdl_type() { return this.dl_type; }

    public void setdl_type(String dl_type) { this.dl_type = dl_type; }

    private String dl_address;

    public String getdl_address() { return this.dl_address; }

    public void setdl_address(String dl_address) { this.dl_address = dl_address; }

    private String dl_state;

    public String getdl_state() { return this.dl_state; }

    public void setdl_state(String dl_state) { this.dl_state = dl_state; }

    private String dl_issued_on;

    public String getdl_issued_on() { return this.dl_issued_on; }

    public void setdl_issued_on(String dl_issued_on) { this.dl_issued_on = dl_issued_on; }

    private String dl_valid_upto;

    public String getdl_valid_upto() { return this.dl_valid_upto; }

    public void setdl_valid_upto(String dl_valid_upto) { this.dl_valid_upto = dl_valid_upto; }

    private String city;

    public String getcity() { return this.city; }

    public void setcity(String city) { this.city = city; }

    private String photograph;

    public String getPhotograph() { return this.photograph; }

    public void setPhotograph(String photograph) { this.photograph = photograph; }

    private String proof_address;

    public String getproof_address() { return this.proof_address; }

    public void setproof_address(String proof_address) { this.proof_address = proof_address; }

    private String proof_dl;

    public String getproof_dl() { return this.proof_dl; }

    public void setproof_dl(String proof_dl) { this.proof_dl = proof_dl; }

    private String proof_rc;

    public String getproof_rc() { return this.proof_rc; }

    public void setproof_rc(String proof_rc) { this.proof_rc = proof_rc; }

    private String proof_pan;

    public String getproof_pan() { return this.proof_pan; }

    public void setproof_pan(String proof_pan) { this.proof_pan = proof_pan; }

    private String bicycle;

    public String getBicycle() { return this.bicycle; }

    public void setBicycle(String bicycle) { this.bicycle = bicycle; }

    private String sim_number;

    public String getsim_number() { return this.sim_number; }

    public void setsim_number(String sim_number) { this.sim_number = sim_number; }

    private String t_shirt_size;

    public String gett_shirt_size() { return this.t_shirt_size; }

    public void sett_shirt_size(String t_shirt_size) { this.t_shirt_size = t_shirt_size; }

    private String t_shirt_quantity;

    public String gett_shirt_quantity() { return this.t_shirt_quantity; }

    public void sett_shirt_quantity(String t_shirt_quantity) { this.t_shirt_quantity = t_shirt_quantity; }

    private String raincoat_size;

    public String getraincoat_size() { return this.raincoat_size; }

    public void setraincoat_size(String raincoat_size) { this.raincoat_size = raincoat_size; }

    private String raincoat_quantity;

    public String getraincoat_quantity() { return this.raincoat_quantity; }

    public void setraincoat_quantity(String raincoat_quantity) { this.raincoat_quantity = raincoat_quantity; }

    private String jacket_size;

    public String getjacket_size() { return this.jacket_size; }

    public void setjacket_size(String jacket_size) { this.jacket_size = jacket_size; }

    private String jacket_quantity;

    public String getjacket_quantity() { return this.jacket_quantity; }

    public void setjacket_quantity(String jacket_quantity) { this.jacket_quantity = jacket_quantity; }

    private String pan_type;

    public String getpan_type() { return this.pan_type; }

    public void setpan_type(String pan_type) { this.pan_type = pan_type; }

    private String security_deposit_receipt_no;

    public String getsecurity_depositReceiptNo() { return this.security_deposit_receipt_no; }

    public void setsecurity_depositReceiptNo(String security_deposit_receipt_no) { this.security_deposit_receipt_no = security_deposit_receipt_no; }

    private String shifts;

    public String getShifts() { return this.shifts; }

    public void setShifts(String shifts) { this.shifts = shifts; }

    public void build() {
        CandidateHelper candidateHelper = new CandidateHelper();
        if(this.getaadhaar_no() == null)
            this.setaadhaar_no(CandidateConstant.aadhar_no);
        if(this.getaccount_holder_name() == null)
            this.setaccount_holder_name(CandidateConstant.account_name);
        if(this.getaccount_number() == null)
            this.setaccount_number(CandidateConstant.account_no);
        if(this.getagency_name() == null)
            this.setagency_name("");
        if(this.getbag_qr_code() == null)
            this.setbag_qr_code(CandidateConstant.qr_code);
        if(this.getbank_name() == null)
            this.setbank_name(CandidateConstant.bank_name);
        if(this.getBicycle() == null)
            this.setBicycle(CandidateConstant.bicycle_name);
        if(this.getbike_model() == null)
            this.setbike_model(CandidateConstant.bike_model);
        if(this.getbike_registration_number() == null)
            this.setbike_registration_number(CandidateConstant.bike_reg_no);
        if(this.getblood_group() == null)
            this.setblood_group(CandidateConstant.blood_group);
        if(this.getC_city() == null)
            this.setC_city(CandidateConstant.c_city);
        if(this.getc_pin_code() == null)
            this.setc_pin_code(CandidateConstant.c_pincode);
        if(this.getc_state() == null)
            this.setc_state(CandidateConstant.c_state);
        if(this.getc_street_address() == null)
            this.setc_street_address(CandidateConstant.c_street);
        if(this.getcity() == null)
            this.setcity(CandidateConstant.city);
        //TODO : Mobile No
        if(this.getcontact_number() == null)
            this.setcontact_number(CandidateConstant.contact_no);

        if(this.getdl_address() == null)
            this.setdl_address(CandidateConstant.dl_address);
        if(this.getdl_issued_on() == null)
            this.setdl_issued_on(CandidateConstant.dl_issuedon);
        if(this.getdl_number() == null)
            this.setdl_number(CandidateConstant.dl_no);
        if(this.getdl_state() == null)
            this.setdl_state(CandidateConstant.dl_state);
        if(this.getdl_type() == null)
            this.setdl_type(CandidateConstant.dl_type);
        if(this.getdl_valid_upto() == null)
            this.setdl_valid_upto(CandidateConstant.dl_valid);
        if(this.getDob() == null)
            this.setDob(CandidateConstant.dob);
        if(this.geteducation_level() == null)
            this.seteducation_level(CandidateConstant.education_level);
        if(this.getemergency_contact_number() == null)
            this.setemergency_contact_number(CandidateConstant.emergency_no);
        if(this.getfathers_name() == null)
            this.setfathers_name(CandidateConstant.fathers_name);
        if(this.getfirst_name() == null)
            this.setfirst_name(CandidateConstant.first_name);
        if(this.getidfy_reference_id() == null)
            this.setidfy_reference_id(CandidateConstant.idfy_ref_id);
        if(this.getifsc_code() == null)
            this.setifsc_code(CandidateConstant.ifsc_code);
        if(this.getjacket_quantity() == null)
            this.setjacket_quantity(CandidateConstant.jacket_quantity);
        if(this.getjacket_size() == null)
            this.setjacket_size(CandidateConstant.jacket_size);
        if(this.getlanguages_known() == null)
            this.setlanguages_known(CandidateConstant.languages_known);
        if(this.getlast_name() == null)
            this.setlast_name(CandidateConstant.last_name);
        if(this.getmarital_status() == null)
            this.setmarital_status(CandidateConstant.martial_status);
        if(this.getmobile_device_type() == null)
            this.setmobile_device_type(CandidateConstant.mobile_device_type);
        if(this.getp_city() == null)
            this.setp_city(CandidateConstant.p_city);
        if(this.getp_pin_code() == null)
            this.setp_pin_code(CandidateConstant.p_pincode);
        if(this.getp_state() == null)
            this.setp_state(CandidateConstant.p_state);
        if(this.getp_street_address() == null)
            this.setp_street_address(CandidateConstant.p_street_address);
        if(this.getpan_no() == null)
            this.setpan_no(CandidateConstant.pan_no);
        if(this.getpan_type() == null)
            this.setpan_type(CandidateConstant.pan_type);
        if(this.getPhotograph() == null)
            this.setPhotograph(CandidateConstant.photograph);
        if(this.getprimary_area() == null)
            this.setprimary_area(CandidateConstant.primary_area);
        if(this.getprimary_source() == null)
            this.setprimary_source(CandidateConstant.primary_source);
        if(this.getproof_address() == null)
            this.setproof_address(CandidateConstant.proof_address);
        if(this.getproof_address() == null)
            this.setproof_dl(CandidateConstant.proof_dl);
        if(this.getproof_pan() == null)
            this.setproof_pan(CandidateConstant.proof_pan);
        if(this.getproof_rc() == null)
            this.setproof_rc(CandidateConstant.proof_rc);
        if(this.getraincoat_quantity() == null)
            this.setraincoat_quantity(CandidateConstant.raincoat_quantity);
        if(this.getraincoat_size() == null)
            this.setraincoat_size(CandidateConstant.raincoat_size);
        if(this.getrc_number() == null)
            this.setrc_number(CandidateConstant.rc_number);
        if(this.getrc_type() == null)
            this.setrc_type(CandidateConstant.rc_type);
        if(this.getreferrer_de_id() == null)
            this.setreferrer_de_id(CandidateConstant.referrer_id);
        if(this.getreferrer_name() == null)
            this.setreferrer_name(CandidateConstant.referrer_name);
        if(this.getrelation_with_emergency_contact() == null)
            this.setrelation_with_emergency_contact(CandidateConstant.relationwith_emergency);
        if(this.getsecondary_area() == null)
            this.setsecondary_area(CandidateConstant.secondary_area);
        if(this.getsecurity_deposit() == null)
            this.setsecurity_deposit(CandidateConstant.security_deposit);
        if(this.getsecurity_depositReceiptNo() == null)
            this.setsecurity_depositReceiptNo(CandidateConstant.security_deposit_receiptno);
        if(this.getshift_type() == null)
            this.setshift_type(CandidateConstant.shift_type);
        if(this.getShifts() == null)
            this.setShifts(CandidateConstant.shifts);
        if(this.getsim_number() == null)
            this.setsim_number(CandidateConstant.simno);
        if(this.gett_shirt_quantity() == null)
            this.sett_shirt_quantity(CandidateConstant.tshirt_quantity);
        if(this.gett_shirt_size() == null)
            this.sett_shirt_size(CandidateConstant.tshirt_size);
        if(this.getvoter_id_no() == null)
            this.setvoter_id_no(CandidateConstant.voterid_no);
    }
}

