package com.swiggy.api.erp.crm.dp.foodissues;

import com.swiggy.api.erp.ff.helper.LOSHelper;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CCServicesDP {	
	
    static LOSHelper losHelper = new LOSHelper();
	public static String orderId = null;
	
	public void createOrder() throws Exception{	    
		orderId = losHelper.getAnOrder("manual");		
		
    	Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String order_time = dateFormat.format(date);
        
		losHelper.statusUpdate(orderId, order_time, "delivered");
	}
	
	@DataProvider(name = "createfoodissues")
	
	public static Object[][] createFoodIssues() throws IOException, InterruptedException {
  /*
  "orderId", "fullOrder", "items_id", "addons_id", "addons_name", "addons_groupId", "variants_id", "variants_name", "variants_groupId" 
  */
	return new Object[][] { 
			{ "CPOSITIVE_1", new String[] { losHelper.getAnOrder("manual"), "true", "123", "", "", "", "", "", ""}},
			{ "CPOSITIVE_2", new String[] { losHelper.getAnOrder("manual"), "true", "123", "", "", "", "", "", ""}} ,
			{ "CPOSITIVE_3", new String[] { losHelper.getAnOrder("manual"), "false", "987",  "", "", "", "", "", ""}},
			 
			{ "CPOSITIVE_4", new String[] { losHelper.getAnOrder("manual"), "true", "987", "195", "addons_name", "591", "", "", ""}},
			{ "CPOSITIVE_5", new String[] { losHelper.getAnOrder("manual"), "true", "987",  "", "", "", "195", "addons_name", "591"}},
			{ "CPOSITIVE_6", new String[] { losHelper.getAnOrder("manual"), "true", "987", "195", "addons_name", "591", "195", "addons_name", "591"}},
			
			{ "CPOSITIVE_7", new String[] { losHelper.getAnOrder("manual"), "false", "987", "195", "addons_name", "591", "", "", ""}},
			{ "CPOSITIVE_8", new String[] { losHelper.getAnOrder("manual"), "false", "987",  "", "", "", "195", "addons_name", "591"}},
			{ "CPOSITIVE_9", new String[] { losHelper.getAnOrder("manual"), "false", "987", "195", "addons_name", "591", "195", "addons_name", "591"}},
			
			{ "CNEGATIVE_1", new String[] { losHelper.getAnOrder("manual"), "false", "", "", "", "", "", "", ""}},
			{ "CNEGATIVE_2", new String[] { null, "false", "876", "", "", "", "", "", ""}}
			};
}

	@DataProvider(name = "createfoodissuesSanity")

	public static Object[][] createFoodIssuesSanity() throws IOException, InterruptedException {
  /*
  "orderId", "fullOrder", "items_id", "addons_id", "addons_name", "addons_groupId", "variants_id", "variants_name", "variants_groupId"
  */
		return new Object[][] {
				{ "CPOSITIVE_1", new String[] { losHelper.getAnOrder("manual"), "true", "123", "", "", "", "", "", ""}},
		};
	}
	
@DataProvider(name = "updatefoodissues")
	public static Object[][] updateFoodIssues() throws IOException {
  /*
     "containerCount", "image_link", "image_name", "comment text"
  */
	return new Object[][] { 
			{ "UPOSITIVE_1", new String[] { "2", "http://automationtest.com", "testimage", "this is automated test"}}
			};
}
}
