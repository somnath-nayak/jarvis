package com.swiggy.api.erp.cms.pojo.BulkAddonGroup;

import com.swiggy.api.erp.cms.constants.MenuConstants;

/**
 * Created by kiran.j on 2/22/18.
 */
public class BulkAddonGroup
{
    private String restaurant_id;

    private Entities[] entities;

    public String getRestaurant_id ()
    {
        return restaurant_id;
    }

    public void setRestaurant_id (String restaurant_id)
    {
        this.restaurant_id = restaurant_id;
    }

    public Entities[] getEntities ()
    {
        return entities;
    }

    public void setEntities (Entities[] entities)
    {
        this.entities = entities;
    }

    public BulkAddonGroup build() {
        Entities entities = new Entities();
        entities.build();
        if(this.getRestaurant_id() == null)
            this.setRestaurant_id(MenuConstants.rest_id);
        if(this.getEntities() == null)
            this.setEntities(new Entities[]{entities});
        return this;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [restaurant_id = "+restaurant_id+", entities = "+entities+"]";
    }
}