package com.swiggy.api.erp.cms.pojo.SelfServeV1Item;

import com.swiggy.api.erp.cms.constants.MenuConstants;
import com.swiggy.api.erp.cms.constants.SelfServeConstants;

public class Item_slot {
	private String item_id;

    private int open_time;

    private String external_id;

    private int close_time;

    private String id;

    private String slot;

    private String third_party_id;

    private String day_of_week;

    public String getItem_id ()
    {
        return item_id;
    }

    public void setItem_id (String item_id)
    {
        this.item_id = item_id;
    }

    public int getOpen_time ()
    {
        return open_time;
    }

    public void setOpen_time (int openTime)
    {
        this.open_time = openTime;
    }

    public String getExternal_id ()
    {
        return external_id;
    }

    public void setExternal_id (String external_id)
    {
        this.external_id = external_id;
    }

    public int getClose_time ()
    {
        return close_time;
    }

    public void setClose_time (int close_time)
    {
        this.close_time = close_time;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getSlot ()
    {
        return slot;
    }

    public void setSlot (String slot)
    {
        this.slot = slot;
    }

    public String getThird_party_id ()
    {
        return third_party_id;
    }

    public void setThird_party_id (String third_party_id)
    {
        this.third_party_id = third_party_id;
    }

    public String getDay_of_week ()
    {
        return day_of_week;
    }

    public void setDay_of_week (String day_of_week)
    {
        this.day_of_week = day_of_week;
    }

    public Item_slot build() {
		setDefaultValues();
		return this;

	}

	private void setDefaultValues() {
		  if(this.getOpen_time() == 0)
	            this.setOpen_time(SelfServeConstants.open_time);
	        if(this.getDay_of_week()==null)
	            this.setDay_of_week(SelfServeConstants.day_of_week[0]);
	        if(this.getClose_time() == 0)
	            this.setClose_time(SelfServeConstants.close_time);
//	        if(this.getItem_id()==null)
//	        	this.setItem_id(SelfServeConstants.item_id);
	}
    @Override
    public String toString()
    {
        return "ClassPojo [item_id = "+item_id+", open_time = "+open_time+", external_id = "+external_id+", close_time = "+close_time+", id = "+id+", slot = "+slot+", third_party_id = "+third_party_id+", day_of_week = "+day_of_week+"]";
    }
}
			
			