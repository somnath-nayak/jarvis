package com.swiggy.api.erp.vms.dp;

import org.testng.annotations.DataProvider;

import com.swiggy.api.erp.vms.constants.RMSConstants;
import com.swiggy.api.erp.vms.helper.RMSCommonHelper;
import com.swiggy.api.erp.vms.helper.RMSHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.snd.helper.SnDHelper;

import java.util.HashMap;


public class VendorInsightsDP extends RMSCommonHelper{
	
	static HashMap<String, String> m = new HashMap<>();
	
	static String restaurantId = null;
	static String restaurantId1 = null;
	static String restaurantId2 = null;
	public static String VENDOR_PASSWORD = null;
	public static String VENDOR_USERNAME = null;
	public static String user_id=null;
	public static String offerId = null;

	static {
		try {
			if (getEnv().equalsIgnoreCase("stage1")) {
				VENDOR_PASSWORD = "El3ment@ry";
				VENDOR_USERNAME = "5271";
				restaurantId = "9990";
				restaurantId1 = "30751";
				restaurantId2 = "220";
				user_id ="68174";
				offerId = "6989";
			} else if (getEnv().equalsIgnoreCase("stage2")) {
				VENDOR_PASSWORD = "El3ment@ry";
				VENDOR_USERNAME = "9990";
				restaurantId = "9990";
				restaurantId1 = "30751";
				restaurantId2 = "220";
				user_id ="68174";
				offerId = "6989";
			} else if (getEnv().equalsIgnoreCase("stage3")) {
				VENDOR_PASSWORD = "El3ment@ry";
				VENDOR_USERNAME = "9990";
				restaurantId = "9990";
				restaurantId1 = "30751";
				restaurantId2 = "220";
				user_id ="68174";
				offerId = "6989";
			} else if (getEnv().equalsIgnoreCase("prod")) {
				VENDOR_PASSWORD = "$w199y@zolb";
				VENDOR_USERNAME = "9742471472";
				restaurantId = "9990";
				restaurantId1 = "30751";
				restaurantId2 = "220";
				user_id ="68174";
				offerId = "6989";
			}
			m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

    @DataProvider(name="getRestaurant_VI")
    public static Object[][] getRestaurantVI(){

    	return new Object[][] {{ m.get("accessToken"), "9742471472", "9990", RMSConstants.startDate(-30),RMSConstants.endDate(-1),RMSConstants.group_by,RMSConstants.getrestaurantValidMessage, true},
    		                   { m.get("accessToken"), "9742471472", RMSConstants.invalidrest_ids, RMSConstants.startDate(-30),RMSConstants.endDate(-1),RMSConstants.group_by,RMSConstants.inValidSessionMessage, false}};
    }
    
    @DataProvider(name="orderForRest_VI")
    public static Object[][] orderForRest_VI(){

    	return new Object[][] {{ m.get("accessToken"), user_id, restaurantId, RMSConstants.startDate(-30),RMSConstants.endDate(-1),RMSConstants.group_by,RMSConstants.orderForRestValidMessage,true},
    							{ m.get("accessToken"), user_id, RMSConstants.invalidrest_id, RMSConstants.startDate(-30),RMSConstants.endDate(-1),RMSConstants.group_by,RMSConstants.inValidSessionMessage,false}};
    }
    
    @DataProvider(name="helpContext_VI")
    public static Object[][] helpContextVI(){

    	return new Object[][] {{ m.get("accessToken"), restaurantId, user_id,RMSConstants.helpContextValidMessage}};
    }
    
    @DataProvider(name="orderFeedback_VI")
    public static Object[][] orderFeedbackVI(){

    	return new Object[][] {{ m.get("accessToken"), RMSConstants.order_id,restaurantId, user_id,RMSConstants.orderFeedbackValidMessage}};
    }
    
    @DataProvider(name="lastUpdatedTime_VI")
    public static Object[][] lastUpdatedTimeVI(){

    	return new Object[][] {{m.get("accessToken"), restaurantId,RMSConstants.lastUpdatedTimeValidMessage}};
    }
    
    @DataProvider(name="confirmationTime_VI")
    public static Object[][] confirmationTimeVI(){

    	return new Object[][] {{ m.get("accessToken"), RMSConstants.start_date,RMSConstants.end_date,restaurantId,user_id,RMSConstants.group_by1,RMSConstants.confirmationTimeValidMessage}};
    }

    @DataProvider(name="orderForRestWeekly_VI")
    public static Object[][] orderForRestWeekly_VI(){

    	return new Object[][] {{ m.get("accessToken"), user_id, restaurantId +"\","+"\""+restaurantId1, RMSConstants.start_date_weekly,RMSConstants.end_date_weekly,RMSConstants.group_by_weekly,RMSConstants.orderForRestValidMessage, true},
    		{ m.get("accessToken"), user_id, restaurantId +"\","+"\""+restaurantId2, RMSConstants.start_date_weekly,RMSConstants.end_date_weekly,RMSConstants.group_by_weekly,RMSConstants.inValidSessionMessage, false}};
    }
    
    @DataProvider(name="orderForRestMonthly_VI")
    public static Object[][] orderForRestMonthly_VI(){

    	return new Object[][] {{ m.get("accessToken"), user_id, restaurantId, RMSConstants.start_date_monthly,RMSConstants.end_date_monthly,RMSConstants.group_by_mothly,RMSConstants.orderForRestValidMessage, true},
				{ m.get("accessToken"), user_id, m.get("accessToken"), RMSConstants.start_date_monthly,RMSConstants.end_date_monthly,RMSConstants.group_by_mothly,RMSConstants.inValidSessionMessage, false}};
    }
    
    @DataProvider(name="metricsForRest_VI")
    public static Object[][] metricsForRest_VI(){

    	return new Object[][] {{ m.get("accessToken"), user_id, restaurantId, RMSConstants.start_date,RMSConstants.end_date,RMSConstants.group_by,RMSConstants.metricsForRestValidMessage, true},
				{ m.get("accessToken"), user_id, m.get("accessToken"), RMSConstants.start_date,RMSConstants.end_date,RMSConstants.group_by,RMSConstants.inValidSessionMessage, false}};
    }
    
    @DataProvider(name="ratingsForRest_VI")
    public static Object[][] ratingsForRest_VI(){

    	return new Object[][] {{m.get("accessToken"), RMSConstants.start_date,RMSConstants.end_date,restaurantId,RMSConstants.ratingsForRestValidMessage, true},
				{m.get("accessToken"), RMSConstants.start_date,RMSConstants.end_date,RMSConstants.invalidrest_id,RMSConstants.inValidSessionMessage, false}};
    }
    @DataProvider(name="ordersForRatings_VI")
    public static Object[][] ordersForRatings_VI(){

    	return new Object[][] {{m.get("accessToken"), restaurantId,RMSConstants.in_ratings+","+RMSConstants.in_ratings1,user_id,RMSConstants.orderForRatingsValidMessage, true},
				{m.get("accessToken"), RMSConstants.invalidrest_id,RMSConstants.in_ratings+","+RMSConstants.in_ratings1,user_id,RMSConstants.inValidSessionMessage, false}};
    }
    
    @DataProvider(name="ordersForItems_VI")
    public static Object[][] ordersForItems_VI(){

    	return new Object[][] {{m.get("accessToken"), RMSConstants.start_date,RMSConstants.end_date,restaurantId,user_id,RMSConstants.ordersforitemsValidMessage, true},
				{m.get("accessToken"), RMSConstants.start_date,RMSConstants.end_date,RMSConstants.invalidrest_id,user_id,RMSConstants.inValidSessionMessage, false}};
    }
    
    @DataProvider(name="ordersForCategories_VI")
    public static Object[][] ordersForCategories_VI	(){

    	return new Object[][] {{m.get("accessToken"), RMSConstants.start_date,RMSConstants.end_date,restaurantId,user_id,RMSConstants.ordersforcategoriesValidMessage, true},
				{m.get("accessToken"), RMSConstants.start_date,RMSConstants.end_date,RMSConstants.invalidrest_id,user_id,RMSConstants.inValidSessionMessage, false}};
    }
    
    @DataProvider(name="cancellationReasons_VI")
    public static Object[][] cancellationReasons_VI	(){

    	return new Object[][] {{m.get("accessToken"), RMSConstants.start_date,RMSConstants.end_date,restaurantId,user_id,RMSConstants.cancellationReasonsValidMessage, true},
				{m.get("accessToken"), RMSConstants.start_date,RMSConstants.end_date,RMSConstants.invalidrest_id,user_id,RMSConstants.inValidSessionMessage, false}};
    }
    
    @DataProvider(name="oosOrdersAndReasons_VI")
    public static Object[][] oosOrdersAndReasons_VI	(){

    	return new Object[][] {{m.get("accessToken"), RMSConstants.start_date,RMSConstants.end_date,restaurantId,user_id,RMSConstants.group_by_weekly,RMSConstants.oosOrdersAndReasonsReasonsValidMessage, true},
				{m.get("accessToken"), RMSConstants.start_date,RMSConstants.end_date,RMSConstants.invalidrest_id,user_id,RMSConstants.group_by_weekly,RMSConstants.inValidSessionMessage, false}};
    }

	@DataProvider(name="sellerTieringVI")
	public static Object[][] sellerTieringVI() {

		return new Object[][] {
				{m.get("accessToken"), RMSConstants.start_date,RMSConstants.end_date, restaurantId, user_id, "Fetched Insights sellerTiering Successfully", true},
				{m.get("accessToken"), "",RMSConstants.end_date, restaurantId, user_id, "Invalid Input", false},
				{m.get("accessToken"), RMSConstants.start_date,"", restaurantId, user_id, "Invalid Input", false},
				{m.get("accessToken"), RMSConstants.start_date,RMSConstants.end_date, "", user_id, "Invalid Input", false},
				{m.get("accessToken"), RMSConstants.start_date,RMSConstants.end_date, restaurantId, "", "Invalid Input", false},
				{"Yd82329302", RMSConstants.start_date,RMSConstants.end_date, restaurantId, user_id, "Invalid Session", false}};
	}

	@DataProvider(name="tdMetricsVI")
	public static Object[][] tdMetricsVI() {

		return new Object[][] {
				{m.get("accessToken"), RMSConstants.start_date,RMSConstants.end_date, restaurantId, "Percentage", offerId, "Fetched trade discount metrics Successfully", true},
				{m.get("accessToken"), "",RMSConstants.end_date, restaurantId, "Percentage", offerId, "Invalid Input", false},
				{m.get("accessToken"), RMSConstants.start_date,RMSConstants.end_date, "", "Percentage", offerId, "Invalid Input", false},
				{m.get("accessToken"), RMSConstants.start_date,RMSConstants.end_date, restaurantId, "Percentage", "", "Invalid Input", false},
				{"Yud232-2ck", RMSConstants.start_date,RMSConstants.end_date, restaurantId, "Percentage", offerId, "Invalid Session", false},
				{m.get("accessToken"), RMSConstants.start_date,RMSConstants.end_date, restaurantId, "Percentage", "abc", "Action Failed", false}
		};
	}

	@DataProvider(name = "getOverview")
	public static Object[][] getOverview() throws Exception {

		return new Object[][] {{ m.get("accessToken"), restaurantId, 0, "Fetched Insights Overview Successfully" } };

	}

	@DataProvider(name = "getOverview_regression")
	public static Object[][] getOverview_regression() {

		return new Object[][] {
				{ m.get("accessToken"), "332", -3, "Invalid Session" },
				{ m.get("accessToken"), "", -1, "Invalid Input" }
		};
	}

	@DataProvider(name = "getRevenue")
	public static Object[][] getRevenue() {

		return new Object[][] {
				{ m.get("accessToken"), restaurantId, RMSConstants.startdate,
				RMSConstants.enddate, "day", 0, RMSConstants.validMessage_revenue },
				{ m.get("accessToken"), restaurantId, RMSConstants.startdate,
						RMSConstants.enddate, "week", 0, RMSConstants.validMessage_revenue }
		};

	}

	@DataProvider(name = "getRevenue_regression")
	public static Object[][] getRevenue_regression() {

		return new Object[][] {
				{ m.get("accessToken"), "112", RMSConstants.startdate, RMSConstants.enddate, "week", -3,
						"Invalid Session" },
				{ m.get("accessToken"), "9990", RMSConstants.enddate, RMSConstants.startdate, "week", 0,
						"Fetched Revenue Insights Successfully" },
				{ m.get("accessToken"), "9990", RMSConstants.startdate, RMSConstants.enddate, "day", 0,
						"Fetched Revenue Insights Successfully" },
				{ m.get("accessToken"), "9990", RMSConstants.enddate, RMSConstants.startdate, "day", 0,
						"Fetched Revenue Insights Successfully" },
				{ m.get("accessToken"), "112", RMSConstants.startdate, RMSConstants.enddate, "day", -3,
						"Invalid Session" },
				{ m.get("accessToken"), "112", RMSConstants.enddate, RMSConstants.startdate, "week", -3,
						"Invalid Session" },
				{ m.get("accessToken"), "112", RMSConstants.startdate, RMSConstants.startdate, "day", -3,
						"Invalid Session" } };
	}

	@DataProvider(name = "getOperations")
	public static Object[][] getOperations() throws Exception {

		return new Object[][] {
				{ m.get("accessToken"), restaurantId, RMSConstants.startdate1, RMSConstants.enddate1,
				"day", 0, "Fetched Operations Insights Successfully" },
				{ m.get("accessToken"), "9990", RMSConstants.startdate, RMSConstants.enddate, "week", 0,
						"Fetched Operations Insights Successfully" }
		};

	}

	@DataProvider(name = "getOperations_regression")
	public static Object[][] getOperations_regression() {

		return new Object[][] {
				{ m.get("accessToken"), "112", RMSConstants.startdate, RMSConstants.enddate, "week", -3,
						"Invalid Session" },
				{ m.get("accessToken"), "9990", RMSConstants.enddate, RMSConstants.startdate, "week", 0,
						"Fetched Operations Insights Successfully" },
				{ m.get("accessToken"), "9990", RMSConstants.startdate, RMSConstants.enddate, "day", 0,
						"Fetched Operations Insights Successfully" },
				{ m.get("accessToken"), "112", RMSConstants.enddate, RMSConstants.startdate, "week", -3,
						"Invalid Session" },
				{ m.get("accessToken"), "112", RMSConstants.startdate, RMSConstants.enddate, "day", -3,
						"Invalid Session" },
				{ m.get("accessToken"), "9990", RMSConstants.enddate, RMSConstants.startdate, "day", 0,
						"Fetched Operations Insights Successfully" },
				{ m.get("accessToken"), "112", RMSConstants.enddate, RMSConstants.startdate, "day", -3,
						"Invalid Session" } };
	}

	@DataProvider(name = "getRatings")
	public static Object[][] getRatings() throws Exception {

		return new Object[][] {
				{ m.get("accessToken"), restaurantId, RMSConstants.startdate1,
					RMSConstants.enddate1, "day", 0, "Fetched Ratings Insights Successfully" },
				{ m.get("accessToken"), restaurantId, RMSConstants.startdate,
						RMSConstants.enddate, RMSConstants.group_by_week, 0, "Fetched Ratings Insights Successfully" }
		};

	}

	@DataProvider(name = "getRatings_regression")
	public static Object[][] getRatings_regression() {

		return new Object[][] {
				{ m.get("accessToken"), RMSConstants.restaurantId_Invalid, RMSConstants.startdate1,
						RMSConstants.enddate1, "week", -3, RMSConstants.inValidSessionMessage },
				{ m.get("accessToken"), RMSConstants.rest_id_overview, RMSConstants.enddate1, RMSConstants.startdate1,
						"week", -4, RMSConstants.invalidMesage_ratings },
				{ m.get("accessToken"), RMSConstants.rest_id_overview, RMSConstants.startdate1, RMSConstants.enddate1,
						"day", 0, RMSConstants.validMessage_ratings },
				{ m.get("accessToken"), RMSConstants.restaurantId_Invalid, RMSConstants.startdate1,
						RMSConstants.enddate1, "day", -3, RMSConstants.inValidSessionMessage },
				{ m.get("accessToken"), RMSConstants.restaurantId_Invalid, RMSConstants.enddate1,
						RMSConstants.startdate1, "week", -3, RMSConstants.inValidSessionMessage },
				{ m.get("accessToken"), RMSConstants.rest_id_overview, RMSConstants.enddate1, RMSConstants.startdate1,
						"day", -4, RMSConstants.invalidMesage_ratings },
				{ m.get("accessToken"), RMSConstants.restaurantId_Invalid, RMSConstants.enddate1,
						RMSConstants.startdate1, "day", -3, RMSConstants.inValidSessionMessage } };
	}

	@DataProvider(name = "getMfrReport")
	public static Object[][] getMfrReport() {

		return new Object[][] {
				{ restaurantId, RMSConstants.fromDate,
						RMSConstants.toDate, 0, RMSConstants.validMessage_mfrReport }
		};

	}

	@DataProvider(name = "getMfrReportRegression")
	public static Object[][] getMfrReportRegression() {

		return new Object[][] {
				{ "-99", RMSConstants.fromDate,
						RMSConstants.toDate, 1, RMSConstants.invalidMessage_mfrReport },
				{ "%20", RMSConstants.fromDate,
						RMSConstants.toDate, 1, RMSConstants.invalidRest1 },
				{ "ad99", RMSConstants.fromDate,
						RMSConstants.toDate, 0, RMSConstants.invalidRest2 }
		};

	}
}
 
	