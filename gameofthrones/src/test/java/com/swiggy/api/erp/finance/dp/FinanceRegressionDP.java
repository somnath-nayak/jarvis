package com.swiggy.api.erp.finance.dp;

import com.swiggy.api.erp.cms.helper.BaseServiceHelper;
import com.swiggy.api.erp.ff.helper.LOSHelper;
import com.swiggy.api.erp.finance.constants.FinanceConstants;
import com.swiggy.api.erp.finance.helper.CashMgmtRegressionHelper;
import com.swiggy.api.erp.finance.helper.FinanceHelper;
import com.swiggy.api.erp.finance.helper.finHelper;
import framework.gameofthrones.JonSnow.DateHelper;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.util.HashMap;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.erp.finance.dp
 **/
public class FinanceRegressionDP {
    public static FinanceHelper helper = new FinanceHelper();
    DateHelper dateHelper = new DateHelper();
    LOSHelper losHelper = new LOSHelper();
    public static BaseServiceHelper baseServiceHelper = new BaseServiceHelper();
    CashMgmtRegressionHelper cashMgmtRegressionHelper = new CashMgmtRegressionHelper();
    com.swiggy.api.erp.finance.helper.finHelper finHelper =  new finHelper();
    public static String prepaid_restId_old_mou, prepaid_restId_new_mou, postpaid_restId_old_mou, postpaid_restId_new_mou,restId_Multi_Td;
    public static String[] statusList = {"assigned", "confirmed", "arrived", "pickedup", "reached", "delivered"};
    public static String billWithoutTax="[bill_without_taxes]*0.05", billWithTax="[bill]*0.18", replicatedTrue="true", replicatedFalse="false";
    

    public static HashMap<String,String> createAllRestaurants() {
        HashMap<String, String> hm = new HashMap<>();
        String prepaid_new_mou = helper.createRestaurantOfGivenAgreementType("pre_paid");
        helper.makeRestaurantNewMou(prepaid_new_mou);
        String prepaid_old_mou = helper.createRestaurantOfGivenAgreementType("pre_paid");
        String postpaid_new_mou=helper.createRestaurantOfGivenAgreementType("post_paid");
        helper.makeRestaurantNewMou(postpaid_new_mou);
        String postpaid_old_mou=helper.createRestaurantOfGivenAgreementType("post_paid");
        hm.put("prepaid_new_mou",prepaid_new_mou);
        hm.put("postpaid_new_mou",postpaid_new_mou);
        hm.put("prepaid_old_mou",prepaid_old_mou);
        hm.put("postpaid_old_mou",postpaid_old_mou);
        return hm;
    }

    @DataProvider(name = "autoReconciliationDeliveredOrderDP")
    public Object[][] autoReconciliationDeliveredOrderDP() throws IOException {
      String[] orderID_postpaid_oldmou = helper.createOrderOfAGivenType(postpaid_restId_old_mou,"Postpaid","false",billWithTax);
       String[] orderID_postpaid_newmou = helper.createOrderOfAGivenType(postpaid_restId_new_mou,"Postpaid",replicatedFalse,billWithoutTax);
       String[] orderID_prepaid_oldmou = helper.createOrderOfAGivenType(prepaid_restId_old_mou,"Prepaid","false",billWithTax);
       String[] orderID_prepaid_newmou = helper.createOrderOfAGivenType(prepaid_restId_old_mou,"Prepaid",replicatedFalse,billWithoutTax);
       String[] orderID_pop = helper.createOtherOrders(postpaid_restId_old_mou,"Postpaid","../Data/Payloads/JSON/fin2_CreatePopOrder.json");
       String[] orderID_super = helper.createOtherOrders(postpaid_restId_old_mou,"Postpaid","../Data/Payloads/JSON/fin2_CreateSuperOrder.json");
       String[] orderID_multiTD = helper.createOtherOrders(postpaid_restId_old_mou,"Postpaid","../Data/Payloads/JSON/fin2_CreateMultiTDOrder.json");
        String[] orderID_dominos = helper.createOtherOrders(postpaid_restId_old_mou,"Postpaid","../Data/Payloads/JSON/losCreateDominosOrder.json");
        String[] orderID_Online_Tax = helper.createOnlineOrders(postpaid_restId_old_mou,"Postpaid","../Data/Payloads/JSON/fin2_CreateOnlineOrder.json",billWithTax);
        String[] orderID_Online_WithoutTax = helper.createOnlineOrders(postpaid_restId_old_mou,"Postpaid","../Data/Payloads/JSON/fin2_CreateOnlineOrder.json",billWithoutTax);


        return new Object[][] {
                {orderID_postpaid_oldmou[0],orderID_postpaid_oldmou[1]},
               {orderID_postpaid_newmou[0],orderID_postpaid_newmou[1]},
                {orderID_prepaid_oldmou[0],orderID_prepaid_oldmou[1]},
                {orderID_prepaid_newmou[0],orderID_prepaid_newmou[1]},
                {orderID_pop[0],orderID_pop[1]},
                {orderID_super[0],orderID_super[1]},
                {orderID_multiTD[0],orderID_multiTD[1]},
                {orderID_dominos[0],orderID_dominos[1]},
                {orderID_Online_Tax[0],orderID_Online_Tax[1]},
                {orderID_Online_WithoutTax[0],orderID_Online_WithoutTax[1]}

        };
    }

    @DataProvider(name="OrderJsonsDP")
    public Object[][] orderCAPRD() throws IOException, InterruptedException {
        //MultiTD DataProvider
      String[] MT01=  helper.createOrderFinanceHelper(prepaid_restId_old_mou,"Prepaid","[bill_with_taxes]*20","Percentage","20","5",FinanceConstants.multi_td_online_json);
      String[] MT02=   helper.createOrderFinanceHelper(prepaid_restId_old_mou,"Prepaid","[bill]*20","Percentage","20","5",FinanceConstants.multi_td_online_json);
      String[] MT03=     helper.createOrderFinanceHelper(prepaid_restId_old_mou,"Prepaid","[bill_with_taxes]*20+10","Percentage","20","5",FinanceConstants.multi_td_online_json);
      String[] MT04=   helper.createOrderFinanceHelper(prepaid_restId_old_mou,"Prepaid","[bill]*20+20","Percentage","20","5",FinanceConstants.multi_td_online_json);
      String[] MT05=   helper.createOrderFinanceHelper(prepaid_restId_old_mou,"Prepaid","[bill_with_taxes]*20","Flat","30","5",FinanceConstants.multi_td_online_json);
     String[] MT06=  helper.createOrderFinanceHelper(prepaid_restId_old_mou,"Prepaid","[bill]*20","Flat","30","5",FinanceConstants.multi_td_online_json);
      String[] MT07=    helper.createOrderFinanceHelper(prepaid_restId_old_mou,"Prepaid","[bill_with_taxes]*20+10","Flat","30","5",FinanceConstants.multi_td_online_json);
      String[] MT08=      helper.createOrderFinanceHelper(prepaid_restId_old_mou,"Prepaid","[bill]*20+20","Flat","30","5",FinanceConstants.multi_td_online_json);
      String[] MT09=  helper.createOrderFinanceHelper(prepaid_restId_old_mou,"Prepaid","50","Flat","30","5",FinanceConstants.multi_td_online_json);
      String[] MT10=  helper.createOrderFinanceHelper(postpaid_restId_old_mou,"Postpaid","[bill_with_taxes]*20","Percentage","20","5",FinanceConstants.multi_td_online_json);
      String[] MT11=  helper.createOrderFinanceHelper(postpaid_restId_old_mou,"Postpaid","[bill]*20","Percentage","20","5",FinanceConstants.multi_td_online_json);
      String[] MT12=  helper.createOrderFinanceHelper(postpaid_restId_old_mou,"Postpaid","[bill_with_taxes]*20+10","Percentage","20","5",FinanceConstants.multi_td_online_json);
      String[] MT13=  helper.createOrderFinanceHelper(postpaid_restId_old_mou,"Postpaid","[bill]*20+20","Percentage","20","5",FinanceConstants.multi_td_online_json);
      String[] MT14=  helper.createOrderFinanceHelper(postpaid_restId_old_mou,"Postpaid","[bill_with_taxes]*20","Flat","30","5",FinanceConstants.multi_td_online_json);
      String[] MT15=  helper.createOrderFinanceHelper(postpaid_restId_old_mou,"Postpaid","[bill]*20","Flat","30","5",FinanceConstants.multi_td_online_json);
      String[] MT16=  helper.createOrderFinanceHelper(postpaid_restId_old_mou,"Postpaid","[bill_with_taxes]*20+10","Flat","30","5",FinanceConstants.multi_td_online_json);
      String[] MT17=   helper.createOrderFinanceHelper(postpaid_restId_old_mou,"Postpaid","[bill]*20+20","Flat","30","5",FinanceConstants.multi_td_online_json);
      String[] MT18=  helper.createOrderFinanceHelper(postpaid_restId_old_mou,"Postpaid","50","Flat","30","5",FinanceConstants.multi_td_online_json);

      String[] MT19=  helper.createOrderFinanceHelper(prepaid_restId_new_mou,"Prepaid","[bill_with_taxes]*20","Percentage","20","5",FinanceConstants.multi_td_online_json);
      String[] MT20=  helper.createOrderFinanceHelper(prepaid_restId_new_mou,"Prepaid","[bill]*20","Percentage","20","5",FinanceConstants.multi_td_online_json);
      String[] MT21=  helper.createOrderFinanceHelper(prepaid_restId_new_mou,"Prepaid","[bill_with_taxes]*20+10","Percentage","20","5",FinanceConstants.multi_td_online_json);
      String[] MT22=  helper.createOrderFinanceHelper(prepaid_restId_new_mou,"Prepaid","[bill]*20+20","Percentage","20","5",FinanceConstants.multi_td_online_json);
      String[] MT23=   helper.createOrderFinanceHelper(prepaid_restId_new_mou,"Prepaid","[bill_with_taxes]*20","Flat","30","5",FinanceConstants.multi_td_online_json);
      String[] MT24=  helper.createOrderFinanceHelper(prepaid_restId_new_mou,"Prepaid","[bill]*20","Flat","30","5",FinanceConstants.multi_td_online_json);
      String[] MT25=  helper.createOrderFinanceHelper(prepaid_restId_new_mou,"Prepaid","[bill_with_taxes]*20+10","Flat","30","5",FinanceConstants.multi_td_online_json);
      String[] MT26=   helper.createOrderFinanceHelper(prepaid_restId_new_mou,"Prepaid","[bill]*20+20","Flat","30","5",FinanceConstants.multi_td_online_json);
      String[] MT27=  helper.createOrderFinanceHelper(prepaid_restId_new_mou,"Prepaid","50","Flat","30","5",FinanceConstants.multi_td_online_json);
      String[] MT28=  helper.createOrderFinanceHelper(postpaid_restId_new_mou,"Postpaid","[bill_with_taxes]*20","Percentage","20","5",FinanceConstants.multi_td_online_json);
      String[] MT29=  helper.createOrderFinanceHelper(postpaid_restId_new_mou,"Postpaid","[bill]*20","Percentage","20","5",FinanceConstants.multi_td_online_json);
      String[] MT30=   helper.createOrderFinanceHelper(postpaid_restId_new_mou,"Postpaid","[bill_with_taxes]*20+10","Percentage","20","5",FinanceConstants.multi_td_online_json);
      String[] MT31= helper.createOrderFinanceHelper(postpaid_restId_new_mou,"Postpaid","[bill]*20+20","Percentage","20","5",FinanceConstants.multi_td_online_json);
      String[] MT32= helper.createOrderFinanceHelper(postpaid_restId_new_mou,"Postpaid","[bill_with_taxes]*20","Flat","30","5",FinanceConstants.multi_td_online_json);
      String[] MT33=  helper.createOrderFinanceHelper(postpaid_restId_new_mou,"Postpaid","[bill]*20","Flat","30","5",FinanceConstants.multi_td_online_json);
      String[] MT34= helper.createOrderFinanceHelper(postpaid_restId_new_mou,"Postpaid","[bill_with_taxes]*20+10","Flat","30","5",FinanceConstants.multi_td_online_json);
      String[] MT35= helper.createOrderFinanceHelper(postpaid_restId_new_mou,"Postpaid","[bill]*20+20","Flat","30","5",FinanceConstants.multi_td_online_json);
      String[] MT36=helper.createOrderFinanceHelper(postpaid_restId_new_mou,"Postpaid","50","Flat","30","5",FinanceConstants.multi_td_online_json);

        String[] MT37=  helper.createOrderFinanceHelper(prepaid_restId_old_mou,"Prepaid","[bill_with_taxes]*20","Percentage","20","5",FinanceConstants.Multi_td_cash_json);
        String[] MT38=         helper.createOrderFinanceHelper(prepaid_restId_old_mou,"Prepaid","[bill]*20","Percentage","20","5",FinanceConstants.Multi_td_cash_json);
        String[] MT39=         helper.createOrderFinanceHelper(prepaid_restId_old_mou,"Prepaid","[bill_with_taxes]*20+10","Percentage","20","5",FinanceConstants.Multi_td_cash_json);
        String[] MT40=         helper.createOrderFinanceHelper(prepaid_restId_old_mou,"Prepaid","[bill]*20+20","Percentage","20","5",FinanceConstants.Multi_td_cash_json);
        String[] MT41= helper.createOrderFinanceHelper(prepaid_restId_old_mou,"Prepaid","[bill_with_taxes]*20","Flat","30","5",FinanceConstants.Multi_td_cash_json);
        String[] MT42= helper.createOrderFinanceHelper(prepaid_restId_old_mou,"Prepaid","[bill]*20","Flat","30","5",FinanceConstants.Multi_td_cash_json);
        String[] MT43=   helper.createOrderFinanceHelper(prepaid_restId_old_mou,"Prepaid","[bill_with_taxes]*20+10","Flat","30","5",FinanceConstants.Multi_td_cash_json);
        String[] MT44=     helper.createOrderFinanceHelper(prepaid_restId_old_mou,"Prepaid","[bill]*20+20","Flat","30","5",FinanceConstants.Multi_td_cash_json);
        String[] MT45=   helper.createOrderFinanceHelper(prepaid_restId_old_mou,"Prepaid","50","Flat","30","5",FinanceConstants.Multi_td_cash_json);
        String[] MT46=    helper.createOrderFinanceHelper(postpaid_restId_old_mou,"Postpaid","[bill_with_taxes]*20","Percentage","20","5",FinanceConstants.Multi_td_cash_json);
        String[] MT47=    helper.createOrderFinanceHelper(postpaid_restId_old_mou,"Postpaid","[bill]*20","Percentage","20","5",FinanceConstants.Multi_td_cash_json);
        String[] MT48=    helper.createOrderFinanceHelper(postpaid_restId_old_mou,"Postpaid","[bill_with_taxes]*20+10","Percentage","20","5",FinanceConstants.Multi_td_cash_json);
        String[] MT49=    helper.createOrderFinanceHelper(postpaid_restId_old_mou,"Postpaid","[bill]*20+20","Percentage","20","5",FinanceConstants.Multi_td_cash_json);
        String[] MT50=    helper.createOrderFinanceHelper(postpaid_restId_old_mou,"Postpaid","[bill_with_taxes]*20","Flat","30","5",FinanceConstants.Multi_td_cash_json);
        String[] MT51=    helper.createOrderFinanceHelper(postpaid_restId_old_mou,"Postpaid","[bill]*20","Flat","30","5",FinanceConstants.Multi_td_cash_json);
        String[] MT52=     helper.createOrderFinanceHelper(postpaid_restId_old_mou,"Postpaid","[bill_with_taxes]*20+10","Flat","30","5",FinanceConstants.Multi_td_cash_json);
        String[] MT53=    helper.createOrderFinanceHelper(postpaid_restId_old_mou,"Postpaid","[bill]*20+20","Flat","30","5",FinanceConstants.Multi_td_cash_json);
        String[] MT54=    helper.createOrderFinanceHelper(postpaid_restId_old_mou,"Postpaid","50","Flat","30","5",FinanceConstants.Multi_td_cash_json);

        String[] MT55=    helper.createOrderFinanceHelper(prepaid_restId_new_mou,"Prepaid","[bill_with_taxes]*20","Percentage","20","5",FinanceConstants.Multi_td_cash_json);
        String[] MT56=   helper.createOrderFinanceHelper(prepaid_restId_new_mou,"Prepaid","[bill]*20","Percentage","20","5",FinanceConstants.Multi_td_cash_json);
        String[] MT57=   helper.createOrderFinanceHelper(prepaid_restId_new_mou,"Prepaid","[bill_with_taxes]*20+10","Percentage","20","5",FinanceConstants.Multi_td_cash_json);
        String[] MT58=  helper.createOrderFinanceHelper(prepaid_restId_new_mou,"Prepaid","[bill]*20+20","Percentage","20","5",FinanceConstants.Multi_td_cash_json);
        String[] MT59=  helper.createOrderFinanceHelper(prepaid_restId_new_mou,"Prepaid","[bill_with_taxes]*20","Flat","30","5",FinanceConstants.Multi_td_cash_json);
        String[] MT60=   helper.createOrderFinanceHelper(prepaid_restId_new_mou,"Prepaid","[bill]*20","Flat","30","5",FinanceConstants.Multi_td_cash_json);
        String[] MT61=    helper.createOrderFinanceHelper(prepaid_restId_new_mou,"Prepaid","[bill_with_taxes]*20+10","Flat","30","5",FinanceConstants.Multi_td_cash_json);
        String[] MT62=   helper.createOrderFinanceHelper(prepaid_restId_new_mou,"Prepaid","[bill]*20+20","Flat","30","5",FinanceConstants.Multi_td_cash_json);
        String[] MT63=    helper.createOrderFinanceHelper(prepaid_restId_new_mou,"Prepaid","50","Flat","30","5",FinanceConstants.Multi_td_cash_json);
        String[] MT64=   helper.createOrderFinanceHelper(postpaid_restId_new_mou,"Postpaid","[bill_with_taxes]*20","Percentage","20","5",FinanceConstants.Multi_td_cash_json);
        String[] MT65=   helper.createOrderFinanceHelper(postpaid_restId_new_mou,"Postpaid","[bill]*20","Percentage","20","5",FinanceConstants.Multi_td_cash_json);
        String[] MT66=   helper.createOrderFinanceHelper(postpaid_restId_new_mou,"Postpaid","[bill_with_taxes]*20+10","Percentage","20","5",FinanceConstants.Multi_td_cash_json);
        String[] MT67=  helper.createOrderFinanceHelper(postpaid_restId_new_mou,"Postpaid","[bill]*20+20","Percentage","20","5",FinanceConstants.Multi_td_cash_json);
        String[] MT68=  helper.createOrderFinanceHelper(postpaid_restId_new_mou,"Postpaid","[bill_with_taxes]*20","Flat","30","5",FinanceConstants.Multi_td_cash_json);
        String[] MT69=   helper.createOrderFinanceHelper(postpaid_restId_new_mou,"Postpaid","[bill]*20","Flat","30","5",FinanceConstants.Multi_td_cash_json);
        String[] MT70=     helper.createOrderFinanceHelper(postpaid_restId_new_mou,"Postpaid","[bill_with_taxes]*20+10","Flat","30","5",FinanceConstants.Multi_td_cash_json);
        String[] MT71=    helper.createOrderFinanceHelper(postpaid_restId_new_mou,"Postpaid","[bill]*20+20","Flat","30","5",FinanceConstants.Multi_td_cash_json);
        String[] MT72=     helper.createOrderFinanceHelper(postpaid_restId_new_mou,"Postpaid","50","Flat","30","5",FinanceConstants.Multi_td_cash_json);

        return new Object[][]{
                {MT01[0],MT01[1]},{MT02[0],MT02[1]},{MT03[0],MT03[1]},{MT04[0],MT04[1]},{MT05[0],MT05[1]},{MT06[0],MT06[1]},{MT07[0],MT07[1]},{MT08[0],MT08[1]},{MT09[0],MT09[1]},{MT10[0],MT10[1]},{MT11[0],MT11[1]},{MT12[0],MT12[1]},{MT13[0],MT13[1]},{MT14[0],MT14[1]},{MT15[0],MT15[1]},{MT16[0],MT16[1]},{MT17[0],MT17[1]},{MT18[0],MT18[1]},{MT19[0],MT19[1]},{MT20[0],MT20[1]},{MT21[0],MT21[1]},{MT22[0],MT22[1]},{MT23[0],MT23[1]},{MT24[0],MT24[1]},{MT25[0],MT25[1]},{MT26[0],MT26[1]},{MT27[0],MT27[1]},{MT28[0],MT28[1]},{MT29[0],MT29[1]},{MT30[0],MT30[1]},{MT31[0],MT31[1]},{MT32[0],MT32[1]},{MT33[0],MT33[1]},{MT34[0],MT34[1]},{MT35[0],MT35[1]},{MT36[0],MT36[1]},{MT37[0],MT37[1]},{MT38[0],MT38[1]},{MT39[0],MT39[1]},{MT40[0],MT40[1]},{MT41[0],MT41[1]},{MT42[0],MT42[1]},
                {MT43[0],MT43[1]},{MT44[0],MT44[1]},{MT45[0],MT45[1]},{MT46[0],MT46[1]},{MT47[0],MT47[1]},{MT48[0],MT48[1]},{MT49[0],MT49[1]},{MT50[0],MT50[1]},{MT51[0],MT51[1]},{MT52[0],MT52[1]},{MT53[0],MT53[1]},{MT54[0],MT54[1]},{MT55[0],MT55[1]},{MT56[0],MT56[1]},{MT57[0],MT57[1]},{MT58[0],MT58[1]},{MT59[0],MT59[1]},{MT60[0],MT60[1]},{MT61[0],MT61[1]},{MT62[0],MT62[1]},{MT63[0],MT63[1]},{MT64[0],MT64[1]},{MT65[0],MT65[1]},{MT66[0],MT66[1]},{MT67[0],MT67[1]},{MT68[0],MT68[1]},{MT69[0],MT69[1]},{MT70[0],MT70[1]},{MT71[0],MT71[1]},{MT72[0],MT72[1]}

        };
    }


    @DataProvider(name="DP_FOR_POP_SUPER_NORMAL")
    public Object[][] orderHelperForPOPSuperNormal() throws IOException, InterruptedException {

        //Pop Orders
        String[] P01=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","[bill]*20",FinanceConstants.pop_order_online_json);
        String[] P02=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","[bill_with_taxes]*20",FinanceConstants.pop_order_online_json);
        String[] P03=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","[bill]*20+20",FinanceConstants.pop_order_online_json);
        String[] P04=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","[bill_with_taxes]*20+20",FinanceConstants.pop_order_online_json);
        String[] P05=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","50",FinanceConstants.pop_order_online_json);

        String[] P06=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","[bill]*20",FinanceConstants.pop_order_online_json);
        String[] P07=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","[bill_with_taxes]*20",FinanceConstants.pop_order_online_json);
        String[] P08=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","[bill]*20+20",FinanceConstants.pop_order_online_json);
        String[] P09=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","[bill_with_taxes]*20+20",FinanceConstants.pop_order_online_json);
        String[] P10=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","50",FinanceConstants.pop_order_online_json);

        String[] P11=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","[bill]*20",FinanceConstants.pop_order_online_json);
        String[] P12=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","[bill_with_taxes]*20",FinanceConstants.pop_order_online_json);
        String[] P13=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","[bill]*20+20",FinanceConstants.pop_order_online_json);
        String[] P14=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","[bill_with_taxes]*20+20",FinanceConstants.pop_order_online_json);
        String[] P15=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","50",FinanceConstants.pop_order_online_json);

        String[] P16=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","[bill]*20",FinanceConstants.pop_order_online_json);
        String[] P17=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","[bill_with_taxes]*20",FinanceConstants.pop_order_online_json);
        String[] P18=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","[bill]*20+20",FinanceConstants.pop_order_online_json);
        String[] P19=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","[bill_with_taxes]*20+20",FinanceConstants.pop_order_online_json);
        String[] P20=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","50",FinanceConstants.pop_order_online_json);

        String[] P21=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","[bill]*20",FinanceConstants.pop_order_cash_json);
        String[] P22=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","[bill_with_taxes]*20",FinanceConstants.pop_order_cash_json);
        String[] P23=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","[bill]*20+20",FinanceConstants.pop_order_cash_json);
        String[] P24=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","[bill_with_taxes]*20+20",FinanceConstants.pop_order_cash_json);
        String[] P25=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","50",FinanceConstants.pop_order_cash_json);

        String[] P26=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","[bill]*20",FinanceConstants.pop_order_cash_json);
        String[] P27=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","[bill_with_taxes]*20",FinanceConstants.pop_order_cash_json);
        String[] P28=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","[bill]*20+20",FinanceConstants.pop_order_cash_json);
        String[] P29=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","[bill_with_taxes]*20+20",FinanceConstants.pop_order_cash_json);
        String[] P30=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","50",FinanceConstants.pop_order_cash_json);

        String[] P31=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","[bill]*20",FinanceConstants.pop_order_cash_json);
        String[] P32=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","[bill_with_taxes]*20",FinanceConstants.pop_order_cash_json);
        String[] P33=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","[bill]*20+20",FinanceConstants.pop_order_cash_json);
        String[] P34=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","[bill_with_taxes]*20+20",FinanceConstants.pop_order_cash_json);
        String[] P35=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","50",FinanceConstants.pop_order_cash_json);

        String[] P36=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","[bill]*20",FinanceConstants.pop_order_cash_json);
        String[] P37=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","[bill_with_taxes]*20",FinanceConstants.pop_order_cash_json);
        String[] P38=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","[bill]*20+20",FinanceConstants.pop_order_cash_json);
        String[] P39=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","[bill_with_taxes]*20+20",FinanceConstants.pop_order_cash_json);
        String[] P40=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","50",FinanceConstants.pop_order_cash_json);

        //Super Order
        String[] S01=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","[bill]*20",FinanceConstants.super_order_online_json);
        String[] S02=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","[bill_with_taxes]*20",FinanceConstants.super_order_online_json);
        String[] S03=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","[bill]*20+20",FinanceConstants.super_order_online_json);
        String[] S04=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","[bill_with_taxes]*20+20",FinanceConstants.super_order_online_json);
        String[] S05=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","50",FinanceConstants.super_order_online_json);

        String[] S06=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","[bill]*20",FinanceConstants.super_order_online_json);
        String[] S07=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","[bill_with_taxes]*20",FinanceConstants.super_order_online_json);
        String[] S08=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","[bill]*20+20",FinanceConstants.super_order_online_json);
        String[] S09=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","[bill_with_taxes]*20+20",FinanceConstants.super_order_online_json);
        String[] S10=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","50",FinanceConstants.super_order_online_json);

        String[] S11=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","[bill]*20",FinanceConstants.super_order_online_json);
        String[] S12=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","[bill_with_taxes]*20",FinanceConstants.super_order_online_json);
        String[] S13=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","[bill]*20+20",FinanceConstants.super_order_online_json);
        String[] S14=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","[bill_with_taxes]*20+20",FinanceConstants.super_order_online_json);
        String[] S15=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","50",FinanceConstants.super_order_online_json);

        String[] S16=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","[bill]*20",FinanceConstants.super_order_online_json);
        String[] S17=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","[bill_with_taxes]*20",FinanceConstants.super_order_online_json);
        String[] S18=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","[bill]*20+20",FinanceConstants.super_order_online_json);
        String[] S19=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","[bill_with_taxes]*20+20",FinanceConstants.super_order_online_json);
        String[] S20=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","50",FinanceConstants.super_order_online_json);

        String[] S21=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","[bill]*20",FinanceConstants.super_order_cash_json);
        String[] S22=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","[bill_with_taxes]*20",FinanceConstants.super_order_cash_json);
        String[] S23=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","[bill]*20+20",FinanceConstants.super_order_cash_json);
        String[] S24=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","[bill_with_taxes]*20+20",FinanceConstants.super_order_cash_json);
        String[] S25=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","50",FinanceConstants.super_order_cash_json);

        String[] S26=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","[bill]*20",FinanceConstants.super_order_cash_json);
        String[] S27=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","[bill_with_taxes]*20",FinanceConstants.super_order_cash_json);
        String[] S28=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","[bill]*20+20",FinanceConstants.super_order_cash_json);
        String[] S29=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","[bill_with_taxes]*20+20",FinanceConstants.super_order_cash_json);
        String[] S30=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","50",FinanceConstants.super_order_cash_json);

        String[] S31=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","[bill]*20",FinanceConstants.super_order_cash_json);
        String[] S32=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","[bill_with_taxes]*20",FinanceConstants.super_order_cash_json);
        String[] S33=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","[bill]*20+20",FinanceConstants.super_order_cash_json);
        String[] S34=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","[bill_with_taxes]*20+20",FinanceConstants.super_order_cash_json);
        String[] S35=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","50",FinanceConstants.super_order_cash_json);

        String[] S36=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","[bill]*20",FinanceConstants.super_order_cash_json);
        String[] S37=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","[bill_with_taxes]*20",FinanceConstants.super_order_cash_json);
        String[] S38=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","[bill]*20+20",FinanceConstants.super_order_cash_json);
        String[] S39=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","[bill_with_taxes]*20+20",FinanceConstants.super_order_cash_json);
        String[] S40=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","50",FinanceConstants.super_order_cash_json);

        //Online Order Reguler
        String[] O01=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","[bill]*20",FinanceConstants.online_order_json);
        String[] O02=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","[bill_with_taxes]*20",FinanceConstants.online_order_json);
        String[] O03=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","[bill]*20+20",FinanceConstants.online_order_json);
        String[] O04=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","[bill_with_taxes]*20+20",FinanceConstants.online_order_json);
        String[] O05=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","50",FinanceConstants.online_order_json);

        String[] O06=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","[bill]*20",FinanceConstants.online_order_json);
        String[] O07=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","[bill_with_taxes]*20",FinanceConstants.online_order_json);
        String[] O08=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","[bill]*20+20",FinanceConstants.online_order_json);
        String[] O09=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","[bill_with_taxes]*20+20",FinanceConstants.online_order_json);
        String[] O10=     helper.createOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","50",FinanceConstants.online_order_json);

        String[] O11=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","[bill]*20",FinanceConstants.online_order_json);
        String[] O12=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","[bill_with_taxes]*20",FinanceConstants.online_order_json);
        String[] O13=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","[bill]*20+20",FinanceConstants.online_order_json);
        String[] O14=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","[bill_with_taxes]*20+20",FinanceConstants.online_order_json);
        String[] O15=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","50",FinanceConstants.online_order_json);

        String[] O16=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","[bill]*20",FinanceConstants.online_order_json);
        String[] O17=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","[bill_with_taxes]*20",FinanceConstants.online_order_json);
        String[] O18=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","[bill]*20+20",FinanceConstants.online_order_json);
        String[] O19=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","[bill_with_taxes]*20+20",FinanceConstants.online_order_json);
        String[] O20=     helper.createOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","50",FinanceConstants.online_order_json);


        return new Object[][]{{S01[0],S01[1]},{S02[0],S02[1]},{S03[0],S03[1]},{S04[0],S04[1]},{S05[0],S05[1]},{S06[0],S06[1]},{S07[0],S07[1]},{S08[0],S08[1]},{S09[0],S09[1]},{S10[0],S10[1]},{S11[0],S11[1]},{S12[0],S12[1]},{S13[0],S13[1]},{S14[0],S14[1]},{S15[0],S15[1]},{S16[0],S16[1]},{S17[0],S17[1]},{S18[0],S18[1]},{S19[0],S19[1]},{S20[0],S20[1]},{S21[0],S21[1]},{S22[0],S22[1]},{S23[0],S23[1]},{S24[0],S24[1]},{S25[0],S25[1]},{S26[0],S26[1]},{S27[0],S27[1]},{S28[0],S28[1]},{S29[0],S29[1]},{S30[0],S30[1]},{S31[0],S31[1]},{S32[0],S32[1]},{S33[0],S33[1]},{S34[0],S34[1]},{S35[0],S35[1]},{S36[0],S36[1]},{S37[0],S37[1]},{S38[0],S38[1]},{S39[0],S39[1]},{S40[0],S40[1]},
                {P01[0],P01[1]},{P02[0],P02[1]},{P03[0],P03[1]},{P04[0],P04[1]},{P05[0],P05[1]},{P06[0],P06[1]},{P07[0],P07[1]},{P08[0],P08[1]},{P09[0],P09[1]},{P10[0],P10[1]},{P11[0],P11[1]},{P12[0],P12[1]},{P13[0],P13[1]},{P14[0],P14[1]},{P15[0],P15[1]},{P16[0],P16[1]},{P17[0],P17[1]},{P18[0],P18[1]},{P19[0],P19[1]},{P20[0],P20[1]},{P21[0],P21[1]},{P22[0],P22[1]},{P23[0],P23[1]},{P24[0],P24[1]},{P25[0],P25[1]},{P26[0],P26[1]},{P27[0],P27[1]},{P28[0],P28[1]},{P29[0],P29[1]},{P30[0],P30[1]},{P31[0],P31[1]},{P32[0],P32[1]},{P33[0],P33[1]},{P34[0],P34[1]},{P35[0],P35[1]},{P36[0],P36[1]},{P37[0],P37[1]},{P38[0],P38[1]},{P39[0],P39[1]},{P40[0],P40[1]},
                {O01[0],O01[1]},{O02[0],O02[1]},{O03[0],O03[1]},{O04[0],O04[1]},{O05[0],O05[1]},{O06[0],O06[1]},{O07[0],O07[1]},{O08[0],O08[1]},{O09[0],O09[1]},{O10[0],O10[1]},{O11[0],O11[1]},{O12[0],O12[1]},{O13[0],O13[1]},{O14[0],O14[1]},{O15[0],O15[1]},{O16[0],O16[1]},{O17[0],O17[1]},{O18[0],O18[1]},{O19[0],O19[1]},{O20[0],O20[1]}};

    }


    //Cancelled order DataProvider

    @DataProvider(name="CancelOrderJsonsDP")
    public Object[][] orderCAPC() throws IOException, InterruptedException {
        //MultiTD DataProvider
        String[] MT01=  helper.createAndCancelOrderFinanceHelper(FinanceConstants.rest_id_prepaid_oldmou,"Prepaid","[bill_with_taxes]*20","Percentage","20","5",FinanceConstants.multi_td_online_json,FinanceConstants.rule17);
        String[] MT02=  helper.createAndCancelOrderFinanceHelper(prepaid_restId_old_mou,"Prepaid","[bill]*20","Percentage","20","5",FinanceConstants.multi_td_online_json,FinanceConstants.rule2);
        String[] MT03=  helper.createAndCancelOrderFinanceHelper(prepaid_restId_old_mou,"Prepaid","[bill_with_taxes]*20+10","Percentage","20","5",FinanceConstants.multi_td_online_json,FinanceConstants.rule3);
        String[] MT04=  helper.createAndCancelOrderFinanceHelper(prepaid_restId_old_mou,"Prepaid","[bill]*20+20","Percentage","20","5",FinanceConstants.multi_td_online_json,FinanceConstants.rule4);
        String[] MT05=  helper.createAndCancelOrderFinanceHelper(prepaid_restId_old_mou,"Prepaid","[bill_with_taxes]*20","Flat","30","5",FinanceConstants.multi_td_online_json,FinanceConstants.rule5);
        String[] MT06=  helper.createAndCancelOrderFinanceHelper(prepaid_restId_old_mou,"Prepaid","[bill]*20","Flat","30","5",FinanceConstants.multi_td_online_json,FinanceConstants.rule6);
        String[] MT07=  helper.createAndCancelOrderFinanceHelper(prepaid_restId_old_mou,"Prepaid","[bill_with_taxes]*20+10","Flat","30","5",FinanceConstants.multi_td_online_json,FinanceConstants.rule7);
        String[] MT08=  helper.createAndCancelOrderFinanceHelper(prepaid_restId_old_mou,"Prepaid","[bill]*20+20","Flat","30","5",FinanceConstants.multi_td_online_json,FinanceConstants.rule8);
        String[] MT09=  helper.createAndCancelOrderFinanceHelper(prepaid_restId_old_mou,"Prepaid","50","Flat","30","5",FinanceConstants.multi_td_online_json,FinanceConstants.rule9);
        String[] MT10=  helper.createAndCancelOrderFinanceHelper(postpaid_restId_old_mou,"Postpaid","[bill_with_taxes]*20","Percentage","20","5",FinanceConstants.multi_td_online_json,FinanceConstants.rule10);
        String[] MT11=  helper.createAndCancelOrderFinanceHelper(postpaid_restId_old_mou,"Postpaid","[bill]*20","Percentage","20","5",FinanceConstants.multi_td_online_json,FinanceConstants.rule11);
        String[] MT12=  helper.createAndCancelOrderFinanceHelper(postpaid_restId_old_mou,"Postpaid","[bill_with_taxes]*20+10","Percentage","20","5",FinanceConstants.multi_td_online_json,FinanceConstants.rule12);
        String[] MT13=  helper.createAndCancelOrderFinanceHelper(postpaid_restId_old_mou,"Postpaid","[bill]*20+20","Percentage","20","5",FinanceConstants.multi_td_online_json,FinanceConstants.rule13);
        String[] MT14=  helper.createAndCancelOrderFinanceHelper(postpaid_restId_old_mou,"Postpaid","[bill_with_taxes]*20","Flat","30","5",FinanceConstants.multi_td_online_json,FinanceConstants.rule14);
        String[] MT15=  helper.createAndCancelOrderFinanceHelper(postpaid_restId_old_mou,"Postpaid","[bill]*20","Flat","30","5",FinanceConstants.multi_td_online_json,FinanceConstants.rule1);
        String[] MT16=  helper.createAndCancelOrderFinanceHelper(postpaid_restId_old_mou,"Postpaid","[bill_with_taxes]*20+10","Flat","30","5",FinanceConstants.multi_td_online_json,FinanceConstants.rule2);
        String[] MT17=   helper.createAndCancelOrderFinanceHelper(postpaid_restId_old_mou,"Postpaid","[bill]*20+20","Flat","30","5",FinanceConstants.multi_td_online_json,FinanceConstants.rule3);
        String[] MT18=  helper.createAndCancelOrderFinanceHelper(postpaid_restId_old_mou,"Postpaid","50","Flat","30","5",FinanceConstants.multi_td_online_json,FinanceConstants.rule4);

        String[] MT19=  helper.createAndCancelOrderFinanceHelper(prepaid_restId_new_mou,"Prepaid","[bill_with_taxes]*20","Percentage","20","5",FinanceConstants.multi_td_online_json,FinanceConstants.rule5);
        String[] MT20=  helper.createAndCancelOrderFinanceHelper(prepaid_restId_new_mou,"Prepaid","[bill]*20","Percentage","20","5",FinanceConstants.multi_td_online_json,FinanceConstants.rule6);
        String[] MT21=  helper.createAndCancelOrderFinanceHelper(prepaid_restId_new_mou,"Prepaid","[bill_with_taxes]*20+10","Percentage","20","5",FinanceConstants.multi_td_online_json,FinanceConstants.rule7);
        String[] MT22=  helper.createAndCancelOrderFinanceHelper(prepaid_restId_new_mou,"Prepaid","[bill]*20+20","Percentage","20","5",FinanceConstants.multi_td_online_json,FinanceConstants.rule8);
        String[] MT23=   helper.createAndCancelOrderFinanceHelper(prepaid_restId_new_mou,"Prepaid","[bill_with_taxes]*20","Flat","30","5",FinanceConstants.multi_td_online_json,FinanceConstants.rule9);
        String[] MT24=  helper.createAndCancelOrderFinanceHelper(prepaid_restId_new_mou,"Prepaid","[bill]*20","Flat","30","5",FinanceConstants.multi_td_online_json,FinanceConstants.rule10);
        String[] MT25=  helper.createAndCancelOrderFinanceHelper(prepaid_restId_new_mou,"Prepaid","[bill_with_taxes]*20+10","Flat","30","5",FinanceConstants.multi_td_online_json,FinanceConstants.rule11);
        String[] MT26=   helper.createAndCancelOrderFinanceHelper(prepaid_restId_new_mou,"Prepaid","[bill]*20+20","Flat","30","5",FinanceConstants.multi_td_online_json,FinanceConstants.rule12);
        String[] MT27=  helper.createAndCancelOrderFinanceHelper(prepaid_restId_new_mou,"Prepaid","50","Flat","30","5",FinanceConstants.multi_td_online_json,FinanceConstants.rule13);
        String[] MT28=  helper.createAndCancelOrderFinanceHelper(postpaid_restId_new_mou,"Postpaid","[bill_with_taxes]*20","Percentage","20","5",FinanceConstants.multi_td_online_json,FinanceConstants.rule14);
        String[] MT29=  helper.createAndCancelOrderFinanceHelper(postpaid_restId_new_mou,"Postpaid","[bill]*20","Percentage","20","5",FinanceConstants.multi_td_online_json,FinanceConstants.rule1);
        String[] MT30=   helper.createAndCancelOrderFinanceHelper(postpaid_restId_new_mou,"Postpaid","[bill_with_taxes]*20+10","Percentage","20","5",FinanceConstants.multi_td_online_json,FinanceConstants.rule2);
        String[] MT31= helper.createAndCancelOrderFinanceHelper(postpaid_restId_new_mou,"Postpaid","[bill]*20+20","Percentage","20","5",FinanceConstants.multi_td_online_json,FinanceConstants.rule3);
        String[] MT32= helper.createAndCancelOrderFinanceHelper(postpaid_restId_new_mou,"Postpaid","[bill_with_taxes]*20","Flat","30","5",FinanceConstants.multi_td_online_json,FinanceConstants.rule4);
        String[] MT33=  helper.createAndCancelOrderFinanceHelper(postpaid_restId_new_mou,"Postpaid","[bill]*20","Flat","30","5",FinanceConstants.multi_td_online_json,FinanceConstants.rule5);
        String[] MT34= helper.createAndCancelOrderFinanceHelper(postpaid_restId_new_mou,"Postpaid","[bill_with_taxes]*20+10","Flat","30","5",FinanceConstants.multi_td_online_json,FinanceConstants.rule6);
        String[] MT35= helper.createAndCancelOrderFinanceHelper(postpaid_restId_new_mou,"Postpaid","[bill]*20+20","Flat","30","5",FinanceConstants.multi_td_online_json,FinanceConstants.rule7);
        String[] MT36=helper.createAndCancelOrderFinanceHelper(postpaid_restId_new_mou,"Postpaid","50","Flat","30","5",FinanceConstants.multi_td_online_json,FinanceConstants.rule8);

        String[] MT37=  helper.createAndCancelOrderFinanceHelper(prepaid_restId_old_mou,"Prepaid","[bill_with_taxes]*20","Percentage","20","5",FinanceConstants.Multi_td_cash_json,FinanceConstants.rule9);
        String[] MT38=         helper.createAndCancelOrderFinanceHelper(prepaid_restId_old_mou,"Prepaid","[bill]*20","Percentage","20","5",FinanceConstants.Multi_td_cash_json,FinanceConstants.rule10);
        String[] MT39=         helper.createAndCancelOrderFinanceHelper(prepaid_restId_old_mou,"Prepaid","[bill_with_taxes]*20+10","Percentage","20","5",FinanceConstants.Multi_td_cash_json,FinanceConstants.rule11);
        String[] MT40=         helper.createAndCancelOrderFinanceHelper(prepaid_restId_old_mou,"Prepaid","[bill]*20+20","Percentage","20","5",FinanceConstants.Multi_td_cash_json,FinanceConstants.rule12);
        String[] MT41= helper.createAndCancelOrderFinanceHelper(prepaid_restId_old_mou,"Prepaid","[bill_with_taxes]*20","Flat","30","5",FinanceConstants.Multi_td_cash_json,FinanceConstants.rule13);
        String[] MT42= helper.createAndCancelOrderFinanceHelper(prepaid_restId_old_mou,"Prepaid","[bill]*20","Flat","30","5",FinanceConstants.Multi_td_cash_json,FinanceConstants.rule14);
        String[] MT43=   helper.createAndCancelOrderFinanceHelper(prepaid_restId_old_mou,"Prepaid","[bill_with_taxes]*20+10","Flat","30","5",FinanceConstants.Multi_td_cash_json,FinanceConstants.rule1);
        String[] MT44=     helper.createAndCancelOrderFinanceHelper(prepaid_restId_old_mou,"Prepaid","[bill]*20+20","Flat","30","5",FinanceConstants.Multi_td_cash_json,FinanceConstants.rule2);
        String[] MT45=   helper.createAndCancelOrderFinanceHelper(prepaid_restId_old_mou,"Prepaid","50","Flat","30","5",FinanceConstants.Multi_td_cash_json,FinanceConstants.rule3);
        String[] MT46=    helper.createAndCancelOrderFinanceHelper(postpaid_restId_old_mou,"Postpaid","[bill_with_taxes]*20","Percentage","20","5",FinanceConstants.Multi_td_cash_json,FinanceConstants.rule4);
        String[] MT47=    helper.createAndCancelOrderFinanceHelper(postpaid_restId_old_mou,"Postpaid","[bill]*20","Percentage","20","5",FinanceConstants.Multi_td_cash_json,FinanceConstants.rule5);
        String[] MT48=    helper.createAndCancelOrderFinanceHelper(postpaid_restId_old_mou,"Postpaid","[bill_with_taxes]*20+10","Percentage","20","5",FinanceConstants.Multi_td_cash_json,FinanceConstants.rule6);
        String[] MT49=    helper.createAndCancelOrderFinanceHelper(postpaid_restId_old_mou,"Postpaid","[bill]*20+20","Percentage","20","5",FinanceConstants.Multi_td_cash_json,FinanceConstants.rule7);
        String[] MT50=    helper.createAndCancelOrderFinanceHelper(postpaid_restId_old_mou,"Postpaid","[bill_with_taxes]*20","Flat","30","5",FinanceConstants.Multi_td_cash_json,FinanceConstants.rule8);
        String[] MT51=    helper.createAndCancelOrderFinanceHelper(postpaid_restId_old_mou,"Postpaid","[bill]*20","Flat","30","5",FinanceConstants.Multi_td_cash_json,FinanceConstants.rule9);
        String[] MT52=     helper.createAndCancelOrderFinanceHelper(postpaid_restId_old_mou,"Postpaid","[bill_with_taxes]*20+10","Flat","30","5",FinanceConstants.Multi_td_cash_json,FinanceConstants.rule10);
        String[] MT53=    helper.createAndCancelOrderFinanceHelper(postpaid_restId_old_mou,"Postpaid","[bill]*20+20","Flat","30","5",FinanceConstants.Multi_td_cash_json,FinanceConstants.rule11);
        String[] MT54=    helper.createAndCancelOrderFinanceHelper(postpaid_restId_old_mou,"Postpaid","50","Flat","30","5",FinanceConstants.Multi_td_cash_json,FinanceConstants.rule12);

        String[] MT55=    helper.createAndCancelOrderFinanceHelper(prepaid_restId_new_mou,"Prepaid","[bill_with_taxes]*20","Percentage","20","5",FinanceConstants.Multi_td_cash_json,FinanceConstants.rule13);
        String[] MT56=   helper.createAndCancelOrderFinanceHelper(prepaid_restId_new_mou,"Prepaid","[bill]*20","Percentage","20","5",FinanceConstants.Multi_td_cash_json,FinanceConstants.rule14);
        String[] MT57=   helper.createAndCancelOrderFinanceHelper(prepaid_restId_new_mou,"Prepaid","[bill_with_taxes]*20+10","Percentage","20","5",FinanceConstants.Multi_td_cash_json,FinanceConstants.rule1);
        String[] MT58=  helper.createAndCancelOrderFinanceHelper(prepaid_restId_new_mou,"Prepaid","[bill]*20+20","Percentage","20","5",FinanceConstants.Multi_td_cash_json,FinanceConstants.rule2);
        String[] MT59=  helper.createAndCancelOrderFinanceHelper(prepaid_restId_new_mou,"Prepaid","[bill_with_taxes]*20","Flat","30","5",FinanceConstants.Multi_td_cash_json,FinanceConstants.rule3);
        String[] MT60=   helper.createAndCancelOrderFinanceHelper(prepaid_restId_new_mou,"Prepaid","[bill]*20","Flat","30","5",FinanceConstants.Multi_td_cash_json,FinanceConstants.rule4);
        String[] MT61=    helper.createAndCancelOrderFinanceHelper(prepaid_restId_new_mou,"Prepaid","[bill_with_taxes]*20+10","Flat","30","5",FinanceConstants.Multi_td_cash_json,FinanceConstants.rule5);
        String[] MT62=   helper.createAndCancelOrderFinanceHelper(prepaid_restId_new_mou,"Prepaid","[bill]*20+20","Flat","30","5",FinanceConstants.Multi_td_cash_json,FinanceConstants.rule6);
        String[] MT63=    helper.createAndCancelOrderFinanceHelper(prepaid_restId_new_mou,"Prepaid","50","Flat","30","5",FinanceConstants.Multi_td_cash_json,FinanceConstants.rule7);
        String[] MT64=   helper.createAndCancelOrderFinanceHelper(postpaid_restId_new_mou,"Postpaid","[bill_with_taxes]*20","Percentage","20","5",FinanceConstants.Multi_td_cash_json,FinanceConstants.rule8);
        String[] MT65=   helper.createAndCancelOrderFinanceHelper(postpaid_restId_new_mou,"Postpaid","[bill]*20","Percentage","20","5",FinanceConstants.Multi_td_cash_json,FinanceConstants.rule9);
        String[] MT66=   helper.createAndCancelOrderFinanceHelper(postpaid_restId_new_mou,"Postpaid","[bill_with_taxes]*20+10","Percentage","20","5",FinanceConstants.Multi_td_cash_json,FinanceConstants.rule10);
        String[] MT67=  helper.createAndCancelOrderFinanceHelper(postpaid_restId_new_mou,"Postpaid","[bill]*20+20","Percentage","20","5",FinanceConstants.Multi_td_cash_json,FinanceConstants.rule11);
        String[] MT68=  helper.createAndCancelOrderFinanceHelper(postpaid_restId_new_mou,"Postpaid","[bill_with_taxes]*20","Flat","30","5",FinanceConstants.Multi_td_cash_json,FinanceConstants.rule12);
        String[] MT69=   helper.createAndCancelOrderFinanceHelper(postpaid_restId_new_mou,"Postpaid","[bill]*20","Flat","30","5",FinanceConstants.Multi_td_cash_json,FinanceConstants.rule13);
        String[] MT70=     helper.createAndCancelOrderFinanceHelper(postpaid_restId_new_mou,"Postpaid","[bill_with_taxes]*20+10","Flat","30","5",FinanceConstants.Multi_td_cash_json,FinanceConstants.rule14);
        String[] MT71=    helper.createAndCancelOrderFinanceHelper(postpaid_restId_new_mou,"Postpaid","[bill]*20+20","Flat","30","5",FinanceConstants.Multi_td_cash_json,FinanceConstants.rule15);
        String[] MT72=     helper.createAndCancelOrderFinanceHelper(postpaid_restId_new_mou,"Postpaid","50","Flat","30","5",FinanceConstants.Multi_td_cash_json,FinanceConstants.rule16);

        return new Object[][]{
                {MT01[0],MT01[1]},{MT02[0],MT02[1]},{MT03[0],MT03[1]},{MT04[0],MT04[1]},{MT05[0],MT05[1]},{MT06[0],MT06[1]},{MT07[0],MT07[1]},{MT08[0],MT08[1]},{MT09[0],MT09[1]},{MT10[0],MT10[1]},{MT11[0],MT11[1]},{MT12[0],MT12[1]},{MT13[0],MT13[1]},{MT14[0],MT14[1]},{MT15[0],MT15[1]},{MT16[0],MT16[1]},{MT17[0],MT17[1]},{MT18[0],MT18[1]},{MT19[0],MT19[1]},{MT20[0],MT20[1]},{MT21[0],MT21[1]},{MT22[0],MT22[1]},{MT23[0],MT23[1]},{MT24[0],MT24[1]},{MT25[0],MT25[1]},{MT26[0],MT26[1]},{MT27[0],MT27[1]},{MT28[0],MT28[1]},{MT29[0],MT29[1]},{MT30[0],MT30[1]},{MT31[0],MT31[1]},{MT32[0],MT32[1]},{MT33[0],MT33[1]},{MT34[0],MT34[1]},{MT35[0],MT35[1]},{MT36[0],MT36[1]},{MT37[0],MT37[1]},{MT38[0],MT38[1]},{MT39[0],MT39[1]},{MT40[0],MT40[1]},{MT41[0],MT41[1]},{MT42[0],MT42[1]},
                {MT43[0],MT43[1]},{MT44[0],MT44[1]},{MT45[0],MT45[1]},{MT46[0],MT46[1]},{MT47[0],MT47[1]},{MT48[0],MT48[1]},{MT49[0],MT49[1]},{MT50[0],MT50[1]},{MT51[0],MT51[1]},{MT52[0],MT52[1]},{MT53[0],MT53[1]},{MT54[0],MT54[1]},{MT55[0],MT55[1]},{MT56[0],MT56[1]},{MT57[0],MT57[1]},{MT58[0],MT58[1]},{MT59[0],MT59[1]},{MT60[0],MT60[1]},{MT61[0],MT61[1]},{MT62[0],MT62[1]},{MT63[0],MT63[1]},{MT64[0],MT64[1]},{MT65[0],MT65[1]},{MT66[0],MT66[1]},{MT67[0],MT67[1]},{MT68[0],MT68[1]},{MT69[0],MT69[1]},{MT70[0],MT70[1]},{MT71[0],MT71[1]},{MT72[0],MT72[1]}

        };
    }


    @DataProvider(name="DP_FOR_CANCEL_POP_SUPER_NORMAL")
    public Object[][] orderHelperForCancelPOPSuperNormal() throws IOException, InterruptedException {

        //Pop Orders
        String[] P01=     helper.createCancelOrderFinanceHelperPOPSuperNormal(FinanceConstants.rest_id_prepaid_oldmou,"Postpaid","[bill]*20",FinanceConstants.pop_order_online_json,FinanceConstants.rule1);
        String[] P02=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","[bill_with_taxes]*20",FinanceConstants.pop_order_online_json,FinanceConstants.rule2);
        String[] P03=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","[bill]*20+20",FinanceConstants.pop_order_online_json,FinanceConstants.rule3);
        String[] P04=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","[bill_with_taxes]*20+20",FinanceConstants.pop_order_online_json,FinanceConstants.rule4);
        String[] P05=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","50",FinanceConstants.pop_order_online_json,FinanceConstants.rule5);

        String[] P06=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","[bill]*20",FinanceConstants.pop_order_online_json,FinanceConstants.rule6);
        String[] P07=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","[bill_with_taxes]*20",FinanceConstants.pop_order_online_json,FinanceConstants.rule7);
        String[] P08=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","[bill]*20+20",FinanceConstants.pop_order_online_json,FinanceConstants.rule8);
        String[] P09=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","[bill_with_taxes]*20+20",FinanceConstants.pop_order_online_json,FinanceConstants.rule9);
        String[] P10=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","50",FinanceConstants.pop_order_online_json,FinanceConstants.rule10);

        String[] P11=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","[bill]*20",FinanceConstants.pop_order_online_json,FinanceConstants.rule11);
        String[] P12=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","[bill_with_taxes]*20",FinanceConstants.pop_order_online_json,FinanceConstants.rule12);
        String[] P13=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","[bill]*20+20",FinanceConstants.pop_order_online_json,FinanceConstants.rule13);
        String[] P14=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","[bill_with_taxes]*20+20",FinanceConstants.pop_order_online_json,FinanceConstants.rule14);
        String[] P15=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","50",FinanceConstants.pop_order_online_json,FinanceConstants.rule15);

        String[] P16=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","[bill]*20",FinanceConstants.pop_order_online_json,FinanceConstants.rule16);
        String[] P17=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","[bill_with_taxes]*20",FinanceConstants.pop_order_online_json,FinanceConstants.rule17);
        String[] P18=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","[bill]*20+20",FinanceConstants.pop_order_online_json,FinanceConstants.rule4);
        String[] P19=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","[bill_with_taxes]*20+20",FinanceConstants.pop_order_online_json,FinanceConstants.rule5);
        String[] P20=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","50",FinanceConstants.pop_order_online_json,FinanceConstants.rule6);

        String[] P21=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","[bill]*20",FinanceConstants.pop_order_cash_json,FinanceConstants.rule7);
        String[] P22=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","[bill_with_taxes]*20",FinanceConstants.pop_order_cash_json,FinanceConstants.rule8);
        String[] P23=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","[bill]*20+20",FinanceConstants.pop_order_cash_json,FinanceConstants.rule9);
        String[] P24=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","[bill_with_taxes]*20+20",FinanceConstants.pop_order_cash_json,FinanceConstants.rule10);
        String[] P25=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","50",FinanceConstants.pop_order_cash_json,FinanceConstants.rule11);

        String[] P26=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","[bill]*20",FinanceConstants.pop_order_cash_json,FinanceConstants.rule13);
        String[] P27=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","[bill_with_taxes]*20",FinanceConstants.pop_order_cash_json,FinanceConstants.rule14);
        String[] P28=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","[bill]*20+20",FinanceConstants.pop_order_cash_json,FinanceConstants.rule1);
        String[] P29=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","[bill_with_taxes]*20+20",FinanceConstants.pop_order_cash_json,FinanceConstants.rule2);
        String[] P30=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","50",FinanceConstants.pop_order_cash_json,FinanceConstants.rule3);

        String[] P31=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","[bill]*20",FinanceConstants.pop_order_cash_json,FinanceConstants.rule4);
        String[] P32=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","[bill_with_taxes]*20",FinanceConstants.pop_order_cash_json,FinanceConstants.rule5);
        String[] P33=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","[bill]*20+20",FinanceConstants.pop_order_cash_json,FinanceConstants.rule6);
        String[] P34=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","[bill_with_taxes]*20+20",FinanceConstants.pop_order_cash_json,FinanceConstants.rule7);
        String[] P35=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","50",FinanceConstants.pop_order_cash_json,FinanceConstants.rule8);

        String[] P36=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","[bill]*20",FinanceConstants.pop_order_cash_json,FinanceConstants.rule9);
        String[] P37=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","[bill_with_taxes]*20",FinanceConstants.pop_order_cash_json,FinanceConstants.rule10);
        String[] P38=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","[bill]*20+20",FinanceConstants.pop_order_cash_json,FinanceConstants.rule11);
        String[] P39=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","[bill_with_taxes]*20+20",FinanceConstants.pop_order_cash_json,FinanceConstants.rule12);
        String[] P40=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","50",FinanceConstants.pop_order_cash_json,FinanceConstants.rule13);

        //Super Order
        String[] S01=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","[bill]*20",FinanceConstants.super_order_online_json,FinanceConstants.rule14);
        String[] S02=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","[bill_with_taxes]*20",FinanceConstants.super_order_online_json,FinanceConstants.rule15);
        String[] S03=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","[bill]*20+20",FinanceConstants.super_order_online_json,FinanceConstants.rule16);
        String[] S04=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","[bill_with_taxes]*20+20",FinanceConstants.super_order_online_json,FinanceConstants.rule17);
        String[] S05=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","50",FinanceConstants.super_order_online_json,FinanceConstants.rule1);

        String[] S06=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","[bill]*20",FinanceConstants.super_order_online_json,FinanceConstants.rule1);
        String[] S07=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","[bill_with_taxes]*20",FinanceConstants.super_order_online_json,FinanceConstants.rule6);
        String[] S08=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","[bill]*20+20",FinanceConstants.super_order_online_json,FinanceConstants.rule7);
        String[] S09=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","[bill_with_taxes]*20+20",FinanceConstants.super_order_online_json,FinanceConstants.rule8);
        String[] S10=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","50",FinanceConstants.super_order_online_json,FinanceConstants.rule9);

        String[] S11=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","[bill]*20",FinanceConstants.super_order_online_json,FinanceConstants.rule10);
        String[] S12=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","[bill_with_taxes]*20",FinanceConstants.super_order_online_json,FinanceConstants.rule11);
        String[] S13=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","[bill]*20+20",FinanceConstants.super_order_online_json,FinanceConstants.rule12);
        String[] S14=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","[bill_with_taxes]*20+20",FinanceConstants.super_order_online_json,FinanceConstants.rule13);
        String[] S15=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","50",FinanceConstants.super_order_online_json,FinanceConstants.rule14);

        String[] S16=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","[bill]*20",FinanceConstants.super_order_online_json,FinanceConstants.rule1);
        String[] S17=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","[bill_with_taxes]*20",FinanceConstants.super_order_online_json,FinanceConstants.rule2);
        String[] S18=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","[bill]*20+20",FinanceConstants.super_order_online_json,FinanceConstants.rule3);
        String[] S19=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","[bill_with_taxes]*20+20",FinanceConstants.super_order_online_json,FinanceConstants.rule4);
        String[] S20=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","50",FinanceConstants.super_order_online_json,FinanceConstants.rule5);

        String[] S21=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","[bill]*20",FinanceConstants.super_order_cash_json,FinanceConstants.rule6);
        String[] S22=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","[bill_with_taxes]*20",FinanceConstants.super_order_cash_json,FinanceConstants.rule7);
        String[] S23=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","[bill]*20+20",FinanceConstants.super_order_cash_json,FinanceConstants.rule8);
        String[] S24=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","[bill_with_taxes]*20+20",FinanceConstants.super_order_cash_json,FinanceConstants.rule9);
        String[] S25=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","50",FinanceConstants.super_order_cash_json,FinanceConstants.rule10);

        String[] S26=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","[bill]*20",FinanceConstants.super_order_cash_json,FinanceConstants.rule11);
        String[] S27=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","[bill_with_taxes]*20",FinanceConstants.super_order_cash_json,FinanceConstants.rule12);
        String[] S28=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","[bill]*20+20",FinanceConstants.super_order_cash_json,FinanceConstants.rule13);
        String[] S29=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","[bill_with_taxes]*20+20",FinanceConstants.super_order_cash_json,FinanceConstants.rule14);
        String[] S30=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","50",FinanceConstants.super_order_cash_json,FinanceConstants.rule1);

        String[] S31=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","[bill]*20",FinanceConstants.super_order_cash_json,FinanceConstants.rule2);
        String[] S32=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","[bill_with_taxes]*20",FinanceConstants.super_order_cash_json,FinanceConstants.rule3);
        String[] S33=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","[bill]*20+20",FinanceConstants.super_order_cash_json,FinanceConstants.rule4);
        String[] S34=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","[bill_with_taxes]*20+20",FinanceConstants.super_order_cash_json,FinanceConstants.rule5);
        String[] S35=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","50",FinanceConstants.super_order_cash_json,FinanceConstants.rule6);

        String[] S36=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","[bill]*20",FinanceConstants.super_order_cash_json,FinanceConstants.rule7);
        String[] S37=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","[bill_with_taxes]*20",FinanceConstants.super_order_cash_json,FinanceConstants.rule8);
        String[] S38=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","[bill]*20+20",FinanceConstants.super_order_cash_json,FinanceConstants.rule9);
        String[] S39=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","[bill_with_taxes]*20+20",FinanceConstants.super_order_cash_json,FinanceConstants.rule10);
        String[] S40=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","50",FinanceConstants.super_order_cash_json,FinanceConstants.rule11);

        //Online Order Reguler
        String[] O01=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","[bill]*20",FinanceConstants.online_order_json,FinanceConstants.rule12);
        String[] O02=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","[bill_with_taxes]*20",FinanceConstants.online_order_json,FinanceConstants.rule13);
        String[] O03=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","[bill]*20+20",FinanceConstants.online_order_json,FinanceConstants.rule14);
        String[] O04=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","[bill_with_taxes]*20+20",FinanceConstants.online_order_json,FinanceConstants.rule1);
        String[] O05=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_new_mou,"Postpaid","50",FinanceConstants.online_order_json,FinanceConstants.rule2);

        String[] O06=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","[bill]*20",FinanceConstants.online_order_json,FinanceConstants.rule3);
        String[] O07=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","[bill_with_taxes]*20",FinanceConstants.online_order_json,FinanceConstants.rule4);
        String[] O08=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","[bill]*20+20",FinanceConstants.online_order_json,FinanceConstants.rule5);
        String[] O09=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","[bill_with_taxes]*20+20",FinanceConstants.online_order_json,FinanceConstants.rule6);
        String[] O10=     helper.createCancelOrderFinanceHelperPOPSuperNormal(postpaid_restId_old_mou,"Postpaid","50",FinanceConstants.online_order_json,FinanceConstants.rule7);

        String[] O11=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","[bill]*20",FinanceConstants.online_order_json,FinanceConstants.rule8);
        String[] O12=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","[bill_with_taxes]*20",FinanceConstants.online_order_json,FinanceConstants.rule9);
        String[] O13=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","[bill]*20+20",FinanceConstants.online_order_json,FinanceConstants.rule10);
        String[] O14=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","[bill_with_taxes]*20+20",FinanceConstants.online_order_json,FinanceConstants.rule11);
        String[] O15=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_new_mou,"Prepaid","50",FinanceConstants.online_order_json,FinanceConstants.rule12);

        String[] O16=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","[bill]*20",FinanceConstants.online_order_json,FinanceConstants.rule13);
        String[] O17=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","[bill_with_taxes]*20",FinanceConstants.online_order_json,FinanceConstants.rule14);
        String[] O18=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","[bill]*20+20",FinanceConstants.online_order_json,FinanceConstants.rule15);
        String[] O19=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","[bill_with_taxes]*20+20",FinanceConstants.online_order_json,FinanceConstants.rule16);
        String[] O20=     helper.createCancelOrderFinanceHelperPOPSuperNormal(prepaid_restId_old_mou,"Prepaid","50",FinanceConstants.online_order_json,FinanceConstants.rule17);


        return new Object[][]{{P01[0],P01[1]},{P02[0],P02[1]},{P03[0],P03[1]},{P04[0],P04[1]},{P05[0],P05[1]},{P06[0],P06[1]},{P07[0],P07[1]},{P08[0],P08[1]},{P09[0],P09[1]},{P10[0],P10[1]},{P11[0],P11[1]},{P12[0],P12[1]},{P13[0],P13[1]},{P14[0],P14[1]},{P15[0],P15[1]},{P16[0],P16[1]},{P17[0],P17[1]},{P18[0],P18[1]},{P19[0],P19[1]},{P20[0],P20[1]},{P21[0],P21[1]},{P22[0],P22[1]},{P23[0],P23[1]},{P24[0],P24[1]},{P25[0],P25[1]},{P26[0],P26[1]},{P27[0],P27[1]},{P28[0],P28[1]},{P29[0],P29[1]},{P30[0],P30[1]},{P31[0],P31[1]},{P32[0],P32[1]},{P33[0],P33[1]},{P34[0],P34[1]},{P35[0],P35[1]},{P36[0],P36[1]},{P37[0],P37[1]},{P38[0],P38[1]},{P39[0],P39[1]},{P40[0],P40[1]},
        {S01[0], S01[1]}, {S02[0], S02[1]}, {S03[0], S03[1]}, {S04[0], S04[1]}, {S05[0], S05[1]}, {S06[0], S06[1]}, {S07[0], S07[1]}, {S08[0], S08[1]}, {S09[0], S09[1]}, {S10[0], S10[1]}, {S11[0], S11[1]}, {S12[0], S12[1]},{S13[0],S13[1]},{S14[0],S14[1]},{S15[0],S15[1]},{S16[0],S16[1]},{S17[0],S17[1]},{S18[0],S18[1]},{S19[0],S19[1]},{S20[0],S20[1]},{S21[0],S21[1]},{S22[0],S22[1]},{S23[0],S23[1]},{S24[0],S24[1]},{S25[0],S25[1]},{S26[0],S26[1]},{S27[0],S27[1]},{S28[0],S28[1]},{S29[0],S29[1]},{S30[0],S30[1]},{S31[0],S31[1]},{S32[0],S32[1]},{S33[0],S33[1]},{S34[0],S34[1]},{S35[0],S35[1]},{S36[0],S36[1]},{S37[0],S37[1]},{S38[0],S38[1]},{S39[0],S39[1]},{S40[0],S40[1]},

                {O01[0],O01[1]},{O02[0],O02[1]},{O03[0],O03[1]},{O04[0],O04[1]},{O05[0],O05[1]},{O06[0],O06[1]},{O07[0],O07[1]},{O08[0],O08[1]},{O09[0],O09[1]},{O10[0],O10[1]},{O11[0],O11[1]},{O12[0],O12[1]},{O13[0],O13[1]},{O14[0],O14[1]},{O15[0],O15[1]},{O16[0],O16[1]},{O17[0],O17[1]},{O18[0],O18[1]},{O19[0],O19[1]},{O20[0],O20[1]}
          }  ;
    }

    @DataProvider(name="zombieOrderDP")
    public Object[][] zombieOrderDataProvider() throws IOException, InterruptedException {

        //Super Order
        String[] S01=     helper.createZombieOrderHelper(postpaid_restId_new_mou,"Postpaid","[bill]*20",FinanceConstants.super_order_online_json);
        String[] S02=     helper.createZombieOrderHelper(postpaid_restId_new_mou,"Postpaid","[bill_with_taxes]*20",FinanceConstants.super_order_online_json);
        String[] S03=     helper.createZombieOrderHelper(postpaid_restId_new_mou,"Postpaid","[bill]*20+20",FinanceConstants.super_order_online_json);
        String[] S04=     helper.createZombieOrderHelper(postpaid_restId_new_mou,"Postpaid","[bill_with_taxes]*20+20",FinanceConstants.super_order_online_json);
        String[] S05=     helper.createZombieOrderHelper(postpaid_restId_new_mou,"Postpaid","50",FinanceConstants.super_order_online_json);

        String[] S06=     helper.createZombieOrderHelper(postpaid_restId_old_mou,"Postpaid","[bill]*20",FinanceConstants.super_order_online_json);
        String[] S07=     helper.createZombieOrderHelper(postpaid_restId_old_mou,"Postpaid","[bill_with_taxes]*20",FinanceConstants.super_order_online_json);
        String[] S08=     helper.createZombieOrderHelper(postpaid_restId_old_mou,"Postpaid","[bill]*20+20",FinanceConstants.super_order_online_json);
        String[] S09=     helper.createZombieOrderHelper(postpaid_restId_old_mou,"Postpaid","[bill_with_taxes]*20+20",FinanceConstants.super_order_online_json);
        String[] S10=     helper.createZombieOrderHelper(postpaid_restId_old_mou,"Postpaid","50",FinanceConstants.super_order_online_json);

        String[] S11=     helper.createZombieOrderHelper(prepaid_restId_new_mou,"Prepaid","[bill]*20",FinanceConstants.super_order_online_json);
        String[] S12=     helper.createZombieOrderHelper(prepaid_restId_new_mou,"Prepaid","[bill_with_taxes]*20",FinanceConstants.super_order_online_json);
        String[] S13=     helper.createZombieOrderHelper(prepaid_restId_new_mou,"Prepaid","[bill]*20+20",FinanceConstants.super_order_online_json);
        String[] S14=     helper.createZombieOrderHelper(prepaid_restId_new_mou,"Prepaid","[bill_with_taxes]*20+20",FinanceConstants.super_order_online_json);
        String[] S15=     helper.createZombieOrderHelper(prepaid_restId_new_mou,"Prepaid","50",FinanceConstants.super_order_online_json);

        String[] S16=     helper.createZombieOrderHelper(prepaid_restId_old_mou,"Prepaid","[bill]*20",FinanceConstants.super_order_online_json);
        String[] S17=     helper.createZombieOrderHelper(prepaid_restId_old_mou,"Prepaid","[bill_with_taxes]*20",FinanceConstants.super_order_online_json);
        String[] S18=     helper.createZombieOrderHelper(prepaid_restId_old_mou,"Prepaid","[bill]*20+20",FinanceConstants.super_order_online_json);
        String[] S19=     helper.createZombieOrderHelper(prepaid_restId_old_mou,"Prepaid","[bill_with_taxes]*20+20",FinanceConstants.super_order_online_json);
        String[] S20=     helper.createZombieOrderHelper(prepaid_restId_old_mou,"Prepaid","50",FinanceConstants.super_order_online_json);

        //Online Order Reguler
        String[] O01=     helper.createZombieOrderHelper(postpaid_restId_new_mou,"Postpaid","[bill]*20",FinanceConstants.online_order_json);
        String[] O02=     helper.createZombieOrderHelper(postpaid_restId_new_mou,"Postpaid","[bill_with_taxes]*20",FinanceConstants.online_order_json);
        String[] O03=     helper.createZombieOrderHelper(postpaid_restId_new_mou,"Postpaid","[bill]*20+20",FinanceConstants.online_order_json);
        String[] O04=     helper.createZombieOrderHelper(postpaid_restId_new_mou,"Postpaid","[bill_with_taxes]*20+20",FinanceConstants.online_order_json);
        String[] O05=     helper.createZombieOrderHelper(postpaid_restId_new_mou,"Postpaid","50",FinanceConstants.online_order_json);

        String[] O06=     helper.createZombieOrderHelper(postpaid_restId_old_mou,"Postpaid","[bill]*20",FinanceConstants.online_order_json);
        String[] O07=     helper.createZombieOrderHelper(postpaid_restId_old_mou,"Postpaid","[bill_with_taxes]*20",FinanceConstants.online_order_json);
        String[] O08=     helper.createZombieOrderHelper(postpaid_restId_old_mou,"Postpaid","[bill]*20+20",FinanceConstants.online_order_json);
        String[] O09=     helper.createZombieOrderHelper(postpaid_restId_old_mou,"Postpaid","[bill_with_taxes]*20+20",FinanceConstants.online_order_json);
        String[] O10=     helper.createZombieOrderHelper(postpaid_restId_old_mou,"Postpaid","50",FinanceConstants.online_order_json);

        String[] O11=     helper.createZombieOrderHelper(prepaid_restId_new_mou,"Prepaid","[bill]*20",FinanceConstants.online_order_json);
        String[] O12=     helper.createZombieOrderHelper(prepaid_restId_new_mou,"Prepaid","[bill_with_taxes]*20",FinanceConstants.online_order_json);
        String[] O13=     helper.createZombieOrderHelper(prepaid_restId_new_mou,"Prepaid","[bill]*20+20",FinanceConstants.online_order_json);
        String[] O14=     helper.createZombieOrderHelper(prepaid_restId_new_mou,"Prepaid","[bill_with_taxes]*20+20",FinanceConstants.online_order_json);
        String[] O15=     helper.createZombieOrderHelper(prepaid_restId_new_mou,"Prepaid","50",FinanceConstants.online_order_json);

        String[] O16=     helper.createZombieOrderHelper(prepaid_restId_old_mou,"Prepaid","[bill]*20",FinanceConstants.online_order_json);
        String[] O17=     helper.createZombieOrderHelper(prepaid_restId_old_mou,"Prepaid","[bill_with_taxes]*20",FinanceConstants.online_order_json);
        String[] O18=     helper.createZombieOrderHelper(prepaid_restId_old_mou,"Prepaid","[bill]*20+20",FinanceConstants.online_order_json);
        String[] O19=     helper.createZombieOrderHelper(prepaid_restId_old_mou,"Prepaid","[bill_with_taxes]*20+20",FinanceConstants.online_order_json);
        String[] O20=     helper.createZombieOrderHelper(prepaid_restId_old_mou,"Prepaid","50",FinanceConstants.online_order_json);


        return new Object[][]{{S01[0],S01[1]},{S02[0],S02[1]},{S03[0],S03[1]},{S04[0],S04[1]},{S05[0],S05[1]},{S06[0],S06[1]},{S07[0],S07[1]},{S08[0],S08[1]},{S09[0],S09[1]},{S10[0],S10[1]},{S11[0],S11[1]},{S12[0],S12[1]},{S13[0],S13[1]},{S14[0],S14[1]},{S15[0],S15[1]},{S16[0],S16[1]},{S17[0],S17[1]},{S18[0],S18[1]},{S19[0],S19[1]},{S20[0],S20[1]},
                {O01[0],O01[1]},{O02[0],O02[1]},{O03[0],O03[1]},{O04[0],O04[1]},{O05[0],O05[1]},{O06[0],O06[1]},{O07[0],O07[1]},{O08[0],O08[1]},{O09[0],O09[1]},{O10[0],O10[1]},{O11[0],O11[1]},{O12[0],O12[1]},{O13[0],O13[1]},{O14[0],O14[1]},{O15[0],O15[1]},{O16[0],O16[1]},{O17[0],O17[1]},{O18[0],O18[1]},{O19[0],O19[1]},{O20[0],O20[1]}};

    }



    @DataProvider(name = "autoReconciliationCancelledOrderOldMouDP")
    public Object[][] autoReconciliationCancelledOrderOldMouDP() {
        String[] orderID_prepaid_oldmou = helper.createOrderOfAGivenType(prepaid_restId_old_mou,"Prepaid",replicatedFalse,billWithoutTax);
        String[] orderID_postpaid_oldmou = helper.createOrderOfAGivenType(postpaid_restId_old_mou,"Postpaid",replicatedFalse,billWithTax);
        String[] orderID_pop = helper.createOtherOrders(postpaid_restId_old_mou,"Postpaid","../Data/Payloads/JSON/fin2_CreatePopOrder.json");
        String[] orderID_super = helper.createOtherOrders(postpaid_restId_old_mou,"Postpaid","../Data/Payloads/JSON/fin2_CreateSuperOrder.json");
        String[] orderID_multiTD = helper.createOtherOrders(postpaid_restId_old_mou,"Postpaid","../Data/Payloads/JSON/fin2_CreateMultiTDOrder.json");
        String[] orderID_dominos = helper.createOtherOrders(postpaid_restId_old_mou,"Postpaid","../Data/Payloads/JSON/losCreateDominosOrder.json");
        String[] orderID_Online_Tax = helper.createOnlineOrders(postpaid_restId_old_mou,"Postpaid","../Data/Payloads/JSON/fin2_CreateOnlineOrder.json",billWithTax);
        String[] orderID_Online_WithoutTax = helper.createOnlineOrders(postpaid_restId_old_mou,"Postpaid","../Data/Payloads/JSON/fin2_CreateOnlineOrder.json",billWithoutTax);

        return new Object[][] {
                {orderID_prepaid_oldmou[0],orderID_prepaid_oldmou[1]},
                {orderID_postpaid_oldmou[0],orderID_postpaid_oldmou[1]},
                {orderID_pop[0],orderID_pop[1]},
                {orderID_super[0],orderID_super[1]},
                {orderID_multiTD[0],orderID_multiTD[1]},
                {orderID_dominos[0],orderID_dominos[1]},
                {orderID_Online_Tax[0],orderID_Online_Tax[1]},
                {orderID_Online_WithoutTax[0],orderID_Online_WithoutTax[1]}
        };
    }

    @DataProvider(name = "autoReconciliationCancelledOrderNewMouDP")
    public Object[][] autoReconciliationCancelledOrderNewMouDP() {
        String[] orderID_prepaid_newmou = helper.createOrderOfAGivenType(prepaid_restId_new_mou,"Prepaid",replicatedFalse,billWithoutTax);
        String[] orderID_postpaid_newmou = helper.createOrderOfAGivenType(postpaid_restId_new_mou,"Postpaid",replicatedFalse,billWithTax);
        String[] orderID_pop = helper.createOtherOrders(postpaid_restId_old_mou,"Postpaid","../Data/Payloads/JSON/fin2_CreatePopOrder.json");
        String[] orderID_super = helper.createOtherOrders(postpaid_restId_old_mou,"Postpaid","../Data/Payloads/JSON/fin2_CreateSuperOrder.json");
        String[] orderID_multiTD = helper.createOtherOrders(postpaid_restId_old_mou,"Postpaid","../Data/Payloads/JSON/fin2_CreateMultiTDOrder.json");
        String[] orderID_dominos = helper.createOtherOrders(postpaid_restId_old_mou,"Postpaid","../Data/Payloads/JSON/losCreateDominosOrder.json");
        String[] orderID_Online_Tax = helper.createOnlineOrders(postpaid_restId_old_mou,"Postpaid","../Data/Payloads/JSON/fin2_CreateOnlineOrder.json",billWithTax);
        String[] orderID_Online_WithoutTax = helper.createOnlineOrders(postpaid_restId_old_mou,"Postpaid","../Data/Payloads/JSON/fin2_CreateOnlineOrder.json",billWithoutTax);

        return new Object[][] {
                {orderID_prepaid_newmou[0],orderID_prepaid_newmou[1]},
                {orderID_postpaid_newmou[0],orderID_postpaid_newmou[1]},
                {orderID_pop[0],orderID_pop[1]},
                {orderID_super[0],orderID_super[1]},
                {orderID_multiTD[0],orderID_multiTD[1]},
                {orderID_dominos[0],orderID_dominos[1]},
                {orderID_Online_Tax[0],orderID_Online_Tax[1]},
                {orderID_Online_WithoutTax[0],orderID_Online_WithoutTax[1]}
        };
    }

    @DataProvider(name = "payoutDP")
    public Object[][] payoutDP() throws IOException, InterruptedException {
    	String[] orderID_multi_td = helper.createOrderOfAGivenType(restId_Multi_Td,"multiTd",replicatedFalse,billWithoutTax);
        String[] orderID_prepaid_newmou = helper.createOrderOfAGivenType(prepaid_restId_new_mou,"Prepaid",replicatedFalse,billWithoutTax);
        String[] orderID_postpaid_newmou = helper.createOrderOfAGivenType(postpaid_restId_new_mou,"Postpaid",replicatedFalse,billWithoutTax);
        String[] orderID_prepaid_oldmou = helper.createOrderOfAGivenType(prepaid_restId_old_mou,"Prepaid",replicatedFalse,billWithoutTax);
        String[] orderID_postpaid_oldmou = helper.createOrderOfAGivenType(postpaid_restId_old_mou,"Postpaid",replicatedFalse,billWithoutTax);
        String[] orderID_dominos = helper.createOtherOrders(postpaid_restId_old_mou,"Postpaid","../Data/Payloads/JSON/losCreateDominosOrder.json");
        String[] orderID_pop = helper.createOtherOrders(postpaid_restId_old_mou,"Postpaid","../Data/Payloads/JSON/lospopOrder.json");
        String[] orderID_diff_payment_method = helper.createOtherOrders(postpaid_restId_old_mou,"Postpaid","../Data/Payloads/JSON/los_createOrderWithDifferentPaymentMethods.json");

        for (int i = 0; i < statusList.length; i++) {
            losHelper.statusUpdate(orderID_prepaid_newmou[0],orderID_prepaid_newmou[1],statusList[i]);
            losHelper.statusUpdate(orderID_postpaid_newmou[0],orderID_postpaid_newmou[1],statusList[i]);
            losHelper.statusUpdate(orderID_prepaid_oldmou[0],orderID_prepaid_oldmou[1],statusList[i]);
            losHelper.statusUpdate(orderID_postpaid_oldmou[0],orderID_postpaid_oldmou[1],statusList[i]);
            losHelper.statusUpdate(orderID_dominos[0],orderID_dominos[1],statusList[i]);

            losHelper.statusUpdate(orderID_pop[0],orderID_pop[1],statusList[i]);

            losHelper.statusUpdate(orderID_diff_payment_method[0],orderID_diff_payment_method[1],statusList[i]);

        }
        return new Object[][] {
                {orderID_prepaid_newmou[0],orderID_prepaid_newmou[1]},
                {orderID_postpaid_newmou[0],orderID_postpaid_newmou[1]},
                {orderID_prepaid_oldmou[0],orderID_prepaid_oldmou[1]},
                {orderID_postpaid_oldmou[0],orderID_postpaid_oldmou[1]},
                {orderID_multi_td[0],orderID_multi_td[1]},
                {orderID_dominos[0],orderID_dominos[1]},
                {orderID_pop[0],orderID_pop[1]},
                {orderID_diff_payment_method[0],orderID_diff_payment_method[1]}

        };

    }

    @DataProvider(name = "invoiceDP")
    public Object[][] invoiceDP() throws IOException, InterruptedException {
        String[] orderID_prepaid_newmou = helper.createOrderOfAGivenType(prepaid_restId_new_mou,"Prepaid",replicatedFalse,billWithoutTax);
        String[] orderID_postpaid_newmou = helper.createOrderOfAGivenType(postpaid_restId_new_mou,"Postpaid",replicatedFalse,billWithoutTax);
        String[] orderID_prepaid_oldmou = helper.createOrderOfAGivenType(prepaid_restId_old_mou,"Prepaid",replicatedFalse,billWithoutTax);
        String[] orderID_postpaid_oldmou = helper.createOrderOfAGivenType(postpaid_restId_old_mou,"Postpaid",replicatedFalse,billWithoutTax);
        for (int i = 0; i < statusList.length; i++) {
            losHelper.statusUpdate(orderID_prepaid_newmou[0],orderID_prepaid_newmou[1],statusList[i]);
            losHelper.statusUpdate(orderID_postpaid_newmou[0],orderID_postpaid_newmou[1],statusList[i]);
            losHelper.statusUpdate(orderID_prepaid_oldmou[0],orderID_prepaid_oldmou[1],statusList[i]);
            losHelper.statusUpdate(orderID_postpaid_oldmou[0],orderID_postpaid_oldmou[1],statusList[i]);
        }

        return new Object[][] {
                {orderID_prepaid_newmou[0],prepaid_restId_new_mou},
                {orderID_postpaid_newmou[0],postpaid_restId_new_mou},
                {orderID_prepaid_oldmou[0],prepaid_restId_old_mou},
                {orderID_postpaid_oldmou[0],postpaid_restId_old_mou}
        };
    }

    @DataProvider(name = "masterFileDP")
    public Object[][] masterFileDP() throws IOException, InterruptedException {
        String from = dateHelper.getDatePlusOrMinusCurrentDate_yyyy_mm_dd(-1);
        String to = dateHelper.getCurrentDateIn_yyyy_mm_dd();
        String[] orderID_prepaid_newmou = helper.createOrderOfAGivenType(prepaid_restId_new_mou,"Prepaid",replicatedFalse,billWithoutTax);
        String[] orderID_postpaid_newmou = helper.createOrderOfAGivenType(postpaid_restId_new_mou,"Postpaid",replicatedFalse,billWithoutTax);
        String[] orderID_prepaid_oldmou = helper.createOrderOfAGivenType(prepaid_restId_old_mou,"Prepaid",replicatedFalse,billWithoutTax);
        String[] orderID_postpaid_oldmou = helper.createOrderOfAGivenType(postpaid_restId_old_mou,"Postpaid",replicatedFalse,billWithoutTax);
        String[] orderID_dominos = helper.createOtherOrders(postpaid_restId_old_mou,"Postpaid","../Data/Payloads/JSON/losCreateDominosOrder.json");
        String[] orderID_pop = helper.createOtherOrders(postpaid_restId_old_mou,"Postpaid","../Data/Payloads/JSON/lospopOrder.json");
        String[] orderID_diff_payment_method = helper.createOtherOrders(postpaid_restId_old_mou,"Postpaid","../Data/Payloads/JSON/los_createOrderWithDifferentPaymentMethods.json");

        for (int i = 0; i < statusList.length; i++) {
            losHelper.statusUpdate(orderID_prepaid_newmou[0],orderID_prepaid_newmou[1],statusList[i]);
            losHelper.statusUpdate(orderID_postpaid_newmou[0],orderID_postpaid_newmou[1],statusList[i]);
            losHelper.statusUpdate(orderID_prepaid_oldmou[0],orderID_prepaid_oldmou[1],statusList[i]);
            losHelper.statusUpdate(orderID_postpaid_oldmou[0],orderID_postpaid_oldmou[1],statusList[i]);
            losHelper.statusUpdate(orderID_dominos[0],orderID_dominos[1],statusList[i]);
            losHelper.statusUpdate(orderID_pop[0],orderID_pop[1],statusList[i]);
            losHelper.statusUpdate(orderID_diff_payment_method[0],orderID_diff_payment_method[1],statusList[i]);
       
        }
        return new Object[][] {
                {from,to,orderID_prepaid_newmou[0]},
                {from,to,orderID_postpaid_newmou[0]},
                {from,to,orderID_prepaid_oldmou[0]},
                {from,to,orderID_postpaid_oldmou[0]},
                {orderID_dominos[0],orderID_dominos[1]},
                {orderID_pop[0],orderID_pop[1]},
                {orderID_diff_payment_method[0],orderID_diff_payment_method[1]}};
    }

    
    @DataProvider(name = "replicatedOrder")
    public Object[][] replicatedOrder() throws IOException {
      String[] orderID_postpaid_oldmou = helper.createOrderOfAGivenType(prepaid_restId_old_mou,"Prepaid",replicatedFalse,billWithoutTax);
      String[] orderID_postpaid_replicate = helper.createOrderOfAGivenType(prepaid_restId_old_mou,"Prepaid",replicatedTrue,billWithoutTax);

        return new Object[][] {
                {orderID_postpaid_oldmou[0],orderID_postpaid_replicate[0]}
               

        };
    }

        
}
