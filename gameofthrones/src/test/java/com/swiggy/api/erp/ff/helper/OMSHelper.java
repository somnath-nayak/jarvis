package com.swiggy.api.erp.ff.helper;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceHelper;
import com.swiggy.api.erp.delivery.helper.EndToEndHelp;
import com.swiggy.api.erp.ff.constants.LosConstants;
import com.swiggy.api.erp.ff.constants.OMSConstants;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.pojo.Cart;
import com.swiggy.api.sf.checkout.pojo.CreateMenuEntry;
import com.swiggy.api.sf.checkout.pojo.CreateOrderBuilder;
import com.swiggy.api.sf.snd.helper.OrderPlace;
import com.swiggy.api.sf.snd.helper.SnDHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Daenerys.Initializer;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.RedisHelper;
import org.testng.Assert;

import javax.ws.rs.core.Cookie;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class OMSHelper implements OMSConstants {
	//Initialize gameofthrones = new Initialize();
	Initialize gameofthrones =Initializer.getInitializer();
	RedisHelper redisHelper = new RedisHelper();
	CheckoutHelper checkoutHelper = new CheckoutHelper();


	Long order_id;
	String tid, token, servicableAddress = null;

	public void redisFlushAll(String name) {
		redisHelper.flushAll(name);
	}

	public ArrayList<String> getOmsSession() {
		HashMap<String, String> requestheaders_getSession = new HashMap<String, String>();
		requestheaders_getSession.put("Content-Type", "application/json");
		String[] params = new String[]{"shashank", "Sha@shank1"};
		GameOfThronesService getSession = new GameOfThronesService("oms", "getsession", gameofthrones);
		Processor processor = new Processor(getSession, requestheaders_getSession);
		String csrf_token = processor.ResponseValidator.GetNodeValue("csrf_token");
		System.out.println("csrf_token is = " + processor.ResponseValidator.GetNodeValue("csrf_token"));
		String sessionid = processor.ResponseValidator.GetNodeValue("sessionId");
		System.out.println("session ID is = " + processor.ResponseValidator.GetNodeValue("sessionId"));
		String cookie = "csrftoken=" + csrf_token + "; sessionid=" + sessionid;
		ArrayList<String> sessionData = new ArrayList<String>();
		sessionData.add(csrf_token);
		sessionData.add(cookie);
		return sessionData;
	}
	


	public String getMeAnOrder(String restId) throws Exception {
		String orderId = null;
		CheckoutHelper checkoutHelper = new CheckoutHelper();
		try {
			Processor loginResponse = SnDHelper.consumerLogin(mobile, password);
			String tid = loginResponse.ResponseValidator.GetNodeValue("tid");
			System.out.println("Tid" + tid);
			String sid = loginResponse.ResponseValidator.GetNodeValue("sid");
			String token = loginResponse.ResponseValidator.GetNodeValue("data.token");
			OrderPlace orderPlace = new OrderPlace();
			EndToEndHelp endToEndHelp = new EndToEndHelp();
			List<Cart> cart = new ArrayList<>();
			//cart.add(new Cart(null, null, endToEndHelp.getRestaurantMenu("683"), 1));
			cart.add(new Cart(null, null, orderPlace.getRestaurantMenu(restId,tid,token), 1));
			CreateMenuEntry createOrder = new CreateOrderBuilder()
					.password(password)
					.mobile(Long.parseLong(mobile))
					.cart(cart)
					.restaurant(Integer.parseInt(restId))
					.paymentMethod("Cash")
					.orderComments("Test-Order")
					.buildAll();
			orderId = orderPlace.createOrder(createOrder);
			System.out.println(orderId);
			DeliveryServiceHelper delhelp = new DeliveryServiceHelper();
			delhelp.processOrderInDeliveryProd(orderId, "placed", "216");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return orderId;
	}

	public String getMeRestId(String orderId){
	    String rest_id = "";
        List<Map<String, Object>> restId = SystemConfigProvider.getTemplate(OMSConstants.SERVICE).queryForList("select restaurant_id from oms_restaurantdetails where id = (select restaurant_details_id from oms_order where order_id =" +orderId + ");");
        rest_id= restId.get(0).get("restaurant_id").toString();
        return rest_id;
    }


	/**
	 * Get OMS CSRF Token and Session ID For Further Order Flow Operations
	 *
	 * @param name
	 * @param password
	 * @return HashMap
	 */
	public HashMap<String, String> getSession(String name, String password) {
		String sessionId = "h7f0oe7hdrcyyvpx1pjy2k57att0r9h0";
		HashMap<String, String> token = new HashMap<>();
		GameOfThronesService service = new GameOfThronesService(OMSConstants.SERVICE, "getsession", gameofthrones);
		HashMap<String, String> requestheaders = new HashMap<>();
		requestheaders.put("content-type", "application/json");
		List<Cookie> cookies = new ArrayList<>();
		cookies.add(new Cookie("sessionid", sessionId));
		Processor processor = new Processor(requestheaders, null, new String[]{name, password}, cookies, service);
		String response = processor.ResponseValidator.GetBodyAsText();
		String csrf_token = JsonPath.read(response, "$.csrf_token");
//		String csrf_token = "roWJvIHFVF6kgaeGH3k6Dt5YWzVg7ozJ";
		token.put("csrfToken", csrf_token);
		token.put("sessionId", sessionId);
		return token;
	}

	private boolean validateResponseStatus(Processor processor) {
		System.out.println(processor.ResponseValidator.GetResponseCode());
		if (processor.ResponseValidator.GetResponseCode() == 200) {
			return true;
		} else {
			Assert.assertTrue(false);
			return false;
		}
	}

	/**
	 * Verify Order in OMS
	 *
	 * @param orderId
	 * @param action
	 * @return Processor
	 */
	public boolean verifyOrder(String orderId, String action) {
		HashMap<String, String> session = getSession(USERNAME, PASSSWORD);

		GameOfThronesService service = new GameOfThronesService(OMSConstants.SERVICE, "verifyorder", gameofthrones);
		Processor processor = new Processor(OMSConstants.getOMSHeader(session.get("csrfToken")), new String[]{orderId, action},
				null, OMSConstants.getOMSCookies(session.get("csrfToken")), service);
		return validateResponseStatus(processor);
	}

	public boolean verifyOrders(String orderId, String action, String user, String password){
		HashMap<String, String> session = getSession(user, password);
		GameOfThronesService service = new GameOfThronesService(OMSConstants.SERVICE, "verifyorder", gameofthrones);
		Processor processor = new Processor(OMSConstants.getOMSHeader(session.get("csrfToken")), new String[]{orderId, action},
				null, OMSConstants.getOMSCookies(session.get("csrfToken")), service);
		return validateResponseStatus(processor);
	}

	public boolean reassignPlacer(String orderId) {
		HashMap<String, String> session = getSession(OMSConstants.USERNAME, OMSConstants.PASSSWORD);
		GameOfThronesService service = new GameOfThronesService(OMSConstants.SERVICE, "reassignplacer", gameofthrones);
		Processor processor = new Processor(OMSConstants.getOMSHeader(session.get("csrfToken")), new String[]{orderId},
				null, OMSConstants.getOMSCookies(session.get("csrfToken")), service);
		return validateResponseStatus(processor);
	}

	public boolean deAssistance(String orderId, String reason) {
		HashMap<String, String> session = getSession(OMSConstants.USERNAME, OMSConstants.PASSSWORD);
		GameOfThronesService service = new GameOfThronesService(OMSConstants.SERVICE, "deassistance", gameofthrones);
		Processor processor = new Processor(OMSConstants.getOMSHeader(session.get("csrfToken")), new String[]{orderId, reason},
				null, OMSConstants.getOMSCookies(session.get("csrfToken")), service);
		return validateResponseStatus(processor);
	}

	public boolean deAssistanceCloseRequest(String orderId, String reason) {
		HashMap<String, String> session = getSession(OMSConstants.USERNAME, OMSConstants.PASSSWORD);
		GameOfThronesService service = new GameOfThronesService(OMSConstants.SERVICE, "deassistancecloserequest", gameofthrones);
		Processor processor = new Processor(OMSConstants.getOMSHeader(session.get("csrfToken")), new String[]{orderId, reason},
				null, OMSConstants.getOMSCookies(session.get("csrfToken")), service);
		return validateResponseStatus(processor);
	}

	public boolean unparkOrder(String orderId) {
		HashMap<String, String> session = getSession(OMSConstants.USERNAME, OMSConstants.PASSSWORD);
		GameOfThronesService service = new GameOfThronesService(OMSConstants.SERVICE, "unpark", gameofthrones);
		Processor processor = new Processor(OMSConstants.getOMSHeader(session.get("csrfToken")), new String[]{orderId, String.valueOf(Instant.now().toEpochMilli())},
				null, OMSConstants.getOMSCookies(session.get("csrfToken")), service);
		return validateResponseStatus(processor);
	}


	/**
	 * Verify Order in OMS
	 *
	 * @param orderId
	 * @param action
	 * @return Processor
	 */
	public boolean verifyOrder(String orderId, String action, String csrf_Token, String sessionId) {
		GameOfThronesService service = new GameOfThronesService(OMSConstants.SERVICE, "verifyorder", gameofthrones);
		Processor processor = new Processor(OMSConstants.getOMSHeader(csrf_Token), new String[]{orderId, action},
				null, OMSConstants.getOMSCookies(csrf_Token), service);
		return validateResponseStatus(processor);
	}

	/**
	 * Change Order Status to Placed
	 *
	 * @param orderId
	 * @return Processor
	 */
	public boolean changeOrderStatusToPlaced(String orderId) {
		HashMap<String, String> session = getSession(OMSConstants.USERNAME, OMSConstants.PASSSWORD);
		GameOfThronesService service = new GameOfThronesService(OMSConstants.SERVICE, "updateStatus", gameofthrones);
		Processor processor = new Processor(OMSConstants.getOMSHeader(session.get("csrfToken")), new String[]{orderId, "placed", "" + Instant.now().getEpochSecond()},
				null, OMSConstants.getOMSCookies(session.get("csrfToken")), service);
		System.out.println("Response Code : ------ " + processor.ResponseValidator.GetResponseCode());
		return validateResponseStatus(processor);
	}

	public boolean changeOrderStatusToPlaced(String orderId, String user, String password) {
		HashMap<String, String> session = getSession(user, password);
		GameOfThronesService service = new GameOfThronesService(OMSConstants.SERVICE, "updateStatus", gameofthrones);
		Processor processor = new Processor(OMSConstants.getOMSHeader(session.get("csrfToken")), new String[]{orderId, "placed", "" + Instant.now().getEpochSecond()},
				null, OMSConstants.getOMSCookies(session.get("csrfToken")), service);
		System.out.println("Response Code : ------ " + processor.ResponseValidator.GetResponseCode());
		return validateResponseStatus(processor);
	}

	/**
	 * Change Order Status to Placed
	 *
	 * @param orderId
	 * @return Processor
	 */
	public boolean changeOrderStatusToPlaced(String orderId, String csrfToken) {
		GameOfThronesService service = new GameOfThronesService(OMSConstants.SERVICE, "updateStatus", gameofthrones);

		Processor processor = new Processor(OMSConstants.getOMSHeader(csrfToken), new String[]{orderId, "placed", "" + Instant.now().getEpochSecond()},
				null, OMSConstants.getOMSCookies(csrfToken), service);
		System.out.println("Response Code : ------ " + processor.ResponseValidator.GetResponseCode());
		return validateResponseStatus(processor);
	}

	public boolean changeOrderStatusToPlacedPartner(String orderId) {
		GameOfThronesService service = new GameOfThronesService(OMSConstants.SERVICE, "restconfirm", gameofthrones);
HashMap<String,String>headers = new HashMap<>();
		Processor processor = new Processor(headers, new String[]{orderId, getMeRestId(orderId)},
				null, null, service);
		System.out.println("Response Code : ------ " + processor.ResponseValidator.GetResponseCode());
		return validateResponseStatus(processor);
	}


	/**
	 * Assign DE
	 *
	 * @param orderId
	 * @return Processor
	 */
	public boolean assignDE(String orderId, String deId) {
		HashMap<String, String> session = getSession(OMSConstants.USERNAME, OMSConstants.PASSSWORD);
		GameOfThronesService service = new GameOfThronesService(OMSConstants.SERVICE, "assignDE", gameofthrones);
		Processor processor = new Processor(OMSConstants.getOMSHeader(session.get("csrfToken")), new String[]{orderId, deId},
				null, OMSConstants.getOMSCookies(session.get("csrfToken")), service);

		return validateResponseStatus(processor);
	}

	/**
	 * Change Order Status to Confirmed
	 *
	 * @param orderId
	 * @return Processor
	 */
	public boolean changeOrderStatusToConfirmed(String orderId, String deId) {
		HashMap<String, String> session = getSession(OMSConstants.USERNAME, OMSConstants.PASSSWORD);
		GameOfThronesService service = new GameOfThronesService(OMSConstants.SERVICE, "updateStatus", gameofthrones);
		Processor processor = new Processor(OMSConstants.getOMSHeader(session.get("csrfToken")), new String[]{orderId, "confirmed", "" + Instant.now().getEpochSecond()},
				null, OMSConstants.getOMSCookies(session.get("csrfToken")), service);
		return validateResponseStatus(processor);
	}

	/**
	 * Change Order Status to Arrived
	 *
	 * @param orderId
	 * @return Processor
	 */
	public boolean changeOrderStatusToArrived(String orderId) {
		HashMap<String, String> session = getSession(OMSConstants.USERNAME, OMSConstants.PASSSWORD);
		GameOfThronesService service = new GameOfThronesService(OMSConstants.SERVICE, "updateStatus", gameofthrones);
		Processor processor = new Processor(OMSConstants.getOMSHeader(session.get("csrfToken")), new String[]{orderId, "arrived", "" + Instant.now().getEpochSecond()},
				null, OMSConstants.getOMSCookies(session.get("csrfToken")), service);
		return validateResponseStatus(processor);
	}

	/**
	 * Change Order Status to Picked Up
	 *
	 * @param orderId
	 * @return Processor
	 */
	public boolean changeOrderStatusToPickedUp(String orderId) {
		HashMap<String, String> session = getSession(OMSConstants.USERNAME, OMSConstants.PASSSWORD);
		GameOfThronesService service = new GameOfThronesService(OMSConstants.SERVICE, "updateStatus", gameofthrones);
		Processor processor = new Processor(OMSConstants.getOMSHeader(session.get("csrfToken")), new String[]{orderId, "pickedup", "" + Instant.now().getEpochSecond()},
				null, OMSConstants.getOMSCookies(session.get("csrfToken")), service);
		return validateResponseStatus(processor);
	}


	/**
	 * Change Order Status to Reached
	 *
	 * @param orderId
	 * @return Processor
	 */
	public boolean changeOrderStatusToReached(String orderId) {
		HashMap<String, String> session = getSession(OMSConstants.USERNAME, OMSConstants.PASSSWORD);
		GameOfThronesService service = new GameOfThronesService(OMSConstants.SERVICE, "updateStatus", gameofthrones);
		Processor processor = new Processor(OMSConstants.getOMSHeader(session.get("csrfToken")), new String[]{orderId, "reached", "" + Instant.now().getEpochSecond()},
				null, OMSConstants.getOMSCookies(session.get("csrfToken")), service);
		return validateResponseStatus(processor);
	}

	/**
	 * Change Order Status to Delivered
	 *
	 * @param orderId
	 * @return Processor
	 */
	public boolean changeOrderStatusToDelivered(String orderId) {
		HashMap<String, String> session = getSession(OMSConstants.USERNAME, OMSConstants.PASSSWORD);
		GameOfThronesService service = new GameOfThronesService(OMSConstants.SERVICE, "updateStatus", gameofthrones);
		Processor processor = new Processor(OMSConstants.getOMSHeader(session.get("csrfToken")), new String[]{orderId, "delivered", "" + Instant.now().getEpochSecond()},
				null, OMSConstants.getOMSCookies(session.get("csrfToken")), service);
		return validateResponseStatus(processor);
	}

	/**
	 * Get Order Information in OMS
	 *
	 * @param orderId
	 * @return String
	 */
	public String getOrderInfoFromOMS(String orderId) {
		HashMap<String, String> session = getSession(OMSConstants.USERNAME, OMSConstants.PASSSWORD);
		GameOfThronesService service = new GameOfThronesService(OMSConstants.SERVICE, "getOrder", gameofthrones);
		Processor processor = new Processor(OMSConstants.getOMSHeader(session.get("csrfToken")), null,
				new String[]{orderId}, OMSConstants.getOMSCookies(session.get("csrfToken")), service);
		if (processor.ResponseValidator.GetResponseCode() == 200) {
			return processor.ResponseValidator.GetBodyAsText();
		} else {
			return null;
		}
	}

	public String getOrderInfoFromOMS(String orderId, String user, String password) {
		HashMap<String, String> session = getSession(user, password);
		GameOfThronesService service = new GameOfThronesService(OMSConstants.SERVICE, "getOrder", gameofthrones);
		Processor processor = new Processor(OMSConstants.getOMSHeader(session.get("csrfToken")), null,
				new String[]{orderId}, OMSConstants.getOMSCookies(session.get("csrfToken")), service);
		if (processor.ResponseValidator.GetResponseCode() == 200) {
			return processor.ResponseValidator.GetBodyAsText();
		} else {
			return null;
		}
	}

	//value of waiting for must be, L1Placer, L2Placer, Verifier, CallDE_OE or all
	public void clearOrdersInQueue(String waitingFor) {
		if (!waitingFor.equals("L1Placer") || !waitingFor.equals("L2Placer") || !waitingFor.equals("Verifier") || !waitingFor.equals("CallDE_OE") || !waitingFor.equalsIgnoreCase("all")) {
			SystemConfigProvider.getTemplate(LosConstants.hostName).execute(queryToClearOrdersInQueue(waitingFor));
			SystemConfigProvider.getTemplate(LosConstants.hostName).execute(queryToclearoms_orderexecutivemapping_orders());
			SystemConfigProvider.getTemplate(LosConstants.hostName).execute(queryToClearoms_orderexecutivemapping());
		} else {
			System.out.println("########## value of waiting for must be, L1Placer, L2Placer, Verifier, CallDE_OE or all ##########");
		}
	}

	public void clearOrdersInQueueExceptOrder(String waitingFor, String orderId) {
		if (!waitingFor.equals("L1Placer") || !waitingFor.equals("L2Placer") || !waitingFor.equals("Verifier") || !waitingFor.equals("CallDE_OE") || !waitingFor.equalsIgnoreCase("all")) {
			SystemConfigProvider.getTemplate(LosConstants.hostName).execute(queryToClearOrdersInQueue(waitingFor, orderId));
			SystemConfigProvider.getTemplate(LosConstants.hostName).execute(queryToclearoms_orderexecutivemapping_orders());
			SystemConfigProvider.getTemplate(LosConstants.hostName).execute(queryToClearoms_orderexecutivemapping());
		} else {
			System.out.println("########## value of waiting for must be, L1Placer, L2Placer, Verifier, CallDE_OE or all ##########");
		}
    }
    
    public void forceLogout(String userName){
    	SystemConfigProvider.getTemplate(LosConstants.hostName).execute(forceLogoutQuery(userName));
    }
    
    public void getrestaurantItems(){
    	
    }
    
    public void recheckBill(){
    	
    }
    
    public void confirmEdit(){
    	
    }

	/**
	 * Verify Order in OMS
	 *
	 * @param orderId
	 * @param action
	 * @return Processor
	 */
	public boolean verifyOrderPROD(String orderId, String action) {
		HashMap<String, String> session = getSessionProd(OMSConstants.E2EUSERPROD, OMSConstants.E2EPASSWORDPROD);

		GameOfThronesService service = new GameOfThronesService(OMSConstants.SERVICE, "verifyorder", gameofthrones);
		Processor processor = new Processor(OMSConstants.getOMSHeader(session.get("csrfToken")), new String[]{orderId, action},
				null, OMSConstants.getOMSCookies(session.get("csrfToken")), service);
		return validateResponseStatus(processor);
	}

	public String getOrderInfoFromOMSProd(String orderId) {
		HashMap<String, String> session = getSessionProd(OMSConstants.E2EUSERPROD, OMSConstants.E2EPASSWORDPROD);
		GameOfThronesService service = new GameOfThronesService(OMSConstants.SERVICE, "getOrder", gameofthrones);
		Processor processor = new Processor(OMSConstants.getOMSHeader(session.get("csrfToken")), null,
				new String[]{orderId}, OMSConstants.getOMSCookiesProd(session.get("csrfToken"), session.get("sessionId")), service);
		if (processor.ResponseValidator.GetResponseCode() == 200) {
			return processor.ResponseValidator.GetBodyAsText();
		} else {
			return null;
		}
	}

	public HashMap<String, String> getSessionProd(String name, String password) {
		String sessionId = "036cji9thl6kvmobgjhgcfnsmej0z6ut";
		HashMap<String, String> token = new HashMap<>();
		GameOfThronesService service = new GameOfThronesService(OMSConstants.SERVICE, "getsession", gameofthrones);
		HashMap<String, String> requestheaders = new HashMap<>();
		requestheaders.put("content-type", "application/json");
		List<Cookie> cookies = new ArrayList<>();
		cookies.add(new Cookie("sessionid", sessionId));
		Processor processor = new Processor(requestheaders, null, new String[]{name, password}, cookies, service);
		String response = processor.ResponseValidator.GetBodyAsText();
		String csrf_token = JsonPath.read(response, "$.csrf_token");
		token.put("csrfToken", csrf_token);
		token.put("sessionId", sessionId);
		return token;
	}

	public Processor OrderCancellationInProd(String dispositionId, String subDispositionId, String cancellationAmount, String cancellationFeeApplicability, String OrderId){
		HashMap<String, String> requestheaders= new HashMap<>();
		GameOfThronesService service= new GameOfThronesService("omss", "ffcancellationapi", gameofthrones);
		String[] urlparams= {OrderId};
		String[] payloadparams= {dispositionId,subDispositionId,cancellationAmount,cancellationFeeApplicability};
		Processor processor = new Processor(service, requestheaders, payloadparams, urlparams);
		return processor;
	}

	public Processor getOrderInfoFromOMSReturn(String orderId, String user, String password) {
		HashMap<String, String> session = getSession(user, password);
		GameOfThronesService service = new GameOfThronesService(OMSConstants.SERVICE, "getOrder", gameofthrones);
		Processor processor = new Processor(OMSConstants.getOMSHeader(session.get("csrfToken")), null,
				new String[]{orderId}, OMSConstants.getOMSCookies(session.get("csrfToken")), service);
		return processor;
	}



}
