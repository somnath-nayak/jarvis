package com.swiggy.api.erp.cms.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.dp.slotsDP;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.Assert;
import org.testng.annotations.Test;

public class GetSlots extends slotsDP {
	Initialize gameofthrones = new Initialize();
	CMSHelper cmshelper= new CMSHelper();

	@Test(dataProvider="slotCreation")
	//public void createTag_method()
	public void GetRestaurantSlots(String restaurant_id, String open_time,String close_time,String day)
	{

		System.out.println("***************************************** Get Restaurant slot started *****************************************");

		Processor p =cmshelper.getRestaurantTimeSlot(restaurant_id);

		Assert.assertEquals(1, p.ResponseValidator.GetNodeValueAsInt("status"));

		String resp=p.ResponseValidator.GetBodyAsText();
		int message=JsonPath.read(resp,"$.status");
		System.out.println(">>>>>>>>"+message);
		System.out.println("######################################### Get Restaurant slot compleated #########################################");
	}

}
