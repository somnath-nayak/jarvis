package com.swiggy.api.erp.finance.tests;


import com.swiggy.api.erp.finance.constants.CashMgmtConstant;
import com.swiggy.api.erp.finance.dp.CashDataProvider;
import com.swiggy.api.erp.finance.helper.CashMgmtDBhelper;
import com.swiggy.api.erp.finance.helper.CashMgmtHelper;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class CashMgmtBulkTxnTest extends CashDataProvider {
    CashMgmtHelper cmh=new CashMgmtHelper();
    CashMgmtDBhelper cmdh=new CashMgmtDBhelper();
    ArrayList<String> deList=new ArrayList<>();


    //Getting DE List from DB having employment status=1
    @Test
    public List<String> getDE(){
        List<Map<String, Object>> lst=(List<Map<String, Object>>)cmdh.getActiveDEFromDB("1");
        Iterator<Map<String, Object>> iterator = lst.iterator();

        if(deList.isEmpty()) {
            while (iterator.hasNext()) {
                deList.add(iterator.next().get("id").toString());
            }
        }
        return deList;

    }
//Printing DE list
    @Test(priority = 1)
    public void getDEList(){
        List<String> dlist=getDE();
        System.out.println("de list is ="+dlist);
        System.out.println("Size of  dlist is ="+dlist.size());


    }
// Adding multiple type of transaction for all DE's received from DB
    @Test(priority = 2,dataProvider = "addBulkTxnDP")
    public void addMultipleTxnForTestData(String description, String incoming, String spending, String txn_type){
        List<String> dlist=getDE();
        for(int i=0;i<dlist.size();i++) {

            System.out.println("Adding the txn for DE =>"+dlist.get(i));
            String de_id=dlist.get(i);
            if(txn_type==CashMgmtConstant.CASH_INCOMING || txn_type==CashMgmtConstant.CASH_SPENDING || txn_type==CashMgmtConstant.POCKETING_TRANSACTION){
                String order_id=cmh.getOrderId();
                Processor processor=cmh.addTransactionHelper(de_id,description,order_id,incoming,spending,txn_type);
                int response_code=processor.ResponseValidator.GetResponseCode();

                //validating the  response code of add txn
                Assert.assertEquals(200,response_code);
            }
            else if(txn_type==CashMgmtConstant.MANUAL_TRANSACTION) {

                //getting order id to create transaction
                String order_id=cmh.getOrderId();
                Processor processor = cmh.addTransactionHelper(de_id, description, order_id, incoming, spending, txn_type);

                //Validating the response code
                int response_code = processor.ResponseValidator.GetResponseCode();

                Assert.assertEquals(200, response_code);
            }
            else
            {
                Processor processor = cmh.addTransactionHelper(de_id, description,"", incoming, spending, txn_type);
                int response_code = processor.ResponseValidator.GetResponseCode();
                Assert.assertEquals(200, response_code);
            }


        }


    }
// Making Floating cash Zero for all DE's
    @Test(priority =3)
    public void makeFCZero() throws InterruptedException {
        List<String> dlist=getDE();
        for(int i=0;i<dlist.size();i++) {
            String de_id=dlist.get(i);

            String fCash=cmdh.getFloatingCashFromDB(de_id).toString();
            int fCash2=Integer.parseInt(fCash);
            System.out.println("Floating cash of DE ="+de_id+" is "+fCash2);
            if(fCash2<0) {

                Processor process = cmh.addTransactionHelper(de_id, "Make FC Zero", "", "0", fCash, CashMgmtConstant.CASH_SESSION_DISBURSE);
                int resp = process.ResponseValidator.GetResponseCode();
                Assert.assertEquals(200, resp);
                Thread.sleep(2000);
                String fCash3 = cmdh.getFloatingCashFromDB(de_id).toString();

                Assert.assertEquals("0", fCash3);
            }
            else if(fCash2>0)
            {
                Processor process = cmh.addTransactionHelper(de_id, "Make FC Zero", "", "0", fCash, CashMgmtConstant.CASH_SESSION_COLLECT);
                int resp = process.ResponseValidator.GetResponseCode();
                Assert.assertEquals(200, resp);
                Thread.sleep(2000);
                String fCash3 = cmdh.getFloatingCashFromDB(de_id).toString();
                Assert.assertEquals("0", fCash3);

            }
        }



    }

    // Closing the session for all DE's
    @Test(priority = 4)
    public void closeSessionInBulk(){
        List<String> dlist=getDE();
        for(int i=0;i<dlist.size();i++) {
            String de_id = dlist.get(i);
            System.out.println("Closing the session of DE "+de_id);
            Processor prc=cmh.closeSessionHelper(de_id);
            int  rsp= prc.ResponseValidator.GetResponseCode();
            Assert.assertEquals(200,rsp);

        }
    }


}
