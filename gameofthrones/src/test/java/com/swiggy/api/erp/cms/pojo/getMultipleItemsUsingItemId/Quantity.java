package com.swiggy.api.erp.cms.pojo.getMultipleItemsUsingItemId;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "value",
        "unit"
})
public class Quantity {

    @JsonProperty("value")
    private Integer value;
    @JsonProperty("unit")
    private String unit;

    /**
     * No args constructor for use in serialization
     *
     */
    public Quantity() {
    }

    /**
     *
     * @param unit
     * @param value
     */
    public Quantity(Integer value, String unit) {
        super();
        this.value = value;
        this.unit = unit;
    }

    @JsonProperty("value")
    public Integer getValue() {
        return value;
    }

    @JsonProperty("value")
    public void setValue(Integer value) {
        this.value = value;
    }

    @JsonProperty("unit")
    public String getUnit() {
        return unit;
    }

    @JsonProperty("unit")
    public void setUnit(String unit) {
        this.unit = unit;
    }

}