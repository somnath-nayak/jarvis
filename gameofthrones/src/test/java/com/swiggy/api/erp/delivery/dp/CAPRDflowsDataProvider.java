package com.swiggy.api.erp.delivery.dp;

import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.erp.delivery.helper.*;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import framework.gameofthrones.JonSnow.Processor;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.annotations.DataProvider;

public class CAPRDflowsDataProvider {
	//static String order_id;
	DeliveryServiceHelper helpdel = new DeliveryServiceHelper();
	AutoassignHelper hepauto=new AutoassignHelper();
	DeliveryHelperMethods delmeth=new DeliveryHelperMethods();
	POPHelper popHelper= new POPHelper();
	DeliveryDataHelper deliveryDataHelper= new DeliveryDataHelper();
	String restaurantId;
	String restaurantLat;
	String restaurantLon;
	Integer  areaid;
	String de_id=null;
	Integer zoneid=34;
	String appversion;

	public CAPRDflowsDataProvider(String restaurantId,String restaurantLat, String restaurantLon)
	{
		this.restaurantId=restaurantId;
		this.restaurantLat=restaurantLat;
		this.restaurantLon=restaurantLon;
		areaid= Integer.parseInt(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).
						queryForMap("select area_code from retaurant where id="+restaurantId).get("area_code").toString());
		zoneid=Integer.parseInt(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).
				queryForMap("select zone_id from area where id ="+areaid).get("zone_id").toString());
	de_id=deliveryDataHelper.CreateDE(zoneid);
		appversion="1.9";
}

	public CAPRDflowsDataProvider()
	{

	}

	@DataProvider(name = "QE-332")
	public Object[][] CAPRDDP332() throws Exception {
		return new Object[][] {
		{DeliveryConstant.MayurDE,"1.8.2.1",de_id,appversion}

		};
	}
	@DataProvider(name = "QE-333")
	public Object[][] CAPRDDP333() throws Exception {
		return new Object[][] {
		{DeliveryConstant.MayurDE,"1.8.2.1",de_id,appversion}

		};
	}

	@DataProvider(name = "QE-299")
	public Object[][] CAPRDDP299()
	{
		String generatedOrder=null;
		String order_id=deliveryDataHelper.getOrderId(restaurantId,restaurantLat,restaurantLon);
/*		try {
			order_id = createOrder();
			System.out.println("Came in Order");
		}
		catch(Exception e)
		{
			order_id=null;
		}
		if(order_id==null)
		{
			String orderJson=deliveryDataHelper.returnOrderJson("10820","12.927396","77.597894");
			System.out.println("REACHED BELOW JSON");
			try {
				JSONObject order = new JSONObject(orderJson);
				generatedOrder =order.get("order_id").toString();
			} catch (JSONException je) {
				je.printStackTrace();
			}
			if(deliveryDataHelper.pushOrderJsonToRMQAndValidate(orderJson))
			{
				order_id=generatedOrder;
				System.out.println("PRINTING ORDER ID final "+order_id);
			}
		}*/
		return new Object[][] {
		{order_id,de_id,appversion}
		};


	}
	@DataProvider(name = "QE-253")
	public Object[][] CAPRDDP253() throws Exception {
		String de_id =deliveryDataHelper.CreateDE(1);
//String de_id =deliveryDataHelper.CreateDE(1);
		delmeth.dbhelperupdate("update delivery_boys set employment_status=2 where id="+de_id);
		return new Object[][] {
		{de_id,appversion}

		};
	}
	@DataProvider(name = "QE-300")
	public Object[][] CAPRDDP300() throws Exception {
		//order_id = createOrder();
		String order_id=deliveryDataHelper.getOrderId(restaurantId,restaurantLat,restaurantLon);
		return new Object[][] {
		{order_id,de_id,appversion}

		};
	}
	@DataProvider(name = "QE-301")
	public Object[][] CAPRDDP301() throws Exception {
		//order_id = createOrder();
		String order_id=deliveryDataHelper.getOrderId(restaurantId,restaurantLat,restaurantLon);
		return new Object[][] {
		{order_id,de_id,appversion}

		};
	}

	@DataProvider(name = "QE-302")
	public Object[][] CAPRDDP302() throws Exception {
		String order_id=deliveryDataHelper.getOrderId(restaurantId,restaurantLat,restaurantLon);
		hepauto.delogin(de_id, appversion);
       // String deotp = delmeth.redisconnectget(DeliveryConstant.de_otp, de_id,DeliveryConstant.redisdb);
		String deotp= null;
		try {
			deotp = (new JSONObject(helpdel.getOtp(de_id.toString()).ResponseValidator.GetBodyAsText())).get("data").toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		hepauto.deotp(de_id, deotp);
		helpdel.makeDEFree(de_id);
		Thread.sleep(10000);
		helpdel.assignOrder(order_id,de_id);
		Thread.sleep(5000);

		return new Object[][] {
				{order_id,"arrived"},
				{order_id,"pickedup"},
				{order_id,"reached"},
				{order_id,"delivered"}
		};
	}
	@DataProvider(name = "QE-303")
	public Object[][] CAPRDDP303() throws Exception {
		//order_id = createOrder();
		String order_id=deliveryDataHelper.getOrderId(restaurantId,restaurantLat,restaurantLon);
		return new Object[][] {
		{order_id,de_id,appversion}

		};
	}

	@DataProvider(name = "QE-304")
	public Object[][] CAPRDDP304() throws Exception {
		//order_id = createOrder();
		String order_id=deliveryDataHelper.getOrderId(restaurantId,restaurantLat,restaurantLon);
		hepauto.delogin(de_id,appversion);
		String deotp= null;
		try {
			deotp = (new JSONObject(helpdel.getOtp(de_id).ResponseValidator.GetBodyAsText())).get("data").toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
       // String deotp = delmeth.redisconnectget(DeliveryConstant.de_otp, de_id, DeliveryConstant.redisdb);
		hepauto.deotp(de_id, deotp);
		helpdel.makeDEFree(de_id);
		Thread.sleep(10000);
		helpdel.assignOrder(order_id,de_id);
		Thread.sleep(5000);
		helpdel.zipDialConfirmDE(order_id);

		return new Object[][] {
				{order_id,"pickedup"},
				{order_id,"reached"},
				{order_id,"delivered"}
		};
	}
	@DataProvider(name = "QE-305")
	public Object[][] CAPRDDP305() throws Exception {
		//order_id = createOrder();
		String order_id=deliveryDataHelper.getOrderId(restaurantId,restaurantLat,restaurantLon);
		return new Object[][] {
		{order_id,de_id,appversion}

		};
	}
	@DataProvider(name = "QE-306")
	public Object[][] CAPRDDP306() throws Exception {
		//order_id = createOrder();
		String order_id=deliveryDataHelper.getOrderId(restaurantId,restaurantLat,restaurantLon);
		hepauto.delogin(de_id, appversion);
		String deotp= null;
		try {
			deotp = (new JSONObject(helpdel.getOtp(de_id.toString()).ResponseValidator.GetBodyAsText())).get("data").toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
       // String deotp = delmeth.redisconnectget(DeliveryConstant.de_otp, de_id, DeliveryConstant.redisdb);
		hepauto.deotp(de_id, deotp);
		helpdel.makeDEFree(de_id);
		Thread.sleep(10000);
		helpdel.assignOrder(order_id,de_id);
		Thread.sleep(10000);
		helpdel.zipDialConfirmDE(order_id);
		Thread.sleep(10000);

		return new Object[][] {
				{order_id,"reached"},
				{order_id,"delivered"}
		};
	}

	
	//tillhere
	@DataProvider(name = "QE-307")
	public Object[][] CAPRDDP307() throws Exception {
		//order_id = createOrder();
		String order_id=deliveryDataHelper.getOrderId(restaurantId,restaurantLat,restaurantLon);
		return new Object[][] {
		{order_id,de_id,appversion}

		};
	}
	@DataProvider(name = "QE-308")
	public Object[][] CAPRDDP308() throws Exception {
		//order_id = createOrder();
		String order_id=deliveryDataHelper.getOrderId(restaurantId,restaurantLat,restaurantLon);
		hepauto.delogin(de_id, appversion);
		String deotp= null;
		try {
			deotp = (new JSONObject(helpdel.getOtp(de_id.toString()).ResponseValidator.GetBodyAsText())).get("data").toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		//String deotp = delmeth.redisconnectget(DeliveryConstant.de_otp,de_id, DeliveryConstant.redisdb);
		hepauto.deotp(de_id, deotp);
		helpdel.makeDEFree(de_id);
		Thread.sleep(10000);
		helpdel.assignOrder(order_id,de_id);
		Thread.sleep(10000);
		helpdel.zipDialConfirmDE(order_id);
		Thread.sleep(10000);
		helpdel.zipDialArrivedDE(order_id);
		Thread.sleep(10000);
		helpdel.zipDialPickedUpDE(order_id);
		Thread.sleep(10000);
		return new Object[][] {
				{order_id,"delivered"}
		};
	}
	@DataProvider(name = "QE-310")
	public Object[][] CAPRDDP310() throws Exception {
		//order_id = createOrder();
		String order_id=deliveryDataHelper.getOrderId(restaurantId,restaurantLat,restaurantLon);
		return new Object[][] {
		{order_id,de_id,appversion}

		};
	}
	@DataProvider(name = "QE-311")
	public Object[][] CAPRDDP311() throws Exception {
		String order_id=deliveryDataHelper.getOrderId(restaurantId,restaurantLat,restaurantLon);
		return new Object[][] {
		{order_id,"2418",appversion}

		};
	}
	@DataProvider(name = "QE-312")
	public Object[][] CAPRDDP312() throws Exception {
		//order_id = createOrder();
		String order_id=deliveryDataHelper.getOrderId(restaurantId,restaurantLat,restaurantLon);
		return new Object[][] {
		{order_id,de_id,appversion}

		};
	}
	public static String createOrder()  {
		CheckoutHelper helperc = new CheckoutHelper();
		Object[][] data = cartItems();
		String order_id=null;
		Processor p=null;
		try {
			p = helperc.placeOrder(DeliveryConstant.mobile,
					DeliveryConstant.password, data[0][0].toString(),
					data[0][1].toString(), data[0][2].toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		try
		{
			order_id = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.order_id");
		}
		catch (NullPointerException ne)
		{
			order_id=null;
		}
		return order_id;
	}



	public static Object[][] cartItems() {

		return new Object[][] { { "8805487", "2", "4977"} };

	}



}