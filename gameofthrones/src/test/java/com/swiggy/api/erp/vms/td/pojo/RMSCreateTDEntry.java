package com.swiggy.api.erp.vms.td.pojo;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
"enabled",
"restaurantIds",
"valid_from",
"valid_till",
"campaign_type",
"ruleDiscountType",
"createdBy",
"discountLevel",
"restaurantList",
"slots",
"firstOrderRestriction",
"userRestriction",
"restaurantFirstOrder",
"ruleDiscount",
"restaurant_hit",
"swiggy_hit",
"taxesOnDiscountedBill",
"commissionOnFullBill",
"timeSlotRestriction",
"itemName"
})

/***
 * 
 * @author ramzi
 *
 */
public class RMSCreateTDEntry {

@JsonProperty("enabled")
private String enabled;
@JsonProperty("restaurantIds")
private List<Integer> restaurantIds = null;
//@JsonProperty("id")
//private int id;
@JsonProperty("valid_from")
private Long validFrom;
@JsonProperty("valid_till")
private Long validTill;
@JsonProperty("campaign_type")
private String campaignType;
@JsonProperty("ruleDiscountType")
private String ruleDiscountType;
@JsonProperty("createdBy")
private Long createdBy;
@JsonProperty("updatedBy")
private Long updatedBy;
@JsonProperty("discountLevel")
private String discountLevel;
@JsonProperty("restaurantList")
private List<Restaurant> restaurantList = null;
@JsonProperty("slots")
private List<Slots> slots = null;
@JsonProperty("firstOrderRestriction")
private String firstOrderRestriction;
@JsonProperty("userRestriction")
private String userRestriction;
@JsonProperty("restaurantFirstOrder")
private String restaurantFirstOrder;
@JsonProperty("ruleDiscount")
private RuleDiscount ruleDiscount;
@JsonProperty("restaurant_hit")
private Integer restaurantHit;
@JsonProperty("swiggy_hit")
private Integer swiggyHit;
@JsonProperty("taxesOnDiscountedBill")
private String taxesOnDiscountedBill;
@JsonProperty("commissionOnFullBill")
private String commissionOnFullBill;
@JsonProperty("timeSlotRestriction")
private String timeSlotRestriction;
@JsonProperty("itemName")
private String itemName;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

/**
* No args constructor for use in serialization
*
*/
public RMSCreateTDEntry() {
}

/**
*
* @param ruleDiscount
* @param timeSlotRestriction
* @param enabled
* @param restaurantFirstOrder
* @param campaignType
* @param restaurantList
* @param commissionOnFullBill
* @param userRestriction
* @param swiggyHit
* @param restaurantHit
* @param validTill
* @param firstOrderRestriction
* @param createdBy
* @param slots
* @param validFrom
* @param discountLevel
* @param restaurantIds
* @param taxesOnDiscountedBill
* @param ruleDiscountType
* @param itemName
*/
public RMSCreateTDEntry(String enabled, List<Integer> restaurantIds, Long validFrom, Long validTill, String campaignType, String ruleDiscountType, Long createdBy, Long updatedBy, String discountLevel, List<Restaurant> restaurantList, List<Slots> slots, String firstOrderRestriction, String userRestriction, String restaurantFirstOrder, RuleDiscount ruleDiscount, Integer restaurantHit, Integer swiggyHit, String taxesOnDiscountedBill, String commissionOnFullBill, String timeSlotRestriction) {
super();
this.enabled = enabled;
this.restaurantIds = restaurantIds;
//this.id = id;
this.validFrom = validFrom;
this.validTill = validTill;
this.campaignType = campaignType;
this.ruleDiscountType = ruleDiscountType;
this.createdBy = createdBy;
this.updatedBy = updatedBy;
this.discountLevel = discountLevel;
this.restaurantList = restaurantList;
this.slots = slots;
this.firstOrderRestriction = firstOrderRestriction;
this.userRestriction = userRestriction;
this.restaurantFirstOrder = restaurantFirstOrder;
this.ruleDiscount = ruleDiscount;
this.restaurantHit = restaurantHit;
this.swiggyHit = swiggyHit;
this.taxesOnDiscountedBill = taxesOnDiscountedBill;
this.commissionOnFullBill = commissionOnFullBill;
this.timeSlotRestriction = timeSlotRestriction;
this.itemName = itemName;
}

@JsonProperty("enabled")
public String getEnabled() {
return enabled;
}

@JsonProperty("enabled")
public void setEnabled(String enabled) {
this.enabled = enabled;
}

public RMSCreateTDEntry withEnabled(String enabled) {
this.enabled = enabled;
return this;
}

@JsonProperty("restaurantIds")
public List<Integer> getRestaurantIds() {
return restaurantIds;
}

@JsonProperty("restaurantIds")
public void setRestaurantIds(List<Integer> restaurantIds) {
this.restaurantIds = restaurantIds;
}

public RMSCreateTDEntry withRestaurantIds(List<Integer> restaurantIds) {
this.restaurantIds = restaurantIds;
return this;
}

//@JsonProperty("id")
//public int getId() {
//return id;
//}
//
//@JsonProperty("id")
//public void setId(int id) {
//this.id = id;
//}

//public RMSCreateTDEntry withId(int id) {
//this.id = id;
//return this;
//}


@JsonProperty("valid_from")
public Long getValidFrom() {
return validFrom;
}

@JsonProperty("valid_from")
public void setValidFrom(Long validFrom) {
this.validFrom = validFrom;
}

public RMSCreateTDEntry withValidFrom(Long validFrom) {
this.validFrom = validFrom;
return this;
}

@JsonProperty("valid_till")
public Long getValidTill() {
return validTill;
}

@JsonProperty("valid_till")
public void setValidTill(Long validTill) {
this.validTill = validTill;
}

public RMSCreateTDEntry withValidTill(Long validTill) {
this.validTill = validTill;
return this;
}

@JsonProperty("campaign_type")
public String getCampaignType() {
return campaignType;
}

@JsonProperty("campaign_type")
public void setCampaignType(String campaignType) {
this.campaignType = campaignType;
}

public RMSCreateTDEntry withCampaignType(String campaignType) {
this.campaignType = campaignType;
return this;
}

@JsonProperty("ruleDiscountType")
public String getRuleDiscountType() {
return ruleDiscountType;
}

@JsonProperty("ruleDiscountType")
public void setRuleDiscountType(String ruleDiscountType) {
this.ruleDiscountType = ruleDiscountType;
}

public RMSCreateTDEntry withRuleDiscountType(String ruleDiscountType) {
this.ruleDiscountType = ruleDiscountType;
return this;
}

@JsonProperty("createdBy")
public Long getCreatedBy() {
return createdBy;
}

@JsonProperty("createdBy")
public void setCreatedBy(Long createdBy) {
this.createdBy = createdBy;
}

public RMSCreateTDEntry withCreatedBy(long createdBy) {
this.createdBy = createdBy;
return this;
}

@JsonProperty("updatedBy")
public Long getUpdatedBy() {
return updatedBy;
}

@JsonProperty("updatedBy")
public void setUpdatedBy(Long updatedBy) {
this.updatedBy = updatedBy;
}

public RMSCreateTDEntry withUpdatedBy(long updatedBy) {
this.updatedBy = updatedBy;
return this;
}


@JsonProperty("discountLevel")
public String getDiscountLevel() {
return discountLevel;
}

@JsonProperty("discountLevel")
public void setDiscountLevel(String discountLevel) {
this.discountLevel = discountLevel;
}

public RMSCreateTDEntry withDiscountLevel(String discountLevel) {
this.discountLevel = discountLevel;
return this;
}

@JsonProperty("restaurantList")
public List<Restaurant> getRestaurantList() {
return restaurantList;
}

@JsonProperty("restaurantList")
public void setRestaurantList(List<Restaurant> restaurantList) {
this.restaurantList = restaurantList;
}

public RMSCreateTDEntry withRestaurantList(List<Restaurant> restaurantList) {
this.restaurantList = restaurantList;
return this;
}

@JsonProperty("slots")
public List<Slots> getSlots() {
return slots;
}

@JsonProperty("slots")
public void setSlots(List<Slots> slots) {
this.slots = slots;
}

public RMSCreateTDEntry withSlots(List<Slots> slots) {
this.slots = slots;
return this;
}

@JsonProperty("firstOrderRestriction")
public String getFirstOrderRestriction() {
return firstOrderRestriction;
}

@JsonProperty("firstOrderRestriction")
public void setFirstOrderRestriction(String firstOrderRestriction) {
this.firstOrderRestriction = firstOrderRestriction;
}

public RMSCreateTDEntry withFirstOrderRestriction(String firstOrderRestriction) {
this.firstOrderRestriction = firstOrderRestriction;
return this;
}

@JsonProperty("userRestriction")
public String getUserRestriction() {
return userRestriction;
}

@JsonProperty("userRestriction")
public void setUserRestriction(String userRestriction) {
this.userRestriction = userRestriction;
}

public RMSCreateTDEntry withUserRestriction(String userRestriction) {
this.userRestriction = userRestriction;
return this;
}

@JsonProperty("restaurantFirstOrder")
public String getRestaurantFirstOrder() {
return restaurantFirstOrder;
}

@JsonProperty("restaurantFirstOrder")
public void setRestaurantFirstOrder(String restaurantFirstOrder) {
this.restaurantFirstOrder = restaurantFirstOrder;
}

public RMSCreateTDEntry withRestaurantFirstOrder(String restaurantFirstOrder) {
this.restaurantFirstOrder = restaurantFirstOrder;
return this;
}

@JsonProperty("ruleDiscount")
public RuleDiscount getRuleDiscount() {
return ruleDiscount;
}

@JsonProperty("ruleDiscount")
public void setRuleDiscount(RuleDiscount ruleDiscount) {
this.ruleDiscount = ruleDiscount;
}

public RMSCreateTDEntry withRuleDiscount(RuleDiscount ruleDiscount) {
this.ruleDiscount = ruleDiscount;
return this;
}

@JsonProperty("restaurant_hit")
public Integer getRestaurantHit() {
return restaurantHit;
}

@JsonProperty("restaurant_hit")
public void setRestaurantHit(Integer restaurantHit) {
this.restaurantHit = restaurantHit;
}

public RMSCreateTDEntry withRestaurantHit(Integer restaurantHit) {
this.restaurantHit = restaurantHit;
return this;
}

@JsonProperty("swiggy_hit")
public Integer getSwiggyHit() {
return swiggyHit;
}

@JsonProperty("swiggy_hit")
public void setSwiggyHit(Integer swiggyHit) {
this.swiggyHit = swiggyHit;
}

public RMSCreateTDEntry withSwiggyHit(Integer swiggyHit) {
this.swiggyHit = swiggyHit;
return this;
}

@JsonProperty("taxesOnDiscountedBill")
public String getTaxesOnDiscountedBill() {
return taxesOnDiscountedBill;
}

@JsonProperty("taxesOnDiscountedBill")
public void setTaxesOnDiscountedBill(String taxesOnDiscountedBill) {
this.taxesOnDiscountedBill = taxesOnDiscountedBill;
}

public RMSCreateTDEntry withTaxesOnDiscountedBill(String taxesOnDiscountedBill) {
this.taxesOnDiscountedBill = taxesOnDiscountedBill;
return this;
}

@JsonProperty("commissionOnFullBill")
public String getCommissionOnFullBill() {
return commissionOnFullBill;
}

@JsonProperty("commissionOnFullBill")
public void setCommissionOnFullBill(String commissionOnFullBill) {
this.commissionOnFullBill = commissionOnFullBill;
}

public RMSCreateTDEntry withCommissionOnFullBill(String commissionOnFullBill) {
this.commissionOnFullBill = commissionOnFullBill;
return this;
}

@JsonProperty("timeSlotRestriction")
public String getTimeSlotRestriction() {
return timeSlotRestriction;
}

@JsonProperty("timeSlotRestriction")
public void setTimeSlotRestriction(String timeSlotRestriction) {
this.timeSlotRestriction = timeSlotRestriction;
}

public RMSCreateTDEntry withTimeSlotRestriction(String timeSlotRestriction) {
this.timeSlotRestriction = timeSlotRestriction;
return this;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

public RMSCreateTDEntry withAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
return this;
}

@JsonProperty("itemName")
public String getitemName() {
return itemName;
}

@JsonProperty("itemName")
public void setitemName(String itemName) {
this.itemName = itemName;
}

public RMSCreateTDEntry withitemName(String itemName) {
this.itemName = itemName;
return this;
}



@Override
public String toString() {
return new ToStringBuilder(this).append("enabled", enabled).append("restaurantIds", restaurantIds).append("validFrom", validFrom).append("validTill", validTill).append("campaignType", campaignType).append("ruleDiscountType", ruleDiscountType).append("createdBy", createdBy).append("discountLevel", discountLevel).append("restaurantList", restaurantList).append("slots", slots).append("firstOrderRestriction", firstOrderRestriction).append("userRestriction", userRestriction).append("restaurantFirstOrder", restaurantFirstOrder).append("ruleDiscount", ruleDiscount).append("restaurantHit", restaurantHit).append("swiggyHit", swiggyHit).append("taxesOnDiscountedBill", taxesOnDiscountedBill).append("commissionOnFullBill", commissionOnFullBill).append("timeSlotRestriction", timeSlotRestriction).append("additionalProperties", additionalProperties).append("itemName", itemName).toString();
}

}