package com.swiggy.api.erp.crm.constants;

import java.util.HashMap;

public class NodeIDs {

    public HashMap<String, String> nodeValues()
    {
    	//List<HashMap<String, String> >nodeIds = new ArrayList<HashMap<String,String>>();
    	HashMap<String, String> nodeId= new HashMap<String,String>();
    	
    	nodeId.put("1", "Help");
    	
    	nodeId.put("2", "Order specific issues");
    	
    	nodeId.put("5", "I want to cancel my order");
    	nodeId.put("6", "I Changed my mind-OrderPlacedNotOnTrack");
    	nodeId.put("7", "Yes, Cancel it-OrderPlacedNotOnTrack");
    	nodeId.put("9", "Yes, I need more help-OrderPlacedNotOnTrack");
    	nodeId.put("11", "Refund my money to my card/wallet-Yes, I need more help-OrderPlacedNotOnTrack");
    	nodeId.put("12", "Yes, I need more help-OrderPlacedNotOnTrack");
    	
 // TODO Will update all node and title mapping once get the time   	
    	    	
    	return nodeId;
    	
    }
}

