package com.swiggy.api.erp.crm.tests.ChatMiddleware;

import com.swiggy.api.erp.crm.dp.cancellationflows.CreateData;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.annotations.Test;

import java.util.HashMap;

//import framework.gameofthrones.JonSnow.XMLValidator;


public class CreateConversationCM{
	
	Initialize gameofthrones = new Initialize();
	HashMap<String,String> layerUserIdToken = new HashMap<String,String>();
	HashMap<String,String> layerUserConversation= new HashMap<String,String>();
	
	
	
@Test( dataProviderClass = CreateData.class, dataProvider = "user info",enabled = true,description = "Create the conversation")
public void conversationCreate(HashMap<Integer,String[]> data)
{
	String conversationId,layerUserId;
	String[] userdata =data.get(1); 
	
	HashMap<String, String> requestheaders_conversation = new HashMap<String, String>();
	requestheaders_conversation.put("Content-Type", "application/json");
	requestheaders_conversation.put("userId", userdata[0]);
	requestheaders_conversation.put("username", userdata[1]);
	
	String payloadparam[] = new String[3];
	payloadparam[0]=userdata[2]; 
	payloadparam[1]=userdata[3];
	payloadparam[2]=userdata[4];
	
	
	GameOfThronesService conversation_create = new GameOfThronesService("chatmiddlewear", "conversationCreate", gameofthrones);
    Processor conversation_response = new Processor(conversation_create, requestheaders_conversation, payloadparam);
    
    System.out.println("Response" +conversation_response);
    int http_status= conversation_response.ResponseValidator.GetResponseCode();
     System.out.println("stats code is:"+http_status);
    
    if(http_status==200)
    {
    	conversationId= conversation_response.ResponseValidator.GetNodeValue("data.layerConversationId");
    	layerUserId=conversation_response.ResponseValidator.GetNodeValue("profile.layerUserId");
    	System.out.println("ConversationId : "+conversationId);
    	layerUserConversation.put(layerUserId, conversationId);
       	
    }
		
}


		
}
