package com.swiggy.api.erp.cms.pojo.AddonGroup;

/**
 * Created by kiran.j on 2/21/18.
 */
public class AddonGroup
{
    private Entity entity;

    public Entity getEntity ()
    {
        return entity;
    }

    public void setEntity (Entity entity)
    {
        this.entity = entity;
    }

    public AddonGroup build() {
        Entity entity = new Entity();
        entity.build();
        if(this.getEntity() == null)
            this.setEntity(entity);
        return this;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [entity = "+entity+"]";
    }
}
