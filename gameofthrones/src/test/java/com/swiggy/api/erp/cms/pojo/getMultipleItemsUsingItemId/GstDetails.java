package com.swiggy.api.erp.cms.pojo.getMultipleItemsUsingItemId;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "sgst",
        "cgst",
        "igst",
        "inclusive"
})
public class GstDetails {

    @JsonProperty("sgst")
    private String sgst;
    @JsonProperty("cgst")
    private String cgst;
    @JsonProperty("igst")
    private String igst;
    @JsonProperty("inclusive")
    private Boolean inclusive;

    /**
     * No args constructor for use in serialization
     *
     */
    public GstDetails() {
    }

    /**
     *
     * @param inclusive
     * @param igst
     * @param cgst
     * @param sgst
     */
    public GstDetails(String sgst, String cgst, String igst, Boolean inclusive) {
        super();
        this.sgst = sgst;
        this.cgst = cgst;
        this.igst = igst;
        this.inclusive = inclusive;
    }

    @JsonProperty("sgst")
    public String getSgst() {
        return sgst;
    }

    @JsonProperty("sgst")
    public void setSgst(String sgst) {
        this.sgst = sgst;
    }

    @JsonProperty("cgst")
    public String getCgst() {
        return cgst;
    }

    @JsonProperty("cgst")
    public void setCgst(String cgst) {
        this.cgst = cgst;
    }

    @JsonProperty("igst")
    public String getIgst() {
        return igst;
    }

    @JsonProperty("igst")
    public void setIgst(String igst) {
        this.igst = igst;
    }

    @JsonProperty("inclusive")
    public Boolean getInclusive() {
        return inclusive;
    }

    @JsonProperty("inclusive")
    public void setInclusive(Boolean inclusive) {
        this.inclusive = inclusive;
    }

}