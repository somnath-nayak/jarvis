package com.swiggy.api.erp.cms.tests;

import com.swiggy.api.erp.cms.dp.MainCategoryDp;
import com.swiggy.api.erp.cms.helper.FullMenuHelper;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

/**
 * Created by kiran.j on 2/19/18.
 */
public class MainCategoryTest extends MainCategoryDp{

    @Test(dataProvider = "maincategory", description = "Verifies Different Scenarios for Creating Main Categories")
    public void mainCategoryTests(String rest_id, String json, String[] errors, String token_id) {
        SoftAssert softAssert = new SoftAssert();
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        Processor processor = fullMenuHelper.createMainCategory(rest_id, json, token_id);
        if(errors.length == 0)
            softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive(processor));
        else if(errors.length == 1)
            softAssert.assertTrue(processor.ResponseValidator.GetResponseCode() == Integer.parseInt(errors[0]));
        else
            softAssert.assertTrue(fullMenuHelper.validateErrors(processor, errors));
        softAssert.assertAll();
    }

    @Test(dataProvider = "updatemaincategory", description = "Verifies Different Scenarios for Updating Main Categories")
    public void updateMainCategoryTests(String rest_id, String json, String[] errors, String token_id, String cat_id ) {
        SoftAssert softAssert = new SoftAssert();
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        Processor processor = fullMenuHelper.updateMainCategory(rest_id, json, token_id, cat_id);
        if(errors.length == 0)
            softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive(processor));
        else if(errors.length == 1)
            softAssert.assertTrue(processor.ResponseValidator.GetResponseCode() == Integer.parseInt(errors[0]));
        else
            softAssert.assertTrue(fullMenuHelper.validateErrors(processor, errors));
        softAssert.assertAll();
    }
}
