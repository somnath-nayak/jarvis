package com.swiggy.api.erp.crm.tests.foodissues;

import com.swiggy.api.erp.crm.constants.CRMConstants;
import com.swiggy.api.erp.crm.dp.foodissues.FoodIssuesRules;
import com.swiggy.api.erp.crm.dp.foodissues.RecommendationDP;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.DBHelper;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashMap;


public class Recommendation {

    Initialize gameofthrones = new Initialize();
    
    FoodIssuesRules foodIssuesRules = new FoodIssuesRules();
    RecommendationDP recommendationDP = new RecommendationDP();
    DBHelper dbHelper = new DBHelper();
    public HashMap<String, String> requestheaders = new HashMap<>();
    public HashMap<String, String> headers = new HashMap<>();

    public String userId=null, userSegment=null, fraudSegment=null, orderId=null, restId=null, restName=null, cost=null, quantity=null, is_veg=null,
    		itemId=null, itemName=null, orderType=null, recommendationId=null, slaTime=null;
    public int res;

    public HashMap<String, String> getRequestHeaders()
    {
        requestheaders.put("Content-Type", "application/json");
        requestheaders.put("initiator-id", "212");
        requestheaders.put("initiator-type", "AGENT");
        return requestheaders;
    }

    public HashMap<String, String> getHeaders()
    {
        headers.put("Content-Type", "application/json");
        return headers;
    }

    public void fetchOrderDetails(String orderId) throws Exception
    {
        GameOfThronesService getOrderDetail = new GameOfThronesService("crmff", "orderstatusff", gameofthrones);

        Processor getOrderDetailResponse = new Processor(getOrderDetail, headers, null, new String[]{orderId});

        restId = getOrderDetailResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("objects..restaurant_details.restaurant_id").replace("[","").replace("]","");
        restName = getOrderDetailResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("objects..restaurant_details.name").replace("[","").replace("]","");
        cost = getOrderDetailResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("objects..items..sub_total").replace("[","").replace("]","");
        quantity = getOrderDetailResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("objects..items..quantity").replace("[","").replace("]","");
        is_veg = getOrderDetailResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("objects..items..is_veg").replace("[","").replace("]","").replace("\"","").replace("\"","");
        itemId = getOrderDetailResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("objects..items..item_id").replace("[","").replace("]","").replace("\"","").replace("\"","");
        itemName = getOrderDetailResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("objects..items..name").replace("[","").replace("]","").replace("\"","").replace("\"","");
        orderType=getOrderDetailResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("objects..type").replace("[","").replace("]","").replace("\"","").replace("\"","");
    }

    public void changeAndValidateUserSegments(String userId, String userSegment, String fraudSegment) throws Exception
    {
        // Update segments for the user
        if (fraudSegment.equalsIgnoreCase("Null")) {
            SystemConfigProvider.getTemplate("crmSegmentation").update("update user_segment set value_segment='" + userSegment + "', igcc_segment="+null+" where user_id='" + userId + "' limit 1;");
        }
        else if (fraudSegment.equalsIgnoreCase("Good")) {
            fraudSegment = "NA";
            SystemConfigProvider.getTemplate("crmSegmentation").update("update user_segment set value_segment='" + userSegment + "', igcc_segment='" + fraudSegment + "' where user_id='" + userId + "' limit 1;");
        }
        else {
            SystemConfigProvider.getTemplate("crmSegmentation").update("update user_segment set value_segment='" + userSegment + "', igcc_segment='" + fraudSegment + "' where user_id='" + userId + "' limit 1;");
        }

        GameOfThronesService segment = new GameOfThronesService("segment", "getSegment",gameofthrones);
        Processor segmentResponse = new Processor(segment,headers,null,new String[]{userId});

        //Validate the user id, segment and igcc fraud
        Assert.assertEquals(segmentResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.user_id"),userId);
        Assert.assertEquals(segmentResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.value_segment"),userSegment);
        if (!fraudSegment.equalsIgnoreCase("Null"))
        {
        	Assert.assertEquals(segmentResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.igcc_segment"),fraudSegment);
        } else 
        {
            Assert.assertNull(segmentResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.igcc_segment"));
        }
        }
    
    public Processor cloneCheck(String orderId, String restId, String itemId)
    {
        GameOfThronesService cloneTest = new GameOfThronesService("cconeview", "cloneCheck", gameofthrones);
        String[] payload = new String[]{orderId, restId, itemId};

        Processor cloneTestResponse = new Processor(cloneTest, headers, payload);
        int res = cloneTestResponse.ResponseValidator.GetNodeValueAsInt("statusCode");
        slaTime = cloneTestResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.sla_time");
        return cloneTestResponse;
    }

    @Test (priority = 0, enabled = true, dataProviderClass = RecommendationDP.class, dataProvider = "regularPostPaidOrder", description = "Recommendation tests positive")
    public void recommendationRegularPostPaidOrder(HashMap<String,String> issueDetails) throws Exception
    {
    	String length = issueDetails.get("length");
    	int size = Integer.valueOf(length);

    	//Fetch Order type
        orderType=issueDetails.get("orderType");

        orderId = CRMConstants.regularPostPaidOrder;

        System.out.println("order ID: \t" +orderId);

        userSegment=issueDetails.get("segment");
        fraudSegment=issueDetails.get("fraud");
        userId = issueDetails.get("userId");


        try {
            changeAndValidateUserSegments(userId,userSegment,fraudSegment);
        }
        catch (Exception e) {
            e.printStackTrace();
        }


        System.out.println("fraudSegment:\t" +fraudSegment);
        fetchOrderDetails(orderId);

        requestheaders = getRequestHeaders();

        System.out.println("requestheaders   " +requestheaders);

        String[] urlParam = new String[]{orderId,userId};

        String[] payload = new String[]{orderId,userId,restId,restName.replace("\"","").replace("\"",""),issueDetails.get("issueType"),itemId,itemName,is_veg,quantity,cost,issueDetails.get("fullItem")};
        // ,issueDetails.get("disposition")

        GameOfThronesService recommendation = new GameOfThronesService("cconeview", "recommendations", gameofthrones);

        System.out.println("\t userId:" +userId+ "\t userSegment:" +userSegment+ "\t fraudSegment:" +fraudSegment);

        Processor recommendationResponse = new Processor(recommendation,requestheaders,payload,urlParam);

        Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("status"),"SUCCESS");

        if (size>0)
        {
        	Assert.assertNotNull(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..recommendationId"));
		    recommendationId = recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..recommendationId").replace("[","").replace("]","").replace("\"","").replace("\"","");

		    for (int k=0; k<size; k++)
        	{
            	String expResponse = issueDetails.get("action["+k+"]");

            	if (expResponse == "Replacement")
            	{
            		cloneCheck(orderId, restId, itemId);
            		if (res == 0)
            		{
            	        Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.replicate.cart..id"),itemId);
            	        Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.replicate.cart..name"),itemName);
            	        Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.replicate.cart..isVeg"),is_veg);
            	        Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.replicate.cart..quantity"),quantity);
            	        Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.replicate.orderId"),orderId);
            	        Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.replicate.restaurantId"),restId);
            	        Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.replicate.restaurantName"),restName);
            	        Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.replicate.sla_time"),slaTime);
            		}
            	} else if (expResponse == "Refund")
            	{
        	        Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.refund.cart..id"),itemId.replace("[","").replace("]",""));
        	        Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.refund.cart..name"),itemName);
        	        Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.refund.cart..isVeg"),is_veg);
        	        Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.refund.cart..quantity"),quantity);
        	        Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.refund.orderId"),orderId);
        	        Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.refund.refundAmount"),cost);
            	} else if (expResponse == "Coupon")
            	{
                    Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.refund.cart..id"),itemId.replace("[","").replace("]",""));
                    Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.refund.cart..name"),itemName);
                    Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.refund.cart..isVeg"),is_veg);
                    Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.refund.cart..quantity"),quantity);
                    Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.refund.userId"),userId);
            	}
        	}

        } else
	        Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data..sla"),slaTime);
    }

    @Test (priority = 1, enabled = true, description = "Recommendation tests positive")
    public void recommendationSpecialCases(HashMap<String,String> issueDetails) throws Exception {
        String length = issueDetails.get("length");
        int size = Integer.valueOf(length);

        //Fetch Order type
        orderType = issueDetails.get("orderType");

        if (orderType.equalsIgnoreCase("Regular")) {
            orderId = CRMConstants.regularPrePaidOrderUnServiceable;
        }

        userSegment = issueDetails.get("segment");
        fraudSegment = issueDetails.get("fraud");
        userId = issueDetails.get("userId");

        changeAndValidateUserSegments(userId, userSegment, fraudSegment);

        fetchOrderDetails(orderId);

        //Clone check should fail for the orders
        Processor cloneCheck = cloneCheck(orderId, restId, itemId);
        Assert.assertEquals(cloneCheck.ResponseValidator.GetNodeValueAsStringFromJsonArray("statusCode"), "1");
        Assert.assertEquals(cloneCheck.ResponseValidator.GetNodeValueAsStringFromJsonArray("status"), "FAILURE");
    }


    @Test (priority = 2, enabled = true, dataProviderClass = RecommendationDP.class, dataProvider = "regularPrePaidOrder", description = "Recommendation tests positive")
    public void recommendationRegularPrePaidOrder(HashMap<String,String> issueDetails) throws Exception {
        String length = issueDetails.get("length");
        int size = Integer.valueOf(length);

        //Fetch Order type
        orderType = issueDetails.get("orderType");

        orderId = CRMConstants.regularPrePaidOrder;

        System.out.println("order ID: \t" + orderId);

        userSegment = issueDetails.get("segment");
        fraudSegment = issueDetails.get("fraud");
        userId = issueDetails.get("userId");

        changeAndValidateUserSegments(userId, userSegment, fraudSegment);

        System.out.println("fraudSegment:\t" + fraudSegment);
        fetchOrderDetails(orderId);

        requestheaders = getRequestHeaders();

        System.out.println("requestheaders   " + requestheaders);

        String[] urlParam = new String[]{orderId, userId};

        String[] payload = new String[]{orderId, userId, restId, restName.replace("\"", "").replace("\"", ""), issueDetails.get("issueType"), itemId, itemName, is_veg, quantity, cost, issueDetails.get("fullItem")};

        GameOfThronesService recommendation = new GameOfThronesService("cconeview", "recommendations", gameofthrones);

        System.out.println("\t userId:" + userId + "\t userSegment:" + userSegment + "\t fraudSegment:" + fraudSegment);

        Processor recommendationResponse = new Processor(recommendation, requestheaders, payload, urlParam);

        Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("status"), "SUCCESS");

        if (size > 0) {
            Assert.assertNotNull(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..recommendationId"));
            recommendationId = recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..recommendationId").replace("[", "").replace("]", "").replace("\"", "").replace("\"", "");

            for (int k = 0; k < size; k++) {
                String expResponse = issueDetails.get("action[" + k + "]");

                if (expResponse == "Replacement") {
                    cloneCheck(orderId, restId, itemId);
                    if (res == 0) {
                        Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.replicate.cart..id"), itemId);
                        Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.replicate.cart..name"), itemName);
                        Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.replicate.cart..isVeg"), is_veg);
                        Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.replicate.cart..quantity"), quantity);
                        Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.replicate.orderId"), orderId);
                        Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.replicate.restaurantId"), restId);
                        Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.replicate.restaurantName"), restName);
                        Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.replicate.sla_time"), slaTime);
                    }
                } else if (expResponse == "Refund") {
                    Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.refund.cart..id"), itemId.replace("[", "").replace("]", ""));
                    Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.refund.cart..name"), itemName);
                    Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.refund.cart..isVeg"), is_veg);
                    Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.refund.cart..quantity"), quantity);
                    Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.refund.orderId"), orderId);
                    Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.refund.refundAmount"), cost);
                } else if (expResponse == "Coupon") {
                    Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.refund.cart..id"), itemId.replace("[", "").replace("]", ""));
                    Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.refund.cart..name"), itemName);
                    Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.refund.cart..isVeg"), is_veg);
                    Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.refund.cart..quantity"), quantity);
                    Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.refund.userId"), userId);
                }
            }
        } else
            Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data..sla"), slaTime);

    }

    @Test (priority = 3, enabled = true, dataProviderClass = RecommendationDP.class, dataProvider = "regularPostPaidOrderUnserviceable", description = "Recommendation tests positive")
    public void recommendationRegularPostPaidOrderUnserviceable(HashMap<String,String> issueDetails) throws Exception {
        String length = issueDetails.get("length");
        int size = Integer.valueOf(length);

        //Fetch Order type
        orderType = issueDetails.get("orderType");

        orderId = CRMConstants.regularPostPaidOrderUnserviceable;

        System.out.println("order ID: \t" + orderId);

        userSegment = issueDetails.get("segment");
        fraudSegment = issueDetails.get("fraud");
        userId = issueDetails.get("userId");

        changeAndValidateUserSegments(userId, userSegment, fraudSegment);

        System.out.println("fraudSegment:\t" + fraudSegment);
        fetchOrderDetails(orderId);

        requestheaders = getRequestHeaders();

        System.out.println("requestheaders   " + requestheaders);

        String[] urlParam = new String[]{orderId, userId};

        String[] payload = new String[]{orderId, userId, restId, restName.replace("\"", "").replace("\"", ""), issueDetails.get("issueType"), itemId, itemName, is_veg, quantity, cost, issueDetails.get("fullItem")};
        // ,issueDetails.get("disposition")

        GameOfThronesService recommendation = new GameOfThronesService("cconeview", "recommendations", gameofthrones);

        System.out.println("\t userId:" + userId + "\t userSegment:" + userSegment + "\t fraudSegment:" + fraudSegment);

        Processor recommendationResponse = new Processor(recommendation, requestheaders, payload, urlParam);

        Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("status"), "SUCCESS");

        if (size > 0) {
            Assert.assertNotNull(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..recommendationId"));
            recommendationId = recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..recommendationId").replace("[", "").replace("]", "").replace("\"", "").replace("\"", "");

            for (int k = 0; k < size; k++) {
                String expResponse = issueDetails.get("action[" + k + "]");

                if (expResponse == "Replacement") {
                    cloneCheck(orderId, restId, itemId);
                    if (res == 0) {
                        Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.replicate.cart..id"), itemId);
                        Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.replicate.cart..name"), itemName);
                        Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.replicate.cart..isVeg"), is_veg);
                        Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.replicate.cart..quantity"), quantity);
                        Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.replicate.orderId"), orderId);
                        Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.replicate.restaurantId"), restId);
                        Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.replicate.restaurantName"), restName);
                        Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.replicate.sla_time"), slaTime);
                    }
                } else if (expResponse == "Refund") {
                    Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.refund.cart..id"), itemId.replace("[", "").replace("]", ""));
                    Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.refund.cart..name"), itemName);
                    Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.refund.cart..isVeg"), is_veg);
                    Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.refund.cart..quantity"), quantity);
                    Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.refund.orderId"), orderId);
                    Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.refund.refundAmount"), cost);
                } else if (expResponse == "Coupon") {
                    Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.refund.cart..id"), itemId.replace("[", "").replace("]", ""));
                    Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.refund.cart..name"), itemName);
                    Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.refund.cart..isVeg"), is_veg);
                    Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.refund.cart..quantity"), quantity);
                    Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.refund.userId"), userId);
                }
            }
        } else
            Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data..sla"), slaTime);

    }

    @Test (priority = 4, enabled = true, dataProviderClass = RecommendationDP.class, dataProvider = "regularPrePaidOrderUnServiceable", description = "Recommendation tests positive")
    public void recommendationRegularPrePaidOrderUnserviceable(HashMap<String,String> issueDetails) throws Exception {
        String length = issueDetails.get("length");
        int size = Integer.valueOf(length);

        //Fetch Order type
        orderType = issueDetails.get("orderType");

        orderId = CRMConstants.regularPrePaidOrderUnServiceable;

        System.out.println("order ID: \t" + orderId);

        userSegment = issueDetails.get("segment");
        fraudSegment = issueDetails.get("fraud");
        userId = issueDetails.get("userId");

        changeAndValidateUserSegments(userId, userSegment, fraudSegment);

        System.out.println("fraudSegment:\t" + fraudSegment);
        fetchOrderDetails(orderId);

        requestheaders = getRequestHeaders();

        System.out.println("requestheaders   " + requestheaders);

        String[] urlParam = new String[]{orderId, userId};

        String[] payload = new String[]{orderId, userId, restId, restName.replace("\"", "").replace("\"", ""), issueDetails.get("issueType"), itemId, itemName, is_veg, quantity, cost, issueDetails.get("fullItem")};
        // ,issueDetails.get("disposition")

        GameOfThronesService recommendation = new GameOfThronesService("cconeview", "recommendations", gameofthrones);

        System.out.println("\t userId:" + userId + "\t userSegment:" + userSegment + "\t fraudSegment:" + fraudSegment);

        Processor recommendationResponse = new Processor(recommendation, requestheaders, payload, urlParam);

        Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("status"), "SUCCESS");

        if (size > 0) {
            Assert.assertNotNull(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..recommendationId"));
            recommendationId = recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..recommendationId").replace("[", "").replace("]", "").replace("\"", "").replace("\"", "");

            for (int k = 0; k < size; k++) {
                String expResponse = issueDetails.get("action[" + k + "]");

                if (expResponse == "Replacement") {
                    cloneCheck(orderId, restId, itemId);
                    if (res == 0) {
                        Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.replicate.cart..id"), itemId);
                        Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.replicate.cart..name"), itemName);
                        Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.replicate.cart..isVeg"), is_veg);
                        Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.replicate.cart..quantity"), quantity);
                        Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.replicate.orderId"), orderId);
                        Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.replicate.restaurantId"), restId);
                        Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.replicate.restaurantName"), restName);
                        Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.replicate.sla_time"), slaTime);
                    }
                } else if (expResponse == "Refund") {
                    Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.refund.cart..id"), itemId.replace("[", "").replace("]", ""));
                    Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.refund.cart..name"), itemName);
                    Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.refund.cart..isVeg"), is_veg);
                    Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.refund.cart..quantity"), quantity);
                    Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.refund.orderId"), orderId);
                    Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.refund.refundAmount"), cost);
                } else if (expResponse == "Coupon") {
                    Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.refund.cart..id"), itemId.replace("[", "").replace("]", ""));
                    Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.refund.cart..name"), itemName);
                    Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.refund.cart..isVeg"), is_veg);
                    Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.refund.cart..quantity"), quantity);
                    Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data.refund.userId"), userId);
                }
            }

        } else
            Assert.assertEquals(recommendationResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.recommendations..data..sla"), slaTime);

    }
}
