package com.swiggy.api.erp.ff.dp.WatchDog;

import com.swiggy.api.erp.ff.POJO.WatchDog.ActionContext;
import com.swiggy.api.erp.ff.POJO.WatchDog.Body;
import com.swiggy.api.erp.ff.constants.WatchDogConstants;

import org.testng.annotations.DataProvider;


public class ConditionData {


    @DataProvider(name="registerCondtion")
    public Object[][] getRegisterCondtionData()
    {
        return new Object[][]{
                {"orderId == @{eventContext.instanceId} && eventName == @{eventContext.eventName}"},{"null"},{"orderId == @{eventContext.instanceId} && eventName == null"}
        };


    }

    @DataProvider(name="evlauateCondtion")
    public Object[][] getEvaluateCondtionData()
    {
        String c1 =  "\"eventContext.instanceId\": \"12345\",\"eventContext.eventName\": \"delivered\"";

        return new Object[][]{
                {"101",c1}, {"null",c1}, {"101","null"}
        };


    }


    @DataProvider(name="ecaData")
    public Object[][] getECAData()
    {

        String delivered_state = WatchDogConstants.delivered_state;
        String groupName = WatchDogConstants.groupName;

        String conditionId1 = WatchDogConstants.condtionId1;
        String condition1 = WatchDogConstants.condtion1;


        ActionContext ac1 = new ActionContext();
        Body body1 = new Body();

        /** Action Object 1 **/
        body1.setOrder_id("@{eventContext.instanceId}");
        body1.setWorkflowId("3345666");
        ac1.setType("HTTP_POST");
        ac1.setUrl("http://fulfillment.swiggyint.in/v1/relay_to_delivery");
        ac1.setArgs("");
        ac1.setHeaders("");
        ac1.setBody(body1);


        /** Action Object 1 **/



        return new Object[][]{
                {groupName,delivered_state,conditionId1,condition1,ac1}, {"null",delivered_state,conditionId1,condition1,ac1},
                {groupName,"null",conditionId1,condition1,ac1}, {groupName,delivered_state,"null",condition1,ac1},
                {groupName,delivered_state,conditionId1,condition1,ac1},{groupName,delivered_state,conditionId1,condition1,ac1}

        };


    }



}
