package com.swiggy.api.erp.delivery.helper;

import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import org.springframework.dao.EmptyResultDataAccessException;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author preetesh
 * @project swiggy_test
 * @createdon 11/09/18
 */
public class DeliveryDataBaseUtils {

    static DeliveryDataHelper deliveryDataHelper= new DeliveryDataHelper();
    static Serviceability_Helper serviceablility_Helper = new Serviceability_Helper();

    public static Integer getAreaFromZone(String zoneId)
    {
        String query="select id from area where zone_id="+zoneId;
        return Integer.parseInt(SystemConfigProvider.getTemplate(DeliveryConstant.dbname).queryForMap(query).get("id").toString());
    }

    public static Integer getZoneFromArea(String zoneId)
    {
        String query="select id from area where zone_id="+zoneId;
        return Integer.parseInt(SystemConfigProvider.getTemplate(DeliveryConstant.dbname).queryForMap(query).get("id").toString());
    }

    public static Integer getCityFromZone(String zoneId)
    {
        String query="select city_id from zone where id="+zoneId;
        return Integer.parseInt(SystemConfigProvider.getTemplate(DeliveryConstant.dbname).queryForMap(query).get("city_id").toString());
    }

    public static Integer getRandomBatchingEnabledRestaurantId(String zoneId)
    {
        String query="select r.id  as restId from restaurant r inner join area a on a.id=r.area_code where a.zone_id="+zoneId+" and r.enabled=1 and r.batching_enabled=1 order by 1 desc limit 1";
        return Integer.parseInt(SystemConfigProvider.getTemplate(DeliveryConstant.dbname).queryForMap(query).get("restId").toString());
    }

    public static Integer getRandomEnabledRestaurant(String zoneId)
    {
        String query="select r.id  as restId from restaurant r inner join area a on a.id=r.area_code where a.zone_id="+zoneId+" and r.enabled=1 order by 1 desc limit 1";
        return Integer.parseInt(SystemConfigProvider.getTemplate(DeliveryConstant.dbname).queryForMap(query).get("restId").toString());
    }

    public static Integer getRandomEnabledRestaurantFromArea(String areacode)
    {
        String query="select id from restaurant where area_code =" +areacode+" and enabled=1 order by 1 desc limit 1";
        return Integer.parseInt(SystemConfigProvider.getTemplate(DeliveryConstant.dbname).queryForMap(query).get("id").toString());
    }

    public static String getRestaurantLatLong(String restId)
    {
        String query="select lat_long from restaurant where id ="+restId;
        return SystemConfigProvider.getTemplate(DeliveryConstant.dbname).queryForMap(query).get("lat_long").toString();
    }


    public static String getCentroidLatLonFromZone(String zoneId)
    {
        String query="select path from zone where id ="+zoneId;
        List<String> pathList= new ArrayList();
        String[] latlons=SystemConfigProvider.getTemplate(DeliveryConstant.dbname).queryForMap(query).get("path").toString().split(" ");
        for(String eachlatLaon:latlons)
        {
            pathList.add(eachlatLaon);
        }
        String centroidLatLons=deliveryDataHelper.getCentroid(pathList);
        return centroidLatLons;
    }

    public static String getOrderIdForOrderGeneration()
    {
        String query="select order_id from trips order by id desc limit 1";
        String orderId= SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap(query).get("order_id").toString();
        BigInteger order=(new BigInteger(orderId).add(new BigInteger(new Integer((int)(Math.random()*10+5)).toString())));
        return order.toString();
    }

    public static  boolean  polldb(String dbname,String Query,String searchforparam, String tosearchvalue, int pollint, int maxpollinsec)
    {
        long current=System.currentTimeMillis();
        long finaltime=current+maxpollinsec*1000;
        Boolean pollflag= false;
        if(pollint<100)
        {
            pollint*=1000;
        }
        int numtimespolled =0;
        do
        {
            try {
                Map<String, Object> testMap = SystemConfigProvider.getTemplate(dbname).queryForMap(Query);
                if(testMap.get(searchforparam).toString().equalsIgnoreCase(tosearchvalue))
                {
                    pollflag=true;
                    break;
                }
                else{
                    numtimespolled++;
                    waitIntervalMili(pollint);
                }
            }
            catch (Exception e)
            {
                System.out.println("In Exception");
                numtimespolled++;
                waitIntervalMili(pollint);
            }
        }while(!(System.currentTimeMillis()>finaltime));
        System.out.println("numtimespolled  "+numtimespolled+"  num seconds we polled for in total " +(System.currentTimeMillis()-current)+ "final flag that went out " +pollflag);
        return pollflag;
    }

    public static String getBatchingVersion(String zone_id)
    {
        String query = "select batching_version from zone where id="+zone_id;
        return SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap(query).get("batching_version").toString();

    }

    public static String getBatchIdFromOrderId(String order_id)
    {
        String query="select batch_id from trips where order_id="+order_id;
        return SystemConfigProvider.getTemplate(DeliveryConstant.dbname).queryForMap(query).get("batch_id").toString();
    }

    public static Map<String,Object> getZoneRainParamsFromZoneId(String rain_mode, String zone_id)
    {
        String query="select * from zone_rain_params where zone_id="+zone_id+" and rain_mode="+rain_mode;
        Map<String,Object> zoneRainParams = new LinkedHashMap<>();
        try
        {
            zoneRainParams = SystemConfigProvider.getTemplate(DeliveryConstant.dbname).queryForMap(query);
        }
        catch(EmptyResultDataAccessException ex)
        { zoneRainParams = returnRowNotFoundMap(); }

        return zoneRainParams;
    }

    public static Map<String, Object> returnRowNotFoundMap()
    {
        Map<String,Object> map = new LinkedHashMap<>();
        map.put("rowNotFound", "rowNotFound");
        return map;
    }

    public static Boolean setZoneRainMode(String rain_mode,String zone_id)
    {
        String query="update zone set rain_mode_type="+rain_mode+" where id="+zone_id;
        int update_status=SystemConfigProvider.getTemplate(DeliveryConstant.dbname).update(query);
        return update_status==1;
    }

    public static Boolean setZoneRainModeAndClearCache(String rain_mode, String zone_id)
    {
        String query="update zone set rain_mode_type='" + rain_mode + "' where id='" + zone_id + "'";
        int update_status=SystemConfigProvider.getTemplate(DeliveryConstant.dbname).update(query);
        serviceablility_Helper.clearCache("zone", zone_id);
        return update_status==1;
    }


    public static String getZoneRainMode(String zone_id)
    {
        String query="select rain_mode_type from zone where id='" + zone_id + "'";
        return SystemConfigProvider.getTemplate(DeliveryConstant.dbname).queryForMap(query).get("rain_mode_type").toString();
    }

    public static Boolean updateZoneRainParam(String param_name,String param_value,String rain_mode,String zone_id)
    {
        String query="update zone_rain_params set "+param_name+"="+param_value+" where zone_id="+zone_id+" and rain_mode="+rain_mode;
        int update_status=SystemConfigProvider.getTemplate(DeliveryConstant.dbname).update(query);
        return update_status==1;
    }

    public static Boolean updateZoneRainParamAndClearCache(String param_name, String param_value, String rain_mode, String zone_id)
    {
        String query="update zone_rain_params set "+param_name+"="+param_value+" where zone_id="+zone_id+" and rain_mode="+rain_mode;
        int update_status=SystemConfigProvider.getTemplate(DeliveryConstant.dbname).update(query);
        serviceablility_Helper.clearCache("zone_rain_params", DeliveryDataBaseUtils.getZoneRainParamIdFromZoneId(zone_id.toString(),rain_mode));
        return update_status==1;
    }

    public static String  getZoneRainParamIdFromZoneId(String zone_id, String rain_mode)
    {
        String query="select id from zone_rain_params where zone_id="+zone_id+" and rain_mode="+rain_mode;
        return SystemConfigProvider.getTemplate(DeliveryConstant.dbname).queryForMap(query).get("id").toString();
    }

    public static void waitIntervalMili(int pollinterval)
    {
        try
        {
            Thread.sleep(pollinterval);
        }
        catch(InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    public static Boolean setMaxLastMileForArea(String area_id,String val)
    {
        String query="update area set last_mile_cap="+val+" where id="+area_id;
        int update_status=SystemConfigProvider.getTemplate(DeliveryConstant.dbname).update(query);
        return update_status==1;
    }

    public static Boolean setMaxLastMileForAreaAndClearCache(String area_id,String val)
    {
        String query="update area set last_mile_cap="+val+" where id="+area_id;
        int update_status=SystemConfigProvider.getTemplate(DeliveryConstant.dbname).update(query);
        serviceablility_Helper.clearCache("area", area_id);
        return update_status==1;
    }

    public static Boolean setMaxLastMileForRestaurant(String restaurant_id,String val)
    {
        String query="update restaurant set max_second_mile="+val+" where id="+restaurant_id;
        int update_status=SystemConfigProvider.getTemplate(DeliveryConstant.dbname).update(query);
        return update_status==1;
    }

    public static Boolean setMaxLastMileForRestaurantAndClearCache(String restaurant_id,String val)
    {
        String query="update restaurant set max_second_mile="+val+" where id="+restaurant_id;
        int update_status=SystemConfigProvider.getTemplate(DeliveryConstant.dbname).update(query);
        serviceablility_Helper.clearCache("restaurant", restaurant_id);
        return update_status==1;
    }

    public static String getMaxLastMileForArea(String area_id)
    {
        String query="select last_mile_cap from area where id="+area_id;
        String lastMile = SystemConfigProvider.getTemplate(DeliveryConstant.dbname).queryForMap(query).get("last_mile_cap").toString();
        return lastMile;
    }

    public static String getMaxLastMileForRestaurant(String restaurant_id)
    {
        String query="select max_second_mile from restaurant where id='" + restaurant_id + "'";
        String lastMile = SystemConfigProvider.getTemplate(DeliveryConstant.dbname).queryForMap(query).get("max_second_mile").toString();
        return lastMile;
    }

    public static Boolean setZoneRaincause(String zone_id,String val,String rain_mode)
    {
        String query="update zone_rain_params set cause='" + val + "' where zone_id=" + zone_id + " and rain_mode='" + rain_mode + "'";
        int update_status=SystemConfigProvider.getTemplate(DeliveryConstant.dbname).update(query);
        return update_status==1;
    }

    public static String getZoneRaincause(String zone_id, String rain_mode)
    {
        String query="select cause from zone_rain_params where zone_id='" + zone_id + "' and rain_mode='" + rain_mode + "'";
        String cause = SystemConfigProvider.getTemplate(DeliveryConstant.dbname).queryForMap(query).get("cause").toString();
        return cause;
    }

    public static Boolean setZoneMaxSla(String value, String zoneId)
    {
        String query = "update zone set max_delivery_time=" + value + " where id=" + zoneId;
        int status = SystemConfigProvider.getTemplate(DeliveryConstant.dbname).update(query);
        return status == 1;
    }

    public static String getZoneMaxSla(String zoneId)
    {
        String query = "select max_delivery_time from zone where id=" + zoneId;
        String maxSla = SystemConfigProvider.getTemplate(DeliveryConstant.dbname).queryForMap(query).get("max_delivery_time").toString();
        return maxSla;
    }

    public static String getLDmaxSla(String longDistanceId)
    {
        String value = SystemConfigProvider.getTemplate(DeliveryConstant.dbname).queryForMap("select max_sla from long_distance where id='" + longDistanceId + "'").get("max_sla").toString();
        return value;
    }

    public static String getCityIdFromArea(String areaId)
    {
        String id = SystemConfigProvider.getTemplate(DeliveryConstant.dbname).queryForMap("select city_id from area where id ='" + areaId + "'").get("city_id").toString();
        return id;
    }

    public static String getZoneIdFromArea(String areaId)
    {
        String id = SystemConfigProvider.getTemplate(DeliveryConstant.dbname).queryForMap("select zone_id from area where id='" + areaId + "'").get("zone_id").toString();
        return id;
    }

    public static String getDeliveryRadiusFromCity(String cityId)
    {
        String id = SystemConfigProvider.getTemplate(DeliveryConstant.dbname).queryForMap("select delivery_radius from city where id='"+cityId + "'").get("delivery_radius").toString();
        return id;
    }

    public static String getMaxBannerFactorFromRestaurant(String restaurantId)
    {
        String banner_factor = SystemConfigProvider.getTemplate(DeliveryConstant.dbname).queryForMap("select max_banner_factor from restaurant where id ='"+restaurantId + "'").get("max_banner_factor").toString();
        return banner_factor;
    }

    public static String getAreaCodeFromRestaurant(String restaurantId)
    {
        String areaCode = SystemConfigProvider.getTemplate(DeliveryConstant.dbname).queryForMap("select area_code from restaurant where id='"+ restaurantId + "'").get("area_code").toString();
        return areaCode;
    }

    public static String getLDmaxLastMile(String lDPolygonId)
    {
        String maxLastMile = SystemConfigProvider.getTemplate(DeliveryConstant.dbname).queryForMap("select max_last_mile from long_distance where polygon_id='" + lDPolygonId + "'").get("max_last_mile").toString();
        return maxLastMile;
    }

    public static String getLongDistanceIdFromPolygonId(String lDPolygonId)
    {
        String longDistanceId = SystemConfigProvider.getTemplate(DeliveryConstant.dbname).queryForMap("select id from long_distance where polygon_id='" + lDPolygonId + "'").get("id").toString();
        return longDistanceId;
    }

    public static Map<String, Integer> setNoRainsAtRestaurantAndCustomer(String restZoneId, String custZoneId)
    {
        Integer restRainMode = 0, customerRainMode = 0;
        Map<String, Integer> rainModeMap = new LinkedHashMap<String, Integer>();
        if(restZoneId != null)
        {
            restRainMode = getRainMode(restZoneId);
            rainModeMap.put("restaurantRainMode", restRainMode);
            if(restRainMode != 3)
                serviceablility_Helper.setNoRainsAtZone(restZoneId);
        }
        if(custZoneId != null)
        {
            customerRainMode = getRainMode(String.valueOf(custZoneId));
            rainModeMap.put("customerRainMode", customerRainMode);
            if(customerRainMode != 3)
                serviceablility_Helper.setNoRainsAtZone(String.valueOf(custZoneId));
        }
        System.out.println("***** MAP ****** " + rainModeMap);
        return rainModeMap;
    }

    public static Integer getRainMode(String zone_id)
    {
        return Integer.parseInt(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).
                queryForMap( "select rain_mode_type from zone where id="+zone_id).get("rain_mode_type").toString());
    }

    public static void setRainModeAndCause(Integer actualRainMode, Integer expectedRainMode, Integer zone_id)
    {

        if(actualRainMode != expectedRainMode)
            //SystemConfigProvider.getTemplate("deliverydb").update("update zone set rain_mode_type="+expectedRainMode+ " where id="+zone_id);
           serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(expectedRainMode), "id", String.valueOf(zone_id));
       //String cause= SystemConfigProvider.getTemplate("deliverydb").queryForMap("select cause from zone_rain_params where zone_id="+zone_id +" and rain_mode=" +expectedRainMode).get("cause").toString();
        String cause = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select cause from zone_rain_params where zone_id=" + zone_id + " and rain_mode='" + expectedRainMode + "'").get("cause").toString();
        if(!cause.equals("rain") || cause == null)
            // SystemConfigProvider.getTemplate("deliverydb").update("update zone_rain_params set cause='rain' where zone_id="+zone_id);
            setCauseRain(zone_id, expectedRainMode);
    }

    public static void setCauseRain(Integer zoneId, Integer rainMode)
    {
        String id = getZoneRainParamIdFromZoneId(zoneId.toString(), rainMode.toString());
        SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update("update zone_rain_params set cause='rain' where zone_id='" + zoneId + "' and rain_mode='" + rainMode + "'");
        serviceablility_Helper.clearCache("zone_rain_params", id);
    }

    public static Boolean isRestaurantPresentInDeliveryDB(String restId)
    {
        Boolean flag = false;
        try {

            Map<String, Object> map = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select * from restaurant where id='" + restId + "'");
            System.out.println(" ************ MAP ********** " + map);
            flag = true;
        }
        catch(org.springframework.dao.EmptyResultDataAccessException ex){flag = false;}
        System.out.println("****** flag ******** " + flag);
        return flag;
    }

    public static String getEntityIdFromEsme(String name)
    {
        String id = SystemConfigProvider.getTemplate("degradation").queryForMap("select id from entities where name='" + name + "'").get("id").toString();
        return id;
    }

    public static String getEntityRefIdFromEsme(String name) {
        String id = SystemConfigProvider.getTemplate("degradation").queryForMap("select reference_id from entities where name='" + name + "'").get("reference_id").toString();
        return id;
    }

    public Boolean isMetaKeyPresentInDeliveryConfigDB(String metaKey)
    {
        Boolean flag = false;
        try
        {
            String key = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select meta_key from delivery_config where meta_key='" + metaKey + "'").get("meta_key").toString();
            flag = true;
        }
        catch(org.springframework.dao.EmptyResultDataAccessException e){}
        return flag;
    }

    public Boolean addKeyInDeliveryConfigDB(String metaKey, String metaValue, String desc)
    {
        Boolean flag = false;
        try
        {
            String key = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select meta_key from delivery_config where meta_key='" + metaKey + "'").get("meta_key").toString();
        }
        catch(org.springframework.dao.EmptyResultDataAccessException e){
            SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).execute("INSERT INTO `delivery_config` (`meta_key`, `meta_value`, `description`) VALUES('" + metaKey + "', '" + metaValue + "', '" + desc + "')");
            flag = isMetaKeyPresentInDeliveryConfigDB(metaKey);
        }
        return flag;
    }

    public String getValueOfMetaKeyFromDeliveryConfig(String metaKey)
    {
        String value = "nil";
        try
        {
            value = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select meta_value from delivery_config where meta_key='" + metaKey + "'").get("meta_value").toString();
        }
        catch(org.springframework.dao.EmptyResultDataAccessException e){}
        return value;
    }

    public void updateMetaValueInDeliveryConfig(String metaKey, String metaValue)
    {
        SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update("update delivery_config set meta_value='" + metaValue + "' where meta_key='" + metaKey + "'");
    }

    public Boolean IsMetaKeyContainsCityOrZoneId(String metaValue, String id){
        Boolean flag = false;
        if(metaValue.contains(id))
        {
            flag = true;
        }
        return flag;
    }

    public void setTempLastMileInZone(Boolean isTempLastMileEnabled, String zoneId, String lmValue)
    {
        String temp_lm_enabled = (isTempLastMileEnabled.equals(true)) ? "1" : "0";
        SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update("update zone set temp_last_mile='" + lmValue + "' and temp_lm_enabled='" + temp_lm_enabled + "' where id='" + zoneId + "'");
        serviceablility_Helper.clearCache("zone", zoneId);
    }
}
