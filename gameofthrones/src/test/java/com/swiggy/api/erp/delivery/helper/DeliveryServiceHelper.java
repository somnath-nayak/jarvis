package com.swiggy.api.erp.delivery.helper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.castleblack.CastleBlackConstants;
import com.swiggy.api.castleblack.CastleBlackManager;
import com.swiggy.api.castleblack.SlackMessenger;
import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.erp.delivery.constants.EndToEndConstants;
import com.swiggy.api.erp.ff.constants.LosConstants;
import com.swiggy.api.erp.ff.helper.OMSHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Daenerys.Initializer;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.DBHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;
import org.testng.Assert;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DeliveryServiceHelper {


	Initialize gameofthrones;
	DeliveryHelperMethods del;
	CastleBlackManager castleBlackManager;
	EndToEndHelp endToEndHelp;
	DBHelper dbHelper;

	//Initialize gameofthrones = new Initialize();
		
	public DeliveryServiceHelper() {
		gameofthrones = Initializer.getInitializer();
		del=new DeliveryHelperMethods();
		castleBlackManager = new CastleBlackManager();
		endToEndHelp=new EndToEndHelp();
		dbHelper = new DBHelper();
		}
		
//Initialize gameofthrones = new Initialize();
	

	
	boolean b;

	// TODO Auto-generated constructor stub

	HashMap<String, String> requestheaders ;
	// TODO Auto-generated constructor stub
    public DeliveryServiceHelper(String auth)
    {
        gameofthrones= new Initialize();
        requestheaders= new HashMap<>();
        requestheaders.put("content-type", "application/json");
        requestheaders.put("Authorization", auth);
    }


 	

	public Processor makeDEFree(String de_id)  {
/*		String query1=DeliveryConstant.destatusfree + de_id +";";
		String query2=DeliveryConstant.debatchidnull + de_id +";";
		delmeth.dbhelperupdate(query1);
		Thread.sleep(5000);
		delmeth.dbhelperupdate(query2);
		Thread.sleep(5000);
		//This was not required as status is updated to FR and also batch id is set to null by API Itself
		 * */
		 
		GameOfThronesService service = new GameOfThronesService(
				DeliveryConstant.delivery_Service, "makeDEFree", gameofthrones);
		HashMap<String, String> requestheaders = del.doubleheader();
		String[] payloadparams = new String[] { de_id };
		Processor processor = new Processor(service, requestheaders,
				payloadparams);
		System.out.println("RESPONSESTATUS FROM MAKE DE FREE"+processor.ResponseValidator.GetResponseCode());
		return processor;
	}

	public Boolean makeDEFree(String de_id, String appversion)  {
		GameOfThronesService service = new GameOfThronesService(
				DeliveryConstant.delivery_Service, "makeDEFree", gameofthrones);
		HashMap<String, String> requestheaders = del.doubleheader();
		String[] payloadparams = new String[]{de_id};
		Processor processor = new Processor(service, requestheaders,
				payloadparams);
		Boolean statuscodeflag=processor.ResponseValidator.GetResponseCode()==200;
		Boolean destatuscheck=SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select status from delivery_boys where id="+de_id).get("status").toString().equalsIgnoreCase("FR");
		//need to add check for batch id as well
		return statuscodeflag&destatuscheck;
	}
	public Processor makeDEFreeInProd(String de_id) throws InterruptedException {
		GameOfThronesService service = new GameOfThronesService(
				DeliveryConstant.delivery_Service, "makeDEFree", gameofthrones);
		HashMap<String, String> requestheaders = del.doubleheader();
		String[] payloadparams = new String[] { de_id };
		Processor processor = new Processor(service, requestheaders,
				payloadparams);
		return processor;
	}

	public Processor makeDEActive (String de_id){
		GameOfThronesService service = new GameOfThronesService(DeliveryConstant.delivery_Service,"makeDEActive", gameofthrones);
		HashMap<String, String> requestheaders = del.doubleheader();
		String[] payloadparams = new String[] { de_id };
		Processor processor = new Processor(service, requestheaders, payloadparams);
		//if(processor.ResponseValidator.GetResponseCode()==200)
		SystemConfigProvider.getTemplate("deliverydb").update("update `delivery_boys`set `enabled`=1 WHERE id="+de_id);
		return  processor;
//		else {
//			return null;
//		}
	}

	public Processor makeDEBusy(String de_id) {
		GameOfThronesService service = new GameOfThronesService(
				DeliveryConstant.delivery_Service, "makeDEBusy", gameofthrones);
		HashMap<String, String> requestheaders = del.doubleheader();
		String[] payloadparams = new String[] { de_id };
		Processor processor = new Processor(service, requestheaders,
				payloadparams);
		return processor;

	}

	public Processor deLogout(String auth) {
		GameOfThronesService service = new GameOfThronesService(
				DeliveryConstant.delivery_Service, "DELogout", gameofthrones);
		HashMap<String, String> requestheaders = del.singleheader();
		requestheaders.put("Authorization", auth);
		Processor processor = new Processor(service, requestheaders,
				null);
		return processor;

	}

	public Processor reassign(String order_id, String reassign_status_code,
							  String city_id) throws InterruptedException {
		GameOfThronesService service = new GameOfThronesService(
				DeliveryConstant.delivery_Service, "orderreassign", gameofthrones);
		HashMap<String, String> requestheaders = del.doubleheader();

		Thread.sleep(12000);

		String[] payloadparams = new String[] { order_id, reassign_status_code,
				city_id };
		Processor processor = new Processor(service, requestheaders,
				payloadparams);
		return processor;

	}

	public Processor unAssign(String order_id, String delivery_boy_id)
			throws InterruptedException {

		HashMap<String, String> requestheaders = del.doubleheader();
		Thread.sleep(12000);
		GameOfThronesService service = new GameOfThronesService(
				DeliveryConstant.delivery_Service, "orderunassign", gameofthrones);
		String[] payloadparams = new String[] { order_id, delivery_boy_id };
		Processor processor = new Processor(service, requestheaders,
				payloadparams);
		return processor;
	}

	public Processor orderAssign(String order_id, String delivery_boy_id)
			throws InterruptedException {

		HashMap<String, String> requestheaders = del.doubleheader();
		Thread.sleep(12000);
		makeDEActive(delivery_boy_id);
		makeDEFree(delivery_boy_id);
		GameOfThronesService service = new GameOfThronesService(
				DeliveryConstant.delivery_Service, "ordderassign", gameofthrones);
		String[] payloadparams = new String[] { order_id, delivery_boy_id };
		Processor processor = new Processor(service, requestheaders,
				payloadparams);
		return processor;
	}

	public Processor orderAssignProd(String order_id, String delivery_boy_id)
			throws InterruptedException {

		HashMap<String, String> requestheaders = del.doubleheader();
		Thread.sleep(12000);
		makeDEActive(delivery_boy_id);
		makeDEFreeInProd(delivery_boy_id);
		GameOfThronesService service = new GameOfThronesService(
				DeliveryConstant.delivery_Service, "ordderassign", gameofthrones);
		String[] payloadparams = new String[] { order_id, delivery_boy_id };
		Processor processor = new Processor(service, requestheaders,
				payloadparams);
		return processor;
	}



	// Sample order_id:30001322 and de_id=216
	public boolean assignOrder(String order_id) {
		String batch_id = null;
		HashMap<String, String> requestheaders = del.doubleheader();
		String query = "Select * from trips where order_id=" + order_id + ";";
		Object obj = del.dbhelperget(query, "batch_id",  DeliveryConstant.databaseName);
		if (null == obj) {
			return false;
		} else {
			batch_id = obj.toString();
			
		}
		makeDEFree(DeliveryConstant.snded_id);
		makeDEActive(DeliveryConstant.snded_id);
		GameOfThronesService service = new GameOfThronesService(
				"deliveryservice", "assignorder", gameofthrones);
		String[] payloadparams = new String[] { batch_id,
				DeliveryConstant.snded_id };
		Processor processor = new Processor(service, requestheaders,
				payloadparams);
        return (processor.ResponseValidator.GetResponseCode()) == 200;
			
			
		}
		
	

	public Processor assignOrder(String order_id,String de_id)
	{
		try {
			Thread.sleep(15000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		HashMap<String, String> requestheaders = del.doubleheader();
		String batch_id =SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForList(DeliveryConstant.batchIdFromOrderId+order_id).get(0).get("batch_id").toString();
		makeDEFree(de_id);
		makeDEActive(de_id);
		//Thread.sleep(10000);
		GameOfThronesService service = new GameOfThronesService("deliveryservice", "assignorder", gameofthrones);
		String[] payloadparams = new String[] { batch_id, de_id };
		Processor processor = new Processor(service, requestheaders,
				payloadparams);
		System.out.println("Printing Assignment statuscode"+processor.ResponseValidator.GetResponseCode());
		System.out.println("Printing Assignment Resposne"+processor.ResponseValidator.GetBodyAsText());
		return processor;
	}


/*	public Processor assignOrder(String order_id,String de_id)
	{
		HashMap<String, String> requestheaders = del.doubleheader();
		String batch_id =SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForList(DeliveryConstant.batchIdFromOrderId+order_id).get(0).get("batch_id").toString();
		makeDEActive(de_id);
		GameOfThronesService service = new GameOfThronesService("deliveryservice", "assignorder", gameofthrones);
		String[] payloadparams = new String[] { batch_id, de_id };
		Processor processor = new Processor(service, requestheaders, payloadparams);
		return processor;
	}*/

	public boolean zipDialConfirm(String order_id) {
		Processor processor =zipDialConfirmDE(order_id);
        return (processor.ResponseValidator.GetResponseCode()) == 200;
	}

	// Sample order_id:30001322
	public Processor zipDialConfirmDE(String order_id) {
		HashMap<String, String> requestheaders = del.singleheader();
		GameOfThronesService service = new GameOfThronesService(
				DeliveryConstant.delivery_Service, "zipdialorderconfirmed", gameofthrones);
		String[] payloadparams = new String[] { order_id };
		Processor processor = new Processor(service, requestheaders,
				payloadparams);
		return processor;
	}

	public boolean zipDialArrived(String order_id) {
		Processor processor = zipDialArrivedDE(order_id);
        return (processor.ResponseValidator.GetResponseCode()) == 200;
	}

	public Processor zipDialArrivedDE(String order_id) {
		HashMap<String, String> requestheaders = del.singleheader();
		GameOfThronesService service = new GameOfThronesService(
				"deliveryservice", "zipdialorderarrived", gameofthrones);
		String[] payloadparams = new String[] { order_id };
		Processor processor = new Processor(service, requestheaders,
				payloadparams);
		return processor;

	}
	// Sample order_id:30001322
	public boolean zipDialPickedUp(String order_id) {
		HashMap<String, String> requestheaders = del.singleheader();
		GameOfThronesService service = new GameOfThronesService(
				DeliveryConstant.delivery_Service, "zipdialorderpickedup", gameofthrones);
		String[] payloadparams = new String[] { order_id };
		Processor processor = new Processor(service, requestheaders,
				payloadparams);
        return (processor.ResponseValidator.GetResponseCode()) == 200;
	}

	public Processor zipDialPickedUpDE(String order_id) {
		HashMap<String, String> requestheaders = del.singleheader();
		GameOfThronesService service = new GameOfThronesService(
				DeliveryConstant.delivery_Service, "zipdialorderpickedup", gameofthrones);
		String[] payloadparams = new String[] { order_id };
		Processor processor = new Processor(service, requestheaders,
				payloadparams);
		return processor;
	}

	public boolean zipDialReached(String order_id) {
		Processor processor = zipDialReachedDE(order_id);
        return (processor.ResponseValidator.GetResponseCode()) == 200;

	}
	// Sample order_id:30001322
	public Processor zipDialReachedDE(String order_id) {
		HashMap<String, String> requestheaders = del.singleheader();
		GameOfThronesService service = new GameOfThronesService(
				DeliveryConstant.delivery_Service, "zipdialorderreached", gameofthrones);
		String[] payloadparams = new String[] { order_id };
		Processor processor = new Processor(service, requestheaders,
				payloadparams);
		return processor;

	}

	public String addArea_MappedDETest(String de_id, String area) {
		GameOfThronesService service = new GameOfThronesService(
				DeliveryConstant.delivery_Service, "AddArea_MappedDE", gameofthrones);

		Processor processor = new Processor(service,
				del.doubleheader(), new String[] { de_id, area });
		int data = processor.ResponseValidator.GetNodeValueAsInt("$.data");
		return String.valueOf(data);

	}

	public boolean zipDialDelivered(String order_id) {
		Processor processor = zipDialDeliveredDE(order_id);
        return (processor.ResponseValidator.GetResponseCode()) == 200;

	}
	public Processor zipDialDeliveredDE(String order_id) {
		HashMap<String, String> requestheaders = del.singleheader();
		GameOfThronesService service = new GameOfThronesService(
				DeliveryConstant.delivery_Service, "zipdialorderdelivered", gameofthrones);
		String[] payloadparams = new String[] { order_id };
		Processor processor = new Processor(service, requestheaders,
				payloadparams);
		return processor;


	}
	
	public Processor zipDialRejectedDE(String order_id) {
		HashMap<String, String> requestheaders = del.doubleheader();
		GameOfThronesService service = new GameOfThronesService(
				DeliveryConstant.delivery_Service, "zipdialorderrejected", gameofthrones);
		String[] payloadparams = new String[] { order_id };
		Processor processor = new Processor(service, requestheaders,
				payloadparams);
		return processor;


	}

	public boolean enableRainMode(String rainModeType, String restid)
			throws InterruptedException {
		String id = null, namequery = null, name = null, cityId = null;
		String query = "select * from restaurant r inner join area a on r.area_code=a.id where r.id="
				+ restid + ";";
		Object obj = del.dbhelperget(query, "zone_id",  DeliveryConstant.databaseName);
		if (null == obj) {
			return false;

		} else {
			id = obj.toString();
		}
		Object obj1 = del.dbhelperget(query, "city_id",  DeliveryConstant.databaseName);
		if (null == obj1) {
			return false;
		} else {
			cityId = obj1.toString();
		}
		Object obj2 = "Select * from zone where id=" + id + ";";
		if (null == obj2) {
			return false;
		} else {
			namequery = obj2.toString();
		}
		Object obj3 = del.dbhelperget(namequery,"name",  DeliveryConstant.databaseName);
		if (null == obj3) {
			return false;
		} else {
			name = obj3.toString();
		}
		GameOfThronesService service = new GameOfThronesService(
				DeliveryConstant.delivery_Service, "enablerainmode", gameofthrones);
		HashMap<String, String> requestheaders = del.singleheader();
		String[] payloadparams = new String[] { id, name, cityId, rainModeType };
		Processor processor = new Processor(service, requestheaders,
				payloadparams);
        return (processor.ResponseValidator.GetResponseCode()) != 200;
	}

	public Processor systemRejectOrder(String deId, String orderId) {
		GameOfThronesService service = new GameOfThronesService(
				DeliveryConstant.delivery_Service, "Systemreject", gameofthrones);
		HashMap<String, String> requestheaders = del.doubleheader();
		String[] payloadparams = new String[] { deId, orderId };
		Processor processor = new Processor(service, requestheaders,
				payloadparams);
		return processor;

	}
	
	// update parameter for autoassigment
		public Processor updateParamAuto(String City, String zone, String Param_id, String Param_value, String user)
				throws InterruptedException {
			HashMap<String, String> requestheaders = del.singleheader();
			GameOfThronesService service = new GameOfThronesService(
					"deliveryservice", "autoassignupdateparam", gameofthrones);
			String[] payloadparams = new String[]{City,zone,Param_id,Param_value,user};
					Processor processor = new Processor(service, requestheaders,
					payloadparams);
			return processor;
		}

	public Processor markOrderWithDE(String orderId)
	{
		GameOfThronesService service = new GameOfThronesService(
				DeliveryConstant.delivery_Service, "MarkorderWithDE", gameofthrones);
		HashMap<String, String> requestheaders = del.doubleheader();
		String[] payloadparams = new String[] {orderId};
		Processor processor = new Processor(service, requestheaders,
				payloadparams);
		return processor;

	}
	
	public Processor updateZoneBatchingParams(String id,String name,String cityId,String rainModeType,String maxOrdersInBatch, String maxSlaOfBatch,String batchingVersion,String maxOrderDiff,String maxCustEdge,String maxBatchElapsedTime,String maxItemsInBatch,String maxCustEdgeV2,String maxOrderDiffV2,String maxRestEdge,String maxBill) {
		GameOfThronesService service = new GameOfThronesService(
				DeliveryConstant.delivery_Service, "enablerainmode", gameofthrones);
		HashMap<String, String> requestheaders = del.doubleheader();
		String[] payloadparams = new String[] {id,name,cityId,rainModeType,maxOrdersInBatch,maxSlaOfBatch,batchingVersion,maxOrderDiff,maxCustEdge,maxBatchElapsedTime,maxItemsInBatch,maxCustEdgeV2,maxOrderDiffV2,maxRestEdge,maxBill};
		Processor processor = new Processor(service, requestheaders,
				payloadparams);
		return processor;
	}




	public List<Map<String, Object>> deliveryStatus(String orderId)
	{
		List<Map<String, Object>> orderDetailsList=dbHelper.getMySqlTemplate(LosConstants.hostName).queryForList(CastleBlackConstants.dbStatus+orderId+")"+";");
		return orderDetailsList;
	}

	public List<Map<String, Object>> delDB(String orderId)
	{
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CastleBlackConstants.deDb);
		List<Map<String, Object>> list = sqlTemplate.queryForList(CastleBlackConstants.deStatus+orderId+CastleBlackConstants.rko);
		return list;
	}

	/**
	 * Process Order in delivery Till Deliver
	 * @param orderId
	 * @param toStatus
	 * @throws InterruptedException 
	 */
	public void processOrderInDelivery(String orderId, String toStatus,String de_id) throws InterruptedException ,IOException {
		OMSHelper omsHelper = new OMSHelper();
		String env= gameofthrones.EnvironmentDetails.setup.getEnvironmentUrl().substring(40,46);
		System.out.println("--"+env);
		try {
			String repsone = omsHelper.getOrderInfoFromOMS(orderId, CastleBlackConstants.E2EUSER,CastleBlackConstants.E2EPASSWORD);
			String currentStatus = JsonPath.read(repsone, "$..status.placed_status").toString().
					replace("[", "").replace("]", "").replaceAll("\"", "");
			System.out.println("====" + currentStatus);
			Assert.assertEquals(currentStatus, "unplaced","Status from OMS is null");
			boolean verificationRequired = JsonPath.read(repsone, "$..status.current_order_action").toString().
					replace("[", "").replace("]", "").equals("001");
			String[] rules = getRulesForExecution(currentStatus + ":" + toStatus);
			Assert.assertNotNull(rules.length);

			makeDEFree(de_id);
			Thread.sleep(5000);
			makeDEActive(de_id);
			Thread.sleep(5000);
			for (String rule : rules) {
				System.out.println("--status--" + rule);
				Thread.sleep(5000);
				switch (rule) {
					case "updateaction":
						omsHelper.verifyOrders(orderId, "001",CastleBlackConstants.E2EUSER,CastleBlackConstants.E2EPASSWORD);
						omsHelper.changeOrderStatusToPlaced(orderId, CastleBlackConstants.E2EUSER,CastleBlackConstants.E2EPASSWORD);
						break;
					case OrderStatus.ASSIGNED:
						if (null == de_id) {
							assignOrder(orderId);
						} else {
							assignOrder(orderId, de_id);
						}
						break;
					case OrderStatus.CONFIRMED:
						zipDialConfirm(orderId);
						break;
					case OrderStatus.ARRIVED:
						zipDialArrived(orderId);
						break;
					case OrderStatus.PICKEDUP:
						zipDialPickedUp(orderId);
						break;
					case OrderStatus.REACHED:
						zipDialReached(orderId);
						break;
					case OrderStatus.DELIVERED:
						zipDialDelivered(orderId);
						break;
					default:
						break;
				}
			}
				String repsone1 = omsHelper.getOrderInfoFromOMS(orderId,CastleBlackConstants.E2EUSER,CastleBlackConstants.E2EPASSWORD);
				String currentStatus1 = JsonPath.read(repsone1, "$..status.placed_status").toString().
						replace("[", "").replace("]", "").replaceAll("\"", "");
				System.out.println("====>" + currentStatus1);
				Thread.sleep(5000);
				List<Map<String, Object>> l= delDB(orderId);
				System.out.println("==2"+l.get(0).get("delivered_time"));
				Assert.assertNotNull(l.get(0).get("delivered_time"), "Order is not in Delivered Status");
				Thread.sleep(5000);

				List<Map<String, Object>> ffStatus=deliveryStatus(orderId);
				String stat= (String) ffStatus.get(0).get("delivery_status");
				System.out.println(stat+" "+toStatus);
				//Assert.assertEquals(stat, toStatus, "Order is not in Delivered Status");
		}

		catch (Exception e)
		{
			String msg="Order is not in delivered state";
			String stage= env.substring(env.length() - 1);
			System.out.println("--"+stage);
			System.out.println("---"+e);
			castleBlackManager.insideCastleBlack(endToEndHelp.runId.s, EndToEndConstants.omsPageName, "oms-u-cuat-0"+stage+".swiggyops.de/get_order_from_id/"+orderId+"/formatted", "500", "NOT OK");
			SlackMessenger.webHook1(">>>" + "*Environment*" + "--" + "`" + env + "`" + "\n" + "*Service Name*" + "--" + "`" + EndToEndConstants.omsPageName + "`" + "\n" + "*Service URL*" + "--" + "`" + "oms-u-cuat-0"+stage+".swiggyops.de/get_order_from_id/"+"/formatted" + "`" + "\n" + "*Status Code*" + "--" + "`" + "500" + "`" + "\n" + "*Request Type*" + "--" + "`" + "POST" + "`" + "\n" + "*Message*" + "--" + "`" + msg + "`" + "\n" + "*Response*" + "--" + "```" + "Product is not in"+" "+toStatus+" "+"status"+"```" ,"200");

		}
		catch(AssertionError e)
		{
			String msg=e.getMessage();
			String stage= env.substring(env.length() - 1);
			System.out.println("--"+stage);
			castleBlackManager.insideCastleBlack(endToEndHelp.runId.s, EndToEndConstants.omsPageName, "oms-u-cuat-0"+stage+".swiggyops.de/get_order_from_id/"+orderId+"/formatted", "500", "NOT OK");
			SlackMessenger.webHook1(">>>" + "*Environment*" + "--" + "`" + env + "`" + "\n" + "*Service Name*" + "--" + "`" + EndToEndConstants.omsPageName + "`" + "\n" + "*Service URL*" + "--" + "`" + "oms-u-cuat-0"+stage+".swiggyops.de/get_order_from_id/"+"/formatted" + "`" + "\n" + "*Status Code*" + "--" + "`" + "500" + "`" + "\n" + "*Request Type*" + "--" + "`" + "POST" + "`" + "\n" + "*Message*" + "--" + "`" + msg+ "`" + "\n" + "*Response*" + "--" + "```" + "Product is not in"+" "+toStatus+" "+"status"+"```" ,"200");
		}
	}


	public void processOrderInDeliveryStage(String orderId, String toStatus,String de_id) throws IOException {
		OMSHelper omsHelper = new OMSHelper();
		String env= gameofthrones.EnvironmentDetails.setup.getEnvironmentUrl().substring(40,46);
		try {
			String repsone=omsHelper.getOrderInfoFromOMSReturn(orderId, CastleBlackConstants.E2EUSER,CastleBlackConstants.E2EPASSWORD).ResponseValidator.GetBodyAsText();
			Assert.assertEquals(omsHelper.getOrderInfoFromOMSReturn(orderId, CastleBlackConstants.E2EUSER,CastleBlackConstants.E2EPASSWORD).ResponseValidator.GetResponseCode(),200, "Not able to place order in OMS");
			String currentStatus = JsonPath.read(repsone, "$..status.placed_status").toString().
					replace("[", "").replace("]", "").replaceAll("\"", "");
			System.out.println("====" + currentStatus);
			Assert.assertEquals(currentStatus, "unplaced","Status from OMS is null");
			boolean verificationRequired = JsonPath.read(repsone, "$..status.current_order_action").toString().
					replace("[", "").replace("]", "").equals("001");
			String[] rules = getRulesForExecution(currentStatus + ":" + toStatus);
			Assert.assertNotNull(rules.length);

			Assert.assertEquals(makeDEFree(de_id).ResponseValidator.GetResponseCode(), 200, "DE is not available");
			Thread.sleep(5000);
			Assert.assertEquals(makeDEActive(de_id).ResponseValidator.GetResponseCode(),200, "DE is not able to login");
			Thread.sleep(5000);
			for (String rule : rules) {
				System.out.println("--status--" + rule);
				Thread.sleep(15000);
				switch (rule) {
					case "updateaction":
						Assert.assertTrue(omsHelper.verifyOrders(orderId, "001",CastleBlackConstants.E2EUSER,CastleBlackConstants.E2EPASSWORD));
						omsHelper.changeOrderStatusToPlaced(orderId, CastleBlackConstants.E2EUSER,CastleBlackConstants.E2EPASSWORD);
						break;
					case OrderStatus.ASSIGNED:
						if (null == de_id) {
							//assignOrder(orderId);
							Assert.assertTrue(assignOrder(orderId), "order is not assigned to de");
						} else {
							assignOrder(orderId, de_id).ResponseValidator.GetResponseCode();
						}
						break;
					case OrderStatus.CONFIRMED:
						//zipDialConfirm(orderId);
						Thread.sleep(5000);
						Assert.assertTrue(zipDialConfirm(orderId), "order is not confirmed");
						break;
					case OrderStatus.ARRIVED:
						//zipDialArrived(orderId);
						Thread.sleep(5000);
						Assert.assertTrue(zipDialArrived(orderId), "order is not in arrived state");
						break;
					case OrderStatus.PICKEDUP:
						//zipDialPickedUp(orderId);
						Thread.sleep(15000);
						Assert.assertTrue(zipDialPickedUp(orderId), "order is not in pickedup state");
						break;
					case OrderStatus.REACHED:
						//zipDialReached(orderId);
						Thread.sleep(15000);
						Assert.assertTrue(zipDialReached(orderId), "order is not in reached state");
						break;
					case OrderStatus.DELIVERED:
						Thread.sleep(15000);
						//zipDialDelivered(orderId);
						Assert.assertTrue(zipDialDelivered(orderId), "order is not in delivered state");
						break;
					default:
						break;
				}
			}
			String repsone1 = omsHelper.getOrderInfoFromOMS(orderId,CastleBlackConstants.E2EUSER,CastleBlackConstants.E2EPASSWORD);
			String currentStatus1 = JsonPath.read(repsone1, "$..status.placed_status").toString().
					replace("[", "").replace("]", "").replaceAll("\"", "");
			System.out.println("====>" + currentStatus1);
			Thread.sleep(5000);
			List<Map<String, Object>> l= delDB(orderId);
			System.out.println("==2"+l.get(0).get("delivered_time"));
			Assert.assertNotNull(l.get(0).get("delivered_time"), "Order is not in Delivered Status");
			Thread.sleep(5000);

			List<Map<String, Object>> ffStatus=deliveryStatus(orderId);
			String stat= (String) ffStatus.get(0).get("delivery_status");
			System.out.println(stat+" "+toStatus);
			//Assert.assertEquals(stat, toStatus, "Order is not in Delivered Status");
		}
		catch (Exception e)
		{
			String msg=e.getMessage();
			String stage= env.substring(env.length() - 1);
			castleBlackManager.insideCastleBlack(endToEndHelp.runId.s, EndToEndConstants.omsPageName, "http://oms-u-cuat-0"
					+stage+".swiggyops.de/get_order_from_id"+"/formatted", "500", "NOT OK");
			SlackMessenger.webHook1(">>>" + "*Environment*" + "--" + "`" + env + "`" + "\n" + "*Service Name*" + "--"
					+ "`" + EndToEndConstants.omsPageName + "`" + "\n" + "*Service URL*" + "--" + "`" + "http://oms-u-cuat-0"+stage
					+".swiggyops.de/get_order_from_id/"+orderId+"/formatted" + "`" + "\n" + "*Status Code*" + "--" + "`" + "500"
					+ "`" + "\n" + "*Request Type*" + "--" + "`" + "POST" + "`" + "\n" + "*Message*" + "--" + "`" + msg + "`"
					+ "\n" + "*Response*" + "--" + "```" + "Product is not in"+" "+toStatus+" "+"status"+"```" ,"200");
		}
		catch(AssertionError e)
		{
			String msg=e.getMessage();
			String stage= env.substring(env.length() - 1);
			System.out.println("--"+stage);
			castleBlackManager.insideCastleBlack(endToEndHelp.runId.s, EndToEndConstants.omsPageName, "http://oms-u-cuat-0"
					+stage+".swiggyops.de/get_order_from_id"+"/formatted", "500", "NOT OK");
			SlackMessenger.webHook1(">>>" + "*Environment*" + "--" + "`" + env + "`" + "\n" + "*Service Name*" + "--"
					+ "`" + EndToEndConstants.omsPageName + "`" + "\n" + "*Service URL*" + "--" + "`" + "http://oms-u-cuat-0"+stage
					+".swiggyops.de/get_order_from_id/"+orderId+"/formatted" + "`" + "\n" + "*Status Code*" + "--" + "`" + "500"
					+ "`" + "\n" + "*Request Type*" + "--" + "`" + "POST" + "`" + "\n" + "*Message*" + "--" + "`" + msg+ "`"
					+ "\n" + "*Response*" + "--" + "```" + "Product is not in"+" "+toStatus+" "+"status"+"```" ,"200");
		}
	}

	public String[] getRulesForExecution(String key){

		HashMap<String, String[]> ruleMap = new HashMap();

		ruleMap.put("unplaced:placed", new String[] {"updateaction"});
		ruleMap.put("unplaced:assigned", new String[] {"updateaction","assigned"});
		ruleMap.put("unplaced:confirmed", new String[] {"updateaction", "assigned", "confirmed"});
		ruleMap.put("unplaced:arrived", new String[] {"updateaction","assigned","confirmed", "arrived"});
		ruleMap.put("unplaced:pickedup", new String[] {"updateaction","assigned","confirmed", "arrived", "pickedup"});
		ruleMap.put("unplaced:reached", new String[] {"updateaction","assigned","confirmed", "arrived", "pickedup", "reached"});
		ruleMap.put("unplaced:delivered", new String[] {"updateaction", "assigned","confirmed", "arrived", "pickedup", "reached", "delivered"});

		ruleMap.put("with_de:assigned", new String[] {"updateaction","assigned"});
		ruleMap.put("with_de:confirmed", new String[] {"updateaction", "assigned", "confirmed"});
		ruleMap.put("with_de:arrived", new String[] {"updateaction","assigned","confirmed", "arrived"});
		ruleMap.put("with_de:pickedup", new String[] {"updateaction","assigned","confirmed", "arrived", "pickedup"});
		ruleMap.put("with_de:reached", new String[] {"updateaction","assigned","confirmed", "arrived", "pickedup", "reached"});
		ruleMap.put("with_de:delivered", new String[] {"updateaction", "assigned","confirmed", "arrived", "pickedup", "reached", "delivered"});

		ruleMap.put("parked:assigned", new String[] {"updateaction","assigned"});
		ruleMap.put("parked:confirmed", new String[] {"updateaction", "assigned", "confirmed"});
		ruleMap.put("parked:arrived", new String[] {"updateaction","assigned","confirmed", "arrived"});
		ruleMap.put("parked:pickedup", new String[] {"updateaction","assigned","confirmed", "arrived", "pickedup"});
		ruleMap.put("parked:reached", new String[] {"updateaction","assigned","confirmed", "arrived", "pickedup", "reached"});
		ruleMap.put("parked:delivered", new String[] {"updateaction", "assigned","confirmed", "arrived", "pickedup", "reached", "delivered"});
		
		

		ruleMap.put("assigned:assigned", new String[] {});
		ruleMap.put("assigned:confirmed", new String[] {"confirmed"});
		ruleMap.put("assigned:arrived", new String[]{"confirmed", "arrived"});
		ruleMap.put("assigned:pickedup", new String[]{"confirmed", "arrived", "pickedup"});
		ruleMap.put("assigned:reached", new String[]{"confirmed", "arrived", "pickedup", "reached"});
		ruleMap.put("assigned:delivered", new String[]{"confirmed", "arrived", "pickedup", "reached", "delivered"});

		ruleMap.put("confirmed:confirmed", new String[]{});
		ruleMap.put("confirmed:arrived", new String[]{"assigned","confirmed", "arrived"});
		ruleMap.put("confirmed:pickedup", new String[]{"assigned","confirmed", "arrived", "pickedup"});
		ruleMap.put("confirmed:reached", new String[]{"assigned","confirmed", "arrived", "pickedup", "reached"});
		ruleMap.put("confirmed:delivered", new String[]{"assigned","confirmed", "arrived", "pickedup", "reached", "delivered"});

		ruleMap.put("arrived:arrived", new String[]{});
		ruleMap.put("arrived:pickedup", new String[]{ "pickedup"});
		ruleMap.put("arrived:reached", new String[]{ "pickedup", "reached"});
		ruleMap.put("arrived:delivered", new String[]{ "pickedup", "reached", "delivered"});

		ruleMap.put("picedkup:pickedup", new String[]{});
		ruleMap.put("picedkup:reached", new String[]{ "reached"});
		ruleMap.put("picedkup:delivered", new String[]{ "reached", "delivered"});

		ruleMap.put("reached:reached", new String[]{});
		ruleMap.put("reached:delivered", new String[]{"delivered"});

		return ruleMap.get(key);
	}
	public Processor exotelConfirmed(String From, String To) {
		GameOfThronesService service = new GameOfThronesService(
				"deliveryservice", "Exotelorderconfirm", gameofthrones);
		
		HashMap<String, String> requestheaders= del.doubleheader();
		String[] payloadparams = new String[] {From,To};
		Processor processor = new Processor(service, requestheaders,
				payloadparams);
		return processor;

	}
	public Processor exotelArrived(String From,String To)
	{
		GameOfThronesService service = new GameOfThronesService(
				"deliveryservice", "Exotelorderarrived", gameofthrones);
		
		HashMap<String, String> requestheaders= del.doubleheader();
		String[] payloadparams = new String[] {From,To};
		Processor processor = new Processor(service, requestheaders,
				payloadparams);
		return processor;
	}
	public Processor exotelPickedup(String From,String To)
	{
		GameOfThronesService service = new GameOfThronesService(
				"deliveryservice", "Exotelorderpickedup", gameofthrones);
		
		HashMap<String, String> requestheaders= del.doubleheader();
		String[] payloadparams = new String[] {From,To};
		Processor processor = new Processor(service, requestheaders,
				payloadparams);
		return processor;
	}
	public Processor exotelReached(String From,String To)
	{
		GameOfThronesService service = new GameOfThronesService(
				"deliveryservice", "Exotelorderreached", gameofthrones);
		
		HashMap<String, String> requestheaders= del.doubleheader();
		String[] payloadparams = new String[] {From,To};
		Processor processor = new Processor(service, requestheaders,
				payloadparams);
		return processor;
	}
		
	public Processor exotelDelivered(String From,String To)
	{
		GameOfThronesService service = new GameOfThronesService(
				"deliveryservice", "Exotelorderdelivered", gameofthrones);
		
		HashMap<String, String> requestheaders= del.doubleheader();
		String[] payloadparams = new String[] {From,To};
		Processor processor = new Processor(service, requestheaders,
				payloadparams);
		return processor;
	}
	
	public Processor updateDeliveryRestaurantParams(String id,String batchingEnabled)
	{
		GameOfThronesService service = new GameOfThronesService(
				"deliveryservice", "RestParamsUpdate", gameofthrones);
		
		HashMap<String, String> requestheaders= del.doubleheader();
		String[] payloadparams = new String[] {id,batchingEnabled};
		Processor processor = new Processor(service, requestheaders,
				payloadparams);
		return processor;
	}


	public Processor getOrderStatus(String order_id) {
		GameOfThronesService service = new GameOfThronesService("deliveryservice","getorderstatus", gameofthrones);
		return new Processor(service, del.doubleheader(), new String[]{order_id});
	}

	public Processor serviceabilityCheckByLoc(String lat, String lng ) {
		GameOfThronesService service = new GameOfThronesService("deliverycerebro", "newlisting", gameofthrones);
		Processor processor = new Processor(service,
				del.doubleheader(), new String[] { lat, lng});
		return processor;

	}
	public Processor serviceabilityCheckByRest(String lat, String lng,String rest_id) {
		GameOfThronesService service = new GameOfThronesService("deliverycerebro", "newlisting", gameofthrones);
		Processor processor = new Processor(service,
				del.doubleheader(), new String[] { lat, lng, rest_id });
		return processor;

	}
	public Processor serviceabilityCheckC(String lat, String lng, String restID,String  area_id) {
		GameOfThronesService service = new GameOfThronesService("deliverycerebro", "newcart", gameofthrones);
		Processor processor = new Processor(service,
				del.doubleheader(), new String[] { lat+","+lng, restID, area_id });
		return processor;

	}

	public Processor markDEInactive(String de_id,String enabled)
	{
		GameOfThronesService service = new GameOfThronesService(
				DeliveryConstant.delivery_Service, "DEinactive", gameofthrones);
		HashMap<String, String> requestheaders = del.doubleheader();
		String[] payloadparams = new String[] {de_id,enabled};
		Processor processor = new Processor(service, requestheaders,
				payloadparams);
		return processor;

	}

	public Processor getOtp(String de_id) {
		GameOfThronesService service = new GameOfThronesService(
				DeliveryConstant.delivery_Service, "getdeotp", gameofthrones);
		HashMap<String, String> requestheaders = del.doubleheader();
		return new Processor(service, requestheaders, null, new String[]{de_id});
	}

	public Integer getOtp(String de_id, String appversion) {
		GameOfThronesService service = new GameOfThronesService(
				DeliveryConstant.delivery_Service, "getdeotp", gameofthrones);
		HashMap<String, String> requestheaders = del.doubleheader();
		Processor processor= new Processor(service, requestheaders, null, new String[]{de_id});
		System.out.println("Printing from response"+processor.ResponseValidator.GetNodeValueAsInt("data"));
		return processor.ResponseValidator.GetNodeValueAsInt("data");
	}
	public Processor delogin(String de_id, String version) {
		GameOfThronesService service = new GameOfThronesService(
				"deliveryservice", "delogin", gameofthrones);
		HashMap<String, String> headers = del.formdataheaders();
		HashMap<String, String> formcontent = new HashMap<String, String>();
		headers.put("id", de_id);
		headers.put("version", version);
		Processor response = new Processor(service, headers, null, null,
				formcontent);
		return response;

	}

	public Processor deotp(String de_id, String otp) {
		GameOfThronesService service = new GameOfThronesService(
				"deliveryservice", "deotp", gameofthrones);
		HashMap<String, String> headers = del.formdataheaders();
		HashMap<String, String> formcontent = new HashMap<String, String>();
		headers.put("id", de_id);
		headers.put("otp", otp);
		Processor response = new Processor(service, headers, null, null,
				formcontent);
		return response;
	}

	public String deotp(String de_id, String otp,String appversion) {
		GameOfThronesService service = new GameOfThronesService(
				"deliveryservice", "deotp", gameofthrones);
		HashMap<String, String> headers = del.formdataheaders();
		HashMap<String, String> formcontent = new HashMap<String, String>();
		headers.put("id", de_id);
		headers.put("otp", otp);
		Processor processor = new Processor(service, headers, null, null, formcontent);
		return processor.ResponseValidator.GetBodyAsText();
	}

	public Processor ordercancel(String order_id) {
		GameOfThronesService service = new GameOfThronesService(
				"deliveryservice", "deliverycancelorder", gameofthrones);
		HashMap<String, String> requestheaders = del.doubleheader();
		String[] payloadparams = new String[] {order_id};
		Processor processor = new Processor(service, requestheaders,
				payloadparams);
		return processor;
	}
	public Processor createLongDistancePolgyon(String latLongs,String enabled,String maxSla,String maxLastMile) {
		GameOfThronesService service = new GameOfThronesService(
				"deliveryservice", "addlongdistanepolygon", gameofthrones);
		HashMap<String, String> requestheaders = del.doubleheader();
		String[] payloadparams = new String[] {latLongs,enabled,maxSla,maxLastMile};
		Processor processor = new Processor(service, requestheaders,
				payloadparams);
		return processor;
	}

	public Processor assignOrderInProd(String order_id,String de_id) throws InterruptedException
	{
		String batch_id = null;
		HashMap<String, String> requestheaders = del.doubleheader();
		makeDEFreeInProd(de_id);
		makeDEActive(de_id);
		GameOfThronesService service = new GameOfThronesService(
				"deliveryservice", "assignorder", gameofthrones);
		String[] payloadparams = new String[] { batch_id,
				de_id };
		Processor processor = new Processor(service, requestheaders,
				payloadparams);
		return processor;
	}

	/**
	 * Process Order Till Delivered state in Prod
	 * @param orderId
	 * @param toStatus
	 * 
	 * @throws InterruptedException
	 */
	public void processOrderInDeliveryProd(String orderId, String toStatus,String de_id) throws InterruptedException ,IOException {
		OMSHelper omsHelper = new OMSHelper();
		String env= gameofthrones.EnvironmentDetails.setup.getEnvironmentUrl().substring(40,44);
		System.out.println("--"+env);
		try {
			String repsone = omsHelper.getOrderInfoFromOMSProd(orderId);
			String currentStatus = JsonPath.read(repsone, "$..status.placed_status").toString().
					replace("[", "").replace("]", "").replaceAll("\"", "");
			System.out.println("====" + currentStatus);
			Assert.assertEquals(currentStatus, "unplaced","Status from OMS is null");
			boolean verificationRequired = JsonPath.read(repsone, "$..status.current_order_action").toString().
					replace("[", "").replace("]", "").equals("001");
			String[] rules = getRulesForExecution(currentStatus + ":" + toStatus);
			//Assert.assertNotNull(rules.length);

			makeDEFreeInProd(de_id);
			Thread.sleep(000);
			makeDEActive(de_id);
			Thread.sleep(000);
			for (String rule : rules) {
				System.out.println("--status--" + rule);
				Thread.sleep(000);
				switch (rule) {
					case "updateaction":
						omsHelper.verifyOrderPROD(orderId, "001"); //no db conn
						//omsHelper.changeOrderStatusToPlaced(orderId);
						break;
					case OrderStatus.ASSIGNED:
						orderAssignProd(orderId, de_id);
						break;
					case OrderStatus.CONFIRMED:
						Thread.sleep(10000);
						zipDialConfirm(orderId);
						break;
					case OrderStatus.ARRIVED:
						Thread.sleep(10000);
						zipDialArrived(orderId);
						break;
					case OrderStatus.PICKEDUP:
						Thread.sleep(10000);
					zipDialPickedUp(orderId);
					break;
					case OrderStatus.REACHED:
						Thread.sleep(10000);
						zipDialReached(orderId);
						break;
					case OrderStatus.DELIVERED:
						Thread.sleep(10000);
						zipDialDelivered(orderId);
						break;
					default:
						break;
				}
			}
			String repsone1 = omsHelper.getOrderInfoFromOMSProd(orderId);
			String currentStatus1 = JsonPath.read(repsone1, "$..status.placed_status").toString().
					replace("[", "").replace("]", "").replaceAll("\"", "");
			System.out.println("====>" + currentStatus1);
			String order_status = JsonPath.read(repsone1, "$.objects..status.order_status").toString().
					replace("[", "").replace("]", "").replaceAll("\"", "");
			Assert.assertEquals(order_status,toStatus, "Order is not in Delivered state");
			Thread.sleep(5000);
			/*List<Map<String, Object>> l= delDB(orderId);
			System.out.println("==2"+l.get(0).get("delivered_time"));
			Assert.assertNotNull(l.get(0).get("delivered_time"), "Order is not in Delivered Status");
			Thread.sleep(000);
*/
			/*List<Map<String, Object>> ffStatus=deliveryStatus(orderId);
			String stat= (String) ffStatus.get(0).get("delivery_status");
			System.out.println(stat+" "+toStatus);*/
			//Assert.assertEquals(stat, toStatus, "Order is not in Delivered Status");
		}

		catch (Exception e)
		{
			String msg="Order is not in delivered state";
			String stage= env.substring(env.length() - 1);
			System.out.println("--"+stage);
			System.out.println("---"+e);
			castleBlackManager.insideCastleBlack(endToEndHelp.runId.s, EndToEndConstants.omsPageName, "http://oms-u-cuat-0"+stage+".swiggyops.de/get_order_from_id"+"/formatted", "00", "NOT OK");
			SlackMessenger.webHook1(">>>" + "*Environment*" + "--" + "`" + env + "`" + "\n" + "*Service Name*" + "--" + "`" + EndToEndConstants.omsPageName + "`" + "\n" + "*Service URL*" + "--" + "`" + "oms-u-cuat-0"+stage+".swiggyops.de/get_order_from_id/"+orderId+"/formatted" + "`" + "\n" + "*Status Code*" + "--" + "`" + "00" + "`" + "\n" + "*Request Type*" + "--" + "`" + "POST" + "`" + "\n" + "*Message*" + "--" + "`" + msg + "`" + "\n" + "*Response*" + "--" + "```" + "Product is not in"+" "+toStatus+" "+"status"+"```" ,"200");

		}
		catch(AssertionError e)
		{
			String msg=e.getMessage();
			String stage= env.substring(env.length() - 1);
			System.out.println("--"+stage);
			castleBlackManager.insideCastleBlack(endToEndHelp.runId.s, EndToEndConstants.omsPageName, "http://oms-u-cuat-0"+stage+".swiggyops.de/get_order_from_id"+"/formatted", "00", "NOT OK");
			SlackMessenger.webHook1(">>>" + "*Environment*" + "--" + "`" + env + "`" + "\n" + "*Service Name*" + "--" + "`" + EndToEndConstants.omsPageName + "`" + "\n" + "*Service URL*" + "--" + "`" + "oms-u-cuat-0"+stage+".swiggyops.de/get_order_from_id/"+orderId+"/formatted" + "`" + "\n" + "*Status Code*" + "--" + "`" + "00" + "`" + "\n" + "*Request Type*" + "--" + "`" + "POST" + "`" + "\n" + "*Message*" + "--" + "`" + msg+ "`" + "\n" + "*Response*" + "--" + "```" + "Product is not in"+" "+toStatus+" "+"status"+"```" ,"200");
		}
	}

	public boolean isAreaPresent(int area_id) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName);
		List<Map<String, Object>> list = sqlTemplate.queryForList(DeliveryConstant.area_present + area_id);
		return list.size() > 0;
	}

	public boolean isCityPresent(int city_id) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName);
		List<Map<String, Object>> list = sqlTemplate.queryForList(DeliveryConstant.city_present + city_id);
		return list.size() > 0;
	}

	
	public Processor mergeorder(String order_id1,String order_id2) throws InterruptedException
	{
		String batch_id1 = null;
		String batch_id2 = null;
		String query1 = "Select * from trips where order_id=" + order_id1 + ";";
		String query2 = "Select * from trips where order_id=" + order_id2 + ";";
		Object obj1 = del.dbhelperget(query1, "batch_id");
		Object obj2 = del.dbhelperget(query2, "batch_id");
		if (obj1 == null && obj2== null) {
			System.out.println("********************** Batch Id not found **********************");
			return null;
		} else {
			batch_id1 = obj1.toString();
			batch_id2 = obj2.toString();
		}
		System.out.println("Batch_id of 1st Order : "+batch_id1);
		System.out.println("Batch_id of 2nd Order : "+batch_id2);
		GameOfThronesService service = new GameOfThronesService("deliveryservice", "mergeorders", gameofthrones);
		String[] payloadparams = new String[] { batch_id1,batch_id2 };
		Processor processor = new Processor(service, DeliveryConstant.headers(), payloadparams);
		return processor;
	}

	public Object verifyMergedOrder(String order_id1,String order_id2){
		String batch_id1 = null;
		String batch_id2 = null;
		String query1 = "Select * from trips where order_id=" + order_id1 + ";";
		String query2 = "Select * from trips where order_id=" + order_id2 + ";";
		Object obj1 = del.dbhelperget(query1, "batch_id");
		Object obj2 = del.dbhelperget(query2, "batch_id");
		if (obj1 == null && obj2== null) {
			System.out.println("********************** Batch Id not found **********************");
			return false;
		} else {
			batch_id1 = obj1.toString();
			batch_id2 = obj2.toString();
		}
		System.out.println("Batch_id of 1st Order : "+batch_id1);
		System.out.println("Batch_id of 2nd Order : "+batch_id2);
		GameOfThronesService service = new GameOfThronesService("deliveryservice", "mergeorders", gameofthrones);
		String[] payloadparams = new String[] { batch_id1,batch_id2 };
		Processor processor = new Processor(service, DeliveryConstant.headers(), payloadparams);

		String batch1 = del.dbhelperget(query1, "batch_id").toString();
		String batch2 = del.dbhelperget(query2, "batch_id").toString();
		if (batch1.equalsIgnoreCase(batch2)){
			System.out.println("********************** Both orders are successfully merged **********************");
			return batch1;
		}else {
			System.out.println("********************** Both orders are not merged **********************");
			return false;
		}
	}


	public Processor nextorder(String order_id, String delivery_boy_id)
			throws InterruptedException {
		String batch_id = null;
		HashMap<String, String> requestheaders = del.doubleheader();
		Thread.sleep(10000);
		String query = "Select * from trips where order_id=" + order_id + ";";
		Object obj = del.dbhelperget(query, "batch_id");
		
		if (null == obj)
		{
			return null;
		}
		else
		{
			 batch_id=obj.toString();
		}
		
		GameOfThronesService service = new GameOfThronesService(
				DeliveryConstant.delivery_Service, "nextOrder", gameofthrones);
		String[] payloadparams = new String[] { batch_id, delivery_boy_id };
		Processor processor = new Processor(service, requestheaders,
				payloadparams);
		return processor;
	}
	
	public Processor floatingcashChannel(String auth) {
		GameOfThronesService service = new GameOfThronesService(
				DeliveryConstant.delivery_Service, "floatingcash", gameofthrones);
		HashMap<String, String> requestheaders = del.singleheader();
		requestheaders.put("Authorization", auth);
		Processor processor = new Processor(service, requestheaders,
                null);
		return processor;

	}


	public Processor floatingdynamiccode(String auth, String channelid) {
		GameOfThronesService service = new GameOfThronesService(
				DeliveryConstant.delivery_Service, "floatingcashdynamicchannelid", gameofthrones);
		HashMap<String, String> requestheaders = del.doubleheader();
		requestheaders.put("Authorization", auth);
		Processor processor = new Processor(service, requestheaders,
				null,new String[] {channelid});
		return processor;

	}
	
	
	//*****************************************Food Issues Helper**************
    public Processor billCheck( String orderId, String inputValue, String inputType, String billProvided, String verifyWithVendor )
    {
        GameOfThronesService service = new GameOfThronesService(DeliveryConstant.delivery_Service, "billCheck", gameofthrones);
        String[] payloadparams = new String[] {orderId,inputValue,inputType,billProvided,verifyWithVendor};

         Processor processor= new Processor(service, requestheaders,payloadparams);
        return processor;
    }
    
    public Processor foodIssueAck( String orderId,String billProvided)
    {
        GameOfThronesService service = new GameOfThronesService(DeliveryConstant.delivery_Service, "foodIssueAck", gameofthrones);
        String[] payloadparams = new String[] {orderId,billProvided};

         Processor processor= new Processor(service, requestheaders,payloadparams);
        return processor;
    }
   

	public  Processor blockde(String de_id) throws InterruptedException {

		HashMap<String, String> requestheaders = del.doubleheader();

		GameOfThronesService service = new GameOfThronesService(
				"deliveryservice", "blockde", gameofthrones);
		String[] payloadparams = new String[] { de_id };
		Processor processor = new Processor(service, requestheaders,
				payloadparams);
		return processor;
	
			
		}
	
	public Processor unblockde(String de_id) throws InterruptedException {

		HashMap<String, String> requestheaders = del.doubleheader();

		GameOfThronesService service = new GameOfThronesService(
				"deliveryservice", "unblockde", gameofthrones);
		String[] payloadparams = new String[] { de_id };
		Processor processor = new Processor(service, requestheaders,
				payloadparams);
		return processor;
	
			
		}
	
	
	public Processor bankdetails(String auth) {
		GameOfThronesService service = new GameOfThronesService(
				DeliveryConstant.delivery_Service, "bankdetails", gameofthrones);
		HashMap<String, String> requestheaders = del.singleheader();
		requestheaders.put("Authorization", auth);
		Processor processor = new Processor(service, requestheaders,
                null);
		return processor;

	}
	
	
	
	public Processor startDuty(String auth) {
		GameOfThronesService service = new GameOfThronesService(
				DeliveryConstant.delivery_Service, "startduty", gameofthrones);
		HashMap<String, String> requestheaders = del.singleheader();
		requestheaders.put("Authorization", auth);
		String[] payloadparams = new String[] {};
		Processor processor = new Processor(service, requestheaders,
				payloadparams);
	return processor;

	}
	
	public Processor stopDuty(String auth) {
		GameOfThronesService service = new GameOfThronesService(
				DeliveryConstant.delivery_Service, "stopduty", gameofthrones);
		HashMap<String, String> requestheaders = del.singleheader();
		requestheaders.put("Authorization", auth);
		String[] payloadparams = new String[] {};
		Processor processor = new Processor(service, requestheaders,
				payloadparams);
		return processor;

	}
	
	public Processor getDeliveryboyFloatingcash(String auth) {
		GameOfThronesService service = new GameOfThronesService(
				DeliveryConstant.delivery_Service, "getFloatingofDe", gameofthrones);
		HashMap<String, String> requestheaders = del.singleheader();
		requestheaders.put("Authorization", auth);
		Processor processor = new Processor(service, requestheaders,
                null);
		return processor;
	}
	
	
	public Processor CheckDeisFree(String auth) {
		GameOfThronesService service = new GameOfThronesService(
				DeliveryConstant.delivery_Service, "DeFreecheck", gameofthrones);
		HashMap<String, String> requestheaders = del.singleheader();
		requestheaders.put("Authorization", auth);
		Processor processor = new Processor(service, requestheaders,
                null);
		return processor;
	}
	
	public Processor getdeliveryboylocation(String de_id) {
		GameOfThronesService service = new GameOfThronesService(
				DeliveryConstant.delivery_Service, "getdelocation", gameofthrones);
		HashMap<String, String> requestheaders = del.doubleheader();
		Processor processor = new Processor(service, requestheaders,
				null,new String[] {de_id});
		return processor;
	}
	
	
	public  Processor addzonetode(String de_id, String de_zone_map_id,String de_zone_id ) throws InterruptedException {

		HashMap<String, String> requestheaders = del.doubleheader();

		GameOfThronesService service = new GameOfThronesService(
				"deliveryservice", "updatedezone", gameofthrones);
		String[] payloadparams = new String[] { de_id, de_zone_map_id, de_zone_id};
		Processor processor = new Processor(service, requestheaders,
				payloadparams);
		return processor;
			
		}
	

	public  Processor deletezonemap(String de_zone_map_id ) throws InterruptedException {

		HashMap<String, String> requestheaders = del.doubleheader();
		GameOfThronesService service = new GameOfThronesService(
				"deliveryservice", "deletezone", gameofthrones);
		String[] payloadparams = new String[] { de_zone_map_id};
		Processor processor = new Processor(service, requestheaders,
				payloadparams);
		return processor;
			
		}
	
	
	public Processor getdeorderhistory(String auth, String date) {
		GameOfThronesService service = new GameOfThronesService(
				DeliveryConstant.delivery_Service, "orderhistory", gameofthrones);
		HashMap<String, String> requestheaders = del.doubleheader();
		requestheaders.put("Authorization", auth);
		Processor processor = new Processor(service, requestheaders,
				null,new String[] {date});
		return processor;

	}


	public Processor deloginhistory(String auth, String date1,String date2) {
		GameOfThronesService service = new GameOfThronesService(
				DeliveryConstant.delivery_Service, "loginhistory", gameofthrones);
		HashMap<String, String> requestheaders = del.doubleheader();
		requestheaders.put("Authorization", auth);
		Processor processor = new Processor(service, requestheaders,
				null,new String[] {date1,date2});
		return processor;

	}
public Processor Enablebatchresturant(String rest1, String rest2, String update_value) throws InterruptedException {
      GameOfThronesService service = new GameOfThronesService(
                        DeliveryConstant.delivery_Service, "enableBatchingforRestaurant", gameofthrones);
          HashMap<String, String> requestheaders = del.doubleheader();
           String[] payloadparams = new String[] { rest1, rest2, update_value };
                     Processor processor = new Processor(service, requestheaders,payloadparams);
                   return processor;

}



	
	public Processor deliveryboystatusupdate(String order_id, String Status, String bill,String pay,String collect ,String header)
			throws InterruptedException, JsonProcessingException {
		GameOfThronesService service = new GameOfThronesService(
				"deliveryservice", "deliveryboystatusupdate", gameofthrones);
	
		HashMap<String, String> requestheaders = del.doubleheader();
		requestheaders.put("Authorization", header);
		HashMap<String, String> headers = del.formdataheaders();
		HashMap<String, String> formcontent = new HashMap<String, String>();
		formcontent.put("order_id", order_id);
		formcontent.put("status", Status);
		HashMap<String, String>  formcontent1 = new HashMap<String, String>();
		formcontent1.put("bill", bill);
		formcontent1.put("pay",pay );
		formcontent1.put("collect", collect);
		formcontent1.put("order_id", order_id);
		String json = new ObjectMapper().writeValueAsString(formcontent1);
		formcontent.put("data",json);
      Processor response = new Processor(service, headers, null, null,formcontent);
		return response;
	}

//*********************loyalty************************************
	
	
	
	
	public Processor getPointdetailpage(String auth) {
		GameOfThronesService service = new GameOfThronesService(
				DeliveryConstant.delivery_Service, "pointdetailpage", gameofthrones);
		HashMap<String, String> requestheaders = del.singleheader();
		requestheaders.put("Authorization", auth);
		Processor processor = new Processor(service, requestheaders,
                null);
		return processor;

	}
	
	public Processor getRedeempoints(String auth) {
		GameOfThronesService service = new GameOfThronesService(
				DeliveryConstant.delivery_Service, "redeempoints", gameofthrones);
		HashMap<String, String> requestheaders = del.singleheader();
		requestheaders.put("Authorization", auth);
		Processor processor = new Processor(service, requestheaders,
                null);
		return processor;

	}
	

	public Processor getTotalpoints(String auth) {
		GameOfThronesService service = new GameOfThronesService(
				DeliveryConstant.delivery_Service, "getTotalpoints", gameofthrones);
		HashMap<String, String> requestheaders = del.singleheader();
		requestheaders.put("Authorization", auth);
		Processor processor = new Processor(service, requestheaders,
                null);
		return processor;

	}
	
	public Processor getVouchers(String auth) {
		GameOfThronesService service = new GameOfThronesService(
				DeliveryConstant.delivery_Service, "getVouchers", gameofthrones);
		HashMap<String, String> requestheaders = del.singleheader();
		requestheaders.put("Authorization", auth);
		Processor processor = new Processor(service, requestheaders,
               null);
		return processor;

	}
	

	public Processor getGif(String auth) {
		GameOfThronesService service = new GameOfThronesService(
				DeliveryConstant.delivery_Service, "getgif", gameofthrones);
		HashMap<String, String> requestheaders = del.singleheader();
		requestheaders.put("Authorization", auth);
		Processor processor = new Processor(service, requestheaders,
             null);
		return processor;

	}
	
	
	public void markDEActive(String de_id) {
		String query=DeliveryConstant.deActivequery+de_id;
		del.dbhelperupdate(query);

	}
	
	public void markDEInActive(String de_id) {
		String query=DeliveryConstant.deInactivequery+de_id;
		del.dbhelperupdate(query);

	}
	
	public  Processor incoming( String order_id,String collect) throws InterruptedException {
		HashMap<String, String> requestheaders = del.doubleheader();
		GameOfThronesService service = new GameOfThronesService(
				"deliveryservice", "incoming", gameofthrones);
		String[] payloadparams = new String[] {order_id,collect };
		Processor processor = new Processor(service, requestheaders,
				payloadparams);
		return processor;
		
		}
	public Processor Assign(String order_id) {
		String batch_id = null;
		HashMap<String, String> requestheaders = del.doubleheader();
		String query = "Select * from trips where order_id=" + order_id + ";";
		Object obj = del.dbhelperget(query, "batch_id");
		batch_id = obj.toString();
		makeDEActive(DeliveryConstant.snded_id);
		makeDEFree(DeliveryConstant.snded_id);
		GameOfThronesService service = new GameOfThronesService(
				DeliveryConstant.delivery_Service, "assignorder", gameofthrones);
		String[] payloadparams = new String[] { batch_id,
				DeliveryConstant.snded_id };
		Processor processor = new Processor(service, requestheaders,
				payloadparams);

			return processor;
			
		}

	public Processor assignOrderbyBatch(String batch_id,String de_id)
	{
		try {
			Thread.sleep(15000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		HashMap<String, String> requestheaders = del.doubleheader();
		makeDEFree(de_id);
		makeDEActive(de_id);
		//Thread.sleep(10000);
		GameOfThronesService service = new GameOfThronesService("deliveryservice", "assignorder", gameofthrones);
		String[] payloadparams = new String[] { batch_id, de_id };
		Processor processor = new Processor(service, requestheaders, payloadparams);
		System.out.println("Printing Assignment statuscode"+processor.ResponseValidator.GetResponseCode());
		System.out.println("Printing Assignment Resposne"+processor.ResponseValidator.GetBodyAsText());
		return processor;
	}

}


//write a method to poll db

//retry the maincode sfter every x seconds, if value is found break else,loop again



//write a method to do retries of an api till a certain code in body

//





