package com.swiggy.api.erp.cms.dp;

import com.swiggy.api.erp.cms.constants.Constants;
import com.swiggy.api.erp.cms.helper.TaggingHelper;
import com.swiggy.api.erp.cms.pojo.EntityTag.*;

import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.annotations.DataProvider;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class TaggingDP {
    JsonHelper jsonHelper= new JsonHelper();

    public String randomGenerator()
    {
        UUID rand = UUID.randomUUID();
        int id = Math.abs(rand.hashCode());
        String random=String.valueOf(id).substring(0,5);
        return random;
    }

    @DataProvider(name = "createTagCategory")
    public Object[][] createTagCategory() {
        return new Object[][]{{Constants.taggingTrue,"testTagCategory"+randomGenerator(), Constants.updateBy},
                {Constants.taggingFalse,"testTagCate"+randomGenerator(), Constants.updateBy}};}

    @DataProvider(name = "createTagCategoryNegative")
    public Object[][] createTagCategoryNegative() {
        return new Object[][]{{"","testTagCategory"+randomGenerator(), Constants.updateBy},
                {"null","testTagCate"+randomGenerator(), Constants.updateBy},
                {Constants.taggingTrue,"", Constants.updateBy},
                {Constants.taggingTrue,"testTagCate"+randomGenerator(), ""}};}

    @DataProvider(name = "updateTagCategory")
    public Object[][] updateTagCategory() {
        return new Object[][]{{Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy, "test"+randomGenerator()},
                {Constants.taggingFalse,"testTagCate"+randomGenerator(), Constants.updateBy,"test"+randomGenerator()}};
    }

    @DataProvider(name = "createTag")
    public Object[][] createTag() {
        return new Object[][]{{Constants.taggingTrue,"testTagCategory"+randomGenerator(), Constants.updateBy, "NorthIndian"}};}

    @DataProvider(name = "createTagNegative")
    public Object[][] createTagNegative() {
        return new Object[][]{{Constants.taggingTrue,"testTagCategory"+randomGenerator(), Constants.updateBy, ""}};}

    @DataProvider(name = "multipleTag")
    public Object[][] multipleTag() {
        return new Object[][]{{Constants.taggingTrue,"testTagCategory"+randomGenerator(), Constants.updateBy, "NorthIndian", "south"}};}

    @DataProvider(name = "createTagNoTagCategory")
    public Object[][] createTagNoTagCategory() {
        return new Object[][]{{Constants.taggingTrue,randomGenerator(), Constants.updateBy, "NorthIndian"}};}

    @DataProvider(name = "createTagFalse")
    public Object[][] createTagFalse() {
        return new Object[][]{{Constants.taggingFalse,"testTagCate"+randomGenerator(), Constants.updateBy, "punjabi"}};}

    @DataProvider(name = "createTagCategoryTrue")
    public Object[][] createTagCategoryTrue() {
        return new Object[][]{{Constants.taggingTrue,"testTagCate"+randomGenerator(), Constants.updateBy, Constants.taggingFalse,"punjabi"}};}


    @DataProvider(name = "createTagCategoryFalse")
    public Object[][] createTagCategoryFalse() {
        return new Object[][]{{Constants.taggingFalse,"testTagCate"+randomGenerator(), Constants.updateBy, Constants.taggingTrue,"punjabi"}};}

    @DataProvider(name = "updateTag")
    public Object[][] updateTag() {
        return new Object[][]{{Constants.taggingTrue,"testTagCate"+randomGenerator(), Constants.updateBy, Constants.taggingTrue,"SouthIndian", "northIndian"}};}

    @DataProvider(name = "updateTag1")
    public Object[][] updateTag1() {
        return new Object[][]{{Constants.taggingTrue,"testTagCate"+randomGenerator(), Constants.updateBy, Constants.taggingFalse,"SouthIndian", "northIndian"}};}

    @DataProvider(name = "deleteTagNegative")
    public Object[][] deleteTagNegative() {
        return new Object[][]{{randomGenerator()}};}

    @DataProvider(name = "deleteTagCategoryNegative")
    public Object[][] deleteTagCategoryNegative() {
        return new Object[][]{{randomGenerator()}};}

    @DataProvider(name = "createTagConfig")
    public Object[][] createTagConfig() {
        return new Object[][]{{Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,"item","true","0"},{Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,"restaurant","true","0"}, {Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,"item","false","0"}, {Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,"restaurant","false","0"}};}

    @DataProvider(name = "createTagConfigTag")
    public Object[][] createTagConfigTag() {
        return new Object[][]{{Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,"item","true","0","NorthIndian", "south"},{Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,"restaurant","true","0", "NorthIndian", "south"}, {Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,"item","false","0", "NorthIndian", "south"}, {Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,"restaurant","false","0", "NorthIndian", "south"}};}

    @DataProvider(name = "createTagConfigSingleTag")
    public Object[][] createTagConfigSingleTag() {
        return new Object[][]{{Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,"item","true","0","NorthIndian"},{Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,"restaurant","true","0", "NorthIndian"}, {Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,"item","false","0", "SouthIndian"}, {Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,"restaurant","false","0", "Indian"}};}

    @DataProvider(name = "createTagConfigItem")
    public Object[][] createTagConfigItem() {
        return new Object[][]{{Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityItem,Constants.taggingFalse,"0","NorthIndian", "south"}, {Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityItem,Constants.taggingTrue,"0", "NorthIndian", "south"} };}

    @DataProvider(name = "updateTagConfig")
    public Object[][] updateTagConfig() {
        return new Object[][]{{Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityItem,Constants.taggingTrue,Constants.taggingFalse,"0"},{Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityRest,Constants.taggingTrue,Constants.taggingFalse,"0"}};}

    @DataProvider(name = "updateTagConfigEntityType")
    public Object[][] updateTagConfigEntityType() {
        return new Object[][]{{Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityItem,Constants.taggingTrue,Constants.entityRest,"0"},{Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityRest,Constants.taggingTrue,Constants.entityItem,"0"}};}


    @DataProvider(name = "createEntityTagMap")
    public Object[][] createEntityTagMap() {
        return new Object[][]{{Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityItem,Constants.taggingTrue,"0","NorthIndian", "south", "primary", "212",randomGenerator()},
                {Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityItem,Constants.taggingTrue,"0", "NorthIndian", "south","secondary", "212",randomGenerator()},
                {Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityItem,Constants.taggingFalse,"0", "NorthIndian", "punjabi","other", "212",randomGenerator()},
                };}

    @DataProvider(name = "createEntityTagMapRest")
    public Object[][] createEntityTagMapRest() {
        return new Object[][]{{Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityRest,Constants.taggingTrue,"0","NorthIndian", "south", "primary", "212",randomGenerator()},
                {Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityRest,Constants.taggingTrue,"0", "NorthIndian", "south","secondary", "212",randomGenerator()},
                {Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityRest,Constants.taggingFalse,"0", "NorthIndian", "punjabi","other", "212",randomGenerator()},
        };}

    @DataProvider(name = "updateEntityTagMapCategory")
    public Object[][] updateEntityTagMapCategory() {
        return new Object[][]{{Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityRest,Constants.taggingTrue,"0","NorthIndian", "south", "primary", "212",randomGenerator(),"secondary"},
                {Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityRest,Constants.taggingTrue,"0", "NorthIndian", "south","secondary", "212",randomGenerator(),"primary"},
                {Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityRest,Constants.taggingFalse,"0", "NorthIndian", "punjabi","other", "212",randomGenerator(),"primary"},
                {Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityRest,Constants.taggingTrue,"0","NorthIndian", "south", "primary", "212",randomGenerator(),"other"},
        {Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityRest,Constants.taggingTrue,"0", "NorthIndian", "south","secondary", "212",randomGenerator(),"other"},
        {Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityRest,Constants.taggingFalse,"0", "NorthIndian", "punjabi","other", "212",randomGenerator(),"secondary"}
    };}

    @DataProvider(name = "updateEntityTagMapRest")
    public Object[][] updateEntityTagMapRest() {
        return new Object[][]{{Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityRest,Constants.taggingTrue,"0","NorthIndian", "south", "primary", "212",randomGenerator(),Constants.entityItem},
                {Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityRest,Constants.taggingTrue,"0", "NorthIndian", "south","secondary", "212",randomGenerator(),Constants.entityItem},
                {Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityRest,Constants.taggingFalse,"0", "NorthIndian", "punjabi","other", "212",randomGenerator(),Constants.entityItem}
                };}

    @DataProvider(name = "updateEntityTagMapItem")
    public Object[][] updateEntityTagMapItem() {
        return new Object[][]{{Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityRest,Constants.taggingTrue,"0","NorthIndian", "south", "primary", "212",randomGenerator(),Constants.entityItem},
                {Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityRest,Constants.taggingTrue,"0", "NorthIndian", "south","secondary", "212",randomGenerator(),Constants.entityItem},
                {Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityRest,Constants.taggingFalse,"0", "NorthIndian", "punjabi","other", "212",randomGenerator(),Constants.entityItem}
        };}

    @DataProvider(name = "getEntityTagMapEntity")
    public Object[][] getEntityTagMapEntity() {
        return new Object[][]{{Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityRest,Constants.taggingTrue,"0","NorthIndian", "south", "primary", "212",randomGenerator()},
                {Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityRest,Constants.taggingTrue,"0", "NorthIndian", "south","secondary", "212",randomGenerator()},
                {Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityRest,Constants.taggingFalse,"0", "NorthIndian", "punjabi","other", "212",randomGenerator()},
                {Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityItem,Constants.taggingTrue,"0","NorthIndian", "south", "primary", "212",randomGenerator()},
                {Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityItem,Constants.taggingTrue,"0", "NorthIndian", "south","secondary", "212",randomGenerator()},
                {Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityItem,Constants.taggingFalse,"0", "NorthIndian", "punjabi","other", "212",randomGenerator()}

        };}

    @DataProvider(name = "getEntityTagMap")
    public Object[][] getEntityTagMap() {
        return new Object[][]{{Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityItem,Constants.taggingTrue,"0","NorthIndian", "south", "primary", "212",randomGenerator(), Constants.pageNO, Constants.rows},
                {Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityItem,Constants.taggingTrue,"0", "NorthIndian", "south","secondary", "212",randomGenerator(),Constants.pageNO, Constants.rows},
                {Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityItem,Constants.taggingFalse,"0", "NorthIndian", "punjabi","other", "212",randomGenerator(), Constants.pageNO, Constants.rows},
                {Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityRest,Constants.taggingTrue,"0","NorthIndian", "south", "primary", "212",randomGenerator(), Constants.pageNO, Constants.rows},
                {Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityRest,Constants.taggingTrue,"0", "NorthIndian", "south","secondary", "212",randomGenerator(),Constants.pageNO, Constants.rows},
                {Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityRest,Constants.taggingFalse,"0", "NorthIndian", "punjabi","other", "212",randomGenerator(), Constants.pageNO, Constants.rows}

        };}

    @DataProvider(name = "getEntit")
    public Object[][] getEntit() throws IOException{
        EntityTagMapBulk entityTagMapBulk= new EntityTagMapBulkUploadBuilder().buildAll();
        return new Object[][]{{jsonHelper.getObjectToJSON(entityTagMapBulk)}};
    }

    @DataProvider(name = "getEntityAdd")
    public Object[][] getEntityAdd() throws IOException{
        List<EntityTagMap> tagMap= new ArrayList<>();
        tagMap.add(new EntityTagMap(Constants.categoryP,212,Constants.entityItem,213,0,179,"jitendra"));
        EntityTagMapBulk entityTagMapBulk= new EntityTagMapBulkUploadBuilder()
                .entityTagMap(tagMap).buildEntityTagMap();
        return new Object[][]{{jsonHelper.getObjectToJSON(entityTagMapBulk)}};
    }


    @DataProvider(name = "getEntityAddSingleMultipleTag")
    public Object[][] getEntityAddSingleMultipleTag() throws IOException{
        TaggingHelper taggingHelper= new TaggingHelper();
        String tagId=taggingHelper.tagIdGeneration(Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityItem,Constants.taggingTrue,"0","NorthIndian", "south");
        List<EntityTagMap> tagMap= new ArrayList<>();

        tagMap.add(new EntityTagMap(Constants.categoryP,212,Constants.entityItem,213,0,Integer.parseInt(tagId),"jitender"));
        //tagMap.add(new EntityTagMap(Constants.categoryS,212,Constants.entityItem,217,0,181,"jitendra"));
        EntityTagMapBulk entityTagMapBulk= new EntityTagMapBulkUploadBuilder()
                .entityTagMap(tagMap)
                .buildEntityTagMap();
        return new Object[][]{{jsonHelper.getObjectToJSON(entityTagMapBulk)}};
    }


    @DataProvider(name = "getEntityAddMultipleTag")
    public Object[][] getEntityAddMultipleTag() throws IOException{
        TaggingHelper taggingHelper= new TaggingHelper();
        String tagId=taggingHelper.tagIdGeneration(Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityItem,Constants.taggingTrue,"0","NorthIndian", "south");
        String tagId1=taggingHelper.tagIdGeneration(Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityItem,Constants.taggingTrue,"0","NorthIndian", "south");

        List<EntityTagMap> tagMap= new ArrayList<>();

        tagMap.add(new EntityTagMap(Constants.categoryP,212,Constants.entityItem,213,0,Integer.parseInt(tagId),Constants.updateBy));
        tagMap.add(new EntityTagMap(Constants.categoryS,212,Constants.entityItem,217,0,Integer.parseInt(tagId1),Constants.updateBy));
        EntityTagMapBulk entityTagMapBulk= new EntityTagMapBulkUploadBuilder()
                .entityTagMap(tagMap)
                .buildEntityTagMap();
        return new Object[][]{{jsonHelper.getObjectToJSON(entityTagMapBulk)}};
    }

    @DataProvider(name = "getEntityAndDeleteMultipleTag")
    public Object[][] getEntityAndDeleteMultipleTag() throws IOException{
        TaggingHelper taggingHelper= new TaggingHelper();
        String tagId=taggingHelper.tagIdGeneration(Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityItem,Constants.taggingTrue,"0","NorthIndian", "south");
        String tagId1=taggingHelper.tagIdGeneration(Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityRest,Constants.taggingTrue,"0","NorthIndian", "south");

        List<EntityTagMap> tagMap= new ArrayList<>();
        List<DeleteById> deleteMap= new ArrayList<>();

        tagMap.add(new EntityTagMap(Constants.categoryP,212,Constants.entityItem,213,0,Integer.parseInt(tagId),Constants.updateBy));
        tagMap.add(new EntityTagMap(Constants.categoryS,212,Constants.entityRest,217,0,Integer.parseInt(tagId1),Constants.updateBy));
        deleteMap.add(new DeleteById(Constants.categoryP,212,Constants.entityItem,213,0,Integer.parseInt(tagId),Constants.updateBy));
        deleteMap.add(new DeleteById(Constants.categoryP,212,Constants.entityRest,217,0,Integer.parseInt(tagId),Constants.updateBy));
        EntityTagMapBulk entityTagMapBulk= new EntityTagMapBulkUploadBuilder()
                .entityTagMap(tagMap)
                .deleteById(deleteMap)
                .buildAll();
        return new Object[][]{{jsonHelper.getObjectToJSON(entityTagMapBulk)}};
    }

    @DataProvider(name = "getEntityAndDeleteMultipleOneTag")
    public Object[][] getEntityAndDeleteMultipleOneTag() throws IOException{
        TaggingHelper taggingHelper= new TaggingHelper();
        String tagId=taggingHelper.tagIdGeneration(Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityItem,Constants.taggingTrue,"0","NorthIndian", "south");
        String tagId1=taggingHelper.tagIdGeneration(Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityRest,Constants.taggingTrue,"0","NorthIndian", "south");

        List<EntityTagMap> tagMap= new ArrayList<>();
        List<DeleteById> deleteMap= new ArrayList<>();

        tagMap.add(new EntityTagMap(Constants.categoryP,212,Constants.entityItem,213,0,Integer.parseInt(tagId),Constants.updateBy));
        tagMap.add(new EntityTagMap(Constants.categoryS,212,Constants.entityRest,217,0,Integer.parseInt(tagId1),Constants.updateBy));
        deleteMap.add(new DeleteById(Constants.categoryP,212,Constants.entityItem,213,0,Integer.parseInt(tagId),Constants.updateBy));
        //deleteMap.add(new DeleteById(Constants.categoryP,212,Constants.entityRest,217,0,Integer.parseInt(tagId),Constants.updateBy));
        EntityTagMapBulk entityTagMapBulk= new EntityTagMapBulkUploadBuilder()
                .entityTagMap(tagMap)
                .deleteById(deleteMap)
                .buildAll();
        return new Object[][]{{jsonHelper.getObjectToJSON(entityTagMapBulk)}};
    }

    @DataProvider(name = "getEntityAndUpdateMultipleTag")
    public Object[][] getEntityAndUpdateMultipleTag() throws IOException{
        TaggingHelper taggingHelper= new TaggingHelper();
        String tagId=taggingHelper.tagIdGeneration(Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityItem,Constants.taggingTrue,"0","NorthIndian", "south");
        //String tagId1=taggingHelper.tagIdGeneration(Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityRest,Constants.taggingTrue,"0","NorthIndian", "south");

        List<EntityTagMap> tagMap= new ArrayList<>();
        List<DeleteById> deleteMap= new ArrayList<>();

        tagMap.add(new EntityTagMap(Constants.categoryP,212,Constants.entityItem,213,0,Integer.parseInt(tagId),Constants.updateBy));
        tagMap.add(new EntityTagMap(Constants.categoryS,212,Constants.entityItem,213,0,Integer.parseInt(tagId),Constants.updateBy));

        EntityTagMapBulk entityTagMapBulk= new EntityTagMapBulkUploadBuilder()
                .entityTagMap(tagMap)
                .buildEntityTagMap();
        return new Object[][]{{jsonHelper.getObjectToJSON(entityTagMapBulk)}};
    }

    @DataProvider(name = "getEntityAndUpdateAndDeleteMultipleTag")
    public Object[][] getEntityAndUpdateAndDeleteMultipleTag() throws IOException{
        TaggingHelper taggingHelper= new TaggingHelper();
        String tagId=taggingHelper.tagIdGeneration(Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityItem,Constants.taggingTrue,"0","NorthIndian", "south");
        String tagId1=taggingHelper.tagIdGeneration(Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityRest,Constants.taggingTrue,"0","NorthIndian", "south");

        List<EntityTagMap> tagMap= new ArrayList<>();
        List<DeleteById> deleteMap= new ArrayList<>();

        tagMap.add(new EntityTagMap(Constants.categoryP,212,Constants.entityItem,213,0,Integer.parseInt(tagId),Constants.updateBy));
        tagMap.add(new EntityTagMap(Constants.categoryS,212,Constants.entityItem,213,0,Integer.parseInt(tagId1),Constants.updateBy));
        deleteMap.add(new DeleteById(Constants.categoryP,212,Constants.entityItem,213,0,Integer.parseInt(tagId),Constants.updateBy));
        //deleteMap.add(new DeleteById(Constants.categoryP,212,Constants.entityItem,213,0,Integer.parseInt(tagId),Constants.updateBy));

        EntityTagMapBulk entityTagMapBulk= new EntityTagMapBulkUploadBuilder()
                .entityTagMap(tagMap)
                .deleteById(deleteMap)
                .buildEntityTagMap();
        return new Object[][]{{jsonHelper.getObjectToJSON(entityTagMapBulk)}};
    }

    @DataProvider(name = "getEntityAddMultiple")
    public Object[][] getEntityAddSingle() throws IOException{
        TaggingHelper taggingHelper= new TaggingHelper();
        String tagId=taggingHelper.tagIdGeneration(Constants.taggingTrue,"TestCategory"+randomGenerator(), Constants.updateBy,Constants.entityItem,Constants.taggingTrue,"0","NorthIndian", "south");
        List<EntityTagMap> tagMap= new ArrayList<>();

        tagMap.add(new EntityTagMap(Constants.categoryP,212,Constants.entityItem,213,0,Integer.parseInt(tagId),"jitendra"));
        //tagMap.add(new EntityTagMap(Constants.categoryS,212,Constants.entityItem,217,0,181,"jitendra"));
        EntityTagMapBulk entityTagMapBulk= new EntityTagMapBulkUploadBuilder()
                .entityTagMap(tagMap)
                .buildEntityTagMap();
        return new Object[][]{{jsonHelper.getObjectToJSON(entityTagMapBulk)}};
    }


    public String dateTime()
    {
        Date date = new Date();
        String modifiedDate= new SimpleDateFormat("yyyy-MM-dd").format(date);
        String formatTime = new SimpleDateFormat("HH:mm:ss").format(date);
        String DT=modifiedDate+"T"+formatTime+"Z";
        return DT;
    }

    public String extendedTime(int i)
    {
        Date date = new Date();
        String modifiedDate= new SimpleDateFormat("yyyy-MM-dd").format(date);
        LocalDateTime d = LocalDateTime.now();
        String dateFormat = DateTimeFormatter.ofPattern("HH:mm:ss").format(d.plusMinutes(i));
        String DT=modifiedDate+"T"+dateFormat+"Z";
        return DT;
    }


}
