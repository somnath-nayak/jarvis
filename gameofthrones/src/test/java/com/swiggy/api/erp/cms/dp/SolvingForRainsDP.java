package com.swiggy.api.erp.cms.dp;

import java.io.IOException;

import org.json.JSONException;
import org.testng.annotations.DataProvider;
import com.swiggy.api.erp.cms.helper.CMSHelper;

public class SolvingForRainsDP {
	CMSHelper cmsHelper = new CMSHelper();

	@DataProvider(name = "PopItemCreation")
	public Object[][] ItemCreation() throws IOException, JSONException {

		return new Object[][] { { System.getenv("rest_Id"), System.getenv("POP_USAGE_TYPE") }

		};

	}

	@DataProvider(name = "itemScheduleAreaCREATEData")
	public Object[][] createItemScheduledataByNewAreaSchedule() throws JSONException {

		return new Object[][] { { System.getenv("ItemId") } };
	}

	@DataProvider(name = "PopItem")
	public Object[][] itemCreationForAGivenRestaurant() throws IOException, JSONException {

		return new Object[][] { { "444","POP_ONLY","12.999865,77.615885"} };

	}


}
