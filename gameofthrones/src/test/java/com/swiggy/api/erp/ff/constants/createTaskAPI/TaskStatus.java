package com.swiggy.api.erp.ff.constants.createTaskAPI;

public enum TaskStatus {

    ACTIVE("active"),
    CANCELLED("cancelled"),
    COMPLETED("completed"),
    FAILED("failed"),
    REJECTED("rejected");

    private final String status;

    TaskStatus(String status) {
        this.status = status;
    }

    public String toString() {
        return this.status;
    }
}
