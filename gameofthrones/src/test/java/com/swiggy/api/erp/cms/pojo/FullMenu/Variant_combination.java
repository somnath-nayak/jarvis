package com.swiggy.api.erp.cms.pojo.FullMenu;

import com.swiggy.api.erp.cms.constants.MenuConstants;

/**
 * Created by kiran.j on 2/2/18.
 */
public class Variant_combination
{
    private String variant_group_id;

    private String variant_id;

    public String getVariant_group_id ()
    {
        return variant_group_id;
    }

    public void setVariant_group_id (String variant_group_id)
    {
        this.variant_group_id = variant_group_id;
    }

    public String getVariant_id ()
    {
        return variant_id;
    }

    public void setVariant_id (String variant_id)
    {
        this.variant_id = variant_id;
    }

    public Variant_combination build() {
        setDefaultValues();
        return this;
    }

    public void setDefaultValues() {
        if(this.variant_group_id == null)
            this.setVariant_group_id(MenuConstants.variant_groups_id);
        if(this.variant_id == null)
            this.setVariant_id(MenuConstants.variants_id);
    }

    @Override
    public String toString()
    {
        return "{ variant_group_id = "+variant_group_id+", variant_id = "+variant_id+"}";
    }
}