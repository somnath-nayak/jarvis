package com.swiggy.api.erp.crm.tests.IntegrationServicesTests;

import java.util.ArrayList;

public class HelpcenterIntegrationRegression {

    public ArrayList<Object[]> fetchIssueDetails(String orderType, String dispositionType) throws Exception
    {
        ArrayList<Object[]> flowList = new ArrayList<Object[]>();
        String ffStatus = null, disposition = null, sla = null;
        String[] deliveryStatus = null;
        String[] dispositionList = new String[0];
        String[] validFlows = null;
        String restaurant_type = "TAKEAWAY";

        if (dispositionType.equalsIgnoreCase("mockFlows")) {
            dispositionList = new String[]
                    {
                            "ChangedMyMindOrderNotPlacedNotOnTrack",
                            "ChangedMyMindOrderNotPlacedOnTrack",
                            "ChangedMyMindOrderPlacedNotOnTrack",
                            "ChangedMyMindOrderPlacedOnTrack",
                            "ChangedMyMindOrderPickedupOnTrack",
                            "ChangedMyMindOrderPickedupNotOnTrack",
                            "IncorrectAddressOrderPlacedNotOnTrack",
                            "IncorrectAddressOrderNotPlacedNotOnTrack",
                            "IncorrectAddressOrderNotPlacedOnTrack",
                            "IncorrectAddressOrderPlacedOnTrack",
                            "IncorrectAddressOrderPickedupNotOnTrack",
                            "IncorrectAddressOrderPickedupOnTrack",
                            "PlacedByMistakeOrderPlacedNotOnTrack",
                            "PlacedByMistakeOrderPlacedOnTrack",
                            "PlacedByMistakeOrderNotPlacedNotOnTrack",
                            "PlacedByMistakeOrderNotPlacedOnTrack",
                            "PlacedByMistakeOrderPickedupNotOnTrack",
                            "PlacedByMistakeOrderPickedupOnTrack",
                            "OrderDelayedOrderPlacedNotOnTrack",
                            "OrderDelayedOrderNotPlacedNotOnTrack",
                            "OrderDelayedOrderPickedupNotOnTrack",
                            "ExpectedFasterOrderPlacedOnTrack",
                            "ExpectedFasterOrderNotPlacedOnTrack",
                            "ExpectedFasterOrderPickedupOnTrack"
                    };
        } else if (dispositionType.equalsIgnoreCase("integrationFlows")) {
            dispositionList = new String[]{

                    "PlacedByMistakeOrderPlacedOnTrack",
                    "ChangedMyMindOrderPlacedOnTrack",
                    "IncorrectAddressOrderPlacedOnTrack",
                    "ExpectedFasterOrderPlacedOnTrack",

                    "IncorrectAddressOrderPickedupOnTrack",
                    "ChangedMyMindOrderPickedupOnTrack",
                    "PlacedByMistakeOrderPickedupOnTrack",
                    "ExpectedFasterOrderPickedupOnTrack"
                    };
        }
        int dispositionSize = dispositionList.length;
        for (int k=0;k<dispositionSize;k++) {
            if (dispositionList[k].equalsIgnoreCase("PlacedByMistakeOrderPlacedOnTrack")) {
                validFlows = new String[]{"flow115", "flow116", "flow117", "flow118",
                        "flow119", "flow120", "flow121", "flow122"};
                ffStatus = "placed";
                sla = "onTrack";
                disposition = dispositionList[k];
                deliveryStatus = new String[]{"assigned", "confirmed", "arrived"};
            } else if (dispositionList[k].equalsIgnoreCase("ChangedMyMindOrderPlacedOnTrack")) {
                validFlows = new String[]{"flow22", "flow23", "flow24", "flow25"};
                ffStatus = "placed";
                sla = "onTrack";
                disposition = dispositionList[k];
                deliveryStatus = new String[]{"assigned", "confirmed", "arrived", "pickedup", "reached"};
            } else if (dispositionList[k].equalsIgnoreCase("IncorrectAddressOrderPlacedOnTrack")) {
                validFlows = new String[]{"flow106", "flow107", "flow108", "flow109",
                        "flow110", "flow111", "flow112", "flow113", "flow114"};
                ffStatus = "placed";
                sla = "onTrack";
                disposition = dispositionList[k];
                deliveryStatus = new String[]{"assigned", "confirmed", "arrived"};
            } else if (dispositionList[k].equalsIgnoreCase("ExpectedFasterOrderPlacedOnTrack")) {
                validFlows = new String[]{"flow123", "flow124", "flow125", "flow126",
                        "flow127", "flow128", "flow129", "flow130"};
                ffStatus = "placed";
                sla = "onTrack";
                disposition = dispositionList[k];
                deliveryStatus = new String[]{"assigned", "confirmed", "arrived"};
            } else if (dispositionList[k].equalsIgnoreCase("IncorrectAddressOrderPickedupOnTrack")) {
                validFlows = new String[]{"flow131", "flow132", "flow133", "flow134",
                        "flow135", "flow136", "flow137", "flow138"};
                ffStatus = "placed";
                sla = "onTrack";
                disposition = dispositionList[k];
                deliveryStatus = new String[]{"pickedup"};
            } else if (dispositionList[k].equalsIgnoreCase("ChangedMyMindOrderPickedupOnTrack")) {
                validFlows = new String[]{"flow100", "flow101", "flow102"};
                ffStatus = "placed";
                sla = "onTrack";
                disposition = dispositionList[k];
                deliveryStatus = new String[]{"pickedup", "reached"};
            } else if (dispositionList[k].equalsIgnoreCase("PlacedByMistakeOrderPickedupOnTrack")) {
                validFlows = new String[]{"flow139", "flow140", "flow141", "flow142",
                        "flow143", "flow144", "flow145", "flow146"};
                ffStatus = "placed";
                sla = "onTrack";
                disposition = dispositionList[k];
                deliveryStatus = new String[]{"pickedup"};
            } else if (dispositionList[k].equalsIgnoreCase("ExpectedFasterOrderPickedupOnTrack")) {
                validFlows = new String[]{"flow147", "flow148", "flow149", "flow150",
                        "flow151", "flow152", "flow153", "flow154"};
                ffStatus = "placed";
                sla = "onTrack";
                disposition = dispositionList[k];
                deliveryStatus = new String[]{"pickedup"};
            }
        }
        return flowList;
    }

}
