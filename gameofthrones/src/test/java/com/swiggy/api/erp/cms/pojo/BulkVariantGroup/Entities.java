package com.swiggy.api.erp.cms.pojo.BulkVariantGroup;

import com.swiggy.api.erp.cms.constants.MenuConstants;

/**
 * Created by kiran.j on 2/21/18.
 */
public class Entities
{
    private String id;

    private int order;

    private String item_id;

    private String name;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public int getOrder ()
    {
        return order;
    }

    public void setOrder (int order)
    {
        this.order = order;
    }

    public String getItem_id ()
    {
        return item_id;
    }

    public void setItem_id (String item_id)
    {
        this.item_id = item_id;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public Entities build() {
        if(this.getId() == null)
            this.setId(MenuConstants.variants_id);
        if(this.getName() == null)
            this.setName(MenuConstants.variant_groups_name);
        if(this.getItem_id() == null)
            this.setItem_id(MenuConstants.variants_id);

        return this;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", order = "+order+", item_id = "+item_id+", name = "+name+"]";
    }
}
