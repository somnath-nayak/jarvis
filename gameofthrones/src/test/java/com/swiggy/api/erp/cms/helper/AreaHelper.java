package com.swiggy.api.erp.cms.helper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.rabbitmq.client.AMQP;
import com.swiggy.api.erp.cms.constants.AreaConstants;
import com.swiggy.api.erp.cms.constants.CmsConstants;
import com.swiggy.api.erp.cms.pojo.Area;
import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.DBHelper;
import framework.gameofthrones.Tyrion.JsonHelper;
import framework.gameofthrones.Tyrion.RabbitMQHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;
import org.apache.commons.io.FileUtils;
import org.json.JSONException;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by kiran.j on 3/7/18.
 */
public class AreaHelper {

    RabbitMQHelper rmqHelper = new RabbitMQHelper();
    Initialize gameofthrones = new Initialize();
    private static int max_counter = 0;


    public Area getDefaultCityPojo() {
        Area area = new Area();
        area.build();
        return area;
    }

    public String[] buildQuery(HashMap<String, Object> map) {
        String column = "(", value = "(";
        for(Map.Entry<String, Object> entry: map.entrySet()) {
            if(!entry.getKey().toString().equalsIgnoreCase("city")) {
                column += "`" + entry.getKey() + "`,";
                value += "'" + entry.getValue() + "',";
            }
        }
        column = column.substring(0, column.length()-1);
        value = value.substring(0, value.length()-1);
        column += ")";
        value += ")";
        System.out.println(column + value);
        return new String[]{column, value};
    }

    public HashMap<String, Object> getCityMap(Area city) {
        Gson gson = new GsonBuilder().create();
        String json = gson.toJson(city);
        HashMap<String,Object> map = new Gson().fromJson(json, HashMap.class);
        return map;
    }

    public int createArea(Area area) throws IOException {
        String[] query_params = buildQuery(getCityMap(area));
        int counter = 0;
        String query = "";
        JsonHelper jsonHelper = new JsonHelper();
        query = AreaConstants.insert_into + AreaConstants.area_table + query_params[0] +AreaConstants.values+ query_params[1];
        System.out.println("final query is "+query);
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.execute(query);
        int area_id = getAreaId(area.getName());
        area.setId(area_id);
        File file = new File("../Data/Payloads/JSON/createCityArea");
        String queue = FileUtils.readFileToString(file);
        queue = queue.replace("${action}", AreaConstants.insert).replace("${data}", jsonHelper.getObjectToJSON(area)).replace("${model}", AreaConstants.area);
        System.out.println(queue);
//        rmqHelper.pushMessageToExchange("oms", "swiggy.catalog_changes_exchange", new AMQP.BasicProperties().builder().contentType("application/json"), queue);
        createAreaSlots(area_id);
        while(!waitFoDelivery(area_id) && counter < max_counter) {
            rmqHelper.pushMessageToExchange("oms", "swiggy.catalog_changes_exchange", new AMQP.BasicProperties().builder().contentType("application/json"), queue);
            counter++;
        }
        return area_id;
    }

    public int getAreaId(String name) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(AreaConstants.area_id + name + AreaConstants.like_end);
        return (list.size() > 0) ? (int) (list.get(0).get(AreaConstants.id)) : 0;
    }

    public void createAreaSlots(int area_id) {
        String days[] = CmsConstants.days;
        for(String day: days)
            areaSlot(Integer.toString(area_id), CmsConstants.open_time, CmsConstants.close_time, day);
    }

    public Processor areaSlot(String area_id, String openTime, String closeTime, String day) {
        HashMap<String, String> requestheaders = requestHeaders();
        GameOfThronesService service = new GameOfThronesService("cmsrestaurantservice", "areaslots", gameofthrones);
        Processor processor = new Processor(service, requestheaders, new String[]{area_id, closeTime, day, openTime});
        return processor;
    }

    public HashMap<String, String> requestHeaders()
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        requestheaders.put("user_meta","{\"source\": \"CMS\", \"user\":\"cms-tester\"}");
        return requestheaders;
    }

    public void pushAreaUpdateEvent(String area_id) throws IOException, JSONException {
        Gson gson = new Gson();
        CMSHelper cmsHelper = new CMSHelper();
        JsonHelper jsonHelper = new JsonHelper();
        String json = gson.toJson(cmsHelper.getAreaDetails(area_id));
        JsonObject jsonObject = new JsonObject();
        jsonObject = gson.fromJson(json, JsonObject.class);
        jsonObject.addProperty("city", jsonObject.get("city_id").toString());
        json = gson.toJson(jsonObject);
        File file = new File("../Data/Payloads/JSON/createCityArea");
        String queue = FileUtils.readFileToString(file);
        queue = queue.replace("${action}", AreaConstants.update).replace("${data}", json).replace("${model}", AreaConstants.area_name);
        System.out.println(queue);
        rmqHelper.pushMessageToExchange("oms", "swiggy.catalog_changes_exchange", new AMQP.BasicProperties().builder().contentType("application/json"), queue);
    }

    public boolean waitFoDelivery(int area_id) {
        String query = DeliveryConstant.area_present + area_id;
        return DBHelper.pollDB(DeliveryConstant.dbname, query, "id", Integer.toString(area_id), 10, 600);
    }

}
