package com.swiggy.api.erp.ff.tests.fraudService;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.ff.POJO.*;
import com.swiggy.api.erp.ff.constants.FraudServiceConstants;
import com.swiggy.api.erp.ff.dp.FraudServiceDP.FraudServiceData;
import com.swiggy.api.erp.ff.helper.FraudServiceHelper;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import framework.gameofthrones.Tyrion.WireMockHelper;
import org.testng.Assert;
import org.testng.annotations.*;

import java.io.IOException;


public class CODcheck {

    CODCheckPayload ch;
    JsonHelper j = new JsonHelper();
    FraudServiceHelper fsh = new FraudServiceHelper();
    Processor processor;
    WireMockHelper wireMockHelper = new WireMockHelper();


    @BeforeSuite
    public void mockServerStart() throws InterruptedException {
        fsh.flushRedis();
        fsh.clearRedisKeys(FraudServiceConstants.CityId_1);
        fsh.clearRedisKeys(FraudServiceConstants.CityId_2);
        fsh.clearRedisKeys(FraudServiceConstants.CityId_3);
        fsh.clearRedisKeys(FraudServiceConstants.CityId_4);
        fsh.clearRedisKeys(FraudServiceConstants.CityId_5);
        fsh.clearRedisKeys(FraudServiceConstants.CityId_6);
        wireMockHelper.startMockServer(FraudServiceConstants.mockPort);
        fsh.AddCityConfigurations(FraudServiceConstants.CityId_5, FraudServiceConstants.startTime, FraudServiceConstants.endTime, FraudServiceConstants.Cityfee_300);
        fsh.AddCityConfigurations(FraudServiceConstants.CityId_6, FraudServiceConstants.startTime, FraudServiceConstants.endTime, FraudServiceConstants.Cityfee_0);
        fsh.AddToExceptionList(FraudServiceConstants.userID);
        fsh.AddToExceptionList(FraudServiceConstants.userID1);
        fsh.AddCityConfigurations(FraudServiceConstants.CityId_8, FraudServiceConstants.startTime_8,FraudServiceConstants.endTime_8 , FraudServiceConstants.Cityfee_8);
        fsh.AddCityConfigurations(FraudServiceConstants.CityId_9, FraudServiceConstants.startTime_9,FraudServiceConstants.endTime_9 , FraudServiceConstants.Cityfee_9);
        fsh.AddToExceptionList(FraudServiceConstants.userID1);
        String previousHour = fsh.getPreviousHour();
        fsh.AddCityConfigurations(FraudServiceConstants.CityId_7, FraudServiceConstants.startTime, previousHour, FraudServiceConstants.Cityfee_300);
        fsh.AddCityDefaultConfigurations(FraudServiceConstants.CityId_1, FraudServiceConstants.Cityfee_300);
        fsh.AddCityDefaultConfigurations(FraudServiceConstants.CityId_2, FraudServiceConstants.Cityfee_100);
        fsh.AddCityDefaultConfigurations(FraudServiceConstants.CityId_3, FraudServiceConstants.Cityfee_0);
        fsh.AddCityDefaultConfigurations(FraudServiceConstants.Default, FraudServiceConstants.Cityfee_Default);

    }


    @Test(dataProvider = "getCityConfiguration_basedTimeDataSet", dataProviderClass = FraudServiceData.class,description = "COD check incase when City Late night fee is applicable")
    public void codCheckfor_CityConfiguration_afterBeforeTime(String requestId, CustomerDetails customerDetails, SessionDetails sessionDetails, DeliveryDetails deliveryDetails, CartDetails cartDetails,boolean isCODCheck) throws IOException {


        ch = new CODCheckPayload(requestId, customerDetails, sessionDetails, deliveryDetails, cartDetails);
        String body = j.getObjectToJSON(ch);
        processor = fsh.getCODCheckServiceProcessor(body);
        fsh.statusCheck(processor);
        String response = processor.ResponseValidator.GetBodyAsText();

        Processor delivery = fsh.getMockDeliveryProcessor();
        Processor dsp = fsh.getMockDspProcessor();


        System.out.println("DeliveryResposne ======================"+delivery.ResponseValidator.GetBodyAsText());


        System.out.println("DSPResposne ======================"+dsp.ResponseValidator.GetBodyAsText());


        if(isCODCheck)
        {
            Assert.assertEquals(JsonPath.read(response, "$.cod_enabled").toString(), "true");
            Assert.assertEquals(JsonPath.read(response, "reason"), "cod_model_check");

        }
        else
        {
            Assert.assertEquals(JsonPath.read(response, "$.cod_enabled").toString(), "false");
            Assert.assertEquals(JsonPath.read(response, "reason"), "late_night_fee_check", "User have pending cancellation fee");

        }


    }

    @Test(dataProvider = "exclustionList", dataProviderClass = FraudServiceData.class,description = "COD check for Exclusion list")
    public void codCheckfor_exclustionList(String requestId, CustomerDetails customerDetails, SessionDetails sessionDetails, DeliveryDetails deliveryDetails, CartDetails cartDetails) throws IOException {
        String customerId = customerDetails.getCustomerId().toString();
        ch = new CODCheckPayload(requestId, customerDetails, sessionDetails, deliveryDetails, cartDetails);
        String body = j.getObjectToJSON(ch);
        processor = fsh.getCODCheckServiceProcessor(body);
        Processor delivery = fsh.getMockDeliveryProcessor();
        Processor dsp = fsh.getMockDspProcessor();

        System.out.println("DeliveryResposne ======================"+delivery.ResponseValidator.GetBodyAsText());


        System.out.println("DSPResposne ======================"+dsp.ResponseValidator.GetBodyAsText());

        fsh.statusCheck(processor);
        String response = processor.ResponseValidator.GetBodyAsText();
        if (customerId.equalsIgnoreCase(FraudServiceConstants.userID)) {
            Assert.assertEquals(JsonPath.read(response, "$.cod_enabled").toString(), "true");
            Assert.assertEquals(JsonPath.read(response, "reason"), "cod_exclusion_check");
        } else {
            Assert.assertEquals(JsonPath.read(response, "$.cod_enabled").toString(), "false");
            Assert.assertEquals(JsonPath.read(response, "reason"), "pending_cancellation_fee_check");
        }

    }

    @Test(dataProvider = "PendingCancellationFee", dataProviderClass = FraudServiceData.class,description = "COD check with Pending Cancellation Fee")
    public void codCheckfor_PendingCancellationFee(String requestId, CustomerDetails customerDetails, SessionDetails sessionDetails, DeliveryDetails deliveryDetails, CartDetails cartDetails) throws IOException {

        ch = new CODCheckPayload(requestId, customerDetails, sessionDetails, deliveryDetails, cartDetails);
        String body = j.getObjectToJSON(ch);
        processor = fsh.getCODCheckServiceProcessor(body);
        fsh.statusCheck(processor);


        Processor delivery = fsh.getMockDeliveryProcessor();
        Processor dsp = fsh.getMockDspProcessor();


        System.out.println("DeliveryResposne ======================"+delivery.ResponseValidator.GetBodyAsText());


        System.out.println("DSPResposne ======================"+dsp.ResponseValidator.GetBodyAsText());

        String response = processor.ResponseValidator.GetBodyAsText();
        int pendingCancellationFee = customerDetails.getPendingCancellationFee();
        double swiggyMoneyUsed = customerDetails.getSwiggyMoneyUsed();
        statusBasedOnPendingFeeCheck(pendingCancellationFee,swiggyMoneyUsed,response);
    }

    @Test(dataProvider = "CityConfigurations", dataProviderClass = FraudServiceData.class,description = "COD check incase when City Late night fee is applicable")
    public void codCheckfor_CityConfiguration(String requestId, CustomerDetails customerDetails, SessionDetails sessionDetails, DeliveryDetails deliveryDetails, CartDetails cartDetails) throws IOException {

        Processor delivery = fsh.getMockDeliveryProcessor();
        Processor dsp = fsh.getMockDspProcessor();

        System.out.println("DeliveryResposne ======================"+delivery.ResponseValidator.GetBodyAsText());


        System.out.println("DSPResposne ======================"+dsp.ResponseValidator.GetBodyAsText());



        ch = new CODCheckPayload(requestId, customerDetails, sessionDetails, deliveryDetails, cartDetails);
        String body = j.getObjectToJSON(ch);
        processor = fsh.getCODCheckServiceProcessor(body);
        fsh.statusCheck(processor);
        String response = processor.ResponseValidator.GetBodyAsText();

        String cityId = deliveryDetails.getCityId().toString();
        System.out.println("**********************"+cityId);
        int cartAmount = cartDetails.getBillAmount();

        stautsBasedOnCityCheck(cityId, cartAmount, response);


    }

    @Test(dataProvider = "PendingFeeCityConfigurations", dataProviderClass = FraudServiceData.class,description = "COD check when pending fee and City configuration both are applied")
    public void codCheckfor_PendingFeeAndCityConfiguration(String requestId, CustomerDetails customerDetails, SessionDetails sessionDetails, DeliveryDetails deliveryDetails, CartDetails cartDetails) throws IOException {

        ch = new CODCheckPayload(requestId, customerDetails, sessionDetails, deliveryDetails, cartDetails);
        String body = j.getObjectToJSON(ch);
        processor = fsh.getCODCheckServiceProcessor(body);
        fsh.statusCheck(processor);
        String response = processor.ResponseValidator.GetBodyAsText();

        String cityId = deliveryDetails.getCityId().toString();
        int cartAmount = cartDetails.getBillAmount();

        stautsBasedOnCityCheck(cityId, cartAmount, response);


    }

    @Test(dataProvider = "DSCheck", dataProviderClass = FraudServiceData.class,description = "COD check for DS True Conditions")
    public void codCheckfor_DSCheck_False(String requestId, CustomerDetails customerDetails, SessionDetails sessionDetails, DeliveryDetails deliveryDetails, CartDetails cartDetails) throws IOException, InterruptedException {


        fsh.mockDeliveryBoyResponse(FraudServiceConstants.statusCode);
        fsh.mockDSResponse(FraudServiceConstants.statusCode, FraudServiceConstants.predication_1,"1");



        Processor delivery = fsh.getMockDeliveryProcessor();
        Processor dsp = fsh.getMockDspProcessor();


        System.out.println("DeliveryResposne ======================"+delivery.ResponseValidator.GetBodyAsText());


        System.out.println("DSPResposne ======================"+dsp.ResponseValidator.GetBodyAsText());


        ch = new CODCheckPayload(requestId, customerDetails, sessionDetails, deliveryDetails, cartDetails);
        String body = j.getObjectToJSON(ch);
        processor = fsh.getCODCheckServiceProcessor(body);
        fsh.statusCheck(processor);
        String response = processor.ResponseValidator.GetBodyAsText();

        Assert.assertEquals(JsonPath.read(response, "$.cod_enabled").toString(), "false");
        Assert.assertEquals(JsonPath.read(response, "reason"), "cod_model_check");


    }

    @Test(dataProvider = "DSCheck", dataProviderClass = FraudServiceData.class,description = "COD check for DS false Condition")
    public void codCheckfor_DSCheck_True(String requestId, CustomerDetails customerDetails, SessionDetails sessionDetails, DeliveryDetails deliveryDetails, CartDetails cartDetails) throws IOException, InterruptedException {



        fsh.mockDeliveryBoyResponse(FraudServiceConstants.statusCode);
        fsh.mockDSResponse(FraudServiceConstants.statusCode, FraudServiceConstants.predication_0,"1");


        Processor delivery = fsh.getMockDeliveryProcessor();
        Processor dsp = fsh.getMockDspProcessor();


        System.out.println("DeliveryResposne ======================"+delivery.ResponseValidator.GetBodyAsText());


        System.out.println("DSPResposne ======================"+dsp.ResponseValidator.GetBodyAsText());



        ch = new CODCheckPayload(requestId, customerDetails, sessionDetails, deliveryDetails, cartDetails);
        String body = j.getObjectToJSON(ch);
        processor = fsh.getCODCheckServiceProcessor(body);
        fsh.statusCheck(processor);
        String response = processor.ResponseValidator.GetBodyAsText();

        Assert.assertEquals(JsonPath.read(response, "$.cod_enabled").toString(), "true");
        Assert.assertEquals(JsonPath.read(response, "reason"), "cod_model_check");

    }

    public void stautsBasedOnCityCheck(String cityId, int cartAmount, String response) {
        switch (cityId) {
            case FraudServiceConstants.CityId_5: {
                if (cartAmount > Integer.parseInt(FraudServiceConstants.Cityfee_300)) {
                    Assert.assertEquals(JsonPath.read(response, "$.cod_enabled").toString(), "false");
                    Assert.assertEquals(JsonPath.read(response, "reason"), "late_night_fee_check", "User have pending cancellation fee");

                } else {
                    Assert.assertEquals(JsonPath.read(response, "$.cod_enabled").toString(), "true");
                    Assert.assertEquals(JsonPath.read(response, "reason"), "cod_model_check", "User have pending cancellation fee");

                }
                break;
            }


            case FraudServiceConstants.CityId_6: {
                Assert.assertEquals(JsonPath.read(response, "$.cod_enabled").toString(), "false");
                Assert.assertEquals(JsonPath.read(response, "reason"), "late_night_fee_check", "User have pending cancellation fee");
                break;
            }
            default: {
                Assert.assertEquals(JsonPath.read(response, "$.cod_enabled").toString(), "true");
                Assert.assertEquals(JsonPath.read(response, "reason"), "cod_model_check", "User have pending cancellation fee");
                break;
            }

        }
    }

    public void statusBasedOnPendingFeeCheck(int pendingCancellationFee,double swiggyMoneyUsed,String response) {
        if (pendingCancellationFee <= 0) {
            Assert.assertEquals(JsonPath.read(response, "$.cod_enabled").toString(), "true","User have pending cancellation fee");
            Assert.assertEquals(JsonPath.read(response, "reason"), "cod_model_check","User have pending cancellation fee");

        } else {
            if (pendingCancellationFee > (int)swiggyMoneyUsed) {
                Assert.assertEquals(JsonPath.read(response, "$.cod_enabled").toString(), "false","user have more pending cancellation fee than swiggy money");
                Assert.assertEquals(JsonPath.read(response, "reason"), "pending_cancellation_fee_check");

            } else {
                Assert.assertEquals(JsonPath.read(response, "$.cod_enabled").toString(), "true","");
                Assert.assertEquals(JsonPath.read(response, "reason"), "cod_model_check","User have pending cancellation fee");

            }
        }

    }

    @AfterSuite
    public void mockServerStop() {
        wireMockHelper.stopMockServer();

    }

}


