package com.swiggy.api.erp.delivery.tests;

import com.swiggy.api.erp.delivery.helper.DeliveryDataHelper;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.erp.delivery.dp.CAPRDflowsDataProvider;
import com.swiggy.api.erp.delivery.helper.AutoassignHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryHelperMethods;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import framework.gameofthrones.JonSnow.Processor;

public class CAPRDflowsTest extends CAPRDflowsDataProvider {

	DeliveryServiceHelper helpdel = new DeliveryServiceHelper();
	AutoassignHelper hepauto = new AutoassignHelper();
	DeliveryHelperMethods delmeth = new DeliveryHelperMethods();
	CheckoutHelper checkhelp=new CheckoutHelper();
	DeliveryDataHelper deliveryDataHelper= new DeliveryDataHelper();
	public CAPRDflowsTest()
	{
		super(DeliveryConstant.caprdrestaurantid,DeliveryConstant.caprdrestaurantLat,DeliveryConstant.getCaprdrestaurantLon);
	}
		
	@BeforeTest
	public void datasetup()
	{
		//13168450$.data.addresses
		//String resp=SnDHelper.consumerLogin(DeliveryConstant.mobile, DeliveryConstant.password).ResponseValidator.GetBodyAsText();
		//String tid = JsonPath.read(resp, "$.tid").toString().replace("[", "").replace("]", "");
		//String token = JsonPath.read(resp, "$.data.token").toString().replace("[", "").replace("]", "");

		//checkhelp.invokegetAllAddressAPI();
		//delmeth.dbhelperupdate("update zone set is_open=1 where id=34");
		//delmeth.dbhelperupdate("update delivery_boys set status='FR' where id=2418");
		//delmeth.dbhelperupdate("update delivery_boys set batch_id=null where id=2418");

	}
	
	
	@Test(dataProvider = "QE-332", groups = "CAPRDregressionMayur", description = "Verify DE is able to Login to system")
	public void CAPRDDP332(String de_id, String version) {
		Processor processor = hepauto.delogin(de_id, version);
		String resp = processor.ResponseValidator.GetBodyAsText();
		if ((processor.ResponseValidator.GetResponseCode()) == 200) {
			Assert.assertEquals(JsonPath.read(resp, "$.statusCode").toString(),
					"0");
			Assert.assertEquals(JsonPath.read(resp, "$.statusMessage")
					.toString(), "OTP Sent");
		} else {
			Assert.assertTrue(false);
		}
	}

	@Test(dataProvider = "QE-333", groups = "CAPRDregressionMayur", description = "Verify DE is able to login to app with OTP recieved")
	public void CAPRDDP333(String de_id, String version)
			throws InterruptedException {
			hepauto.delogin(de_id, version);
		String deotp= null;
		try {
			deotp = (new JSONObject(helpdel.getOtp(de_id.toString()).ResponseValidator.GetBodyAsText())).get("data").toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		//String deotp = delmeth.redisconnectget(DeliveryConstant.de_otp, de_id,DeliveryConstant.redisdb);
		Processor processor = hepauto.deotp(de_id, deotp);
		String resp = processor.ResponseValidator.GetBodyAsText();
		if ((processor.ResponseValidator.GetResponseCode()) == 200) {
			Assert.assertEquals(JsonPath.read(resp, "$.statusCode").toString(),
					"0");
			Assert.assertEquals(JsonPath.read(resp, "$.statusMessage")
					.toString(), "OTP Valid");

		} else {
			Assert.assertTrue(false);
		}
	}

	@Test(dataProvider = "QE-299", groups = "CAPRDregressionMayur", description = "Verify order is assigned to DE if DE is active and free")
	public void CAPRDDP229(String order_id, String de_id, String version)
			throws InterruptedException {
		Assert.assertNotNull(order_id,"Order is not created through checkout helper");
		hepauto.delogin(de_id, version);
		String deotp= null;
		try {
			deotp = (new JSONObject(helpdel.getOtp(de_id.toString()).ResponseValidator.GetBodyAsText())).get("data").toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		//String deotp = delmeth.redisconnectget(DeliveryConstant.de_otp, de_id,DeliveryConstant.redisdb);
		hepauto.deotp(de_id, deotp);
		helpdel.makeDEFree(de_id);
		Thread.sleep(10000);
		Processor processor = helpdel.assignOrder(order_id, de_id);

		if ((processor.ResponseValidator.GetResponseCode()) == 200) {
			String query = "Select * from trips where order_id=" + order_id
					+ ";";
			Object assignedde = delmeth.dbhelperget(query, "de_id");
			if (null == assignedde.toString()
					|| !((assignedde.toString()).equalsIgnoreCase(de_id))) {
				Assert.assertTrue(false);
			}

			else {
				Assert.assertEquals(assignedde.toString(), de_id);
			}
		}
	}

	@Test(dataProvider = "QE-253", groups = "CAPRDregressionMayur", description = "Verify that DE is not able to login to system if DE status is not working with swiggy")
	public void CAPRDDP253(String de_id, String version)
			throws InterruptedException {
		Processor processor = hepauto.delogin(de_id, version);
		String resp = processor.ResponseValidator.GetBodyAsText();
		if ((processor.ResponseValidator.GetResponseCode()) == 403) {
			Assert.assertEquals(JsonPath.read(resp, "$.statusCode").toString(),
					"1");
			Assert.assertEquals(JsonPath.read(resp, "$.statusMessage")
					.toString(),
					"DE who is currently not working at Swiggy, is trying to login.");
		} else {
			Assert.assertTrue(false);
		}
	}

	@Test(dataProvider = "QE-300", groups = "CAPRDregressionMayur", description = "Verify DE is able to mark order confirmed once order is assigned to DE")
	public void CAPRDDP300(String order_id, String de_id, String version)
			throws InterruptedException {
		Assert.assertNotNull(order_id,"Order is not created through checkout helper");
		hepauto.delogin(de_id, version);
		String deotp= null;
		try {
			deotp = (new JSONObject(helpdel.getOtp(de_id.toString()).ResponseValidator.GetBodyAsText())).get("data").toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		//String deotp = delmeth.redisconnectget(DeliveryConstant.de_otp, de_id,DeliveryConstant.redisdb);
		hepauto.deotp(de_id, deotp);
		helpdel.makeDEFree(de_id);
		Thread.sleep(10000);
		helpdel.assignOrder(order_id, de_id);
		Thread.sleep(5000);
		boolean status = helpdel.zipDialConfirm(order_id);
		if (status == true) {
			String query = "Select * from trips where order_id=" + order_id
					+ ";";
			Object confirmedtime = delmeth.dbhelperget(query, "confirmed_time");
			if (null == confirmedtime.toString()) {
				Assert.assertTrue(false);
			}

			else {
				Assert.assertTrue(true);
			}
		}
	}

	@Test(dataProvider = "QE-301", groups = "CAPRDregressionMayur", description = "Verify DE is not able to mark confirmed if order is not assigned to DE")
	public void CAPRDDP301(String order_id, String de_id, String version)
			throws InterruptedException {
		Assert.assertNotNull(order_id,"Order is not created through checkout helper");
		hepauto.delogin(de_id, version);
		String deotp= null;
		try {
			deotp = (new JSONObject(helpdel.getOtp(de_id.toString()).ResponseValidator.GetBodyAsText())).get("data").toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		//String deotp = delmeth.redisconnectget(DeliveryConstant.de_otp, de_id,DeliveryConstant.redisdb);
		hepauto.deotp(de_id, deotp);
		helpdel.makeDEFree(de_id);
		Thread.sleep(10000);
		Processor processor = helpdel.zipDialConfirmDE(order_id);
		String resp = processor.ResponseValidator.GetBodyAsText();
		if ((processor.ResponseValidator.GetResponseCode()) == 200) {
			Assert.assertEquals(JsonPath.read(resp, "$.statusCode").toString(),
					"1");
			Assert.assertEquals(JsonPath.read(resp, "$.statusMessage")
					.toString(),
					"can  not update the state from null to confirmed");

		} else {
			Assert.assertTrue(false);
		}
	}

	@Test(dataProvider = "QE-302", groups = "CAPRDregressionMayur", description = "Verify DE is not able to mark order arrived/pickedup/reached/delivered if current order status is assigned")
	public void CAPRDDP302(String order_id, String state)
			throws InterruptedException {

		if (state.equalsIgnoreCase("arrived")) {
			Processor processor = helpdel.zipDialArrivedDE(order_id);
			String resp = processor.ResponseValidator.GetBodyAsText();
			if ((processor.ResponseValidator.GetResponseCode()) == 200) {
				Assert.assertEquals(JsonPath.read(resp, "$.statusCode")
						.toString(), "1");
				Assert.assertEquals(JsonPath.read(resp, "$.statusMessage")
						.toString(),
						"can  not update the state from assigned to arrived");

			} else {
				Assert.assertTrue(false);
			}
		} else if (state.equalsIgnoreCase("pickedup")) {
			Thread.sleep(10000);
			Processor processor = helpdel.zipDialPickedUpDE(order_id);
			String resp = processor.ResponseValidator.GetBodyAsText();
			if ((processor.ResponseValidator.GetResponseCode()) == 200) {
				Assert.assertEquals(JsonPath.read(resp, "$.statusCode")
						.toString(), "1");
				Assert.assertEquals(JsonPath.read(resp, "$.statusMessage")
						.toString(),
						"can  not update the state from assigned to pickedup");

			} else {
				Assert.assertTrue(false);
			}
		} else if (state.equalsIgnoreCase("reached")) {
			Thread.sleep(10000);
			Processor processor = helpdel.zipDialReachedDE(order_id);
			String resp = processor.ResponseValidator.GetBodyAsText();
			if ((processor.ResponseValidator.GetResponseCode()) == 200) {
				Assert.assertEquals(JsonPath.read(resp, "$.statusCode")
						.toString(), "1");
				Assert.assertEquals(JsonPath.read(resp, "$.statusMessage")
						.toString(),
						"can  not update the state from assigned to reached");

			} else {
				Assert.assertTrue(false);
			}
		} else if (state.equalsIgnoreCase("delivered")) {
			Thread.sleep(10000);
			Processor processor = helpdel.zipDialDeliveredDE(order_id);
			String resp = processor.ResponseValidator.GetBodyAsText();
			if ((processor.ResponseValidator.GetResponseCode()) == 200) {
				Assert.assertEquals(JsonPath.read(resp, "$.statusCode")
						.toString(), "1");
				Assert.assertEquals(JsonPath.read(resp, "$.statusMessage")
						.toString(),
						"can  not update the state from assigned to delivered");

			} else {
				Assert.assertTrue(false);
			}
		}
	}

	@Test(dataProvider = "QE-303", groups = "CAPRDregressionMayur", description = "Verify DE is able to mark order arrived once order is confirmed by DE")
	public void CAPRDDP303(String order_id, String de_id, String version)
			throws InterruptedException {
		Assert.assertNotNull(order_id,"Order is not created through checkout helper");
		hepauto.delogin(de_id, version);
		String deotp= null;
		try {
			deotp = (new JSONObject(helpdel.getOtp(de_id.toString()).ResponseValidator.GetBodyAsText())).get("data").toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		//String deotp = delmeth.redisconnectget(DeliveryConstant.de_otp, de_id,DeliveryConstant.redisdb);
		hepauto.deotp(de_id, deotp);
		helpdel.makeDEFree(de_id);
		Thread.sleep(10000);
		helpdel.assignOrder(order_id, de_id);
		Thread.sleep(10000);
		boolean b = helpdel.zipDialConfirm(order_id);
		if (b == true) {
			Thread.sleep(10000);
			Processor processor = helpdel.zipDialArrivedDE(order_id);
			String resp = processor.ResponseValidator.GetBodyAsText();
			if ((processor.ResponseValidator.GetResponseCode()) == 200) {
				Assert.assertEquals(JsonPath.read(resp, "$.statusCode")
						.toString(), "0");
				Assert.assertEquals(JsonPath.read(resp, "$.statusMessage")
						.toString(), "Successfully pushed to queue");
			}
		} else {
			Assert.assertTrue(false);
		}
	}

	@Test(dataProvider = "QE-304", groups = "CAPRDregressionMayur", description = "Verify DE is not able to mark order pickedup/reached/delivered if current order status is confirmed")
	public void CAPRDDP304(String order_id, String state)
			throws InterruptedException {
		Assert.assertNotNull(order_id,"Order is not created through checkout helper");
		if (state.equalsIgnoreCase("pickedup")) {
			Thread.sleep(10000);
			Processor processor = helpdel.zipDialPickedUpDE(order_id);
			String resp = processor.ResponseValidator.GetBodyAsText();
			if ((processor.ResponseValidator.GetResponseCode()) == 200) {
				Assert.assertEquals(JsonPath.read(resp, "$.statusCode")
						.toString(), "1");
				Assert.assertEquals(JsonPath.read(resp, "$.statusMessage")
						.toString(),
						"can  not update the state from confirmed to pickedup");

			} else {
				Assert.assertTrue(false);
			}
		} else if (state.equalsIgnoreCase("reached")) {
			Thread.sleep(10000);
			Processor processor = helpdel.zipDialReachedDE(order_id);
			String resp = processor.ResponseValidator.GetBodyAsText();
			if ((processor.ResponseValidator.GetResponseCode()) == 200) {
				Assert.assertEquals(JsonPath.read(resp, "$.statusCode")
						.toString(), "1");
				Assert.assertEquals(JsonPath.read(resp, "$.statusMessage")
						.toString(),
						"can  not update the state from confirmed to reached");

			} else {
				Assert.assertTrue(false);
			}
		} else if (state.equalsIgnoreCase("delivered")) {
			Thread.sleep(10000);
			Processor processor = helpdel.zipDialDeliveredDE(order_id);
			String resp = processor.ResponseValidator.GetBodyAsText();
			if ((processor.ResponseValidator.GetResponseCode()) == 200) {
				Assert.assertEquals(JsonPath.read(resp, "$.statusCode")
						.toString(), "1");
				Assert.assertEquals(JsonPath.read(resp, "$.statusMessage")
						.toString(),
						"can  not update the state from confirmed to delivered");

			} else {
				Assert.assertTrue(false);
			}
		}
	}

	@Test(dataProvider = "QE-305", groups = "CAPRDregressionMayur", description = "Verify DE is able to mark order pickedup once order is arrived by DE")
	public void CAPRDDP305(String order_id, String de_id, String version)
			throws InterruptedException {
		Assert.assertNotNull(order_id,"Order is not created through checkout helper");
		hepauto.delogin(de_id, version);
		String deotp= null;
		try {
			deotp = (new JSONObject(helpdel.getOtp(de_id.toString()).ResponseValidator.GetBodyAsText())).get("data").toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		//String deotp = delmeth.redisconnectget(DeliveryConstant.de_otp, de_id,DeliveryConstant.redisdb);
		hepauto.deotp(de_id, deotp);
		helpdel.makeDEFree(de_id);
		Thread.sleep(10000);
		helpdel.assignOrder(order_id, de_id);
		Thread.sleep(10000);
		helpdel.zipDialConfirm(order_id);
		Thread.sleep(10000);
		boolean b = helpdel.zipDialArrived(order_id);
		if (b == true) {
			Thread.sleep(10000);
			Processor processor = helpdel.zipDialPickedUpDE(order_id);
			String resp = processor.ResponseValidator.GetBodyAsText();
			if ((processor.ResponseValidator.GetResponseCode()) == 200) {
				Assert.assertEquals(JsonPath.read(resp, "$.statusCode")
						.toString(), "0");
				Assert.assertEquals(JsonPath.read(resp, "$.statusMessage")
						.toString(), "Successfully pushed to queue");
			}
		} else {
			Assert.assertTrue(false);
		}
	}

	@Test(dataProvider = "QE-306", groups = "CAPRDregressionMayur", description = "Verify DE is not able to mark order reached/delivered if current order status is arrived")
	public void CAPRDDP306(String order_id, String state)
			throws InterruptedException {
		Assert.assertNotNull(order_id,"Order is not created through checkout helper");
		if (state.equalsIgnoreCase("reached")) {
			Thread.sleep(10000);
			Processor processor = helpdel.zipDialReachedDE(order_id);
			String resp = processor.ResponseValidator.GetBodyAsText();
			if ((processor.ResponseValidator.GetResponseCode()) == 200) {
				Assert.assertEquals(JsonPath.read(resp, "$.statusCode")
						.toString(), "1");
				Assert.assertEquals(JsonPath.read(resp, "$.statusMessage")
						.toString(),
						"can  not update the state from confirmed to reached");

			} else {
				Assert.assertTrue(false);
			}
		} else if (state.equalsIgnoreCase("delivered")) {
			Thread.sleep(10000);
			Processor processor = helpdel.zipDialDeliveredDE(order_id);
			String resp = processor.ResponseValidator.GetBodyAsText();
			if ((processor.ResponseValidator.GetResponseCode()) == 200) {
				Assert.assertEquals(JsonPath.read(resp, "$.statusCode")
						.toString(), "1");
				Assert.assertEquals(JsonPath.read(resp, "$.statusMessage")
						.toString(),
						"can  not update the state from confirmed to delivered");

			} else {
				Assert.assertTrue(false);
			}
		}
	}

	@Test(dataProvider = "QE-307", groups = "CAPRDregressionMayur", description = "Verify DE is able to mark order reached once order is pickedup by DE")
	public void CAPRDDP307(String order_id, String de_id, String version)
			throws InterruptedException {
		Assert.assertNotNull(order_id,"Order is not created through checkout helper");
		hepauto.delogin(de_id, version);
		String deotp= null;
		try {
			deotp = (new JSONObject(helpdel.getOtp(de_id.toString()).ResponseValidator.GetBodyAsText())).get("data").toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		//String deotp = delmeth.redisconnectget(DeliveryConstant.de_otp, de_id,DeliveryConstant.redisdb);
		hepauto.deotp(de_id, deotp);
		helpdel.makeDEFree(de_id);
		Thread.sleep(10000);
		helpdel.assignOrder(order_id, de_id);
		Thread.sleep(10000);
		helpdel.zipDialConfirm(order_id);
		Thread.sleep(10000);
		helpdel.zipDialArrived(order_id);
		Thread.sleep(10000);
		boolean b = helpdel.zipDialPickedUp(order_id);
		if (b == true) {
			Thread.sleep(10000);
			Processor processor = helpdel.zipDialReachedDE(order_id);
			String resp = processor.ResponseValidator.GetBodyAsText();
			if ((processor.ResponseValidator.GetResponseCode()) == 200) {
				Assert.assertEquals(JsonPath.read(resp, "$.statusCode")
						.toString(), "0");
				Assert.assertEquals(JsonPath.read(resp, "$.statusMessage")
						.toString(), "Successfully pushed to queue");
			}
		} else {
			Assert.assertTrue(false);
		}
	}

	@Test(dataProvider = "QE-308", groups = "CAPRDregressionMayur", description = "Verify DE is able to mark order delivered if current order status is pickedup ")
	public void CAPRDDP308(String order_id, String state)
			throws InterruptedException {
		Assert.assertNotNull(order_id,"Order is not created through checkout helper");
		Thread.sleep(10000);
		Processor processor = helpdel.zipDialDeliveredDE(order_id);
		String resp = processor.ResponseValidator.GetBodyAsText();
		if ((processor.ResponseValidator.GetResponseCode()) == 200) {
			Assert.assertEquals(JsonPath.read(resp, "$.statusCode").toString(),
					"0");
			Assert.assertEquals(JsonPath.read(resp, "$.statusMessage")
					.toString(), "Successfully pushed to queue");

		} else {
			Assert.assertTrue(false);
		}
	}

	@Test(dataProvider = "QE-310", groups = "CAPRDregressionMayur", description = "Verify DE is able to mark order delivered if current order status is pickedup ")
	public void CAPRDDP310(String order_id, String de_id, String version)
			throws InterruptedException {
		Assert.assertNotNull(order_id,"Order is not created through checkout helper");
		hepauto.delogin(de_id, version);
		String deotp= null;
		try {
			deotp = (new JSONObject(helpdel.getOtp(de_id.toString()).ResponseValidator.GetBodyAsText())).get("data").toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		//String deotp = delmeth.redisconnectget(DeliveryConstant.de_otp, de_id,DeliveryConstant.redisdb);
		hepauto.deotp(de_id, deotp);
		helpdel.makeDEFree(de_id);
		Thread.sleep(10000);
		helpdel.assignOrder(order_id, de_id);
		Thread.sleep(10000);
		Processor processor = helpdel.systemRejectOrder(de_id, order_id);
		if ((processor.ResponseValidator.GetResponseCode()) == 200) {
			String query = "Select * from trips where order_id=" + order_id
					+ ";";
			Object assignedde = delmeth.dbhelperget(query, "de_id");
			if (null == assignedde
					|| !(assignedde.toString().equalsIgnoreCase(de_id))) {
				Assert.assertTrue(true);
			}
		} else {
			Assert.assertTrue(false);
		}
	}

	@Test(dataProvider = "QE-311", groups = "CAPRDregressionMayur", description = "Verify cancelled order is not assigned to DE ")
	public void cancellOrderIsNotAssignedToDe(String order_id, String de_id,
			String version) throws InterruptedException {
		Assert.assertNotNull(order_id,"Order is not created through checkout helper");
		hepauto.delogin(de_id, version);
		String deotp= null;
		try {
			deotp = (new JSONObject(helpdel.getOtp(de_id.toString()).ResponseValidator.GetBodyAsText())).get("data").toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		//String deotp = delmeth.redisconnectget(DeliveryConstant.de_otp, de_id,DeliveryConstant.redisdb);
		hepauto.deotp(de_id, deotp);
		helpdel.makeDEFree(de_id);
		Thread.sleep(10000);
		CheckoutHelper hel = new CheckoutHelper();
		hel.OrderCancel(DeliveryConstant.mobile, DeliveryConstant.password,
				order_id);
		Thread.sleep(10000);
		Processor processor = helpdel.assignOrder(order_id, de_id);
		String resp = processor.ResponseValidator.GetBodyAsText();
		System.out.println("resp=" + resp);
		if ((processor.ResponseValidator.GetResponseCode()) == 400) {
			Assert.assertEquals(JsonPath.read(resp, "$.statusCode").toString(),
					"1");
			Assert.assertEquals(JsonPath.read(resp, "$.statusMessage")
					.toString(), "Trying to assign cancelled order " + order_id);
		} else {
			Assert.assertTrue(false);
		}
	}
	@Test(dataProvider = "QE-312", groups = "CAPRDregressionMayur", description = "Verify delivered order is not assigned to DE")
	public void deliveredordernotgettingassigned(String order_id, String de_id,
			String version) throws InterruptedException {
		Assert.assertNotNull(order_id,"Order is not created through checkout helper");
		hepauto.delogin(de_id, version);
		String deotp= null;
		try {
			deotp = (new JSONObject(helpdel.getOtp(de_id.toString()).ResponseValidator.GetBodyAsText())).get("data").toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		//String deotp = delmeth.redisconnectget(DeliveryConstant.de_otp, de_id,DeliveryConstant.redisdb);
		hepauto.deotp(de_id, deotp);
		helpdel.makeDEFree(de_id);
		Thread.sleep(10000);
		helpdel.assignOrder(order_id, de_id);
		Thread.sleep(10000);
		helpdel.zipDialConfirm(order_id);
		Thread.sleep(10000);
		helpdel.zipDialArrived(order_id);
		Thread.sleep(10000);
		helpdel.zipDialPickedUp(order_id);
		Thread.sleep(10000);
		helpdel.zipDialReached(order_id);
		Thread.sleep(10000);
		helpdel.zipDialDelivered(order_id);
		Thread.sleep(10000);
		Processor processor = helpdel.assignOrder(order_id, de_id);
		String resp = processor.ResponseValidator.GetBodyAsText();
		System.out.println("resp=" + resp);
		if ((processor.ResponseValidator.GetResponseCode()) == 200) {
			Assert.assertEquals(JsonPath.read(resp, "$.statusCode").toString(),
					"1");
			Assert.assertEquals(JsonPath.read(resp, "$.statusMessage")
					.toString(), "Batch already assigned to " + de_id);
		} else {
			Assert.assertTrue(false);
		}
	}
	
}
