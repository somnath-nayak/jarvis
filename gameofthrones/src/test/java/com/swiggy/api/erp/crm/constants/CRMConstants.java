package com.swiggy.api.erp.crm.constants;

public final class CRMConstants {


	  public static final String
			  userMobile = "9871377578",
			  userPassword = "Ratan@100",
			  itemId = "55784",
			  quantity = "1",
			  restId = "475",
			  deID = "216",
			  userID = "393325",
			  userIDForCoupon = "393325",
			  regularPostPaidOrder = "11312145",
			  regularPrePaidOrder = "13290496081",
			  regularPostPaidOrderUnserviceable = "1970381",
			  regularPrePaidOrderUnServiceable = "1970383",
              orderManualRecommendation = "15709740000923",
	          redisTrackingScreen = "crmredis";

	  public  static final String
			ORDERISSUETYPE = "order",
  			GENERALISSUETYPE = "general",
		  	LEGALISSUETYPE = "legal",
		  	FAQISSUETYPE = "faq",
		  	SUPERFAQISSUETYPE = "superFaq",
		  	DASHORDERISSUETYPE = "dash_order";

	  public  static final String
			POSITIVETEST = "positive",
	  		NEGATIVETEST = "negative";

	  public static final String errorMessageHelpMultiTenant = "does not belong to any of the workflows for the tenant with identifier:";
// Dash Order Status Constant
	public static String
			PLACED=  "PLACED" ,
			CONFIRMED= "CONFIRMED",
			PAYMENTPENDING = "PAYMENT_PENDING",
			DELIVERYCREATED = "DELIVERY_CREATED",
			DELIVERYASSIGNED = "DELIVERY_ASSIGNED",
			DELIVERYCONFIRMED = "DELIVERY_CONFIRMED",
			DELIVERYPAY = "DELIVERY_PAY",
			ITEMCOMFIRMATIONREQUESTED = "ITEM_CONFIRMATION_REQUESTED",
			ITEMCONFIRMED = "ITEM_CONFIRMED",
			CARTCONFIRMATIONREQUESTED = "CART_CONFIRMATION_REQUESTED",
			CARTCONFIRMED = "CART_CONFIRMED",
			DELIVERYARRIVED = "DELIVERY_ARRIVED",
			DELIVERYPICKEDUP = "DELIVERY_PICKEDUP",
			DELIVERYREACHED = "DELIVERY_REACHED",
			DELIVERYDELIVERED = "DELIVERY_DELIVERED",
			CANCELLIED = "CANCELLED",
			FAILED = "FAILED",
			POSTDELIVERYSLOT = "SCHEDULED",
			PRESCHEDULECUTOFF = "SCHEDULED",
			POSTSCHEDULECUTOFF = "SCHEDULED",
			DELIVERYUNASSIGN = "DELIVERY_UNASSIGNED";

	public static final String
			DAILY_TENANT_ID = "1ab94827507ac1a979392b639a0e0d85f61cc20cd66a064ede8311c74e0011ec",
			SWIGGY_TENANT_ID = "eefdeac62db1ef01652053dd85f474ef626f3710277a63e77e109fc35cdaff6e";

	public static String
			HELPCENTERDB = "helpcenter",
			POST_ORDER_SERVICE_DB = "post_order_services",
			CHAT_MIDDLEWARE_DB = "swiggy_chat_middleware",
			CC_SERVICE_DB ="swiggy_cc_services",

			HP_CHECKOUT_SERVICE = "'checkout.service.url'",
			HP_DELIVERY_SERVICE = "'delivery.service.url'",
			HP_FF_SERVICE = "'fulfillment.service.url'",
			HP_VENDOR_SERVICE = "'vendor.service.url'",
			HP_DELIVERY_HEIMDALL = "'delivery.heimdall.service.url'",
			HP_FF_COD = "'fulfillment.cod.service.url'",

			CC_CHECKOUT_URL = "'checkout.service.url'",
			CC_FF_URL = "'ff.service.url'",
			CC_DATA_SCIENCE_URL = "'data.science.service.url'",
			CC_RNG_URL = "'rng.service.url'",
			CC_RNG_USER_SEG_URL = "'rng.user.segment.url'",

			MOCK_SERVER ="'http://10.0.6.224:9005'";





	// Cancellation
	public static final String
			Help = "1",
			OrderIssues = "2",
			GeneralQueries = "3",
            OrderToCancel = "5";


	public static final String

	//Customer Inittiated
			OrderToEditBeforeArrived="234",

				editOrderBeforeArrivedAddItems="235",

					editOrderBeforeArrivedAddItemsSelectItems="950",

						editOrderBeforeArrivedAddItemsAddNeedHelp="951",
							editOrderBeforeArrivedAddItemsAddNeedHelpContinueChat="953",
							editOrderBeforeArrivedAddItemsAddNeedHelpCallUs="954",

						editOrderBeforeArrivedAddItemsAddNoHelp="952",

				editOrderBeforeArrivedRemoveItems="236",

				editOrderBeforeArrivedReplaceItems="237",

				editOrderBeforeArrivedOthers="238",

					editOrderBeforeArrivedOthersNeedHelp="955",
						editOrderBeforeArrivedOthersNeedHelpContinueChat="957",
						editOrderBeforeArrivedOthersNeedHelpCallUs="958",

					editOrderBeforeArrivedOthersNoHelp="956",

			OrderToEditArrived="258",
				editOrderArrivedNeedHelp="262",
				editOrderArrivedNoHelp="263",

	//Restaurant Initiated

		restaurantInitiateedit="970",

			restaurantEditChooseAlternative="971",

					restaurantEditChooseAlternativeSelectItems="974",

						restaurantEditChooseAlternativeSelectItemsDeliverWithoutItem="987",
								restaurantEditChooseAlternativeSelectItemsDeliverWithoutItemConfirm="992",
									restaurantEditChooseAlternativeSelectItemsDeliverWithoutItemConfirmHelp="994",
										restaurantEditChooseAlternativeSelectItemsDeliverWithoutItemConfirmHelpChat="996",
										restaurantEditChooseAlternativeSelectItemsDeliverWithoutItemConfirmHelpCall="997",

									restaurantEditChooseAlternativeSelectItemsDeliverWithoutItemConfirmNoHelp="995",

								restaurantEditChooseAlternativeSelectItemsDeliverWithoutItemHelp="993",

						restaurantEditChooseAlternativeSelectItemsCancelOrder="988",

								restaurantEditChooseAlternativeSelectItemsCancelOrderYes="998",

									restaurantEditChooseAlternativeSelectItemsCancelOrderYesHelp="1000",
										restaurantEditChooseAlternativeSelectItemsCancelOrderYesChat="1002",
										restaurantEditChooseAlternativeSelectItemsCancelOrderYesCall="1003",

									restaurantEditChooseAlternativeSelectItemsCancelOrderYesNoHelp="1001",

								restaurantEditChooseAlternativeSelectItemsCancelOrderHelp="999",

						restaurantEditChooseAlternativeSelectItemsTalkToSomeone="989",

						restaurantEditChooseAlternativeSelectItemsNeedHelp="990",
							restaurantEditChooseAlternativeSelectItemsNeedHelpChat="1004",
							restaurantEditChooseAlternativeSelectItemsNeedHelpCall="1005",

						restaurantEditChooseAlternativeSelectItemsNoHelp="991",

			restaurantEditDeliverWithoutItems="972",

					restaurantEditDeliverWithoutItemsConfirm="975",

						restaurantEditDeliverWithoutItemsConfirmNeedHelp="977",
							restaurantEditDeliverWithoutItemsConfirmNeedHelpContinueChat="979",
							restaurantEditDeliverWithoutItemsConfirmNeedHelpCallUs="980",

						restaurantEditDeliverWithoutItemsConfirmNoHelp="978",

					restaurantEditDeliverWithoutItemsNeedHelp="976",


			restaurantEditCancelTheOrder="973",

				restaurantEditCancelTheOrderYesCancel="981",

					restaurantEditCancelTheOrderYesCancelNeedHelp="983",
						restaurantEditCancelTheOrderYesCancelNeedHelpContinueChat="985",
						restaurantEditCancelTheOrderYesCancelNeedHelpCallUs="986",

					restaurantEditCancelTheOrderYesCancelNoHelp="984",

				restaurantEditCancelTheOrderTalkToSomeone="982";


	public static final String
			ChangedMyMindOrderPlacedNotOnTrack = "6",
			ChangedMyMindOrderNotPlacedNotOnTrack = "66",
			ChangedMyMindOrderNotPlacedOnTrack = "126",
			ChangedMyMindOrderPlacedOnTrack = "162",
			ChangedMyMindOrderPickedupOnTrack = "550",
			ChangedMyMindOrderPickedupNotOnTrack = "551";

	public static final String
			IncorrectAddOrderPlacedNotOnTrack = "30",
			IncorrectAddOrderNotPlacedNotOnTrack = "90",
			IncorrectAddOrderNotPlacedOnTrack = "150",
			IncorrectAddOrderPlacedOnTrack = "186",
			IncorrectAddOrderPickedupOnTrack = "553",
			IncorrectAddOrderPickedupNotOnTrack = "552";

	public static final String
			PlacedByMistakeOrderPlacedNotOnTrack = "18",
			PlacedByMistakeOrderNotPlacedNotOnTrack = "78",
			PlacedByMistakeOrderNotPlacedOnTrack = "138",
			PlacedByMistakeOrderPlacedOnTrack = "174",
			PlacedByMistakeOrderPickedupNotOnTrack = "555",
			PlacedByMistakeOrderPickedupOnTrack = "556";

	public static final String
			OrderDelayedOrderPlacedNotOnTrack = "42",
			OrderDelayedOrderNotPlacedNotOnTrack = "102";

	public static final String
			ExpectedFasterOrderPlacedOnTrack = "54",
			ExpectedFasterOrderNotPlacedOnTrack = "114",
			ExpectedFasterOrderPickedupOnTrack = "557";

	// Changed my mind Not Placed and Not On Track
	public static final String
			YesCancelChangedMindNotPlacedNoTrack = "67",
			NoCancelChangedMindNotPlacedNoTrack = "68",
			NeedMoreHelpYesCancelChangedMindNotPlacedNoTrack = "69",
			NoDoneYesCancelChangedMindNotPlacedNoTrack = "70",
			YesNeedMoreHelpYesCancelChangedMindNotPlacedNoTrack = "74",
			NoNeedMoreHelpYesCancelChangedMindNotPlacedNoTrack = "75",
			CHATYesNeedMoreHelpYesCancelChangedMindNotPlacedNoTrack = "683",
			CALLYesNeedMoreHelpYesCancelChangedMindNotPlacedNoTrack = "684",
			YesNeedMoreHelpNoCancelChangedMindNotPlacedNoTrack = "76",
			NoDoneNoCancelChangedMindNotPlacedNoTrack = "77",
			CHATYesNeedMoreHelpNoCancelChangedMindNotPlacedNoTrack = "685",
			CALLYesNeedMoreHelpNoCancelChangedMindNotPlacedNoTrack = "686";

	// Changed my mind Not Placed and On Track
	public static final String
			YesCancelChangedMindNotPlacedOnTrack = "127",
			NoCancelChangedMindNotPlacedOnTrack = "128",
			NeedMoreHelpYesCancelChangedMindNotPlacedOnTrack = "129",
			NoDoneYesCancelChangedMindNotPlacedOnTrack = "130",
			YesNeedMoreHelpYesCancelChangedMindNotPlacedOnTrack = "134",
			NoNeedMoreHelpYesCancelChangedMindNotPlacedOnTrack = "135",
			CHATYesNeedMoreHelpYesCancelChangedMindNotPlacedOnTrack = "713",
			CALLYesNeedMoreHelpYesCancelChangedMindNotPlacedOnTrack = "714",
			YesNeedMoreHelpNoCancelChangedMindNotPlacedOnTrack = "136",
			NoDoneNoCancelChangedMindNotPlacedOnTrack = "137",
			CHATYesNeedMoreHelpNoCancelChangedMindNotPlacedOnTrack = "715",
			CALLYesNeedMoreHelpNoCancelChangedMindNotPlacedOnTrack = "716";

	// Changed my mind Placed and Not On Track
	public static final String
			YesCancelChangedMindPlacedNoTrack = "7",
			NoCancelChangedMindPlacedNoTrack = "8",
			NeedMoreHelpYesCancelChangedMindPlacedNoTrack = "9",
			NoDoneYesCancelChangedMindPlacedNoTrack = "10",
			YesNeedMoreHelpYesCancelChangedMindPlacedNoTrack = "14",
			NoNeedMoreHelpYesCancelChangedMindPlacedNoTrack = "15",
			CHATYesNeedMoreHelpYesCancelChangedMindPlacedNoTrack = "651",
			CALLYesNeedMoreHelpYesCancelChangedMindPlacedNoTrack = "652",
			YesNeedMoreHelpNoCancelChangedMindPlacedNoTrack = "16",
			NoDoneNoCancelChangedMindPlacedNoTrack = "17",
			CHATYesNeedMoreHelpNoCancelChangedMindPlacedNoTrack = "653",
			CALLYesNeedMoreHelpNoCancelChangedMindPlacedNoTrack = "654";

	// Changed my mind Placed and On Track
	public static final String
			YesCancelChangedMindPlacedOnTrack = "163",
			NeedMoreHelpYesCancelChangedMindPlacedOnTrack = "165",
			CancellationFeeYesCancelChangedMindPlacedOnTrack = "170",
			OtherIssueChangedMindPlacedOnTrack = "171",
			CHATCancellationFeeYesCancelChangedMindPlacedOnTrack = "731",
			CALLCancellationFeeYesCancelChangedMindPlacedOnTrack = "732",
			CHATOtherIssueYesCancelChangedMindPlacedOnTrack = "733",
			CALLOtherIssueYesCancelChangedMindPlacedOnTrack = "734";
	// NoCancelChangedMindPlacedOnTrack = "164", NoDoneYesCancelChangedMindPlacedOnTrack = "166",

	// Changed my mind Pickedup and On Track
	public static final String
			YesCancelChangedMindPickedupOnTrack = "558",
			YesCancelNoHelpChangedMindPickedupOnTrack = "561",
			DontCancelChangedMindPickedupOnTrack = "559",
			DontCancelNeedHelpChangedMindPickedupOnTrack = "567",
			DontCancelNeedHelpContinueChatChangedMindPickedupOnTrack = "761",
			DontCancelNoHelpChangedMindPickedupOnTrack = "568";



	// Changed my mind Pickedup and not on Track
	public static final String
			YesCancelChangedMindPickedupNotOnTrack = "569",
			YesCancelNoHelpChangedMindPickedupNotOnTrack = "572",
			DontCancelChangedMindPickedupNotOnTrack = "570",
			DontCancelNeedHelpChangedMindPickedupNotOnTrack = "578",
			DontCancelNeedHelpContinueChatChangedMindPickedupNotOnTrack = "767",
			DontCancelNoHelpChangedMindPickedupNotOnTrack = "579";

	// Incorrect Address Not Placed and Not On Track
	public static final String
			YesCancelIncorrectAddNotPlacedNoTrack = "91",
			NoCancelIncorrectAddNotPlacedNoTrack = "92",
			NeedMoreHelpYesCancelIncorrectAddNotPlacedNoTrack = "93",
			NoDoneYesCancelIncorrectAddNotPlacedNoTrack = "94",
			YesNeedMoreHelpYesCancelIncorrectAddNotPlacedNoTrack = "98",
			NoNeedMoreHelpYesCancelIncorrectAddNotPlacedNoTrack = "99",
			CHATYesNeedMoreHelpYesCancelIncorrectAddNotPlacedNoTrack = "695",
			CALLYesNeedMoreHelpYesCancelIncorrectAddNotPlacedNoTrack = "696",
			YesNeedMoreHelpNoCancelIncorrectAddNotPlacedNoTrack = "100",
			NoDoneNoCancelIncorrectAddNotPlacedNoTrack = "101",
			CHATYesNeedMoreHelpNoCancelIncorrectAddNotPlacedNoTrack = "697",
			CALLYesNeedMoreHelpNoCancelIncorrectAddNotPlacedNoTrack = "698";

	// Incorrect Address Not Placed and On Track
	public static final String
			YesCancelIncorrectAddNotPlacedOnTrack = "151",
			NoCancelIncorrectAddNotPlacedOnTrack = "152",
			NeedMoreHelpYesCancelIncorrectAddNotPlacedOnTrack = "153",
			NoDoneYesCancelIncorrectAddNotPlacedOnTrack = "154",
			YesNeedMoreHelpYesCancelIncorrectAddNotPlacedOnTrack = "158",
			NoNeedMoreHelpYesCancelIncorrectAddNotPlacedOnTrack = "159",
			CHATYesNeedMoreHelpYesCancelIncorrectAddNotPlacedOnTrack = "725",
			CALLYesNeedMoreHelpYesCancelIncorrectAddNotPlacedOnTrack = "726",
			YesNeedMoreHelpNoCancelIncorrectAddNotPlacedOnTrack = "160",
			NoDoneNoCancelIncorrectAddNotPlacedOnTrack = "161",
			CHATYesNeedMoreHelpNoCancelIncorrectAddNotPlacedOnTrack = "727",
			CALLYesNeedMoreHelpNoCancelIncorrectAddNotPlacedOnTrack = "728";

	// Incorrect Address Placed and Not On Track
	public static final String
			YesCancelIncorrectAddPlacedNoTrack = "31",
			NoCancelIncorrectAddPlacedNoTrack = "32",
			NeedMoreHelpYesCancelIncorrectAddPlacedNoTrack = "33",
			NoDoneYesCancelIncorrectAddPlacedNoTrack = "34",
			YesNeedMoreHelpYesCancelIncorrectAddPlacedNoTrack = "38",
			NoNeedMoreHelpYesCancelIncorrectAddPlacedNoTrack = "39",
			CHATYesNeedMoreHelpYesCancelIncorrectAddPlacedNoTrack = "663",
			CALLYesNeedMoreHelpYesCancelIncorrectAddPlacedNoTrack = "664",
			YesNeedMoreHelpNoCancelIncorrectAddPlacedNoTrack = "40",
			NoDoneNoCancelIncorrectAddPlacedNoTrack = "41",
			CHATYesNeedMoreHelpNoCancelIncorrectAddPlacedNoTrack = "665",
			CALLYesNeedMoreHelpNoCancelIncorrectAddPlacedNoTrack = "666";

	// TODO One more disposition and Pickedup flows need to come here


	// Placed By Mistake Not Placed and Not On Track
	public static final String
			YesCancelPlacedByMistakeNotPlacedNoTrack = "79",
			NoCancelPlacedByMistakeNotPlacedNoTrack = "80",
			NeedMoreHelpYesCancelPlacedByMistakeNotPlacedNoTrack = "81",
			NoDoneYesCancelPlacedByMistakeNotPlacedNoTrack = "82",
			YesNeedMoreHelpYesCancelPlacedByMistakeNotPlacedNoTrack = "86",
			NoNeedMoreHelpYesCancelPlacedByMistakeNotPlacedNoTrack = "87",
			CHATYesNeedMoreHelpYesCancelPlacedByMistakeNotPlacedNoTrack = "689",
			CALLYesNeedMoreHelpYesCancelPlacedByMistakeNotPlacedNoTrack = "690",
			YesNeedMoreHelpNoCancelPlacedByMistakeNotPlacedNoTrack = "88",
			NoDoneNoCancelPlacedByMistakeNotPlacedNoTrack = "89",
			CHATYesNeedMoreHelpNoCancelPlacedByMistakeNotPlacedNoTrack = "691",
			CALLYesNeedMoreHelpNoCancelPlacedByMistakeNotPlacedNoTrack = "692";

	// Placed By Mistake Not Placed and On Track
	public static final String
			YesCancelPlacedByMistakeNotPlacedOnTrack = "139",
			NoCancelPlacedByMistakeNotPlacedOnTrack = "140",
			NeedMoreHelpYesCancelPlacedByMistakeNotPlacedOnTrack = "141",
			NoDoneYesCancelPlacedByMistakeNotPlacedOnTrack = "142",
			YesNeedMoreHelpYesCancelPlacedByMistakeNotPlacedOnTrack = "146",
			NoNeedMoreHelpYesCancelPlacedByMistakeNotPlacedOnTrack = "147",
			CHATYesNeedMoreHelpYesCancelPlacedByMistakeNotPlacedOnTrack = "719",
			CALLYesNeedMoreHelpYesCancelPlacedByMistakeNotPlacedOnTrack = "720",
			YesNeedMoreHelpNoCancelPlacedByMistakeNotPlacedOnTrack = "148",
			NoDoneNoCancelPlacedByMistakeNotPlacedOnTrack = "149",
			CHATYesNeedMoreHelpNoCancelPlacedByMistakeNotPlacedOnTrack = "721",
			CALLYesNeedMoreHelpNoCancelPlacedByMistakeNotPlacedOnTrack = "722";

	// Placed By Mistake Placed and Not On Track
	public static final String
			YesCancelPlacedByMistakePlacedNoTrack = "19",
			NoCancelPlacedByMistakePlacedNoTrack = "20",
			NeedMoreHelpYesCancelPlacedByMistakePlacedNoTrack = "21",
			NoDoneYesCancelPlacedByMistakePlacedNoTrack = "22",
			YesNeedMoreHelpYesCancelPlacedByMistakePlacedNoTrack = "26",
			NoNeedMoreHelpYesCancelPlacedByMistakePlacedNoTrack = "27",
			CHATYesNeedMoreHelpYesCancelPlacedByMistakePlacedNoTrack = "657",
			CALLYesNeedMoreHelpYesCancelPlacedByMistakePlacedNoTrack = "658",
			YesNeedMoreHelpNoCancelPlacedByMistakePlacedNoTrack = "28",
			NoDoneNoCancelPlacedByMistakePlacedNoTrack = "29",
			CHATYesNeedMoreHelpNoCancelPlacedByMistakePlacedNoTrack = "659",
			CALLYesNeedMoreHelpNoCancelPlacedByMistakePlacedNoTrack = "660";


	// TODO One more disposition and Pickedup flows need to come here

	// Order Delayed Not Placed and Not On Track
	public static final String
			YesCancelOrderDelayedNotPlacedNoTrack = "103",
			NoCancelOrderDelayedNotPlacedNoTrack = "104",
			NeedMoreHelpYesCancelOrderDelayedNotPlacedNoTrack = "105",
			NoDoneYesCancelOrderDelayedNotPlacedNoTrack = "106",
			YesNeedMoreHelpYesCancelOrderDelayedNotPlacedNoTrack = "110",
			NoNeedMoreHelpYesCancelOrderDelayedNotPlacedNoTrack = "111",
			CHATYesNeedMoreHelpYesCancelOrderDelayedNotPlacedNoTrack = "701",
			CALLYesNeedMoreHelpYesCancelOrderDelayedNotPlacedNoTrack = "702",
			YesNeedMoreHelpNoCancelOrderDelayedNotPlacedNoTrack = "112",
			NoDoneNoCancelOrderDelayedNotPlacedNoTrack = "113",
			CHATYesNeedMoreHelpNoCancelOrderDelayedNotPlacedNoTrack = "703",
			CALLYesNeedMoreHelpNoCancelOrderDelayedNotPlacedNoTrack = "704";

	// Order Delayed Placed and Not On Track
	public static final String
			YesCancelOrderDelayedPlacedNoTrack = "43",
			NoCancelOrderDelayedPlacedNoTrack = "44",
			NeedMoreHelpYesCancelOrderDelayedPlacedNoTrack = "45",
			NoDoneYesCancelOrderDelayedPlacedNoTrack = "46",
			YesNeedMoreHelpYesCancelOrderDelayedPlacedNoTrack = "50",
			NoNeedMoreHelpYesCancelOrderDelayedPlacedNoTrack = "51",
			CHATYesNeedMoreHelpYesCancelOrderDelayedPlacedNoTrack = "669",
			CALLYesNeedMoreHelpYesCancelOrderDelayedPlacedNoTrack = "670",
			YesNeedMoreHelpNoCancelOrderDelayedPlacedNoTrack = "52",
			NoDoneNoCancelOrderDelayedPlacedNoTrack = "53",
			CHATYesNeedMoreHelpNoCancelOrderDelayedPlacedNoTrack = "671",
			CALLYesNeedMoreHelpNoCancelOrderDelayedPlacedNoTrack = "672";

	// Food Issues
	  public static final String
			itemsMissing = "201",
			itemsDifferent = "202",
			packagingORspillageIssue = "203",
			receivedBadQuality = "204",
			foodQuantity = "205";

      public static final String
			  splInstructionsNotFollowed = "267",
			  itemsMissingContainersNo = "268",
			  itemsMissingEnterContainer = "275",
			  itemsDifferentSelectItems = "269",
			  itemsDifferentAddComments = "276",
			  packagingORspillageIssueSomeItems = "270",
			  packagingORspillageEntireItem = "271",
			  packagingORspillageSelectIssue = "278",
			  packagingORspillageAddPhotos = "279",
			  packagingORspillageSKIP = "280",
			  packagingORspillageEntireItemAddPhotos = "281",
			  packagingORspillageEntireItemSKIP = "282",
			  receivedBadQualitySelectItems = "273",
			  foodQuantitySelectItems = "272",
			  splInstructionsNotFollowedAddComments = "274",
			  splInstructionsNotFollowedSubmitComments = "277";

	  public static final String
			  //cafeOrderSpecific = "900",
			  cafeCancel = "901",
			  cafeEdit = "902",
			  cafeItemsMissing = "903",
			  cafeBadQuality = "904",
			  cafeNotRcvdRefund = "905",
			  cafePaymentRefundQueries = "906",
			  cafeBuffetCancel = "907",
			  cafeDifferentIssue = "908",
			  cafeChangedMyMind = "911",
			  cafeItemsNotAvailable = "912",
			  cafeItemsNotAvailableYesCancel="923",
			  cafeItemsNotAvailableDontCancel="924",
			  cafeItemsNotAvailableYesHelp="935",
			  cafeItemsNotAvailableYesNoHelp="936",
			  cafeItemsNotAvailableDontCancelHelp="937",
			  cafeItemsNotAvailableDontCancelNoHelp="938",
			  cafeOtherIssues = "913",
			  cafeYesCancelChangedMind = "921",
			  cafeNotCancelChangedMind = "922",
			  cafeNeedInfoChangedMindYes = "931",
			  cafeDoneChangedYes = "932",
			  cafeNeedInfoChangedMindNot = "933",
			  cafeDoneChangedNot = "934";

	public static final String
			dominosCancel = "198",
			dominosCancelNoHelpDone = "200",
			dominosCancelNeedHelp = "199",
			dominosCancelNeedHelpContinueChat = "753",
			dominosCancelNeedHelpCallUs = "754";


	// Incorrect Address Placed and Not On Track
	public static final String
			YesCancelIncorrectAddPlacedOnTrack = "187",
			NoCancelIncorrectAddPlacedOnTrack = "188",

			NeedMoreHelpYesCancelIncorrectAddPlacedOnTrack = "189",
			NoDoneYesCancelIncorrectAddPlacedOnTrack = "190",

			CancelFeeIssueYesCancelIncorrectAddPlacedOnTrack="194",
			ContinueChatCancelFeeIssueYesCancelIncorrectAddPlacedOnTrack="747",
			CallUsCancelFeeIssueYesCancelIncorrectAddPlacedOnTrack="748",


			OtherIssueYesCancelIncorrectAddPlacedOnTrack="195",
			ContinueChatOtherIssueYesCancelIncorrectAddPlacedOnTrack="749",
			CallUsOtherIssueYesCancelIncorrectAddPlacedOnTrack="750",

			YesNeedMoreHelpNoCancelIncorrectAddPlacedOnTrack = "196",
			NoDoneNoCancelIncorrectAddPlacedOnTrack = "197",

			CHATYesNeedMoreHelpNoCancelIncorrectAddPlacedOnTrack = "751",
			CALLYesNeedMoreHelpNoCancelIncorrectAddPlacedOnTrack = "752";

	// Placed By Mistake Placed and Not On Track
	public static final String
			YesCancelPlacedByMistakePlacedOnTrack = "175",
			NoCancelPlacedByMistakePlacedOnTrack = "176",

			NeedMoreHelpYesCancelPlacedByMistakePlacedOnTrack = "177",
			NoDoneYesCancelPlacedByMistakePlacedOnTrack = "178",


			CancellationFeeNeedMoreHelpYesCancelPlacedByMistakePlacedOnTrack ="182",
			CHATCancellationFeeNeedMoreHelpYesCancelPlacedByMistakePlacedOnTrack ="739",
			CALLCancellationFeeNeedMoreHelpYesCancelPlacedByMistakePlacedOnTrack = "740",

			OtherIssuesNeedMoreHelpYesCancelPlacedByMistakePlacedOnTrack ="183",
			CHATOtherIssuesNeedMoreHelpYesCancelPlacedByMistakePlacedOnTrack = "741",
			CALLOtherIssuesNeedMoreHelpYesCancelPlacedByMistakePlacedOnTrack = "742",

			YesNeedMoreHelpNoCancelPlacedByMistakePlacedOnTrack = "184",
			NoDoneNoCancelPlacedByMistakePlacedOnTrack = "185",

			CHATYesNeedMoreHelpNoCancelPlacedByMistakePlacedOnTrack = "743",
			CALLYesNeedMoreHelpNoCancelPlacedByMistakePlacedOnTrack = "744";


	// Expected Faster Delivery  Placed and  On Track
	public static final String
			YesCancelExpectedFasterPlacedOnTrack = "55",
			NoCancelExpectedFasterPlacedOnTrack = "56",

			NeedMoreHelpYesCancelExpectedFasterPlacedOnTrack = "57",
			NoDoneYesCancelExpectedFasterPlacedOnTrack = "58",

			CancellationFessNeedMoreHelpYesCancelExpectedFasterPlacedOnTrack = "62",
			CHATCancellationFessNeedMoreHelpYesCancelExpectedFasterPlacedOnTrack= "675",
			CALLCancellationFessNeedMoreHelpYesCancelExpectedFasterPlacedOnTrack = "676",

			OtherIssuesNeedMoreHelpYesCancelExpectedFasterPlacedOnTrack = "63",
			CHATOtherIssuesNeedMoreHelpYesCancelExpectedFasterPlacedOnTrack = "677",
			CALLOtherIssuesNeedMoreHelpYesCancelExpectedFasterPlacedOnTrack = "678",

			YesNeedMoreHelpNoCancelExpectedFasterPlacedOnTrack = "64",
			NoDoneNoCancelExpectedFasterPlacedOnTrack = "65",

			CHATYesNeedMoreHelpNoCancelExpectedFasterPlacedOnTrack = "679",
			CALLYesNeedMoreHelpNoCancelExpectedFasterPlacedOnTrack = "680";

	// Incorrect Address Pickedup and On Track
	public static final String
			YesCancelIncorrectAddPickedupOnTrack = "591",
			NoCancelIncorrectAddPickedupOnTrack = "592",

			NeedMoreHelpYesCancelIncorrectAddPickedupOnTrack = "593",
			NoDoneYesCancelIncorrectAddPickedupOnTrack = "594",

			CancellationFeeNeedMoreHelpYesCancelIncorrectAddPickedupOnTrack = "596",
			CHATCancellationFeeNeedMoreHelpYesCancelIncorrectAddPickedupOnTrack = "775",
			CALLCancellationFeeNeedMoreHelpYesCancelIncorrectAddPickedupOnTrack = "776",

			OtherIssuesNeedMoreHelpYesCancelIncorrectAddPickedupOnTrack = "597",
			CHATOtherIssuesNeedMoreHelpYesCancelIncorrectAddPickedupOnTrack = "777",
			CALLOtherIssuesNeedMoreHelpYesCancelIncorrectAddPickedupOnTrack = "778",


			YesNeedMoreHelpNoCancelIncorrectAddPickedupOnTrack = "600",
			NoDoneNoCancelIncorrectAddPickedupOnTrack = "601",

			CHATYesNeedMoreHelpNoCancelIncorrectAddPickedupOnTrack = "781",
			CALLYesNeedMoreHelpNoCancelIncorrectAddPickedupOnTrack = "782";

	// Placed By Mistake Pickedup and On Track
	public static final String
			YesCancelPlacedByMistakePickedupOnTrack = "624",
			NoCancelPlacedByMistakePickedupOnTrack = "625",

			NeedMoreHelpYesCancelPlacedByMistakePickedupOnTrack = "626",
			NoDoneYesCancelPlacedByMistakePickedupOnTrack = "627",

			CancellationFeeNeedMoreHelpYesCancelPlacedByMistakePickedupOnTrack = "629",
			CHATCancellationFeeNeedMoreHelpYesCancelPlacedByMistakePickedupOnTrack = "795",
			CALLCancellationFeeNeedMoreHelpYesCancelPlacedByMistakePickedupOnTrack = "796",

			OtherIssueNeedMoreHelpYesCancelPlacedByMistakePickedupOnTrack = "630",
			CHATOtherIssueNeedMoreHelpYesCancelPlacedByMistakePickedupOnTrack = "797",
			CALLOtherIssueNeedMoreHelpYesCancelPlacedByMistakePickedupOnTrack = "798",

			YesNeedMoreHelpNoCancelPlacedByMistakePickedupOnTrack = "633",
			NoDoneNoCancelPlacedByMistakePickedupOnTrack = "634",

			CHATYesNeedMoreHelpNoCancelPlacedByMistakePickedupOnTrack = "801",
			CALLYesNeedMoreHelpNoCancelPlacedByMistakePickedupOnTrack = "802";

	// Expected Faster Delivery  Pickedup and  On Track
	public static final String
			YesCancelExpectedFasterPickedupOnTrack = "635",
			NoCancelExpectedFasterPickedupOnTrack = "636",

			NeedMoreHelpYesCancelExpectedFasterPickedupOnTrack = "637",
			NoDoneYesCancelExpectedFasterPickedupOnTrack = "638",


			CancellationFeeNeedMoreHelpYesCancelExpectedFasterPickedupOnTrack = "640",
			CHATCancellationFeeNeedMoreHelpYesCancelExpectedFasterPickedupOnTrack = "803",
			CALLCancellationFeeNeedMoreHelpYesCancelExpectedFasterPickedupOnTrack = "804",

			OtherIssuesNeedMoreHelpYesCancelExpectedFasterPickedupOnTrack = "641",
			CHATOtherIssuesNeedMoreHelpYesCancelExpectedFasterPickedupOnTrack = "805",
			CALLOtherIssuesNeedMoreHelpYesCancelExpectedFasterPickedupOnTrack = "806",

			YesNeedMoreHelpNoCancelExpectedFasterPickedupOnTrack = "644",
			NoDoneNoCancelExpectedFasterPickedupOnTrack = "645",

			CHATYesNeedMoreHelpNoCancelExpectedFasterPickedupOnTrack = "809",
			CALLYesNeedMoreHelpNoCancelExpectedFasterPickedupOnTrack = "810";

	// Expected Faster Delivery  not placed and  On Track
	public static final String
			YesCancelExpectedFasterOrderNotPlacedOnTrack = "115",
			NoCancelExpectedFasterOrderNotPlacedOnTrack = "116",

			NeedMoreHelpYesCancelExpectedFasterOrderNotPlacedOnTrack = "117",
					NeedMoreHelpNeedMoreHelpYesCancelExpectedFasterOrderNotPlacedOnTrack = "122",
					CHATNeedMoreHelpYesCancelExpectedFasterOrderNotPlacedOnTrack = "707",
					CALLNeedMoreHelpYesCancelExpectedFasterOrderNotPlacedOnTrack = "708",

					NoDoneNeedMoreHelpYesCancelExpectedFasterOrderNotPlacedOnTrack = "123",

			NoDoneYesCancelExpectedFasterOrderNotPlacedOnTrack = "118",


			NeedMoreHelpNoCancelExpectedFasterOrderNotPlacedOnTrack = "124",
			CHATNeedMoreHelpNoCancelExpectedFasterOrderNotPlacedOnTrack = "709",
			CALLNeedMoreHelpNoCancelExpectedFasterOrderNotPlacedOnTrack = "710",


			NoDoneNoCancelExpectedFasterOrderNotPlacedOnTrack = "125";

// COD Block Check
	public static final String
			CheckCodEnabled = "1050",
				NeedMoreHelpCheckCodEnabled = "1051",
					CHATNeedMoreHelpCheckCodEnabled = "1053",
					CALLNeedMoreHelpCheckCodEnabled = "1054",
				NoDoneCheckCodEnabled = "1052",

			CheckCodEnabledAfterPlacingOrder = "1055",
				NeedMoreHelpCheckCodEnabledAfterPlacingOrder = "1056",
					CHATNeedMoreHelpCheckCodEnabledAfterPlacingOrder = "1058",
					CALLNeedMoreHelpCheckCodEnabledAfterPlacingOrder = "1059",
				NoDoneCheckCodEnabledAfterPlacingOrder = "1057";

	//Payments and Refund
	public static final String
			PaymentsAndRefundIssues = "247",
			BillRelatedIssue = "248",
				HowWouldYouLikeToKnow = "1301",

			PaymentFailureRelatedIssue = "249",
				YesNeedMoreHelpPaymentFailureRelatedIssue = "1302",
					CHATYesNeedMoreHelpPaymentFailureRelatedIssue = "1304",
					CALLYesNeedMoreHelpPaymentFailureRelatedIssue = "1305",
				NoDonePaymentFailureRelatedIssue = "1303",

			ChangePaymentMethod = "250",
				YesNeedMoreHelpChangePaymentMethod = "1306",
					CHATYesNeedMoreHelpChangePaymentMethod = "1308",
					CALLYesNeedMoreHelpChangePaymentMethod = "1309",
				NoDoneChangePaymentMethod = "1307",

			KnowRefundStatus = "251",
				YesNeedMoreHelpKnowRefundStatus = "1310",
					CHATYesNeedMoreHelpKnowRefundStatus = "1312",
					CALLYesNeedMoreHelpKnowRefundStatus = "1313",
				NoDoneKnowRefundStatus = "1311",

			KnowInvoiceForOrder = "252",
				YesNeedMoreHelpKnowInvoiceForOrder = "1314",
					CHATYesNeedMoreHelpKnowInvoiceForOrder = "1316",
					CALLYesNeedMoreHelpKnowInvoiceForOrder = "1317",
				NoDoneKnowInvoiceForOrder = "1315";

	// Takeaway Flows
	public static final String
	// Where is my order
			TakeAwayWhereIsMyOrder = "1400",
				YesNeedMoreHelpTakeAwayWhereIsMyOrder = "1406",
					CHATYesNeedMoreHelpTakeAwayWhereIsMyOrder = "1408",
					CALLYesNeedMoreHelpTakeAwayWhereIsMyOrder = "1409",


				NoDoneTakeAwayWhereIsMyOrder = "1407",

	// Cancellation Flows
			TakeAwayOrderToCancel = "1401",
					TakeawayChangedMyMind = "1410",
						YesCancelTakeawayChangedMyMind ="1414",
							NeedMoreHelpYesCancelTakeawayChangedMyMind = "1422",
								CHATNeedMoreHelpYesCancelTakeawayChangedMyMind = "1424",
								CALLNeedMoreHelpYesCancelTakeawayChangedMyMind = "1425",

							NoDoneYesCancelTakeawayChangedMyMind = "1423",

						NoCancelTakeawayChangedMyMind ="1415",
							NeedMoreHelpNoCancelTakeawayChangedMyMind = "1426",
								CHATNeedMoreHelpNoCancelTakeawayChangedMyMind = "1428",
								CALLNeedMoreHelpNoCancelTakeawayChangedMyMind = "1429",
							NoDoneNoCancelTakeawayChangedMyMind = "1427",

					TakeawayUnableToPickup = "1411",
						YesCancelTakeawayUnableToPickup = "1416",
							NeedMoreHelpYesCancelTakeawayUnableToPickup = "1430",
								CHATNeedMoreHelpYesCancelTakeawayUnableToPickup = "1432",
								CALLNeedMoreHelpYesCancelTakeawayUnableToPickup = "1433",

							NoDoneYesCancelTakeawayUnableToPickup = "1431",

						NoCancelTakeawayUnableToPickup = "1417",
							NeedMoreHelpNoCancelTakeawayUnableToPickup = "1434",
								CHATNeedMoreHelpNoCancelTakeawayUnableToPickup = "1436",
								CALLNeedMoreHelpNoCancelTakeawayUnableToPickup = "1437",
							NoDoneNoCancelTakeawayUnableToPickup = "1435",

					TakeawayPlacedByMistake = "1412",
						YesCancelTakeawayPlacedByMistake = "1418",
							NeedMoreHelpYesCancelTakeawayPlacedByMistake = "1442",
								CHATNeedMoreHelpYesCancelTakeawayPlacedByMistake = "1444",
								CALLNeedMoreHelpYesCancelTakeawayPlacedByMistake = "1445",
							NoDoneYesCancelTakeawayPlacedByMistake = "1443",

						NoCancelTakeawayPlacedByMistake = "1419",
							NeedMoreHelpNoCancelTakeawayPlacedByMistake = "1446",
								CHATNeedMoreHelpNoCancelTakeawayPlacedByMistake = "1448",
								CALLNeedMoreHelpNoCancelTakeawayPlacedByMistake = "1449",
							NoDoneNoCancelTakeawayPlacedByMistake = "1447",

					TakeawayOrderDelayed = "1413",
						YesCancelTakeawayOrderDelayed = "1420",
							NeedMoreHelpYesCancelTakeawayOrderDelayed = "1450",
								CHATNeedMoreHelpYesCancelTakeawayOrderDelayed = "1452",
								CALLNeedMoreHelpYesCancelTakeawayOrderDelayed = "1453",
							NoDoneYesCancelTakeawayOrderDelayed = "1451",

						NoCancelTakeawayOrderDelayed = "1421",
							NeedMoreHelpNoCancelTakeawayOrderDelayed = "1454",
								CHATNeedMoreHelpNoCancelTakeawayOrderDelayed = "1456",
								CALLNeedMoreHelpNoCancelTakeawayOrderDelayed = "1457",
							NoDoneNoCancelTakeawayOrderDelayed = "1455",

	// Order Edit Flows
			TakeAwayOrderToEdit = "1402",
				NeedMoreHelpTakeAwayOrderToEdit = "1458",
					CHATNeedMoreHelpTakeAwayOrderToEdit = "1460",
					CALLNeedMoreHelpTakeAwayOrderToEdit = "1461",

				NoDoneTakeAwayOrderToEdit = "1459",

	// Food Preparation Instruction
			TakeAwayFoodPreInstruction = "1403",
				NeedMoreHelpTakeAwayFoodPreInstruction = "1462",
					CHATNeedMoreHelpTakeAwayFoodPreInstruction = "1464",
					CALLNeedMoreHelpTakeAwayFoodPreInstruction = "1465",

				NoDoneTakeAwayFoodPreInstruction = "1463",

	// Food Issues
			TakeAwayMissingItems = " 1404",
				NeedMoreHelpTakeAwayMissingItems = "1466",
					CHATNeedMoreHelpTakeAwayMissingItems = "1468",
					CALLNeedMoreHelpTakeAwayMissingItems = "1469",
				NoDoneTakeAwayMissingItems = "1467",

			TakeAwayPackagingIssue = "1405",
				NeedMoreHelpTakeAwayPackagingIssue = "1470",
					CHATNeedMoreHelpTakeAwayPackagingIssue = "1472",
					CALLNeedMoreHelpTakeAwayPackagingIssue = "1473",
				NoDoneTakeAwayPackagingIssue = "1471";


	// DASH Work Flows
	public static final String
			DashOrderStatus = "1600",
				YesNeedMoreHelpDashOrderStatus = "1611",
				NoDoneDashOrderStatus = "1612",

			DashCancelOrder = "1601",
				YedNeedMoreHelpDashCancelOrder = "1613",
				NoDoneDashCancelOrder = "1614",

			DashModityItemsUntillDEArrived = "1602",
					YedNeedMoreHelpDashModityItemsUntillDEArrived = "1615",
					NoDoneDashModityItemsUntillDEArrived = "1616",

			DashAddModifyStore = "1603",
					YedNeedMoreHelpDashAddModifyStore = "1617",
					NoDoneDashAddModifyStore = "1617",

			DashModityItemsPostDEArrived = "1604",
					YedNeedMoreHelpDashModityItemsPostDEArrived = "1619",
					NoDoneDashModityItemsPostDEArrived = "1620",

			DashPaymentAndRefundIssues = "1605",
					YedNeedMoreHelpDashPaymentAndRefundIssues = "1621",
					NoDoneDashPaymentAndRefundIssues = "1622",

			DashCouponsRelatedIssues = "1606",
					YedNeedMoreHelpDashCouponsRelatedIssues = "1623",
					NoDoneDashCouponsRelatedIssues = "1624",

			DashBillRelatedIssues = "1607",
					YedNeedMoreHelpDashBillRelatedIssues = "1625",
					NoDoneDashBillRelatedIssues = "1626",

			DashOrderMarkedIncorrectly = "1608",
					YedNeedMoreHelpDashOrderMarkedIncorrectly = "1627",
					NoDoneDashOrderMarkedIncorrectly = "1628",

			DashItemsDamage = "1609",
					YedNeedMoreHelpDashItemsDamage = "1629",
					NoDoneDashItemsDamage = "1630",

			DashItemsMissing = "1610",
					YedNeedMoreHelpDashItemsMissing = "",
					NoDoneDashItemsMissing = "";

	public static final String
			LeaveDeliveryInstruction = "1200",

				OrderDropOffInstr = "1201",
						OrderDropOffInstrAtReception = "1208",
							NeedMoreHelpOrderDropOffInstrAtReception = "1220",
							NoDoneOrderDropOffInstrAtReception = "1221",

						OrderDropOffInstrAtSecurityGate = "1209",
							NeedMoreHelpOrderDropOffInstrAtSecurityGate = "1222",
							NoDoneOrderDropOffInstrAtSecurityGate = "1223",

						OrderDropOffInstrAtDifferentInstr = "1210",
							TextOrderDropOffInstrAtDifferentInstr= "1211",
								NeedMoreHelpTextOrderDropOffInstrAtDifferentInstr = "1224",
								NoDoneTextOrderDropOffInstrAtDifferentInstr = "1225",

				LeaveAddressNavigationInstr = "1202",
					AddNavigationInstr = "1207",
						YesNeedMoreHelpAddNavigationInstr = "1218",
						NoDoneAddNavigationInstr = "1219",

				UpdatePhoneNumber = "1203",
					UpdateYourPhoneNumber = "1206",
						YesNeedMoreHelpUpdateYourPhoneNumber = "1216",
						NoDoneUpdateYourPhoneNumber = "1217",

				RequestToGetChange = "1204",
					YesNeedMoreHelpRequestToGetChange = "1212",
					NoDoneRequestToGetChange = "1213",

				UpdateDeliveryAddress = "1205",
					YesNeedMoreHelpUpdateDeliveryAddress = "1214",
					NoDoneUpdateDeliveryAddress = "1215";

	// Order status Flow BCP Mode
	public static final String
			DENotReachable = "266",
				NeedMoreHelpDENotReachable = "1250",
				NoDoneDENotReachable = "1251",

			BCPWhereIsMyOrder = "1260",
				CustomerWantToCancelBCPWhereIsMyOrder ="1261",
					YesCancelCustomerWantToCancel = "1264",
						NeedMoreHelpYesCancelCustomerWantToCancel = "1266",
							RefundMyMoneyNeedMoreHelp = "1268",
								NeedMoreHelpRRefundMyMoneyNeedMoreHelp = "1271",
								NoDoneRefundMyMoneyNeedMoreHelp = "1272",

							CancellationFeeChargedNeedMoreHelpYesCancel = "1269",
							OtherIssuesNeedMoreHelpYesCancel = "1670",
						NoDoneYesCancelCustomerWantToCancel = "1267",

					DontCancelCustomerWantToCancel = "1265",
						NeedMoreHelpDontCancelCustomerWantToCancel = "1273",
						NoDoneDontCancelCustomerWantToCancel = "1274",

				NeedMoreHelpBCPWhereIsMyOrder = "1262",

				NoDoneBCPWhereIsMyOrder = "1263",

				UnableToReachDEBCPWhereIsMyOrder = "1275",
					NeedMoreHelpUnableToReachDEBCPWhereIsMyOrder = "1276",
					NoDoneUnableToReachDEBCPWhereIsMyOrder = "1277";

	public static final String
			WhereIsMyOrder = "240",
			CouponRelatedQueries = "253",
			IHaventReceivedMyOrder = "239",

			PlacingOrderGeneralIssue = "206",
			LoginIssueGeneralIssue = "212",
			PaymentAndRefundGeneralIssue = "215",
			CouponGeneralIssue = "223",
			WantToUnsubcribeGeneralIssue = "228",

			CanIEditOrderFaqIssue = "473",
			WantToCancelOrderFaqIssue = "474",
			QualityQuantityFaqIssue = "475",
			IsThereMiniumOrderValueFaqIssue = "476",
			ChangeForDeliveryFaqIssue = "477",
			DeliveryFaqIssue = "478",
			DeliveryHoursFaqIssue = "479",
			CanIOrderFromAnyLocationFAQIssue = "480",
			SingleOrderFromManyRestroFaqIssue = "481",
			BulkOrderFaqIssue = "482",
			OrderInAdvanceFaqIssue = "483",
			ChangeAddressNUmberFaqIssue = "484",
			DidNotReceivedOTPFaqIssue = "485",
			DidNotReceivedReferalCouponFaqIssue = "486",
			DeactivateMyAccountFaqIssue = "487",
			ProfileDetailsFaqIssue = "488",
			SodexoFaqIssue = "490",
			OrderInvoiceFaqIssue = "491",
			RestaurantPartnerFaqIssue = "492",
			CarrerOpportunityFaqIssue = "494",
			WantToProvideFeedbackFaqIssue = "485",

			TermsOfUseLegalIssue = "229",
			PrivacyPolicyLegalIssue = "230",
			CancellationAndRefundLegalIssue = "231",
			TermasOfUseForSwiggyLegalIssue = "232",

			SwiggySuperFaq = "496",
			SubscribeSuperFaq = "497",
			SuperBenefitsSuperFAQ = "498",
			MembershipSuperFaq = "499",
			UseSuperFaq = "500",
			RenewSuperFaq = "501",
			CancelSuperMembership = "502",
			PaySuperFaq = "503",
			SuperPlanSuperFaq = "504",
			ChagesWaivedOffSuperFaq = "505",
			LimitOnOrderSuperFaq = "506",
			SuperOnOrderSuperFaq = "507";

	public static final String
			WhereIsMyOrderTakingTooLong = "241",
				NeedMoreHelpWhereIsMyOrderTakingTooLong = "851",
					ChatNeedMoreHelpWhereIsMyOrderTakingTooLong = "1229",
					CHATNeedMoreHelpWhereIsMyOrderTakingTooLong = "1234",
				NoDoneWhereIsMyOrderTakingTooLong = "852";

	// Daily
	public static final String
			WhereIsMyOrderPreOrder = "1697",

			IWantToCancelOrderPreOrder = "1698",
				DoNotLikeFoodCancelPreOrder = "1712",
					YesCancelDoNotLikeFoodCancelPreOrder = "1716",
						NeedMorehelpIsCancelDoNotLikeFoodCancelPreOrder = "1724",
						NoDoneIsCancelDoNotLikeFoodCancelPreOrder = "1725",
					DontCancelDoNotLikeFoodCancelPreOrder = "1717",
						NeedHelpDontCancelDoNotLikeFoodCancelPreOrder = "1726",
						NoDoneDontCancelDoNotLikeFoodCancelPreOrder = "1727",

				IncorrectAddressSelectedCancelPreOrder = "1713",
					YesCancelIncorrectAddressSelectedCancelPreOrder = "1718",
						NeedHelpYesCancelIncorrectAddressSelectedCancelPreOrder = "1728",
						NoDoneYesCancelIncorrectAddressSelectedCancelPreOrder = "1729",
					DontCancelIncorrectAddressSelectedCancelPreOrder = "1719",
						NeedHelpDontCancelIncorrectAddressSelectedCancelPreOrder = "1730",
						NoDoneDontCancelIncorrectAddressSelectedCancelPreOrder = "1731",

				GotBoredOfFoodCancelPreOrder = "1714",
					YesCancelGotBoredOfFoodCancelPreOrder = "1720",
						NeedHelpYesCancelGotBoredOfFoodCancelPreOrder = "1732",
						NoDoneYesCancelGotBoredOfFoodCancelPreOrder = "1733",
					DontCancelGotBoredOfFoodCancelPreOrder = "1721",
						NeedHelpDontCancelGotBoredOfFoodCancelPreOrder = "1734",
						NoDoneDontCancelGotBoredOfFoodCancelPreOrder = "1735",

				OthersPreOrder = "1715",
					YesCancelOthersPreOrder = "1722",
						NeedHelpYesCancelOthersPreOrder = "1736",
						NoDoneYesCancelOthersPreOrder = "1737",
					DontCancelOthersPreOrder = "1723",
						NeedHelpDontCancelOthersPreOrder = "1738",
						NoDoneDontCancelOthersPreOrder = "1739",

			ChangeDeliverySlotPreOrder = "1699",
			SkipMyMealPreOrder = "1700",
			AddModifyPreOrder = "1701",
			ChangeSwapPreOrder = "1702",
			FoodPreInstPreOrder = "1703",
			DeliveryInstrPreOrder = "1704",
			PaymentsAndRefundPreOrder = "1705",
			MarkedDeliveredIncorrectlyPreOrder = "1706",
			MissingItemPreOrder = "1707",
			IncorrectMealProvidedPreOrder = "1708",
			BadQualityPreOrder = "1709",
			PackagingIssuesPreOrder = "1710",
			ReasonNotListedPreOrder = "1711",
			WhereIsMyOrderPostCutOffPreOrder = "1783",

			WhereIsMyOrderSubMeal = "1740",
			IWantToCancelOrderSubMeal = "1741",
				DoNotLikeFoodCancelSubMeal = "1755",
					YesCancelDoNotLikeFoodCancelSubMeal = "1759",
						NeedMorehelpIsCancelDoNotLikeFoodCancelSubMeal = "1767",
						NoDoneIsCancelDoNotLikeFoodCancelSubMeal = "1768",
					DontCancelDoNotLikeFoodCancelSubMeal = "1760",
						NeedHelpDontCancelDoNotLikeFoodCancelSubMeal = "1769",
						NoDoneDontCancelDoNotLikeFoodCancelSubMeal = "1770",

				IncorrectAddressSelectedCancelSubMeal = "1756",
					YesCancelIncorrectAddressSelectedCancelSubMeal = "1761",
						NeedHelpYesCancelIncorrectAddressSelectedCancelSubMeal = "1771",
						NoDoneYesCancelIncorrectAddressSelectedCancelSubMeal = "1772",
					DontCancelIncorrectAddressSelectedCancelSubMeal = "1762",
						NeedHelpDontCancelIncorrectAddressSelectedCancelSubMeal = "1773",
						NoDoneDontCancelIncorrectAddressSelectedCancelSubMeal = "1774",

				GotBoredOfFoodCancelSubMeal = "1757",
					YesCancelGotBoredOfFoodCancelSubMeal = "1763",
						NeedHelpYesCancelGotBoredOfFoodCancelSubMeal = "1775",
						NoDoneYesCancelGotBoredOfFoodCancelSubMeal = "1776",
					DontCancelGotBoredOfFoodCancelSubMeal = "1764",
						NeedHelpDontCancelGotBoredOfFoodCancelSubMeal = "1777",
						NoDoneDontCancelGotBoredOfFoodCancelSubMeal = "1778",

				OthersSubMeal = "1758",
					YesCancelOthersSubMeal = "1765",
						NeedHelpYesCancelOthersSubMeal = "1779",
						NoDoneYesCancelOthersSubMeal = "1780",
					DontCancelOthersSubMeal = "1766",
						NeedHelpDontCancelOthersSubMeal = "1781",
						NoDoneDontCancelOthersSubMeal = "1782",

			ChangeDeliverySlotSubMeal = "1742",
			SkipMyMealSubMeal = "1743",
			AddModifySubMeal = "1744",
			ChangeSwapSubMeal = "1745",
			FoodPreInstSubMeal = "1746",
			DeliveryInstrSubMeal = "1747",
			PaymentsAndRefundSubMeal = "1748",
			MarkedDeliveredIncorrectlySubMeal = "1749",
			MissingItemSubMeal = "1750",
			IncorrectMealProvidedSubMeal = "1751",
			BadQualitySubMeal = "1752",
			PackagingIssuesSubMeal = "1753",
			ReasonNotListedSubMeal = "1754",
			WhereIsMyOrderPostCutOffSubMeal = "1784";

}
