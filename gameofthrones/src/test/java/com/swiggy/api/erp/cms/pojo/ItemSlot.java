package com.swiggy.api.erp.cms.pojo;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;

public class ItemSlot {

    @JsonProperty("data")
    private ItemData data;
    @JsonProperty("user_meta")
    private ItemUserMeta userMeta;

    /**
     * No args constructor for use in serialization
     *
     */
    public ItemSlot() {
    }

    /**
     *
     * @param userMeta
     * @param data
     */
    public ItemSlot(ItemData data, ItemUserMeta userMeta) {
        super();
        this.data = data;
        this.userMeta = userMeta;
    }

    public ItemData getData() {
        return data;
    }

    public void setData(ItemData data) {
        this.data = data;
    }

    public ItemUserMeta getUserMeta() {
        return userMeta;
    }

    public void setUserMeta(ItemUserMeta userMeta) {
        this.userMeta = userMeta;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("data", data).append("userMeta", userMeta).toString();
    }

}
