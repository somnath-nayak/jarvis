package com.swiggy.api.erp.cms.pojo.Variants;

/**
 * Created by kiran.j on 2/21/18.
 */
public class Variants
{
    private Entity entity;

    public Entity getEntity ()
    {
        return entity;
    }

    public void setEntity (Entity entity)
    {
        this.entity = entity;
    }

    public Variants build() {
        setDefaultValues();
        return this;
    }

    public void setDefaultValues() {
        Entity entity = new Entity();
        entity.build();
        if(this.getEntity() == null)
            this.setEntity(entity);
    }

    @Override
    public String toString()
    {
        return "ClassPojo [entity = "+entity+"]";
    }
}