package com.swiggy.api.erp.cms.pojo;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;

public class ItemData {

    //private String id;
    @JsonProperty("item_id")
    private Integer itemId;
    @JsonProperty("day_of_week")
    private Integer dayOfWeek;
    @JsonProperty("open_time")
    private Integer openTime;
    @JsonProperty("close_time")
    private Integer closeTime;

    /**
     * No args constructor for use in serialization
     *
     */
    public ItemData() {
    }

    /**
     *
     *
     * @param closeTime
     * @param openTime
     * @param itemId
     * @param dayOfWeek
     */
    public ItemData(Integer itemId, Integer dayOfWeek, Integer openTime, Integer closeTime) {
        super();
        //this.id = id;
        this.itemId = itemId;
        this.dayOfWeek = dayOfWeek;
        this.openTime = openTime;
        this.closeTime = closeTime;
    }

    /*public String getId() {
        return id;
    }*/

    /*public void setId(String id) {
        this.id = id;
    }*/

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public Integer getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(Integer dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public Integer getOpenTime() {
        return openTime;
    }

    public void setOpenTime(Integer openTime) {
        this.openTime = openTime;
    }

    public Integer getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(Integer closeTime) {
        this.closeTime = closeTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("itemId", itemId).append("dayOfWeek", dayOfWeek).append("openTime", openTime).append("closeTime", closeTime).toString();
    }

}
