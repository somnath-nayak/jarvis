package com.swiggy.api.erp.crm.foodissues.pojo.CCResolution;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"userId",
"orderId",
"issueType",
"issueId"
})
public class ResolutionRequestData {

@JsonProperty("userId")
private Integer userId;
@JsonProperty("orderId")
private Integer orderId;
@JsonProperty("issueType")
private String issueType;
@JsonProperty("issueId")
private int issueId;

@JsonProperty("userId")
public Integer getUserId() {
return userId;
}

@JsonProperty("userId")
public void setUserId(Integer userId) {
this.userId = userId;
}

@JsonProperty("orderId")
public Integer getOrderId() {
return orderId;
}

@JsonProperty("orderId")
public void setOrderId(Integer orderId) {
this.orderId = orderId;
}

@JsonProperty("issueType")
public String getIssueType() {
return issueType;
}

@JsonProperty("issueType")
public void setIssueType(String issueType) {
this.issueType = issueType;
}

@JsonProperty("issueId")
public Integer getIssueId() {
return issueId;
}

@JsonProperty("issueCode")
public void setIssueId(Integer issueId) {
this.issueId = issueId;
}

@Override
public String toString(){
    return getUserId() + ", "+getOrderId() + ", " +getIssueType() + ", " +getIssueId();
}

}