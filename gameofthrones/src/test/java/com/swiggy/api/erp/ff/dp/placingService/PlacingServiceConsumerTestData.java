package com.swiggy.api.erp.ff.dp.placingService;

import com.swiggy.api.erp.ff.helper.LOSHelper;
import com.swiggy.api.erp.ff.helper.PlacingServiceHelper;
import org.testng.annotations.DataProvider;

import java.io.IOException;

public class PlacingServiceConsumerTestData {

    LOSHelper losHelper = new LOSHelper();
    PlacingServiceHelper helper = new PlacingServiceHelper();

    @DataProvider(name = "placingConsumerTestdata")
    public Object[][] placingConsumerTestdata() throws IOException, InterruptedException {
        return new Object[][] {
//                timeStamp, orderId, toState scenario
                {helper.getTimeStampForPlacingServiceMessage(), losHelper.getAnOrder("partner"), "PLACED", "To \"PLACED\""},
                {helper.getTimeStampForPlacingServiceMessage(), losHelper.getAnOrder("partner"), "WITH_RELAYER", "To \"WITH_PARTNER\""},
                {helper.getTimeStampForPlacingServiceMessage(), losHelper.getAnOrder("partner"), "WITH_DE", "To \"WITH_DE\""},

        };
    }

    @DataProvider(name = "placingConsumerDeadLetterTestdata")
    public Object[][] placingConsumerDeadLetterTestdata() throws IOException, InterruptedException {
        return new Object[][] {
//                timeStamp, orderId, toState scenario
                {helper.getTimeStampForPlacingServiceMessage(), "1", "PLACD", "PLACED wrong spelling To \"Dead Letter\""},
//                {helper.getTimeStampForPlacingServiceMessage(), losHelper.getAnOrder("partner"), "placed", "PLACED lower case To \"Dead Letter\""},
//                {helper.getTimeStampForPlacingServiceMessage(), losHelper.getAnOrder("partner"), "WITH_RELAYR", "WITH_RELAYER wrong spelling To \"Dead Letter\""},
//                {helper.getTimeStampForPlacingServiceMessage(), losHelper.getAnOrder("partner"), "with_relayer", "WITH_RELAYER lower case To \"Dead Letter\""},
//                {helper.getTimeStampForPlacingServiceMessage(), losHelper.getAnOrder("partner"), "WITH-DE", "WITH_DE wrong spelling To \"Dead Letter\""},
//                {helper.getTimeStampForPlacingServiceMessage(), losHelper.getAnOrder("partner"), "with_de", "WITH_DE lower case To \"Dead Letter\""},

        };
    }
}
