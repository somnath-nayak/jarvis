package com.swiggy.api.erp.cms.pojo.SpinService;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(Include.NON_NULL)

public class SpinSearch {


        private String manufacturer;

        private Integer mrp;

        private String brand;

        private Integer quantity;

        private String type;


    public SpinSearch(String manufacturer, Integer mrp, String brand, Integer quantity, String type) {
        this.manufacturer = manufacturer;
        this.mrp = mrp;
        this.brand = brand;
        this.quantity = quantity;
        this.type = type;
    }


        public String getManufacturer ()
        {
            return manufacturer;
        }

        public void setManufacturer (String manufacturer)
        {
            this.manufacturer = manufacturer;
        }

        public Integer getMrp ()
        {
            return mrp;
        }

        public void setMrp (int mrp)
        {
            this.mrp = mrp;
        }

        public String getBrand ()
        {
            return brand;
        }

        public void setBrand (String brand)
        {
            this.brand = brand;
        }


        public Integer getQuantity ()
        {
            return quantity;
        }

        public void setQuantity (int quantity)
        {
            this.quantity = quantity;
        }

        public String getType ()
        {
            return type;
        }

        public void setType (String type)
        {
            this.type = type;
        }


}



