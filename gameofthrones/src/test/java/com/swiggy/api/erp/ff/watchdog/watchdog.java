package com.swiggy.api.erp.ff.watchdog;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.ff.constants.OMSConstants;
import com.swiggy.api.erp.ff.helper.LOSHelper;
import com.swiggy.api.erp.ff.helper.WatchdogHelper;

import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Tyrion.RabbitMQHelper;

public class watchdog {
	
	WatchdogHelper watchdoghelper = new WatchdogHelper();
	LOSHelper loshelper = new LOSHelper();
	RabbitMQHelper rmqhelper = new RabbitMQHelper();
	Long orderId = Instant.now().getEpochSecond();
	

	@Test(priority=1,description="Swiggy cafe order placing delay notification",groups= {"sanity","regression"})
	public void SwiggyCafePlacedDeliveredWorkflowWatchdogNotify() throws IOException, InterruptedException {
		System.out.println("====================================================3");    	
    	Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        loshelper.createSwiggyCafeOrder(""+orderId, dateFormat.format(date), ""+orderId,"cafe");
        Thread.sleep(3000);
        List<Map<String, Object>> instanceId = SystemConfigProvider.getTemplate(OMSConstants.WATCHDOG_DB).queryForList("SELECT instance_id,state FROM watchdog.eca_logs where instance_id = "+ orderId  +";");
        Assert.assertEquals(""+orderId, ""+instanceId.get(0).get("instance_id"));
        Assert.assertEquals(""+orderId, ""+instanceId.get(1).get("instance_id"));
        Assert.assertEquals("COMPLETED", ""+instanceId.get(0).get("state"));
        Assert.assertEquals("COMPLETED", ""+instanceId.get(1).get("state"));
        
	}
	
	@Test(priority=2,description="Verify pulished message to queue after defined interval",groups={"sanity","regression"})
	public void verifyPulishedMessagePlacedWorkflow() throws IOException, TimeoutException, InterruptedException {
		System.out.println("====================================================4");
		rmqhelper.purgeQueue(OMSConstants.RMQHOSTNAME,OMSConstants.SWIGGYCAFEQUEUE);
		Thread.sleep(60000);
		String message = rmqhelper.getMessage(OMSConstants.RMQHOSTNAME,OMSConstants.SWIGGYCAFEQUEUE);
		System.out.println("Message is " +message);
        String orderid = JsonPath.read(message, "$.order_id");
        String hint = JsonPath.read(message, "$.hint");
        Assert.assertEquals(""+orderId, ""+orderid);
        Assert.assertEquals("PLACING_BREACHED", ""+hint);
        	
	}

	@Test(priority=3,description="Verify pulished message to queue after defined interval",groups={"sanity","regression"})
	public void verifyPulishedMessageDeliveredWorkflow() throws IOException, TimeoutException, InterruptedException {
		
		rmqhelper.purgeQueue(OMSConstants.RMQHOSTNAME,OMSConstants.SWIGGYCAFEQUEUE);
		Thread.sleep(120000);
		String message = rmqhelper.getMessage(OMSConstants.RMQHOSTNAME,OMSConstants.SWIGGYCAFEQUEUE);
		System.out.println("Message is " +message);
        String orderid = JsonPath.read(message, "$.order_id");
        String hint = JsonPath.read(message, "$.hint");
        Assert.assertEquals(""+orderId, ""+orderid);
        Assert.assertEquals("DELIVERY_BREACHED", ""+hint);
		
	}
	
	@Test(priority=4,description="Verify dead letter flow",groups= {"sanity","regression"})
	public void deadLetterFlow() throws IOException, InterruptedException, TimeoutException {
		
		rmqhelper.purgeQueue(OMSConstants.RMQHOSTNAME,OMSConstants.SWIGGYCAFEDEADLETTER);
		Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        loshelper.createOrderWithWrongJson("null", dateFormat.format(date), ""+orderId);
        Thread.sleep(30000);
        String message = rmqhelper.getMessage(OMSConstants.RMQHOSTNAME,OMSConstants.SWIGGYCAFEDEADLETTER);
        System.out.println("Message is " +message);
        String order_id = JsonPath.read(message, "$.order_id");
        Assert.assertEquals(""+orderId, ""+order_id);
           
	}
	
}
