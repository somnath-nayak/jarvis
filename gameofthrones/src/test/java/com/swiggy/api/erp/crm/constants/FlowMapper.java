package com.swiggy.api.erp.crm.constants;

import com.swiggy.api.sf.checkout.helper.CheckoutHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FlowMapper {

	public static List<String[]> Flowlist = new ArrayList<String[]>();

	CheckoutHelper checkoutHelper = new CheckoutHelper();
	
	public void nodeId() {
			setFlow();
		}

	//Deciding which payload to be used for the different nodes
	public String decidePayload(String nodeId){

		String payload=null;

		//Payload for select items
		if(nodeId== "268" ||nodeId=="269"||nodeId=="272"||nodeId=="273") {
			payload="orderSelectionOverlayInput";
		}

		//Payload for edit redirect response
		else if(nodeId== "950" || nodeId=="974") {
			payload="editflow";
		}

		//Payload for comments
		else if (nodeId== "274" ||nodeId=="276"||nodeId=="277"){
			payload="userComment";
		}

		//Payload for image uploads
		else if (nodeId== "279" ||nodeId=="281"){
			payload="imageUploads";
		}

		//Payload for container count
		else if (nodeId== "275"){
			payload="containerOverlay";
		}
		else {
			payload="validChild";
		}

		return payload;
	}

    public HashMap<String, String[]> setFlow()
 {
    	HashMap<String,String[]> flow = new HashMap<String,String[]>();

	 // Changed my mind Not Placed and Not On Track
	   flow.put("flow1", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ChangedMyMindOrderNotPlacedNotOnTrack, CRMConstants.YesCancelChangedMindNotPlacedNoTrack,
			   CRMConstants.NeedMoreHelpYesCancelChangedMindNotPlacedNoTrack, CRMConstants.YesNeedMoreHelpYesCancelChangedMindNotPlacedNoTrack, CRMConstants.CHATYesNeedMoreHelpYesCancelChangedMindNotPlacedNoTrack}); // Leaf Done

	   flow.put("flow2", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ChangedMyMindOrderNotPlacedNotOnTrack, CRMConstants.YesCancelChangedMindNotPlacedNoTrack,
			 CRMConstants.NeedMoreHelpYesCancelChangedMindNotPlacedNoTrack, CRMConstants.YesNeedMoreHelpYesCancelChangedMindNotPlacedNoTrack, CRMConstants.CALLYesNeedMoreHelpYesCancelChangedMindNotPlacedNoTrack}); // Leaf Done

	   flow.put("flow3", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ChangedMyMindOrderNotPlacedNotOnTrack, CRMConstants.YesCancelChangedMindNotPlacedNoTrack,
			 CRMConstants.NeedMoreHelpYesCancelChangedMindNotPlacedNoTrack, CRMConstants.NoNeedMoreHelpYesCancelChangedMindNotPlacedNoTrack}); // Leaf Done

	   flow.put("flow4", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ChangedMyMindOrderNotPlacedNotOnTrack, CRMConstants.YesCancelChangedMindNotPlacedNoTrack,
			   CRMConstants.NoDoneYesCancelChangedMindNotPlacedNoTrack}); // Leaf Done

	 	flow.put("flow5", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ChangedMyMindOrderNotPlacedNotOnTrack, CRMConstants.NoCancelChangedMindNotPlacedNoTrack,
	      CRMConstants.YesNeedMoreHelpNoCancelChangedMindNotPlacedNoTrack, CRMConstants.CHATYesNeedMoreHelpNoCancelChangedMindNotPlacedNoTrack}); // Leaf Done

	 	flow.put("flow6", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ChangedMyMindOrderNotPlacedNotOnTrack, CRMConstants.NoCancelChangedMindNotPlacedNoTrack,
			 CRMConstants.YesNeedMoreHelpNoCancelChangedMindNotPlacedNoTrack, CRMConstants.CALLYesNeedMoreHelpNoCancelChangedMindNotPlacedNoTrack}); // Leaf Done

	 	flow.put("flow7", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ChangedMyMindOrderNotPlacedNotOnTrack, CRMConstants.NoCancelChangedMindNotPlacedNoTrack,
	 		CRMConstants.NoDoneNoCancelChangedMindNotPlacedNoTrack}); // Leaf Done


	 // Changed my mind Not Placed and On Track
	 flow.put("flow8", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ChangedMyMindOrderNotPlacedOnTrack, CRMConstants.YesCancelChangedMindNotPlacedOnTrack,
			 CRMConstants.NeedMoreHelpYesCancelChangedMindNotPlacedOnTrack, CRMConstants.YesNeedMoreHelpYesCancelChangedMindNotPlacedOnTrack, CRMConstants.CHATYesNeedMoreHelpYesCancelChangedMindNotPlacedOnTrack}); // Leaf Done

	 flow.put("flow9", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ChangedMyMindOrderNotPlacedOnTrack, CRMConstants.YesCancelChangedMindNotPlacedOnTrack,
			 CRMConstants.NeedMoreHelpYesCancelChangedMindNotPlacedOnTrack, CRMConstants.YesNeedMoreHelpYesCancelChangedMindNotPlacedOnTrack, CRMConstants.CALLYesNeedMoreHelpYesCancelChangedMindNotPlacedOnTrack}); // Leaf Done

	 flow.put("flow10", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ChangedMyMindOrderNotPlacedOnTrack, CRMConstants.YesCancelChangedMindNotPlacedOnTrack,
			 CRMConstants.NeedMoreHelpYesCancelChangedMindNotPlacedOnTrack, CRMConstants.NoNeedMoreHelpYesCancelChangedMindNotPlacedOnTrack}); // Leaf Done

	 flow.put("flow11", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ChangedMyMindOrderNotPlacedOnTrack, CRMConstants.YesCancelChangedMindNotPlacedOnTrack,
			 CRMConstants.NoDoneYesCancelChangedMindNotPlacedOnTrack}); // Leaf Done

	 flow.put("flow12", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ChangedMyMindOrderNotPlacedOnTrack, CRMConstants.NoCancelChangedMindNotPlacedOnTrack,
			 CRMConstants.YesNeedMoreHelpNoCancelChangedMindNotPlacedOnTrack, CRMConstants.CHATYesNeedMoreHelpNoCancelChangedMindNotPlacedOnTrack}); // Leaf Done

	 flow.put("flow13", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ChangedMyMindOrderNotPlacedOnTrack, CRMConstants.NoCancelChangedMindNotPlacedOnTrack,
			 CRMConstants.YesNeedMoreHelpNoCancelChangedMindNotPlacedOnTrack, CRMConstants.CALLYesNeedMoreHelpNoCancelChangedMindNotPlacedOnTrack}); // Leaf Done

	 flow.put("flow14", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ChangedMyMindOrderNotPlacedOnTrack, CRMConstants.NoCancelChangedMindNotPlacedOnTrack,
			 CRMConstants.NoDoneNoCancelChangedMindNotPlacedOnTrack}); // Leaf Done


	 // Changed my mind Placed and Not On Track
	 flow.put("flow15", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ChangedMyMindOrderPlacedNotOnTrack, CRMConstants.YesCancelChangedMindPlacedNoTrack,
			 CRMConstants.NeedMoreHelpYesCancelChangedMindPlacedNoTrack, CRMConstants.YesNeedMoreHelpYesCancelChangedMindPlacedNoTrack, CRMConstants.CHATYesNeedMoreHelpYesCancelChangedMindPlacedNoTrack}); // Leaf Done

	 flow.put("flow16", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ChangedMyMindOrderPlacedNotOnTrack, CRMConstants.YesCancelChangedMindPlacedNoTrack,
			 CRMConstants.NeedMoreHelpYesCancelChangedMindPlacedNoTrack, CRMConstants.YesNeedMoreHelpYesCancelChangedMindPlacedNoTrack, CRMConstants.CALLYesNeedMoreHelpYesCancelChangedMindPlacedNoTrack}); // Leaf Done

	 flow.put("flow17", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ChangedMyMindOrderPlacedNotOnTrack, CRMConstants.YesCancelChangedMindPlacedNoTrack,
			 CRMConstants.NeedMoreHelpYesCancelChangedMindPlacedNoTrack, CRMConstants.NoNeedMoreHelpYesCancelChangedMindPlacedNoTrack}); // Leaf Done

	 flow.put("flow18", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ChangedMyMindOrderPlacedNotOnTrack, CRMConstants.YesCancelChangedMindPlacedNoTrack,
			 CRMConstants.NoDoneYesCancelChangedMindPlacedNoTrack}); // Leaf Done

	 flow.put("flow19", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ChangedMyMindOrderPlacedNotOnTrack, CRMConstants.NoCancelChangedMindPlacedNoTrack,
			 CRMConstants.YesNeedMoreHelpNoCancelChangedMindPlacedNoTrack, CRMConstants.CHATYesNeedMoreHelpNoCancelChangedMindPlacedNoTrack}); // Leaf Done

	 flow.put("flow20", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ChangedMyMindOrderPlacedNotOnTrack, CRMConstants.NoCancelChangedMindPlacedNoTrack,
			 CRMConstants.YesNeedMoreHelpNoCancelChangedMindPlacedNoTrack, CRMConstants.CALLYesNeedMoreHelpNoCancelChangedMindPlacedNoTrack}); // Leaf Done

	 flow.put("flow21", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ChangedMyMindOrderPlacedNotOnTrack, CRMConstants.NoCancelChangedMindPlacedNoTrack,
			 CRMConstants.NoDoneNoCancelChangedMindPlacedNoTrack}); // Leaf Done


	 // Changed my mind Placed and On Track
	 flow.put("flow22", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ChangedMyMindOrderPlacedOnTrack, CRMConstants.YesCancelChangedMindPlacedOnTrack,
			 CRMConstants.NeedMoreHelpYesCancelChangedMindPlacedOnTrack, CRMConstants.CancellationFeeYesCancelChangedMindPlacedOnTrack, CRMConstants.CHATCancellationFeeYesCancelChangedMindPlacedOnTrack}); // Leaf Done

	 flow.put("flow23", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ChangedMyMindOrderPlacedOnTrack, CRMConstants.YesCancelChangedMindPlacedOnTrack,
			 CRMConstants.NeedMoreHelpYesCancelChangedMindPlacedOnTrack, CRMConstants.CancellationFeeYesCancelChangedMindPlacedOnTrack, CRMConstants.CALLCancellationFeeYesCancelChangedMindPlacedOnTrack}); // Leaf Done

	 flow.put("flow24", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ChangedMyMindOrderPlacedOnTrack, CRMConstants.YesCancelChangedMindPlacedOnTrack,
			 CRMConstants.NeedMoreHelpYesCancelChangedMindPlacedOnTrack, CRMConstants.OtherIssueChangedMindPlacedOnTrack, CRMConstants.CHATOtherIssueYesCancelChangedMindPlacedOnTrack}); // Leaf Done

	 flow.put("flow25", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ChangedMyMindOrderPlacedOnTrack, CRMConstants.YesCancelChangedMindPlacedOnTrack,
			 CRMConstants.NeedMoreHelpYesCancelChangedMindPlacedOnTrack, CRMConstants.OtherIssueChangedMindPlacedOnTrack, CRMConstants.CALLOtherIssueYesCancelChangedMindPlacedOnTrack}); // Leaf Done


	 // Changed my mind pickedup and On Track
	 flow.put("flow100", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ChangedMyMindOrderPickedupOnTrack,CRMConstants.DontCancelChangedMindPickedupOnTrack,
			 CRMConstants.DontCancelNeedHelpChangedMindPickedupOnTrack,CRMConstants.DontCancelNeedHelpContinueChatChangedMindPickedupOnTrack});

	 flow.put("flow101", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ChangedMyMindOrderPickedupOnTrack,CRMConstants.DontCancelChangedMindPickedupOnTrack,
			 CRMConstants.DontCancelNoHelpChangedMindPickedupOnTrack});

	 flow.put("flow102", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ChangedMyMindOrderPickedupOnTrack,CRMConstants.YesCancelChangedMindPickedupOnTrack,
			 CRMConstants.YesCancelNoHelpChangedMindPickedupOnTrack});

	 // Changed my mind pickedup and not on Track
	 flow.put("flow103", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ChangedMyMindOrderPickedupNotOnTrack,CRMConstants.DontCancelChangedMindPickedupNotOnTrack,
			 CRMConstants.DontCancelNeedHelpChangedMindPickedupNotOnTrack,CRMConstants.DontCancelNeedHelpContinueChatChangedMindPickedupNotOnTrack});

	 flow.put("flow104", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ChangedMyMindOrderPickedupNotOnTrack,CRMConstants.DontCancelChangedMindPickedupNotOnTrack,
			 CRMConstants.DontCancelNoHelpChangedMindPickedupNotOnTrack});

	 flow.put("flow105", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ChangedMyMindOrderPickedupNotOnTrack,CRMConstants.YesCancelChangedMindPickedupNotOnTrack,
			 CRMConstants.YesCancelNoHelpChangedMindPickedupNotOnTrack});


	 // Incorrect Address Not Placed and Not On Track
	 flow.put("flow43", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.IncorrectAddOrderNotPlacedNotOnTrack, CRMConstants.YesCancelIncorrectAddNotPlacedNoTrack,
			 CRMConstants.NeedMoreHelpYesCancelIncorrectAddNotPlacedNoTrack, CRMConstants.YesNeedMoreHelpYesCancelIncorrectAddNotPlacedNoTrack, CRMConstants.CHATYesNeedMoreHelpYesCancelIncorrectAddNotPlacedNoTrack}); // Leaf Done

	 flow.put("flow44", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.IncorrectAddOrderNotPlacedNotOnTrack, CRMConstants.YesCancelIncorrectAddNotPlacedNoTrack,
			 CRMConstants.NeedMoreHelpYesCancelIncorrectAddNotPlacedNoTrack, CRMConstants.YesNeedMoreHelpYesCancelIncorrectAddNotPlacedNoTrack, CRMConstants.CALLYesNeedMoreHelpYesCancelIncorrectAddNotPlacedNoTrack}); // Leaf Done

	 flow.put("flow45", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.IncorrectAddOrderNotPlacedNotOnTrack, CRMConstants.YesCancelIncorrectAddNotPlacedNoTrack,
			 CRMConstants.NeedMoreHelpYesCancelIncorrectAddNotPlacedNoTrack, CRMConstants.NoNeedMoreHelpYesCancelIncorrectAddNotPlacedNoTrack}); // Leaf Done

	 flow.put("flow46", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.IncorrectAddOrderNotPlacedNotOnTrack, CRMConstants.YesCancelIncorrectAddNotPlacedNoTrack,
			 CRMConstants.NoDoneYesCancelIncorrectAddNotPlacedNoTrack}); // Leaf Done

	 flow.put("flow47", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.IncorrectAddOrderNotPlacedNotOnTrack, CRMConstants.NoCancelIncorrectAddNotPlacedNoTrack,
			 CRMConstants.YesNeedMoreHelpNoCancelIncorrectAddNotPlacedNoTrack, CRMConstants.CHATYesNeedMoreHelpNoCancelIncorrectAddNotPlacedNoTrack}); // Leaf Done

	 flow.put("flow48", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.IncorrectAddOrderNotPlacedNotOnTrack, CRMConstants.NoCancelIncorrectAddNotPlacedNoTrack,
			 CRMConstants.YesNeedMoreHelpNoCancelIncorrectAddNotPlacedNoTrack, CRMConstants.CALLYesNeedMoreHelpNoCancelIncorrectAddNotPlacedNoTrack}); // Leaf Done

	 flow.put("flow49", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.IncorrectAddOrderNotPlacedNotOnTrack, CRMConstants.NoCancelIncorrectAddNotPlacedNoTrack,
			 CRMConstants.NoDoneNoCancelIncorrectAddNotPlacedNoTrack}); // Leaf Done


     // Incorrect Address Not Placed and On Track
	 flow.put("flow50", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.IncorrectAddOrderNotPlacedOnTrack, CRMConstants.YesCancelIncorrectAddNotPlacedOnTrack,
			 CRMConstants.NeedMoreHelpYesCancelIncorrectAddNotPlacedOnTrack, CRMConstants.YesNeedMoreHelpYesCancelIncorrectAddNotPlacedOnTrack, CRMConstants.CHATYesNeedMoreHelpYesCancelIncorrectAddNotPlacedOnTrack}); // Leaf Done

	 flow.put("flow51", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.IncorrectAddOrderNotPlacedOnTrack, CRMConstants.YesCancelIncorrectAddNotPlacedOnTrack,
			 CRMConstants.NeedMoreHelpYesCancelIncorrectAddNotPlacedOnTrack, CRMConstants.YesNeedMoreHelpYesCancelIncorrectAddNotPlacedOnTrack, CRMConstants.CALLYesNeedMoreHelpYesCancelIncorrectAddNotPlacedOnTrack}); // Leaf Done

	 flow.put("flow52", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.IncorrectAddOrderNotPlacedOnTrack, CRMConstants.YesCancelIncorrectAddNotPlacedOnTrack,
			 CRMConstants.NeedMoreHelpYesCancelIncorrectAddNotPlacedOnTrack, CRMConstants.NoNeedMoreHelpYesCancelIncorrectAddNotPlacedOnTrack}); // Leaf Done

	 flow.put("flow53", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.IncorrectAddOrderNotPlacedOnTrack, CRMConstants.YesCancelIncorrectAddNotPlacedOnTrack,
			 CRMConstants.NoDoneYesCancelIncorrectAddNotPlacedOnTrack}); // Leaf Done

	 flow.put("flow54", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.IncorrectAddOrderNotPlacedOnTrack, CRMConstants.NoCancelIncorrectAddNotPlacedOnTrack,
			 CRMConstants.YesNeedMoreHelpNoCancelIncorrectAddNotPlacedOnTrack, CRMConstants.CHATYesNeedMoreHelpNoCancelIncorrectAddNotPlacedOnTrack}); // Leaf Done

	 flow.put("flow55", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.IncorrectAddOrderNotPlacedOnTrack, CRMConstants.NoCancelIncorrectAddNotPlacedOnTrack,
			 CRMConstants.YesNeedMoreHelpNoCancelIncorrectAddNotPlacedOnTrack, CRMConstants.CALLYesNeedMoreHelpNoCancelIncorrectAddNotPlacedOnTrack}); // Leaf Done

	 flow.put("flow56", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.IncorrectAddOrderNotPlacedOnTrack, CRMConstants.NoCancelIncorrectAddNotPlacedOnTrack,
			 CRMConstants.NoDoneNoCancelIncorrectAddNotPlacedOnTrack}); // Leaf Done


     // Incorrect Address Placed and Not On Track
	 flow.put("flow57", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.IncorrectAddOrderPlacedNotOnTrack, CRMConstants.YesCancelIncorrectAddPlacedNoTrack,
			 CRMConstants.NeedMoreHelpYesCancelIncorrectAddPlacedNoTrack, CRMConstants.YesNeedMoreHelpYesCancelIncorrectAddPlacedNoTrack, CRMConstants.CHATYesNeedMoreHelpYesCancelIncorrectAddPlacedNoTrack}); // Leaf Done

	 flow.put("flow58", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.IncorrectAddOrderPlacedNotOnTrack, CRMConstants.YesCancelIncorrectAddPlacedNoTrack,
			 CRMConstants.NeedMoreHelpYesCancelIncorrectAddPlacedNoTrack, CRMConstants.YesNeedMoreHelpYesCancelIncorrectAddPlacedNoTrack, CRMConstants.CALLYesNeedMoreHelpYesCancelIncorrectAddPlacedNoTrack}); // Leaf Done

	 flow.put("flow59", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.IncorrectAddOrderPlacedNotOnTrack, CRMConstants.YesCancelIncorrectAddPlacedNoTrack,
			 CRMConstants.NeedMoreHelpYesCancelIncorrectAddPlacedNoTrack, CRMConstants.NoNeedMoreHelpYesCancelIncorrectAddPlacedNoTrack}); // Leaf Done

	 flow.put("flow60", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.IncorrectAddOrderPlacedNotOnTrack, CRMConstants.YesCancelIncorrectAddPlacedNoTrack,
			 CRMConstants.NoDoneYesCancelIncorrectAddPlacedNoTrack}); // Leaf Done

	 flow.put("flow61", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.IncorrectAddOrderPlacedNotOnTrack, CRMConstants.NoCancelIncorrectAddPlacedNoTrack,
			 CRMConstants.YesNeedMoreHelpNoCancelIncorrectAddPlacedNoTrack, CRMConstants.CHATYesNeedMoreHelpNoCancelIncorrectAddPlacedNoTrack}); // Leaf Done

	 flow.put("flow62", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.IncorrectAddOrderPlacedNotOnTrack, CRMConstants.NoCancelIncorrectAddPlacedNoTrack,
			 CRMConstants.YesNeedMoreHelpNoCancelIncorrectAddPlacedNoTrack, CRMConstants.CALLYesNeedMoreHelpNoCancelIncorrectAddPlacedNoTrack}); // Leaf Done

	 flow.put("flow63", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.IncorrectAddOrderPlacedNotOnTrack, CRMConstants.NoCancelIncorrectAddPlacedNoTrack,
			 CRMConstants.NoDoneNoCancelIncorrectAddPlacedNoTrack}); // Leaf Done


	 // TODO need add flows for one more disposition and Pickedup status


	 // Placed By Mistake Not Placed and Not On Track

	 flow.put("flow64", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.PlacedByMistakeOrderNotPlacedNotOnTrack, CRMConstants.YesCancelPlacedByMistakeNotPlacedNoTrack,
			 CRMConstants.NeedMoreHelpYesCancelPlacedByMistakeNotPlacedNoTrack, CRMConstants.YesNeedMoreHelpYesCancelPlacedByMistakeNotPlacedNoTrack, CRMConstants.CHATYesNeedMoreHelpYesCancelPlacedByMistakeNotPlacedNoTrack}); // Leaf Done

	 flow.put("flow65", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.PlacedByMistakeOrderNotPlacedNotOnTrack, CRMConstants.YesCancelPlacedByMistakeNotPlacedNoTrack,
			 CRMConstants.NeedMoreHelpYesCancelPlacedByMistakeNotPlacedNoTrack, CRMConstants.YesNeedMoreHelpYesCancelPlacedByMistakeNotPlacedNoTrack, CRMConstants.CALLYesNeedMoreHelpYesCancelPlacedByMistakeNotPlacedNoTrack}); // Leaf Done

	 flow.put("flow66", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.PlacedByMistakeOrderNotPlacedNotOnTrack, CRMConstants.YesCancelPlacedByMistakeNotPlacedNoTrack,
			 CRMConstants.NeedMoreHelpYesCancelPlacedByMistakeNotPlacedNoTrack, CRMConstants.NoNeedMoreHelpYesCancelPlacedByMistakeNotPlacedNoTrack}); // Leaf Done

	 flow.put("flow67", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.PlacedByMistakeOrderNotPlacedNotOnTrack, CRMConstants.YesCancelPlacedByMistakeNotPlacedNoTrack,
			 CRMConstants.NoDoneYesCancelPlacedByMistakeNotPlacedNoTrack}); // Leaf Done

	 flow.put("flow68", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.PlacedByMistakeOrderNotPlacedNotOnTrack, CRMConstants.NoCancelPlacedByMistakeNotPlacedNoTrack,
			 CRMConstants.YesNeedMoreHelpNoCancelPlacedByMistakeNotPlacedNoTrack, CRMConstants.CHATYesNeedMoreHelpNoCancelPlacedByMistakeNotPlacedNoTrack}); // Leaf Done

	 flow.put("flow69", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.PlacedByMistakeOrderNotPlacedNotOnTrack, CRMConstants.NoCancelPlacedByMistakeNotPlacedNoTrack,
			 CRMConstants.YesNeedMoreHelpNoCancelPlacedByMistakeNotPlacedNoTrack, CRMConstants.CALLYesNeedMoreHelpNoCancelPlacedByMistakeNotPlacedNoTrack}); // Leaf Done

	 flow.put("flow70", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.PlacedByMistakeOrderNotPlacedNotOnTrack, CRMConstants.NoCancelPlacedByMistakeNotPlacedNoTrack,
			 CRMConstants.NoDoneNoCancelPlacedByMistakeNotPlacedNoTrack}); // Leaf Done


	 // Placed By Mistake Not Placed and On Track

	 flow.put("flow71", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.PlacedByMistakeOrderNotPlacedOnTrack, CRMConstants.YesCancelPlacedByMistakeNotPlacedOnTrack,
			 CRMConstants.NeedMoreHelpYesCancelPlacedByMistakeNotPlacedOnTrack, CRMConstants.YesNeedMoreHelpYesCancelPlacedByMistakeNotPlacedOnTrack, CRMConstants.CHATYesNeedMoreHelpYesCancelPlacedByMistakeNotPlacedOnTrack}); // Leaf Done

	 flow.put("flow72", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.PlacedByMistakeOrderNotPlacedOnTrack, CRMConstants.YesCancelPlacedByMistakeNotPlacedOnTrack,
			 CRMConstants.NeedMoreHelpYesCancelPlacedByMistakeNotPlacedOnTrack, CRMConstants.YesNeedMoreHelpYesCancelPlacedByMistakeNotPlacedOnTrack, CRMConstants.CALLYesNeedMoreHelpYesCancelPlacedByMistakeNotPlacedOnTrack}); // Leaf Done

	 flow.put("flow73", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.PlacedByMistakeOrderNotPlacedOnTrack, CRMConstants.YesCancelPlacedByMistakeNotPlacedOnTrack,
			 CRMConstants.NeedMoreHelpYesCancelPlacedByMistakeNotPlacedOnTrack, CRMConstants.NoNeedMoreHelpYesCancelPlacedByMistakeNotPlacedOnTrack}); // Leaf Done

	 flow.put("flow74", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.PlacedByMistakeOrderNotPlacedOnTrack, CRMConstants.YesCancelPlacedByMistakeNotPlacedOnTrack,
			 CRMConstants.NoDoneYesCancelPlacedByMistakeNotPlacedOnTrack}); // Leaf Done

	 flow.put("flow75", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.PlacedByMistakeOrderNotPlacedOnTrack, CRMConstants.NoCancelPlacedByMistakeNotPlacedOnTrack,
			 CRMConstants.YesNeedMoreHelpNoCancelPlacedByMistakeNotPlacedOnTrack, CRMConstants.CHATYesNeedMoreHelpNoCancelPlacedByMistakeNotPlacedOnTrack}); // Leaf Done

	 flow.put("flow76", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.PlacedByMistakeOrderNotPlacedOnTrack, CRMConstants.NoCancelPlacedByMistakeNotPlacedOnTrack,
			 CRMConstants.YesNeedMoreHelpNoCancelPlacedByMistakeNotPlacedOnTrack, CRMConstants.CALLYesNeedMoreHelpNoCancelPlacedByMistakeNotPlacedOnTrack}); // Leaf Done

	 flow.put("flow77", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.PlacedByMistakeOrderNotPlacedOnTrack, CRMConstants.NoCancelPlacedByMistakeNotPlacedOnTrack,
			 CRMConstants.NoDoneNoCancelPlacedByMistakeNotPlacedOnTrack}); // Leaf Done


	 // Placed By Mistake Placed and Not On Track

	 flow.put("flow78", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.PlacedByMistakeOrderPlacedNotOnTrack, CRMConstants.YesCancelPlacedByMistakePlacedNoTrack,
			 CRMConstants.NeedMoreHelpYesCancelPlacedByMistakePlacedNoTrack, CRMConstants.YesNeedMoreHelpYesCancelPlacedByMistakePlacedNoTrack, CRMConstants.CHATYesNeedMoreHelpYesCancelPlacedByMistakePlacedNoTrack}); // Leaf Done

	 flow.put("flow79", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.PlacedByMistakeOrderPlacedNotOnTrack, CRMConstants.YesCancelPlacedByMistakePlacedNoTrack,
			 CRMConstants.NeedMoreHelpYesCancelPlacedByMistakePlacedNoTrack, CRMConstants.YesNeedMoreHelpYesCancelPlacedByMistakePlacedNoTrack, CRMConstants.CALLYesNeedMoreHelpYesCancelPlacedByMistakePlacedNoTrack}); // Leaf Done

	 flow.put("flow80", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.PlacedByMistakeOrderPlacedNotOnTrack, CRMConstants.YesCancelPlacedByMistakePlacedNoTrack,
			 CRMConstants.NeedMoreHelpYesCancelPlacedByMistakePlacedNoTrack, CRMConstants.NoNeedMoreHelpYesCancelPlacedByMistakePlacedNoTrack}); // Leaf Done

	 flow.put("flow81", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.PlacedByMistakeOrderPlacedNotOnTrack, CRMConstants.YesCancelPlacedByMistakePlacedNoTrack,
			 CRMConstants.NoDoneYesCancelPlacedByMistakePlacedNoTrack}); // Leaf Done

	 flow.put("flow82", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.PlacedByMistakeOrderPlacedNotOnTrack, CRMConstants.NoCancelPlacedByMistakePlacedNoTrack,
			 CRMConstants.YesNeedMoreHelpNoCancelPlacedByMistakePlacedNoTrack, CRMConstants.CHATYesNeedMoreHelpNoCancelPlacedByMistakePlacedNoTrack}); // Leaf Done

	 flow.put("flow83", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.PlacedByMistakeOrderPlacedNotOnTrack, CRMConstants.NoCancelPlacedByMistakePlacedNoTrack,
			 CRMConstants.YesNeedMoreHelpNoCancelPlacedByMistakePlacedNoTrack, CRMConstants.CALLYesNeedMoreHelpNoCancelPlacedByMistakePlacedNoTrack}); // Leaf Done

	 flow.put("flow84", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.PlacedByMistakeOrderPlacedNotOnTrack, CRMConstants.NoCancelPlacedByMistakePlacedNoTrack,
			 CRMConstants.NoDoneNoCancelPlacedByMistakePlacedNoTrack}); // Leaf Done


	 // TODO need add flows for one more disposition and Pickedup status


	 // Order Delayed Not Placed and Not On Track

	 flow.put("flow85", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.OrderDelayedOrderNotPlacedNotOnTrack, CRMConstants.YesCancelOrderDelayedNotPlacedNoTrack,
			 CRMConstants.NeedMoreHelpYesCancelOrderDelayedNotPlacedNoTrack, CRMConstants.YesNeedMoreHelpYesCancelOrderDelayedNotPlacedNoTrack, CRMConstants.CHATYesNeedMoreHelpYesCancelOrderDelayedNotPlacedNoTrack}); // Leaf Done

	 flow.put("flow86", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.OrderDelayedOrderNotPlacedNotOnTrack, CRMConstants.YesCancelOrderDelayedNotPlacedNoTrack,
			 CRMConstants.NeedMoreHelpYesCancelOrderDelayedNotPlacedNoTrack, CRMConstants.YesNeedMoreHelpYesCancelOrderDelayedNotPlacedNoTrack, CRMConstants.CALLYesNeedMoreHelpYesCancelOrderDelayedNotPlacedNoTrack}); // Leaf Done

	 flow.put("flow87", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.OrderDelayedOrderNotPlacedNotOnTrack, CRMConstants.YesCancelOrderDelayedNotPlacedNoTrack,
			 CRMConstants.NeedMoreHelpYesCancelOrderDelayedNotPlacedNoTrack, CRMConstants.NoNeedMoreHelpYesCancelOrderDelayedNotPlacedNoTrack}); // Leaf Done

	 flow.put("flow88", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.OrderDelayedOrderNotPlacedNotOnTrack, CRMConstants.YesCancelOrderDelayedNotPlacedNoTrack,
			 CRMConstants.NoDoneYesCancelOrderDelayedNotPlacedNoTrack}); // Leaf Done

	 flow.put("flow89", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.OrderDelayedOrderNotPlacedNotOnTrack, CRMConstants.NoCancelOrderDelayedNotPlacedNoTrack,
			 CRMConstants.YesNeedMoreHelpNoCancelOrderDelayedNotPlacedNoTrack, CRMConstants.CHATYesNeedMoreHelpNoCancelOrderDelayedNotPlacedNoTrack}); // Leaf Done

	 flow.put("flow90", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.OrderDelayedOrderNotPlacedNotOnTrack, CRMConstants.NoCancelOrderDelayedNotPlacedNoTrack,
			 CRMConstants.YesNeedMoreHelpNoCancelOrderDelayedNotPlacedNoTrack, CRMConstants.CALLYesNeedMoreHelpNoCancelOrderDelayedNotPlacedNoTrack}); // Leaf Done

	 flow.put("flow91", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.OrderDelayedOrderNotPlacedNotOnTrack, CRMConstants.NoCancelOrderDelayedNotPlacedNoTrack,
			 CRMConstants.NoDoneNoCancelOrderDelayedNotPlacedNoTrack}); // Leaf Done


	 // Order Delayed Placed and Not On Track
	 flow.put("flow92", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.OrderDelayedOrderPlacedNotOnTrack, CRMConstants.YesCancelOrderDelayedPlacedNoTrack,
			 CRMConstants.NeedMoreHelpYesCancelOrderDelayedPlacedNoTrack, CRMConstants.YesNeedMoreHelpYesCancelOrderDelayedPlacedNoTrack, CRMConstants.CHATYesNeedMoreHelpYesCancelOrderDelayedPlacedNoTrack}); // Leaf Done

	 flow.put("flow93", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.OrderDelayedOrderPlacedNotOnTrack, CRMConstants.YesCancelOrderDelayedPlacedNoTrack,
			 CRMConstants.NeedMoreHelpYesCancelOrderDelayedPlacedNoTrack, CRMConstants.YesNeedMoreHelpYesCancelOrderDelayedPlacedNoTrack, CRMConstants.CALLYesNeedMoreHelpYesCancelOrderDelayedPlacedNoTrack}); // Leaf Done

	 flow.put("flow94", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.OrderDelayedOrderPlacedNotOnTrack, CRMConstants.YesCancelOrderDelayedPlacedNoTrack,
			 CRMConstants.NeedMoreHelpYesCancelOrderDelayedPlacedNoTrack, CRMConstants.NoNeedMoreHelpYesCancelOrderDelayedPlacedNoTrack}); // Leaf Done

	 flow.put("flow95", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.OrderDelayedOrderPlacedNotOnTrack, CRMConstants.YesCancelOrderDelayedPlacedNoTrack,
			 CRMConstants.NoDoneYesCancelOrderDelayedPlacedNoTrack}); // Leaf Done

	 flow.put("flow96", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.OrderDelayedOrderPlacedNotOnTrack, CRMConstants.NoCancelOrderDelayedPlacedNoTrack,
			 CRMConstants.YesNeedMoreHelpNoCancelOrderDelayedPlacedNoTrack, CRMConstants.CHATYesNeedMoreHelpNoCancelOrderDelayedPlacedNoTrack}); // Leaf Done

	 flow.put("flow97", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.OrderDelayedOrderPlacedNotOnTrack, CRMConstants.NoCancelOrderDelayedPlacedNoTrack,
			 CRMConstants.YesNeedMoreHelpNoCancelOrderDelayedPlacedNoTrack, CRMConstants.CALLYesNeedMoreHelpNoCancelOrderDelayedPlacedNoTrack}); // Leaf Done

	 flow.put("flow98", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.OrderDelayedOrderPlacedNotOnTrack, CRMConstants.NoCancelOrderDelayedPlacedNoTrack,
			 CRMConstants.NoDoneNoCancelOrderDelayedPlacedNoTrack}); // Leaf Done


	 // Incorrect Address Placed and  On Track -- Swati
	 flow.put("flow106", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.IncorrectAddOrderPlacedOnTrack, CRMConstants.NoCancelIncorrectAddPlacedOnTrack,
			 CRMConstants.YesNeedMoreHelpNoCancelIncorrectAddPlacedOnTrack, CRMConstants.CHATYesNeedMoreHelpNoCancelIncorrectAddPlacedOnTrack}); // Leaf Done

	 flow.put("flow107", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.IncorrectAddOrderPlacedOnTrack, CRMConstants.NoCancelIncorrectAddPlacedOnTrack,
			 CRMConstants.YesNeedMoreHelpNoCancelIncorrectAddPlacedOnTrack, CRMConstants.CALLYesNeedMoreHelpNoCancelIncorrectAddPlacedOnTrack}); // Leaf Done

	 flow.put("flow108", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.IncorrectAddOrderPlacedOnTrack, CRMConstants.NoCancelIncorrectAddPlacedOnTrack,
			 CRMConstants.NoDoneNoCancelIncorrectAddPlacedOnTrack}); // Leaf Done

	 flow.put("flow109", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.IncorrectAddOrderPlacedOnTrack, CRMConstants.YesCancelIncorrectAddPlacedOnTrack,
			 CRMConstants.NoDoneYesCancelIncorrectAddPlacedOnTrack}); // Leaf Done

	 flow.put("flow110", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.IncorrectAddOrderPlacedOnTrack, CRMConstants.YesCancelIncorrectAddPlacedOnTrack,
			 CRMConstants.NoDoneYesCancelIncorrectAddPlacedOnTrack}); // Leaf Done

	 flow.put("flow111", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.IncorrectAddOrderPlacedOnTrack, CRMConstants.YesCancelIncorrectAddPlacedOnTrack,
			 CRMConstants.NeedMoreHelpYesCancelIncorrectAddPlacedOnTrack, CRMConstants.CancelFeeIssueYesCancelIncorrectAddPlacedOnTrack, CRMConstants.ContinueChatCancelFeeIssueYesCancelIncorrectAddPlacedOnTrack}); // Leaf Done

	 flow.put("flow112", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.IncorrectAddOrderPlacedOnTrack, CRMConstants.YesCancelIncorrectAddPlacedOnTrack,
			 CRMConstants.NeedMoreHelpYesCancelIncorrectAddPlacedOnTrack, CRMConstants.CancelFeeIssueYesCancelIncorrectAddPlacedOnTrack, CRMConstants.CallUsCancelFeeIssueYesCancelIncorrectAddPlacedOnTrack}); // Leaf Done

	 flow.put("flow113", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.IncorrectAddOrderPlacedOnTrack, CRMConstants.YesCancelIncorrectAddPlacedOnTrack,
			 CRMConstants.NeedMoreHelpYesCancelIncorrectAddPlacedOnTrack, CRMConstants.OtherIssueYesCancelIncorrectAddPlacedOnTrack, CRMConstants.ContinueChatOtherIssueYesCancelIncorrectAddPlacedOnTrack}); // Leaf Done

	 flow.put("flow114", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.IncorrectAddOrderPlacedOnTrack, CRMConstants.YesCancelIncorrectAddPlacedOnTrack,
			 CRMConstants.NeedMoreHelpYesCancelIncorrectAddPlacedOnTrack, CRMConstants.OtherIssueYesCancelIncorrectAddPlacedOnTrack, CRMConstants.CallUsOtherIssueYesCancelIncorrectAddPlacedOnTrack}); // Leaf Done


// Placed By Mistake Placed and  On Track -- swati

	 flow.put("flow115", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.PlacedByMistakeOrderPlacedOnTrack, CRMConstants.NoCancelPlacedByMistakePlacedOnTrack,
			 CRMConstants.YesNeedMoreHelpNoCancelPlacedByMistakePlacedOnTrack, CRMConstants.CHATYesNeedMoreHelpNoCancelPlacedByMistakePlacedOnTrack}); // Leaf Done

	 flow.put("flow116", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.PlacedByMistakeOrderPlacedOnTrack, CRMConstants.NoCancelPlacedByMistakePlacedOnTrack,
			 CRMConstants.YesNeedMoreHelpNoCancelPlacedByMistakePlacedOnTrack, CRMConstants.CALLYesNeedMoreHelpNoCancelPlacedByMistakePlacedOnTrack}); // Leaf Done

	 flow.put("flow117", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.PlacedByMistakeOrderPlacedOnTrack, CRMConstants.NoCancelPlacedByMistakePlacedOnTrack,
			 CRMConstants.NoDoneNoCancelPlacedByMistakePlacedOnTrack}); // Leaf Done

	 flow.put("flow118", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.PlacedByMistakeOrderPlacedOnTrack, CRMConstants.YesCancelPlacedByMistakePlacedOnTrack,
			 CRMConstants.NoDoneYesCancelPlacedByMistakePlacedOnTrack}); // Leaf Done

	 flow.put("flow119", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.PlacedByMistakeOrderPlacedOnTrack, CRMConstants.YesCancelPlacedByMistakePlacedOnTrack,
			 CRMConstants.NeedMoreHelpYesCancelPlacedByMistakePlacedOnTrack, CRMConstants.CancellationFeeNeedMoreHelpYesCancelPlacedByMistakePlacedOnTrack, CRMConstants.CHATCancellationFeeNeedMoreHelpYesCancelPlacedByMistakePlacedOnTrack}); // Leaf Done

	 flow.put("flow120", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.PlacedByMistakeOrderPlacedOnTrack, CRMConstants.YesCancelPlacedByMistakePlacedOnTrack,
			 CRMConstants.NeedMoreHelpYesCancelPlacedByMistakePlacedOnTrack, CRMConstants.CancellationFeeNeedMoreHelpYesCancelPlacedByMistakePlacedOnTrack, CRMConstants.CALLCancellationFeeNeedMoreHelpYesCancelPlacedByMistakePlacedOnTrack}); // Leaf Done

	 flow.put("flow121", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.PlacedByMistakeOrderPlacedOnTrack, CRMConstants.YesCancelPlacedByMistakePlacedOnTrack,
			 CRMConstants.NeedMoreHelpYesCancelPlacedByMistakePlacedOnTrack, CRMConstants.OtherIssuesNeedMoreHelpYesCancelPlacedByMistakePlacedOnTrack, CRMConstants.CHATOtherIssuesNeedMoreHelpYesCancelPlacedByMistakePlacedOnTrack}); // Leaf Done

	 flow.put("flow122", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.PlacedByMistakeOrderPlacedOnTrack, CRMConstants.YesCancelPlacedByMistakePlacedOnTrack,
			 CRMConstants.NeedMoreHelpYesCancelPlacedByMistakePlacedOnTrack, CRMConstants.OtherIssuesNeedMoreHelpYesCancelPlacedByMistakePlacedOnTrack, CRMConstants.CALLOtherIssuesNeedMoreHelpYesCancelPlacedByMistakePlacedOnTrack}); // Leaf Done

// Expected Faster Delivery Placed and On Track - Swati
	 flow.put("flow123", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ExpectedFasterOrderPlacedOnTrack, CRMConstants.YesCancelExpectedFasterPlacedOnTrack,
			 CRMConstants.NoDoneYesCancelExpectedFasterPlacedOnTrack}); // Leaf Done

	 flow.put("flow124", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ExpectedFasterOrderPlacedOnTrack, CRMConstants.NoCancelExpectedFasterPlacedOnTrack,
			 CRMConstants.YesNeedMoreHelpNoCancelExpectedFasterPlacedOnTrack, CRMConstants.CHATYesNeedMoreHelpNoCancelExpectedFasterPlacedOnTrack}); // Leaf Done

	 flow.put("flow125", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ExpectedFasterOrderPlacedOnTrack, CRMConstants.NoCancelExpectedFasterPlacedOnTrack,
			 CRMConstants.YesNeedMoreHelpNoCancelExpectedFasterPlacedOnTrack, CRMConstants.CALLYesNeedMoreHelpNoCancelExpectedFasterPlacedOnTrack}); // Leaf Done

	 flow.put("flow126", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ExpectedFasterOrderPlacedOnTrack, CRMConstants.NoCancelExpectedFasterPlacedOnTrack,
			 CRMConstants.NoDoneNoCancelExpectedFasterPlacedOnTrack}); // Leaf Done

	 flow.put("flow127", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ExpectedFasterOrderPlacedOnTrack, CRMConstants.YesCancelExpectedFasterPlacedOnTrack,
			 CRMConstants.NeedMoreHelpYesCancelExpectedFasterPlacedOnTrack, CRMConstants.CancellationFessNeedMoreHelpYesCancelExpectedFasterPlacedOnTrack, CRMConstants.CHATCancellationFessNeedMoreHelpYesCancelExpectedFasterPlacedOnTrack}); // Leaf Done

	 flow.put("flow128", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ExpectedFasterOrderPlacedOnTrack, CRMConstants.YesCancelExpectedFasterPlacedOnTrack,
			 CRMConstants.NeedMoreHelpYesCancelExpectedFasterPlacedOnTrack, CRMConstants.CancellationFessNeedMoreHelpYesCancelExpectedFasterPlacedOnTrack, CRMConstants.CALLCancellationFessNeedMoreHelpYesCancelExpectedFasterPlacedOnTrack});

	 flow.put("flow129", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ExpectedFasterOrderPlacedOnTrack, CRMConstants.YesCancelExpectedFasterPlacedOnTrack,
			 CRMConstants.NeedMoreHelpYesCancelExpectedFasterPlacedOnTrack, CRMConstants.OtherIssuesNeedMoreHelpYesCancelExpectedFasterPlacedOnTrack, CRMConstants.CHATOtherIssuesNeedMoreHelpYesCancelExpectedFasterPlacedOnTrack});

	 flow.put("flow130", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ExpectedFasterOrderPlacedOnTrack, CRMConstants.YesCancelExpectedFasterPlacedOnTrack,
			 CRMConstants.NeedMoreHelpYesCancelExpectedFasterPlacedOnTrack, CRMConstants.OtherIssuesNeedMoreHelpYesCancelExpectedFasterPlacedOnTrack, CRMConstants.CALLOtherIssuesNeedMoreHelpYesCancelExpectedFasterPlacedOnTrack});

// Incorrect Address Pickedup and On Track - Swati
	 flow.put("flow131", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ExpectedFasterOrderPlacedOnTrack, CRMConstants.YesCancelIncorrectAddPickedupOnTrack,
			 CRMConstants.NoDoneYesCancelIncorrectAddPickedupOnTrack}); // Leaf Done

	 flow.put("flow132", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.IncorrectAddOrderPickedupOnTrack, CRMConstants.NoCancelIncorrectAddPickedupOnTrack,
			 CRMConstants.YesNeedMoreHelpNoCancelIncorrectAddPickedupOnTrack, CRMConstants.CHATYesNeedMoreHelpNoCancelIncorrectAddPickedupOnTrack}); // Leaf Done

	 flow.put("flow133", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.IncorrectAddOrderPickedupOnTrack, CRMConstants.NoCancelIncorrectAddPickedupOnTrack,
			 CRMConstants.YesNeedMoreHelpNoCancelIncorrectAddPickedupOnTrack, CRMConstants.CALLYesNeedMoreHelpNoCancelIncorrectAddPickedupOnTrack}); // Leaf Done

	 flow.put("flow134", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.IncorrectAddOrderPickedupOnTrack, CRMConstants.NoCancelIncorrectAddPickedupOnTrack,
			 CRMConstants.NoDoneNoCancelIncorrectAddPickedupOnTrack}); // Leaf Done

	 flow.put("flow135", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ExpectedFasterOrderPlacedOnTrack, CRMConstants.YesCancelIncorrectAddPickedupOnTrack,
			 CRMConstants.NeedMoreHelpYesCancelIncorrectAddPickedupOnTrack, CRMConstants.CancellationFeeNeedMoreHelpYesCancelIncorrectAddPickedupOnTrack, CRMConstants.CHATCancellationFeeNeedMoreHelpYesCancelIncorrectAddPickedupOnTrack}); // Leaf Done

	 flow.put("flow136", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ExpectedFasterOrderPlacedOnTrack, CRMConstants.YesCancelIncorrectAddPickedupOnTrack,
			 CRMConstants.NeedMoreHelpYesCancelIncorrectAddPickedupOnTrack, CRMConstants.CancellationFeeNeedMoreHelpYesCancelIncorrectAddPickedupOnTrack, CRMConstants.CALLCancellationFeeNeedMoreHelpYesCancelIncorrectAddPickedupOnTrack}); // Leaf Done

	 flow.put("flow137", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ExpectedFasterOrderPlacedOnTrack, CRMConstants.YesCancelIncorrectAddPickedupOnTrack,
			 CRMConstants.NeedMoreHelpYesCancelIncorrectAddPickedupOnTrack, CRMConstants.OtherIssuesNeedMoreHelpYesCancelIncorrectAddPickedupOnTrack, CRMConstants.CHATOtherIssuesNeedMoreHelpYesCancelIncorrectAddPickedupOnTrack}); // Leaf Done

	 flow.put("flow138", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ExpectedFasterOrderPlacedOnTrack, CRMConstants.YesCancelIncorrectAddPickedupOnTrack,
			 CRMConstants.NeedMoreHelpYesCancelIncorrectAddPickedupOnTrack, CRMConstants.OtherIssuesNeedMoreHelpYesCancelIncorrectAddPickedupOnTrack, CRMConstants.CALLOtherIssuesNeedMoreHelpYesCancelIncorrectAddPickedupOnTrack}); // Leaf Done

// Placed By Mistake Pickedup and On Track - Swati
	 flow.put("flow139", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.PlacedByMistakeOrderPickedupOnTrack, CRMConstants.YesCancelPlacedByMistakePickedupOnTrack,
			 CRMConstants.NoDoneYesCancelPlacedByMistakePickedupOnTrack}); // Leaf Done

	 flow.put("flow140", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.PlacedByMistakeOrderPickedupOnTrack, CRMConstants.NoCancelPlacedByMistakePickedupOnTrack,
			 CRMConstants.YesNeedMoreHelpNoCancelPlacedByMistakePickedupOnTrack, CRMConstants.CHATYesNeedMoreHelpNoCancelPlacedByMistakePickedupOnTrack}); // Leaf Done

	 flow.put("flow141", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.PlacedByMistakeOrderPickedupOnTrack, CRMConstants.NoCancelPlacedByMistakePickedupOnTrack,
			 CRMConstants.YesNeedMoreHelpNoCancelPlacedByMistakePickedupOnTrack, CRMConstants.CALLYesNeedMoreHelpNoCancelPlacedByMistakePickedupOnTrack}); // Leaf Done

	 flow.put("flow142", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.PlacedByMistakeOrderPickedupOnTrack, CRMConstants.NoCancelPlacedByMistakePickedupOnTrack,
			 CRMConstants.NoDoneNoCancelPlacedByMistakePickedupOnTrack}); // Leaf Done

	 flow.put("flow143", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.PlacedByMistakeOrderPickedupOnTrack, CRMConstants.YesCancelPlacedByMistakePickedupOnTrack,
			 CRMConstants.NeedMoreHelpYesCancelPlacedByMistakePickedupOnTrack, CRMConstants.CancellationFeeNeedMoreHelpYesCancelPlacedByMistakePickedupOnTrack, CRMConstants.CHATCancellationFeeNeedMoreHelpYesCancelPlacedByMistakePickedupOnTrack}); // Leaf Done

	 flow.put("flow144", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.PlacedByMistakeOrderPickedupOnTrack, CRMConstants.YesCancelPlacedByMistakePickedupOnTrack,
			 CRMConstants.NeedMoreHelpYesCancelPlacedByMistakePickedupOnTrack, CRMConstants.CancellationFeeNeedMoreHelpYesCancelPlacedByMistakePickedupOnTrack, CRMConstants.CALLCancellationFeeNeedMoreHelpYesCancelPlacedByMistakePickedupOnTrack}); // Leaf Done

	 flow.put("flow145", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.PlacedByMistakeOrderPickedupOnTrack, CRMConstants.YesCancelPlacedByMistakePickedupOnTrack,
			 CRMConstants.NeedMoreHelpYesCancelPlacedByMistakePickedupOnTrack, CRMConstants.OtherIssueNeedMoreHelpYesCancelPlacedByMistakePickedupOnTrack, CRMConstants.CHATOtherIssueNeedMoreHelpYesCancelPlacedByMistakePickedupOnTrack}); // Leaf Done

	 flow.put("flow146", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.PlacedByMistakeOrderPickedupOnTrack, CRMConstants.YesCancelPlacedByMistakePickedupOnTrack,
			 CRMConstants.NeedMoreHelpYesCancelPlacedByMistakePickedupOnTrack, CRMConstants.OtherIssueNeedMoreHelpYesCancelPlacedByMistakePickedupOnTrack, CRMConstants.CALLOtherIssueNeedMoreHelpYesCancelPlacedByMistakePickedupOnTrack}); // Leaf Done


// Expected faster Delivery Pickedup and On Track - Swati
	 flow.put("flow147", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ExpectedFasterOrderPickedupOnTrack, CRMConstants.YesCancelExpectedFasterPickedupOnTrack,
			 CRMConstants.NoDoneYesCancelExpectedFasterPickedupOnTrack}); // Leaf Done

	 flow.put("flow148", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ExpectedFasterOrderPickedupOnTrack, CRMConstants.NoCancelExpectedFasterPickedupOnTrack,
			 CRMConstants.YesNeedMoreHelpNoCancelExpectedFasterPickedupOnTrack, CRMConstants.CHATYesNeedMoreHelpNoCancelExpectedFasterPickedupOnTrack}); // Leaf Done

	 flow.put("flow149", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ExpectedFasterOrderPickedupOnTrack, CRMConstants.NoCancelExpectedFasterPickedupOnTrack,
			 CRMConstants.YesNeedMoreHelpNoCancelExpectedFasterPickedupOnTrack, CRMConstants.CALLYesNeedMoreHelpNoCancelExpectedFasterPickedupOnTrack}); // Leaf Done

	 flow.put("flow150", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ExpectedFasterOrderPickedupOnTrack, CRMConstants.NoCancelExpectedFasterPickedupOnTrack,
			 CRMConstants.NoDoneNoCancelExpectedFasterPickedupOnTrack}); // Leaf Done

	 flow.put("flow151", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ExpectedFasterOrderPickedupOnTrack, CRMConstants.YesCancelExpectedFasterPickedupOnTrack,
			 CRMConstants.NeedMoreHelpYesCancelExpectedFasterPickedupOnTrack, CRMConstants.CancellationFeeNeedMoreHelpYesCancelExpectedFasterPickedupOnTrack, CRMConstants.CHATCancellationFeeNeedMoreHelpYesCancelExpectedFasterPickedupOnTrack}); // Leaf Done

	 flow.put("flow152", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ExpectedFasterOrderPickedupOnTrack, CRMConstants.YesCancelExpectedFasterPickedupOnTrack,
			 CRMConstants.NeedMoreHelpYesCancelExpectedFasterPickedupOnTrack, CRMConstants.CancellationFeeNeedMoreHelpYesCancelExpectedFasterPickedupOnTrack, CRMConstants.CALLCancellationFeeNeedMoreHelpYesCancelExpectedFasterPickedupOnTrack}); // Leaf Done

	 flow.put("flow153", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ExpectedFasterOrderPickedupOnTrack, CRMConstants.YesCancelExpectedFasterPickedupOnTrack,
			 CRMConstants.NeedMoreHelpYesCancelExpectedFasterPickedupOnTrack, CRMConstants.OtherIssuesNeedMoreHelpYesCancelExpectedFasterPickedupOnTrack, CRMConstants.CHATOtherIssuesNeedMoreHelpYesCancelExpectedFasterPickedupOnTrack}); // Leaf Done

	 flow.put("flow154", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ExpectedFasterOrderPickedupOnTrack, CRMConstants.YesCancelExpectedFasterPickedupOnTrack,
			 CRMConstants.NeedMoreHelpYesCancelExpectedFasterPickedupOnTrack, CRMConstants.OtherIssuesNeedMoreHelpYesCancelExpectedFasterPickedupOnTrack, CRMConstants.CALLOtherIssuesNeedMoreHelpYesCancelExpectedFasterPickedupOnTrack}); // Leaf Done

	 // Expected faster delivery Not Placed and onTrack
	 flow.put("flow155", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ExpectedFasterOrderNotPlacedOnTrack, CRMConstants.YesCancelExpectedFasterOrderNotPlacedOnTrack,
			 CRMConstants.NeedMoreHelpYesCancelExpectedFasterOrderNotPlacedOnTrack, CRMConstants.NeedMoreHelpNeedMoreHelpYesCancelExpectedFasterOrderNotPlacedOnTrack, CRMConstants.CHATNeedMoreHelpYesCancelExpectedFasterOrderNotPlacedOnTrack}); // Leaf Done

	 flow.put("flow156", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ExpectedFasterOrderNotPlacedOnTrack, CRMConstants.YesCancelExpectedFasterOrderNotPlacedOnTrack,
			 CRMConstants.NeedMoreHelpYesCancelExpectedFasterOrderNotPlacedOnTrack, CRMConstants.NeedMoreHelpNeedMoreHelpYesCancelExpectedFasterOrderNotPlacedOnTrack, CRMConstants.CALLNeedMoreHelpYesCancelExpectedFasterOrderNotPlacedOnTrack}); // Leaf Done

	 flow.put("flow157", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ExpectedFasterOrderNotPlacedOnTrack, CRMConstants.YesCancelExpectedFasterOrderNotPlacedOnTrack,
			 CRMConstants.NeedMoreHelpYesCancelExpectedFasterOrderNotPlacedOnTrack, CRMConstants.NoDoneNeedMoreHelpYesCancelExpectedFasterOrderNotPlacedOnTrack}); // Leaf Done

	 flow.put("flow158", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ExpectedFasterOrderNotPlacedOnTrack, CRMConstants.YesCancelExpectedFasterOrderNotPlacedOnTrack,
			 CRMConstants.NoDoneYesCancelExpectedFasterOrderNotPlacedOnTrack}); // Leaf Done

	 flow.put("flow159", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ExpectedFasterOrderNotPlacedOnTrack, CRMConstants.NoCancelExpectedFasterOrderNotPlacedOnTrack,
			 CRMConstants.NoDoneNoCancelExpectedFasterOrderNotPlacedOnTrack}); // Leaf Done

	 flow.put("flow160", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ExpectedFasterOrderNotPlacedOnTrack, CRMConstants.NoCancelExpectedFasterOrderNotPlacedOnTrack,
			 CRMConstants.NeedMoreHelpNoCancelExpectedFasterOrderNotPlacedOnTrack, CRMConstants.CHATNeedMoreHelpNoCancelExpectedFasterOrderNotPlacedOnTrack}); // Leaf Done

	 flow.put("flow161", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.OrderToCancel, CRMConstants.ExpectedFasterOrderNotPlacedOnTrack, CRMConstants.NoCancelExpectedFasterOrderNotPlacedOnTrack,
			 CRMConstants.NeedMoreHelpNoCancelExpectedFasterOrderNotPlacedOnTrack, CRMConstants.CALLNeedMoreHelpNoCancelExpectedFasterOrderNotPlacedOnTrack}); // Leaf Done


	 // Take Away Order To Cancel
	 flow.put("flow170",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.TakeAwayOrderToCancel, CRMConstants.TakeawayChangedMyMind,
			 CRMConstants.YesCancelTakeawayChangedMyMind, CRMConstants.NeedMoreHelpYesCancelTakeawayChangedMyMind, CRMConstants.CHATNeedMoreHelpYesCancelTakeawayChangedMyMind});
	 flow.put("flow171",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.TakeAwayOrderToCancel, CRMConstants.TakeawayChangedMyMind,
			 CRMConstants.YesCancelTakeawayChangedMyMind, CRMConstants.NeedMoreHelpYesCancelTakeawayChangedMyMind, CRMConstants.CALLNeedMoreHelpYesCancelTakeawayChangedMyMind});
	 flow.put("flow172",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.TakeAwayOrderToCancel, CRMConstants.TakeawayChangedMyMind,
			 CRMConstants.YesCancelTakeawayChangedMyMind, CRMConstants.NoDoneYesCancelTakeawayChangedMyMind});
	 flow.put("flow173",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.TakeAwayOrderToCancel, CRMConstants.TakeawayChangedMyMind,
			 CRMConstants.NoCancelTakeawayChangedMyMind, CRMConstants.NeedMoreHelpNoCancelTakeawayChangedMyMind, CRMConstants.CHATNeedMoreHelpNoCancelTakeawayChangedMyMind});
	 flow.put("flow174",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.TakeAwayOrderToCancel, CRMConstants.TakeawayChangedMyMind,
			 CRMConstants.NoCancelTakeawayChangedMyMind, CRMConstants.NeedMoreHelpNoCancelTakeawayChangedMyMind, CRMConstants.CALLNeedMoreHelpNoCancelTakeawayChangedMyMind});
	 flow.put("flow175",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.TakeAwayOrderToCancel, CRMConstants.TakeawayChangedMyMind,
			 CRMConstants.NoCancelTakeawayChangedMyMind, CRMConstants.NoDoneNoCancelTakeawayChangedMyMind});

	 flow.put("flow176",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.TakeAwayOrderToCancel, CRMConstants.TakeawayUnableToPickup,
			 CRMConstants.YesCancelTakeawayUnableToPickup, CRMConstants.NeedMoreHelpYesCancelTakeawayUnableToPickup, CRMConstants.CHATNeedMoreHelpYesCancelTakeawayUnableToPickup});
	 flow.put("flow177",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.TakeAwayOrderToCancel, CRMConstants.TakeawayUnableToPickup,
			 CRMConstants.YesCancelTakeawayUnableToPickup, CRMConstants.NeedMoreHelpYesCancelTakeawayUnableToPickup, CRMConstants.CALLNeedMoreHelpYesCancelTakeawayUnableToPickup});
	 flow.put("flow178",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.TakeAwayOrderToCancel, CRMConstants.TakeawayUnableToPickup,
			 CRMConstants.YesCancelTakeawayUnableToPickup, CRMConstants.NoDoneYesCancelTakeawayUnableToPickup});
	 flow.put("flow179",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.TakeAwayOrderToCancel, CRMConstants.TakeawayUnableToPickup,
			 CRMConstants.NoCancelTakeawayUnableToPickup, CRMConstants.NeedMoreHelpNoCancelTakeawayUnableToPickup, CRMConstants.CHATNeedMoreHelpNoCancelTakeawayUnableToPickup});
	 flow.put("flow180",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.TakeAwayOrderToCancel, CRMConstants.TakeawayUnableToPickup,
			 CRMConstants.NoCancelTakeawayUnableToPickup, CRMConstants.NeedMoreHelpNoCancelTakeawayUnableToPickup, CRMConstants.CALLNeedMoreHelpNoCancelTakeawayUnableToPickup});
	 flow.put("flow181",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.TakeAwayOrderToCancel, CRMConstants.TakeawayUnableToPickup,
			 CRMConstants.NoCancelTakeawayUnableToPickup, CRMConstants.NoDoneNoCancelTakeawayUnableToPickup});

	 flow.put("flow182",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.TakeAwayOrderToCancel, CRMConstants.TakeawayPlacedByMistake,
			 CRMConstants.YesCancelTakeawayPlacedByMistake, CRMConstants.NeedMoreHelpYesCancelTakeawayPlacedByMistake, CRMConstants.CHATNeedMoreHelpYesCancelTakeawayPlacedByMistake});
	 flow.put("flow183",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.TakeAwayOrderToCancel, CRMConstants.TakeawayPlacedByMistake,
			 CRMConstants.YesCancelTakeawayPlacedByMistake, CRMConstants.NeedMoreHelpYesCancelTakeawayPlacedByMistake, CRMConstants.CALLNeedMoreHelpYesCancelTakeawayPlacedByMistake});
	 flow.put("flow184",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.TakeAwayOrderToCancel, CRMConstants.TakeawayPlacedByMistake,
			 CRMConstants.YesCancelTakeawayPlacedByMistake, CRMConstants.NoDoneYesCancelTakeawayPlacedByMistake});
	 flow.put("flow185",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.TakeAwayOrderToCancel, CRMConstants.TakeawayPlacedByMistake,
			 CRMConstants.NoCancelTakeawayPlacedByMistake, CRMConstants.NeedMoreHelpNoCancelTakeawayPlacedByMistake, CRMConstants.CHATNeedMoreHelpNoCancelTakeawayPlacedByMistake});
	 flow.put("flow186",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.TakeAwayOrderToCancel, CRMConstants.TakeawayPlacedByMistake,
			 CRMConstants.NoCancelTakeawayPlacedByMistake, CRMConstants.NeedMoreHelpNoCancelTakeawayPlacedByMistake, CRMConstants.CALLNeedMoreHelpNoCancelTakeawayPlacedByMistake});
	 flow.put("flow187",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.TakeAwayOrderToCancel, CRMConstants.TakeawayPlacedByMistake,
			 CRMConstants.NoCancelTakeawayPlacedByMistake, CRMConstants.NoDoneNoCancelTakeawayPlacedByMistake});

	 flow.put("flow188",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.TakeAwayOrderToCancel, CRMConstants.TakeawayOrderDelayed,
			 CRMConstants.YesCancelTakeawayOrderDelayed, CRMConstants.NeedMoreHelpYesCancelTakeawayOrderDelayed, CRMConstants.CHATNeedMoreHelpYesCancelTakeawayOrderDelayed});
	 flow.put("flow189",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.TakeAwayOrderToCancel, CRMConstants.TakeawayOrderDelayed,
			 CRMConstants.YesCancelTakeawayOrderDelayed, CRMConstants.NeedMoreHelpYesCancelTakeawayOrderDelayed, CRMConstants.CALLNeedMoreHelpYesCancelTakeawayOrderDelayed});
	 flow.put("flow190",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.TakeAwayOrderToCancel, CRMConstants.TakeawayOrderDelayed,
			 CRMConstants.YesCancelTakeawayOrderDelayed, CRMConstants.NoDoneYesCancelTakeawayOrderDelayed});
	 flow.put("flow191",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.TakeAwayOrderToCancel, CRMConstants.TakeawayOrderDelayed,
			 CRMConstants.NoCancelTakeawayOrderDelayed, CRMConstants.NeedMoreHelpNoCancelTakeawayOrderDelayed, CRMConstants.CHATNeedMoreHelpNoCancelTakeawayOrderDelayed});
	 flow.put("flow192",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.TakeAwayOrderToCancel, CRMConstants.TakeawayOrderDelayed,
			 CRMConstants.NoCancelTakeawayOrderDelayed, CRMConstants.NeedMoreHelpNoCancelTakeawayOrderDelayed, CRMConstants.CALLNeedMoreHelpNoCancelTakeawayOrderDelayed});
	 flow.put("flow193",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.TakeAwayOrderToCancel, CRMConstants.TakeawayOrderDelayed,
			 CRMConstants.NoCancelTakeawayOrderDelayed, CRMConstants.NoDoneNoCancelTakeawayOrderDelayed});

	 //Complete cafe flows
     flow.put("cafe1",new String[]{CRMConstants.Help,CRMConstants.OrderIssues, CRMConstants.cafeCancel, CRMConstants.cafeChangedMyMind, CRMConstants.cafeYesCancelChangedMind, CRMConstants.cafeNeedInfoChangedMindYes});
     flow.put("cafe2",new String[]{CRMConstants.Help,CRMConstants.OrderIssues, CRMConstants.cafeCancel, CRMConstants.cafeChangedMyMind, CRMConstants.cafeYesCancelChangedMind, CRMConstants.cafeDoneChangedYes});

     flow.put("cafe3",new String[]{CRMConstants.Help,CRMConstants.OrderIssues, CRMConstants.cafeCancel, CRMConstants.cafeChangedMyMind, CRMConstants.cafeNotCancelChangedMind, CRMConstants.cafeNeedInfoChangedMindNot});
     flow.put("cafe4",new String[]{CRMConstants.Help,CRMConstants.OrderIssues, CRMConstants.cafeCancel, CRMConstants.cafeChangedMyMind, CRMConstants.cafeNotCancelChangedMind, CRMConstants.cafeDoneChangedNot});

     flow.put("cafe5",new String[]{CRMConstants.Help,CRMConstants.OrderIssues, CRMConstants.cafeCancel, CRMConstants.cafeItemsNotAvailable,CRMConstants.cafeItemsNotAvailableYesCancel,CRMConstants.cafeItemsNotAvailableYesHelp});
	 flow.put("cafe6",new String[]{CRMConstants.Help,CRMConstants.OrderIssues, CRMConstants.cafeCancel, CRMConstants.cafeItemsNotAvailable,CRMConstants.cafeItemsNotAvailableYesCancel,CRMConstants.cafeItemsNotAvailableYesNoHelp});
	 flow.put("cafe7",new String[]{CRMConstants.Help,CRMConstants.OrderIssues, CRMConstants.cafeCancel, CRMConstants.cafeItemsNotAvailable,CRMConstants.cafeItemsNotAvailableDontCancel,CRMConstants.cafeItemsNotAvailableDontCancelHelp});
	 flow.put("cafe8",new String[]{CRMConstants.Help,CRMConstants.OrderIssues, CRMConstants.cafeCancel, CRMConstants.cafeItemsNotAvailable,CRMConstants.cafeItemsNotAvailableDontCancel,CRMConstants.cafeItemsNotAvailableDontCancelNoHelp});
     flow.put("cafe9",new String[]{CRMConstants.Help,CRMConstants.OrderIssues, CRMConstants.cafeCancel, CRMConstants.cafeOtherIssues});

     flow.put("cafe10",new String[]{CRMConstants.Help,CRMConstants.OrderIssues, CRMConstants.cafeEdit});
     flow.put("cafe11",new String[]{CRMConstants.Help,CRMConstants.OrderIssues, CRMConstants.cafeItemsMissing});
     flow.put("cafe12",new String[]{CRMConstants.Help,CRMConstants.OrderIssues, CRMConstants.cafeBadQuality});
     flow.put("cafe13",new String[]{CRMConstants.Help,CRMConstants.OrderIssues, CRMConstants.cafeNotRcvdRefund});
     flow.put("cafe14",new String[]{CRMConstants.Help,CRMConstants.OrderIssues, CRMConstants.cafePaymentRefundQueries});
     flow.put("cafe15",new String[]{CRMConstants.Help,CRMConstants.OrderIssues, CRMConstants.cafeBuffetCancel});
     flow.put("cafe16",new String[]{CRMConstants.Help,CRMConstants.OrderIssues, CRMConstants.cafeDifferentIssue});


     //Cancellation Dominos Flow
     flow.put("dominos1", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.dominosCancel, CRMConstants.dominosCancelNoHelpDone}); // Leaf Done
     flow.put("dominos2", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.dominosCancel, CRMConstants.dominosCancelNeedHelp,CRMConstants.dominosCancelNeedHelpContinueChat});
     flow.put("dominos3", new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.dominosCancel, CRMConstants.dominosCancelNeedHelp,CRMConstants.dominosCancelNeedHelpCallUs});



     //Edit Flows

	 //Customer Initiated

	 //Edit order Before Arrived (OrderToEditBeforeArrived)
	 flow.put("edit1",new String[]{CRMConstants.OrderToEditBeforeArrived,CRMConstants.editOrderBeforeArrivedAddItems,CRMConstants.editOrderBeforeArrivedAddItemsSelectItems,CRMConstants.editOrderBeforeArrivedAddItemsAddNeedHelp,CRMConstants.editOrderBeforeArrivedAddItemsAddNeedHelpContinueChat});
	 flow.put("edit2",new String[]{CRMConstants.OrderToEditBeforeArrived,CRMConstants.editOrderBeforeArrivedAddItems,CRMConstants.editOrderBeforeArrivedAddItemsSelectItems,CRMConstants.editOrderBeforeArrivedAddItemsAddNeedHelp,CRMConstants.editOrderBeforeArrivedAddItemsAddNeedHelpCallUs});
	 flow.put("edit3",new String[]{CRMConstants.OrderToEditBeforeArrived,CRMConstants.editOrderBeforeArrivedAddItems,CRMConstants.editOrderBeforeArrivedAddItemsSelectItems,CRMConstants.editOrderBeforeArrivedAddItemsAddNoHelp});
	 flow.put("edit4",new String[]{CRMConstants.OrderToEditBeforeArrived,CRMConstants.editOrderBeforeArrivedRemoveItems});
	 flow.put("edit5",new String[]{CRMConstants.OrderToEditBeforeArrived,CRMConstants.editOrderBeforeArrivedReplaceItems});
	 flow.put("edit6",new String[]{CRMConstants.OrderToEditBeforeArrived,CRMConstants.editOrderBeforeArrivedOthers,CRMConstants.editOrderBeforeArrivedOthersNeedHelp,CRMConstants.editOrderBeforeArrivedOthersNeedHelpContinueChat});
	 flow.put("edit7",new String[]{CRMConstants.OrderToEditBeforeArrived,CRMConstants.editOrderBeforeArrivedOthers,CRMConstants.editOrderBeforeArrivedOthersNeedHelp,CRMConstants.editOrderBeforeArrivedOthersNeedHelpCallUs});
	 flow.put("edit8",new String[]{CRMConstants.OrderToEditBeforeArrived,CRMConstants.editOrderBeforeArrivedOthers,CRMConstants.editOrderBeforeArrivedOthersNoHelp});

	 //Edit order arrived (OrderToEditArrived)

	 flow.put("edit9",new String[]{CRMConstants.OrderToEditArrived, CRMConstants.editOrderArrivedNeedHelp});
	 flow.put("edit10",new String[]{CRMConstants.OrderToEditArrived, CRMConstants.editOrderArrivedNoHelp});


	 //Restaurant Initiated
	 flow.put("edit11",new String[]{CRMConstants.restaurantInitiateedit,CRMConstants.restaurantEditChooseAlternative,CRMConstants.restaurantEditChooseAlternativeSelectItems,
			 CRMConstants.restaurantEditChooseAlternativeSelectItemsDeliverWithoutItem,CRMConstants.restaurantEditChooseAlternativeSelectItemsDeliverWithoutItemConfirm,
			 CRMConstants.restaurantEditChooseAlternativeSelectItemsDeliverWithoutItemConfirmHelp,CRMConstants.restaurantEditChooseAlternativeSelectItemsDeliverWithoutItemConfirmHelpChat});

	 flow.put("edit12",new String[]{CRMConstants.restaurantInitiateedit,CRMConstants.restaurantEditChooseAlternative,CRMConstants.restaurantEditChooseAlternativeSelectItems,
			 CRMConstants.restaurantEditChooseAlternativeSelectItemsDeliverWithoutItem,CRMConstants.restaurantEditChooseAlternativeSelectItemsDeliverWithoutItemConfirm,
			 CRMConstants.restaurantEditChooseAlternativeSelectItemsDeliverWithoutItemConfirmHelp,CRMConstants.restaurantEditChooseAlternativeSelectItemsDeliverWithoutItemConfirmHelpCall});

	 flow.put("edit13",new String[]{CRMConstants.restaurantInitiateedit,CRMConstants.restaurantEditChooseAlternative,CRMConstants.restaurantEditChooseAlternativeSelectItems,
			 CRMConstants.restaurantEditChooseAlternativeSelectItemsDeliverWithoutItem,CRMConstants.restaurantEditChooseAlternativeSelectItemsDeliverWithoutItemConfirm,
			 CRMConstants.restaurantEditChooseAlternativeSelectItemsDeliverWithoutItemConfirmNoHelp});

	 flow.put("edit14",new String[]{CRMConstants.restaurantInitiateedit,CRMConstants.restaurantEditChooseAlternative,CRMConstants.restaurantEditChooseAlternativeSelectItems,
			 CRMConstants.restaurantEditChooseAlternativeSelectItemsDeliverWithoutItem,CRMConstants.restaurantEditChooseAlternativeSelectItemsDeliverWithoutItemHelp});

	 flow.put("edit15",new String[]{CRMConstants.restaurantInitiateedit,CRMConstants.restaurantEditChooseAlternative,CRMConstants.restaurantEditChooseAlternativeSelectItems,
			 CRMConstants.restaurantEditChooseAlternativeSelectItemsCancelOrder,CRMConstants.restaurantEditChooseAlternativeSelectItemsCancelOrderYes,
			 CRMConstants.restaurantEditChooseAlternativeSelectItemsCancelOrderYesHelp,CRMConstants.restaurantEditChooseAlternativeSelectItemsCancelOrderYesChat});

	 flow.put("edit16",new String[]{CRMConstants.restaurantInitiateedit,CRMConstants.restaurantEditChooseAlternative,CRMConstants.restaurantEditChooseAlternativeSelectItems,
			 CRMConstants.restaurantEditChooseAlternativeSelectItemsCancelOrder,CRMConstants.restaurantEditChooseAlternativeSelectItemsCancelOrderYes,
			 CRMConstants.restaurantEditChooseAlternativeSelectItemsCancelOrderYesHelp,CRMConstants.restaurantEditChooseAlternativeSelectItemsCancelOrderYesCall});

	 flow.put("edit17",new String[]{CRMConstants.restaurantInitiateedit,CRMConstants.restaurantEditChooseAlternative,CRMConstants.restaurantEditChooseAlternativeSelectItems,
			 CRMConstants.restaurantEditChooseAlternativeSelectItemsCancelOrder,CRMConstants.restaurantEditChooseAlternativeSelectItemsCancelOrderYes,
			 CRMConstants.restaurantEditChooseAlternativeSelectItemsCancelOrderYesNoHelp});

	 flow.put("edit18",new String[]{CRMConstants.restaurantInitiateedit,CRMConstants.restaurantEditChooseAlternative,CRMConstants.restaurantEditChooseAlternativeSelectItems,
			 CRMConstants.restaurantEditChooseAlternativeSelectItemsCancelOrder,CRMConstants.restaurantEditChooseAlternativeSelectItemsCancelOrderHelp});

	 flow.put("edit19",new String[]{CRMConstants.restaurantInitiateedit,CRMConstants.restaurantEditChooseAlternative,CRMConstants.restaurantEditChooseAlternativeSelectItems,
			 CRMConstants.restaurantEditChooseAlternativeSelectItemsTalkToSomeone});

	 flow.put("edit20",new String[]{CRMConstants.restaurantInitiateedit,CRMConstants.restaurantEditChooseAlternative,CRMConstants.restaurantEditChooseAlternativeSelectItems,
			 CRMConstants.restaurantEditChooseAlternativeSelectItemsNeedHelp,CRMConstants.restaurantEditChooseAlternativeSelectItemsNeedHelpChat});

	 flow.put("edit21",new String[]{CRMConstants.restaurantInitiateedit,CRMConstants.restaurantEditChooseAlternative,CRMConstants.restaurantEditChooseAlternativeSelectItems,
			 CRMConstants.restaurantEditChooseAlternativeSelectItemsNeedHelp,CRMConstants.restaurantEditChooseAlternativeSelectItemsNeedHelpCall});

	 flow.put("edit22",new String[]{CRMConstants.restaurantInitiateedit,CRMConstants.restaurantEditChooseAlternative,CRMConstants.restaurantEditChooseAlternativeSelectItems,
			 CRMConstants.restaurantEditChooseAlternativeSelectItemsNoHelp});

	 flow.put("edit23",new String[]{CRMConstants.restaurantInitiateedit,CRMConstants.restaurantEditDeliverWithoutItems,CRMConstants.restaurantEditDeliverWithoutItemsConfirm,
	 		 CRMConstants.restaurantEditDeliverWithoutItemsConfirmNeedHelp,CRMConstants.restaurantEditDeliverWithoutItemsConfirmNeedHelpContinueChat});

	 flow.put("edit24",new String[]{CRMConstants.restaurantInitiateedit,CRMConstants.restaurantEditDeliverWithoutItems,CRMConstants.restaurantEditDeliverWithoutItemsConfirm,
			 CRMConstants.restaurantEditDeliverWithoutItemsConfirmNeedHelp,CRMConstants.restaurantEditDeliverWithoutItemsConfirmNeedHelpCallUs});

	 flow.put("edit25",new String[]{CRMConstants.restaurantInitiateedit,CRMConstants.restaurantEditDeliverWithoutItems,CRMConstants.restaurantEditDeliverWithoutItemsNeedHelp});

	 flow.put("edit26",new String[]{CRMConstants.restaurantInitiateedit,CRMConstants.restaurantEditCancelTheOrder,CRMConstants.restaurantEditCancelTheOrderYesCancel,
	 		 CRMConstants.restaurantEditCancelTheOrderYesCancelNeedHelp,CRMConstants.restaurantEditCancelTheOrderYesCancelNeedHelpContinueChat});

	 flow.put("edit27",new String[]{CRMConstants.restaurantInitiateedit,CRMConstants.restaurantEditCancelTheOrder,CRMConstants.restaurantEditCancelTheOrderYesCancel,
			 CRMConstants.restaurantEditCancelTheOrderYesCancelNeedHelp,CRMConstants.restaurantEditCancelTheOrderYesCancelNeedHelpCallUs});

	 flow.put("edit28",new String[]{CRMConstants.restaurantInitiateedit,CRMConstants.restaurantEditCancelTheOrder,CRMConstants.restaurantEditCancelTheOrderYesCancel,
			 CRMConstants.restaurantEditCancelTheOrderYesCancelNoHelp});

	 flow.put("edit29",new String[]{CRMConstants.restaurantInitiateedit,CRMConstants.restaurantEditCancelTheOrder,CRMConstants.restaurantEditCancelTheOrderTalkToSomeone});

	 // COD check
	 flow.put("cod_flow1",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.CheckCodEnabledAfterPlacingOrder, CRMConstants.NeedMoreHelpCheckCodEnabledAfterPlacingOrder,
	 			CRMConstants.CHATNeedMoreHelpCheckCodEnabledAfterPlacingOrder});

	 flow.put("cod_flow2",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.CheckCodEnabledAfterPlacingOrder, CRMConstants.NeedMoreHelpCheckCodEnabledAfterPlacingOrder,
			 CRMConstants.CALLNeedMoreHelpCheckCodEnabledAfterPlacingOrder});

	 flow.put("cod_flow3",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.CheckCodEnabledAfterPlacingOrder, CRMConstants.NoDoneCheckCodEnabledAfterPlacingOrder});

	 flow.put("cod_flow4",new String[]{CRMConstants.Help, CRMConstants.GeneralQueries, CRMConstants.CheckCodEnabled, CRMConstants.NeedMoreHelpCheckCodEnabled,
			 CRMConstants.CHATNeedMoreHelpCheckCodEnabled});

	 flow.put("cod_flow5",new String[]{CRMConstants.Help, CRMConstants.GeneralQueries, CRMConstants.CheckCodEnabled, CRMConstants.NeedMoreHelpCheckCodEnabled,
			 CRMConstants.CALLNeedMoreHelpCheckCodEnabled});

	 flow.put("cod_flow6",new String[]{CRMConstants.Help, CRMConstants.GeneralQueries, CRMConstants.CheckCodEnabled, CRMConstants.NoDoneCheckCodEnabled});

	 // Payments and Refund Flows
	 // Any order COD or non COD
	 flow.put("refund1",new String[]{//CRMConstants.Help,
             CRMConstants.OrderIssues, CRMConstants.PaymentsAndRefundIssues, CRMConstants.BillRelatedIssue, CRMConstants.HowWouldYouLikeToKnow});

	 // Non COD order (net banking )
	 flow.put("refund2",new String[]{//CRMConstants.Help,
             CRMConstants.OrderIssues, CRMConstants.PaymentsAndRefundIssues, CRMConstants.PaymentFailureRelatedIssue, CRMConstants.YesNeedMoreHelpPaymentFailureRelatedIssue,
			 CRMConstants.CHATYesNeedMoreHelpPaymentFailureRelatedIssue});
	 flow.put("refund3",new String[]{//CRMConstants.Help,
             CRMConstants.OrderIssues, CRMConstants.PaymentsAndRefundIssues, CRMConstants.PaymentFailureRelatedIssue, CRMConstants.YesNeedMoreHelpPaymentFailureRelatedIssue,
			 CRMConstants.CALLYesNeedMoreHelpPaymentFailureRelatedIssue});
	 flow.put("refund4",new String[]{//CRMConstants.Help,
	          CRMConstants.OrderIssues, CRMConstants.PaymentsAndRefundIssues, CRMConstants.PaymentFailureRelatedIssue, CRMConstants.NoDonePaymentFailureRelatedIssue});

	 // COD order
	 flow.put("refund5",new String[]{//CRMConstants.Help,
             CRMConstants.OrderIssues,CRMConstants.PaymentsAndRefundIssues,  CRMConstants.ChangePaymentMethod, CRMConstants.YesNeedMoreHelpChangePaymentMethod,
			 CRMConstants.CHATYesNeedMoreHelpChangePaymentMethod});
	 flow.put("refund6",new String[]{//CRMConstants.Help,
             CRMConstants.OrderIssues, CRMConstants.PaymentsAndRefundIssues, CRMConstants.ChangePaymentMethod, CRMConstants.YesNeedMoreHelpChangePaymentMethod,
			 CRMConstants.CALLYesNeedMoreHelpChangePaymentMethod});
	 flow.put("refund7",new String[]{//CRMConstants.Help,
	          CRMConstants.OrderIssues, CRMConstants.PaymentsAndRefundIssues, CRMConstants.ChangePaymentMethod, CRMConstants.NoDoneChangePaymentMethod});

	 // Non COD order (net banking )
	 flow.put("refund8",new String[]{//CRMConstants.Help,
             CRMConstants.OrderIssues, CRMConstants.PaymentsAndRefundIssues, CRMConstants.KnowRefundStatus, CRMConstants.YesNeedMoreHelpKnowRefundStatus,
			 CRMConstants.CHATYesNeedMoreHelpKnowRefundStatus});
	 flow.put("refund9",new String[]{//CRMConstants.Help,
             CRMConstants.OrderIssues, CRMConstants.PaymentsAndRefundIssues, CRMConstants.KnowRefundStatus, CRMConstants.YesNeedMoreHelpKnowRefundStatus,
			 CRMConstants.CALLYesNeedMoreHelpKnowRefundStatus});
	 flow.put("refund10",new String[]{//CRMConstants.Help,
             CRMConstants.OrderIssues, CRMConstants.PaymentsAndRefundIssues, CRMConstants.KnowRefundStatus, CRMConstants.NoDoneKnowRefundStatus});

	 // Any order COD or non COD
	 flow.put("refund11",new String[]{//CRMConstants.Help,
             CRMConstants.OrderIssues, CRMConstants.PaymentsAndRefundIssues, CRMConstants.KnowInvoiceForOrder, CRMConstants.YesNeedMoreHelpKnowInvoiceForOrder,
			 CRMConstants.CHATYesNeedMoreHelpKnowInvoiceForOrder});
	 flow.put("refund12",new String[]{//CRMConstants.Help,
             CRMConstants.OrderIssues, CRMConstants.PaymentsAndRefundIssues, CRMConstants.KnowInvoiceForOrder, CRMConstants.YesNeedMoreHelpKnowInvoiceForOrder,
			 CRMConstants.CALLYesNeedMoreHelpKnowInvoiceForOrder});
	 flow.put("refund13",new String[]{//CRMConstants.Help,
             CRMConstants.OrderIssues, CRMConstants.PaymentsAndRefundIssues, CRMConstants.KnowInvoiceForOrder, CRMConstants.NoDoneKnowInvoiceForOrder});
     flow.put("refundNegative",new String[]{//CRMConstants.Help,
             CRMConstants.OrderIssues, CRMConstants.PaymentsAndRefundIssues, CRMConstants.ChangePaymentMethod});

	 // Take Away Order
	 // Order Track
	 flow.put("takeOrderTrack1",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.TakeAwayWhereIsMyOrder, CRMConstants.YesNeedMoreHelpTakeAwayWhereIsMyOrder,
			 CRMConstants.CHATYesNeedMoreHelpTakeAwayWhereIsMyOrder});
	 flow.put("takeOrderTrack2",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.TakeAwayWhereIsMyOrder, CRMConstants.YesNeedMoreHelpTakeAwayWhereIsMyOrder,
			 CRMConstants.CALLYesNeedMoreHelpTakeAwayWhereIsMyOrder});
	 flow.put("takeOrderTrack3",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.TakeAwayWhereIsMyOrder, CRMConstants.NoDoneTakeAwayWhereIsMyOrder});

	 // Take Away edit Flow
	 flow.put("takeOrderEdit1",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.TakeAwayOrderToEdit, CRMConstants.NeedMoreHelpTakeAwayOrderToEdit,
			 CRMConstants.CHATNeedMoreHelpTakeAwayOrderToEdit});
	 flow.put("takeOrderEdit2",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.TakeAwayOrderToEdit, CRMConstants.NeedMoreHelpTakeAwayOrderToEdit,
			 CRMConstants.CALLNeedMoreHelpTakeAwayOrderToEdit});
	 flow.put("takeOrderEdit3",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.TakeAwayOrderToEdit, CRMConstants.NoDoneTakeAwayOrderToEdit});

	 // Take away Food Pre Instruction
	 flow.put("takeOrderFoodPreInst1",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.TakeAwayFoodPreInstruction, CRMConstants.NeedMoreHelpTakeAwayFoodPreInstruction,
			 CRMConstants.CHATNeedMoreHelpTakeAwayFoodPreInstruction});
	 flow.put("takeOrderFoodPreInst2",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.TakeAwayFoodPreInstruction, CRMConstants.NeedMoreHelpTakeAwayFoodPreInstruction,
			 CRMConstants.CALLNeedMoreHelpTakeAwayFoodPreInstruction});
	 flow.put("takeOrderFoodPreInst3",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.TakeAwayFoodPreInstruction, CRMConstants.NoDoneTakeAwayFoodPreInstruction});

	 // Take Away Food Issues
	 flow.put("takeOrderFoodIssues1",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.TakeAwayMissingItems, CRMConstants.NeedMoreHelpTakeAwayMissingItems,
			 CRMConstants.CHATNeedMoreHelpTakeAwayMissingItems});
	 flow.put("takeOrderFoodIssues2",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.TakeAwayMissingItems, CRMConstants.NeedMoreHelpTakeAwayMissingItems,
			 CRMConstants.CALLNeedMoreHelpTakeAwayMissingItems});
	 flow.put("takeOrderFoodIssues3",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.TakeAwayMissingItems, CRMConstants.NoDoneTakeAwayMissingItems});

	 flow.put("takeOrderFoodIssues4",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.TakeAwayPackagingIssue, CRMConstants.NeedMoreHelpTakeAwayPackagingIssue,
			 CRMConstants.CHATNeedMoreHelpTakeAwayPackagingIssue});
	 flow.put("takeOrderFoodIssues5",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.TakeAwayPackagingIssue, CRMConstants.NeedMoreHelpTakeAwayPackagingIssue,
			 CRMConstants.CALLNeedMoreHelpTakeAwayPackagingIssue});
	 flow.put("takeOrderFoodIssues6",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.TakeAwayPackagingIssue, CRMConstants.NoDoneTakeAwayPackagingIssue});

	 // DASH WorkFlows
	 flow.put("DorderCancel1",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.DashCancelOrder, CRMConstants.YedNeedMoreHelpDashCancelOrder});
	 flow.put("DorderCancel2",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.DashCancelOrder, CRMConstants.NoDoneDashCancelOrder});

	 flow.put("DOrderTrack1",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.DashOrderStatus, CRMConstants.YesNeedMoreHelpDashOrderStatus});
	 flow.put("DOrderTrack2",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.DashOrderStatus, CRMConstants.NoDoneDashOrderStatus});

	 flow.put("DOEdit1",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.DashModityItemsUntillDEArrived, CRMConstants.YedNeedMoreHelpDashModityItemsUntillDEArrived});
	 flow.put("DOEdit2",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.DashModityItemsUntillDEArrived, CRMConstants.NoDoneDashModityItemsUntillDEArrived});

	 flow.put("DOEditPostDE1",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.DashModityItemsPostDEArrived, CRMConstants.YedNeedMoreHelpDashModityItemsPostDEArrived});
	 flow.put("DOEditPostDE2",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.DashModityItemsPostDEArrived, CRMConstants.NoDoneDashModityItemsPostDEArrived});

	 flow.put("DOPayments1",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.DashPaymentAndRefundIssues, CRMConstants.YedNeedMoreHelpDashPaymentAndRefundIssues});
	 flow.put("DOPayments2",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.DashPaymentAndRefundIssues, CRMConstants.NoDoneDashPaymentAndRefundIssues});

	 flow.put("DOCoupons1",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.DashCouponsRelatedIssues, CRMConstants.YedNeedMoreHelpDashCouponsRelatedIssues});
	 flow.put("DOCoupons2",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.DashCouponsRelatedIssues, CRMConstants.NoDoneDashCouponsRelatedIssues});

	 flow.put("DOBill1",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.DashBillRelatedIssues, CRMConstants.YedNeedMoreHelpDashBillRelatedIssues});
	 flow.put("DOBill2",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.DashBillRelatedIssues, CRMConstants.NoDoneDashBillRelatedIssues});

	 flow.put("DOMarkedIncorrect1",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.DashOrderMarkedIncorrectly, CRMConstants.YedNeedMoreHelpDashOrderMarkedIncorrectly});
	 flow.put("DOMarkedIncorrect2",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.DashOrderMarkedIncorrectly, CRMConstants.NoDoneDashOrderMarkedIncorrectly});

	 flow.put("DOdamage1",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.DashItemsDamage, CRMConstants.YedNeedMoreHelpDashItemsDamage});
	 flow.put("DOdamage2",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.DashItemsDamage, CRMConstants.NoDoneDashItemsDamage});

	 flow.put("DOmissing1",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.DashItemsMissing, CRMConstants.YedNeedMoreHelpDashItemsMissing});
	 flow.put("DOmissing2",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.DashItemsMissing, CRMConstants.NoDoneDashItemsMissing});

	 flow.put("DOModifyStore1",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.DashAddModifyStore, CRMConstants.YedNeedMoreHelpDashAddModifyStore});
	 flow.put("DOModifyStore2",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.DashAddModifyStore, CRMConstants.NoDoneDashAddModifyStore});

	// Delivery Instruction Flows
	 flow.put("DEInstr01",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.LeaveDeliveryInstruction, CRMConstants.OrderDropOffInstr,
	 			CRMConstants.OrderDropOffInstrAtReception, CRMConstants.NeedMoreHelpOrderDropOffInstrAtReception});
	 flow.put("DEInstr02",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.LeaveDeliveryInstruction, CRMConstants.OrderDropOffInstr,
			 CRMConstants.OrderDropOffInstrAtReception, CRMConstants.NoDoneOrderDropOffInstrAtReception});
	 flow.put("DEInstr03",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.LeaveDeliveryInstruction, CRMConstants.OrderDropOffInstr,
			 CRMConstants.OrderDropOffInstrAtReception, CRMConstants.NeedMoreHelpOrderDropOffInstrAtSecurityGate});
	 flow.put("DEInstr04",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.LeaveDeliveryInstruction, CRMConstants.OrderDropOffInstr,
			 CRMConstants.OrderDropOffInstrAtSecurityGate, CRMConstants.NoDoneOrderDropOffInstrAtSecurityGate});
	 flow.put("DEInstr05",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.LeaveDeliveryInstruction, CRMConstants.OrderDropOffInstr,
			 CRMConstants.OrderDropOffInstrAtDifferentInstr, CRMConstants.TextOrderDropOffInstrAtDifferentInstr, CRMConstants.NeedMoreHelpTextOrderDropOffInstrAtDifferentInstr});
	 flow.put("DEInstr06",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.LeaveDeliveryInstruction, CRMConstants.OrderDropOffInstr,
			 CRMConstants.OrderDropOffInstrAtDifferentInstr, CRMConstants.TextOrderDropOffInstrAtDifferentInstr, CRMConstants.NoDoneTextOrderDropOffInstrAtDifferentInstr});


	 flow.put("DEInstr07",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.LeaveDeliveryInstruction, CRMConstants.LeaveAddressNavigationInstr,
			 CRMConstants.AddNavigationInstr, CRMConstants.YesNeedMoreHelpAddNavigationInstr});
	 flow.put("DEInstr08",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.LeaveDeliveryInstruction, CRMConstants.LeaveAddressNavigationInstr,
			 CRMConstants.AddNavigationInstr, CRMConstants.NoDoneAddNavigationInstr});
	 flow.put("DEInstr09",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.LeaveDeliveryInstruction, CRMConstants.UpdatePhoneNumber,
			 CRMConstants.UpdateYourPhoneNumber, CRMConstants.YesNeedMoreHelpUpdateYourPhoneNumber});
	 flow.put("DEInstr10",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.LeaveDeliveryInstruction, CRMConstants.UpdatePhoneNumber,
			 CRMConstants.UpdateYourPhoneNumber, CRMConstants.NoDoneUpdateYourPhoneNumber});
	 flow.put("DEInstr11",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.LeaveDeliveryInstruction, CRMConstants.RequestToGetChange,
			 CRMConstants.YesNeedMoreHelpRequestToGetChange});
	 flow.put("DEInstr12",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.LeaveDeliveryInstruction, CRMConstants.RequestToGetChange,
			 CRMConstants.NoDoneRequestToGetChange});
	 flow.put("DEInstr13",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.LeaveDeliveryInstruction, CRMConstants.UpdateDeliveryAddress,
			 CRMConstants.YesNeedMoreHelpUpdateDeliveryAddress});
	 flow.put("DEInstr14",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.LeaveDeliveryInstruction, CRMConstants.UpdateDeliveryAddress,
			 CRMConstants.NoDoneUpdateDeliveryAddress});
	 flow.put("DEInstrNegative",new String[]{CRMConstants.Help, CRMConstants.OrderIssues, CRMConstants.LeaveDeliveryInstruction});

	 // Workflows
	 flow.put("orderWorkflow",new String[]{CRMConstants.OrderToCancel, CRMConstants.OrderToEditBeforeArrived, CRMConstants.WhereIsMyOrder, CRMConstants.PaymentsAndRefundIssues,
	 		});
	 flow.put("deliveredOrderWorkflow",new String[]{CRMConstants.itemsMissing, CRMConstants.itemsDifferent, CRMConstants.packagingORspillageIssue, CRMConstants.receivedBadQuality,
	 		CRMConstants.foodQuantity, CRMConstants.IHaventReceivedMyOrder, CRMConstants.PaymentsAndRefundIssues, CRMConstants.CouponRelatedQueries});
	 flow.put("generalWorkflow",new String[]{CRMConstants.PlacingOrderGeneralIssue, CRMConstants.LoginIssueGeneralIssue, CRMConstants.PaymentAndRefundGeneralIssue,
			 CRMConstants.CouponGeneralIssue, CRMConstants.WantToUnsubcribeGeneralIssue});
	 flow.put("faqWorkflow",new String[]{CRMConstants.CanIEditOrderFaqIssue, CRMConstants.WantToCancelOrderFaqIssue, CRMConstants.QualityQuantityFaqIssue, CRMConstants.IsThereMiniumOrderValueFaqIssue,
			 CRMConstants.ChangeForDeliveryFaqIssue, CRMConstants.DeliveryFaqIssue, CRMConstants.DeliveryHoursFaqIssue,CRMConstants.CanIOrderFromAnyLocationFAQIssue, CRMConstants.SingleOrderFromManyRestroFaqIssue,
			 CRMConstants.BulkOrderFaqIssue, CRMConstants.OrderInAdvanceFaqIssue, CRMConstants.ChangeAddressNUmberFaqIssue, CRMConstants.DidNotReceivedOTPFaqIssue, CRMConstants.DidNotReceivedReferalCouponFaqIssue,
			 CRMConstants.DeactivateMyAccountFaqIssue, CRMConstants.ProfileDetailsFaqIssue, CRMConstants.SodexoFaqIssue, CRMConstants.OrderInvoiceFaqIssue, CRMConstants.RestaurantPartnerFaqIssue,
			 CRMConstants.CarrerOpportunityFaqIssue, CRMConstants.WantToProvideFeedbackFaqIssue});
	 flow.put("legalWorkflow",new String[]{CRMConstants.TermsOfUseLegalIssue, CRMConstants.PrivacyPolicyLegalIssue, CRMConstants.CancellationAndRefundLegalIssue,
			 CRMConstants.TermasOfUseForSwiggyLegalIssue});
	 flow.put("superfaqWorkflow",new String[]{CRMConstants.SwiggySuperFaq, CRMConstants.SubscribeSuperFaq, CRMConstants.SuperBenefitsSuperFAQ, CRMConstants.MembershipSuperFaq,
	 		CRMConstants.UseSuperFaq, CRMConstants.RenewSuperFaq, CRMConstants.CancelSuperMembership, CRMConstants.PaySuperFaq, CRMConstants.SuperPlanSuperFaq,
	 		CRMConstants.ChagesWaivedOffSuperFaq, CRMConstants.LimitOnOrderSuperFaq, CRMConstants.SuperOnOrderSuperFaq});
	 flow.put("dashorderWorkflow",new String[]{});

	 // Daily WorkFlow
	 flow.put("OTPreOrder",new String[]{CRMConstants.WhereIsMyOrderPreOrder});
	 flow.put("OTSubMeal",new String[]{CRMConstants.WhereIsMyOrderSubMeal});

	 flow.put("cancelPreOrder1",new String[]{CRMConstants.IWantToCancelOrderPreOrder, CRMConstants.DoNotLikeFoodCancelPreOrder,
	 		CRMConstants.YesCancelDoNotLikeFoodCancelPreOrder, CRMConstants.NeedMorehelpIsCancelDoNotLikeFoodCancelPreOrder});
	 flow.put("cancelPreOrder2",new String[]{CRMConstants.IWantToCancelOrderPreOrder, CRMConstants.DoNotLikeFoodCancelPreOrder,
			 CRMConstants.YesCancelDoNotLikeFoodCancelPreOrder, CRMConstants.NoDoneIsCancelDoNotLikeFoodCancelPreOrder});
	 flow.put("cancelPreOrder3",new String[]{CRMConstants.IWantToCancelOrderPreOrder, CRMConstants.DoNotLikeFoodCancelPreOrder,
			 CRMConstants.DontCancelDoNotLikeFoodCancelPreOrder, CRMConstants.NeedHelpDontCancelDoNotLikeFoodCancelPreOrder});
	 flow.put("cancelPreOrder4",new String[]{CRMConstants.IWantToCancelOrderPreOrder, CRMConstants.DoNotLikeFoodCancelPreOrder,
			 CRMConstants.DontCancelDoNotLikeFoodCancelPreOrder, CRMConstants.NoDoneDontCancelDoNotLikeFoodCancelPreOrder});
	 flow.put("cancelPreOrder5",new String[]{CRMConstants.IWantToCancelOrderPreOrder, CRMConstants.IncorrectAddressSelectedCancelPreOrder,
			 CRMConstants.YesCancelIncorrectAddressSelectedCancelPreOrder, CRMConstants.NeedHelpYesCancelIncorrectAddressSelectedCancelPreOrder});
	 flow.put("cancelPreOrder6",new String[]{CRMConstants.IWantToCancelOrderPreOrder, CRMConstants.IncorrectAddressSelectedCancelPreOrder,
			 CRMConstants.YesCancelIncorrectAddressSelectedCancelPreOrder, CRMConstants.NoDoneYesCancelIncorrectAddressSelectedCancelPreOrder});
	 flow.put("cancelPreOrder7",new String[]{CRMConstants.IWantToCancelOrderPreOrder, CRMConstants.IncorrectAddressSelectedCancelPreOrder,
			 CRMConstants.DontCancelIncorrectAddressSelectedCancelPreOrder, CRMConstants.NeedHelpDontCancelIncorrectAddressSelectedCancelPreOrder});
	 flow.put("cancelPreOrder8",new String[]{CRMConstants.IWantToCancelOrderPreOrder, CRMConstants.IncorrectAddressSelectedCancelPreOrder,
			 CRMConstants.DontCancelIncorrectAddressSelectedCancelPreOrder, CRMConstants.NoDoneDontCancelIncorrectAddressSelectedCancelPreOrder});
	 flow.put("cancelPreOrder9",new String[]{CRMConstants.IWantToCancelOrderPreOrder, CRMConstants.GotBoredOfFoodCancelPreOrder,
			 CRMConstants.YesCancelGotBoredOfFoodCancelPreOrder, CRMConstants.NeedHelpYesCancelGotBoredOfFoodCancelPreOrder});
	 flow.put("cancelPreOrder10",new String[]{CRMConstants.IWantToCancelOrderPreOrder, CRMConstants.GotBoredOfFoodCancelPreOrder,
			 CRMConstants.YesCancelGotBoredOfFoodCancelPreOrder, CRMConstants.NoDoneYesCancelGotBoredOfFoodCancelPreOrder});
	 flow.put("cancelPreOrder11",new String[]{CRMConstants.IWantToCancelOrderPreOrder, CRMConstants.GotBoredOfFoodCancelPreOrder,
			 CRMConstants.DontCancelGotBoredOfFoodCancelPreOrder, CRMConstants.NeedHelpDontCancelGotBoredOfFoodCancelPreOrder});
	 flow.put("cancelPreOrder12",new String[]{CRMConstants.IWantToCancelOrderPreOrder, CRMConstants.GotBoredOfFoodCancelPreOrder,
			 CRMConstants.DontCancelGotBoredOfFoodCancelPreOrder, CRMConstants.NoDoneDontCancelGotBoredOfFoodCancelPreOrder});
	 flow.put("cancelPreOrder13",new String[]{CRMConstants.IWantToCancelOrderPreOrder, CRMConstants.OthersPreOrder,
			 CRMConstants.YesCancelOthersPreOrder, CRMConstants.NeedHelpYesCancelOthersPreOrder});
	 flow.put("cancelPreOrder14",new String[]{CRMConstants.IWantToCancelOrderPreOrder, CRMConstants.OthersPreOrder,
			 CRMConstants.YesCancelOthersPreOrder, CRMConstants.NoDoneYesCancelOthersPreOrder});
	 flow.put("cancelPreOrder15",new String[]{CRMConstants.IWantToCancelOrderPreOrder, CRMConstants.OthersPreOrder,
			 CRMConstants.DontCancelOthersPreOrder, CRMConstants.NeedHelpDontCancelOthersPreOrder});
	 flow.put("cancelPreOrder16",new String[]{CRMConstants.IWantToCancelOrderPreOrder, CRMConstants.OthersPreOrder,
			 CRMConstants.DontCancelOthersPreOrder, CRMConstants.NoDoneDontCancelOthersPreOrder});
	 flow.put("cancelSubMeal1",new String[]{CRMConstants.IWantToCancelOrderSubMeal, CRMConstants.DoNotLikeFoodCancelSubMeal,
	 		CRMConstants.YesCancelDoNotLikeFoodCancelSubMeal, CRMConstants.NeedMorehelpIsCancelDoNotLikeFoodCancelSubMeal});
	 flow.put("cancelSubMeal2",new String[]{CRMConstants.IWantToCancelOrderSubMeal, CRMConstants.DoNotLikeFoodCancelSubMeal,
			 CRMConstants.YesCancelDoNotLikeFoodCancelSubMeal, CRMConstants.NoDoneIsCancelDoNotLikeFoodCancelSubMeal});
	 flow.put("cancelSubMeal3",new String[]{CRMConstants.IWantToCancelOrderSubMeal, CRMConstants.DoNotLikeFoodCancelSubMeal,
			 CRMConstants.DontCancelDoNotLikeFoodCancelSubMeal, CRMConstants.NeedHelpDontCancelDoNotLikeFoodCancelSubMeal});
	 flow.put("cancelSubMeal4",new String[]{CRMConstants.IWantToCancelOrderSubMeal, CRMConstants.DoNotLikeFoodCancelSubMeal,
			 CRMConstants.DontCancelDoNotLikeFoodCancelSubMeal, CRMConstants.NoDoneDontCancelDoNotLikeFoodCancelSubMeal});
	 flow.put("cancelSubMeal5",new String[]{CRMConstants.IWantToCancelOrderSubMeal, CRMConstants.IncorrectAddressSelectedCancelSubMeal,
			 CRMConstants.YesCancelIncorrectAddressSelectedCancelSubMeal, CRMConstants.NeedHelpYesCancelIncorrectAddressSelectedCancelSubMeal});
	 flow.put("cancelSubMeal6",new String[]{CRMConstants.IWantToCancelOrderSubMeal, CRMConstants.IncorrectAddressSelectedCancelSubMeal,
			 CRMConstants.YesCancelIncorrectAddressSelectedCancelSubMeal, CRMConstants.NoDoneYesCancelIncorrectAddressSelectedCancelSubMeal});
	 flow.put("cancelSubMeal7",new String[]{CRMConstants.IWantToCancelOrderSubMeal, CRMConstants.IncorrectAddressSelectedCancelSubMeal,
			 CRMConstants.DontCancelIncorrectAddressSelectedCancelSubMeal, CRMConstants.NeedHelpDontCancelIncorrectAddressSelectedCancelSubMeal});
	 flow.put("cancelSubMeal8",new String[]{CRMConstants.IWantToCancelOrderSubMeal, CRMConstants.IncorrectAddressSelectedCancelSubMeal,
			 CRMConstants.DontCancelIncorrectAddressSelectedCancelSubMeal, CRMConstants.NoDoneDontCancelIncorrectAddressSelectedCancelSubMeal});
	 flow.put("cancelSubMeal9",new String[]{CRMConstants.IWantToCancelOrderSubMeal, CRMConstants.GotBoredOfFoodCancelSubMeal,
			 CRMConstants.YesCancelGotBoredOfFoodCancelSubMeal, CRMConstants.NeedHelpYesCancelGotBoredOfFoodCancelSubMeal});
	 flow.put("cancelSubMeal10",new String[]{CRMConstants.IWantToCancelOrderSubMeal, CRMConstants.GotBoredOfFoodCancelSubMeal,
			 CRMConstants.YesCancelGotBoredOfFoodCancelSubMeal, CRMConstants.NoDoneYesCancelGotBoredOfFoodCancelSubMeal});
	 flow.put("cancelSubMeal11",new String[]{CRMConstants.IWantToCancelOrderSubMeal, CRMConstants.GotBoredOfFoodCancelSubMeal,
			 CRMConstants.DontCancelGotBoredOfFoodCancelSubMeal, CRMConstants.NeedHelpDontCancelGotBoredOfFoodCancelSubMeal});
	 flow.put("cancelSubMeal12",new String[]{CRMConstants.IWantToCancelOrderSubMeal, CRMConstants.GotBoredOfFoodCancelSubMeal,
			 CRMConstants.DontCancelGotBoredOfFoodCancelSubMeal, CRMConstants.NoDoneDontCancelGotBoredOfFoodCancelSubMeal});
	 flow.put("cancelSubMeal13",new String[]{CRMConstants.IWantToCancelOrderSubMeal, CRMConstants.OthersSubMeal,
			 CRMConstants.YesCancelOthersSubMeal, CRMConstants.NeedHelpYesCancelOthersSubMeal});
	 flow.put("cancelSubMeal14",new String[]{CRMConstants.IWantToCancelOrderSubMeal, CRMConstants.OthersSubMeal,
			 CRMConstants.YesCancelOthersSubMeal, CRMConstants.NoDoneYesCancelOthersSubMeal});
	 flow.put("cancelSubMeal15",new String[]{CRMConstants.IWantToCancelOrderSubMeal, CRMConstants.OthersSubMeal,
			 CRMConstants.DontCancelOthersSubMeal, CRMConstants.NeedHelpDontCancelOthersSubMeal});
	 flow.put("cancelSubMeal16",new String[]{CRMConstants.IWantToCancelOrderSubMeal, CRMConstants.OthersSubMeal,
			 CRMConstants.DontCancelOthersSubMeal, CRMConstants.NoDoneDontCancelOthersSubMeal});

	 flow.put("changeDeliveryPreOrder",new String[]{CRMConstants.ChangeDeliverySlotPreOrder});
	 flow.put("changeDeliverySubMeal",new String[]{CRMConstants.ChangeDeliverySlotSubMeal});

	 flow.put("skipOrderPreOrder",new String[]{CRMConstants.SkipMyMealPreOrder});
	 flow.put("skipOrderSubMeal",new String[]{CRMConstants.SkipMyMealSubMeal});

	 flow.put("addModifyPreOrder",new String[]{CRMConstants.AddModifyPreOrder});
	 flow.put("addModifySubMeal",new String[]{CRMConstants.AddModifySubMeal});

	 flow.put("changeSwapPreOrder",new String[]{CRMConstants.ChangeSwapPreOrder});
	 flow.put("changeSwapSubMeal",new String[]{CRMConstants.ChangeSwapSubMeal});

	 flow.put("foodInstPreOrder",new String[]{CRMConstants.FoodPreInstPreOrder});
	 flow.put("foodInstSubMeal",new String[]{CRMConstants.FoodPreInstSubMeal});

	 flow.put("deInstrPreOrder",new String[]{CRMConstants.DeliveryInstrPreOrder});
	 flow.put("deInstrSubMeal",new String[]{CRMConstants.DeliveryInstrSubMeal});

	 flow.put("paymentPreOrder",new String[]{CRMConstants.PaymentsAndRefundPreOrder});
	 flow.put("paymentSubMeal",new String[]{CRMConstants.PaymentsAndRefundSubMeal});

	 flow.put("markIncorrectPreOrder",new String[]{CRMConstants.MarkedDeliveredIncorrectlyPreOrder});
	 flow.put("markIncorrectSubMeal",new String[]{CRMConstants.MarkedDeliveredIncorrectlySubMeal});

	 flow.put("missingItemPreOrder",new String[]{CRMConstants.MissingItemPreOrder});
	 flow.put("MissingItemSubMeal",new String[]{CRMConstants.MissingItemSubMeal});

	 flow.put("IncorectMealPreOrder",new String[]{CRMConstants.IncorrectMealProvidedPreOrder});
	 flow.put("IncorrectSubMeal",new String[]{CRMConstants.IncorrectMealProvidedSubMeal});

	 flow.put("BadQualityPreOrder",new String[]{CRMConstants.BadQualityPreOrder});
	 flow.put("BadQualitySubMeal",new String[]{CRMConstants.BadQualitySubMeal});

	 flow.put("packagingPreOrder",new String[]{CRMConstants.PackagingIssuesPreOrder});
	 flow.put("packingSubMeal",new String[]{CRMConstants.PackagingIssuesSubMeal});

	 flow.put("reasonNotPreMeal",new String[]{CRMConstants.ReasonNotListedPreOrder});
	 flow.put("reasonNotSubMeal",new String[]{CRMConstants.ReasonNotListedSubMeal});

	 flow.put("otPostCutPreOrder",new String[]{CRMConstants.WhereIsMyOrderPostCutOffPreOrder});
	 flow.put("otPostCutSubMeal",new String[]{CRMConstants.WhereIsMyOrderPostCutOffSubMeal});

	 return flow;


 	}
}