package com.swiggy.api.erp.cms.constants;

/**
 * Created by kiran.j on 2/1/18.
 */
public interface CityConstants {

        String name = "TestCity";
        int open_time = 0;
        int close_time = 2359;
        int enabled = 1;
        int disabled = 0;
        String banner_message = "Test Banner Message";
        int max_delivery_time = 60;
        int min_delivery_time = 25;
        String help_line = "02233835850";
        String customer_care = "08067466792";
        String slug = "bangalore";
        int is_open = 1;
        String state = "Karnataka";
        String db_helpline = "0";
        int show_ratings = 1;
        float delivery_radius = 5;
        String cap_on_cod = " Please pay online. Inconvenience regretted.";
        String create_city = "";
        String insert_into = "INSERT INTO ";
        String values = " VALUES ";
        String city_table = "city";
        String json = "{\"action\": @action, \"model\": @model, \"data\": @data}";
        String insert = "insert";
        String update = "update";
        String city = "city";
        String city_id = "select * from swiggy.city where name like '%";
        String like_end = "%'";
        String id = "id";
        String city_name = "City";
}
