package com.swiggy.api.erp.cms.pojo;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;

public class HolidaySlot {
    @JsonProperty("restaurant_id")
    private Integer restaurantId;
    @JsonProperty("from_time")
    private String fromTime;
    @JsonProperty("to_time")
    private String toTime;
    private ItemData data;
    private ItemUserMeta userMeta;

    /**
     * No args constructor for use in serialization
     *
     */
    public HolidaySlot() {
    }

    /**
     *
     * @param fromTime
     * @param toTime
     * @param restaurantId
     */
    public HolidaySlot(Integer restaurantId, String fromTime, String toTime, ItemData data, ItemUserMeta userMeta) {
        super();
        this.restaurantId = restaurantId;
        this.fromTime = fromTime;
        this.toTime = toTime;
        this.data = data;
        this.userMeta = userMeta;
    }

    public Integer getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(Integer restaurantId) {
        this.restaurantId = restaurantId;
    }

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String getToTime() {
        return toTime;
    }

    public void setToTime(String toTime) {
        this.toTime = toTime;
    }

    public ItemData getData() {
        return data;
    }

    public void setData(ItemData data) {
        this.data = data;
    }

    public ItemUserMeta getUserMeta() {
        return userMeta;
    }

    public void setUserMeta(ItemUserMeta userMeta) {
        this.userMeta = userMeta;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this).append("restaurantId", restaurantId).append("fromTime", fromTime).append("toTime", toTime).append("data", data).append("userMeta", userMeta).toString();
    }

}
