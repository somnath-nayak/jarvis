
package com.swiggy.api.erp.cms.pojo.StoreOnOff;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "partner_type",
    "store_id",
    "user_meta",
    "from_time",
    "to_time",
    "open"
})
public class StoreOnOff {

    @JsonProperty("partner_type")
    private Integer partnerType;
    @JsonProperty("store_id")
    private String storeId;
    @JsonProperty("user_meta")
    private UserMeta userMeta;
    @JsonProperty("from_time")
    private String fromTime;
    @JsonProperty("to_time")
    private String toTime;
    @JsonProperty("open")
    private Boolean open;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public StoreOnOff() {
    }

    /**
     * 
     * @param open
     * @param userMeta
     * @param fromTime
     * @param storeId
     * @param partnerType
     * @param toTime
     */
    public StoreOnOff(Integer partnerType, String storeId, UserMeta userMeta, String fromTime, String toTime, Boolean open) {
        super();
        this.partnerType = partnerType;
        this.storeId = storeId;
        this.userMeta = userMeta;
        this.fromTime = fromTime;
        this.toTime = toTime;
        this.open = open;
    }

    @JsonProperty("partner_type")
    public Integer getPartnerType() {
        return partnerType;
    }

    @JsonProperty("partner_type")
    public void setPartnerType(Integer partnerType) {
        this.partnerType = partnerType;
    }

    @JsonProperty("store_id")
    public String getStoreId() {
        return storeId;
    }

    @JsonProperty("store_id")
    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    @JsonProperty("user_meta")
    public UserMeta getUserMeta() {
        return userMeta;
    }

    @JsonProperty("user_meta")
    public void setUserMeta(UserMeta userMeta) {
        this.userMeta = userMeta;
    }

    @JsonProperty("from_time")
    public String getFromTime() {
        return fromTime;
    }

    @JsonProperty("from_time")
    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    @JsonProperty("to_time")
    public String getToTime() {
        return toTime;
    }

    @JsonProperty("to_time")
    public void setToTime(String toTime) {
        this.toTime = toTime;
    }

    @JsonProperty("open")
    public Boolean getOpen() {
        return open;
    }

    @JsonProperty("open")
    public void setOpen(Boolean open) {
        this.open = open;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
