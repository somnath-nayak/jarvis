package com.swiggy.api.erp.cms.tests;

import org.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.dp.POP_dataProvider;
import com.swiggy.api.erp.cms.helper.POP_Area_Schedule_Helper;

import framework.gameofthrones.JonSnow.Processor;

/*
 * Area scheduling can be created from this class and same details can be updated for area-code=44 (Powai)
 */

public class POP_Area_Schedule extends POP_dataProvider {
	int newAreaScheduleId=0;
	POP_Area_Schedule_Helper obj = new POP_Area_Schedule_Helper();
	
	@Test(dataProvider="areaScheduleCREATEData", priority=0, groups={"Sanity_TC", "Smoke_TC", "Regression_TC"}, 
			description="Creates an area schedule")
	public void createAreaSchedule(String slot_type, int area_id, int openTime,
			int closeTime, String day, String menu_type) throws JSONException, InterruptedException
	{
		  Processor p1 = obj.createArea_schedule_helper(slot_type, area_id, openTime, closeTime, day, menu_type);
		  String response = p1.ResponseValidator.GetBodyAsText();
		  newAreaScheduleId = JsonPath.read(response, "$.data.id");
		  System.out.println("Newly created Area-Schedule-id  = " + newAreaScheduleId);
		  int statusCode = JsonPath.read(response, "$.status");
		  Assert.assertEquals(statusCode, 1);
	}

	
	@Test(dataProvider="areaScheduleUPDATEData", priority=1, groups={"Sanity_TC", "Smoke_TC", "Regression_TC"},
			description="Updates the created area schedule")
	public void updateAreaSchedule(String slot_type, int area_id, int openTime,
			int closeTime, String day, String menu_type) throws JSONException
	{
		Processor p1 = obj.updateArea_schedule_helper(slot_type, area_id, openTime, closeTime, day, menu_type, newAreaScheduleId );
		
		String response = p1.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(response, "$.status");
		Assert.assertEquals(statusCode, 1);
	}

	@Test(priority=2, groups={"Sanity_TC", "Smoke_TC", "Regression_TC"}, description="Retrives the created area schedule")
	public void getAreaSchedule() throws JSONException
	{
		Processor p1 = obj.getArea_schedule_helper(newAreaScheduleId);
		String response = p1.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(response, "$.status");
		Assert.assertEquals(statusCode, 1);
	}

	@Test(priority=3, groups={"Sanity_TC", "Smoke_TC", "Regression_TC"}, description="Deletes the created area schedule")
	public void deleteAreaSchedule() throws JSONException
	{
		Processor p1 = obj.deleteArea_schedule_helper(newAreaScheduleId);
		String response = p1.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(response, "$.status");
		Assert.assertEquals(statusCode, 1);
		System.out.println("Deleted id successfully = ");
	}

	@Test(dataProvider="invalidareaScheduleCREATEData", priority=4, groups={"Regression_TC", "Smoke_TC"},
			description="Validating while creating area schedule for invalid/non-existence area id")
	public void createInvalidAreaSchedule(String slot_type, int area_id, int openTime,
			int closeTime, String day, String menu_type) throws JSONException
	{
		Processor p1 = obj.create_InvalidArea_schedule_helper(slot_type, area_id, openTime, closeTime, day, menu_type);
		
		String response = p1.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(response, "$.status");
		String message = JsonPath.read(response, "$.message");
		String entity_id = JsonPath.read(response, "$.data.field_validation_errors[0].value");
		
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(message, "Validation Error");
		Assert.assertEquals(entity_id, "Entity doesn't exist.");
	}

	@Test(dataProvider="areaScheduleWithExistingSlot", priority=5, groups={"Regression_TC"},
			description="Validating for creating an area-schedule with existing area-schedule slot ")
	public void createDuplicateAreaSchedule(String slot_type, int area_id, int openTime,
			int closeTime, String day, String menu_type) throws JSONException
	{
		Processor p1 = obj.create_Area_schedule_For_ExistingSlot_helper(slot_type, area_id, openTime, closeTime, day, menu_type);
		
		String response = p1.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(response, "$.status");
		String message = JsonPath.read(response, "$.message");
		String openTimeValue = JsonPath.read(response, "$.data.field_validation_errors[0].value");
		String closeTimeValue = JsonPath.read(response, "$.data.field_validation_errors[1].value");

		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(message, "Validation Error");
		Assert.assertEquals(openTimeValue, "Open time shouldn't lie between existing slots.");
		Assert.assertEquals(closeTimeValue, "Schedule shouldn't cover existing schedule.");
	}

	@Test(dataProvider="areaScheduleWithInvalidDayName", priority=6, groups={"Regression_TC"},
			description="Validating for creating an area-schedule with invalid/full Day Name  ")
	public void createAreaScheduleWithInvalidOREmptyDayName(String slot_type, int area_id, int openTime,
			int closeTime, String day, String menu_type) throws JSONException
	{
		Processor p1 = obj.create_Area_schedule_With_Invalid_OR_Empty_DayName_helper(slot_type, area_id, openTime, closeTime, day, menu_type);
		String response = p1.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(response, "$.status");
		String errorMsg = JsonPath.read(response, "$.error");
		Assert.assertEquals(statusCode, 400);
		Assert.assertEquals(errorMsg, "Bad Request");
	}


}
