package com.swiggy.api.erp.crm.tests.MockServiceTests;

import com.swiggy.api.erp.crm.constants.FlowMapper;
import com.swiggy.api.erp.crm.constants.MockServiceConstants;
import com.swiggy.api.erp.crm.dp.MockServiceDP;
import com.swiggy.api.erp.crm.helper.MockServicesHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.WireMockHelper;
import io.advantageous.boon.core.Sys;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static org.testng.Assert.assertTrue;

public class RefundAndCouponsMockFlow  extends MockServiceDP {

    MockServicesHelper serviceHelper = new MockServicesHelper();
    WireMockHelper wireMockHelper = new WireMockHelper();

    FlowMapper flowMapper = new FlowMapper();
    MockServiceConstants mockServiceConstants = new MockServiceConstants();

    Initialize gameofthrones = new Initialize();

    @BeforeClass
    public void startMockServer() {
        wireMockHelper.startMockServer(9005);
    }

    @AfterClass
    public void stopMockServer() {
        wireMockHelper.stopMockServer();
    }


    @Test(dataProvider = "mockCodFlow", groups = {"regression"}, description = "COD Block Flow ")
    public void checkCodBlockFlow(HashMap<String,String> flowDetails) throws IOException, InterruptedException {

        System.out.println("Starting Test " + flowDetails);

        String orderType = flowDetails.get("orderType").toString();
        String ffStatus = flowDetails.get("ffStatus").toString();
        String disposition = flowDetails.get("disposition").toString();
        String deliveryStatus = flowDetails.get("deliveryStatus").toString();
        String flowName = flowDetails.get("flow").toString();
        String sla = flowDetails.get("sla").toString();
        String conversationId, deviceId;

        HashMap<String, String> orderDetails = mockServiceConstants.getOrderDetails(orderType);
        System.out.println("order details" +orderDetails);

        HashMap<String, String> deliveryDetails =  mockServiceConstants.getDeliveryDetails(sla, deliveryStatus);
       // String[] dispositionDetails = mockServiceConstants.getDispositionDetails().get(disposition);
        String[] flow = flowMapper.setFlow().get(flowName);

        String queryparam[] = new String[1];

        String nodeId, orderId;
        orderId = orderDetails.get("orderId").toString();
        conversationId = orderId;
        deviceId = orderId;

        int lengthofflow = flow.length;
        System.out.println("lengthofflow \t" + lengthofflow);

        //Following code is just to mock the ff order details api
        serviceHelper.stubOrderDetailAPIResponse(orderDetails.get("orderId").toString(), orderDetails.get("restId").toString(),
                orderDetails.get("itemId").toString(), orderDetails.get("cost").toString(), orderType, ffStatus,"TAKEAWAY");

        // Order details call to checkout
        serviceHelper.stubOrderDetailAPIResponse2(orderDetails.get("orderId").toString(), orderDetails.get("restId").toString(),
                orderDetails.get("itemId").toString(), orderDetails.get("cost").toString(), orderType, ffStatus, "TAKEAWAY");


        serviceHelper.stubDeliveryTrackAPIResponse(
                orderDetails.get("orderId").toString(),
                deliveryDetails.get("deliveryStatus").toString(),
                deliveryDetails.get("assignedPredTime").toString(),
                deliveryDetails.get("assignedActualTime").toString(),
                deliveryDetails.get("confirmedPredTime").toString(),
                deliveryDetails.get("confirmedActualTime").toString(),
                deliveryDetails.get("arrivedPredTime").toString(),
                deliveryDetails.get("arrivedActualTime").toString(),
                deliveryDetails.get("pickedupPredTime").toString(),
                deliveryDetails.get("pickedupActualTime").toString(),
                deliveryDetails.get("reachedPredTime").toString(),
                deliveryDetails.get("reachedActualTime").toString(),
                deliveryDetails.get("deliveredPredTime").toString(),
                deliveryDetails.get("deliveredActualTime").toString());

        serviceHelper.stubRefundPGFlag(orderDetails.get("orderId").toString());

        serviceHelper.stubFraudServiceFF();

    for (int i = 0; i < lengthofflow - 1; i++) {
        nodeId = flow[i];
        if (nodeId == "1") {
            continue;
        } else {
            queryparam[0] = nodeId;
            String[] paylaodparam = new String[]{"0", orderId, conversationId};

            System.out.println("length of flow" + lengthofflow);

            HashMap<String, String> requestheaders_cancellationinbound = new HashMap<String, String>();
            requestheaders_cancellationinbound.put("Content-Type", "application/json");
            GameOfThronesService cancellationinbound = new GameOfThronesService("crm", "codBlock", gameofthrones);

            Processor cancellationinbound_response = new Processor(cancellationinbound,
                    requestheaders_cancellationinbound, paylaodparam, queryparam);

            Assert.assertEquals(cancellationinbound_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("status"), "SUCCESS");

            String NodeList = cancellationinbound_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.childrenNodes..id").toString().replace("[", "").replace("]", "");
            List<String> nodes = Arrays.asList(NodeList.split(","));

            System.out.println("addonlist" + nodes);
            System.out.println("Flow Name " + Arrays.asList(flow));
            System.out.println("flowname[i+1]" + flow[i + 1]);
            System.out.println("nodes.contains(flow[i + 1]) " + nodes.contains(flow[i + 1]));
            assertTrue(nodes.contains(flow[i + 1]));
        }
    }
    }

    @Test(dataProvider = "mockPaymentAndRefundFlow", groups = {"regression"}, description = "Payment And Refund Flow ")
    public void paymentAndRefundFlow(HashMap<String,String> flowDetails) throws IOException, InterruptedException {

        System.out.println("Starting Test " + flowDetails);

        String orderType = flowDetails.get("orderType").toString();
        String ffStatus = flowDetails.get("ffStatus").toString();
        String flowName = flowDetails.get("flow").toString();
        String paymentMode = flowDetails.get("paymentMode").toString();
        String conversationId, deviceId;

        HashMap<String, String> orderDetails = mockServiceConstants.getOrderDetails(orderType);
        System.out.println("order details" +orderDetails);

        String[] flow = flowMapper.setFlow().get(flowName);

        String queryparam[] = new String[1];

        String nodeId, orderId;
        orderId = orderDetails.get("orderId").toString();
        conversationId = orderId;
        deviceId = orderId;
        String userId = orderId;

        int lengthofflow = flow.length;
        System.out.println("lengthofflow \t" + lengthofflow);

        //Following code is just to mock the UPSTREAM APIS
        serviceHelper.stubOrderDetailAPIRefundPayment(orderDetails.get("orderId").toString(), orderDetails.get("restId").toString(),
                orderDetails.get("itemId").toString(), orderDetails.get("cost").toString(), orderType, ffStatus,paymentMode);
        serviceHelper.stubFraudServiceFF();
        serviceHelper.stubRefundStatus(orderId, paymentMode);
        serviceHelper.stubRefundDetails(userId, paymentMode);

        for (int i = 0; i < lengthofflow - 1; i++) {
            nodeId = flow[i];
            if (nodeId == "1") {
                continue;
            } else {
                queryparam[0] = nodeId;
                String[] paylaodparam = new String[]{"0", orderId, conversationId};

                System.out.println("length of flow" + lengthofflow);

                HashMap<String, String> requestheaders_cancellationinbound = new HashMap<String, String>();
                requestheaders_cancellationinbound.put("Content-Type", "application/json");
                GameOfThronesService cancellationinbound = new GameOfThronesService("crm", "codBlock", gameofthrones);

                Processor cancellationinbound_response = new Processor(cancellationinbound,
                        requestheaders_cancellationinbound, paylaodparam, queryparam);

                Assert.assertEquals(cancellationinbound_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("status"), "SUCCESS");

                String NodeList = cancellationinbound_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.childrenNodes..id").toString().replace("[", "").replace("]", "");
                List<String> nodes = Arrays.asList(NodeList.split(","));

                System.out.println("addonlist" + nodes);
                System.out.println("Flow Name " + Arrays.asList(flow));
                System.out.println("flowname[i+1]" + flow[i + 1]);
                System.out.println("nodes.contains(flow[i + 1]) " + nodes.contains(flow[i + 1]));
                assertTrue(nodes.contains(flow[i + 1]));
            }
        }
    }
}
