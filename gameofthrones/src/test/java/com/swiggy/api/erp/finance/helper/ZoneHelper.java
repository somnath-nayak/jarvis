package com.swiggy.api.erp.finance.helper;

import com.rabbitmq.client.AMQP;
import com.swiggy.api.erp.finance.constants.FinanceConstants;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Tyrion.RabbitMQHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Created by kiran.j on 7/11/18.
 */
public class ZoneHelper {

    RabbitMQHelper rmqHelper = new RabbitMQHelper();

    public String createZone()  {
        File file = new File("../Data/Payloads/JSON/zone");
        String create_zone = null;
        try {
            create_zone = FileUtils.readFileToString(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String id = Integer.toString(new Random().nextInt(10001));
        create_zone = create_zone.replace("${id}", id).replace("${name}", "Zone_" + id).replace("${area_id}", id);
        rmqHelper.pushMessageToExchange(FinanceConstants.oms_queue, FinanceConstants.zone_exchange, new AMQP.BasicProperties().builder().contentType("application/json"), create_zone);
        return id;
    }

    public String createZone(String zone_id, String area_id)  {
        File file = new File("../Data/Payloads/JSON/zone");
        String create_zone = null;
        try {
            create_zone = FileUtils.readFileToString(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String id = Integer.toString(new Random().nextInt(10001));
        create_zone = create_zone.replace("${id}", zone_id).replace("${name}", "Zone_" + zone_id).replace("${area_id}", area_id);
        rmqHelper.pushMessageToExchange(FinanceConstants.oms_queue, FinanceConstants.zone_exchange, new AMQP.BasicProperties().builder().contentType("application/json"), create_zone);
        return id;
    }

    public List<Map<String, Object>> getZoneFromDb() {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(FinanceConstants.db);
        List<Map<String, Object>> list = sqlTemplate.queryForList(FinanceConstants.get_zone);
        return list;
    }

    public List<Map<String, Object>> getAreaFromDb() {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(FinanceConstants.db);
        List<Map<String, Object>> list = sqlTemplate.queryForList(FinanceConstants.get_area);
        return list;
    }

    public List<Map<String, Object>> getZoneDetails(String zone_id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(FinanceConstants.db);
        List<Map<String, Object>> list = sqlTemplate.queryForList(FinanceConstants.get_zone_details + zone_id);
        return list;
    }
}
