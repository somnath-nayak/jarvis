package com.swiggy.api.erp.delivery.dp;

import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.erp.delivery.helper.AutoassignHelper;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;

import com.swiggy.api.erp.delivery.helper.DeliveryHelperMethods;
import com.swiggy.api.erp.delivery.helper.DeliveryReferalHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;

import framework.gameofthrones.JonSnow.Processor;

import org.json.*;

import javax.xml.transform.sax.SAXSource;

public class DeliveryReferralDataProvider {
	
	
    DeliveryServiceHelper helpdel;
	DeliveryHelperMethods delmeth;
	DeliveryReferalHelper DRhelper;
	AutoassignHelper autoassignHelper;


	public DeliveryReferralDataProvider()
	{
		helpdel=new DeliveryServiceHelper();
		delmeth=new DeliveryHelperMethods();
		DRhelper=new DeliveryReferalHelper("1026","password");
		autoassignHelper= new AutoassignHelper();

	}
	@BeforeSuite
	public void loginde()
	{

	}

	@DataProvider(name = "Referral1")
	public  Object[][] createRef() throws Exception {
		String finalauth=loginDE();
		String mobileno="78"+Math.round(Math.random()*10000)+Math.round(Math.random())*1000;
		return new Object[][] {
				{"\"test Referral\"",mobileno,"2","1",DeliveryConstant.snded_id,finalauth},
				{"\"12345\"",mobileno,"2","1",DeliveryConstant.snded_id,finalauth},

		};
	}

	@DataProvider(name = "Referral2")
	public  Object[][] getrefConfig() throws Exception {
		String finalauth=loginDE();
		return new Object[][] {
		{DeliveryConstant.mobile2,finalauth},
		};
	 }
	
	@DataProvider(name = "Referral3")
	public  Object[][] getEligible() throws Exception {
		String finalauth=loginDE();

		return new Object[][] {	
		{DeliveryConstant.snded_id, finalauth},
		};
    }
	
	public String loginDE() throws JSONException
	{
		String loginresponse=autoassignHelper.delogin(DeliveryConstant.snded_id, DeliveryConstant.delivery_app_version).ResponseValidator.GetBodyAsText();
		System.out.println("login Response " +loginresponse);
		String otp=(new JSONObject(helpdel.getOtp(DeliveryConstant.snded_id).ResponseValidator.GetBodyAsText())).get("data").toString();
		System.out.println("OTP IS "+otp);
		JSONObject auth=(JSONObject)(new JSONObject(autoassignHelper.deotp(DeliveryConstant.snded_id,otp).ResponseValidator.GetBodyAsText()).get("data"));
		String finalauth=auth.get("Authorization").toString();
		System.out.println("Auth is "+finalauth);
		return finalauth;
	}
	
	public static String createOrder() throws Exception {
		CheckoutHelper helperc = new CheckoutHelper();
		Object[][] data = cartItems();
		Processor p = helperc.placeOrder(DeliveryConstant.mobile,
				DeliveryConstant.password, data[0][0].toString(),
				data[0][1].toString(), data[0][2].toString());
		return p.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.order_id");
	}

	public static Object[][] cartItems() {

		return new Object[][] { { DeliveryConstant.auto_assign_mayur_item_id, "2", DeliveryConstant.auto_assign_mayur_rest_id } };

	}
}
