package com.swiggy.api.erp.cms.pojo.AddItemToRest;

import com.swiggy.api.erp.cms.constants.MenuConstants;
import com.swiggy.api.erp.cms.constants.RestaurantConstants;
import com.swiggy.api.erp.cms.helper.BaseServiceHelper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class RestBuilder {
    private AddItemToRestaurant addItemToRestaurant;

    public RestBuilder()
    {
        addItemToRestaurant=new AddItemToRestaurant();
    }

    public RestBuilder desc(String description) {
        addItemToRestaurant.setDescription(description);
        return this;
    }
    public RestBuilder enable(Integer enabled) {
        addItemToRestaurant.setEnabled(enabled);
        return this;
    }

    public RestBuilder inStock(Integer inStock) {
        addItemToRestaurant.setInStock(inStock);
        return this;
    }

    public RestBuilder perishable(Integer isPerishable) {
        addItemToRestaurant.setIsPerishable(isPerishable);
        return this;
    }

    public RestBuilder spicy(Integer isSpicy) {
        addItemToRestaurant.setIsSpicy(isSpicy);
        return this;
    }

    public RestBuilder veg(Integer isVeg) {
        addItemToRestaurant.setIsVeg(isVeg);
        return this;
    }

    public RestBuilder nam(String name) {
        addItemToRestaurant.setName(name);
        return this;
    }

    public RestBuilder packingCharge(Integer packingCharges) {
        addItemToRestaurant.setPackingCharges(packingCharges);
        return this;
    }

    public RestBuilder packingSlab(Integer packingSlabCount) {
        addItemToRestaurant.setPackingSlabCount(packingSlabCount);
        return this;
    }

    public RestBuilder pric(Integer price) {
        addItemToRestaurant.setPrice(price);
        return this;
    }

    public RestBuilder serves(Integer servesHowMany) {
        addItemToRestaurant.setServesHowMany(servesHowMany);
        return this;
    }

    public RestBuilder serviceCharge(String serviceCharges) {
        addItemToRestaurant.setServiceCharges(serviceCharges);
        return this;
    }
    public RestBuilder addon(String addonsAndVariantChangeRequest) {
        addItemToRestaurant.setAddonsAndVariantChangeRequest(addonsAndVariantChangeRequest);
        return this;
    }

    public RestBuilder itemChange(String itemChangeRequest) {
        addItemToRestaurant.setItemChangeRequest(itemChangeRequest);
        return this;
    }

    public RestBuilder imageUrl(String s3ImageUrl) {
        addItemToRestaurant.setS3ImageUrl(s3ImageUrl);
        return this;
    }

    public RestBuilder mainCategory(Integer mainCategoryId) {
        addItemToRestaurant.setMainCategoryId(mainCategoryId);
        return this;
    }

    public RestBuilder categoryId(String categoryId) {
        addItemToRestaurant.setCategoryId(categoryId);
        return this;
    }

    private void defaultAddItem() throws IOException {
        BaseServiceHelper baseServiceHelper = new BaseServiceHelper();
        String random = baseServiceHelper.getTimeStampRandomised();

        if(addItemToRestaurant.getDescription()==null){
            addItemToRestaurant.setDescription("");
        }
        if(addItemToRestaurant.getEnabled()==null){
            addItemToRestaurant.setEnabled(1);
        }
        if(addItemToRestaurant.getInStock()==null){
            addItemToRestaurant.setInStock(1);
        }
        if(addItemToRestaurant.getIsPerishable()==null){
            addItemToRestaurant.setIsPerishable(1);
        }
        if(addItemToRestaurant.getIsSpicy()==null){
            addItemToRestaurant.setIsSpicy(1);
        }
        if(addItemToRestaurant.getIsVeg()==null){
            addItemToRestaurant.setIsVeg(0);
        }
        if(addItemToRestaurant.getName() == null){
            addItemToRestaurant.setName("Test-Item-"+random);
        }
        if(addItemToRestaurant.getPackingCharges()==null){
            addItemToRestaurant.setPackingCharges(22);
        }

        if(addItemToRestaurant.getPackingSlabCount()==null){
            addItemToRestaurant.setPackingSlabCount(1);
        }
        if(addItemToRestaurant.getPrice()==null){
            addItemToRestaurant.setPrice(22);
        }
        if(addItemToRestaurant.getServesHowMany()==null){
            addItemToRestaurant.setServesHowMany(1);
        }
        if(addItemToRestaurant.getServiceCharges()==null){
            addItemToRestaurant.setServiceCharges("20");
        }

        if(addItemToRestaurant.getAddonsAndVariantChangeRequest()==null){
            addItemToRestaurant.setAddonsAndVariantChangeRequest("New TEST item");
        }
        if(addItemToRestaurant.getItemChangeRequest()==null){
            addItemToRestaurant.setItemChangeRequest("Change now");
        }
        if(addItemToRestaurant.getS3ImageUrl()==null){
            addItemToRestaurant.setS3ImageUrl(RestaurantConstants.s3_image);
        }
        if(addItemToRestaurant.getMainCategoryId()==null){
            addItemToRestaurant.setMainCategoryId(12);
        }
        if(addItemToRestaurant.getCategoryId()==null){
            addItemToRestaurant.setCategoryId("20");
        }
        if(addItemToRestaurant.getServiceTax() == null)
            addItemToRestaurant.setServiceTax(20);
        if(addItemToRestaurant.getVat() == null)
            addItemToRestaurant.setVat(10);
        if(addItemToRestaurant.getAddon_free_limit() == null)
            addItemToRestaurant.setAddon_free_limit(-1);
        if(addItemToRestaurant.getAddon_limit() == null)
            addItemToRestaurant.setAddon_limit(-1);
        if(addItemToRestaurant.getOrder() == null)
            addItemToRestaurant.setOrder(0);
        if(addItemToRestaurant.getType() == null)
            addItemToRestaurant.setType(MenuConstants.regular_item);
        if(addItemToRestaurant.getPopUsageType() == null)
            addItemToRestaurant.setPopUsageType(RestaurantConstants.pop_type.POP_ONLY.toString());
        if(addItemToRestaurant.getImageId() == null)
            addItemToRestaurant.setImageId(RestaurantConstants.image_id);
        if(addItemToRestaurant.getImageUrl() == null)
            addItemToRestaurant.setImageUrl(RestaurantConstants.image);
    }

    public AddItemToRestaurant buildItemSlot() throws IOException{
        defaultAddItem();
        return addItemToRestaurant;
    }

    private void defaultTrigger() throws IOException {
        if(addItemToRestaurant.getMetadata()==null){
            addItemToRestaurant.setMetadata(new Metadata());
        }
        if(addItemToRestaurant.getTickets()==null){
            List<Ticket> list= new ArrayList<>();
            Ticket ticket= new Ticket();
            if(ticket.getId()==null)
                ticket.setId(778);
            list.add(ticket);
            addItemToRestaurant.setTickets(list);
        }
    }

    public AddItemToRestaurant buildTrigger() throws IOException {
        defaultTrigger();
        return addItemToRestaurant;
    }

    }
