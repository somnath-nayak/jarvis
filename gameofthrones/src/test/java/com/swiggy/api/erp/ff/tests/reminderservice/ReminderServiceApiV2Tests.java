package com.swiggy.api.erp.ff.tests.reminderservice;


import com.swiggy.api.erp.ff.dp.ReminderServiceTestData;
import com.swiggy.api.erp.ff.helper.ReminderServiceHelper;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.util.Calendar;

/**
 Created by Narendra on 02-Feb-2019.
 */

public class ReminderServiceApiV2Tests extends ReminderServiceTestData {

	ReminderServiceHelper rshelper = new ReminderServiceHelper();

	@Test(priority = 1, dataProvider = "setreminder", groups = { "sanity", "regression" }, description = "Set Bulk Reminder")
	public void setBulkReminder(String to, String from, String msg) {

		Calendar calendar = Calendar.getInstance();
		System.out.println(calendar.getTimeInMillis() / 1000);
		calendar.add(Calendar.MINUTE, 1);
		System.out.println(calendar.getTimeInMillis() / 1000);
		Processor response = rshelper.setBulkReminder("" + calendar.getTimeInMillis() / 1000, to, from, msg);
		Assert.assertEquals(String.valueOf(response.ResponseValidator.GetResponseCode()), "200",
				"Response code did not match");
		Assert.assertNotNull(response.RequestValidator.GetBodyAsText(), "Response is null ");

	}
	

}
