package com.swiggy.api.erp.cms.pojo;

import com.swiggy.api.erp.cms.constants.CmsConstants;
import com.swiggy.api.erp.cms.helper.RestaurantHelper;
import com.swiggy.api.sf.checkout.helper.CommonAPIHelper;

/**
 * Created by kiran.j on 1/22/18.
 */
public class Restaurant {

    private String invoicing_name;
    private String city;
    private String pan_no;
    private String updated_by;
    private String name;
    private String locality;
    private String bank_ifsc_code;
    private String invoicing_email;
    private String bank_name;
    private String area;
    private String benificiary_name;
    private String bank_city;
    private String z_id;
    private String bank_account_no;
    private String commission_exp;
    private String mou;
    private String address;
    private String tan_no;
    private String pay_by_system_value;
    private String cancelled_cheque;
    private String agreement_type;
    private String lat_long;
    private String gst_state;
    private String gstin;
    private String gst_business_name;
    private String gst_registration_doc;

    public Restaurant(String invoicing_name, String city, String pan_no, String updated_by, String name, String locality, String bank_ifsc_code, String invoicing_email, String bank_name, String area, String benificiary_name, String bank_city, String z_id, String bank_account_no, String commission_exp, String mou, String address, String tan_no, String pay_by_system_value, String cancelled_cheque, String agreement_type) {
        this.invoicing_name = invoicing_name;
        this.city = city;
        this.pan_no = pan_no;
        this.updated_by = updated_by;
        this.name = name;
        this.locality = locality;
        this.bank_ifsc_code = bank_ifsc_code;
        this.invoicing_email = invoicing_email;
        this.bank_name = bank_name;
        this.area = area;
        this.benificiary_name = benificiary_name;
        this.bank_city = bank_city;
        this.z_id = z_id;
        this.bank_account_no = bank_account_no;
        this.commission_exp = commission_exp;
        this.mou = mou;
        this.address = address;
        this.tan_no = tan_no;
        this.pay_by_system_value = pay_by_system_value;
        this.cancelled_cheque = cancelled_cheque;
        this.agreement_type = agreement_type;
    }

    public Restaurant() {
    }

    public String getLat_long() {
        return lat_long;
    }

    public void setLat_long(String lat_long) {
        this.lat_long = lat_long;
    }

    public String getInvoicing_name() {
        return invoicing_name;
    }

    public Restaurant setInvoicing_name(String invoicing_name) {
        this.invoicing_name = invoicing_name;
        return this;
    }

    public String getCity() {
        return city;
    }

    public Restaurant setCity(String city) {
        this.city = city;
        return this;
    }

    public String getPan_no() {
        return pan_no;
    }

    public Restaurant setPan_no(String pan_no) {
        this.pan_no = pan_no;
        return this;
    }

    public String getUpdated_by() {
        return updated_by;
    }

    public Restaurant setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
        return this;
    }

    public String getName() {
        return name;
    }

    public Restaurant setName(String name) {
        this.name = name;
        return this;
    }

    public String getLocality() {
        return locality;
    }

    public Restaurant setLocality(String locality) {
        this.locality = locality;
        return this;
    }

    public String getBank_ifsc_code() {
        return bank_ifsc_code;
    }

    public Restaurant setBank_ifsc_code(String bank_ifsc_code) {
        this.bank_ifsc_code = bank_ifsc_code;
        return this;
    }

    public String getInvoicing_email() {
        return invoicing_email;
    }

    public Restaurant setInvoicing_email(String invoicing_email) {
        this.invoicing_email = invoicing_email;
        return this;
    }

    public String getBank_name() {
        return bank_name;
    }

    public Restaurant setBank_name(String bank_name) {
        this.bank_name = bank_name;
        return this;
    }

    public String getArea() {
        return area;
    }

    public Restaurant setArea(String area) {
        this.area = area;
        return this;
    }

    public String getBenificiary_name() {
        return benificiary_name;
    }

    public Restaurant setBenificiary_name(String benificiary_name) {
        this.benificiary_name = benificiary_name;
        return this;
    }

    public String getBank_city() {
        return bank_city;
    }

    public Restaurant setBank_city(String bank_city) {
        this.bank_city = bank_city;
        return this;
    }

    public String getZ_id() {
        return z_id;
    }

    public Restaurant setZ_id(String z_id) {
        this.z_id = z_id;
        return this;
    }

    public String getBank_account_no() {
        return bank_account_no;
    }

    public Restaurant setBank_account_no(String bank_account_no) {
        this.bank_account_no = bank_account_no;
        return this;
    }

    public String getCommission_exp() {
        return commission_exp;
    }

    public Restaurant setCommission_exp(String commission_exp) {
        this.commission_exp = commission_exp;
        return this;
    }

    public String getMou() {
        return mou;
    }

    public Restaurant setMou(String mou) {
        this.mou = mou;
        return this;
    }

    public String getAddress() {
        return address;
    }

    public Restaurant setAddress(String address) {
        this.address = address;
        return this;
    }

    public String getTan_no() {
        return tan_no;
    }

    public Restaurant setTan_no(String tan_no) {
        this.tan_no = tan_no;
        return this;
    }

    public String getPay_by_system_value() {
        return pay_by_system_value;
    }

    public Restaurant setPay_by_system_value(String pay_by_system_value) {
        this.pay_by_system_value = pay_by_system_value;
        return this;
    }

    public String getCancelled_cheque() {
        return cancelled_cheque;
    }

    public Restaurant setCancelled_cheque(String cancelled_cheque) {
        this.cancelled_cheque = cancelled_cheque;
        return this;
    }

    public String getAgreement_type() {
        return agreement_type;
    }

    public Restaurant setAgreement_type(String agreement_type) {
        this.agreement_type = agreement_type;
        return this;
    }

    public String getGst_state() {
        return gst_state;
    }

    public void setGst_state(String gst_state) {
        this.gst_state = gst_state;
    }

    public String getGstin() {
        return gstin;
    }

    public void setGstin(String gstin) {
        this.gstin = gstin;
    }

    public String getGst_business_name() {
        return gst_business_name;
    }

    public void setGst_business_name(String gst_business_name) {
        this.gst_business_name = gst_business_name;
    }

    public String getGst_registration_doc() {
        return gst_registration_doc;
    }

    public void setGst_registration_doc(String gst_registration_doc) {
        this.gst_registration_doc = gst_registration_doc;
    }

    public Restaurant build() {
        setDefaultValues();
        return this;
    }

    public void setDefaultValues() {
        CommonAPIHelper commonAPIHelper = new CommonAPIHelper();
        RestaurantHelper restaurantHelper = new RestaurantHelper();
        int random = commonAPIHelper.getRandomNo(0,10000);
        if(this.getInvoicing_name() == null)
            this.setInvoicing_name(CmsConstants.default_rest_name+random);
        if(this.getCity() == null)
            this.setCity(CmsConstants.city_code);
        if(this.getPan_no() == null)
            this.setPan_no(Integer.toString(random));
        if(this.getUpdated_by() == null)
            this.setUpdated_by(CmsConstants.email_id);
        if(this.getName() == null)
            this.setName(CmsConstants.default_rest_name+ random);
        if(this.getLocality() == null)
            this.setLocality(CmsConstants.default_locality);
        if(this.getBank_ifsc_code() == null)
            this.setBank_ifsc_code(Integer.toString(random));
        if(this.getInvoicing_email() == null)
            this.setInvoicing_email(CmsConstants.email_id);
        if(this.getBank_name() == null)
            this.setBank_name(CmsConstants.bank_name);
        if(this.getArea() == null)
            this.setArea(CmsConstants.area_code);
        if(this.getBenificiary_name() == null)
            this.setBenificiary_name(CmsConstants.beneficiary_name);
        if(this.getBank_city() == null)
            this.setBank_city(CmsConstants.city);
        if(this.getZ_id() == null)
            this.setZ_id(Integer.toString(random));
        if(this.getBank_account_no() == null)
            this.setBank_account_no(Integer.toString(random));
        if(this.getCommission_exp() == null)
            this.setCommission_exp(CmsConstants.commission_exp);
        if(this.getMou() == null)
            this.setMou(CmsConstants.url);
        if(this.getAddress() == null)
            this.setAddress(CmsConstants.default_address);
        if(this.getTan_no() == null)
            this.setTan_no(Integer.toString(random));
        if(this.getPay_by_system_value() == null)
            this.setPay_by_system_value(CmsConstants.pay_by_system_value);
        if(this.getCancelled_cheque() == null)
            this.setCancelled_cheque(CmsConstants.url);
        if(this.getAgreement_type() == null)
            this.setAgreement_type(CmsConstants.aggrement_type);
        if(this.getLat_long() == null)
            this.setLat_long(Double.toString(CmsConstants.lat)+","+Double.toString(CmsConstants.longi));
        if(this.getGst_state() == null)
            this.setGst_state(CmsConstants.gst_state);
    }

    @Override
    public String toString() {
        return "Restaurant{" +
                "invoicing_name='" + invoicing_name + '\'' +
                ", city='" + city + '\'' +
                ", pan_no='" + pan_no + '\'' +
                ", updated_by='" + updated_by + '\'' +
                ", name='" + name + '\'' +
                ", locality='" + locality + '\'' +
                ", bank_ifsc_code='" + bank_ifsc_code + '\'' +
                ", invoicing_email='" + invoicing_email + '\'' +
                ", bank_name='" + bank_name + '\'' +
                ", area='" + area + '\'' +
                ", benificiary_name='" + benificiary_name + '\'' +
                ", bank_city='" + bank_city + '\'' +
                ", z_id='" + z_id + '\'' +
                ", bank_account_no='" + bank_account_no + '\'' +
                ", commission_exp='" + commission_exp + '\'' +
                ", mou='" + mou + '\'' +
                ", address='" + address + '\'' +
                ", tan_no='" + tan_no + '\'' +
                ", pay_by_system_value='" + pay_by_system_value + '\'' +
                ", cancelled_cheque='" + cancelled_cheque + '\'' +
                ", agreement_type='" + agreement_type + '\'' +
                '}';
    }
}
