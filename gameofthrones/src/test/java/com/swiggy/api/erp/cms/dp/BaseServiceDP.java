package com.swiggy.api.erp.cms.dp;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.constants.CmsConstants;
import com.swiggy.api.erp.cms.constants.MenuConstants;
import com.swiggy.api.erp.cms.helper.BaseServiceHelper;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import com.swiggy.api.erp.cms.pojo.BaseServiceBulkAddon.Addon_Bulk;
import com.swiggy.api.erp.cms.pojo.BaseServiceBulkAddon.BulkAddons;
import com.swiggy.api.erp.cms.pojo.BaseServiceBulkAddonGroup.Addon_group;
import com.swiggy.api.erp.cms.pojo.BaseServiceBulkAddonGroup.BulkAddonGroups;
import com.swiggy.api.erp.cms.pojo.BaseServiceBulkCategory.BulkCategory;
import com.swiggy.api.erp.cms.pojo.BaseServiceBulkItems.BulkItems;
import com.swiggy.api.erp.cms.pojo.BaseServiceBulkItems.Item;
import com.swiggy.api.erp.cms.pojo.BaseServiceBulkVariant.BulkVariants;
import com.swiggy.api.erp.cms.pojo.BaseServiceBulkVariant.Variant;
import com.swiggy.api.erp.cms.pojo.BaseServiceBulkVariantGroup.BulkVariantGroups;
import com.swiggy.api.erp.cms.pojo.BaseServiceBulkVariantGroup.Variant_group;
import com.swiggy.api.erp.cms.pojo.BaseServiceCategory.Category;
import com.swiggy.api.sf.checkout.helper.CommonAPIHelper;

import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.annotations.DataProvider;
import com.swiggy.api.erp.cms.pojo.BaseServiceAddons.Addon;
import java.io.IOException;
import java.util.*;

public class BaseServiceDP {

    static BaseServiceHelper baseServiceHelper = new BaseServiceHelper();
    String timeStamp = baseServiceHelper.getTimeStamp();
    public static int restaurantId = baseServiceHelper.createRestaurantAndReturnID();
    public int restaurantId2 = baseServiceHelper.createRestaurantAndReturnID();
    public int menuId = baseServiceHelper.getMenuIdForRestId(restaurantId);
    public int cat_id = baseServiceHelper.createCategoryIdAndReturnInt(restaurantId);
    public int sub_cat_id = baseServiceHelper.createSubCategoryAndReturnInt(restaurantId,cat_id);
    public String item_id = baseServiceHelper.createItemIdAndReturnString(restaurantId,cat_id,sub_cat_id);
    public String tmp_item_id = baseServiceHelper.createItemIdAndReturnString(restaurantId,cat_id,sub_cat_id);
    ArrayList<Integer> g_addon_group_id = new ArrayList<>();

    public String timestampRandomiser() {
        Random random = new Random();
        int x = random.nextInt(100);
        String timeStamp = baseServiceHelper.getTimeStamp();
        return timeStamp+String.valueOf(x);
    }

    public String createSingleItem(int catID, int subCatID, int addonLimit, int addonFreeLimit) throws IOException {
        JsonHelper jsonHelper = new JsonHelper();
        Item items = new Item();
        items.build(restaurantId);
        items.setCategory_id(catID);
        items.setSub_category_id(subCatID);
        items.setAddon_limit(addonLimit);
        items.setAddon_free_limit(addonFreeLimit);
        String bodyItem = baseServiceHelper.createSingleItemHelper(jsonHelper.getObjectToJSON(items)).ResponseValidator.GetBodyAsText();
        String itemID = JsonPath.read(bodyItem, "$..data..id").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        System.out.println("NEW ITEMID CREATED ::: "+itemID);
        return itemID;
    }

    //returns: ItemID,Name,ThirdPartyId,AddonFreeLimit,AddonLimit,Price,ServiceCharges,Type,servesHowmany,restID
    public ArrayList<String> createSingleItemAndReturnItemData(int catID, int subCatID, int addonLimit, int addonFreeLimit) throws IOException {
        JsonHelper jsonHelper = new JsonHelper();
        ArrayList<String> returnValues = new ArrayList<>();
        Item items = new Item();
        items.build(restaurantId);
        items.setCategory_id(catID);
        items.setSub_category_id(subCatID);
        items.setAddon_limit(addonLimit);
        items.setAddon_free_limit(addonFreeLimit);
        String bodyItem = baseServiceHelper.createSingleItemHelper(jsonHelper.getObjectToJSON(items)).ResponseValidator.GetBodyAsText();
        String itemID = JsonPath.read(bodyItem, "$..data..id").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        System.out.println("NEW ITEMID CREATED ::: "+itemID);
        returnValues.add(itemID);
        returnValues.add(items.getName());
        returnValues.add(items.getThird_party_id());
        returnValues.add(String.valueOf(items.getAddon_free_limit()));
        returnValues.add(String.valueOf(items.getAddon_limit()));
        returnValues.add(String.valueOf(items.getPrice()));
        returnValues.add(items.getService_charges());
        returnValues.add(items.getType());
        returnValues.add(String.valueOf(items.getServes_how_many()));
        returnValues.add(String.valueOf(items.getRestaurant_id()));
        return returnValues;
    }



    public String createSingleAddon (String addonGroupId) throws IOException {
        Addon addon = new Addon();
        JsonHelper jsonHelper = new JsonHelper();
        addon.build(restaurantId);
        addon.setAddon_group_ids(new int[] {Integer.parseInt(addonGroupId)});
        String bodyAddon = baseServiceHelper.createAddonHelper(jsonHelper.getObjectToJSON(addon)).ResponseValidator.GetBodyAsText();
        String addon_id = JsonPath.read(bodyAddon, "$..data..id").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        System.out.println("ADDON_ID ::: "+addon_id);
        return addon_id;
    }

    //returns: addonID,restID,name,thirdPartyID,price,inStock,active
    public List<String> createSingleAddonAndReturnData (Integer[] addonGroupId, int restID) throws IOException {
        Addon addon = new Addon();
        JsonHelper jsonHelper = new JsonHelper();
        List<String> values = new ArrayList<>();
        addon.build(restaurantId);
        int[] agID = new int[addonGroupId.length];
        for (int i = 0; i < addonGroupId.length; i++) {
            agID[i] = addonGroupId[i].intValue();
        }
        if(restID == 0)
            addon.setRestaurant_id(restaurantId);
        else
            addon.setRestaurant_id(restID);
        addon.setAddon_group_ids(agID);
        String bodyAddon = baseServiceHelper.createAddonHelper(jsonHelper.getObjectToJSON(addon)).ResponseValidator.GetBodyAsText();
        String addon_id = JsonPath.read(bodyAddon, "$..data..id").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        System.out.println("ADDON_ID ::: "+addon_id);
        values.add(addon_id);
        values.add(String.valueOf(addon.getRestaurant_id()));
        values.add(addon.getName());
        values.add(addon.getThird_party_id());
        values.add(String.valueOf(addon.getPrice()));
        values.add(String.valueOf(addon.getIn_stock()));
        values.add(String.valueOf(addon.getActive()));
        return values;
    }

    public String[] createSingleItemDP(HashMap<String, String> keyValue) {
        LinkedHashMap<String, String> def_hm = new LinkedHashMap<>();
        LinkedHashMap<String, String> tmp_hm = new LinkedHashMap<>();
        def_hm.put("active", "true");
        def_hm.put("addon_description", "Testing Addons - Toppings");
        def_hm.put("addon_free_limit", "10");
        def_hm.put("addon_limit", "50");
        def_hm.put("category_id",String.valueOf(cat_id));
        def_hm.put("eligible_for_long_distance","0");
        def_hm.put("enabled","1");
        def_hm.put("in_stock","1");
        def_hm.put("is_discoverable","true");
        def_hm.put("is_perishable","1");
        def_hm.put("is_spicy", "1");
        def_hm.put("is_veg","1");
        def_hm.put("name", "Automation_CMS_baseService"+baseServiceHelper.getTimeStamp());
        def_hm.put("order","0");
        def_hm.put("packing_charges","0");
        def_hm.put("packing_slab_count","0");
        def_hm.put("price", "100");
        def_hm.put("recommended","true");
        def_hm.put("restaurant_id",String.valueOf(restaurantId));
        def_hm.put("serves_how_many","1");
        def_hm.put("service_charges","string");
        def_hm.put("service_tax", "string");
        def_hm.put("sub_category_id",String.valueOf(sub_cat_id));
        def_hm.put("type", "REGULAR_ITEM");
        def_hm.put("third_party_id",("Automation"+baseServiceHelper.getTimeStamp()));
        def_hm.put("variant_description","string");
        def_hm.put("vat","string");
        def_hm.put("image_id","");

        tmp_hm.putAll(def_hm);

        for (HashMap.Entry<String,String> map : keyValue.entrySet()) {
            if (tmp_hm.containsKey(map.getKey()) && !map.getKey().equals("null")) {
                tmp_hm.put(map.getKey(),map.getValue());
            }
            else {
                System.out.println("Invalid/NULL values given for creating ITEMID. Default values will be used to create new ITEM");
                break;
            }
        }

        ArrayList<String> a1 = new ArrayList<>();
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a1.add(map.getValue());
        }

        String[] payload = a1.toArray(new String[0]);
        return payload;
    }

    public String[] createSingleAddonGroup(HashMap<String, String> keyValue) {

        LinkedHashMap<String, String> def_hm = new LinkedHashMap<>();
        LinkedHashMap<String, String> tmp_hm = new LinkedHashMap<>();
        def_hm.put("active","true");
        def_hm.put("addonFreeLimit","2");
        def_hm.put("addonMaxLimit", "2");
        def_hm.put("addonMinLimit", "0");
        def_hm.put("itemId", item_id );
        def_hm.put("name", ("Automation_BaseService_"+timestampRandomiser()));
        def_hm.put("order","0");
        def_hm.put("third_party_id",("Automation_"+timestampRandomiser()));

        tmp_hm.putAll(def_hm);
        for (HashMap.Entry<String,String> map : keyValue.entrySet()) {
            if (tmp_hm.containsKey(map.getKey()) && !map.getKey().equals("null")) {
                tmp_hm.put(map.getKey(),map.getValue());
            }
            else {
                System.out.println("Invalid/NULL values given for creating ADDONGROUPID. Default values will be used to create new ADDONGROUP");
                break;
            }
        }

        ArrayList<String> a1 = new ArrayList<>();
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a1.add(map.getValue());
        }

        String[] payload = a1.toArray(new String[0]);
        return payload;
    }

    public String createSingleVariant(String name, String thirdparty, String variantgroupID, String inStock) {
        String[] payload =  {"1","[VARIANT_PRICE]*0.2","[VARIANT_PRICE]*0.2","false","[VARIANT_PRICE]*0.2",inStock,"0",name,"0","100",thirdparty,variantgroupID};
        String body = baseServiceHelper.createVariant(payload).ResponseValidator.GetBodyAsText();
        String variantID = JsonPath.read(body, "$..data..uniqueId").toString().replace("[","").replace("]","").replace("\"","").trim();
        return variantID;
    }

    //@Overload
    public String createSingleVariant(Integer variantGroupID) throws IOException {
        Variant variant = new Variant();
        JsonHelper jsonHelper = new JsonHelper();
        variant.build();
        variant.setVariant_group_id(variantGroupID);
        variant.setDefault(1);
        String body = baseServiceHelper.createSingleVariant(jsonHelper.getObjectToJSON(variant)).ResponseValidator.GetBodyAsText();
        String variantID = JsonPath.read(body, "$..data..uniqueId").toString().replace("[","").replace("]","").replace("\"","").trim();
        return  variantID;
    }

    //@Overload
    //returns: variantID, name, thirdParty id, inStock
    public ArrayList<String> createSingleVariant(Integer variantGroupID, Integer inStock) throws IOException {
        Variant variant = new Variant();
        JsonHelper jsonHelper = new JsonHelper();
        ArrayList<String> returnValues = new ArrayList<>();
        variant.build();
        variant.setVariant_group_id(variantGroupID);
        variant.setDefault(1);
        variant.setIn_stock(inStock);
        String body = baseServiceHelper.createSingleVariant(jsonHelper.getObjectToJSON(variant)).ResponseValidator.GetBodyAsText();
        String variantID = JsonPath.read(body, "$..data..uniqueId").toString().replace("[","").replace("]","").replace("\"","").trim();
        returnValues.add(variantID);
        returnValues.add(variant.getName());
        returnValues.add(variant.getThird_party_id());
        returnValues.add(String.valueOf(variant.getIn_stock()));
        return returnValues;
    }

    public String createSingleVariantGroup(String itemID, String name, String thirdParty){
        String[] payload = {itemID,name,"0",thirdParty};
        String body = baseServiceHelper.createVariantGroup(payload).ResponseValidator.GetBodyAsText();
        String variantgroupId = JsonPath.read(body, "$..data..uniqueId").toString().replace("[","").replace("]","").replace("\"","").trim();
        return variantgroupId;
    }

    //returns: cat_ID/subCat_ID, name, thirdPartyID, restID, parentCatID, type, isActive, menuType
    public ArrayList<String> createSingleCategoryWithReturnData(int parentCatID,int restID, String menuType, String type) throws IOException {
        ArrayList<String> values = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        Category category = new Category();
        category.build(restaurantId,menuId);
        //if parent category id is set 0 it will take the default value i.e. menu ID
        if(parentCatID != 0)
            category.setParent_category_id(parentCatID);
        category.setMenu_type(menuType);
        category.setType(type);
        if(restID != 0)
            category.setRestaurant_id(restID);
        String body = baseServiceHelper.createCategoryHelper(jsonHelper.getObjectToJSON(category)).ResponseValidator.GetBodyAsText();
        String catID = JsonPath.read(body, "$.data.id").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        System.out.println("NEW CATEGORY/SUBCATEGORY CREATED ::: "+catID);
        values.add(catID);
        values.add(category.getName());
        values.add(category.getThird_party_id());
        values.add(String.valueOf(category.getRestaurant_id()));
        values.add(String.valueOf(category.getParent_category_id()));
        values.add(category.getType());
        values.add(String.valueOf(category.getActive()));
        values.add(category.getMenu_type());
        return values;
    }

    @DataProvider(name = "createVariantGroupDP")
    public Object[][] createVariantGroupDP() {
        //order, third_party
        return new Object[][] {
                {"0", "null"}
        };
    }

    @DataProvider(name = "negativeCreateVariantGroupDP")
    public Object[][] negativeCreateVariantGroupDP() {
        //name, order, third_party, message
        return new Object[][] {
                {"", "0","null", "name - needs to be - NotBlank"},
                {baseServiceHelper.getName(),"-1","null", "order - must be greater than or equal to 0"},
                {baseServiceHelper.getName()+"1","1","Automation_"+timeStamp,"null"},
                {baseServiceHelper.getName()+"2","0","Automation_"+timeStamp, "Duplicate 'VariantGroup' with id null and name '${NAME}' found for third party id '${THIRD_PARTY}'."}
        };
    }

    @DataProvider(name = "updateVariantGroupDP")
    public Object[][] updateVariantGroupDP() {
        //q_variant_group,item_id, name,order, third_party_id, updated_by, status message, status code
        return new Object[][] {
                {"1234",item_id,baseServiceHelper.getName(),"0", "null", "aviral","item is not present for item id:123","0"}, // different itemid
                {"1234","1234",baseServiceHelper.getName(),"0", "null", "aviral","item is not present for item id:123","0"}, // wrong itemid
                {"1","0",baseServiceHelper.getName(),"0", "null", "aviral","null","0"}, //different values of variant group
                {"1234","0",baseServiceHelper.getName(),"-1", "null", "aviral","Invalid values for variantGroup. order is Invalid","0"},// -ve order
                {"1234","0",baseServiceHelper.getName(),"0", "Automation_"+timeStamp+"3", "aviral", "null","1"},//third party id change
                {"1234","0",baseServiceHelper.getName(),"0", "Automation_"+timeStamp+"3", "aviralnigam","null","1"},//updated_by change
                {"1234","0",baseServiceHelper.getName(),"0", "Automation_"+timeStamp+"3", "aviral", "null","1"}, //update name
        };
    }

    @DataProvider(name = "createVariantDP")
    public Object[][] createVariantDP() {
        String name = baseServiceHelper.getName();
        String ts = baseServiceHelper.getTimeStamp();
        return new Object[][] {
                //default,cgst,igst,inclusive,sgst,in_stock,is_veg,name,order,price,third_party,variant_group_id,status,statusMessage
                {"1","[VARIANT_PRICE]*0.2","[VARIANT_PRICE]*0.2","false","[VARIANT_PRICE]*0.2","0","0",name.concat("1"),"0","100","","default_variant_group","1","null"},
                {"1","[VARIANT_PRICE]*0.2","[VARIANT_PRICE]*0.2","false","[VARIANT_PRICE]*0.2","0","0",name.concat("2"),"0","100","","default_variant_group","1","null"},
                {"0","[VARIANT_PRICE]*0.2","[VARIANT_PRICE]*0.2","true","[VARIANT_PRICE]*0.2","0","0",name.concat("3"),"0","100","","default_variant_group","1","null"},
                {"0","[VARIANT_PRICE]*0.2","[VARIANT_PRICE]*0.2","false","[VARIANT_PRICE]*0.2","0","0",name.concat("4"),"0","100","","0","0","variant group not found for given entity"},
                {"0","[VARIANT_PRICE]*0.2","[VARIANT_PRICE]*0.2","false","[VARIANT_PRICE]*0.2","1","0",name.concat("5"),"0","100","","default_variant_group","1","null"},
                {"0","[VARIANT_PRICE]*0.2","[VARIANT_PRICE]*0.2","false","[VARIANT_PRICE]*0.2","0","1",name.concat("6"),"0","100","","default_variant_group","1","null"},
                {"0","[VARIANT_PRICE]*0.2","[VARIANT_PRICE]*0.2","false","[VARIANT_PRICE]*0.2","-1","1",name.concat("7"),"0","100","","default_variant_group","0","inStock - must be greater than or equal to 0"},
                {"0","[VARIANT_PRICE]*0.2","[VARIANT_PRICE]*0.2","false","[VARIANT_PRICE]*0.2","0","-1",name.concat("8"),"0","100","","default_variant_group","0","isVeg - must be greater than or equal to 0"},
                {"0","[VARIANT_PRICE]*0.2","[VARIANT_PRICE]*0.2","false","[VARIANT_PRICE]*0.2","1","0",name.concat("9"),"0","100","Automation"+ts,"default_variant_group","1","null"},
                {"0","[VARIANT_PRICE]*0.2","[VARIANT_PRICE]*0.2","false","[VARIANT_PRICE]*0.2","0","0",name.concat("10"),"0","100","Automation"+ts,"default_variant_group","0","Duplicate 'Variant' with id ${VARIANTID} and name \'"+name.concat("9")+"\' found for third party id \'"+"Automation"+ts+"\'"+"."},
                {"0","[VARIANT_PRICE]*0.2","[VARIANT_PRICE]*0.2","false","[VARIANT_PRICE]*0.2","0","0","","0","100","Automation"+ts,"default_variant_group","0","name - needs to be - NotBlank"},
                {"0","[VARIANT_PRICE]*0.2","[VARIANT_PRICE]*0.2","false","[VARIANT_PRICE]*0.2","0","0",name.concat("11"),"0","-1","Automation"+ts+"2","default_variant_group","0","price - must be greater than or equal to 0"},
                {"0","[VARIANT_PRICE]*0.2","[VARIANT_PRICE]*0.2","false","[VARIANT_PRICE]*0.2","5","0",name.concat("12"),"0","101","Automation"+ts,"default_variant_group","0","inStock - must be less than or equal to 1"},
                {"0","[VARIANT_PRICE]*0.2","[VARIANT_PRICE]*0.2","false","[VARIANT_PRICE]*0.2","0","9",name.concat("13"),"0","101","Automation"+ts+"","default_variant_group","0","isVeg - must be less than or equal to 1"},
                {"0","[VARIANT_PRICE]*0.2","[VARIANT_PRICE]*0.2","false","[VARIANT_PRICE]*0.2","0","0",name.concat("3"),"0","100","","default_variant_group","0","Duplicate entity found with same name and price"},
                {"0","[VARIANT_PRICE]*0.2","[VARIANT_PRICE]*0.2","false","[VARIANT_PRICE]*0.2","-1","-1",name.concat("14"),"-1","100","","default_variant_group","0","isVeg - must be greater than or equal to 0, order - must be greater than or equal to 0, inStock - must be greater than or equal to 0"},//is Veg  , order and is stock  as negative
                {"0","[VARIANT_PRICE]*0.2","[VARIANT_PRICE]*0.2","false","[VARIANT_PRICE]*0.2","0","12",name.concat("15"),"1","100","","default_variant_group","0","isVeg - must be less than or equal to 1"},
                {"1","[VARIANT_PRICE]*0.2","[VARIANT_PRICE]*0.2","false","[VARIANT_PRICE]*0.2","12","1",name.concat("16"),"0","100","","default_variant_group","0","inStock - must be less than or equal to 1"},
                {"12","[VARIANT_PRICE]*0.2","[VARIANT_PRICE]*0.2","false","[VARIANT_PRICE]*0.2","1","1",name.concat("17"),"0","100","","default_variant_group","0","isDefault - must be less than or equal to 1"},
                {"-1","[VARIANT_PRICE]*0.2","[VARIANT_PRICE]*0.2","false","[VARIANT_PRICE]*0.2","1","1",name.concat("18"),"0","100","","default_variant_group","0","isDefault - must be greater than or equal to 0"},

        };
    }

    @DataProvider(name = "updateVariantDP")
    public Object[][] updateVariantDP() {
        String name = baseServiceHelper.getName();
        String ts = baseServiceHelper.getTimeStampRandomised();
        return new Object[][] {
                //payload_variant_id, default, cgst, igst, inclusive, sgst, id, in_stock, is_veg, name, order, price, third_party, updated_by, variant_group , status, status_message
                {"default_variant_id","1","[VARIANT_PRICE]*0.3","[VARIANT_PRICE]*0.3","false","[VARIANT_PRICE]*0.2","default_variant_id","0","1","\""+name+timestampRandomiser()+"\"","0","200","","aviral","default_variant_group","1","null"},
                {"default_variant_id","1","[VARIANT_PRICE]*0.3","[VARIANT_PRICE]*0.3","false","[VARIANT_PRICE]*0.2","default_variant_id","0","1","\""+name+timestampRandomiser()+"\"","0","200","","aviral","0","0","parent entity id not present for given entity."},

                // {"default_variant_id","1","[VARIANT_PRICE]*0.3","[VARIANT_PRICE]*0.3","false","[VARIANT_PRICE]*0.2",ts,"0","1","\""+name+timestampRandomiser()+"\"","0","200","","aviral","default_variant_group","0","null"},
                {"default_variant_id","0","[VARIANT_PRICE]*0.3","[VARIANT_PRICE]*0.3","true","[VARIANT_PRICE]*0.2","default_variant_id","0","1","\""+name+timestampRandomiser()+"\"","0","200","","aviral","default_variant_group","1","null"},
                {"1234567801","0","[VARIANT_PRICE]*0.3","[VARIANT_PRICE]*0.3","false","[VARIANT_PRICE]*0.2","default_variant_id","0","1","\""+name+timestampRandomiser()+"\"","0","200","","aviral","default_variant_group","1","null"},
                {"default_variant_id","0","[VARIANT_PRICE]*0.3","[VARIANT_PRICE]*0.3","false","[VARIANT_PRICE]*0.2","default_variant_id","10","1","\""+name+timestampRandomiser()+"\"","0","200","","aviral","default_variant_group","0","Invalid values for variant. inStock is Invalid"},
                {"default_variant_id","0","[VARIANT_PRICE]*0.3","[VARIANT_PRICE]*0.3","false","[VARIANT_PRICE]*0.2","default_variant_id","1","11","\""+name+timestampRandomiser()+"\"","0","200","","aviral","default_variant_group","0","Invalid values for variant. isVeg is Invalid"},
                {"default_variant_id","0","[VARIANT_PRICE]*0.3","[VARIANT_PRICE]*0.3","false","[VARIANT_PRICE]*0.2","default_variant_id","1","1","\""+name+timestampRandomiser()+"\"","0","-200","","aviral","default_variant_group","0","Invalid values for variant. price is Invalid"},
                {"default_variant_id","0","[VARIANT_PRICE]*0.3","[VARIANT_PRICE]*0.3","true","[VARIANT_PRICE]*0.2","default_variant_id","0","1","\""+name+timestampRandomiser()+"\"","0","200","\"null\"","aviral","default_variant_group","1","null"},
                {"default_variant_id","0","[VARIANT_PRICE]*0.3","[VARIANT_PRICE]*0.3","false","[VARIANT_PRICE]*0.2","default_variant_id","1","1","\""+name+timestampRandomiser()+"\"","0","200","null","aviral","default_variant_group","1","null"},
                {"default_variant_id","0","[VARIANT_PRICE]*0.3","[VARIANT_PRICE]*0.3","false","[VARIANT_PRICE]*0.2","default_variant_id","1","1","\""+name+timestampRandomiser()+"\"","0","200","Automation"+timestampRandomiser(),"aviral","default_variant_group","1","null"},
                {"default_variant_id","0","[VARIANT_PRICE]*0.3","[VARIANT_PRICE]*0.3","false","[VARIANT_PRICE]*0.2","default_variant_id","1","1","null","0","200","Automation"+timestampRandomiser(),"aviral","default_variant_group","1","null"},
                {"default_variant_id","0","[VARIANT_PRICE]*0.3","[VARIANT_PRICE]*0.3","false","[VARIANT_PRICE]*0.2","default_variant_id","1","1","\"null\"","0","200","Automation"+timestampRandomiser(),"aviral","default_variant_group","1","null"},
                {"default_variant_id","0","[VARIANT_PRICE]*0.3","[VARIANT_PRICE]*0.3","false","[VARIANT_PRICE]*0.2","default_variant_id","1","1","\"\"","0","200","Automation"+ts,"aviral","default_variant_group","1","null"},
                {"default_variant_id","0","[VARIANT_PRICE]*0.3","[VARIANT_PRICE]*0.3","false","[VARIANT_PRICE]*0.2","default_variant_id","1","1","\""+name+timestampRandomiser()+"\"","0","200","Automation"+timestampRandomiser(),"aviral","default_variant_group","1","null"},
                {"default_variant_id","11","[VARIANT_PRICE]*0.3","[VARIANT_PRICE]*0.3","false","[VARIANT_PRICE]*0.2","default_variant_id","1","1","\""+name+timestampRandomiser()+"\"","0","200","Automation"+timestampRandomiser(),"aviral","default_variant_group","0","Invalid values for variant. isDefault is Invalid"},
                {"default_variant_id","-1","[VARIANT_PRICE]*0.3","[VARIANT_PRICE]*0.3","false","[VARIANT_PRICE]*0.2","default_variant_id","1","1","\""+name+timestampRandomiser()+"\"","0","200","Automation"+timestampRandomiser(),"aviral","default_variant_group","0","Invalid values for variant. isDefault is Invalid"},
                {"default_variant_id","1","[VARIANT_PRICE]*0.3","[VARIANT_PRICE]*0.3","false","[VARIANT_PRICE]*0.2","default_variant_id","1","1","\""+name+timestampRandomiser()+"\"","0","200","Automation"+timestampRandomiser(),"avi","default_variant_group","1","null"},
                {"default_variant_id","1","[VARIANT_PRICE]*0.3","[VARIANT_PRICE]*0.3","false","[VARIANT_PRICE]*0.2","default_variant_id","1","1","\""+name+timestampRandomiser()+"\"","-1","200","Automation"+timestampRandomiser(),"avi","default_variant_group","0","Invalid values for variant. order is Invalid"}
        };
    }
    @DataProvider(name = "searchVariantDP")
    public Object[][] searchVariantDP() {
        return new Object[][] {
                {"global_name","1"},
                {"null","1"},
                {"Automation","1"},
                {"","1"},
                {"\"\"","1"}
        };
    }

    @DataProvider(name = "createItemDP")
    public Object[][] createItemDP() {
        timeStamp = baseServiceHelper.getTimeStamp();
        LinkedHashMap<String, String> def_hm = new LinkedHashMap<>();
        LinkedHashMap<String, String> tmp_hm = new LinkedHashMap<>();
        def_hm.put("active", "true");
        def_hm.put("addon_description", "Testing Addons - Toppings");
        def_hm.put("addon_free_limit", "-1");
        def_hm.put("addon_limit", "-1");
        def_hm.put("category_id",String.valueOf(cat_id));
        def_hm.put("eligible_for_long_distance","0");
        def_hm.put("enabled","1");
        def_hm.put("in_stock","1");
        def_hm.put("is_discoverable","true");
        def_hm.put("is_perishable","1");
        def_hm.put("is_spicy", "1");
        def_hm.put("is_veg","1");
        def_hm.put("name", "Automation_CMS_baseService_"+timestampRandomiser());
        def_hm.put("order","0");
        def_hm.put("packing_charges","0.0");
        def_hm.put("packing_slab_count","0");
        def_hm.put("price", "100.0");
        def_hm.put("recommended","true");
        def_hm.put("restaurant_id",String.valueOf(restaurantId));
        def_hm.put("serves_how_many","1");
        def_hm.put("service_charges","string");
        def_hm.put("service_tax", "string");
        def_hm.put("sub_category_id",String.valueOf(sub_cat_id));
        def_hm.put("type", "REGULAR_ITEM");
        def_hm.put("third_party_id",("Automation_"+timestampRandomiser()));
        def_hm.put("variant_description","string");
        def_hm.put("vat","string");
        def_hm.put("image_id","");



        ArrayList<String> a1 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("third_party_id","Automation"+timeStamp);
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a1.add(map.getValue());
        }
        //dublicate entry
        ArrayList<String> aX = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("third_party_id","Automation"+timeStamp);
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            aX.add(map.getValue());
        }


        //(is stock ,is perishable ,is veg ,is spicy as 0)
        ArrayList<String> a2 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("in_stock","0");
        tmp_hm.put("is_veg", "0");
        tmp_hm.put("is_spicy","0");
        tmp_hm.put("is_perishable","0");
        tmp_hm.put("name", "Automation_CMS_baseService_"+timestampRandomiser());
        tmp_hm.put("third_party_id", "Automation_"+timestampRandomiser());
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a2.add(map.getValue());
        }

        //(is stock ,is perishable ,is veg ,is spicy as -1)
        ArrayList<String> a3 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("in_stock","-1");
        tmp_hm.put("name", "Automation_CMS_baseService_"+timestampRandomiser());
        tmp_hm.put("third_party_id", "Automation_"+timestampRandomiser());
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a3.add(map.getValue());
        }
        ArrayList<String> a4 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("is_veg", "-1");
        tmp_hm.put("name", "Automation_CMS_baseService_"+timestampRandomiser());
        tmp_hm.put("third_party_id", "Automation_"+timestampRandomiser());
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a4.add(map.getValue());
        }
        ArrayList<String> a5 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("is_perishable","-1");
        tmp_hm.put("name", "Automation_CMS_baseService_"+timestampRandomiser());
        tmp_hm.put("third_party_id", "Automation_"+timestampRandomiser());
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a5.add(map.getValue());
        }
        ArrayList<String> a6 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("is_spicy","-1");
        tmp_hm.put("name", "Automation_CMS_baseService_"+timestampRandomiser());
        tmp_hm.put("third_party_id", "Automation_"+timestampRandomiser());
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a6.add(map.getValue());
        }
        ArrayList<String> a7 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("in_stock","8");
        tmp_hm.put("name", "Automation_CMS_baseService_"+timestampRandomiser());
        tmp_hm.put("third_party_id", "Automation_"+timestampRandomiser());
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a7.add(map.getValue());
        }
        ArrayList<String> a8 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("is_veg", "8");
        tmp_hm.put("name", "Automation_CMS_baseService_"+timestampRandomiser());
        tmp_hm.put("third_party_id", "Automation_"+timestampRandomiser());
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a8.add(map.getValue());
        }
        ArrayList<String> a9 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("is_perishable","8");
        tmp_hm.put("name", "Automation_CMS_baseService_"+timestampRandomiser());
        tmp_hm.put("third_party_id", "Automation_"+timestampRandomiser());
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a9.add(map.getValue());
        }
        ArrayList<String> a10 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("is_spicy","8");
        tmp_hm.put("name", "Automation_CMS_baseService_"+timestampRandomiser());
        tmp_hm.put("third_party_id", "Automation_"+timestampRandomiser());
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a10.add(map.getValue());
        }

        ArrayList<String> a11 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("packing_charges","-8.0");
        tmp_hm.put("name", "Automation_CMS_baseService_"+timestampRandomiser());
        tmp_hm.put("third_party_id", "Automation_"+timestampRandomiser());
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a11.add(map.getValue());
        }

        ArrayList<String> a12 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("price","0.0");
        tmp_hm.put("name", "Automation_CMS_baseService_"+timestampRandomiser());
        tmp_hm.put("third_party_id", "Automation_"+timestampRandomiser());
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a12.add(map.getValue());
        }

        ArrayList<String> a13 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("price","-8.0");
        tmp_hm.put("name", "Automation_CMS_baseService_"+timestampRandomiser());
        tmp_hm.put("third_party_id", "Automation_"+timestampRandomiser());
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a13.add(map.getValue());
        }

        ArrayList<String> a14 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("service_charges","-8");
        tmp_hm.put("name", "Automation_CMS_baseService_"+timestampRandomiser());
        tmp_hm.put("third_party_id", "Automation_"+timestampRandomiser());
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a14.add(map.getValue());
        }
        ArrayList<String> a15 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("vat","-8");
        tmp_hm.put("name", "Automation_CMS_baseService_"+timestampRandomiser());
        tmp_hm.put("third_party_id", "Automation_"+timestampRandomiser());
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a15.add(map.getValue());
        }
        ArrayList<String> a16 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("service_tax","-8");
        tmp_hm.put("name", "Automation_CMS_baseService_"+timestampRandomiser());
        tmp_hm.put("third_party_id", "Automation_"+timestampRandomiser());
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a16.add(map.getValue());
        }
        ArrayList<String> a17 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("serves_how_many","-8");
        tmp_hm.put("name", "Automation_CMS_baseService_"+timestampRandomiser());
        tmp_hm.put("third_party_id", "Automation_"+timestampRandomiser());
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a17.add(map.getValue());
        }
        ArrayList<String> a18 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("enabled","0");
        tmp_hm.put("name", "Automation_CMS_baseService_"+timestampRandomiser());
        tmp_hm.put("third_party_id", "Automation_"+timestampRandomiser());
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a18.add(map.getValue());
        }

        ArrayList<String> a19 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("image_id","https://www.swiggy.com/product/gobi-65-9");
        tmp_hm.put("name", "Automation_CMS_baseService_"+timestampRandomiser());
        tmp_hm.put("third_party_id", "Automation_"+timestampRandomiser());
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a19.add(map.getValue());
        }

        ArrayList<String> a20 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("name", "Automation_CMS_baseService_"+timestampRandomiser());
        tmp_hm.put("third_party_id", "Automation_"+timestampRandomiser());
        tmp_hm.put("category_id","1098605123456");
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a20.add(map.getValue());
        }

        ArrayList<String> a21 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("name", "");
        tmp_hm.put("third_party_id", "Automation_"+timestampRandomiser());
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a21.add(map.getValue());
        }

        ArrayList<String> a22 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("name", "Automation_CMS_baseService_"+timestampRandomiser());
        tmp_hm.put("third_party_id", "");
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a22.add(map.getValue());
        }

        //TODO: a11-a17 are bug scenarios... to be done after fix
        return new Object[][] {
                //arraylist obj, message, status code
                {a1, "null", "1"},
                {aX, "Duplicate \'Item\' with id ${ITEM_ID} and name \'"+aX.get(12)+"\' found for third party id \'"+aX.get(24)+"\'.","0"},
                {a2, "null", "1"},
                {a3,"could not execute statement; SQL n\\/a; nested exception is org.hibernate.exception.DataException: could not execute statement", "0"},
                {a4,"could not execute statement; SQL n\\/a; nested exception is org.hibernate.exception.DataException: could not execute statement", "0"},
                {a5,"could not execute statement; SQL n\\/a; nested exception is org.hibernate.exception.DataException: could not execute statement", "0"},
                {a6,"could not execute statement; SQL n\\/a; nested exception is org.hibernate.exception.DataException: could not execute statement", "0"},
                {a7,"null", "1"},
                {a8,"null", "1"},
                {a9,"null", "1"},
                {a10,"null", "1"},
                {a11,"null", "1"},
                {a12,"null", "1"},
                {a13,"null", "1"},
                {a14,"null", "1"},
                {a15,"null", "1"},
                {a16,"null", "1"},
                {a17,"null", "1"},
                {a18,"null", "1"},
                {a19,"null", "1"},
                {a20,"No category found for categoryId:1098605123456","0"},
                {a21,"name - needs to be - NotBlank","0"},
                {a22,"null","1"}
        };
    }

    @DataProvider(name = "createAddonGroupDP")
    public Object[][] createAddonGroupDP() {
        timeStamp = baseServiceHelper.getTimeStamp();

        LinkedHashMap<String, String> def_hm = new LinkedHashMap<>();
        LinkedHashMap<String, String> tmp_hm = new LinkedHashMap<>();
        def_hm.put("active","true");
        def_hm.put("addon_free_limit","2");
        def_hm.put("addon_limit", "2");
        def_hm.put("addon_min_limit", "0");
        def_hm.put("item_id", item_id );
        def_hm.put("name", ("Automation_BaseService_"+timestampRandomiser()));
        def_hm.put("order","0");
        def_hm.put("third_party_id",("Automation_"+timestampRandomiser()));

        //valid input
        ArrayList<String> a1 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("name",("Automation_baseService"+timeStamp));
        tmp_hm.put("third_party_id","Automation"+timeStamp);
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a1.add(map.getValue());
        }

        //dublicate entry third_party
        ArrayList<String> a2 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("third_party_id","Automation"+timeStamp);
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a2.add(map.getValue());
        }

        //non existing itemId
        ArrayList<String> a3 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("item_id", "123456798");
        tmp_hm.put("name",("Automation_baseService"+timestampRandomiser()));
        tmp_hm.put("third_party_id","Automation"+timestampRandomiser());
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a3.add(map.getValue());
        }

        ArrayList<String> a4 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("addon_free_limit", "0");
        tmp_hm.put("name",("Automation_baseService"+timestampRandomiser()));
        tmp_hm.put("third_party_id","Automation"+timestampRandomiser());
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a4.add(map.getValue());
        }

        ArrayList<String> a5 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("addon_limit","0");
        tmp_hm.put("name",("Automation_baseService"+timestampRandomiser()));
        tmp_hm.put("third_party_id","Automation"+timestampRandomiser());
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a5.add(map.getValue());
        }

        ArrayList<String> a6 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("addon_min_limit","1");
        tmp_hm.put("name",("Automation_baseService"+timestampRandomiser()));
        tmp_hm.put("third_party_id","Automation"+timestampRandomiser());
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a6.add(map.getValue());
        }

        ArrayList<String> a7 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("addon_free_limit","-1");
        tmp_hm.put("addon_limit","-1");
        tmp_hm.put("name",("Automation_baseService"+timestampRandomiser()));
        tmp_hm.put("third_party_id","Automation"+timestampRandomiser());
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a7.add(map.getValue());
        }

        //dublicate entry name
        ArrayList<String> a8 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("name",("Automation_baseService"+timeStamp));
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a8.add(map.getValue());
        }

        //null  empty values
        ArrayList<String> a9 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("name","");
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a9.add(map.getValue());
        }
        ArrayList<String> a10 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("name","null");
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a10.add(map.getValue());
        }

        ArrayList<String> a11 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("third_party_id","");
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a11.add(map.getValue());
        }
        ArrayList<String> a12 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("name",("Automation_baseService"+timestampRandomiser()));
        tmp_hm.put("third_party_id","null");
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a12.add(map.getValue());
        }

        ArrayList<String> a13 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("order","-1");
        tmp_hm.put("name",("Automation_baseService"+timestampRandomiser()));
        tmp_hm.put("third_party_id","Automation"+timestampRandomiser());
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a13.add(map.getValue());
        }

        //addon group min limit < 0
        ArrayList<String> a14 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("addon_min_limit","-1");
        tmp_hm.put("name",("Automation_baseService"+timestampRandomiser()));
        tmp_hm.put("third_party_id","Automation"+timestampRandomiser());
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a14.add(map.getValue());
        }

        //same external(third party) id but different item
        ArrayList<String> a15 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("item_id",tmp_item_id);
        tmp_hm.put("name",("Automation_baseService"+timestampRandomiser()));
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a15.add(map.getValue());
        }

        //item level AG test cases
        //item level 50/50,VG -1/-1
        ArrayList<String> a16 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("addon_free_limit","-1");
        tmp_hm.put("addon_limit","-1");
        tmp_hm.put("item_id","${ITEMID}");
        tmp_hm.put("name",("Automation_baseService_"+timestampRandomiser()));
        tmp_hm.put("third_party_id","Automation_"+timestampRandomiser());
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a16.add(map.getValue());
        }

        //item level AG test cases
        //item level 50/50,VG 50/-1
        ArrayList<String> a17 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("addon_free_limit","-1");
        tmp_hm.put("addon_limit","50");
        tmp_hm.put("item_id","${ITEMID}");
        tmp_hm.put("name",("Automation_baseService_"+timestampRandomiser()));
        tmp_hm.put("third_party_id","Automation_"+timestampRandomiser());
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a17.add(map.getValue());
        }

        //item level AG test cases
        //item level 50/50,VG -1/50
        ArrayList<String> a18 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("addon_free_limit","50");
        tmp_hm.put("addon_limit","-1");
        tmp_hm.put("item_id","${ITEMID}");
        tmp_hm.put("name",("Automation_baseService_"+timestampRandomiser()));
        tmp_hm.put("third_party_id","Automation_"+timestampRandomiser());
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a18.add(map.getValue());
        }

        //item level AG test cases
        //item level 50/50,VG -1/50
        ArrayList<String> a19 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("addon_free_limit","-1");
        tmp_hm.put("addon_limit","50");
        tmp_hm.put("item_id","${ITEMID_LEVEL}");
        tmp_hm.put("name",("Automation_baseService_"+timestampRandomiser()));
        tmp_hm.put("third_party_id","Automation_"+timestampRandomiser());
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a19.add(map.getValue());
        }

        //item level AG test cases
        //item level 50/50,VG -1/50
        ArrayList<String> a20 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("addon_free_limit","50");
        tmp_hm.put("addon_limit","-1");
        tmp_hm.put("item_id","${ITEMID_LEVEL}");
        tmp_hm.put("name",("Automation_baseService_"+timestampRandomiser()));
        tmp_hm.put("third_party_id","Automation_"+timestampRandomiser());
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a20.add(map.getValue());
        }



        return new Object[][] {
                {a1, "null", "1"},
                {a2, "External Id of an addon group should be unique.", "0"},
                {a3, "Parent\\/Item is not present for id:"+a3.get(4), "0"},
                {a4, "Addon free limit of an addon group cannot be zero.", "0"},
                {a5, "Addon free limit and addon limit of an addon group making invalid combination.", "0"},
                {a6, "Addon minimum limit of an addon group cannot be greater than number of addons.", "0"},
                {a7, "null", "1"},
                {a8, "Addon group with same name already exists, Cannot update\\/create addon group.", "0"},
                {a9, "name - needs to be - NotBlank", "0"},
                {a10, "null", "1"},
                {a11, "null", "1"},
                {a12, "null", "1"},
                {a13, "order - must be greater than or equal to 0", "0"},
                {a14, "addonMinLimit - must be greater than or equal to 0", "0"},
                {a15, "null", "1"},
                {a16, "Addon free limit and addon limit of an addon group making invalid combination.", "0"},
                {a17, "Addon free limit and addon limit of an addon group making invalid combination.", "0"},
                {a18, "Addon free limit and addon limit of an addon group making invalid combination.", "0"},
                {a19, "Addon free limit and addon limit of an addon group making invalid combination.", "0"},
                {a20, "null", "1"},
        };
    }

    @DataProvider(name = "updateAddonGroupDP")
    public Object[][] updateAddonGroupDP() {
        timeStamp = baseServiceHelper.getTimeStamp();
        LinkedHashMap<String, String> def_hm = new LinkedHashMap<>();
        LinkedHashMap<String, String> tmp_hm = new LinkedHashMap<>();
        def_hm.put("active", "true");
        def_hm.put("addon_free_limit", "2");
        def_hm.put("addon_limit", "2");
        def_hm.put("addon_min_limit", "0");
        def_hm.put("item_id", item_id);
        def_hm.put("name", ("Automation_BaseService_" + timestampRandomiser()));
        def_hm.put("order", "0");
        def_hm.put("third_party_id", ("Automation_" + timestampRandomiser()));
        //def_hm.put("id","${ADDONGROUP}");

        //valid input
        ArrayList<String> a1 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("addon_free_limit","-1");
        tmp_hm.put("addon_limit","-1");
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a1.add(map.getValue());
        }

        //changing 3rd party id
        ArrayList<String> a2 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("third_party_id","Automation"+timeStamp);
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a2.add(map.getValue());
        }

        //non existant id
        ArrayList<String> a3 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("name",("Automation_baseService_"+timestampRandomiser()));
        tmp_hm.put("third_party_id","Automation_"+timestampRandomiser());
        tmp_hm.put("item_id","${INVALIDID}");
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a3.add(map.getValue());
        }

        //addon free and max less than item
        ArrayList<String> a4 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("name",("Automation_baseService_"+timestampRandomiser()));
        tmp_hm.put("third_party_id","Automation_"+timestampRandomiser());
        tmp_hm.put("id","123456879");
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a4.add(map.getValue());
        }

        //addon max / addon more than item level
        ArrayList<String> a5 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("addon_free_limit","10");
        tmp_hm.put("addon_limit","10");
        tmp_hm.put("item_id","${ITEMID}");
        tmp_hm.put("name",("Automation_baseService_"+timestampRandomiser()));
        tmp_hm.put("third_party_id","Automation_"+timestampRandomiser());
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a5.add(map.getValue());
        }

        //addon max more than item level addon max
        ArrayList<String> a6 = new ArrayList<>();
        tmp_hm.putAll(def_hm);
        tmp_hm.put("addon_limit","10");
        tmp_hm.put("item_id","${ITEMID}");
        tmp_hm.put("name",("Automation_baseService_"+timestampRandomiser()));
        tmp_hm.put("third_party_id","Automation_"+timestampRandomiser());
        for (HashMap.Entry<String,String> map : tmp_hm.entrySet()) {
            a6.add(map.getValue());
        }

        return new Object[][] {
                {a1, "null", "1"},
                {a2, "null", "1"},
                {a3, "\'Addon Group' with id 123456879 does not exist", "0"},
                {a4, "null", "1"},
                {a5, "null", "0"},
                {a6, "null", "1"},
        };


    }

    @DataProvider(name = "getAddonGroupByIdDP")
    public Object[][] getAddonGroupByIdDP() {
        return new Object[][] {
                {"${VALID_ADDONGROUP}"},
                {"123456879"}
        };

    }

    @DataProvider(name = "searchAddonGroupDP")
    public Object[][] searchAddonGroupDP() {
        String time = baseServiceHelper.getTimeStamp().concat("0123");
        String name ="Automation_"+time;
        String third_party = "Automation_TP_"+time;
        String itemID = baseServiceHelper.createItemIdAndReturnString(restaurantId,cat_id,sub_cat_id);
        HashMap<String, String> hm = new HashMap<>();
        hm.put("name",name);
        hm.put("item_id",itemID);
        hm.put("third_party_id",third_party);
        String[] tmp_payload = createSingleAddonGroup(hm);
        String body = baseServiceHelper.createAddonGroupHelper(tmp_payload).ResponseValidator.GetBodyAsText();
        //System.out.println("BODY---> "+body);
        return new Object[][] {
                {name,third_party,itemID,true},
                {"Butter or Curd", "1", "106014", true },
                {"123456879", "\"\"", "\"\"",false},
                {"", "\"\"", "\"\"",true},
                {"null", "\"\"", "\"\"",true}
        };

    }

    @DataProvider(name = "createAddonDP")
    public Iterator<Object[]> createAddonDP () throws IOException {
        String itemID = tmp_item_id;
        System.out.println("ITEMID USED ::: "+itemID);
        HashMap<String, String> hm = new HashMap<>();
        hm.put("item_id",itemID);
        hm.put("addon_free_limit","50");
        hm.put("addon_limit","50");
        String[] addongroup = createSingleAddonGroup(hm);
        String bodyAG = baseServiceHelper.createAddonGroupHelper(addongroup).ResponseValidator.GetBodyAsText();
        String get_addon_group_id = JsonPath.read(bodyAG,"$..data..id").toString().replace("[","").replace("]","").replace("\"","").trim();
        if(!get_addon_group_id.equals("") && !get_addon_group_id.equals("0") && !get_addon_group_id.equals("null") && !get_addon_group_id.equals(" ")) {
            g_addon_group_id.add(Integer.parseInt(get_addon_group_id));
            System.out.println("Addon_Group_ID ----> "+get_addon_group_id);
            System.out.println("G_Addon_Group_ID ----> "+g_addon_group_id);
        }
        List<Object[]> obj = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        Addon addon = new Addon();
        addon.build(restaurantId);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(addon),addon.getAddon_group_ids(),"null","1"});

        addon = new Addon();
        addon.build(restaurantId);
        addon.setAddon_group_ids(g_addon_group_id.stream().mapToInt(i -> i).toArray());
        obj.add(new Object[] {jsonHelper.getObjectToJSON(addon),addon.getAddon_group_ids(),"null","1"});

        HashMap<String, String> hm1 = new HashMap<>();
        hm1.put("item_id",itemID);
        hm1.put("addon_free_limit","50");
        hm1.put("addon_limit","50");
        String[] addongroup1 = createSingleAddonGroup(hm1);
        String bodyAG1 = baseServiceHelper.createAddonGroupHelper(addongroup1).ResponseValidator.GetBodyAsText();
        get_addon_group_id = JsonPath.read(bodyAG1,"$..data..id").toString().replace("[","").replace("]","").replace("\"","").trim();
        if(!get_addon_group_id.equals("") && !get_addon_group_id.equals("0") && !get_addon_group_id.equals("null") && !get_addon_group_id.equals(" ")) {
            g_addon_group_id.add(Integer.parseInt(get_addon_group_id));
            System.out.println("Addon_Group_ID ----> "+get_addon_group_id);
            System.out.println("G_Addon_Group_ID ----> "+g_addon_group_id);
        }
        addon = new Addon();
        addon.build(restaurantId);
        addon.setAddon_group_ids(g_addon_group_id.stream().mapToInt(i -> i).toArray());
        obj.add(new Object[] {jsonHelper.getObjectToJSON(addon),addon.getAddon_group_ids(),"null","1"});

        addon = new Addon();
        addon.build(restaurantId);
        g_addon_group_id.add(123456978);
        //debug
        System.out.println("G_ADDON: "+ g_addon_group_id);
        addon.setAddon_group_ids(g_addon_group_id.stream().mapToInt(i -> i).toArray());
        obj.add(new Object[] {jsonHelper.getObjectToJSON(addon),addon.getAddon_group_ids(),"Addon group(s) is\\/are not present for id(s) mentioned.","0"});
        g_addon_group_id.remove((g_addon_group_id.size()-1));
        //debug
        System.out.println("G_ADDON: "+ g_addon_group_id);

        addon = new Addon();
        addon.build(restaurantId);
        int[] tmp = {123456978};
        addon.setAddon_group_ids(tmp);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(addon),tmp,"Addon group(s) is\\/are not present for id(s) mentioned.","0"});
        g_addon_group_id.remove((g_addon_group_id.size()-1));
        System.out.println(g_addon_group_id);

        addon = new Addon();
        addon.build(restaurantId);
        addon.setIs_veg(false);
        addon.setIn_stock(false);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(addon),addon.getAddon_group_ids(),"null","1"});

        addon = new Addon();
        addon.build(restaurantId);
        addon.setAddon_group_ids(new int[] {g_addon_group_id.get(0)});
        addon.setPrice(-100.0);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(addon),addon.getAddon_group_ids(),"price - must be greater than or equal to 0","0"});

        addon = new Addon();
        addon.build(restaurantId);
        addon.setAddon_group_ids(new int[] {g_addon_group_id.get(0)});
        addon.setOrder(-1);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(addon),addon.getAddon_group_ids(),"order - must be greater than or equal to 0","0"});

        addon = new Addon();
        addon.build(restaurantId);
        addon.setAddon_group_ids(new int[] {g_addon_group_id.get(0)});
        addon.setName("");
        obj.add(new Object[] {jsonHelper.getObjectToJSON(addon),addon.getAddon_group_ids(),"name - needs to be - NotBlank","0"});

        addon = new Addon();
        addon.build(restaurantId);
        addon.setAddon_group_ids(new int[] {g_addon_group_id.get(0)});
        addon.setName("null");
        obj.add(new Object[] {jsonHelper.getObjectToJSON(addon),addon.getAddon_group_ids(),"null","1"});

        addon = new Addon();
        addon.build(restaurantId);
        addon.setAddon_group_ids(new int[] {g_addon_group_id.get(0)});
        addon.setName("null");
        obj.add(new Object[] {jsonHelper.getObjectToJSON(addon),addon.getAddon_group_ids(),"null","1"});

        addon = new Addon();
        addon.build(restaurantId);
        addon.setAddon_group_ids(new int[] {g_addon_group_id.get(0)});
        addon.setName("null");
        addon.setThird_party_id("null");
        obj.add(new Object[] {jsonHelper.getObjectToJSON(addon),addon.getAddon_group_ids(),"null","1"});

        addon = new Addon();
        addon.build(restaurantId);
        addon.setAddon_group_ids(new int[] {g_addon_group_id.get(0)});
        addon.setThird_party_id("");
        obj.add(new Object[] {jsonHelper.getObjectToJSON(addon),addon.getAddon_group_ids(),"null","1"});

        addon = new Addon();
        addon.build(restaurantId);
        addon.setAddon_group_ids(new int[] {g_addon_group_id.get(0)});
        addon.setName("null");
        addon.setThird_party_id("null");
        obj.add(new Object[] {jsonHelper.getObjectToJSON(addon),addon.getAddon_group_ids(),"External Id of an addon is not unique.","0"});

        //TODO: is default values allowed other than 0/1?
        addon = new Addon();
        addon.build(restaurantId);
        addon.setAddon_group_ids(new int[] {g_addon_group_id.get(0)});
        addon.setIs_Default(4);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(addon),addon.getAddon_group_ids(),"null","1"});

        return obj.iterator();
    }


    @DataProvider(name = "updateAddonDP")
    public Iterator<Object[]> updateAddonDP() throws IOException {
        String itemID = tmp_item_id;
        System.out.println("ITEMID USED ::: " + itemID);
        HashMap<String, String> hm = new HashMap<>();
        hm.put("item_id",itemID);
        hm.put("addon_free_limit","50");
        hm.put("addon_limit","50");
        String[] addongroup = createSingleAddonGroup(hm);
        String bodyAG = baseServiceHelper.createAddonGroupHelper(addongroup).ResponseValidator.GetBodyAsText();
        String get_addon_group_id = JsonPath.read(bodyAG,"$..data..id").toString().replace("[","").replace("]","").replace("\"","").trim();
        if(!get_addon_group_id.equals("") && !get_addon_group_id.equals("0") && !get_addon_group_id.equals("null") && !get_addon_group_id.equals(" ")) {
            g_addon_group_id.add(Integer.parseInt(get_addon_group_id));
            System.out.println("Addon_Group_ID ----> "+get_addon_group_id);
            System.out.println("G_Addon_Group_ID ----> "+g_addon_group_id);
        }

        List<Object[]> obj = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        String addon_id = createSingleAddon(get_addon_group_id);

        Addon addon = new Addon();
        addon.build(restaurantId);
        addon.setIs_veg(false);
        addon.setIn_stock(false);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(addon),addon_id,"null","1"});

        addon = new Addon();
        addon.build(restaurantId);
        addon.setAddon_group_ids(new int[] {g_addon_group_id.get(0)});
        addon.setPrice(-100.0);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(addon),addon_id,"Invalid values for addon. price is Invalid","0"});

        addon = new Addon();
        addon.build(restaurantId);
        addon.setAddon_group_ids(new int[] {g_addon_group_id.get(0)});
        addon.setOrder(-1);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(addon),addon_id,"Invalid values for addon. order is Invalid","0"});

        addon = new Addon();
        addon.build(restaurantId);
        addon.setAddon_group_ids(new int[] {g_addon_group_id.get(0)});
        addon.setThird_party_id("Automation1234");
        obj.add(new Object[] {jsonHelper.getObjectToJSON(addon),addon_id,"null","1"});

        addon = new Addon();
        addon.build(restaurantId);
        addon.setAddon_group_ids(new int[] {g_addon_group_id.get(0)});
        addon.setName("");
        obj.add(new Object[] {jsonHelper.getObjectToJSON(addon),addon_id,"null","1"});

        addon = new Addon();
        addon.build(restaurantId);
        addon.setAddon_group_ids(g_addon_group_id.stream().mapToInt(i -> i).toArray());
        obj.add(new Object[] {jsonHelper.getObjectToJSON(addon),addon_id,"null","1"});

        addon = new Addon();
        addon.build(restaurantId);
        addon.setAddon_group_ids(new int[] {123456978});
        obj.add(new Object[] {jsonHelper.getObjectToJSON(addon),addon_id,"Addon group(s) is\\/are not present for id(s) mentioned.","0"});

        addon = new Addon();
        addon.build(restaurantId);
        g_addon_group_id.add(123456978);
        //debug
        System.out.println("G_ADDON: "+ g_addon_group_id);
        addon.setAddon_group_ids(g_addon_group_id.stream().mapToInt(i -> i).toArray());
        obj.add(new Object[] {jsonHelper.getObjectToJSON(addon),addon_id,"Addon group(s) is\\/are not present for id(s) mentioned.","0"});
        g_addon_group_id.remove((g_addon_group_id.size()-1));
        //debug
        System.out.println("G_ADDON: "+ g_addon_group_id);

        addon = new Addon();
        addon.build(restaurantId);
        addon.setAddon_group_ids(new int[] {g_addon_group_id.get(0)});
        int new_restID = 333;
        addon.setRestaurant_id(new_restID);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(addon),addon_id,"Restaurant addon map is not present for addon id :"+addon_id+" and restaurant id : "+new_restID,"0"});

        addon = new Addon();
        addon.build(restaurantId);
        addon.setAddon_group_ids(new int[] {g_addon_group_id.get(0)});
        obj.add(new Object[] {jsonHelper.getObjectToJSON(addon),"123456879","No value present","0"});

        return obj.iterator();
    }

    @DataProvider(name = "getAddonByIdDP")
    public Object[][] getAddonByIdDP() throws IOException {
        String itemID = tmp_item_id;
        System.out.println("ITEMID USED ::: "+itemID);
        HashMap<String, String> hm = new HashMap<>();
        hm.put("item_id",itemID);
        hm.put("addon_free_limit","50");
        hm.put("addon_limit","50");
        String[] addongroup = createSingleAddonGroup(hm);
        String bodyAG = baseServiceHelper.createAddonGroupHelper(addongroup).ResponseValidator.GetBodyAsText();
        String get_addon_group_id = JsonPath.read(bodyAG,"$..data..id").toString().replace("[","").replace("]","").replace("\"","").trim();
        if(!get_addon_group_id.equals("") && !get_addon_group_id.equals("0") && !get_addon_group_id.equals("null") && !get_addon_group_id.equals(" ")) {
            g_addon_group_id.add(Integer.parseInt(get_addon_group_id));
            System.out.println("Addon_Group_ID ----> "+get_addon_group_id);
            System.out.println("G_Addon_Group_ID ----> "+g_addon_group_id);
        }
        String addon_id = createSingleAddon(get_addon_group_id);
        return new Object[][] {
                {addon_id, "null", "1"},
                {"1234568790","\'Addon' with id 123456879 does not exist","0"}
        };
    }


    @DataProvider(name = "searchAddonDP")
    public Object[][] searchAddonDP() throws IOException {
        String itemID = tmp_item_id;
        String timeStamp = baseServiceHelper.getTimeStamp();
        System.out.println("ITEMID USED ::: "+itemID);
        HashMap<String, String> hm = new HashMap<>();
        hm.put("item_id",itemID);
        hm.put("addon_free_limit","50");
        hm.put("addon_limit","50");
        String[] addongroup = createSingleAddonGroup(hm);
        String bodyAG = baseServiceHelper.createAddonGroupHelper(addongroup).ResponseValidator.GetBodyAsText();
        String get_addon_group_id = JsonPath.read(bodyAG,"$..data..id").toString().replace("[","").replace("]","").replace("\"","").trim();
        if(!get_addon_group_id.equals("") && !get_addon_group_id.equals("0") && !get_addon_group_id.equals("null") && !get_addon_group_id.equals(" ")) {
            g_addon_group_id.add(Integer.parseInt(get_addon_group_id));
            System.out.println("Addon_Group_ID ----> "+get_addon_group_id);
            System.out.println("G_Addon_Group_ID ----> "+g_addon_group_id);
        }
        JsonHelper jsonHelper = new JsonHelper();
        Addon addon = new Addon();
        addon.build(restaurantId);
        addon.setAddon_group_ids(new int[] {Integer.parseInt(get_addon_group_id)});
        String new_Name = "Automation_"+timeStamp;
        addon.setName(new_Name);
        String bodyAddon = baseServiceHelper.createAddonHelper(jsonHelper.getObjectToJSON(addon)).ResponseValidator.GetBodyAsText();
        String addon_id = JsonPath.read(bodyAddon, "$..data..id").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();

        return new Object[][] {
                {addon_id,new_Name,get_addon_group_id,"null",true},
                {addon_id,"",get_addon_group_id,"null",true},
                {addon_id,"12345697801",get_addon_group_id,"null",false},
                {addon_id,"null",get_addon_group_id,"null",true}
        };

    }

    @DataProvider(name = "updateItemDP")
    public Iterator<Object[]> updateItemDP() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        int cat_id = baseServiceHelper.createCategoryIdAndReturnInt(restaurantId);
        int sub_cat_id = baseServiceHelper.createSubCategoryAndReturnInt(restaurantId,cat_id);
        System.out.println("CAT_ID: "+cat_id);
        System.out.println("SUB_CAT_ID: "+sub_cat_id);
        String item_id = createSingleItem(cat_id,sub_cat_id,50,50);
        String t_name = "AutomationBaseService123401234";
        String t_third_party = "Automation12345";


        Item items = new Item();
        items.build(restaurantId);
        items.setName(t_name);
        items.setThird_party_id(t_third_party);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(items),item_id,items.getName(),items.getThird_party_id(),items.getAddon_free_limit(),items.getAddon_limit(),items.getPrice(),items.getCategory_id(),"null","1"});


        items = new Item();
        items.build(restaurantId);
        items.setName(t_name);
        items.setThird_party_id(t_third_party);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(items),item_id,items.getName(),items.getThird_party_id(),items.getAddon_free_limit(),items.getAddon_limit(),items.getPrice(),items.getCategory_id(),"null","1"});

        items = new Item();
        items.build(restaurantId);
        items.setName("");
        obj.add(new Object[] {jsonHelper.getObjectToJSON(items),item_id,t_name,items.getThird_party_id(),items.getAddon_free_limit(),items.getAddon_limit(),items.getPrice(),items.getCategory_id(),"null","1"});

        items = new Item();
        items.build(restaurantId);
        items.setName("null");
        obj.add(new Object[] {jsonHelper.getObjectToJSON(items),item_id,t_name,items.getThird_party_id(),items.getAddon_free_limit(),items.getAddon_limit(),items.getPrice(),items.getCategory_id(),"null","1"});

        items = new Item();
        items.build(restaurantId);
        items.setIn_stock(0);
        items.setIs_spicy(0);
        items.setIs_veg(0);
        items.setIs_perishable(0);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(items),item_id,items.getName(),items.getThird_party_id(),items.getAddon_free_limit(),items.getAddon_limit(),items.getPrice(),items.getCategory_id(),"null","1"});

        items = new Item();
        items.build(restaurantId);
        items.setIs_spicy(-1);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(items),item_id,items.getName(),items.getThird_party_id(),items.getAddon_free_limit(),items.getAddon_limit(),items.getPrice(),items.getCategory_id(),"could not execute statement; SQL n\\/a; nested exception is org.hibernate.exception.DataException: could not execute statement","0"});

        items = new Item();
        items.build(restaurantId);
        items.setIs_perishable(-1);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(items),item_id,items.getName(),items.getThird_party_id(),items.getAddon_free_limit(),items.getAddon_limit(),items.getPrice(),items.getCategory_id(),"could not execute statement; SQL n\\/a; nested exception is org.hibernate.exception.DataException: could not execute statement","0"});

        items = new Item();
        items.build(restaurantId);
        items.setIn_stock(-1);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(items),item_id,items.getName(),items.getThird_party_id(),items.getAddon_free_limit(),items.getAddon_limit(),items.getPrice(),items.getCategory_id(),"could not execute statement; SQL n\\/a; nested exception is org.hibernate.exception.DataException: could not execute statement","0"});

        items = new Item();
        items.build(restaurantId);
        items.setIs_veg(-1);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(items),item_id,items.getName(),items.getThird_party_id(),items.getAddon_free_limit(),items.getAddon_limit(),items.getPrice(),items.getCategory_id(),"could not execute statement; SQL n\\/a; nested exception is org.hibernate.exception.DataException: could not execute statement","0"});

        items = new Item();
        items.build(restaurantId);
        items.setIs_perishable(8);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(items),item_id,items.getName(),items.getThird_party_id(),items.getAddon_free_limit(),items.getAddon_limit(),items.getPrice(),items.getCategory_id(),"null","1"});

        items = new Item();
        items.build(restaurantId);
        items.setIn_stock(8);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(items),item_id,items.getName(),items.getThird_party_id(),items.getAddon_free_limit(),items.getAddon_limit(),items.getPrice(),items.getCategory_id(),"null","1"});
        items = new Item();

        items.build(restaurantId);
        items.setIs_spicy(8);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(items),item_id,items.getName(),items.getThird_party_id(),items.getAddon_free_limit(),items.getAddon_limit(),items.getPrice(),items.getCategory_id(),"null","1"});

        items = new Item();
        items.build(restaurantId);
        items.setIs_veg(8);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(items),item_id,items.getName(),items.getThird_party_id(),items.getAddon_free_limit(),items.getAddon_limit(),items.getPrice(),items.getCategory_id(),"null","1"});

        //TODO: FIX message after bug fix
        items = new Item();
        items.build(restaurantId);
        items.setPacking_charges(-1.0);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(items),item_id,items.getName(),items.getThird_party_id(),items.getAddon_free_limit(),items.getAddon_limit(),items.getPrice(),items.getCategory_id(),"null","0"});

        //TODO: FIX message after bug fix
        items = new Item();
        items.build(restaurantId);
        items.setPrice(-100.0);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(items),item_id,items.getName(),items.getThird_party_id(),items.getAddon_free_limit(),items.getAddon_limit(),items.getPrice(),items.getCategory_id(),"null","0"});

        //TODO: FIX message after bug fix
        items = new Item();
        items.build(restaurantId);
        items.setVat("-10");
        obj.add(new Object[] {jsonHelper.getObjectToJSON(items),item_id,items.getName(),items.getThird_party_id(),items.getAddon_free_limit(),items.getAddon_limit(),items.getPrice(),items.getCategory_id(),"null","0"});

        //TODO: FIX message after bug fix
        items = new Item();
        items.build(restaurantId);

        items.setService_tax("-10");
        obj.add(new Object[] {jsonHelper.getObjectToJSON(items),item_id,items.getName(),items.getThird_party_id(),items.getAddon_free_limit(),items.getAddon_limit(),items.getPrice(),items.getCategory_id(),"null","0"});

        //TODO: FIX message after bug fix
        items = new Item();
        items.build(restaurantId);

        items.setService_charges("-10");
        obj.add(new Object[] {jsonHelper.getObjectToJSON(items),item_id,items.getName(),items.getThird_party_id(),items.getAddon_free_limit(),items.getAddon_limit(),items.getPrice(),items.getCategory_id(),"null","0"});

        //TODO: FIX message after bug fix
        items = new Item();
        items.build(restaurantId);
        items.setService_tax("-10");
        obj.add(new Object[] {jsonHelper.getObjectToJSON(items),item_id,items.getName(),items.getThird_party_id(),items.getAddon_free_limit(),items.getAddon_limit(),items.getPrice(),items.getCategory_id(),"null","0"});

        //TODO: FIX message after bug fix
        items = new Item();
        items.build(restaurantId);
        items.setServes_how_many(-2);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(items),item_id,items.getName(),items.getThird_party_id(),items.getAddon_free_limit(),items.getAddon_limit(),items.getPrice(),items.getCategory_id(),"null","0"});

        items = new Item();
        items.build(restaurantId);
        String tmp_tpi = items.getThird_party_id();
        items.setImage_id("https://www.swiggy.com/product/gobi-65-9");
        obj.add(new Object[] {jsonHelper.getObjectToJSON(items),item_id,items.getName(),items.getThird_party_id(),items.getAddon_free_limit(),items.getAddon_limit(),items.getPrice(),items.getCategory_id(),"null","1"});

        items = new Item();
        items.build(restaurantId);
        items.setThird_party_id("");
        obj.add(new Object[] {jsonHelper.getObjectToJSON(items),item_id,items.getName(),tmp_tpi,items.getAddon_free_limit(),items.getAddon_limit(),items.getPrice(),items.getCategory_id(),"null","1"});

        items = new Item();
        items.build(restaurantId);
        items.setThird_party_id("null");
        obj.add(new Object[] {jsonHelper.getObjectToJSON(items),item_id,items.getName(),tmp_tpi,items.getAddon_free_limit(),items.getAddon_limit(),items.getPrice(),items.getCategory_id(),"null","1"});

        items = new Item();
        items.build(restaurantId);
        int invalid_catID = 1234560987;
        items.setCategory_id(invalid_catID);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(items),item_id,items.getName(),tmp_tpi,items.getAddon_free_limit(),items.getAddon_limit(),items.getPrice(),items.getCategory_id(),"No category found for categoryId:"+String.valueOf(invalid_catID),"0"});

        items = new Item();
        items.build(restaurantId);
        items.setEnabled(0);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(items),item_id,items.getName(),items.getThird_party_id(),items.getAddon_free_limit(),items.getAddon_limit(),items.getPrice(),items.getCategory_id(),"null","1"});

        items = new Item();
        items.build(restaurantId);
        items.setSub_category_id(null);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(items),item_id,items.getName(), items.getThird_party_id(),items.getAddon_free_limit(),items.getAddon_limit(),items.getPrice(),items.getCategory_id(),"null","1"});

        items = new Item();
        items.build(restaurantId);
        items.setType("POP_ITEM");
        obj.add(new Object[] {jsonHelper.getObjectToJSON(items),item_id,items.getName(), items.getThird_party_id(),items.getAddon_free_limit(),items.getAddon_limit(),items.getPrice(),items.getCategory_id(),"null","1"});

        return obj.iterator();
    }

    @DataProvider(name = "getItemByIdDP")
    public Iterator<Object[]> getItemByIdDP() throws IOException {
        JsonHelper jsonHelper = new JsonHelper();
        int catID = baseServiceHelper.createCategoryIdAndReturnInt(restaurantId);
        int subCatID = baseServiceHelper.createSubCategoryAndReturnInt(restaurantId,catID);
        ArrayList<String> getItemDetails1 = createSingleItemAndReturnItemData(catID, subCatID,50,50);
        String itemID1 = getItemDetails1.get(0);

        List<Object[]> obj = new ArrayList<>();
        ArrayList<String> to_validate = new ArrayList<>();
        List<String> variantIds = new ArrayList<>();
        List<String> variantGroupIds = new ArrayList<>();
        List<String> addon_id =  new ArrayList<>();
        List<String> addon_group_id = new ArrayList<>();

        //default valid input with values null
        variantIds.add("null");
        variantGroupIds.add("null");
        addon_id.add("null");
        addon_group_id.add("null");

        //Name,ThirdPartyId,AddonFreeLimit,AddonLimit,Price,ServiceCharges,Type,servesHowmany,restID
        //to_validate.add(itemID);
        obj.add(new Object[] {itemID1,getItemDetails1,catID,subCatID,variantGroupIds,variantIds,addon_group_id,addon_id,"Item Found Successfully","1"});

        //1A-1AG with same itemID
        ArrayList<String> getItemDetails2 = createSingleItemAndReturnItemData(catID,subCatID,50,50);
        String itemID2 = getItemDetails2.get(0);
        addon_id =  new ArrayList<>();
        addon_group_id = new ArrayList<>();
        HashMap<String, String> hm = new HashMap<>();
        hm.put("item_id",itemID2);
        String[] addonGroupPayload = createSingleAddonGroup(hm);
        String bodyAddonGroup = baseServiceHelper.createAddonGroupHelper(addonGroupPayload).ResponseValidator.GetBodyAsText();
        String addonGroupID = JsonPath.read(bodyAddonGroup, "$..data..id").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        addon_group_id.add(addonGroupID);
        String addonID = createSingleAddon(addonGroupID);
        addon_id.add(addonID);
        obj.add(new Object[] {itemID2,getItemDetails2,catID,subCatID,variantGroupIds,variantIds,addon_group_id,addon_id,"Item Found Successfully","1"});

        //2A-1AG with same itemID
        addon_id =  new ArrayList<>();
        addon_group_id = new ArrayList<>();
        ArrayList<String> getItemDetails3 = createSingleItemAndReturnItemData(catID,subCatID,50,50);
        String itemID3 = getItemDetails3.get(0);
        hm = new HashMap<>();
        hm.put("item_id",itemID3);
        String[] addonGroupPayload1 = createSingleAddonGroup(hm);
        String bodyAddonGroup1 = baseServiceHelper.createAddonGroupHelper(addonGroupPayload1).ResponseValidator.GetBodyAsText();
        String addonGroupID1 = JsonPath.read(bodyAddonGroup1, "$..data..id").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        addon_group_id.add(addonGroupID1);
        String addonID1 = createSingleAddon(addonGroupID1);
        addon_id.add(addonID1);
        String new_addonID = createSingleAddon(addonGroupID1);
        addon_id.add(new_addonID);
        obj.add(new Object[] {itemID3,getItemDetails3,catID,subCatID,variantGroupIds,variantIds,addon_group_id,addon_id,"Item Found Successfully","1"});

        //2A-2AG with same itemID
        addon_id =  new ArrayList<>();
        addon_group_id = new ArrayList<>();
        ArrayList<String> getItemDetails4 = createSingleItemAndReturnItemData(catID,subCatID,50,50);
        String itemID4 = getItemDetails4.get(0);
        HashMap<String, String> hm1 = new HashMap<>();
        hm1.put("item_id",itemID4);
        String[] addonGroupPayload2 = createSingleAddonGroup(hm1);
        String bodyAddonGroup2 = baseServiceHelper.createAddonGroupHelper(addonGroupPayload2).ResponseValidator.GetBodyAsText();
        String addonGroupID2 = JsonPath.read(bodyAddonGroup2, "$..data..id").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        addon_group_id.add(addonGroupID2);
        hm = new HashMap<>();
        hm.put("item_id",itemID4);
        String[] new_addonGroupPayload = createSingleAddonGroup(hm);
        String new_bodyAddonGroup = baseServiceHelper.createAddonGroupHelper(new_addonGroupPayload).ResponseValidator.GetBodyAsText();
        String new_addonGroupID = JsonPath.read(new_bodyAddonGroup, "$..data..id").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        addon_group_id.add(new_addonGroupID);
        String new_addon = createSingleAddon(new_addonGroupID);
        addon_id.add(new_addon);
        obj.add(new Object[] {itemID4,getItemDetails4,catID,subCatID,variantGroupIds,variantIds,addon_group_id,addon_id,"Item Found Successfully","1"});

        //1V-1VG with same itemID
        variantGroupIds =  new ArrayList<>();
        variantIds = new ArrayList<>();
        addon_id = new ArrayList<>();
        addon_id.add("null");
        addon_group_id = new ArrayList<>();
        addon_group_id.add("null");

        String name_vg = "Test_VG_"+timestampRandomiser();
        String name_v = "Test_V_"+timestampRandomiser();
        String tpID = "Testing_"+timestampRandomiser();

        ArrayList<String> getItemDetails5 = createSingleItemAndReturnItemData(catID,subCatID,50,50);
        String itemID5 = getItemDetails5.get(0);
        variantGroupIds.add(createSingleVariantGroup(itemID5,name_vg,tpID));
        variantIds.add(createSingleVariant(name_v,tpID,variantGroupIds.get(0),"1"));
        obj.add(new Object[] {itemID5,getItemDetails5,catID,subCatID,variantGroupIds,variantIds,addon_group_id,addon_id,"Item Found Successfully","1"});

        //2V-1VG with same itemID
        variantGroupIds =  new ArrayList<>();
        variantIds = new ArrayList<>();
        ArrayList<String> getItemDetails6 = createSingleItemAndReturnItemData(catID,subCatID,50,50);
        String itemID6 = getItemDetails6.get(0);
        variantGroupIds.add(createSingleVariantGroup(itemID6,name_vg,tpID));
        variantIds.add(createSingleVariant(name_v,tpID,variantGroupIds.get(0),"1"));
        variantIds.add(createSingleVariant("new_"+name_v, "new_"+tpID,variantGroupIds.get(0),"1"));
        obj.add(new Object[] {itemID6,getItemDetails6,catID,subCatID,variantGroupIds,variantIds,addon_group_id,addon_id,"Item Found Successfully","1"});

        //3V-2VG with same itemID
        variantGroupIds =  new ArrayList<>();
        variantIds = new ArrayList<>();
        ArrayList<String> getItemDetails7 = createSingleItemAndReturnItemData(catID,subCatID,50,50);
        String itemID7 = getItemDetails7.get(0);
        variantGroupIds.add(createSingleVariantGroup(itemID7,name_vg,tpID));
        variantIds.add(createSingleVariant(name_v,tpID,variantGroupIds.get(0),"1"));
        variantIds.add(createSingleVariant("new_"+name_v, "new_"+tpID,variantGroupIds.get(0),"1"));
        variantGroupIds.add(createSingleVariantGroup(itemID7,"new1_"+name_vg,"new1_"+tpID));
        variantIds.add(createSingleVariant("new1_"+name_v, "new1_"+tpID,variantGroupIds.get(1),"1"));
        obj.add(new Object[] {itemID7,getItemDetails7,catID,subCatID,variantGroupIds,variantIds,addon_group_id,addon_id,"Item Found Successfully","1"});

        obj.add(new Object[] {"1234567987",getItemDetails7,catID,subCatID,variantGroupIds,variantIds,addon_group_id,addon_id,"null","1"});

        return obj.iterator();

        //TODO: FIX THIS ONCE CREATE VARIANT WORKS
    }

    @DataProvider(name = "searchItemDP")
    public Object[][] searchItemDP() throws IOException {
        int catID = baseServiceHelper.createCategoryIdAndReturnInt(restaurantId);
        int subCatID = baseServiceHelper.createSubCategoryAndReturnInt(restaurantId,catID);
        ArrayList<String> getItemDetails = createSingleItemAndReturnItemData(catID,subCatID,50,50);
        ArrayList<String> itemDetailsWithInvalid = new ArrayList<>(getItemDetails);
        ArrayList<String> itemDetailsWithNull = new ArrayList<>(getItemDetails);
        ArrayList<String> itemDetailsWithEmpty = new ArrayList<>(getItemDetails);
        itemDetailsWithEmpty.add(1,"");
        itemDetailsWithInvalid.add(1,"123456789098765");
        itemDetailsWithNull.add(1,"null");

        return new Object[][] {
                {getItemDetails,"null",true},
                {itemDetailsWithInvalid,"null",false},
                {itemDetailsWithNull,"null",true},
                {itemDetailsWithEmpty,"null",true}
        };
    }


    private ArrayList<String> categoryDPHelper(Category category) {
        //name, thirdPartyID, restID, parentCatID, type, isActive, menuType
        ArrayList<String> values = new ArrayList<>();
        values.add(category.getName());
        values.add(category.getThird_party_id());
        values.add(String.valueOf(category.getRestaurant_id()));
        values.add(String.valueOf(category.getParent_category_id()));
        values.add(category.getType());
        values.add(String.valueOf(category.getActive()));
        values.add(category.getMenu_type());
        return values;
    }

    @DataProvider(name = "createCategoryDP")
    public Iterator<Object[]> createCategoryDP() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        int catID = baseServiceHelper.createCategoryIdAndReturnInt(restaurantId);

        Category category = new Category();
        category.build(restaurantId, menuId);
        ArrayList<String> values = categoryDPHelper(category);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(category), values,"null", "1"});

        category = new Category();
        category.build(restaurantId, menuId);
        category.setParent_category_id(catID);
        category.setType("SubCat");
        values = new ArrayList<>(categoryDPHelper(category));
        obj.add(new Object[] {jsonHelper.getObjectToJSON(category), values,"null", "1"});

        category = new Category();
        category.build(restaurantId, menuId);
        category.setRestaurant_id(1234237898);

        values = new ArrayList<>(categoryDPHelper(category));
        obj.add(new Object[] {jsonHelper.getObjectToJSON(category), values,"null", "1"});

        category = new Category();
        category.build(restaurantId, menuId);
        category.setName("");
        values = new ArrayList<>(categoryDPHelper(category));
        obj.add(new Object[] {jsonHelper.getObjectToJSON(category), values,"name - needs to be - NotBlank", "0"});

        category = new Category();
        category.build(restaurantId, menuId);
        category.setName("null");
        values = new ArrayList<>(categoryDPHelper(category));
        obj.add(new Object[] {jsonHelper.getObjectToJSON(category), values,"Duplicate Category with id 1342047 and name 'null' found.", "0"});

        category = new Category();
        category.build(restaurantId, menuId);
        category.setThird_party_id("");
        values = new ArrayList<>(categoryDPHelper(category));
        obj.add(new Object[] {jsonHelper.getObjectToJSON(category), values,"null", "1"});

        category = new Category();
        category.build(restaurantId, menuId);
        category.setThird_party_id("null");
        values = new ArrayList<>(categoryDPHelper(category));
        obj.add(new Object[] {jsonHelper.getObjectToJSON(category), values,"Duplicate Category with id 1342049 and name 'Automation_BaseService_11289614622840840' found for third party id null.", "0"});

        category = new Category();
        category.build(restaurantId, menuId);
        category.setParent_category_id(1234987634);
        values = new ArrayList<>(categoryDPHelper(category));
        obj.add(new Object[] {jsonHelper.getObjectToJSON(category), values,"null", "0"});

        category = new Category();
        category.build(restaurantId, menuId);
        category.setType("null");
        values = new ArrayList<>(categoryDPHelper(category));
        obj.add(new Object[] {jsonHelper.getObjectToJSON(category), values,"null", "400"});

        category = new Category();
        category.build(restaurantId, menuId);
        category.setActive(false);
        values = new ArrayList<>(categoryDPHelper(category));
        values.set(5,"true");
        obj.add(new Object[] {jsonHelper.getObjectToJSON(category), values,"null", "1"});

        category = new Category();
        category.build(restaurantId, menuId);
        category.setMenu_type("null");
        values = new ArrayList<>(categoryDPHelper(category));
        obj.add(new Object[] {jsonHelper.getObjectToJSON(category), values,"null", "400"});

        return obj.iterator();
    }

    @DataProvider(name = "updateCategoryDP")
    public Iterator<Object[]> updateCategoryDP() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        ArrayList<String> getCategoryData = new ArrayList<>(createSingleCategoryWithReturnData(0,0,"REGULAR_MENU","Cat"));
        String catID = getCategoryData.get(0);

        Category category = new Category();
        category.build(restaurantId, menuId);
        ArrayList<String> values = new ArrayList<>(categoryDPHelper(category));
        obj.add(new Object[] {jsonHelper.getObjectToJSON(category), catID,values,"null", "1"});

        category = new Category();
        category.build(restaurantId, menuId);
        category.setType("SubCat");
        category.setParent_category_id(Integer.parseInt(catID));
        values = new ArrayList<>(categoryDPHelper(category));
        obj.add(new Object[] {jsonHelper.getObjectToJSON(category), catID,values,"null", "1"});

        category = new Category();
        category.build(restaurantId, menuId);
        category.setRestaurant_id(1234237898);
        values = new ArrayList<>(categoryDPHelper(category));
        obj.add(new Object[] {jsonHelper.getObjectToJSON(category),catID, values,"null", "1"});

        category = new Category();
        category.build(restaurantId, menuId);
        category.setParent_category_id(1234987634);
        values = new ArrayList<>(categoryDPHelper(category));
        obj.add(new Object[] {jsonHelper.getObjectToJSON(category),catID,values,"null", "0"});

        category = new Category();
        category.build(restaurantId, menuId);
        category.setThird_party_id("Automate_"+timestampRandomiser());
        values = new ArrayList<>(categoryDPHelper(category));
        obj.add(new Object[] {jsonHelper.getObjectToJSON(category),catID,values,"null", "1"});

        category = new Category();
        category.build(restaurantId, menuId);
        category.setName("Automate_Base_"+timestampRandomiser());
        values = new ArrayList<>(categoryDPHelper(category));
        obj.add(new Object[] {jsonHelper.getObjectToJSON(category),catID,values,"null", "1"});

        category = new Category();
        category.build(restaurantId, menuId);
        category.setType("null");
        values = new ArrayList<>(categoryDPHelper(category));
        obj.add(new Object[] {jsonHelper.getObjectToJSON(category),catID, values,"null", "400"});

        category = new Category();
        category.build(restaurantId, menuId);
        category.setActive(false);
        values = new ArrayList<>(categoryDPHelper(category));
        values.set(5,"true");
        obj.add(new Object[] {jsonHelper.getObjectToJSON(category),catID, values,"null", "1"});

        category = new Category();
        category.build(restaurantId, menuId);
        category.setMenu_type("POP_MENU");
        values = new ArrayList<>(categoryDPHelper(category));
        String tmp_name = category.getName();
        obj.add(new Object[] {jsonHelper.getObjectToJSON(category),catID, values,"null", "1"});

        category = new Category();
        category.build(restaurantId, menuId);
        category.setName("");
        values = new ArrayList<>(categoryDPHelper(category));
        values.set(0,tmp_name);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(category), catID,values,"null", "1"});

        category = new Category();
        category.build(restaurantId, menuId);
        category.setName("null");
        values = new ArrayList<>(categoryDPHelper(category));
        values.set(0,tmp_name);
        String tmp_tpi = category.getThird_party_id();
        obj.add(new Object[] {jsonHelper.getObjectToJSON(category), catID,values,"null", "1"});

        category = new Category();
        category.build(restaurantId, menuId);
        category.setThird_party_id("");
        values = new ArrayList<>(categoryDPHelper(category));
        values.set(1,tmp_tpi);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(category), catID,values,"null", "1"});

        category = new Category();
        category.build(restaurantId, menuId);
        category.setThird_party_id("null");
        values = new ArrayList<>(categoryDPHelper(category));
        values.set(1,tmp_tpi);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(category), catID,values,"null", "1"});

        obj.add(new Object[] {jsonHelper.getObjectToJSON(category), "12345679876",values,"Bad Entity to update", "0"});

        return obj.iterator();
    }

    @DataProvider(name = "getCategoryByIdDP")
    public Object[][] getCategoryByIdDP() throws IOException {
        ArrayList<String> values = createSingleCategoryWithReturnData(0,0,"REGULAR_MENU","Cat");
        ArrayList<String> valuesInvalid = createSingleCategoryWithReturnData(0,0,"REGULAR_MENU","Cat");
        ArrayList<String> valuesSubCat = createSingleCategoryWithReturnData(0,0,"REGULAR_MENU","SubCat");
        ArrayList<String> valuesSubCatInvalid = createSingleCategoryWithReturnData(0,0,"REGULAR_MENU","SubCat");
        valuesInvalid.set(0,"12345654098762");
        valuesSubCatInvalid.set(0,"12345654098762");
        return new Object[][] {
                {values,"null","1"},
                {valuesInvalid,"null","0"},
                {valuesSubCat,"null","1"},
                {valuesSubCatInvalid,"null","0"}
        };
    }

    @DataProvider(name = "getCategoryByRestIdDP")
    public Object[][] getCategoryByRestIdDP() {
        return new Object[][] {
                {String.valueOf(restaurantId),"null","1"},
                {"123454398702","Restaurant not found for id:123454398702","0"}
        };
    }

    @DataProvider(name = "getSubCategoryByCatIdDP")
    public Object[][] getSubCategoryByCatIdDP() throws IOException {
        ArrayList<String> values = createSingleCategoryWithReturnData(0,0,"REGULAR_MENU","Cat");
        ArrayList<String> values_subCat = createSingleCategoryWithReturnData(Integer.parseInt(values.get(0)),0,"REGULAR_MENU","SubCat");
        ArrayList<String> values_subCat1 = createSingleCategoryWithReturnData(Integer.parseInt(values.get(0)),0,"REGULAR_MENU","SubCat");
        String[] subcatid =  {values_subCat.get(0),values_subCat1.get(0)};
        String[] subcatname =  {values_subCat.get(1),values_subCat1.get(1)};
        //debug
        System.out.println("SUBCAT IDS IN DP : "+values_subCat.get(0)+", "+values_subCat1.get(0));
        System.out.println("SUBCAT NAMES IN DP : "+values_subCat.get(1)+", "+values_subCat1.get(1));
        //TODO: FIX Invalid case once bug is fixed
        return new Object[][] {
                {values.get(0),subcatid,subcatname,"null","1"},
                {"123454398702",subcatid,subcatname,"null","1"}
        };
    }

    @DataProvider(name = "searchCategoryDP")
    public Object[][] searchCategoryDP() throws IOException {
        //TODO: FIX invalid case messages once bug is fixed
        ArrayList<String> values = createSingleCategoryWithReturnData(0,0,"REGULAR_MENU","Cat");
        String name_cat = values.get(1);
        return new Object[][] {
                {String.valueOf(restaurantId),name_cat,values,"null","1"},
                {"1234565437809",name_cat,values,"null","0"},
                {String.valueOf(restaurantId2),name_cat,values,"null","0"},
                {String.valueOf(restaurantId),"1234567654329",values,"null","0"}
        };
    }

    public String[] createMultipleItems(int n) throws IOException {
        String[] id = new String[n];
        for (int i = 0; i < n; i++) {
            id[i] = createSingleItem(cat_id,sub_cat_id,-1,-1);
        }
        return id;
    }

    public Integer[] createMultipleAddonGroups(String[] itemIds) throws  IOException {
        int n = itemIds.length;
        Integer[] id = new Integer[n];
        HashMap<String, String> hm;
        for (int i = 0; i < n; i++) {
            hm = new HashMap<>();
            hm.put("item_id",itemIds[i]);
            hm.put("addon_free_limit","-1");
            hm.put("addon_limit","-1");
            String[] ag= createSingleAddonGroup(hm);
            String body_ag = baseServiceHelper.createAddonGroupHelper(ag).ResponseValidator.GetBodyAsText();
            id[i]= Integer.parseInt(JsonPath.read(body_ag,"$..data.id").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim());

        }
        return id;
    }

    public List<Addon_group> createMultipleBulkAddonGroups(int n) {
        Addon_group addon_group;
        List<Addon_group> list_ag = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            addon_group = new Addon_group();
            list_ag.add(addon_group.build(restaurantId));
        }
        return list_ag;
    }

    //return: json,delete_by_id,delete_by_third_party,names,tpi,status message,create_error,update_error,delete_error
    @DataProvider(name = "bulkAddonGroupsDP")
    public Iterator<Object[]> bulkAddonGroupsDP() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        ArrayList<Integer> delete_by_id = new ArrayList<>();
        ArrayList<String> delete_by_third_party = new ArrayList<>();
        ArrayList<String> names = new ArrayList<>();
        ArrayList<String> tpi = new ArrayList<>();
        String[] itemIDs = createMultipleItems(4);
        //create only
        BulkAddonGroups bulkAG =  new BulkAddonGroups();
        bulkAG.build();
        List<Addon_group> list_ag = new ArrayList<>(createMultipleBulkAddonGroups(4));
        bulkAG.setAddon_groups(list_ag);
        for (int i = 0; i < list_ag.size(); i++) {
            names.add(list_ag.get(i).getName());
            tpi.add(list_ag.get(i).getThird_party_id());
        }
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkAG),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});
        //update only
        bulkAG = new BulkAddonGroups();
        bulkAG.build();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        Integer[] AGs = createMultipleAddonGroups(itemIDs);
        for (int i = 0; i < list_ag.size() ; i++) {
            list_ag.get(i).setId(String.valueOf(AGs[i]));
            list_ag.get(i).setName("new_Automation_BaseService"+timestampRandomiser());
            list_ag.get(i).setThird_party_id("new_Automation_"+timestampRandomiser());
            names.add(list_ag.get(i).getName());
            tpi.add(list_ag.get(i).getThird_party_id());
        }
        bulkAG.setAddon_groups(list_ag);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkAG),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});
        //delete by tpi only
        bulkAG = new BulkAddonGroups();
        bulkAG.build();
        bulkAG.setDelete_by_third_party_ids(tpi);
        delete_by_third_party = new ArrayList<>(tpi);
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkAG),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});
        //delete by id only
        bulkAG = new BulkAddonGroups();
        bulkAG.build();
        delete_by_third_party = new ArrayList<>();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        Integer[] addongroups = createMultipleAddonGroups(itemIDs);
        bulkAG.setDelete_by_ids(Arrays.asList(addongroups));
        delete_by_id = new ArrayList<>(Arrays.asList(addongroups));
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkAG),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});
        //create x, update y , delete z
        bulkAG = new BulkAddonGroups();
        bulkAG.build();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        list_ag = new ArrayList<>(createMultipleBulkAddonGroups(3));
        Integer[] ags = createMultipleAddonGroups(itemIDs);
        //update index 0
        list_ag.get(0).setId(String.valueOf(ags[0]));
        for (int i = 0; i < list_ag.size(); i++) {
            names.add(list_ag.get(i).getName());
            tpi.add(list_ag.get(i).getThird_party_id());
        }
        bulkAG.setAddon_groups(list_ag);
        bulkAG.setDelete_by_ids(Arrays.asList(ags[1],ags[2]));
        delete_by_id = new ArrayList<>(Arrays.asList(ags[1],ags[2]));
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkAG),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});

        //create x, update y , delete by third party id z
        bulkAG = new BulkAddonGroups();
        bulkAG.build();
        names = new ArrayList<>();
        delete_by_id = new ArrayList<>();
        list_ag = new ArrayList<>(createMultipleBulkAddonGroups(4));
        Integer[] AG1 = createMultipleAddonGroups(itemIDs);
        //Delete by tpi 3rd from above
        bulkAG.setDelete_by_third_party_ids(Arrays.asList(tpi.get(2)));
        //update index 0
        tpi = new ArrayList<>();
        list_ag.get(0).setId(String.valueOf(AG1[0]));
        for (int i = 0; i < list_ag.size(); i++) {
            names.add(list_ag.get(i).getName());
            tpi.add(list_ag.get(i).getThird_party_id());
        }
        bulkAG.setAddon_groups(list_ag);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkAG),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});

        //create x, delete x, update x
        //create x
        list_ag = new ArrayList<>(createMultipleBulkAddonGroups(4));
        Integer[] x = createMultipleAddonGroups(itemIDs);
        //delete x
        bulkAG = new BulkAddonGroups();
        bulkAG.build();
        delete_by_third_party =  new ArrayList<>();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        bulkAG.setDelete_by_ids(Arrays.asList(x[0]));
        delete_by_id = new ArrayList<>(Arrays.asList(x[0]));
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkAG),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});
        //update x
        bulkAG = new BulkAddonGroups();
        bulkAG.build();
        delete_by_id = new ArrayList<>();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        ArrayList<Addon_group> list = new ArrayList<>(list_ag);
        list.get(0).setId(String.valueOf(x[0]));
        list.get(0).setName("new_Automation_BaseService"+timestampRandomiser());
        list.get(0).setThird_party_id("new_Automation_"+timestampRandomiser());
        names.add(list.get(0).getName());
        tpi.add(list.get(0).getThird_party_id());
        bulkAG.setAddon_groups(list);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkAG),delete_by_id,delete_by_third_party,names,tpi,"null","false","true","false"});

        //duplicate create
        bulkAG = new BulkAddonGroups();
        bulkAG.build();
        Addon_group addon_group = new Addon_group();
        delete_by_id = new ArrayList<>();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        list_ag = new ArrayList<>();
        list_ag.add(addon_group.build(restaurantId));
        list_ag.add(addon_group.build(restaurantId));
        names.add(list_ag.get(0).getName());
        tpi.add(list_ag.get(0).getThird_party_id());
        bulkAG.setAddon_groups(list_ag);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkAG),delete_by_id,delete_by_third_party,names,tpi,"null","true","false","false"});

        //twice update
        bulkAG = new BulkAddonGroups();
        bulkAG.build();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        list_ag = new ArrayList<>(createMultipleBulkAddonGroups(1));
        list_ag.get(0).setId(String.valueOf(x[1]));
        list_ag.get(0).setName("new_Automation_BaseService"+timestampRandomiser());
        list_ag.get(0).setThird_party_id("new_Automation_"+timestampRandomiser());
        names.add(list_ag.get(0).getName());
        tpi.add(list_ag.get(0).getThird_party_id());
        bulkAG.setAddon_groups(list_ag);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkAG),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});
        bulkAG = new BulkAddonGroups();
        bulkAG.build();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        list_ag.get(0).setName("new1_Automation_BaseService"+timestampRandomiser());
        list_ag.get(0).setThird_party_id("new1_Automation_"+timestampRandomiser());
        names.add(list_ag.get(0).getName());
        tpi.add(list_ag.get(0).getThird_party_id());
        bulkAG.setAddon_groups(list_ag);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkAG),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});

        //valid create invalid update - order value negative
        bulkAG = new BulkAddonGroups();
        bulkAG.build();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        delete_by_id = new ArrayList<>();
        delete_by_third_party = new ArrayList<>();
        list_ag = new ArrayList<>(createMultipleBulkAddonGroups(2));
        list_ag.get(0).setId(String.valueOf(addongroups[1]));
        list_ag.get(0).setName("Automation_"+timestampRandomiser());
        list_ag.get(0).setOrder(-1);
        names.add(list_ag.get(0).getName());
        tpi.add(list_ag.get(0).getThird_party_id());
        bulkAG.setAddon_groups(list_ag);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkAG),delete_by_id,delete_by_third_party,names,tpi,"null","false","true","false"});

        //invalid delete by id
        bulkAG = new BulkAddonGroups();
        bulkAG.build();
        delete_by_third_party =  new ArrayList<>();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        bulkAG.setDelete_by_ids(Arrays.asList(1235654328));
        delete_by_id = new ArrayList<>(Arrays.asList(1235654328));
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkAG),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","true"});

        //invalid delete by tpi
        bulkAG = new BulkAddonGroups();
        bulkAG.build();
        delete_by_id =  new ArrayList<>();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        bulkAG.setDelete_by_third_party_ids(Arrays.asList("YO_YO_1235654328"));
        delete_by_third_party = new ArrayList<>(Arrays.asList("YO_YO_1235654328"));
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkAG),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","true"});

        return obj.iterator();
    }

    public List<Integer> createMultipleAddons(int n,Integer[] addonGroupIDs) throws IOException {
        //int n = addonGroupIDs.length;
        List<Integer> addon = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            addon.add(Integer.parseInt(createSingleAddonAndReturnData(addonGroupIDs,0).get(0)));
        }
        return addon;
    }

    public List<Addon_Bulk> createMultipleBulkAddonObjectList(int n,Integer[] addonGroupIds) {
        Addon_Bulk addon;
        List<Addon_Bulk> list_a = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            addon = new Addon_Bulk();
            addon.build(restaurantId);
            addon.setAddon_group_ids(Arrays.asList(addonGroupIds));
            list_a.add(addon);
        }
        return list_a;
    }

    @DataProvider(name = "bulkAddonsDP")
    public Iterator<Object[]> bulkAddonsDP() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        ArrayList<Integer> delete_by_id = new ArrayList<>();
        ArrayList<String> delete_by_third_party = new ArrayList<>();
        ArrayList<String> names = new ArrayList<>();
        ArrayList<String> tpi = new ArrayList<>();
        List<Integer> Addons;
        String[] itemIDs = createMultipleItems(4);
        Integer[] addonGroupIDs = createMultipleAddonGroups(itemIDs);
        //create only
        BulkAddons bulkAddons = new BulkAddons();
        bulkAddons.build();
        List<Addon_Bulk> list_a = createMultipleBulkAddonObjectList(4,addonGroupIDs);
        bulkAddons.setAddons(list_a);
        for (int i = 0; i < list_a.size(); i++) {
            names.add(list_a.get(i).getName());
            tpi.add(list_a.get(i).getThird_party_id());
        }
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkAddons),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});

        //update only
        bulkAddons = new BulkAddons();
        bulkAddons.build();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        Addons = createMultipleAddons(4,addonGroupIDs);
        for (int i = 0; i < list_a.size() ; i++) {
            list_a.get(i).setId(String.valueOf(Addons.get(i)));
            list_a.get(i).setName("new_Automation_BaseService"+timestampRandomiser());
            list_a.get(i).setThird_party_id("new_Automation_"+timestampRandomiser());
            names.add(list_a.get(i).getName());
            tpi.add(list_a.get(i).getThird_party_id());
        }
        bulkAddons.setAddons(list_a);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkAddons),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});

        //delete by tpi only
        bulkAddons = new BulkAddons();
        bulkAddons.build();
        bulkAddons.setDelete_by_third_party_ids(tpi);
        delete_by_third_party = new ArrayList<>(tpi);
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkAddons),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});
        //delete by id only
        bulkAddons = new BulkAddons();
        bulkAddons.build();
        delete_by_third_party = new ArrayList<>();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        Addons = new ArrayList<>(createMultipleAddons(4,addonGroupIDs));
        bulkAddons.setDelete_by_ids(Addons);
        delete_by_id = new ArrayList<>(Addons);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkAddons),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});
        //create x, update y , delete z
        //TODO: FIX once create/update simultaneously bug is fixed
        bulkAddons = new BulkAddons();
        bulkAddons.build();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        list_a = new ArrayList<>(createMultipleBulkAddonObjectList(4,addonGroupIDs));
        Addons = new ArrayList<>(createMultipleAddons(4,addonGroupIDs));
        //update index 0
        list_a.get(0).setId(String.valueOf(Addons.get(0)));
        for (int i = 0; i < list_a.size(); i++) {
            names.add(list_a.get(i).getName());
            tpi.add(list_a.get(i).getThird_party_id());
        }
        bulkAddons.setAddons(list_a);
        bulkAddons.setDelete_by_ids(Arrays.asList(Addons.get(1),Addons.get(2)));
        delete_by_id = new ArrayList<>(Arrays.asList(Addons.get(1),Addons.get(2)));
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkAddons),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});

        //create x, update y , delete by third party id z
        bulkAddons = new BulkAddons();
        bulkAddons.build();
        names = new ArrayList<>();
        delete_by_id = new ArrayList<>();
        list_a = new ArrayList<>(createMultipleBulkAddonObjectList(4,addonGroupIDs));
        Addons = new ArrayList<>(createMultipleAddons(4,addonGroupIDs));
        //Delete by tpi 3rd from above
        bulkAddons.setDelete_by_third_party_ids(Arrays.asList(tpi.get(2)));
        //update index 0
        tpi = new ArrayList<>();
        list_a.get(0).setId(String.valueOf(Addons.get(0)));
        for (int i = 0; i < list_a.size(); i++) {
            names.add(list_a.get(i).getName());
            tpi.add(list_a.get(i).getThird_party_id());
        }
        bulkAddons.setAddons(list_a);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkAddons),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});


        //create x, delete x, update x
        //create x
        list_a = new ArrayList<>(createMultipleBulkAddonObjectList(4,addonGroupIDs));
        Addons = new ArrayList<>(createMultipleAddons(4,addonGroupIDs));
        //delete x
        bulkAddons = new BulkAddons();
        bulkAddons.build();
        delete_by_third_party =  new ArrayList<>();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        bulkAddons.setDelete_by_ids(Arrays.asList(Addons.get(0)));
        delete_by_id = new ArrayList<>(Arrays.asList(Addons.get(0)));
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkAddons),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});
        //update x
        //TODO: Fix when bug is fixed once INTERNAL_SERVER_ERROR issue is fixed
        bulkAddons = new BulkAddons();
        bulkAddons.build();
        delete_by_id = new ArrayList<>();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        ArrayList<Addon_Bulk> list = new ArrayList<>(list_a);
        list.get(0).setId(String.valueOf(Addons.get(0)));
        list.get(0).setName("new_Automation_BaseService"+timestampRandomiser());
        list.get(0).setThird_party_id("new_Automation_"+timestampRandomiser());
        names.add(list.get(0).getName());
        tpi.add(list.get(0).getThird_party_id());
        bulkAddons.setAddons(list);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkAddons),delete_by_id,delete_by_third_party,names,tpi,"null","false","true","false"});
        //duplicate create
        bulkAddons = new BulkAddons();
        bulkAddons.build();
        Addon_Bulk addon_bulk = new Addon_Bulk();
        addon_bulk.build(restaurantId);
        delete_by_id = new ArrayList<>();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        list_a = new ArrayList<>();
        addon_bulk.setAddon_group_ids(Arrays.asList(addonGroupIDs));
        list_a.add(addon_bulk);
        list_a.add(addon_bulk);
        names.add(list_a.get(0).getName());
        tpi.add(list_a.get(0).getThird_party_id());
        bulkAddons.setAddons(list_a);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkAddons),delete_by_id,delete_by_third_party,names,tpi,"null","true","false","false"});
        //twice update
        //TODO: UPDATE ISSUE Bug
        bulkAddons = new BulkAddons();
        bulkAddons.build();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        list_a = new ArrayList<>(createMultipleBulkAddonObjectList(1,addonGroupIDs));
        list_a.get(0).setId(String.valueOf(Addons.get(0)));
        list_a.get(0).setName("new_Automation_BaseService"+timestampRandomiser());
        list_a.get(0).setThird_party_id("new_Automation_"+timestampRandomiser());
        names.add(list_a.get(0).getName());
        tpi.add(list_a.get(0).getThird_party_id());
        bulkAddons.setAddons(list_a);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkAddons),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});
        bulkAddons = new BulkAddons();
        bulkAddons.build();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        list_a.get(0).setName("new1_Automation_BaseService"+timestampRandomiser());
        list_a.get(0).setThird_party_id("new1_Automation_"+timestampRandomiser());
        names.add(list_a.get(0).getName());
        tpi.add(list_a.get(0).getThird_party_id());
        bulkAddons.setAddons(list_a);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkAddons),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});

        //valid create invalid update - order value negative
        bulkAddons = new BulkAddons();
        bulkAddons.build();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        delete_by_id = new ArrayList<>();
        delete_by_third_party = new ArrayList<>();
        list_a = new ArrayList<>(createMultipleBulkAddonObjectList(1, addonGroupIDs));
        list_a.get(0).setId(String.valueOf(Addons.get(1)));
        list_a.get(0).setName("Automation_"+timestampRandomiser());
        list_a.get(0).setOrder(-1);
        names.add(list_a.get(0).getName());
        tpi.add(list_a.get(0).getThird_party_id());
        bulkAddons.setAddons(list_a);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkAddons),delete_by_id,delete_by_third_party,names,tpi,"null","false","true","false"});

        //update to different restID
        bulkAddons = new BulkAddons();
        bulkAddons.build();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        delete_by_id = new ArrayList<>();
        delete_by_third_party = new ArrayList<>();
        list_a = new ArrayList<>(createMultipleBulkAddonObjectList(1, addonGroupIDs));
        list_a.get(0).setId(String.valueOf(Addons.get(3)));
        list_a.get(0).setName("Automation_"+timestampRandomiser());
        list_a.get(0).setRestaurant_id(restaurantId2);
        names.add(list_a.get(0).getName());
        tpi.add(list_a.get(0).getThird_party_id());
        bulkAddons.setAddons(list_a);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkAddons),delete_by_id,delete_by_third_party,names,tpi,"null","false","true","false"});

        //invalid delete by id
        //TODO: FIX BELOW 2 cases when non-existent values update bug is fixed
        bulkAddons = new BulkAddons();
        bulkAddons.build();
        delete_by_third_party =  new ArrayList<>();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        bulkAddons.setDelete_by_ids(Arrays.asList(1235654328));
        delete_by_id = new ArrayList<>(Arrays.asList(1235654328));
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkAddons),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","true"});
        //invalid delete by tpi
        bulkAddons = new BulkAddons();
        bulkAddons.build();
        delete_by_id =  new ArrayList<>();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        bulkAddons.setDelete_by_third_party_ids(Arrays.asList("YO_YO_1235654328"));
        delete_by_third_party = new ArrayList<>(Arrays.asList("YO_YO_1235654328"));
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkAddons),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","true"});

        return obj.iterator();
    }


    public List<Integer> createMultipleVariantGroup(int n, String itemID) throws  IOException {
        //int n = addonGroupIDs.length;
        List<Integer> vg = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            String name = "Automation_BaseService_"+timestampRandomiser();
            String tpi = "Automation_"+timestampRandomiser();
            vg.add(Integer.parseInt(createSingleVariantGroup(itemID,name,tpi)));
        }
        return vg;
    }

    public List<Variant_group> createMultipleBulkVariantGroupObjectList(int n, String itemID) {
        Variant_group vg;
        List<Variant_group> list_vg = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            vg = new Variant_group();
            vg.build(restaurantId);
            vg.setItem_id(Integer.parseInt(itemID));
            list_vg.add(vg);
        }
        return list_vg;
    }

    @DataProvider(name = "bulkVariantGroupsDP")
    public Iterator<Object[]> bulkVariantGroupsDP() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        ArrayList<Integer> delete_by_id = new ArrayList<>();
        ArrayList<String> delete_by_third_party = new ArrayList<>();
        ArrayList<String> names = new ArrayList<>();
        ArrayList<String> tpi = new ArrayList<>();
        List<Integer> vgObj;
        String[] itemIDs = createMultipleItems(2);
        //create only
        BulkVariantGroups bulkVG = new BulkVariantGroups();
        bulkVG.build();
        List<Variant_group> list_vg = createMultipleBulkVariantGroupObjectList(4,itemIDs[0]);
        bulkVG.setVariant_groups(list_vg);
        for (int i = 0; i < list_vg.size(); i++) {
            names.add(list_vg.get(i).getName());
            tpi.add(list_vg.get(i).getThird_party_id());
        }
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkVG),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});

        //update only
        bulkVG = new BulkVariantGroups();
        bulkVG.build();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        vgObj = createMultipleVariantGroup(4,itemIDs[0]);
        for (int i = 0; i < list_vg.size() ; i++) {
            list_vg.get(i).setId(vgObj.get(i));
            list_vg.get(i).setName("new_Automation_BaseService"+timestampRandomiser());
            list_vg.get(i).setThird_party_id("new_Automation_"+timestampRandomiser());
            names.add(list_vg.get(i).getName());
            tpi.add(list_vg.get(i).getThird_party_id());
        }
        bulkVG.setVariant_groups(list_vg);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkVG),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});

        //delete by tpi only
        //TODO: FIX failed to update by tpi bug not working
        bulkVG = new BulkVariantGroups();
        bulkVG.build();
        bulkVG.setDelete_by_third_party_ids(tpi);
        delete_by_third_party = new ArrayList<>(tpi);
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkVG),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});
        //delete by id only
        bulkVG = new BulkVariantGroups();
        bulkVG.build();
        delete_by_third_party = new ArrayList<>();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        vgObj = new ArrayList<>(createMultipleVariantGroup(4,itemIDs[0]));
        bulkVG.setDelete_by_ids(vgObj);
        delete_by_id = new ArrayList<>(vgObj);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkVG),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});
        //create x, update y , delete z
        bulkVG = new BulkVariantGroups();
        bulkVG.build();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        list_vg = new ArrayList<>(createMultipleBulkVariantGroupObjectList(4,itemIDs[0]));
        vgObj = new ArrayList<>(createMultipleVariantGroup(4,itemIDs[0]));
        //update index 0
        list_vg.get(0).setId(vgObj.get(0));
        for (int i = 0; i < list_vg.size(); i++) {
            names.add(list_vg.get(i).getName());
            tpi.add(list_vg.get(i).getThird_party_id());
        }
        bulkVG.setVariant_groups(list_vg);
        bulkVG.setDelete_by_ids(Arrays.asList(vgObj.get(1),vgObj.get(2)));
        delete_by_id = new ArrayList<>(Arrays.asList(vgObj.get(1),vgObj.get(2)));
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkVG),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});

        //create x, update y , delete by third party id z
        bulkVG = new BulkVariantGroups();
        bulkVG.build();
        names = new ArrayList<>();
        delete_by_id = new ArrayList<>();
        list_vg = new ArrayList<>(createMultipleBulkVariantGroupObjectList(4,itemIDs[0]));
        vgObj = new ArrayList<>(createMultipleVariantGroup(4,itemIDs[0]));
        //Delete by tpi 3rd from above
        bulkVG.setDelete_by_third_party_ids(Arrays.asList(tpi.get(2)));
        //update index 0
        tpi = new ArrayList<>();
        list_vg.get(0).setId(vgObj.get(0));
        for (int i = 0; i < list_vg.size(); i++) {
            names.add(list_vg.get(i).getName());
            tpi.add(list_vg.get(i).getThird_party_id());
        }
        bulkVG.setVariant_groups(list_vg);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkVG),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});

        //create x, delete x, update x
        //create x
        list_vg = new ArrayList<>(createMultipleBulkVariantGroupObjectList(4,itemIDs[0]));
        vgObj = new ArrayList<>(createMultipleVariantGroup(4,itemIDs[0]));
        //delete x
        bulkVG = new BulkVariantGroups();
        bulkVG.build();
        delete_by_third_party =  new ArrayList<>();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        bulkVG.setDelete_by_ids(Arrays.asList(vgObj.get(0)));
        delete_by_id = new ArrayList<>(Arrays.asList(vgObj.get(0)));
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkVG),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});
        //update x
        bulkVG = new BulkVariantGroups();
        bulkVG.build();
        delete_by_id = new ArrayList<>();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        ArrayList<Variant_group> list = new ArrayList<>(list_vg);
        list.get(0).setId(vgObj.get(0));
        list.get(0).setName("new_Automation_BaseService"+timestampRandomiser());
        list.get(0).setThird_party_id("new_Automation_"+timestampRandomiser());
        names.add(list.get(0).getName());
        tpi.add(list.get(0).getThird_party_id());
        bulkVG.setVariant_groups(list);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkVG),delete_by_id,delete_by_third_party,names,tpi,"null","false","true","false"});
        //duplicate create
        bulkVG = new BulkVariantGroups();
        bulkVG.build();
        Variant_group variant_group = new Variant_group();
        delete_by_id = new ArrayList<>();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        list_vg = new ArrayList<>();
        variant_group.build(restaurantId);
        variant_group.setItem_id(Integer.parseInt(itemIDs[0]));
        list_vg.add(variant_group);
        list_vg.add(variant_group);
        names.add(list_vg.get(0).getName());
        tpi.add(list_vg.get(0).getThird_party_id());
        bulkVG.setVariant_groups(list_vg);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkVG),delete_by_id,delete_by_third_party,names,tpi,"null","true","false","false"});
        //twice update
        //TODO: UPDATE ISSUE Bug
        bulkVG = new BulkVariantGroups();
        bulkVG.build();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        list_vg = new ArrayList<>(createMultipleBulkVariantGroupObjectList(1,itemIDs[0]));
        list_vg.get(0).setId(vgObj.get(0));
        list_vg.get(0).setName("new_Automation_BaseService"+timestampRandomiser());
        list_vg.get(0).setThird_party_id("new_Automation_"+timestampRandomiser());
        names.add(list_vg.get(0).getName());
        tpi.add(list_vg.get(0).getThird_party_id());
        bulkVG.setVariant_groups(list_vg);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkVG),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});
        bulkVG = new BulkVariantGroups();
        bulkVG.build();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        list_vg.get(0).setName("new1_Automation_BaseService"+timestampRandomiser());
        list_vg.get(0).setThird_party_id("new1_Automation_"+timestampRandomiser());
        names.add(list_vg.get(0).getName());
        tpi.add(list_vg.get(0).getThird_party_id());
        bulkVG.setVariant_groups(list_vg);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkVG),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});

        //valid create invalid update - order value negative
        bulkVG = new BulkVariantGroups();
        bulkVG.build();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        delete_by_id = new ArrayList<>();
        delete_by_third_party = new ArrayList<>();
        list_vg = new ArrayList<>(createMultipleBulkVariantGroupObjectList(2,itemIDs[0]));
        list_vg.get(0).setId(vgObj.get(1));
        list_vg.get(0).setName("Automation_"+timestampRandomiser());
        list_vg.get(0).setOrder(-1);
        names.add(list_vg.get(0).getName());
        tpi.add(list_vg.get(0).getThird_party_id());
        bulkVG.setVariant_groups(list_vg);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkVG),delete_by_id,delete_by_third_party,names,tpi,"null","false","true","false"});


        //invalid delete by id
        //TODO: FIX BELOW 2 cases when non-existent values update bug is fixed
        bulkVG = new BulkVariantGroups();
        bulkVG.build();
        delete_by_third_party =  new ArrayList<>();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        bulkVG.setDelete_by_ids(Arrays.asList(1235654328));
        delete_by_id = new ArrayList<>(Arrays.asList(1235654328));
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkVG),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","true"});
        //invalid delete by tpi
        bulkVG = new BulkVariantGroups();
        bulkVG.build();
        delete_by_id =  new ArrayList<>();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        bulkVG.setDelete_by_third_party_ids(Arrays.asList("YO_YO_1235654328"));
        delete_by_third_party = new ArrayList<>(Arrays.asList("YO_YO_1235654328"));
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkVG),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","true"});


        return obj.iterator();
    }


    public List<Integer> createMultipleVariants(int n, Integer vgID) throws  IOException {
        //int n = addonGroupIDs.length;
        List<Integer> variant = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            variant.add(Integer.parseInt(createSingleVariant(vgID)));
        }
        return variant;
    }

    public List<Variant> createMultipleBulkVariantObjectInList(int n, Integer vgID) {
        Variant v;
        List<Variant> list_v = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            v = new Variant();
            v.build();
            v.setVariant_group_id(vgID);
            list_v.add(v);
        }
        return list_v;
    }

    @DataProvider(name = "bulkVariantsDP")
    public Iterator<Object[]> bulkVariantsDP() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        ArrayList<Integer> delete_by_id = new ArrayList<>();
        ArrayList<String> delete_by_third_party = new ArrayList<>();
        ArrayList<String> names = new ArrayList<>();
        ArrayList<String> tpi = new ArrayList<>();
        List<Integer> vObj;
        String[] itemIDs = createMultipleItems(2);
        List<Integer> vg = new ArrayList<>(createMultipleVariantGroup(2,itemIDs[0]));

        //create only
        BulkVariants bulkV = new BulkVariants();
        bulkV.build();
        List<Variant> list_v = createMultipleBulkVariantObjectInList(4,vg.get(0));
        bulkV.setVariants(list_v);
        for (int i = 0; i < list_v.size(); i++) {
            names.add(list_v.get(i).getName());
            tpi.add(list_v.get(i).getThird_party_id());
        }
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkV),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});

        //update only
        bulkV = new BulkVariants();
        bulkV.build();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        vObj = createMultipleVariants(4,vg.get(0));
        for (int i = 0; i < list_v.size() ; i++) {
            list_v.get(i).setId(vObj.get(i));
            list_v.get(i).setName("new_Automation_BaseService"+timestampRandomiser());
            list_v.get(i).setThird_party_id("new_Automation_"+timestampRandomiser());
            names.add(list_v.get(i).getName());
            tpi.add(list_v.get(i).getThird_party_id());
        }
        bulkV.setVariants(list_v);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkV),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});

        //delete by tpi only
        //TODO: FIX failed to update by tpi bug not working
        bulkV = new BulkVariants();
        bulkV.build();
        bulkV.setDelete_by_third_party_ids(tpi);
        delete_by_third_party = new ArrayList<>(tpi);
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkV),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});
        //delete by id only
        bulkV = new BulkVariants();
        bulkV.build();
        delete_by_third_party = new ArrayList<>();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        vObj = new ArrayList<>(createMultipleVariants(4,vg.get(0)));
        bulkV.setDelete_by_ids(vObj);
        delete_by_id = new ArrayList<>(vObj);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkV),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});
        //create x, update y , delete by id z
        bulkV = new BulkVariants();
        bulkV.build();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        list_v = new ArrayList<>(createMultipleBulkVariantObjectInList(4,vg.get(0)));
        vObj = new ArrayList<>(createMultipleVariants(4,vg.get(0)));
        //update index 0
        list_v.get(0).setId(vObj.get(0));
        for (int i = 0; i < list_v.size(); i++) {
            names.add(list_v.get(i).getName());
            tpi.add(list_v.get(i).getThird_party_id());
        }
        bulkV.setVariants(list_v);
        bulkV.setDelete_by_ids(Arrays.asList(vObj.get(1),vObj.get(2)));
        delete_by_id = new ArrayList<>(Arrays.asList(vObj.get(1),vObj.get(2)));
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkV),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});

        //create x, update y , delete by third party id z
        bulkV = new BulkVariants();
        bulkV.build();
        names = new ArrayList<>();
        delete_by_id = new ArrayList<>();
        list_v = new ArrayList<>(createMultipleBulkVariantObjectInList(4,vg.get(0)));
        vObj = new ArrayList<>(createMultipleVariants(4,vg.get(0)));
        //Delete by tpi 3rd from above
        bulkV.setDelete_by_third_party_ids(Arrays.asList(tpi.get(2)));
        //update index 0
        tpi = new ArrayList<>();
        list_v.get(0).setId(vObj.get(0));
        for (int i = 0; i < list_v.size(); i++) {
            names.add(list_v.get(i).getName());
            tpi.add(list_v.get(i).getThird_party_id());
        }
        bulkV.setVariants(list_v);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkV),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});

        //create x, delete x, update x
        //create x
        list_v = new ArrayList<>(createMultipleBulkVariantObjectInList(4,vg.get(0)));
        vObj = new ArrayList<>(createMultipleVariants(4,vg.get(0)));
        //delete x
        bulkV = new BulkVariants();
        bulkV.build();
        delete_by_third_party =  new ArrayList<>();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        bulkV.setDelete_by_ids(Arrays.asList(vObj.get(0)));
        delete_by_id = new ArrayList<>(Arrays.asList(vObj.get(0)));
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkV),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});
        //update x
        bulkV = new BulkVariants();
        bulkV.build();
        delete_by_id = new ArrayList<>();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        ArrayList<Variant> list = new ArrayList<>(list_v);
        list.get(0).setId(vObj.get(0));
        list.get(0).setName("new_Automation_BaseService"+timestampRandomiser());
        list.get(0).setThird_party_id("new_Automation_"+timestampRandomiser());
        names.add(list.get(0).getName());
        tpi.add(list.get(0).getThird_party_id());
        bulkV.setVariants(list);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkV),delete_by_id,delete_by_third_party,names,tpi,"null","false","true","false"});
        //duplicate create
        bulkV = new BulkVariants();
        bulkV.build();
        Variant variant = new Variant();
        delete_by_id = new ArrayList<>();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        list_v = new ArrayList<>();
        variant.build();
        variant.setVariant_group_id(vg.get(0));
        list_v.add(variant);
        list_v.add(variant);
        names.add(list_v.get(0).getName());
        tpi.add(list_v.get(0).getThird_party_id());
        bulkV.setVariants(list_v);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkV),delete_by_id,delete_by_third_party,names,tpi,"null","true","false","false"});
        //twice update
        //TODO: UPDATE ISSUE Bug
        bulkV = new BulkVariants();
        bulkV.build();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        list_v = new ArrayList<>(createMultipleBulkVariantObjectInList(1,vg.get(0)));
        list_v.get(0).setId(vObj.get(0));
        list_v.get(0).setName("new_Automation_BaseService"+timestampRandomiser());
        list_v.get(0).setThird_party_id("new_Automation_"+timestampRandomiser());
        names.add(list_v.get(0).getName());
        tpi.add(list_v.get(0).getThird_party_id());
        bulkV.setVariants(list_v);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkV),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});
        bulkV = new BulkVariants();
        bulkV.build();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        list_v.get(0).setName("new1_Automation_BaseService"+timestampRandomiser());
        list_v.get(0).setThird_party_id("new1_Automation_"+timestampRandomiser());
        names.add(list_v.get(0).getName());
        tpi.add(list_v.get(0).getThird_party_id());
        bulkV.setVariants(list_v);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkV),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});

        //valid create invalid update - order value negative
        bulkV = new BulkVariants();
        bulkV.build();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        delete_by_id = new ArrayList<>();
        delete_by_third_party = new ArrayList<>();
        list_v = new ArrayList<>(createMultipleBulkVariantObjectInList(2,vg.get(0)));
        list_v.get(0).setId(vObj.get(1));
        list_v.get(0).setName("Automation_"+timestampRandomiser());
        list_v.get(0).setOrder(-1);
        names.add(list_v.get(0).getName());
        tpi.add(list_v.get(0).getThird_party_id());
        bulkV.setVariants(list_v);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkV),delete_by_id,delete_by_third_party,names,tpi,"null","false","true","false"});

        //invalid delete by id
        bulkV = new BulkVariants();
        bulkV.build();
        delete_by_third_party =  new ArrayList<>();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        bulkV.setDelete_by_ids(Arrays.asList(1235654328));
        delete_by_id = new ArrayList<>(Arrays.asList(1235654328));
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkV),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","true"});
        //invalid delete by tpi
        bulkV = new BulkVariants();
        bulkV.build();
        delete_by_id =  new ArrayList<>();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        bulkV.setDelete_by_third_party_ids(Arrays.asList("YO_YO_1235664328"));
        delete_by_third_party = new ArrayList<>(Arrays.asList("YO_YO_1235664328"));
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkV),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","true"});

        return obj.iterator();
    }


    public List<Integer> createMultipleCategories(int n, Integer parentCatID, Integer restID, String menuType, String type) throws  IOException {

        List<Integer> cat = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            cat.add(Integer.parseInt(createSingleCategoryWithReturnData(parentCatID,restID, menuType,type).get(0)));
        }
        return cat;
    }

    public List<com.swiggy.api.erp.cms.pojo.BaseServiceBulkCategory.Category> createMultipleBulkCategoryObjectInList(int n, Integer parentCatID, String type) {
        com.swiggy.api.erp.cms.pojo.BaseServiceBulkCategory.Category cat;
        List<com.swiggy.api.erp.cms.pojo.BaseServiceBulkCategory.Category> list_cat = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            cat = new com.swiggy.api.erp.cms.pojo.BaseServiceBulkCategory.Category();
            cat.build(restaurantId,menuId);
            cat.setParent_category_id(parentCatID);
            cat.setType(type);
            list_cat.add(cat);
        }
        return list_cat;
    }

    @DataProvider(name = "bulkCategoryDP")
    public Iterator<Object[]> bulkCategoryDP() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        ArrayList<Integer> delete_by_id = new ArrayList<>();
        ArrayList<String> delete_by_third_party = new ArrayList<>();
        ArrayList<String> names = new ArrayList<>();
        ArrayList<String> tpi = new ArrayList<>();
        //default - menu id
        Integer defaultParentCatId = menuId;
        String defaultMenuType = "REGULAR_MENU";

        List<Integer> catIds;

        //create only
        BulkCategory bulkCat = new BulkCategory();
        bulkCat.build();
        List<com.swiggy.api.erp.cms.pojo.BaseServiceBulkCategory.Category> list_cat = createMultipleBulkCategoryObjectInList(4, defaultParentCatId,"Cat");
        bulkCat.setCategories(list_cat);
        for (int i = 0; i < list_cat.size(); i++) {
            names.add(list_cat.get(i).getName());
            tpi.add(list_cat.get(i).getThird_party_id());
        }
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkCat),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});

        //update only
        bulkCat = new BulkCategory();
        bulkCat.build();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        catIds = createMultipleCategories(4,defaultParentCatId,restaurantId,defaultMenuType,"Cat");
        for (int i = 0; i < list_cat.size() ; i++) {
            list_cat.get(i).setId(catIds.get(i));
            list_cat.get(i).setName("new_Automation_BaseService"+timestampRandomiser());
            list_cat.get(i).setThird_party_id("new_Automation_"+timestampRandomiser());
            names.add(list_cat.get(i).getName());
            tpi.add(list_cat.get(i).getThird_party_id());
        }
        bulkCat.setCategories(list_cat);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkCat),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});

        //delete by tpi only
        //TODO: FIX failed to update by tpi bug not working
        bulkCat = new BulkCategory();
        bulkCat.build();
        bulkCat.setDelete_by_third_party_ids(tpi);
        delete_by_third_party = new ArrayList<>(tpi);
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkCat),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});
        //delete by id only
        bulkCat = new BulkCategory();
        bulkCat.build();
        delete_by_third_party = new ArrayList<>();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        catIds = new ArrayList<>(createMultipleCategories(4,defaultParentCatId,restaurantId,defaultMenuType,"Cat"));
        bulkCat.setDelete_by_ids(catIds);
        delete_by_id = new ArrayList<>(catIds);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkCat),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});
        //create x, update y , delete by id z
        bulkCat = new BulkCategory();
        bulkCat.build();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        list_cat = new ArrayList<>(createMultipleBulkCategoryObjectInList(4,defaultParentCatId,"Cat"));
        catIds = new ArrayList<>(createMultipleCategories(4,defaultParentCatId,restaurantId,defaultMenuType,"Cat"));
        //update index 0
        list_cat.get(0).setId(catIds.get(0));
        for (int i = 0; i < list_cat.size(); i++) {
            names.add(list_cat.get(i).getName());
            tpi.add(list_cat.get(i).getThird_party_id());
        }
        bulkCat.setCategories(list_cat);
        bulkCat.setDelete_by_ids(Arrays.asList(catIds.get(1),catIds.get(2)));
        delete_by_id = new ArrayList<>(Arrays.asList(catIds.get(1),catIds.get(2)));
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkCat),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});

        //create x, update y , delete by third party id z
        bulkCat = new BulkCategory();
        bulkCat.build();
        delete_by_id = new ArrayList<>();
        names = new ArrayList<>();
        list_cat = new ArrayList<>(createMultipleBulkCategoryObjectInList(4,defaultParentCatId,"Cat"));
        catIds = new ArrayList<>(createMultipleCategories(4,defaultParentCatId,restaurantId,defaultMenuType,"Cat"));
        //Delete by tpi 3rd from above
        bulkCat.setDelete_by_third_party_ids(Arrays.asList(tpi.get(2)));
        //update index 0
        tpi = new ArrayList<>();
        list_cat.get(0).setId(catIds.get(0));
        for (int i = 0; i < list_cat.size(); i++) {
            names.add(list_cat.get(i).getName());
            tpi.add(list_cat.get(i).getThird_party_id());
        }
        bulkCat.setCategories(list_cat);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkCat),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});

        //create x, delete x, update x
        //create x
        list_cat = new ArrayList<>(createMultipleBulkCategoryObjectInList(4,defaultParentCatId,"Cat"));
        catIds = new ArrayList<>(createMultipleCategories(4,defaultParentCatId,restaurantId,defaultMenuType,"Cat"));
        //delete x
        bulkCat = new BulkCategory();
        bulkCat.build();
        delete_by_third_party =  new ArrayList<>();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        bulkCat.setDelete_by_ids(Arrays.asList(catIds.get(0)));
        delete_by_id = new ArrayList<>(Arrays.asList(catIds.get(0)));
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkCat),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});
        //update x
        //TODO: Fix once error message issue is fixed
        bulkCat = new BulkCategory();
        bulkCat.build();
        delete_by_id = new ArrayList<>();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        ArrayList<com.swiggy.api.erp.cms.pojo.BaseServiceBulkCategory.Category> list = new ArrayList<>(list_cat);
        list.get(0).setId(catIds.get(0));
        list.get(0).setName("new_Automation_BaseService"+timestampRandomiser());
        list.get(0).setThird_party_id("new_Automation_"+timestampRandomiser());
        names.add(list.get(0).getName());
        tpi.add(list.get(0).getThird_party_id());
        bulkCat.setCategories(list);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkCat),delete_by_id,delete_by_third_party,names,tpi,"null","false","true","false"});
        //duplicate create
        bulkCat = new BulkCategory();
        bulkCat.build();
        com.swiggy.api.erp.cms.pojo.BaseServiceBulkCategory.Category category = new com.swiggy.api.erp.cms.pojo.BaseServiceBulkCategory.Category();
        delete_by_id = new ArrayList<>();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        list_cat = new ArrayList<>();
        category.build(restaurantId,menuId);
        category.setParent_category_id(defaultParentCatId);
        category.setRestaurant_id(restaurantId);
        list_cat.add(category);
        list_cat.add(category);
        names.add(list_cat.get(0).getName());
        tpi.add(list_cat.get(0).getThird_party_id());
        bulkCat.setCategories(list_cat);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkCat),delete_by_id,delete_by_third_party,names,tpi,"null","true","false","false"});
        //twice update
        //TODO: UPDATE ISSUE Bug
        bulkCat = new BulkCategory();
        bulkCat.build();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        list_cat = new ArrayList<>(createMultipleBulkCategoryObjectInList(1,defaultParentCatId,"Cat"));
        list_cat.get(0).setId(catIds.get(2));
        list_cat.get(0).setName("new_Automation_BaseService"+timestampRandomiser());
        list_cat.get(0).setThird_party_id("new_Automation_"+timestampRandomiser());
        names.add(list_cat.get(0).getName());
        tpi.add(list_cat.get(0).getThird_party_id());
        bulkCat.setCategories(list_cat);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkCat),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});
        bulkCat = new BulkCategory();
        bulkCat.build();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        list_cat.get(0).setName("new1_Automation_BaseService"+timestampRandomiser());
        list_cat.get(0).setThird_party_id("new1_Automation_"+timestampRandomiser());
        names.add(list_cat.get(0).getName());
        tpi.add(list_cat.get(0).getThird_party_id());
        bulkCat.setCategories(list_cat);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkCat),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});

        //TODO: FIx once bug is fixed
        //valid create invalid update - order value negative
        bulkCat = new BulkCategory();
        bulkCat.build();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        delete_by_id = new ArrayList<>();
        delete_by_third_party = new ArrayList<>();
        list_cat = new ArrayList<>(createMultipleBulkCategoryObjectInList(2,defaultParentCatId,"Cat"));
        list_cat.get(0).setId(catIds.get(1));
        list_cat.get(0).setName("Automation_"+timestampRandomiser());
        list_cat.get(0).setOrder(-1);
        names.add(list_cat.get(0).getName());
        tpi.add(list_cat.get(0).getThird_party_id());
        bulkCat.setCategories(list_cat);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkCat),delete_by_id,delete_by_third_party,names,tpi,"null","false","true","false"});

        //TODO: Obviously fix once bug is fixed
        //invalid delete by id
        bulkCat = new BulkCategory();
        bulkCat.build();
        delete_by_third_party =  new ArrayList<>();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        bulkCat.setDelete_by_ids(Arrays.asList(1235654328));
        delete_by_id = new ArrayList<>(Arrays.asList(1235654328));
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkCat),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","true"});
        //invalid delete by tpi
        bulkCat = new BulkCategory();
        bulkCat.build();
        delete_by_id =  new ArrayList<>();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        bulkCat.setDelete_by_third_party_ids(Arrays.asList("YO_YO_1235664328"));
        delete_by_third_party = new ArrayList<>(Arrays.asList("YO_YO_1235664328"));
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkCat),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","true"});

        return obj.iterator();
    }


    public List<Item> createMultipleBulkItemsObjectInList(int n) {
        Item item;
        List<Item> list_item = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            item = new Item();
            item.build(restaurantId);
            list_item.add(item);
        }
        return list_item;
    }

    public List<Integer> getItemListInInteger (List<String> items) {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < items.size() ; i++) {
            if(!items.get(i).equals("") && !items.get(i).equals("null"))
                list.add(Integer.parseInt(items.get(i)));
        }
        return list;
    }


    @DataProvider(name = "bulkItemsDP")
    public Iterator<Object[]> bulkItemsDP() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        ArrayList<Integer> delete_by_id = new ArrayList<>();
        ArrayList<String> delete_by_third_party = new ArrayList<>();
        ArrayList<String> names = new ArrayList<>();
        ArrayList<String> tpi = new ArrayList<>();
        List<String> itemIDs;
        //create only
        BulkItems bulkItem = new BulkItems();
        bulkItem.build();
        List<Item> list_items = createMultipleBulkItemsObjectInList(4);
        bulkItem.setItems(list_items);
        for (int i = 0; i < list_items.size(); i++) {
            names.add(list_items.get(i).getName());
            tpi.add(list_items.get(i).getThird_party_id());
        }
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkItem),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});

        //update only
        bulkItem = new BulkItems();
        bulkItem.build();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        itemIDs = new ArrayList<>(Arrays.asList(createMultipleItems(4)));
        for (int i = 0; i < list_items.size() ; i++) {
            list_items.get(i).setId(Integer.parseInt(itemIDs.get(i)));
            list_items.get(i).setName("new_Automation_BaseService"+timestampRandomiser());
            list_items.get(i).setThird_party_id("new_Automation_"+timestampRandomiser());
            names.add(list_items.get(i).getName());
            tpi.add(list_items.get(i).getThird_party_id());
        }
        bulkItem.setItems(list_items);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkItem),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});

        //delete by tpi only
        //TODO: FIX failed to update by tpi bug not working
        bulkItem = new BulkItems();
        bulkItem.build();
        bulkItem.setDelete_by_third_party_ids(tpi);
        delete_by_third_party = new ArrayList<>(tpi);
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkItem),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});
        //delete by id only
        bulkItem = new BulkItems();
        bulkItem.build();
        delete_by_third_party = new ArrayList<>();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        itemIDs = new ArrayList<>(Arrays.asList(createMultipleItems(4)));
        bulkItem.setDelete_by_ids(getItemListInInteger(itemIDs));
        delete_by_id = new ArrayList<>(getItemListInInteger(itemIDs));
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkItem),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});
        //create x, update y , delete by id z
        bulkItem = new BulkItems();
        bulkItem.build();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        list_items = new ArrayList<>(createMultipleBulkItemsObjectInList(4));
        itemIDs = new ArrayList<>(Arrays.asList(createMultipleItems(4)));
        //update index 0
        list_items.get(0).setId(Integer.parseInt(itemIDs.get(0)));
        for (int i = 0; i < list_items.size(); i++) {
            names.add(list_items.get(i).getName());
            tpi.add(list_items.get(i).getThird_party_id());
        }
        bulkItem.setItems(list_items);
        bulkItem.setDelete_by_ids(Arrays.asList(Integer.parseInt(itemIDs.get(1)),Integer.parseInt(itemIDs.get(2))));
        delete_by_id = new ArrayList<>(Arrays.asList(Integer.parseInt(itemIDs.get(1)),Integer.parseInt(itemIDs.get(2))));
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkItem),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});

        //create x, update y , delete by third party id z
        bulkItem = new BulkItems();
        bulkItem.build();
        names = new ArrayList<>();
        delete_by_id = new ArrayList<>();
        list_items = new ArrayList<>(createMultipleBulkItemsObjectInList(4));
        itemIDs = new ArrayList<>(Arrays.asList(createMultipleItems(4)));
        //Delete by tpi 3rd from above
        bulkItem.setDelete_by_third_party_ids(Arrays.asList(tpi.get(2)));
        //update index 0
        tpi = new ArrayList<>();
        list_items.get(0).setId(Integer.parseInt(itemIDs.get(0)));
        for (int i = 0; i < list_items.size(); i++) {
            names.add(list_items.get(i).getName());
            tpi.add(list_items.get(i).getThird_party_id());
        }
        bulkItem.setItems(list_items);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkItem),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});

        //create x, delete x, update x
        //create x
        list_items = new ArrayList<>(createMultipleBulkItemsObjectInList(4));
        itemIDs = new ArrayList<>(Arrays.asList(createMultipleItems(4)));
        //delete x
        bulkItem = new BulkItems();
        bulkItem.build();
        delete_by_third_party =  new ArrayList<>();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        bulkItem.setDelete_by_ids(getItemListInInteger(Arrays.asList(itemIDs.get(0))));
        delete_by_id = new ArrayList<>(getItemListInInteger(Arrays.asList(itemIDs.get(0))));
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkItem),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});
        //update x
        bulkItem = new BulkItems();
        bulkItem.build();
        delete_by_id = new ArrayList<>();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        ArrayList<Item> list = new ArrayList<>(list_items);
        list.get(0).setId(Integer.parseInt(itemIDs.get(0)));
        list.get(0).setName("new_Automation_BaseService"+timestampRandomiser());
        list.get(0).setThird_party_id("new_Automation_"+timestampRandomiser());
        names.add(list.get(0).getName());
        tpi.add(list.get(0).getThird_party_id());
        bulkItem.setItems(list);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkItem),delete_by_id,delete_by_third_party,names,tpi,"null","false","true","false"});
        //duplicate create
        bulkItem = new BulkItems();
        bulkItem.build();
        Item item = new Item();
        delete_by_id = new ArrayList<>();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        list_items = new ArrayList<>();
        item.build(restaurantId);
        list_items.add(item);
        list_items.add(item);
        names.add(list_items.get(0).getName());
        tpi.add(list_items.get(0).getThird_party_id());
        bulkItem.setItems(list_items);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkItem),delete_by_id,delete_by_third_party,names,tpi,"null","true","false","false"});
        //twice update
        bulkItem = new BulkItems();
        bulkItem.build();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        list_items = new ArrayList<>(createMultipleBulkItemsObjectInList(1));
        list_items.get(0).setId(Integer.parseInt(itemIDs.get(0)));
        list_items.get(0).setName("new_Automation_BaseService"+timestampRandomiser());
        list_items.get(0).setThird_party_id("new_Automation_"+timestampRandomiser());
        names.add(list_items.get(0).getName());
        tpi.add(list_items.get(0).getThird_party_id());
        bulkItem.setItems(list_items);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkItem),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});
        bulkItem = new BulkItems();
        bulkItem.build();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        list_items.get(0).setName("new1_Automation_BaseService"+timestampRandomiser());
        list_items.get(0).setThird_party_id("new1_Automation_"+timestampRandomiser());
        names.add(list_items.get(0).getName());
        tpi.add(list_items.get(0).getThird_party_id());
        bulkItem.setItems(list_items);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkItem),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","false"});

        //TODO: fix once bug is resolved for invalid values update
        //valid create invalid update - order value negative
        bulkItem = new BulkItems();
        bulkItem.build();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        delete_by_id = new ArrayList<>();
        delete_by_third_party = new ArrayList<>();
        list_items = new ArrayList<>(createMultipleBulkItemsObjectInList(2));
        list_items.get(0).setId(Integer.parseInt(itemIDs.get(1)));
        list_items.get(0).setName("Automation_"+timestampRandomiser());
        list_items.get(0).setOrder(-1);
        names.add(list_items.get(0).getName());
        tpi.add(list_items.get(0).getThird_party_id());
        bulkItem.setItems(list_items);
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkItem),delete_by_id,delete_by_third_party,names,tpi,"null","false","true","false"});

        //TODO: Obviously bug for invalid so fix it
        //invalid delete by id
        bulkItem = new BulkItems();
        bulkItem.build();
        delete_by_third_party =  new ArrayList<>();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        bulkItem.setDelete_by_ids(Arrays.asList(1235654328));
        delete_by_id = new ArrayList<>(Arrays.asList(1235654328));
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkItem),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","true"});
        //invalid delete by tpi
        bulkItem = new BulkItems();
        bulkItem.build();
        delete_by_id =  new ArrayList<>();
        names = new ArrayList<>();
        tpi = new ArrayList<>();
        bulkItem.setDelete_by_third_party_ids(Arrays.asList("YO_YO_1235664328"));
        delete_by_third_party = new ArrayList<>(Arrays.asList("YO_YO_1235664328"));
        obj.add(new Object[] {jsonHelper.getObjectToJSON(bulkItem),delete_by_id,delete_by_third_party,names,tpi,"null","false","false","true"});

        return obj.iterator();

    }


    //returns: restID, catID, subCatID,ItemID, VGID, variantID, itemName, VGName, VariantName, item instock, variant instock
    public ArrayList<String> returnValuesAsList(int restID, int catID, int subCatID, String itemID, String VGID, String variantID, String CategoryName, String SubCategoryName, String itemName, String VGName, String VariantName, String itemInStock, String variantInStock, String menuType) {
        ArrayList<String> values = new ArrayList<>();
        values.add(String.valueOf(restID));
        values.add(String.valueOf(catID));
        values.add(String.valueOf(subCatID));
        values.add(itemID);
        values.add(VGID);
        values.add(variantID);
        values.add(CategoryName);
        values.add(SubCategoryName);
        values.add(itemName);
        values.add(VGName);
        values.add(VariantName);
        values.add(itemInStock);
        values.add(variantInStock);
        values.add(menuType);
        return values;
    }

    @DataProvider(name = "getMenuDP")
    public Iterator<Object[]> getMenuDP() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        ArrayList<String> values;
        int restID = restaurantId;
        //without variants - in stock
        List<String> catID = new ArrayList<>(createSingleCategoryWithReturnData(menuId,restID,"REGULAR_MENU","Cat"));
        List<String> subCatID = new ArrayList<>(createSingleCategoryWithReturnData(Integer.parseInt(catID.get(0)),restID,"REGULAR_MENU","SubCat"));
        List<String> itemID = new ArrayList<>(baseServiceHelper.createItem(restID,Integer.parseInt(catID.get(0)),Integer.parseInt(subCatID.get(0)),1));
        values = new ArrayList<>(returnValuesAsList(restID,Integer.parseInt(catID.get(0)),Integer.parseInt(subCatID.get(0)),itemID.get(0),"null","null",catID.get(1),subCatID.get(1),itemID.get(1),"null","null","1","null","REGULAR_MENU"));
        obj.add(new Object[] {values});

        //with variants - in stock
        String name =  "Automation_BaseService_"+timestampRandomiser();
        String vgID = createSingleVariantGroup(itemID.get(0),name,"");
        ArrayList<String> variantID = new ArrayList<>(createSingleVariant(Integer.parseInt(vgID),1));
        values = new ArrayList<>(returnValuesAsList(restID,Integer.parseInt(catID.get(0)),Integer.parseInt(subCatID.get(0)),itemID.get(0),vgID,variantID.get(0),catID.get(1),subCatID.get(1),itemID.get(1),name,variantID.get(1),"1","1","REGULAR_MENU"));
        obj.add(new Object[] {values});

        //with variant - out of stock
        catID = new ArrayList<>(createSingleCategoryWithReturnData(menuId,restID,"REGULAR_MENU","Cat"));
        subCatID = new ArrayList<>(createSingleCategoryWithReturnData(Integer.parseInt(catID.get(0)),restID,"REGULAR_MENU","SubCat"));
        itemID = new ArrayList<>(baseServiceHelper.createItem(restID,Integer.parseInt(catID.get(0)),Integer.parseInt(subCatID.get(0)),1));
        String vgID1 = createSingleVariantGroup(itemID.get(0),name,"");
        variantID = new ArrayList<>(createSingleVariant(Integer.parseInt(vgID1),0));
        values = new ArrayList<>(returnValuesAsList(restID,Integer.parseInt(catID.get(0)),Integer.parseInt(subCatID.get(0)),itemID.get(0),vgID1,variantID.get(0),catID.get(1),subCatID.get(1),itemID.get(1),name,variantID.get(1),"0","0","REGULAR_MENU"));
        obj.add(new Object[] {values});

        //TODO: Unable to update
        //update item to instock
        itemID = new ArrayList<>(baseServiceHelper.createItem(restID,Integer.parseInt(catID.get(0)),Integer.parseInt(subCatID.get(0)),0));
        Item item = new Item();
        item.build(restaurantId);
        //item.setId(Integer.parseInt(itemID.get(0)));
        item.setCategory_id(Integer.parseInt(catID.get(0)));
        item.setSub_category_id(Integer.parseInt(subCatID.get(0)));
        item.setRestaurant_id(restID);
        item.setIn_stock(1);
        String body = baseServiceHelper.updateItemsHelper(jsonHelper.getObjectToJSON(item),itemID.get(0)).ResponseValidator.GetBodyAsText();
        values = new ArrayList<>(returnValuesAsList(restID,Integer.parseInt(catID.get(0)),Integer.parseInt(subCatID.get(0)),itemID.get(0),"null","null",catID.get(1),subCatID.get(1),item.getName(),"null","null","1","null","REGULAR_MENU"));
        obj.add(new Object[] {values});

        //2 variant groups with all in stock
        catID = new ArrayList<>(createSingleCategoryWithReturnData(menuId,restID,"REGULAR_MENU","Cat"));
        subCatID = new ArrayList<>(createSingleCategoryWithReturnData(Integer.parseInt(catID.get(0)),restID,"REGULAR_MENU","SubCat"));
        itemID = new ArrayList<>(baseServiceHelper.createItem(restID,Integer.parseInt(catID.get(0)),Integer.parseInt(subCatID.get(0)),1));
        String vg1 = createSingleVariantGroup(itemID.get(0),name,"");
        variantID = new ArrayList<>(createSingleVariant(Integer.parseInt(vg1),1));
        values = new ArrayList<>(returnValuesAsList(restID,Integer.parseInt(catID.get(0)),Integer.parseInt(subCatID.get(0)),itemID.get(0),vg1,variantID.get(0),catID.get(1),subCatID.get(1),itemID.get(1),name,variantID.get(1),"1","1","REGULAR_MENU"));
        obj.add(new Object[] {values});
        String vg2 = createSingleVariantGroup(itemID.get(0),"new_"+name,"");
        variantID = new ArrayList<>(createSingleVariant(Integer.parseInt(vg2),1));
        values = new ArrayList<>(returnValuesAsList(restID,Integer.parseInt(catID.get(0)),Integer.parseInt(subCatID.get(0)),itemID.get(0),vg2,variantID.get(0),catID.get(1),subCatID.get(1),itemID.get(1),"new_"+name,variantID.get(1),"1","1","REGULAR_MENU"));
        obj.add(new Object[] {values});

        //2 variant groups with 1VG - all instock & 1VG - all out of stock
        catID = new ArrayList<>(createSingleCategoryWithReturnData(menuId,restID,"REGULAR_MENU","Cat"));
        subCatID = new ArrayList<>(createSingleCategoryWithReturnData(Integer.parseInt(catID.get(0)),restID,"REGULAR_MENU","SubCat"));
        itemID = new ArrayList<>(baseServiceHelper.createItem(restID,Integer.parseInt(catID.get(0)),Integer.parseInt(subCatID.get(0)),1));
        String VG1 = createSingleVariantGroup(itemID.get(0),name,"");
        variantID = new ArrayList<>(createSingleVariant(Integer.parseInt(VG1),1));
        values = new ArrayList<>(returnValuesAsList(restID,Integer.parseInt(catID.get(0)),Integer.parseInt(subCatID.get(0)),itemID.get(0),VG1,variantID.get(0),catID.get(1),subCatID.get(1),itemID.get(1),name,variantID.get(1),"1","1","REGULAR_MENU"));
        obj.add(new Object[] {values});
        String VG2 = createSingleVariantGroup(itemID.get(0),"new_"+name,"");
        variantID = new ArrayList<>(createSingleVariant(Integer.parseInt(VG2),0));
        values = new ArrayList<>(returnValuesAsList(restID,Integer.parseInt(catID.get(0)),Integer.parseInt(subCatID.get(0)),itemID.get(0),VG2,variantID.get(0),catID.get(1),subCatID.get(1),itemID.get(1),"new_"+name,variantID.get(1),"0","0","REGULAR_MENU"));
        obj.add(new Object[] {values});

        return obj.iterator();
    }

    public String[] createAddonGroupAndAddItemIdHelper(String[] payload, String addonFreelimit, String addonLimit) {
        HashMap<String, String> hm = new HashMap<>();
        hm.put("addon_free_limit", addonFreelimit);
        hm.put("addon_limit", addonLimit);
        String[] tmp_payload = createSingleItemDP(hm);
        String body = baseServiceHelper.createItemHelper(tmp_payload).ResponseValidator.GetBodyAsText();
        String get_itemId = JsonPath.read(body, "$..data.id").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        payload[4] = get_itemId;
        return payload;
    }
    @DataProvider(name = "itemoutofstock")
	public static Object[][] partnermappingupdatecity() throws IOException {

    	CommonAPIHelper apihelper=new CommonAPIHelper();
    	int random_number=apihelper.getRandomNo(100000000, 288339393);
    	CMSHelper cmsHelper=new CMSHelper();
    	
    	return new Object[][] {
    		
//    		//Test By passing invalid item_id in the param.
    		{ String.valueOf(random_number), cmsHelper.getcurrentdaytime(),cmsHelper.nextDayDateTime(),0,"null"},
//			//Test By passing valid item_id in the param with in-stock as 1
			{ cmsHelper.getEnableAndActiveItemID(String.valueOf(restaurantId),1), cmsHelper.getcurrentdaytime(),cmsHelper.nextDayDateTime(),1,"success"},
//			//Test By passing valid item_id in the param with in-stock as 0
			{ cmsHelper.getEnableAndActiveItemID(String.valueOf(restaurantId),0), cmsHelper.getcurrentdaytime(), cmsHelper.nextDayDateTime(),1,"success"},
//            //Test By passing valid item_id in the param with in-stock as 1 and current time minus 30 mins to set the out of stock time.
     		{ cmsHelper.getEnableAndActiveItemID(String.valueOf(restaurantId),1), cmsHelper.getcurrentDayTimeMinusMins(-25),cmsHelper.nextDayDateTime(),1,"success"},
//			//Test By passing valid item_id in the param with in-stock as 0 and current time minus 30 mins to set the out of stock time.
			{ cmsHelper.getEnableAndActiveItemID(String.valueOf(restaurantId),0), cmsHelper.getcurrentDayTimeMinusMins(-25),cmsHelper.nextDayDateTime(),1,"success"},
//            //Test By passing valid item_id in the param with in-stock as 1 and current time minus 40 mins to set the out of stock time.			
     		{ cmsHelper.getEnableAndActiveItemID(String.valueOf(restaurantId),1), cmsHelper.getcurrentDayTimeMinusMins(-40),cmsHelper.nextDayDateTime(),0,"Invalid item holiday slot request!"},
//            //Test By passing valid item_id in the param with in-stock as 0 and current time minus 40 mins to set the out of stock time.		
			{ cmsHelper.getEnableAndActiveItemID(String.valueOf(restaurantId),0), cmsHelper.getcurrentDayTimeMinusMins(-40),cmsHelper.nextDayDateTime(),0,"Invalid item holiday slot request!"},
            };
	}
}
