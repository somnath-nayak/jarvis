package com.swiggy.api.erp.ff.dp.FraudServiceDP;

import org.testng.annotations.DataProvider;

public class CODStatusData {

    CODStatusDP dataService=new CODStatusDP();

        @DataProvider(name = "PendingCancellationFee")
        public Object[][] getPendingFeeDataSet() {


            return dataService.getPendingCancellationFeeData();
        }

        @DataProvider(name = "CityConfigurations")
        public Object[][] getCityConfigurationDataSet() {



            return dataService.getCityConfiguration();
        }

        @DataProvider(name = "PendingFeeCityConfigurations")
        public Object[][] PendingFeeCityConfigurations() {



            return dataService.getPendingFeeCityConfiguration();
        }


        @DataProvider(name = "exclustionList")
        public Object[][] getexclusionlistDataSet() {


            return dataService.getExclustionListData();
        }

        @DataProvider(name = "DSCheck")
        public Object[][] getDSCheckDataSet() {


            return dataService.getDSCheckData();
        }


    @DataProvider(name = "fieldLevelValidation")
    public Object[][] getFieldLevelValidationDataSet() {


        return dataService.getFieldLevelData();
    }

}
