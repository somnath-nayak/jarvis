package com.swiggy.api.erp.cms.dp;

import com.swiggy.api.erp.cms.constants.MenuConstants;
import com.swiggy.api.erp.cms.pojo.BulkItem.BulkItem;
import com.swiggy.api.erp.cms.pojo.BulkItem.Entities;
import com.swiggy.api.erp.cms.pojo.Items.Entity;
import com.swiggy.api.erp.cms.pojo.Items.Gst_details;
import com.swiggy.api.erp.cms.pojo.Items.Item;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by kiran.j on 2/19/18.
 */
public class CreateItemDp {

    @DataProvider(name = "createItems")
    public Iterator<Object[]> createItems() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        Item item = new Item();
        item.build();
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(item), new String[]{}, MenuConstants.token_id3});

        Entity entity = new Entity();
        entity.build();
        entity.setId("");
        item.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(item), MenuConstants.additem_id, MenuConstants.token_id3});

        entity = new Entity();
        entity.build();
        entity.setSub_category_id("");
        item.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(item), MenuConstants.additem_subcategory, MenuConstants.token_id3});

        entity = new Entity();
        entity.build();
        entity.setName("");
        item.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(item), MenuConstants.additem_name, MenuConstants.token_id3});

        entity = new Entity();
        entity.build();
        entity.setPrice(MenuConstants.negative_cgst);
        item.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(item), MenuConstants.additem_price, MenuConstants.token_id3});

        entity = new Entity();
        entity.build();
        entity.setPacking_charges(MenuConstants.negative_cgst);
        item.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(item), MenuConstants.additem_packaging, MenuConstants.token_id3});

        entity = new Entity();
        entity.build();
        Gst_details gst_details = entity.getGst_details();
        gst_details.setSgst(MenuConstants.negative_sgst);
        entity.setGst_details(gst_details);
        item.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(item), MenuConstants.additem_sgst, MenuConstants.token_id3});

        entity = new Entity();
        entity.build();
        gst_details = entity.getGst_details();
        gst_details.setIgst(MenuConstants.negative_igst);
        entity.setGst_details(gst_details);
        item.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(item), MenuConstants.additem_igst, MenuConstants.token_id3});

        entity = new Entity();
        entity.build();
        gst_details = entity.getGst_details();
        gst_details.setCgst(MenuConstants.negative_cgst);
        entity.setGst_details(gst_details);
        item.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(item), MenuConstants.additem_cgst, MenuConstants.token_id3});

        entity = new Entity();
        entity.build();
        entity.setId(null);
        item.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(item), MenuConstants.additem_id, MenuConstants.token_id3});

        entity = new Entity();
        entity.build();
        entity.setSub_category_id(null);
        item.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(item), new String[]{}, MenuConstants.token_id3});

        entity = new Entity();
        entity.build();
        entity.setName(null);
        item.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(item), MenuConstants.additem_name, MenuConstants.token_id3});


        return obj.iterator();
    }

    @DataProvider(name = "updateItems")
    public Iterator<Object[]> updateItems() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        Item item = new Item();
        item.build();
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(item), new String[]{}, MenuConstants.token_id3, item.getEntity().getId()});

        Entity entity = new Entity();
        entity.build();
        entity.setId("");
        item.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(item), MenuConstants.additem_id, MenuConstants.token_id3, "123"});

        entity = new Entity();
        entity.build();
        entity.setSub_category_id("");
        item.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(item), MenuConstants.additem_subcategory, MenuConstants.token_id3, item.getEntity().getId()});

        entity = new Entity();
        entity.build();
        entity.setName("");
        item.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(item), MenuConstants.updateitem_name, MenuConstants.token_id3, item.getEntity().getId()});

        entity = new Entity();
        entity.build();
        entity.setPrice(MenuConstants.negative_cgst);
        item.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(item), MenuConstants.additem_price, MenuConstants.token_id3, item.getEntity().getId()});

        entity = new Entity();
        entity.build();
        entity.setPacking_charges(MenuConstants.negative_cgst);
        item.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(item), MenuConstants.additem_packaging, MenuConstants.token_id3, item.getEntity().getId()});

        entity = new Entity();
        entity.build();
        Gst_details gst_details = entity.getGst_details();
        gst_details.setSgst(MenuConstants.negative_sgst);
        entity.setGst_details(gst_details);
        item.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(item), MenuConstants.additem_sgst, MenuConstants.token_id3, item.getEntity().getId()});

        entity = new Entity();
        entity.build();
        gst_details = entity.getGst_details();
        gst_details.setIgst(MenuConstants.negative_igst);
        entity.setGst_details(gst_details);
        item.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(item), MenuConstants.additem_igst, MenuConstants.token_id3, item.getEntity().getId()});

        entity = new Entity();
        entity.build();
        gst_details = entity.getGst_details();
        gst_details.setCgst(MenuConstants.negative_cgst);
        entity.setGst_details(gst_details);
        item.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(item), MenuConstants.additem_cgst, MenuConstants.token_id3, item.getEntity().getId()});

        entity = new Entity();
        entity.build();
        entity.setId(null);
        item.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(item), MenuConstants.additem_id, MenuConstants.token_id3, "123"});

        entity = new Entity();
        entity.build();
        entity.setSub_category_id(null);
        item.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(item), new String[]{}, MenuConstants.token_id3, item.getEntity().getId()});

        entity = new Entity();
        entity.build();
        entity.setName(null);
        item.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(item), new String[]{}, MenuConstants.token_id3, item.getEntity().getId()});

        return obj.iterator();
    }

    @DataProvider(name = "createbulkItems")
    public Iterator<Object[]> createbulkItems() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        BulkItem bulkItem = new BulkItem();
        bulkItem.build();
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(bulkItem), new String[]{}, MenuConstants.token_id3});

        Entities entities2 = new Entities();
        entities2.build();
        Entities entities1 = bulkItem.getEntities()[0];
        entities2.setCategory_id("");
        bulkItem.setEntities(new Entities[]{entities1, entities2});
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(bulkItem), MenuConstants.bulkadditem_categoryid, MenuConstants.token_id3});

        entities2 = new Entities();
        entities2.build();
        entities1 = bulkItem.getEntities()[0];
        entities2.setId("");
        bulkItem.setEntities(new Entities[]{entities1, entities2});
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(bulkItem),  MenuConstants.bulkadditem_id, MenuConstants.token_id3});

        entities2 = new Entities();
        entities2.build();
        entities1 = bulkItem.getEntities()[0];
        entities2.setName("");
        bulkItem.setEntities(new Entities[]{entities1, entities2});
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(bulkItem),  MenuConstants.bulkadditem_name, MenuConstants.token_id3});

        entities2 = new Entities();
        entities2.build();
        entities1 = bulkItem.getEntities()[0];
        entities2.setPrice(MenuConstants.negative_cgst);
        bulkItem.setEntities(new Entities[]{entities1, entities2});
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(bulkItem),  MenuConstants.bulkadditem_price, MenuConstants.token_id3});

        entities2 = new Entities();
        entities2.build();
        entities1 = bulkItem.getEntities()[0];
        entities2.setPacking_charges(MenuConstants.negative_cgst);
        bulkItem.setEntities(new Entities[]{entities1, entities2});
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(bulkItem),  MenuConstants.bulkadditem_packagingprice, MenuConstants.token_id3});

        entities2 = new Entities();
        entities2.build();
        entities1 = bulkItem.getEntities()[0];
        Gst_details gst_details = new Gst_details();
        gst_details.setCgst(MenuConstants.negative_cgst);
        entities2.setGst_details(gst_details);
        bulkItem.setEntities(new Entities[]{entities1, entities2});
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(bulkItem),  MenuConstants.bulkadditem_cgst, MenuConstants.token_id3});

        entities2 = new Entities();
        entities2.build();
        entities1 = bulkItem.getEntities()[0];
        gst_details = new Gst_details();
        gst_details.setIgst(MenuConstants.negative_igst);
        entities2.setGst_details(gst_details);
        bulkItem.setEntities(new Entities[]{entities1, entities2});
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(bulkItem),  MenuConstants.bulkadditem_igst, MenuConstants.token_id3});

        entities2 = new Entities();
        entities2.build();
        entities1 = bulkItem.getEntities()[0];
        gst_details = new Gst_details();
        gst_details.setSgst(MenuConstants.negative_sgst);
        entities2.setGst_details(gst_details);
        bulkItem.setEntities(new Entities[]{entities1, entities2});
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(bulkItem),  MenuConstants.bulkadditem_sgst, MenuConstants.token_id3});

        return obj.iterator();
    }

    @DataProvider(name = "updatebulkItems")
    public Iterator<Object[]> updatebulkItems() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        BulkItem bulkItem = new BulkItem();
        bulkItem.build();
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(bulkItem), new String[]{}, MenuConstants.token_id3});

        Entities entities2 = new Entities();
        entities2.build();
        Entities entities1 = bulkItem.getEntities()[0];
        entities2.setCategory_id("");
        bulkItem.setEntities(new Entities[]{entities1, entities2});
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(bulkItem), MenuConstants.bulkadditem_categoryid, MenuConstants.token_id3});

        entities2 = new Entities();
        entities2.build();
        entities1 = bulkItem.getEntities()[0];
        entities2.setId("");
        bulkItem.setEntities(new Entities[]{entities1, entities2});
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(bulkItem),  MenuConstants.bulkadditem_id, MenuConstants.token_id3});

        entities2 = new Entities();
        entities2.build();
        entities1 = bulkItem.getEntities()[0];
        entities2.setName("");
        bulkItem.setEntities(new Entities[]{entities1, entities2});
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(bulkItem),  MenuConstants.bulkupdateitem_name, MenuConstants.token_id3});

        entities2 = new Entities();
        entities2.build();
        entities1 = bulkItem.getEntities()[0];
        entities2.setPrice(MenuConstants.negative_cgst);
        bulkItem.setEntities(new Entities[]{entities1, entities2});
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(bulkItem),  MenuConstants.bulkadditem_price, MenuConstants.token_id3});

        entities2 = new Entities();
        entities2.build();
        entities1 = bulkItem.getEntities()[0];
        entities2.setPacking_charges(MenuConstants.negative_cgst);
        bulkItem.setEntities(new Entities[]{entities1, entities2});
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(bulkItem),  MenuConstants.bulkadditem_packagingprice, MenuConstants.token_id3});

        entities2 = new Entities();
        entities2.build();
        entities1 = bulkItem.getEntities()[0];
        Gst_details gst_details = new Gst_details();
        gst_details.setCgst(MenuConstants.negative_cgst);
        entities2.setGst_details(gst_details);
        bulkItem.setEntities(new Entities[]{entities1, entities2});
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(bulkItem),  MenuConstants.bulkadditem_cgst, MenuConstants.token_id3});

        entities2 = new Entities();
        entities2.build();
        entities1 = bulkItem.getEntities()[0];
        gst_details = new Gst_details();
        gst_details.setIgst(MenuConstants.negative_igst);
        entities2.setGst_details(gst_details);
        bulkItem.setEntities(new Entities[]{entities1, entities2});
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(bulkItem),  MenuConstants.bulkadditem_igst, MenuConstants.token_id3});

        entities2 = new Entities();
        entities2.build();
        entities1 = bulkItem.getEntities()[0];
        gst_details = new Gst_details();
        gst_details.setSgst(MenuConstants.negative_sgst);
        entities2.setGst_details(gst_details);
        bulkItem.setEntities(new Entities[]{entities1, entities2});
        obj.add(new Object[]{MenuConstants.rest_id3, jsonHelper.getObjectToJSON(bulkItem),  MenuConstants.bulkadditem_sgst, MenuConstants.token_id3});

        return obj.iterator();
    }

}
