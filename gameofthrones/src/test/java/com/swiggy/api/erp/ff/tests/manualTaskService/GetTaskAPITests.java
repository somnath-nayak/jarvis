package com.swiggy.api.erp.ff.tests.manualTaskService;

import com.swiggy.api.erp.ff.dp.manualTaskService.GetTaskAPIData;
import com.swiggy.api.erp.ff.helper.ManualTaskServiceHelper;
import framework.gameofthrones.JonSnow.Processor;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashMap;

public class GetTaskAPITests extends GetTaskAPIData {

    ManualTaskServiceHelper helper = new ManualTaskServiceHelper();


    @Test(priority = 1, dataProvider = "getTaskData", groups = {"sanity", "regression"},description = "End to end tests for new user verification")
    public void getTaskAPITests(String orderId, String reason, String subReason, String source, HashMap<String, String> expectedResponseMap){
        System.out.println("***************************************** getTaskAPITests started *****************************************");
        
        Processor createTaskResponse = helper.createTask(orderId, reason, subReason, source);
        Assert.assertEquals(createTaskResponse.ResponseValidator.GetNodeValueAsInt("statusCode"), Integer.parseInt(expectedResponseMap.get("statusCode")));
        Processor getTaskResponse = helper.getTask(orderId);
        Assert.assertEquals(getTaskResponse.ResponseValidator.GetNodeValue("data[0].requestReason"),reason);
        Assert.assertEquals(getTaskResponse.ResponseValidator.GetNodeValue("data[0].status"),expectedResponseMap.get("status"));
  
        System.out.println("######################################### getTaskAPITests completed #########################################");
    }
}
