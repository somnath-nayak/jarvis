package com.swiggy.api.erp.ff.dp.omtRewrite;

import org.testng.annotations.DataProvider;

public class OmtBackendData {

	@DataProvider(name = "oldCustomerNewAreaData")
	public Object[][] getOldCustomerNewAreaData() {
		return new Object[][] {
				{ "shashank.shekhar@swiggy.in", "8892028093", "Test",
						"old customer email, old customer mobile and old area", 200, 0, "Success!", false },
				{ "shashank.shekhar@swiggy.in", "8892028093", "Test1",
						"old customer email, old customer mobile and new area", 200, 0, "Success!", true },
				{ "shashank.shekhar@swiggy.in", "8892028093", "Test",
						"new customer email, new customer mobile and new area", 200, 0, "Success!", false } };
	}

	@DataProvider(name = "filterorder")
	public Object[][] filterorder() {
		return new Object[][] { 
			{ "25796","validcase" },
			{ "","badcase" },
			{ "","!@$%^%" }
			

		};
	}
}
