package com.swiggy.api.erp.delivery.helper;

import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Base64;
import java.util.HashMap;


/**
 * Created by preetesh.sharma on 06/03/18.
 */
public class DeliveryReferalHelper {

    Initialize gameofthrones;
    String encoding=null;
    HashMap<String, String> requestheaders ;
    public DeliveryReferalHelper(String username, String password)
    {
        byte[] b=null;
        gameofthrones= new Initialize();
        try
        {
           b  = (username + ":" + password).getBytes("UTF-8");
        }
        catch (IOException e )
        {
            e.printStackTrace();
        }
        encoding=Base64.getEncoder().encodeToString(b);
        requestheaders= new HashMap<>();
        requestheaders.put("content-type", "application/json");
        requestheaders.put("Authorization", "Basic "+encoding);
    }

    public DeliveryReferalHelper(String auth)
    {

        gameofthrones= new Initialize();
        requestheaders= new HashMap<>();
        requestheaders.put("content-type", "application/json");
        requestheaders.put("Authorization", auth);
    }

    public DeliveryReferalHelper()
    {
        gameofthrones= new Initialize();
    }

    public Processor createReferal(String applicantName,String mobileNumber, String cityId, String referralType, String refId)
    {
        GameOfThronesService service = new GameOfThronesService(DeliveryConstant.delivery_Service, "createReferral", gameofthrones);
        String[] payloadparams = new String[] {applicantName,mobileNumber,cityId,referralType,refId};
        for(String str:payloadparams)
        {
            System.out.println("String is "+str);
        }
        Processor processor = new Processor(service, requestheaders,payloadparams);
        return processor;
    }

    public Processor getReferalConfig(String de_id)
    {
        GameOfThronesService service = new GameOfThronesService(DeliveryConstant.delivery_Service, "getReferralConfig", gameofthrones);
        return new Processor(service, requestheaders, null, new String[]{de_id});
    }

    public  Processor  getLeadEligibility(String mobileno)
    {
        GameOfThronesService service = new GameOfThronesService(DeliveryConstant.delivery_Service, "getLeadEligibility", gameofthrones);
        return new Processor(service, requestheaders, null, new String[]{mobileno});
    }

    public Processor getReferal(String id)
    {
        GameOfThronesService service = new GameOfThronesService(DeliveryConstant.delivery_Service, "getReferral", gameofthrones);
        return new Processor(service, requestheaders, null, new String[]{id});
    }

    public Processor getReferalList( String refId)
    {
        GameOfThronesService service = new GameOfThronesService(DeliveryConstant.delivery_Service, "getReferralList", gameofthrones);
        String[] payloadparams = new String[] {refId};

        for(String str:payloadparams)
        {
            System.out.println("String is "+str);
        }
         Processor processor= new Processor(service, requestheaders, null,payloadparams);
        return processor;
    }

    @Test
    public void compileTest()
    {
        DeliveryReferalHelper deliveryReferalHelper= new DeliveryReferalHelper();
        System.out.println("Testing compilation");
    }
}
