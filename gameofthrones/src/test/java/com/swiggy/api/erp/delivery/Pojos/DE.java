package com.swiggy.api.erp.delivery.Pojos;

/**
 * Created by preetesh.sharma on 18/02/18.
 */

import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import framework.gameofthrones.Aegon.SystemConfigProvider;

import java.util.List;
import java.util.Map;

public class DE
{


    private String educational_level;

    private String bank_name;

    private String photograph;

    private String classroom_training_date;

    private String bike_reg_no;

    private int secondary_areacode;

    private int security_money;

    private String father_name;

    private String rc_book;

    private int bicycle;

    private String shift_type;

    private String bank_ifsc;

    private String rc_type;

    private String field_training_date;

    private String proof_rc;

    private int marital_status;

    private int candidate_id;

    private String ref_name;

    private String dl_id;

    private String shifts;

    private String current_address_details;

    private int zone_id;

    private String bank_ac;

    private String permanent_address_details;

    private String bank_ac_name;

    private String agency_name;

    private String record_timestamp;

    private String bike_name;

    private String proof_address;

    private String inventory_items;

    private String blood_group;

    private String child2_name;

    private String voter_id;

    private String security_deposit_receipt;

    private String swiggy_mobile_number;

    private int areacode;

    private int secondary_zone;

    private String wife_name;

    private int city_id;

    private String child1_name;

    private String name;

    private String proof_dl;

    private String child1_sex;

    private String proof_pan;

    private String languages_known;

    private int employment_status;

    private int byod;

    private String aadhar_card;

    private String dl_details;

    private String emergency_no;

    private String pan_type;

    private String relationship;

    private String source;

    private String pan_card_number;

    private String child2_sex;

    private String personal_no;

    private String bag_qr_code;

    public DE(int zone_id)
    {
        this("");
        this.zone_id=zone_id;
        this.shifts="1";
        this.shift_type="F";
    }

    public DE(int zone_id, String shifts)
    {
        this("");
        this.zone_id=zone_id;
    }

    public DE(int zone_id,int areacode)
    {
        this("");
        this.zone_id=zone_id;
        this.areacode=areacode;
        this.shifts="1";
        this.shift_type="F";
    }

    public DE()
    {

    }
    public DE(String str)
    {
        String emptyString=str;
        this.educational_level=emptyString;
        this.bank_name=emptyString;
        this.photograph=emptyString;
        this.bike_reg_no=emptyString;
        this.rc_book=emptyString;
        this.shift_type=emptyString;
        this.bank_ifsc=emptyString;
        this.rc_type=emptyString;
        this.proof_rc=emptyString;
        this.ref_name=emptyString;
        this.shifts=emptyString;
        this.bank_ac=emptyString;
        this.bank_ac_name=emptyString;
        this.agency_name=emptyString;
        this.bike_name=emptyString;
        this.proof_address=emptyString;
        this.blood_group=emptyString;
        this.child2_name=emptyString;
        this.voter_id=emptyString;
        this.security_deposit_receipt=emptyString;
        this.wife_name=emptyString;
        this.child1_name=emptyString;
        this.proof_dl=emptyString;
        this.child1_sex=emptyString;
        this.proof_pan=emptyString;
        this.languages_known=emptyString;
        this.aadhar_card=emptyString;
        this.emergency_no=emptyString;
        this.pan_type=emptyString;
        this.relationship=emptyString;
        this.source=emptyString;
        this.pan_card_number=emptyString;
        this.child2_sex=emptyString;
        this.bag_qr_code=emptyString;
    }

    public String getEducational_level ()
    {
        return educational_level;
    }

    public void setEducational_level (String educational_level)
    {
        this.educational_level = educational_level;
    }

    public String getBank_name ()
    {
        return bank_name;
    }

    public void setBank_name (String bank_name)
    {
        this.bank_name = bank_name;
    }

    public String getPhotograph ()
    {
        return photograph;
    }

    public void setPhotograph (String photograph)
    {
        this.photograph = photograph;
    }

    public String getClassroom_training_date ()
    {
        return classroom_training_date;
    }

    public void setClassroom_training_date (String classroom_training_date)
    {
        this.classroom_training_date = classroom_training_date;
    }

    public String getBike_reg_no ()
    {
        return bike_reg_no;
    }

    public void setBike_reg_no (String bike_reg_no)
    {
        this.bike_reg_no = bike_reg_no;
    }

    public int getSecondary_areacode ()
    {
        return secondary_areacode;
    }

    public void setSecondary_areacode (int secondary_areacode)
    {
        this.secondary_areacode = secondary_areacode;
    }

    public int getSecurity_money ()
    {
        return security_money;
    }

    public void setSecurity_money (int security_money)
    {
        this.security_money = security_money;
    }

    public String getFather_name ()
    {
        return father_name;
    }

    public void setFather_name (String father_name)
    {
        this.father_name = father_name;
    }

    public String getRc_book ()
    {
        return rc_book;
    }

    public void setRc_book (String rc_book)
    {
        this.rc_book = rc_book;
    }

    public int getBicycle ()
    {
        return bicycle;
    }

    public void setBicycle (int bicycle)
    {
        this.bicycle = bicycle;
    }

    public String getShift_type ()
    {
        return shift_type;
    }

    public void setShift_type (String shift_type)
    {
        this.shift_type = shift_type;
    }

    public String getBank_ifsc ()
    {
        return bank_ifsc;
    }

    public void setBank_ifsc (String bank_ifsc)
    {
        this.bank_ifsc = bank_ifsc;
    }

    public String getRc_type ()
    {
        return rc_type;
    }

    public void setRc_type (String rc_type)
    {
        this.rc_type = rc_type;
    }

    public String getField_training_date ()
    {
        return field_training_date;
    }

    public void setField_training_date (String field_training_date)
    {
        this.field_training_date = field_training_date;
    }

    public String getProof_rc ()
    {
        return proof_rc;
    }

    public void setProof_rc (String proof_rc)
    {
        this.proof_rc = proof_rc;
    }

    public int getMarital_status ()
    {
        return marital_status;
    }

    public void setMarital_status (int marital_status)
    {
        this.marital_status = marital_status;
    }

    public int getCandidate_id ()
    {
        return candidate_id;
    }

    public void setCandidate_id (int candidate_id)
    {
        this.candidate_id = candidate_id;
    }

    public String getRef_name ()
    {
        return ref_name;
    }

    public void setRef_name (String ref_name)
    {
        this.ref_name = ref_name;
    }

    public String getDl_id ()
    {
        return dl_id;
    }

    public void setDl_id (String dl_id)
    {
        this.dl_id = dl_id;
    }

    public String getShifts ()
    {
        return shifts;
    }

    public void setShifts (String shifts)
    {
        this.shifts = shifts;
    }

    public String getCurrent_address_details ()
    {
        return current_address_details;
    }

    public void setCurrent_address_details (String current_address_details)
    {
        this.current_address_details = current_address_details;
    }

    public int getZone_id ()
    {
        return zone_id;
    }

    public void setZone_id (int zone_id)
    {
        this.zone_id = zone_id;
    }

    public String getBank_ac ()
    {
        return bank_ac;
    }

    public void setBank_ac (String bank_ac)
    {
        this.bank_ac = bank_ac;
    }

    public String getPermanent_address_details ()
    {
        return permanent_address_details;
    }

    public void setPermanent_address_details (String permanent_address_details)
    {
        this.permanent_address_details = permanent_address_details;
    }

    public String getBank_ac_name ()
    {
        return bank_ac_name;
    }

    public void setBank_ac_name (String bank_ac_name)
    {
        this.bank_ac_name = bank_ac_name;
    }

    public String getAgency_name ()
    {
        return agency_name;
    }

    public void setAgency_name (String agency_name)
    {
        this.agency_name = agency_name;
    }

    public String getRecord_timestamp ()
    {
        return record_timestamp;
    }

    public void setRecord_timestamp (String record_timestamp)
    {
        this.record_timestamp = record_timestamp;
    }

    public String getBike_name ()
    {
        return bike_name;
    }

    public void setBike_name (String bike_name)
    {
        this.bike_name = bike_name;
    }

    public String getProof_address ()
    {
        return proof_address;
    }

    public void setProof_address (String proof_address)
    {
        this.proof_address = proof_address;
    }

    public String getInventory_items ()
    {
        return inventory_items;
    }

    public void setInventory_items (String inventory_items)
    {
        this.inventory_items = inventory_items;
    }

    public String getBlood_group ()
    {
        return blood_group;
    }

    public void setBlood_group (String blood_group)
    {
        this.blood_group = blood_group;
    }

    public String getChild2_name ()
    {
        return child2_name;
    }

    public void setChild2_name (String child2_name)
    {
        this.child2_name = child2_name;
    }

    public String getVoter_id ()
    {
        return voter_id;
    }

    public void setVoter_id (String voter_id)
    {
        this.voter_id = voter_id;
    }

    public String getSecurity_deposit_receipt ()
    {
        return security_deposit_receipt;
    }

    public void setSecurity_deposit_receipt (String security_deposit_receipt)
    {
        this.security_deposit_receipt = security_deposit_receipt;
    }

    public String getSwiggy_mobile_number ()
    {
        return swiggy_mobile_number;
    }

    public void setSwiggy_mobile_number (String swiggy_mobile_number)
    {
        this.swiggy_mobile_number = swiggy_mobile_number;
    }

    public int getAreacode ()
    {
        return areacode;
    }

    public void setAreacode (int areacode)
    {
        this.areacode = areacode;
    }

    public int getSecondary_zone ()
    {
        return secondary_zone;
    }

    public void setSecondary_zone (int secondary_zone)
    {
        this.secondary_zone = secondary_zone;
    }

    public String getWife_name ()
    {
        return wife_name;
    }

    public void setWife_name (String wife_name)
    {
        this.wife_name = wife_name;
    }

    public int getCity_id ()
    {
        return city_id;
    }

    public void setCity_id (int city_id)
    {
        this.city_id = city_id;
    }

    public String getChild1_name ()
    {
        return child1_name;
    }

    public void setChild1_name (String child1_name)
    {
        this.child1_name = child1_name;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getProof_dl ()
    {
        return proof_dl;
    }

    public void setProof_dl (String proof_dl)
    {
        this.proof_dl = proof_dl;
    }

    public String getChild1_sex ()
    {
        return child1_sex;
    }

    public void setChild1_sex (String child1_sex)
    {
        this.child1_sex = child1_sex;
    }

    public String getProof_pan ()
    {
        return proof_pan;
    }

    public void setProof_pan (String proof_pan)
    {
        this.proof_pan = proof_pan;
    }

    public String getLanguages_known ()
    {
        return languages_known;
    }

    public void setLanguages_known (String languages_known)
    {
        this.languages_known = languages_known;
    }

    public int getEmployment_status ()
    {
        return employment_status;
    }

    public void setEmployment_status (int employment_status)
    {
        this.employment_status = employment_status;
    }

    public int getByod ()
    {
        return byod;
    }

    public void setByod (int byod)
    {
        this.byod = byod;
    }

    public String getAadhar_card ()
    {
        return aadhar_card;
    }

    public void setAadhar_card (String aadhar_card)
    {
        this.aadhar_card = aadhar_card;
    }

    public String getDl_details ()
    {
        return dl_details;
    }

    public void setDl_details (String dl_details)
    {
        this.dl_details = dl_details;
    }

    public String getEmergency_no ()
    {
        return emergency_no;
    }

    public void setEmergency_no (String emergency_no)
    {
        this.emergency_no = emergency_no;
    }

    public String getPan_type ()
    {
        return pan_type;
    }

    public void setPan_type (String pan_type)
    {
        this.pan_type = pan_type;
    }

    public String getRelationship ()
    {
        return relationship;
    }

    public void setRelationship (String relationship)
    {
        this.relationship = relationship;
    }

    public String getSource ()
    {
        return source;
    }

    public void setSource (String source)
    {
        this.source = source;
    }

    public String getPan_card_number ()
    {
        return pan_card_number;
    }

    public void setPan_card_number (String pan_card_number)
    {
        this.pan_card_number = pan_card_number;
    }

    public String getChild2_sex ()
    {
        return child2_sex;
    }

    public void setChild2_sex (String child2_sex)
    {
        this.child2_sex = child2_sex;
    }

    public String getPersonal_no ()
    {
        return personal_no;
    }

    public void setPersonal_no (String personal_no)
    {
        this.personal_no = personal_no;
    }

    public String getBag_qr_code ()
    {
        return bag_qr_code;
    }

    public void setBag_qr_code (String bag_qr_code)
    {
        this.bag_qr_code = bag_qr_code;
    }

/*        @Override
        public String toString()
        {
            return "ClassPojo [educational_level = "+educational_level+", bank_name = "+bank_name+", photograph = "+photograph+", classroom_training_date = "+classroom_training_date+", bike_reg_no = "+bike_reg_no+", secondary_areacode = "+secondary_areacode+", security_money = "+security_money+", father_name = "+father_name+", rc_book = "+rc_book+", bicycle = "+bicycle+", shift_type = "+shift_type+", bank_ifsc = "+bank_ifsc+", rc_type = "+rc_type+", field_training_date = "+field_training_date+", proof_rc = "+proof_rc+", marital_status = "+marital_status+", candidate_id = "+candidate_id+", ref_name = "+ref_name+", dl_id = "+dl_id+", shifts = "+shifts+", current_address_details = "+current_address_details+", zone_id = "+zone_id+", bank_ac = "+bank_ac+", permanent_address_details = "+permanent_address_details+", bank_ac_name = "+bank_ac_name+", agency_name = "+agency_name+", record_timestamp = "+record_timestamp+", bike_name = "+bike_name+", proof_address = "+proof_address+", inventory_items = "+inventory_items+", blood_group = "+blood_group+", child2_name = "+child2_name+", voter_id = "+voter_id+", security_deposit_receipt = "+security_deposit_receipt+", swiggy_mobile_number = "+swiggy_mobile_number+", areacode = "+areacode+", secondary_zone = "+secondary_zone+", wife_name = "+wife_name+", city_id = "+city_id+", child1_name = "+child1_name+", name = "+name+", proof_dl = "+proof_dl+", child1_sex = "+child1_sex+", proof_pan = "+proof_pan+", languages_known = "+languages_known+", employment_status = "+employment_status+", byod = "+byod+", aadhar_card = "+aadhar_card+", dl_details = "+dl_details+", emergency_no = "+emergency_no+", pan_type = "+pan_type+", relationship = "+relationship+", source = "+source+", pan_card_number = "+pan_card_number+", child2_sex = "+child2_sex+", personal_no = "+personal_no+", bag_qr_code = "+bag_qr_code+"]";
        }*/

    public void build()
    {
        if(candidate_id==0)
        {
            List<Map<String, Object>> thisObject= SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForList(DeliveryConstant.lastcandidatequery);
            Map<String,Object>testMap=thisObject.get(0);
            for(String o:testMap.keySet())
            {
                System.out.println(o+"Printing Map" +testMap.get(o).toString());
            }
            int cid=Integer.parseInt(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForList(DeliveryConstant.lastcandidatequery).get(0).get("candidate_id").toString());
            this.candidate_id=cid;
        }
        if(this.name==null)
        {
            this.setName("TestDE");
        }
        if(this.record_timestamp==null)
        {
            this.setRecord_timestamp("2018-02-16 18:33:00.497649");
            //Need to derive it randomly
        }
        if(this.city_id==0)
        {
            this.setCity_id(1);
            //Neeed this asweel randomly
        }
        if(this.marital_status==0)
        {
            this.setMarital_status(1);
        }
        if(this.father_name==null)
        {
            this.setFather_name("defather");
        }
        if(this.personal_no==null)
        {
            this.setPersonal_no("2323232323");
        }
        if(this.byod==0)
        {
            this.setByod(0);
        }
        if(this.bicycle==0)
        {
            this.setBicycle(0);
        }
        if(this.current_address_details==null)
        {
            this.setCurrent_address_details("{}");
        }
        if(this.permanent_address_details==null)
        {
            this.setPermanent_address_details("{}");
        }
        if(this.dl_details==null)
        {
            this.setDl_details("{}");
        }
        if(this.dl_id==null)
        {
            String licenseno="MP0"+Math.random()*10+"-"+(Math.random()*10000+Math.random()*1000+Math.random()*100)+"-"+(Math.random()*10000+Math.random()*1000+Math.random()*100)*1000;
            this.setDl_id(licenseno);
        }
        if (this.pan_card_number==null)
        {
            this.setPan_card_number("BNQPS"+Math.random()*10000+"E");
        }
        if(this.classroom_training_date==null)
        {
            this.setClassroom_training_date("2018-02-13");
        }
        if(this.field_training_date==null)
        {
            this.setField_training_date("2018-02-13");
        }
        if(this.inventory_items==null)
        {
            this.setInventory_items("{}");
        }
        if(this.employment_status==0)
        {
            this.setEmployment_status(1);
        }
        if(this.swiggy_mobile_number==null)
        {
            String mobileno="96"+Math.round(Math.random()*10000)+Math.round(Math.random())*1000;
            System.out.println("mobile no is "+mobileno);
            this.setSwiggy_mobile_number(mobileno);
        }
        if(this.areacode==0)
        {
            this.setAreacode(1);
        }
        if(this.zone_id==0)
        {
            this.setZone_id(4);
        }
        if(this.security_money==0)
        {
            this.setSecurity_money(20000);
        }

    }
}