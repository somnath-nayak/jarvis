package com.swiggy.api.erp.delivery.helper;

public interface OrderStatus {
	
	String ASSIGNED = "assigned";
	String CONFIRMED="confirmed";
	String ARRIVED="arrived";
	String PICKEDUP="pickedup";
	String REACHED="reached";
	String DELIVERED="delivered";
}
	

