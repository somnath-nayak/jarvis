package com.swiggy.api.erp.ff.dp.verificationService;

import org.testng.annotations.DataProvider;


public class VerificationServiceApiTestData {


	private Object[][] queueOrderToAssignVerifierDP(){
		return new Object[][]{
				{"manual","true", "true", "true"},
				{"manual","true", "true", "false"},
				{"manual","true", "false", "true"},
				{"manual","true", "false", "false"},
				{"manual","false", "true", "true"},
				{"manual","false", "true", "false"},
				{"manual","false", "false", "true"},
				{"manual","false", "false", "false"},
				{"ivrs","true", "true", "true"},
				{"ivrs","true", "true", "false"},
				{"ivrs","true", "false", "true"},
				{"ivrs","true", "false", "false"},
				{"ivrs","false", "true", "true"},
				{"ivrs","false", "true", "false"},
				{"ivrs","false", "false", "true"},
				{"ivrs","false", "false", "false"}
		};
	}


	@DataProvider(name = "queueOrderToAssignVerifier")
	public Object[][] queueManualOrderToAssignVerifier(){
		return queueOrderToAssignVerifierDP();
	}

	@DataProvider(name = "queueOrderToAssignVerifier1")
	public Object[][] queueManualOrderToAssignVerifier1(){
		return queueOrderToAssignVerifierDP();
	}

	@DataProvider(name = "queueOrderToAssignVerifier2")
	public Object[][] queueManualOrderToAssignVerifier2(){
		return queueOrderToAssignVerifierDP();
	}

	@DataProvider(name = "queueOrderToAssignVerifier3")
	public Object[][] queueManualOrderToAssignVerifier3(){
		return queueOrderToAssignVerifierDP();
	}

	@DataProvider(name = "queueOrderToAssignVerifier4")
	public Object[][] queueManualOrderToAssignVerifier4(){
		return queueOrderToAssignVerifierDP();
	}

	@DataProvider(name = "queueOrderToAssignVerifier5")
	public Object[][] queueManualOrderToAssignVerifier5(){
		return queueOrderToAssignVerifierDP();
	}

	@DataProvider(name = "queueOrderToAssignVerifier6")
	public Object[][] queueManualOrderToAssignVerifier6(){
		return queueOrderToAssignVerifierDP();
	}

	@DataProvider(name = "newUserE2ETestData")
	public Object[][] newUserE2ETestData(){
		return new Object[][]{
//			{boolean newUser, boolean longDistance, boolean crossAreaVerification, boolean isOrderReplicated, String orderType}
			{true, false, false, false, "manual"},
			{true, false, true, false, "manual"},
			{true, true, false, false, "manual"},
			{true, true, true, false, "manual"},
			{true, false, false, false, "partner"},
			{true, false, true, false, "partner"},
			{true, true, false, false, "partner"},
			{true, true, true, false, "partner"},

			{true, false, false, true, "manual"},
			{true, false, true, true, "manual"},
			{true, true, false, true, "manual"},
			{true, true, true, true, "manual"},
			{true, false, false, true, "partner"},
			{true, false, true, true, "partner"},
			{true, true, false, true, "partner"},
			{true, true, true, true, "partner"},
		};
	}
	
	@DataProvider(name = "crossAreaE2ETestData")
	public Object[][] crossAreaE2ETestData(){
		return new Object[][]{
//			{boolean newUser, boolean longDistance, boolean crossAreaVerification, boolean isOrderReplicated, String orderType}
			{false, false, false, false, "manual"},
			{false, false, true, false, "manual"},
			{false, true, false, false, "manual"},
			{false, true, true, false, "manual"},
			{false, false, false, false, "partner"},
			{false, false, true, false, "partner"},
			{false, true, false, false, "partner"},
			{false, true, true, false, "partner"},

			{false, false, false, true, "manual"},
			{false, false, true, true, "manual"},
			{false, true, false, true, "manual"},
			{false, true, true, true, "manual"},
			{false, false, false, true, "partner"},
			{false, false, true, true, "partner"},
			{false, true, false, true, "partner"},
			{false, true, true, true, "partner"},
		};
	}
	
}
