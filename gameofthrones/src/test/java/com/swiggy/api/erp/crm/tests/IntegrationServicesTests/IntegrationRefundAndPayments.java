package com.swiggy.api.erp.crm.tests.refundCoupons;

import com.swiggy.api.erp.crm.constants.CRMConstants;
import com.swiggy.api.erp.crm.constants.FlowMapper;
import com.swiggy.api.erp.crm.dp.MockServiceDP;
import com.swiggy.api.erp.crm.helper.CRMUpstreamValidation;
import com.swiggy.api.erp.ff.helper.FraudServiceHelper;
import com.swiggy.api.sf.snd.helper.SnDHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.RedisHelper;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static org.testng.Assert.assertTrue;

public class IntegrationRefundAndPayments  extends MockServiceDP {

        FlowMapper flowMapper = new FlowMapper();
        FraudServiceHelper fraudServiceHelper = new FraudServiceHelper();
        Initialize gameofthrones = new Initialize();
        RedisHelper redisHelper = new RedisHelper();
        CRMUpstreamValidation crmUpstreamValidation = new CRMUpstreamValidation();

        public Processor setCodStatusFF(String customerId, String cancellationFee){
            String body  = "{\"request_id\":\"Testing\",\"customer_details\":{\"customer_id\":\"${customer_id}\",\"pending_cancellation_fee\":\"${pending_cancellation_fee}\",\"swiggy_money_used\":0.0,\"mobile_no\":1.23456789E8},\"session_details\":{\"timestamp\":\"2018-07-03 13:56:00\",\"device_id\":\"abcde-fghij-klmno\",\"user_agent\":\"Swiggy-Android\"},\"delivery_details\":{\"customer_geohash\":\"abcde\",\"restaurant_customer_distance_kms\":\"1.45\",\"city_id\":5,\"area_id\":2},\"cart_details\":{\"restaurant_id\":\"12345\",\"bill_amount\":345,\"item_quantity\":10,\"item_count\":3}}";
            body = body.replace("${customer_id}", customerId).replace("${pending_cancellation_fee}", cancellationFee);
            System.out.println("getCODCheckServiceProcessor "+body);
            return fraudServiceHelper.getCODCheckServiceProcessor(body);
         }

        @Test(dataProvider = "mockCodFlow", groups = {"regression"}, description = "End to end tests for Cod Block flow")
        public void integrationCodFlow(HashMap<String, String> flowDetails) throws IOException, InterruptedException {
            HashMap<String, String> requestheaders_inbound = new HashMap<String, String>();
            requestheaders_inbound.put("Content-Type", "application/json");
            String ffStatus = flowDetails.get("ffStatus").toString();
            String deliveryStatus = flowDetails.get("deliveryStatus").toString();
            String flowName = flowDetails.get("flow").toString();
            String conversationId, deviceId;
            String orderId,nodeId ;//= flowDetails.get("order").toString();
            String[] flow = flowMapper.setFlow().get(flowName);
            String cancellationFees = flowDetails.get("cancellationFee").toString();
            String queryparam[] = new String[1];
            int lengthofflow = flow.length;

            System.out.println("lengthofflow \t" + lengthofflow);

            Processor logginResponse = SnDHelper.consumerLogin("7507220659","Test@2211");
            String customerId = "5769085";//logginResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.customer_id");//"387395";
            System.out.println("Customer ID "+customerId);
            redisHelper.setValueJson("checkoutredis", 0, "user_credit_7191663", "{\"userId\":"+customerId+",\"swiggyMoney\":0.0,\"cancellationFee\":0.0}");
            String getValue = (String) redisHelper.getValue("checkoutredis", 0, "user_credit_7191663");
            System.out.println("key value" + getValue);
            HashMap<String, String> map = crmUpstreamValidation.e2e("7507220659", "Test@2211", "757", "613982", "1", deliveryStatus, ffStatus);
            System.out.print("map returned \t " + map);
            orderId ="1055955769";//map.get("order_id");//1055952078
            Assert.assertNotNull(orderId);
            conversationId = orderId;
            int cnt = lengthofflow - 1;
            Assert.assertNotNull(orderId);
            boolean deliveryStatusFlag = false;
            String cod_enabled = "true";
            if (deliveryStatus.equals("delivered"))
                deliveryStatusFlag = true;
                Processor codstatusresponse = setCodStatusFF( customerId, cancellationFees);
                cod_enabled = codstatusresponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("cod_enabled");
                 System.out.println("cod_enabled "+cod_enabled);
                for (int i = 0; i < cnt; i++) {
                    System.out.println("lengthofflow "+ cnt);
                    nodeId = flow[i];
                    if(nodeId == "1" ){
                        continue;
                    } else {
                        queryparam[0] = nodeId;
                        String[] paylaodparam = new String[]{customerId, orderId, conversationId};

                        GameOfThronesService isvalidcod = new GameOfThronesService("crm", "codBlock", gameofthrones);
                        Processor isvalidcod_response = new Processor(isvalidcod,
                                requestheaders_inbound, paylaodparam, queryparam);

                        Assert.assertEquals(isvalidcod_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("status"), "SUCCESS",
                                "Actual status code doesnt match with expected status code ");

                        String addOnGroupList = isvalidcod_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.childrenNodes..id").toString().replace("[", "").replace("]", "");
                        List<String> addOnlist = Arrays.asList(addOnGroupList.split(","));

                        System.out.println("addonlist" + addOnlist);
                        System.out.println("flowname[i+1]" + flow[i + 1]);
                        System.out.println("lengthofflow " + Arrays.asList(lengthofflow));

                        if (i != cnt) {
                            if (deliveryStatusFlag || cod_enabled.equals("true")) {
                                if (nodeId == "2" || deliveryStatusFlag) {
                                    assertTrue(!addOnlist.contains(CRMConstants.CheckCodEnabledAfterPlacingOrder));
                                    break;
                                } else if (nodeId == "3") {
                                    assertTrue(!addOnlist.contains(CRMConstants.CheckCodEnabled));
                                    break;
                                }
                            } else
                                assertTrue(addOnlist.contains(flow[i + 1]));
                        }
                    }
            }
        }
    }

