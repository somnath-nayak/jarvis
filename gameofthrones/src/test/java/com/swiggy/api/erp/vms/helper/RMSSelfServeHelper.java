package com.swiggy.api.erp.vms.helper;


import com.swiggy.api.erp.vms.constants.RMSConstants;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import java.util.HashMap;

public class RMSSelfServeHelper extends RMSConstants{

    Initialize initializer=new Initialize();

    public Processor selfServeMenuGETMENU(String accessToken, String restaurantId){
        GameOfThronesService services=new GameOfThronesService("rms", "get_MENU", initializer);
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("Content-Type", "application/json");
        requestheaders.put("Cookie", "Swiggy_Session-alpha="+accessToken);

        Processor processor= new Processor(services,requestheaders,null,new String[]{restaurantId});
        return processor;
    }

    public Processor selfServeMenuAddNewItem(String accessToken, String name,String is_veg, String packing_charges,String description, String item_change_request, String addons_and_variant_change_request, String restId, String item_Id, String markInStock,String s3_image_url,String price,String category_id,String main_category_id){
        GameOfThronesService services=new GameOfThronesService("rms", "add_NEW_ITEM", initializer);
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("Cookie", "Swiggy_Session-alpha="+accessToken);

        Processor processor= new Processor(services,requestheaders,new String[]{
                name,is_veg,packing_charges,description,item_change_request,addons_and_variant_change_request,
                restId,item_Id,markInStock,s3_image_url,price,category_id,main_category_id},null);
        return processor;

    }

    public Processor selfServeMenuEditITEM(String accessToken, String item_Id,String restaurantId,String description, String seller_tier){

        GameOfThronesService services=new GameOfThronesService("rms", "editITEM", initializer);
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("Cookie", "Swiggy_Session-alpha="+accessToken);

        Processor processor= new Processor(services,requestheaders,new String[]{ item_Id,restaurantId,description,seller_tier, SSMEI_statusMessage},null);
        return processor;

    }

    public Processor selfServeMenuDeleteItem(String accessToken, String restaurantId,String item_id){
        GameOfThronesService services=new GameOfThronesService("rms", "deletITEM", initializer);
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("Cookie", "Swiggy_Session-alpha="+accessToken);

        Processor processor= new Processor(services,requestheaders,new String[]{restaurantId,item_id},null);
        return processor;

    }
    public Processor selfServeMenuRevisonHistory(String accessToken, String restaurantId){
        GameOfThronesService services=new GameOfThronesService("rms", "revisonHistory", initializer);
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("Content-Type", "application/json");
        requestheaders.put("Cookie", "Swiggy_Session-alpha="+accessToken);
        Processor processor= new Processor(services,requestheaders,null,new String[]{restaurantId});
        return processor;
    }

    public Processor selfServeMenuRevisonHistory(String accessToken, String restaurantId, String searchQuery){
        GameOfThronesService services=new GameOfThronesService("rms", "revisonHistory1", initializer);
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("Content-Type", "application/json");
        requestheaders.put("Cookie", "Swiggy_Session-alpha="+accessToken);
        Processor processor= new Processor(services,requestheaders,null,new String[]{restaurantId, searchQuery});
        return processor;
    }

    public Processor selfServeMenuItemImageUpload(String accessToken, String filePath, String restaurantId){
        GameOfThronesService services=new GameOfThronesService("rms", "itemImageUpload", initializer);
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("Cookie", "Swiggy_Session-alpha="+accessToken);

        HashMap<String, String> formData = new HashMap<>();
        formData.put("file", filePath);
        formData.put("restId", restaurantId);
        Processor processor= new Processor(services,requestheaders,null,null, formData);
        return processor;
    }

    public Processor selfServeMenuUploadItemCatalogueImage(String accessToken, String filePath,String restaurantId){
        GameOfThronesService services=new GameOfThronesService("rms", "uploadItemCatalogueImage", initializer);
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("Cookie", "Swiggy_Session-alpha="+accessToken);

        HashMap<String, String> formData = new HashMap<>();
        formData.put("file", filePath);
        formData.put("restaurantId", restaurantId);
        Processor processor= new Processor(services,requestheaders,null,null, formData);
        return processor;

    }

    public Processor selfServeMenuSubmitPendingTickets(String accessToken, String restaurantId,String ticketIds){
        GameOfThronesService services=new GameOfThronesService("rms", "submitPendingTickets", initializer);
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("Content-Type", "application/json");
        requestheaders.put("Cookie", "Swiggy_Session-alpha="+accessToken);

        Processor processor= new Processor(services,requestheaders,new String[]{restaurantId,ticketIds},null);
        return processor;

    }

    public Processor selfServeMenuCalcualteService(String accessToken, String packagingCharges,String price, String restaurantid){

        GameOfThronesService services=new GameOfThronesService("rms", "calcualteService", initializer);
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("Content-Type", "application/json");
        requestheaders.put("Cookie", "Swiggy_Session-alpha="+accessToken);
        Processor processor= new Processor(services,requestheaders,null, new String[]{packagingCharges,price,restaurantid});
        return processor;
    }

    public Processor selfServeMenudeleteCartImage(String accessToken, String restaurantId,String imageId){

        GameOfThronesService services=new GameOfThronesService("rms", "deleteCartImage", initializer);
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("Content-Type", "application/json");
        requestheaders.put("Cookie", "Swiggy_Session-alpha="+accessToken);
        Processor processor= new Processor(services,requestheaders,null, new String[]{restaurantId,imageId});
        return processor;
    }
    public Processor selfServeMenuItemTicket(String accessToken, String restaurantId,String ticketId){

        GameOfThronesService services=new GameOfThronesService("rms", "itemTicket", initializer);
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("Content-Type", "application/json");
        requestheaders.put("Cookie", "Swiggy_Session-alpha="+accessToken);
        Processor processor= new Processor(services,requestheaders,null, new String[]{restaurantId,ticketId});
        return processor;
    }

    public Processor selfServeMenuCancelTicket(String accessToken, String restaurantId,String ticketId){

        GameOfThronesService services=new GameOfThronesService("rms", "cancelTicket", initializer);
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("Content-Type", "application/json");
        requestheaders.put("Cookie", "Swiggy_Session-alpha="+accessToken);
        Processor processor= new Processor(services,requestheaders,null, new String[]{restaurantId,ticketId});
        return processor;
    }

    public boolean aggregateResponseAndStatusCodes(Processor response) {
        boolean success = false;

        System.out.println("SuccessresponseCode :" + response.ResponseValidator.GetResponseCode());
        System.out.println("SuccessstatusCode : " + response.ResponseValidator.GetNodeValueAsInt("statusCode"));

        if ((RMSConstants.successresponseCode == response.ResponseValidator.GetResponseCode())
                && RMSConstants.successstatusCode == response.ResponseValidator.GetNodeValueAsInt("statusCode")) {
            System.out.println("1..inside response");
            success = true;
        } else if ((RMSConstants.successresponseCode == response.ResponseValidator.GetResponseCode())
                && RMSConstants.failedstatusCode == response.ResponseValidator.GetNodeValueAsInt("statusCode")) {
            System.out.println("2..inside response");
            System.out.println("SuccessresponseCode: " + response.ResponseValidator.GetResponseCode());
            System.out.println("SuccessstatusCode :  " + response.ResponseValidator.GetNodeValueAsInt("statusCode"));
            success = true;
        } else if ((RMSConstants.successresponseCode == response.ResponseValidator.GetResponseCode())
                && RMSConstants.failedstatusCode_invalidSession == response.ResponseValidator
                .GetNodeValueAsInt("statusCode")) {
            System.out.println("3..inside response");
            System.out.println("SuccessresponseCode: " + response.ResponseValidator.GetResponseCode());
            System.out.println("SuccessstatusCode :  " + response.ResponseValidator.GetNodeValueAsInt("statusCode"));
            success = true;
        }

        else if((RMSConstants.successresponseCode == response.ResponseValidator.GetResponseCode()) &&
                RMSConstants.failedstatusCode_alreadyExist == response.ResponseValidator.GetNodeValueAsInt("statusCode")){
            System.out.println("4..inside response");
            System.out.println("SuccessresponseCode: " + response.ResponseValidator.GetResponseCode());
            System.out.println("SuccessstatusCode :  " + response.ResponseValidator.GetNodeValueAsInt("statusCode"));
            success = true;
        }
        return success;
    }

    public Processor fetchItems(String accessToken, String restaurantId) {
        GameOfThronesService services = new GameOfThronesService("rms", "fetchItems", initializer);
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("Content-Type", "application/json");
        requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
        Processor processor = new Processor(services, requestheaders, null, new String[] { restaurantId });
        return processor;
    }

    public Processor markCategoryOOS(String accessToken, String restaurantId, String catId) {
        GameOfThronesService services = new GameOfThronesService("rms", "markCategoryOOS", initializer);
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("Content-Type", "application/json");
        requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
        Processor processor = new Processor(services, requestheaders, new String[] { restaurantId, catId });
        return processor;
    }

    public Processor markCategoryOOS(String accessToken, String restaurantId, String catId, String fromTime, String toTime) {
        GameOfThronesService services = new GameOfThronesService("rms", "markCategoryOOS1", initializer);
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("Content-Type", "application/json");
        requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
        Processor processor = new Processor(services, requestheaders, new String[] { restaurantId, catId, fromTime, toTime });
        return processor;
    }

    public Processor markCategoryInStock(String accessToken, String restaurantId, String catId) {
        GameOfThronesService services = new GameOfThronesService("rms", "markCategoryInStock", initializer);
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("Content-Type", "application/json");
        requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
        Processor processor = new Processor(services, requestheaders, new String[] { restaurantId, catId });
        return processor;
    }

}
