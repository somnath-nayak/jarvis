package com.swiggy.api.erp.cms.tests.CatalogV2;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.helper.CatalogV2Helper;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CreateProductCategoryWithRule {
    CatalogV2Helper catalogV2Helper=new CatalogV2Helper();
    String catId;
    @Test(description = "Creates product category with rules and without allowed values")
    public void createProductCategoryWithRule(){
        String response=catalogV2Helper.createCategoryWithRule().ResponseValidator.GetBodyAsText();
        catId=JsonPath.read(response,"$.id");
        Assert.assertNotNull(catId);
    }
}
