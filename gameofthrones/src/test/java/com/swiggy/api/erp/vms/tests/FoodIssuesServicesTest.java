package com.swiggy.api.erp.vms.tests;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.ff.helper.OMSHelper;
import com.swiggy.api.erp.vms.constants.RMSConstants;
import com.swiggy.api.erp.vms.dp.FoodIssuesServicesDP;
import com.swiggy.api.erp.vms.helper.RMSCommonHelper;
import com.swiggy.api.erp.vms.helper.RMSHelper;
import framework.gameofthrones.Cersei.ToolBox;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import org.apache.commons.collections.CollectionUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Imran Khan.
 */

public class FoodIssuesServicesTest extends FoodIssuesServicesDP
{

		RMSHelper helper = new RMSHelper();
		OMSHelper omsHellper = new OMSHelper();
		SchemaValidatorUtils schemaValidatorUtils = new SchemaValidatorUtils();

		@Test(dataProvider = "foodIssuesGetConfig_DP", groups = { "sanity", "smoke" },  description = "Check food Issues GetConfig file")
		public void foodIssuesGetConfig(String restaurantId, String FFIGC_statusMessage, ArrayList<String> expectedFlowConfigList)throws IOException, ProcessingException
		{
			Processor processor = helper.foodIssuesGetConfig(restaurantId);
			String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/get_config");
			List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema, processor.ResponseValidator.GetBodyAsText());
			Assert.assertTrue(CollectionUtils.isEmpty(missingNodeList),missingNodeList + " Nodes Are Missing Or Not Matching For Get Config API");
			Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
			Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), FFIGC_statusMessage);
			Assert.assertTrue(expectedFlowConfigList.contains(processor.ResponseValidator.GetNodeValue("data.flowConfig")));
			Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.billMismatchBlockerTimeout"));
			Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.noBillBlockerTimeout"));
		}

		@Test(dataProvider = "foodIssuesGetConfig_DP1", groups = {"regression" },  description = "Check food Issues GetConfig file regression use cases")
		public void foodIssuesGetConfig1(String restaurantId,int expectedStatusCode ,String expectedStatusMessage)throws IOException, ProcessingException
		{
			Processor processor = helper.foodIssuesGetConfig(restaurantId);
			Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), expectedStatusCode);
			Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), expectedStatusMessage);
		}

		//@Test(dataProvider = "foodIssuesVerifyBill_DP", groups = { "sanity", "smoke"},  description = "Check food Issues Verify Bill")
		public void foodIssuesVerifyBill(String inputType, String billProvided,String FIVB_statusMessage) throws Exception
		{
			String[] response=RMSCommonHelper.createOrder(RMSConstants.restaurantId, RMSConstants.consumerAppMobile, RMSConstants.consumerAppPassword, RMSConstants.PAYMENTMODE_CASH, RMSConstants.TEST_ORDER_COMMENTS);
			Thread.sleep(10000);
			String orderInfo = omsHellper.getOrderInfoFromOMS(response[0]);
			System.out.println(orderInfo);
			String updatedSource= JsonPath.read(orderInfo, "$.bill").toString();
			System.out.println(updatedSource);
			Processor processor = helper.foodIssuesVerifyBill(response[0],updatedSource, inputType, billProvided);
			Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.verificationStatus"));
			Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.issueType"));
		}


		@Test(dataProvider = "foodIssuesVerifyBill_DP1", groups = {"regression" },  description = "Check food Issues Verify Bill_DP1 regression use cases")
		public void foodIssuesVerifyBill1(String orderId, String inputValue, String inputType, String billProvided,int expectedStatusCode ,String expectedStatusMessage)throws IOException, ProcessingException
		{
			Processor processor = helper.foodIssuesVerifyBill(orderId, inputValue, inputType, billProvided);
			Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), expectedStatusCode);
			Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), expectedStatusMessage);
		}

		@Test(dataProvider = "foodIssuesSaveFinalBill_DP", groups = { "sanity", "smoke" },  description = "Check food Issues Save Final Bill")
		public void foodIssuesSaveFinalBill(String issueId, String orderId, String finalBillValue,String FISFB_statusMessage) throws IOException, ProcessingException
		{
			Processor processor = helper.foodIssuesSaveFinalBill(issueId, orderId, finalBillValue);
			//Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
			Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), FISFB_statusMessage);
			//Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
		}

		//@Test(dataProvider = "foodIssuesSaveFinalBill_DP", groups = { "ETE" },  description = "Food issues End To End flow")
		public void verifyFoodIssuesEtEFlow(String issueId, String orderId, String finalBillValue,String FISFB_statusMessage) throws Exception
		{
			String accessToken=helper.getLoginToken(RMSConstants.username_VI, RMSConstants.password1);
			String[] response=RMSCommonHelper.createOrder(RMSConstants.restaurantId, RMSConstants.consumerAppMobile, RMSConstants.consumerAppPassword, RMSConstants.PAYMENTMODE_CASH, RMSConstants.TEST_ORDER_COMMENTS);
			Thread.sleep(10000);
			OMSHelper omsHellper = new OMSHelper();
			String orderInfo = omsHellper.getOrderInfoFromOMS(response[0]);
			System.out.println(orderInfo);
			String FetchOrder=helper.fetchOrder(RMSConstants.restaurantId, accessToken).ResponseValidator.GetBodyAsText();
		    String updatedSource= JsonPath.read(FetchOrder, "$.bill").toString();
		    System.out.println(updatedSource);
		    String  foodIssuesVerifyBill = helper.foodIssuesVerifyBill(response[0],updatedSource, "BILL", "false").ResponseValidator.GetBodyAsText();
		    String IssueId= JsonPath.read(foodIssuesVerifyBill, "$.issueId").toString();
			Processor processor = helper.foodIssuesSaveFinalBill(IssueId, response[0], updatedSource);
			String resp = processor.ResponseValidator.GetBodyAsText();
			Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
			Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), FISFB_statusMessage);

		}

	    @Test(dataProvider = "foodIssuesSaveFinalBill_DP1", groups = {"regression" },  description = "food Issues Save Final Bill regression use cases")
		public void foodIssuesSaveFinalBill1(String issueId, String orderId, String finalBillValue,int expectedStatusCode ,String expectedStatusMessage)throws IOException, ProcessingException
		{
			Processor processor = helper.foodIssuesSaveFinalBill(issueId, orderId, finalBillValue);
			String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/save_bill");
			List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema, processor.ResponseValidator.GetBodyAsText());
			Assert.assertTrue(CollectionUtils.isEmpty(missingNodeList),missingNodeList + " Nodes Are Missing Or Not Matching For Get Config API");
			Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), expectedStatusCode);
			Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), expectedStatusMessage);
		}

		@Test(dataProvider = "foodIssuesRectifybill_DP", groups = { "sanity", "smoke" },  description = "Check food Issues Rectify bill")
		public void foodIssuesRectifybill(String issueId, String resolution, String FIRB_FI_statusMessage, ArrayList<String> expectedresolutionList)throws IOException, ProcessingException
		{
			Processor processor = helper.foodIssuesRectifybill(issueId, resolution);
			//Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
			Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), FIRB_FI_statusMessage);
			//Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
		}


		@Test(dataProvider = "foodIssuesRectifybill_DP1", groups = {"regression" },  description = "food Issues Rectify bill regression use cases")
		public void foodIssuesRectifybill1(String issueId,String resolution,int expectedStatusCode ,String expectedStatusMessage)throws IOException, ProcessingException
		{
			Processor processor = helper.foodIssuesRectifybill(issueId, resolution);
			Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), expectedStatusCode);
			Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), expectedStatusMessage);
		}

}
