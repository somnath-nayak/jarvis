package com.swiggy.api.erp.vms.editservice.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"item",
"oos_and_alternates"
})
public class Oos_datum {

@JsonProperty("item")
private Item item;
@JsonProperty("oos_and_alternates")
private Oos_and_alternates oos_and_alternates;

/**
* No args constructor for use in serialization
* 
*/
public Oos_datum() {
}

/**
* 
* @param item
* @param oos_and_alternates
*/
public Oos_datum(Item item, Oos_and_alternates oos_and_alternates) {
super();
this.item = item;
this.oos_and_alternates = oos_and_alternates;
}

@JsonProperty("item")
public Item getItem() {
return item;
}

@JsonProperty("item")
public void setItem(Item item) {
this.item = item;
}

public Oos_datum withItem(Item item) {
this.item = item;
return this;
}

@JsonProperty("oos_and_alternates")
public Oos_and_alternates getOos_and_alternates() {
return oos_and_alternates;
}

@JsonProperty("oos_and_alternates")
public void setOos_and_alternates(Oos_and_alternates oos_and_alternates) {
this.oos_and_alternates = oos_and_alternates;
}

public Oos_datum withOos_and_alternates(Oos_and_alternates oos_and_alternates) {
this.oos_and_alternates = oos_and_alternates;
return this;
}

@Override
public String toString() {
return new ToStringBuilder(this).append("item", item).append("oos_and_alternates", oos_and_alternates).toString();
}

@Override
public int hashCode() {
return new HashCodeBuilder().append(item).append(oos_and_alternates).toHashCode();
}

@Override
public boolean equals(Object other) {
if (other == this) {
return true;
}
if ((other instanceof Oos_datum) == false) {
return false;
}
Oos_datum rhs = ((Oos_datum) other);
return new EqualsBuilder().append(item, rhs.item).append(oos_and_alternates, rhs.oos_and_alternates).isEquals();
}

}