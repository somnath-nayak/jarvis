package com.swiggy.api.erp.cms.pojo.Addons;

import com.swiggy.api.erp.cms.constants.MenuConstants;
import com.swiggy.api.erp.cms.pojo.Items.Gst_details;

/**
 * Created by kiran.j on 2/8/18.
 */
public class Entity
{
    private String id;

    private double price;

    private int order;

    private boolean in_stock;

    private String item_id;

    private boolean is_veg;

    private String name;

    private String addon_group_id;

    private Gst_details gst_details;

    private boolean is_default;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public double getPrice ()
    {
        return price;
    }

    public void setPrice (double price)
    {
        this.price = price;
    }

    public int getOrder ()
    {
        return order;
    }

    public void setOrder (int order)
    {
        this.order = order;
    }

    public boolean getIn_stock ()
    {
        return in_stock;
    }

    public void setIn_stock (boolean in_stock)
    {
        this.in_stock = in_stock;
    }

    public String getItem_id ()
    {
        return item_id;
    }

    public void setItem_id (String item_id)
    {
        this.item_id = item_id;
    }

    public boolean getIs_veg ()
    {
        return is_veg;
    }

    public void setIs_veg (boolean is_veg)
    {
        this.is_veg = is_veg;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getAddon_group_id ()
    {
        return addon_group_id;
    }

    public void setAddon_group_id (String addon_group_id)
    {
        this.addon_group_id = addon_group_id;
    }

    public Gst_details getGst_details ()
    {
        return gst_details;
    }

    public void setGst_details (Gst_details gst_details)
    {
        this.gst_details = gst_details;
    }

    public boolean getIs_default ()
    {
        return is_default;
    }

    public void setIs_default (boolean is_default)
    {
        this.is_default = is_default;
    }

    public Entity build() {
        Gst_details gst_details = new Gst_details();
        if(this.getId() == null)
            this.setId(MenuConstants.variants_id);
        if(this.getAddon_group_id() == null)
            this.setAddon_group_id(MenuConstants.addon_group_id);
        if(this.getName() == null)
            this.setName(MenuConstants.variants_name);
        if(this.getItem_id() == null)
            this.setItem_id(MenuConstants.items_id);
        if(this.getPrice() == 0)
            this.setPrice(MenuConstants.default_price);

        this.setGst_details(gst_details);

        return this;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", price = "+price+", order = "+order+", in_stock = "+in_stock+", item_id = "+item_id+", is_veg = "+is_veg+", name = "+name+", addon_group_id = "+addon_group_id+", gst_details = "+gst_details+", is_default = "+is_default+"]";
    }
}
