package com.swiggy.api.erp.cms.pojo.CategoryServiceCreateCategory;

import com.swiggy.api.erp.cms.constants.CatalogV2Constants;

public class Params {
    private String $min;

    private String $max;

    public String get$min ()
    {
        return $min;
    }

    public void set$min (String $min)
    {
        this.$min = $min;
    }

    public String get$max ()
    {
        return $max;
    }

    public void set$max (String $max)
    {
        this.$max = $max;
    }

    public Params build(){
        setDefaultValues();
        return this;
    }

    public void setDefaultValues(){
        if(this.get$min()==null){
            this.set$min(CatalogV2Constants.min);
        }
        if(this.get$max()==null){
            this.set$max(CatalogV2Constants.max);
        }
    }

     @Override
    public String toString()
   {
       return "ClassPojo [$min = "+$min+", $max = "+$max+"]";
   }
}
