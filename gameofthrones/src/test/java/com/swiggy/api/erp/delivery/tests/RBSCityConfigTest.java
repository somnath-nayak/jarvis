package com.swiggy.api.erp.delivery.tests;

import com.swiggy.api.erp.delivery.helper.ClusterServiceHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryDataBaseUtils;
import com.swiggy.api.erp.delivery.helper.Serviceability_Helper;
import com.swiggy.api.erp.delivery.helper.SolrHelper;
import com.swiggy.automation.common.utils.DBUtil;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

/*PRE-REQUISITES :
In delivery_config :-
1) is_cluster_serviceability_enabled = 1
2) cluster_enabled_zone_ids = cityIds:ZoneIds / cityIds:-1 / zoneIds
 */

public class RBSCityConfigTest {

    DeliveryDataBaseUtils dbUtils = new DeliveryDataBaseUtils();
    SolrHelper solrHelper = new SolrHelper();
    Serviceability_Helper servHelper = new Serviceability_Helper();
    ClusterServiceHelper clusterHelper = new ClusterServiceHelper();
    static String metaKey = "cluster_enabled_zone_ids", isClusterServiceabilityEnabledKey = "is_cluster_serviceability_enabled", isClusterServiceabilityEnabledValue = "0", clusterEnableDesc = "Enables Cluster Service Flag";
    static String keyDescription = "cluster enabled city:zone id's";
    static String defaultValue = "1:-1";
    static Double lat = 12.9628837, lng = 77.6572272;
    static String customerZoneId, customerCityId, defaultRestId = "621", clusterEnabledFlag = "false";
    static String restAreaId, restCityId, restName, restLat, restLng;
    static String[] listingPayload, cartPayload, listingWithoutServiceabilityPayload;
    static String metaValue, clusterEnabledMetaValue;
    static int numRetries = 10, pollInt = 1000;
    static String listingItemCount = "1",  totalItemCount = "1";
    Map<String,String> mappedListingResponse, mappedCartResponse, mappedListingWithoutServResponse = new LinkedHashMap<>();

    @BeforeTest
    public void beforeTest()
    {
        if(!dbUtils.isMetaKeyPresentInDeliveryConfigDB(isClusterServiceabilityEnabledKey)){
            dbUtils.addKeyInDeliveryConfigDB(isClusterServiceabilityEnabledKey, isClusterServiceabilityEnabledValue, clusterEnableDesc);
        }
        else {
            clusterEnabledMetaValue = dbUtils.getValueOfMetaKeyFromDeliveryConfig(isClusterServiceabilityEnabledKey);
            if(clusterEnabledMetaValue.equals("0"))
                dbUtils.updateMetaValueInDeliveryConfig(isClusterServiceabilityEnabledKey, "1");
        }
        if(!dbUtils.isMetaKeyPresentInDeliveryConfigDB(metaKey)){
            dbUtils.addKeyInDeliveryConfigDB(metaKey, defaultValue, keyDescription);
            }
            else {
            metaValue = dbUtils.getValueOfMetaKeyFromDeliveryConfig(metaKey);
        }
        Map<String, String> userZoneSolrDoc = solrHelper.getCustomerZoneDetailsFromLatLon(lat, lng, "id", "cityId");
        customerZoneId = userZoneSolrDoc.get("id");
        customerCityId = userZoneSolrDoc.get("cityId");

        Map<String, String> restDetailsSolrDoc = solrHelper.getRestaurantDetailsFromLatLon(defaultRestId, "area_id", "city_id", "name", "place");
        restAreaId = restDetailsSolrDoc.get("area_id");
        restCityId = restDetailsSolrDoc.get("city_id");
        restName = restDetailsSolrDoc.get("name");
        restLat = restDetailsSolrDoc.get("place").split(",")[0];
        restLng = restDetailsSolrDoc.get("place").split(",")[1];

        listingPayload = new String[]{lat.toString(), lng.toString(), customerCityId, defaultRestId, clusterEnabledFlag};
        cartPayload = new String[]{listingItemCount, totalItemCount, defaultRestId, restAreaId, restCityId, restName, restLat, restLng, clusterEnabledFlag, String.valueOf(lat), String.valueOf(lng)};
        listingWithoutServiceabilityPayload = new String[]{String.valueOf(lat), String.valueOf(lng), customerCityId, defaultRestId, clusterEnabledFlag};
    }



    @AfterTest
    public void afterTest(){
        if(metaValue != null){
            System.out.println("************** meta-value : " + metaValue);
            dbUtils.updateMetaValueInDeliveryConfig(metaKey, metaValue);
            dbUtils.updateMetaValueInDeliveryConfig(isClusterServiceabilityEnabledKey, clusterEnabledMetaValue);
        }
    }




    @Test(groups = "rashmi", description = "RBS_CC_01 :- (Value syntax = cityIds:zoneIds) customer is lying in both, cityId & zoneId list in delivery_config (key : is_cluster_serviceability_enabled), response will be from cluster-service")
    public void cityId_ZoneIdBothPresent()
    {
        if(metaValue.contains(":"))
        {
            String[] splitValue = metaValue.split(":");

                splitValue[0] = dbUtils.IsMetaKeyContainsCityOrZoneId(splitValue[1], customerCityId) ? splitValue[0]: splitValue[0].concat(customerCityId);
                splitValue[1] = dbUtils.IsMetaKeyContainsCityOrZoneId(splitValue[1], customerZoneId) ? splitValue[1]: splitValue[1].concat(customerZoneId);
                String value = splitValue[0].concat(splitValue[1]);
                dbUtils.updateMetaValueInDeliveryConfig(metaKey, value);
        }
        else
            {
                String keyMetaValue = customerCityId + ":" + customerZoneId;
                dbUtils.updateMetaValueInDeliveryConfig(metaKey, keyMetaValue);
            }

            clusterHelper.deleteClusterCache();
        mappedListingResponse = servHelper.retryCerebroListingViaCluster(numRetries, pollInt, listingPayload, new LinkedHashMap<String, String>() {{
            put("distance_calculation_method", "HAVERSINE");      //response from cluster service..
        }});

        mappedCartResponse = servHelper.retryCerebroCartViaCluster(numRetries, pollInt, cartPayload, new LinkedHashMap<String, String>() {{
            put("distance_method", "HAVERSINE");      //response from cluster service..
        }});

        mappedListingWithoutServResponse = servHelper.retryCerebroListingWithoutServCheckViaCluster(numRetries, pollInt, listingWithoutServiceabilityPayload, new LinkedHashMap<String, String>() {{
            put("distance_calculation_method", "HAVERSINE");      //response from cluster service..
        }});

        Assert.assertEquals(mappedListingResponse.get("distance_calculation_method"),"HAVERSINE", "distance_calculation_method");
        Assert.assertEquals(mappedCartResponse.get("distance_method"),"HAVERSINE", "distance_calculation_method");
        Assert.assertEquals(mappedListingWithoutServResponse.get("distance_calculation_method"),"HAVERSINE", "distance_calculation_method");
    }


    @Test(groups = "rashmi", description = "RBS_CC_02 :- (Value syntax = cityIds:zoneIds) customer lies in cityId list, but not in zoneId list in delivery_config, response will be from cluster-service")
    public void cityIdPresentZoneIdMissing()
    {
        String newMetaZoneValue = String.valueOf(Integer.parseInt(customerZoneId) + 1), newMetaCityValue = customerCityId;
       /* if(metaValue.contains(":"))
        {
            String[] splitValue = metaValue.split(":");

            splitValue[0] = dbUtils.IsMetaKeyContainsCityOrZoneId(splitValue[1], customerCityId) ? splitValue[0]: splitValue[0].concat(customerCityId);

            if(splitValue[1].contains(customerZoneId))
            {
                String[] newValue = splitValue[1].split( customerZoneId);
                String s1 = newValue[0].trim();
                String s2 = newValue[1].trim();
                String zoneIdList1 = s1.substring(0, s1.length());
                String zoneIdList2 = s2.substring(1, s2.length());
                newMetaZoneValue = splitValue[0] + ":" + zoneIdList1 + zoneIdList2;
                System.out.println("******************* VALUE AFTER REMOVING ********* " + newMetaZoneValue);
                dbUtils.updateMetaValueInDeliveryConfig(metaKey, newMetaZoneValue);
            }
        }
        else
        {*/
            String keyMetaValue = newMetaCityValue + ":" + newMetaZoneValue;
            dbUtils.updateMetaValueInDeliveryConfig(metaKey, keyMetaValue);
        //}

        clusterHelper.deleteClusterCache();
        mappedListingResponse = servHelper.retryCerebroListingViaCluster(numRetries, pollInt, listingPayload, new LinkedHashMap<String, String>() {{
            put("distance_calculation_method", "HAVERSINE");      //response from cluster service..
        }});

        mappedCartResponse = servHelper.retryCerebroCartViaCluster(numRetries, pollInt, cartPayload, new LinkedHashMap<String, String>() {{
            put("distance_method", "HAVERSINE");      //response from cluster service..
        }});

        mappedListingWithoutServResponse = servHelper.retryCerebroListingWithoutServCheckViaCluster(numRetries, pollInt, listingWithoutServiceabilityPayload, new LinkedHashMap<String, String>() {{
            put("distance_calculation_method", "HAVERSINE");      //response from cluster service..
        }});

        Assert.assertEquals(mappedListingResponse.get("distance_calculation_method"),"HAVERSINE", "distance_calculation_method");
        Assert.assertEquals(mappedCartResponse.get("distance_method"),"HAVERSINE", "distance_calculation_method");
        Assert.assertEquals(mappedListingWithoutServResponse.get("distance_calculation_method"),"HAVERSINE", "distance_calculation_method");
       }

    @Test(groups = "rashmi", description = "RBS_CC_03 :- (Value syntax = cityIds:zoneIds) customer does not lie in both, cityId & zoneId list in delivery_config, response will be from cerebro")
    public void cityIdMissingZoneIdMissing()
    {
        String newZoneMetaValue = (String.valueOf(customerZoneId) + 1), newCityMetaValue = String.valueOf(Integer.parseInt(customerCityId) + 1);

            String keyMetaValue = newCityMetaValue + ":" + newZoneMetaValue;
            dbUtils.updateMetaValueInDeliveryConfig(metaKey, keyMetaValue);


        clusterHelper.deleteClusterCache();
        mappedListingResponse = servHelper.retryCerebroListingViaCluster(numRetries, pollInt, listingPayload, new LinkedHashMap<String, String>() {{
            put("thirty_mins_or_free", "true");      //response from cerebro..
        }});

        mappedCartResponse = servHelper.retryCerebroCartViaCluster(numRetries, pollInt, cartPayload, new LinkedHashMap<String, String>() {{
            put("thirty_mins_or_free", "true");      //response from cerebro..
        }});

        mappedListingWithoutServResponse = servHelper.retryCerebroListingWithoutServCheckViaCluster(numRetries, pollInt, listingWithoutServiceabilityPayload, new LinkedHashMap<String, String>() {{
            put("thirty_mins_or_free", "true");      //response from cerebro..
        }});

        try{Assert.assertEquals(mappedListingResponse.get("thirty_mins_or_free"),"true", "thirty_mins_or_free");}
        catch (java.lang.AssertionError e){
            Assert.assertEquals(mappedListingResponse.get("thirty_mins_or_free"),"false", "thirty_mins_or_free");
        }
        try{Assert.assertEquals(mappedCartResponse.get("thirty_mins_or_free"),"true", "thirty_mins_or_free");}
        catch (java.lang.AssertionError e){
            Assert.assertEquals(mappedCartResponse.get("thirty_mins_or_free"),"false", "thirty_mins_or_free");
        }
        try{Assert.assertEquals(mappedListingWithoutServResponse.get("thirty_mins_or_free"),"true", "thirty_mins_or_free");}
        catch (java.lang.AssertionError e){
            Assert.assertEquals(mappedListingWithoutServResponse.get("thirty_mins_or_free"),"false", "thirty_mins_or_free");
        }
    }


    @Test(groups = "rashmi", description = "RBS_CC_04 :- (Value syntax = cityIds:zoneIds) customer lies in given cityId list, but does not lie in zoneId list, response will be from cluster-service")
    public void cityIdMissingZoneIdPresent()
    {
        String newZoneMetaValue = customerZoneId;

        String keyMetaValue = newZoneMetaValue;
        dbUtils.updateMetaValueInDeliveryConfig(metaKey, keyMetaValue);

        clusterHelper.deleteClusterCache();
        mappedListingResponse = servHelper.retryCerebroListingViaCluster(numRetries, pollInt, listingPayload, new LinkedHashMap<String, String>() {{
            put("thirty_mins_or_free", "true");      //response from cluster service..
        }});

        mappedCartResponse = servHelper.retryCerebroCartViaCluster(numRetries, pollInt, cartPayload, new LinkedHashMap<String, String>() {{
            put("thirty_mins_or_free", "true");      //response from cluster service..
        }});

        mappedListingWithoutServResponse = servHelper.retryCerebroListingWithoutServCheckViaCluster(numRetries, pollInt, listingWithoutServiceabilityPayload, new LinkedHashMap<String, String>() {{
            put("thirty_mins_or_free", "true");      //response from cluster service..
        }});

        Assert.assertEquals(mappedListingResponse.get("distance_calculation_method"),"HAVERSINE", "distance_calculation_method");
        Assert.assertEquals(mappedCartResponse.get("distance_method"),"HAVERSINE", "distance_calculation_method");
        Assert.assertEquals(mappedListingWithoutServResponse.get("distance_calculation_method"),"HAVERSINE", "distance_calculation_method");
    }

    @Test(groups = "rashmi", description = "RBS_CC_05 :- (Value syntax = cityIds:-1) customer lies in given cityId list, whereas zoneId list is having value -1, response will be from cluster-service")
    public void cityIdPresent_ZoneIdMissing()
    {
        String newZoneMetaValue = "-1", newCityMetaValue = customerCityId;

        String keyMetaValue = newCityMetaValue + ":" + newZoneMetaValue;
        dbUtils.updateMetaValueInDeliveryConfig(metaKey, keyMetaValue);

        clusterHelper.deleteClusterCache();
        mappedListingResponse = servHelper.retryCerebroListingViaCluster(numRetries, pollInt, listingPayload, new LinkedHashMap<String, String>() {{
            put("distance_calculation_method", "HAVERSINE");      //response from cluster service..
        }});

        mappedCartResponse = servHelper.retryCerebroCartViaCluster(numRetries, pollInt, cartPayload, new LinkedHashMap<String, String>() {{
            put("distance_method", "HAVERSINE");      //response from cluster service..
        }});

        mappedListingWithoutServResponse = servHelper.retryCerebroListingWithoutServCheckViaCluster(numRetries, pollInt, listingWithoutServiceabilityPayload, new LinkedHashMap<String, String>() {{
            put("distance_calculation_method", "HAVERSINE");      //response from cluster service..
        }});

        Assert.assertEquals(mappedListingResponse.get("distance_calculation_method"),"HAVERSINE", "distance_calculation_method");
        Assert.assertEquals(mappedCartResponse.get("distance_method"),"HAVERSINE", "distance_calculation_method");
        Assert.assertEquals(mappedListingWithoutServResponse.get("distance_calculation_method"),"HAVERSINE", "distance_calculation_method");
    }

    @Test(groups = "rashmi", description = "RBS_CC_06 :- (Value syntax = cityIds:-1) customer does not lie in given cityId list, whereas zoneId list is having value -1, response will be from cerebro")
    public void cityIdMissing_ZoneIdMissing()
    {
        String newZoneMetaValue = "-1", newCityMetaValue = String.valueOf(Integer.parseInt(customerCityId) + 1);

        String keyMetaValue = newCityMetaValue + ":" + newZoneMetaValue;
        dbUtils.updateMetaValueInDeliveryConfig(metaKey, keyMetaValue);

        clusterHelper.deleteClusterCache();
        mappedListingResponse = servHelper.retryCerebroListingViaCluster(numRetries, pollInt, listingPayload, new LinkedHashMap<String, String>() {{
            put("thirty_mins_or_free", "true");      //response from cerebro..
        }});

        mappedCartResponse = servHelper.retryCerebroCartViaCluster(numRetries, pollInt, cartPayload, new LinkedHashMap<String, String>() {{
            put("thirty_mins_or_free", "true");      //response from cerebro..
        }});

        mappedListingWithoutServResponse = servHelper.retryCerebroListingWithoutServCheckViaCluster(numRetries, pollInt, listingWithoutServiceabilityPayload, new LinkedHashMap<String, String>() {{
            put("thirty_mins_or_free", "true");      //response from cerebro..
        }});

        try{Assert.assertEquals(mappedListingResponse.get("thirty_mins_or_free"),"true", "thirty_mins_or_free");}
        catch (java.lang.AssertionError e){
            Assert.assertEquals(mappedListingResponse.get("thirty_mins_or_free"),"false", "thirty_mins_or_free");
        }
        try{Assert.assertEquals(mappedCartResponse.get("thirty_mins_or_free"),"true", "thirty_mins_or_free");}
        catch (java.lang.AssertionError e){
            Assert.assertEquals(mappedCartResponse.get("thirty_mins_or_free"),"false", "thirty_mins_or_free");
        }
        try{Assert.assertEquals(mappedListingWithoutServResponse.get("thirty_mins_or_free"),"true", "thirty_mins_or_free");}
        catch (java.lang.AssertionError e){
            Assert.assertEquals(mappedListingWithoutServResponse.get("thirty_mins_or_free"),"false", "thirty_mins_or_free");
        }
    }

    @Test(groups = "rashmi", description = "RBS_CC_07 :- (Value syntax = zoneIds) Only zoneId list is given & customer's zone is present in that list, response will be from cluster-service")
    public void OnlyZoneIds_CustZoneIdPresent()
    {
        String newZoneMetaValue = customerZoneId;

        String keyMetaValue = newZoneMetaValue;
        dbUtils.updateMetaValueInDeliveryConfig(metaKey, keyMetaValue);

        clusterHelper.deleteClusterCache();
        mappedListingResponse = servHelper.retryCerebroListingViaCluster(numRetries, pollInt, listingPayload, new LinkedHashMap<String, String>() {{
            put("distance_calculation_method", "HAVERSINE");      //response from cluster service..
        }});

        mappedCartResponse = servHelper.retryCerebroCartViaCluster(numRetries, pollInt, cartPayload, new LinkedHashMap<String, String>() {{
            put("distance_method", "HAVERSINE");      //response from cluster service..
        }});

        mappedListingWithoutServResponse = servHelper.retryCerebroListingWithoutServCheckViaCluster(numRetries, pollInt, listingWithoutServiceabilityPayload, new LinkedHashMap<String, String>() {{
            put("distance_calculation_method", "HAVERSINE");      //response from cluster service..
        }});

        Assert.assertEquals(mappedListingResponse.get("distance_calculation_method"),"HAVERSINE", "distance_calculation_method");
        Assert.assertEquals(mappedCartResponse.get("distance_method"),"HAVERSINE", "distance_calculation_method");
        Assert.assertEquals(mappedListingWithoutServResponse.get("distance_calculation_method"),"HAVERSINE", "distance_calculation_method");
    }

    @Test(groups = "rashmi", description = "RBS_CC_08 :- (Value syntax = zoneIds) Only zoneId list is given & customer's zone is not present in that list, response will be from cerebro")
    public void OnlyZoneIdList_ZoneIdMissing()
    {
        String newZoneMetaValue = String.valueOf(Integer.parseInt(customerZoneId) + 1);

        String keyMetaValue = newZoneMetaValue;
        dbUtils.updateMetaValueInDeliveryConfig(metaKey, keyMetaValue);

        clusterHelper.deleteClusterCache();
        mappedListingResponse = servHelper.retryCerebroListingViaCluster(numRetries, pollInt, listingPayload, new LinkedHashMap<String, String>() {{
            put("thirty_mins_or_free", "true");      //response from cerebro..
        }});

        mappedCartResponse = servHelper.retryCerebroCartViaCluster(numRetries, pollInt, cartPayload, new LinkedHashMap<String, String>() {{
            put("thirty_mins_or_free", "true");      //response from cerebro..
        }});

        mappedListingWithoutServResponse = servHelper.retryCerebroListingWithoutServCheckViaCluster(numRetries, pollInt, listingWithoutServiceabilityPayload, new LinkedHashMap<String, String>() {{
            put("thirty_mins_or_free", "true");      //response from cerebro..
        }});

        try{Assert.assertEquals(mappedListingResponse.get("thirty_mins_or_free"),"true", "thirty_mins_or_free");}
        catch (java.lang.AssertionError e){
            Assert.assertEquals(mappedListingResponse.get("thirty_mins_or_free"),"false", "thirty_mins_or_free");
        }
        try{Assert.assertEquals(mappedCartResponse.get("thirty_mins_or_free"),"true", "thirty_mins_or_free");}
        catch (java.lang.AssertionError e){
            Assert.assertEquals(mappedCartResponse.get("thirty_mins_or_free"),"false", "thirty_mins_or_free");
        }
        try{Assert.assertEquals(mappedListingWithoutServResponse.get("thirty_mins_or_free"),"true", "thirty_mins_or_free");}
        catch (java.lang.AssertionError e){
            Assert.assertEquals(mappedListingWithoutServResponse.get("thirty_mins_or_free"),"false", "thirty_mins_or_free");
        }
    }

    @Test(groups = "rashmi", description = "RBS_CC_09 :- (Value syntax = -1)'cluster_enabled_zone_ids' is having value as -1, response will be from cerebro")
    public void OnlyMinusMetaValue()
    {
        String newMetaValue = "-1";

        String keyMetaValue = newMetaValue;
        dbUtils.updateMetaValueInDeliveryConfig(metaKey, keyMetaValue);

        clusterHelper.deleteClusterCache();
        mappedListingResponse = servHelper.retryCerebroListingViaCluster(numRetries, pollInt, listingPayload, new LinkedHashMap<String, String>() {{
            put("thirty_mins_or_free", "true");      //response from cerebro..
        }});

        mappedCartResponse = servHelper.retryCerebroCartViaCluster(numRetries, pollInt, cartPayload, new LinkedHashMap<String, String>() {{
            put("thirty_mins_or_free", "true");      //response from cerebro..
        }});

        mappedListingWithoutServResponse = servHelper.retryCerebroListingWithoutServCheckViaCluster(numRetries, pollInt, listingWithoutServiceabilityPayload, new LinkedHashMap<String, String>() {{
            put("thirty_mins_or_free", "true");      //response from cerebro..
        }});

        try{Assert.assertEquals(mappedListingResponse.get("thirty_mins_or_free"),"true", "thirty_mins_or_free");}
        catch (java.lang.AssertionError e){
            Assert.assertEquals(mappedListingResponse.get("thirty_mins_or_free"),"false", "thirty_mins_or_free");
        }
        try{Assert.assertEquals(mappedCartResponse.get("thirty_mins_or_free"),"true", "thirty_mins_or_free");}
        catch (java.lang.AssertionError e){
            Assert.assertEquals(mappedCartResponse.get("thirty_mins_or_free"),"false", "thirty_mins_or_free");
        }
        try{Assert.assertEquals(mappedListingWithoutServResponse.get("thirty_mins_or_free"),"true", "thirty_mins_or_free");}
        catch (java.lang.AssertionError e){
            Assert.assertEquals(mappedListingWithoutServResponse.get("thirty_mins_or_free"),"false", "thirty_mins_or_free");
        }
    }
}