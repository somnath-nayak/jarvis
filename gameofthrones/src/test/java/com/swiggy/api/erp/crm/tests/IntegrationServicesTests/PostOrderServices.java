package com.swiggy.api.erp.crm.tests.pos;

import com.swiggy.api.erp.delivery.helper.DeliveryDataHelper;
import com.swiggy.api.sf.snd.helper.SnDHelper;
import com.swiggy.api.erp.crm.dp.pos.CreateDataPos;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.HashMap;

public class PostOrderServices {

        Initialize gameofthrones = new Initialize();
        String layerConversationId ;

        String mobile = "7507220659";
        String password = "Test@2211";

        Processor loginResponse = SnDHelper.consumerLogin(mobile, password);
        String tid = loginResponse.ResponseValidator.GetNodeValue("tid");
        String token = loginResponse.ResponseValidator.GetNodeValue("data.token");

        Processor loginResponse01 = SnDHelper.consumerLogin(mobile, password);
        String tid02 = loginResponse01.ResponseValidator.GetNodeValue("tid");
        String token02 = loginResponse01.ResponseValidator.GetNodeValue("data.token");

        public HashMap<String, String> request_headers(String tid, String token) {
            HashMap<String, String> requestheaders = new HashMap<String, String>();
            requestheaders.put("Content-Type", "application/json");
            requestheaders.put("tid", tid);
            requestheaders.put("token", token);

            return requestheaders;
        }

        public String[] payload(String[] userdata){
            String payloadparam[] = new String[3];
            payloadparam[0] = userdata[0];
            payloadparam[1] = userdata[1];
            payloadparam[2] = userdata[2];

            return payloadparam;
        }

        public void verifyResults(Processor response){
            System.out.println("Response" + response);
            int http_status = response.ResponseValidator.GetResponseCode();
            System.out.println("stats code is:" + http_status);

            Assert.assertEquals(http_status, 200, "Actual http_status doesn't match with expected");
            Assert.assertEquals(response.ResponseValidator.GetNodeValue("status"), "Success",
                    "Actual status doesn't match with expected.");
            Assert.assertEquals(response.ResponseValidator.GetNodeValue("tid"), tid,
                    "Actual tid doesn't match with expected.");
        }

        public void verifyResponseNegativeTC(Processor response){
            System.out.println("Response" + response);
            int http_status = response.ResponseValidator.GetResponseCode();
            System.out.println("stats code is:" + http_status);

            Assert.assertEquals(http_status, 500, "Actual http_status doesn't match with expected.");
        }

        @Test(dataProviderClass = CreateDataPos.class, dataProvider = "issue data",enabled = true,
            groups = {"positive"}, description = "Create the conversation")
        public void conversationCreate(HashMap<Integer,String[]> data) {

            for(int i=0;i<data.size();i++) {
                String[] userdata = data.get(i);
                HashMap<String, String> requestheaders_create_conversation = request_headers(tid, token);
                String payloadparam[] = payload(userdata);
                System.out.println("Payload param : "+ Arrays.asList(payloadparam));

                GameOfThronesService create_conversation = new GameOfThronesService("pos", "createconversation", gameofthrones);
                Processor conversation_response = new Processor(create_conversation, requestheaders_create_conversation, payloadparam);

                layerConversationId = conversation_response.ResponseValidator.GetNodeValue("data.layerConversationId");
                System.out.println("layerConversationId "+layerConversationId);

                verifyResults(conversation_response);
            }
        }

        @Test(dataProviderClass = CreateDataPos.class, dataProvider = "issue data",enabled = true,
                groups = {"negative"}, description = "Create the conversation")
        public void conversationCreateInvalidData(HashMap<Integer,String[]> data) {
            for(int i=0;i<data.size();i++) {
                String[] userdata = data.get(i);

                HashMap<String, String> requestheaders_create_conversation = new HashMap<String, String>();
                requestheaders_create_conversation = request_headers(tid, token02);

                String payloadparam[] = payload(userdata);
                System.out.println("Payload param : "+ Arrays.asList(payloadparam));
                GameOfThronesService create_conversation = new GameOfThronesService("pos", "createconversation", gameofthrones);
                Processor conversation_response = new Processor(create_conversation, requestheaders_create_conversation, payloadparam);

                verifyResponseNegativeTC(conversation_response);
            }
        }

        @Test(groups = {"positive"}, description = "List the conversations")
        public void listConversation() {
            HashMap<String, String> requestheaders_list_conversation = new HashMap<String, String>();
            requestheaders_list_conversation = request_headers(tid, token);

            GameOfThronesService list_conversation = new GameOfThronesService("pos", "conversationlisting", gameofthrones);
            Processor list_conversation_response = new Processor(list_conversation, requestheaders_list_conversation);

            verifyResults(list_conversation_response);
        }

        @Test(groups = {"negative"}, description = "List the conversations")
        public void listConversationNegative() {
            HashMap<String, String> requestheaders_list_conversation = new HashMap<String, String>();
            requestheaders_list_conversation = request_headers(tid, token02);

            GameOfThronesService list_conversation = new GameOfThronesService("pos", "conversationlisting", gameofthrones);
            Processor list_conversation_response = new Processor(list_conversation, requestheaders_list_conversation); // need to pass query param

            verifyResponseNegativeTC(list_conversation_response);
        }

        @Test(dataProviderClass = CreateDataPos.class, dataProvider = "issue data",enabled = true,
                groups = {"positive"}, description = "Reinitiate the chat")
        public void reinitiateChat(HashMap<Integer,String[]> data) {
            for( int i = 0; i<data.size(); i++) {
                String queryparam[] = new String[3];
                String[] userdata = data.get(i);
                HashMap<Integer, String[]> data1 = new HashMap<Integer, String[]>();
                data1.put(0, userdata);
                conversationCreate(data1);

                queryparam[0] = userdata[1];
                queryparam[1] = userdata[0];
                queryparam[2] = layerConversationId;

                HashMap<String, String> requestheaders_reiniate_chat = new HashMap<String, String>();
                requestheaders_reiniate_chat = request_headers(tid, token);

                GameOfThronesService reiniate_chat = new GameOfThronesService("pos", "chatreinitiate", gameofthrones);
                Processor reiniate_chat_response = new Processor(reiniate_chat, requestheaders_reiniate_chat, null, queryparam);

                verifyResults(reiniate_chat_response);
            }
        }

        @Test(dataProviderClass = CreateDataPos.class, dataProvider = "issue data",enabled = true,
                groups = {"negative"}, description = "Reinitiate the chat")
        public void reinitiateChatInvalidData(HashMap<Integer,String[]> data) {
            for( int i = 0; i<data.size(); i++) {
                String queryparam[] = new String[3];
                String[] userdata = data.get(i);
                HashMap<Integer, String[]> data1 = new HashMap<Integer, String[]>();
                data1.put(0, userdata);
                conversationCreate(data1);

                queryparam[0] = userdata[1];
                queryparam[1] = userdata[0];
                queryparam[2] = layerConversationId;

                HashMap<String, String> requestheaders_reiniate_chat = new HashMap<String, String>();
                requestheaders_reiniate_chat = request_headers(tid, token02);

                GameOfThronesService reiniate_chat = new GameOfThronesService("pos", "chatreinitiate", gameofthrones);
                Processor reiniate_chat_response = new Processor(reiniate_chat, requestheaders_reiniate_chat, null, queryparam);

                verifyResponseNegativeTC(reiniate_chat_response);
            }
        }

        @Test(dataProviderClass = CreateDataPos.class, dataProvider = "action data",enabled = true,
                groups = {"positive"}, description = "Conversation action")
        public void conversationAction(HashMap<String,String[]> issuedata, HashMap<String,String[]> action) {
            String [] actiondata = action.get("action");
            for( int i = 0; i<issuedata.size(); i++) {
                for(int j = 0; j<actiondata.length; j++) {
                    String queryparam[] = new String[3];
                    String[] userdata = issuedata.get("conversation0"+i);
                    HashMap<Integer, String[]> data1 = new HashMap<Integer, String[]>();
                    data1.put(0, userdata);
                    conversationCreate(data1);

                    queryparam[0] = layerConversationId;
                    queryparam[1] = actiondata[j];

                    String payloadparam[] = payload(userdata);
                    System.out.println("Payload param : "+ Arrays.asList(payloadparam));

                    HashMap<String, String> requestheaders_conversation_action = new HashMap<String, String>();
                    requestheaders_conversation_action = request_headers(tid, token);

                    GameOfThronesService conversation_action = new GameOfThronesService("pos", "conversationaction", gameofthrones);
                    Processor conversation_action_response = new Processor(conversation_action, requestheaders_conversation_action, payloadparam, queryparam);

                    verifyResults(conversation_action_response);
                }
            }
        }

        @Test(dataProviderClass = CreateDataPos.class, dataProvider = "action data",enabled = true,
                groups = {"negative"}, description = "Conversation action")
        public void conversationActionInvalidData(HashMap<String,String[]> issuedata, HashMap<String,String[]> action) {
            String [] actiondata = action.get("action");
            for( int i = 0; i<issuedata.size(); i++) {
                for(int j = 0; j<actiondata.length; j++) {
                    String queryparam[] = new String[3];
                    String[] userdata = issuedata.get("conversation0"+i);
                    HashMap<Integer, String[]> data1 = new HashMap<Integer, String[]>();
                    data1.put(0, userdata);
                    conversationCreate(data1);

                    queryparam[0] = layerConversationId;
                    queryparam[1] = actiondata[j];

                    String payloadparam[] = payload(userdata);
                    System.out.println("Payload param : "+ Arrays.asList(payloadparam));
                    HashMap<String, String> requestheaders_conversation_action = new HashMap<String, String>();
                    requestheaders_conversation_action = request_headers(tid, token02);

                    GameOfThronesService conversation_action = new GameOfThronesService("pos", "conversationaction", gameofthrones);
                    Processor conversation_action_response = new Processor(conversation_action, requestheaders_conversation_action, payloadparam, queryparam);

                    verifyResponseNegativeTC(conversation_action_response);
                }
            }
        }

        @Test(groups = {"positive"}, description = "Help Authenticate")
        public void helpAuthenticate() {
            HashMap<String, String> requestheaders_help_authenticate = new HashMap<String, String>();
            requestheaders_help_authenticate = request_headers(tid, token);

            GameOfThronesService help_authenticate = new GameOfThronesService("pos", "helpauthenticate", gameofthrones);
            Processor help_authenticate_response = new Processor(help_authenticate, requestheaders_help_authenticate,null);

            verifyResults(help_authenticate_response);
        }

        @Test(groups = {"negative"}, description = "Help Authenticate")
        public void helpAuthenticateInvalidData() {
            HashMap<String, String> requestheaders_help_authenticate = new HashMap<String, String>();
            requestheaders_help_authenticate = request_headers(tid, token02);

            GameOfThronesService help_authenticate = new GameOfThronesService("pos", "helpauthenticate", gameofthrones);
            Processor help_authenticate_response = new Processor(help_authenticate, requestheaders_help_authenticate,null);

            verifyResponseNegativeTC(help_authenticate_response);
        }

        @Test(groups = {"positive"}, description = "Help and Suport")
        public void helpandsupport() {
            HashMap<String, String> requestheaders_help_and_support = new HashMap<String, String>();
            requestheaders_help_and_support = request_headers(tid, token);

            GameOfThronesService help_and_support = new GameOfThronesService("pos", "helpandsupport", gameofthrones);
            Processor help_and_support_response = new Processor(help_and_support, requestheaders_help_and_support);

            verifyResults(help_and_support_response);
        }

        @Test(groups = {"negative"}, description = "Help and Suport")
        public void helpandsupportInvalidData() {
            HashMap<String, String> requestheaders_help_and_support = new HashMap<String, String>();
            requestheaders_help_and_support = request_headers(tid, token02);

            GameOfThronesService help_and_support = new GameOfThronesService("pos", "helpandsupport", gameofthrones);
            Processor help_and_support_response = new Processor(help_and_support, requestheaders_help_and_support);

            verifyResponseNegativeTC(help_and_support_response);
        }

        @Test(dataProviderClass = CreateDataPos.class, dataProvider = "action data",enabled = true,
                groups = {"positive"}, description = "Conversation action")
        public void getConversationAction(HashMap<String,String[]> issuedata, HashMap<String,String[]> action) {
            String [] actiondata = action.get("action");
            for( int i = 0; i<issuedata.size(); i++) {
                for(int j = 0; j<actiondata.length; j++) {
                    String queryparam[] = new String[3];
                    String[] userdata = issuedata.get("conversation0"+i);
                    HashMap<Integer, String[]> data1 = new HashMap<Integer, String[]>();
                    data1.put(0, userdata);
                    conversationCreate(data1);

                    queryparam[0] = layerConversationId;
                    queryparam[1] = actiondata[j];

                    HashMap<String, String> requestheaders_conversation_action = new HashMap<String, String>();
                    requestheaders_conversation_action = request_headers(tid, token);

                    GameOfThronesService get_conversation_action = new GameOfThronesService("pos", "conversationactionproxy", gameofthrones);
                    Processor get_conversation_action_response = new Processor(get_conversation_action, requestheaders_conversation_action, null, queryparam);

                    verifyResults(get_conversation_action_response);
                }
            }
        }

        @Test(dataProviderClass = CreateDataPos.class, dataProvider = "action data",enabled = true,
                groups = {"positive"}, description = "Conversation action")
        public void getConversation(HashMap<String,String[]> issuedata, HashMap<String,String[]> action) {
            String [] actiondata = action.get("action");
            for( int i = 0; i<issuedata.size(); i++) {
                for(int j = 0; j<actiondata.length; j++) {
                    String queryparam[] = new String[3];
                    String[] userdata = issuedata.get("conversation0"+i);
                    HashMap<Integer, String[]> data1 = new HashMap<Integer, String[]>();
                    data1.put(0, userdata);
                    conversationCreate(data1);

                    queryparam[0] = layerConversationId;


                    HashMap<String, String> requestheaders_conversation_action = new HashMap<String, String>();
                    requestheaders_conversation_action = request_headers(tid, token);

                    GameOfThronesService get_conversation = new GameOfThronesService("pos", "getconversation", gameofthrones);
                    Processor get_conversation_response = new Processor(get_conversation, requestheaders_conversation_action,null,  queryparam);

                    verifyResults(get_conversation_response);
                }
            }
        }

        @Test(dataProviderClass = CreateDataPos.class, dataProvider = "action data",enabled = true,
                groups = {"positive"}, description = "Conversation action")
        public void getUser(HashMap<String,String[]> issuedata, HashMap<String,String[]> action) {
            String [] actiondata = action.get("action");
            for( int i = 0; i<issuedata.size(); i++) {
                for(int j = 0; j<actiondata.length; j++) {
                    String queryparam[] = new String[3];
                    String[] userdata = issuedata.get("conversation0"+i);
                    HashMap<Integer, String[]> data1 = new HashMap<Integer, String[]>();
                    data1.put(0, userdata);
                    conversationCreate(data1);

                    queryparam[0] = layerConversationId;

                    HashMap<String, String> requestheaders_conversation_action = new HashMap<String, String>();
                    requestheaders_conversation_action = request_headers(tid, token);

                    GameOfThronesService get_user = new GameOfThronesService("pos", "getuser", gameofthrones);
                    Processor get_user_response = new Processor(get_user, requestheaders_conversation_action, null, queryparam);

                    verifyResults(get_user_response);
                }
            }
        }


}
