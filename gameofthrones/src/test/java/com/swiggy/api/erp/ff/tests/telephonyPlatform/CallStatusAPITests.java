package com.swiggy.api.erp.ff.tests.telephonyPlatform;

import com.swiggy.api.erp.ff.dp.telephonyPlatform.TelephonyPlatformTestData;
import com.swiggy.api.erp.ff.helper.TelephonyPlatformHelper;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.WireMockHelper;
import org.testng.annotations.*;

import java.io.IOException;
import java.util.HashMap;

public class CallStatusAPITests extends TelephonyPlatformTestData {

    TelephonyPlatformHelper helper = new TelephonyPlatformHelper();
    WireMockHelper wireMockHelper = new WireMockHelper();

    @BeforeClass
    public void startMockServer() {
//        wireMockHelper.startMockServer(6666);
    }

    @AfterClass
    public void stopMockServer() throws InterruptedException {
//        wireMockHelper.stopMockServer();
//        Thread.sleep(1000);
    }

    @Test(dataProvider = "callStatusData", description = "Test Telephony Platform call status API")
    public void callStatusAPITest(String callId, String clientId, HashMap<String, String> expectedResponseMap, String scenario, String stubResponse) throws IOException, InterruptedException {
        System.out.println("***************************************** callStatusAPITest with "+scenario+" started *****************************************");

        helper.stubExotelCallStatusAPIResponse(stubResponse);
        Processor response = helper.getCallStatus(clientId, callId);
        helper.assertCallStatusAPIResponses(response, expectedResponseMap);
        System.out.println(response.ResponseValidator.GetBodyAsText());

        System.out.println("######################################### callStatusAPITest with "+scenario+" compleated #########################################");
    }
}
