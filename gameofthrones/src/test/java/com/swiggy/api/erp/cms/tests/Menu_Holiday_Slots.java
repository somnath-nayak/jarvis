package com.swiggy.api.erp.cms.tests;

import org.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.dp.Critical_API_dataProvider;
import com.swiggy.api.erp.cms.helper.CriticalAPI_Helper;

import framework.gameofthrones.JonSnow.Processor;

/*
 *   Create menu-holiday slots for a given menu-id for a restaurant
 */

public class Menu_Holiday_Slots extends Critical_API_dataProvider {
	
	CriticalAPI_Helper obj =  new CriticalAPI_Helper();
	int menuIdCreated=0;
	int menuId=0;
	int invalidMenuHolidaySlotid=1234;
	
	@Test(dataProvider="menuHolidaySlotCreate", priority=0, groups={"Sanity_TC", "Smoke_TC", "Regression_TC"}, 
			description="Creates a new menu-holiday slot")
	public void createMenuHolidaySlot(int menu_id, String from_time, String to_time )
			throws JSONException, InterruptedException
	{
		  Processor p1 = obj.createMenuHolidaySlotHelper(menu_id, from_time, to_time);
		  
		  String response = p1.ResponseValidator.GetBodyAsText();
		  int statusCode = JsonPath.read(response, "$.statusCode");
		  Assert.assertEquals(statusCode, 1);
		  String statusMessage = JsonPath.read(response, "$.statusMessage");
		  Assert.assertEquals(statusMessage, "success");
		  
		  int menuId_Response =  JsonPath.read(response, "$.data.menu_id");
		  Assert.assertEquals(menuId_Response, menu_id);
		  
		  int id_Response =  JsonPath.read(response, "$.data.id");
		  Assert.assertNotNull(id_Response);
		  menuIdCreated = id_Response;
		  
		  menuId = menuId_Response;
		  
		  
	}

	
	@Test(dataProvider="menuHolidaySlotUpdate", priority=1, groups={"Sanity_TC", "Smoke_TC", "Regression_TC"}, 
			description="Updates an existing menu-holiday slot")
	public void updateMenuHolidaySlot(int menu_id, String from_time, String to_time )
			throws JSONException, InterruptedException
	{
		  Processor p1 = obj.updateMenuHolidaySlotHelper(menu_id, from_time, to_time, menuIdCreated);
		  
		  String response = p1.ResponseValidator.GetBodyAsText();
		  int statusCode = JsonPath.read(response, "$.statusCode");
		  Assert.assertEquals(statusCode, 1);
		  String statusMessage = JsonPath.read(response, "$.statusMessage");
		  Assert.assertEquals(statusMessage, "success");
		  
		  int menuId_Response =  JsonPath.read(response, "$.data.menu_id");
		  Assert.assertEquals(menuId_Response, menu_id);
		  
		  int id_Response =  JsonPath.read(response, "$.data.id");
		  Assert.assertNotNull(id_Response);	  
	}

	
	@Test(priority=2, groups={"Sanity_TC", "Smoke_TC", "Regression_TC"}, 
			description="Get an existing menu-holiday slot")
	public void getMenuHolidaySlot()
			throws JSONException, InterruptedException
	{
		  Processor p1 = obj.getMenuHolidaySlotHelper(menuId, menuIdCreated);
		  
		  String response = p1.ResponseValidator.GetBodyAsText();
		  int statusCode = JsonPath.read(response, "$.statusCode");
		  Assert.assertEquals(statusCode, 1);
		  String statusMessage = JsonPath.read(response, "$.statusMessage");
		  Assert.assertEquals(statusMessage, "success");
		  
		  int menuId_Response =  JsonPath.read(response, "$.data.menuId");
		  Assert.assertEquals(menuId_Response, menuId);
		  
		  int id_Response =  JsonPath.read(response, "$.data.id");
		  Assert.assertNotNull(id_Response);	  
	}

	@Test(priority=3, groups={"Sanity_TC", "Smoke_TC", "Regression_TC"}, 
			description="Delete an existing menu-holiday slot")
	public void deleteMenuHolidaySlot()
			throws JSONException, InterruptedException
	{
		  Processor p1 = obj.deleteMenuHolidaySlotHelper(menuIdCreated);
		  
		  String response = p1.ResponseValidator.GetBodyAsText();
		  int statusCode = JsonPath.read(response, "$.statusCode");
		  Assert.assertEquals(statusCode, 1);
		  
		  String statusMessage = JsonPath.read(response, "$.statusMessage");
		  Assert.assertEquals(statusMessage, "success");
		  
		  boolean  data =  JsonPath.read(response, "$.data");
		  Assert.assertEquals(data, true);
		  
	}

	@Test(priority=4, groups={"Smoke_TC", "Regression_TC"}, 
			description="Get an invalid pr deleted menu-holiday slot")
	public void getInvalidMenuHolidaySlot()
			throws JSONException, InterruptedException
	{
		  Processor p1 = obj.getMenuHolidaySlotHelper(menuId, menuIdCreated);
		  
		  String response = p1.ResponseValidator.GetBodyAsText();
		  int statusCode = JsonPath.read(response, "$.statusCode");
		  Assert.assertEquals(statusCode, 1);
		  String statusMessage = JsonPath.read(response, "$.statusMessage");
		  Assert.assertEquals(statusMessage, "success");
		  	  
	}
	
	@Test(dataProvider="menuHolidaySlotCreate_greaterOpenTime", priority=5, groups={"Smoke_TC", "Regression_TC"}, 
			description="Creates a menu-holiday slot with open time greater than close time")
	public void createMenuHolidaySlot_greaterOpenTime(int menu_id, String from_time, String to_time )
			throws JSONException, InterruptedException
	{
		  Processor p1 = obj.createMenuHolidaySlotHelper(menu_id, from_time, to_time);
		  
		  String response = p1.ResponseValidator.GetBodyAsText();
		  int statusCode = JsonPath.read(response, "$.statusCode");
		  Assert.assertEquals(statusCode, 0);
		  
		  String statusMessage = JsonPath.read(response, "$.statusMessage");
		  Assert.assertEquals(statusMessage, "Validation Error");
		  
		  String from_time_error =  JsonPath.read(response, "$.data.field_validation_errors[0].value");
		  Assert.assertEquals(from_time_error, "From time should be before To time.");
		  
		  String to_time_error =  JsonPath.read(response, "$.data.field_validation_errors[1].value");
		  Assert.assertEquals(to_time_error, "To time should be after from time.");
		  
	}

	
	@Test(dataProvider="menuHolidaySlotCreate_sameOpenCloseTime", priority=6, groups={"Smoke_TC", "Regression_TC"}, 
			description="Creates a menu-holiday slot with same open time and close time")
	public void createMenuHolidaySlot_sameOpenCloseTime(int menu_id, String from_time, String to_time )
			throws JSONException, InterruptedException
	{
		  Processor p1 = obj.createMenuHolidaySlotHelper(menu_id, from_time, to_time);
		  
		  String response = p1.ResponseValidator.GetBodyAsText();
		  int statusCode = JsonPath.read(response, "$.statusCode");
		  Assert.assertEquals(statusCode, 0);
		  
		  String statusMessage = JsonPath.read(response, "$.statusMessage");
		  Assert.assertEquals(statusMessage, "Validation Error");
		  
		  String from_time_error =  JsonPath.read(response, "$.data.field_validation_errors[0].value");
		  Assert.assertEquals(from_time_error, "From time should be before To time.");
		  
		  String to_time_error =  JsonPath.read(response, "$.data.field_validation_errors[1].value");
		  Assert.assertEquals(to_time_error, "To time should be after from time.");
		  
	}

	
	@Test(dataProvider="menuHolidaySlotCreate_pastTime", priority=7, groups={"Smoke_TC", "Regression_TC"}, 
			description="Creates a menu-holiday slot with open and close time as past times")
	public void createMenuHolidaySlot_pastTime(int menu_id, String from_time, String to_time )
			throws JSONException, InterruptedException
	{
		  Processor p1 = obj.createMenuHolidaySlotHelper(menu_id, from_time, to_time);
		  
		  String response = p1.ResponseValidator.GetBodyAsText();
		  int statusCode = JsonPath.read(response, "$.statusCode");
		  Assert.assertEquals(statusCode, 0);
		  
		  String statusMessage = JsonPath.read(response, "$.statusMessage");
		  Assert.assertEquals(statusMessage, "Validation Error");
		  
		  String from_time_error =  JsonPath.read(response, "$.data.field_validation_errors[0].value");
		  Assert.assertEquals(from_time_error, "From time should be after current time.");
		  
		  String to_time_error =  JsonPath.read(response, "$.data.field_validation_errors[1].value");
		  Assert.assertEquals(to_time_error, "To time should be after current time.");
		  
	}
	
	@Test(dataProvider="createMenuHolidaySlot_InvalidMenuId", priority=8, groups={"Regression_TC"}, 
			description="Create a new menu-holiday slot for an invalid menu id")
	public void createMenuHolidaySlot_InvalidMenuId(int menu_id, String from_time, String to_time )
			throws JSONException, InterruptedException
	{
		  Processor p1 = obj.createMenuHolidaySlotHelper(menu_id, from_time, to_time);
		  
		  String response = p1.ResponseValidator.GetBodyAsText();
		  int statusCode = JsonPath.read(response, "$.statusCode");
		  Assert.assertEquals(statusCode, 0);
		  String fieldName = JsonPath.read(response, "$.data.field_validation_errors[0].field");
		  Assert.assertEquals(fieldName, "menu_id");
		  
		  String errorMessage = JsonPath.read(response, "$.data.field_validation_errors[0].value");
		  Assert.assertEquals(errorMessage, "Menu doesn't exist.");
		  
		  String statusMessage = JsonPath.read(response, "$.statusMessage");
		  Assert.assertEquals(statusMessage, "Validation Error");
	}
	
	@Test(dataProvider="createMenuHolidaySlot_MenuIdWithoutCat", priority=9, groups={"Regression_TC"}, 
			description="Create a new menu-holiday slot for a menu without the Category-ID ")
	public void createMenuHolidaySlot_MenuIdWithoutCat(int menu_id, String from_time, String to_time )
			throws JSONException, InterruptedException
	{
		 Processor p1 = obj.createMenuHolidaySlotHelper(menu_id, from_time, to_time);
		  
		  String response = p1.ResponseValidator.GetBodyAsText();
		  int statusCode = JsonPath.read(response, "$.statusCode");
		  Assert.assertEquals(statusCode, 1);
		  String statusMessage = JsonPath.read(response, "$.statusMessage");
		  Assert.assertEquals(statusMessage, "success");
		  
		  int menuId_Response =  JsonPath.read(response, "$.data.menu_id");
		  Assert.assertEquals(menuId_Response, menu_id);
		  
		  int id_Response =  JsonPath.read(response, "$.data.id");
		  Assert.assertNotNull(id_Response);
		  
		  int menuIdCreated2 = id_Response;
		  obj.deleteMenuHolidaySlotHelper(menuIdCreated2);
	}


}
