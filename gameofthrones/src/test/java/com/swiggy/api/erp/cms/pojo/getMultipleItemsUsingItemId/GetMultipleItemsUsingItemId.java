package com.swiggy.api.erp.cms.pojo.getMultipleItemsUsingItemId;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "statusCode",
        "data",
        "statusMessage"
})
public class GetMultipleItemsUsingItemId {

    @JsonProperty("statusCode")
    private Integer statusCode;
    @JsonProperty("data")
    private List<Data> data = null;
    @JsonProperty("statusMessage")
    private String statusMessage;

    /**
     * No args constructor for use in serialization
     *
     */
    public GetMultipleItemsUsingItemId() {
    }

    /**
     *
     * @param statusCode
     * @param data
     * @param statusMessage
     */
    public GetMultipleItemsUsingItemId(Integer statusCode, List<Data> data, String statusMessage) {
        super();
        this.statusCode = statusCode;
        this.data = data;
        this.statusMessage = statusMessage;
    }

    @JsonProperty("statusCode")
    public Integer getStatusCode() {
        return statusCode;
    }

    @JsonProperty("statusCode")
    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    @JsonProperty("data")
    public List<Data> getData() {
        return data;
    }

    @JsonProperty("data")
    public void setData(List<Data> data) {
        this.data = data;
    }

    @JsonProperty("statusMessage")
    public String getStatusMessage() {
        return statusMessage;
    }

    @JsonProperty("statusMessage")
    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

}