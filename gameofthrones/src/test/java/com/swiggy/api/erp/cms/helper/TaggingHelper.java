package com.swiggy.api.erp.cms.helper;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.constants.CmsConstants;
import com.swiggy.api.erp.cms.constants.Constants;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.SqlTemplate;
import org.testng.Assert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TaggingHelper{

    Initialize gameofthrones = new Initialize();

/*----------------------Tag-Config-----------------------------------------------*/

    public Processor createTagConfig(String tag_id, String entity_type, String is_multi_select, String position)
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("taggingservice", "createtagconfig", gameofthrones);
        String[] payloadParams={tag_id,entity_type,is_multi_select,position};
        Processor processor = new Processor(service, requestheaders, payloadParams);
        return processor;
    }

    public Processor getAllTagConfig()
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("taggingservice", "getalltagconfig", gameofthrones);
        Processor processor = new Processor(service, requestheaders, null, null);
        return processor;
    }

    public Processor getTagConfigforParticularId(String tag_id)
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("taggingservice", "tagconfigforparticularid", gameofthrones);
        String[] urlParams={tag_id};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

    public Processor getTagConfigforItemOrRest(String entity_type,String tag_id)
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("taggingservice", "tagconfigforrestoritem", gameofthrones);
        String[] urlParams={entity_type,tag_id};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

    public Processor updateTagConfig(String tag_id, String entity_type, String is_multi_select, String position)
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("taggingservice", "updatetagconfigforitem", gameofthrones);
        String[] payloadParams={tag_id,entity_type,is_multi_select,position};
        String[] urlParams={entity_type,tag_id};
        Processor processor = new Processor(service, requestheaders, payloadParams,urlParams);
        return processor;
    }

    public Processor deleteTagConfigForItemOrRest(String entity_type, String tag_id)
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("taggingservice", "deletetagconfigforitemorrest", gameofthrones);
        String[] urlParams={entity_type,tag_id};
        Processor processor = new Processor(service, requestheaders, null,urlParams);
        return processor;
    }

    public Processor getTagConfigByParentId(String tag_id)
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("taggingservice", "tagconfigbyparentid", gameofthrones);
        String[] urlParams={tag_id};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

/*----------------------Tag-----------------------------------------------*/

    public Processor createTag(String enabled, String name, String parent, String update)
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("taggingservice", "createtag", gameofthrones);
        String[] payloadParams={enabled,name, parent, update};
        Processor processor = new Processor(service, requestheaders, payloadParams);
        return processor;
    }

    public Processor getAllTag()
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("taggingservice", "getalltag", gameofthrones);
        Processor processor = new Processor(service, requestheaders, null, null);
        return processor;
    }

    public Processor getTag(String tag_id)
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("taggingservice", "gettagforrestoritem", gameofthrones);
        String[] urlParams={tag_id};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

    public Processor updateTag(String enabled,String tag_id, String name, String parent, String update)
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("taggingservice", "updatetag", gameofthrones);
        String[] payloadParams={enabled, tag_id, name, parent, update};
        String[] urlParams={tag_id};
        Processor processor = new Processor(service, requestheaders, payloadParams,urlParams);
        return processor;
    }

    public Processor deleteTag(String tag_id)
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("taggingservice", "deletetag", gameofthrones);
        String[] urlParams={tag_id};
        Processor processor = new Processor(service, requestheaders, null,urlParams);
        return processor;
    }

    public Processor getTagByParentId(String tag_id)
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("taggingservice", "tagbyparentid", gameofthrones);
        String[] urlParams={tag_id};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

/*----------------------Tag-Cateogory-----------------------------------------------*/

    public Processor createTagCategory(String enabled, String name, String update)
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("taggingservice", "tagcategory", gameofthrones);
        String[] payloadParams={enabled, name, update};
        Processor processor = new Processor(service, requestheaders, payloadParams);
        return processor;
    }

    public Processor getAllTagCategory()
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("taggingservice", "getalltagcategory", gameofthrones);
        Processor processor = new Processor(service, requestheaders, null, null);
        return processor;
    }

    public Processor getTagById(String tag_id)
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("taggingservice", "gettagcategorybyid", gameofthrones);
        String[] urlParams={tag_id};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

    public Processor updateTagCategory(String enabled, String tag_id, String name, String update)
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("taggingservice", "updatetagcategory", gameofthrones);
        String[] payloadParams={enabled ,name, update};
        String[] urlParams={tag_id};
        Processor processor = new Processor(service, requestheaders, payloadParams,urlParams);
        return processor;
    }

    public Processor deleteTagCategory(String tag_id)
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("taggingservice", "deletetagcategory", gameofthrones);
        String[] urlParams={tag_id};
        Processor processor = new Processor(service, requestheaders, null,urlParams);
        return processor;
    }

    /*----------------------Entity-tag-map------------------------------------------------*/

    public Processor createEntityTagMap(String category, String entity_id, String entity_type, String partiotion_id,String position, String tag_id, String tag_name)
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("taggingservice", "createentitytagmap", gameofthrones);
        String[] payloadParams={category,entity_id,entity_type,partiotion_id,position,tag_id, tag_name};
        Processor processor = new Processor(service, requestheaders, payloadParams);
        return processor;
    }

    public Processor getEntityTagMap(String tag_id,String entity_type,String page, String rows)
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("taggingservice", "getentitytagmap", gameofthrones);
        String[] urlParams={tag_id,entity_type,page,rows};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

    public Processor getAllByEntityIdEntitytype(String entity_id, String entity_type,String partition_id)
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("taggingservice", "getentitytagmapbyentity", gameofthrones);
        String[] urlParams={entity_id,entity_type,partition_id};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

    public Processor updateEntityTag(String category, String entity_id, String entity_type, String partiotion_id,String position, String tag_id, String tag_name)
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("taggingservice", "updateentitytagmap", gameofthrones);
        String[] payloadParams={category, entity_id, entity_type, partiotion_id,position,  tag_id, tag_name};
        String[] urlParams={entity_type, entity_id, tag_id};
        Processor processor = new Processor(service, requestheaders, payloadParams,urlParams);
        return processor;
    }

    public Processor deleteEntityMap( String entity_type, String entity_id,String tag_id,String partition_id)
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("taggingservice", "deleteentitytagmap", gameofthrones);
        String[] urlParams={entity_type, entity_id,tag_id,partition_id};
        Processor processor = new Processor(service, requestheaders, null,urlParams);
        return processor;
    }

    public Processor entityTagMapbyTag( String tag_id,String entity_type, String page,String rows_per_page)
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("taggingservice", "entitytagmapbytag", gameofthrones);
        String[] urlParams={tag_id,entity_type,page,rows_per_page};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

    /*----------------------DB Connection------------------------------------------------*/

    public List<Map<String, Object>> getParent() {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(Constants.parentQuery);
        return list;
    }

    public List<Map<String, Object>> tagConfig(String categoryId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(Constants.tagConfigQuery+categoryId+"'");
        return list;
    }

    public List<Map<String, Object>> tagConfigDetails(String tagId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(Constants.tagConfigdetails+tagId+"'");
        return list;
    }

    public List<Map<String, Object>> entityTagMapDetails(String tagId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(Constants.entityTagMapDetails+tagId+"'");
        return list;
    }


    /*----------------------Entity Tag Map bulk upload------------------------------------------------*/

    public Processor entityTagMapBulk(String payload)
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("taggingservice", "entitytagmapbulkupload", gameofthrones);
        String[] payloadParams={payload};
        Processor processor = new Processor(service, requestheaders, payloadParams);
        return processor;
    }

    public String tagIdGeneration(String enabled, String name,String update, String entity_type, String multi, String position, String tagName,String tagName2)
    {
        String response= createTagCategory(enabled,name, update).ResponseValidator.GetBodyAsText();
        String statusCode= JsonPath.read(response,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.status, statusCode,"statusCode is not 1");
        String id= JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertNotNull(id,"id is null");
        String updateBy= JsonPath.read(response,"$.data.updated_by").toString().replace("[","").replace("]","");
        Assert.assertEquals(update, updateBy,"statusCode is not 1");

        String tagResponse= createTag(enabled,tagName,id,update).ResponseValidator.GetBodyAsText();
        statusCode(tagResponse);
        String parentTagCategory= JsonPath.read(tagResponse,"$.data.parent").toString().replace("[","").replace("]","");
        Assert.assertEquals(id, parentTagCategory,"parent field is not same");
        String tagId= JsonPath.read(tagResponse,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertNotNull(id,"id is null");

        String tagResponse1= createTag(enabled,tagName2,id,update).ResponseValidator.GetBodyAsText();
        statusCode(tagResponse1);
        String parentTagCategory1= JsonPath.read(tagResponse,"$.data.parent").toString().replace("[","").replace("]","");
        Assert.assertEquals(id, parentTagCategory1,"parent field is not same");

        String createTagConfigRsponse= createTagConfig(id,entity_type,multi,position).ResponseValidator.GetBodyAsText();
        statusCode(createTagConfigRsponse);
        String categoryId= JsonPath.read(createTagConfigRsponse,"$.data.tag_category_id").toString().replace("[","").replace("]","");
        Assert.assertEquals(id, categoryId,"id is not same");
        String multiSelect=JsonPath.read(createTagConfigRsponse,"$.data.is_multi_select").toString().replace("[","").replace("]","");
        Assert.assertEquals(multi, multiSelect,"multi select values is different");
        return tagId;
    }

    public void statusCode(String response)
    {
        String statusCode= JsonPath.read(response,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.status, statusCode,"statusCode is not 1");
    }

}
