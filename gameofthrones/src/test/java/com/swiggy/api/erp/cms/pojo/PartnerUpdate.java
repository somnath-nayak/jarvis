package com.swiggy.api.erp.cms.pojo;

/**
 * Created by kiran.j on 4/4/18.
 */
public class PartnerUpdate
{
    private Long id;

    private int type_of_partner;

    private Long city_id;

    private String partner_id;

    private boolean is_enabled;

    private String city_name;

    private Long area_id;

    private String restaurant_helpline;

    private String area_name;

    private String restaurant_id;

    private String otp;

    private String restaurant_name;

    public Long getId ()
    {
        return id;
    }

    public void setId (Long id)
    {
        this.id = id;
    }

    public int getType_of_partner ()
    {
        return type_of_partner;
    }

    public void setType_of_partner (int type_of_partner)
    {
        this.type_of_partner = type_of_partner;
    }

    public Long getCity_id ()
    {
        return city_id;
    }

    public void setCity_id (Long city_id)
    {
        this.city_id = city_id;
    }

    public String getPartner_id ()
    {
        return partner_id;
    }

    public void setPartner_id (String partner_id)
    {
        this.partner_id = partner_id;
    }

    public boolean getIs_enabled ()
    {
        return is_enabled;
    }

    public void setIs_enabled (boolean is_enabled)
    {
        this.is_enabled = is_enabled;
    }

    public String getCity_name ()
    {
        return city_name;
    }

    public void setCity_name (String city_name)
    {
        this.city_name = city_name;
    }

    public Long getArea_id ()
    {
        return area_id;
    }

    public void setArea_id (Long area_id)
    {
        this.area_id = area_id;
    }

    public String getRestaurant_helpline ()
    {
        return restaurant_helpline;
    }

    public void setRestaurant_helpline (String restaurant_helpline)
    {
        this.restaurant_helpline = restaurant_helpline;
    }

    public String getArea_name ()
    {
        return area_name;
    }

    public void setArea_name (String area_name)
    {
        this.area_name = area_name;
    }

    public String getRestaurant_id ()
    {
        return restaurant_id;
    }

    public void setRestaurant_id (String restaurant_id)
    {
        this.restaurant_id = restaurant_id;
    }

    public String getOtp ()
    {
        return otp;
    }

    public void setOtp (String otp)
    {
        this.otp = otp;
    }

    public String getRestaurant_name ()
    {
        return restaurant_name;
    }

    public void setRestaurant_name (String restaurant_name)
    {
        this.restaurant_name = restaurant_name;
    }

    public PartnerUpdate build() {
        this.setIs_enabled(true);
        return this;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", type_of_partner = "+type_of_partner+", city_id = "+city_id+", partner_id = "+partner_id+", is_enabled = "+is_enabled+", city_name = "+city_name+", area_id = "+area_id+", restaurant_helpline = "+restaurant_helpline+", area_name = "+area_name+", restaurant_id = "+restaurant_id+", otp = "+otp+", restaurant_name = "+restaurant_name+"]";
    }
}
