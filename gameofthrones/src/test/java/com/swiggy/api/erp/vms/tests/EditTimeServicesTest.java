package com.swiggy.api.erp.vms.tests;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.vms.dp.EditTimeServicesDP;
import com.swiggy.api.erp.vms.helper.EditTimeServiceHelper;

import framework.gameofthrones.JonSnow.Processor;

public class EditTimeServicesTest extends EditTimeServicesDP {

	@Test(dataProvider = "OrderEditRequest")
	public void testOrderEditRequest(String payLoad, int code, String message) {
		Processor p = EditTimeServiceHelper.editOrderRequest(payLoad, null);
		Assert.assertEquals(p.ResponseValidator.GetNodeValueAsInt("statusCode"),code);
		Assert.assertEquals( p.ResponseValidator.GetNodeValue("statusMessage"),message);
		if (code == 0) {
			Assert.assertTrue(
					Long.parseLong(JsonPath.read(p.ResponseValidator.GetBodyAsText(), "$.data.id").toString()) > 0);
		}
	}
	
	
	@Test(dataProvider = "orderEditRequestStatusById")
	public void testOrderEditRequestStatusById(String payLoad, String orderId, String status) {
		Processor p = EditTimeServiceHelper.editOrderRequest(payLoad, null);
		String orderEditRequestId = JsonPath.read(p.ResponseValidator.GetBodyAsText(), "$.data.id").toString();
		Assert.assertEquals(0, p.ResponseValidator.GetNodeValueAsInt("statusCode"));
		p = EditTimeServiceHelper.orderEditRequestStatus(null, orderEditRequestId);
		String orderEditRequestIdPresent = JsonPath.read(p.ResponseValidator.GetBodyAsText(), "$.data.id").toString();
		Assert.assertEquals(0, p.ResponseValidator.GetNodeValueAsInt("statusCode"));
		Assert.assertEquals("success", p.ResponseValidator.GetNodeValue("statusMessage"));
		Assert.assertEquals(orderEditRequestIdPresent, orderEditRequestId, "RequestId Not Matching");
		Assert.assertEquals(
				Long.parseLong(JsonPath.read(p.ResponseValidator.GetBodyAsText(), "$.data.order_id").toString()),
				Long.parseLong(orderId), "OrderId Not Matching");
		Assert.assertEquals(JsonPath.read(p.ResponseValidator.GetBodyAsText(), "$.data.status").toString(), status,
				"Status Failed");
	}
	
	@Test(dataProvider = "orderEditRequestStatusByIdWithInvalidData")
	public void testOrderEditRequestStatusByIdFieldChecksWithInvalidData(String RequestId, int Code,String status) {
		Processor p = EditTimeServiceHelper.orderEditRequestStatus(null, RequestId);
		Assert.assertEquals(p.ResponseValidator.GetNodeValueAsInt("statusCode"),Code);
		Assert.assertEquals( p.ResponseValidator.GetNodeValue("statusMessage"),status);

	}
	
	

	@Test(dataProvider = "CheckIfOrderEditable")
	public void testCheckIfOrderEditableBeforeDEAssign(String orderId,int code,String message) {
		Processor p = EditTimeServiceHelper.orderEditTableCheck(null, orderId);
		Assert.assertEquals(code, p.ResponseValidator.GetNodeValueAsInt("statusCode"));
		Assert.assertEquals(message, p.ResponseValidator.GetNodeValue("statusMessage"));
		if(code==0) {
			Assert.assertNotNull(JsonPath.read(p.ResponseValidator.GetBodyAsText(), "$.data").toString(), "Status Failed");
			Assert.assertTrue(JsonPath.read(p.ResponseValidator.GetBodyAsText(), "$.data.item_addition_allowed"),
					"item_addition_allowed Before DE Assign Failed");
		}
		
	}

	@Test()
	public void testEditTimeServiceHealth() {
		Processor p = EditTimeServiceHelper.ping(null);
		int stringResponse = p.ResponseValidator.GetResponseCode();
		System.out.println("Hai=======>" + stringResponse);
	}

}
