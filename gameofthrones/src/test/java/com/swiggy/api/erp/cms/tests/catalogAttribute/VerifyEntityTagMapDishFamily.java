package com.swiggy.api.erp.cms.tests.catalogAttribute;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.dp.itemDP;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Tyrion.RabbitMQHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;

public class VerifyEntityTagMapDishFamily extends itemDP {

    Initialize gameofthrones = new Initialize();
    RabbitMQHelper helper=new RabbitMQHelper();
    CMSHelper cmsHelper= new CMSHelper();
    SqlTemplate sqlTemplateCI = SystemConfigProvider.getTemplate("cms");
    @BeforeClass
    public void purgeRmq(){
        try {
            helper.purgeQueue("oms", "item.nam.change.queue");
        }
        catch (IOException e){}
        catch (TimeoutException e){}
    }
    String itemName;
    int itmId;


    @Test(dataProvider="createitempj",description = "Create Item API")
    public void createItem(String name) {
        String response=cmsHelper.createItem(name).ResponseValidator.GetBodyAsText();
        itemName=JsonPath.read(response,"$.data.name");

    }

    @Test(dependsOnMethods = "createItem",description = "Verify item in the rabbitMQ")
    public void rmqItemNameChange(){
        if (itemName!=null) {
            String str = helper.getMessage("oms", "item.nam.change.queue");
            String newNameRmq = JsonPath.read(str, "$.new_item.name");
            itmId=JsonPath.read(str, "$.new_item.id");
            Assert.assertEquals(itemName, newNameRmq);

        }


    }

    @Test(dependsOnMethods = "rmqItemNameChange",description = "Verify item id is present in entity tag map table")
    public void predScoreGreater(){
        String response=cmsHelper.dpAPIDishType(itemName).ResponseValidator.GetBodyAsText();
        Double predScore=JsonPath.read(response,"$.result.prediction_score");
        if(predScore>=0.7){
            sqlTemplateCI = SystemConfigProvider.getTemplate("cms");
            List<Map<String, Object>> entitytag = sqlTemplateCI.queryForList("select entity_id from `entity_tag_map` where `entity_id`="+itmId);
            Assert.assertTrue(!entitytag.isEmpty(), "Entity id not present in entity tag map table");

        }

        else{
            Assert.fail("Prediction score/Confidence for item is less than 0.7");
        }

    }
}
