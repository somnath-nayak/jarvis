package com.swiggy.api.erp.delivery.helper;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.swiggy.api.erp.delivery.Pojos.ClusterItem;
import com.swiggy.api.erp.delivery.Pojos.Coordinate;
import com.swiggy.api.erp.delivery.Pojos.CreateCluster;
import com.swiggy.api.erp.delivery.Pojos.Polygon;
import com.swiggy.api.erp.delivery.constants.ClusterConstants;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Daenerys.Initializer;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.dao.EmptyResultDataAccessException;

import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 	Modified by somnath.nayak on 20/04/18.
 */
public class ClusterServiceHelper
{
	Initialize gameofthrones;

//	ClusterServiceHelper clusterHelper;

	public static String connectionGroup = null;
	public static String connectionId = null;
	public static String custClusterId, restClusterId;
	public static String[] tempConnectionGroup, tempConnectionGroup1;
	public static String[] tempConnection, tempConnection1;
	public static String ItemId, selfDelItemId, item_refid, selfDelRefId;
	int numRetries = 5, pollInt = 1000;
	private static final Logger LOGGER = Logger.getLogger(ClusterServiceHelper.class.getName());
	public String customerClusterName = "SMOKE-CUSTOMER-CLUSTER", restaurantClusterName = "SMOKE-RESTAURANT-CLUSTER";
	public String cgName = "SMOKE-CONNECTION-GROUP";

	public ClusterServiceHelper()
	{
		if(gameofthrones==null)
		{
			gameofthrones = Initializer.getInitializer();
		}
	}

	public String createConnectionGroup(String name)
	{
		return createcg_andGetId(name, custClusterId);
	}

	public String getConnectionGroup(String name)
	{
		if(connectionGroup==null)
		{
			connectionGroup = createConnectionGroup(name);
		}
		return connectionGroup;
	}

	public String createConnection(String name)
	{
		if(custClusterId==null)
		{
			custClusterId = createCluster("customerSmoke"+name);
		}
		if(restClusterId ==null)
		{
			restClusterId = createCluster("restaurantSmoke"+name);
		}
		if(connectionGroup==null)
		{
			connectionGroup = createConnectionGroup(name);
		}

		return createConnection_andGetId(connectionGroup, custClusterId, restClusterId);
	}

	public String getConnection(String name)
	{
		if(connectionId == null)
		{
			connectionId = createConnection(name);
		}
		return connectionId;
	}

	public String getCustomerCluster(String name)
	{
		if(custClusterId==null)
		{
			custClusterId = createCluster("customerSmoke"+name);
		}
		return custClusterId;
	}

	public String getRestaurantCluster(String name)
	{
		if(restClusterId ==null)
		{
			restClusterId = createCluster("restaurantSmoke"+name);
		}
		return restClusterId;
	}


	public List<String> getClusterLatLong(String custClusterId) {
		List<String> list = new ArrayList();

		//String customerClustername = SystemConfigProvider.getTemplate("deliveryclusterdb").queryForMap("select name from clusters where id='" + custClusterId/*clusterHelper.getCustomerCluster(name)*/ + "' and tags='customer'").get("name").toString();
		Map<String, Object> content = SystemConfigProvider.getTemplate("deliveryclusterdb").queryForMap("select * from clusters where id='" + custClusterId + "' and tags='customer'");
		String polygon = content.get("polygon").toString();

		try {
			JSONObject test = new JSONObject(polygon);
			Object test1 = test.get("coordinates");
			int totalSize = ((JSONArray) test1).length();

			for (int i = 0; i < totalSize; i++) {
				String latlng = ((JSONArray) test1).get(i).toString();
				JSONObject latlangObject = new JSONObject(latlng);
				String lat = String.valueOf(latlangObject.getDouble("lat"));
				String lng = String.valueOf(latlangObject.getDouble("lng"));
				list.add(lat + "," + lng);
			}
		}
		catch (org.json.JSONException e){}
		return list;
	}


	public void createCluster(String nameForFetchingDetailsFromDB, String name, String tag)
	{
		CreateCluster createCluster = new CreateCluster();

		Polygon poly = new Polygon();
		List<Coordinate> coordinates = new ArrayList<>();
		Map<String, Object> content = SystemConfigProvider.getTemplate("deliveryclusterdb").queryForMap("select * from clusters where name='" + nameForFetchingDetailsFromDB + "' and tags='" + tag + "'");
		String polygon = content.get("polygon").toString();

		try {
			JSONObject test = new JSONObject(polygon);
			Object test1 = test.get("coordinates");
			int totalSize = ((JSONArray) test1).length();

			for (int i = 0; i < totalSize; i++) {
				String latlng = ((JSONArray) test1).get(i).toString();
				JSONObject latlangObject = new JSONObject(latlng);
				Double lat = latlangObject.getDouble("lat");
				Double lng = latlangObject.getDouble("lng");
				Coordinate coord = new Coordinate();
				coord.setLng(lng);
				coord.setLat(lat);
				coordinates.add(coord);
			}
		}
		catch (org.json.JSONException e){}

		Map<String, Object> attribute = new LinkedHashMap();
		//String attributesStr = "{\"enable\": 1, \"openTime\": \"0000\", \"closeTime\": \"2359\", \"degradedState\": \"normal\"}";//content.get("attributes").toString();
		//cluster attributes..
		attribute.put("enable", "1");
		attribute.put("openTime", "0000");
		attribute.put("closeTime", "2359");
		attribute.put("degradedState", "normal");
		/*try {
			JSONObject test = new JSONObject(attributesStr);
			int totalSize = test.length();

			for (int i = 0; i < totalSize; i++) {
				String attributeKey =  test.names().get(i).toString();
				Object attributeValue = test.get(attributeKey);
				attribute.put(attributeKey, attributeValue);
			}
		} catch (org.json.JSONException e){}*/


		poly.setCoordinates(coordinates);
		createCluster.setPolygon(poly);
		String entityType = content.get("entity_type").toString();
		//String parentId = content.get("parent_id").toString();
		String parenClusterId = content.get("id").toString();
		String type = "custom";//content.get("type").toString();

		createCluster.setEntityType(entityType);
		createCluster.setParentId(parenClusterId);
		createCluster.setName(name);
		createCluster.setType(type);
		createCluster.setAttributes(attribute);

		String tagsStr = content.get("tags").toString();
		ArrayList<String> tags = new ArrayList<>(Arrays.asList(tagsStr));
		createCluster.setTags(tags);

		ObjectMapper om = new ObjectMapper();
		String json=null;
		try
		{
			json = om.writeValueAsString(createCluster);
		} catch (IOException e) {
			e.printStackTrace();
		}

		createClusterWithPOJO(json);
	}

	public String createCluster(String name)
	{
		//fetching details from BTM cluster
		String clusterNameForGettingDetailsFromDB = "BLR_BTM";
		String clusterId = "";
		if(ifClusterExists(name)) {
			if(name.toLowerCase().contains("customer"))
			{

				createCluster(clusterNameForGettingDetailsFromDB, name, "customer");
				//createCluster(new String[]{"12.937484", "77.612284", "12.942994", "77.619920", "12.931175", "77.628585", "12.926514", "77.618387", ClusterConstants.cityId, type, "customer"});
				clusterId = SystemConfigProvider.getTemplate("deliveryclusterdb").queryForMap("select id from clusters where name='" + name + "'").get("id").toString();
			}else if(name.toLowerCase().contains("restaurant"))
			{
				createCluster(clusterNameForGettingDetailsFromDB, name,"restaurant");
				//createCluster(new String[]{"12.937484", "77.612284", "12.942994", "77.619920", "12.931175", "77.628585", "12.926514", "77.618387", ClusterConstants.cityId, type, "restaurant"});
				clusterId = SystemConfigProvider.getTemplate("deliveryclusterdb").queryForMap("select id from clusters where name='" + name + "'").get("id").toString();
			}
		}
		return clusterId;
	}

	public void disableConnectionGroupAndConnection()
	{
		List<Map<String, Object>> enabledId = SystemConfigProvider.getTemplate("deliveryclusterdb")
				.queryForList("select id from connection_groups where enabled='1'");
		List<String> arr = new ArrayList<>();
		for (Map<String, Object> test : enabledId) {
			arr.add(test.get("id").toString());
		}
		tempConnectionGroup = arr.toArray(new String[arr.size()]);

		/*List<Map<String, Object>> enabledConId = SystemConfigProvider.getTemplate("deliveryclusterdb")
				.queryForList("select id from connections where enabled='1'");
		List<String> arr1 = new ArrayList<>();
		for (Map<String, Object> test : enabledConId) {
			arr1.add(test.get("id").toString());
		}
		tempConnection = arr1.toArray(new String[arr1.size()]);*/

		updateMultipleTuplesQuery("connection_groups", "enabled", "0", "id", tempConnectionGroup);
		//updateMultipleTuplesQuery("connections", "enabled", "0", "id", tempConnection);
	}

	public void enableConnectionGroupAndConnection()
	{
		updateMultipleTuplesQuery("connection_groups", "enabled", "1", "id", tempConnectionGroup);
		//updateMultipleTuplesQuery("connections", "enabled", "1", "id", tempConnection);

		Object[] enabled_id = SystemConfigProvider.getTemplate("deliveryclusterdb")
				.queryForList("select id from connection_groups where enabled='1'").toArray();
		tempConnectionGroup1 = Arrays.toString(enabled_id).replace("{", "").replace("}", "").replace("[", "{").replace("]", "}").split(",");
		LOGGER.log(Level.INFO,Arrays.toString(tempConnectionGroup1));
		

		/*Object[] enabled_cgid = SystemConfigProvider.getTemplate("deliveryclusterdb")
				.queryForList("select id from connections where enabled='1'").toArray();
		tempConnection1 = Arrays.toString(enabled_cgid).replace("{", "").replace("}", "").replace("[", "{").replace("]", "}").split(",");
		LOGGER.log(Level.INFO,Arrays.toString(tempConnection1));*/

	}


	public void updateCluster(String name, String tag, String attributeName, String attributeVal)
	{
		CreateCluster createCluster = new CreateCluster();

		Polygon poly = new Polygon();
		List<Coordinate> coordinates = new ArrayList<>();
		Map<String, Object> content = SystemConfigProvider.getTemplate("deliveryclusterdb").queryForMap("select * from clusters where name='" + name + "' and tags='" + tag + "'");
		String polygon = content.get("polygon").toString();

		try {
			JSONObject test = new JSONObject(polygon);
			Object test1 = test.get("coordinates");
			int totalSize = ((JSONArray) test1).length();

			for (int i = 0; i < totalSize; i++) {
				String latlng = ((JSONArray) test1).get(i).toString();
				JSONObject latlangObject = new JSONObject(latlng);
				Double lat = latlangObject.getDouble("lat");
				Double lng = latlangObject.getDouble("lng");
				Coordinate coord = new Coordinate();
				coord.setLng(lng);
				coord.setLat(lat);
				coordinates.add(coord);
			}
		}
		catch (org.json.JSONException e){}

		Map<String, Object> attribute = new LinkedHashMap();
		String attributesStr = content.get("attributes").toString();

		try {
			JSONObject test = new JSONObject(attributesStr);
			int totalSize = test.length();

			for (int i = 0; i < totalSize; i++) {
				String attributeKey =  test.names().get(i).toString();
				Object attributeValue = test.get(attributeKey);
				attributeValue = (attributeKey.equals(attributeName)) ? attributeVal : attributeValue;
				attribute.put(attributeKey, attributeValue);
			}
		} catch (org.json.JSONException e){}


		poly.setCoordinates(coordinates);
		createCluster.setPolygon(poly);
		String entityType = content.get("entity_type").toString();
		String parentId = content.get("parent_id").toString();
		String type = content.get("type").toString();

		entityType = (attributeName.equals("entity_type")) ? attributeVal : entityType;
		parentId = (attributeName.equals("parent_id")) ? attributeVal : parentId;
		name = (attributeName.equals("name")) ? attributeVal : name;
		type = (attributeName.equals("type")) ? attributeVal : type;
		createCluster.setEntityType(entityType);
		createCluster.setParentId(parentId);
		createCluster.setName(name);
		createCluster.setType(type);
		createCluster.setAttributes(attribute);

		String tagsStr = content.get("tags").toString();
		ArrayList<String> tags = new ArrayList<>(Arrays.asList(tagsStr));
		createCluster.setTags(tags);

		ObjectMapper om = new ObjectMapper();
		String json=null;
		try
		{
			json = om.writeValueAsString(createCluster);
		} catch (IOException e) {
			e.printStackTrace();
		}

		updateClusterWithPOJO(json, content.get("id").toString());
	}

	public void updateItemUsingPOJO(String itemId, String attributeName, String attributeVal)
	{
		ClusterItem updateItem = new ClusterItem();

		Coordinate coordinates = new Coordinate();
		Map<String, Object> content = SystemConfigProvider.getTemplate("deliveryclusterdb").queryForMap("select * from items where id='" + itemId + "'");

		Map<String, Object> attribute = new LinkedHashMap();
		String attributesStr = content.get("attributes").toString();

		try {
			JSONObject test = new JSONObject(attributesStr);
			int totalSize = test.length();

			for (int i = 0; i < totalSize; i++) {
				String attributeKey =  test.names().get(i).toString();
				Object attributeValue = test.get(attributeKey);
				attributeValue = (attributeKey.equals(attributeName)) ? attributeVal : attributeValue;
				attribute.put(attributeKey, attributeValue);
			}
		} catch (org.json.JSONException e){}

		coordinates.setLat(Double.parseDouble(attributeName.equals("lat") ? attributeVal : content.get("lat").toString()));
		coordinates.setLng(Double.parseDouble(attributeName.equals("lng") ? attributeVal : content.get("lng").toString()));

		String referenceId = (attributeName.equals("reference_id") ? attributeVal : content.get("reference_id").toString());
		String type = (attributeName.equals("type") ? attributeVal : content.get("type").toString());
		//int enabled = Integer.parseInt(attributeName.equals("enabled") ? attributeVal : content.get("enabled").toString());
		String id = (attributeName.equals("id") ? attributeVal : content.get("id").toString());

		updateItem.setAttributes(attribute);
		updateItem.setCoordinate(coordinates);
		updateItem.setReferenceId(referenceId);
		updateItem.setType(type);
		//updateItem.setEnabled(enabled);

		ObjectMapper om = new ObjectMapper();
		String json=null;
		try
		{
			json = om.writeValueAsString(updateItem);
		} catch (IOException e) {
			e.printStackTrace();
		}

		updateItemWithPOJO(json, itemId);
	}




	public Processor updateClusterWithPOJO(String json, String clusterId)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","updateClusterPOJO",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(), new String[] {json}, new String[]{clusterId});
		return processor;
	}

	public Processor updateItemWithPOJO(String json, String itemId)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","updateItemPOJO",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(), new String[] {json}, new String[]{itemId});
		return processor;
	}

	
	public Processor getConnectionsDetails(String cg_id)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","connectionsDetails",gameofthrones);
		Processor processor=new Processor(gameOfThronesService, ClusterConstants.singleHeader(),null,new String[] {cg_id});
		return processor;
	}

	public Processor getClusterChild(String cluster_id)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","clusterChild",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(), null, new String[] {cluster_id});
		return processor;
	}

	public Processor getItemsWithinParentCluster(String cluster_id)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","itemsWithinParentCluster",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(), null, new String[] {cluster_id});
		return processor;
	}

	public Processor getCluster_Details_Based_EntityType_LatLng(String entity, String lat, String lng)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","clusterDetailsBasedEntityTypeLatLng",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(), null, new String[] {entity, lat, lng});
		return processor;
	}

	public Processor getConnectionGroupsDetailsInCluster(String cluster_id)		//All connection groups details under a cluster
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","connectionGroupsInClusterDetails",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(), null, new String[] {cluster_id});
		return processor;
	}

	public Processor getConnetionGroupDetails(String cg_id)				//A particular connection group details
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","connectionGroupDetails",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(), null, new String[] {cg_id});
		return processor;
	}

	public Processor getItemDetails_referenceID(String item_refID)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","itemDetails_refId",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(), null, new String[] {item_refID});
		return processor;
	}

	public Processor getClusterItems(String cluster_id)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","clusterItems",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(), null, new String[] {cluster_id});
		return processor;
	}

	public Processor getClusterDetail(String cluster_id)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","clusterDetails",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(), null, new String[] {cluster_id});
		return processor;
	}

	public Processor getItemsDetails(String item_id)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","itemDetails",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(), null, new String[] {item_id});
		return processor;
	}

	public Processor deleteConnection(String connection_id)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","connectionDelete",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(), null, new String[] {connection_id});
		return processor;
	}

	public Processor deleteCluster(String cluster_id)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","clusterDelete",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(), null, new String[] {cluster_id});
		return processor;
	}

	public Processor deleteItem(String item_id)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","itemDelete",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(), null, new String[] {item_id});
		return processor;
	}

	public Processor deleteConnectionGroup(String cg_id)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","connectionGroupDelete",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(), null, new String[] {cg_id});
		return processor;
	}

	public Processor deleteItem_fromCluster(String cluster_id, String item_id)			//Delete a mapped item from that cluster
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","itemDelete_Cluster",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(), null, new String[] {cluster_id, item_id});
		return processor;
	}

	public Processor deleteItemsFromCluster(String cluster_id, String[] items)		//Delete multiple mapped items from that cluster
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","itemsDelete_Cluster",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(), items, new String[] {cluster_id});
		return processor;
	}

	public Processor filterConnectionGroup(String cluster_id, String type, String value)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","connectionGroupFilter",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(), new String[] {type, value}, new String[] {cluster_id});
		return processor;
	}

	public Processor Item_Check(String customerLat, String customerLng, String itemCount, String zoneId, String current_bannerFactor,
								String slaExtraTime, String isZoneOpen, String restaurant_id, String rule_type )
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","ItemCheck",gameofthrones);

		Processor processor=new Processor(gameOfThronesService, ClusterConstants.singleHeader(),
				new String[] {customerLat,customerLng,itemCount,zoneId,current_bannerFactor,slaExtraTime,isZoneOpen,restaurant_id,rule_type});
		return processor;
	}
	
	public String[] getSelfDelItemCheckPayload(String lat, String lng, String refId)
	{
		String[] payload = {lat, lng, ClusterConstants.item_count, ClusterConstants.zone_id, ClusterConstants.currentBF,
				ClusterConstants.sla_extra_time, ClusterConstants.is_cluster_zone_open, refId, ClusterConstants.rule_group_type};
		return payload;
	}
	
	
	public Processor Item_Check(String[] payload)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","ItemCheck",gameofthrones);
		
		Processor processor=new Processor(gameOfThronesService, ClusterConstants.singleHeader(), payload);
		return processor;    
	}

	public Processor createConnectionGroup(String cluster_id, String time_slot, String name, String priority, String type, String enabled)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","connectionGroupCreation",gameofthrones);

		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(),
				new String[] {time_slot,name,priority,type,enabled}, new String[] {cluster_id});
		return processor;
	}

	public Processor createConnectionGroup_NullTimeSlot(String cluster_id, String cg_name)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","cgTimeSlotNull",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(), new String[] {cg_name}, new String[] {cluster_id});
		return processor;
	}

	public Processor connections_Odering(String cg_id, String connection_id, String position)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","connectionsOrder",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(),
				new String[] {connection_id,position}, new String[] {cg_id});
		return processor;
	}

	public Processor ItemDisable_inCluster(String cluster_id, String item_id)	//Disable mapped items in cluster
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","itemDisableInCluster",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(), null, new String[] {cluster_id, item_id});
		return processor;
	}

	public Processor create_Item(String[] payload)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","itemCreation",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(), payload);
		return processor;
	}

	public Processor createConnection(String cg_id, String from_clusterId, String to_clusterId, String last_mile, String max_sla,
									  String openBF, String closeBF, String timeSlots, String priority, String connection_type, String enabled, String degradedState)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","connectionCreation",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(),
				new String[] {from_clusterId,to_clusterId,last_mile,max_sla,openBF,closeBF,timeSlots,priority,connection_type,enabled, degradedState}, new String[] {cg_id});
		return processor;
	}

	public Processor addItem_toCluster(String cluster_id, String item_id)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","itemAddToCluster",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(), null, new String[] {cluster_id,item_id});
		return processor;
	}

	public Processor enableItem_inCluster(String cluster_id, String item_id)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","itemEnableInCluster",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(), null, new String[] {cluster_id, item_id});
		return processor;
	}

	public Processor updateConnectionGroup(String cg_id, String cg_name, String time_slots, String priority, String type, String enabled, String tag)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","connectionGroupUpdate",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(),
				new String[] {cg_name,time_slots,priority,type,enabled,tag}, new String[] {cg_id});
		return processor;
	}

	public Processor enableCluster(String cluster_id)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","clusterEnable",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(), null, new String[] {cluster_id});
		return processor;
	}

	public Processor updateConnection(String from_cluster_id, String to_cluster_id, String last_mile, String max_sla,
									  String open_banner_factor, String close_banner_factor, String connection_type, String isEnabled, String degradedState, String connection_id)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","connectionUpdate",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(),
				new String[] {from_cluster_id,to_cluster_id,last_mile,max_sla,open_banner_factor,close_banner_factor,connection_type,isEnabled, degradedState},
				new String[] {connection_id});
		return processor;
	}

	public Processor disable_Item(String item_id)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","itemDisable",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(), null, new String[] {item_id});
		return processor;
	}

	public Processor enable_Item(String item_id)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","itemEnable",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(), null, new String[] {item_id});
		return processor;
	}

	public Processor disable_Cluster(String cluster_id)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","clusterDisable",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(), null, new String[] {cluster_id});
		return processor;
	}

	public Processor Item_Search(String lat, String lng, String entity_type, String item_count, String zoneId, String isZoneOpen,
								 String slaExtraTime, String bannerFactor, String rule_type)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","ItemSearch",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(),
				new String[] {lat,lng,entity_type,item_count,zoneId,isZoneOpen,slaExtraTime,bannerFactor,rule_type});
		return processor;
	}

	public Processor Item_Search(String[] payload)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","ItemSearch",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(), payload);
		return processor;
	}

	public Processor createBulkItem(String[] payload)							//Not Needed
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","itemBulkCreation",gameofthrones);
		Processor processor=new Processor(gameOfThronesService, ClusterConstants.singleHeader(), payload);
		return processor;
	}

	public Processor getItemWithinCoordinate(String[] payload)				//Not Needed
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","itemsWithinCoordinate",gameofthrones);
		Processor processor=new Processor(gameOfThronesService, ClusterConstants.singleHeader(), payload);
		return processor;
	}

	public Processor filterCluster(String[] payload, String cluster_id)			//Not Nedded
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","clusterFilter",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(), payload, new String[] {cluster_id});
		return processor;
	}

	public Processor addItemsToCluster(String[] payload, String cluster_id)		//Not Needed => (payload)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","itemsAddToCluster",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(), payload, new String[] {cluster_id});
		return processor;
	}

	public Processor createCluster(String[] payload)							//Not Needed
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","clusterCreation",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(), payload);
		return processor;
	}




	public Processor createClusterWithPOJO(String json)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","createCluster",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(), new String[] {json});
		return processor;
	}


	public Processor updateItemByRefId(String[] payload, String item_reference_id)		//Not Needed
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","itemUpdateTypeRefID",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(), payload, new String[] {item_reference_id});
		return processor;
	}

	public Processor updateItem(String[] payload, String item_id)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","itemUpdate",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(), payload, new String[] {item_id});
		return processor;
	}

	public Processor updateCluster(String[] payload, String cluster_id)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","updateCluster",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(), payload, new String[] {cluster_id});
		return processor;
	}

	public Processor updateCluster(String isGlobalClusterZoneOpen, String cluster_id)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","clusterUpdate",gameofthrones);
		String[] payload = {isGlobalClusterZoneOpen};
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(), payload, new String[] {cluster_id});
		return processor;
	}

	public Processor createExclusionTrueConnection(String fromClusterId, String toClusterId, String position, String isEnabled, String groupId)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","connectionCreationWithTrueFilter",gameofthrones);
		String[] payload = {fromClusterId, toClusterId, position, isEnabled};
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(), payload, new String[] {groupId});
		return processor;
	}

	public Processor createConnectionWithSla(String fromClusterId, String toClusterId, String max_sla, String priority, String isEnabled, String groupId)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","connectionCreationWithSlaFilter",gameofthrones);
		String[] payload = {fromClusterId, toClusterId, max_sla, priority, isEnabled};
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(), payload, new String[] {groupId});
		return processor;
	}

	public Processor createConnectionWithDistance(String fromClusterId, String toClusterId, String last_mile, String priority,
												  String connectionType, String isEnabled, String groupId)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","connectionCreationWithDistanceFilter",gameofthrones);
		String[] payload = {fromClusterId, toClusterId, last_mile, priority, connectionType, isEnabled};
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(), payload, new String[] {groupId});
		return processor;
	}

	public Processor updateSelfDeliveryItem(String[] payload, String item_id)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","selfDeliveryItemUpdate",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(), payload, new String[] {item_id});
		return processor;
	}
	
	public Processor updateSelfDeliveryItemWithoutPartnerId(String[] payload, String item_id)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","SDItemUpdateWithoutPartnerId",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(), payload, new String[] {item_id});
		return processor;
	}
	
	public Processor createSelfDeliveryItem(String[] payload)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","selfDeliveryItemCreate",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(), payload);
		return processor;
	}
	
	public void updateMultipleTuplesQuery(String tableName, String field_toUpdate_name, String field_toUpdate_value, 
			  String reference_fieldName, String[] reference_fieldValue)
	{
		System.out.println("before update----------" + Arrays.toString(reference_fieldValue));
		//String query = "update connections set enabled='0' WHERE id in ('" + distanceBasedConnectionId + "', '" + slaBasedConnectionId + "')";
		String attributes = "";
		for(int i = 0; i < reference_fieldValue.length; i++)
		{
			if(i == (reference_fieldValue.length - 1))
				attributes = attributes + reference_fieldValue[i];
			else
				attributes = attributes + reference_fieldValue[i] + "', '";
		}
		String query = "update " + tableName + " set " + field_toUpdate_name + "='" + field_toUpdate_value + "' WHERE " + reference_fieldName + " in ('" + attributes + "')";
		SystemConfigProvider.getTemplate("deliveryclusterdb").execute(query);
		List<Map<String,Object>> arrr= SystemConfigProvider.getTemplate("deliveryclusterdb").queryForList("select id from connection_groups where enabled='1'");
		System.out.println("****** after update*********" + arrr);
	}
	
	public boolean ifTupleExistsInDB(String table_name, String column_name, String reference_value)
	{
		List<Map<String, Object>> query = SystemConfigProvider.getTemplate("deliveryclusterdb")
										  .queryForList("select * from " + table_name + " where " + column_name + "='" + reference_value + "'");
		System.out.println("-----Tuple list------" + query);
		boolean flag = query.size() != 0;
		return flag;
	}
	
	public String deleteQuery(String tableName, String reference_fieldName, String reference_fieldValue)
	{
		String query = "delete from " + tableName + " where " + reference_fieldName + "='" + reference_fieldValue + "'";
		SystemConfigProvider.getTemplate("deliveryclusterdb").execute(query);
		return query;
	}

	public String deleteQueryWithRegEx(String tableName, String reference_fieldName, String reference_fieldValue)
	{
		String query = "delete from " + tableName + " where " + reference_fieldName + "like'%" + reference_fieldValue + "%'";
		SystemConfigProvider.getTemplate("deliveryclusterdb").execute(query);
		return query;
	}
	
	public int updateQuery(String tableName, String field_toUpdate_name, String field_toUpdate_value, String reference_fieldName, String reference_fieldValue)
	{
		String query = "update " + tableName + " set " + field_toUpdate_name + "='" + field_toUpdate_value + "' where " + reference_fieldName + "='" + reference_fieldValue + "'";
		
		int count = SystemConfigProvider.getTemplate("deliveryclusterdb").update(query);
		return count;
	}




	public String createcg_andGetId(String cg_name, String custClusterId)
	{
		createConnectionGroup(custClusterId, ClusterConstants.time_slot, cg_name, ClusterConstants.connection_position,
				ClusterConstants.rule_group_type, ClusterConstants.is_connection_enabled);

		String cg_id = SystemConfigProvider.getTemplate("deliveryclusterdb").queryForList("select id from connection_groups where name='" + cg_name + "'").get(0).get("id").toString();
		System.out.println("-----Test connection group id--------" + cg_id);
		return cg_id;
	}

	public String createConnection_andGetId(String cg_id, String custClusterId, String restClusterId)
	{
		Processor createdConnection = createConnection(cg_id, custClusterId, restClusterId, ClusterConstants.last_mile,
				ClusterConstants.max_sla, ClusterConstants.openBF, ClusterConstants.closeBF,
				ClusterConstants.time_slot, ClusterConstants.connection_position,
				ClusterConstants.connection_type, ClusterConstants.is_connection_enabled, ClusterConstants.degradedState);

		String connection_id = createdConnection.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.id");
		return connection_id;
	}


	public void deleteConnectionAndConnectionGroupInDB(String connection_id, String cg_id)
	{
		try{
			deleteQuery("connections", "id", connection_id);
			deleteQuery("connection_groups", "id", cg_id);
		}catch (Exception e)
		{
			e.printStackTrace();
		}

	}

	public void deleteAfterTest(String connection_id_Test, String cg_id_Test, String ItemId, String selfDelItemId, String custClusterId, String restClusterId)
	{
		try{
			deleteConnectionAndConnectionGroupInDB(connection_id_Test, cg_id_Test);
			deleteQuery("cluster_items", "item_id", selfDelItemId);
			deleteQuery("cluster_items", "item_id", ItemId);
			deleteQuery("items", "id", ItemId);
			deleteQuery("items", "id", selfDelItemId);
			deleteQuery("clusters", "id", custClusterId);
			deleteQuery("clusters", "id", restClusterId);
		}catch (Exception e)
		{
			e.printStackTrace();
		}

		//This is to refresh the cache in all the boxes just hitting either itemCheck or itemSearch
		for(int i=0 ; i < 10; i++)
		{
			getSelfDelItemCheckPayload("12.937484", "77.612284",ItemId);
		}
	}

	public boolean ifClusterExists(String clusterName)
	{
		boolean flag = true;
		if(ifTupleExistsInDB("clusters", "name", clusterName))
		{
			try
			{
				Map<String, Object> clusterContent = SystemConfigProvider.getTemplate("deliveryclusterdb").queryForMap("select * from clusters where name='" + clusterName + "'");
				String id = clusterContent.get("id").toString();

				System.out.println("******* Content *********** " +  clusterContent);
				System.out.println("******* Id *********** " +  id);
				System.out.println("******* TAG *********** " +  clusterContent.get("tags"));

				if(clusterContent.get("tags").equals("restaurant")) {
					for (int i = 0; i < id.length(); i++) {
						if (id != null)
							deleteQuery("cluster_items", "cluster_id", id);
					}
				}
				if(clusterContent.get("tags").equals("customer")) {
					String cgId = SystemConfigProvider.getTemplate("deliveryclusterdb").queryForMap("select id from connection_groups where cluster_id='" + id + "'").get("id").toString();
					deleteQuery("connections", "group_id", cgId);
					deleteQuery("connection_groups", "cluster_id", id);
				}
				// String idInClusterItems = SystemConfigProvider.getTemplate("deliveryclusterdb").queryForMap("select id from cluster_items where cluster_id='" + id + "'").get("id").toString();
				deleteQuery("clusters", "name", clusterName);
				System.out.println("***** SIZE ***** " + id + " ******* " + id.length());


			} catch (EmptyResultDataAccessException e) {
			}
		}
		if(ifTupleExistsInDB("clusters", "name", clusterName))
			flag = false;
		return flag;
	}


	public String getZoneId(String restaurant_ref_id)
	{
		String[] item_attributes = SystemConfigProvider.getTemplate("deliveryclusterdb")
				.queryForList("select attributes from items where reference_id='" + restaurant_ref_id + "'").toString().replace("{", "").replace("}", "").replace(" ", "").split(",");
		System.out.println("*****ItemAttributes******" + edu.emory.mathcs.backport.java.util.Arrays.toString(item_attributes));
		String zoneId = "";
		for(String zone : item_attributes)
		{
			boolean zoneExists = zone.contains("zoneId");
			if(zoneExists)
			{
				String[] zoneDetails = zone.replace(" ", "").split(":");
				zoneId = zoneDetails[1];
				break;
			}
		}
		System.out.println("zone id" + zoneId);
		return zoneId;
	}


	public HashMap<String, String> itemHashMap()

	{
		HashMap<String, String> itemMap = new LinkedHashMap<String, String>();
		itemMap.put("avgPrepTime", "15.68");
		itemMap.put("serviceable", "true");
		itemMap.put("avgItemCount", "1.68");
		itemMap.put("prepTimePeak", "35");
		itemMap.put("maxSecondMile", "10");
		itemMap.put("avgPrepTimeWDP", "15.73");
		itemMap.put("avgPrepTimeWEP", "18.03");
		itemMap.put("avgPrepTimeWDNP", "16.19");
		itemMap.put("avgPrepTimeWENP", "17.45");
		itemMap.put("minsPerKm80Perc", "5.99");
		itemMap.put("avgAreaFirstMile", "5.31");
		itemMap.put("avgRestFirstMile", "4.99");
		itemMap.put("avgPlacementDelay", "10");
		itemMap.put("compliancePercent", "0.66");
		itemMap.put("percentWithDeOrders", "0.03");
		itemMap.put("selfDeliveryOverriden", "false");
		itemMap.put("avgAssignmentDelayRest", "5.29");
		itemMap.put("avgAssignmentDelayLunch", "6.85");
		itemMap.put("lastMileRegressionSlope", "2.63");
		itemMap.put("avgAssignmentDelayDinner", "4.89");
		itemMap.put("lastMileRegressionIntercept", "8.25");
		itemMap.put("avgFirstMile", "10");
		itemMap.put("slaPrepTimeCategoryFlag", "true");
		itemMap.put("restaurantFirstMileFlag", "true");
		return itemMap;
	}


	public String[] getCreateItemElements(String lat, String lng, String refId, String areaId, String zoneId)
	{
		String[] attributes = new String[itemHashMap().size()+5];
		attributes[0] = lat;
		attributes[1] = lng;
		attributes[2] = refId;
		attributes[3] = areaId;
		attributes[4] = zoneId;
		int i = 5;
		for (Map.Entry<String, String> entry : itemHashMap().entrySet()) {
			attributes[i++] = entry.getValue();
		}
		System.out.println("***item create attributes******" + edu.emory.mathcs.backport.java.util.Arrays.toString(attributes));
		return attributes;
	}

	public String createItem_andGetId(String item_refId)
	{
		String[] payload = getCreateItemElements(ClusterConstants.item_lat, ClusterConstants.item_lng, item_refId, ClusterConstants.area_id, ClusterConstants.zone_id);
		Processor item_create = create_Item(payload);
		String item_id = item_create.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.id");
		return item_id;
	}

	public static boolean ifItemExists(String ref_Id)
	{
		List<Map<String, Object>> query = SystemConfigProvider.getTemplate("deliveryclusterdb")
				.queryForList("select * from items where reference_id='" + ref_Id + "'");
		boolean flag = query.size() != 0;
		return flag;
	}


	public String[] getSelfDelItemElements(String lat, String lng, String refId, LinkedHashMap<String, String> map)
	{
		String[] attributes = new String[map.size() + 3];
		attributes[0] = lat;
		attributes[1] = lng;
		attributes[2] = refId;
		int i = 3;
		for (Map.Entry<String, String> entry : map.entrySet()) {
			attributes[i++] = entry.getValue();
		}
		System.out.println("***self delivery item create attributes******" + edu.emory.mathcs.backport.java.util.Arrays.toString(attributes));
		return attributes;
	}


	public LinkedHashMap<String, String> selfDelItemAttributes()
	{
		LinkedHashMap<String, String> itemMap = new LinkedHashMap<String, String>();
		itemMap.put("areaId", ClusterConstants.area_id);
		itemMap.put("zoneId", ClusterConstants.zone_id);
		//itemMap.put("partnerId", ClusterConstants.partnerId);
		itemMap.put("partnerId", "12");
		itemMap.put("serviceable", ClusterConstants.serviceable);
		itemMap.put("fixedSla", ClusterConstants.fixedSla);
		itemMap.put("deliveryPartnerActive", ClusterConstants.deliveryPartnerActive);
		itemMap.put("selfDeliveryOverriden", ClusterConstants.selfDeliveryOverriden);
		itemMap.put("deliveryPartnerFixedSla", ClusterConstants.deliveryPartnerFixedSla);

		return itemMap;
	}


	public String createSelfDelItem_andGetId(String item_refId) {
		String[] payload = getSelfDelItemElements(ClusterConstants.selfDelItemLat, ClusterConstants.selfDelItemLng, item_refId, selfDelItemAttributes());
		Processor item_create = createSelfDeliveryItem(payload);
		String item_id = item_create.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.id");
		return item_id;
	}


	public Processor item_check_response(String customer_lat, String customer_lng, String item_refid)
	{
		Processor response = Item_Check(customer_lat, customer_lng, ClusterConstants.item_count, ClusterConstants.zone_id,
				ClusterConstants.currentBF, ClusterConstants.sla_extra_time,
				ClusterConstants.is_cluster_zone_open, item_refid, ClusterConstants.rule_group_type);
		return response;
	}


	public void connectionUpdateWithDistance(String last_mile, String custClusterId, String restClusterId, String connection_id_Test)
	{
		updateConnection(custClusterId, restClusterId,
				last_mile, ClusterConstants.max_sla, ClusterConstants.openBF,
				ClusterConstants.closeBF, ClusterConstants.connection_type,
				ClusterConstants.is_connection_enabled, ClusterConstants.degradedState, connection_id_Test);
	}


	public String fetchItemList_fromItem_Check_response(String customer_lat, String customer_lng, String item_refid) {
		Processor processor = Item_Check(customer_lat, customer_lng, ClusterConstants.item_count, ClusterConstants.zone_id, ClusterConstants.currentBF,
				ClusterConstants.sla_extra_time, ClusterConstants.is_cluster_zone_open, item_refid,
				ClusterConstants.rule_group_type);

		String item_list = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.itemList");
		return item_list;
	}


	public String updateRainModeInZoneAndZoneRainParams(String zoneid, String rainModeType)
	{
		String rainModeTypeInZone = SystemConfigProvider.getTemplate("deliverydb").queryForMap("select rain_mode_type from zone where id='" + ClusterConstants.zone_id + "'").get("rain_mode_type").toString();
		SystemConfigProvider.getTemplate("deliverydb").update("update zone set rain_mode_type=" + rainModeType + " where id='" + ClusterConstants.zone_id + "'");

		return rainModeTypeInZone;

	}


	public Processor createConnectionWithDegradationState(String cg_id, String from_clusterId, String to_clusterId, String last_mile, String max_sla,
														  String openBF, String closeBF, String timeSlots, String priority, String connection_type, String enabled, String degradationState)
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","createConnectionWithDegradedState",gameofthrones);
		Processor processor = new Processor(gameOfThronesService, ClusterConstants.singleHeader(),
				new String[] {from_clusterId,to_clusterId,last_mile,max_sla,openBF,closeBF,timeSlots,priority,connection_type,enabled, degradationState}, new String[] {cg_id});
		return processor;
	}


	public void makeSDserviceable(String selfDelItemId, String selfDelRefId) {
		LinkedHashMap<String, String> itemMap = selfDelItemAttributes();
		itemMap.put("selfDeliveryOverriden", "false");
		itemMap.put("deliveryPartnerActive", "true");
		itemMap.put("serviceable", "true");
		itemMap.put("partnerId", "12");

		String[] payload = getSelfDelItemElements(ClusterConstants.selfDelItemLat, ClusterConstants.selfDelItemLng, selfDelRefId, itemMap);
		updateSelfDeliveryItem(payload, selfDelItemId);
	}




	public void connectionUpdate(String max_sla, String custClusterId, String restClusterId, String connection_id_Test)
	{
		updateConnection(custClusterId, restClusterId,
				ClusterConstants.last_mile, max_sla, ClusterConstants.openBF,
				ClusterConstants.closeBF, ClusterConstants.connection_type,
				ClusterConstants.is_connection_enabled, ClusterConstants.degradedState, connection_id_Test);
	}
	public void connectionUpdateBF(String closeBF, String custClusterId, String restClusterId, String connection_id_Test)
	{
		updateConnection(custClusterId, restClusterId,
				ClusterConstants.last_mile, ClusterConstants.max_sla, ClusterConstants.openBF,
				closeBF, ClusterConstants.connection_type,
				ClusterConstants.is_connection_enabled, ClusterConstants.degradedState, connection_id_Test);
	}

	public String fetchIgnoredItemList_fromItem_Check_response(String customer_lat, String customer_lng, String currentBF, String isZoneOpen, String item_refid) {
		Processor processor = Item_Check(customer_lat, customer_lng, ClusterConstants.item_count, getZoneId(item_refid), currentBF,
				ClusterConstants.sla_extra_time, isZoneOpen, item_refid, ClusterConstants.rule_group_type);

		String item_list = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.ignoredItemList");
		return item_list;
	}


	public Processor item_check_response(String customer_lat, String customer_lng, String currentBF,
										 String sla_extra_time, String item_ref_id) {
		Processor response = Item_Check(customer_lat, customer_lng, ClusterConstants.item_count, getZoneId(item_ref_id), currentBF, sla_extra_time,
				ClusterConstants.is_cluster_zone_open, item_ref_id, ClusterConstants.rule_group_type);
		return response;
	}


	public String[] getSelfDelItemCheckPayload(String lat, String lng, String refId, String currentBF)
	{
		String[] payload = {lat, lng, ClusterConstants.item_count, ClusterConstants.zone_id, currentBF,
				ClusterConstants.sla_extra_time, ClusterConstants.is_cluster_zone_open, refId, ClusterConstants.rule_group_type};
		return payload;
	}

	public Processor deleteClusterCache()
	{
		GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryclusterservice","clusterCacheDelete",gameofthrones);
		Processor processor=new Processor(gameOfThronesService, singleHeader(),null,null);
		return processor;
	}

	static HashMap<String, String> singleHeader(){
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("content-type", "application/json");
		requestheaders.put("Cache-Control", "no-cache");
		return requestheaders;
	}
}
