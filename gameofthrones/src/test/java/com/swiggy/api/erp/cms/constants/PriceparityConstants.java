package com.swiggy.api.erp.cms.constants;

/**
 * @author abhijit.p
 */
public interface PriceparityConstants {

    int restaurantId_pp_basic = 30648;
    int  compRestaurantId_basic = 59812;
    int  agentIdMapping = 427694;
    int  agentIdVerification = 427695;
}
