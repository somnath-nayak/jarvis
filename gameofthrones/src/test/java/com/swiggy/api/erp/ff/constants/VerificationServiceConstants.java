package com.swiggy.api.erp.ff.constants;

import java.util.HashMap;

public interface VerificationServiceConstants{

	String omsDBHost = "oms";
	String verificationDBHost = "ff_verification";
	String status[] = {"assigned", "confirmed", "arrived", "pickedup", "reached", "delivered"};
	String verifyAction = "001";
	String customerIdForVerificationService =  "987654";
	String customerIds =  "67414";
	String defaultOrderIncoming = "328";
	String orderIncomingForNewUser = "600";
	String orderIncomingForCrossArea = "328";
	String orderIncomingForHighCOD = "2000";
	int verificationBCP_Pending=0;
	int verificationBCP_Accepted=1;
	int verificationBCP_Rejected=2;
	int verificationBCP_Force_Verified=3;
	String item_id="123";
	String rest_id = "223";

	public default String queryForOrdersInqueue(String order_id, String oeRole){
		return "select order_id from oms_ordersinqueue where order_id = " + order_id + " and waiting_for = '" + oeRole + "';";
	}

	public default String queryForOrdersInqueue(String order_id){
		return "select order_id from oms_ordersinqueue where order_id = " + order_id + ";";
	}
	
	public default String getOeRole(String oe){
		HashMap<String, String> oeRole = new HashMap<String, String>();
		oeRole.put("Verifier", "Verifier");
		oeRole.put("L1Placer", "L1Placer");
		oeRole.put("L2Placer", "L2Placer");
		return oeRole.get(oe);
	}
	
	public default String queryToCheckRelayToRMS(long order_id){
		return "select relay_to_rms from oms_orderstatus where id = (select status_id from oms_order where order_id = "+ order_id +");";
	}
	
}
