package com.swiggy.api.erp.cms.helper;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.google.gson.JsonObject;

import com.jayway.jsonpath.JsonPath;

import com.swiggy.api.erp.cms.constants.CmsConstants;
import com.swiggy.api.erp.cms.constants.Constants;
import com.swiggy.api.erp.cms.constants.MenuConstants;
import com.swiggy.api.erp.cms.constants.SelfServeConstants;
import com.swiggy.api.erp.cms.pojo.Category;
import com.swiggy.api.erp.cms.pojo.FullMenu.Addon_Groups;
import com.swiggy.api.erp.cms.pojo.FullMenu.Addons;
import com.swiggy.api.erp.cms.pojo.FullMenu.Entity;
import com.swiggy.api.erp.cms.pojo.FullMenu.FullMenuPojo;
import com.swiggy.api.erp.cms.pojo.FullMenu.Items;
import com.swiggy.api.erp.cms.pojo.FullMenu.Main_categories;
import com.swiggy.api.erp.cms.pojo.FullMenu.Pricing_combinations;
import com.swiggy.api.erp.cms.pojo.FullMenu.Variant_groups;
import com.swiggy.api.erp.cms.pojo.FullMenu.Variants;
import com.swiggy.api.erp.cms.pojo.SelfServeV1Item.Item;
import com.swiggy.api.erp.cms.pojo.SelfServeV1Item.Item_vo;
import com.swiggy.api.erp.cms.pojo.SelfServeV1Item.SelfServeV1ItemsPojo;
import com.swiggy.api.erp.cms.pojo.StoreOnOff.Meta;
import com.swiggy.api.erp.cms.pojo.StoreOnOff.StoreOnOff;
import com.swiggy.api.erp.cms.pojo.StoreOnOff.UserMeta;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.testng.Assert;



/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.erp.cms.helper
 **/
public class SelfServeHelper<cmsselfserve_updatecategoryticket> {

	static Initialize gameofthrones = new Initialize();
	BaseServiceHelper baseServiceHelper = new BaseServiceHelper();
	//int restID = baseServiceHelper.createRestaurantAndReturnID();
	String agentID, authKey, authUserID;
	static String menu;

	private HashMap<String, String> getAgentControllerHeaders(String auth_key) {
		HashMap<String, String> headers =  new HashMap<>();
		headers.put("Content-Type", "application/json");
		headers.put("Authorization",SelfServeConstants.agent_authorization_key);
		headers.put("app-secret",SelfServeConstants.app_secret);
		headers.put("auth-key",auth_key);
		return headers;
	}
	private HashMap<String, String> getheadersforticketdetails() {
		HashMap<String, String> headers =  new HashMap<>();
		headers.put("Content-Type", "application/json");
		headers.put("Authorization",SelfServeConstants.agent_authorization_key);
		headers.put("app-secret",SelfServeConstants.app_secret);
		return headers;
	}

	private static HashMap<String, String> getVendorControllerHeaders () {
		HashMap<String, String> headers =  new HashMap<>();
		headers.put("Content-Type", "application/json");
		headers.put("Authorization",SelfServeConstants.vendor_authorization_key);
		return headers;
	}

	private static HashMap<String, String> getAgentControllerHeaders() {
		HashMap<String, String> headers =  new HashMap<>();
		headers.put("Content-Type", "application/json");
		headers.put("Authorization",SelfServeConstants.agent_authorization_key);
		return headers;
	}
	
	private HashMap<String, String> getAgentControllerwithusername(String auth_key) {
		HashMap<String, String> headers =  new HashMap<>();
		headers.put("Content-Type", "application/json");
		headers.put("Authorization",SelfServeConstants.agent_authorization_key);
		headers.put("app-secret",SelfServeConstants.app_secret);
		headers.put("auth-username",SelfServeConstants.authUsername);
		headers.put("auth-key",auth_key);
		return headers;
	}

	private HashMap<String, String> getDefaultHeader() {
		HashMap<String, String> requestHeader = new HashMap<String, String>();
		requestHeader.put("Content-Type", "application/json");
		return requestHeader;
	}

	public String generateName() {
		String name = "SelfServe_"+baseServiceHelper.getTimeStampRandomised();
		System.out.println("--NAME ::: "+name);
		return name;
	}

	public Processor addItemHelper(String json, String restID) {
		String[] payload = {json};
		String[] params = {restID};
		GameOfThronesService gots = new GameOfThronesService("cmsselfserve", "additemrestaurant", gameofthrones);
		Processor processor = new Processor(gots, getVendorControllerHeaders(), payload,params);
		return processor;
	}

	public Processor updateItemHelper(String json,String restID, String itemID) {
		String[] payload = {json};
		String[] params = {restID, itemID};
		GameOfThronesService gots = new GameOfThronesService("cmsselfserve", "updateitem", gameofthrones);
		Processor processor = new Processor(gots, getVendorControllerHeaders(), payload,params);
		return processor;
	}

	public Processor deleteItemHelper(String restID, String itemID) {
		String[] params = {restID, itemID};
		GameOfThronesService gots = new GameOfThronesService("cmsselfserve", "deleteitem", gameofthrones);
		Processor processor = new Processor(gots, getVendorControllerHeaders(),null,params);
		return processor;
	}

	public Processor submitTicketHelper(String json, String restID) {
		String[] payload = {json};
		String[] params = {restID};
		GameOfThronesService gots = new GameOfThronesService("cmsselfserve", "triggerevent", gameofthrones);
		Processor processor = new Processor(gots, getVendorControllerHeaders(), payload,params);
		return processor;
	}

	public Processor cancelTicketHelper(String ticketID) {
		String[] params = {ticketID};
		GameOfThronesService gots = new GameOfThronesService("cmsselfserve", "cancelticket", gameofthrones);
		Processor processor = new Processor(gots, getVendorControllerHeaders(), null,params);
		return processor;
	}

	public Processor getTicketDetailsHelper(String ticketID, String auth_key) {
		String[] params = {ticketID};
		GameOfThronesService gots = new GameOfThronesService("cmsselfserve", "getticketdetails", gameofthrones);
		Processor processor = new Processor(gots, getAgentControllerHeaders(auth_key), null,params);
		return processor;
	}

	public Processor approveTicketHelper(String json,String ticketID, String auth_key) {
		String[] payload = {json};
		String[] params = {ticketID};
		GameOfThronesService gots = new GameOfThronesService("cmsselfserve", "approveticket", gameofthrones);
		Processor processor = new Processor(gots, getAgentControllerHeaders(auth_key), payload,params);
		return processor;
	}

	public Processor approveWithEditsTicketHelper(String json,String ticketID, String auth_key) {
		String[] payload = {json};
		String[] params = {ticketID};
		GameOfThronesService gots = new GameOfThronesService("cmsselfserve", "approvewitheditsticket", gameofthrones);
		Processor processor = new Processor(gots, getAgentControllerHeaders(auth_key), payload,params);
		return processor;
	}

	public Processor rejectTicketHelper(String json,String ticketID, String auth_key) {
		String[] payload = {json};
		String[] params = {ticketID};
		GameOfThronesService gots = new GameOfThronesService("cmsselfserve", "rejectticket", gameofthrones);
		Processor processor = new Processor(gots, getAgentControllerHeaders(auth_key), payload,params);
		return processor;
	}

	public Processor assignTicketHelper(String json,String ticketID, String auth_key) {
		String[] payload = {json};
		String[] params = {ticketID};
		GameOfThronesService gots = new GameOfThronesService("cmsselfserve", "assignticket", gameofthrones);
		Processor processor = new Processor(gots, getAgentControllerHeaders(auth_key), payload,params);
		return processor;
	}

	public String getAgentIDFromDB() {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(Constants.cmsDB);
		List<Map<String, Object>> list = sqlTemplate.queryForList(SelfServeConstants.get_active_agent_id);
		String get_agentID = list.get(0).get("id").toString();
		return get_agentID;
	}

	public String getAuthUserIDFromDB(String agentID) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(Constants.cmsDB);
		String query = SelfServeConstants.get_auth_user_id_of_agent + agentID;
		List<Map<String, Object>> list = sqlTemplate.queryForList(query);
		String get_authUserID = list.get(0).get("auth_user_id").toString();
		return get_authUserID;
	}

	public String getAuthKeyOfAgentFromDB(String agentID) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(Constants.cmsDB);
		String query = SelfServeConstants.get_agent_authKey + agentID;
		List<Map<String, Object>> list = sqlTemplate.queryForList(query);
		String get_agent_auth_key = list.get(0).get("auth_key").toString();
		return get_agent_auth_key;
	}

	public List<Map<String, Object>> getCreatedTicketDetailsFromDB(String parent_resc_id, String name) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(Constants.cmsDB);
		String query = SelfServeConstants.get_created_ticket.replace("${PARENT_RESC_ID}",parent_resc_id).replace("${NAME}",name);
		List<Map<String, Object>> list = sqlTemplate.queryForList(query);
		return list;
	}

	public List<Map<String, Object>> getSubmittedTicketDetailsFromDB(String parent_resc_id, String name) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(Constants.cmsDB);
		String query = SelfServeConstants.get_submitted_ticket.replace("${PARENT_RESC_ID}",parent_resc_id).replace("${NAME}",name);
		List<Map<String, Object>> list = sqlTemplate.queryForList(query);
		return list;
	}

	public List<Map<String, Object>> getAssignedTicketDetailsFromDB(String parent_resc_id, String name) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(Constants.cmsDB);
		String query = SelfServeConstants.get_assigned_ticket.replace("${PARENT_RESC_ID}",parent_resc_id).replace("${NAME}",name);
		List<Map<String, Object>> list = sqlTemplate.queryForList(query);
		return list;
	}

	public List<String> getAllAvailableAgentsFromDB() {
		List<String> returnAgents = new ArrayList<>() ;
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(Constants.cmsDB);
		List<Map<String, Object>> list = sqlTemplate.queryForList(SelfServeConstants.get_all_available_agents);
		for (int i = 0; i < list.size(); i++) {
			returnAgents.add(list.get(i).get("id").toString());
		}
		return returnAgents;
	}

	public Processor enableDisableAgentByAuthUserID(String auth_key, String auth_user_id, boolean enable) {
		String[] params = {auth_user_id,String.valueOf(enable)};
		GameOfThronesService gots = new GameOfThronesService("cmsagentservice", "enabledisableagent", gameofthrones);
		Processor processor = new Processor(gots, getAgentControllerHeaders(auth_key),null,params);
		return processor;
	}

	public Processor availableUnavailableAgentByAuthUserID(String auth_key, String auth_user_id, boolean available) {
		String[] params = {auth_user_id, String.valueOf(available)};
		GameOfThronesService gots = new GameOfThronesService("cmsagentservice", "availableunavailableagent", gameofthrones);
		Processor processor = new Processor(gots, getAgentControllerHeaders(auth_key),null,params);
		return processor;
	}

	public void unavailableAllAvailableAgents() {
		System.out.println("--IN unavailableAllAvailableAgents()");
		List<String> availableAgents = getAllAvailableAgentsFromDB();
		agentID = getAgentIDFromDB();
		authUserID = getAuthUserIDFromDB(agentID);
		authKey = getAuthKeyOfAgentFromDB(agentID);
		System.out.println("--authUserID ::: "+ authUserID);
		System.out.println("-- No. of available Agents: "+availableAgents.size());
		for (int i = 0; i < availableAgents.size(); i++) {
			availableUnavailableAgentByAuthUserID(authKey, authUserID, false);
		}
	}

	public void makeSingleAgentAvailable(String agentId) {
		String authID = getAuthUserIDFromDB(agentId);
		String authkey = getAuthKeyOfAgentFromDB(agentId);
		System.out.println("--AuthID made available ::: "+authID);
		availableUnavailableAgentByAuthUserID(authkey,authID,true);
	}

	public void makeSingleAgentUnavailable(String agentId) {
		String authId = getAuthUserIDFromDB(agentId);
		String authkey = getAuthKeyOfAgentFromDB(agentId);
		System.out.println("--AuthID made unavailable ::: "+authId);
		availableUnavailableAgentByAuthUserID(authkey,authId,false);
	}

	public void makeSingleAgentEnabled (String agentId) {
		String authId = getAuthUserIDFromDB(agentId);
		String authkey = getAuthKeyOfAgentFromDB(agentId);
		System.out.println("--AuthID enabled ::: "+authId);
		enableDisableAgentByAuthUserID(authkey,authId,true);
	}

	public void makeSingleAgentDisabled (String agentId) {
		String authId = getAuthUserIDFromDB(agentId);
		String authkey = getAuthKeyOfAgentFromDB(agentId);
		System.out.println("--AuthID disabled ::: "+authId);
		enableDisableAgentByAuthUserID(authkey,authId,false);
	}

	public Processor getTicketDetailsTMSHelper(String authKey,String ticketID) {
		HashMap<String, String> headers = new HashMap<>();
		headers.put("Content-Type","application/json");
		headers.put("auth-key",authKey);
		headers.put("app-secret", SelfServeConstants.app_secret);
		String[] params = {ticketID};
		GameOfThronesService gots = new GameOfThronesService("cmsticketingservice", "getticket", gameofthrones);
		Processor processor = new Processor(gots, headers, null,params);
		return processor;
	}

	public String getTicketStateTMS(String authKey,String ticketID) {
		String ticket_state = getTicketDetailsTMSHelper(authKey,ticketID).ResponseValidator.GetNodeValue("$.data.state");
		return ticket_state;
	}

	//    public String getGroupIDFromDB(String parent_resc_id, String name) {
	//        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(Constants.cmsDB);
	//        String query = SelfServeConstants.get_assigned_ticket.replace("${PARENT_RESC_ID}",parent_resc_id).replace("${NAME}",name);
	//        List<Map<String, Object>> list = sqlTemplate.queryForList(query);
	//        String group_id = list.get(0).get("group_id").toString();
	//        return group_id;
	//    }

	public Processor findAgentByAuthUserID(String authKey, String authUserID) {
		String[] params = {authUserID};
		GameOfThronesService gots = new GameOfThronesService("cmsagentservice", "findbyauthuserid", gameofthrones);
		Processor processor = new Processor(gots, getAgentControllerHeaders(authKey), null,params);
		return processor;
	}

	public Processor findAgentByAgentID (String authKey, String agentID) {
		String[] params = {agentID};
		GameOfThronesService gots = new GameOfThronesService("cmsagentservice", "findagentbyid", gameofthrones);
		Processor processor = new Processor(gots, getAgentControllerHeaders(authKey), null,params);
		return processor;
	}


	public String getAuthKey(String ticketId){
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
		return String.valueOf(sqlTemplate.queryForList(SelfServeConstants.test_auth_key1));
	}


	public Processor postStoreAvailability(boolean open, String storeId, int partnerType, String ontime, String offtime)
	{
		Processor processor=null;
		HashMap<String, String> headers= getVendorControllerHeaders();
		GameOfThronesService gots = new GameOfThronesService("cmsselfserve", "storeon", gameofthrones);
		ObjectMapper mapper =new ObjectMapper();
		Meta meta = new Meta("gunjan_automation");
		UserMeta userMeta = new UserMeta("VENDOR",meta);
		StoreOnOff storeOnOff;
		if(!open)
		{
			storeOnOff = new StoreOnOff(partnerType,storeId,userMeta,ontime,offtime,false);
		}
		else
			storeOnOff = new StoreOnOff(partnerType,storeId,userMeta,null,null,true);

		try
		{
			processor = new Processor(gots, headers, new String[]{mapper.writeValueAsString(storeOnOff)});
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}

		return processor;
	}




	public static String authUerIdforEnableDisable() {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
		return String.valueOf(sqlTemplate.queryForList(SelfServeConstants.authUserIdForEnableDisable).get(0).get("id"));
	}

	public static String authAuthKeyforEnableDisable(String authUserId) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
		return String.valueOf(sqlTemplate.queryForList(SelfServeConstants.authKeyEnableDisable+authUserId).get(0).get("auth_key"));
	}

	public static String getAuthUser() {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
		return String.valueOf(sqlTemplate.queryForList(SelfServeConstants.authUserIdForEnableDisable).get(0).get("id"));
	}

	public static String authUerIdforEnableDisable(String authUserId) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
		return String.valueOf(sqlTemplate.queryForList(SelfServeConstants.authKeyEnableDisable+authUserId).get(0).get("auth_key"));
	}


	public Processor getMenu(String restID, String status){
		String[] payload = {restID};
		String[] params = {restID, status};
		GameOfThronesService gots = new GameOfThronesService("cmsselfserve", "updateitem", gameofthrones);
		Processor processor = new Processor(gots, getVendorControllerHeaders(), payload,params);
		return processor;
	}

	public Processor getCategoryTicketId(String ticketId,String restId) {
		String[] urlParams = {restId,ticketId};
		GameOfThronesService service = new GameOfThronesService("cmsselfserve", "getCategoryTickets", gameofthrones);
		Processor processor = new Processor(service, getVendorControllerHeaders(), null,urlParams);
		return processor;
	}

	public Processor updateCategory(String restId,String catId,String type,String catName) {
		String[] urlParams = {restId,catId,type};
		String[] payloadParams = {catId,catName,type};
		GameOfThronesService service = new GameOfThronesService("cmsselfserve", "updateCategory", gameofthrones);
		Processor processor = new Processor(service, getVendorControllerHeaders(), payloadParams,urlParams);
		return processor;
	}


	/*  public Processor getMenu(String restID, String status){
	        String[] payload = {restID};
	        String[] params = {restID, status};
	        GameOfThronesService gots = new GameOfThronesService("cmsselfserve", "updateitem", gameofthrones);
	        Processor processor = new Processor(gots, getVendorControllerHeaders(), payload,params);
	        return processor;
	    }*/


    public Processor processMenuService (String restId){
        Processor processor = null;
        String[] urlparams = new String[1];
        urlparams[0] = restId;
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("Authorization","Basic YWdlbnQtZGFzaGJvYXJkOnNlbGZzZXJ2ZS1zeXN0ZW0=");


            GameOfThronesService got = new GameOfThronesService("cmsselfserve", "getitemidforslots", gameofthrones);

            processor = new Processor(got, headers, null, urlparams);


        return processor;

    }

    public int getItemIdMenu(String menuServiceResponse) throws Exception {
        int itemId = 0;
        JSONObject object = new JSONObject(menuServiceResponse);
        JSONObject dataObject = object.getJSONObject("data");
        JSONObject restaurantModelObject = dataObject.getJSONObject("restaurantModel");
        JSONArray categoriesArray = restaurantModelObject.getJSONArray("categories");
        JSONObject firstObjectOfCategories = null;
        if(categoriesArray.length()>0){
            firstObjectOfCategories = categoriesArray.getJSONObject(0);
            JSONArray subCategoriesArray = firstObjectOfCategories.getJSONArray("subCategories");
            if(subCategoriesArray.length()>0){
                JSONObject firstObjectOfSubCategories = subCategoriesArray.getJSONObject(0);
                JSONArray arrayOfMenu = firstObjectOfSubCategories.getJSONArray("menu");
                JSONObject firstObjectOfMenu = arrayOfMenu.getJSONObject(0);
                itemId = (Integer) firstObjectOfMenu.get("id");
            }else{
                itemId= (Integer) firstObjectOfCategories.get("id");
            }
        }return itemId;

    }




	public Processor deleteCategory(String restId,String catId) {
		String[] urlParams = {restId,catId,SelfServeConstants.creatdOrUpdatedBy[0]};
		GameOfThronesService service = new GameOfThronesService("cmsselfserve", "deleteCategory", gameofthrones);
		Processor processor = new Processor(service, getVendorControllerHeaders(), null,urlParams);
		return processor;
	}
	//
	public static String getCatId(String restId) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
		List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.catId + restId+CmsConstants.catId1 );
		return list.get(0).get("id").toString();
	}

	public static String getSubCatId(String restId) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
		List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.subCatId + restId+CmsConstants.subCatId1 );
		return list.get(0).get("id").toString();
	}

	public static Processor getSubCatIdAPI(String categoryID) {
		//getCatbyRestId
		String[] urlParams = {categoryID};
		GameOfThronesService service = new GameOfThronesService("cmsselfserve", "getSubbyCatId", gameofthrones);
		Processor processor = new Processor(service, getAgentControllerHeaders(), null,urlParams);
		processor.ResponseValidator.GetBodyAsText();
		return processor;
	}
	public static  Processor getCatIdAPI(String restID) {
		String[] urlParams = {restID};
		GameOfThronesService service = new GameOfThronesService("cmsselfserve", "getCatbyRestId", gameofthrones);
		Processor processor = new Processor(service, getAgentControllerHeaders(), null,urlParams);
		return processor;
	}

	public static void setCategoryActive(String restId,String  catId) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
		List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.catdetails + catId+CmsConstants.catdetails1 );

		Category c=new Category();
		c.setDefaultValues();
		c.setActive(true);
		c.setName("Starters"+11);
		c.setRestaurant_id(Integer.parseInt(restId));
		c.setParent_category_id(Integer.parseInt(list.get(0).get("parent").toString()));
		c.setDescription(list.get(0).get("description").toString());
		c.setOrder(Integer.parseInt(list.get(0).get("order").toString()));
		c.setType(list.get(0).get("type").toString());
		//new String[]{jsonHelper.getObjectToJSON(variantgrouppojo)}
		BaseServiceHelper bh=new BaseServiceHelper();
		ObjectMapper mapperObj = new ObjectMapper();
		String payload=null; 
		try {
			payload = mapperObj.writeValueAsString(c);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		bh.updateCategoryHelper(payload,catId);

	}

	public Processor addSelfServeV1Item(String restId,String payload) {
		GameOfThronesService service = new GameOfThronesService("cmsselfserve", "addItemSelfServeV1", gameofthrones);
		return new Processor(service, getVendorControllerHeaders(),new String[]{payload.toString()},new String[] {restId});

	}

	public static  String getCatOrSubCatIdHelper(String id,String type) {
		String response,catOrSubCatId;
		if(type=="SubCat")
		{
			response = getSubCatIdAPI(id).ResponseValidator.GetBodyAsText();
			catOrSubCatId = JsonPath.read(response, "$.data.search_data.[0].id").toString();

		}
		else
		{
			response = getCatIdAPI(id).ResponseValidator.GetBodyAsText();
			catOrSubCatId = JsonPath.read(response, "$.data.search_data.[0].id").toString();

		}
		return catOrSubCatId;
	}

	public SelfServeV1ItemsPojo createDefaultSelfServePojo() throws IOException {
		SelfServeV1ItemsPojo selfservepojo = new SelfServeV1ItemsPojo();
		selfservepojo.build();
		Item_vo item_vo =new Item_vo();
		Item item=new Item();
		item.build();
		item_vo.setItem(item);
		selfservepojo.setItem_vo(item_vo);
		return selfservepojo;

	}


	// getMenu
	public static Processor getMenu(String restId) {
		String[] urlParams = {restId};
		GameOfThronesService service = new GameOfThronesService("cmsselfserve", "getMenu", gameofthrones);
		Processor processor = new Processor(service, getAgentControllerHeaders(), null,urlParams);
		return processor;
	}

	public static  String[] getItemId(String restId) {
		String[] itemId=null;
		menu=getMenu(restId).ResponseValidator.GetBodyAsText();
		itemId=JsonPath.read(menu, "$.data.restaurantModel.categories[*].subCategories[*].menu[*].id").toString().replace("[","").replace("]","").replace("\"","").split(","); 
		return itemId;
	}

	public Processor updateItemSelfServeV1(String restId,String itemId,String payload) {
		GameOfThronesService service = new GameOfThronesService("cmsselfserve", "updateItemSelfServeV1", gameofthrones);
		return new Processor(service, getVendorControllerHeaders(),new String[]{payload.toString()},new String[] {restId,itemId});

	}

	public static String[] getCatIdforItem(String itemId) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
		List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.catandsubcatforItem + itemId+CmsConstants.catandsubcatforItem1);
		String[] catSubcatId={list.get(0).get("parent").toString(),list.get(0).get("id").toString()};
		return catSubcatId;
	}
	public static String getdiffCatIdforItem(String catid) {
		String catSubcatId=null;
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
		List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.differentsubcat + catid+CmsConstants.differentsubcat1);
		if(list.size()>1)
			catSubcatId=list.get(1).get("id").toString();
		else 
			catSubcatId=list.get(0).get("id").toString();
		return catSubcatId;
	}

	public Processor submitTicket(String restId,String payload) {
		GameOfThronesService service = new GameOfThronesService("cmsselfserve", "submitticket", gameofthrones);
		return new Processor(service, getVendorControllerHeaders(),new String[]{payload.toString()},new String[] {restId});

	}

	public Processor cancleticket(String restId,String ticketId) {
		GameOfThronesService service = new GameOfThronesService("cmsselfserve", "cancleticket", gameofthrones);
		return new Processor(service, getVendorControllerHeaders(),null,new String[] {restId,ticketId});


	}

	public Processor updatecategoryticketstate(String ticketId,String ticketstate,String payload,String agentid) {
		String authKey=getAuthKeyOfAgentFromDB(agentid);
		GameOfThronesService service = new GameOfThronesService("cmsselfserve", "updatecategoryticket", gameofthrones);
		return new Processor(service, getAgentControllerwithusername(authKey),new String[]{payload.toString()},new String[] {ticketId,ticketstate});

	}

	public Processor updateitemticketstate(String ticketId,String ticketstate,String payload,String agentid) {
		String authKey=getAuthKeyOfAgentFromDB(agentid);
		GameOfThronesService service = new GameOfThronesService("cmsselfserve", "updateitemticket", gameofthrones);
		return new Processor(service, getAgentControllerwithusername(authKey),new String[]{payload.toString()},new String[] {ticketId,ticketstate});

	}

	public Processor getticketbyagentid(String agentid,String tickettype) {
		GameOfThronesService service = new GameOfThronesService("cmsselfserve", "getticketsbyagentid", gameofthrones);
		return new Processor(service,getheadersforticketdetails(),null,new String[] {agentid,tickettype});
	}


	public Processor getticketdetailsbyTicketId(String ticketId) {
		GameOfThronesService service = new GameOfThronesService("cmsticketingservice", "getticket", gameofthrones);
		return new Processor(service, getheadersforticketdetails(),null,new String[] {ticketId});
	}

	public Processor getrestaurantMenu(String restId,boolean is_available) {
		GameOfThronesService service = new GameOfThronesService("cmsselfserve", "getticketsbyagentid", gameofthrones);
		return new Processor(service,getheadersforticketdetails(),null,new String[] {restId,Boolean.toString(is_available)});
	}


	public Processor pollingforAgentAssignemett(int count, String agentId,String TicketType) throws InterruptedException{
		boolean agentassigned=false;
		int i=-1;
		Processor p=null;
		do {
			i++;
			p=getticketbyagentid(agentId,TicketType);

			if (p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.search_data[*]").length()!=0) {
				agentassigned = true;
				break;
			}
			Thread.sleep(1000);

		 }while(i<count);

		return p;

	}

	public Processor pollingforticketAssignment(int count, String ticketId) throws InterruptedException{
		boolean ticketassigned=false;
		int i=-1;
		Processor p=null;
		do {
			i++;
			p=getticketdetailsbyTicketId(ticketId);

			if (p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.state").toString().equalsIgnoreCase("ASSIGNED")) {
				ticketassigned = true;
				break;
			}
			Thread.sleep(1000);

		}while(i<count);

		return p;

	}

	public Processor cancleTicket(String restId,String ticketId) {
		GameOfThronesService service = new GameOfThronesService("cmsselfserve", "cancleticket", gameofthrones);
		return new Processor(service, getVendorControllerHeaders(),null,new String[] {restId,ticketId});

	}

	public Processor ticke(String restId,String ticketId) {
		GameOfThronesService service = new GameOfThronesService("cmsselfserve", "cancleticket", gameofthrones);
		return new Processor(service, getVendorControllerHeaders(),null,new String[] {restId,ticketId});

	}
}