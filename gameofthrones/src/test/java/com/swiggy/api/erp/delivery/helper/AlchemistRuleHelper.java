package com.swiggy.api.erp.delivery.helper;

import com.swiggy.api.erp.delivery.constants.DEAlchemistConstants;
import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.Processor;
import org.springframework.jdbc.BadSqlGrammarException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AlchemistRuleHelper {

    DeliveryServiceApiWrapperHelper api_helper = new DeliveryServiceApiWrapperHelper();
    Map<String,String> expressionMap=null;
    HashMap<String,String> ruleDetailsMap=null;
    ArrayList<String> ruleIds=null;
    static int TC_Counter=0;
    List<String> allActiveRules = new ArrayList<>();
    Processor processor = null;
    Random random=new Random();

    public HashMap<String,String> Beforerule(HashMap<String,String> finalMap)
    {
        expressionMap=expressionParser(finalMap.get("expression"));
        finalMap.putAll(expressionMap);
        return finalMap;
    }

    public HashMap<String,String> getGoldenData(HashMap<String,String> finalMap)
    {
        expressionMap=breakExpressions(finalMap.get("orderJsonValues"));
        finalMap.putAll(expressionMap);
        return finalMap;
    }

    public HashMap<String,String> verifyActiveRuleInDB(String expression){
        ruleDetailsMap=new HashMap<>();
        String IncentiveRulesQuery="select * from incentive_rules where active=1 and expression like '%";
        String RuleId = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).queryForList(IncentiveRulesQuery+expression+"%'").get(0).get("id").toString();
        String RuleExpression = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).queryForList(IncentiveRulesQuery+expression+"%'").get(0).get("expression").toString();
        String ZoneIdFetchQuery="select * from rule_zone_mapping where rule_id=";
        String zoneId=SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).queryForList(ZoneIdFetchQuery+RuleId).get(0).get("zone_id").toString();
        String CityFetchQuery="select * from area where id=";
        String cityId=SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForList(CityFetchQuery+zoneId).get(0).get("city_id").toString();
        String computeOn = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).queryForList(IncentiveRulesQuery+expression+"%'").get(0).get("compute_on").toString();
        ruleIds.add(RuleId);
        ruleDetailsMap.put("ruleid",RuleId);
        ruleDetailsMap.put("city",cityId);
        ruleDetailsMap.put("zone",zoneId);
        ruleDetailsMap.put("event",computeOn);
        ruleDetailsMap.put("expression", RuleExpression);

        System.out.println(cityId);
        System.out.println(RuleId);
        System.out.println(zoneId);
        System.out.println(computeOn);
        System.out.println(RuleExpression);

        return ruleDetailsMap;
    }

    public HashMap<String,String> createRules(String zone,String expression,String computeOn){
        ruleIds=new ArrayList<>();
        TC_Counter++;
        String incentive_group= DEAlchemistConstants.IncentiveGroup;
        ruleDetailsMap= new HashMap<>();
        String bonus= String.valueOf((random.nextInt(25)));
        String ruleNameInDB = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).queryForList("select * from incentive_rules where rule_name like '%AutomatedRules%' order by id desc").get(0).get("rule_name").toString();
        int counter= Integer.parseInt(ruleNameInDB.replaceAll("[^0-9]", ""));
        counter++;
        String ruleName="AutomatedRules"+counter;
        String zoneName = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForList("select * from zone where id=" + zone).get(0).get("name").toString();
        processor = api_helper.createRule(ruleName,bonus,incentive_group,zoneName,expression,computeOn);
        if (processor.ResponseValidator.GetResponseCode() == 200) {
            String ruleId = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).queryForList("select * from incentive_rules where rule_name='" + ruleName +"'").get(0).get("id").toString();
            String CityfetchQuery="select * from area where id=";
            String cityId=SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForList(CityfetchQuery+zone).get(0).get("city_id").toString();
            System.out.println("Rule id " + ruleId + " is created successfully");
            ruleIds.add(ruleId);
            ruleDetailsMap.put("ruleid",ruleId);
            ruleDetailsMap.put("zone_id",zone);
            ruleDetailsMap.put("event",computeOn);
            ruleDetailsMap.put("expression", expression);
            ruleDetailsMap.put("city_id",cityId);
            ruleDetailsMap=Beforerule(ruleDetailsMap);
            return ruleDetailsMap;
        }
        else{
            System.out.println(processor.ResponseValidator.GetBodyAsText());
            return null;
        }
    }

    public HashMap<String,String> createRulesWithMap(HashMap<String,String> createNewRuleMap){
        ruleIds=new ArrayList<>();
        TC_Counter++;
        String incentive_group= DEAlchemistConstants.IncentiveGroup;
        createNewRuleMap.put("group",incentive_group);
        ruleDetailsMap= new HashMap<>();
        createNewRuleMap.put("bonus",String.valueOf((random.nextInt(25))));
        String ruleNameInDB = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).queryForList("select * from incentive_rules where rule_name like '%AutomatedRules%' order by id desc").get(0).get("rule_name").toString();
        int counter= Integer.parseInt(ruleNameInDB.replaceAll("[^0-9]", ""));
        counter++;
        createNewRuleMap.put("name","AutomatedRules"+counter);
        createNewRuleMap.put("zoneName",SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForList("select * from zone where id=" + createNewRuleMap.get("zone")).get(0).get("name").toString());
        processor = api_helper.createRuleWithMap(createNewRuleMap);
        if (processor.ResponseValidator.GetResponseCode() == 200) {
            String ruleId = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).queryForList("select * from incentive_rules where rule_name='" + createNewRuleMap.get("name") +"'").get(0).get("id").toString();
            String CityfetchQuery="select * from area where id=";
            String cityId=SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForList(CityfetchQuery+createNewRuleMap.get("zone")).get(0).get("city_id").toString();
            System.out.println("Rule id " + ruleId + " is created successfully");
            ruleIds.add(ruleId);
            ruleDetailsMap.put("ruleid",ruleId);
            ruleDetailsMap.put("zone_id",createNewRuleMap.get("zone"));
            ruleDetailsMap.put("event",createNewRuleMap.get("computeOn"));
            ruleDetailsMap.put("expression", createNewRuleMap.get("expression"));
            ruleDetailsMap.put("city_id",cityId);
            ruleDetailsMap.put("orderJsonValues",createNewRuleMap.get("orderJsonValues"));
            ruleDetailsMap=getGoldenData(ruleDetailsMap);
            return ruleDetailsMap;
        }
        else{
            System.out.println(processor.ResponseValidator.GetBodyAsText());
            return null;
        }
    }



    public void disableRulesCreatedByCurrentTestCase(){
        String allRules=String.join(",",ruleIds);
        String disableRulesQuery="update incentive_rules set active=0 where id IN (";
        try{
            SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).update(disableRulesQuery+allRules+")");
        }
        catch (BadSqlGrammarException e)
        {
            System.out.println("No rules available to disable");
        }
    }

    public boolean disableAllActiveRulesWhichAreActivateBeforeSuiteExecution(){
        List<Map<String, Object>> activeRules =null;
        try {
            activeRules = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).queryForList("select * from incentive_rules where active=1");
        }
        catch(NullPointerException np){
            System.out.println("Currently no rules are active");
            return true;
        }
        for (int i=0;i<activeRules.size();i++){
            allActiveRules.add(activeRules.get(i).get("id").toString());
        }
        String RulesList = String.join(",", allActiveRules);
        System.out.println("All pre Active Rules Before Suite run are deactivated: "+ RulesList);
        String disableActiveRulesQuery="update incentive_rules set active=0 where id IN (";
        if(allActiveRules.size()>0)
            SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).update(disableActiveRulesQuery +RulesList+")");
        return true;
    }

    public List<String> getAllActiveRulesBeforeSuiteExecution(){
        if(allActiveRules.size()>0)
            return allActiveRules;
        else {
            System.out.println("Currently no rules are active");
            return Collections.emptyList();
        }
    }

    public void enableAllRulesWhichWereDeactivatedBeforeSuiteExecution(){
        String enableActiveRulesQuery="update incentive_rules set active=1 where id IN (";
        String RulesList = String.join(",", allActiveRules);
        if(allActiveRules.size()>0)
            SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).update(enableActiveRulesQuery +RulesList+")");
        System.out.println("All pre Active Rules Before Suite run are Activated again: "+ RulesList);
    }

    public void fireEvent(String event,String de_id){
        Date date = new Date();
        int curDay=date.getDate();
        int curMonth=date.getMonth()+1;
        int curYear= date.getYear()+1900;
        String time;
        if(event.equals("eod")){
            curDay++;
        }
        else if(event.equals("eow")){
            String strDateFormat = "dd";
            SimpleDateFormat sdf = new SimpleDateFormat(strDateFormat);
            date.setDate(date.getDate() + (1 + 7 - date.getDay()) % 7);
            curDay=Integer.parseInt(sdf.format(date));
            strDateFormat = "MM";
            sdf = new SimpleDateFormat(strDateFormat);
            curMonth=Integer.parseInt(sdf.format(date));
        }
        else if(event.equals("eom")){
            curMonth++;
            curDay=01;
        }
        time=String.valueOf(curYear)+"-"+String.valueOf(curMonth)+"-"+String.valueOf(curDay)+" 06:15:00";
        processor =api_helper.fireAlchemistAggregateEvents(event,time,de_id);
    }

    public Map<String,String> expressionParser(String expression)
    {
        Map<String, String> finalMap=new HashMap<>();
        expression=expression.replaceAll("\\s","");
        String[] expString=expression.split("and");
        for(String str :expString)
        {
            Map<String,String> exptemp=ExpressionInterpretor(str);
            Map.Entry<String,String> entry = exptemp.entrySet().iterator().next();
            String key= entry.getKey();
            if(!finalMap.containsKey(key))
            {
                finalMap.putAll(exptemp);
            }
        }
        return finalMap;
    }

    public Map<String,String> ExpressionInterpretor(String exp)
    {
        Integer value=0;
        Map<String,String> allExps= new HashMap<>();
        Pattern pattern= Pattern.compile("[<>=]");
        Pattern pattern2= Pattern.compile("([0-9]*[.])?[0-9]+");
        Matcher matcher= pattern.matcher(exp.toString());
        Matcher matcher2= pattern2.matcher(exp.toString());
        matcher.find();
        matcher2.find();
        String operator=matcher.group();
        String no=matcher2.group();
        if(operator.equalsIgnoreCase(">"))
        {
            try
            {
                value= Integer.parseInt(no)+1;
            }
            catch (Exception e)
            {
                Double d=Double.parseDouble(no);
                value=d.intValue()+1;
            }
        }
        else if (operator.equalsIgnoreCase("<"))
        {
            try
            {
                value= Integer.parseInt(no)-1;
            }
            catch (Exception e)
            {
                Double d=Double.parseDouble(no);
                value=d.intValue()-1;
            }
        }
        else if (operator.equalsIgnoreCase("="))
        {
            try
            {
                value= Integer.parseInt(no);
            }
            catch (Exception e)
            {
                Double d=Double.parseDouble(no);
                value=d.intValue();
            }
        }
        allExps.put(exp.split(operator)[0],value.toString());
        return allExps;
    }

    public Map<String,String> breakExpressions(String expression)
    {
        Map<String, String> finalMap=new HashMap<>();
        expression=expression.replaceAll("\\s","");
        String[] expString=expression.split("and");
        for(String str :expString)
        {
            Map<String,String> exptemp=ValueInterpretor(str);
            Map.Entry<String,String> entry = exptemp.entrySet().iterator().next();
            String key= entry.getKey();
            if(!finalMap.containsKey(key))
            {
                finalMap.putAll(exptemp);
            }
        }
        return finalMap;
    }

    public Map<String,String> ValueInterpretor(String exp)
    {
        Integer value=0;
        Map<String,String> allExps= new HashMap<>();
        Pattern pattern= Pattern.compile("[<>=]");
        Pattern pattern2= Pattern.compile("([0-9]*[.])?[0-9]+");
        Matcher matcher= pattern.matcher(exp.toString());
        Matcher matcher2= pattern2.matcher(exp.toString());
        matcher.find();
        matcher2.find();
        String operator=matcher.group();
        String no=matcher2.group();
        if(operator.equalsIgnoreCase(">"))
        {
            try
            {
                value= Integer.parseInt(no)+1;
            }
            catch (Exception e)
            {
                Double d=Double.parseDouble(no);
                value=d.intValue()+1;
            }
        }
        else if (operator.equalsIgnoreCase("<"))
        {
            try
            {
                value= Integer.parseInt(no)-1;
            }
            catch (Exception e)
            {
                Double d=Double.parseDouble(no);
                value=d.intValue()-1;
            }
        }
        else if (operator.equalsIgnoreCase("="))
        {
            try
            {
                value= Integer.parseInt(no);
            }
            catch (Exception e)
            {
                Double d=Double.parseDouble(no);
                value=d.intValue();
            }
        }
        allExps.put(exp.split(operator)[0],value.toString());
        return allExps;
    }

}
