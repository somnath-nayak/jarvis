package com.swiggy.api.erp.ff.tests.manualTaskService;

import com.swiggy.api.erp.ff.dp.manualTaskService.CreateTaskAPIData;
import com.swiggy.api.erp.ff.helper.ManualTaskServiceHelper;
import framework.gameofthrones.JonSnow.Processor;
import org.apache.log4j.Logger;
import org.testng.annotations.Test;
import org.testng.Assert;
import java.util.HashMap;
/**
 Created by Narendra on 06-Mar-2019.
 */


public class NullifyTaskAPITests extends CreateTaskAPIData {

    ManualTaskServiceHelper helper = new ManualTaskServiceHelper();
    Logger log = Logger.getLogger(NullifyTaskAPITests.class);

    @Test(priority = 1, dataProvider = "createTaskData", groups = {"sanity", "regression"},description = "Nullify Tasks API")
    public void nullifyTaskAPITests(String orderId, String reason, int reasonId, String reasonOE, String subReason, int subReasonid, String subReasonOE, String source, HashMap<String, String> expectedResponseMap, int unassignmentReasonId) throws InterruptedException {
        log.info("************************ nullifyTaskAPITests started ***********************");

        Processor response = helper.createTask(orderId, reason, subReason, source);
        String nullifyTaskId, taskId =null;
        helper.assertCreateTaskApiResponses(response, expectedResponseMap);
        Thread.sleep(1000);
        if (response.ResponseValidator.GetResponseCode() == 200){
            taskId = response.ResponseValidator.GetNodeValue("data.task_id");
            helper.assertDataAfterTaskCreation(orderId, source, taskId, reasonId, subReasonid, reasonOE, subReasonOE);
        }

        Processor nullifyResponse = helper.nullifyTask(unassignmentReasonId);
        if (nullifyResponse.ResponseValidator.GetResponseCode() == 200){
            nullifyTaskId = response.ResponseValidator.GetNodeValue("data");
            String statusMessage = response.ResponseValidator.GetNodeValue("status_message");
            Assert.assertEquals( statusMessage, "Success", "Status message is not as expected ");
            Assert.assertEquals( taskId, nullifyTaskId, "taskId are not matching ");
            helper.assertDataAfterNullifyTask(orderId, source, taskId, reasonId, subReasonid, reasonOE, subReasonOE);
        }
        log.info("####################### nullifyTaskAPITests compleated ##########################");
    }
}
