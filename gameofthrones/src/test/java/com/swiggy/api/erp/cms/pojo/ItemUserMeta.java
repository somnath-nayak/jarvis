package com.swiggy.api.erp.cms.pojo;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;

public class ItemUserMeta {

    @JsonProperty("source")
    private String source;
    @JsonProperty("meta")
    private ItemMeta meta;

    /**
     * No args constructor for use in serialization
     *
     */
    public ItemUserMeta() {
    }

    /**
     *
     * @param source
     * @param meta
     */
    public ItemUserMeta(String source, ItemMeta meta) {
        super();
        this.source = source;
        this.meta = meta;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public ItemMeta getMeta() {
        return meta;
    }

    public void setMeta(ItemMeta meta) {
        this.meta = meta;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("source", source).append("meta", meta).toString();
    }

}



