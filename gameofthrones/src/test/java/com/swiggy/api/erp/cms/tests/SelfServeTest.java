package com.swiggy.api.erp.cms.tests;

import com.swiggy.api.erp.cms.constants.SelfServeConstants;
import com.swiggy.api.erp.cms.dp.SelfServeDP;
import com.swiggy.api.erp.cms.helper.SelfServeHelper;
import com.swiggy.api.erp.cms.pojo.SelfServeTriggerEventForTicketAssignment.Ticket;
import com.swiggy.api.erp.cms.pojo.SelfServeTriggerEventForTicketAssignment.TriggerEventForTicketAssignment;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import io.gatling.core.json.Json;
import org.apache.xpath.operations.Bool;
import org.glassfish.hk2.api.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.erp.cms.tests
 **/
public class SelfServeTest extends SelfServeDP {

    private static Logger log = LoggerFactory.getLogger(SelfServeTest.class);
    SelfServeHelper selfServeHelper = new SelfServeHelper();
    String g_ticket_id, g_auth_key, g_agent_id,g_auth_user_id;


    @Test(dataProvider = "addItemDP", priority = 1,groups = {"SelfServe", "E2E", "Regression"},description = "validate add Item E2E",enabled = true)
    public void addItem(String json, String state, String name, String restID, boolean hitAddItem, boolean hitTriggerEvent, boolean hitEnableAgent, boolean enableAgentAction, String ticketState, String description) throws IOException {
        log.info("-- Description ::: "+description);
        log.info("-- Name ::: "+name);
        log.info(json);
        JsonHelper jsonHelper = new JsonHelper();
        List<Map<String, Object>> DBlist = new ArrayList<>();
        String ticketID="";
        SoftAssert softAssert = new SoftAssert();
        if (hitAddItem) {
            Processor p = selfServeHelper.addItemHelper(json, restID);
            int get_status = p.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            boolean get_data = p.ResponseValidator.GetNodeValueAsBool("$.data");
            String get_statusMessage = p.ResponseValidator.GetNodeValue("$.statusMessage");
            //assert
            softAssert.assertEquals(get_status, SelfServeConstants.statusOne, "Add Item status not same");
            softAssert.assertEquals(get_data, true, "Add Item data not same");
            softAssert.assertEquals(get_statusMessage, SelfServeConstants.message_success, "Add Item message not same");
        }

        if (state.equalsIgnoreCase("NEW")) {
            DBlist = selfServeHelper.getCreatedTicketDetailsFromDB(restID,name);
        } else if (state.equalsIgnoreCase("SUBMITTED")) {
            DBlist = selfServeHelper.getSubmittedTicketDetailsFromDB(restID,name);
        } else if (state.equalsIgnoreCase("ASSIGNED")) {
            DBlist = selfServeHelper.getAssignedTicketDetailsFromDB(restID,name);
        }
        if(!DBlist.isEmpty()) {
            ticketID = DBlist.get(0).get("id").toString();
            g_ticket_id = ticketID;
            log.info("--TICKETID FROM DB ::: " + ticketID);
        } else {
            softAssert.assertTrue(false, "--DB response list is empty for the given test case");
            softAssert.assertAll();
        }

        Processor p1 = selfServeHelper.getTicketDetailsTMSHelper(selfServeHelper.getAuthKey(ticketID), ticketID);
        String get_state = p1.ResponseValidator.GetNodeValue("$.data.state").replace("{","").replace("[","").replace("}","").replace("]","").replace("\"","").trim();
        String get_subject = p1.ResponseValidator.GetNodeValue("$.data.subject").replace("{","").replace("[","").replace("}","").replace("]","").replace("\"","").trim();
        String get_id = p1.ResponseValidator.GetNodeValue("$.data.uniqueId").replace("{","").replace("[","").replace("}","").replace("]","").replace("\"","").trim();
        int get_statusCode = p1.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String get_statusMsg = p1.ResponseValidator.GetNodeValue("$.statusMessage");
        //assert
        softAssert.assertEquals(get_statusCode,SelfServeConstants.statusOne, "Status not same");
        softAssert.assertNull(get_statusMsg, "Message not same");
        softAssert.assertEquals(get_state,state, "State Not same");
        softAssert.assertEquals(get_id,ticketID, "TicketID not same");
        softAssert.assertEquals(get_subject,name, "Name not same");
        if (hitTriggerEvent) {
            Ticket ticket = new Ticket();
            ticket.setId(Integer.parseInt(ticketID));
            List<Ticket> ticket_ID = new ArrayList<>();
            ticket_ID.add(ticket);
            TriggerEventForTicketAssignment triggerEventForTicketAssignment = new TriggerEventForTicketAssignment();
            triggerEventForTicketAssignment.build(ticket_ID);
            Processor processor =selfServeHelper.submitTicketHelper(jsonHelper.getObjectToJSON(triggerEventForTicketAssignment), String.valueOf(restID));
            int statusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            String status_msg = processor.ResponseValidator.GetNodeValue("$.statusMessage");
            //assert
            softAssert.assertEquals(statusCode,SelfServeConstants.statusOne, "TriggerEventForTicketAssignment status is not same");
            softAssert.assertNull(status_msg,"TriggerEventForTicketAssignment message not null");
        }

        if (hitEnableAgent) {
            String agentID = selfServeHelper.getAgentIDFromDB();
            log.info("--Agent Enabled ::: "+agentID);
            selfServeHelper.makeSingleAgentEnabled(agentID);
            selfServeHelper.makeSingleAgentAvailable(agentID);
        }
        if (enableAgentAction) {
            Processor p2;

            if (ticketState.equalsIgnoreCase("approved")) {
                p2 = selfServeHelper.approveTicketHelper(json,g_ticket_id,SelfServeConstants.test_auth_key);
            } else if (ticketState.equalsIgnoreCase("approved-with-edits")) {
                p2 = selfServeHelper.approveWithEditsTicketHelper(json,g_ticket_id,SelfServeConstants.test_auth_key);
            } else {
                p2 = selfServeHelper.rejectTicketHelper(json,g_ticket_id,SelfServeConstants.test_auth_key);
            }

            int get_status = p2.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            String get_msg = p2.ResponseValidator.GetNodeValue("$.statusMessage");
            //assert
            softAssert.assertEquals(get_status,SelfServeConstants.statusOne, "Agent Action status is not same");
            softAssert.assertNull(get_msg,"Agent Action message not null");

        }


        softAssert.assertAll();
    }


    @Test(dataProvider = "updateAgentAvailabilityDP", priority = 2,groups = {"SelfServe", "Regression"},description = "update Agent Availability",enabled = true)

    public void updateAgentAvailability(String agentID, String isAvailable, String isEnabled, String description) {
        SoftAssert softAssert = new SoftAssert();
        Processor p;
        String auth_key,auth_user_id;
        log.info("-- Description ::: "+description);
        log.info("-- AgentID ::: "+ agentID);

        if(isEnabled.equals("false")) {
            selfServeHelper.makeSingleAgentDisabled(agentID);
        }
        if (agentID.equals("0")) {
            auth_key = SelfServeConstants.test_auth_key;
            auth_user_id = agentID;
            p = selfServeHelper.availableUnavailableAgentByAuthUserID(auth_key, auth_user_id, Boolean.parseBoolean(isAvailable));
            int get_status = p.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            String get_message = p.ResponseValidator.GetNodeValue("$.statusMessage");
            softAssert.assertEquals(get_message,"No such authuser id present :0", "Message not same in invalid agentid");
            softAssert.assertEquals(get_status,SelfServeConstants.statusZero, "Status code not one in invalid agent id ");
        } else {
            auth_key = selfServeHelper.getAuthKeyOfAgentFromDB(agentID);
            auth_user_id = selfServeHelper.getAuthUserIDFromDB(agentID);
            g_agent_id = agentID;
            g_auth_user_id =auth_user_id;
            g_auth_key = auth_key;
            Processor p1 = selfServeHelper.findAgentByAuthUserID(auth_key,auth_user_id);
            boolean get_isAvailable = p1.ResponseValidator.GetNodeValueAsBool("$.data.isAvailable");
            log.info("--IS Agent Already Available ::: "+ String.valueOf(get_isAvailable));
//            boolean get_isEnabled = p1.ResponseValidator.GetNodeValueAsBool("$.data.isEnabled");
            p = selfServeHelper.availableUnavailableAgentByAuthUserID(auth_key, auth_user_id, Boolean.parseBoolean(isAvailable));
            int get_status = p.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            String get_message = p.ResponseValidator.GetNodeValue("$.statusMessage");
            if (get_isAvailable && isAvailable.equals("true")) {
                softAssert.assertEquals(get_message,SelfServeConstants.msg_agentAlreadyAvailable, "Message not same in valid agent id");
                softAssert.assertEquals(get_status,SelfServeConstants.statusZero, "Status code not one in valid agent id ");
            } else if (isEnabled.equals("false") && isAvailable.equals("true")) {
                softAssert.assertEquals(get_message,SelfServeConstants.msg_agentDisabled, "Message not same in valid agent id");
                softAssert.assertEquals(get_status,SelfServeConstants.statusZero, "Status code not one in valid agent id ");
            } else if ( get_isAvailable && isAvailable.equals("false")) {
                boolean get_availability = p.ResponseValidator.GetNodeValueAsBool("$.data.isAvailable");
                boolean get_enabled = p.ResponseValidator.GetNodeValueAsBool("$.data.isEnabled");
                softAssert.assertEquals(get_availability,false, "Availability not true");
                softAssert.assertEquals(get_enabled,true, "Enabled not true");
                softAssert.assertEquals(get_status,SelfServeConstants.statusOne, "Status code not one in valid agent id ");
                softAssert.assertNull(get_message);
            } else if (!get_isAvailable && isAvailable.equals("false")) {
                softAssert.assertEquals(get_status,SelfServeConstants.statusZero, "Status code not one in valid agent id ");
                softAssert.assertEquals(get_message,SelfServeConstants.msg_agentDisabled, "Status message for already unavailable user not same");
            }
            else {
                boolean get_availability = p.ResponseValidator.GetNodeValueAsBool("$.data.isAvailable");
                boolean get_enabled = p.ResponseValidator.GetNodeValueAsBool("$.data.isEnabled");
                softAssert.assertEquals(get_availability,true, "Availability not true");
                softAssert.assertEquals(get_enabled,true, "Enabled not true");
                softAssert.assertEquals(get_status,SelfServeConstants.statusOne, "Status code not one in valid agent id ");
                softAssert.assertNull(get_message);
            }
        }
        softAssert.assertAll();
    }


    @Test(dataProvider = "agentEnableDisableDP", priority = 3,groups = {"SelfServe", "Regression"},description = "validate agent Enable Disable",enabled = true)

    public void agentEnableDisable(String agentID,String enabled, String description) {
        SoftAssert softAssert = new SoftAssert();
        Processor p;
        String auth_key,auth_user_id;
        log.info("-- Description ::: "+description);
        log.info("-- AgentID ::: "+ agentID);

        if (agentID.equals("0")) {
            auth_key = SelfServeConstants.test_auth_key;
            auth_user_id = agentID;
            p = selfServeHelper.enableDisableAgentByAuthUserID(auth_key, auth_user_id, Boolean.parseBoolean(enabled));
            int get_status = p.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            String get_message = p.ResponseValidator.GetNodeValue("$.statusMessage");
            softAssert.assertEquals(get_message, "No such authuser id present :0", "Message not same in invalid agentid");
            softAssert.assertEquals(get_status, SelfServeConstants.statusZero, "Status code not one in invalid agent id ");
        }
        else {
            auth_key = selfServeHelper.getAuthKeyOfAgentFromDB(agentID);
            auth_user_id = selfServeHelper.getAuthUserIDFromDB(agentID);
            g_agent_id = agentID;
            g_auth_user_id =auth_user_id;
            g_auth_key = auth_key;
            Processor p1 = selfServeHelper.findAgentByAuthUserID(auth_key,auth_user_id);
            boolean get_isEnabled = p1.ResponseValidator.GetNodeValueAsBool("$.data.isEnabled");
            p = selfServeHelper.enableDisableAgentByAuthUserID(auth_key, auth_user_id, Boolean.parseBoolean(enabled));
            int get_status = p.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            String get_message = p.ResponseValidator.GetNodeValue("$.statusMessage");
            softAssert.assertEquals(get_status,SelfServeConstants.statusOne, "Status code not one in valid agent id ");
            softAssert.assertNull(get_message, "Message not same in valid agent id");
            if (get_isEnabled && enabled.equals("true")) {
                boolean get_current_enabled_status = p.ResponseValidator.GetNodeValueAsBool("$.data.isEnabled");
                softAssert.assertEquals(get_current_enabled_status, true, "enabled status is not true in the response of valid user");
                softAssert.assertNull(get_message, "Message not same in valid agent id");
            }
            else {
                boolean get_current_enabled_status = p.ResponseValidator.GetNodeValueAsBool("$.data.isEnabled");
                softAssert.assertEquals(get_current_enabled_status,false,"enabled status is not false in the response of valid user");
            }
        }
        softAssert.assertAll();
        }


    @Test(dataProvider = "getAgentDetailsDP", priority = 3,groups = {"SelfServe", "Regression"},description = "validate get Agent Details",enabled = true)

    public void getAgentDetails(String agentID,String authkey, String authuserid, String agentType, String description) {
        SoftAssert softAssert = new SoftAssert();
        log.info("-- Description ::: "+description);
        log.info("-- AgentID ::: "+ agentID);
        Processor p;
        if (agentID.equals("0")) {
            p = selfServeHelper.findAgentByAgentID(authkey, agentID);
            int get_status = p.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            String get_message = p.ResponseValidator.GetNodeValue("$.statusMessage");
            softAssert.assertEquals(get_status,SelfServeConstants.statusZero, "Status not 0 in invalid");
            softAssert.assertNull(get_message, "Message not null in invalid");
        } else {
            p = selfServeHelper.findAgentByAgentID(authkey, agentID);
            int get_status = p.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            String get_message = p.ResponseValidator.GetNodeValue("$.statusMessage");
            int get_id = p.ResponseValidator.GetNodeValueAsInt("$.data.id");
            int get_auth_user_id = p.ResponseValidator.GetNodeValueAsInt("$.data.auth_user_id");
            String get_agent_type =p.ResponseValidator.GetNodeValue("$.data.agent_type");
            //asserts
            softAssert.assertEquals(get_status,SelfServeConstants.statusOne, "Status not 1 in valid");
            softAssert.assertEquals(get_message,SelfServeConstants.message_success, "MSG not success in valid");
            softAssert.assertEquals(String.valueOf(get_id),agentID, "AgentID not same");
            softAssert.assertEquals(get_agent_type,agentType, "Agent type not same");
            softAssert.assertEquals(String.valueOf(get_auth_user_id),authuserid, "Auth user id not same");
        }

        softAssert.assertAll();
    }

    @Test(dataProvider = "addItemToRestDP", priority = 3,groups = {"SelfServe", "Regression"},description = "validate add Item To Restaurant",enabled = true)

    public void addItemToRestaurant(String json, String name, String validCase, String submitTicket, String restID, String description) throws IOException {
        log.info("-- Description ::: "+description);
        log.info("-- Name ::: "+name);
        log.info(json);
        JsonHelper jsonHelper = new JsonHelper();
        List<Map<String, Object>> DBlist = new ArrayList<>();
        SoftAssert softAssert = new SoftAssert();
        String ticketID = "";
        Processor p = selfServeHelper.addItemHelper(json,restID);
        int get_status = p.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String get_message = p.ResponseValidator.GetNodeValue("$.statusMessage");
        boolean get_data = p.ResponseValidator.GetNodeValueAsBool("$.data");
        softAssert.assertEquals(get_status, SelfServeConstants.statusOne, "Status not 1 ");
        softAssert.assertEquals(get_message, SelfServeConstants.message_success, "MSG not success in valid");
        softAssert.assertEquals(get_data,true, "data value not true");
        if (Boolean.parseBoolean(submitTicket)) {
            DBlist = selfServeHelper.getCreatedTicketDetailsFromDB(restID,name);
            if (!DBlist.isEmpty())
                ticketID = DBlist.get(0).get("id").toString();
            else {
                log.info("DBLIST is empty");
                if (Boolean.parseBoolean(validCase))
                    softAssert.assertTrue(false);
                else
                    softAssert.assertTrue(true);
            }
            Ticket ticket = new Ticket();
            ticket.setId(Integer.parseInt(ticketID));
            List<Ticket> ticket_ID = new ArrayList<>();
            ticket_ID.add(ticket);
            TriggerEventForTicketAssignment triggerEventForTicketAssignment = new TriggerEventForTicketAssignment();
            triggerEventForTicketAssignment.build(ticket_ID);
            Processor processor =selfServeHelper.submitTicketHelper(jsonHelper.getObjectToJSON(triggerEventForTicketAssignment), String.valueOf(restID));
            int statusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            String status_msg = processor.ResponseValidator.GetNodeValue("$.statusMessage");
            if (Boolean.parseBoolean(validCase)){
                softAssert.assertEquals(statusCode, SelfServeConstants.statusOne, "Status not 1 ");
                softAssert.assertNull(status_msg, "MSG not success in valid");
            } else if (!Boolean.parseBoolean(validCase)) {
                softAssert.assertEquals(statusCode, SelfServeConstants.statusZero, "Status not 0 in invalid");
                softAssert.assertEquals(status_msg, "Restuarant info not found ... restIds:[0]", "MSG not success in invalid");
            }
        }
        softAssert.assertAll();
    }


    @Test(dataProvider = "getTicketDetailsDP", priority = 3,groups = {"SelfServe", "Regression"},description = "validate get Ticket Details",enabled = true)

    public void getTicketDetails(String ticketID, String name, String restID, String ticketType, String description) {
        log.info("-- Description ::: "+description);
        log.info("-- Name ::: "+name);
        log.info("-- TicketID ::: "+ticketID);
        SoftAssert softAssert = new SoftAssert();
        String agentID = selfServeHelper.getAgentIDFromDB();
        String authKey = selfServeHelper.getAuthKeyOfAgentFromDB(agentID);
        Processor p = selfServeHelper.getTicketDetailsHelper(ticketID,authKey);
        int get_status = p.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String get_message = p.ResponseValidator.GetNodeValue("$.statusMessage");
        if (ticketID.equals("0")) {
            softAssert.assertEquals(get_status, SelfServeConstants.statusZero, "Status not 0 ");
            softAssert.assertEquals(get_message, "no ticket found for id:0", "MSG not same in invalid");
        } else {
            String get_type = p.ResponseValidator.GetNodeValue("$.data.type");
            int get_restID = p.ResponseValidator.GetNodeValueAsInt("$.data.restaurantId");
            String get_id = p.ResponseValidator.GetNodeValue("$.data.uniqueId");
            String get_name =  p.ResponseValidator.GetNodeValue("$.data.item_name");
            softAssert.assertEquals(get_status, SelfServeConstants.statusOne, "Status not 1 ");
            softAssert.assertEquals(get_message, SelfServeConstants.message_success, "MSG not success in valid");
            softAssert.assertEquals(get_type,ticketType, "Type not same");
            softAssert.assertEquals(get_id, ticketID, "ticket id not same");
            softAssert.assertEquals(get_restID,Integer.parseInt(restID),"Rest id not same");
            softAssert.assertEquals(get_name,name, "name not same");
        }
        softAssert.assertAll();
    }

}
