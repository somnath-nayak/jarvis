package com.swiggy.api.erp.cms.pojo.getMultipleItemsUsingItemId;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "name",
        "order",
        "price",
        "id",
        "isVeg",
        "inStock",
        "gst_details"
})
public class Choices {

    @JsonProperty("name")
    private String name;
    @JsonProperty("order")
    private Integer order;
    @JsonProperty("price")
    private Integer price;
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("isVeg")
    private Integer isVeg;
    @JsonProperty("inStock")
    private Integer inStock;
    @JsonProperty("gst_details")
    private GstDetails gstDetails;

    /**
     * No args constructor for use in serialization
     *
     */
    public Choices() {
    }

    /**
     *
     * @param id
     * @param gstDetails
     * @param isVeg
     * @param price
     * @param order
     * @param name
     * @param inStock
     */
    public Choices(String name, Integer order, Integer price, Integer id, Integer isVeg, Integer inStock, GstDetails gstDetails) {
        super();
        this.name = name;
        this.order = order;
        this.price = price;
        this.id = id;
        this.isVeg = isVeg;
        this.inStock = inStock;
        this.gstDetails = gstDetails;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("order")
    public Integer getOrder() {
        return order;
    }

    @JsonProperty("order")
    public void setOrder(Integer order) {
        this.order = order;
    }

    @JsonProperty("price")
    public Integer getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(Integer price) {
        this.price = price;
    }

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("isVeg")
    public Integer getIsVeg() {
        return isVeg;
    }

    @JsonProperty("isVeg")
    public void setIsVeg(Integer isVeg) {
        this.isVeg = isVeg;
    }

    @JsonProperty("inStock")
    public Integer getInStock() {
        return inStock;
    }

    @JsonProperty("inStock")
    public void setInStock(Integer inStock) {
        this.inStock = inStock;
    }

    @JsonProperty("gst_details")
    public GstDetails getGstDetails() {
        return gstDetails;
    }

    @JsonProperty("gst_details")
    public void setGstDetails(GstDetails gstDetails) {
        this.gstDetails = gstDetails;
    }

}
