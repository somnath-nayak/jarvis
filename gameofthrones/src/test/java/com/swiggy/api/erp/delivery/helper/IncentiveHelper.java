package com.swiggy.api.erp.delivery.helper;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;

import java.util.HashMap;

public class IncentiveHelper {
	DeliveryServiceHelper helpdel = new DeliveryServiceHelper();
	Initialize gameofthrones = new Initialize();
	DeliveryHelperMethods delmeth = new DeliveryHelperMethods();


	
	public void caprD(String de_id, String order_id) throws InterruptedException
	{
		helpdel.makeDEActive(de_id);
		helpdel.makeDEFree(de_id);
		Thread.sleep(10000);
		helpdel.assignOrder(order_id, de_id);
		Thread.sleep(10000);
		helpdel.zipDialConfirm(order_id);
		Thread.sleep(10000);
		helpdel.zipDialArrivedDE(order_id);
		Thread.sleep(10000);
	    helpdel.zipDialPickedUpDE(order_id);
		Thread.sleep(10000);
		helpdel.zipDialReachedDE(order_id);
		Thread.sleep(10000);
		helpdel.zipDialDeliveredDE(order_id);
		Thread.sleep(10000);
		
	}
	
	
	public void deReject(String de_id, String order_id) throws InterruptedException
	{
		helpdel.makeDEActive(de_id);
		helpdel.makeDEFree(de_id);
		//Thread.sleep(10000);
		helpdel.assignOrder(order_id, de_id);
		Thread.sleep(10000);
		helpdel.zipDialConfirm(order_id);
		Thread.sleep(10000);
		//helpdel.zipDialRejectedDE(order_id);
		helpdel.reassign(order_id, "1", "1");
	}

	public void CancelAfterArrived(String de_id, String order_id) throws InterruptedException
	{
		helpdel.makeDEActive(de_id);
		helpdel.makeDEFree(de_id);
		Thread.sleep(10000);
		helpdel.assignOrder(order_id, de_id);
		Thread.sleep(10000);
		helpdel.zipDialConfirm(order_id);
		Thread.sleep(10000);
		helpdel.zipDialArrivedDE(order_id);
		Thread.sleep(10000);
		helpdel.ordercancel(order_id);
	}
	public void CancelBeforePickedup(String de_id,String order_id) throws InterruptedException
	{
		helpdel.makeDEActive(de_id);
		helpdel.makeDEFree(de_id);
		Thread.sleep(10000);
		helpdel.assignOrder(order_id, de_id);
		Thread.sleep(10000);
		helpdel.zipDialConfirm(order_id);
		Thread.sleep(10000);
		helpdel.zipDialArrivedDE(order_id);
		Thread.sleep(10000);
	   	helpdel.ordercancel(order_id);
	}
	
	public boolean ComputeEvent(String eventName, String time) throws InterruptedException {

	{
		GameOfThronesService service = new GameOfThronesService("deliveryalchemist", "computeevent", gameofthrones);
		HashMap<String, String> requestheaders = delmeth.singleheader();
		String[] payloadparams = new String[] { eventName, time };
		Processor processor = new Processor(service, requestheaders, payloadparams);
        return processor.ResponseValidator.GetResponseCode() == 200;
	
	}
	}
	
}