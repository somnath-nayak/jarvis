package com.swiggy.api.erp.crm.helper;

import com.swiggy.api.order_flow.PlaceOrder;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;

import java.util.HashMap;

/**
 * Created by sumit.m on 12/07/18.
 */
public class CRMUpstreamValidation {

    Initialize gameofthrones = new Initialize();
    PlaceOrder e2eOrderFlow = new PlaceOrder();

    public String orderId, deliveryStatus, ffStatus;
    public String ff_status, delivery_status;

    public Processor getOrder_response;


    public Processor getOrderDetails(String OrderId)
    {
        String[] orderArray = new String[]{orderId};
        String[] queryparam = orderArray;

        HashMap<String, String> requestheaders_getOrder = new HashMap<String, String>();
        requestheaders_getOrder.put("Content-Type", "application/json");
        GameOfThronesService getOrder = new GameOfThronesService("oms", "getOrder", gameofthrones);
        getOrder_response = new Processor(getOrder, requestheaders_getOrder, null, queryparam);

        return getOrder_response;
    }

    public String getDeliveryStatus(String orderId)
    {
        getOrderDetails(orderId);

        deliveryStatus = getOrder_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("objects..status.delivery_status");
        System.out.println("order_status is ##############   .. " + deliveryStatus);

        deliveryStatus = deliveryStatus.replace("[\"", "").replace("\"]", "");
        return deliveryStatus;
    }

    public String getOrderId(String OrderId)
    {
        getOrderDetails(OrderId);

        String order_id = getOrder_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("objects..order_id").replace("[\"", "").replace("\"]", "");
        return order_id;
    }

    public String getFFStatus(String orderId)
    {
        getOrderDetails(orderId);

        ffStatus = getOrder_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("objects..status.status");

        ffStatus = ffStatus.replace("[\"", "").replace("[\"", "");

        return ffStatus;
    }

    public HashMap<String, String> e2e(String mobileno, String password, String rest_id, String item_id, String quantity, String delivery_status, String ff_status)

    {
        HashMap <String, String> map = new HashMap<>();

        HashMap<String, String> result = new HashMap<>();

            map = e2eOrderFlow.placeOrder(mobileno, password, rest_id, item_id, quantity, delivery_status, ff_status);

            result.put("order_id", map.get("order_id"));
            result.put("success", map.get("success"));

           return result;
    }
}
