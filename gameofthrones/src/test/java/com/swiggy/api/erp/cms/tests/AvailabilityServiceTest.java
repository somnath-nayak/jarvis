package com.swiggy.api.erp.cms.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.constants.Constants;
import com.swiggy.api.erp.cms.dp.AvailabiltyDP;
import com.swiggy.api.erp.cms.helper.AvailabilityHelper;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import com.swiggy.api.erp.cms.helper.TimeSlotsHelper;
import com.typesafe.config.ConfigException;
import framework.gameofthrones.Aegon.Initialize;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.Test;
import scala.util.parsing.json.JSON;

public class AvailabilityServiceTest extends AvailabiltyDP {
    AvailabilityHelper availabilityHelper= new AvailabilityHelper();
    Initialize gameofthrones = new Initialize();
    TimeSlotsHelper timeSlotsHelper= new TimeSlotsHelper();
    CMSHelper cmsHelper= new CMSHelper();

    @Test(dataProvider = "restTime")
    public void restTimeSlots(String restId)
    {
        String response= availabilityHelper.restTimeSlots(restId).ResponseValidator.GetBodyAsText();
        String data= JsonPath.read(response,"$.data..day").toString().replace("[","").replace("]","");
        int noOfDays= data.split(",").length;
        Assert.assertNotNull(noOfDays,"Length of array is not 7");
        String code= JsonPath.read(response,"$.code").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.code,code,"Code is not successful");
    }

    @Test(dataProvider = "restTimeNegative")
    public void restTimeSlotsNegative(String restId)
    {
        String response= availabilityHelper.restTimeSlots(restId).ResponseValidator.GetBodyAsText();
        String data= JsonPath.read(response,"$.data..day").toString().replace("[","").replace("]","");
        int noOfDays= data.split(",").length;
        Assert.assertNotNull(noOfDays,"Length of array is not 7");
        String code= JsonPath.read(response,"$.code").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.failureCode,code,"Code is not successful");
    }

    @Test(dataProvider = "restTime")
    public void restHolidaySlots(String restId)
    {
        String response= availabilityHelper.restHolidaySlots(restId).ResponseValidator.GetBodyAsText();
        String fromTime= JsonPath.read(response,"$.data..from_time").toString().replace("[","").replace("]","");
        Assert.assertNotNull(fromTime);
        String code= JsonPath.read(response,"$.code").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.code,code,"Code is not successful");
    }

    @Test(dataProvider = "areaSlot")
    public void areaSlots(String areaId)
    {
        String response= availabilityHelper.areaTimeSlots(areaId).ResponseValidator.GetBodyAsText();
        String fromTime= JsonPath.read(response,"$.data..from_time").toString().replace("[","").replace("]","");
        Assert.assertNotNull(fromTime);
        String code= JsonPath.read(response,"$.code").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.code,code,"Code is not successful");
    }

    @Test(dataProvider = "areaSlotError")
    public void areaSlotsErrorCase(String areaId)
    {
        String response= availabilityHelper.areaTimeSlots(areaId).ResponseValidator.GetBodyAsText();
        String fromTime= JsonPath.read(response,"$.data..from_time").toString().replace("[","").replace("]","");
        Assert.assertNotNull(fromTime);
        String code= JsonPath.read(response,"$.code").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.errorCode,code,"Code is not successful");
    }

    @Test(dataProvider = "areaSlotFailure")
    public void areaSlotsFailure(String areaId)
    {
        String response= availabilityHelper.areaTimeSlots(areaId).ResponseValidator.GetBodyAsText();
        String fromTime= JsonPath.read(response,"$.data..from_time").toString().replace("[","").replace("]","");
        Assert.assertNotNull(fromTime);
        String code= JsonPath.read(response,"$.code").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.failureCode,code,"Code is not successful");
    }

    @Test(dataProvider = "citySlot")
    public void citySlots(String cityId)
    {
        String response= availabilityHelper.cityTimeSlots(cityId).ResponseValidator.GetBodyAsText();
        String fromTime= JsonPath.read(response,"$.data..from_time").toString().replace("[","").replace("]","");
        Assert.assertNotNull(fromTime);
        String code= JsonPath.read(response,"$.code").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.code,code,"Code is not successful");
    }

    @Test(dataProvider = "citySlotFailure")
    public void citySlotsFailure(String cityId)
    {
        String response= availabilityHelper.cityTimeSlots(cityId).ResponseValidator.GetBodyAsText();
        String fromTime= JsonPath.read(response,"$.data..from_time").toString().replace("[","").replace("]","");
        Assert.assertNotNull(fromTime);
        String code= JsonPath.read(response,"$.code").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.FailureCode,code,"Code is not successful");
    }

    @Test(dataProvider = "citySlotError")
    public void citySlotsError(String cityId)
    {
        String response= availabilityHelper.cityTimeSlots(cityId).ResponseValidator.GetBodyAsText();
        String fromTime= JsonPath.read(response,"$.data..from_time").toString().replace("[","").replace("]","");
        Assert.assertNotNull(fromTime);
        String code= JsonPath.read(response,"$.code").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.errorCode,code,"Code is not successful");
    }

    @Test(dataProvider = "restTime")
    public void areaIdRestSlots(String restId)
    {
        String response= availabilityHelper.areaTimeSlotsRest(restId).ResponseValidator.GetBodyAsText();
        String fromTime= JsonPath.read(response,"$.data..from_time").toString().replace("[","").replace("]","");
        Assert.assertNotNull(fromTime);
        String code= JsonPath.read(response,"$.code").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.code,code,"Code is not successful");
    }

    @Test(dataProvider = "restTimeInvalid")
    public void areaIdRestSlotsInvalidData(String restId)
    {
        String response= availabilityHelper.areaTimeSlotsRest(restId).ResponseValidator.GetBodyAsText();
        String fromTime= JsonPath.read(response,"$.data..from_time").toString().replace("[","").replace("]","");
        Assert.assertNotNull(fromTime);
        String code= JsonPath.read(response,"$.code").toString().replace("[","").replace("]","");
        Assert.assertNotEquals(Constants.code,code,"Code is not successful");
    }

    @Test(dataProvider = "areaIdRestSlotsFailure")
    public void areaIdRestSlotsFailure(String restId)
    {
        String response= availabilityHelper.areaTimeSlotsRest(restId).ResponseValidator.GetBodyAsText();
        String fromTime= JsonPath.read(response,"$.data..from_time").toString().replace("[","").replace("]","");
        Assert.assertNotNull(fromTime);
        String code= JsonPath.read(response,"$.code").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.failureCode,code,"Code is not successful");
    }

    @Test(dataProvider = "areaIdRestSlotsError")
    public void areaIdRestSlotsError(String restId)
    {
        String response= availabilityHelper.areaTimeSlotsRest(restId).ResponseValidator.GetBodyAsText();
        String fromTime= JsonPath.read(response,"$.data..from_time").toString().replace("[","").replace("]","");
        Assert.assertNotNull(fromTime);
        String code= JsonPath.read(response,"$.code").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.errorCode,code,"Code is not successful");
    }

    @Test(dataProvider = "areaSlot")
    public void areaRestSlots(String restId)
    {
        String response= availabilityHelper.restAreaSlots(restId).ResponseValidator.GetBodyAsText();
        System.out.print(response);
        String fromTime= JsonPath.read(response,"$.data").toString().replace("[","").replace("]","");
        Assert.assertNotNull(fromTime);
        String code= JsonPath.read(response,"$.code").toString().replace("[","").replace("]","");
        String status= JsonPath.read(response,"$.status").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.code1,code,"Code is not successful");
        Assert.assertEquals(Constants.status,status,"status is not 1");
    }

    @Test(dataProvider = "restTime")
    public void cityRestSlots(String restId)
    {
        String response= availabilityHelper.restCitySlots(restId).ResponseValidator.GetBodyAsText();
        String fromTime= JsonPath.read(response,"$.data..from_time").toString().replace("[","").replace("]","");
        Assert.assertNotNull(fromTime);
        String code= JsonPath.read(response,"$.code").toString().replace("[","").replace("]","");
        String status= JsonPath.read(response,"$.status").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.code1,code,"Code is not successful");
        Assert.assertEquals(Constants.status,status,"status is not 1");
    }

    @Test(dataProvider = "cityRestSlotsFailure")
    public void cityRestSlotsFailure(String restId)
    {
        String response= availabilityHelper.restCitySlots(restId).ResponseValidator.GetBodyAsText();
        String fromTime= JsonPath.read(response,"$.data..from_time").toString().replace("[","").replace("]","");
        Assert.assertNotNull(fromTime);
        String code= JsonPath.read(response,"$.code").toString().replace("[","").replace("]","");
        String status= JsonPath.read(response,"$.status").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.failureCode,code,"Code is not successful");
        Assert.assertEquals(Constants.status0,status,"status is not 1");
    }


    @Test(dataProvider = "areaSlot")
    public void restIdByArea(String areaId)
    {
        String response= availabilityHelper.restIdByArea(areaId).ResponseValidator.GetBodyAsText();
        String data= JsonPath.read(response,"$.data").toString().replace("[","").replace("]","");
        Assert.assertNotNull(data);
        String code= JsonPath.read(response,"$.code").toString().replace("[","").replace("]","");
        String status= JsonPath.read(response,"$.status").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.code1,code,"Code is not successful");
        Assert.assertEquals(Constants.status,status,"status is not 1");
    }

    @Test(dataProvider = "citySlot")
    public void restIdByCity(String cityId)
    {
        String response= availabilityHelper.restIdByCity(cityId).ResponseValidator.GetBodyAsText();
        String data= JsonPath.read(response,"$.data").toString().replace("[","").replace("]","");
        Assert.assertNotNull(data);
        String code= JsonPath.read(response,"$.code").toString().replace("[","").replace("]","");
        String status= JsonPath.read(response,"$.status").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.code1,code,"Code is not successful");
        Assert.assertEquals(Constants.status,status,"status is not 1");
    }

    @Test(dataProvider = "citySlotFailure")
    public void restIdByCityFailure(String cityId)
    {
        String response= availabilityHelper.restIdByCity(cityId).ResponseValidator.GetBodyAsText();
        String data= JsonPath.read(response,"$.data").toString().replace("[","").replace("]","");
        Assert.assertNotNull(data);
        String code= JsonPath.read(response,"$.code").toString().replace("[","").replace("]","");
        String status= JsonPath.read(response,"$.status").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.failureCode,code,"Code is not successful");
        Assert.assertEquals(Constants.status0,status,"status is not 1");
    }

    @Test(dataProvider = "citySlot")
    public void areaIdByCity(String cityId) {
        String response = availabilityHelper.areaIdCityId(cityId).ResponseValidator.GetBodyAsText();
        String data = JsonPath.read(response, "$.data").toString().replace("[", "").replace("]", "");
        Assert.assertNotNull(data);
        String code = JsonPath.read(response, "$.code").toString().replace("[", "").replace("]", "");
        String status = JsonPath.read(response, "$.status").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(Constants.code1, code, "Code is not successful");
        Assert.assertEquals(Constants.status, status, "status is not 1");

    }

    @Test(dataProvider = "citySlotFailure")
    public void areaIdByCityFailure(String cityId)
    {

        String response = availabilityHelper.areaIdCityId(cityId).ResponseValidator.GetBodyAsText();
        String data = JsonPath.read(response, "$.data").toString().replace("[", "").replace("]", "");
        Assert.assertNotNull(data);
        String code = JsonPath.read(response, "$.code").toString().replace("[", "").replace("]", "");
        String status = JsonPath.read(response, "$.status").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(Constants.failureCode, code, "Code is not successful");
        Assert.assertEquals(Constants.status0, status, "status is not 1");

    }

    @Test(dataProvider = "itemSlot")
    public void itemHolidaySlots(String restId)
    {
        String itemId=cmsHelper.getItemsFromRest(restId).get(0).get("item_id").toString();
        String response= availabilityHelper.itemHolidaySlots(itemId).ResponseValidator.GetBodyAsText();
        String data= JsonPath.read(response,"$.data").toString().replace("[","").replace("]","");
        Assert.assertNotNull(data);
        String id= JsonPath.read(response,"$.data..id").toString().replace("[","").replace("]","");
        Assert.assertNotNull(id);
        String status= JsonPath.read(response,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.status,status,"status is not 1");
    }

    @Test(dataProvider = "itemSlotErr")
    public void itemHolidaySlotsError(String itemId)
    {
        String response= availabilityHelper.itemHolidaySlots(itemId).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(response,"Type mismatch. Please check parameter values.","Something wrong");
    }

    @Test(dataProvider = "itemSlot")
    public void itemSlot(String restId)
    {
        String itemId=cmsHelper.getItemsFromRest(restId).get(0).get("item_id").toString();
        String response= availabilityHelper.itemSlots(itemId).ResponseValidator.GetBodyAsText();
        String data= JsonPath.read(response,"$.data..day_of_week").toString().replace("[","").replace("]","");
        Assert.assertNotNull(data, "data node is empty, There is no slt for the item");
        String[] noOfday= data.split(",");
        String code= JsonPath.read(response,"$.statusMessage").toString().replace("[","").replace("]","");
        String status= JsonPath.read(response,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.code1,code,"Code is not successful");
        Assert.assertEquals(Constants.status,status,"status is not 1");
        //Assert.assertEquals(Constants.lengthOfData, noOfday.length, "NoOfDays is not 7");
    }

    @Test(dataProvider = "restTypeSlot")
    public void itemHolidaySlotWithType(String restId, String type)
    {
        String response= availabilityHelper.itemHolidaySlotType(restId,type).ResponseValidator.GetBodyAsText();
        String data= JsonPath.read(response,"$.data").toString().replace("[","").replace("]","");
        Assert.assertNotNull(data);
        String code= JsonPath.read(response,"$.statusCode").toString().replace("[","").replace("]","");
        String status= JsonPath.read(response,"$.statusMessage").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.status,code,"Code is not successful");
        Assert.assertEquals(Constants.code1,status,"status is not 1");
    }

    @Test(dataProvider = "restTypeSlot")
    public void itemSlotsWithType(String restId, String type)
    {
        String response= availabilityHelper.itemSlotType(restId, type).ResponseValidator.GetBodyAsText();
        String fromTime= JsonPath.read(response,"$.data..from_time").toString().replace("[","").replace("]","");
        String toTime= JsonPath.read(response,"$.data..to_time").toString().replace("[","").replace("]","");
        Assert.assertNotNull(fromTime,"from time is null");
        Assert.assertNotNull(toTime, "to time is null");
        String code= JsonPath.read(response,"$.statusMessage").toString().replace("[","").replace("]","");
        String status= JsonPath.read(response,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.code1,code,"Code is not successful");
        Assert.assertEquals(Constants.status,status,"status is not 1");
    }

    @Test(dataProvider = "restTypeSlotFailure")
    public void itemSlotsWithTypeFailureCase(String restId, String type)
    {
        String response= availabilityHelper.itemSlotType(restId, type).ResponseValidator.GetBodyAsText();
        if(response.contains("statusMessage")) {
            String fromTime = JsonPath.read(response, "$.data..from_time").toString().replace("[", "").replace("]", "");
            String toTime = JsonPath.read(response, "$.data..to_time").toString().replace("[", "").replace("]", "");
            Assert.assertNotNull(fromTime, "from time is null");
            Assert.assertNotNull(toTime, "to time is null");
            //String code= JsonPath.read(response,"$.statusMessage").toString().replace("[","").replace("]","");
            String status = JsonPath.read(response, "$.statusCode").toString().replace("[", "").replace("]", "");
            //Assert.assertEquals(Constants.code1,code,"Code is not successful");
            Assert.assertEquals(Constants.status0, status, "status is not 1");
        }
        else{
            Assert.assertEquals(true,response.contains("status=404"));
        }
    }

    @Test(dataProvider = "menuHolidaySlots")
    public void menuHoliday(String menuId)
    {
        String response= availabilityHelper.menuHolidaySlots(menuId).ResponseValidator.GetBodyAsText();
        String code= JsonPath.read(response,"$.statusMessage").toString().replace("[","").replace("]","");
        String status= JsonPath.read(response,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.code1,code,"Code is not successful");
        Assert.assertEquals(Constants.status,status,"status is not 1");
    }

    @Test(dataProvider = "itemSlot")
    public void itemRest(String restId) {
        String itemId=cmsHelper.getItemsFromRest(restId).get(0).get("item_id").toString();
        String response = availabilityHelper.itemRest(itemId).ResponseValidator.GetBodyAsText();
        String data = JsonPath.read(response, "$.data").toString().replace("[", "").replace("]", "");
        Assert.assertNotNull(data);
        //Assert.assertEquals(data, );
        String statusMessage = JsonPath.read(response, "$.statusMessage").toString().replace("[", "").replace("]", "");
        String status = JsonPath.read(response, "$.statusCode").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(Constants.status, status, "Code is not successful");
        Assert.assertEquals(Constants.code1, statusMessage, "it is not successful");
    }

    @Test(dataProvider = "itemSlotError")
    public void itemRestInvalidData(String restId) {
        String response = availabilityHelper.itemRest(restId).ResponseValidator.GetBodyAsText();
        System.out.println(response);
        String statusMessage= JsonPath.read(response, "$.statusCode").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(Constants.status0, statusMessage, "Code is not successful");
    }

    @Test(dataProvider = "restTypeSlot")
    public void getItemId(String restId, String type) {
        String response = availabilityHelper.getItemIds(restId, type).ResponseValidator.GetBodyAsText();
        String data = JsonPath.read(response, "$.data").toString().replace("[", "").replace("]", "");
        Assert.assertNotNull(data);
        String statusMessage = JsonPath.read(response, "$.statusMessage").toString().replace("[", "").replace("]", "");
        String status = JsonPath.read(response, "$.statusCode").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(Constants.status, status, "Code is not successful");
        Assert.assertEquals(Constants.code1, statusMessage, "it is not successful");
    }

    @Test(dataProvider = "itemSlot")
    public void ItemMenuID(String restId) {
        String itemId = cmsHelper.getItemsFromRest(restId).get(0).get("item_id").toString();
        String response = availabilityHelper.itemMenuId(itemId).ResponseValidator.GetBodyAsText();
        System.out.println(response);
        String data = JsonPath.read(response, "$.data").toString().replace("[", "").replace("]", "");
        Assert.assertNotNull(data);
        String statusMessage = JsonPath.read(response, "$.statusMessage").toString().replace("[", "").replace("]", "");
        String status = JsonPath.read(response, "$.statusCode").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(Constants.status, status, "Code is not successful");
        Assert.assertEquals(Constants.code1, statusMessage, "it is not successful");
    }

    @Test(dataProvider = "menuHolidaySlots")
    public void MenuRestID(String menuId)  {
        String response = availabilityHelper.menuRestId(menuId).ResponseValidator.GetBodyAsText();
        String data = JsonPath.read(response, "$.data").toString().replace("[", "").replace("]", "");
        Assert.assertNotNull(data);
        String statusMessage = JsonPath.read(response, "$.statusMessage").toString().replace("[", "").replace("]", "");
        String status = JsonPath.read(response, "$.statusCode").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(Constants.status, status, "Code is not successful");
        Assert.assertEquals(Constants.code1, statusMessage, "it is not successful");
    }

    @Test(dataProvider = "menuHolidaySlotsError")
    public void MenuRestIDError(String menuId){
        String response = availabilityHelper.menuRestId(menuId).ResponseValidator.GetBodyAsText();
        String statusMessage = JsonPath.read(response, "$.statusMessage").toString().replace("[", "").replace("]", "");
        String status = JsonPath.read(response, "$.statusCode").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(Constants.status, status, "Code is not successful");
        Assert.assertEquals(Constants.code1, statusMessage, "it is not successful");
    }

    @Test(dataProvider = "restTypeSlot")
    public void itemAllTypeSlots(String restId, String type) {
        String response = availabilityHelper.itemAllTypeSlot(restId, type).ResponseValidator.GetBodyAsText();
        String data = JsonPath.read(response, "$.data").toString().replace("[", "").replace("]", "");
        Assert.assertNotNull(data);
        String statusMessage = JsonPath.read(response, "$.statusMessage").toString().replace("[", "").replace("]", "");
        String status = JsonPath.read(response, "$.statusCode").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(Constants.status, status, "Code is not successful");
        Assert.assertEquals(Constants.code1, statusMessage, "it is not successful");
    }

    @Test(dataProvider = "itemSlot")
    public void itemMappings(String restId) {
        String itemId=cmsHelper.getItemsFromRest(restId).get(0).get("item_id").toString();
        String response = availabilityHelper.itemMapping(itemId).ResponseValidator.GetBodyAsText();
        String data = JsonPath.read(response, "$.data").toString().replace("[", "").replace("]", "");
        Assert.assertNotNull(data);
        String statusMessage = JsonPath.read(response, "$.statusMessage").toString().replace("[", "").replace("]", "");
        String status = JsonPath.read(response, "$.statusCode").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(Constants.status, status, "Code is not successful");
        Assert.assertEquals(Constants.code1, statusMessage, "it is not successful");
    }

    @Test(dataProvider = "restTypeSlot")
    public void itemAbsoluteSlots(String restId, String type) {
        String response = availabilityHelper.itemAbsoluteSlots(restId,type).ResponseValidator.GetBodyAsText();
        String data = JsonPath.read(response, "$.data").toString().replace("[", "").replace("]", "");
        Assert.assertNotNull(data);
        String statusMessage = JsonPath.read(response, "$.statusMessage").toString().replace("[", "").replace("]", "");
        String status = JsonPath.read(response, "$.statusCode").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(Constants.status, status, "Code is not successful");
        Assert.assertEquals(Constants.code1, statusMessage, "it is not successful");
    }

    @Test(dataProvider = "restTime")
    public void menuGetIdsByRestId(String restId) {
        String response = availabilityHelper.restMenuMap(restId).ResponseValidator.GetBodyAsText();
        String data = JsonPath.read(response, "$.data").toString().replace("[", "").replace("]", "");
        Assert.assertNotNull(data);
        String statusMessage = JsonPath.read(response, "$.statusMessage").toString().replace("[", "").replace("]", "");
        String status = JsonPath.read(response, "$.statusCode").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(Constants.status, status, "Code is not successful");
        Assert.assertEquals(Constants.code1, statusMessage, "it is not successful");
    }


    /*@Test(dataProvider = "restTime")
    public void redisUpdateIMR(String restId, String open, String close, String day) {
        String response = timeSlotsHelper.postTimeSlots(restId,open,close,day).ResponseValidator.GetBodyAsText();
    }*/
    /*@Test(dataProvider = "citySlot")
    public void areaIdByCity(String cityId)
    {
        String response= availabilityHelper.areaIdCityId(cityId).ResponseValidator.GetBodyAsText();
        String data= JsonPath.read(response,"$.data").toString().replace("[","").replace("]","");
        Assert.assertNotNull(data);
        String code= JsonPath.read(response,"$.code").toString().replace("[","").replace("]","");
        String status= JsonPath.read(response,"$.status").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.code1,code,"Code is not successful");
        Assert.assertEquals(Constants.status,status,"status is not 1");
    }

    @Test(dataProvider = "citySlot")
    public void areaIdByCity(String cityId)
    {
        String response= availabilityHelper.areaIdCityId(cityId).ResponseValidator.GetBodyAsText();
        String data= JsonPath.read(response,"$.data").toString().replace("[","").replace("]","");
        Assert.assertNotNull(data);
        String code= JsonPath.read(response,"$.code").toString().replace("[","").replace("]","");
        String status= JsonPath.read(response,"$.status").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.code1,code,"Code is not successful");
        Assert.assertEquals(Constants.status,status,"status is not 1");
    }

    @Test(dataProvider = "citySlot")
    public void areaIdByCity(String cityId)
    {
        String response= availabilityHelper.areaIdCityId(cityId).ResponseValidator.GetBodyAsText();
        String data= JsonPath.read(response,"$.data").toString().replace("[","").replace("]","");
        Assert.assertNotNull(data);
        String code= JsonPath.read(response,"$.code").toString().replace("[","").replace("]","");
        String status= JsonPath.read(response,"$.status").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.code1,code,"Code is not successful");
        Assert.assertEquals(Constants.status,status,"status is not 1");
    }*/

}
