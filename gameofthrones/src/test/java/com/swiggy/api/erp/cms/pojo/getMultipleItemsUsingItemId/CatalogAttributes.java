package com.swiggy.api.erp.cms.pojo.getMultipleItemsUsingItemId;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "quantity",
        "prep_style",
        "serves_how_many",
        "spice_level",
        "veg_classifier",
        "packaging",
        "cutlery",
        "accompaniments"
})
public class CatalogAttributes {

    @JsonProperty("quantity")
    private Quantity quantity;
    @JsonProperty("prep_style")
    private String prepStyle;
    @JsonProperty("serves_how_many")
    private Integer servesHowMany;
    @JsonProperty("spice_level")
    private String spiceLevel;
    @JsonProperty("veg_classifier")
    private String vegClassifier;
    @JsonProperty("packaging")
    private String packaging;
    @JsonProperty("cutlery")
    private String cutlery;
    @JsonProperty("accompaniments")
    private List<String> accompaniments;

    /**
     * No args constructor for use in serialization
     *
     */
    public CatalogAttributes() {
    }

    /**
     *
     * @param accompaniments
     * @param packaging
     * @param vegClassifier
     * @param prepStyle
     * @param cutlery
     * @param servesHowMany
     * @param quantity
     * @param spiceLevel
     */
    public CatalogAttributes(Quantity quantity, String prepStyle, Integer servesHowMany, String spiceLevel, String vegClassifier, String packaging, String cutlery, List<String> accompaniments) {
        super();
        this.quantity = quantity;
        this.prepStyle = prepStyle;
        this.servesHowMany = servesHowMany;
        this.spiceLevel = spiceLevel;
        this.vegClassifier = vegClassifier;
        this.packaging = packaging;
        this.cutlery = cutlery;
        this.accompaniments = accompaniments;
    }

    @JsonProperty("quantity")
    public Quantity getQuantity() {
        return quantity;
    }

    @JsonProperty("quantity")
    public void setQuantity(Quantity quantity) {
        this.quantity = quantity;
    }

    @JsonProperty("prep_style")
    public String getPrepStyle() {
        return prepStyle;
    }

    @JsonProperty("prep_style")
    public void setPrepStyle(String prepStyle) {
        this.prepStyle = prepStyle;
    }

    @JsonProperty("serves_how_many")
    public Integer getServesHowMany() {
        return servesHowMany;
    }

    @JsonProperty("serves_how_many")
    public void setServesHowMany(Integer servesHowMany) {
        this.servesHowMany = servesHowMany;
    }

    @JsonProperty("spice_level")
    public String getSpiceLevel() {
        return spiceLevel;
    }

    @JsonProperty("spice_level")
    public void setSpiceLevel(String spiceLevel) {
        this.spiceLevel = spiceLevel;
    }

    @JsonProperty("veg_classifier")
    public String getVegClassifier() {
        return vegClassifier;
    }

    @JsonProperty("veg_classifier")
    public void setVegClassifier(String vegClassifier) {
        this.vegClassifier = vegClassifier;
    }

    @JsonProperty("packaging")
    public String getPackaging() {
        return packaging;
    }

    @JsonProperty("packaging")
    public void setPackaging(String packaging) {
        this.packaging = packaging;
    }

    @JsonProperty("cutlery")
    public String getCutlery() {
        return cutlery;
    }

    @JsonProperty("cutlery")
    public void setCutlery(String cutlery) {
        this.cutlery = cutlery;
    }

    @JsonProperty("accompaniments")
    public List<String> getAccompaniments() {
        return accompaniments;
    }

    @JsonProperty("accompaniments")
    public void setAccompaniments(List<String> accompaniments) {
        this.accompaniments = accompaniments;
    }

}