package com.swiggy.api.erp.delivery.tests;


import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.delivery.constants.DegradationConstant;
import com.swiggy.api.erp.delivery.dp.DegradationDataProvider;
import com.swiggy.api.erp.delivery.helper.DegradationHelper;

import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Tyrion.RedisHelper;


public class DegradationTest extends DegradationDataProvider
{
	DegradationHelper degradationHelper = new DegradationHelper();
	DegradationDataProvider degradationDp = new DegradationDataProvider();
	RedisHelper redis = new RedisHelper();
	
	String zone_id = DegradationConstant.ZONE_ID;
	String entityId = DegradationConstant.ENTITY_ID;
	String currentMachineId = null;
	String currentStateId = null;
	String transitionWindow = null;
	
	
	public String getNameFromId(String id)
	{
		String name = SystemConfigProvider.getTemplate("degradation").queryForMap("select name from states where id='" + id + "'").get("name").toString();
		return name;
	}

	
	public String getTransitionedState(String bannerFactor, String entityId, String initial_state, String expected_state, int transitionWindowTime)
	{
		redis.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+zone_id, bannerFactor);
		System.out.println("*******BANNER FACTOR AFTER CHANGING IN REDIS*********" + redis.getValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+zone_id));
		
		String state = getCurrentState(entityId, initial_state, expected_state, transitionWindowTime);
		return state;
	}
	
	
	public String getCurrentState(String entityId, String initial_state, String expected_state, int transitionWindow)
	{
		String state = null;
		long start_time= System.currentTimeMillis();
		long end_time = 0;
		if(transitionWindow == 0)
			end_time = start_time + 30000;
		else
		{
			long twMillisec = TimeUnit.MINUTES.toMillis(transitionWindow);
			end_time = start_time + twMillisec;
		}
		
		boolean flag = initial_state.equals(expected_state);
		while(System.currentTimeMillis() < end_time)
		{
			state = degradationHelper.get_current_degradation_state(entityId);
			if(state.equals(expected_state))
			{
				if(flag)
				{
					try {
						Thread.sleep(10000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				else
				{
					System.out.println("***************************BREAK****************************");
					break;
				}
			}
		}
		return state;
	}
	
	
	public Boolean manualStateTransition(String[] payload, String activatedMachineId, String currentStateId)
	{
		String to_state = null;
		String toStateName = null;
		String transitionedState = null;
		
		switch(currentStateId)
		{
			case "1":
			{
				degradationHelper.updateEntity(payload, entityId);
				to_state = String.valueOf(Integer.sum(Integer.parseInt(currentStateId), 1));
				degradationHelper.changeMachineState(Integer.parseInt(entityId), Integer.parseInt(activatedMachineId), Integer.parseInt(to_state));
				toStateName = getNameFromId(to_state);
				transitionedState = getCurrentState(entityId, "light", toStateName, Integer.parseInt(payload[1]));
				break;
			}
			case "2":
			{
				degradationHelper.updateEntity(payload, entityId);
				to_state = String.valueOf(Integer.sum(Integer.parseInt(currentStateId), 1));
				degradationHelper.changeMachineState(Integer.parseInt(entityId), Integer.parseInt(activatedMachineId), Integer.parseInt(to_state));
				toStateName = getNameFromId(to_state);
				transitionedState = getCurrentState(entityId, "heavy", toStateName, Integer.parseInt(payload[1]));
				break;
			}
			case "3":
			{
				degradationHelper.updateEntity(payload, entityId);
				to_state = String.valueOf(Integer.sum(Integer.parseInt(currentStateId), 1));
				degradationHelper.changeMachineState(Integer.parseInt(entityId), Integer.parseInt(activatedMachineId), Integer.parseInt(to_state));
				toStateName = getNameFromId(to_state);
				transitionedState = getCurrentState(entityId, "extreme", toStateName, Integer.parseInt(payload[1]));
				break;
			}
			case "4":
			{
				degradationHelper.updateEntity(payload, entityId);
				to_state = String.valueOf(Integer.parseInt(currentStateId) - 1);
				degradationHelper.changeMachineState(Integer.parseInt(entityId), Integer.parseInt(activatedMachineId), Integer.parseInt(to_state));
				toStateName = getNameFromId(to_state);
				transitionedState = getCurrentState(entityId, "extreme", toStateName, Integer.parseInt(payload[1]));
				break;
			}
		}
		return transitionedState.equals(toStateName);
	}
	
	
	public Boolean manualTransitionAfterAutoTransition(String currentStateId, String currentMachineId)
	{
		String state = null, activeMachineName = degradationHelper.getSupplyStateNameFromId(currentMachineId);
		Boolean final_state = false ;
		switch(currentStateId)
		{
			case "1":
			{
				String BFforNtoL = String.valueOf(getBannerForTransition("normal", "light", activeMachineName));
				state = getTransitionedState(BFforNtoL, entityId, "normal", "light", Integer.parseInt(entityZeroTWPayload[1]));
				Assert.assertEquals(state, "light");
				final_state = manualStateTransition(entityZeroTWPayload, currentMachineId, currentStateId);
			}
			case "2":
			{
				String BFforLtoH = String.valueOf(getBannerForTransition("light", "heavy", activeMachineName));
				state = getTransitionedState(BFforLtoH, entityId, "light", "heavy", Integer.parseInt(entityZeroTWPayload[1]));
				Assert.assertEquals(state, "heavy");
				final_state = manualStateTransition(entityZeroTWPayload, currentMachineId, currentStateId);
			}
			case "3":
			{
				String BFforHtoE = String.valueOf(getBannerForTransition("heavy", "extreme", activeMachineName));
				state = getTransitionedState(BFforHtoE, entityId, "heavy", "extreme", Integer.parseInt(entityZeroTWPayload[1]));
				Assert.assertEquals(state, "extreme");
				final_state = manualStateTransition(entityZeroTWPayload, currentMachineId, currentStateId);
			}
			case "4":
			{
				String BFforEtoN = String.valueOf(getBannerForTransition("extreme", "normal", activeMachineName));
				state = getTransitionedState(BFforEtoN, entityId, "extreme", "normal", Integer.parseInt(entityZeroTWPayload[1]));
				Assert.assertEquals(state, "normal");
				final_state = manualStateTransition(entityZeroTWPayload, currentMachineId, currentStateId);
			}
		}
		return final_state;
	}
	
	
	public List<Object> autoStateTransition(String initial_state, String final_state, String desired_state, String expectedState, String BFforTransition, String BFforDesiredState)
	{
		if(initial_state.equals(desired_state))
		{
			System.out.println("****************************IF*********************************");
			final_state = getTransitionedState(BFforTransition, entityId, desired_state, expectedState, Integer.parseInt(entityZeroTWPayload[1]));
		}
		else
		{
			System.out.println("****************************ELSE*********************************");
			initial_state = getTransitionedState(BFforDesiredState, entityId, initial_state, desired_state, Integer.parseInt(entityZeroTWPayload[1]));
			System.out.println("****banner??****" + redis.getValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+zone_id));
			System.out.println("*************current state" + initial_state);
			if(initial_state.equals(desired_state))
			{
				final_state = getTransitionedState(BFforTransition, entityId, desired_state, expectedState, Integer.parseInt(entityZeroTWPayload[1]));
				System.out.println("*******************" + final_state);
			}
		}
		return Arrays.asList(initial_state, final_state);
	}
	
	public String getBFboundaryValue(String initialState, String finalState, String activeMachineName)
	{
		String[] banner = degradationHelper.getBannerFactorRule(initialState, finalState, Integer.parseInt(zone_id), activeMachineName).split("__");
		System.out.println("$$$$$$ BANNER : " + banner[0]);
		return banner[0];
	}
	
	
	public Boolean ifStateTransitionedAtBoundaryValues(String initial_state, String final_state, String activeMachineName)
	{
		switch(initial_state)
		{
			case "normal":
			{
				String banner = getBFboundaryValue(initial_state, "light", activeMachineName);
				final_state = getTransitionedState(banner, entityId, "normal", "normal", Integer.parseInt(entityZeroTWPayload[1]));
				//final_state = getTransitionedState("1", entityId, "normal", "normal", Integer.parseInt(entityZeroTWPayload[1]));
				System.out.println("******** " + initial_state + " ******* " + final_state);
				break;
			}
			case "light":
			{
				final_state = getTransitionedState("2", entityId, "light", "light", Integer.parseInt(entityZeroTWPayload[1]));
				System.out.println("******** " + initial_state + " ******* " + final_state);
				break;
			}
			case "heavy":
			{
				final_state = getTransitionedState("3", entityId, "heavy", "heavy", Integer.parseInt(entityZeroTWPayload[1]));
				System.out.println("******** " + initial_state + " ******* " + final_state);
				break;
			}
			case "extreme":
			{
				final_state = getTransitionedState("3", entityId, "extreme", "extreme", Integer.parseInt(entityZeroTWPayload[1]));
				System.out.println("******** " + initial_state + " ******* " + final_state);
				break;
			}
		}
		return initial_state.equals(final_state);
	}
	
	
	public double getBannerForTransition(String initialState, String finalState, String activeMachineName)
	{
		String[] banner = degradationHelper.getBannerFactorRule(initialState, finalState, Integer.parseInt(zone_id), activeMachineName).split("__");
		double BFforTransition = banner[1].equals(">") ? (Double.parseDouble(banner[0]) + 0.2) : (Double.parseDouble(banner[0]) - 0.2);
		return BFforTransition;
	}
	
	
	
	public Boolean isTransitionWindowConsidered(String initial_state, String final_state, long transitionWindow, String[] payload, String activeMachineName)
	{
		Boolean flag = false;
		switch(initial_state)
		{
			case "normal":
			{
				String BFforNtoL = String.valueOf(getBannerForTransition("normal", "light", activeMachineName));
				String firstState = getTransitionedState(BFforNtoL, entityId, "normal", "light", Integer.parseInt(entityZeroTWPayload[1]));
				if(firstState.equals("light"))
				{
					String BFforLtoH = String.valueOf(getBannerForTransition("light", "heavy", activeMachineName));
					degradationHelper.updateEntity(payload, entityId);
					String state = getTransitionedState(BFforLtoH, entityId, "light", "light", Integer.parseInt(payload[1]));
					Assert.assertEquals(state, "light");
				}
				final_state = getCurrentState(entityId, "light", "heavy", Integer.parseInt(payload[1]));
				flag = final_state.equals("heavy");
				break;
			}
			case "light":
			{
				String BFforLtoH = String.valueOf(getBannerForTransition("light", "heavy", activeMachineName));
				String firstState = getTransitionedState(BFforLtoH, entityId, "light", "heavy", Integer.parseInt(entityZeroTWPayload[1]));
				if(firstState.equals("heavy"))
				{
					degradationHelper.updateEntity(payload, entityId);
					String BFforHtoE = String.valueOf(getBannerForTransition("heavy", "extreme", activeMachineName));
					String state = getTransitionedState(BFforHtoE, entityId, "heavy", "heavy", Integer.parseInt(payload[1]));
					Assert.assertEquals(state, "heavy");
				}
				final_state = getCurrentState(entityId, "heavy", "extreme", Integer.parseInt(payload[1]));
				flag = final_state.equals("extreme");
				break;
			}
			case "heavy":
			{
				String BFforHtoL = String.valueOf(getBannerForTransition("heavy", "light", activeMachineName));
				String firstState = getTransitionedState(BFforHtoL, entityId, "heavy", "light", Integer.parseInt(entityZeroTWPayload[1]));
				if(firstState.equals("light"))
				{
					degradationHelper.updateEntity(payload, entityId);
					String BFforLtoN = String.valueOf(getBannerForTransition("light", "normal", activeMachineName));
					String state = getTransitionedState(BFforLtoN, entityId, "light", "light", Integer.parseInt(payload[1]));
					Assert.assertEquals(state, "light");
				}
				final_state = getCurrentState(entityId, "light", "normal", Integer.parseInt(payload[1]));
				flag = final_state.equals("normal");
				break;
			}
			case "extreme":
			{
				String BFforEtoH = String.valueOf(getBannerForTransition("extreme", "heavy", activeMachineName));
				String firstState = getTransitionedState(BFforEtoH, entityId, "extreme", "heavy", Integer.parseInt(entityZeroTWPayload[1]));
				if(firstState.equals("heavy"))
				{
					degradationHelper.updateEntity(payload, entityId);
					String BFforHtoL = String.valueOf(getBannerForTransition("heavy", "light", activeMachineName));
					String state = getTransitionedState(BFforHtoL, entityId, "heavy", "heavy", Integer.parseInt(payload[1]));
					Assert.assertEquals(state, "heavy");
				}
				final_state = getCurrentState(entityId, "heavy", "light", Integer.parseInt(payload[1]));
				flag = final_state.equals("light");
				break;
			}
		}
		return flag;
	}
	
	
	public List<Double> getBFforTransitionAndDesiredState(String initialState, String finalState, String activeMachineName)
	{
		
		String[] banner = degradationHelper.getBannerFactorRule(initialState, finalState, Integer.parseInt(zone_id), activeMachineName).trim().split("__");
		BigDecimal bd=new BigDecimal(banner[0]).add(new BigDecimal(".1"));
		BigDecimal bd1 = new BigDecimal(banner[0]).subtract(new BigDecimal(".1"));
		
		//double BFforTransition = banner[1].equals(">") ? (Double.parseDouble(banner[0]) + 0.1) : (Double.parseDouble(banner[0]) - 0.1);
		double BFforTransition = banner[1].equals(">") ? bd.doubleValue() : bd1.doubleValue();
		String[] banner1 = degradationHelper.getBannerFactorRule(finalState, initialState, Integer.parseInt(zone_id), activeMachineName).split("__");
		BigDecimal bd2=new BigDecimal(banner1[0]).add(new BigDecimal(".1"));
		BigDecimal bd3 = new BigDecimal(banner1[0]).subtract(new BigDecimal(".1"));
		//double BFforDesiredState = banner1[1].equals(">") ? (Double.parseDouble(banner1[0]) + 0.1) : (Double.parseDouble(banner1[0]) - 0.1);
		double BFforDesiredState = banner1[1].equals(">") ? bd2.doubleValue() : bd3.doubleValue();
		System.out.println("BANNER FOR DESIRED STATE :- " + BFforDesiredState + ", BANNER FOR TRANSITION :-  " + BFforTransition);
		return Arrays.asList(BFforTransition, BFforDesiredState);
	}
	
	public String getTransitionWindowTime()
	{
		String transitionWindow = SystemConfigProvider.getTemplate("degradation").queryForMap("select no_transition_window_minutes from entities where id=" + entityId).get("no_transition_window_minutes").toString();
		return transitionWindow;
	}
	
	
	@BeforeTest
	public void getCurrentActiveMachineAndState()
	{
		currentMachineId = JsonPath.read(degradationHelper.getActiveMachinesForEntity(entityId).ResponseValidator.GetBodyAsText(), "$..machineId").toString().replace("[", "").replace("]", "");
		currentStateId = JsonPath.read(degradationHelper.getEntityCurrentState(entityId).ResponseValidator.GetBodyAsText(), "$..stateId").toString().replace("[", "").replace("]", "");
		transitionWindow = getTransitionWindowTime();
		System.out.println("************************* BEFORE **************************");
	}
	
	
	@AfterTest
	public void setPreviouslyActiveMachineAndState()
	{
		System.out.println("************************* AFTER **************************");
		degradationHelper.activateMachine(Integer.parseInt(entityId), Integer.parseInt(currentMachineId)).toString();
		degradationHelper.changeMachineState(Integer.parseInt(entityId), Integer.parseInt(currentMachineId), Integer.parseInt(currentStateId)).toString();
		degradationHelper.updateEntity(new String[] {degradationHelper.getEntityNameFromId(entityId), transitionWindow, zone_id}, entityId).toString();
	}
	
	
	
	@Test(description="TC_GD_02 :- validate transition from Normal-to-light")
	public void NtoL_Transition() 
	{
		String activeMachineId = JsonPath.read(degradationHelper.getActiveMachinesForEntity(entityId).ResponseValidator.GetBodyAsText(), "$..machineId").toString().replace("[", "").replace("]", "");
		degradationHelper.updateEntity(entityZeroTWPayload, entityId);
		
		String initial_state = degradationHelper.get_current_degradation_state(entityId);
		String final_state = null;
		List<Object> states = null;
		
		switch(activeMachineId)
		{
			case "1":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("normal", "light", "BusinessAsUsual");
				states = autoStateTransition(initial_state, final_state, "normal", "light", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
				
			}
			case "2":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("normal", "light", "SlightlyStrained");
				states = autoStateTransition(initial_state, final_state, "normal", "light", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
			}
			case "3":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("normal", "light", "HeavilyStrained");
				states = autoStateTransition(initial_state, final_state, "normal", "light", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
			}
			case "4":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("normal", "light", "ExtremelyStrained");
				states = autoStateTransition(initial_state, final_state, "normal", "light", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
			}
		}
		Assert.assertEquals(states.get(0), "normal");
		Assert.assertEquals(states.get(1), "light");
		
	}
	
	
	@Test(description="TC_GD_03 :- Validate transition from to Normal to Heavy")
	public void NtoH_Transition() throws InterruptedException 
	{
		String activeMachineId = JsonPath.read(degradationHelper.getActiveMachinesForEntity(entityId).ResponseValidator.GetBodyAsText(), "$..machineId").toString().replace("[", "").replace("]", "");
		degradationHelper.updateEntity(entityZeroTWPayload, entityId);
		
		String initial_state = degradationHelper.get_current_degradation_state(entityId);
		String final_state = null;
		List<Object> states = null;
		
		switch(activeMachineId)
		{
			case "1":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("normal", "heavy", "BusinessAsUsual");
				states = autoStateTransition(initial_state, final_state, "normal", "heavy", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
			}
			case "2":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("normal", "heavy", "SlightlyStrained");
				states = autoStateTransition(initial_state, final_state, "normal", "heavy", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
			}
			case "3":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("normal", "heavy", "HeavilyStrained");
				states = autoStateTransition(initial_state, final_state, "normal", "heavy", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
			}
			case "4":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("normal", "heavy", "ExtremelyStrained");
				states = autoStateTransition(initial_state, final_state, "normal", "heavy", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
			}
		}
		Assert.assertEquals(states.get(0), "normal");
		Assert.assertEquals(states.get(1), "heavy");
	}
	
	
	@Test(description="TC_GD_04 :- Validate transition from to Normal to Extreme")
	public void NtoE_Transition() throws InterruptedException 
	{
		String activeMachineId = JsonPath.read(degradationHelper.getActiveMachinesForEntity(entityId).ResponseValidator.GetBodyAsText(), "$..machineId").toString().replace("[", "").replace("]", "");
		degradationHelper.updateEntity(entityZeroTWPayload, entityId);
		
		String initial_state = degradationHelper.get_current_degradation_state(entityId);
		String final_state = null;
		List<Object> states = null;
		
		switch(activeMachineId)
		{
			case "1":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("normal", "extreme", "BusinessAsUsual");
				states = autoStateTransition(initial_state, final_state, "normal", "extreme", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
			}
			case "2":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("normal", "extreme", "SlightlyStrained");
				states = autoStateTransition(initial_state, final_state, "normal", "extreme", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
			}
			case "3":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("normal", "extreme", "HeavilyStrained");
				states = autoStateTransition(initial_state, final_state, "normal", "extreme", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
			}
			case "4":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("normal", "extreme", "ExtremelyStrained");
				states = autoStateTransition(initial_state, final_state, "normal", "extreme", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
			}
		}
		Assert.assertEquals(states.get(0), "normal");
		Assert.assertEquals(states.get(1), "extreme");
	}
	
	
	@Test(description="TC_GD_05 :- Validate transition from to Light to heavy")
	public void LtoH_Transition() throws InterruptedException 
	{
		String activeMachineId = JsonPath.read(degradationHelper.getActiveMachinesForEntity(entityId).ResponseValidator.GetBodyAsText(), "$..machineId").toString().replace("[", "").replace("]", "");
		degradationHelper.updateEntity(entityZeroTWPayload, entityId);
		
		String initial_state = degradationHelper.get_current_degradation_state(entityId);
		String final_state = null;
		List<Object> states = null;
		
		switch(activeMachineId)
		{
			case "1":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("light", "heavy", "BusinessAsUsual");
				states = autoStateTransition(initial_state, final_state, "light", "heavy", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
			}
			case "2":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("light", "heavy", "SlightlyStrained");
				states = autoStateTransition(initial_state, final_state, "light", "heavy", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
			}
			case "3":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("light", "heavy", "HeavilyStrained");
				states = autoStateTransition(initial_state, final_state, "light", "heavy", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
			}
			case "4":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("light", "heavy", "ExtremelyStrained");
				states = autoStateTransition(initial_state, final_state, "light", "heavy", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
			}
		}
		Assert.assertEquals(states.get(0), "light");
		Assert.assertEquals(states.get(1), "heavy");
	}
	
	
	@Test(description="TC_GD_06 :- Validate transition from to Light to extreme")
	public void LtoE_Transition() throws InterruptedException 
	{
		String activeMachineId = JsonPath.read(degradationHelper.getActiveMachinesForEntity(entityId).ResponseValidator.GetBodyAsText(), "$..machineId").toString().replace("[", "").replace("]", "");
		degradationHelper.updateEntity(entityZeroTWPayload, entityId);
		
		String initial_state = degradationHelper.get_current_degradation_state(entityId);
		String final_state = null;
		List<Object> states = null;
		
		switch(activeMachineId)
		{
			case "1":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("light", "extreme", "BusinessAsUsual");
				states = autoStateTransition(initial_state, final_state, "light", "extreme", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
			}
			case "2":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("light", "extreme", "SlightlyStrained");
				states = autoStateTransition(initial_state, final_state, "light", "extreme", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
			}
			case "3":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("light", "extreme", "HeavilyStrained");
				states = autoStateTransition(initial_state, final_state, "light", "extreme", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
			}
			case "4":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("light", "extreme", "ExtremelyStrained");
				states = autoStateTransition(initial_state, final_state, "light", "extreme", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
			}
		}
		Assert.assertEquals(states.get(0), "light");
		Assert.assertEquals(states.get(1), "extreme");
	}
	
	@Test(description="TC_GD_07 :-  Validate transition from to Light to normal")
	public void LtoN_Transition() throws InterruptedException 
	{
		String activeMachineId = JsonPath.read(degradationHelper.getActiveMachinesForEntity(entityId).ResponseValidator.GetBodyAsText(), "$..machineId").toString().replace("[", "").replace("]", "");
		degradationHelper.updateEntity(entityZeroTWPayload, entityId);
		
		String initial_state = degradationHelper.get_current_degradation_state(entityId);
		String final_state = null;
		List<Object> states = null;
		
		switch(activeMachineId)
		{
			case "1":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("light", "normal", "BusinessAsUsual");
				states = autoStateTransition(initial_state, final_state, "light", "normal", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
			}
			case "2":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("light", "normal", "SlightlyStrained");
				states = autoStateTransition(initial_state, final_state, "light", "normal", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
			}
			case "3":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("light", "normal", "HeavilyStrained");
				states = autoStateTransition(initial_state, final_state, "light", "normal", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
			}
			case "4":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("light", "normal", "ExtremelyStrained");
				states = autoStateTransition(initial_state, final_state, "light", "normal", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
			}
		}
		Assert.assertEquals(states.get(0), "light");
		Assert.assertEquals(states.get(1), "normal");
	}
	
	@Test(description="TC_GD_08 :- Validate transition from to Heavy to extreme")
	public void HtoE_Transition() throws InterruptedException 
	{
		String activeMachineId = JsonPath.read(degradationHelper.getActiveMachinesForEntity(entityId).ResponseValidator.GetBodyAsText(), "$..machineId").toString().replace("[", "").replace("]", "");
		degradationHelper.updateEntity(entityZeroTWPayload, entityId);
		
		String initial_state = degradationHelper.get_current_degradation_state(entityId);
		String final_state = null;
		List<Object> states = null;
		
		switch(activeMachineId)
		{
			case "1":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("heavy", "extreme", "BusinessAsUsual");
				states = autoStateTransition(initial_state, final_state, "heavy", "extreme", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
			}
			case "2":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("heavy", "extreme", "SlightlyStrained");
				states = autoStateTransition(initial_state, final_state, "heavy", "extreme", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
			}
			case "3":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("heavy", "extreme", "HeavilyStrained");
				states = autoStateTransition(initial_state, final_state, "heavy", "extreme", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
			}
			case "4":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("heavy", "extreme", "ExtremelyStrained");
				states = autoStateTransition(initial_state, final_state, "heavy", "extreme", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
			}
		}
		Assert.assertEquals(states.get(0), "heavy");
		Assert.assertEquals(states.get(1), "extreme");
	}
	
	@Test(description="TC_GD_09 :- Validate transition from to Heavy to Light")
	public void HtoL_Transition() throws InterruptedException 
	{
		String activeMachineId = JsonPath.read(degradationHelper.getActiveMachinesForEntity(entityId).ResponseValidator.GetBodyAsText(), "$..machineId").toString().replace("[", "").replace("]", "");
		degradationHelper.updateEntity(entityZeroTWPayload, entityId);
		
		String initial_state = degradationHelper.get_current_degradation_state(entityId);
		String final_state = null;
		List<Object> states = null;
		
		switch(activeMachineId)
		{
			case "1":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("heavy", "light", "BusinessAsUsual");
				states = autoStateTransition(initial_state, final_state, "heavy", "light", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
			}
			case "2":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("heavy", "light", "SlightlyStrained");
				states = autoStateTransition(initial_state, final_state, "heavy", "light", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
			}
			case "3":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("heavy", "light", "HeavilyStrained");
				states = autoStateTransition(initial_state, final_state, "heavy", "light", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
			}
			case "4":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("heavy", "light", "ExtremelyStrained");
				states = autoStateTransition(initial_state, final_state, "heavy", "light", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
			}
		}
		Assert.assertEquals(states.get(0), "heavy");
		Assert.assertEquals(states.get(1), "light");
	}
	
	
	@Test(description="TC_GD_10 :- Validate transition from to Heavy to normal")
	public void HtoN_Transition() throws InterruptedException 
	{
		String activeMachineId = JsonPath.read(degradationHelper.getActiveMachinesForEntity(entityId).ResponseValidator.GetBodyAsText(), "$..machineId").toString().replace("[", "").replace("]", "");
		degradationHelper.updateEntity(entityZeroTWPayload, entityId);
		
		String initial_state = degradationHelper.get_current_degradation_state(entityId);
		String final_state = null;
		List<Object> states = null;
		
		switch(activeMachineId)
		{
			case "1":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("heavy", "normal", "BusinessAsUsual");
				states = autoStateTransition(initial_state, final_state, "heavy", "normal", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
			}
			case "2":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("heavy", "normal", "SlightlyStrained");
				states = autoStateTransition(initial_state, final_state, "heavy", "normal", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
			}
			case "3":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("heavy", "normal", "HeavilyStrained");
				states = autoStateTransition(initial_state, final_state, "heavy", "normal", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
			}
			case "4":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("heavy", "normal", "ExtremelyStrained");
				states = autoStateTransition(initial_state, final_state, "heavy", "normal", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
			}
		}
		Assert.assertEquals(states.get(0), "heavy");
		Assert.assertEquals(states.get(1), "normal");
	}
	
	
	
	@Test(description="TC_GD_11 :- validate transition from Extreme to heavy")
	public void EtoH_Transition() throws InterruptedException 
	{
		String activeMachineId = JsonPath.read(degradationHelper.getActiveMachinesForEntity(entityId).ResponseValidator.GetBodyAsText(), "$..machineId").toString().replace("[", "").replace("]", "");
		degradationHelper.updateEntity(entityZeroTWPayload, entityId);
		
		String initial_state = degradationHelper.get_current_degradation_state(entityId);
		String final_state = null;
		List<Object> states = null;
		
		switch(activeMachineId)
		{
			case "1":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("extreme", "heavy", "BusinessAsUsual");
				states = autoStateTransition(initial_state, final_state, "extreme", "heavy", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
			}
			case "2":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("extreme", "heavy", "SlightlyStrained");
				states = autoStateTransition(initial_state, final_state, "extreme", "heavy", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
			}
			case "3":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("extreme", "heavy", "HeavilyStrained");
				states = autoStateTransition(initial_state, final_state, "extreme", "heavy", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
			}
			case "4":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("extreme", "heavy", "ExtremelyStrained");
				states = autoStateTransition(initial_state, final_state, "extreme", "heavy", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
			}
		}
		Assert.assertEquals(states.get(0), "extreme");
		Assert.assertEquals(states.get(1), "heavy");
	}
	
	
	@Test(description="TC_GD_12 :- validate transition from Extreme to Light")
	public void EtoL_Transition() throws InterruptedException 
	{
		String activeMachineId = JsonPath.read(degradationHelper.getActiveMachinesForEntity(entityId).ResponseValidator.GetBodyAsText(), "$..machineId").toString().replace("[", "").replace("]", "");
		degradationHelper.updateEntity(entityZeroTWPayload, entityId);
		
		String initial_state = degradationHelper.get_current_degradation_state(entityId);
		String final_state = null;
		List<Object> states = null;
		
		switch(activeMachineId)
		{
			case "1":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("extreme", "light", "BusinessAsUsual");
				states = autoStateTransition(initial_state, final_state, "extreme", "light", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
			}
			case "2":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("extreme", "light", "SlightlyStrained");
				states = autoStateTransition(initial_state, final_state, "extreme", "light", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
			}
			case "3":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("extreme", "light", "HeavilyStrained");
				states = autoStateTransition(initial_state, final_state, "extreme", "light", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
			}
			case "4":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("extreme", "light", "ExtremelyStrained");
				states = autoStateTransition(initial_state, final_state, "extreme", "light", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
			}
		}
		Assert.assertEquals(states.get(0), "extreme");
		Assert.assertEquals(states.get(1), "light");
	}
	
	
	//63
	@Test(description="TC_GD_13 :- validate transition from Extreme to Normal")
	public void EtoN_Transition() throws InterruptedException 
	{
		String activeMachineId = JsonPath.read(degradationHelper.getActiveMachinesForEntity(entityId).ResponseValidator.GetBodyAsText(), "$..machineId").toString().replace("[", "").replace("]", "");
		degradationHelper.updateEntity(entityZeroTWPayload, entityId);
		
		String initial_state = degradationHelper.get_current_degradation_state(entityId);
		String final_state = null;
		List<Object> states = null;
		
		switch(activeMachineId)
		{
			case "1":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("extreme", "normal", "BusinessAsUsual");
				states = autoStateTransition(initial_state, final_state, "extreme", "normal", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
			}
			case "2":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("extreme", "normal", "SlightlyStrained");
				states = autoStateTransition(initial_state, final_state, "extreme", "normal", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
			}
			case "3":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("extreme", "normal", "HeavilyStrained");
				states = autoStateTransition(initial_state, final_state, "extreme", "normal", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
			}
			case "4":
			{
				List<Double> banners = getBFforTransitionAndDesiredState("extreme", "normal", "ExtremelyStrained");
				states = autoStateTransition(initial_state, final_state, "extreme", "normal", String.valueOf(banners.get(0)), String.valueOf(banners.get(1)));
				break;
			}
		}
		Assert.assertEquals(states.get(0), "extreme");
		Assert.assertEquals(states.get(1), "normal");
	}
	
	
	//35
	@Test(description = "TC_GD_14 :- Verify that no state transition happens on boundary values of trigger condition")
	public void NoTransitionOnBoundaryBFvalue()
	{
		String activeMachineId = JsonPath.read(degradationHelper.getActiveMachinesForEntity(entityId).ResponseValidator.GetBodyAsText(), "$..machineId").toString().replace("[", "").replace("]", "");
		degradationHelper.updateEntity(entityZeroTWPayload, entityId);
		
		String initial_state = degradationHelper.get_current_degradation_state(entityId), final_state = null;
		String machineName = degradationHelper.getSupplyStateNameFromId(activeMachineId);
		Boolean flag = false;
		
		switch(activeMachineId)
		{
			case "1":
			{
				flag = ifStateTransitionedAtBoundaryValues(initial_state, final_state, machineName);
				break;
			}
			case "2":
			{
				flag = ifStateTransitionedAtBoundaryValues(initial_state, final_state, machineName);
				break;
			}
			case "3":
			{
				flag = ifStateTransitionedAtBoundaryValues(initial_state, final_state, machineName);
				break;
			}
			case "4":
			{
				flag = ifStateTransitionedAtBoundaryValues(initial_state, final_state, machineName);
				break;
			}
		}
		Assert.assertTrue(flag);
	}
	
	
	//170(2.50)
	@Test(dataProvider = "TC_GD_15", priority = 14, description = "TC_GD_15 :- Validate transition window is considered for transition between states")
	public void transitionWindowTimeBtwnTransition(String[] payload)
	{
		String activeMachineId = JsonPath.read(degradationHelper.getActiveMachinesForEntity(entityId).ResponseValidator.GetBodyAsText(), "$..machineId").toString().replace("[", "").replace("]", "");
		degradationHelper.updateEntity(entityZeroTWPayload, entityId);
		
		String initial_state = degradationHelper.get_current_degradation_state(entityId), final_state = null;
		String activeMachineName = degradationHelper.getSupplyStateNameFromId(activeMachineId);
		int transitionWindow = Integer.parseInt(payload[1])*60000;
		Boolean flag = false;
		
		switch(activeMachineId)
		{
			case "1":
			{
				flag = isTransitionWindowConsidered(initial_state, final_state, transitionWindow, payload, activeMachineName);
				break;
			}
			case "2":
			{
				flag = isTransitionWindowConsidered(initial_state, final_state, transitionWindow, payload, activeMachineName);
				break;
			}
			case "3":
			{
				flag = isTransitionWindowConsidered(initial_state, final_state, transitionWindow, payload, activeMachineName);
				break;
			}
			case "4":
			{
				flag = isTransitionWindowConsidered(initial_state, final_state, transitionWindow, payload, activeMachineName);
				break;
			}
		}
		Assert.assertTrue(flag);
	}
	
	
	@Test(dataProvider = "TC_GD_16", description = "TC_GD_16 :- Verify that Z-Index cannot be same for all outbound edges from a state")
	public void uniqueZindex(String[] payload)
	{
		String edgeCreationResponse = degradationHelper.createEdgeBetweenStates(payload).ResponseValidator.GetBodyAsText();

		Assert.assertEquals(JsonPath.read(edgeCreationResponse, "$.success")
				   .toString().replace("[", "").replace("]", ""), "false");
		
		Assert.assertEquals(JsonPath.read(edgeCreationResponse, "$.message")
				   .toString().replace("[", "").replace("]", ""), "Machine '" + payload[1] + "' already has an edge from state '" + payload[0] + "' to state '" + payload[3] + "'");
	}
	
	
	
	@Test(dataProvider = "TC_GD_17", description = "Verify only one single directional edge can be created between two states")
	public void uniDirectionalEdgeBtwnStates(String[] payload)
	{
		String edgeCreationResponse = degradationHelper.createEdgeBetweenStates(payload).ResponseValidator.GetBodyAsText();

		Assert.assertEquals(JsonPath.read(edgeCreationResponse, "$.success")
				   .toString().replace("[", "").replace("]", ""), "false");
		
		Assert.assertEquals(JsonPath.read(edgeCreationResponse, "$.message").toString().replace("[", "").replace("]", ""), 
				"Machine '" + payload[1] + "' already has an edge from state '" + payload[0] + "' to state '" + payload[3] + "'");
	}
	
	
	/*@Test(dataProvider = "TC_GD_18", description = "Verify edge cannot be deleted while the machine is active")
	public void EdgeDeletionNotAllowedOfActiveMachine(String[] payload)
	{
		String active_machine = JsonPath.read(degradationHelper.getActiveMachinesForEntity(DegradationConstant.ENTITY_ID).ResponseValidator.GetBodyAsText(), "$..machineId").toString().replace("[", "").replace("]", "");
		
		Map<String, Object> map = SystemConfigProvider.getTemplate("degradation")
				.queryForMap("select id from edges where machine_id=" + active_machine + " limit 1");
		
		String response = degradationHelper.deleteEdgeById(map.get("id").toString()).ResponseValidator.GetBodyAsText();
	}*/
	
	
	//130
	@Test(dataProvider = "TC_GD_21_22", description = "Verify the state transition could be manually triggered")
	public void manualTransition(String[] payload)
	{
		degradationHelper.updateEntity(entityZeroTWPayload, entityId);
		String response = degradationHelper.getEntityCurrentState(entityId).ResponseValidator.GetBodyAsText();
		
		String currentStateId = JsonPath.read(response, "$..stateId").toString().replace("[", "").replace("]", "");
		String currentMachineId = JsonPath.read(response, "$..machineId").toString().replace("[", "").replace("]", "");
		Boolean flag = false;
		
		switch(currentMachineId)
		{
			case "1":
			{
				flag = manualStateTransition(payload, currentMachineId, currentStateId);
				break;
			}
			case "2":
			{
				flag = manualStateTransition(payload, currentMachineId, currentStateId);
				break;
			}
			case "3":
			{
				flag = manualStateTransition(payload, currentMachineId, currentStateId);
				break;
			}
			case "4":
			{
				flag = manualStateTransition(payload, currentMachineId, currentStateId);
				break;
			}
		}
		Assert.assertTrue(flag);
	}
	
	
	//131
	@Test(dataProvider = "TC_GD_21_22", description = "Verify manual transition doesnt happen if the machine is not activated ")
	public void noTransitionForUnActivatedMachine(String[] payload)
	{
		degradationHelper.updateEntity(entityZeroTWPayload, entityId);
		String response = degradationHelper.getEntityCurrentState(entityId).ResponseValidator.GetBodyAsText();
		
		String activatedMachineId = JsonPath.read(response, "$..machineId").toString().replace("[", "").replace("]", "");
		String currentStateId = JsonPath.read(response, "$..stateId").toString().replace("[", "").replace("]", "");
		Boolean isStateTransitioned = false;
		
		switch(activatedMachineId)
		{
			case "1":
			{
				isStateTransitioned = manualStateTransition(payload, activatedMachineId, currentStateId);
				break;
			}
			case "2":
			{
				isStateTransitioned = manualStateTransition(payload, activatedMachineId, currentStateId);
				break;
			}
			case "3":
			{
				isStateTransitioned = manualStateTransition(payload, activatedMachineId, currentStateId);
				break;
			}
			case "4":
			{
				isStateTransitioned = manualStateTransition(payload, activatedMachineId, currentStateId);
				break;
			}
		}
		Assert.assertTrue(isStateTransitioned);
	}
	
	
	//238
	@Test(description = "Verify manual transition when auto transition has happened just before triggering manual")
	public void ManualAfterAutoTransition()
	{
		degradationHelper.updateEntity(entityZeroTWPayload, entityId);
		String response = degradationHelper.getEntityCurrentState(entityId).ResponseValidator.GetBodyAsText();
		
		String activatedMachineId = JsonPath.read(response, "$..machineId").toString().replace("[", "").replace("]", "");
		String currentStateId = JsonPath.read(response, "$..stateId").toString().replace("[", "").replace("]", "");
		Boolean flag = false;
		
		switch(activatedMachineId)
		{
			case "1":
			{
				flag = manualTransitionAfterAutoTransition(currentStateId, activatedMachineId);
				break;
			}
			case "2":
			{
				flag = manualTransitionAfterAutoTransition(currentStateId, activatedMachineId);
				break;
			}
			case "3":
			{
				flag = manualTransitionAfterAutoTransition(currentStateId, activatedMachineId);
				break;
			}
			case "4":
			{
				flag = manualTransitionAfterAutoTransition(currentStateId, activatedMachineId);
				break;
			}
		}
		Assert.assertTrue(flag);
	}
	
	
	@AfterMethod
	public void getRunTime(ITestResult tr) {
	    long time = tr.getEndMillis() - tr.getStartMillis();
	    System.out.println("******************TEST EXECUTION TIME *********" + TimeUnit.MILLISECONDS.toSeconds(time));
	}
	
	

}
