package com.swiggy.api.erp.ff.helper;

import com.rabbitmq.client.AMQP;
import framework.gameofthrones.Tyrion.RabbitMQHelper;
import framework.gameofthrones.joffrey.WebSocketSynchronousConnection;
import org.springframework.web.socket.TextMessage;

public class WebSocketHelper {

    public String sendMessageToRabbitForWebSocket(String text) throws InterruptedException {
        WebSocketSynchronousConnection ws = new WebSocketSynchronousConnection();
        ws.start("http://13.229.112.83:8092/eventbus?oe_id=234");
        RabbitMQHelper helper = new RabbitMQHelper();
        helper.pushMessageToExchange("ff","swiggy.ff_push_updates",
                new AMQP.BasicProperties().builder().contentType("application/json"), text);
        TextMessage msg = ws.pollMessage(500);
        System.out.println(msg.getPayload());
        String message = msg.getPayload();
        ws.stop();
        return message;

    }
}
