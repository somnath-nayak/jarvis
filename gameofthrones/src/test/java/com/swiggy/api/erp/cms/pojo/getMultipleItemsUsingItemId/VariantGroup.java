package com.swiggy.api.erp.cms.pojo.getMultipleItemsUsingItemId;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "name",
        "variations",
        "group_id"
})
public class VariantGroup {

    @JsonProperty("name")
    private String name;
    @JsonProperty("variations")
    private List<Variation> variations = null;
    @JsonProperty("group_id")
    private String groupId;

    /**
     * No args constructor for use in serialization
     *
     */
    public VariantGroup() {
    }

    /**
     *
     * @param groupId
     * @param name
     * @param variations
     */
    public VariantGroup(String name, List<Variation> variations, String groupId) {
        super();
        this.name = name;
        this.variations = variations;
        this.groupId = groupId;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("variations")
    public List<Variation> getVariations() {
        return variations;
    }

    @JsonProperty("variations")
    public void setVariations(List<Variation> variations) {
        this.variations = variations;
    }

    @JsonProperty("group_id")
    public String getGroupId() {
        return groupId;
    }

    @JsonProperty("group_id")
    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

}