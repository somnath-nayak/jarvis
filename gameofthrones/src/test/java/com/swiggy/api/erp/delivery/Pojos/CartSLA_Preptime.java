package com.swiggy.api.erp.delivery.Pojos;

import com.swiggy.api.erp.delivery.constants.RTSConstants;
import com.swiggy.api.sf.checkout.helper.CommonAPIHelper;

public class CartSLA_Preptime {

	private String session_id;

    private String request_id;

    private String bill;

    private Customer_segment customer_segment;

    private String order_id;

    private Restaurant restaurant;

    private String listing_item_count;

    private Last_miles[] last_miles;

    public String getSession_id ()
    {
        return session_id;
    }

    public void setSession_id (String session_id)
    {
        this.session_id = session_id;
    }

    public String getRequest_id ()
    {
        return request_id;
    }

    public void setRequest_id (String request_id)
    {
        this.request_id = request_id;
    }

    public String getBill ()
    {
        return bill;
    }

    public void setBill (String bill)
    {
        this.bill = bill;
    }

    public Customer_segment getCustomer_segment ()
    {
        return customer_segment;
    }

    public void setCustomer_segment (Customer_segment customer_segment)
    {
        this.customer_segment = customer_segment;
    }

    public String getOrder_id ()
    {
        return order_id;
    }

    public void setOrder_id (String order_id)
    {
        this.order_id = order_id;
    }

    public Restaurant getRestaurant ()
    {
        return restaurant;
    }

    public void setRestaurant (Restaurant restaurant)
    {
        this.restaurant = restaurant;
    }

    public String getListing_item_count ()
    {
        return listing_item_count;
    }

    public void setListing_item_count (String listing_item_count)
    {
        this.listing_item_count = listing_item_count;
    }

    public Last_miles[] getLast_miles ()
    {
        return last_miles;
    }

    public void setLast_miles (Last_miles[] last_miles)
    {
        this.last_miles = last_miles;
    }

    public void setdefaultresult()
    {
    	Last_miles last_mile=new Last_miles();
    	Customer_segment cust_seg=new Customer_segment();
    	cust_seg.setdefaultresultset();
    	last_mile.setdefaultvalues();
    	Restaurant rest=new Restaurant();
    	rest.defaultresultset();
    	 CommonAPIHelper commonAPIHelper = new CommonAPIHelper();
         int random = commonAPIHelper.getRandomNo(0,10000);
    	if(this.getBill()==null)
    	{
    		this.setBill("1500.5");
    	}
    	if(this.getCustomer_segment()==null)
    	{
    		this.setCustomer_segment(cust_seg);
    	}
    	if(this.getLast_miles()==null)
    	{
    		this.setLast_miles(new Last_miles []{last_mile});
    	}
    	if(this.getListing_item_count()==null)
    	{
    		this.setListing_item_count("7");
    	}
    	if(this.getOrder_id()==null)
    	{
    		this.setOrder_id(RTSConstants.randomorder_id+random);
    	}
    	if(this.getRequest_id()==null)
    	{
    		this.setRequest_id(RTSConstants.defaultrequestid);
    	}
    	if(this.getRestaurant()==null)
    	{
    		this.setRestaurant(rest);
    	}
    		
    	if(this.getSession_id()==null)
    	{
    		this.setSession_id(session_id);
    	}
    }
    
    
    
    
    @Override
    public String toString()
    {
        return "ClassPojo [session_id = "+session_id+", request_id = "+request_id+", bill = "+bill+", customer_segment = "+customer_segment+", order_id = "+order_id+", restaurant = "+restaurant+", listing_item_count = "+listing_item_count+", last_miles = "+last_miles+"]";
    }

			

}
