package com.swiggy.api.erp.cms.pojo.FullMenu;

import com.swiggy.api.erp.cms.constants.MenuConstants;

/**
 * Created by kiran.j on 2/2/18.
 */
public class Variants
{
    private String id;

    private String veg;

    private Gst_Details gst_details;
    
    private String default_dependent_variant_id;

    private Boolean is_default;

    private double price;

    private int order;

    private boolean in_stock;

    private boolean is_veg;

    private String name;

    private String default_dependent_variant_group_id;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getVeg ()
    {
        return veg;
    }

    public void setVeg (String veg)
    {
        this.veg = veg;
    }

    public String getDefault_dependent_variant_id ()
{
    return default_dependent_variant_id;
}

    public void setDefault_dependent_variant_id (String default_dependent_variant_id)
    {
        this.default_dependent_variant_id = default_dependent_variant_id;
    }

    public Boolean getDefault ()
    {
        return is_default;
    }

    public void setDefault (Boolean is_default)
    {
        this.is_default = is_default;
    }

    public double getPrice ()
    {
        return price;
    }

    public void setPrice (double price)
    {
        this.price = price;
    }

    public int getOrder ()
    {
        return order;
    }

    public void setOrder (int order)
    {
        this.order = order;
    }

    public boolean getIn_stock ()
    {
        return in_stock;
    }

    public void setIn_stock (boolean in_stock)
    {
        this.in_stock = in_stock;
    }

    public boolean getIs_veg ()
    {
        return is_veg;
    }

    public void setIs_veg (boolean is_veg)
    {
        this.is_veg = is_veg;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }
    public Gst_Details getGst_details ()
    {
        return gst_details;
    }

        public void setGst_details (Gst_Details gst_details)
        {
            this.gst_details = gst_details;
        }

    public String getDefault_dependent_variant_group_id ()
{
    return default_dependent_variant_group_id;
}

    public void setDefault_dependent_variant_group_id (String default_dependent_variant_group_id)
    {
        this.default_dependent_variant_group_id = default_dependent_variant_group_id;
    }

    public Variants build() {
        setDefaultValues();
        return this;
    }

    public void setDefaultValues() {
    	 Gst_Details gst_details = new Gst_Details();
        if(this.getId() == null)
            this.setId(MenuConstants.variants_id);
        if(this.getName() == null)
            this.setName(MenuConstants.variants_name);
        if(!this.in_stock)
        	this.setIn_stock(true);
        this.setGst_details(gst_details);
        this.is_default=true;
    }

    @Override
    public String toString()
    {
        return "{ id = "+id+", veg = "+veg+", default_dependent_variant_id = "+default_dependent_variant_id+", default = "+is_default+", price = "+price+", order = "+order+", in_stock = "+in_stock+", is_veg = "+is_veg+", name = "+name+", default_dependent_variant_group_id = "+default_dependent_variant_group_id+"}";
    }
}
