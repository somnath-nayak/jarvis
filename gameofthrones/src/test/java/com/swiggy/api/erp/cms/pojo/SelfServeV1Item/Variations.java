package com.swiggy.api.erp.cms.pojo.SelfServeV1Item;

public class Variations {
	  private Gst_details gst_details;

	    private String default1;

	    private String isVeg;

	    private Catalog_attributes catalog_attributes;

	    private String price;

	    private String variantGroupId;

	    private String name;

	    private String externalVariantId;

	    private String inStock;

	    private Catalog_multi_value_attributes[] catalog_multi_value_attributes;

	    private String id;

	    private String uniqueId;

	    private String order;

	    public Gst_details getGst_details ()
	    {
	        return gst_details;
	    }

	    public void setGst_details (Gst_details gst_details)
	    {
	        this.gst_details = gst_details;
	    }

	    public String getDefault ()
	    {
	        return default1;
	    }

	    public void setDefault (String default1)
	    {
	        this.default1 = default1;
	    }

	    public String getIsVeg ()
	    {
	        return isVeg;
	    }

	    public void setIsVeg (String isVeg)
	    {
	        this.isVeg = isVeg;
	    }

	    public Catalog_attributes getCatalog_attributes ()
	    {
	        return catalog_attributes;
	    }

	    public void setCatalog_attributes (Catalog_attributes catalog_attributes)
	    {
	        this.catalog_attributes = catalog_attributes;
	    }

	    public String getPrice ()
	    {
	        return price;
	    }

	    public void setPrice (String price)
	    {
	        this.price = price;
	    }

	    public String getVariantGroupId ()
	    {
	        return variantGroupId;
	    }

	    public void setVariantGroupId (String variantGroupId)
	    {
	        this.variantGroupId = variantGroupId;
	    }

	    public String getName ()
	    {
	        return name;
	    }

	    public void setName (String name)
	    {
	        this.name = name;
	    }

	    public String getExternalVariantId ()
	    {
	        return externalVariantId;
	    }

	    public void setExternalVariantId (String externalVariantId)
	    {
	        this.externalVariantId = externalVariantId;
	    }

	    public String getInStock ()
	    {
	        return inStock;
	    }

	    public void setInStock (String inStock)
	    {
	        this.inStock = inStock;
	    }

	    public Catalog_multi_value_attributes[] getCatalog_multi_value_attributes ()
	    {
	        return catalog_multi_value_attributes;
	    }

	    public void setCatalog_multi_value_attributes (Catalog_multi_value_attributes[] catalog_multi_value_attributes)
	    {
	        this.catalog_multi_value_attributes = catalog_multi_value_attributes;
	    }

	    public String getId ()
	    {
	        return id;
	    }

	    public void setId (String id)
	    {
	        this.id = id;
	    }

	    public String getUniqueId ()
	    {
	        return uniqueId;
	    }

	    public void setUniqueId (String uniqueId)
	    {
	        this.uniqueId = uniqueId;
	    }

	    public String getOrder ()
	    {
	        return order;
	    }

	    public void setOrder (String order)
	    {
	        this.order = order;
	    }

	    @Override
	    public String toString()
	    {
	        return "ClassPojo [gst_details = "+gst_details+", default = "+default1+", isVeg = "+isVeg+", catalog_attributes = "+catalog_attributes+", price = "+price+", variantGroupId = "+variantGroupId+", name = "+name+", externalVariantId = "+externalVariantId+", inStock = "+inStock+", catalog_multi_value_attributes = "+catalog_multi_value_attributes+", id = "+id+", uniqueId = "+uniqueId+", order = "+order+"]";
	    }
	}
				
				