package com.swiggy.api.erp.delivery.helper;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.delivery.Pojos.Last_miles;
import com.swiggy.api.erp.delivery.Pojos.Restaurant;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;

import java.io.IOException;
import java.util.HashMap;

public class CartSLA_Helper {
	Initialize gameofthrones = new Initialize();
	DeliveryHelperMethods delmeth=new DeliveryHelperMethods();
	
	 public Processor cartslaapi(Restaurant restaurant,String listing_item_count,Last_miles []last_mile,String bill,String order_id,String cart_id,String cart_state,String total_item_count) throws IOException
	    {
	    	HashMap<String, String> requestheaders = delmeth.singleheader();
			JsonHelper jsonhel=new JsonHelper();
			String rest=jsonhel.getObjectToJSON(restaurant);
			String last_mil=jsonhel.getObjectToJSON(last_mile);
	    	GameOfThronesService service = new GameOfThronesService(
					"deliverycerebro", "cartSLAA", gameofthrones);
			String[] payloadparams = new String[] { rest, listing_item_count,last_mil,bill,order_id,cart_id,cart_state,total_item_count};
			Processor processor = new Processor(service, requestheaders,
					payloadparams);
			return processor;
	    }

	
	 public Last_miles setlastmile(Last_miles last_mile,String address_lat_lng,String id)
	    {
		 	last_mile.setdefaultvalues();
		 	last_mile.setAddress_lat_long(address_lat_lng);
	    	last_mile.setId(id);
			return last_mile;
				    }
	 public Restaurant setRestaurantdetails(Restaurant restaurant,String area_id,String city_id,String is_long_distance_enabled,String lat_long,String restaurant_id,String restaurant_type,String zone_id)
	    
	 {
		 restaurant.defaultresultset();
		 restaurant.setArea_id(area_id);
		 restaurant.setCity_id(city_id);
		 restaurant.setIs_long_distance_enabled(is_long_distance_enabled);
		 restaurant.setLat_long(lat_long);
		 restaurant.setRestaurant_id(restaurant_id);
		 restaurant.setRestaurant_type(restaurant_type);
		 restaurant.setZone_id(zone_id);
		return restaurant;
	 }
	 
	 public Object getlastmilesendbyDS(String hour,String weekday,String restaurant_customer_distance,String restaurantid,String customer_geohash,String banner_factor,String zoneid) throws IOException
	    {
	    	HashMap<String, String> requestheaders = delmeth.singleheader();
			GameOfThronesService service = new GameOfThronesService(
					"dspredicttime", "predictlastmiletime", gameofthrones);
			String[] payloadparams = new String[] {hour,weekday,restaurant_customer_distance,restaurantid,customer_geohash,banner_factor,zoneid};
			Processor processor = new Processor(service, requestheaders,
					payloadparams);
			String resp=processor.ResponseValidator.GetBodyAsText();
			System.out.println(resp);
			Object last=JsonPath.read(resp,"$.result..predicted_last_mile").toString().replace("[", "").replace("]", "");
			if(null==last)
			{
				return null;
			}
			else
			{
			return last;
	    }
	    }
	 public Object getpreptimesendbyDS(String hour,String weekday,String order_item_count,String bill_scaled,String restaurantid,String zoneid,String slot,String banner_factor,String order_total_quantity,String bill_amount) throws IOException
	    {
	    	HashMap<String, String> requestheaders = delmeth.singleheader();
			GameOfThronesService service = new GameOfThronesService(
					"dspredicttime", "predictpreptime", gameofthrones);
			String[] payloadparams = new String[] {hour,weekday,order_item_count,bill_scaled,restaurantid,zoneid,slot,banner_factor,order_total_quantity,bill_amount};
			Processor processor = new Processor(service, requestheaders,
					payloadparams);
			String resp=processor.ResponseValidator.GetBodyAsText();
			Object prep=JsonPath.read(resp,"$.result..predicted_prep_time").toString().replace("[", "").replace("]", "");
			if(null==prep)
			{
				return null;
			}
			
			else
			{
			return prep;
	    }
	    }
	 
	 /*public int getbannerfactor(String keyinitial,String entity,int redisdb)
	 {
		 DeliveryHelperMethods delmeth=new DeliveryHelperMethods();
		Set<String> s= delmeth.getsMembers(keyinitial, entity, redisdb);
		 if(s.size()!=0)
		 {
			 return s.size(); 
		 }
		 else
		 {
			 return 0;
		 }
		*/
		 
	 
	 }