package com.swiggy.api.erp.vms.dp;

import com.swiggy.api.erp.vms.constants.RMSConstants;
import com.swiggy.api.erp.vms.helper.PlacingServiceHelper;
import com.swiggy.api.erp.vms.helper.RMSHelper;
import org.testng.annotations.DataProvider;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PlacingServiceDP {

	PlacingServiceHelper helper = new PlacingServiceHelper();
	RMSHelper rmsHelper = new RMSHelper();
	
	@DataProvider(name = "createEvent")
    public Iterator<Object[]> createEvent() {
		helper.deleteEvents();
		return getCreateEventData().iterator();
    }
	
	@DataProvider(name = "createEventRegression1")
    public Object[][] createEventRegression1() {
		createEvents();
		return new Object[][] 
       	{
        	{"ROUTED", RMSConstants.updatedBy, 1, "Event with type ROUTED already exists."},
        	{"WITH_RELAYER", RMSConstants.updatedBy, 1, "Event with type WITH_RELAYER already exists."},
        	{"RELAYED", RMSConstants.updatedBy, 1, "Event with type RELAYED already exists."},
        	{"PLACED", RMSConstants.updatedBy, 1, "Event with type PLACED already exists."},
        	{"FOOD_PREPARED", RMSConstants.updatedBy, 1, "Event with type FOOD_PREPARED already exists." },
        	{"CALL_PARTNER", RMSConstants.updatedBy, 1, "Event with type CALL_PARTNER already exists." },
        	{"OUT_OF_STOCK", RMSConstants.updatedBy, 1, "Event with type OUT_OF_STOCK already exists." },
       	};                  
                             
    }

	
	@DataProvider(name = "createEventRegression2")
    public static Object[][] createEventRegression2() {

		return new Object[][] 
       	{
        	{" ", RMSConstants.updatedBy, 400, "Bad Request" },
        	{"PLACEDD", " ", 400, "Bad Request" }
        	
       	};                  
                             
    }
	
	@DataProvider(name = "getAndDeleteEvent")
    public Object[][] getAndDeleteEvent() {
		createEvents();
        int eventId = 0, transId = 0;
		return new Object[][] 
       	{
        	{RMSConstants.geteventMessage, eventId, RMSConstants.deleteeventMessage, transId, RMSConstants.deleteTransition }
       	};	
	}
	
	@DataProvider(name = "deleteEventRegression")
    public static Object[][] deleteEventRegression() {

		return new Object[][] 
       	{
        	{0, 1, "No class com.swiggy.placingservice.fsmcore.entities.EventEntity entity with id 0 exists!" },
        	{-1, 1, "No class com.swiggy.placingservice.fsmcore.entities.EventEntity entity with id -1 exists!" }        	
       	};                  
                             
    }
	
	@DataProvider(name = "getAndDeleteState")
    public static Object[][] getAndDeleteState() {

        int stateId = 0;
        int transId = 0;
		return new Object[][] 
       	{
        	{RMSConstants.getstatetMessage, stateId , RMSConstants.deletestateMessage, transId, RMSConstants.deleteTransition }
       	};	
	}
	
	
	@DataProvider(name = "createState")
    public Iterator<Object[]> createState() {
		//helper.deleteStates();
		return getCreateStatesData().iterator();
    }
	
	@DataProvider(name = "createStateRegression1")
    public Object[][] createStateRegression1() {
		createStates();
		return new Object[][] 
       	{
        	{"ROUTED", RMSConstants.updatedBy, 1, "State with type ROUTED already exists."},
        	{"WITH_RELAYER", RMSConstants.updatedBy, 1, "State with type WITH_RELAYER already exists."},
        	{"RELAYED", RMSConstants.updatedBy, 1, "State with type RELAYED already exists."},
        	{"PLACED", RMSConstants.updatedBy, 1, "State with type PLACED already exists."},
        	{"FOOD_PREPARED", RMSConstants.updatedBy, 1, "State with type FOOD_PREPARED already exists." },
        	{"CALL_PARTNER", RMSConstants.updatedBy, 1, "State with type CALL_PARTNER already exists." },
        	{"OUT_OF_STOCK", RMSConstants.updatedBy, 1, "State with type OUT_OF_STOCK already exists." },
       	};                  
                             
    }

	
	@DataProvider(name = "createStateRegression")
    public static Object[][] createStateRegression() {

		return new Object[][] 
       	{
        	{" ", RMSConstants.updatedBy, 400, "Bad Request" },
        	{"PLACEDD", " ", 400, "Bad Request" }        	     	
       	};                  
                             
    }
	
	@DataProvider(name = "deleteStateRegression")
    public static Object[][] deleteStateRegression() {

		return new Object[][] 
       	{
        	{0, 1, "No class com.swiggy.placingservice.fsmcore.entities.StateEntity entity with id 0 exists!" },
        	{-1, 1, "No class com.swiggy.placingservice.fsmcore.entities.StateEntity entity with id -1 exists!" }        	
       	};                  
                             
    }
	
	@DataProvider(name = "addStateTransition")
    public static Object[][] addStateTransition() {

		int fromStateId = 0;
		int eventId = 0;
		int toStateId = 0;
		int transId = 0;

		
        return new Object[][] 
       	{
        	{fromStateId, eventId, toStateId, RMSConstants.updatedBy, RMSConstants.addstateTransitionMessage, transId, RMSConstants.deleteTransition }   	
       	};                  
                             
    }
	
	@DataProvider(name = "addStateTransitionRegression")
    public static Object[][] addStateTransitionRegression() {

		int fromStateId = 0;
		int eventId = 0;
		int toStateId = 0;
		int transId = 0;

		
        return new Object[][] 
       	{
        	{fromStateId, eventId, toStateId, RMSConstants.updatedBy, RMSConstants.addstateTransitionMessage, transId, RMSConstants.deleteTransition }   	
       	};                  
                             
    }
	
	@DataProvider(name = "addStateTransitionRegression2")
    public static Object[][] addStateTransitionRegression2() {

		int fromStateId = 0;
		int eventId = 0;
		int toStateId = 0;

        return new Object[][] 
       	{
        	{0, eventId, toStateId, RMSConstants.updatedBy, 1, "Could not add the State Transition" },
        	{1000, eventId, toStateId, RMSConstants.updatedBy, 1, "Could not add the State Transition" },
        	{-1, eventId, toStateId, RMSConstants.updatedBy, 1, "Could not add the State Transition" },
        	{fromStateId, 0, toStateId, RMSConstants.updatedBy, 1, "Could not add the State Transition" },
        	{fromStateId, 1000, toStateId, RMSConstants.updatedBy, 1, "Could not add the State Transition" },
        	{fromStateId, -1, toStateId, RMSConstants.updatedBy, 1, "Could not add the State Transition" },
        	{fromStateId, eventId, 0, RMSConstants.updatedBy, 1, "Could not add the State Transition" },
        	{fromStateId, eventId, 1000, RMSConstants.updatedBy, 1, "Could not add the State Transition" },
        	{fromStateId, eventId, -1, RMSConstants.updatedBy, 1, "Could not add the State Transition" }
       	};                  
                             
    }

	
	@DataProvider(name = "getAllStateTransition")
    public static Object[][] getAllStateTransition() {

        return new Object[][] 
       	{
        	{RMSConstants.getstatetTransitionMessage}   	
       	};                  
                             
    }

	@DataProvider(name = "makeTransition")
    public Iterator<Object[]> makeTransition() throws InterruptedException {

        List<Object[]> data = new ArrayList<>();
        String[] types = RMSConstants.transition_types;
        for(int i = 0; i<types.length; i++) {
        	Thread.sleep(1000);
        	data.add(new Object[]{rmsHelper.getUniqueId(), types[i], RMSConstants.eventTime, RMSConstants.order, RMSConstants.updatedService, RMSConstants.restaurantId, RMSConstants.makeTramsitionMessage});
		}

		return data.iterator();
    }
	
	@DataProvider(name = "makeTransitionRegression")
    public static Object[][] makeTransitionRegression() {
		
        return new Object[][] 
       	{
        	{"20", " ", RMSConstants.eventTime, RMSConstants.order, RMSConstants.updatedService, RMSConstants.restaurantId, 400, "Bad Request"},
        	{"20", "RELAYED", null, RMSConstants.order, RMSConstants.updatedService, RMSConstants.restaurantId, 400, "Bad Request"},
        	{"20", "WITH_RELAYER", RMSConstants.eventTime, RMSConstants.order, " ", RMSConstants.restaurantId, 400, "Bad Request"}
       	};                  
                             
    }
	

	@DataProvider(name = "getPlacingState")
    public static Object[][] getPlacingState() {

        return new Object[][] 
       	{
        	{RMSConstants.order, RMSConstants.getPlacingStateMessage}   	
       	};                  
                             
    }
	
	@DataProvider(name = "getPlacingStateRegression")
    public static Object[][] getPlacingStateRegression() {

        return new Object[][] 
       	{
        	{123, 1, "Placing state does not exists for this order 123"},
        	{0, 1, "Placing state does not exists for this order 0"},
        	{-1, 1, "Placing state does not exists for this order -1"}
       	};                  
                             
    }
	
	@DataProvider(name = "deleteStateTransition")
    public static Object[][] deleteStateTransition() {

		int transId = 0;

        return new Object[][] 
       	{
        	{transId, RMSConstants.deleteTransition}   	
       	};                  
                             
    }
	
	@DataProvider(name = "deleteStateTransitionRegression")
    public static Object[][] deleteStateTransitionRegression() {

        return new Object[][] 
       	{
        	{1000, 1, "Error while deleting state transition map"},
        	{0, 1, "Error while deleting state transition map"},
        	{-1, 1, "Error while deleting state transition map"}
       	};                  
                             
    }

	@DataProvider(name = "getHealthCheck")
    public static Object[][] getHealthCheck() {

        return new Object[][] 
       	{
        	{RMSConstants.healthCheckMessage}   	
       	};                  
                             
    }

    public List<Object[]> getCreateEventData() {
		String[] types = RMSConstants.event_types;
		List<Object[]> data = new ArrayList<>();
		for(int i=0;i<types.length;i++) {
			data.add(new Object[]{types[i], RMSConstants.updatedBy, RMSConstants.addeventMessage, RMSConstants.geteventMessage, 0, RMSConstants.deleteeventMessage});
		}
		return data;
	}

	public List<Object[]> getCreateStatesData() {
		String[] types = RMSConstants.event_types;
		List<Object[]> data = new ArrayList<>();
		for(int i=0;i<types.length;i++) {
			data.add(new Object[]{types[i], RMSConstants.updatedBy, RMSConstants.addstateMessage, RMSConstants.getstatetMessage, 0, RMSConstants.deletestateMessage});
		}
		return data;
	}

	public void createEvents() {
		List<Object[]> data = getCreateEventData();
		for(Object[] arr : data) {
				helper.createEvent(arr[0].toString(), arr[1].toString());
		}
	}

	public void createStates() {
		List<Object[]> data = getCreateStatesData();
		for(Object[] arr : data) {
			helper.createState(arr[0].toString(), arr[1].toString());
		}
	}
	
}
