package com.swiggy.api.erp.delivery.dp;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.testng.annotations.DataProvider;

import com.swiggy.api.erp.delivery.constants.ClusterConstants;
import com.swiggy.api.erp.delivery.helper.ClusterServiceHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryDataHelper;

import edu.emory.mathcs.backport.java.util.Arrays;
import framework.gameofthrones.Aegon.SystemConfigProvider;

public class ClusterServiceDataProvider 
{
	ClusterServiceHelper clusterHelper = new ClusterServiceHelper();
	DeliveryDataHelper dataHelper = new DeliveryDataHelper();
	
	public double restaurantLat(String item_ref_id)
	{
		double restaurantlat = Double.valueOf(SystemConfigProvider.getTemplate("deliveryclusterdb")
																  .queryForList("select lat from items where reference_id='" + item_ref_id +"'").get(0).get("lat").toString());
		System.out.println("lat" + restaurantlat);
		return restaurantlat;
	}
	
	
	public double restaurantLng(String item_ref_id)
	{
		double restaurantlng = Double.valueOf(SystemConfigProvider.getTemplate("deliveryclusterdb")
																  .queryForList("select lng from items where reference_id='" + item_ref_id +"'").get(0).get("lng").toString());
		System.out.println("lng" + restaurantlng);
		return restaurantlng;
	}
	
	
	public String getAreaId(String restaurant_ref_id)
	{
		String[] item_attributes = SystemConfigProvider.getTemplate("deliveryclusterdb")
													   .queryForMap("select attributes from items where reference_id='" + restaurant_ref_id + "'").get("attributes").toString().replace("{", "").replace("}", "").replace(" ", "").split(",");
		System.out.println("*****ItemAttributes******" + Arrays.toString(item_attributes));
		String areaId = "";
		for(String area : item_attributes)
		{
			boolean areaExists = area.contains("areaId");
			if(areaExists)
			{
				String[] areaDetails = area.replace(" ", "").split(":");
				areaId = areaDetails[1];
				break;
			}
		}
		System.out.println("zone id" + areaId);
		return areaId;
	}
	
	
	public String getZoneId(String restaurant_ref_id)
	{
		String[] item_attributes = SystemConfigProvider.getTemplate("deliveryclusterdb")
													   .queryForList("select attributes from items where reference_id='" + restaurant_ref_id + "'").toString().replace("{", "").replace("}", "").replace(" ", "").split(",");
		System.out.println("*****ItemAttributes******" + Arrays.toString(item_attributes));
		String zoneId = "";
		for(String zone : item_attributes)
		{
			boolean zoneExists = zone.contains("zoneId");
			if(zoneExists)
			{
				String[] zoneDetails = zone.replace(" ", "").split(":");
				zoneId = zoneDetails[1];
				break;
			}
		}
		System.out.println("zone id" + zoneId);
		return zoneId;
	}
	
	
	String getItemId(String refId)
	{
		String id = SystemConfigProvider.getTemplate("deliveryclusterdb")
										.queryForMap("select id from items where reference_id='" + refId + "'").get("id").toString().replace("{", "").replace("}", "").replace(" ", "");
		return id;
	}
	
	
	public HashMap<String, String> itemHashMap()
	{
		HashMap<String, String> itemMap = new LinkedHashMap<String, String>();
		itemMap.put("avgPrepTime", "15.68");
		itemMap.put("serviceable", "1");
		itemMap.put("avgItemCount", "1.68");
		itemMap.put("prepTimePeak", "35");
		itemMap.put("maxSecondMile", "10");
		itemMap.put("avgPrepTimeWDP", "15.73");
		itemMap.put("avgPrepTimeWEP", "18.03");
		itemMap.put("avgPrepTimeWDNP", "16.19");
		itemMap.put("avgPrepTimeWENP", "17.45");
		itemMap.put("minsPerKm80Perc", "5.99");
		itemMap.put("avgAreaFirstMile", "5.31");
		itemMap.put("avgRestFirstMile", "4.99");
		itemMap.put("avgPlacementDelay", "5");
		itemMap.put("compliancePercent", "0.66");
		itemMap.put("percentWithDeOrders", "0.03");
		itemMap.put("selfDeliveryOverriden", "0");
		itemMap.put("avgAssignmentDelayRest", "5.29");
		itemMap.put("avgAssignmentDelayLunch", "6.85");
		itemMap.put("lastMileRegressionSlope", "2.63");
		itemMap.put("avgAssignmentDelayDinner", "4.89");
		itemMap.put("lastMileRegressionIntercept", "8.25");
		itemMap.put("avgFirstMile", "10");
		itemMap.put("slaPrepTimeCategoryFlag", "true");
		itemMap.put("restaurantFirstMileFlag", "true");
		return itemMap;
	}
	
	public LinkedHashMap<String, String> selfDelItemAttributes()
	{
		LinkedHashMap<String, String> itemMap = new LinkedHashMap<String, String>();
		itemMap.put("areaId", ClusterConstants.area_id);
		itemMap.put("zoneId", ClusterConstants.zone_id);
		itemMap.put("partnerId", ClusterConstants.partnerId);
		itemMap.put("serviceable", ClusterConstants.serviceable);
		itemMap.put("fixedSla", ClusterConstants.fixedSla);
		itemMap.put("deliveryPartnerActive", ClusterConstants.deliveryPartnerActive);
		itemMap.put("selfDeliveryOverriden", ClusterConstants.selfDeliveryOverriden);
		itemMap.put("deliveryPartnerFixedSla", ClusterConstants.deliveryPartnerFixedSla);
		
		return itemMap;
	}
	
	
	public String[] getCreateItemElements(String lat, String lng, String refId, String areaId, String zoneId) 
	{
		String[] attributes = new String[itemHashMap().size()+5]; 
		attributes[0] = lat;
		attributes[1] = lng;
		attributes[2] = refId; 
		attributes[3] = areaId;
		attributes[4] = zoneId;
		int i = 5;
		for (Map.Entry<String, String> entry : itemHashMap().entrySet()) {
			attributes[i++] = entry.getValue();
		}
		System.out.println("***item create attributes******" + Arrays.toString(attributes));
		return attributes;
	}
	
	
	public String[] getSelfDelItemElements(String lat, String lng, String refId, LinkedHashMap<String, String> map) 
	{
		String[] attributes = new String[map.size() + 3];
		attributes[0] = lat;
		attributes[1] = lng;
		attributes[2] = refId;
		int i = 3;
		for (Map.Entry<String, String> entry : map.entrySet()) {
			attributes[i++] = entry.getValue();
		}
		System.out.println("***self delivery item create attributes******" + Arrays.toString(attributes));
		return attributes;
	}
	
	
	public String[] getUpdateItemElements(String lat, String lng, String areaId, String zoneId) 
	{
	String[] attributes = new String[itemHashMap().size()+4]; 
	attributes[0] = lat;
	attributes[1] = lng;
	attributes[2] = areaId;
	attributes[3] = zoneId;
		int i = 4;
		for (Map.Entry<String, String> entry : itemHashMap().entrySet()) {
	            attributes[i++] = entry.getValue();
	        }
		System.out.println("***item update attributes******" + Arrays.toString(attributes));
		return attributes;
	}
	
	
	String item_refId = ClusterConstants.ref_id;
	String[] CustomerLatLng = dataHelper.getApproxLatLongAtADistance(Double.valueOf(ClusterConstants.item_lat), Double.valueOf(ClusterConstants.item_lng), 1.0).split(",");

	
	@DataProvider(name = "CSTC_01")					
	public Object[][] itemDistancelessThanConfig() throws Exception 
	{
		return new Object[][] { { CustomerLatLng[0], CustomerLatLng[1]}};
	}
	
	@DataProvider(name = "CSTC_02")					
	public Object[][] itemDistancegreaterThanConfig() throws Exception 
	{
		String[] Customer_LatLng = dataHelper.getApproxLatLongAtADistance(Double.valueOf(ClusterConstants.item_lat), 
																		  Double.valueOf(ClusterConstants.item_lng), 5.0).split(",");
		return new Object[][] { { Customer_LatLng[0], Customer_LatLng[1]}};
	}
	
	@DataProvider(name = "CSTC_07")					
	public Object[][] itemBannerlessThanConfig() throws Exception 
	{
		return new Object[][] { { CustomerLatLng[0], CustomerLatLng[1]}};
	}
	
	@DataProvider(name = "CSTC_08")					
	public Object[][] itemBannerGreaterThanConfig() throws Exception 
	{
		return new Object[][] { { CustomerLatLng[0], CustomerLatLng[1], "4"}};
	}
	
	@DataProvider(name = "CSTC_11")					
	public Object[][] zoneClosedAtCluster() throws Exception 
	{
		return new Object[][] { { CustomerLatLng[0], CustomerLatLng[1], "false"}};
	}
	
	@DataProvider(name = "CSTC_12")					
	public Object[][] zoneClosedAtGlobalCluster() throws Exception 
	{
		return new Object[][] { { CustomerLatLng[0], CustomerLatLng[1], "false"}};
	}
	
	@DataProvider(name = "CSTC_13")
	public Object[][] itemSlaLessThanConfig() throws Exception 			
	{
		return new Object[][] { { CustomerLatLng[0], CustomerLatLng[1]}};
	}
	
	@DataProvider(name = "CSTC_14")			
	public Object[][] itemSlaGreaterThanConfig() throws Exception 
	{
		return new Object[][] { { CustomerLatLng[0], CustomerLatLng[1], "10"}};
	}
	
	@DataProvider(name = "CSTC_17")			
	public Object[][] enabledConnectionExecuting() throws Exception 
	{
		return new Object[][] { {CustomerLatLng[0], CustomerLatLng[1], "1"}};
	}
	
	@DataProvider(name = "CSTC_18")			
	public Object[][] enabledConnectionExecutingAsPriority() throws Exception 
	{
		return new Object[][] { {CustomerLatLng[0], CustomerLatLng[1], "1"}};
	}
	
	@DataProvider(name = "CSTC_20")
	public Object[][] connectionInclusionType() throws Exception
	{
		return new Object[][] {{ CustomerLatLng[0], CustomerLatLng[1]}};
	}
	
	@DataProvider(name = "CSTC_21")
	public Object[][] connectionInclusionType_Exclusion() throws Exception
	{
		return new Object[][] {{"exclusion", CustomerLatLng[0], CustomerLatLng[1]}};
	}
	
	@DataProvider(name = "CSTC_22")
	public Object[][] itemServiceable_When2ndConnection_InclusionType() throws Exception
	{
		return new Object[][] {{"2", "1", CustomerLatLng[0], CustomerLatLng[1]}};
	}
	
	@DataProvider(name = "CSTC_23")
	public Object[][] itemServiceable_When2ndConnection_ExclusionType() throws Exception
	{
		return new Object[][] {{1, CustomerLatLng[0], CustomerLatLng[1], "2.5"}};
	}
	
	@DataProvider(name = "CSTC_24-&-CSTC_25")
	public Object[][] serviceabilityBasedOnFilters() throws Exception
	{
		return new Object[][] { { CustomerLatLng[0], CustomerLatLng[1], 2},
								{ CustomerLatLng[0], CustomerLatLng[1], 4}};
	}
	
	@DataProvider(name = "CSTC_26")
	public Object[][] onlyEnabled_ConnectionGroupExcecuting() throws Exception
	{
		return new Object[][] {{CustomerLatLng[0], CustomerLatLng[1]}};
	}
	
	@DataProvider(name = "CSTC_27")
	public Object[][] ConnectionGroupExcecution_asPriority() throws Exception
	{
		return new Object[][] {{CustomerLatLng[0], CustomerLatLng[1], 1}};
	}
	
	@DataProvider(name = "CSTC_29")
	public Object[][] CgExecution_asPerTimeSlot() throws Exception
	{
		return new Object[][] {{CustomerLatLng[0], CustomerLatLng[1]}};
	}
	
	@DataProvider(name = "CSTC_30")
	public Object[][] cgTimeSlotShouldNotBeNull() throws Exception
	{
		return new Object[][] {{CustomerLatLng[0], CustomerLatLng[1]}};
	}
	
	@DataProvider(name="CSTC_31")
	public Object[][] enabledMappedItem_toCluster_serviceable() throws Exception
	{
		return new Object[][] {{CustomerLatLng[0], CustomerLatLng[1]}};
	}
	
	@DataProvider(name="CSTC_32")
	public Object[][] disabledMappedItem_toCluster_unServiceable() throws Exception
	{
		return new Object[][] {{CustomerLatLng[0], CustomerLatLng[1]}};
	}
	
	@DataProvider(name = "CSTC_33")
	public Object[][] mappedItem_toCluster_deletedUnServiceable() throws Exception
	{
		String item_lat = "12.930171", item_lng = "77.625979";
		String[] CustomerLatLong = dataHelper.getApproxLatLongAtADistance(Double.valueOf(item_lat), Double.valueOf(item_lng), 0.5).split(",");
		return new Object[][] {{CustomerLatLong[0], CustomerLatLong[1], "55"}};
	}
	
	@DataProvider(name = "CSTC_34")
	public Object[][] disabledItem_mappedtoCluster_UnServiceable() throws Exception
	{
		return new Object[][] {{CustomerLatLng[0], CustomerLatLng[1]}};
	}
	
	@DataProvider(name = "CSTC_36")
	public Object[][] ItemMappedInMultiCluster_Serviceabile() throws Exception
	{
		String[] firstRestaurantCluster_create = {ClusterConstants.entity_type, 
				"12.932055", "77.609607", 
				"12.944184", "77.625014",	
				"12.932724", "77.635227", 
				"12.924358", "77.619220",
				ClusterConstants.cluster_id, ClusterConstants.clustersName[0], ClusterConstants.clusterType, ClusterConstants.restaurant_cluster_tag};
		
		String[] secondRestaurantCluster_create = {ClusterConstants.entity_type,
				"12.933644", "77.616216", 
				"12.923522", "77.604114",	
				"12.930591", "77.596861", 
				"12.938747", "77.603856",
				ClusterConstants.cluster_id, ClusterConstants.clustersName[1], ClusterConstants.clusterType, ClusterConstants.restaurant_cluster_tag};
		
		String[] customerCluster_create = {ClusterConstants.entity_type,
				"12.917774", "77.632678",	 
				"12.917802", "77.640248",	 
				"12.908638", "77.641065", 	 
				"12.907161", "77.631102",
				ClusterConstants.cluster_id, ClusterConstants.clustersName[2], ClusterConstants.clusterType, ClusterConstants.customer_cluster_tag};
		
		String defaultRefId = "210";
		String itemRefId = ClusterConstants.getRefIfd(defaultRefId);
		
		return new Object[][] {{firstRestaurantCluster_create, secondRestaurantCluster_create, 
								customerCluster_create, "12.932061", "77.612815", itemRefId, 
								"9000", "12.914488", "77.636012"}};
	}
	
	@DataProvider(name = "CSTC_39")
	public Object[][] disabledItem_mappedtoClusterUnServiceable() throws Exception
	{
		return new Object[][] {{CustomerLatLng[0], CustomerLatLng[1]}};
	}
	
	@DataProvider(name = "CSTC_40")
	public Object[][] itemUnserviceable_shownIn_ignoredItemList() throws Exception
	{
		return new Object[][] {{CustomerLatLng[0], CustomerLatLng[1], "4"}};
	}
	
	@DataProvider(name = "CSTC_41/42/43/44")
	public Object[][] prepTimeAddingWithSLA()
	{
		String[] customerLatLng = dataHelper.getApproxLatLongAtADistance(restaurantLat(item_refId), restaurantLng(item_refId), 0.5).split(",");
		
		return new Object[][] {{getUpdateItemElements(String.valueOf(restaurantLat(item_refId)),
								String.valueOf(restaurantLng(item_refId)), getAreaId(item_refId), 
								getZoneId(item_refId)), customerLatLng[0], customerLatLng[1], item_refId}};
	}
	
	@DataProvider(name = "CSTC_45")
	public Object[][] zoneSlaExtraTime_addingIn_CurrentSla() throws Exception
	{
		return new Object[][] {{CustomerLatLng[0], CustomerLatLng[1], "0"}};
	}
	
	/*@DataProvider(name="CSTC_46")
	public Object[][] avgFirstMileTime_addingIn_CurrentSla() throws Exception
	{
		String[] customerLatLng = getApproxLatLongAtADistance(restaurantLat("590"), restaurantLng("590"), 1.0).split(",");
		//String.valueOf(restaurantLat(item_refId)), String.valueOf(restaurantLng(item_refId))
		return new Object[][] {{item_attributes, customerLatLng[0], customerLatLng[1], "590"}};
	}*/
	
	@DataProvider(name="CSTC_47/48/49")
	public Object[][] avgAssignmentDelayAddingWithSLA()
	{
		String[] customerLatLng = dataHelper.getApproxLatLongAtADistance(Double.parseDouble(ClusterConstants.item_lat), Double.parseDouble(ClusterConstants.item_lng), 0.5).split(",");
		
		return new Object[][] {{getUpdateItemElements(String.valueOf(restaurantLat(item_refId)),
								String.valueOf(restaurantLng(item_refId)), getAreaId(item_refId), 
								getZoneId(item_refId)), customerLatLng[0], customerLatLng[1], item_refId}};
	}
	
	@DataProvider(name = "CSTC_50")
	public Object[][] avgPlacementDelay_addingInSla() throws Exception
	{
		String[] customerLatLng = dataHelper.getApproxLatLongAtADistance(Double.parseDouble(ClusterConstants.item_lat), Double.parseDouble(ClusterConstants.item_lng), 0.5).split(",");
		
		return new Object[][] {{customerLatLng[0], customerLatLng[1], item_refId, "0", 
								getUpdateItemElements(String.valueOf(restaurantLat(item_refId)), 
								String.valueOf(restaurantLng(item_refId)), getAreaId(item_refId), getZoneId(item_refId))}};
	}
	
	@DataProvider(name = "CSTC_51")
	public Object[][] slaRangeWidth_addingWith_Sla() throws Exception
	{
		String[] customerLatLng = dataHelper.getApproxLatLongAtADistance(Double.parseDouble(ClusterConstants.item_lat), Double.parseDouble(ClusterConstants.item_lng), 1.0).split(",");
		
		return new Object[][] {{customerLatLng[0], customerLatLng[1]}};
	}
	
	@DataProvider(name = "CSTC_52")
	public Object[][] itemCountSlaBeefUp_addingWith_Sla() throws Exception
	{
		String[] customerLatLng = dataHelper.getApproxLatLongAtADistance(Double.parseDouble(ClusterConstants.item_lat), Double.parseDouble(ClusterConstants.item_lng), 0.5).split(",");
		
		return new Object[][] {{customerLatLng[0], customerLatLng[1], item_refId}};
	}
	
	@DataProvider(name="CSTC_53/54/55/56")
	public Object[][] avgPrepTimeWDP_WDNP_WEP_WENPZero_prepTimePeakAddingWithSLA()
	{
		String[] customerLatLng = dataHelper.getApproxLatLongAtADistance(Double.parseDouble(ClusterConstants.item_lat), Double.parseDouble(ClusterConstants.item_lng), 0.5).split(",");
		
		return new Object[][] {{getUpdateItemElements(String.valueOf(restaurantLat(item_refId)),
								String.valueOf(restaurantLng(item_refId)), getAreaId(item_refId), 
								getZoneId(item_refId)), item_refId, customerLatLng[0], customerLatLng[1]}};
	}
	
	@DataProvider(name="CSTC_57/58")
	public Object[][] slaFlagFalse_avgPrepTime_addingWithSLA()
	{
		String[] customerLatLng = dataHelper.getApproxLatLongAtADistance(Double.parseDouble(ClusterConstants.item_lat), Double.parseDouble(ClusterConstants.item_lng), 0.5).split(",");
		
		return new Object[][] {{getUpdateItemElements(String.valueOf(restaurantLat(item_refId)),
								String.valueOf(restaurantLng(item_refId)), getAreaId(item_refId), 
								getZoneId(item_refId)), item_refId, customerLatLng[0], customerLatLng[1]}};
	}
	
	@DataProvider(name="CSTC_59/60")
	public Object[][] restaurantFlagTrue_AvgRestFirstMile_addingWithSLA()
	{
		String[] customerLatLng = dataHelper.getApproxLatLongAtADistance(Double.parseDouble(ClusterConstants.item_lat), Double.parseDouble(ClusterConstants.item_lng), 0.5).split(",");
		
		return new Object[][] {{getUpdateItemElements(String.valueOf(restaurantLat(item_refId)),
								String.valueOf(restaurantLng(item_refId)), getAreaId(item_refId), 
								getZoneId(item_refId)), item_refId, customerLatLng[0], customerLatLng[1]}};
	}
	
	@DataProvider(name="CSTC_61-69")
	public Object[][] selfDeliveryServiceabilityCheck()
	{
		String[] customerLatLng = dataHelper.getApproxLatLongAtADistance(Double.parseDouble(ClusterConstants.selfDelItemLat), Double.parseDouble(ClusterConstants.selfDelItemLng), 0.5).split(",");
		
		return new Object[][] {{customerLatLng[0], customerLatLng[1]}};
	}
	
	@DataProvider(name="CSTC_70-71_2")
	public Object[][] slaRangeWidthWontAddWithSla()
	{
		String[] customerLatLng = dataHelper.getApproxLatLongAtADistance(Double.parseDouble(ClusterConstants.selfDelItemLat), Double.parseDouble(ClusterConstants.selfDelItemLng), 0.5).split(",");
		
		return new Object[][] {{customerLatLng[0], customerLatLng[1]}};
	}
	
	@DataProvider(name = "CSTC_72")
	public Object[][] overlapping_cluster_withMidClusterAs_Blackzone() throws Exception
	{
		String[] inner_cluster_creation_payload = {ClusterConstants.entity_type, 
				"13.011668", "77.580791", 
				"13.012034", "77.588709",	
				"13.004905", "77.589760", 
				"13.004863", "77.578291",
				ClusterConstants.cluster_id, ClusterConstants.overlappingClusterNames[0], 
				ClusterConstants.clusterType, ClusterConstants.customer_cluster_tag};
		
		String[] mid_cluster_creation_payload = {ClusterConstants.entity_type, 
				"13.015180", "77.575309", 
				"13.016968", "77.593376",	
				"13.002867", "77.596508", 
				"13.003023", "77.573645",
				ClusterConstants.cluster_id, ClusterConstants.overlappingClusterNames[1], 
				ClusterConstants.clusterType, ClusterConstants.customer_cluster_tag};
		
		String[] outer_cluster_creation_payload = {ClusterConstants.entity_type,
				"13.020135", "77.568689", 
				"13.022790", "77.598268", 
				"12.998257", "77.603825", 
				"12.999061", "77.568892",
				ClusterConstants.cluster_id, ClusterConstants.overlappingClusterNames[2], 
				ClusterConstants.clusterType, ClusterConstants.customer_cluster_tag};
		
		String defaultItemRefId = "100";
		String refId = ClusterConstants.getRefIfd(defaultItemRefId);
		
		return new Object[][] {{inner_cluster_creation_payload, mid_cluster_creation_payload, 
								outer_cluster_creation_payload, "12.995625", "77.545064", refId, "50", "50", 
								"15000", "100", "13.008316", "77.585090", 
								"13.008804", "77.584809", "13.015533", "77.585676"}};
	}
	
	
	
	@DataProvider(name = "CSTC_73")
	public Object[][] itemServiceable_fromCluster_toCluster() throws Exception
	{
		String[] restaurant_cluster_payload = {ClusterConstants.entity_type, 
				"12.932806", "77.615791", 
				"12.933465", "77.626928",	
				"12.928582", "77.627422", 
				"12.927181", "77.616833",
				ClusterConstants.cluster_id, "Kor-RestCluster", 
				ClusterConstants.clusterType, ClusterConstants.restaurant_cluster_tag};
		
		String[] customer_creation_payload = {ClusterConstants.entity_type,
				"12.917774", "77.632678", 
				"12.917802", "77.640248", 
				"12.908638", "77.641065", 
				"12.907161", "77.631102",
				ClusterConstants.cluster_id, "HSR-CustCluster", ClusterConstants.clusterType, 
				ClusterConstants.customer_cluster_tag};
		
		String defaultItemRefId = "50";
		String refId = ClusterConstants.getRefIfd(defaultItemRefId);
		
		return new Object[][] {{restaurant_cluster_payload, customer_creation_payload, 
								"12.930386", "77.619560", refId, "12.914488", "77.636012", "10000"}};
	}
}
