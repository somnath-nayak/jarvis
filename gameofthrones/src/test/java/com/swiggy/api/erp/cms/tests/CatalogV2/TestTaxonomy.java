package com.swiggy.api.erp.cms.tests.CatalogV2;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.dp.CatalogV2TaxonomyDP;
import com.swiggy.api.erp.cms.helper.CatalogV2Helper;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created-By:Ashiwani
 */

public class TestTaxonomy extends CatalogV2TaxonomyDP {
    CatalogV2Helper cms=new CatalogV2Helper();
    public String response;
    public String taxid;
    public String storeid;


    @Test(dataProvider = "createtaxonomydp",description = "This test will create a Taxonomy",priority = 0)
    public void createTaxonomy(String payload,String sid){
        response=cms.createTaxonomy(payload,sid).ResponseValidator.GetBodyAsText();
        taxid=JsonPath.read(response,"$.taxonomy_id");
        storeid=JsonPath.read(response,"$.store_id");
        Assert.assertEquals(storeid,sid);

    }

    @Test(description = "This test will get a Taxonomy",priority = 1)
    public void getTaxonomy(){
        String response1=cms.getTaxonomy(storeid,taxid).ResponseValidator.GetBodyAsText();
        String taxid0=JsonPath.read(response1,"$.taxonomy_id");
        String storeid0=JsonPath.read(response1,"$.store_id");
        Assert.assertEquals(taxid0,taxid);
        Assert.assertEquals(storeid0,storeid);

    }

    @Test(description = "This test will publish a Taxonomy",priority = 2)
    public void publishTaxonomy(){
        String response2=cms.publishTaxonomy(storeid,taxid).ResponseValidator.GetBodyAsText();
        Assert.assertNotNull(response2);

    }

    @Test(description = "This test will get all taxonomy of a store",priority = 3)
    public void getAllTaxonomy(){
        String response2=cms.getAllTaxonomy(storeid).ResponseValidator.GetBodyAsText();
        ArrayList li=JsonPath.read(response2,"$..store_id");
        Assert.assertEquals(li.get(0), storeid);
        Assert.assertNotNull(response2);

    }

    @Test(dataProvider = "updatetaxonomydp",description = "This test will get all taxonomy of a store",priority = 3)
    public void updateTaxonomy(String payload){
        String response2=cms.updateTaxonomy(storeid,taxid,payload).ResponseValidator.GetBodyAsText();
        String taxid01=JsonPath.read(response2,"$.taxonomy_id");
        String storeid1=JsonPath.read(response2,"$.store_id");
        Assert.assertEquals(taxid01,taxid);
        Assert.assertEquals(storeid1,storeid);

    }


}
