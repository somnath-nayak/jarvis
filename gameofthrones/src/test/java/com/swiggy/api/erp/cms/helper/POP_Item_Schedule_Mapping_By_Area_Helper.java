package com.swiggy.api.erp.cms.helper;

import com.jayway.jsonpath.JsonPath;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/*
 * This class depends on the pop_area_schedule class
 *   Please delete the schedule from uat02 for respective area-code=3 as mentioned in POP_Area_Schedule class
 *   New item scheduling can be done from this class for same area-code and inventory and display-sequence 
 *   can be updated for the same
 */

public class POP_Item_Schedule_Mapping_By_Area_Helper  {
	Initialize gameofthrones = new Initialize();
	static JsonPath jsonpath = null; 
	int newlyCreatedAreaScheduleId=0;
	POP_Area_Schedule_Helper area_schedule = new POP_Area_Schedule_Helper();	
	
	public HashMap<String, String> setHeaders1() throws JSONException {
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		JSONObject innerjson = new JSONObject();
		innerjson.put("user", "cms-tester");

		JSONObject json = new JSONObject();
		json.put("source", "cms");
		json.put("meta", innerjson);

		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("user-meta", json.toString());
		return requestheaders;
	}

	public String[] setPayload1(int item_id, int newlyCreatedAreaScheduleId,
			String date, int display_sequence, int inventory) throws JSONException {
		return new String[] { Integer.toString(item_id), ""+newlyCreatedAreaScheduleId,
				date, Integer.toString(display_sequence), Integer.toString(inventory)};
	}
	
	public String[] setPayload2(int display_sequence, int inventory) throws JSONException {
		return new String[] {Integer.toString(display_sequence), Integer.toString(inventory)};
	}
	
	public String[] setPayload3(String slot_type, int area_id, int openTime, int closeTime, String day, String menu_type) throws JSONException {
		return new String[] {slot_type, ""+area_id, ""+openTime, ""+closeTime, day, menu_type};
	}


	public Processor createItemScheduleByArea_helper(int item_id, String date, int display_sequence, int inventory,
			String slot_type, int area_id, int openTime, int closeTime, String day, String menu_type)
					throws JSONException {
		Processor p = area_schedule.createArea_schedule_helper(slot_type, area_id, openTime, closeTime, day, menu_type);
		String response = p.ResponseValidator.GetBodyAsText();
		newlyCreatedAreaScheduleId = JsonPath.read(response, "$.data.id");
			
		GameOfThronesService service2 = new GameOfThronesService("cmsbaseservice", "itemschedulemappingbyareacreate",
				gameofthrones);
		Processor p2 = new Processor(service2, 
						   setHeaders1(), 
						   setPayload1(item_id, newlyCreatedAreaScheduleId, date, display_sequence, inventory),
						   null);
		return p2;
	}
	

	
	public Processor deleteAreaScheduleHelper() throws JSONException
	{
		Processor p = area_schedule.deleteArea_schedule_helper(newlyCreatedAreaScheduleId);
		return p;
	}
		
	public Processor createItemScheduleByExistingArea_helper(int item_id, int area_schedule_id, String date, 
			int display_sequence, int inventory)
					throws JSONException {
		GameOfThronesService service1 = new GameOfThronesService("cmsbaseservice", "itemschedulemappingbyareacreate",
				gameofthrones);
		Processor p1 = new Processor(service1, 
				setHeaders1(), 
				setPayload1(item_id, area_schedule_id, date, display_sequence, inventory),
				null);
		return p1;	
	}
	
	public Processor updateItemScheduleByArea_helper(int display_sequence, int inventory, int itemScheduleIdByArea) throws JSONException {
		
		GameOfThronesService service1 = new GameOfThronesService("cmsbaseservice", "itemschedulemappingbyareaupdate",
				gameofthrones);
		Processor p1 = new Processor(service1, 
									 setHeaders1(), 
									 setPayload2(display_sequence, inventory),
									 new String[] {""+ itemScheduleIdByArea});
		return p1;
	}


	public Processor getItemScheduleByArea_helper(int itemScheduleIdByArea) throws JSONException {
		GameOfThronesService service1 = new GameOfThronesService("cmsbaseservice", "itemschedulemappingbyareaget", gameofthrones);
		Processor p1 = new Processor(service1, 
									setHeaders1(), 
									null, 	
									new String[] {""+ itemScheduleIdByArea});
		return p1;
	}

	public Processor deleteItemScheduleByArea_helper(int itemScheduleIdByArea) throws JSONException {
		GameOfThronesService service1 = new GameOfThronesService("cmsbaseservice", "itemschedulemappingbyareadelete", gameofthrones);
		Processor p1 = new Processor(service1, 
									 setHeaders1(), 
									 null, 
									 new String[] {""+ itemScheduleIdByArea});
		return p1;
	}
	
	
	public String[] setPayload2(int item_id, int newlyCreatedAreaScheduleId,
			String date, int display_sequence, int inventory,String cusine ,String dish_type,String priority) throws JSONException {
		return new String[] { Integer.toString(item_id), ""+newlyCreatedAreaScheduleId,
				date, Integer.toString(display_sequence), Integer.toString(inventory),cusine,dish_type,priority};
	}
	
	public Processor createItemScheduleByExistingArea_helper_PopV2(int item_id, int area_schedule_id, String date, 
			int display_sequence, int inventory,String cusine ,String dish_type,String priority)
					throws JSONException {
		GameOfThronesService service1 = new GameOfThronesService("cmsbaseservice", "itemschedulemappingbyareacreate",
				gameofthrones);
		Processor p1 = new Processor(service1, 
				setHeaders1(), 
				setPayload2(item_id, area_schedule_id, date, display_sequence, inventory,cusine,dish_type,priority),
				null);
		return p1;	
	}

	
}
