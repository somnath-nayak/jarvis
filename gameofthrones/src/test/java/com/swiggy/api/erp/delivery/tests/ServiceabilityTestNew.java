package com.swiggy.api.erp.delivery.tests;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.noggit.JSONUtil;
import org.apache.solr.common.SolrDocumentList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.ibm.icu.math.BigDecimal;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.erp.delivery.constants.Serviceability_Constant;
import com.swiggy.api.erp.delivery.helper.DeliveryDataBaseUtils;
import com.swiggy.api.erp.delivery.helper.DeliveryDataHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryHelperMethods;
import com.swiggy.api.erp.delivery.helper.Serviceability_Helper;
import com.swiggy.api.erp.delivery.helper.ServiceablilityHelper;
import com.swiggy.api.erp.delivery.helper.SolrHelper;

import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.RedisHelper;

@SuppressWarnings("serial")
public class ServiceabilityTestNew {

    Integer area_id;
    Integer city_id;
    String restLatLon;
    Double restLat;
    Integer zone_id, zoneMaxSla;
    Double restLon;
    Double defaultCustomerLat;
    Double defaultCustomerLon;
    String solrUrl;
    String restaurantId;
    Double actualLastMile;
    Double restMaxBannerFactor;
    Double defaultLat;
    Double defaultLon;
    SolrHelper solrHelper = new SolrHelper();
    DeliveryHelperMethods deliveryHelperMethods=new DeliveryHelperMethods();
    DeliveryDataHelper deliveryDataHelper=new DeliveryDataHelper();
    Serviceability_Helper serviceablility_Helper=new Serviceability_Helper();
    ServiceablilityHelper servHelper = new ServiceablilityHelper();
    DeliveryDataBaseUtils dbUtils = new DeliveryDataBaseUtils();
    private static final String core="core_listing";
    RedisHelper redisHelper =new RedisHelper();
    String lDrestaurantId;
    String lDrestLatLon;
    Double lDrestLon;
    Double lDrestLat;
    List<String> LdCoordinates;
    Double LdDistance=6.5;
    Integer lDPolygonId, ldAreaId;
    Double ldLastMile, ldRestaurantLastMile, ldAreaLastMile, defaultDistanceBtwnRestaurantAndCustomer;
    String longDistanceId;
    String defaultCustomerZone;
    String rainMode;
    Double ldRestAndAreaMinLastMile, lastMileAreaCap, lastMileRestaurantCap;
    Boolean tempLmEnabled;
    String row_not_found_key = "[rowNotFound]";                    //key => DeliveryDataBaseUtils returnRowNotFoundMap() map..
    String rain_mode;
    String RAIN_MODE_HEAVY = "1", RAIN_MODE_LIGHT = "2", NO_RAIN = "3", RAIN_MODE_DEGRADATION = "4", RAIN_MODE_EXTREME = "5";
    String DEGRADATION_MODE_LIGHT = "1", DEGRADATION_MODE_HEAVY = "2", DEGRADATION_MODE_EXTREME = "3";


    //customer segmentation (priority/non-priority)..
    //pop..
    //blackzone..
    //restaurant (enabled/disabled)..
    //dominos (banner & rains)


    @BeforeTest
    public void doConfig()
    {
        area_id=10;
        city_id=Integer.parseInt(getValueFromDBusingQeury("select city_id from area where id ="+area_id, "city_id"));
        zone_id=Integer.parseInt(getValueFromDBusingQeury("select zone_id from area where id =" + area_id, "zone_id"));
        solrUrl = deliveryHelperMethods.getSolrUrl();
        String solrquery="enabled:true AND area_id:"+area_id+" AND city_id:"+city_id;
        SolrDocumentList solrDocuments = deliveryDataHelper.getSolrOutput(solrUrl,core,solrquery);
        JSONObject restObject = null;
        tempLmEnabled = Boolean.parseBoolean(serviceablility_Helper.getValueFromDb("temp_lm_enabled", "zone", "id", String.valueOf(zone_id)));
        SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update("update zone set temp_lm_enabled=0 where id="+zone_id);
        zoneMaxSla = Integer.parseInt(serviceablility_Helper.getValueFromDb("max_delivery_time", "zone", "id", String.valueOf(zone_id)));
        SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update("update zone set max_delivery_time=200 where id="+zone_id);
        rainMode = serviceablility_Helper.getValueFromDb("rain_mode_type", "zone", "id", String.valueOf(zone_id));
        SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update("update zone set rain_mode_type=3 where id="+zone_id);
        serviceablility_Helper.clearCache("zone", String.valueOf(zone_id));
        Double cityRadius=Double.parseDouble(getValueFromDBusingQeury("select delivery_radius from city where id="+city_id, "delivery_radius"));
        SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update("update area set last_mile_cap="+(cityRadius-2)+" where id="+area_id);
        serviceablility_Helper.clearCache("area", String.valueOf(area_id));
        try {
            restObject = new JSONArray(JSONUtil.toJSON(solrDocuments)).getJSONObject(1);
            restLatLon=restObject.get("place").toString();
            restLat=Double.parseDouble(restLatLon.split(",")[0]);
            restLon=Double.parseDouble(restLatLon.split(",")[1]);
            restaurantId=restObject.get("id").toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        servHelper.makeServiceable(restaurantId);
        lastMileAreaCap=Double.parseDouble(getValueFromDBusingQeury("select last_mile_cap from area where id="+area_id, "last_mile_cap"));
        lastMileRestaurantCap=Double.parseDouble(getValueFromDBusingQeury("select max_second_mile from restaurant where id="+restaurantId, "max_second_mile"));
        actualLastMile=Math.min(lastMileAreaCap,lastMileRestaurantCap);
        //Double[] latlonarray=deliveryDataHelper.getLatLng(restLat,restLon,90.0,.5);
        Map<String,String> latlonMap=deliveryDataHelper.latLonNotInGivenTagsButInZone(restLat,restLon,.5,"LONG_DISTANCE");
        defaultCustomerLat=Double.parseDouble(latlonMap.get("lat"));
        defaultCustomerLon=Double.parseDouble(latlonMap.get("lon"));
        defaultCustomerZone=latlonMap.get("zone_id");
        restMaxBannerFactor=Double.parseDouble(getValueFromDBusingQeury("select max_banner_factor from restaurant where id ="+restaurantId, "max_banner_factor"));
        String key = "BL_BANNER_FACTOR_ZONE_"+zone_id;
        if(!(redisHelper.checkKeyExists("deliveryredis",1,key)))
        {
            redisHelper.setValue("deliveryredis",1,key,".5");
        }
        defaultDistanceBtwnRestaurantAndCustomer = Double.parseDouble(String.valueOf(deliveryDataHelper.getDistanceBtwnTwoLatLongs(restLat, restLon, defaultCustomerLat, defaultCustomerLon)))/1000;
        System.out.println("********************************* BEFORE TEST *************************************************");
    }

    @AfterTest
    public void afterTest()
    {
        System.out.println("********************************* AFTER TEST *************************************************");
        serviceablility_Helper.setValueInDb("zone", "rain_mode_type", rainMode, "id", String.valueOf(zone_id));
        if(tempLmEnabled)
            serviceablility_Helper.setValueInDb("zone", "temp_lm_enabled", "1", "id", String.valueOf(zone_id));
        else
            serviceablility_Helper.setValueInDb("zone", "temp_lm_enabled", "0", "id", String.valueOf(zone_id));
        serviceablility_Helper.setValueInDb("zone", "max_delivery_time", String.valueOf(zoneMaxSla), "id", String.valueOf(zone_id));
        serviceablility_Helper.setValueInDb("area", "last_mile_cap", String.valueOf(lastMileAreaCap), "id", String.valueOf(area_id));
        serviceablility_Helper.setValueInDb("restaurant", "max_second_mile", String.valueOf(lastMileRestaurantCap), "id", restaurantId);
    }

    @BeforeGroups(groups = {"long_distance"})
    public void beforeLD()
    {
        //servHelper.makeServiceable(lDrestaurantId);
        //Method will return a coordinate inside a ld polygon
        LdCoordinates=findPointInsideLDPolygonLatLon();
        ldAreaId = Integer.parseInt(getValueFromDBusingQeury("select area_code from restaurant where id="+lDrestaurantId, "area_code"));
        ldAreaLastMile = Double.parseDouble(getValueFromDBusingQeury("select last_mile_cap from area where id="+area_id, "last_mile_cap"));
        ldLastMile = Double.parseDouble(getValueFromDBusingQeury("select max_last_mile from long_distance where polygon_id='" + lDPolygonId + "'", "max_last_mile"));
        longDistanceId = getValueFromDBusingQeury("select id from long_distance where polygon_id='" + lDPolygonId + "'", "id");
        ldRestaurantLastMile = Double.parseDouble(getValueFromDBusingQeury("select max_second_mile from restaurant where id=" + lDrestaurantId, "max_second_mile"));
        ldRestAndAreaMinLastMile = Math.min(lastMileAreaCap, Double.parseDouble(getValueFromDBusingQeury("select max_second_mile from restaurant where id=" + lDrestaurantId, "max_second_mile")));
        System.out.println("********************************* BEFORE GROUP *************************************************");
    }

    @AfterGroups(groups = {"long_distance"})
    public void afterLD()
    {
        System.out.println("********************************* AFTER GROUP *************************************************");
        serviceablility_Helper.setValueInDb("area", "last_mile_cap", String.valueOf(ldAreaLastMile), "id", String.valueOf(ldAreaId));
        serviceablility_Helper.setValueInDb("long_distance", "max_last_mile", String.valueOf(ldLastMile), "id", longDistanceId);
        serviceablility_Helper.setValueInDb("restaurant", "max_second_mile", String.valueOf(ldRestaurantLastMile), "id", lDrestaurantId);
    }


    public void setRainModeTypeInZone(String rainModeType, String zoneId)
    {
        serviceablility_Helper.setValueInDb("zone", "rain_mode_type", rainModeType, "id", zoneId);
    }

    public void updateZoneMaxSla(String maxSla, String zoneId)
    {
        SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update("update zone set max_delivery_time='" + maxSla + "' where id='" + zoneId + "'");
        serviceablility_Helper.clearCache("zone", String.valueOf(zone_id));
    }


    @Test(description = "SLTC_01 :- Verify the restaurant is serviceable in Listing when the last_mile_travel is less than the max last mile (minimum of area and restaurant's max last mile)")
    public void a1lessThanLastMileCheck()
    {
        Double[] latlonarray=deliveryDataHelper.getLatLng(restLat,restLon,90.0,(actualLastMile/1.3)-2);
        String restZoneId = getCustomerZoneFromLatLon(restLat,restLon);

        String custZoneId = getCustomerZoneFromLatLon(defaultCustomerLat, defaultCustomerLon);
        Map<String, Integer> rainModeMap = setNoRainsAtRestaurantAndCustomer(restZoneId, custZoneId);


        Map<String,String> mappedresponse = serviceablility_Helper.retryCerebroListingUsingVarargs(10, 1000,
                new String[]{defaultCustomerLat.toString(), defaultCustomerLon.toString(), city_id.toString(), restaurantId},
                returnMap("rain_mode", "0"), returnMap("serviceability", "2"), returnMap("long_distance", "0"));

        if(rainModeMap.get("restaurantRainMode") != 0)
            setRainModeTypeInZone(String.valueOf(rainModeMap.get("restaurantRainMode")), restZoneId);
        if(custZoneId != null && rainModeMap.get("customerRainMode") != 0)
            setRainModeTypeInZone(String.valueOf(rainModeMap.get("customerRainMode")), custZoneId);
        Assert.assertEquals(mappedresponse.get("serviceability"),"2", "serviceability");
    }

    @Test(description = "SLTC_02 :- Verify the restaurant is not serviceable in Listing when the last_mile_travel is greater than the max last mile (minimum of area and restaurant's max last mile)")
    public void greaterThanLastMileCheck()
    {
        String restZoneId = getCustomerZoneFromLatLon(restLat,restLon);
        String custZoneId = getCustomerZoneFromLatLon(defaultCustomerLat, defaultCustomerLon);
        Map<String, Integer> rainModeMap = setNoRainsAtRestaurantAndCustomer(restZoneId, custZoneId);
        serviceablility_Helper.setNoRainsAtZone(String.valueOf(rainModeMap.get("restaurantRainMode")));
        serviceablility_Helper.setNoRainsAtZone(String.valueOf(rainModeMap.get("customerRainMode")));
        String areaLastMile = dbUtils.getMaxLastMileForArea(area_id.toString());
        dbUtils.setMaxLastMileForArea(area_id.toString(), "0");
        Map<String,String> mappedresponse = serviceablility_Helper.retryCerebroListingUsingVarargs(10, 1000,
                new String[]{defaultCustomerLat.toString(), defaultCustomerLon.toString(), city_id.toString(), restaurantId},
                returnMap("rain_mode", "0"), returnMap("serviceability", "0"), returnMap("non_serviceable_reason", "0"), returnMap("long_distance", "0"));

        dbUtils.setMaxLastMileForArea(area_id.toString(), areaLastMile);
        if(rainModeMap.get("restaurantRainMode") != 0)
            setRainModeTypeInZone(String.valueOf(rainModeMap.get("restaurantRainMode")), restZoneId);
        if(custZoneId != null && rainModeMap.get("customerRainMode") != 0)
            setRainModeTypeInZone(String.valueOf(rainModeMap.get("customerRainMode")), custZoneId);
        Assert.assertEquals(mappedresponse.get("serviceability"),"0", "serviceability");
        Assert.assertEquals(mappedresponse.get("non_serviceable_reason"),"0", "non_serviceable_reason");
    }

    @Test(description = "SLTC_03 :- Verify the restaurant is serviceable in Listing when the last_mile_travel is equal to the max last mile (minimum of area and restaurant's max last mile)")
    public void equalLastMileCheck()
    {
        Double distance  = Double.parseDouble(String.valueOf(deliveryDataHelper.getDistanceBtwnTwoLatLongs(restLat, restLon, defaultCustomerLat, defaultCustomerLon)))/1000;
        String restZoneId = getCustomerZoneFromLatLon(restLat,restLon);
        String custZoneId = getCustomerZoneFromLatLon(defaultCustomerLat, defaultCustomerLon);
        Map<String, Integer> rainModeMap = setNoRainsAtRestaurantAndCustomer(restZoneId, custZoneId);
        String areaLastMile = dbUtils.getMaxLastMileForArea(area_id.toString());
        String restLastMile = dbUtils.getMaxLastMileForRestaurant(restaurantId);
        dbUtils.setMaxLastMileForAreaAndClearCache(area_id.toString(), distance.toString());
        dbUtils.setMaxLastMileForRestaurantAndClearCache(restaurantId, distance.toString());
        Logger.getLogger(ServiceabilityTestNew.class.getName()).info(dbUtils.getMaxLastMileForArea(area_id.toString()));
        Map<String,String> mappedresponse = serviceablility_Helper.retryCerebroListingUsingVarargs(10, 1000,
                new String[]{defaultCustomerLat.toString(), defaultCustomerLon.toString(), city_id.toString(), restaurantId},
                returnMap("rain_mode", "0"), returnMap("serviceability", "2"), returnMap("long_distance", "0"));

        dbUtils.setMaxLastMileForAreaAndClearCache(area_id.toString(), areaLastMile);
        dbUtils.setMaxLastMileForRestaurantAndClearCache(restaurantId, restLastMile);
        if(rainModeMap.get("restaurantRainMode") != 0)
            setRainModeTypeInZone(String.valueOf(rainModeMap.get("restaurantRainMode")), restZoneId);
        if(custZoneId != null && rainModeMap.get("customerRainMode") != 0)
            setRainModeTypeInZone(String.valueOf(rainModeMap.get("customerRainMode")), custZoneId);
        Assert.assertEquals(mappedresponse.get("serviceability"),"2", "serviceability");
    }


    @Test(description = "SLTC_04 :- Verify the restaurant is serviceable in Listing when delivery time is less than the max_delivery_time(Max SLA) config")
    public void ltSlaCheck()
    {
        Double[] latlonarray=deliveryDataHelper.getLatLng(restLat,restLon,90.0,actualLastMile/1.3-1);
        String restZoneId = getCustomerZoneFromLatLon(restLat,restLon);
        String custZoneId = getCustomerZoneFromLatLon(latlonarray[0], latlonarray[1]);
        Map<String, Integer> rainModeMap = setNoRainsAtRestaurantAndCustomer(restZoneId, custZoneId);

        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {defaultCustomerLat.toString(), defaultCustomerLon.toString(), city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode","0");
            put("serviceability","2");
            put("long_distance","0");
        }});
        if(rainModeMap.get("restaurantRainMode") != 0)
            setRainModeTypeInZone(String.valueOf(rainModeMap.get("restaurantRainMode")), restZoneId);
        if(custZoneId != null && rainModeMap.get("customerRainMode") != 0)
            setRainModeTypeInZone(String.valueOf(rainModeMap.get("customerRainMode")), custZoneId);
        Assert.assertEquals(mappedResponse.get("serviceability"),"2", "serviceability");
    }

    @Test(description = "SLTC_05 :- Verify the restaurant is not serviceable in Listing when delivery time is greater than the max_delivery_time(Max SLA) config")
    public void gtSlaCheck()
    {
        //Double[] latlonarray=deliveryDataHelper.getLatLng(restLat,restLon,90.0,actualLastMile/1.3-1);
        String restZoneId = getCustomerZoneFromLatLon(restLat,restLon);
        //String custZoneId = getCustomerZoneFromLatLon(latlonarray[0], latlonarray[1]);
        String custZoneId = getCustomerZoneFromLatLon(defaultCustomerLat, defaultCustomerLon);
        Map<String, Integer> rainModeMap = setNoRainsAtRestaurantAndCustomer(restZoneId, custZoneId);
        String maxSla = serviceablility_Helper.getValueFromDb("max_delivery_time", "zone", "id", String.valueOf(zone_id));
        updateZoneMaxSla("10", String.valueOf(zone_id));

        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {defaultCustomerLat.toString(), defaultCustomerLon.toString(), city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode","0");
            put("serviceability","0");
            put("non_serviceable_reason","1");
            put("long_distance","0");
        }});
        if(rainModeMap.get("restaurantRainMode") != 0)
            setRainModeTypeInZone(String.valueOf(rainModeMap.get("restaurantRainMode")), restZoneId);
        if(custZoneId != null && rainModeMap.get("customerRainMode") != 0)
            setRainModeTypeInZone(String.valueOf(rainModeMap.get("customerRainMode")), custZoneId);
        updateZoneMaxSla(maxSla, String.valueOf(zone_id));
        Assert.assertEquals(mappedResponse.get("serviceability"),"0", "serviceability");
        Assert.assertEquals(mappedResponse.get("non_serviceable_reason"),"1", "non serviceable reason");
    }


    @Test(description = "SLTC_06 :- Verify the restaurant is serviceable in Listing when delivery time is equal to the max_delivery_time(Max SLA) config")
    public void eqSlaCheck()
    {
       // Double[] latlonarray=deliveryDataHelper.getLatLng(restLat,restLon,90.0,actualLastMile/1.3-1);
        String restZoneId = getCustomerZoneFromLatLon(restLat,restLon);
        //String custZoneId = getCustomerZoneFromLatLon(latlonarray[0], latlonarray[1]);
        String custZoneId = getCustomerZoneFromLatLon(defaultCustomerLat, defaultCustomerLon);
        Map<String, Integer> rainModeMap = setNoRainsAtRestaurantAndCustomer(restZoneId, custZoneId);
        String maxSla = serviceablility_Helper.getValueFromDb("max_delivery_time", "zone", "id", String.valueOf(zone_id));

        String  response = serviceablility_Helper.listing(new String[]{defaultCustomerLat.toString(), defaultCustomerLon.toString(),city_id.toString(), restaurantId}).ResponseValidator.GetBodyAsText();
        Map<String, String> mappedresponse=getServiceabiltyResponseMap(response);
        String slaInResponse=mappedresponse.get("delivery_time");
        updateZoneMaxSla(slaInResponse, String.valueOf(zone_id));

        Map<String,String> mappedresponse1 = serviceablility_Helper.retryCerebroListingUsingVarargs(10, 1000,
                new String[]{defaultCustomerLat.toString(), defaultCustomerLon.toString(), city_id.toString(), restaurantId},
                returnMap("rain_mode", "0"), returnMap("serviceability", "2"), returnMap("long_distance", "0"));
        updateZoneMaxSla(maxSla, String.valueOf(zone_id));

        if(rainModeMap.get("restaurantRainMode") != 0)
            setRainModeTypeInZone(String.valueOf(rainModeMap.get("restaurantRainMode")), restZoneId);
        if(custZoneId != null && rainModeMap.get("customerRainMode") != 0)
            setRainModeTypeInZone(String.valueOf(rainModeMap.get("customerRainMode")), custZoneId);
        Assert.assertEquals(mappedresponse1.get("serviceability"),"2", "serviceability");
    }


    @Test(description = "SLTC_92 :- Verify the restaurant is serviceable in Listing when temp last mile is enabled and is less than the temp_last_mile at zone level")
    public void tempLastMileLtCheck()
    {
        Double actualtempLastMile=Double.parseDouble(serviceablility_Helper.getValueFromDb("temp_last_mile", "zone", "id", String.valueOf(zone_id)));
        serviceablility_Helper.setValueInDb("zone", "temp_lm_enabled", "1", "id", String.valueOf(zone_id));

        Double[] latlonarray=deliveryDataHelper.getLatLng(restLat,restLon,90.0,actualtempLastMile/1.3-1);
        String restZoneId = getCustomerZoneFromLatLon(restLat,restLon);
        String custZoneId = getCustomerZoneFromLatLon(latlonarray[0], latlonarray[1]);
        Map<String, Integer> rainModeMap = setNoRainsAtRestaurantAndCustomer(restZoneId, custZoneId);
        Map<String,String> mappedresponse = serviceablility_Helper.retryCerebroListingUsingVarargs(10, 1000,
                new String[]{latlonarray[0].toString(), latlonarray[1].toString(), city_id.toString(), restaurantId},
                returnMap("rain_mode", "0"), returnMap("serviceability", "2"), returnMap("long_distance", "0"));

        serviceablility_Helper.setValueInDb("zone", "temp_lm_enabled", "0", "id", String.valueOf(zone_id));
        if(rainModeMap.get("restaurantRainMode") != 0)
            setRainModeTypeInZone(String.valueOf(rainModeMap.get("restaurantRainMode")), restZoneId);
        if(custZoneId != null && rainModeMap.get("customerRainMode") != 0)
            setRainModeTypeInZone(String.valueOf(rainModeMap.get("customerRainMode")), custZoneId);
        Assert.assertEquals(mappedresponse.get("serviceability"),"2", "serviceability");
    }

    @Test(description = "SLTC_93 :- Verify the restaurant is not serviceable in Listing when temp last mile is enabled and is greater than the temp_last_mile at zone level")
    public void tempLastMileGtCheck()
    {
        serviceablility_Helper.setValueInDb("zone", "temp_lm_enabled", "1", "id", String.valueOf(zone_id));
        Double actualtempLastMile=Double.parseDouble(serviceablility_Helper.getValueFromDb("temp_last_mile", "zone", "id", String.valueOf(zone_id)));
        Double[] latlonarray=deliveryDataHelper.getLatLng(restLat,restLon,90.0,actualtempLastMile/1.3+1);

        String restZoneId = getCustomerZoneFromLatLon(restLat,restLon);
        String custZoneId = getCustomerZoneFromLatLon(latlonarray[0], latlonarray[1]);
        Map<String, Integer> rainModeMap = setNoRainsAtRestaurantAndCustomer(restZoneId, custZoneId);
        Map<String,String> mappedresponse = serviceablility_Helper.retryCerebroListingUsingVarargs(10, 1000,
                new String[]{latlonarray[0].toString(), latlonarray[1].toString(), city_id.toString(), restaurantId},
                returnMap("rain_mode", "0"), returnMap("serviceability", "1"), returnMap("non_serviceable_reason", "4"), returnMap("long_distance", "0"));

        serviceablility_Helper.setValueInDb("zone", "temp_lm_enabled", "0", "id", String.valueOf(zone_id));
        if(rainModeMap.get("restaurantRainMode") != 0)
            setRainModeTypeInZone(String.valueOf(rainModeMap.get("restaurantRainMode")), restZoneId);
        if(custZoneId != null && rainModeMap.get("customerRainMode") != 0)
            setRainModeTypeInZone(String.valueOf(rainModeMap.get("customerRainMode")), custZoneId);

        Assert.assertEquals(mappedresponse.get("serviceability"),"1", "serviceability");
        Assert.assertEquals(mappedresponse.get("non_serviceable_reason"),"4", "non serviceable reason");
    }


    @Test(description = "SLTC_94 :- Verify the restaurant is serviceable in Listing when temp last mile is enabled and is equal to the temp_last_mile at zone level")
    public void tempLastMileEqCheck()
    {
        serviceablility_Helper.setValueInDb("zone", "temp_lm_enabled", "1", "id", String.valueOf(zone_id));
        Double actualtempLastMile=Double.parseDouble(serviceablility_Helper.getValueFromDb("temp_last_mile", "zone", "id", String.valueOf(zone_id)));
        Double[] latlonarray=deliveryDataHelper.getLatLng(restLat,restLon,90.0,actualtempLastMile/1.3);

        String restZoneId = getCustomerZoneFromLatLon(restLat,restLon);
        String custZoneId = getCustomerZoneFromLatLon(latlonarray[0], latlonarray[1]);
        Map<String, Integer> rainModeMap = setNoRainsAtRestaurantAndCustomer(restZoneId, custZoneId);
        Map<String,String> mappedresponse = serviceablility_Helper.retryCerebroListingUsingVarargs(10, 1000,
                new String[]{latlonarray[0].toString(), latlonarray[1].toString(), city_id.toString(), restaurantId},
                returnMap("rain_mode", "0"), returnMap("serviceability", "2"), returnMap("long_distance", "0"));

        serviceablility_Helper.setValueInDb("zone", "temp_lm_enabled", "0", "id", String.valueOf(zone_id));
        if(rainModeMap.get("restaurantRainMode") != 0)
            setRainModeTypeInZone(String.valueOf(rainModeMap.get("restaurantRainMode")), restZoneId);
        if(custZoneId != null && rainModeMap.get("customerRainMode") != 0)
            setRainModeTypeInZone(String.valueOf(rainModeMap.get("customerRainMode")), custZoneId);
        Assert.assertEquals(mappedresponse.get("serviceability"),"2", "serviceability");
    }

    @Test(description = "SLTC_98 :- Verify the restaurant is serviceable in Listing when the current banner factor is less than the stop banner factor")
    public void ltBanner()
    {
        String currentBF = redisHelper.getValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_" + zone_id).toString();
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+zone_id, (new Double(restMaxBannerFactor-.5)).toString());

        Map<String,String> mappedresponse = serviceablility_Helper.retryCerebroListingUsingVarargs(10, 1000,
                new String[]{defaultCustomerLat.toString(),defaultCustomerLon.toString(), city_id.toString(), restaurantId},
                returnMap("rain_mode", "0"), returnMap("serviceability", "2"), returnMap("long_distance", "0"));

        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+zone_id, (new Double(currentBF)).toString());
        Assert.assertEquals(mappedresponse.get("serviceability"),"2", "serviceability");
    }


    @Test(description = "SLTC_99 :- Verify the restaurant is not serviceable in Listing when the current banner factor is greater than the stop banner factor")
    public void gtBanner()
    {
        String currentBF = redisHelper.getValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_" + zone_id).toString();
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+zone_id,(new Double(restMaxBannerFactor+.5)).toString());

        Map<String,String> mappedresponse = serviceablility_Helper.retryCerebroListingUsingVarargs(10, 1000,
                new String[]{defaultCustomerLat.toString(),defaultCustomerLon.toString(), city_id.toString(), restaurantId},
                returnMap("rain_mode", "0"), returnMap("serviceability", "1"), returnMap("non_serviceable_reason", "7"), returnMap("long_distance", "0"));

        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+zone_id,(new Double(currentBF)).toString());
        Assert.assertEquals(mappedresponse.get("serviceability"),"1", "serviceability");
        Assert.assertEquals(mappedresponse.get("non_serviceable_reason"),"7", "non serviceable reason");
    }

    @Test(description = "SLTC_100 :- Verify the restaurant is serviceable in Listing when the current banner factor is equal to the stop banner factor")
    public void eqBanner()
    {
        String currentBF = redisHelper.getValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_" + zone_id).toString();
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+zone_id,(new Double(restMaxBannerFactor)).toString());

        Map<String,String> mappedresponse = serviceablility_Helper.retryCerebroListingUsingVarargs(10, 1000,
                new String[]{defaultCustomerLat.toString(),defaultCustomerLon.toString(), city_id.toString(), restaurantId},
                returnMap("rain_mode", "0"), returnMap("serviceability", "2"), returnMap("long_distance", "0"));

        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+zone_id,(new Double(currentBF)).toString());
        Assert.assertEquals(mappedresponse.get("serviceability"),"2", "serviceability");
    }


    @Test(description = "SLTC_102 :- Verify restaurant is not serviceable if zone is closed")
    public void zoneCloseServiceabilityTest()
    {
        Integer close_time = Integer.parseInt(serviceablility_Helper.getValueFromDb("close_time", "zone", "id", String.valueOf(zone_id)));
        Integer currentTimeMinutes = deliveryDataHelper.getcurrentDateTimeInMinutes();
        serviceablility_Helper.setValueInDb("zone", "close_time", String.valueOf(currentTimeMinutes-10), "id", String.valueOf(zone_id));

        Map<String,String> mappedresponse = serviceablility_Helper.retryCerebroListingUsingVarargs(10, 1000,
                new String[]{defaultCustomerLat.toString(),defaultCustomerLon.toString(), city_id.toString(), restaurantId},
                returnMap("rain_mode", "0"), returnMap("serviceability", "1"), returnMap("non_serviceable_reason", "5"), returnMap("long_distance", "0"));

        serviceablility_Helper.setValueInDb("zone", "close_time", String.valueOf(close_time), "id", String.valueOf(zone_id));
        Assert.assertEquals(mappedresponse.get("serviceability"),"1", "serviceability");
        Assert.assertEquals(mappedresponse.get("non_serviceable_reason"),"5", "non serviceable reason");
    }

    @Test(description = "SLTC_101 :- Verify restaurant is serviceable if zone is open")
    public void zoneOpenServiceabilityTest()
    {
        Integer close_time = Integer.parseInt(serviceablility_Helper.getValueFromDb("close_time", "zone", "id", String.valueOf(zone_id)));
        Integer currentTimeMinutes = deliveryDataHelper.getcurrentDateTimeInMinutes();
        serviceablility_Helper.setValueInDb("zone", "close_time", String.valueOf(currentTimeMinutes+10), "id", String.valueOf(zone_id));

        Map<String,String> mappedresponse = serviceablility_Helper.retryCerebroListingUsingVarargs(10, 1000,
                new String[]{defaultCustomerLat.toString(),defaultCustomerLon.toString(), city_id.toString(), restaurantId},
                returnMap("rain_mode", "0"), returnMap("serviceability", "2"), returnMap("long_distance", "0"));

        serviceablility_Helper.setValueInDb("zone", "close_time", String.valueOf(close_time), "id", String.valueOf(zone_id));
        Assert.assertEquals(mappedresponse.get("serviceability"),"2", "serviceability");
    }


    @Test(description = "SLTC_103 :- Verify restaurant is serviceable at closing time of zone")
    public void zoneOpenBoundaryServiceabilityTest()
    {
        Integer close_time=Integer.parseInt(serviceablility_Helper.getValueFromDb("close_time", "zone", "id", String.valueOf(zone_id)));;
        Integer currentTimeMinutes=deliveryDataHelper.getcurrentDateTimeInMinutes();
        serviceablility_Helper.setValueInDb("zone", "close_time", String.valueOf(currentTimeMinutes), "id", String.valueOf(zone_id));

        Map<String,String> mappedresponse = serviceablility_Helper.retryCerebroListingUsingVarargs(10, 1000,
                new String[]{defaultCustomerLat.toString(),defaultCustomerLon.toString(), city_id.toString(), restaurantId},
                returnMap("rain_mode", "0"), returnMap("serviceability", "2"), returnMap("long_distance", "0"));

        serviceablility_Helper.setValueInDb("zone", "close_time", String.valueOf(close_time), "id", String.valueOf(zone_id));
        Assert.assertEquals(mappedresponse.get("serviceability"),"2", "serviceability");
    }


    @Test(description = "SLTC_07 :- Verify restaurant is serviceable when the distance from customer to restaurant is less than the max last mile of 'rain_mode:1' defined at zone_rain_params of restaurant zone")
    public void rainsAtRestaurantMode1_LtLastMile()
    {
        rain_mode = "1";
        Integer restaurantRainMode = getRainMode(String.valueOf(zone_id));
        setRainModeAndCause(restaurantRainMode, Integer.parseInt(rain_mode), zone_id);
        //Double zoneRainParamsLastMile = Double.parseDouble(getValueFromDBusingQeury("select last_mile from zone_rain_params where zone_id=" + zone_id + " and rain_mode=" + rain_mode, "last_mile"));
        //Double[] latlonarray = deliveryDataHelper.getLatLng(restLat, restLon, 90.0, (BigDecimal.valueOf(zoneRainParamsLastMile/1.3).divide(BigDecimal.valueOf(2))).doubleValue());

        String custZone = getCustomerZoneFromLatLon(defaultCustomerLat,defaultCustomerLon);
        Integer customerRainMode = getRainMode(String.valueOf(custZone));
        if(zone_id != Integer.parseInt(custZone) && customerRainMode != 3)
            serviceablility_Helper.setNoRainsAtZone(String.valueOf(custZone));
        Map<String, Object> restaurantZoneRainParams = DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("1", zone_id.toString());
        if(restaurantZoneRainParams.keySet().toString().equals(row_not_found_key))
            Assert.fail("add entry in zone rain params of rain mode : " + rain_mode + " and zone id : " + zone_id.toString());
        String restZoneMaxSla = restaurantZoneRainParams.get("zone_max_sla").toString();
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "200", "1", zone_id.toString());
        Double ZRMMaxLastMile = Double.parseDouble(restaurantZoneRainParams.get("last_mile").toString());
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", "4", rain_mode, zone_id.toString());

        Map<String,String> mappedresponse = serviceablility_Helper.retryCerebroListingUsingVarargs(10, 1000,
                new String[]{defaultCustomerLat.toString(),defaultCustomerLon.toString(), city_id.toString(), restaurantId},
                returnMap("rain_mode", "1"), returnMap("serviceability", "2"), returnMap("long_distance", "0"));

        String degradationMode = getResponseDegradationMode(mappedresponse);
        String degradationCause = getResponseDegradationCause(mappedresponse);
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", ZRMMaxLastMile.toString(), rain_mode, zone_id.toString());
        serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(restaurantRainMode), "id", String.valueOf(zone_id));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", restZoneMaxSla, rain_mode, zone_id.toString());
        if(zone_id != Integer.parseInt(custZone) && customerRainMode != 3)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(customerRainMode), "id", String.valueOf(custZone));

        Assert.assertEquals(mappedresponse.get("rain_mode"), "1", "rain mode");
        Assert.assertEquals(degradationMode, "2", "degradation mode");
        Assert.assertEquals(degradationCause, "rain", "degradation cause");
        Assert.assertEquals(mappedresponse.get("serviceability"), "2", "serviceability");
        if(zone_id != Integer.parseInt(custZone) && custZone != null)
            Assert.assertEquals(mappedresponse.get("customer_zone_raining"), "false", "customer zone raining");
        else if(zone_id == Integer.parseInt(custZone))
            Assert.assertEquals(mappedresponse.get("customer_zone_raining"), "true", "customer zone raining");
    }


    @Test(description = "SLTC_08 :- Verify restaurant is not serviceable when the distance from customer to restaurant is greater than the max last mile of 'rain_mode:1' defined at zone_rain_params of restaurant zone")
    public void rainsAtRestaurantMode1_GtLastMile()
    {
        rain_mode = "1";
        Integer restaurantRainMode = getRainMode(String.valueOf(zone_id));
        setRainModeAndCause(restaurantRainMode, Integer.parseInt(rain_mode), zone_id);
        Double zoneRainParamsLastMile = Double.parseDouble(getValueFromDBusingQeury("select last_mile from zone_rain_params where zone_id=" + zone_id + " and rain_mode=" + rain_mode, "last_mile"));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", "0.0", rain_mode, zone_id.toString());
        //Double[] latlonarray = deliveryDataHelper.getLatLng(restLat, restLon, 90.0, (BigDecimal.valueOf(zoneRainParamsLastMile/1.3).add(BigDecimal.valueOf(1))).doubleValue());
        Double areaLastMile = Double.parseDouble(DeliveryDataBaseUtils.getMaxLastMileForArea(area_id.toString()));
        Double restaurantLastMile = Double.parseDouble(DeliveryDataBaseUtils.getMaxLastMileForRestaurant(restaurantId));
        DeliveryDataBaseUtils.setMaxLastMileForArea(area_id.toString(), "4.0");
        DeliveryDataBaseUtils.setMaxLastMileForRestaurant(restaurantId,"4.0");
        Double[] latlonarray = deliveryDataHelper.getLatLng(restLat, restLon, 90.0, 1.0);
        String custZone = getCustomerZoneFromLatLon(latlonarray[0], latlonarray[1]);
        Integer customerRainMode = getRainMode(String.valueOf(custZone));
        if(zone_id != Integer.parseInt(custZone) && customerRainMode != 3)
            serviceablility_Helper.setNoRainsAtZone(String.valueOf(custZone));

        String zrmMaxSla = DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("zone_max_sla").toString();
        if(Integer.parseInt(zrmMaxSla) < 200)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "200", rain_mode, zone_id.toString());


        Map<String,String> mappedresponse = serviceablility_Helper.retryCerebroListingUsingVarargs(10, 1000,
                new String[]{latlonarray[0].toString(), latlonarray[1].toString(), city_id.toString(), restaurantId},
                returnMap("rain_mode", "1"), returnMap("serviceability", "1"), returnMap("non_serviceable_reason", "0"), returnMap("long_distance", "0"));

        String degradationMode = getResponseDegradationMode(mappedresponse);
        String degradationCause = getResponseDegradationCause(mappedresponse);
        serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(restaurantRainMode), "id", String.valueOf(zone_id));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", zoneRainParamsLastMile.toString(), rain_mode, zone_id.toString());
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", zrmMaxSla, rain_mode, zone_id.toString());
        DeliveryDataBaseUtils.setMaxLastMileForArea(area_id.toString(), areaLastMile.toString());
        DeliveryDataBaseUtils.setMaxLastMileForRestaurant(restaurantId,restaurantLastMile.toString());
        if(customerRainMode != 3)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(customerRainMode), "id", String.valueOf(custZone));

        Assert.assertEquals(mappedresponse.get("rain_mode"), "1", "rain mode");
        Assert.assertEquals(degradationMode, "2", "degradation mode");
        Assert.assertEquals(degradationCause, "rain", "degradation cause");
        Assert.assertEquals(mappedresponse.get("serviceability"), "1", "serviceability");
        Assert.assertEquals(mappedresponse.get("non_serviceable_reason"), "0", "non serviceability reason");
        if(zone_id != Integer.parseInt(custZone) && custZone != null)
            Assert.assertEquals(mappedresponse.get("customer_zone_raining"), "false", "customer zone raining");
        else if(zone_id == Integer.parseInt(custZone))
            Assert.assertEquals(mappedresponse.get("customer_zone_raining"), "true", "customer zone raining");
    }


    @Test(description = "SVC_PE_32 :- Verify restaurant is serviceable when the current last mile is equals to the max last_mile of 'rain_mode:1' defined at zone_rain_params of restaurant zone")
    public void rainsAtRestaurantMode1_EqLM()
    {
        rain_mode = "1";
        Double distance  = Double.parseDouble(String.valueOf(deliveryDataHelper.getDistanceBtwnTwoLatLongs(restLat, restLon, defaultCustomerLat, defaultCustomerLon)))/1000;
        String custZoneId = getCustomerZoneFromLatLon(defaultCustomerLat, defaultCustomerLon);
        Integer restaurantRainMode = getRainMode(String.valueOf(zone_id));
        setRainModeAndCause(restaurantRainMode, 1, zone_id);

        String zoneRainParamsLM= dbUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("last_mile").toString();
        dbUtils.updateZoneRainParamAndClearCache("last_mile", distance.toString(), rain_mode, zone_id.toString());
        Logger.getLogger(ServiceabilityTestNew.class.getName()).info("MAX LAST MILE AT ZONE RAIN PARAMS : " + dbUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("last_mile"));

        Integer customerRainMode = getRainMode(String.valueOf(custZoneId));
        if(zone_id != Integer.parseInt(custZoneId) && customerRainMode != 3)
            serviceablility_Helper.setNoRainsAtZone(String.valueOf(custZoneId));

        Map<String,String> mappedresponse = serviceablility_Helper.retryCerebroListingUsingVarargs(10, 1000,
                new String[]{defaultCustomerLat.toString(), defaultCustomerLon.toString(), city_id.toString(), restaurantId},
                returnMap("rain_mode", "1"), returnMap("serviceability", "2"), returnMap("long_distance", "0"));

        dbUtils.updateZoneRainParamAndClearCache("last_mile", zoneRainParamsLM, rain_mode, zone_id.toString());
        serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(restaurantRainMode), "id", String.valueOf(zone_id));
        if(customerRainMode != 3)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(customerRainMode), "id", String.valueOf(custZoneId));

        Assert.assertEquals(mappedresponse.get("rain_mode"), "1", "rain mode");
        Assert.assertEquals(getResponseDegradationMode(mappedresponse), "2", "degradation mode");
        Assert.assertEquals(getResponseDegradationCause(mappedresponse), "rain", "degradation cause");
        Assert.assertEquals(mappedresponse.get("serviceability"), "2", "serviceability");
        if(zone_id != Integer.parseInt(custZoneId) && custZoneId != null)
            Assert.assertEquals(mappedresponse.get("customer_zone_raining"), "false", "customer zone raining");
        else if(zone_id == Integer.parseInt(custZoneId))
            Assert.assertEquals(mappedresponse.get("customer_zone_raining"), "true", "customer zone raining");
    }


    @Test(description = "SLTC_09 :- Verify restaurant is serviceable when the current sla is less than the max sla of 'rain_mode:1' defined at zone_rain_params of restaurant zone")
    public void rainsAtRestaurantMode1_LtSLA()
    {
        rain_mode = "1";
        Integer restaurantRainMode = getRainMode(String.valueOf(zone_id));
        setRainModeAndCause(restaurantRainMode, 1, zone_id);

        Double zoneRainParamsLastMile = Double.parseDouble(getValueFromDBusingQeury("select last_mile from zone_rain_params where zone_id=" + zone_id + " and rain_mode=1", "last_mile"));
        Double[] latlonarray = deliveryDataHelper.getLatLng(restLat, restLon, 90.0, (BigDecimal.valueOf(zoneRainParamsLastMile/1.3).divide(BigDecimal.valueOf(2))).doubleValue());

        String custZone = getCustomerZoneFromLatLon(latlonarray[0], latlonarray[1]);

        Integer customerRainMode = getRainMode(String.valueOf(custZone));
        if(zone_id != Integer.parseInt(custZone) && customerRainMode != 3)
            serviceablility_Helper.setNoRainsAtZone(String.valueOf(custZone));

        Map<String,String> mappedresponse = serviceablility_Helper.retryCerebroListingUsingVarargs(10, 1000,
                new String[]{defaultCustomerLat.toString(), defaultCustomerLon.toString(), city_id.toString(), restaurantId},
                returnMap("rain_mode", "1"), returnMap("serviceability", "2"), returnMap("long_distance", "0"));

        serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(restaurantRainMode), "id", String.valueOf(zone_id));
        if(customerRainMode != 3)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(customerRainMode), "id", String.valueOf(custZone));

        Assert.assertEquals(mappedresponse.get("rain_mode"), "1", "rain mode");
        Assert.assertEquals(getResponseDegradationMode(mappedresponse), "2", "degradation mode");
        Assert.assertEquals(getResponseDegradationCause(mappedresponse), "rain", "degradation cause");
        Assert.assertEquals(mappedresponse.get("serviceability"), "2", "serviceability");
        if(zone_id != Integer.parseInt(custZone) && custZone != null)
            Assert.assertEquals(mappedresponse.get("customer_zone_raining"), "false", "customer zone raining");
        else if(zone_id == Integer.parseInt(custZone))
            Assert.assertEquals(mappedresponse.get("customer_zone_raining"), "true", "customer zone raining");
    }


    @Test(description = "SLTC_10 :- Verify restaurant is not serviceable when the current sla is greater than the max sla of 'rain_mode:1' defined at zone_rain_params of restaurant zone")
    public void rainsAtRestaurantMode1_GtSLA()
    {
        rain_mode = "1";
        Integer restaurantRainMode = getRainMode(String.valueOf(zone_id));
        setRainModeAndCause(restaurantRainMode, Integer.parseInt(rain_mode), zone_id);
        Double zoneRainParamsLastMile = Double.parseDouble(getValueFromDBusingQeury("select last_mile from zone_rain_params where zone_id=" + zone_id + " and rain_mode=" + rain_mode, "last_mile"));
        Double zoneRainParamsMaxSLA = Double.parseDouble(getValueFromDBusingQeury("select zone_max_sla from zone_rain_params where zone_id=" + zone_id + " and rain_mode=" + rain_mode, "zone_max_sla"));
        String id = getValueFromDBusingQeury("select id from zone_rain_params where zone_id=" + zone_id + " and rain_mode=1", "id");
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "10", rain_mode, String.valueOf(zone_id));
        Double[] latlonarray = deliveryDataHelper.getLatLng(restLat, restLon, 90.0, (BigDecimal.valueOf(zoneRainParamsLastMile/1.3).divide(BigDecimal.valueOf(2))).doubleValue());

        String custZone = getCustomerZoneFromLatLon(latlonarray[0], latlonarray[1]);
        Integer customerRainMode = getRainMode(String.valueOf(custZone));
        if(zone_id != Integer.parseInt(custZone) && customerRainMode != 3)
            serviceablility_Helper.setNoRainsAtZone(String.valueOf(custZone));

        Map<String,String> mappedresponse = serviceablility_Helper.retryCerebroListingUsingVarargs(10, 1000,
                new String[]{latlonarray[0].toString(), latlonarray[1].toString(), city_id.toString(), restaurantId},
                returnMap("rain_mode", "1"), returnMap("serviceability", "1"), returnMap("non_serviceable_reason", "1"), returnMap("long_distance", "0"));

        String degradationMode = getResponseDegradationMode(mappedresponse);
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", String.valueOf(zoneRainParamsMaxSLA), rain_mode, String.valueOf(zone_id));

        serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(restaurantRainMode), "id", String.valueOf(zone_id));
        if(customerRainMode != 3)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(customerRainMode), "id", String.valueOf(custZone));

        Assert.assertEquals(mappedresponse.get("rain_mode"), "1", "rain mode");
        Assert.assertEquals(degradationMode, "2", "degradation mode");
        Assert.assertEquals(mappedresponse.get("serviceability"), "1", "serviceability");
        Assert.assertEquals(mappedresponse.get("non_serviceable_reason"), "1", "non serviceable reason");
        if(zone_id != Integer.parseInt(custZone) && custZone != null)
            Assert.assertEquals(mappedresponse.get("customer_zone_raining"), "false", "customer zone raining");
        else if(zone_id == Integer.parseInt(custZone))
            Assert.assertEquals(mappedresponse.get("customer_zone_raining"), "true", "customer zone raining");
    }


    @Test(description = "SVC_PE_35 :- Verify restaurant is serviceable when the current SLA is equals to the max SLA of 'rain_mode:1' defined at zone_rain_params of restaurant zone")
    public void rainsAtRestaurantMode1_EqSLA()
    {
        rain_mode = "1";
        Double distance  = Double.parseDouble(String.valueOf(deliveryDataHelper.getDistanceBtwnTwoLatLongs(restLat, restLon, defaultCustomerLat, defaultCustomerLon)))/1000;
        String custZoneId = getCustomerZoneFromLatLon(defaultCustomerLat, defaultCustomerLon);
        Integer restaurantRainMode = getRainMode(String.valueOf(zone_id));
        setRainModeAndCause(restaurantRainMode, 1, zone_id);

        Integer customerRainMode = getRainMode(String.valueOf(custZoneId));
        if(zone_id != Integer.parseInt(custZoneId) && customerRainMode != 3)
            serviceablility_Helper.setNoRainsAtZone(String.valueOf(custZoneId));

        String  response = serviceablility_Helper.listing(new String[]{defaultCustomerLat.toString(), defaultCustomerLon.toString(),city_id.toString(), restaurantId}).ResponseValidator.GetBodyAsText();
        //Map<String, String> mappedresponse=getServiceabiltyResponseMap(response);
        String slaInResponse=getServiceabiltyResponseMap(response).get("delivery_time");

        String zoneRainParamsSLA= dbUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("zone_max_sla").toString();
        dbUtils.updateZoneRainParamAndClearCache("zone_max_sla", slaInResponse, rain_mode, zone_id.toString());
        Logger.getLogger(ServiceabilityTestNew.class.getName()).info("MAX SLA AT ZONE RAIN PARAMS : " + dbUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("zone_max_sla"));

        Map<String,String> mappedresponse = serviceablility_Helper.retryCerebroListingUsingVarargs(10, 1000,
                new String[]{defaultCustomerLat.toString(), defaultCustomerLon.toString(), city_id.toString(), restaurantId},
                returnMap("rain_mode", "1"), returnMap("serviceability", "2"), returnMap("long_distance", "0"));

        dbUtils.updateZoneRainParamAndClearCache("zone_max_sla", zoneRainParamsSLA, rain_mode, zone_id.toString());
        serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(restaurantRainMode), "id", String.valueOf(zone_id));
        if(customerRainMode != 3)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(customerRainMode), "id", String.valueOf(custZoneId));

        Assert.assertEquals(mappedresponse.get("rain_mode"), "1", "rain mode");
        Assert.assertEquals(getResponseDegradationMode(mappedresponse), "2", "degradation mode");
        Assert.assertEquals(getResponseDegradationCause(mappedresponse), "rain", "degradation cause");
        Assert.assertEquals(mappedresponse.get("serviceability"), "2", "serviceability");
        Assert.assertEquals(slaInResponse, mappedresponse.get("delivery_time"));
        if(zone_id != Integer.parseInt(custZoneId) && custZoneId != null)
            Assert.assertEquals(mappedresponse.get("customer_zone_raining"), "false", "customer zone raining");
        else if(zone_id == Integer.parseInt(custZoneId))
            Assert.assertEquals(mappedresponse.get("customer_zone_raining"), "true", "customer zone raining");
    }


    @Test(description = "SLTC_11 :- Verify restaurant is serviceable when the distance from customer to restaurant is less than the max last mile of 'rain_mode:2' defined at zone_rain_params of restaurant zone")
    public void rainsAtRestaurantMode2_LtLastMile()
    {
        rain_mode = "2";
        Integer restaurantRainMode = getRainMode(String.valueOf(zone_id));
        setRainModeAndCause(restaurantRainMode, Integer.parseInt(rain_mode), zone_id);
        Double zoneRainParamsLastMile = Double.parseDouble(getValueFromDBusingQeury("select last_mile from zone_rain_params where zone_id=" + zone_id + " and rain_mode=" + rain_mode, "last_mile"));
        Double[] latlonarray = deliveryDataHelper.getLatLng(restLat, restLon, 90.0, (BigDecimal.valueOf(zoneRainParamsLastMile/1.3).divide(BigDecimal.valueOf(2))).doubleValue());

        Map<String, Object> restaurantZoneRainParamsDetails = DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("4", zone_id.toString());
        if(restaurantZoneRainParamsDetails.keySet().toString().equals(row_not_found_key))
            Assert.fail("add entry in zone rain params of rain mode : 4 and zone id : " + zone_id.toString());
        String restaurantZRMmaxSla = restaurantZoneRainParamsDetails.get("zone_max_sla").toString();
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "200", rain_mode, zone_id.toString());
        String custZone = getCustomerZoneFromLatLon(latlonarray[0], latlonarray[1]);
        Integer customerRainMode = getRainMode(String.valueOf(custZone));
        if(zone_id != Integer.parseInt(custZone) && customerRainMode != 3)
            serviceablility_Helper.setNoRainsAtZone(String.valueOf(custZone));

        Map<String,String> mappedresponse = serviceablility_Helper.retryCerebroListingUsingVarargs(10, 1000,
                new String[]{latlonarray[0].toString(), latlonarray[1].toString(), city_id.toString(), restaurantId},
                returnMap("rain_mode", "2"), returnMap("serviceability", "2"), returnMap("long_distance", "0"));

        //String degradationMode = getResponseDegradationMode(mappedresponse);
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", restaurantZRMmaxSla, rain_mode, zone_id.toString());

        serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(restaurantRainMode), "id", String.valueOf(zone_id));
        if(customerRainMode != 3)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(customerRainMode), "id", String.valueOf(custZone));

        Assert.assertEquals(mappedresponse.get("rain_mode"), "2", "rain mode");
        Assert.assertEquals(getResponseDegradationMode(mappedresponse), "1", "degradation mode");
        Assert.assertEquals(getResponseDegradationCause(mappedresponse), "rain", "degradation cause");
        Assert.assertEquals(mappedresponse.get("serviceability"), "2", "restaurant not serviceable");
        if(zone_id != Integer.parseInt(custZone) && custZone != null)
            Assert.assertEquals(mappedresponse.get("customer_zone_raining"), "false", "customer zone raining");
        else if(zone_id == Integer.parseInt(custZone))
            Assert.assertEquals(mappedresponse.get("customer_zone_raining"), "true", "customer zone raining");
    }


    @Test(description = "SLTC_12 :- Verify restaurant is not serviceable when the distance from customer to restaurant is greater than the max last mile of 'rain_mode:2' defined at zone_rain_params of restaurant zone")
    public void rainsAtRestaurantMode2_GtLastMile()
    {
        rain_mode = "2";
        Integer restaurantRainMode = getRainMode(String.valueOf(zone_id));
        setRainModeAndCause(restaurantRainMode, Integer.parseInt(rain_mode), zone_id);

        Double zoneRainParamsLastMile = Double.parseDouble(getValueFromDBusingQeury("select last_mile from zone_rain_params where zone_id=" + zone_id + " and rain_mode=" + rain_mode, "last_mile"));
        if(zoneRainParamsLastMile > 1)
        {
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", "1", rain_mode, String.valueOf(zone_id));
            zoneRainParamsLastMile = Double.parseDouble(getValueFromDBusingQeury("select last_mile from zone_rain_params where zone_id=" + zone_id + " and rain_mode=" + rain_mode, "last_mile"));
        }
        Double[] latlonarray = deliveryDataHelper.getLatLng(restLat, restLon, 90.0, (BigDecimal.valueOf(zoneRainParamsLastMile/1.3).add(BigDecimal.valueOf(1.5))).doubleValue());
        String custZone = getCustomerZoneFromLatLon(latlonarray[0], latlonarray[1]);
        Integer customerRainMode = getRainMode(String.valueOf(custZone));
        if(zone_id != Integer.parseInt(custZone) && customerRainMode != 3)
            serviceablility_Helper.setNoRainsAtZone(String.valueOf(custZone));
        String zrmMaxSla = DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("zone_max_sla").toString();
        if(Integer.parseInt(zrmMaxSla) < 200)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "200", rain_mode, zone_id.toString());

        Map<String,String> mappedresponse = serviceablility_Helper.retryCerebroListingUsingVarargs(10, 1000,
                new String[]{latlonarray[0].toString(), latlonarray[1].toString(), city_id.toString(), restaurantId},
                returnMap("rain_mode", "2"), returnMap("serviceability", "1"), returnMap("non_serviceable_reason", "0"), returnMap("long_distance", "0"));

        String degradationMode = getResponseDegradationMode(mappedresponse);
        serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(restaurantRainMode), "id", String.valueOf(zone_id));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", zrmMaxSla, rain_mode, zone_id.toString());
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", "1", rain_mode, String.valueOf(zone_id));
        if(customerRainMode != 3)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(customerRainMode), "id", String.valueOf(custZone));

        Assert.assertEquals(mappedresponse.get("rain_mode"), "2", "rain mode");
        Assert.assertEquals(degradationMode, "1", "degradation mode");
        Assert.assertEquals(mappedresponse.get("serviceability"), "1", "serviceability");
        Assert.assertEquals(mappedresponse.get("non_serviceable_reason"), "0", "non serviceable reason");
        if(zone_id != Integer.parseInt(custZone) && custZone != null)
            Assert.assertEquals(mappedresponse.get("customer_zone_raining"), "false", "customer zone raining");
        else if(zone_id == Integer.parseInt(custZone))
            Assert.assertEquals(mappedresponse.get("customer_zone_raining"), "true", "customer zone raining");
    }


    @Test(description = "SVC_PE_38 :- Verify restaurant is serviceable when the current last mile is equals to the max last_mile of 'rain_mode:2' defined at zone_rain_params of restaurant zone")
    public void rainsAtRestaurantMode2_EqLM()
    {
        rain_mode = "2";
        Double distance  = Double.parseDouble(String.valueOf(deliveryDataHelper.getDistanceBtwnTwoLatLongs(restLat, restLon, defaultCustomerLat, defaultCustomerLon)))/1000;
        String custZoneId = getCustomerZoneFromLatLon(defaultCustomerLat, defaultCustomerLon);
        Integer restaurantRainMode = getRainMode(String.valueOf(zone_id));
        setRainModeAndCause(restaurantRainMode, 2, zone_id);

        String zoneRainParamsLM= dbUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("last_mile").toString();
        dbUtils.updateZoneRainParamAndClearCache("last_mile", distance.toString(), rain_mode, zone_id.toString());
        String zoneRainParamsSLA= dbUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("zone_max_sla").toString();
        dbUtils.updateZoneRainParamAndClearCache("zone_max_sla", "200", rain_mode, zone_id.toString());
        Logger.getLogger(ServiceabilityTestNew.class.getName()).info("MAX LAST MILE AT ZONE RAIN PARAMS : " + dbUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("last_mile"));

        Integer customerRainMode = getRainMode(String.valueOf(custZoneId));
        if(zone_id != Integer.parseInt(custZoneId) && customerRainMode != 3)
            serviceablility_Helper.setNoRainsAtZone(String.valueOf(custZoneId));

        Map<String,String> mappedresponse = serviceablility_Helper.retryCerebroListingUsingVarargs(10, 1000,
                new String[]{defaultCustomerLat.toString(), defaultCustomerLon.toString(), city_id.toString(), restaurantId},
                returnMap("rain_mode", "2"), returnMap("serviceability", "2"), returnMap("long_distance", "0"));

        dbUtils.updateZoneRainParamAndClearCache("last_mile", zoneRainParamsLM, rain_mode, zone_id.toString());
        dbUtils.updateZoneRainParamAndClearCache("zone_max_sla", zoneRainParamsSLA, rain_mode, zone_id.toString());
        serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(restaurantRainMode), "id", String.valueOf(zone_id));
        if(customerRainMode != 3)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(customerRainMode), "id", String.valueOf(custZoneId));

        Assert.assertEquals(mappedresponse.get("rain_mode"), "2", "rain mode");
        Assert.assertEquals(getResponseDegradationMode(mappedresponse), "1", "degradation mode");
        Assert.assertEquals(getResponseDegradationCause(mappedresponse), "rain", "degradation cause");
        Assert.assertEquals(mappedresponse.get("serviceability"), "2", "serviceability");
        Assert.assertEquals(distance.toString(), mappedresponse.get("last_mile_travel"));
        if(zone_id != Integer.parseInt(custZoneId) && custZoneId != null)
            Assert.assertEquals(mappedresponse.get("customer_zone_raining"), "false", "customer zone raining");
        else if(zone_id == Integer.parseInt(custZoneId))
            Assert.assertEquals(mappedresponse.get("customer_zone_raining"), "true", "customer zone raining");
    }


    @Test(description = "SLTC_13 :- Verify restaurant is serviceable when the current sla is less than the max sla of 'rain_mode:2' defined at zone_rain_params of restaurant zone")
    public void rainsAtRestaurantMode2_LtSLA()
    {
        rain_mode = "2";
        Integer restaurantRainMode = getRainMode(String.valueOf(zone_id));
        setRainModeAndCause(restaurantRainMode, 2, zone_id);
        Double zoneRainParamsLastMile = Double.parseDouble(getValueFromDBusingQeury("select last_mile from zone_rain_params where zone_id=" + zone_id + " and rain_mode=" + rain_mode, "last_mile"));
        Double[] latlonarray = deliveryDataHelper.getLatLng(restLat, restLon, 90.0, (BigDecimal.valueOf(zoneRainParamsLastMile/1.3).divide(BigDecimal.valueOf(2))).doubleValue());
        Map<String, Object> restaurantZoneRainParamsDetails = DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString());
        if(restaurantZoneRainParamsDetails.keySet().toString().equals(row_not_found_key))
            Assert.fail("add entry in zone rain params of rain mode : " + rain_mode + " and zone id : " + zone_id.toString());
        String restaurantZRMmaxSla = restaurantZoneRainParamsDetails.get("zone_max_sla").toString();
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "200", rain_mode, zone_id.toString());
        String custZone = getCustomerZoneFromLatLon(latlonarray[0], latlonarray[1]);
        Integer customerRainMode = getRainMode(String.valueOf(custZone));
        if(zone_id != Integer.parseInt(custZone) && customerRainMode != 3)
            serviceablility_Helper.setNoRainsAtZone(String.valueOf(custZone));

        Map<String,String> mappedresponse = serviceablility_Helper.retryCerebroListingUsingVarargs(10, 1000,
                new String[]{latlonarray[0].toString(), latlonarray[1].toString(), city_id.toString(), restaurantId},
                returnMap("rain_mode", "2"), returnMap("serviceability", "2"), returnMap("long_distance", "0"));

       // String degradationMode = getResponseDegradationMode(mappedresponse);
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", restaurantZRMmaxSla, rain_mode, zone_id.toString());
        serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(restaurantRainMode), "id", String.valueOf(zone_id));
        if(customerRainMode != 3)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(customerRainMode), "id", String.valueOf(custZone));

        Assert.assertEquals(mappedresponse.get("rain_mode"), "2", "rain mode");
        Assert.assertEquals(getResponseDegradationMode(mappedresponse), "1", "degradation mode");
        Assert.assertEquals(getResponseDegradationCause(mappedresponse), "rain", "degradation cause");
        Assert.assertEquals(mappedresponse.get("serviceability"), "2", "serviceability");
        if(zone_id != Integer.parseInt(custZone) && custZone != null)
            Assert.assertEquals(mappedresponse.get("customer_zone_raining"), "false", "customer zone raining");
        else if(zone_id == Integer.parseInt(custZone))
            Assert.assertEquals(mappedresponse.get("customer_zone_raining"), "true", "customer zone raining");
    }


    @Test(description = "SLTC_14 :- Verify restaurant is not serviceable when the current sla is greater than the max sla of 'rain_mode:2' defined at zone_rain_params of restaurant zone")
    public void rainsAtRestaurantMode2_GtSLA()
    {
        rain_mode = "2";
        Integer restaurantRainMode = getRainMode(String.valueOf(zone_id));
        setRainModeAndCause(restaurantRainMode, Integer.parseInt(rain_mode), zone_id);
        Double zoneRainParamsLastMile = Double.parseDouble(getValueFromDBusingQeury("select last_mile from zone_rain_params where zone_id='" + zone_id + "' and rain_mode='" + rain_mode + "'", "last_mile"));
        Double zoneRainParamsMaxSLA = Double.parseDouble(getValueFromDBusingQeury("select zone_max_sla from zone_rain_params where zone_id='" + zone_id + "' and rain_mode='" + rain_mode + "'", "zone_max_sla"));

        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "10", rain_mode, String.valueOf(zone_id));
        Double[] latlonarray = deliveryDataHelper.getLatLng(restLat, restLon, 90.0, (BigDecimal.valueOf(zoneRainParamsLastMile/1.3).divide(BigDecimal.valueOf(2))).doubleValue());

        String custZone = getCustomerZoneFromLatLon(latlonarray[0], latlonarray[1]);
        Integer customerRainMode = getRainMode(String.valueOf(custZone));
        if(zone_id != Integer.parseInt(custZone) && customerRainMode != 3)
            serviceablility_Helper.setNoRainsAtZone(String.valueOf(custZone));

        Map<String,String> mappedresponse = serviceablility_Helper.retryCerebroListingUsingVarargs(10, 1000,
                new String[]{latlonarray[0].toString(), latlonarray[1].toString(), city_id.toString(), restaurantId},
                returnMap("rain_mode", "2"), returnMap("serviceability", "1"), returnMap("non_serviceable_reason", "1"), returnMap("long_distance", "0"));

        //String degradationMode = getResponseDegradationMode(mappedresponse);
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", String.valueOf(zoneRainParamsMaxSLA), rain_mode, String.valueOf(zone_id));

        serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(restaurantRainMode), "id", String.valueOf(zone_id));
        if(customerRainMode != 3)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(customerRainMode), "id", String.valueOf(custZone));

        Assert.assertEquals(mappedresponse.get("rain_mode"), "2", "rain mode");
        Assert.assertEquals(getResponseDegradationMode(mappedresponse), "1", "degradation mode");
        Assert.assertEquals(getResponseDegradationCause(mappedresponse), "rain", "degradation cause");
        Assert.assertEquals(mappedresponse.get("serviceability"), "1", "serviceability");
        Assert.assertEquals(mappedresponse.get("non_serviceable_reason"), "1", "non serviceable reason");
        if(zone_id != Integer.parseInt(custZone) && custZone != null)
            Assert.assertEquals(mappedresponse.get("customer_zone_raining"), "false", "customer zone raining");
        else if(zone_id == Integer.parseInt(custZone))
            Assert.assertEquals(mappedresponse.get("customer_zone_raining"), "true", "customer zone raining");
    }

    @Test(description = "SVC_PE_41 :- Verify restaurant is serviceable when the current SLA is equals to the max SLA of 'rain_mode:2' defined at zone_rain_params of restaurant zone")
    public void rainsAtRestaurantMode2_EqSLA()
    {
        rain_mode = "2";
        String custZoneId = getCustomerZoneFromLatLon(defaultCustomerLat, defaultCustomerLon);
        Integer restaurantRainMode = getRainMode(String.valueOf(zone_id));
        setRainModeAndCause(restaurantRainMode, Integer.parseInt(rain_mode), zone_id);

        Integer customerRainMode = getRainMode(String.valueOf(custZoneId));
        if(zone_id != Integer.parseInt(custZoneId) && customerRainMode != 3)
            serviceablility_Helper.setNoRainsAtZone(String.valueOf(custZoneId));

        String  response = serviceablility_Helper.listing(new String[]{defaultCustomerLat.toString(), defaultCustomerLon.toString(),city_id.toString(), restaurantId}).ResponseValidator.GetBodyAsText();
        //Map<String, String> mappedresponse=getServiceabiltyResponseMap(response);
        String slaInResponse=getServiceabiltyResponseMap(response).get("delivery_time");

        String zoneRainParamsSLA= dbUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("zone_max_sla").toString();
        dbUtils.updateZoneRainParamAndClearCache("zone_max_sla", slaInResponse, rain_mode, zone_id.toString());
        Logger.getLogger(ServiceabilityTestNew.class.getName()).info("MAX SLA AT ZONE RAIN PARAMS : " + dbUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("zone_max_sla"));

        Map<String,String> mappedresponse = serviceablility_Helper.retryCerebroListingUsingVarargs(10, 1000,
                new String[]{defaultCustomerLat.toString(), defaultCustomerLon.toString(), city_id.toString(), restaurantId},
                returnMap("rain_mode", "1"), returnMap("serviceability", "2"), returnMap("long_distance", "0"));

        dbUtils.updateZoneRainParamAndClearCache("zone_max_sla", zoneRainParamsSLA, rain_mode, zone_id.toString());
        serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(restaurantRainMode), "id", String.valueOf(zone_id));
        if(customerRainMode != 3)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(customerRainMode), "id", String.valueOf(custZoneId));

        Assert.assertEquals(mappedresponse.get("rain_mode"), "2", "rain mode");
        Assert.assertEquals(getResponseDegradationMode(mappedresponse), "1", "degradation mode");
        Assert.assertEquals(getResponseDegradationCause(mappedresponse), "rain", "degradation cause");
        Assert.assertEquals(mappedresponse.get("serviceability"), "2", "serviceability");
        Assert.assertEquals(slaInResponse, mappedresponse.get("delivery_time"));
        if(zone_id != Integer.parseInt(custZoneId) && custZoneId != null)
            Assert.assertEquals(mappedresponse.get("customer_zone_raining"), "false", "customer zone raining");
        else if(zone_id == Integer.parseInt(custZoneId))
            Assert.assertEquals(mappedresponse.get("customer_zone_raining"), "true", "customer zone raining");
    }


    @Test(description = "SLTC_15 :- Verify restaurant is serviceable when the distance from customer to restaurant is less than the max last mile of 'rain_mode:4' defined at zone_rain_params of restaurant zone")
    public void rainsAtRestaurantMode4Gd1_LtLastMile()
    {
        Integer restaurantRainMode = getRainMode(String.valueOf(zone_id));
        setRainModeAndCause(restaurantRainMode, Integer.parseInt(RAIN_MODE_DEGRADATION), zone_id);
        Double degradationState = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select degradation_state from zone_rain_params where zone_id=" + zone_id + " and rain_mode='" + RAIN_MODE_DEGRADATION + "'").get("degradation_state").toString());
        if(degradationState != Double.parseDouble(DEGRADATION_MODE_LIGHT))
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", DEGRADATION_MODE_LIGHT, RAIN_MODE_DEGRADATION, String.valueOf(zone_id));

        Double zoneRainParamsLastMile = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select last_mile from zone_rain_params where zone_id=" + zone_id + " and rain_mode='" + RAIN_MODE_DEGRADATION + "'").get("last_mile").toString());
        Double[] latlonarray = deliveryDataHelper.getLatLng(restLat, restLon, 90.0, (BigDecimal.valueOf(zoneRainParamsLastMile/1.3).divide(BigDecimal.valueOf(2))).doubleValue());
        Map<String, Object> restaurantZoneRainParamDetails = DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(RAIN_MODE_DEGRADATION, String.valueOf(zone_id));
        if(restaurantZoneRainParamDetails.keySet().toString().equals(row_not_found_key))
            Assert.fail("add entry in zone rain params of rain mode : " + RAIN_MODE_DEGRADATION + " and zone id : " + zone_id.toString());
        Integer restZRMmaxSla = Integer.parseInt(restaurantZoneRainParamDetails.get("zone_max_sla").toString());
        if(restZRMmaxSla < 100)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "200", RAIN_MODE_DEGRADATION, String.valueOf(zone_id));

        String custZone = getCustomerZoneFromLatLon(latlonarray[0], latlonarray[1]);
        Integer customerRainMode = 0;
        if(zone_id != Integer.parseInt(custZone) && custZone != null)
        {
            customerRainMode = getRainMode(String.valueOf(custZone));
            if(customerRainMode != Integer.parseInt(NO_RAIN))
                serviceablility_Helper.setNoRainsAtZone(String.valueOf(custZone));
        }

        Map<String,String> mappedresponse = serviceablility_Helper.retryCerebroListingUsingVarargs(10, 1000,
                new String[]{latlonarray[0].toString(), latlonarray[1].toString(), city_id.toString(), restaurantId},
                returnMap("rain_mode", "2"), returnMap("serviceability", "2"), returnMap("long_distance", "0"));

        //String degradationMode = getResponseDegradationMode(mappedresponse);
        serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(restaurantRainMode), "id", String.valueOf(zone_id));
        if(customerRainMode != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(customerRainMode), "id", String.valueOf(custZone));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", String.valueOf(restZRMmaxSla), RAIN_MODE_DEGRADATION, String.valueOf(zone_id));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", String.valueOf(degradationState), RAIN_MODE_DEGRADATION, String.valueOf(zone_id));

        Assert.assertEquals(mappedresponse.get("rain_mode"), RAIN_MODE_LIGHT, "rain mode");
        Assert.assertEquals(getResponseDegradationMode(mappedresponse), DEGRADATION_MODE_LIGHT, "degradation mode");
        Assert.assertEquals(getResponseDegradationCause(mappedresponse), "rain", "degradation cause");
        Assert.assertEquals(mappedresponse.get("serviceability"), "2", "serviceability");
        if(zone_id != Integer.parseInt(custZone) && custZone != null)
            Assert.assertEquals(mappedresponse.get("customer_zone_raining"), "false", "customer zone raining");
        else if(zone_id == Integer.parseInt(custZone))
            Assert.assertEquals(mappedresponse.get("customer_zone_raining"), "true", "customer zone raining");
    }


    public Double zoneRainParamLastMile(Double zoneRainParamLM, Double defaultMaxlastMile, String rainMode)
    {
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", String.valueOf(actualLastMile - 2), rainMode, String.valueOf(zone_id));
        zoneRainParamLM = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select last_mile from zone_rain_params where zone_id=" + zone_id + " and rain_mode='" + rainMode + "'").get("last_mile").toString());
        return zoneRainParamLM;
    }


    @Test(description = "SLTC_16 :- Verify restaurant is not serviceable when the distance from customer to restaurant is greater than the max last mile of 'rain_mode:4' defined at zone_rain_params of restaurant zone")
    public void rainsAtRestaurantMode4Gd1_GtLastMile()
    {
        Integer restaurantRainMode = getRainMode(String.valueOf(zone_id));
        setRainModeAndCause(restaurantRainMode, Integer.parseInt(RAIN_MODE_DEGRADATION), zone_id);
        Double degradationState = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select degradation_state from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("degradation_state").toString());
        if(degradationState != 1)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", "1", "4", String.valueOf(zone_id));

        Double zoneRainParamsLastMile = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select last_mile from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("last_mile").toString());
        Double maxLastMile = zoneRainParamsLastMile;
        maxLastMile = (zoneRainParamsLastMile >= actualLastMile) ? zoneRainParamLastMile(zoneRainParamsLastMile, actualLastMile, "4") : zoneRainParamsLastMile;
        Integer restZRMmaxSla = Integer.parseInt(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("4", String.valueOf(zone_id)).get("zone_max_sla").toString());
        if(restZRMmaxSla < 100)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "200", "4", String.valueOf(zone_id));
        Double[] latlonarray = deliveryDataHelper.getLatLng(restLat, restLon, 90.0, (BigDecimal.valueOf(maxLastMile/1.3).add(BigDecimal.valueOf(0.5))).doubleValue());
        String custZone = getCustomerZoneFromLatLon(latlonarray[0], latlonarray[1]);
        Integer customerRainMode = 0;
        if(zone_id != Integer.parseInt(custZone) && custZone != null)
        {
            customerRainMode = getRainMode(custZone);
            if(customerRainMode != 3)
                serviceablility_Helper.setNoRainsAtZone(String.valueOf(custZone));
        }

        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {latlonarray[0].toString(), latlonarray[1].toString(), city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode","2");
            put("serviceability","1");
            put("non_serviceable_reason","0");
            put("long_distance","0");
        }});
        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", String.valueOf(zoneRainParamsLastMile), "4", String.valueOf(zone_id));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", String.valueOf(degradationState), "4", String.valueOf(zone_id));
        serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(restaurantRainMode), "id", String.valueOf(zone_id));
        if(customerRainMode != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(customerRainMode), "id", String.valueOf(custZone));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", String.valueOf(restZRMmaxSla), "4", String.valueOf(zone_id));

        Assert.assertEquals(mappedResponse.get("rain_mode"), "2", "rain mode");
        Assert.assertEquals(degradationMode, "1", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceability"), "1", "serviceability");
        Assert.assertEquals(mappedResponse.get("non_serviceable_reason"), "0", "non serviceable reason");
        if(zone_id != Integer.parseInt(custZone) && custZone != null)
            Assert.assertEquals(mappedResponse.get("customer_zone_raining"), "false", "customer zone raining");
        else if(zone_id == Integer.parseInt(custZone))
            Assert.assertEquals(mappedResponse.get("customer_zone_raining"), "true", "customer zone raining");
    }


    @Test(description = "SVC_PE_92 :- Verify restaurant is serviceable when the current last mile is equals to the max last_mile of 'rain_mode:4' & 'degradarion_state:1' defined at zone_rain_params of restaurant zone")
    public void rainsAtRestaurantMode4GD1_EqLM()
    {
        rain_mode = RAIN_MODE_DEGRADATION;
        Double distance  = Double.parseDouble(String.valueOf(deliveryDataHelper.getDistanceBtwnTwoLatLongs(restLat, restLon, defaultCustomerLat, defaultCustomerLon)))/1000;
        String custZoneId = getCustomerZoneFromLatLon(defaultCustomerLat, defaultCustomerLon);
        Integer restaurantRainMode = getRainMode(String.valueOf(zone_id));
        setRainModeAndCause(restaurantRainMode, Integer.parseInt(rain_mode), zone_id);

        String zoneRainParamsGDState= dbUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("degradation_state").toString();
        dbUtils.updateZoneRainParamAndClearCache("degradation_state", DEGRADATION_MODE_LIGHT, rain_mode, zone_id.toString());
        String zoneRainParamsLM= dbUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("last_mile").toString();
        dbUtils.updateZoneRainParamAndClearCache("last_mile", distance.toString(), rain_mode, zone_id.toString());
        String zoneRainParamsSLA= dbUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("zone_max_sla").toString();
        dbUtils.updateZoneRainParamAndClearCache("zone_max_sla", "200", rain_mode, zone_id.toString());
        Logger.getLogger(ServiceabilityTestNew.class.getName()).info("MAX LAST MILE AT ZONE RAIN PARAMS : " + dbUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("last_mile"));

        Integer customerRainMode = getRainMode(String.valueOf(custZoneId));
        if(zone_id != Integer.parseInt(custZoneId) && customerRainMode != 3)
            serviceablility_Helper.setNoRainsAtZone(String.valueOf(custZoneId));

        Map<String,String> mappedresponse = serviceablility_Helper.retryCerebroListingUsingVarargs(10, 1000,
                new String[]{defaultCustomerLat.toString(), defaultCustomerLon.toString(), city_id.toString(), restaurantId},
                returnMap("rain_mode", "2"), returnMap("serviceability", "2"), returnMap("long_distance", "0"));

        dbUtils.updateZoneRainParamAndClearCache("degradation_state", zoneRainParamsGDState, rain_mode, zone_id.toString());
        dbUtils.updateZoneRainParamAndClearCache("last_mile", zoneRainParamsLM, rain_mode, zone_id.toString());
        dbUtils.updateZoneRainParamAndClearCache("zone_max_sla", zoneRainParamsSLA, rain_mode, zone_id.toString());
        serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(restaurantRainMode), "id", String.valueOf(zone_id));
        if(customerRainMode != 3)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(customerRainMode), "id", String.valueOf(custZoneId));

        Assert.assertEquals(mappedresponse.get("rain_mode"), RAIN_MODE_LIGHT, "rain mode");
        Assert.assertEquals(getResponseDegradationMode(mappedresponse), DEGRADATION_MODE_LIGHT, "degradation mode");
        Assert.assertEquals(getResponseDegradationCause(mappedresponse), "rain", "degradation cause");
        Assert.assertEquals(mappedresponse.get("serviceability"), "2", "serviceability");
        Assert.assertEquals(distance.toString(), mappedresponse.get("last_mile_travel"));
        if(zone_id != Integer.parseInt(custZoneId) && custZoneId != null)
            Assert.assertEquals(mappedresponse.get("customer_zone_raining"), "false", "customer zone raining");
        else if(zone_id == Integer.parseInt(custZoneId))
            Assert.assertEquals(mappedresponse.get("customer_zone_raining"), "true", "customer zone raining");
    }



    @Test(description = "SLTC_17 :- Verify restaurant is serviceable when the current sla is less than the max sla of 'rain_mode:4' defined at zone_rain_params of restaurant zone")
    public void rainsAtRestaurantMode4Gd1_LtSLA()
    {
        rain_mode = RAIN_MODE_DEGRADATION;
        String custZoneId = getCustomerZoneFromLatLon(defaultCustomerLat, defaultCustomerLon);
        Integer restaurantRainMode = getRainMode(String.valueOf(zone_id));
        setRainModeAndCause(restaurantRainMode, Integer.parseInt(rain_mode), zone_id);

        String zoneRainParamsGDState= dbUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("degradation_state").toString();
        dbUtils.updateZoneRainParamAndClearCache("degradation_state", DEGRADATION_MODE_LIGHT, rain_mode, zone_id.toString());
        String zoneRainParamsSLA= dbUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("zone_max_sla").toString();
        dbUtils.updateZoneRainParamAndClearCache("zone_max_sla", "200", rain_mode, zone_id.toString());
        Logger.getLogger(ServiceabilityTestNew.class.getName()).info("MAX LAST MILE AT ZONE RAIN PARAMS : " + dbUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("last_mile"));

        Integer customerRainMode = getRainMode(String.valueOf(custZoneId));
        if(zone_id != Integer.parseInt(custZoneId) && customerRainMode != 3)
            serviceablility_Helper.setNoRainsAtZone(String.valueOf(custZoneId));

        Double zoneRainParamsLastMile = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select last_mile from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("last_mile").toString());
        Double maxLastMile = zoneRainParamsLastMile;
        if(zoneRainParamsLastMile >= actualLastMile)
        {
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", String.valueOf((actualLastMile - 1)), "4", String.valueOf(zone_id));
            zoneRainParamsLastMile = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select last_mile from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("last_mile").toString());
        }
        Double[] latlonarray = deliveryDataHelper.getLatLng(restLat, restLon, 90.0, (BigDecimal.valueOf(zoneRainParamsLastMile/1.3).divide(BigDecimal.valueOf(2))).doubleValue());

        String custZone = getCustomerZoneFromLatLon(latlonarray[0], latlonarray[1]);
        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {latlonarray[0].toString(), latlonarray[1].toString(), city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode","2");
            put("serviceability","2");
            put("long_distance","0");
        }});

        //String degradationMode = getResponseDegradationMode(mappedResponse);
        //DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", String.valueOf(maxLastMile), "4", String.valueOf(zone_id));
        //DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", String.valueOf(degradationState), "4", String.valueOf(zone_id));

        dbUtils.updateZoneRainParamAndClearCache("degradation_state", zoneRainParamsGDState, rain_mode, zone_id.toString());
        dbUtils.updateZoneRainParamAndClearCache("last_mile", maxLastMile.toString(), rain_mode, zone_id.toString());
        dbUtils.updateZoneRainParamAndClearCache("zone_max_sla", zoneRainParamsSLA, rain_mode, zone_id.toString());

        serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(restaurantRainMode), "id", String.valueOf(zone_id));
        if(customerRainMode != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(customerRainMode), "id", String.valueOf(custZone));

        Assert.assertEquals(mappedResponse.get("rain_mode"), RAIN_MODE_LIGHT, "rain mode");
        Assert.assertEquals(getResponseDegradationMode(mappedResponse), DEGRADATION_MODE_LIGHT, "degradation mode");
        Assert.assertEquals(getResponseDegradationCause(mappedResponse), "rain", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceability"), "2", "serviceability");
        if(zone_id != Integer.parseInt(custZone) && custZone != null)
            Assert.assertEquals(mappedResponse.get("customer_zone_raining"), "false", "customer zone raining");
        else if(zone_id == Integer.parseInt(custZone))
            Assert.assertEquals(mappedResponse.get("customer_zone_raining"), "true", "customer zone raining");
    }


    @Test(description = "SLTC_18 :- Verify restaurant is not serviceable when the current sla is greater than the max sla of 'rain_mode:4' defined at zone_rain_params of restaurant zone")
    public void rainsAtRestaurantMode4Gd1_GtSLA()
    {
        Integer restaurantRainMode = getRainMode(String.valueOf(zone_id));
        setRainModeAndCause(restaurantRainMode, 4, zone_id);
        Double degradationState = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select degradation_state from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("degradation_state").toString());
        if(degradationState != 1)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", "1", "4", String.valueOf(zone_id));

        Double zoneRainParamsLastMile = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select last_mile from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("last_mile").toString());
        Double maxLastMile = zoneRainParamsLastMile;
        if(zoneRainParamsLastMile >= actualLastMile)
        {
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", String.valueOf(actualLastMile - 1), "4", String.valueOf(zone_id));
            zoneRainParamsLastMile = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select last_mile from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("last_mile").toString());
        }

        Double zoneRainParamsMaxSLA = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select zone_max_sla from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("zone_max_sla").toString());
        SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update("update zone_rain_params set zone_max_sla='10' where zone_id=" + zone_id + " and rain_mode='4'");
        Double[] latlonarray = deliveryDataHelper.getLatLng(restLat, restLon, 90.0, (BigDecimal.valueOf(zoneRainParamsLastMile/1.3).divide(BigDecimal.valueOf(2))).doubleValue());

        String custZone = getCustomerZoneFromLatLon(latlonarray[0], latlonarray[1]);
        Integer customerRainMode = 0;
        if(zone_id != Integer.parseInt(custZone) && custZone != null)
        {
            customerRainMode = getRainMode(String.valueOf(custZone));
            if(customerRainMode != 3)
                serviceablility_Helper.setNoRainsAtZone(String.valueOf(custZone));
        }

        Map<String,String> mappedResponse = serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {latlonarray[0].toString(), latlonarray[1].toString(), city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode","2");
            put("serviceability","1");
            put("non_serviceable_reason", "1");
            put("long_distance","0");
        }});

        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", String.valueOf(zoneRainParamsMaxSLA), "4", String.valueOf(zone_id));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", String.valueOf(maxLastMile), "4", String.valueOf(zone_id));
        serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(restaurantRainMode), "id", String.valueOf(zone_id));
        if(customerRainMode != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(customerRainMode), "id", String.valueOf(custZone));

        Assert.assertEquals(mappedResponse.get("rain_mode"), "2", "rain mode");
        Assert.assertEquals(degradationMode, "1", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceability"), "1", "serviceability");
        Assert.assertEquals(mappedResponse.get("non_serviceable_reason"), "1", "non serviceable reason");
        if(zone_id != Integer.parseInt(custZone) && custZone != null)
            Assert.assertEquals(mappedResponse.get("customer_zone_raining"), "false", "customer zone raining");
        else if(zone_id == Integer.parseInt(custZone))
            Assert.assertEquals(mappedResponse.get("customer_zone_raining"), "true", "customer zone raining");
    }


    @Test(description = "SVC_PE_95 :- Verify restaurant is serviceable when the current SLA is equals to the max SLA of 'rain_mode:4' & 'degradation_state:1' defined at zone_rain_params of restaurant zone")
    public void rainsAtRestaurantMode4GD1_EqSLA()
    {
        rain_mode = RAIN_MODE_DEGRADATION;
        String custZoneId = getCustomerZoneFromLatLon(defaultCustomerLat, defaultCustomerLon);
        Integer restaurantRainMode = getRainMode(String.valueOf(zone_id));
        setRainModeAndCause(restaurantRainMode, Integer.parseInt(rain_mode), zone_id);

        Integer customerRainMode = getRainMode(String.valueOf(custZoneId));
        if(zone_id != Integer.parseInt(custZoneId) && customerRainMode != Integer.parseInt(NO_RAIN))
            serviceablility_Helper.setNoRainsAtZone(String.valueOf(custZoneId));

        String  response = serviceablility_Helper.listing(new String[]{defaultCustomerLat.toString(), defaultCustomerLon.toString(),city_id.toString(), restaurantId}).ResponseValidator.GetBodyAsText();
        //Map<String, String> mappedresponse=getServiceabiltyResponseMap(response);
        String slaInResponse=getServiceabiltyResponseMap(response).get("delivery_time");

        String zoneRainParamsGDState= dbUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("degradation_state").toString();
        dbUtils.updateZoneRainParamAndClearCache("degradation_state", DEGRADATION_MODE_LIGHT, rain_mode, zone_id.toString());

        String zoneRainParamsSLA= dbUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("zone_max_sla").toString();
        dbUtils.updateZoneRainParamAndClearCache("zone_max_sla", slaInResponse, rain_mode, zone_id.toString());
        Logger.getLogger(ServiceabilityTestNew.class.getName()).info("MAX SLA AT ZONE RAIN PARAMS : " + dbUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("zone_max_sla"));

        Map<String,String> mappedresponse = serviceablility_Helper.retryCerebroListingUsingVarargs(10, 1000,
                new String[]{defaultCustomerLat.toString(), defaultCustomerLon.toString(), city_id.toString(), restaurantId},
                returnMap("rain_mode", RAIN_MODE_LIGHT), returnMap("serviceability", "2"), returnMap("long_distance", "0"));

        dbUtils.updateZoneRainParamAndClearCache("degradation_state", zoneRainParamsGDState, rain_mode, zone_id.toString());
        dbUtils.updateZoneRainParamAndClearCache("zone_max_sla", zoneRainParamsSLA, rain_mode, zone_id.toString());
        serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(restaurantRainMode), "id", String.valueOf(zone_id));
        if(customerRainMode != Integer.parseInt(NO_RAIN))
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(customerRainMode), "id", String.valueOf(custZoneId));

        Assert.assertEquals(mappedresponse.get("rain_mode"), RAIN_MODE_LIGHT, "rain mode");
        Assert.assertEquals(getResponseDegradationMode(mappedresponse), DEGRADATION_MODE_LIGHT, "degradation mode");
        Assert.assertEquals(getResponseDegradationCause(mappedresponse), "rain", "degradation cause");
        Assert.assertEquals(mappedresponse.get("serviceability"), "2", "serviceability");
        Assert.assertEquals(slaInResponse, mappedresponse.get("delivery_time"));
        if(zone_id != Integer.parseInt(custZoneId) && custZoneId != null)
            Assert.assertEquals(mappedresponse.get("customer_zone_raining"), "false", "customer zone raining");
        else if(zone_id == Integer.parseInt(custZoneId))
            Assert.assertEquals(mappedresponse.get("customer_zone_raining"), "true", "customer zone raining");
    }

    @Test(description = "SLTC_19 :- Verify restaurant is serviceable when the distance from customer to restaurant is less than the max last mile of 'rain_mode:4' defined at zone_rain_params of restaurant zone")
    public void rainsAtRestaurantMode4Gd2_LtLastMile()
    {
        Integer restaurantRainMode = getRainMode(String.valueOf(zone_id));
        setRainModeAndCause(restaurantRainMode, 4, zone_id);

        Double degradationState = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select degradation_state from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("degradation_state").toString());
        if(degradationState != 2)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", "2", "4", String.valueOf(zone_id));

        Double zoneRainParamsLastMile = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select last_mile from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("last_mile").toString());
        Double maxLastMile = zoneRainParamsLastMile;
        if(zoneRainParamsLastMile >= actualLastMile)
        {
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", String.valueOf((actualLastMile - 1)), "4", String.valueOf(zone_id));
            zoneRainParamsLastMile = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select last_mile from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("last_mile").toString());
        }
        Integer restZRMmaxSla = Integer.parseInt(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("4", String.valueOf(zone_id)).get("zone_max_sla").toString());
        if(restZRMmaxSla < 100)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "200", "4", String.valueOf(zone_id));
        Double[] latlonarray = deliveryDataHelper.getLatLng(restLat, restLon, 90.0, (BigDecimal.valueOf(zoneRainParamsLastMile/1.3).divide(BigDecimal.valueOf(2))).doubleValue());

        String custZone = getCustomerZoneFromLatLon(latlonarray[0], latlonarray[1]);
        Integer customerRainMode = 0;
        if(zone_id != Integer.parseInt(custZone) && custZone != null)
        {
            customerRainMode = getRainMode(String.valueOf(custZone));
            if(customerRainMode != 3)
                serviceablility_Helper.setNoRainsAtZone(String.valueOf(custZone));
        }
        Map<String,String> mappedResponse = serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {latlonarray[0].toString(), latlonarray[1].toString(), city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode","1");
            put("serviceability","2");
            put("long_distance","0");
        }});
        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", String.valueOf(maxLastMile), "4", String.valueOf(zone_id));

        serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(restaurantRainMode), "id", String.valueOf(zone_id));
        if(customerRainMode != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(customerRainMode), "id", String.valueOf(custZone));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", String.valueOf(restZRMmaxSla), "4", String.valueOf(zone_id));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", String.valueOf(degradationState), "4", String.valueOf(zone_id));

        Assert.assertEquals(mappedResponse.get("rain_mode"), "1", "rain mode");
        Assert.assertEquals(degradationMode, "2", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceability"), "2", "serviceability");
        if(zone_id != Integer.parseInt(custZone) && custZone != null)
            Assert.assertEquals(mappedResponse.get("customer_zone_raining"), "false", "customer zone raining");
        else if(zone_id == Integer.parseInt(custZone))
            Assert.assertEquals(mappedResponse.get("customer_zone_raining"), "true", "customer zone raining");
    }


    @Test(description = "SLTC_20 :- Verify restaurant is not serviceable when the distance from customer to restaurant is greater than the max last mile of 'rain_mode:4' defined at zone_rain_params of restaurant zone")
    public void rainsAtRestaurantMode4Gd2_GtLastMile()
    {
        Integer restaurantRainMode = getRainMode(String.valueOf(zone_id));
        setRainModeAndCause(restaurantRainMode, 4, zone_id);
        Double degradationState = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select degradation_state from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("degradation_state").toString());
        if(degradationState != 2)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", "2", "4", String.valueOf(zone_id));

        Double zoneRainParamsLastMile = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select last_mile from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("last_mile").toString());
        if(zoneRainParamsLastMile > 1) {
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", "1", "4", String.valueOf(zone_id));
            zoneRainParamsLastMile = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("4", String.valueOf(zone_id)).get("last_mile").toString());
        }
        Integer restZRMmaxSla = Integer.parseInt(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("4", String.valueOf(zone_id)).get("zone_max_sla").toString());
        if(restZRMmaxSla < 100)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "200", "4", String.valueOf(zone_id));
        Double[] latlonarray = deliveryDataHelper.getLatLng(restLat, restLon, 90.0, (BigDecimal.valueOf(zoneRainParamsLastMile/1.3).add(BigDecimal.valueOf(1))).doubleValue());
        String custZone = getCustomerZoneFromLatLon(latlonarray[0], latlonarray[1]);
        Integer customerRainMode = 0;
        if(zone_id != Integer.parseInt(custZone) && custZone != null)
        {
            customerRainMode = getRainMode(custZone);
            if(customerRainMode != 3)
                serviceablility_Helper.setNoRainsAtZone(String.valueOf(custZone));
        }
        Map<String,String> mappedResponse = serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {latlonarray[0].toString(), latlonarray[1].toString(), city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode", "1");
            put("serviceability", "1");
            put("non_serviceable_reason", "0");
            put("long_distance", "0");
        }});
        String degradationMode = getResponseDegradationMode(mappedResponse);
        serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(restaurantRainMode), "id", String.valueOf(zone_id));
        if(customerRainMode != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(customerRainMode), "id", String.valueOf(custZone));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", String.valueOf(restZRMmaxSla), "4", String.valueOf(zone_id));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", String.valueOf(degradationState), "4", String.valueOf(zone_id));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", String.valueOf(zoneRainParamsLastMile), "4", String.valueOf(zone_id));


        Assert.assertEquals(mappedResponse.get("rain_mode"), "1", "rain mode");
        Assert.assertEquals(degradationMode, "2", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceability"), "1", "serviceability");
        Assert.assertEquals(mappedResponse.get("non_serviceable_reason"), "0", "non serviceable reason");
        if(zone_id != Integer.parseInt(custZone) && custZone != null)
            Assert.assertEquals(mappedResponse.get("customer_zone_raining"), "false", "customer zone raining");
        else if(zone_id == Integer.parseInt(custZone))
            Assert.assertEquals(mappedResponse.get("customer_zone_raining"), "true", "customer zone raining");
    }


    @Test(description = "SVC_PE_104 :- Verify restaurant is serviceable when the current last mile is equals to the max last_mile of 'rain_mode:4', 'degradarion_state:2' & 'cause:rain' defined at zone_rain_params of restaurant zone")
    public void rainsAtRestaurantMode4GD2_EqLM()
    {
        rain_mode = RAIN_MODE_DEGRADATION;
        Double distance  = Double.parseDouble(String.valueOf(deliveryDataHelper.getDistanceBtwnTwoLatLongs(restLat, restLon, defaultCustomerLat, defaultCustomerLon)))/1000;
        String custZoneId = getCustomerZoneFromLatLon(defaultCustomerLat, defaultCustomerLon);
        Integer restaurantRainMode = getRainMode(String.valueOf(zone_id));
        setRainModeAndCause(restaurantRainMode, Integer.parseInt(rain_mode), zone_id);

        String zoneRainParamsGDState= dbUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("degradation_state").toString();
        dbUtils.updateZoneRainParamAndClearCache("degradation_state", DEGRADATION_MODE_HEAVY, rain_mode, zone_id.toString());
        String zoneRainParamsLM= dbUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("last_mile").toString();
        dbUtils.updateZoneRainParamAndClearCache("last_mile", distance.toString(), rain_mode, zone_id.toString());
        String zoneRainParamsSLA= dbUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("zone_max_sla").toString();
        dbUtils.updateZoneRainParamAndClearCache("zone_max_sla", "200", rain_mode, zone_id.toString());
        Logger.getLogger(ServiceabilityTestNew.class.getName()).info("MAX LAST MILE AT ZONE RAIN PARAMS : " + dbUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("last_mile"));

        Integer customerRainMode = getRainMode(String.valueOf(custZoneId));
        if(zone_id != Integer.parseInt(custZoneId) && customerRainMode != 3)
            serviceablility_Helper.setNoRainsAtZone(String.valueOf(custZoneId));

        Map<String,String> mappedresponse = serviceablility_Helper.retryCerebroListingUsingVarargs(10, 1000,
                new String[]{defaultCustomerLat.toString(), defaultCustomerLon.toString(), city_id.toString(), restaurantId},
                returnMap("rain_mode", RAIN_MODE_HEAVY), returnMap("serviceability", "2"), returnMap("long_distance", "0"));

        dbUtils.updateZoneRainParamAndClearCache("degradation_state", zoneRainParamsGDState, rain_mode, zone_id.toString());
        dbUtils.updateZoneRainParamAndClearCache("last_mile", zoneRainParamsLM, rain_mode, zone_id.toString());
        dbUtils.updateZoneRainParamAndClearCache("zone_max_sla", zoneRainParamsSLA, rain_mode, zone_id.toString());
        serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(restaurantRainMode), "id", String.valueOf(zone_id));
        if(customerRainMode != Integer.parseInt(NO_RAIN))
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(customerRainMode), "id", String.valueOf(custZoneId));

        Assert.assertEquals(mappedresponse.get("rain_mode"), RAIN_MODE_HEAVY, "rain mode");
        Assert.assertEquals(getResponseDegradationMode(mappedresponse), DEGRADATION_MODE_HEAVY, "degradation mode");
        Assert.assertEquals(getResponseDegradationCause(mappedresponse), "rain", "degradation cause");
        Assert.assertEquals(mappedresponse.get("serviceability"), "2", "serviceability");
        Assert.assertEquals(distance.toString(), mappedresponse.get("last_mile_travel"));
        if(zone_id != Integer.parseInt(custZoneId) && custZoneId != null)
            Assert.assertEquals(mappedresponse.get("customer_zone_raining"), "false", "customer zone raining");
        else if(zone_id == Integer.parseInt(custZoneId))
            Assert.assertEquals(mappedresponse.get("customer_zone_raining"), "true", "customer zone raining");
    }


    @Test(description = "SLTC_21 :- Verify restaurant is serviceable when the current sla is less than the max sla of 'rain_mode:4' defined at zone_rain_params of restaurant zone")
    public void rainsAtRestaurantMode4Gd2_LtSLA()
    {
        Integer restaurantRainMode = getRainMode(String.valueOf(zone_id));
        setRainModeAndCause(restaurantRainMode, 4, zone_id);
        Double degradationState = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select degradation_state from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("degradation_state").toString());
        if(degradationState != 2)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", "2", "4", String.valueOf(zone_id));

        Integer restZRMmaxSla = Integer.parseInt(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("4", String.valueOf(zone_id)).get("zone_max_sla").toString());
        if(restZRMmaxSla < 100)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "200", "4", String.valueOf(zone_id));
        Double zoneRainParamsLastMile = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select last_mile from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("last_mile").toString());
        Double[] latlonarray = deliveryDataHelper.getLatLng(restLat, restLon, 90.0, (BigDecimal.valueOf(zoneRainParamsLastMile/1.3).divide(BigDecimal.valueOf(2))).doubleValue());

        String custZone = getCustomerZoneFromLatLon(latlonarray[0], latlonarray[1]);
        Integer customerRainMode = 0;
        if(zone_id != Integer.parseInt(custZone) && custZone != null)
        {
            customerRainMode = getRainMode(String.valueOf(custZone));
            if(customerRainMode != 3)
                serviceablility_Helper.setNoRainsAtZone(String.valueOf(custZone));
        }
        Map<String,String> mappedResponse = serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {latlonarray[0].toString(), latlonarray[1].toString(), city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode", "1");
            put("serviceability", "2");
            put("long_distance", "0");
        }});
        String degradationMode = getResponseDegradationMode(mappedResponse);
        serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(restaurantRainMode), "id", String.valueOf(zone_id));
        if(customerRainMode != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(customerRainMode), "id", String.valueOf(custZone));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", String.valueOf(restZRMmaxSla), "4", String.valueOf(zone_id));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", String.valueOf(degradationState), "4", String.valueOf(zone_id));

        Assert.assertEquals(mappedResponse.get("rain_mode"), "1", "rain mode");
        Assert.assertEquals(degradationMode, "2", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceability"), "2", "serviceability");
        if(zone_id != Integer.parseInt(custZone) && custZone != null)
            Assert.assertEquals(mappedResponse.get("customer_zone_raining"), "false", "customer zone raining");
        else if(zone_id == Integer.parseInt(custZone))
            Assert.assertEquals(mappedResponse.get("customer_zone_raining"), "true", "customer zone raining");
    }


    @Test(description = "SLTC_22 :- Verify restaurant is not serviceable when the current sla is greater than the max sla of 'rain_mode:4' defined at zone_rain_params of restaurant zone")
    public void rainsAtRestaurantMode4Gd2_GtSLA()
    {
        Integer restaurantRainMode = getRainMode(String.valueOf(zone_id));
        setRainModeAndCause(restaurantRainMode, 4, zone_id);
        Double degradationState = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select degradation_state from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("degradation_state").toString());
        if(degradationState != 2)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", "2", "4", String.valueOf(zone_id));

        Double zoneRainParamsLastMile = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select last_mile from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("last_mile").toString());
        Double zoneRainParamsMaxSLA = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select zone_max_sla from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("zone_max_sla").toString());
        Double[] latlonarray = deliveryDataHelper.getLatLng(restLat, restLon, 90.0, (BigDecimal.valueOf(zoneRainParamsLastMile/1.3).divide(BigDecimal.valueOf(2))).doubleValue());

        String custZone = getCustomerZoneFromLatLon(latlonarray[0], latlonarray[1]);
        Integer customerRainMode = 0;
        if(zone_id != Integer.parseInt(custZone) && custZone != null)
        {
            customerRainMode = getRainMode(String.valueOf(custZone));
            if(customerRainMode != 3)
                serviceablility_Helper.setNoRainsAtZone(String.valueOf(custZone));
        }
        Map<String,String> mappedResponse = serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {latlonarray[0].toString(), latlonarray[1].toString(), city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode", "1");
            put("serviceability", "1");
            put("non_serviceable_reason", "1");
            put("long_distance", "0");
        }});
        String degradationMode = getResponseDegradationMode(mappedResponse);
        Double sla = Double.parseDouble(mappedResponse.get("delivery_time"));
        if(sla <= zoneRainParamsMaxSLA)
        {
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", String.valueOf(sla - 10), "4", String.valueOf(zone_id));
            mappedResponse = serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                    {latlonarray[0].toString(), latlonarray[1].toString(), city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
                put("rain_mode", "1");
                put("serviceability", "1");
                put("non_serviceable_reason", "1");
                put("long_distance", "0");
            }});
            try {
                degradationMode =  (new JSONObject(mappedResponse.get("degradation"))).get("mode").toString();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", String.valueOf(zoneRainParamsMaxSLA), "4", String.valueOf(zone_id));
        }
        serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(restaurantRainMode), "id", String.valueOf(zone_id));
        if(customerRainMode != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(customerRainMode), "id", String.valueOf(custZone));

        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", String.valueOf(degradationState), "4", String.valueOf(zone_id));

        Assert.assertEquals(mappedResponse.get("rain_mode"), "1", "rain mode");
        Assert.assertEquals(degradationMode, "2", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceability"), "1", "serviceability");
        Assert.assertEquals(mappedResponse.get("non_serviceable_reason"), "1", "non serviceable reason");
        if(zone_id != Integer.parseInt(custZone) && custZone != null)
            Assert.assertEquals(mappedResponse.get("customer_zone_raining"), "false", "customer zone raining");
        else if(zone_id == Integer.parseInt(custZone))
            Assert.assertEquals(mappedResponse.get("customer_zone_raining"), "true", "customer zone raining");
    }


    @Test(description = "SVC_PE_107 :- Verify restaurant is serviceable when the current SLA is equals to the max SLA of 'rain_mode:4', 'degradation_state:2' & 'cause:rain' defined at zone_rain_params of restaurant zone")
    public void rainsAtRestaurantMode4GD2_EqSLA()
    {
        rain_mode = RAIN_MODE_DEGRADATION;
        String custZoneId = getCustomerZoneFromLatLon(defaultCustomerLat, defaultCustomerLon);
        Integer restaurantRainMode = getRainMode(String.valueOf(zone_id));
        setRainModeAndCause(restaurantRainMode, Integer.parseInt(rain_mode), zone_id);

        Integer customerRainMode = getRainMode(String.valueOf(custZoneId));
        if(zone_id != Integer.parseInt(custZoneId) && customerRainMode != Integer.parseInt(NO_RAIN))
            serviceablility_Helper.setNoRainsAtZone(String.valueOf(custZoneId));

        String  response = serviceablility_Helper.listing(new String[]{defaultCustomerLat.toString(), defaultCustomerLon.toString(),city_id.toString(), restaurantId}).ResponseValidator.GetBodyAsText();
        //Map<String, String> mappedresponse=getServiceabiltyResponseMap(response);
        String slaInResponse=getServiceabiltyResponseMap(response).get("delivery_time");

        String zoneRainParamsGDState= dbUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("degradation_state").toString();
        dbUtils.updateZoneRainParamAndClearCache("degradation_state", DEGRADATION_MODE_HEAVY, rain_mode, zone_id.toString());

        String zoneRainParamsSLA= dbUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("zone_max_sla").toString();
        dbUtils.updateZoneRainParamAndClearCache("zone_max_sla", slaInResponse, rain_mode, zone_id.toString());
        Logger.getLogger(ServiceabilityTestNew.class.getName()).info("MAX SLA AT ZONE RAIN PARAMS : " + dbUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("zone_max_sla"));

        Map<String,String> mappedresponse = serviceablility_Helper.retryCerebroListingUsingVarargs(10, 1000,
                new String[]{defaultCustomerLat.toString(), defaultCustomerLon.toString(), city_id.toString(), restaurantId},
                returnMap("rain_mode", RAIN_MODE_LIGHT), returnMap("serviceability", "2"), returnMap("long_distance", "0"));

        dbUtils.updateZoneRainParamAndClearCache("zone_max_sla", zoneRainParamsSLA, rain_mode, zone_id.toString());
        dbUtils.updateZoneRainParamAndClearCache("degradation_state", zoneRainParamsGDState, rain_mode, zone_id.toString());
        serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(restaurantRainMode), "id", String.valueOf(zone_id));
        if(customerRainMode != Integer.parseInt(NO_RAIN))
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(customerRainMode), "id", String.valueOf(custZoneId));

        Assert.assertEquals(mappedresponse.get("rain_mode"), RAIN_MODE_HEAVY, "rain mode");
        Assert.assertEquals(getResponseDegradationMode(mappedresponse), DEGRADATION_MODE_HEAVY, "degradation mode");
        Assert.assertEquals(getResponseDegradationCause(mappedresponse), "rain", "degradation cause");
        Assert.assertEquals(mappedresponse.get("serviceability"), "2", "serviceability");
        Assert.assertEquals(slaInResponse, mappedresponse.get("delivery_time"));
        if(zone_id != Integer.parseInt(custZoneId) && custZoneId != null)
            Assert.assertEquals(mappedresponse.get("customer_zone_raining"), "false", "customer zone raining");
        else if(zone_id == Integer.parseInt(custZoneId))
            Assert.assertEquals(mappedresponse.get("customer_zone_raining"), "true", "customer zone raining");
    }


    @Test(description = "SLTC_23 :- Verify restaurant is serviceable when the distance from customer to restaurant is less than the max last mile of 'rain_mode:4' defined at zone_rain_params of restaurant zone")
    public void rainsAtRestaurantMode4Gd3_LtLastMile()
    {
        Integer restaurantRainMode = getRainMode(String.valueOf(zone_id));
        setRainModeAndCause(restaurantRainMode, 4, zone_id);

        Double degradationState = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select degradation_state from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("degradation_state").toString());
        if(degradationState != 3)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", "3", "4", String.valueOf(zone_id));

        Integer restZRMmaxSla = Integer.parseInt(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("4", String.valueOf(zone_id)).get("zone_max_sla").toString());
        if(restZRMmaxSla < 100)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "200", "4", String.valueOf(zone_id));
        Double zoneRainParamsLastMile = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select last_mile from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("last_mile").toString());
        Double[] latlonarray = deliveryDataHelper.getLatLng(restLat, restLon, 90.0, (BigDecimal.valueOf(zoneRainParamsLastMile/1.3).divide(BigDecimal.valueOf(2))).doubleValue());

        String custZone = getCustomerZoneFromLatLon(latlonarray[0], latlonarray[1]);
        Integer customerRainMode = 0;
        if(zone_id != Integer.parseInt(custZone) && custZone != null)
        {
            customerRainMode = getRainMode(String.valueOf(custZone));
            if(customerRainMode != 3)
                serviceablility_Helper.setNoRainsAtZone(String.valueOf(custZone));
        }

        Map<String,String> mappedResponse = serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {latlonarray[0].toString(), latlonarray[1].toString(), city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode", "5");
            put("serviceability", "2");
            put("long_distance", "0");
        }});

        String degradationMode = getResponseDegradationMode(mappedResponse);
        serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(restaurantRainMode), "id", String.valueOf(zone_id));
        if(customerRainMode != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(customerRainMode), "id", String.valueOf(custZone));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", String.valueOf(restZRMmaxSla), "4", String.valueOf(zone_id));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", String.valueOf(degradationState), "4", String.valueOf(zone_id));

        Assert.assertEquals(mappedResponse.get("rain_mode"), "5", "rain mode");
        Assert.assertEquals(degradationMode, "3", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceability"), "2", "serviceability");
        if(zone_id != Integer.parseInt(custZone) && custZone != null)
            Assert.assertEquals(mappedResponse.get("customer_zone_raining"), "false", "customer zone raining");
        else if(zone_id == Integer.parseInt(custZone))
            Assert.assertEquals(mappedResponse.get("customer_zone_raining"), "true", "customer zone raining");
    }


    @Test(description = "SLTC_24 :- Verify restaurant is not serviceable when the distance from customer to restaurant is greater than the max last mile of 'rain_mode:4' defined at zone_rain_params of restaurant zone")
    public void rainsAtRestaurantMode4Gd3_GtLastMile()
    {
        Integer restaurantRainMode = getRainMode(String.valueOf(zone_id));
        setRainModeAndCause(restaurantRainMode, 4, zone_id);
        Double degradationState = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select degradation_state from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("degradation_state").toString());
        String id = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select id from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("id").toString();
        if(degradationState != 3)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", "3", "4", String.valueOf(zone_id));

        Double zoneRainParamsLastMile = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select last_mile from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("last_mile").toString());
        if(zoneRainParamsLastMile > 1)
        {
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", "1", "4", String.valueOf(zone_id));
            zoneRainParamsLastMile = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select last_mile from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("last_mile").toString());
        }
        Integer restZRMmaxSla = Integer.parseInt(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("4", String.valueOf(zone_id)).get("zone_max_sla").toString());
        if(restZRMmaxSla < 100)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "200", "4", String.valueOf(zone_id));

        Double[] latlonarray = deliveryDataHelper.getLatLng(restLat, restLon, 90.0, (BigDecimal.valueOf(zoneRainParamsLastMile/1.3).add(BigDecimal.valueOf(1))).doubleValue());
        String custZone = getCustomerZoneFromLatLon(latlonarray[0], latlonarray[1]);
        Integer customerRainMode = 0;
        if(zone_id != Integer.parseInt(custZone) && custZone != null)
        {
            customerRainMode = getRainMode(custZone);
            if(customerRainMode != 3)
                serviceablility_Helper.setNoRainsAtZone(custZone);
        }

        Map<String,String> mappedResponse = serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {latlonarray[0].toString(), latlonarray[1].toString(), city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode", "5");
            put("serviceability", "1");
            put("non_serviceable_reason", "0");
            put("long_distance", "0");
        }});

        String degradationMode = getResponseDegradationMode(mappedResponse);
        serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(restaurantRainMode), "id", String.valueOf(zone_id));
        if(customerRainMode != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(customerRainMode), "id", String.valueOf(custZone));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", String.valueOf(restZRMmaxSla), "4", String.valueOf(zone_id));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", String.valueOf(degradationState), "4", String.valueOf(zone_id));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", String.valueOf(zoneRainParamsLastMile), "4", String.valueOf(zone_id));

        Assert.assertEquals(mappedResponse.get("rain_mode"), "5", "rain mode");
        Assert.assertEquals(degradationMode, "3", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceability"), "1", "serviceability");
        Assert.assertEquals(mappedResponse.get("non_serviceable_reason"), "0", "non serviceable reason");
        if(zone_id != Integer.parseInt(custZone) && custZone != null)
            Assert.assertEquals(mappedResponse.get("customer_zone_raining"), "false", "customer zone raining");
        else if(zone_id == Integer.parseInt(custZone))
            Assert.assertEquals(mappedResponse.get("customer_zone_raining"), "true", "customer zone raining");
    }


    @Test(description = "SVC_PE_116 :- Verify restaurant is serviceable when the current last mile is equals to the max last_mile of 'rain_mode:4', 'degradarion_state:3' & 'cause:rain' defined at zone_rain_params of restaurant zone")
    public void rainsAtRestaurantMode4GD3_EqLM()
    {
        rain_mode = RAIN_MODE_DEGRADATION;
        Double distance  = Double.parseDouble(String.valueOf(deliveryDataHelper.getDistanceBtwnTwoLatLongs(restLat, restLon, defaultCustomerLat, defaultCustomerLon)))/1000;
        String custZoneId = getCustomerZoneFromLatLon(defaultCustomerLat, defaultCustomerLon);
        Integer restaurantRainMode = getRainMode(String.valueOf(zone_id));
        setRainModeAndCause(restaurantRainMode, Integer.parseInt(rain_mode), zone_id);

        String zoneRainParamsGDState= dbUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("degradation_state").toString();
        dbUtils.updateZoneRainParamAndClearCache("degradation_state", DEGRADATION_MODE_EXTREME, rain_mode, zone_id.toString());
        String zoneRainParamsLM= dbUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("last_mile").toString();
        dbUtils.updateZoneRainParamAndClearCache("last_mile", distance.toString(), rain_mode, zone_id.toString());
        String zoneRainParamsSLA= dbUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("zone_max_sla").toString();
        dbUtils.updateZoneRainParamAndClearCache("zone_max_sla", "200", rain_mode, zone_id.toString());
        Logger.getLogger(ServiceabilityTestNew.class.getName()).info("MAX LAST MILE AT ZONE RAIN PARAMS : " + dbUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("last_mile"));

        Integer customerRainMode = getRainMode(String.valueOf(custZoneId));
        if(zone_id != Integer.parseInt(custZoneId) && customerRainMode != 3)
            serviceablility_Helper.setNoRainsAtZone(String.valueOf(custZoneId));

        Map<String,String> mappedresponse = serviceablility_Helper.retryCerebroListingUsingVarargs(10, 1000,
                new String[]{defaultCustomerLat.toString(), defaultCustomerLon.toString(), city_id.toString(), restaurantId},
                returnMap("rain_mode", RAIN_MODE_EXTREME), returnMap("serviceability", "2"), returnMap("long_distance", "0"));

        dbUtils.updateZoneRainParamAndClearCache("degradation_state", zoneRainParamsGDState, rain_mode, zone_id.toString());
        dbUtils.updateZoneRainParamAndClearCache("last_mile", zoneRainParamsLM, rain_mode, zone_id.toString());
        dbUtils.updateZoneRainParamAndClearCache("zone_max_sla", zoneRainParamsSLA, rain_mode, zone_id.toString());
        serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(restaurantRainMode), "id", String.valueOf(zone_id));
        if(customerRainMode != Integer.parseInt(NO_RAIN))
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(customerRainMode), "id", String.valueOf(custZoneId));

        Assert.assertEquals(mappedresponse.get("rain_mode"), RAIN_MODE_EXTREME, "rain mode");
        Assert.assertEquals(getResponseDegradationMode(mappedresponse), DEGRADATION_MODE_EXTREME, "degradation mode");
        Assert.assertEquals(getResponseDegradationCause(mappedresponse), "rain", "degradation cause");
        Assert.assertEquals(mappedresponse.get("serviceability"), "2", "serviceability");
        Assert.assertEquals(distance.toString(), mappedresponse.get("last_mile_travel"));
        if(zone_id != Integer.parseInt(custZoneId) && custZoneId != null)
            Assert.assertEquals(mappedresponse.get("customer_zone_raining"), "false", "customer zone raining");
        else if(zone_id == Integer.parseInt(custZoneId))
            Assert.assertEquals(mappedresponse.get("customer_zone_raining"), "true", "customer zone raining");
    }


    @Test(description = "SLTC_25 :- Verify restaurant is serviceable when the current sla is less than the max sla of 'rain_mode:4' defined at zone_rain_params of restaurant zone")
    public void rainsAtRestaurantMode4Gd3_LtSLA()
    {
        Integer restaurantRainMode = getRainMode(String.valueOf(zone_id));
        setRainModeAndCause(restaurantRainMode, 4, zone_id);
        Double degradationState = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select degradation_state from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("degradation_state").toString());
        if(degradationState != 3)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", "3", "4", String.valueOf(zone_id));

        Double zoneRainParamsLastMile = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select last_mile from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("last_mile").toString());
        Double[] latlonarray = deliveryDataHelper.getLatLng(restLat, restLon, 90.0, (BigDecimal.valueOf(zoneRainParamsLastMile/1.3).divide(BigDecimal.valueOf(2))).doubleValue());

        Integer restZRMmaxSla = Integer.parseInt(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("4", String.valueOf(zone_id)).get("zone_max_sla").toString());
        if(restZRMmaxSla < 100)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "200", "4", String.valueOf(zone_id));
        String custZone = getCustomerZoneFromLatLon(latlonarray[0], latlonarray[1]);
        Integer customerRainMode = 0;
        if(zone_id != Integer.parseInt(custZone) && custZone != null)
        {
            customerRainMode = getRainMode(String.valueOf(custZone));
            if(customerRainMode != 3)
                serviceablility_Helper.setNoRainsAtZone(String.valueOf(custZone));
        }
        Map<String,String> mappedResponse = serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {latlonarray[0].toString(), latlonarray[1].toString(), city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode", "5");
            put("serviceability", "2");
            put("long_distance", "0");
        }});

        String degradationMode = getResponseDegradationMode(mappedResponse);
        serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(restaurantRainMode), "id", String.valueOf(zone_id));
        if(customerRainMode != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(customerRainMode), "id", String.valueOf(custZone));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", String.valueOf(restZRMmaxSla), "4", String.valueOf(zone_id));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", String.valueOf(degradationState), "4", String.valueOf(zone_id));

        Assert.assertEquals(mappedResponse.get("rain_mode"), "5", "rain mode");
        Assert.assertEquals(degradationMode, "3", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceability"), "2", "serviceability");
        if(zone_id != Integer.parseInt(custZone) && custZone != null)
            Assert.assertEquals(mappedResponse.get("customer_zone_raining"), "false", "customer zone raining");
        else if(zone_id == Integer.parseInt(custZone))
            Assert.assertEquals(mappedResponse.get("customer_zone_raining"), "true", "customer zone raining");
    }


    @Test(description = "SLTC_22 :- Verify restaurant is not serviceable when the current sla is greater than the max sla of 'rain_mode:4' defined at zone_rain_params of restaurant zone")
    public void rainsAtRestaurantMode4Gd3_GtSLA()
    {
        Integer restaurantRainMode = getRainMode(String.valueOf(zone_id));
        setRainModeAndCause(restaurantRainMode, 4, zone_id);
        Double degradationState = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select degradation_state from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("degradation_state").toString());
        if(degradationState != 3)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", "3", "4", String.valueOf(zone_id));

        Double zoneRainParamsLastMile = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select last_mile from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("last_mile").toString());
        Double zoneRainParamsMaxSLA = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select zone_max_sla from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("zone_max_sla").toString());
        if(zoneRainParamsMaxSLA > 10)
        {
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "10", "4", String.valueOf(zone_id));
            zoneRainParamsMaxSLA = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select zone_max_sla from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("zone_max_sla").toString());
        }
        Double[] latlonarray = deliveryDataHelper.getLatLng(restLat, restLon, 90.0, (BigDecimal.valueOf(zoneRainParamsLastMile/1.3).divide(BigDecimal.valueOf(2))).doubleValue());

        String custZone = getCustomerZoneFromLatLon(latlonarray[0], latlonarray[1]);
        Integer customerRainMode = 0;
        if(zone_id != Integer.parseInt(custZone) && custZone != null)
        {
            customerRainMode = getRainMode(String.valueOf(custZone));
            if(customerRainMode != 3)
                serviceablility_Helper.setNoRainsAtZone(String.valueOf(custZone));
        }
        Map<String, String> mappedResponse = serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {latlonarray[0].toString(), latlonarray[1].toString(), city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode", "5");
            put("serviceability", "1");
            put("non_serviceable_reason", "1");
            put("long_distance", "0");
        }});
        String degradationMode =  getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", String.valueOf(zoneRainParamsMaxSLA), "4", String.valueOf(zone_id));

        serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(restaurantRainMode), "id", String.valueOf(zone_id));
        if(customerRainMode != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(customerRainMode), "id", String.valueOf(custZone));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", String.valueOf(degradationState), "4", String.valueOf(zone_id));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", String.valueOf(zoneRainParamsMaxSLA), "4", String.valueOf(zone_id));

        Assert.assertEquals(mappedResponse.get("rain_mode"), "5", "rain mode");
        Assert.assertEquals(degradationMode, "3", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceability"), "1", "serviceability");
        Assert.assertEquals(mappedResponse.get("non_serviceable_reason"), "1", "non serviceable reason");
        if(zone_id != Integer.parseInt(custZone) && custZone != null)
            Assert.assertEquals(mappedResponse.get("customer_zone_raining"), "false", "customer zone raining");
        else if(zone_id == Integer.parseInt(custZone))
            Assert.assertEquals(mappedResponse.get("customer_zone_raining"), "true", "customer zone raining");
    }


    @Test(description = "SVC_PE_119 :- Verify restaurant is serviceable when the current SLA is equals to the max SLA of 'rain_mode:4', 'degradation_state:2' & 'cause:rain' defined at zone_rain_params of restaurant zone")
    public void rainsAtRestaurantMode4GD3_EqSLA()
    {
        rain_mode = RAIN_MODE_DEGRADATION;
        String custZoneId = getCustomerZoneFromLatLon(defaultCustomerLat, defaultCustomerLon);
        Integer restaurantRainMode = getRainMode(String.valueOf(zone_id));
        setRainModeAndCause(restaurantRainMode, Integer.parseInt(rain_mode), zone_id);

        Integer customerRainMode = getRainMode(String.valueOf(custZoneId));
        if(zone_id != Integer.parseInt(custZoneId) && customerRainMode != Integer.parseInt(NO_RAIN))
            serviceablility_Helper.setNoRainsAtZone(String.valueOf(custZoneId));

        String  response = serviceablility_Helper.listing(new String[]{defaultCustomerLat.toString(), defaultCustomerLon.toString(),city_id.toString(), restaurantId}).ResponseValidator.GetBodyAsText();
        //Map<String, String> mappedresponse=getServiceabiltyResponseMap(response);
        String slaInResponse=getServiceabiltyResponseMap(response).get("delivery_time");

        String zoneRainParamsGDState= dbUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("degradation_state").toString();
        dbUtils.updateZoneRainParamAndClearCache("degradation_state", DEGRADATION_MODE_EXTREME, rain_mode, zone_id.toString());

        String zoneRainParamsSLA= dbUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("zone_max_sla").toString();
        dbUtils.updateZoneRainParamAndClearCache("zone_max_sla", slaInResponse, rain_mode, zone_id.toString());
        Logger.getLogger(ServiceabilityTestNew.class.getName()).info("MAX SLA AT ZONE RAIN PARAMS : " + dbUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("zone_max_sla"));

        Map<String,String> mappedresponse = serviceablility_Helper.retryCerebroListingUsingVarargs(10, 1000,
                new String[]{defaultCustomerLat.toString(), defaultCustomerLon.toString(), city_id.toString(), restaurantId},
                returnMap("rain_mode", RAIN_MODE_EXTREME), returnMap("serviceability", "2"), returnMap("long_distance", "0"));

        dbUtils.updateZoneRainParamAndClearCache("zone_max_sla", zoneRainParamsSLA, rain_mode, zone_id.toString());
        dbUtils.updateZoneRainParamAndClearCache("degradation_state", zoneRainParamsGDState, rain_mode, zone_id.toString());
        serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(restaurantRainMode), "id", String.valueOf(zone_id));
        if(customerRainMode != Integer.parseInt(NO_RAIN))
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(customerRainMode), "id", String.valueOf(custZoneId));

        Assert.assertEquals(mappedresponse.get("rain_mode"), RAIN_MODE_EXTREME, "rain mode");
        Assert.assertEquals(getResponseDegradationMode(mappedresponse), DEGRADATION_MODE_EXTREME, "degradation mode");
        Assert.assertEquals(getResponseDegradationCause(mappedresponse), "rain", "degradation cause");
        Assert.assertEquals(mappedresponse.get("serviceability"), "2", "serviceability");
        Assert.assertEquals(slaInResponse, mappedresponse.get("delivery_time"));
        if(zone_id != Integer.parseInt(custZoneId) && custZoneId != null)
            Assert.assertEquals(mappedresponse.get("customer_zone_raining"), "false", "customer zone raining");
        else if(zone_id == Integer.parseInt(custZoneId))
            Assert.assertEquals(mappedresponse.get("customer_zone_raining"), "true", "customer zone raining");
    }


    @Test(groups= "long_distance", description = "SLTC_67 :- Verify Restaurant is serviceable due to long distance max last mile, if distance from customer to restaurant is less than the long distance max last mile")
    public void a1_LDnonLDlessThanLDlmLtLM()
    {
        String[] coordinates = LdCoordinates.get(0).split(",");
        int distance = deliveryDataHelper.getDistance(lDrestLat, lDrestLon, Double.parseDouble(coordinates[0]), Double.parseDouble(coordinates[1]))/1000;
        String lmAtArea = serviceablility_Helper.getValueFromDb("last_mile_cap", "area", "id", String.valueOf(area_id));
        String lmAtLD = serviceablility_Helper.getValueFromDb("max_last_mile", "long_distance", "polygon_id", String.valueOf(lDPolygonId));
        updateLmAtAreaRestaurantAndLD(String.valueOf(distance*1.3-2), String.valueOf(distance*1.3+2));
        String ldMaxSla = DeliveryDataBaseUtils.getLDmaxSla(longDistanceId);
        updateLDmaxSla("200", longDistanceId);

        String restZoneId = getCustomerZoneFromLatLon(lDrestLat, lDrestLon);
        String custZoneId = getCustomerZoneFromLatLon(Double.parseDouble(coordinates[0]), Double.parseDouble(coordinates[1]));
        Map<String, Integer> rainModeMap = setNoRainsAtRestaurantAndCustomer(restZoneId, custZoneId);
        Map<String,String> mappedResponse = serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {coordinates[0], coordinates[1], city_id.toString(), lDrestaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode", "0");
            put("serviceability", "2");
            put("long_distance", "1");
        }});
        if(rainModeMap.get("restaurantRainMode") != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(rainModeMap.get("restaurantRainMode")), "id", restZoneId);
        if(custZoneId != null && rainModeMap.get("customerRainMode") != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(rainModeMap.get("customerRainMode")), "id", custZoneId);

        updateLmAtAreaRestaurantAndLD(lmAtArea, lmAtLD);
        updateLDmaxSla(ldMaxSla, longDistanceId);
        Assert.assertEquals(mappedResponse.get("long_distance"),"1", "long distance");
        Assert.assertEquals(mappedResponse.get("serviceability"),"2", "serviceability");
    }


    @Test(groups= "long_distance", description = "SLTC_68 :- Verify Restaurant is not serviceable due to long distance max last mile, if distance from customer to restaurant is greater than the long distance max last mile")
    public void a2_LDnonLDlessThanLDlm_GtLM()
    {
        String lmAtArea = serviceablility_Helper.getValueFromDb("last_mile_cap", "area", "id", String.valueOf(area_id));
        String lmAtLD = serviceablility_Helper.getValueFromDb("max_last_mile", "long_distance", "polygon_id", String.valueOf(lDPolygonId));
        String[] coordinates = LdCoordinates.get(0).split(",");
        int distance = deliveryDataHelper.getDistance(lDrestLat, lDrestLon, Double.parseDouble(coordinates[0]), Double.parseDouble(coordinates[1]))/1000;

        updateLmAtAreaRestaurantAndLD(String.valueOf(distance*1.3-4), String.valueOf(distance));

        String restZoneId = getCustomerZoneFromLatLon(lDrestLat, lDrestLon);
        String custZoneId = getCustomerZoneFromLatLon(Double.parseDouble(coordinates[0]), Double.parseDouble(coordinates[1]));
        Map<String, Integer> rainModeMap = setNoRainsAtRestaurantAndCustomer(restZoneId, custZoneId);
        Map<String,String> mappedResponse = serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {coordinates[0], coordinates[1], city_id.toString(), lDrestaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode", "0");
            put("serviceability", "0");
            put("non_serviceable_reason", "0");
            put("long_distance", "1");
        }});
        if(rainModeMap.get("restaurantRainMode") != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(rainModeMap.get("restaurantRainMode")), "id", restZoneId);
        if(custZoneId != null && rainModeMap.get("customerRainMode") != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(rainModeMap.get("customerRainMode")), "id", custZoneId);

        updateLmAtAreaRestaurantAndLD(lmAtArea, lmAtLD);

        Assert.assertEquals(mappedResponse.get("long_distance"),"1", "long distance");
        Assert.assertEquals(mappedResponse.get("serviceability"),"0", "serviceability");
        Assert.assertEquals(mappedResponse.get("non_serviceable_reason"),"0", "serviceability");
    }


    @Test(groups= "long_distance", description = "SLTC_69 :- Verify Restaurant is serviceable due to non LD max last mile, if distance from customer to restaurant is less than the non LD max last mile")
    public void a3_LDnonLDgreaterThanLDlmLtLM()
    {
        String lmAtArea = serviceablility_Helper.getValueFromDb("last_mile_cap", "area", "id", String.valueOf(area_id));
        String lmAtLD = serviceablility_Helper.getValueFromDb("max_last_mile", "long_distance", "polygon_id", String.valueOf(lDPolygonId));
        String[] coordinates = LdCoordinates.get(0).split(",");
        int distance = deliveryDataHelper.getDistance(lDrestLat, lDrestLon, Double.parseDouble(coordinates[0]), Double.parseDouble(coordinates[1]))/1000;
        updateLmAtAreaRestaurantAndLD(String.valueOf(distance*1.3+2), String.valueOf(distance*1.3 - 2));

        String restZoneId = getCustomerZoneFromLatLon(lDrestLat, lDrestLon);
        String custZoneId = getCustomerZoneFromLatLon(Double.parseDouble(coordinates[0]), Double.parseDouble(coordinates[1]));
        Map<String, Integer> rainModeMap = setNoRainsAtRestaurantAndCustomer(restZoneId, custZoneId);
        Map<String,String> mappedResponse = serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {coordinates[0], coordinates[1], city_id.toString(), lDrestaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode", "0");
            put("serviceability", "2");
            put("long_distance", "0");
        }});
        if(rainModeMap.get("restaurantRainMode") != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(rainModeMap.get("restaurantRainMode")), "id", restZoneId);
        if(custZoneId != null && rainModeMap.get("customerRainMode") != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(rainModeMap.get("customerRainMode")), "id", custZoneId);
        updateLmAtAreaRestaurantAndLD(lmAtArea, lmAtLD);
        Assert.assertEquals(mappedResponse.get("long_distance"),"0", "long distance");
        Assert.assertEquals(mappedResponse.get("serviceability"),"2", "serviceability");
    }


    @Test(groups= "long_distance", description = "SLTC_70 :- Verify Restaurant is not serviceable due to long distance max last mile, if distance from customer to restaurant is greater than the non LD max last mile")
    public void a4_LDnonLDgreaterThanLDlmGtLM()
    {
        String lmAtArea = serviceablility_Helper.getValueFromDb("last_mile_cap", "area", "id", String.valueOf(area_id));
        String lmAtLD = serviceablility_Helper.getValueFromDb("max_last_mile", "long_distance", "polygon_id", String.valueOf(lDPolygonId));
        String[] coordinates = LdCoordinates.get(0).split(",");
        int distance = deliveryDataHelper.getDistance(lDrestLat, lDrestLon, Double.parseDouble(coordinates[0]), Double.parseDouble(coordinates[1]))/1000;
        updateLmAtAreaRestaurantAndLD(String.valueOf(distance*1.3-2), String.valueOf(distance*1.3 - 4));

        String restZoneId = getCustomerZoneFromLatLon(lDrestLat, lDrestLon);
        String custZoneId = getCustomerZoneFromLatLon(Double.parseDouble(coordinates[0]), Double.parseDouble(coordinates[1]));
        Map<String, Integer> rainModeMap = setNoRainsAtRestaurantAndCustomer(restZoneId, custZoneId);

        Map<String,String> mappedResponse = serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {coordinates[0], coordinates[1], city_id.toString(), lDrestaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode", "0");
            put("serviceability", "0");
            put("non_serviceable_reason", "0");
            put("long_distance", "1");
        }});
        if(rainModeMap.get("restaurantRainMode") != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(rainModeMap.get("restaurantRainMode")), "id", restZoneId);
        if(custZoneId != null && rainModeMap.get("customerRainMode") != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(rainModeMap.get("customerRainMode")), "id", custZoneId);
        updateLmAtAreaRestaurantAndLD(lmAtArea, lmAtLD);
        Assert.assertEquals(mappedResponse.get("long_distance"),"1", "long distance");
        Assert.assertEquals(mappedResponse.get("serviceability"),"0", "serviceability");
        Assert.assertEquals(mappedResponse.get("non_serviceable_reason"),"0", "serviceability");
    }


    @Test(groups= "long_distance", description = "SLTC_71 :- Verify Restaurant is serviceable due to non LD max last mile, if distance from customer to restaurant is less than the non LD max last mile")
    public void a5_LDnonLDequalToLDlmLtLM()
    {
        String lmAtArea = serviceablility_Helper.getValueFromDb("last_mile_cap", "area", "id", String.valueOf(area_id));
        String lmAtLD = serviceablility_Helper.getValueFromDb("max_last_mile", "long_distance", "polygon_id", String.valueOf(lDPolygonId));
        String[] coordinates = LdCoordinates.get(0).split(",");
        int distance = deliveryDataHelper.getDistance(lDrestLat, lDrestLon, Double.parseDouble(coordinates[0]), Double.parseDouble(coordinates[1]))/1000;
        updateLmAtAreaRestaurantAndLD(String.valueOf(distance*1.3 + 2), String.valueOf(distance*1.3 + 2));

        String restZoneId = getCustomerZoneFromLatLon(lDrestLat, lDrestLon);
        String custZoneId = getCustomerZoneFromLatLon(Double.parseDouble(coordinates[0]), Double.parseDouble(coordinates[1]));
        Map<String, Integer> rainModeMap = setNoRainsAtRestaurantAndCustomer(restZoneId, custZoneId);
        Map<String,String> mappedResponse = serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {coordinates[0], coordinates[1], city_id.toString(), lDrestaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode", "0");
            put("serviceability", "2");
            put("long_distance", "0");
        }});
        if(rainModeMap.get("restaurantRainMode") != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(rainModeMap.get("restaurantRainMode")), "id", restZoneId);
        if(custZoneId != null && rainModeMap.get("customerRainMode") != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(rainModeMap.get("customerRainMode")), "id", custZoneId);
        updateLmAtAreaRestaurantAndLD(lmAtArea, lmAtLD);
        Assert.assertEquals(mappedResponse.get("long_distance"),"0", "long distance");
        Assert.assertEquals(mappedResponse.get("serviceability"),"2", "serviceability");
    }


    @Test(groups= "long_distance", description = "SLTC_72 :- Verify Restaurant is not serviceable due to long distance max last mile, if distance from customer to restaurant is greater than the non LD max last mile")
    public void a6_LDnonLDequalToLDlmGtLM()
    {
        String lmAtArea = serviceablility_Helper.getValueFromDb("last_mile_cap", "area", "id", String.valueOf(area_id));
        String lmAtLD = serviceablility_Helper.getValueFromDb("max_last_mile", "long_distance", "polygon_id", String.valueOf(lDPolygonId));
        String[] coordinates = LdCoordinates.get(0).split(",");
        int distance = deliveryDataHelper.getDistance(lDrestLat, lDrestLon, Double.parseDouble(coordinates[0]), Double.parseDouble(coordinates[1]))/1000;
        updateLmAtAreaRestaurantAndLD(String.valueOf(distance - 2), String.valueOf(distance - 2));

        String restZoneId = getCustomerZoneFromLatLon(lDrestLat, lDrestLon);
        String custZoneId = getCustomerZoneFromLatLon(Double.parseDouble(coordinates[0]), Double.parseDouble(coordinates[1]));
        Map<String, Integer> rainModeMap = setNoRainsAtRestaurantAndCustomer(restZoneId, custZoneId);
        Map<String,String> mappedResponse = serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {coordinates[0], coordinates[1], city_id.toString(), lDrestaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode", "0");
            put("serviceability", "0");
            put("non_serviceable_reason", "0");
            put("long_distance", "1");
        }});
        if(rainModeMap.get("restaurantRainMode") != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(rainModeMap.get("restaurantRainMode")), "id", restZoneId);
        if(custZoneId != null && rainModeMap.get("customerRainMode") != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(rainModeMap.get("customerRainMode")), "id", custZoneId);
        updateLmAtAreaRestaurantAndLD(lmAtArea, lmAtLD);
        Assert.assertEquals(mappedResponse.get("long_distance"),"1", "long distance");
        Assert.assertEquals(mappedResponse.get("serviceability"),"0", "serviceability");
        Assert.assertEquals(mappedResponse.get("non_serviceable_reason"),"0", "non serviceability reason");
    }


    @Test(groups= "long_distance", description = "SLTC_73 :- Verify Restaurant is serviceable due to long distance max sla, if current sla is less than the long distance max sla")
    public void a7_LDnonLDlessThanLDLm_ltSLA()
    {
        String lmAtArea = serviceablility_Helper.getValueFromDb("last_mile_cap", "area", "id", String.valueOf(area_id));
        String lmAtLD = serviceablility_Helper.getValueFromDb("max_last_mile", "long_distance", "polygon_id", String.valueOf(lDPolygonId));
        String[] coordinates = LdCoordinates.get(0).split(",");
        int distance = deliveryDataHelper.getDistance(lDrestLat, lDrestLon, Double.parseDouble(coordinates[0]), Double.parseDouble(coordinates[1]))/1000;
        updateLmAtAreaRestaurantAndLD(String.valueOf(distance*1.3-2), String.valueOf(distance*1.3+2));
        String restZoneId = getCustomerZoneFromLatLon(lDrestLat, lDrestLon);
        String custZoneId = getCustomerZoneFromLatLon(Double.parseDouble(coordinates[0]), Double.parseDouble(coordinates[1]));
        Map<String, Integer> rainModeMap = setNoRainsAtRestaurantAndCustomer(restZoneId, custZoneId);
        Map<String,String> mappedResponse = serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {coordinates[0], coordinates[1], city_id.toString(), lDrestaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode", "0");
            put("serviceability", "2");
            put("long_distance", "1");
        }});
        if(rainModeMap.get("restaurantRainMode") != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(rainModeMap.get("restaurantRainMode")), "id", restZoneId);
        if(custZoneId != null && rainModeMap.get("customerRainMode") != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(rainModeMap.get("customerRainMode")), "id", custZoneId);
        updateLmAtAreaRestaurantAndLD(lmAtArea, lmAtLD);
        Assert.assertEquals(mappedResponse.get("long_distance"),"1", "long distance");
        Assert.assertEquals(mappedResponse.get("serviceability"),"2", "serviceability");
    }


    @Test(groups= "long_distance", description = "SLTC_74 :- Verify Restaurant is not serviceable due to long distance max sla, if current sla is greater than the long distance max sla")
    public void a8_LDnonLDlessThanLDLm_GtSLA()
    {
        String lmAtArea = serviceablility_Helper.getValueFromDb("last_mile_cap", "area", "id", String.valueOf(area_id));
        String lmAtLD = serviceablility_Helper.getValueFromDb("max_last_mile", "long_distance", "polygon_id", String.valueOf(lDPolygonId));
        String LDmaxSla = serviceablility_Helper.getValueFromDb("max_sla", "long_distance", "polygon_id", String.valueOf(lDPolygonId));
        String[] coordinates = LdCoordinates.get(0).split(",");
        int distance = deliveryDataHelper.getDistance(lDrestLat, lDrestLon, Double.parseDouble(coordinates[0]), Double.parseDouble(coordinates[1]))/1000;
        updateLmAtAreaRestaurantAndLD(String.valueOf(distance*1.3-2), String.valueOf(distance*1.3+2));
        updateLDmaxSla("20", String.valueOf(longDistanceId));

        String restZoneId = getCustomerZoneFromLatLon(lDrestLat, lDrestLon);
        String custZoneId = getCustomerZoneFromLatLon(Double.parseDouble(coordinates[0]), Double.parseDouble(coordinates[1]));
        Map<String, Integer> rainModeMap = setNoRainsAtRestaurantAndCustomer(restZoneId, custZoneId);
        Map<String,String> mappedResponse = serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {coordinates[0], coordinates[1], city_id.toString(), lDrestaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode", "0");
            put("serviceability", "0");
            put("non_serviceable_reason", "1");
            put("long_distance", "1");
        }});
        if(rainModeMap.get("restaurantRainMode") != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(rainModeMap.get("restaurantRainMode")), "id", restZoneId);
        if(custZoneId != null && rainModeMap.get("customerRainMode") != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(rainModeMap.get("customerRainMode")), "id", custZoneId);
        updateLmAtAreaRestaurantAndLD(lmAtArea, lmAtLD);
        updateLDmaxSla(LDmaxSla, String.valueOf(longDistanceId));

        Assert.assertEquals(mappedResponse.get("long_distance"),"1", "long distance");
        Assert.assertEquals(mappedResponse.get("serviceability"),"0", "serviceability");
        Assert.assertEquals(mappedResponse.get("non_serviceable_reason"),"1", "non serviceability reason");
    }


    @Test(groups= "long_distance", description = "SLTC_75 :- Verify Restaurant is serviceable due to non LD max sla, if current sla is less than the non LD max sla")
    public void a9_LDnonLDgreaterThanLDLm_ltSLA()
    {
        String lmAtArea = serviceablility_Helper.getValueFromDb("last_mile_cap", "area", "id", String.valueOf(area_id));
        String lmAtLD = serviceablility_Helper.getValueFromDb("max_last_mile", "long_distance", "polygon_id", String.valueOf(lDPolygonId));
        String[] coordinates = LdCoordinates.get(0).split(",");
        int distance = deliveryDataHelper.getDistance(lDrestLat, lDrestLon, Double.parseDouble(coordinates[0]), Double.parseDouble(coordinates[1]))/1000;
        updateLmAtAreaRestaurantAndLD(String.valueOf(distance*1.3+2), String.valueOf(distance - 2));

        String restZoneId = getCustomerZoneFromLatLon(lDrestLat, lDrestLon);
        String custZoneId = getCustomerZoneFromLatLon(Double.parseDouble(coordinates[0]), Double.parseDouble(coordinates[1]));
        Map<String, Integer> rainModeMap = setNoRainsAtRestaurantAndCustomer(restZoneId, custZoneId);
        Map<String,String> mappedResponse = serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {coordinates[0], coordinates[1], city_id.toString(), lDrestaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode", "0");
            put("serviceability", "2");
            put("long_distance", "0");
        }});
        if(rainModeMap.get("restaurantRainMode") != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(rainModeMap.get("restaurantRainMode")), "id", restZoneId);
        if(custZoneId != null && rainModeMap.get("customerRainMode") != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(rainModeMap.get("customerRainMode")), "id", custZoneId);
        updateLmAtAreaRestaurantAndLD(lmAtArea, lmAtLD);
        Assert.assertEquals(mappedResponse.get("long_distance"),"0", "long distance");
        Assert.assertEquals(mappedResponse.get("serviceability"),"2", "serviceability");
    }


    @Test(groups= "long_distance", description = "SLTC_76 :- Verify Restaurant is not serviceable due to non LD max sla, if current sla is greater than the non LD max sla")
    public void a10_LDnonLDgreaterThanLDLm_GtSLA()
    {
        String lmAtArea = serviceablility_Helper.getValueFromDb("last_mile_cap", "area", "id", String.valueOf(area_id));
        String lmAtLD = serviceablility_Helper.getValueFromDb("max_last_mile", "long_distance", "polygon_id", String.valueOf(lDPolygonId));
        String zoneMaxSla = serviceablility_Helper.getValueFromDb("max_delivery_time", "zone", "id", String.valueOf(zone_id));
        String[] coordinates = LdCoordinates.get(0).split(",");
        int distance = deliveryDataHelper.getDistance(lDrestLat, lDrestLon, Double.parseDouble(coordinates[0]), Double.parseDouble(coordinates[1]))/1000;
        updateLmAtAreaRestaurantAndLD(String.valueOf(distance*1.3+2), String.valueOf(distance - 2));
        updateZoneMaxSla("20", String.valueOf(zone_id));

        String restZoneId = getCustomerZoneFromLatLon(lDrestLat, lDrestLon);
        String custZoneId = getCustomerZoneFromLatLon(Double.parseDouble(coordinates[0]), Double.parseDouble(coordinates[1]));
        Map<String, Integer> rainModeMap = setNoRainsAtRestaurantAndCustomer(restZoneId, custZoneId);
        Map<String,String> mappedResponse = serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {coordinates[0], coordinates[1], city_id.toString(), lDrestaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode", "0");
            put("serviceability", "0");
            put("non_serviceable_reason", "1");
            put("long_distance", "0");
        }});
        if(rainModeMap.get("restaurantRainMode") != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(rainModeMap.get("restaurantRainMode")), "id", restZoneId);
        if(custZoneId != null && rainModeMap.get("customerRainMode") != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(rainModeMap.get("customerRainMode")), "id", custZoneId);
        updateLmAtAreaRestaurantAndLD(lmAtArea, lmAtLD);
        updateZoneMaxSla(zoneMaxSla, String.valueOf(zone_id));

        Assert.assertEquals(mappedResponse.get("long_distance"),"0", "long distance");
        Assert.assertEquals(mappedResponse.get("serviceability"),"0", "serviceability");
        Assert.assertEquals(mappedResponse.get("non_serviceable_reason"),"1", "non serviceability reason");
    }


    @Test(groups= "long_distance", description = "SLTC_77 :- Verify Restaurant is serviceable due to non LD, if current sla is less than the non LD max sla")
    public void a11_LDnonLDequalToLDlm_LtLmGtSlaAtNonLd()
    {
        String lmAtArea = serviceablility_Helper.getValueFromDb("last_mile_cap", "area", "id", String.valueOf(area_id));
        String lmAtLD = serviceablility_Helper.getValueFromDb("max_last_mile", "long_distance", "polygon_id", String.valueOf(lDPolygonId));
        String zoneMaxSla = serviceablility_Helper.getValueFromDb("max_delivery_time", "zone", "id", String.valueOf(zone_id));
        String[] coordinates = LdCoordinates.get(0).split(",");
        int distance = deliveryDataHelper.getDistance(lDrestLat, lDrestLon, Double.parseDouble(coordinates[0]), Double.parseDouble(coordinates[1]))/1000;
        updateLmAtAreaRestaurantAndLD(String.valueOf(distance*1.3 + 2), String.valueOf(distance*1.3 + 2));
        updateZoneMaxSla("200", String.valueOf(zone_id));

        String restZoneId = getCustomerZoneFromLatLon(lDrestLat, lDrestLon);
        String custZoneId = getCustomerZoneFromLatLon(Double.parseDouble(coordinates[0]), Double.parseDouble(coordinates[1]));
        Map<String, Integer> rainModeMap = setNoRainsAtRestaurantAndCustomer(restZoneId, custZoneId);
        Map<String,String> mappedResponse = serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {coordinates[0], coordinates[1], city_id.toString(), lDrestaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode", "0");
            put("serviceability", "2");
            put("long_distance", "0");
        }});
        if(rainModeMap.get("restaurantRainMode") != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(rainModeMap.get("restaurantRainMode")), "id", restZoneId);
        if(custZoneId != null && rainModeMap.get("customerRainMode") != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(rainModeMap.get("customerRainMode")), "id", custZoneId);
        updateLmAtAreaRestaurantAndLD(lmAtArea, lmAtLD);
        updateZoneMaxSla(zoneMaxSla, String.valueOf(zone_id));
        Assert.assertEquals(mappedResponse.get("long_distance"),"0", "long distance");
        Assert.assertEquals(mappedResponse.get("serviceability"),"2", "serviceability");
    }


    @Test(groups= "long_distance", description = "SLTC_78 :- Verify Restaurant is not serviceable due to non LD, if current sla is greater than the non LD max sla")
    public void a12_LDnonLDequalToLDlm_LtLmLtSlaAtNonLd()
    {
        String lmAtArea = serviceablility_Helper.getValueFromDb("last_mile_cap", "area", "id", String.valueOf(area_id));
        String lmAtLD = serviceablility_Helper.getValueFromDb("max_last_mile", "long_distance", "polygon_id", String.valueOf(lDPolygonId));
        String zoneMaxSla = serviceablility_Helper.getValueFromDb("max_delivery_time", "zone", "id", String.valueOf(zone_id));
        String[] coordinates = LdCoordinates.get(0).split(",");
        int distance = deliveryDataHelper.getDistance(lDrestLat, lDrestLon, Double.parseDouble(coordinates[0]), Double.parseDouble(coordinates[1]))/1000;
        updateLmAtAreaRestaurantAndLD(String.valueOf(distance*1.3 + 2), String.valueOf(distance*1.3 + 2));
        updateZoneMaxSla("20", String.valueOf(zone_id));

        String restZoneId = getCustomerZoneFromLatLon(lDrestLat, lDrestLon);
        String custZoneId = getCustomerZoneFromLatLon(Double.parseDouble(coordinates[0]), Double.parseDouble(coordinates[1]));
        Map<String, Integer> rainModeMap = setNoRainsAtRestaurantAndCustomer(restZoneId, custZoneId);
        Map<String,String> mappedResponse = serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {coordinates[0], coordinates[1], city_id.toString(), lDrestaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode", "0");
            put("serviceability", "0");
            put("non_serviceable_reason", "1");
            put("long_distance", "0");
        }});
        if(rainModeMap.get("restaurantRainMode") != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(rainModeMap.get("restaurantRainMode")), "id", restZoneId);
        if(custZoneId != null && rainModeMap.get("customerRainMode") != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(rainModeMap.get("customerRainMode")), "id", custZoneId);
        updateLmAtAreaRestaurantAndLD(lmAtArea, lmAtLD);
        updateZoneMaxSla(zoneMaxSla, String.valueOf(zone_id));
        Assert.assertEquals(mappedResponse.get("long_distance"),"0", "long distance");
        Assert.assertEquals(mappedResponse.get("serviceability"),"0", "serviceability");
        Assert.assertEquals(mappedResponse.get("non_serviceable_reason"),"1", "non serviceability reason");
    }



    @Test(groups= "long_distance", description = "SLTC_79 :- Verify Restaurant is not serviceable due to long distance last mile, if current sla is less than the long distance max sla")
    public void a13_LDnonLDequalToLDlm_LtLMLtSlaAtLD()
    {
        String lmAtArea = serviceablility_Helper.getValueFromDb("last_mile_cap", "area", "id", String.valueOf(area_id));
        String lmAtLD = serviceablility_Helper.getValueFromDb("max_last_mile", "long_distance", "polygon_id", String.valueOf(lDPolygonId));
        String LDmaxSla = serviceablility_Helper.getValueFromDb("max_sla", "long_distance", "polygon_id", String.valueOf(lDPolygonId));
        String[] coordinates = LdCoordinates.get(0).split(",");
        int distance = deliveryDataHelper.getDistance(lDrestLat, lDrestLon, Double.parseDouble(coordinates[0]), Double.parseDouble(coordinates[1]))/1000;
        updateLmAtAreaRestaurantAndLD(String.valueOf(distance - 2), String.valueOf(distance - 2));
        updateLDmaxSla("200", String.valueOf(longDistanceId));

        String restZoneId = getCustomerZoneFromLatLon(lDrestLat, lDrestLon);
        String custZoneId = getCustomerZoneFromLatLon(Double.parseDouble(coordinates[0]), Double.parseDouble(coordinates[1]));
        Map<String, Integer> rainModeMap = setNoRainsAtRestaurantAndCustomer(restZoneId, custZoneId);
        Map<String,String> mappedResponse = serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {coordinates[0], coordinates[1], city_id.toString(), lDrestaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode", "0");
            put("serviceability", "0");
            put("non_serviceable_reason", "0");
            put("long_distance", "1");
        }});
        if(rainModeMap.get("restaurantRainMode") != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(rainModeMap.get("restaurantRainMode")), "id", restZoneId);
        if(custZoneId != null && rainModeMap.get("customerRainMode") != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(rainModeMap.get("customerRainMode")), "id", custZoneId);
        updateLmAtAreaRestaurantAndLD(lmAtArea, lmAtLD);
        updateLDmaxSla(LDmaxSla, String.valueOf(longDistanceId));
        Assert.assertEquals(mappedResponse.get("long_distance"),"1", "long distance");
        Assert.assertEquals(mappedResponse.get("serviceability"),"0", "serviceability");
        Assert.assertEquals(mappedResponse.get("non_serviceable_reason"),"0", "non serviceability reason");
    }

    ///
    @Test(groups= "long_distance", description = "SLTC_80 :- Verify Restaurant is not serviceable due to long distance sla, if current sla is greater than the long distance max sla")
    public void a14_LDnonLDequalToLDlm_LtLMGtSlaAtLD()
    {
        String lmAtArea = serviceablility_Helper.getValueFromDb("last_mile_cap", "area", "id", String.valueOf(area_id));
        String lmAtLD = serviceablility_Helper.getValueFromDb("max_last_mile", "long_distance", "polygon_id", String.valueOf(lDPolygonId));
        String LDmaxSla = serviceablility_Helper.getValueFromDb("max_sla", "long_distance", "polygon_id", String.valueOf(lDPolygonId));
        String[] coordinates = LdCoordinates.get(0).split(",");
        int distance = deliveryDataHelper.getDistance(lDrestLat, lDrestLon, Double.parseDouble(coordinates[0]), Double.parseDouble(coordinates[1]))/1000;

        updateLmAtAreaRestaurantAndLD(String.valueOf(distance - 2), String.valueOf(distance - 2));
        updateLDmaxSla("200", String.valueOf(longDistanceId));

        String zoneMaxSla = DeliveryDataBaseUtils.getZoneMaxSla(zone_id.toString());
        DeliveryDataBaseUtils.setZoneMaxSla("200", zone_id.toString());

        String restZoneId = getCustomerZoneFromLatLon(lDrestLat, lDrestLon);
        String custZoneId = getCustomerZoneFromLatLon(Double.parseDouble(coordinates[0]), Double.parseDouble(coordinates[1]));
        Map<String, Integer> rainModeMap = setNoRainsAtRestaurantAndCustomer(restZoneId, custZoneId);
        Map<String,String> mappedResponse = serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {coordinates[0], coordinates[1], city_id.toString(), lDrestaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode", "0");
            put("serviceability", "0");
            put("non_serviceable_reason", "0");
            put("long_distance", "1");
        }});
        if(rainModeMap.get("restaurantRainMode") != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(rainModeMap.get("restaurantRainMode")), "id", restZoneId);
        if(custZoneId != null && rainModeMap.get("customerRainMode") != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(rainModeMap.get("customerRainMode")), "id", custZoneId);
        updateLmAtAreaRestaurantAndLD(lmAtArea, lmAtLD);
        updateLDmaxSla(LDmaxSla, String.valueOf(longDistanceId));
        DeliveryDataBaseUtils.setZoneMaxSla(zoneMaxSla, zone_id.toString());
        Assert.assertEquals(mappedResponse.get("long_distance"),"1", "long distance");
        Assert.assertEquals(mappedResponse.get("serviceability"),"0", "serviceability");
        Assert.assertEquals(mappedResponse.get("non_serviceable_reason"),"1", "non serviceability reason");
    }


    @Test(groups= "long_distance", description = "SLTC_81 :- Verify restaurant is not serviceable, even though the last mile is less than the long distance max last mile, if Restaurant is not long distance enabled and customer lies in long distance polygon.")
    public void a15_LDldDisabledRestaurantServiceability()
    {
        String lmAtArea = serviceablility_Helper.getValueFromDb("last_mile_cap", "area", "id", String.valueOf(area_id));
        String lmAtLD = serviceablility_Helper.getValueFromDb("max_last_mile", "long_distance", "polygon_id", String.valueOf(lDPolygonId));
        String LDmaxSla = serviceablility_Helper.getValueFromDb("max_sla", "long_distance", "polygon_id", String.valueOf(lDPolygonId));
        //Map<String, String> customerDetails = deliveryDataHelper.latLonNotInGivenTagsButInZone(restLat, restLon, (BigDecimal.valueOf(actualLastMile/1.3).subtract(BigDecimal.valueOf(1))).doubleValue(), "LONG_DISTANCE");
        Map<String, String> ldDisabledRestaurant = solrHelper.getLdDisabledRestaurantInfo(area_id.toString(), city_id.toString());
        Double distance = Double.parseDouble(String.valueOf(deliveryDataHelper.getDistance(defaultCustomerLat, defaultCustomerLon, Double.parseDouble(ldDisabledRestaurant.get("ldDisabledrestLat")), Double.parseDouble(ldDisabledRestaurant.get("ldDisabledrestLong")))))/1000;

        updateLmAtAreaRestaurantAndLD(String.valueOf(distance - 2), String.valueOf(distance - 2));
        updateLDmaxSla("10", String.valueOf(longDistanceId));

        String zoneMaxSla = DeliveryDataBaseUtils.getZoneMaxSla(zone_id.toString());
        DeliveryDataBaseUtils.setZoneMaxSla("50", zone_id.toString());

        String restZoneId = getCustomerZoneFromLatLon(Double.parseDouble(ldDisabledRestaurant.get("ldDisabledrestLat")), Double.parseDouble(ldDisabledRestaurant.get("ldDisabledrestLong")));
        String custZoneId = getCustomerZoneFromLatLon(defaultCustomerLat, defaultCustomerLon);
        Map<String, Integer> rainModeMap = setNoRainsAtRestaurantAndCustomer(restZoneId, custZoneId);

        Map<String,String> mappedResponse = serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {defaultCustomerLat.toString(), defaultCustomerLon.toString(), city_id.toString(), ldDisabledRestaurant.get("ldDisabledrestaurantId")}, new LinkedHashMap<String,String>(){{
            put("rain_mode", "0");
            put("serviceability", "0");
            put("non_serviceable_reason", "0");
            put("long_distance", "0");
        }});
        if(rainModeMap.get("restaurantRainMode") != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(rainModeMap.get("restaurantRainMode")), "id", restZoneId);
        if(custZoneId != null && rainModeMap.get("customerRainMode") != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(rainModeMap.get("customerRainMode")), "id", custZoneId);
        updateLmAtAreaRestaurantAndLD(lmAtArea, lmAtLD);
        updateLDmaxSla(LDmaxSla, String.valueOf(longDistanceId));
        DeliveryDataBaseUtils.setZoneMaxSla(zoneMaxSla, zone_id.toString());
        Assert.assertEquals(mappedResponse.get("long_distance"),"0", "long distance");
        Assert.assertEquals(mappedResponse.get("serviceability"),"0", "serviceability");
        Assert.assertEquals(mappedResponse.get("non_serviceable_reason"),"0", "non serviceability reason");
    }


    @Test(groups= "long_distance", description = "SLTC_84 :- Verify restaurant is not serviceable, even though the distance between customer and restaurant is less than the long distance max last mile if customer does not lie in long distance polygon.")
    public void a18_LDCustomerNotLyingInLDpolygonServiceability()
    {
        String lmAtArea = serviceablility_Helper.getValueFromDb("last_mile_cap", "area", "id", String.valueOf(area_id));
        String lmAtLD = serviceablility_Helper.getValueFromDb("max_last_mile", "long_distance", "polygon_id", String.valueOf(lDPolygonId));
        String zoneMaxSla = serviceablility_Helper.getValueFromDb("max_delivery_time", "zone", "id", String.valueOf(zone_id));
        Map<String, String> customerDetails = deliveryDataHelper.latLonNotInGivenTagsButInZone(restLat, restLon, (BigDecimal.valueOf(actualLastMile/1.3).subtract(BigDecimal.valueOf(1))).doubleValue(), "LONG_DISTANCE");
        int distance = deliveryDataHelper.getDistance(lDrestLat, lDrestLon, Double.parseDouble(customerDetails.get("lat")), Double.parseDouble(customerDetails.get("lon")))/1000;
        updateLmAtAreaRestaurantAndLD(String.valueOf(distance*1.3 + 2), String.valueOf(distance*1.3 + 2));
        updateZoneMaxSla("200", String.valueOf(zone_id));

        String restZoneId = getCustomerZoneFromLatLon(lDrestLat, lDrestLon);
        String custZoneId = getCustomerZoneFromLatLon(Double.parseDouble(customerDetails.get("lat")), Double.parseDouble(customerDetails.get("lon")));
        Map<String, Integer> rainModeMap = setNoRainsAtRestaurantAndCustomer(restZoneId, custZoneId);
        Map<String,String> mappedResponse = serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {customerDetails.get("lat"), customerDetails.get("lon"), city_id.toString(), lDrestaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode", "0");
            put("serviceability", "2");
            put("long_distance", "0");
        }});
        if(rainModeMap.get("restaurantRainMode") != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(rainModeMap.get("restaurantRainMode")), "id", restZoneId);
        if(custZoneId != null && rainModeMap.get("customerRainMode") != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(rainModeMap.get("customerRainMode")), "id", custZoneId);
        updateLmAtAreaRestaurantAndLD(lmAtArea, lmAtLD);
        updateZoneMaxSla(zoneMaxSla, String.valueOf(zone_id));
        Assert.assertEquals(mappedResponse.get("long_distance"),"0", "long distance");
        Assert.assertEquals(mappedResponse.get("serviceability"),"2", "serviceability");
    }




    //long distance auto disable => only in FSM (GD).. / key `long_distance_auto_disable` is false in old flow..
	/*@Test(groups= "long_distance", description = "SLTC_82 :- Verify restaurant is not serviceable, even though the last mile is less than the long distance max last mile, if Restaurant is not long distance enabled and customer lies in long distance polygon.")
	public void ldNotApplicableAtRainMode()
	{
		double lmAtArea = Double.parseDouble(serviceablility_Helper.getValueFromDb("last_mile_cap", "area", "id", String.valueOf(area_id)));
		String lmAtLD = serviceablility_Helper.getValueFromDb("max_last_mile", "long_distance", "polygon_id", String.valueOf(lDPolygonId));
		String[] coordinates = LdCoordinates.get(0).split(",");
		int distance = deliveryDataHelper.getDistance(lDrestLat, lDrestLon, Double.parseDouble(coordinates[0]), Double.parseDouble(coordinates[1]))/1000;
		updateLmAtAreaRestaurantAndLD(String.valueOf(distance*1.3 - 2), String.valueOf(distance*1.3 + 2));
		*//**//*serviceablility_Helper.setValueInDb("long_distance", "max_last_mile", String.valueOf(distance*1.3+2), "polygon_id", String.valueOf(lDPolygonId));
		serviceablility_Helper.clearCache("long_distance", longDistanceId);*//**//*

		String restZoneId = getCustomerZoneFromLatLon(lDrestLat, lDrestLon);
		Integer restRainMode = getRainMode(String.valueOf(zone_id));
		if(restRainMode == 3)
			serviceablility_Helper.setValueInDb("zone", "rain_mode_type", "4", "id", restZoneId);
		Map<String,String> mappedResponse = serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
				{coordinates[0], coordinates[1], city_id.toString(), lDrestaurantId}, new LinkedHashMap<String,String>(){{
					put("rain_mode", "0");
					put("serviceability", "0");
					put("non_serviceable_reason", "0");
					put("long_distance", "0");
				}});
		/*Map<String,String> mappedresponse= serviceablility_Helper.retryCerebroListing(5, 1000, "0", new String[]{coordinates[0], coordinates[1], city_id.toString(), lDrestaurantId}, "long_distance");*//*

		serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(restRainMode), "id", restZoneId);
		if(Double.parseDouble(mappedResponse.get("last_mile_travel")) < LDLastMile(String.valueOf(lDPolygonId)) && Double.parseDouble(mappedResponse.get("last_mile_travel")) > Math.min(areaLastMile(String.valueOf(area_id)), restaurantLastMile(lDrestaurantId)))
		{
			updateLmAtAreaRestaurantAndLD(String.valueOf(lmAtArea), lmAtLD);
			Assert.assertEquals(mappedResponse.get("long_distance"),"0", "long distance");
		}
		else
		{
			updateLmAtAreaRestaurantAndLD(String.valueOf(lmAtArea), lmAtLD);
			Assert.fail();
		}
	}*/


    @Test(description = "SLTC_27 :- Verify restaurant is serviceable when the distance from customer to restaurant is less than the max last mile (min of max last mile at restaurant's area and restaurant)")
    public void rainsAtCustomerRainMode1ltLM()
    {
        double custZoneMaxSla = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("1", defaultCustomerZone).get("zone_max_sla").toString());
        String restZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(zone_id.toString());
        if(!restZoneRainMode.equals("3"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache("3", zone_id.toString());
        String custZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(defaultCustomerZone);
        if(!custZoneRainMode.equals("1"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache("1", defaultCustomerZone);
        if(custZoneMaxSla < 200)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "200", "1", defaultCustomerZone);
        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroListing(10,1000,new String[]
                {defaultCustomerLat.toString(),defaultCustomerLon.toString(),city_id.toString(),restaurantId},new LinkedHashMap<String,String>(){{
            put("rain_mode","1");
            put("serviceability","2");
            put("long_distance","0");
        }});
        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(restZoneRainMode, zone_id.toString());
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(custZoneRainMode, defaultCustomerZone);
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla",String.valueOf(custZoneMaxSla),"1",defaultCustomerZone);

        Assert.assertEquals(mappedResponse.get("rain_mode"), "1");
        Assert.assertEquals(degradationMode, "2");
        Assert.assertEquals(mappedResponse.get("serviceability"), "2");
        Assert.assertEquals(mappedResponse.get("customer_zone_raining"), "true", "customer zone raining");
    }


    @Test(description = "SLTC_28 :- Verify restaurant is not serviceable when the distance from customer to restaurant is greater than the max last mile")
    public void rainsAtCustomerRainMode1gtLM()
    {
        Map<String, String> customerDetails = deliveryDataHelper.latLonNotInGivenTagsButInZone(restLat, restLon, (BigDecimal.valueOf(actualLastMile/1.3).subtract(BigDecimal.valueOf(1))).doubleValue(), "LONG_DISTANCE");
        int distance = deliveryDataHelper.getDistance(restLat, restLon, Double.parseDouble(customerDetails.get("lat")), Double.parseDouble(customerDetails.get("lon")))/1000;
        double custZoneMaxSla = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("1", customerDetails.get("zone_id")).get("zone_max_sla").toString());
        double custZoneLM = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("1", customerDetails.get("zone_id")).get("last_mile").toString());
        String restZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(zone_id.toString());
        double areaLm = Double.parseDouble(DeliveryDataBaseUtils.getMaxLastMileForArea(String.valueOf(area_id)));
        double restaurantLm = Double.parseDouble(DeliveryDataBaseUtils.getMaxLastMileForRestaurant(restaurantId));
        if(!restZoneRainMode.equals("3"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache("3", zone_id.toString());
        String custZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(customerDetails.get("zone_id"));
        if(!custZoneRainMode.equals("1"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache("1", customerDetails.get("zone_id"));
        if(custZoneMaxSla < 200)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla","200","1",customerDetails.get("zone_id"));
        if(custZoneLM >= distance)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", String.valueOf(distance - 2), "1", customerDetails.get("zone_id"));
        if(areaLm < distance || restaurantLm < distance)
        {
            DeliveryDataBaseUtils.setMaxLastMileForAreaAndClearCache(String.valueOf(area_id), String.valueOf(distance * 1.3 + 2));
            DeliveryDataBaseUtils.setMaxLastMileForRestaurantAndClearCache(String.valueOf(restaurantId), String.valueOf(distance * 1.3 + 2));
        }

        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {customerDetails.get("lat"), customerDetails.get("lon"), city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode","1");
            put("serviceability","1");
            put("non_serviceable_reason","0");
            put("long_distance","0");
        }});
        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.setMaxLastMileForAreaAndClearCache(String.valueOf(area_id), String.valueOf(areaLm));
        DeliveryDataBaseUtils.setMaxLastMileForRestaurantAndClearCache(String.valueOf(restaurantId), String.valueOf(restaurantLm));
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(restZoneRainMode, zone_id.toString());
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(custZoneRainMode, customerDetails.get("zone_id"));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla",String.valueOf(custZoneMaxSla),"1",customerDetails.get("zone_id"));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile",String.valueOf(custZoneLM),"1",customerDetails.get("zone_id"));

        Assert.assertEquals(mappedResponse.get("rain_mode"), "1", "rain mode");
        Assert.assertEquals(degradationMode, "2", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceability"), "1", "serviceability");
        Assert.assertEquals(mappedResponse.get("non_serviceable_reason"), "0", "non serviceable reason");
        Assert.assertEquals(mappedResponse.get("customer_zone_raining"), "true", "customer zone raining");

    }


    @Test(description = "SLTC_29 :- Verify restaurant is serviceable when the current sla is less than the max sla (max_delivery_time at restaurant's zone)")
    public void rainsAtCustomerRainMode1ltSLA()
    {
        Map<String, String> customerDetails = deliveryDataHelper.latLonNotInGivenTagsButInZone(restLat, restLon, (BigDecimal.valueOf(actualLastMile/1.3).subtract(BigDecimal.valueOf(1))).doubleValue(), "LONG_DISTANCE");
        int distance = deliveryDataHelper.getDistance(restLat, restLon, Double.parseDouble(customerDetails.get("lat")), Double.parseDouble(customerDetails.get("lon")))/1000;
        double custZoneMaxSla = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("1", customerDetails.get("zone_id")).get("zone_max_sla").toString());
        double custZoneLM = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("1", customerDetails.get("zone_id")).get("last_mile").toString());
        double custZoneMaxLM = custZoneLM;
        double areaLm = Double.parseDouble(DeliveryDataBaseUtils.getMaxLastMileForArea(String.valueOf(area_id)));
        double restaurantLm = Double.parseDouble(DeliveryDataBaseUtils.getMaxLastMileForRestaurant(restaurantId));

        String restZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(zone_id.toString());
        if(!restZoneRainMode.equals("3"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache("3", zone_id.toString());
        String custZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(customerDetails.get("zone_id"));
        if(!custZoneRainMode.equals("1"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache("1", customerDetails.get("zone_id"));
        if(custZoneMaxSla < 200)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla","200","1",customerDetails.get("zone_id"));
        if(custZoneLM <= distance) {
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", String.valueOf(distance + 2), "1", customerDetails.get("zone_id"));
            custZoneMaxLM = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("1", customerDetails.get("zone_id")).get("last_mile").toString());
        }
        if(areaLm < distance || restaurantLm < distance)
        {
            DeliveryDataBaseUtils.setMaxLastMileForAreaAndClearCache(String.valueOf(area_id), String.valueOf(distance * 1.3 + 2));
            DeliveryDataBaseUtils.setMaxLastMileForRestaurantAndClearCache(String.valueOf(restaurantId), String.valueOf(distance * 1.3 + 2));
        }

        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {customerDetails.get("lat"), customerDetails.get("lon"), city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode","1");
            put("serviceability","2");
            put("long_distance","0");
        }});
        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.setMaxLastMileForAreaAndClearCache(String.valueOf(area_id), String.valueOf(areaLm));
        DeliveryDataBaseUtils.setMaxLastMileForRestaurantAndClearCache(String.valueOf(restaurantId), String.valueOf(restaurantLm));
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(restZoneRainMode, zone_id.toString());
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(custZoneRainMode, customerDetails.get("zone_id"));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla",String.valueOf(custZoneMaxSla),"1",customerDetails.get("zone_id"));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile",String.valueOf(custZoneLM),"1",customerDetails.get("zone_id"));

        Assert.assertEquals(mappedResponse.get("rain_mode"), "1", "rain mode");
        Assert.assertEquals(degradationMode, "2", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceability"), "2", "serviceability");
        Assert.assertEquals(mappedResponse.get("customer_zone_raining"), "true", "customer zone raining");
    }


    @Test(description = "SLTC_30 :- Verify restaurant is not serviceable when the current sla is greater than the max sla")
    public void rainsAtCustomerRainMode1gtSLA()
    {
        Map<String, String> customerDetails = deliveryDataHelper.latLonNotInGivenTagsButInZone(restLat, restLon, (BigDecimal.valueOf(actualLastMile/1.3).subtract(BigDecimal.valueOf(1))).doubleValue(), "LONG_DISTANCE");
        int distance = deliveryDataHelper.getDistance(restLat, restLon, Double.parseDouble(customerDetails.get("lat")), Double.parseDouble(customerDetails.get("lon")))/1000;
        double custZoneMaxSla = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("1", customerDetails.get("zone_id")).get("zone_max_sla").toString());
        double custZoneLM = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("1", customerDetails.get("zone_id")).get("last_mile").toString());
        String restZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(zone_id.toString());
        double areaLm = Double.parseDouble(DeliveryDataBaseUtils.getMaxLastMileForArea(String.valueOf(area_id)));
        double restaurantLm = Double.parseDouble(DeliveryDataBaseUtils.getMaxLastMileForRestaurant(restaurantId));
        if(!restZoneRainMode.equals("3"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache("3", zone_id.toString());
        String custZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(customerDetails.get("zone_id"));
        if(!custZoneRainMode.equals("1"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache("1", customerDetails.get("zone_id"));
        if(custZoneMaxSla > 10)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "10", "1", customerDetails.get("zone_id"));
        if(custZoneLM >= distance)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", String.valueOf(distance + 2), "1", customerDetails.get("zone_id"));

        if(areaLm < distance || restaurantLm < distance)
        {
            DeliveryDataBaseUtils.setMaxLastMileForAreaAndClearCache(String.valueOf(area_id), String.valueOf(distance * 1.3 + 2));
            DeliveryDataBaseUtils.setMaxLastMileForRestaurantAndClearCache(String.valueOf(restaurantId), String.valueOf(distance * 1.3 + 2));
        }

        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {customerDetails.get("lat"), customerDetails.get("lon"), city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode", "1");
            put("serviceability", "1");
            put("non_serviceable_reason", "1");
            put("long_distance", "0");
        }});
        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.setMaxLastMileForAreaAndClearCache(String.valueOf(area_id), String.valueOf(areaLm));
        DeliveryDataBaseUtils.setMaxLastMileForRestaurantAndClearCache(String.valueOf(restaurantId), String.valueOf(restaurantLm));
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(restZoneRainMode, zone_id.toString());
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(custZoneRainMode, customerDetails.get("zone_id"));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla",String.valueOf(custZoneMaxSla),"1",customerDetails.get("zone_id"));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile",String.valueOf(custZoneLM),"1",customerDetails.get("zone_id"));

        Assert.assertEquals(mappedResponse.get("rain_mode"), "1", "rain mode");
        Assert.assertEquals(degradationMode, "2", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceability"), "1", "serviceability");
        Assert.assertEquals(mappedResponse.get("non_serviceable_reason"), "1", "non serviceable reason");
        Assert.assertEquals(mappedResponse.get("customer_zone_raining"), "true", "customer zone raining");
    }


    @Test(description = "SLTC_31 :- Verify restaurant is serviceable when the distance from customer to restaurant is less than the max last mile (min of max last mile at restaurant's area and restaurant)")
    public void rainsAtCustomerRainMode2ltLM()
    {
        double custZoneMaxSla = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("2", defaultCustomerZone).get("zone_max_sla").toString());
        String restZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(zone_id.toString());
        if(!restZoneRainMode.equals("3"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache("3", zone_id.toString());
        String custZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(defaultCustomerZone);
        if(!custZoneRainMode.equals("2"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache("2", defaultCustomerZone);
        if(custZoneMaxSla < 200)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "200", "2", defaultCustomerZone.toString());
        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {defaultCustomerLat.toString(),defaultCustomerLon.toString(),city_id.toString(),restaurantId},new LinkedHashMap<String,String>(){{
            put("rain_mode","2");
            put("serviceability","2");
            put("long_distance","0");
        }});
        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(restZoneRainMode, zone_id.toString());
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(custZoneRainMode, defaultCustomerZone);
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla",String.valueOf(custZoneMaxSla),"2",defaultCustomerZone.toString());

        Assert.assertEquals(mappedResponse.get("rain_mode"), "2", "rain mode");
        Assert.assertEquals(degradationMode, "1", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceability"), "2", "serviceability");
        Assert.assertEquals(mappedResponse.get("customer_zone_raining"), "true", "customer zone raining");
    }


    @Test(description = "SLTC_32 :- Verify restaurant is not serviceable when the distance from customer to restaurant is greater than the max last mile")
    public void rainsAtCustomerRainMode2gtLM()
    {
        Map<String, String> customerDetails = deliveryDataHelper.latLonNotInGivenTagsButInZone(restLat, restLon, (BigDecimal.valueOf(actualLastMile/1.3).subtract(BigDecimal.valueOf(1))).doubleValue(), "LONG_DISTANCE");
        int distance = deliveryDataHelper.getDistance(restLat, restLon, Double.parseDouble(customerDetails.get("lat")), Double.parseDouble(customerDetails.get("lon")))/1000;
        double custZoneMaxSla = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("2", customerDetails.get("zone_id")).get("zone_max_sla").toString());
        double custZoneLM = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("2", customerDetails.get("zone_id")).get("last_mile").toString());
        double custZoneMaxLm = custZoneLM;
        double areaLm = Double.parseDouble(DeliveryDataBaseUtils.getMaxLastMileForArea(String.valueOf(area_id)));
        double restaurantLm = Double.parseDouble(DeliveryDataBaseUtils.getMaxLastMileForRestaurant(restaurantId));
        String restZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(zone_id.toString());
        if(!restZoneRainMode.equals("3"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache("3", zone_id.toString());
        String custZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(customerDetails.get("zone_id"));
        if(!custZoneRainMode.equals("2"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache("2", customerDetails.get("zone_id"));
        if(custZoneMaxSla < 200)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla","200","2",customerDetails.get("zone_id"));
        if(custZoneLM >= distance)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", String.valueOf(distance - 2), "2", customerDetails.get("zone_id"));

        if(areaLm < distance || restaurantLm < distance)
        {
            DeliveryDataBaseUtils.setMaxLastMileForAreaAndClearCache(String.valueOf(area_id), String.valueOf(distance * 1.3 + 2));
            DeliveryDataBaseUtils.setMaxLastMileForRestaurantAndClearCache(String.valueOf(restaurantId), String.valueOf(distance * 1.3 + 2));
        }
        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {customerDetails.get("lat"), customerDetails.get("lon"), city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode","2");
            put("serviceability","1");
            put("non_serviceable_reason","0");
            put("long_distance","0");
        }});
        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.setMaxLastMileForAreaAndClearCache(String.valueOf(area_id), String.valueOf(areaLm));
        DeliveryDataBaseUtils.setMaxLastMileForRestaurantAndClearCache(String.valueOf(restaurantId), String.valueOf(restaurantLm));
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(restZoneRainMode, zone_id.toString());
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(custZoneRainMode, customerDetails.get("zone_id"));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla",String.valueOf(custZoneMaxSla),"2",customerDetails.get("zone_id"));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile",String.valueOf(custZoneLM),"2",customerDetails.get("zone_id"));

        Assert.assertEquals(mappedResponse.get("rain_mode"), "2", "rain mode");
        Assert.assertEquals(degradationMode, "1", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceability"), "1", "serviceability");
        Assert.assertEquals(mappedResponse.get("non_serviceable_reason"), "0", "non serviceable reason");
        Assert.assertEquals(mappedResponse.get("customer_zone_raining"), "true", "customer zone raining");
    }


    @Test(description = "SLTC_33 :- Verify restaurant is serviceable when the current sla is less than the max sla (max_delivery_time at restaurant's zone)")
    public void rainsAtCustomerRainMode2ltSLA()
    {
        Map<String, String> customerDetails = deliveryDataHelper.latLonNotInGivenTagsButInZone(restLat, restLon, (BigDecimal.valueOf(actualLastMile/1.3).subtract(BigDecimal.valueOf(1))).doubleValue(), "LONG_DISTANCE");
        int distance = deliveryDataHelper.getDistance(restLat, restLon, Double.parseDouble(customerDetails.get("lat")), Double.parseDouble(customerDetails.get("lon")))/1000;
        double custZoneMaxSla = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("2", customerDetails.get("zone_id")).get("zone_max_sla").toString());
        double custZoneLM = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("2", customerDetails.get("zone_id")).get("last_mile").toString());
        String restZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(zone_id.toString());
        double areaLm = Double.parseDouble(DeliveryDataBaseUtils.getMaxLastMileForArea(String.valueOf(area_id)));
        double restaurantLm = Double.parseDouble(DeliveryDataBaseUtils.getMaxLastMileForRestaurant(restaurantId));
        if(!restZoneRainMode.equals("3"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache("3", zone_id.toString());
        String custZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(customerDetails.get("zone_id"));
        if(!custZoneRainMode.equals("2"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache("2", customerDetails.get("zone_id"));
        if(custZoneMaxSla < 200)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "200", "2", customerDetails.get("zone_id"));
        if(custZoneLM <= distance)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", String.valueOf(distance*1.3 + 2), "2", customerDetails.get("zone_id"));
        if(areaLm < distance || restaurantLm < distance)
        {
            DeliveryDataBaseUtils.setMaxLastMileForAreaAndClearCache(String.valueOf(area_id), String.valueOf(distance * 1.3 + 2));
            DeliveryDataBaseUtils.setMaxLastMileForRestaurantAndClearCache(String.valueOf(restaurantId), String.valueOf(distance * 1.3 + 2));
        }
        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {customerDetails.get("lat"), customerDetails.get("lon"), city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode","2");
            put("serviceability","2");
            put("long_distance","0");
        }});
        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.setMaxLastMileForAreaAndClearCache(String.valueOf(area_id), String.valueOf(areaLm));
        DeliveryDataBaseUtils.setMaxLastMileForRestaurantAndClearCache(String.valueOf(restaurantId), String.valueOf(restaurantLm));
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(restZoneRainMode, zone_id.toString());
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(custZoneRainMode, customerDetails.get("zone_id"));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla",String.valueOf(custZoneMaxSla),"2",customerDetails.get("zone_id"));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile",String.valueOf(custZoneLM),"2",customerDetails.get("zone_id"));

        Assert.assertEquals(mappedResponse.get("rain_mode"), "2", "rain mode");
        Assert.assertEquals(degradationMode, "1", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceability"), "2", "serviceability");
        Assert.assertEquals(mappedResponse.get("customer_zone_raining"), "true", "customer zone raining");
    }


    @Test(description = "SLTC_34 :- Verify restaurant is not serviceable when the current sla is greater than the max sla")
    public void rainsAtCustomerRainMode2gtSLA()
    {
        Map<String, String> customerDetails = deliveryDataHelper.latLonNotInGivenTagsButInZone(restLat, restLon, (BigDecimal.valueOf(actualLastMile/1.3).subtract(BigDecimal.valueOf(1))).doubleValue(), "LONG_DISTANCE");
        int distance = deliveryDataHelper.getDistance(restLat, restLon, Double.parseDouble(customerDetails.get("lat")), Double.parseDouble(customerDetails.get("lon")))/1000;
        double custZoneMaxSla = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("2", customerDetails.get("zone_id")).get("zone_max_sla").toString());
        double custZoneLM = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("2", customerDetails.get("zone_id")).get("last_mile").toString());
        String restZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(zone_id.toString());
        double areaLm = Double.parseDouble(DeliveryDataBaseUtils.getMaxLastMileForArea(String.valueOf(area_id)));
        double restaurantLm = Double.parseDouble(DeliveryDataBaseUtils.getMaxLastMileForRestaurant(restaurantId));
        if(!restZoneRainMode.equals("3"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache("3", zone_id.toString());
        String custZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(customerDetails.get("zone_id"));
        if(!custZoneRainMode.equals("2"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache("2", customerDetails.get("zone_id"));
        if(custZoneMaxSla > 10)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "10", "2",customerDetails.get("zone_id"));
        if(custZoneLM >= distance)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", String.valueOf(distance + 2), "2", customerDetails.get("zone_id"));
        if(areaLm < distance || restaurantLm < distance)
        {
            DeliveryDataBaseUtils.setMaxLastMileForAreaAndClearCache(String.valueOf(area_id), String.valueOf(distance * 1.3 + 2));
            DeliveryDataBaseUtils.setMaxLastMileForRestaurantAndClearCache(String.valueOf(restaurantId), String.valueOf(distance * 1.3 + 2));
        }
        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {customerDetails.get("lat"), customerDetails.get("lon"), city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode","2");
            put("serviceability","1");
            put("non_serviceable_reason","1");
            put("long_distance","0");
        }});
        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.setMaxLastMileForAreaAndClearCache(String.valueOf(area_id), String.valueOf(areaLm));
        DeliveryDataBaseUtils.setMaxLastMileForRestaurantAndClearCache(String.valueOf(restaurantId), String.valueOf(restaurantLm));
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(restZoneRainMode, zone_id.toString());
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(custZoneRainMode, customerDetails.get("zone_id"));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", String.valueOf(custZoneMaxSla), "2", customerDetails.get("zone_id"));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", String.valueOf(custZoneLM), "2", customerDetails.get("zone_id"));

        Assert.assertEquals(mappedResponse.get("rain_mode"), "2", "rain mode");
        Assert.assertEquals(degradationMode, "1", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceability"), "1", "serviceability");
        Assert.assertEquals(mappedResponse.get("non_serviceable_reason"), "1", "non serviceable reason");
        Assert.assertEquals(mappedResponse.get("customer_zone_raining"), "true", "customer zone raining");
    }


    @Test(description = "SLTC_35 :- Verify restaurant is serviceable when the distance from customer to restaurant is less than the max last mile (min of max last mile at restaurant's area and restaurant)")
    public void rainsAtCustomerRainMode4GD1ltLM()
    {
        double custZoneMaxSla = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("4", defaultCustomerZone).get("zone_max_sla").toString());
        String restZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(zone_id.toString());
        String degradationState = DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("4", defaultCustomerZone).get("degradation_state").toString();
        if(!restZoneRainMode.equals("3"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache("3", zone_id.toString());
        String custZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(defaultCustomerZone);
        if(!custZoneRainMode.equals("4"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache("4", defaultCustomerZone);
        if(custZoneMaxSla < 200)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "200", "4", defaultCustomerZone);
        if(!degradationState.equals("1"))
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", "1", "4", defaultCustomerZone);

        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {defaultCustomerLat.toString(),defaultCustomerLon.toString(),city_id.toString(),restaurantId},new LinkedHashMap<String,String>(){{
            put("rain_mode","2");
            put("serviceability","2");
            put("long_distance","0");
        }});
        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(restZoneRainMode, zone_id.toString());
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(custZoneRainMode, defaultCustomerZone);
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla",String.valueOf(custZoneMaxSla),"4",defaultCustomerZone);
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state",degradationState,"4",defaultCustomerZone);

        Assert.assertEquals(mappedResponse.get("rain_mode"), "2", "rain mode");
        Assert.assertEquals(degradationMode, "1", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceability"), "2", "serviceability");
        Assert.assertEquals(mappedResponse.get("customer_zone_raining"), "true", "customer zone raining");
    }


    @Test(description = "SLTC_36 :- Verify restaurant is not serviceable when the distance from customer to restaurant is greater than the max last mile")
    public void rainsAtCustomerRainMode4GD1gtLM()
    {
        Map<String, String> customerDetails = deliveryDataHelper.latLonNotInGivenTagsButInZone(restLat, restLon, (BigDecimal.valueOf(actualLastMile/1.3).subtract(BigDecimal.valueOf(1))).doubleValue(), "LONG_DISTANCE");
        int distance = deliveryDataHelper.getDistance(restLat, restLon, Double.parseDouble(customerDetails.get("lat")), Double.parseDouble(customerDetails.get("lon")))/1000;
        Map<String, Object> customerZoneRainParamsDetails = DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("4", customerDetails.get("zone_id"));
        if(customerZoneRainParamsDetails.keySet().toString().equals(row_not_found_key))
            Assert.fail("add entry in zone rain params of rain mode : 4 and zone id : " + customerDetails.get("zone_id"));

        double custZoneMaxSla = Double.parseDouble(customerZoneRainParamsDetails.get("zone_max_sla").toString());
        double custZoneLM = Double.parseDouble(customerZoneRainParamsDetails.get("last_mile").toString());
        String degradationState = customerZoneRainParamsDetails.get("degradation_state").toString();
        double areaLm = Double.parseDouble(DeliveryDataBaseUtils.getMaxLastMileForArea(String.valueOf(area_id)));
        double restaurantLm = Double.parseDouble(DeliveryDataBaseUtils.getMaxLastMileForRestaurant(restaurantId));
        String restZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(zone_id.toString());
        if(!restZoneRainMode.equals("3"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache("3", zone_id.toString());
        String custZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(customerDetails.get("zone_id"));
        if(!custZoneRainMode.equals("4"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache("4", customerDetails.get("zone_id"));
        if(custZoneMaxSla < 200)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "200", "4", customerDetails.get("zone_id"));
        if(custZoneLM >= distance)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", String.valueOf(distance - 2), "4", customerDetails.get("zone_id"));
        if(!degradationState.equals("1"))
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", "1", "4", customerDetails.get("zone_id"));
        if(areaLm < distance || restaurantLm < distance)
        {
            DeliveryDataBaseUtils.setMaxLastMileForAreaAndClearCache(String.valueOf(area_id), String.valueOf(distance * 1.3 + 2));
            DeliveryDataBaseUtils.setMaxLastMileForRestaurantAndClearCache(String.valueOf(restaurantId), String.valueOf(distance * 1.3 + 2));
        }
        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {customerDetails.get("lat"), customerDetails.get("lon"), city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode","2");
            put("serviceability","1");
            put("non_serviceable_reason","0");
            put("long_distance","0");
        }});
        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.setMaxLastMileForAreaAndClearCache(String.valueOf(area_id), String.valueOf(areaLm));
        DeliveryDataBaseUtils.setMaxLastMileForRestaurantAndClearCache(String.valueOf(restaurantId), String.valueOf(restaurantLm));
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(restZoneRainMode, zone_id.toString());
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(custZoneRainMode, customerDetails.get("zone_id"));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", String.valueOf(custZoneMaxSla), "4", customerDetails.get("zone_id"));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", String.valueOf(custZoneLM), "4", customerDetails.get("zone_id"));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", degradationState, "4", customerDetails.get("zone_id"));

        Assert.assertEquals(mappedResponse.get("rain_mode"), "2", "rain mode");
        Assert.assertEquals(degradationMode, "1", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceability"), "1", "serviceability");
        Assert.assertEquals(mappedResponse.get("non_serviceable_reason"), "0", "non serviceable reason");
        Assert.assertEquals(mappedResponse.get("customer_zone_raining"), "true", "customer zone raining");
    }


    @Test(description = "SLTC_37 :- Verify restaurant is serviceable when the current sla is less than the max sla (max_delivery_time at restaurant's zone)")
    public void rainsAtCustomerRainMode4GD1ltSLA()
    {
        Map<String, String> customerDetails = deliveryDataHelper.latLonNotInGivenTagsButInZone(restLat, restLon, (BigDecimal.valueOf(actualLastMile/1.3).subtract(BigDecimal.valueOf(1))).doubleValue(), "LONG_DISTANCE");
        int distance = deliveryDataHelper.getDistance(restLat, restLon, Double.parseDouble(customerDetails.get("lat")), Double.parseDouble(customerDetails.get("lon")))/1000;
        double custZoneMaxSla = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("4", customerDetails.get("zone_id")).get("zone_max_sla").toString());
        double custZoneLM = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("4", customerDetails.get("zone_id")).get("last_mile").toString());
        String degradationState = DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("4", customerDetails.get("zone_id")).get("degradation_state").toString();
        double areaLm = Double.parseDouble(DeliveryDataBaseUtils.getMaxLastMileForArea(String.valueOf(area_id)));
        double restaurantLm = Double.parseDouble(DeliveryDataBaseUtils.getMaxLastMileForRestaurant(restaurantId));
        String restZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(zone_id.toString());
        if(!restZoneRainMode.equals("3"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache("3", zone_id.toString());
        String custZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(customerDetails.get("zone_id"));
        if(!custZoneRainMode.equals("4"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache("4", customerDetails.get("zone_id"));
        if(custZoneMaxSla < 200)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "200", "4", customerDetails.get("zone_id"));
        if(custZoneLM <= distance)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", String.valueOf(distance + 2), "4", customerDetails.get("zone_id"));
        if(!degradationState.equals("1"))
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", "1", "4", customerDetails.get("zone_id"));
        if(areaLm < distance || restaurantLm < distance)
        {
            DeliveryDataBaseUtils.setMaxLastMileForAreaAndClearCache(String.valueOf(area_id), String.valueOf(distance * 1.3 + 2));
            DeliveryDataBaseUtils.setMaxLastMileForRestaurantAndClearCache(String.valueOf(restaurantId), String.valueOf(distance * 1.3 + 2));
        }
        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {customerDetails.get("lat"), customerDetails.get("lon"), city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode","2");
            put("serviceability","2");
            put("long_distance","0");
        }});
        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.setMaxLastMileForAreaAndClearCache(String.valueOf(area_id), String.valueOf(areaLm));
        DeliveryDataBaseUtils.setMaxLastMileForRestaurantAndClearCache(String.valueOf(restaurantId), String.valueOf(restaurantLm));
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(restZoneRainMode, zone_id.toString());
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(custZoneRainMode, customerDetails.get("zone_id"));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla",String.valueOf(custZoneMaxSla),"4",customerDetails.get("zone_id"));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile",String.valueOf(custZoneLM),"4",customerDetails.get("zone_id"));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", degradationState, "4", customerDetails.get("zone_id"));

        Assert.assertEquals(mappedResponse.get("rain_mode"), "2", "rain mode");
        Assert.assertEquals(degradationMode, "1", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceability"), "2", "serviceability");
        Assert.assertEquals(mappedResponse.get("customer_zone_raining"), "true", "customer zone raining");
    }


    @Test(description = "SLTC_38 :- Verify restaurant is not serviceable when the current sla is greater than the max sla")
    public void rainsAtCustomerRainMode4GD1gtSLA()
    {
        Map<String, String> customerDetails = deliveryDataHelper.latLonNotInGivenTagsButInZone(restLat, restLon, (BigDecimal.valueOf(actualLastMile/1.3).subtract(BigDecimal.valueOf(1))).doubleValue(), "LONG_DISTANCE");
        int distance = deliveryDataHelper.getDistance(restLat, restLon, Double.parseDouble(customerDetails.get("lat")), Double.parseDouble(customerDetails.get("lon")))/1000;
        double custZoneMaxSla = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("4", customerDetails.get("zone_id")).get("zone_max_sla").toString());
        double custZoneLM = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("4", customerDetails.get("zone_id")).get("last_mile").toString());
        String degradationState = DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("4", customerDetails.get("zone_id")).get("degradation_state").toString();
        String restZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(zone_id.toString());
        double areaLm = Double.parseDouble(DeliveryDataBaseUtils.getMaxLastMileForArea(String.valueOf(area_id)));
        double restaurantLm = Double.parseDouble(DeliveryDataBaseUtils.getMaxLastMileForRestaurant(restaurantId));
        if(!restZoneRainMode.equals("3"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache("3", zone_id.toString());
        String custZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(customerDetails.get("zone_id"));
        if(!custZoneRainMode.equals("4"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache("4", customerDetails.get("zone_id"));
        if(custZoneMaxSla > 10)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "10", "4",customerDetails.get("zone_id"));
        if(custZoneLM >= distance)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", String.valueOf(distance + 2), "4", customerDetails.get("zone_id"));
        if(!degradationState.equals("1"))
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", "1", "4", customerDetails.get("zone_id"));
        if(areaLm < distance || restaurantLm < distance)
        {
            DeliveryDataBaseUtils.setMaxLastMileForAreaAndClearCache(String.valueOf(area_id), String.valueOf(distance * 1.3 + 2));
            DeliveryDataBaseUtils.setMaxLastMileForRestaurantAndClearCache(String.valueOf(restaurantId), String.valueOf(distance * 1.3 + 2));
        }
        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {customerDetails.get("lat"), customerDetails.get("lon"), city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode","2");
            put("serviceability","1");
            put("non_serviceable_reason","1");
            put("long_distance","0");
        }});
        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.setMaxLastMileForAreaAndClearCache(String.valueOf(area_id), String.valueOf(areaLm));
        DeliveryDataBaseUtils.setMaxLastMileForRestaurantAndClearCache(String.valueOf(restaurantId), String.valueOf(restaurantLm));
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(restZoneRainMode, zone_id.toString());
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(custZoneRainMode, customerDetails.get("zone_id"));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", String.valueOf(custZoneMaxSla), "4", customerDetails.get("zone_id"));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", String.valueOf(custZoneLM), "4", customerDetails.get("zone_id"));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", degradationState, "4", customerDetails.get("zone_id"));

        Assert.assertEquals(mappedResponse.get("rain_mode"), "2", "rain mode");
        Assert.assertEquals(degradationMode, "1", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceability"), "1", "serviceability");
        Assert.assertEquals(mappedResponse.get("non_serviceable_reason"), "1", "non serviceable reason");
        Assert.assertEquals(mappedResponse.get("customer_zone_raining"), "true", "customer zone raining");
    }


    @Test(description = "SLTC_39 :- Verify restaurant is serviceable when the distance from customer to restaurant is less than the max last mile (min of max last mile at restaurant's area and restaurant)")
    public void rainsAtCustomerRainMode4GD2ltLM()
    {
        double custZoneMaxSla = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("4", defaultCustomerZone).get("zone_max_sla").toString());
        String restZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(zone_id.toString());
        String degradationState = DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("4", defaultCustomerZone).get("degradation_state").toString();
        if(!restZoneRainMode.equals("3"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache("3", zone_id.toString());
        String custZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(defaultCustomerZone);
        if(!custZoneRainMode.equals("4"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache("4", defaultCustomerZone);
        if(custZoneMaxSla < 200)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "200", "4", defaultCustomerZone);
        if(!degradationState.equals("2"))
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", "2", "4", defaultCustomerZone);
        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {defaultCustomerLat.toString(),defaultCustomerLon.toString(),city_id.toString(),restaurantId},new LinkedHashMap<String,String>(){{
            put("rain_mode","1");
            put("serviceability","2");
            put("long_distance","0");
        }});
        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(restZoneRainMode, zone_id.toString());
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(custZoneRainMode, defaultCustomerZone);
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla",String.valueOf(custZoneMaxSla),"4",defaultCustomerZone);
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state",degradationState,"4",defaultCustomerZone);

        Assert.assertEquals(mappedResponse.get("rain_mode"), "1", "rain mode");
        Assert.assertEquals(degradationMode, "2", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceability"), "2", "serviceability");
        Assert.assertEquals(mappedResponse.get("customer_zone_raining"), "true", "customer zone raining");
    }


    @Test(description = "SLTC_40 :- Verify restaurant is not serviceable when the distance from customer to restaurant is greater than the max last mile")
    public void rainsAtCustomerRainMode4GD2gtLM()
    {
        Map<String, String> customerDetails = deliveryDataHelper.latLonNotInGivenTagsButInZone(restLat, restLon, (BigDecimal.valueOf(actualLastMile/1.3).subtract(BigDecimal.valueOf(1))).doubleValue(), "LONG_DISTANCE");
        int distance = deliveryDataHelper.getDistance(restLat, restLon, Double.parseDouble(customerDetails.get("lat")), Double.parseDouble(customerDetails.get("lon")))/1000;
        double custZoneMaxSla = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("4", customerDetails.get("zone_id")).get("zone_max_sla").toString());
        double custZoneLM = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("4", customerDetails.get("zone_id")).get("last_mile").toString());
        String degradationState = DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("4", customerDetails.get("zone_id")).get("degradation_state").toString();
        double areaLm = Double.parseDouble(DeliveryDataBaseUtils.getMaxLastMileForArea(String.valueOf(area_id)));
        double restaurantLm = Double.parseDouble(DeliveryDataBaseUtils.getMaxLastMileForRestaurant(restaurantId));
        String restZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(zone_id.toString());
        if(!restZoneRainMode.equals("3"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache("3", zone_id.toString());
        String custZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(customerDetails.get("zone_id"));
        if(!custZoneRainMode.equals("4"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache("4", customerDetails.get("zone_id"));
        if(custZoneMaxSla < 200)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "200", "4", customerDetails.get("zone_id"));
        if(custZoneLM >= distance)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", String.valueOf(distance - 2), "4", customerDetails.get("zone_id"));
        if(!degradationState.equals("2"))
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", "2", "4", customerDetails.get("zone_id"));
        if(areaLm < distance || restaurantLm < distance)
        {
            DeliveryDataBaseUtils.setMaxLastMileForAreaAndClearCache(String.valueOf(area_id), String.valueOf(distance * 1.3 + 2));
            DeliveryDataBaseUtils.setMaxLastMileForRestaurantAndClearCache(String.valueOf(restaurantId), String.valueOf(distance * 1.3 + 2));
        }
        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {customerDetails.get("lat"), customerDetails.get("lon"), city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode","1");
            put("serviceability","1");
            put("non_serviceable_reason","0");
            put("long_distance","0");
        }});
        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.setMaxLastMileForAreaAndClearCache(String.valueOf(area_id), String.valueOf(areaLm));
        DeliveryDataBaseUtils.setMaxLastMileForRestaurantAndClearCache(String.valueOf(restaurantId), String.valueOf(restaurantLm));
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(restZoneRainMode, zone_id.toString());
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(custZoneRainMode, customerDetails.get("zone_id"));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", String.valueOf(custZoneMaxSla), "4", customerDetails.get("zone_id"));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", String.valueOf(custZoneLM), "4", customerDetails.get("zone_id"));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", degradationState, "4", customerDetails.get("zone_id"));

        Assert.assertEquals(mappedResponse.get("rain_mode"), "1", "rain mode");
        Assert.assertEquals(degradationMode, "2", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceability"), "1", "serviceability");
        Assert.assertEquals(mappedResponse.get("non_serviceable_reason"), "0", "non serviceable reason");
        Assert.assertEquals(mappedResponse.get("customer_zone_raining"), "true", "customer zone raining");
    }


    @Test(description = "SLTC_41 :- Verify restaurant is serviceable when the current sla is less than the max sla (max_delivery_time at restaurant's zone)")
    public void rainsAtCustomerRainMode4GD2ltSLA()
    {
        Map<String, String> customerDetails = deliveryDataHelper.latLonNotInGivenTagsButInZone(restLat, restLon, (BigDecimal.valueOf(actualLastMile/1.3).subtract(BigDecimal.valueOf(1))).doubleValue(), "LONG_DISTANCE");
        int distance = deliveryDataHelper.getDistance(restLat, restLon, Double.parseDouble(customerDetails.get("lat")), Double.parseDouble(customerDetails.get("lon")))/1000;
        double custZoneMaxSla = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("4", customerDetails.get("zone_id")).get("zone_max_sla").toString());
        double custZoneLM = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("4", customerDetails.get("zone_id")).get("last_mile").toString());
        String degradationState = DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("4", customerDetails.get("zone_id")).get("degradation_state").toString();
        double areaLm = Double.parseDouble(DeliveryDataBaseUtils.getMaxLastMileForArea(String.valueOf(area_id)));
        double restaurantLm = Double.parseDouble(DeliveryDataBaseUtils.getMaxLastMileForRestaurant(restaurantId));
        String restZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(zone_id.toString());
        if(!restZoneRainMode.equals("3"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache("3", zone_id.toString());
        String custZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(customerDetails.get("zone_id"));
        if(!custZoneRainMode.equals("4"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache("4", customerDetails.get("zone_id"));
        if(custZoneMaxSla < 200)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "200", "4", customerDetails.get("zone_id"));
        if(custZoneLM <= distance)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", String.valueOf(distance + 2), "4", customerDetails.get("zone_id"));
        if(!degradationState.equals("2"))
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", "2", "4", customerDetails.get("zone_id"));
        if(areaLm < distance || restaurantLm < distance)
        {
            DeliveryDataBaseUtils.setMaxLastMileForAreaAndClearCache(String.valueOf(area_id), String.valueOf(distance * 1.3 + 2));
            DeliveryDataBaseUtils.setMaxLastMileForRestaurantAndClearCache(String.valueOf(restaurantId), String.valueOf(distance * 1.3 + 2));
        }
        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {customerDetails.get("lat"), customerDetails.get("lon"), city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode","1");
            put("serviceability","2");
            put("long_distance","0");
        }});
        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.setMaxLastMileForAreaAndClearCache(String.valueOf(area_id), String.valueOf(areaLm));
        DeliveryDataBaseUtils.setMaxLastMileForRestaurantAndClearCache(String.valueOf(restaurantId), String.valueOf(restaurantLm));
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(restZoneRainMode, zone_id.toString());
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(custZoneRainMode, customerDetails.get("zone_id"));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla",String.valueOf(custZoneMaxSla),"4",customerDetails.get("zone_id"));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile",String.valueOf(custZoneLM),"4",customerDetails.get("zone_id"));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", degradationState, "4", customerDetails.get("zone_id"));

        Assert.assertEquals(mappedResponse.get("rain_mode"), "1", "rain mode");
        Assert.assertEquals(degradationMode, "2", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceability"), "2", "serviceability");
        Assert.assertEquals(mappedResponse.get("customer_zone_raining"), "true", "customer zone raining");
    }


    @Test(description = "SLTC_42 :- Verify restaurant is not serviceable when the current sla is greater than the max sla")
    public void rainsAtCustomerRainMode4GD2gtSLA()
    {
        Map<String, String> customerDetails = deliveryDataHelper.latLonNotInGivenTagsButInZone(restLat, restLon, (BigDecimal.valueOf(actualLastMile/1.3).subtract(BigDecimal.valueOf(1))).doubleValue(), "LONG_DISTANCE");
        int distance = deliveryDataHelper.getDistance(restLat, restLon, Double.parseDouble(customerDetails.get("lat")), Double.parseDouble(customerDetails.get("lon")))/1000;
        double custZoneMaxSla = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("4", customerDetails.get("zone_id")).get("zone_max_sla").toString());
        double custZoneLM = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("4", customerDetails.get("zone_id")).get("last_mile").toString());
        String degradationState = DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("4", customerDetails.get("zone_id")).get("degradation_state").toString();
        String restZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(zone_id.toString());
        double areaLm = Double.parseDouble(DeliveryDataBaseUtils.getMaxLastMileForArea(String.valueOf(area_id)));
        double restaurantLm = Double.parseDouble(DeliveryDataBaseUtils.getMaxLastMileForRestaurant(restaurantId));
        if(!restZoneRainMode.equals("3"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache("3", zone_id.toString());
        String custZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(customerDetails.get("zone_id"));
        if(!custZoneRainMode.equals("4"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache("4", customerDetails.get("zone_id"));
        if(custZoneMaxSla > 10)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "10", "4",customerDetails.get("zone_id"));
        if(custZoneLM >= distance)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", String.valueOf(distance + 2), "4", customerDetails.get("zone_id"));
        if(!degradationState.equals("2"))
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", "2", "4", customerDetails.get("zone_id"));
        if(areaLm < distance || restaurantLm < distance)
        {
            DeliveryDataBaseUtils.setMaxLastMileForAreaAndClearCache(String.valueOf(area_id), String.valueOf(distance * 1.3 + 2));
            DeliveryDataBaseUtils.setMaxLastMileForRestaurantAndClearCache(String.valueOf(restaurantId), String.valueOf(distance * 1.3 + 2));
        }
        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {customerDetails.get("lat"), customerDetails.get("lon"), city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode","1");
            put("serviceability","1");
            put("non_serviceable_reason","1");
            put("long_distance","0");
        }});
        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.setMaxLastMileForAreaAndClearCache(String.valueOf(area_id), String.valueOf(areaLm));
        DeliveryDataBaseUtils.setMaxLastMileForRestaurantAndClearCache(String.valueOf(restaurantId), String.valueOf(restaurantLm));
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(restZoneRainMode, zone_id.toString());
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(custZoneRainMode, customerDetails.get("zone_id"));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", String.valueOf(custZoneMaxSla), "4", customerDetails.get("zone_id"));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", String.valueOf(custZoneLM), "4", customerDetails.get("zone_id"));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", degradationState, "4", customerDetails.get("zone_id"));

        Assert.assertEquals(mappedResponse.get("rain_mode"), "1", "rain mode");
        Assert.assertEquals(degradationMode, "2", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceability"), "1", "serviceability");
        Assert.assertEquals(mappedResponse.get("non_serviceable_reason"), "1", "non serviceable reason");
        Assert.assertEquals(mappedResponse.get("customer_zone_raining"), "true", "customer zone raining");
    }


    @Test(description = "SLTC_39 :- Verify restaurant is serviceable when the distance from customer to restaurant is less than the max last mile (min of max last mile at restaurant's area and restaurant)")
    public void rainsAtCustomerRainMode4GD3ltLM()
    {
        double custZoneMaxSla = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("4", defaultCustomerZone).get("zone_max_sla").toString());
        String restZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(zone_id.toString());
        String degradationState = DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("4", defaultCustomerZone).get("degradation_state").toString();
        if(!restZoneRainMode.equals("3"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache("3", zone_id.toString());
        String custZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(defaultCustomerZone);
        if(!custZoneRainMode.equals("4"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache("4", defaultCustomerZone);
        if(custZoneMaxSla < 200)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "200", "4", defaultCustomerZone);
        if(!degradationState.equals("3"))
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", "3", "4", defaultCustomerZone);
        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {defaultCustomerLat.toString(),defaultCustomerLon.toString(),city_id.toString(),restaurantId},new LinkedHashMap<String,String>(){{
            put("rain_mode","5");
            put("serviceability","2");
            put("long_distance","0");
        }});
        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(restZoneRainMode, zone_id.toString());
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(custZoneRainMode, defaultCustomerZone);
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla",String.valueOf(custZoneMaxSla),"4",defaultCustomerZone);
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state",degradationState,"4",defaultCustomerZone);

        Assert.assertEquals(mappedResponse.get("rain_mode"), "5", "rain mode");
        Assert.assertEquals(degradationMode, "3", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceability"), "2", "serviceability");
        Assert.assertEquals(mappedResponse.get("customer_zone_raining"), "true", "customer zone raining");
    }


    @Test(description = "SLTC_40 :- Verify restaurant is not serviceable when the distance from customer to restaurant is greater than the max last mile")
    public void rainsAtCustomerRainMode4GD3gtLM()
    {
        Map<String, String> customerDetails = deliveryDataHelper.latLonNotInGivenTagsButInZone(restLat, restLon, (BigDecimal.valueOf(actualLastMile/1.3).subtract(BigDecimal.valueOf(1))).doubleValue(), "LONG_DISTANCE");
        int distance = deliveryDataHelper.getDistance(restLat, restLon, Double.parseDouble(customerDetails.get("lat")), Double.parseDouble(customerDetails.get("lon")))/1000;
        double custZoneMaxSla = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("4", customerDetails.get("zone_id")).get("zone_max_sla").toString());
        double custZoneLM = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("4", customerDetails.get("zone_id")).get("last_mile").toString());
        String degradationState = DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("4", customerDetails.get("zone_id")).get("degradation_state").toString();
        double areaLm = Double.parseDouble(DeliveryDataBaseUtils.getMaxLastMileForArea(String.valueOf(area_id)));
        double restaurantLm = Double.parseDouble(DeliveryDataBaseUtils.getMaxLastMileForRestaurant(restaurantId));
        String restZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(zone_id.toString());
        if(!restZoneRainMode.equals("3"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache("3", zone_id.toString());
        String custZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(customerDetails.get("zone_id"));
        if(!custZoneRainMode.equals("4"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache("4", customerDetails.get("zone_id"));
        if(custZoneMaxSla < 200)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "200", "4", customerDetails.get("zone_id"));
        if(custZoneLM >= distance)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", String.valueOf(distance - 2), "4", customerDetails.get("zone_id"));
        if(!degradationState.equals("3"))
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", "3", "4", customerDetails.get("zone_id"));
        if(areaLm < distance || restaurantLm < distance)
        {
            DeliveryDataBaseUtils.setMaxLastMileForAreaAndClearCache(String.valueOf(area_id), String.valueOf(distance * 1.3 + 2));
            DeliveryDataBaseUtils.setMaxLastMileForRestaurantAndClearCache(String.valueOf(restaurantId), String.valueOf(distance * 1.3 + 2));
        }
        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {customerDetails.get("lat"), customerDetails.get("lon"), city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode","5");
            put("serviceability","1");
            put("non_serviceable_reason","0");
            put("long_distance","0");
        }});
        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.setMaxLastMileForAreaAndClearCache(String.valueOf(area_id), String.valueOf(areaLm));
        DeliveryDataBaseUtils.setMaxLastMileForRestaurantAndClearCache(String.valueOf(restaurantId), String.valueOf(restaurantLm));
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(restZoneRainMode, zone_id.toString());
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(custZoneRainMode, customerDetails.get("zone_id"));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", String.valueOf(custZoneMaxSla), "4", customerDetails.get("zone_id"));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", String.valueOf(custZoneLM), "4", customerDetails.get("zone_id"));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", degradationState, "4", customerDetails.get("zone_id"));

        Assert.assertEquals(mappedResponse.get("rain_mode"), "5", "rain mode");
        Assert.assertEquals(degradationMode, "3", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceability"), "1", "serviceability");
        Assert.assertEquals(mappedResponse.get("non_serviceable_reason"), "0", "non serviceability reason");
        Assert.assertEquals(mappedResponse.get("customer_zone_raining"), "true", "customer zone raining");
    }


    @Test(description = "SLTC_41 :- Verify restaurant is serviceable when the current sla is less than the max sla (max_delivery_time at restaurant's zone)")
    public void rainsAtCustomerRainMode4GD3ltSLA()
    {
        Map<String, String> customerDetails = deliveryDataHelper.latLonNotInGivenTagsButInZone(restLat, restLon, (BigDecimal.valueOf(actualLastMile/1.3).subtract(BigDecimal.valueOf(1))).doubleValue(), "LONG_DISTANCE");
        int distance = deliveryDataHelper.getDistance(restLat, restLon, Double.parseDouble(customerDetails.get("lat")), Double.parseDouble(customerDetails.get("lon")))/1000;
        double custZoneMaxSla = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("4", customerDetails.get("zone_id")).get("zone_max_sla").toString());
        double custZoneLM = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("4", customerDetails.get("zone_id")).get("last_mile").toString());
        String degradationState = DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("4", customerDetails.get("zone_id")).get("degradation_state").toString();
        double areaLm = Double.parseDouble(DeliveryDataBaseUtils.getMaxLastMileForArea(String.valueOf(area_id)));
        double restaurantLm = Double.parseDouble(DeliveryDataBaseUtils.getMaxLastMileForRestaurant(restaurantId));
        String restZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(zone_id.toString());
        if(!restZoneRainMode.equals("3"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache("3", zone_id.toString());
        String custZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(customerDetails.get("zone_id"));
        if(!custZoneRainMode.equals("4"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache("4", customerDetails.get("zone_id"));
        if(custZoneMaxSla < 200)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "200", "4", customerDetails.get("zone_id"));
        if(custZoneLM <= distance)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", String.valueOf(distance + 2), "4", customerDetails.get("zone_id"));
        if(!degradationState.equals("3"))
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", "3", "4", customerDetails.get("zone_id"));
        if(areaLm < distance || restaurantLm < distance)
        {
            DeliveryDataBaseUtils.setMaxLastMileForAreaAndClearCache(String.valueOf(area_id), String.valueOf(distance * 1.3 + 2));
            DeliveryDataBaseUtils.setMaxLastMileForRestaurantAndClearCache(String.valueOf(restaurantId), String.valueOf(distance * 1.3 + 2));
        }
        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {customerDetails.get("lat"), customerDetails.get("lon"), city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode","5");
            put("serviceability","2");
            put("long_distance","0");
        }});
        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.setMaxLastMileForAreaAndClearCache(String.valueOf(area_id), String.valueOf(areaLm));
        DeliveryDataBaseUtils.setMaxLastMileForRestaurantAndClearCache(String.valueOf(restaurantId), String.valueOf(restaurantLm));
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(restZoneRainMode, zone_id.toString());
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(custZoneRainMode, customerDetails.get("zone_id"));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla",String.valueOf(custZoneMaxSla),"4",customerDetails.get("zone_id"));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile",String.valueOf(custZoneLM),"4",customerDetails.get("zone_id"));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", degradationState, "4", customerDetails.get("zone_id"));

        Assert.assertEquals(mappedResponse.get("rain_mode"), "5", "rain mode");
        Assert.assertEquals(degradationMode, "3", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceability"), "2", "serviceability");
        Assert.assertEquals(mappedResponse.get("customer_zone_raining"), "true", "customer zone raining");
    }


    @Test(description = "SLTC_42 :- Verify restaurant is not serviceable when the current sla is greater than the max sla")
    public void rainsAtCustomerRainMode4GD3gtSLA()
    {
        Map<String, String> customerDetails = deliveryDataHelper.latLonNotInGivenTagsButInZone(restLat, restLon, (BigDecimal.valueOf(actualLastMile/1.3).subtract(BigDecimal.valueOf(1))).doubleValue(), "LONG_DISTANCE");
        int distance = deliveryDataHelper.getDistance(restLat, restLon, Double.parseDouble(customerDetails.get("lat")), Double.parseDouble(customerDetails.get("lon")))/1000;
        double custZoneMaxSla = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("4", customerDetails.get("zone_id")).get("zone_max_sla").toString());
        double custZoneLM = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("4", customerDetails.get("zone_id")).get("last_mile").toString());
        String degradationState = DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("4", customerDetails.get("zone_id")).get("degradation_state").toString();
        String restZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(zone_id.toString());
        double areaLm = Double.parseDouble(DeliveryDataBaseUtils.getMaxLastMileForArea(String.valueOf(area_id)));
        double restaurantLm = Double.parseDouble(DeliveryDataBaseUtils.getMaxLastMileForRestaurant(restaurantId));
        if(!restZoneRainMode.equals("3"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache("3", zone_id.toString());
        String custZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(customerDetails.get("zone_id"));
        if(!custZoneRainMode.equals("4"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache("4", customerDetails.get("zone_id"));
        if(custZoneMaxSla > 10)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "10", "4",customerDetails.get("zone_id"));
        if(custZoneLM >= distance)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", String.valueOf(distance + 2), "4", customerDetails.get("zone_id"));
        if(!degradationState.equals("3"))
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", "3", "4", customerDetails.get("zone_id"));
        if(areaLm < distance || restaurantLm < distance)
        {
            DeliveryDataBaseUtils.setMaxLastMileForAreaAndClearCache(String.valueOf(area_id), String.valueOf(distance * 1.3 + 2));
            DeliveryDataBaseUtils.setMaxLastMileForRestaurantAndClearCache(String.valueOf(restaurantId), String.valueOf(distance * 1.3 + 2));
        }
        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {customerDetails.get("lat"), customerDetails.get("lon"), city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode","5");
            put("serviceability","1");
            put("non_serviceable_reason","1");
            put("long_distance","0");
        }});
        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.setMaxLastMileForAreaAndClearCache(String.valueOf(area_id), String.valueOf(areaLm));
        DeliveryDataBaseUtils.setMaxLastMileForRestaurantAndClearCache(String.valueOf(restaurantId), String.valueOf(restaurantLm));
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(restZoneRainMode, zone_id.toString());
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(custZoneRainMode, customerDetails.get("zone_id"));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", String.valueOf(custZoneMaxSla), "4", customerDetails.get("zone_id"));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", String.valueOf(custZoneLM), "4", customerDetails.get("zone_id"));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", degradationState, "4", customerDetails.get("zone_id"));

        Assert.assertEquals(mappedResponse.get("rain_mode"), "5", "rain mode");
        Assert.assertEquals(degradationMode, "3", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceability"), "1", "serviceability");
        Assert.assertEquals(mappedResponse.get("non_serviceable_reason"), "1", "non serviceable reason");
        Assert.assertEquals(mappedResponse.get("customer_zone_raining"), "true", "customer zone raining");
    }


    @Test(description = "Verify the restaurant is serviceable in Listing when item count for pre order is less than the preorder_capacity_per_slot")
    public void ltPreOrderCapacity()
    {
        List<String> timestamp = Serviceability_Constant.getStartTime();
        String startTime = timestamp.get(0), endTime = timestamp.get(1);
        String capacityPerSlot = serviceablility_Helper.getValueFromDb("preorder_capacity_per_slot", "zone", "id", zone_id.toString());
        if(capacityPerSlot.equals("0") || capacityPerSlot == null)
            serviceablility_Helper.setValueInDb("zone", "preorder_capacity_per_slot", "100", "id", zone_id.toString());
        String preOrderListingResponse = serviceablility_Helper.preOrderListing(new String[] {defaultCustomerLat.toString(), defaultCustomerLon.toString(), city_id.toString(), restaurantId, startTime, endTime}).ResponseValidator.GetBodyAsText();

        String serviceable = JsonPath.read(preOrderListingResponse, "$.serviceableRestaurants..listingServiceabilityResponse.serviceability")
                .toString().replace("[", "").replace("]", "");
        serviceablility_Helper.setValueInDb("zone", "preorder_capacity_per_slot", capacityPerSlot, "id", zone_id.toString());
        Assert.assertEquals(serviceable, "2");
    }

    @Test(description = "Verify the restaurant is not serviceable in Listing when item count for pre order is greater than the preorder_capacity_per_slot")
    public void gtPreOrderCapacity()
    {
        List<String> timestamp = Serviceability_Constant.getStartTime();
        String startTime = timestamp.get(0), endTime = timestamp.get(1);
        String capacityPerSlot = serviceablility_Helper.getValueFromDb("preorder_capacity_per_slot", "zone", "id", zone_id.toString());
        if(!capacityPerSlot.equals("0"))
            serviceablility_Helper.setValueInDb("zone", "preorder_capacity_per_slot", "0", "id", zone_id.toString());
        String preOrderListingResponse = serviceablility_Helper.preOrderListing(new String[] {defaultCustomerLat.toString(), defaultCustomerLon.toString(), city_id.toString(), restaurantId, startTime, endTime}).ResponseValidator.GetBodyAsText();

        String serviceable = JsonPath.read(preOrderListingResponse, "$.serviceableRestaurants..listingServiceabilityResponse.serviceability")
                .toString().replace("[", "").replace("]", "");
        String nonServiceabilityReason = JsonPath.read(preOrderListingResponse, "$.serviceableRestaurants..listingServiceabilityResponse.non_serviceable_reason")
                .toString().replace("[", "").replace("]", "");

        serviceablility_Helper.setValueInDb("zone", "preorder_capacity_per_slot", capacityPerSlot, "id", zone_id.toString());
        Assert.assertEquals(serviceable, "0");
        Assert.assertEquals(nonServiceabilityReason, "11");
    }


    //need to place an order.. for equality check..
	/*@Test(description = "Verify the restaurant is serviceable in Listing when item count for pre order is greater than the preorder_capacity_per_slot")
	public void equalPreOrderCapacity()
	{
		List<String> timestamp = Serviceability_Constant.getStartTime();
		String startTime = timestamp.get(0), endTime = timestamp.get(1);
		String capacityPerSlot = serviceablility_Helper.getValueFromDb("preorder_capacity_per_slot", "zone", "id", zone_id.toString());
		if(!capacityPerSlot.equals("1"))
			serviceablility_Helper.setValueInDb("zone", "preorder_capacity_per_slot", "1", "id", zone_id.toString());
		String preOrderListingResponse = serviceablility_Helper.preOrderListing(new String[] {defaultCustomerLat.toString(), defaultCustomerLon.toString(), city_id.toString(), restaurantId, startTime, endTime}).ResponseValidator.GetBodyAsText();

		String serviceable = JsonPath.read(preOrderListingResponse, "$.serviceableRestaurants..listingServiceabilityResponse.serviceability")
				.toString().replace("[", "").replace("]", "");

		serviceablility_Helper.setValueInDb("zone", "preorder_capacity_per_slot", capacityPerSlot, "id", zone_id.toString());
		Assert.assertEquals(serviceable, "2");
	}*/


    @Test(description = "SLTC_47 :- Verify restaurant is serviceable when the distance from customer to restaurant is less than the max last mile at zone_rain_params of restaurant zone with 'rain mode:1'")
    public void rainsAtRestAndCustRainMode1ltLM()
    {
        double restZoneMaxSla = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("1", zone_id.toString()).get("zone_max_sla").toString());
        String restZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(zone_id.toString());
        if(!restZoneRainMode.equals("1"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache("1", zone_id.toString());
        String custZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(defaultCustomerZone);
        if(!custZoneRainMode.equals("1"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache("1", defaultCustomerZone);
        if(restZoneMaxSla < 200)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "200", "1", zone_id.toString());
        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroListing(10,1000,new String[]
                {defaultCustomerLat.toString(), defaultCustomerLon.toString(), city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode","1");
            put("serviceability","2");
            put("long_distance","0");
        }});
        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(restZoneRainMode, zone_id.toString());
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(custZoneRainMode, defaultCustomerZone);
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla",String.valueOf(restZoneMaxSla),"1",zone_id.toString());

        Assert.assertEquals(mappedResponse.get("rain_mode"), "1");
        Assert.assertEquals(degradationMode, "2");
        Assert.assertEquals(mappedResponse.get("serviceability"), "2");
    }

	@Test(description = "SLTC_48 :- Verify restaurant is not serviceable when the distance from customer to restaurant is greater than the max last mile at zone_rain_params of restaurant zone with 'rain mode:1'")
	public void rainsAtRestAndCustRainMode1gtLM()
	{
		String rain_mode="1";
		double restZoneMaxSla = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("1", zone_id.toString()).get("zone_max_sla").toString());
        String lastMileAtArea = DeliveryDataBaseUtils.getMaxLastMileForArea(area_id.toString());
        String lastMileAtRestaurant = DeliveryDataBaseUtils.getMaxLastMileForRestaurant(restaurantId);

        DeliveryDataBaseUtils.setMaxLastMileForArea(area_id.toString(), "4.0");
		DeliveryDataBaseUtils.setMaxLastMileForRestaurant(restaurantId, "4.0");


		Map<String,String> customerDetails = deliveryDataHelper.latLonNotInGivenTagsButInZone(restLat,restLon, 1.5,"LONG_DISTANCE");
		String custlat=customerDetails.get("lat");
		String custlon=customerDetails.get("lon");
		String custzoneid=customerDetails.get("zone_id");
		String restRainMode = DeliveryDataBaseUtils.getZoneRainMode(zone_id.toString());
		if(!restRainMode.equals("1"))
			DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rain_mode,zone_id.toString());
		String custRainMode = DeliveryDataBaseUtils.getZoneRainMode(custzoneid);
		if(custRainMode.equals("1"))
			DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rain_mode,custzoneid);


		if(restZoneMaxSla < 200)
			DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "200", rain_mode, zone_id.toString());
		String maxLmAtRestZoneRainParam = DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("last_mile").toString();
		DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", "1.0", rain_mode, zone_id.toString());

		Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
				{custlat, custlon, city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
			put("rain_mode","1");
			put("serviceability","1");
			put("non_serviceable_reason","0");
			put("long_distance","0");
		}});
		String degradationMode = getResponseDegradationMode(mappedResponse);
		DeliveryDataBaseUtils.setMaxLastMileForArea(area_id.toString(), lastMileAtArea);
		DeliveryDataBaseUtils.setMaxLastMileForRestaurant(restaurantId, lastMileAtRestaurant);
		DeliveryDataBaseUtils.setZoneRainModeAndClearCache(restRainMode, zone_id.toString());
		DeliveryDataBaseUtils.setZoneRainModeAndClearCache(custRainMode, custzoneid);
		DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla",String.valueOf(restZoneMaxSla),"1", zone_id.toString());

		DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", maxLmAtRestZoneRainParam, rain_mode, zone_id.toString());


		Assert.assertEquals(mappedResponse.get("rain_mode"), "1", "rain mode");
		Assert.assertEquals(degradationMode, "2", "degradation mode");
		Assert.assertEquals(mappedResponse.get("serviceability"), "1", "serviceability");
		Assert.assertEquals(mappedResponse.get("non_serviceable_reason"), "0", "non serviceable reason");
	}

    @Test(description = "SLTC_49 :- Verify restaurant is serviceable when the current sla is less than the max sla at zone_rain_params of restaurant zone with 'rain mode:1'")
    public void rainsAtRestAndCustRainMode1ltSla()
    {
        String rain_mode="1";
        double restZoneMaxSla = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("1", zone_id.toString()).get("zone_max_sla").toString());
        String lastMileAtArea = DeliveryDataBaseUtils.getMaxLastMileForArea(area_id.toString());
        String lastMileAtRestaurant = DeliveryDataBaseUtils.getMaxLastMileForRestaurant(restaurantId);

        DeliveryDataBaseUtils.setMaxLastMileForArea(area_id.toString(), "6.0");
        DeliveryDataBaseUtils.setMaxLastMileForRestaurant(restaurantId, "6.0");


        Map<String,String> customerDetails = deliveryDataHelper.latLonNotInGivenTagsButInZone(restLat,restLon, 1.5,"LONG_DISTANCE");
        String custlat=customerDetails.get("lat");
        String custlon=customerDetails.get("lon");
        String custzoneid=customerDetails.get("zone_id");
        String restRainMode = DeliveryDataBaseUtils.getZoneRainMode(zone_id.toString());
        if(!restRainMode.equals("1"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rain_mode,zone_id.toString());
        String custRainMode = DeliveryDataBaseUtils.getZoneRainMode(custzoneid);
        if(custRainMode.equals("1"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rain_mode,custzoneid);


        if(restZoneMaxSla < 100)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "100", rain_mode, zone_id.toString());
        String maxLmAtRestZoneRainParam = DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("last_mile").toString();
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", "10.0", rain_mode, zone_id.toString());

        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {custlat, custlon, city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode","1");
            put("serviceability","2");
            put("long_distance","0");
        }});
        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.setMaxLastMileForArea(area_id.toString(), lastMileAtArea);
        DeliveryDataBaseUtils.setMaxLastMileForRestaurant(restaurantId, lastMileAtRestaurant);
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(restRainMode, zone_id.toString());
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(custRainMode, custzoneid);
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla",String.valueOf(restZoneMaxSla),"1", zone_id.toString());

        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", maxLmAtRestZoneRainParam, rain_mode, zone_id.toString());

        Assert.assertEquals(mappedResponse.get("rain_mode"), "1", "rain mode");
        Assert.assertEquals(degradationMode, "2", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceability"), "2", "serviceability");
    }


    @Test(description = "SLTC_50 :- Verify restaurant is not serviceable when the current sla is greater than the max sla at zone_rain_params of restaurant zone with 'rain mode:1'")
    public void rainsAtRestAndCustRainMode1gtSla()
    {
        String rain_mode="1";
        double restZoneMaxSla = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("1", zone_id.toString()).get("zone_max_sla").toString());
        String lastMileAtArea = DeliveryDataBaseUtils.getMaxLastMileForArea(area_id.toString());
        String lastMileAtRestaurant = DeliveryDataBaseUtils.getMaxLastMileForRestaurant(restaurantId);

        DeliveryDataBaseUtils.setMaxLastMileForArea(area_id.toString(), "6.0");
        DeliveryDataBaseUtils.setMaxLastMileForRestaurant(restaurantId, "6.0");


        Map<String,String> customerDetails = deliveryDataHelper.latLonNotInGivenTagsButInZone(restLat,restLon, 1.5,"LONG_DISTANCE");
        String custlat=customerDetails.get("lat");
        String custlon=customerDetails.get("lon");
        String custzoneid=customerDetails.get("zone_id");
        String restRainMode = DeliveryDataBaseUtils.getZoneRainMode(zone_id.toString());
        if(!restRainMode.equals("1"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rain_mode,zone_id.toString());
        String custRainMode = DeliveryDataBaseUtils.getZoneRainMode(custzoneid);
        if(custRainMode.equals("1"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rain_mode,custzoneid);


        if(restZoneMaxSla > 10)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "10", rain_mode, zone_id.toString());
        String maxLmAtRestZoneRainParam = DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("last_mile").toString();
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", "10.0", rain_mode, zone_id.toString());

        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {custlat, custlon, city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode","1");
            put("serviceability","1");
            put("non_serviceable_reason","1");
            put("long_distance","0");
        }});
        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.setMaxLastMileForArea(area_id.toString(), lastMileAtArea);
        DeliveryDataBaseUtils.setMaxLastMileForRestaurant(restaurantId, lastMileAtRestaurant);
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(restRainMode, zone_id.toString());
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(custRainMode, custzoneid);
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla",String.valueOf(restZoneMaxSla),"1", zone_id.toString());

        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", maxLmAtRestZoneRainParam, rain_mode, zone_id.toString());

        Assert.assertEquals(mappedResponse.get("rain_mode"), "1", "rain mode");
        Assert.assertEquals(degradationMode, "2", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceability"), "1", "serviceability");
        Assert.assertEquals(mappedResponse.get("non_serviceable_reason"), "1", "non_serviceable_reason");
    }


    @Test(description = "SLTC_51 :- Verify restaurant is serviceable when the distance from customer to restaurant is less than the max last mile at zone_rain_params of restaurant zone with 'rain mode:2'")
    public void rainsAtRestAndCustRainMode2ltLM()
    {
        double restZoneMaxSla = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("2", zone_id.toString()).get("zone_max_sla").toString());
        String restZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(zone_id.toString());
        if(!restZoneRainMode.equals("2"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache("2", zone_id.toString());
        String custZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(defaultCustomerZone);
        if(!custZoneRainMode.equals("2"))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache("2", defaultCustomerZone);
        if(restZoneMaxSla < 200)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "200", "2", zone_id.toString());
        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroListing(10,1000,new String[]
                {defaultCustomerLat.toString(), defaultCustomerLon.toString(), city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode","2");
            put("serviceability","2");
            put("long_distance","0");
        }});
        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(restZoneRainMode, zone_id.toString());
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(custZoneRainMode, defaultCustomerZone);
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla",String.valueOf(restZoneMaxSla),"2",zone_id.toString());

        Assert.assertEquals(mappedResponse.get("rain_mode"), "2");
        Assert.assertEquals(degradationMode, "1");
        Assert.assertEquals(mappedResponse.get("serviceability"), "2");
    }


    @Test(description = "SLTC_52 :- Verify restaurant is not serviceable when the distance from customer to restaurant is greater than the max last mile at zone_rain_params of restaurant zone with 'rain mode:2'")
    public void rainsAtRestAndCustRainMode2gtLM()
    {
        String rain_mode="2";
        double restZoneMaxSla = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("zone_max_sla").toString());
        String lastMileAtArea = DeliveryDataBaseUtils.getMaxLastMileForArea(area_id.toString());
        String lastMileAtRestaurant = DeliveryDataBaseUtils.getMaxLastMileForRestaurant(restaurantId);

        DeliveryDataBaseUtils.setMaxLastMileForArea(area_id.toString(), "4.0");
        DeliveryDataBaseUtils.setMaxLastMileForRestaurant(restaurantId, "4.0");


        Map<String,String> customerDetails = deliveryDataHelper.latLonNotInGivenTagsButInZone(restLat,restLon, 1.5,"LONG_DISTANCE");
        String custlat=customerDetails.get("lat");
        String custlon=customerDetails.get("lon");
        String custzoneid=customerDetails.get("zone_id");
        String restRainMode = DeliveryDataBaseUtils.getZoneRainMode(zone_id.toString());
        if(!restRainMode.equals(rain_mode))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rain_mode,zone_id.toString());
        String custRainMode = DeliveryDataBaseUtils.getZoneRainMode(custzoneid);
        if(custRainMode.equals(rain_mode))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rain_mode,custzoneid);


        if(restZoneMaxSla < 200)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "200", rain_mode, zone_id.toString());
        String maxLmAtRestZoneRainParam = DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("last_mile").toString();
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", "1.0", rain_mode, zone_id.toString());

        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {custlat, custlon, city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode","1");
            put("serviceability","1");
            put("non_serviceable_reason","0");
            put("long_distance","0");
        }});
        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.setMaxLastMileForArea(area_id.toString(), lastMileAtArea);
        DeliveryDataBaseUtils.setMaxLastMileForRestaurant(restaurantId, lastMileAtRestaurant);
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(restRainMode, zone_id.toString());
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(custRainMode, custzoneid);
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla",String.valueOf(restZoneMaxSla),rain_mode, zone_id.toString());

        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", maxLmAtRestZoneRainParam, rain_mode, zone_id.toString());


        Assert.assertEquals(mappedResponse.get("rain_mode"), "2", "rain mode");
        Assert.assertEquals(degradationMode, "1", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceability"), "1", "serviceability");
        Assert.assertEquals(mappedResponse.get("non_serviceable_reason"), "0", "non serviceable reason");
    }

    @Test(description = "SLTC_53 :- Verify restaurant is serviceable when the current sla is less than the max sla at zone_rain_params of restaurant zone with 'rain mode:2'")
    public void rainsAtRestAndCustRainMode2ltSla()
    {
        String rain_mode="2";
        double restZoneMaxSla = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("zone_max_sla").toString());
        String lastMileAtArea = DeliveryDataBaseUtils.getMaxLastMileForArea(area_id.toString());
        String lastMileAtRestaurant = DeliveryDataBaseUtils.getMaxLastMileForRestaurant(restaurantId);

        DeliveryDataBaseUtils.setMaxLastMileForArea(area_id.toString(), "10.0");
        DeliveryDataBaseUtils.setMaxLastMileForRestaurant(restaurantId, "10.0");


        Map<String,String> customerDetails = deliveryDataHelper.latLonNotInGivenTagsButInZone(restLat,restLon, 1.5,"LONG_DISTANCE");
        String custlat=customerDetails.get("lat");
        String custlon=customerDetails.get("lon");
        String custzoneid=customerDetails.get("zone_id");
        String restRainMode = DeliveryDataBaseUtils.getZoneRainMode(zone_id.toString());
        if(!restRainMode.equals(rain_mode))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rain_mode,zone_id.toString());
        String custRainMode = DeliveryDataBaseUtils.getZoneRainMode(custzoneid);
        if(custRainMode.equals(rain_mode))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rain_mode,custzoneid);


        if(restZoneMaxSla < 100)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "100", rain_mode, zone_id.toString());
        String maxLmAtRestZoneRainParam = DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("last_mile").toString();
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", "10.0", rain_mode, zone_id.toString());

        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {custlat, custlon, city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode","2");
            put("serviceability","2");
            put("long_distance","0");
        }});
        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.setMaxLastMileForArea(area_id.toString(), lastMileAtArea);
        DeliveryDataBaseUtils.setMaxLastMileForRestaurant(restaurantId, lastMileAtRestaurant);
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(restRainMode, zone_id.toString());
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(custRainMode, custzoneid);
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla",String.valueOf(restZoneMaxSla),rain_mode, zone_id.toString());

        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", maxLmAtRestZoneRainParam, rain_mode, zone_id.toString());

        Assert.assertEquals(mappedResponse.get("rain_mode"), "2", "rain mode");
        Assert.assertEquals(degradationMode, "1", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceability"), "2", "serviceability");
    }


    @Test(description = "SLTC_54 :- Verify restaurant is not serviceable when the current sla is greater than the max sla at zone_rain_params of restaurant zone with 'rain mode:2'")
    public void rainsAtRestAndCustRainMode2gtSla()
    {
        String rain_mode="2";
        double restZoneMaxSla = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("zone_max_sla").toString());
        String lastMileAtArea = DeliveryDataBaseUtils.getMaxLastMileForArea(area_id.toString());
        String lastMileAtRestaurant = DeliveryDataBaseUtils.getMaxLastMileForRestaurant(restaurantId);

        DeliveryDataBaseUtils.setMaxLastMileForArea(area_id.toString(), "10.0");
        DeliveryDataBaseUtils.setMaxLastMileForRestaurant(restaurantId, "10.0");


        Map<String,String> customerDetails = deliveryDataHelper.latLonNotInGivenTagsButInZone(restLat,restLon, 1.5,"LONG_DISTANCE");
        String custlat=customerDetails.get("lat");
        String custlon=customerDetails.get("lon");
        String custzoneid=customerDetails.get("zone_id");
        String restRainMode = DeliveryDataBaseUtils.getZoneRainMode(zone_id.toString());
        if(!restRainMode.equals(rain_mode))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rain_mode,zone_id.toString());
        String custRainMode = DeliveryDataBaseUtils.getZoneRainMode(custzoneid);
        if(custRainMode.equals(rain_mode))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rain_mode,custzoneid);


        if(restZoneMaxSla > 10)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "10", rain_mode, zone_id.toString());
        String maxLmAtRestZoneRainParam = DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("last_mile").toString();
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", "10.0", rain_mode, zone_id.toString());

        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {custlat, custlon, city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode","2");
            put("serviceability","1");
            put("non_serviceable_reason","1");
            put("long_distance","0");
        }});
        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.setMaxLastMileForArea(area_id.toString(), lastMileAtArea);
        DeliveryDataBaseUtils.setMaxLastMileForRestaurant(restaurantId, lastMileAtRestaurant);
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(restRainMode, zone_id.toString());
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(custRainMode, custzoneid);
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla",String.valueOf(restZoneMaxSla),rain_mode, zone_id.toString());

        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", maxLmAtRestZoneRainParam, rain_mode, zone_id.toString());

        Assert.assertEquals(mappedResponse.get("rain_mode"), "2", "rain mode");
        Assert.assertEquals(degradationMode, "1", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceability"), "1", "serviceability");
        Assert.assertEquals(mappedResponse.get("non_serviceable_reason"), "1", "non_serviceable_reason");
    }


    @Test(description = "SLTC_55 :- Verify restaurant is serviceable when the distance from customer to restaurant is less than the max last mile at zone_rain_params of restaurant zone with 'rain mode:4'")
    public void rainsAtRestAndCustRainMode4GD1ltLM()
    {
        rain_mode = "4";
        double restZoneMaxSla = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("zone_max_sla").toString());
        String restZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(zone_id.toString());
        if(!restZoneRainMode.equals(rain_mode))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rain_mode, zone_id.toString());
        String custZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(defaultCustomerZone);
        if(!custZoneRainMode.equals(rain_mode))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rain_mode, defaultCustomerZone);
        if(restZoneMaxSla < 200)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "200", rain_mode, zone_id.toString());
        Double degradationState = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select degradation_state from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("degradation_state").toString());
        if(degradationState != 1)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", "1", rain_mode, String.valueOf(zone_id));

        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroListing(10,1000,new String[]
                {defaultCustomerLat.toString(), defaultCustomerLon.toString(), city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode","2");
            put("serviceability","2");
            put("long_distance","0");
        }});
        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(restZoneRainMode, zone_id.toString());
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(custZoneRainMode, defaultCustomerZone);
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", String.valueOf(degradationState), rain_mode, String.valueOf(zone_id));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla",String.valueOf(restZoneMaxSla),rain_mode, zone_id.toString());

        Assert.assertEquals(mappedResponse.get("rain_mode"), "2");
        Assert.assertEquals(degradationMode, "1");
        Assert.assertEquals(mappedResponse.get("serviceability"), "2");
    }


    @Test(description = "SLTC_56 :- Verify restaurant is not serviceable when the distance from customer to restaurant is greater than the max last mile at zone_rain_params of restaurant zone with 'rain mode:4'")
    public void rainsAtRestAndCustRainMode4GD1gtLM()
    {
        String rain_mode="4";
        double restZoneMaxSla = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("zone_max_sla").toString());
        String lastMileAtArea = DeliveryDataBaseUtils.getMaxLastMileForArea(area_id.toString());
        String lastMileAtRestaurant = DeliveryDataBaseUtils.getMaxLastMileForRestaurant(restaurantId);

        DeliveryDataBaseUtils.setMaxLastMileForArea(area_id.toString(), "10.0");
        DeliveryDataBaseUtils.setMaxLastMileForRestaurant(restaurantId, "10.0");
        Double degradationState = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select degradation_state from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("degradation_state").toString());
        if(degradationState != 1)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", "1", rain_mode, String.valueOf(zone_id));


        Map<String,String> customerDetails = deliveryDataHelper.latLonNotInGivenTagsButInZone(restLat,restLon, 1.5,"LONG_DISTANCE");
        String custlat=customerDetails.get("lat");
        String custlon=customerDetails.get("lon");
        String custzoneid=customerDetails.get("zone_id");
        String restRainMode = DeliveryDataBaseUtils.getZoneRainMode(zone_id.toString());
        if(!restRainMode.equals(rain_mode))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rain_mode,zone_id.toString());
        String custRainMode = DeliveryDataBaseUtils.getZoneRainMode(custzoneid);
        if(custRainMode.equals(rain_mode))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rain_mode,custzoneid);


        if(restZoneMaxSla < 200)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "200", rain_mode, zone_id.toString());
        String maxLmAtRestZoneRainParam = DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("last_mile").toString();
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", "1.0", rain_mode, zone_id.toString());

        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {custlat, custlon, city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode","2");
            put("serviceability","1");
            put("non_serviceable_reason","0");
            put("long_distance","0");
        }});
        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.setMaxLastMileForArea(area_id.toString(), lastMileAtArea);
        DeliveryDataBaseUtils.setMaxLastMileForRestaurant(restaurantId, lastMileAtRestaurant);
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(restRainMode, zone_id.toString());
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(custRainMode, custzoneid);
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", String.valueOf(degradationState), rain_mode, String.valueOf(zone_id));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla",String.valueOf(restZoneMaxSla),rain_mode, zone_id.toString());

        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", maxLmAtRestZoneRainParam, rain_mode, zone_id.toString());


        Assert.assertEquals(mappedResponse.get("rain_mode"), "2", "rain mode");
        Assert.assertEquals(degradationMode, "1", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceability"), "1", "serviceability");
        Assert.assertEquals(mappedResponse.get("non_serviceable_reason"), "0", "non serviceable reason");
    }

    @Test(description = "SLTC_57 :- Verify restaurant is serviceable when the current sla is less than the max sla at zone_rain_params of restaurant zone with 'rain mode:4'")
    public void rainsAtRestAndCustRainMode4GD1ltSla()
    {
        String rain_mode="4";
        double restZoneMaxSla = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("zone_max_sla").toString());
        String lastMileAtArea = DeliveryDataBaseUtils.getMaxLastMileForArea(area_id.toString());
        String lastMileAtRestaurant = DeliveryDataBaseUtils.getMaxLastMileForRestaurant(restaurantId);

        DeliveryDataBaseUtils.setMaxLastMileForArea(area_id.toString(), "10.0");
        DeliveryDataBaseUtils.setMaxLastMileForRestaurant(restaurantId, "10.0");
        Double degradationState = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select degradation_state from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("degradation_state").toString());
        if(degradationState != 1)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", "1", rain_mode, String.valueOf(zone_id));


        Map<String,String> customerDetails = deliveryDataHelper.latLonNotInGivenTagsButInZone(restLat,restLon, 1.5,"LONG_DISTANCE");
        String custlat=customerDetails.get("lat");
        String custlon=customerDetails.get("lon");
        String custzoneid=customerDetails.get("zone_id");
        String restRainMode = DeliveryDataBaseUtils.getZoneRainMode(zone_id.toString());
        if(!restRainMode.equals(rain_mode))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rain_mode,zone_id.toString());
        String custRainMode = DeliveryDataBaseUtils.getZoneRainMode(custzoneid);
        if(custRainMode.equals(rain_mode))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rain_mode,custzoneid);


        if(restZoneMaxSla < 100)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "100", rain_mode, zone_id.toString());
        String maxLmAtRestZoneRainParam = DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("last_mile").toString();
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", "10.0", rain_mode, zone_id.toString());

        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {custlat, custlon, city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode","2");
            put("serviceability","2");
            put("long_distance","0");
        }});
        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.setMaxLastMileForArea(area_id.toString(), lastMileAtArea);
        DeliveryDataBaseUtils.setMaxLastMileForRestaurant(restaurantId, lastMileAtRestaurant);
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(restRainMode, zone_id.toString());
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(custRainMode, custzoneid);
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", String.valueOf(degradationState), rain_mode, String.valueOf(zone_id));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla",String.valueOf(restZoneMaxSla),rain_mode, zone_id.toString());

        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", maxLmAtRestZoneRainParam, rain_mode, zone_id.toString());

        Assert.assertEquals(mappedResponse.get("rain_mode"), "2", "rain mode");
        Assert.assertEquals(degradationMode, "1", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceability"), "2", "serviceability");
    }


    @Test(description = "SLTC_58 :- Verify restaurant is not serviceable when the current sla is greater than the max sla at zone_rain_params of restaurant zone with 'rain mode:4'")
    public void rainsAtRestAndCustRainMode4GD1gtSla()
    {
        String rain_mode="4";
        double restZoneMaxSla = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("zone_max_sla").toString());
        String lastMileAtArea = DeliveryDataBaseUtils.getMaxLastMileForArea(area_id.toString());
        String lastMileAtRestaurant = DeliveryDataBaseUtils.getMaxLastMileForRestaurant(restaurantId);

        DeliveryDataBaseUtils.setMaxLastMileForArea(area_id.toString(), "10.0");
        DeliveryDataBaseUtils.setMaxLastMileForRestaurant(restaurantId, "10.0");
        Double degradationState = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select degradation_state from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("degradation_state").toString());
        if(degradationState != 1)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", "1", rain_mode, String.valueOf(zone_id));

        Map<String,String> customerDetails = deliveryDataHelper.latLonNotInGivenTagsButInZone(restLat,restLon, 1.5,"LONG_DISTANCE");
        String custlat=customerDetails.get("lat");
        String custlon=customerDetails.get("lon");
        String custzoneid=customerDetails.get("zone_id");
        String restRainMode = DeliveryDataBaseUtils.getZoneRainMode(zone_id.toString());
        if(!restRainMode.equals(rain_mode))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rain_mode,zone_id.toString());
        String custRainMode = DeliveryDataBaseUtils.getZoneRainMode(custzoneid);
        if(custRainMode.equals(rain_mode))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rain_mode,custzoneid);


        if(restZoneMaxSla > 10)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "10", rain_mode, zone_id.toString());
        String maxLmAtRestZoneRainParam = DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("last_mile").toString();
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", "10.0", rain_mode, zone_id.toString());

        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {custlat, custlon, city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode","2");
            put("serviceability","1");
            put("non_serviceable_reason","1");
            put("long_distance","0");
        }});
        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.setMaxLastMileForArea(area_id.toString(), lastMileAtArea);
        DeliveryDataBaseUtils.setMaxLastMileForRestaurant(restaurantId, lastMileAtRestaurant);
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(restRainMode, zone_id.toString());
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(custRainMode, custzoneid);
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", String.valueOf(degradationState), rain_mode, String.valueOf(zone_id));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla",String.valueOf(restZoneMaxSla),rain_mode, zone_id.toString());

        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", maxLmAtRestZoneRainParam, rain_mode, zone_id.toString());

        Assert.assertEquals(mappedResponse.get("rain_mode"), "2", "rain mode");
        Assert.assertEquals(degradationMode, "1", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceability"), "1", "serviceability");
        Assert.assertEquals(mappedResponse.get("non_serviceable_reason"), "1", "non_serviceable_reason");
    }


    @Test(description = "SLTC_59 :- Verify restaurant is serviceable when the distance from customer to restaurant is less than the max last mile at zone_rain_params of restaurant zone with 'rain mode:4'")
    public void rainsAtRestAndCustRainMode4GD2ltLM()
    {
        rain_mode = "4";
        double restZoneMaxSla = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("zone_max_sla").toString());
        String restZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(zone_id.toString());
        if(!restZoneRainMode.equals(rain_mode))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rain_mode, zone_id.toString());
        String custZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(defaultCustomerZone);
        if(!custZoneRainMode.equals(rain_mode))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rain_mode, defaultCustomerZone);
        if(restZoneMaxSla < 200)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "200", rain_mode, zone_id.toString());
        Double degradationState = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select degradation_state from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("degradation_state").toString());
        if(degradationState != 2)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", "2", rain_mode, String.valueOf(zone_id));

        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroListing(10,1000,new String[]
                {defaultCustomerLat.toString(), defaultCustomerLon.toString(), city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode","1");
            put("serviceability","2");
            put("long_distance","0");
        }});
        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(restZoneRainMode, zone_id.toString());
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(custZoneRainMode, defaultCustomerZone);
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", String.valueOf(degradationState), rain_mode, String.valueOf(zone_id));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla",String.valueOf(restZoneMaxSla),rain_mode, zone_id.toString());

        Assert.assertEquals(mappedResponse.get("rain_mode"), "1");
        Assert.assertEquals(degradationMode, "2");
        Assert.assertEquals(mappedResponse.get("serviceability"), "2");
    }


    @Test(description = "SLTC_60 :- Verify restaurant is not serviceable when the distance from customer to restaurant is greater than the max last mile at zone_rain_params of restaurant zone with 'rain mode:4'")
    public void rainsAtRestAndCustRainMode4GD2gtLM()
    {
        String rain_mode="4";
        double restZoneMaxSla = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("zone_max_sla").toString());
        String lastMileAtArea = DeliveryDataBaseUtils.getMaxLastMileForArea(area_id.toString());
        String lastMileAtRestaurant = DeliveryDataBaseUtils.getMaxLastMileForRestaurant(restaurantId);

        DeliveryDataBaseUtils.setMaxLastMileForArea(area_id.toString(), "10.0");
        DeliveryDataBaseUtils.setMaxLastMileForRestaurant(restaurantId, "10.0");
        Double degradationState = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select degradation_state from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("degradation_state").toString());
        if(degradationState != 2)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", "2", rain_mode, String.valueOf(zone_id));


        Map<String,String> customerDetails = deliveryDataHelper.latLonNotInGivenTagsButInZone(restLat,restLon, 1.5,"LONG_DISTANCE");
        String custlat=customerDetails.get("lat");
        String custlon=customerDetails.get("lon");
        String custzoneid=customerDetails.get("zone_id");
        String restRainMode = DeliveryDataBaseUtils.getZoneRainMode(zone_id.toString());
        if(!restRainMode.equals(rain_mode))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rain_mode,zone_id.toString());
        String custRainMode = DeliveryDataBaseUtils.getZoneRainMode(custzoneid);
        if(custRainMode.equals(rain_mode))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rain_mode,custzoneid);


        if(restZoneMaxSla < 200)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "200", rain_mode, zone_id.toString());
        String maxLmAtRestZoneRainParam = DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("last_mile").toString();
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", "1.0", rain_mode, zone_id.toString());

        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {custlat, custlon, city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode","1");
            put("serviceability","1");
            put("non_serviceable_reason","0");
            put("long_distance","0");
        }});
        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.setMaxLastMileForArea(area_id.toString(), lastMileAtArea);
        DeliveryDataBaseUtils.setMaxLastMileForRestaurant(restaurantId, lastMileAtRestaurant);
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(restRainMode, zone_id.toString());
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(custRainMode, custzoneid);
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", String.valueOf(degradationState), rain_mode, String.valueOf(zone_id));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla",String.valueOf(restZoneMaxSla),rain_mode, zone_id.toString());

        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", maxLmAtRestZoneRainParam, rain_mode, zone_id.toString());


        Assert.assertEquals(mappedResponse.get("rain_mode"), "1", "rain mode");
        Assert.assertEquals(degradationMode, "2", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceability"), "1", "serviceability");
        Assert.assertEquals(mappedResponse.get("non_serviceable_reason"), "0", "non serviceable reason");
    }

    @Test(description = "SLTC_61 :- Verify restaurant is serviceable when the current sla is less than the max sla at zone_rain_params of restaurant zone with 'rain mode:4'")
    public void rainsAtRestAndCustRainMode4GD2ltSla()
    {
        String rain_mode="4";
        double restZoneMaxSla = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("zone_max_sla").toString());
        String lastMileAtArea = DeliveryDataBaseUtils.getMaxLastMileForArea(area_id.toString());
        String lastMileAtRestaurant = DeliveryDataBaseUtils.getMaxLastMileForRestaurant(restaurantId);

        DeliveryDataBaseUtils.setMaxLastMileForArea(area_id.toString(), "10.0");
        DeliveryDataBaseUtils.setMaxLastMileForRestaurant(restaurantId, "10.0");
        Double degradationState = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select degradation_state from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("degradation_state").toString());
        if(degradationState != 2)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", "2", rain_mode, String.valueOf(zone_id));


        Map<String,String> customerDetails = deliveryDataHelper.latLonNotInGivenTagsButInZone(restLat,restLon, 1.5,"LONG_DISTANCE");
        String custlat=customerDetails.get("lat");
        String custlon=customerDetails.get("lon");
        String custzoneid=customerDetails.get("zone_id");
        String restRainMode = DeliveryDataBaseUtils.getZoneRainMode(zone_id.toString());
        if(!restRainMode.equals(rain_mode))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rain_mode,zone_id.toString());
        String custRainMode = DeliveryDataBaseUtils.getZoneRainMode(custzoneid);
        if(custRainMode.equals(rain_mode))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rain_mode,custzoneid);


        if(restZoneMaxSla < 100)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "100", rain_mode, zone_id.toString());
        String maxLmAtRestZoneRainParam = DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("last_mile").toString();
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", "10.0", rain_mode, zone_id.toString());

        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {custlat, custlon, city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode","1");
            put("serviceability","2");
            put("long_distance","0");
        }});
        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.setMaxLastMileForArea(area_id.toString(), lastMileAtArea);
        DeliveryDataBaseUtils.setMaxLastMileForRestaurant(restaurantId, lastMileAtRestaurant);
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(restRainMode, zone_id.toString());
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(custRainMode, custzoneid);
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", String.valueOf(degradationState), rain_mode, String.valueOf(zone_id));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla",String.valueOf(restZoneMaxSla),rain_mode, zone_id.toString());

        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", maxLmAtRestZoneRainParam, rain_mode, zone_id.toString());

        Assert.assertEquals(mappedResponse.get("rain_mode"), "1", "rain mode");
        Assert.assertEquals(degradationMode, "2", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceability"), "2", "serviceability");
    }


    @Test(description = "SLTC_62 :- Verify restaurant is not serviceable when the current sla is greater than the max sla at zone_rain_params of restaurant zone with 'rain mode:4'")
    public void rainsAtRestAndCustRainMode4GD2gtSla()
    {
        String rain_mode="4";
        double restZoneMaxSla = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("zone_max_sla").toString());
        String lastMileAtArea = DeliveryDataBaseUtils.getMaxLastMileForArea(area_id.toString());
        String lastMileAtRestaurant = DeliveryDataBaseUtils.getMaxLastMileForRestaurant(restaurantId);

        DeliveryDataBaseUtils.setMaxLastMileForArea(area_id.toString(), "10.0");
        DeliveryDataBaseUtils.setMaxLastMileForRestaurant(restaurantId, "10.0");
        Double degradationState = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select degradation_state from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("degradation_state").toString());
        if(degradationState != 2)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", "2", rain_mode, String.valueOf(zone_id));

        Map<String,String> customerDetails = deliveryDataHelper.latLonNotInGivenTagsButInZone(restLat,restLon, 1.5,"LONG_DISTANCE");
        String custlat=customerDetails.get("lat");
        String custlon=customerDetails.get("lon");
        String custzoneid=customerDetails.get("zone_id");
        String restRainMode = DeliveryDataBaseUtils.getZoneRainMode(zone_id.toString());
        if(!restRainMode.equals(rain_mode))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rain_mode,zone_id.toString());
        String custRainMode = DeliveryDataBaseUtils.getZoneRainMode(custzoneid);
        if(custRainMode.equals(rain_mode))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rain_mode,custzoneid);


        if(restZoneMaxSla > 10)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "10", rain_mode, zone_id.toString());
        String maxLmAtRestZoneRainParam = DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("last_mile").toString();
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", "10.0", rain_mode, zone_id.toString());

        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {custlat, custlon, city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode","1");
            put("serviceability","1");
            put("non_serviceable_reason","1");
            put("long_distance","0");
        }});
        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.setMaxLastMileForArea(area_id.toString(), lastMileAtArea);
        DeliveryDataBaseUtils.setMaxLastMileForRestaurant(restaurantId, lastMileAtRestaurant);
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(restRainMode, zone_id.toString());
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(custRainMode, custzoneid);
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", String.valueOf(degradationState), rain_mode, String.valueOf(zone_id));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla",String.valueOf(restZoneMaxSla),rain_mode, zone_id.toString());

        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", maxLmAtRestZoneRainParam, rain_mode, zone_id.toString());

        Assert.assertEquals(mappedResponse.get("rain_mode"), "1", "rain mode");
        Assert.assertEquals(degradationMode, "2", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceability"), "1", "serviceability");
        Assert.assertEquals(mappedResponse.get("non_serviceable_reason"), "1", "non_serviceable_reason");
    }



    @Test(description = "SLTC_63 :- Verify restaurant is serviceable when the distance from customer to restaurant is less than the max last mile at zone_rain_params of restaurant zone with 'rain mode:4'")
    public void rainsAtRestAndCustRainMode4GD3ltLM()
    {
        rain_mode = "4";
        double restZoneMaxSla = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("zone_max_sla").toString());
        String restZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(zone_id.toString());
        if(!restZoneRainMode.equals(rain_mode))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rain_mode, zone_id.toString());
        String custZoneRainMode = DeliveryDataBaseUtils.getZoneRainMode(defaultCustomerZone);
        if(!custZoneRainMode.equals(rain_mode))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rain_mode, defaultCustomerZone);
        if(restZoneMaxSla < 200)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "200", rain_mode, zone_id.toString());
        Double degradationState = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select degradation_state from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("degradation_state").toString());
        if(degradationState != 3)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", "3", rain_mode, String.valueOf(zone_id));

        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroListing(10,1000,new String[]
                {defaultCustomerLat.toString(), defaultCustomerLon.toString(), city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode","5");
            put("serviceability","2");
            put("long_distance","0");
        }});
        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(restZoneRainMode, zone_id.toString());
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(custZoneRainMode, defaultCustomerZone);
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", String.valueOf(degradationState), rain_mode, String.valueOf(zone_id));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla",String.valueOf(restZoneMaxSla),rain_mode, zone_id.toString());

        Assert.assertEquals(mappedResponse.get("rain_mode"), "5");
        Assert.assertEquals(degradationMode, "3");
        Assert.assertEquals(mappedResponse.get("serviceability"), "2");
    }


    @Test(description = "SLTC_64 :- Verify restaurant is not serviceable when the distance from customer to restaurant is greater than the max last mile at zone_rain_params of restaurant zone with 'rain mode:4'")
    public void rainsAtRestAndCustRainMode4GD3gtLM()
    {
        String rain_mode="4";
        double restZoneMaxSla = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("zone_max_sla").toString());
        String lastMileAtArea = DeliveryDataBaseUtils.getMaxLastMileForArea(area_id.toString());
        String lastMileAtRestaurant = DeliveryDataBaseUtils.getMaxLastMileForRestaurant(restaurantId);

        DeliveryDataBaseUtils.setMaxLastMileForArea(area_id.toString(), "10.0");
        DeliveryDataBaseUtils.setMaxLastMileForRestaurant(restaurantId, "10.0");
        Double degradationState = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select degradation_state from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("degradation_state").toString());
        if(degradationState != 3)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", "3", rain_mode, String.valueOf(zone_id));


        Map<String,String> customerDetails = deliveryDataHelper.latLonNotInGivenTagsButInZone(restLat,restLon, 1.5,"LONG_DISTANCE");
        String custlat=customerDetails.get("lat");
        String custlon=customerDetails.get("lon");
        String custzoneid=customerDetails.get("zone_id");
        String restRainMode = DeliveryDataBaseUtils.getZoneRainMode(zone_id.toString());
        if(!restRainMode.equals(rain_mode))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rain_mode,zone_id.toString());
        String custRainMode = DeliveryDataBaseUtils.getZoneRainMode(custzoneid);
        if(custRainMode.equals(rain_mode))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rain_mode,custzoneid);


        if(restZoneMaxSla < 200)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "200", rain_mode, zone_id.toString());
        String maxLmAtRestZoneRainParam = DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("last_mile").toString();
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", "1.0", rain_mode, zone_id.toString());

        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {custlat, custlon, city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode","5");
            put("serviceability","1");
            put("non_serviceable_reason","0");
            put("long_distance","0");
        }});
        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.setMaxLastMileForArea(area_id.toString(), lastMileAtArea);
        DeliveryDataBaseUtils.setMaxLastMileForRestaurant(restaurantId, lastMileAtRestaurant);
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(restRainMode, zone_id.toString());
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(custRainMode, custzoneid);
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", String.valueOf(degradationState), rain_mode, String.valueOf(zone_id));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla",String.valueOf(restZoneMaxSla),rain_mode, zone_id.toString());

        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", maxLmAtRestZoneRainParam, rain_mode, zone_id.toString());


        Assert.assertEquals(mappedResponse.get("rain_mode"), "5", "rain mode");
        Assert.assertEquals(degradationMode, "3", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceability"), "1", "serviceability");
        Assert.assertEquals(mappedResponse.get("non_serviceable_reason"), "0", "non serviceable reason");
    }

    @Test(description = "SLTC_65 :- Verify restaurant is serviceable when the current sla is less than the max sla at zone_rain_params of restaurant zone with 'rain mode:4'")
    public void rainsAtRestAndCustRainMode4GD3ltSla()
    {
        String rain_mode="4";
        double restZoneMaxSla = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("zone_max_sla").toString());
        String lastMileAtArea = DeliveryDataBaseUtils.getMaxLastMileForArea(area_id.toString());
        String lastMileAtRestaurant = DeliveryDataBaseUtils.getMaxLastMileForRestaurant(restaurantId);

        DeliveryDataBaseUtils.setMaxLastMileForArea(area_id.toString(), "10.0");
        DeliveryDataBaseUtils.setMaxLastMileForRestaurant(restaurantId, "10.0");
        Double degradationState = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select degradation_state from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("degradation_state").toString());
        if(degradationState != 3)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", "3", rain_mode, String.valueOf(zone_id));


        Map<String,String> customerDetails = deliveryDataHelper.latLonNotInGivenTagsButInZone(restLat,restLon, 1.5,"LONG_DISTANCE");
        String custlat=customerDetails.get("lat");
        String custlon=customerDetails.get("lon");
        String custzoneid=customerDetails.get("zone_id");
        String restRainMode = DeliveryDataBaseUtils.getZoneRainMode(zone_id.toString());
        if(!restRainMode.equals(rain_mode))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rain_mode,zone_id.toString());
        String custRainMode = DeliveryDataBaseUtils.getZoneRainMode(custzoneid);
        if(custRainMode.equals(rain_mode))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rain_mode,custzoneid);


        if(restZoneMaxSla < 100)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "100", rain_mode, zone_id.toString());
        String maxLmAtRestZoneRainParam = DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("last_mile").toString();
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", "10.0", rain_mode, zone_id.toString());

        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {custlat, custlon, city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode","5");
            put("serviceability","2");
            put("long_distance","0");
        }});
        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.setMaxLastMileForArea(area_id.toString(), lastMileAtArea);
        DeliveryDataBaseUtils.setMaxLastMileForRestaurant(restaurantId, lastMileAtRestaurant);
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(restRainMode, zone_id.toString());
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(custRainMode, custzoneid);
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", String.valueOf(degradationState), rain_mode, String.valueOf(zone_id));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla",String.valueOf(restZoneMaxSla),rain_mode, zone_id.toString());

        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", maxLmAtRestZoneRainParam, rain_mode, zone_id.toString());

        Assert.assertEquals(mappedResponse.get("rain_mode"), "5", "rain mode");
        Assert.assertEquals(degradationMode, "3", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceability"), "2", "serviceability");
    }


    @Test(description = "SLTC_66 :- Verify restaurant is not serviceable when the current sla is greater than the max sla at zone_rain_params of restaurant zone with 'rain mode:4'")
    public void rainsAtRestAndCustRainMode4GD3gtSla()
    {
        String rain_mode="4";
        double restZoneMaxSla = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("zone_max_sla").toString());
        String lastMileAtArea = DeliveryDataBaseUtils.getMaxLastMileForArea(area_id.toString());
        String lastMileAtRestaurant = DeliveryDataBaseUtils.getMaxLastMileForRestaurant(restaurantId);

        DeliveryDataBaseUtils.setMaxLastMileForArea(area_id.toString(), "10.0");
        DeliveryDataBaseUtils.setMaxLastMileForRestaurant(restaurantId, "10.0");
        Double degradationState = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select degradation_state from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("degradation_state").toString());
        if(degradationState != 3)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", "3", rain_mode, String.valueOf(zone_id));

        Map<String,String> customerDetails = deliveryDataHelper.latLonNotInGivenTagsButInZone(restLat,restLon, 1.5,"LONG_DISTANCE");
        String custlat=customerDetails.get("lat");
        String custlon=customerDetails.get("lon");
        String custzoneid=customerDetails.get("zone_id");
        String restRainMode = DeliveryDataBaseUtils.getZoneRainMode(zone_id.toString());
        if(!restRainMode.equals(rain_mode))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rain_mode,zone_id.toString());
        String custRainMode = DeliveryDataBaseUtils.getZoneRainMode(custzoneid);
        if(custRainMode.equals(rain_mode))
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rain_mode,custzoneid);


        if(restZoneMaxSla > 10)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "10", rain_mode, zone_id.toString());
        String maxLmAtRestZoneRainParam = DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("last_mile").toString();
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", "10.0", rain_mode, zone_id.toString());

        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {custlat, custlon, city_id.toString(), restaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode","5");
            put("serviceability","1");
            put("non_serviceable_reason","1");
            put("long_distance","0");
        }});
        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.setMaxLastMileForArea(area_id.toString(), lastMileAtArea);
        DeliveryDataBaseUtils.setMaxLastMileForRestaurant(restaurantId, lastMileAtRestaurant);
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(restRainMode, zone_id.toString());
        DeliveryDataBaseUtils.setZoneRainModeAndClearCache(custRainMode, custzoneid);
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", String.valueOf(degradationState), rain_mode, String.valueOf(zone_id));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla",String.valueOf(restZoneMaxSla),rain_mode, zone_id.toString());

        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", maxLmAtRestZoneRainParam, rain_mode, zone_id.toString());

        Assert.assertEquals(mappedResponse.get("rain_mode"), "5", "rain mode");
        Assert.assertEquals(degradationMode, "3", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceability"), "1", "serviceability");
        Assert.assertEquals(mappedResponse.get("non_serviceable_reason"), "1", "non_serviceable_reason");
    }


    public static List<String> getStartTime()
    {
        Date date = new Date();
        Date date1 = new Date();
        int dt = date.getDate();
        date.setDate(dt + 1);
        String startTimeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(date);
        int dt1 = date1.getDate();
        date1.setDate(dt1 + 1);
        int min = date1.getMinutes();
        date1.setMinutes(min+5);
        String endTimeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(date1);

        System.out.println("****** START TIME SLOT ******* " + startTimeStamp + " *** END TIME SLOT *** " + endTimeStamp);
        return Arrays.asList(startTimeStamp, endTimeStamp);

    }


    public String getValueFromDBusingQeury(String query, String attributeName)
    {
        return SystemConfigProvider.getTemplate(DeliveryConstant.dbname).queryForMap(query).get(attributeName).toString();
    }

    public String updateDBusingQeury(String query)
    {
        return String.valueOf(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(query));
    }


    public double areaLastMile(String areaId)
    {
        return Double.parseDouble(serviceablility_Helper.getValueFromDb("last_mile_cap", "area", "id", areaId));
    }

    public double restaurantLastMile(String restaurantId)
    {
        return Double.parseDouble(serviceablility_Helper.getValueFromDb("max_second_mile", "restaurant", "id", lDrestaurantId));
    }

    public double LDLastMile(String lDPolygonId)
    {
        return Double.parseDouble(serviceablility_Helper.getValueFromDb("max_last_mile", "long_distance", "polygon_id", lDPolygonId));
    }

    //is_long_distance_enabled:false & enabled:true => response
    //enabled:false => empty response


    public void updateLmAtAreaRestaurantAndLD(String nonLDLm, String LDlm)
    {
        serviceablility_Helper.setValueInDb("area", "last_mile_cap", String.valueOf(nonLDLm), "id", String.valueOf(area_id));
        serviceablility_Helper.setValueInDb("restaurant", "max_second_mile", String.valueOf(nonLDLm), "id", lDrestaurantId);
        serviceablility_Helper.setValueInDb("long_distance", "max_last_mile", String.valueOf(LDlm), "polygon_id", String.valueOf(lDPolygonId));
        //serviceablility_Helper.setValueInDb("restaurant", "max_long_distance_last_mile", String.valueOf(Double.parseDouble(LDlm) + 1.0), "id", String.valueOf(lDrestaurantId));
    }

    public void updateLDmaxSla(String maxSla, String longDistanceId)
    {
        serviceablility_Helper.setValueInDb("long_distance", "max_sla", maxSla, "id", longDistanceId);
    }




    public Map<String, String> getLdDisabledRestaurantInfo()
    {
        solrUrl = deliveryHelperMethods.getSolrUrl();
        String solrquery = "area_id:" + area_id + " AND is_long_distance_enabled:false AND enabled:true AND city_id:" + city_id;
        SolrDocumentList solrDocuments = deliveryDataHelper.getSolrOutput(solrUrl,core,solrquery,100);
        System.out.println(solrDocuments);
        Map<String, String> map = new LinkedHashMap<String, String>();
        JSONObject restObject = null;
        try {
            restObject = new JSONArray(JSONUtil.toJSON(solrDocuments)).getJSONObject(0);
            String ldDisabledrestLatLon = restObject.get("place").toString();
            map.put("ldDisabledrestLat", ldDisabledrestLatLon.split(",")[0]);
            map.put("ldDisabledrestLong", ldDisabledrestLatLon.split(",")[1]);
            map.put("ldDisabledrestaurantId", restObject.get("id").toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return map;
    }


    public Map<String, Integer> setNoRainsAtRestaurantAndCustomer(String restZoneId, String custZoneId)
    {
        Integer restRainMode = 0, customerRainMode = 0;
        Map<String, Integer> rainModeMap = new LinkedHashMap<String, Integer>();
        if(restZoneId != null)
        {
            restRainMode = getRainMode(restZoneId);
            rainModeMap.put("restaurantRainMode", restRainMode);
            if(restRainMode != 3)
                serviceablility_Helper.setNoRainsAtZone(restZoneId);
        }
        if(custZoneId != null)
        {
            customerRainMode = getRainMode(String.valueOf(custZoneId));
            rainModeMap.put("customerRainMode", customerRainMode);
            if(customerRainMode != 3)
                serviceablility_Helper.setNoRainsAtZone(String.valueOf(custZoneId));
        }
        System.out.println("***** MAP ****** " + rainModeMap);
        return rainModeMap;
    }

    //Return a list of latlons inside LD polygon

    public List<String> findPointInsideLDPolygonLatLon()
    {
        List<Integer> matchingPolygonIds = GetMatchingPolygonId();
        List<Map<Integer,String>> finalMapList = getCentroidlatLongFromPolygonId(matchingPolygonIds);
        Map<Integer,String> centroidMap = finalMapList.get(0);
        Map<Integer,String> LMMap = finalMapList.get(1);

        System.out.println("Printing Centroid Map");
        printMap(centroidMap);
        System.out.println("Printing LM Map");
        printMap(LMMap);

        List<String> ldLatLonList = new ArrayList<>();
        Integer ldArea = Integer.parseInt(SystemConfigProvider.getTemplate(DeliveryConstant.dbname).queryForMap("select area_code from restaurant where id="+lDrestaurantId).get("area_code").toString());
        SystemConfigProvider.getTemplate(DeliveryConstant.dbname).update("update restaurant set max_second_mile=4 where id="+lDrestaurantId);
        serviceablility_Helper.clearCache("restaurant", lDrestaurantId);
        SystemConfigProvider.getTemplate(DeliveryConstant.dbname).update("update area set last_mile_cap=4 where id="+ldArea);
        serviceablility_Helper.clearCache("area", String.valueOf(ldArea));

        Double lastMileRestaurantCap = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.dbname).
                queryForMap("select a.last_mile_cap from restaurant r inner join area a on a.id=r.area_code  where r.id="+lDrestaurantId).get("last_mile_cap").toString());
        Double lastMileAreaCap = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.dbname).
                queryForMap("select max_second_mile from restaurant where id="+lDrestaurantId).get("max_second_mile").toString());
        Double lm_area_cap = Math.min(lastMileAreaCap,lastMileRestaurantCap);

        System.out.println("lastMileRestaurantCap"+ lastMileRestaurantCap);
        System.out.println("lastMileAreaCap"+ lastMileAreaCap);
        System.out.println("lm_area_cap"+ lm_area_cap);
		/*        //generate random_value between max size of centroid map and 0
        Random r = new Random();
        int Low = 0;
        int High = centroidMap.size()-1;
        int randInt = r.nextInt(High-Low) + Low;
        List<Integer> test=new ArrayList<>(centroidMap.keySet());
        System.out.println(test.size());
        String centroidLatLon=centroidMap.get(test.get(randInt));
        Double fromLat=Double.parseDouble(centroidLatLon.split(",")[0]);
        Double fromLon=Double.parseDouble(centroidLatLon.split(",")[1]);
        String zoneid=getCustomerZoneFromLatLon(fromLat,fromLon);
        if(zoneid=="null"||zoneid=="")
        {
            continue;
        }*/
        /*
        * Iterating each centroid list of polygon Ids and finding out wether the goven coordinate lies in a zone
        * if zone is not found , we continue till we find one
        * once found, we calculate distance from centrold to ldrestaurant lat long, and set the LD distance as +1 in long_distance table
        * */
		for(Integer polygonId : centroidMap.keySet())
        {
            String centroidLatLon = centroidMap.get(polygonId);
            Double fromLat = Double.parseDouble(centroidLatLon.split(",")[0]);
            Double fromLon = Double.parseDouble(centroidLatLon.split(",")[1]);
            String zoneid = getCustomerZoneFromLatLon(fromLat,fromLon);
            System.out.println("***** zone ID **** " + zoneid);
            if(zoneid == null || zoneid == "")
            {
                continue;
            }
            System.out.println();
            Integer distance = deliveryDataHelper.getDistance(lDrestLat,lDrestLon,fromLat,fromLon);
            Double coordDistance =Double.parseDouble(distance.toString())/1000;
            System.out.println("found LM area cap as "+ lm_area_cap+"found distance from centroid as "+coordDistance+"found LM in polygon Core as "+Double.parseDouble(LMMap.get(polygonId)));
            //lm_area_cap is set to a value
            //now update lmarea_cap set value to coordinate distance +1
            String id = SystemConfigProvider.getTemplate(DeliveryConstant.dbname).queryForMap("select id from long_distance where polygon_id='" + polygonId + "'").get("id").toString();
            SystemConfigProvider.getTemplate(DeliveryConstant.dbname).update("update long_distance set max_last_mile="+(coordDistance+3.0)+",max_sla=200 where polygon_id="+polygonId);
            serviceablility_Helper.clearCache("long_distance", id);
            //hit the cache invalidate API here
			/*           if(coordDistance>lm_area_cap && coordDistance<Double.parseDouble(LMMap.get(polygonId)))
           {*/
            System.out.println("found the lat long ");
            ldLatLonList.add(fromLat.toString()+","+fromLon.toString());
            // }

            lDPolygonId=polygonId;
            break;
            //get customer zone from latlong
            //get mastmile from restaurant and latlon
            //checkwether sitance is greater than latmile normal and less than lstmile LD
        }
        System.out.println("Printing final CoordinateList");
        printList(ldLatLonList);
        return ldLatLonList;
    }


    public String getResponseDegradationMode(Map<String, String> mappedResponse)
    {
        String degradationMode = "";
        try {
            degradationMode =  (new JSONObject(mappedResponse.get("degradation"))).get("mode").toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return degradationMode;
    }

    public String getResponseDegradationCause(Map<String, String> mappedResponse)
    {
        String degradationMode = "";
        try {
            degradationMode =  (new JSONObject(mappedResponse.get("degradation"))).get("cause").toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return degradationMode;
    }


    /*
     * Method which finds a random restaurant in a given area which is long distance enabled
     * taking a fixed distance with variable value as 6.5(ldDistance), find all Long Distance polygon Ids
     * and return the list of poygon IDs
     * */
    public List<Integer> GetMatchingPolygonId()
    {
        List<Integer> polygonIdList = new ArrayList<>();
        solrUrl = deliveryHelperMethods.getSolrUrl();
        System.out.println(solrUrl + "Printing solr URL");


        //String polygonCore="polygon";
        String solrquery = "area_id:" + area_id + " AND is_long_distance_enabled:true AND city_id:" + city_id;
        SolrDocumentList solrDocuments = deliveryDataHelper.getSolrOutput(solrUrl,core,solrquery,100);
        System.out.println(solrDocuments);
        JSONObject restObject = null;
        try {
            restObject = new JSONArray(JSONUtil.toJSON(solrDocuments)).getJSONObject(0);
            lDrestLatLon = restObject.get("place").toString();
            lDrestLat = Double.parseDouble(lDrestLatLon.split(",")[0]);
            lDrestLon = Double.parseDouble(lDrestLatLon.split(",")[1]);
            lDrestaurantId = restObject.get("id").toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Double angle = 0.0;
        Integer polygonId = 0;
        for(int i = 0; i < 72; i++)
        {
            Double[] latlonarray = deliveryDataHelper.getLatLng(lDrestLat,lDrestLon,angle,LdDistance);
            Integer id = deliveryDataHelper.checkIFPointIsInLDPloygon(latlonarray[0],latlonarray[1]);

            if(id != 0)
            {
                polygonId = id;
                polygonIdList.add(id);
                System.out.println("found Polygon ID " + id);

            }
            angle = angle+5.0;

        }
        System.out.println("Printing Polygon Id List");
        printList(polygonIdList);
        return polygonIdList;
        //get all Ls
        //find coordinates at 5 degree angle

    }

    public void printList(List<?> somelist)
    {
        for(Object o:somelist)
        {
            System.out.println("List Values" +o.toString());
        }

    }

/*Method that takes the List of polygon Ids a d return a List of map containing
polygon id:latlon
polygon id:LM
* */

    public List<Map<Integer,String>>  getCentroidlatLongFromPolygonId(List<Integer> poygonIdList)
    {
        Map<Integer,String> allPolygonLatLonMap=new HashMap<>();
        Map<Integer,String> allPolygonLastMileMap=new HashMap<>();
        List<Map<Integer,String>> mapList=new ArrayList<>();
        solrUrl = deliveryHelperMethods.getSolrUrl();
        System.out.println(solrUrl+"Printing solr URL");
        for(Integer polygonId:poygonIdList) {
            String solrquery = "id:" + polygonId;
            //String solrquery="id:2727";
            SolrDocumentList solrDocuments = deliveryDataHelper.getSolrOutput(solrUrl, "polygon", solrquery, 1);
            System.out.println(solrDocuments);
            JSONObject polygonObject = null;
            List<String> pathList = new ArrayList<>();
            try {
                System.out.println(JSONUtil.toJSON(solrDocuments) + "XXXXXXX");
                polygonObject = (JSONObject) (new JSONArray(JSONUtil.toJSON(solrDocuments)).get(0));
                String polygonlatlons = polygonObject.getString("latLongs").replace("[", "").replace("]", "").replace("\"", "");
                String lastMile=polygonObject.getString("maxLastMile");
                allPolygonLastMileMap.put(polygonId,lastMile);
                String[] latlonarrray = polygonlatlons.split("\\s*,\\s*");
                for (String str : latlonarrray) {
                    pathList.add(str.split(" ")[0] + "," + str.split(" ")[1]);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String polygoncentre = deliveryDataHelper.getCentroid(pathList);
            System.out.println(polygoncentre);
            allPolygonLatLonMap.put(polygonId,polygoncentre);
        }
        mapList.add(allPolygonLatLonMap);
        printMap(allPolygonLatLonMap);
        mapList.add(allPolygonLastMileMap);
        return mapList;
    }


    public void printMap(Map<?,?> printMap)
    {
        for(Object str:printMap.keySet())
        {
            System.out.println("**********************************************************************************");
            System.out.println(str+"::::"+printMap.get(str).toString());
        }
    }


    public void setRainModeAndCause(Integer actualRainMode, Integer expectedRainMode, Integer zone_id)
    {
        if(actualRainMode != expectedRainMode)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(expectedRainMode), "id", String.valueOf(zone_id));
        String cause = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select cause from zone_rain_params where zone_id=" + zone_id + " and rain_mode='" + expectedRainMode + "'").get("cause").toString();
        if(!cause.equals("rain") || cause == null)
            setCauseRain(zone_id, expectedRainMode);
    }


    public void setCauseRain(Integer zoneId, Integer rainMode)
    {
        String id = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select id from zone_rain_params where zone_id=" + zoneId + " and rain_mode='" + rainMode + "'").toString();
        SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update("update zone_rain_params set cause='rain' where zone_id='" + zoneId + "' and rain_mode='" + rainMode + "'");
        serviceablility_Helper.clearCache("zone_rain_params", id);
    }



    public Map<String,String> getServiceabiltyResponseMap(String response)
    {
        JSONObject mappedResponse=null;
        JSONObject mappedResponsecutomerObject=null;
        Map<String,String> responseMap=new HashMap<String,String>();
        try {
            JSONObject resObject = new JSONObject(response);
            mappedResponse=((JSONArray)(resObject.get("serviceableRestaurants"))).getJSONObject(0).getJSONObject("listingServiceabilityResponse");
            if(resObject.has("customerInfo"))
            {
                mappedResponsecutomerObject=((JSONObject)(resObject.get("customerInfo")));
                Iterator<?> it=mappedResponsecutomerObject.keys();
                while (it.hasNext())
                {
                    String key = (String)it.next();
                    responseMap.put(key,mappedResponsecutomerObject.getString(key));
                }
            }
            Iterator<?> it=mappedResponse.keys();
            while (it.hasNext())
            {
                String key = (String)it.next();
                responseMap.put(key,mappedResponse.getString(key));
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        return responseMap;
    }




    public String getCustomerZoneFromLatLon(Double lat,Double lon)
    {
        SolrDocumentList solrDocuments = deliveryDataHelper.getSolrOutput(solrUrl,"user_zone","polygon:\"Intersects(" + lat.toString() + "  " + lon.toString() + ")\"");
        String jsonDoc = JSONUtil.toJSON(solrDocuments);
        System.out.println("**** json doc **** " + jsonDoc);
        String customerZoneId=null;
        try{
			/*        	 JSONArray test=new JSONArray(jsonDoc);
        	 System.out.println("JsonArray "+test);
        	 JSONObject test1=(JSONObject)test.get(0);
        	 System.out.println(" "+test1);
        	 customerZoneId =test1.get("id").toString();
        	 System.out.println(" "+customerZoneId);*/
            customerZoneId =((JSONObject)((new JSONArray(jsonDoc)).get(0))).get("id").toString();
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        return customerZoneId;
    }

    public Integer getRainMode(String zone_id)
    {
        return Integer.parseInt(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).
                queryForMap( "select rain_mode_type from zone where id="+zone_id).get("rain_mode_type").toString());
    }


    public Map<String,String> returnMap(String key, String value)
    {
        Map<String, String> map = new LinkedHashMap<String, String>();
        map.put(key, value);
        return map;
    }



}
