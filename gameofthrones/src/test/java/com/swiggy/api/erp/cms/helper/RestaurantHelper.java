package com.swiggy.api.erp.cms.helper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jayway.jsonpath.JsonPath;
import com.rabbitmq.client.AMQP;
import com.swiggy.api.erp.cms.CreateAddons;
import com.swiggy.api.erp.cms.constants.CmsConstants;
import com.swiggy.api.erp.cms.constants.Constants;
import com.swiggy.api.erp.cms.constants.MenuConstants;
import com.swiggy.api.erp.cms.constants.RestaurantConstants;
import com.swiggy.api.erp.cms.pojo.AddItemToRest.AddItemToRestaurant;
import com.swiggy.api.erp.cms.pojo.AddItemToRest.RestBuilder;
import com.swiggy.api.erp.cms.pojo.Category;
import com.swiggy.api.erp.cms.pojo.CreateAddonGroup;
import com.swiggy.api.erp.cms.pojo.CreateVariant;
import com.swiggy.api.erp.cms.pojo.Restaurant;
import com.swiggy.api.erp.cms.pojo.VariantGroups.Entity;
import com.swiggy.api.erp.cms.pojo.VariantGroups.VariantGroup;
import com.swiggy.api.sf.checkout.helper.CommonAPIHelper;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.DBHelper;
import framework.gameofthrones.Tyrion.JsonHelper;
import framework.gameofthrones.Tyrion.RabbitMQHelper;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;

/**
 * Created by kiran.j on 1/22/18.
 */
public class RestaurantHelper {

    Initialize gameofthrones = new Initialize();
    RabbitMQHelper rmqHelper = new RabbitMQHelper();
    List<Integer> restIds = new ArrayList<>();

    public HashMap<String, String> getRestaurantMap(Restaurant restaurant) {
        Gson gson = new GsonBuilder().create();
        String json = gson.toJson(restaurant);
        HashMap<String,String> map = new Gson().fromJson(json, HashMap.class);
        return map;
    }

    public Processor createRestaurant(Restaurant restaurant) {
        GameOfThronesService service = new GameOfThronesService("cmstest", "cmsapi", gameofthrones);
        HashMap<String, String> headers = new HashMap<>();
        HashMap<String, String> form = getRestaurantMap(restaurant);
        headers.put("Content-Type", CmsConstants.formdata_contenttype);
        headers.put("WF-CMS-KEY", CmsConstants.wf_cms_key);
        return new Processor(service, headers, null, null, form);
    }

    public int createDefaultRestaurant() {
       Restaurant restaurant = new Restaurant();
        restaurant.build();
        restaurant.setCity("1");
        restaurant.setArea("1");

        restaurant.setName("TestName"+System.currentTimeMillis());
        restaurant.setName("abcd"+System.currentTimeMillis());
        Processor processor = createRestaurant(restaurant);
        if(processor.ResponseValidator.GetNodeValueAsInt(CmsConstants.code) == CmsConstants.rest_status_code) {
            int restId = processor.ResponseValidator.GetNodeValueAsInt(CmsConstants.swiggy_id);
            restIds.add(restId);
            return restId;
        }
        return 0;
        /*Restaurant restaurant = new Restaurant();
        restaurant.build();
        //restaurant.setAgreement_type(agreementType);
        RestaurantHelper rs=new RestaurantHelper();
        Processor processor = rs.createRestaurant(restaurant);
        if(processor.ResponseValidator.GetNodeValueAsInt(CmsConstants.code) == CmsConstants.rest_status_code) {
            int restId = processor.ResponseValidator.GetNodeValueAsInt(CmsConstants.swiggy_id);
            return (restId);
        }
        System.out.println("--RestID creation did not work properly ::: ");
        return 0;*/
    }

    public int createDefaultRestaurant(String restName) {
        Restaurant restaurant = new Restaurant();
        restaurant.build();
        restaurant.setCity("1");
        restaurant.setArea("1");
        restaurant.setName(restName);
        Processor processor = createRestaurant(restaurant);
        if(processor.ResponseValidator.GetNodeValueAsInt(CmsConstants.code) == CmsConstants.rest_status_code) {
            int restId = processor.ResponseValidator.GetNodeValueAsInt(CmsConstants.swiggy_id);
            restIds.add(restId);
            return restId;
        }
        return 0;}

    public String getRandomLatLong(double x0, double y0) {
        Random random = new Random();
        int radius = 6371000;
        double radiusInDegrees = radius / 111000f;

        double u = random.nextDouble();
        double v = random.nextDouble();
        double w = radiusInDegrees * Math.sqrt(u);
        double t = 2 * Math.PI * v;
        double x = w * Math.cos(t);
        double y = w * Math.sin(t);
        double new_x = x / Math.cos(Math.toRadians(y0));

        double foundLongitude = new_x + x0;
        double foundLatitude = y + y0;
        return foundLatitude+","+foundLongitude;
    }

    public RestaurantHelper addRestaurantTimeSlots(int restId) {
        TimeSlotsHelper timeSlotsHelper = new TimeSlotsHelper();
        String days[] = CmsConstants.days;
        for(String day: days) {
            timeSlotsHelper.postTimeSlots(Integer.toString(restId), CmsConstants.open_time, CmsConstants.close_time, day);
        }
        return this;
    }

    public int getMenuIdForRestId(int restaurantId) {
        //List<Map<String, Object>> cms = cmsHelper.getMenuMap(Integer.toString(restaurantId));
        CMSHelper cmsHelper = new CMSHelper();
        String query = Constants.menumap_query+Integer.toString(restaurantId);
        System.out.println("QUERY ::: "+query);
        DBHelper.pollDB(Constants.cmsDB,query,"restaurant_id",Integer.toString(restaurantId),4,60);
        //Thread.sleep(2000);
        System.out.println("Rest_ID ::: "+restaurantId+" -- MenuID ::: ");
        int menuId = Integer.parseInt(cmsHelper.getMenuMap(Integer.toString(restaurantId)).get(0).get(CmsConstants.menuid).toString());
        return menuId;
    }

    public int createCat(int rest_id) {
        CMSHelper cmsHelper = new CMSHelper();
        int menu_id = getMenuIdForRestId(rest_id);
        //String menu_id = cmsHelper.getMenuMap(Integer.toString(rest_id)).get(0).get(CmsConstants.menuid).toString();
        Processor processor = cmsHelper.createCategory(createCategoryObject(rest_id, String.valueOf(menu_id)));
        String cat_id = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data.categories[*].id").toString().replace("[","").replace("]","");
        return Integer.parseInt(cat_id);
    }

    public int createCat(Category category, int rest_id) {
        CMSHelper cmsHelper = new CMSHelper();
        String menu_id = cmsHelper.getMenuMap(Integer.toString(rest_id)).get(0).get(CmsConstants.menuid).toString();
        category.setRestaurant_id(rest_id);
        category.setType(CmsConstants.cat);
        category.setParent_category_id(Integer.parseInt(menu_id)).build();
        Processor processor = cmsHelper.createCategory(category);
        String cat_id = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data.categories[*].id").toString().replace("[","").replace("]","");
        return Integer.parseInt(cat_id);
    }

    public int createSingelCategory(int rest_id) {
        CMSHelper cmsHelper = new CMSHelper();
        String menu_id = cmsHelper.getMenuMap(Integer.toString(rest_id)).get(0).get(CmsConstants.menuid).toString();
        Processor processor = cmsHelper.createCategoryForResturant(createCategoryObject(rest_id, menu_id));
        String cat_id = ""+processor.ResponseValidator.GetNodeValueAsInt("$.data.id");
        return Integer.parseInt(cat_id);
    }

    public int createsubcat(int rest_id) {
        CMSHelper cmsHelper = new CMSHelper();
        CommonAPIHelper commonAPIHelper = new CommonAPIHelper();
        String menu_id = cmsHelper.getMenuMap(Integer.toString(rest_id)).get(0).get(CmsConstants.menuid).toString();
        List<Map<String, Object>> categories_map = cmsHelper.getMenuTaxonomy(menu_id);
        List<String> categories = new ArrayList<>();
        for(Map<String, Object> map : categories_map) {
            categories.add(map.get(CmsConstants.id).toString());
        }
        int random = commonAPIHelper.getRandomNo(0, categories.size()-1);
        Processor processor = cmsHelper.createCategory(createSubCategoryObject(rest_id, categories.get(random)));
        String subcat_id = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data.categories[*].id").toString().replace("[","").replace("]","");
        return Integer.parseInt(subcat_id);
    }

    public int createPopCat(int rest_id) {
        CMSHelper cmsHelper = new CMSHelper();
        String menu_id = cmsHelper.getPopMenuMap(Integer.toString(rest_id)).get(0).get(CmsConstants.menuid).toString();
        Processor processor = cmsHelper.createCategory(createCategoryObject(rest_id, menu_id));
        String cat_id = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data.categories[*].id").toString().replace("[","").replace("]","");
        return Integer.parseInt(cat_id);
    }


    public Category createCategoryObject(int rest_id, String menu_id) {
        Category category = new Category();
        category.setRestaurant_id(rest_id).setType(CmsConstants.cat).setParent_category_id(Integer.parseInt(menu_id)).build();
        return category;
    }

    public Category createSubCategoryObject(int rest_id, String subcat_id) {
        Category category = new Category();
        category.setRestaurant_id(rest_id).setType(CmsConstants.subcat).setParent_category_id(Integer.parseInt(subcat_id)).build();
        return category;
    }

    public int createsubcat(int rest_id, int cat_id) {
        CMSHelper cmsHelper = new CMSHelper();
        CommonAPIHelper commonAPIHelper = new CommonAPIHelper();
        Processor processor = cmsHelper.createCategoryForResturant(createSubCategoryObject(rest_id, Integer.toString(cat_id)));
        String subcat_id = ""+processor.ResponseValidator.GetNodeValueAsInt("$.data.id");
        return Integer.parseInt(subcat_id);
    }

    public int createsubcat(Category subCategory, int rest_id, int cat_id) {
        CMSHelper cmsHelper = new CMSHelper();
        CommonAPIHelper commonAPIHelper = new CommonAPIHelper();
        subCategory.setRestaurant_id(rest_id).setType(CmsConstants.subcat).setParent_category_id(cat_id);
        Processor processor = cmsHelper.createCategory(subCategory);
        String subcat_id = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data.categories[*].id").toString().replace("[","").replace("]","");
        return Integer.parseInt(subcat_id);
    }

    public int createPopsubcat(int rest_id, int cat_id) {
        CMSHelper cmsHelper = new CMSHelper();
        CommonAPIHelper commonAPIHelper = new CommonAPIHelper();
        Processor processor = cmsHelper.createCategory(createSubCategoryObject(rest_id, Integer.toString(cat_id)));
        String subcat_id = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data.categories[*].id").toString().replace("[","").replace("]","");
        return Integer.parseInt(subcat_id);
    }

    public String createItem(int rest_id, int cat_id, int subcat_id) {
        Processor processor = null;
        try {
            CMSHelper cmsHelper = new CMSHelper();
            RestBuilder restBuilder = new RestBuilder();
            AddItemToRestaurant itemToRestaurant = restBuilder.buildItemSlot();
            itemToRestaurant.setCategoryId(Integer.toString(subcat_id));
            itemToRestaurant.setMainCategoryId(cat_id);
            itemToRestaurant.setRestaurantId(rest_id);
            processor = cmsHelper.createItem(itemToRestaurant);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return processor.ResponseValidator.GetNodeValue("data.uniqueId");
    }

    public String createItem(AddItemToRestaurant item, int rest_id, int cat_id, int subcat_id) {
        Processor processor = null;
        try {
            CMSHelper cmsHelper = new CMSHelper();
            RestBuilder restBuilder = new RestBuilder();
            item.setCategoryId(Integer.toString(subcat_id));
            item.setMainCategoryId(cat_id);
            item.setRestaurantId(rest_id);
            processor = cmsHelper.createItem(item);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return processor.ResponseValidator.GetNodeValue("data.uniqueId");
    }

    public void createVariantGroup(String item_id, String rest_name, String third_party_id ) {
        BaseServiceHelper baseServiceHelper = new BaseServiceHelper();
        baseServiceHelper.createVariantGroup(item_id, rest_name, CmsConstants.order, third_party_id);
    }

    public int createRestaurantAndEnable() throws InterruptedException {
        boolean flag = false;
        CMSHelper cmsHelper = new CMSHelper();
        SANDHelper sandHelper = new SANDHelper();
        int rest_id = createDefaultRestaurant();
        addRestaurantTimeSlots(rest_id);
        Thread.sleep(5000);
        //int rest_id = 45003;
        String menu_id = cmsHelper.getMenuMap(Integer.toString(rest_id)).get(0).get(CmsConstants.menuid).toString();
        int cat = createCat(rest_id);
        flag = cat > 0;
        int subcat = createsubcat(rest_id, cat);
        flag = subcat > 0;
        String item_id = createItem(rest_id, cat, subcat);
        cmsHelper.setLiveDate(rest_id);
        cmsHelper.updateCuisine(Integer.toString(rest_id), CmsConstants.cuisine);
        cmsHelper.insertCuisine(rest_id, CmsConstants.cuisinse_id);
        //cmsHelper.setLatLong(CmsConstants.lat+","+CmsConstants.longi, rest_id);
        cmsHelper.setLatLong("9.4570095,76.6149755", rest_id);
        cmsHelper.setRestaurantEnabled(Integer.toString(rest_id), 1);
        sandHelper.solrIndexing(Integer.toString(rest_id));
        return rest_id;
    }


    public void enableRestaurant(int rest_id, String lat_lng) {
        CMSHelper cmsHelper = new CMSHelper();
        SANDHelper sandHelper = new SANDHelper();
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(cal.getTime());
        addRestaurantTimeSlots(rest_id);
        cmsHelper.setLiveDate(Integer.toString(rest_id), date);
        cmsHelper.updateCuisine(Integer.toString(rest_id), CmsConstants.cuisine);
        cmsHelper.insertCuisine(rest_id, CmsConstants.cuisinse_id);
        //cmsHelper.setLatLong(CmsConstants.lat+","+CmsConstants.longi, rest_id);
        if(lat_lng == null)
            cmsHelper.setLatLong("9.4570095,76.6149755", rest_id);
        else
            cmsHelper.setLatLong(lat_lng, rest_id);
        cmsHelper.setRestaurantEnabled(Integer.toString(rest_id), 1);
        sandHelper.solrIndexing(Integer.toString(rest_id));
    }

    public Entity buildVariantGroup(String item_id, String name, String third_party_id) {
        VariantGroup variantGroup = new VariantGroup();
        variantGroup.build();
        Entity entity = variantGroup.getEntity();
        entity.setItem_id(item_id);
        entity.setName(name);
        entity.setId(null);
        entity.setThird_party_id(third_party_id);
        return entity;
    }

    public CreateVariant buildVariant(String variant_group_id , String name, String price, String third_party_id) {
        CreateVariant createVariant = new CreateVariant();
        createVariant.build();
        createVariant.setVariant_group_id(variant_group_id);
        createVariant.setName(name);
        createVariant.setPrice(price);
        createVariant.setThird_party_id(third_party_id);
        return createVariant;
    }

    public CreateAddonGroup buildAddonGroup(String item_id, String name, String third_party_id) {
        CreateAddonGroup addonGroup = new CreateAddonGroup();
        addonGroup.build();
        addonGroup.setItem_id(item_id);
        addonGroup.setName(name);
        addonGroup.setId(null);
        addonGroup.setThird_party_id(third_party_id);
        return addonGroup;
    }

    public CreateAddons buildAddon(String rest_id, String addon_group_ids, String price, String third_party_id) {
        CreateAddons addons = new CreateAddons();
        addons.build();
        addons.setRestaurant_id(rest_id);
        addons.setAddon_group_ids(new String[]{addon_group_ids});
        addons.setPrice(price);
        addons.setThird_party_id(third_party_id);
        return addons;
    }

    public Processor createVariantGroup(Entity entity) throws IOException {
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        JsonHelper jsonHelper = new JsonHelper();
        GameOfThronesService gots = new GameOfThronesService("cms_menu", "CMS_CREATE_VARIANTGROUP_POJO", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, new String[]{jsonHelper.getObjectToJSON(entity)});
        return processor;
    }

    public Processor createAddon_Group(CreateAddonGroup createAddonGroup) throws IOException {
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        JsonHelper jsonHelper = new JsonHelper();
        GameOfThronesService gots = new GameOfThronesService("cms_menu", "CMS_CREATE_ADDONGROUP_POJO", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, new String[]{jsonHelper.getObjectToJSON(createAddonGroup)});
        return processor;
    }

    public Processor createVariant(CreateVariant createVariant) throws IOException {
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        JsonHelper jsonHelper = new JsonHelper();
        GameOfThronesService gots = new GameOfThronesService("cms_menu", "CMS_CREATE_VARIANT_POJO", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, new String[]{jsonHelper.getObjectToJSON(createVariant)});
        return processor;
    }

    public Processor createAddon(CreateAddons createAddons) throws IOException {
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        JsonHelper jsonHelper = new JsonHelper();
        GameOfThronesService gots = new GameOfThronesService("cms_menu", "CMS_CREATE_ADDON_POJO", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, new String[]{jsonHelper.getObjectToJSON(createAddons)});
        return processor;
    }

    public String generateId() {
        return String.valueOf(Instant.now().getEpochSecond());
    }

    public void createPopItemAndSchedule(int rest_id, int cat_id, int subcat_id, String slot_type, int area_id, int openTime, int closeTime, String day,
                                           String menu_type, String date) {
        Processor processor = null;
        try {
            CMSHelper cmsHelper = new CMSHelper();
            RestBuilder restBuilder = new RestBuilder();
            AddItemToRestaurant itemToRestaurant = restBuilder.buildItemSlot();
            itemToRestaurant.setCategoryId(Integer.toString(subcat_id));
            itemToRestaurant.setMainCategoryId(cat_id);
            itemToRestaurant.setRestaurantId(rest_id);
            itemToRestaurant.setType(MenuConstants.pop_item);
            processor = cmsHelper.createItem(itemToRestaurant);
            String item_id = processor.ResponseValidator.GetNodeValue("data.uniqueId");
            POP_Item_Schedule_Mapping_By_Area_Helper pop_item_schedule_mapping_by_area_helper = new POP_Item_Schedule_Mapping_By_Area_Helper();
            pop_item_schedule_mapping_by_area_helper.createItemScheduleByArea_helper(Integer.parseInt(item_id), date, 2, 100, slot_type, area_id, openTime, closeTime,
                    day, menu_type);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void popItemSchedule(int item_id, int area_id, int openTime, int closeTime, String day, String slot_type,
                                String menu_type, String date, RestaurantConstants.pop_type pop_type) {
        try {
            POP_Item_Schedule_Mapping_By_Area_Helper pop_item_schedule_mapping_by_area_helper = new POP_Item_Schedule_Mapping_By_Area_Helper();
            pop_item_schedule_mapping_by_area_helper.createItemScheduleByArea_helper(item_id, date, 2, 100, slot_type, area_id, openTime, closeTime,
                    day, menu_type);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void pushRestaurantEvent(String rest_id) throws IOException {
        Gson gson = new Gson();
        CMSHelper cmsHelper = new CMSHelper();
        JsonHelper jsonHelper = new JsonHelper();
        String json = gson.toJson(cmsHelper.getRestaurantDetails(rest_id));
        File file = new File("../Data/Payloads/JSON/createCityArea");
        String queue = FileUtils.readFileToString(file);
        queue = queue.replace("${action}", RestaurantConstants.update).replace("${data}", json).replace("${model}", RestaurantConstants.restaurant);
        System.out.println(queue);
        rmqHelper.pushMessageToExchange("oms", "swiggy.catalog_restaurant_exchange", new AMQP.BasicProperties().builder().contentType("application/json"), queue);
    }

    public void updateRestaurantisAssured(String rest_id, boolean value) throws IOException {
        CMSHelper cmsHelper = new CMSHelper();
        cmsHelper.setIsSwiggyAssured(rest_id, (value == true)? 1 : 0);
        pushRestaurantEvent(rest_id);
    }

    public void updateRestaurantLongDistance(String rest_id, boolean value) throws IOException {
        CMSHelper cmsHelper = new CMSHelper();
        cmsHelper.setIsLongDistanceEnabled(rest_id, (value == true) ? 1 : 0);
        pushRestaurantEvent(rest_id);
    }

}
