package com.swiggy.api.erp.finance.tests;

import com.swiggy.api.erp.finance.constants.FinConstants;
import com.swiggy.api.erp.finance.constants.FinanceConstants;
import com.swiggy.api.erp.finance.helper.FinanceHelper;
import com.swiggy.api.sf.checkout.helper.CommonAPIHelper;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import org.codehaus.jackson.JsonNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;
import com.swiggy.api.erp.finance.helper.finHelper;
import com.swiggy.api.erp.finance.dp.FinDataProvide;
import org.testng.asserts.SoftAssert;

public class FinTest extends FinDataProvide {

    finHelper finance = new finHelper();
    SchemaValidatorUtils schemavalidator = new SchemaValidatorUtils();
    FinanceHelper helper = new FinanceHelper();
    private static Logger log = LoggerFactory.getLogger(FinTest.class);

    @Test (dataProvider = "getOrderId", description = "Get Reconcilation Status")
    public void reconOrder(String orderId)
    {
        Processor process = finance.getReconOrder(orderId);
        String resp = process.ResponseValidator.GetBodyAsText();
        System.out.println(resp);
    }

    @Test(dataProvider = "getOrder", groups = {"Finance", "FinanceRegression","smoke","sanity","regression"}, description = "Tests Recon Status of Undelivered Orders", enabled = false)
    public void undeliveredOrder(String orderId) {
        SoftAssert softassert = new SoftAssert();
        Processor processor = finance.getReconOrder(orderId);
        softassert.assertTrue(finance.validateUndeliveredRecon(processor));
        softassert.assertTrue(schemavalidator.validateSchema(processor, FinConstants.undelivered_schema));
        softassert.assertAll();
    }

    @Test(dataProvider = "cancelorder",groups = {"Finance", "FinanceRegression","smoke","sanity","regression"}, description = "Tests Recon Status of Cancelled Orders")
    public void cancelledorderRecon(String orderId,String food_prepared) {
        SoftAssert softassert = new SoftAssert();
        Processor processor = finance.getReconOrder(orderId);
        switch (food_prepared) {
            case "false": softassert.assertTrue(finance.validateUndeliveredRecon(processor));
                break;

            case "true":    finance.waitForReconStatus(orderId, FinConstants.autoreconciled);
                            processor = finance.getReconOrder(orderId);
                            softassert.assertEquals(finance.getReconStatus(processor), FinConstants.autoreconciled);
                            break;
        }
        softassert.assertAll();
    }

    @Test (dataProvider = "update_de_Status",groups = {"Finance", "FinanceRegression","smoke","sanity","regression"}, description = "Tests Recon Status with different type of Orders")
    public void reconDeliveredMismatch(String orderId, String food_prepared)
    {
        SoftAssert softassert = new SoftAssert();
        Processor processor;
        switch (food_prepared) {
            case "true":    finance.waitForReconStatus(orderId, FinConstants.unreconciled);
                            processor = finance.getReconOrder(orderId);
                            softassert.assertEquals(finance.getReconStatus(processor), FinConstants.autoreconciled);
                            break;

            case "false":   finance.waitForReconStatus(orderId, FinConstants.autoreconciled);
                            processor = finance.getReconOrder(orderId);
                            softassert.assertEquals(finance.getReconStatus(processor), FinConstants.autoreconciled);
                            break;
        }
        softassert.assertAll();
    }

    @Test(dataProvider = "getOrderId",groups = {"Finance", "FinanceRegression","smoke","sanity","regression"}, description = "Tests Recon Status By Cancelling Recon")
    public void cancelRecon(String orderId) {
        SoftAssert softassert = new SoftAssert();
        finance.waitForReconStatus(orderId, FinConstants.autoreconciled);
        finance.cancelRecon(orderId);
        Processor processor = finance.getReconOrder(orderId);
        softassert.assertEquals(finance.getReconStatus(processor), FinConstants.cancelled);
        //softassert.assertTrue(schemavalidator.validateSchema(processor, FinConstants.undelivered_schema));
        softassert.assertAll();
    }

    @Test(dataProvider = "submitorder",groups = {"Finance", "FinanceRegression","smoke","sanity","regression"}, description = "Tests Recon Status By Updating Order Values")
    public void submitRecon(String orderId) {
        SoftAssert softassert = new SoftAssert();
        try {
            finance.waitForReconStatus(orderId, FinConstants.unreconciled);
            Processor processor = finance.submitRecon(new String[]{orderId, FinConstants.item_total, FinConstants.packing_charges, FinConstants.packaging_reason});
            softassert.assertTrue(schemavalidator.validateSchema(processor, FinConstants.submit_schema));
            System.out.println("Response is"+processor.ResponseValidator.GetBodyAsText());
            finance.waitForReconStatus(orderId, FinConstants.reconciled);
            softassert.assertEquals(finance.getReconStatus(processor), FinConstants.reconciled);
            Processor recon_response = finance.getReconOrder(orderId);
            JsonNode json = CommonAPIHelper.convertStringtoJSON(recon_response.ResponseValidator.GetBodyAsText());
            softassert.assertTrue(json.get("actuals").get("packing_charges").asDouble() == FinConstants.packe_charges);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    @Test(dataProvider = "submitorder",groups = {"Finance", "FinanceRegression","smoke","sanity","regression"}, description = "Tests Recon Status By Updating Cancelled Order Values")
    public void submitReconCancel(String orderId) {
        SoftAssert softassert = new SoftAssert();
        try {
            finance.waitForReconStatus(orderId, FinConstants.unreconciled);
            Processor processor = finance.submitRecon(new String[]{orderId, FinConstants.item_total, FinConstants.packing_charges, FinConstants.packaging_reason});
            finance.waitForReconStatus(orderId, FinConstants.reconciled);
            softassert.assertEquals(finance.getReconStatus(processor), FinConstants.reconciled);
            finance.cancelRecon(orderId);
            Processor recon_response = finance.getReconOrder(orderId);
            softassert.assertEquals(finance.getReconStatus(recon_response), FinConstants.cancelled);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    @Test(description = "Tests Recon Health Check", groups = {"Finance", "FinanceRegression","smoke","sanity"}, enabled = false)
    public void reconCheck() {
        SoftAssert softassert = new SoftAssert();
        Processor processor = finance.getReconHealthCheck();
        softassert.assertEquals(processor.ResponseValidator.GetResponseCode(), FinConstants.status);
        softassert.assertTrue(schemavalidator.validateSchema(processor, FinConstants.reconhealth));
        softassert.assertAll();
    }

    @Test(description = "Tests Recon Version", groups = {"Finance", "FinanceRegression","smoke","sanity"})
    public void reconVersion() {
        SoftAssert softassert = new SoftAssert();
        Processor processor = finance.getReconVersion();
        softassert.assertEquals(processor.ResponseValidator.GetResponseCode(), FinConstants.status);
        //softassert.assertTrue(schemavalidator.validateSchema(processor, FinConstants.reconversion));
        softassert.assertAll();
    }

    @Test(dataProvider = "invoicejob", description = "Tests Create Invoice Job", groups = {"Finance", "FinanceRegression","smoke","sanity"})
    public void invoiceJob(String from_date, String to_date, String user_id, String orderID) {
        SoftAssert softAssert = new SoftAssert();
        log.info("--From ::: "+from_date+" --To ::: "+to_date);
        String payoutID = "";
        Processor p = finance.getCurrentPayout(from_date,to_date,FinanceConstants.user_id,FinanceConstants.city_id);
        int get_status = p.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        softAssert.assertEquals(get_status,FinanceConstants.statusOne, "Status not one");
        String taskID = helper.getTaskIdFromReconCurrentPayout(from_date,to_date);
        boolean pollstatus = helper.pollStatusFromReconCurrentPayout(from_date,to_date,"2");
        if (pollstatus == true) {
            payoutID = helper.getPayoutIdFromRestaurantCurrentPayouts(taskID);
        } else
            softAssert.assertTrue(false, "Status in recon invoice jobs was not 2 (status=done)");
        log.info("--taskID ::: "+taskID);
        log.info("--payoutID ::: "+payoutID);
        Processor processor = finance.invoiceJob(from_date, to_date, user_id, payoutID,FinanceConstants.city_id);
        softAssert.assertEquals(processor.ResponseValidator.GetResponseCode(), FinConstants.status);
        softAssert.assertTrue(finance.isInvoiceJobPresent(to_date),"Job was not created");
        softAssert.assertAll();
    }

    @Test(dataProvider = "invoicecancel", description = "Tests Cancel Invoice by Task Id", groups = {"Finance", "FinanceRegression","smoke","sanity"})
    public void invoiceCancel(String taskid, String user) {
        SoftAssert softassert = new SoftAssert();
        Processor processor = finance.invoiceCancel(taskid, user);
        softassert.assertEquals(processor.ResponseValidator.GetResponseCode(), FinConstants.status);
        softassert.assertAll();
    }

    @Test(dataProvider = "invoiceemail", description = "Tests Send Invoice through Email", groups = {"Finance", "FinanceRegression","smoke","sanity"})
    public void invoiceEmail(String taskid, String email) {
        SoftAssert softassert = new SoftAssert();
        Processor processor = finance.invoiceEmail(taskid, email);
        softassert.assertEquals(processor.ResponseValidator.GetResponseCode(), FinConstants.status);
        softassert.assertAll();
    }

    @Test(dataProvider = "reconconfig", description = "Tests Configuration for Reconcilation", groups = {"Finance", "FinanceRegression","smoke","sanity"})
    public void reconConfig(String key, String value) {
        SoftAssert softassert = new SoftAssert();
        Processor processor = finance.getReconConfig(key, value);
        softassert.assertEquals(processor.ResponseValidator.GetResponseCode(), FinConstants.status);
        softassert.assertAll();
    }

    @Test(dataProvider = "manualinvoice", description = "Tests Manual Invoice", groups = {"Finance", "FinanceRegression","smoke","sanity"})
    public void manualInvoice(String file_url, String user) {
        SoftAssert softassert = new SoftAssert();
        //int count = finance.getManualInvoiceCount();
        Processor processor = finance.fileUpload(file_url, user);
        softassert.assertEquals(processor.ResponseValidator.GetResponseCode(), FinConstants.status);
        //softassert.assertTrue(count < finance.getManualInvoiceCount());
        softassert.assertAll();
    }

    @Test(dataProvider = "currentpayout", description = "Tests Restaurant Current Payouts", groups = {"Finance", "FinanceRegression","smoke","sanity"}, enabled = false)
    public void restaurantCurrentPayout(String from_time, String to_time, String user) {
        SoftAssert softassert = new SoftAssert();
        Processor processor = finance.getCurrentPayout(from_time, to_time, user, FinanceConstants.city_id);
        softassert.assertEquals(processor.ResponseValidator.GetResponseCode(), FinConstants.status);
        softassert.assertTrue(schemavalidator.validateSchema(processor, FinConstants.restcurrentpayout));
        softassert.assertAll();
    }

    @Test(dataProvider = "reconlogout", description = "Tests Reconcilation Specialist Logout", groups = {"Finance", "FinanceRegression","smoke","sanity"}, enabled = false)
    public void reconcilationSpecialistLogout(String file_url,  String user) {
        SoftAssert softassert = new SoftAssert();
        Processor processor = finance.specialistLogout(file_url, user);
        softassert.assertEquals(processor.ResponseValidator.GetResponseCode(), FinConstants.status);
        softassert.assertTrue(schemavalidator.validateSchema(processor, FinConstants.reconlogout));
        softassert.assertAll();
    }

    @Test(dataProvider = "addquickrecon", description = "Tests Quick Add Reconcilation", groups = {"Finance", "FinanceRegression","smoke","sanity"}, enabled = false)
    public void addQuickRecon(String file_url,  String user) {
        SoftAssert softassert = new SoftAssert();
        Processor processor = finance.addQuickRecon(file_url, user);
        softassert.assertEquals(processor.ResponseValidator.GetResponseCode(), FinConstants.status);
        softassert.assertTrue(schemavalidator.validateSchema(processor, FinConstants.quickrecon));
        softassert.assertAll();
    }


    @Test(dataProvider = "autoassignment", description = "Tests Reconcilation Auto Assignment of Orders", groups = {"Finance", "FinanceRegression","smoke","sanity"})
    public void reconAutoAssignment(String user) {
        SoftAssert softassert = new SoftAssert();
        Processor processor = finance.getReconAutoAssignment(user);
        softassert.assertEquals(processor.ResponseValidator.GetResponseCode(), FinConstants.status);
        //softassert.assertTrue(schemavalidator.validateSchema(processor, FinConstants.autoassign));
        softassert.assertAll();
    }

    @Test(dataProvider = "addannexure", description = "Tests Add Annexure for Reconcilation", groups = {"Finance", "FinanceRegression","smoke","sanity"}, enabled=false)
    public void addAnnexure(String file_url, String user) {
        SoftAssert softassert = new SoftAssert();
        Processor processor = finance.invoiceAddAnexure(file_url, user);
        softassert.assertEquals(processor.ResponseValidator.GetResponseCode(), FinConstants.status);
        softassert.assertTrue(schemavalidator.validateSchema(processor, FinConstants.addannexure));
        softassert.assertAll();
    }

    @Test(dataProvider = "restaurantpendingapproval",description = "Tests Restaurant Pending Approval", groups = {"Finance", "FinanceRegression","smoke"}, enabled = false)
    public void restaurantPendingApproval(String pageno) {
        SoftAssert softassert = new SoftAssert();
        Processor processor = finance.restaurantPendingApproval(pageno);
        softassert.assertEquals(processor.ResponseValidator.GetResponseCode(), FinConstants.status);
        softassert.assertTrue(schemavalidator.validateSchema(processor, FinConstants.restpendingapproval));
        softassert.assertAll();
    }

    @Test(dataProvider = "swiggypendingapproval",description = "Tests Restaurant Pending Approval", groups = {"Finance", "FinanceRegression","smoke"}, enabled = false)
    public void swiggyPendingApproval(String pageno) {
        SoftAssert softassert = new SoftAssert();
        Processor processor = finance.swiggyPendingApproval(pageno);
        softassert.assertEquals(processor.ResponseValidator.GetResponseCode(), FinConstants.status);
        softassert.assertTrue(schemavalidator.validateSchema(processor, FinConstants.restpendingapproval));
        softassert.assertAll();
    }

    @Test(dataProvider = "restaurantidpendingapproval",description = "Tests Restaurant Pending Approval from Restaurant ID", groups = {"Finance", "FinanceRegression","smoke"}, enabled = false)
    public void restaurantPendingApprovalByRestId(String pageno, String restId) {
        SoftAssert softassert = new SoftAssert();
        Processor processor = finance.restaurantPendingApprovalRestid(pageno, restId);
        softassert.assertEquals(processor.ResponseValidator.GetResponseCode(), FinConstants.status);
        softassert.assertTrue(schemavalidator.validateSchema(processor, FinConstants.restidpendingapproval));
        softassert.assertAll();
    }

    @Test(dataProvider = "createreports",description = "Tests Create Reports for a particular time period", groups = {"Finance", "FinanceRegression","smoke"}, enabled=false)
    public void createReports(String from_date, String to_date, String user, String type) {
        SoftAssert softassert = new SoftAssert();
        Processor processor = finance.createReports(from_date, to_date, user, type);
        softassert.assertEquals(processor.ResponseValidator.GetResponseCode(), FinConstants.status);
        softassert.assertTrue(schemavalidator.validateSchema(processor, FinConstants.createreports));
        softassert.assertAll();
    }

    @Test(dataProvider = "restaurantpendingpayout",description = "Tests Restaurant Pending Payout", groups = {"Finance", "FinanceRegression","smoke"}, enabled = false)
    public void restaurantPendingPayout(String from, String to, String exclude) {
        SoftAssert softassert = new SoftAssert();
        Processor processor = finance.getRestaurantPendingPayout(from, to, exclude);
        softassert.assertEquals(processor.ResponseValidator.GetResponseCode(), FinConstants.status);
        softassert.assertAll();
    }

    @Test(dataProvider = "restaurantreleasepayout",description = "Tests Restaurant Release Payout", groups = {"Finance", "FinanceRegression","smoke"}, enabled = false)
    public void restaurantReleasePayout(String from, String to, String user, String rest_list) {
        SoftAssert softassert = new SoftAssert();
        Processor processor = finance.getRestaurantReleasePayout(from, to, user, rest_list);
        softassert.assertEquals(processor.ResponseValidator.GetResponseCode(), FinConstants.status);
        softassert.assertAll();
    }

    @Test(dataProvider = "swiggypendingpayout",description = "Tests Create Reports for a particular time period", groups = {"Finance", "FinanceRegression","smoke"}, enabled = false)
    public void swiggyPendingPayout(String from, String to, String pageno, String pagesize, String excludelist) {
        SoftAssert softassert = new SoftAssert();
        Processor processor = finance.getSwiggyPendingPayout(from, to, pageno, pagesize, excludelist);
        softassert.assertEquals(processor.ResponseValidator.GetResponseCode(), FinConstants.status);
        softassert.assertAll();
    }

    @Test(dataProvider = "swiggyreleasepayout",description = "Tests Swiggy Release Payout", groups = {"Finance", "FinanceRegression","smoke"}, enabled = false)
    public void swiggyRelasePayout(String from, String to, String user, String restlist) {
        SoftAssert softassert = new SoftAssert();
        Processor processor = finance.getSwiggyReleasePayout(from, to, user, restlist);
        softassert.assertEquals(processor.ResponseValidator.GetResponseCode(), FinConstants.status);
        softassert.assertAll();
    }

    @Test(dataProvider = "nodalpayout",description = "Tests Restaurnat Nodal Payout", groups = {"Finance", "FinanceRegression","smoke"}, enabled = false)
    public void nodalPayout(String cityId, String from, String to) {
        SoftAssert softassert = new SoftAssert();
        Processor processor = finance.nodalpayout(cityId, from, to);
        softassert.assertEquals(processor.ResponseValidator.GetResponseCode(), FinConstants.status);
        softassert.assertAll();
    }

}
