package com.swiggy.api.erp.delivery.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.erp.delivery.helper.*;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.Processor;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.util.*;

/**
 * Created by preetesh.sharma on 16/04/18.
 */
public class CAPRDFromFFNew {

    Integer zone_id = 34;
    String restlat;
    String restlon;
    String app_version;
    DeliveryDataHelper deliveryDataHelper;
    String de_id;
    Map<String, String> orderDefaultMap;
    String restaurant_id;
    Integer areacode;
    AutoassignHelper autoassignHelper;
    DeliveryServiceHelper deliveryServiceHelper;
    DeliveryHelperMethods deliveryHelperMethods;
    String de_auth;
    DeliveryControllerHelper deliveryControllerHelper;
    String city_id=null;


    public CAPRDFromFFNew() {

    }

    @BeforeSuite
    public void setData()
    {
        orderDefaultMap = new HashMap<>();
        deliveryDataHelper = new DeliveryDataHelper();
        autoassignHelper = new AutoassignHelper();
        deliveryServiceHelper = new DeliveryServiceHelper();
        deliveryHelperMethods = new DeliveryHelperMethods();
        deliveryControllerHelper = new DeliveryControllerHelper();
        de_id = deliveryDataHelper.CreateDE(zone_id);
        //de_id = "87526";
        app_version = "1.9";
        areacode = Integer.parseInt(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select id from area where zone_id=" + zone_id).get("id").toString());
        String latlon = getRestLatLon();
        restlat = latlon.split(",")[0];
        restlon = latlon.split(",")[1];
        restaurant_id = "10820";
        orderDefaultMap.put("restaurant_id", restaurant_id);
        orderDefaultMap.put("restaurant_lat_lng", latlon);
        orderDefaultMap.put("order_time", deliveryDataHelper.getcurrentDateTimefororderTime());
        orderDefaultMap.put("order_type", "regular");
        orderDefaultMap.put("restaurant_area_code", "4");
        city_id=SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select city_id from zone where id="+zone_id).get("city_id").toString();
        Object rest=SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select r.id from restaurant r inner join area a on r.area_code=a.id  where a.zone_id="+zone_id+" and a.enabled=1 and r.with_de=0 and r.partner_id is not null order by 1 desc limit 1;");
        if(rest!=null)
            restaurant_id=rest.toString();
        else
            Assert.assertTrue(false, "Cannot Find restaurant in Zone  "+zone_id);
    }



    public void loginDEandUpdateLocation() {
        autoassignHelper.delogin(de_id, app_version);
        Integer deotp = deliveryServiceHelper.getOtp(de_id, app_version);
        String response = autoassignHelper.deotp(de_id, deotp.toString()).ResponseValidator.GetBodyAsText();
        try {
            de_auth = ((JSONObject) ((new JSONObject(response)).get("data"))).getString("Authorization");
            System.out.println("de Auth is " + de_auth);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        autoassignHelper.locupdate(restlat, restlon, de_auth);

    }

    public String getOrderID(Map<String, String> orderDefaultMap) {
        String orderjson = deliveryDataHelper.returnOrderJson(orderDefaultMap);
        String order_id = null;
        if (orderjson != null) {
            try {
                order_id = new JSONObject(orderjson).get("order_id").toString();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (deliveryDataHelper.pushOrderToDelivery_Orders(orderjson)) {
                return order_id;
            }
        }
        return null;
    }

    public String getRestLatLon() {
        String path = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select path from zone where id=" + zone_id).get("path").toString();
        List<String> latlonllist = Arrays.asList(path.split(" "));
        String latlon = deliveryDataHelper.getCentroid(latlonllist);
        return latlon;
    }

    public Boolean checkAssignmentStatus(Processor processor, String order_id, String de_id) {
        if ((processor.ResponseValidator.GetResponseCode()) == 200) {
            String query = "Select de_id from trips where order_id=" + order_id;
            String deIdAssigned = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap(query).get("de_id").toString();
            return de_id.equalsIgnoreCase(deIdAssigned);
        }
        return false;
    }

    public Boolean orderCreationAndAssignmentstatus(String order_id) {
        loginDEandUpdateLocation();
        Boolean defreestatus = deliveryServiceHelper.makeDEFree(de_id, app_version);
        Processor processor = null;
        if (defreestatus)
            processor = deliveryServiceHelper.assignOrder(order_id, de_id);
        else
            Assert.assertTrue(false, "Cant make de Free");
        deliveryDataHelper.waitInterval(3);
        Boolean assignmentStatus = checkAssignmentStatus(processor, order_id, de_id);
        return assignmentStatus;

    }

    public boolean confirmControllerStatus(String status, String order_id) {
        Boolean orderstatus = false;
        String regex = "\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}";
        switch (status) {
            case "confirmed": {
                String query = DeliveryConstant.zipconfirmquery + order_id;
                orderstatus = deliveryDataHelper.polldbWithRegex(DeliveryConstant.databaseName, query, "confirmed_time", regex, 2, 20);
                break;
            }
            case "arrived": {
                String query = DeliveryConstant.ziparrivequery + order_id;
                orderstatus = deliveryDataHelper.polldbWithRegex(DeliveryConstant.databaseName, query, "arrived_time", regex, 2, 20);
                break;
            }
            case "reached": {
                String query = DeliveryConstant.zipreachedquery + order_id;
                orderstatus = deliveryDataHelper.polldbWithRegex(DeliveryConstant.databaseName, query, "reached_time", regex, 2, 20);
                break;
            }
            case "pickedup": {
                String query = DeliveryConstant.zippickedupquery + order_id;
                orderstatus = deliveryDataHelper.polldbWithRegex(DeliveryConstant.databaseName, query, "pickedup_time", regex, 2, 20);
                break;
            }
            case "delivered": {
                String query = DeliveryConstant.zipdeliveredquery + order_id;
                orderstatus = deliveryDataHelper.polldbWithRegex(DeliveryConstant.databaseName, query, "delivered_time", regex, 2, 20);
                break;
            }
        }
        return orderstatus;

    }

    @Test(groups = "Regression", description = "Verify OE is able to mark order assigned from FF and same is reflected in delivery")
    public void CAPRDFROMFF334() {
        String order_id = getOrderID(orderDefaultMap);
        Assert.assertTrue(orderCreationAndAssignmentstatus(order_id), "Assignment never happened");
    }

    @Test(groups = "Regression", description = "Verify OE is able to mark order confirmed from FF once DE is assigned to order and same is reflected in delivery")
    public void CAPRDFROMFF335() {
        String order_id = getOrderID(orderDefaultMap);
        Boolean assignstatus = orderCreationAndAssignmentstatus(order_id);
        Processor processor = null;
        if (assignstatus)
            Assert.assertTrue(assignstatus,"Assignment failed for Order "+order_id+ " to de_id " +de_id);
            deliveryDataHelper.waitInterval(3);
            processor = deliveryControllerHelper.orderConfirmedFromController(order_id);
            Assert.assertTrue(confirmControllerStatus("confirmed", order_id));
        }

    @Test(groups = "Regression", description = "Verify OE is not able to mark order confirmed from FF if DE is not assigned to order and same should not be reflected in delivery")
    public void CAPRDFROMFF336() {
        String order_id = getOrderID(orderDefaultMap);
        loginDEandUpdateLocation();
        deliveryDataHelper.waitInterval(3);
        Processor processor = deliveryControllerHelper.orderConfirmedFromController(order_id);
        String response = processor.ResponseValidator.GetBodyAsText();
        Assert.assertEquals(JsonPath.read(response, "$.statusMessage").toString(), "can  not update the state from null to confirmed");
        Assert.assertFalse(confirmControllerStatus("confirmed", order_id));
    }

    @Test(groups = "Regression", description = "Verify OE is able to mark order arrived from FF if order status is confirmed and same is reflected in delivery")
    public void CAPRDFROMFF337() {
        String order_id = getOrderID(orderDefaultMap);
        loginDEandUpdateLocation();
        Processor processor = null;
        Assert.assertTrue(orderCreationAndAssignmentstatus(order_id), "order not assigned");
        deliveryDataHelper.waitInterval(3);
        processor = deliveryControllerHelper.orderConfirmedFromController(order_id);
        Assert.assertTrue(orderCreationAndAssignmentstatus(order_id), "order not confirmed");
        deliveryDataHelper.waitInterval(3);
        processor = deliveryControllerHelper.orderArrivedFromController(order_id);
        Assert.assertTrue(confirmControllerStatus("arrived", order_id));
    }

    @Test(groups = "Reresssion", description = "Verify OE is not able to mark order arrived from FF if order status is assigned and same should not be reflected in delivery")
    public void CAPRDFROMFF338() {
        List<String> statelist = new ArrayList<>();
        statelist.add("arrived");
        statelist.add("pickedup");
        statelist.add("reached");
        statelist.add("delivered");
        String order_id = getOrderID(orderDefaultMap);
        loginDEandUpdateLocation();
        Boolean defreestatus = deliveryServiceHelper.makeDEFree(de_id, app_version);
        Assert.assertTrue(defreestatus,"Cannot make de free");
        Processor processor1 = deliveryServiceHelper.assignOrder(order_id, de_id);
        Boolean assignStatus = checkAssignmentStatus(processor1, order_id, de_id);
        Assert.assertTrue(assignStatus, "Order not assigned to " + de_id);
        for (String state : statelist) {

                    switch (state) {
                        case "arrived": {
                            deliveryDataHelper.waitInterval(3);
                            Processor processor = deliveryControllerHelper.orderArrivedFromController(order_id);
                            Assert.assertFalse(confirmControllerStatus(state, order_id), "order status not changed in db");
                            String resp = processor.ResponseValidator.GetBodyAsText();
                            Assert.assertEquals(JsonPath.read(resp, "$.statusCode").toString(), "1");
                            Assert.assertEquals(JsonPath.read(resp, "$.statusMessage").toString(), "can  not update the state from assigned to arrived");
                            break;
                        }
                        case "pickedup": {
                            deliveryDataHelper.waitInterval(3);
                            Processor processor = deliveryControllerHelper.orderPickedupFromController(order_id);
                            Assert.assertFalse(confirmControllerStatus(state, order_id), "order status not changed in db");
                            String resp = processor.ResponseValidator.GetBodyAsText();
                            Assert.assertEquals(JsonPath.read(resp, "$.statusCode").toString(), "1");
                            Assert.assertEquals(JsonPath.read(resp, "$.statusMessage").toString(), "can  not update the state from assigned to pickedup");
                            break;
                        }
                        case "reached": {
                            deliveryDataHelper.waitInterval(3);
                            Processor processor = deliveryControllerHelper.orderReachedFromController(order_id);
                            Assert.assertFalse(confirmControllerStatus(state, order_id), "order status not changed in db");
                            String resp = processor.ResponseValidator.GetBodyAsText();
                            Assert.assertEquals(JsonPath.read(resp, "$.statusCode").toString(), "1");
                            Assert.assertEquals(JsonPath.read(resp, "$.statusMessage").toString(), "can  not update the state from assigned to reached");
                            break;
                        }
                        case "delivered": {
                            deliveryDataHelper.waitInterval(3);
                            Processor processor = deliveryControllerHelper.orderDeliveredFromController(order_id);
                            Assert.assertFalse(confirmControllerStatus(state, order_id), "order status not changed in db");
                            String resp = processor.ResponseValidator.GetBodyAsText();
                            Assert.assertEquals(JsonPath.read(resp, "$.statusCode").toString(), "1");
                            Assert.assertEquals(JsonPath.read(resp, "$.statusMessage").toString(), "can  not update the state from assigned to delivered");
                            break;
                        }
                    }
                }

    }


    @Test(groups = "Reresssion", description = "Verify OE is able to mark order pickedup from FF if order status is arrived and same is reflected in delivery")
    public void CAPRDFROMFF339() {
        String order_id = getOrderID(orderDefaultMap);
        loginDEandUpdateLocation();
        Processor processor = null;
        Assert.assertTrue(orderCreationAndAssignmentstatus(order_id),"Assignment failed for Order "+order_id+ " to de_id " +de_id);
        deliveryDataHelper.waitInterval(3);
        processor = deliveryControllerHelper.orderConfirmedFromController(order_id);
        Assert.assertFalse(confirmControllerStatus("confirmed", order_id), "Order not confirmed");
        deliveryDataHelper.waitInterval(3);
        processor = deliveryControllerHelper.orderArrivedFromController(order_id);
        Assert.assertTrue(confirmControllerStatus("arrived", order_id), "Cannot make order as arrived");
        deliveryDataHelper.waitInterval(3);
        processor = deliveryControllerHelper.orderPickedupFromController(order_id);
        Assert.assertTrue(confirmControllerStatus("pickedup", order_id));
    }






    @Test(groups = "CAPRDregressionMayur", description = "Verify OE is not able to mark order pickedup/reached/delivered from FF if order status is confirmed and same should not be reflected in delivery")
    public void CAPRDFROMFF340() {
        String order_id = getOrderID(orderDefaultMap);
        loginDEandUpdateLocation();
        Processor processor = null;
        Boolean confirmstatus = false;
        Assert.assertTrue(orderCreationAndAssignmentstatus(order_id),"Assignment failed for Order "+order_id+ " to de_id " +de_id);
        deliveryDataHelper.waitInterval(3);
        processor = deliveryControllerHelper.orderConfirmedFromController(order_id);
        Assert.assertTrue(confirmControllerStatus("confirmed", order_id), "Order not confirmed");
        deliveryDataHelper.waitInterval(3);
        processor = deliveryControllerHelper.orderPickedupFromController(order_id);
        Assert.assertFalse(confirmControllerStatus("pickedup", order_id), "order status not changed in db");
        String resp = processor.ResponseValidator.GetBodyAsText();
        Assert.assertEquals(JsonPath.read(resp, "$.statusCode").toString(), "1");
        Assert.assertEquals(JsonPath.read(resp, "$.statusMessage").toString(), "can  not update the state from confirmed to pickedup");
        deliveryDataHelper.waitInterval(3);
        processor = deliveryControllerHelper.orderReachedFromController(order_id);
        Assert.assertFalse(confirmControllerStatus("reached", order_id), "order status not changed in db");
        resp = processor.ResponseValidator.GetBodyAsText();
        Assert.assertEquals(JsonPath.read(resp, "$.statusCode").toString(), "1");
        Assert.assertEquals(JsonPath.read(resp, "$.statusMessage").toString(), "can  not update the state from confirmed to reached");
        deliveryDataHelper.waitInterval(3);
        processor = deliveryControllerHelper.orderDeliveredFromController(order_id);
        Assert.assertFalse(confirmControllerStatus("delivered", order_id), "order status not changed in db");
        resp = processor.ResponseValidator.GetBodyAsText();
        Assert.assertEquals(JsonPath.read(resp, "$.statusCode").toString(), "1");
        Assert.assertEquals(JsonPath.read(resp, "$.statusMessage").toString(), "can  not update the state from confirmed to delivered");
        }




    @Test(groups = "Regression", description = "Verify OE is able to mark order reached from FF if order status is pickedup and same is reflected in delivery")
    public void CAPRDFROMFF341() {
        String order_id = getOrderID(orderDefaultMap);
        loginDEandUpdateLocation();
        Processor processor = null;
        Assert.assertTrue(orderCreationAndAssignmentstatus(order_id),"Assignment failed for Order "+order_id+ " to de_id " +de_id);
        deliveryDataHelper.waitInterval(3);
        processor = deliveryControllerHelper.orderConfirmedFromController(order_id);
        Assert.assertTrue(confirmControllerStatus("confirmed", order_id), "Order not confirmed");
        deliveryDataHelper.waitInterval(3);
        processor = deliveryControllerHelper.orderArrivedFromController(order_id);
        Assert.assertTrue(confirmControllerStatus("arrived", order_id),"Order not arrived");
        deliveryDataHelper.waitInterval(3);
        processor = deliveryControllerHelper.orderPickedupFromController(order_id);
        Assert.assertTrue(confirmControllerStatus("pickedup", order_id),"Order not picked up");
        deliveryDataHelper.waitInterval(3);
        processor = deliveryControllerHelper.orderReachedFromController(order_id);
        Assert.assertTrue(confirmControllerStatus("reached", order_id));

    }

    @Test(groups = "Regression", description = "Verify OE is not able to mark order reached/delivered from FF if order status is arrived and same should not be reflected in delivery")
    public void CAPRDFROMFF342() {
        String order_id = getOrderID(orderDefaultMap);
        loginDEandUpdateLocation();
        Processor processor = null;
        Assert.assertTrue(orderCreationAndAssignmentstatus(order_id),"Assignment failed for Order "+order_id+ " to de_id " +de_id);
        deliveryDataHelper.waitInterval(3);
        processor = deliveryControllerHelper.orderConfirmedFromController(order_id);
        Assert.assertTrue(confirmControllerStatus("confirmed", order_id), "Order not confirmed");
        deliveryDataHelper.waitInterval(3);
        processor = deliveryControllerHelper.orderArrivedFromController(order_id);
        Assert.assertTrue(confirmControllerStatus("arrived", order_id),"Order not arrived");
        deliveryDataHelper.waitInterval(3);
        processor = deliveryControllerHelper.orderReachedFromController(order_id);
        Assert.assertFalse(confirmControllerStatus("reached", order_id), "order status not changed in db");
        String resp = processor.ResponseValidator.GetBodyAsText();
        Assert.assertEquals(JsonPath.read(resp, "$.statusCode").toString(), "1");
        Assert.assertEquals(JsonPath.read(resp, "$.statusMessage").toString(), "can  not update the state from arrived to reached");
        deliveryDataHelper.waitInterval(3);
        processor = deliveryControllerHelper.orderDeliveredFromController(order_id);
        Assert.assertFalse(confirmControllerStatus("delivered", order_id), "order status not changed in db");
        resp = processor.ResponseValidator.GetBodyAsText();
        Assert.assertEquals(JsonPath.read(resp, "$.statusCode").toString(), "1");
        Assert.assertEquals(JsonPath.read(resp, "$.statusMessage").toString(), "can  not update the state from arrived to delivered");
        }


    @Test(groups = "Regression", description = "Verify OE is able to mark order delivered from FF if order status is reached and same is reflected in delivery")
    public void CAPRDFROMFF344()
    {
        String order_id = getOrderID(orderDefaultMap);
        loginDEandUpdateLocation();
        Processor processor = null;
        Assert.assertTrue(orderCreationAndAssignmentstatus(order_id),"Assignment failed for Order "+order_id+ " to de_id " +de_id);
        deliveryDataHelper.waitInterval(3);
        processor = deliveryControllerHelper.orderConfirmedFromController(order_id);
        Assert.assertTrue(confirmControllerStatus("confirmed", order_id), "Order not confirmed");
        deliveryDataHelper.waitInterval(3);
        processor = deliveryControllerHelper.orderArrivedFromController(order_id);
        Assert.assertTrue(confirmControllerStatus("arrived", order_id),"Order not arrived");
        deliveryDataHelper.waitInterval(3);
        processor = deliveryControllerHelper.orderPickedupFromController(order_id);
        Assert.assertTrue(confirmControllerStatus("pivkedup", order_id),"Order not pivkedup");
        deliveryDataHelper.waitInterval(3);
        processor = deliveryControllerHelper.orderReachedFromController(order_id);
        Assert.assertTrue(confirmControllerStatus("reached", order_id),"Order not reached");
        deliveryDataHelper.waitInterval(3);
        processor = deliveryControllerHelper.orderDeliveredFromController(order_id);
        Assert.assertTrue(confirmControllerStatus("delivered", order_id));


    }
//Not doing CAPRDFF-347 as it is redundant

    @Test(groups = "Regression", description = "Verify OE is able to mark order re-assigned and same is reflected in delivery")
    public void CAPRDFROMFF348() {
        String order_id = getOrderID(orderDefaultMap);
        loginDEandUpdateLocation();
        Boolean defreestatus = deliveryServiceHelper.makeDEFree(de_id, app_version);
        Processor processor = null;
        Assert.assertTrue(defreestatus,"Assignment failed for Order "+order_id+ " to de_id " +de_id);
        processor = deliveryServiceHelper.assignOrder(order_id, de_id);
        Assert.assertTrue(orderCreationAndAssignmentstatus(order_id),"Assignment failed for Order "+order_id+ " to de_id " +de_id);
        deliveryDataHelper.waitInterval(3);
        processor = deliveryControllerHelper.orderConfirmedFromController(order_id);
        Assert.assertTrue(confirmControllerStatus("confirmed", order_id), "Order not confirmed");
        deliveryDataHelper.waitInterval(3);
        processor=deliveryControllerHelper.reasssignFromFF(order_id,city_id);
        deliveryDataHelper.waitInterval(3);
        Assert.assertTrue(checkAssignmentStatus(processor,order_id,de_id));

    }
}









