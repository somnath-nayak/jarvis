package com.swiggy.api.erp.delivery.tests;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.delivery.Pojos.Last_miles;
import com.swiggy.api.erp.delivery.Pojos.Restaurant;
import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.erp.delivery.constants.RTSConstants;
import com.swiggy.api.erp.delivery.dp.LastMileandPrepTimePredicitionDataProvider;
import com.swiggy.api.erp.delivery.helper.CartSLA_Helper;
import com.swiggy.api.erp.delivery.helper.DSPRedisKeySetupHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryHelperMethods;
import com.swiggy.api.sf.checkout.helper.CommonAPIHelper;

import framework.gameofthrones.JonSnow.Processor;

public class PrepTimeandLastMilePredictions extends
		LastMileandPrepTimePredicitionDataProvider {

	DeliveryHelperMethods delmeth = new DeliveryHelperMethods();
	CartSLA_Helper serhelp = new CartSLA_Helper();
	DSPRedisKeySetupHelper dsphelper=new DSPRedisKeySetupHelper();
	Object sla;

	@BeforeClass
	public void setup() {
		
		delmeth.dbhelperupdate(RTSConstants.setpreptimepredict_true,DeliveryConstant.databaseName);
		delmeth.dbhelperupdate(RTSConstants.setlastmiletimepredict_true,  DeliveryConstant.databaseName);
		delmeth.dbhelperupdate(RTSConstants.set_max_Last_mile_more,  DeliveryConstant.databaseName);
		delmeth.dbhelperupdate(RTSConstants.set_max_prep_time_more,  DeliveryConstant.databaseName);
		delmeth.dbhelperupdate(RTSConstants.set_prep_time_predictions_zone_enable,  DeliveryConstant.databaseName);
		delmeth.dbhelperupdate(RTSConstants.set_last_mile_prediction_zone_enable,  DeliveryConstant.databaseName);
		delmeth.dbhelperupdate(RTSConstants.set_max_items_cart_more,  DeliveryConstant.databaseName);
		delmeth.dbhelperupdate(RTSConstants.set_max_total_items_cart_more,  DeliveryConstant.databaseName);
		dsphelper.addredisDSPkeysforPrepTime(RTSConstants.default_restaurant_id, RTSConstants.zone_id);
		/*delmeth.redisconnectsetdoubleStrinvalue(RTSConstants.set_last_mile_DS,
				RTSConstants.single_assignedZones, 0,
				RTSConstants.average_last_mile_DSvalue, "dsredis");
		String s = RTSConstants.set_last_mile_DS.concat("tdr1tfm#");
		delmeth.redisconnectsetdoubleStrinvalue(s,
				RTSConstants.single_assignedZones, 0,
				RTSConstants.average_last_mile_DSvalue, "dsredis");
		delmeth.redisconnectsetdoubleStrinvalue(
				RTSConstants.set_average_arrived_to_delivery_key,
				RTSConstants.single_assignedZones, 0,
				RTSConstants.average_last_mile_DSvalue, "dsredis");*/
	}

	@Test(dataProvider = "LMPT_1", priority = 6, groups = "LMPT", description = "Verify cerebro cart call is working fine according to old prep time and old last mile time when both PT and LM feature flag are off")
	public void LMPT_1() throws InterruptedException, IOException {
		double DSlastmiletime=getDSLastmiletime(RTSConstants.DS_restaurant_customer_distance_last_mile, RTSConstants.default_restaurant_id, RTSConstants.DS_customer_geo_hash, RTSConstants.zone_id);
		double DSpreptime=getDSPreptime(RTSConstants.default_listing_item_count_more, RTSConstants.bill_amount_less, RTSConstants.default_restaurant_id, RTSConstants.preptime_zone_id,RTSConstants.order_total_quantity_less, RTSConstants.bill_amount_less);
		Processor processor=getdata(RTSConstants.default_listing_item_count_more, RTSConstants.bill_amount_less,RTSConstants.total_item_count_less,null);
		double lastmilet = getDeliveryLastmiletime(processor);
		double preptimet = getDeliveryPreptime(processor);
		sla=JsonPath.read(
				processor.ResponseValidator.GetBodyAsText(),
				"$.cart..sla");
		System.out.println("SLA before prediction is"+sla.toString());
		Assert.assertNotEquals(lastmilet,DSlastmiletime,"Last mile was equal to " + lastmilet);
			Assert.assertNotEquals(preptimet,DSpreptime,"Prep time was equal to= " + preptimet);
		}
	@Test(dataProvider = "LMPT_2", priority =7, groups = "LMPT", description = "Verify Place order call is working fine according to old prep time and new last mile time when  PT flag is off and LM flag is on")
	public void LMPT_2() throws InterruptedException, IOException {
		double DSlastmiletime=getDSLastmiletime(RTSConstants.DS_restaurant_customer_distance_last_mile, RTSConstants.default_restaurant_id, RTSConstants.DS_customer_geo_hash,RTSConstants.zone_id);
		double DSpreptime=getDSPreptime(RTSConstants.default_listing_item_count_more, RTSConstants.bill_amount_less,RTSConstants.default_restaurant_id, RTSConstants.preptime_zone_id,RTSConstants.order_total_quantity_less, RTSConstants.bill_amount_less);
		Thread.sleep(10000);
		Processor processor=getdata(RTSConstants.default_listing_item_count_more, RTSConstants.bill_amount_less,RTSConstants.total_item_count_less,null);
		double lastmilet = getDeliveryLastmiletime(processor);
		double preptimet = getDeliveryPreptime(processor);
			Assert.assertEquals(lastmilet,DSlastmiletime,"Last mile was equal to " + lastmilet);
			Assert.assertNotEquals(preptimet,DSpreptime,"Prep time was equal to= " + preptimet);
		}

	@Test(dataProvider = "LMPT_3", priority = 8, groups = "LMPT", description = "Verify Place order call is working fine according to new prep time and old last mile time when PT flag is on and LM flag is off")
	public void LMPT_3() throws InterruptedException, IOException {
		double DSlastmiletime=getDSLastmiletime(RTSConstants.DS_restaurant_customer_distance_last_mile, RTSConstants.default_restaurant_id, RTSConstants.DS_customer_geo_hash,RTSConstants.zone_id);
		double DSpreptime=getDSPreptime(RTSConstants.default_listing_item_count_more, RTSConstants.bill_amount_less,RTSConstants.default_restaurant_id, RTSConstants.preptime_zone_id,RTSConstants.order_total_quantity_less, RTSConstants.bill_amount_less);
		Thread.sleep(10000);
		Processor processor=getdata(RTSConstants.default_listing_item_count_more, RTSConstants.bill_amount_less,RTSConstants.total_item_count_less,null);
		double lastmilet = getDeliveryLastmiletime(processor);
		double preptimet = getDeliveryPreptime(processor);
		Assert.assertNotEquals(lastmilet,DSlastmiletime,"Last mile was equal to " + lastmilet);
			Assert.assertEquals(preptimet,DSpreptime,"Prep time was equal to= " + preptimet);
		}

	@Test(dataProvider = "LMPT_4", priority = 9, groups = "LMPT", description = "Verify Place order call is working fine according to new prep time and new last mile time when both PT and LM feature flag are on")
	public void LMPT_4() throws InterruptedException, IOException {
		double DSlastmiletime=getDSLastmiletime(RTSConstants.DS_restaurant_customer_distance_last_mile, RTSConstants.default_restaurant_id, RTSConstants.DS_customer_geo_hash,RTSConstants.zone_id);
		double DSpreptime=getDSPreptime(RTSConstants.default_listing_item_count_more, RTSConstants.bill_amount_less,RTSConstants.default_restaurant_id, RTSConstants.preptime_zone_id,RTSConstants.order_total_quantity_less, RTSConstants.bill_amount_less);
		Thread.sleep(10000);
		Processor processor=getdata(RTSConstants.default_listing_item_count_more, RTSConstants.bill_amount_less,RTSConstants.total_item_count_less,null);
		double lastmilet = getDeliveryLastmiletime(processor);
		double preptimet = getDeliveryPreptime(processor);
			Assert.assertEquals(lastmilet,DSlastmiletime,"Last mile was equal to " + lastmilet);
			Assert.assertEquals(preptimet,DSpreptime,"Prep time was equal to= " + preptimet);
		}

	@Test(dataProvider = "LMPT_5", priority = 10, groups = "LMPT", description = "Verify cerebro sends new prep time and last mile time values which are being sent by DS to RTS when both feature flags are on")
	public void LMPT_5() throws InterruptedException, IOException {
		double DSlastmiletime=getDSLastmiletime(RTSConstants.DS_restaurant_customer_distance_last_mile, RTSConstants.default_restaurant_id, RTSConstants.DS_customer_geo_hash,RTSConstants.zone_id);
		double DSpreptime=getDSPreptime(RTSConstants.default_listing_item_count_more, RTSConstants.bill_amount_less,RTSConstants.default_restaurant_id, RTSConstants.preptime_zone_id,RTSConstants.order_total_quantity_less, RTSConstants.bill_amount_less);
		Processor processor=getdata(RTSConstants.default_listing_item_count_more, RTSConstants.bill_amount_less,RTSConstants.total_item_count_less,null);
		double lastmilet = getDeliveryLastmiletime(processor);
		double preptimet = getDeliveryPreptime(processor);
		Assert.assertEquals(lastmilet,DSlastmiletime,"Last mile was equal to " + lastmilet);
		Assert.assertEquals(preptimet,DSpreptime,"Prep time was equal to= " + preptimet);
		}

	@Test(dataProvider = "LMPT_11", priority = 11, groups = "LMPT", description = "Verify cerebro sends new prep time and last mile time values on create cart request when both feature flags are on")
	public void LMPT_11() throws InterruptedException, IOException {
		double DSlastmiletime=getDSLastmiletime(RTSConstants.DS_restaurant_customer_distance_last_mile, RTSConstants.default_restaurant_id, RTSConstants.DS_customer_geo_hash,RTSConstants.zone_id);
		double DSpreptime=getDSPreptime(RTSConstants.default_listing_item_count_more, RTSConstants.bill_amount_less,RTSConstants.default_restaurant_id, RTSConstants.preptime_zone_id,RTSConstants.order_total_quantity_less, RTSConstants.bill_amount_less);
		Processor processor=getdata(RTSConstants.default_listing_item_count_more, RTSConstants.bill_amount_less,RTSConstants.total_item_count_less,RTSConstants.cart_state_create);
		double lastmilet = getDeliveryLastmiletime(processor);
		double preptimet = getDeliveryPreptime(processor);
		Assert.assertEquals(lastmilet,DSlastmiletime,"Last mile was equal to " + lastmilet);
		Assert.assertEquals(preptimet,DSpreptime,"Prep time was equal to= " + preptimet);
		}
	@Test(dataProvider = "LMPT_6", priority = 12, groups = "LMPT", description = "Veirfy RTS calculate SLA considering last mile time and prep time as suggested by DS")
	public void LMPT_6() throws InterruptedException, IOException {
		Restaurant restaurant = new Restaurant();
		restaurant.defaultresultset();
		Last_miles last_mile = new Last_miles();
		last_mile.setdefaultvalues();
		Last_miles[] last = new Last_miles[1];
		last[0] = last_mile;
		CommonAPIHelper commonAPIHelper = new CommonAPIHelper();
		int random = commonAPIHelper.getRandomNo(0, 10000);
        Thread.sleep(10000);
		serhelp.cartslaapi(restaurant,
				RTSConstants.default_listing_item_count_more, last,
				RTSConstants.bill_amount_less, RTSConstants.randomorder_id
						+ random,
				String.valueOf(RTSConstants.cart_id + random),
				RTSConstants.cart_state_place_order,RTSConstants.total_item_count_less);
		Processor processor = serhelp.cartslaapi(restaurant,
				RTSConstants.default_listing_item_count_more, last,
				RTSConstants.bill_amount_less, RTSConstants.randomorder_id
						+ random + 1,
				String.valueOf(RTSConstants.cart_id + random),
				RTSConstants.cart_state_place_order,RTSConstants.total_item_count_less);
		Object slaa = JsonPath.read(
				processor.ResponseValidator.GetBodyAsText(), "$.cart..sla");
		if (null == slaa) {
			Assert.assertTrue(false, "Not got proper response");
		} else {
			String slla=sla.toString().replace("[", "").replace("]", "");
			String sla = slaa.toString().replace("[", "").replace("]", "");
			Assert.assertNotEquals(sla, slla,
					"SLA was equal to " + sla);

		}

	}

	@Test(dataProvider = "LMPT_7", priority = 13, groups = "LMPT", description = "Verify cerebro does not fall back to old logic for calculating last mile time if DS send last mile time between 0 to 60(configurable)")
	public void LMPT_7() throws InterruptedException, IOException {
		double DSlastmiletime=getDSLastmiletime(RTSConstants.DS_restaurant_customer_distance_last_mile, RTSConstants.default_restaurant_id, RTSConstants.DS_customer_geo_hash,RTSConstants.zone_id);
		Processor processor=getdata(RTSConstants.default_listing_item_count_more, RTSConstants.bill_amount_less,RTSConstants.total_item_count_less,null);
		double lastmilet = getDeliveryLastmiletime(processor);
		Assert.assertEquals(lastmilet,DSlastmiletime,"Last mile was equal to " + lastmilet);
		}
	@Test(dataProvider = "LMPT_8", priority = 14, groups = "LMPT", description = "Verify cerebro falls back to old logic for calculating last mile time if DS send last mile time greater than 10(c0nfigurable) mins")
	public void LMPT_8() throws InterruptedException, IOException {
		Object DSlastmiletime=getDSLastmiletime(RTSConstants.DS_restaurant_customer_distance_last_mile, RTSConstants.default_restaurant_id, RTSConstants.DS_customer_geo_hash,RTSConstants.zone_id);
		Processor processor=getdata(RTSConstants.default_listing_item_count_more, RTSConstants.bill_amount_more,RTSConstants.total_item_count_less,null);
		double lastmilet = getDeliveryLastmiletime(processor);
		Assert.assertNotEquals(lastmilet,DSlastmiletime,"Last mile was equal to " + lastmilet);
		}

	@Test(dataProvider = "LMPT_9", priority = 15, groups = "LMPT", description = "Verify cerebro does not fall back to old logic for calculating prep time if DS send prep time between 0 to 60(configurable)")
	public void LMPT_9() throws InterruptedException, IOException {
		Object DSpreptime=getDSPreptime(RTSConstants.default_listing_item_count_more, RTSConstants.bill_amount_less,RTSConstants.default_restaurant_id, RTSConstants.preptime_zone_id,RTSConstants.order_total_quantity_less, RTSConstants.bill_amount_less);
		Processor processor=getdata(RTSConstants.default_listing_item_count_more, RTSConstants.bill_amount_less,RTSConstants.total_item_count_less,null);
		double preptimet = getDeliveryPreptime(processor);
		Assert.assertEquals(preptimet,DSpreptime,"Prep time was equal to= " + preptimet);
		}
	@Test(dataProvider = "LMPT_10", priority = 16, groups = "LMPT", description = "Verify cerebro falls back to old logic for calculating prep time if DS send prep time greater than 60(configurable) mins")
	public void LMPT_10() throws InterruptedException, IOException {
		Object DSpreptime=getDSPreptime(RTSConstants.default_listing_item_count_more, RTSConstants.bill_amount_less,RTSConstants.default_restaurant_id, RTSConstants.preptime_zone_id, RTSConstants.order_total_quantity_less, RTSConstants.bill_amount_less);
		Processor processor=getdata(RTSConstants.default_listing_item_count_more, RTSConstants.bill_amount_less,RTSConstants.total_item_count_less,null);
		double preptimet = getDeliveryPreptime(processor); 
		Assert.assertNotEquals(preptimet,DSpreptime,"Prep time was equal to= " + preptimet);
		}
	public double getDSPreptime(String listing_count,String bill,String rest,String zone,String order_total_quantity,String bill_amount) throws IOException
	{
		Object banner_factor = delmeth.redisconnectget(RTSConstants.get_bf,
				RTSConstants.single_assignedZones, 1);
		if(banner_factor==null)
		{
		delmeth.redisconnectsetdouble(RTSConstants.get_bf, RTSConstants.single_assignedZones, 1, RTSConstants.rts_bf_prepandlast_double, RTSConstants.delivery_redis);
		banner_factor=RTSConstants.rts_bf_prepandlast_double;
		}
		String hour = delmeth.getcurrtimehour();
		if (hour.equalsIgnoreCase("24")) {
			hour = "0";
		}
		String slot_id = delmeth.getslot(hour);
		String weekday = String.valueOf(delmeth.getweekday());
		Object DSpreptime = serhelp.getpreptimesendbyDS(hour, weekday,
				listing_count,bill, rest,
				zone,slot_id, banner_factor.toString(), order_total_quantity, bill_amount);
		double dsprep=Double.parseDouble(String.format("%.2f",Double.parseDouble(DSpreptime.toString())));
		return dsprep;
	}
	public double getDSLastmiletime(String rest_cust_distance,String rest,String cust_geo_hash,String zoneid) throws IOException
	{
		Object banner_factor = delmeth.redisconnectget(RTSConstants.get_bf,
				RTSConstants.single_assignedZones, 1);
		if(banner_factor==null)
		{
		delmeth.redisconnectsetdouble(RTSConstants.get_bf, RTSConstants.single_assignedZones, 1, RTSConstants.rts_bf_prepandlast_double, RTSConstants.delivery_redis);
		banner_factor=RTSConstants.rts_bf_prepandlast_double;
		}
		String hour = delmeth.getcurrtimehour();
		if (hour.equalsIgnoreCase("24")) {
			hour = "0";
		}
		String weekday = String.valueOf(delmeth.getweekday());
		Object DSlastmiletime = serhelp.getlastmilesendbyDS(hour, weekday,rest_cust_distance,
				rest, cust_geo_hash,banner_factor.toString(), zoneid);
		double dslast=Double.parseDouble(String.format("%.2f",Double.parseDouble(DSlastmiletime.toString())));
		return dslast;
	}
	
	public Processor getdata(String listing_count,String bill,String total_item,Object cartstate) throws IOException
	{
		Restaurant restaurant = new Restaurant();
		restaurant.defaultresultset();
		Last_miles last_mile = new Last_miles();
		last_mile.setdefaultvalues();
		Last_miles[] last = new Last_miles[1];
		last[0] = last_mile;

		CommonAPIHelper commonAPIHelper = new CommonAPIHelper();
		int random = commonAPIHelper.getRandomNo(0, 10000);
		Processor processor;
		if(cartstate==null)
		{
			serhelp.cartslaapi(restaurant,
					listing_count, last,
					bill, RTSConstants.randomorder_id
							+ random,
					String.valueOf(RTSConstants.cart_id + random),
					RTSConstants.cart_state_place_order,total_item);
			processor = serhelp.cartslaapi(restaurant,
					listing_count, last,
					bill, RTSConstants.randomorder_id
							+ random + 1,
					String.valueOf(RTSConstants.cart_id + random),
					RTSConstants.cart_state_place_order,total_item);
		}
		else
		{
			serhelp.cartslaapi(restaurant,
					listing_count, last,
					bill, "",
					String.valueOf(RTSConstants.cart_id + random),
					cartstate.toString(),total_item);
			processor = serhelp.cartslaapi(restaurant,
					listing_count, last,
					bill, "",
					String.valueOf(RTSConstants.cart_id + random),
					cartstate.toString(),total_item);
		}
		
		
		return processor;
	}
	public double getDeliveryLastmiletime(Processor processor)
		{double lastmilet = 0;
			Object lastmileintime = JsonPath.read(
					processor.ResponseValidator.GetBodyAsText(),
					"$.cart..last_mile_time_pred");
			if(null==lastmileintime)
			{
				Assert.assertTrue(false, "Not got proper response");
			}
			else
			{
				lastmilet = Double.parseDouble(String.format("%.2f",Double.parseDouble((lastmileintime.toString().replace("[", "")
						.replace("]", "")))));
			}
			return lastmilet;
		}
	public double getDeliveryPreptime(Processor processor)
	{double preptimet = 0;
	Object preptime = JsonPath.read(
			processor.ResponseValidator.GetBodyAsText(),
			"$.cart..prep_time_pred");
		if(null==preptime)
		{
			Assert.assertTrue(false, "Not got proper response");
		}
		else
		{
		 preptimet = Double.parseDouble(String.format("%.2f", Double.parseDouble((preptime.toString().replace("[", "")
			.replace("]", "")))));
			
		}
		return preptimet;
	}
		
	@AfterClass
	public void deleteDSPrediskeys()
	{
		dsphelper.deleteredisDSPkeysforPrepTime(RTSConstants.default_restaurant_id, RTSConstants.zone_id);
	}
	
	

}