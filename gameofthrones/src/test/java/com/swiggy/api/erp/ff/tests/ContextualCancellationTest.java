package com.swiggy.api.erp.ff.tests;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.ff.constants.ContextualCancellationConstants;
import com.swiggy.api.erp.ff.dp.ContextualCancellationData;
import com.swiggy.api.erp.ff.helper.ContextualCancellationHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Cersei.ToolBox;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import net.minidev.json.JSONArray;
import org.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ContextualCancellationTest extends ContextualCancellationData implements ContextualCancellationConstants {

	Initialize gameofthrones = new Initialize();
	SchemaValidatorUtils schemaValidatorUtils=new SchemaValidatorUtils();

	ContextualCancellationHelper helper = new ContextualCancellationHelper();

	@Test(groups = { "sanity", "regression" },description = "Fetch All Cancellation Groups Validator")
	public void testFetchAllCancellationGroups() {
		System.out.println("***************************************** testFetchAllCancellationGroups started *****************************************");
		GameOfThronesService getSession = new GameOfThronesService("oms", "fetchallcancellationgroups", gameofthrones);
		Processor fetchAllCancellationGroups_response = new Processor(getSession, helper.getHeaders(), null);
		String resp = fetchAllCancellationGroups_response.ResponseValidator.GetBodyAsText();
		String statusMessage = JsonPath.read(resp, "$.response.message");
		Assert.assertEquals(String.valueOf(fetchAllCancellationGroups_response.ResponseValidator.GetResponseCode()), "200");
		Assert.assertEquals(statusMessage, "Success");
		System.out.println("######################################### testFetchAllCancellationGroups compleated #########################################");
	}

	@Test(dataProvider = "fetchMetaInfoForGroups", groups = { "sanity", "regression" },description = "Fetch Meta Info For Groups Validator")
	public void testFetchMetaInfoForGroups(String groupId) {
		System.out.println("***************************************** testFetchMetaInfoForGroups started *****************************************");
		String[] params = new String[] { groupId };
		GameOfThronesService getSession = new GameOfThronesService("oms", "fetchmetainfoforgroups", gameofthrones);
		Processor fetchmetainfoforgroups_response = new Processor(getSession, helper.getHeaders(), null, params);
		String resp = fetchmetainfoforgroups_response.ResponseValidator.GetBodyAsText();
		String groupName = JsonPath.read(resp, "$.data.name");
		List<Map<String, Object>> groupInfo = SystemConfigProvider.getTemplate("oms").queryForList("select * from contextual_cancellations_group where id = " + groupId + ";");
		Assert.assertEquals(String.valueOf(fetchmetainfoforgroups_response.ResponseValidator.GetResponseCode()), "200");
		Assert.assertEquals(groupInfo.get(0).get("name"), groupName);
		System.out.println("######################################### testFetchMetaInfoForGroups compleated #########################################");
	}

	@Test(dataProvider = "fetchAllDispositionIds", groups = { "sanity", "regression" },description = "Fetch All Disposition Ids Validator")
	public void testFetchAllDispositionIds(String order_id, String group_id, HashMap hm) {
		System.out.println("***************************************** testFetchAllDispositionIds started *****************************************");
		String[] params = new String[] { order_id, group_id };
		GameOfThronesService getSession = new GameOfThronesService("oms", "fetchalldispositionids", gameofthrones);
		Processor fetchalldispositionids_response = new Processor(getSession, helper.getHeaders(), null, params);
		String resp = fetchalldispositionids_response.ResponseValidator.GetBodyAsText();
		String statusMessage = JsonPath.read(resp, "$.response.message");
		Assert.assertEquals(String.valueOf(fetchalldispositionids_response.ResponseValidator.GetResponseCode()), hm.get("httpStatusCode"));
		Assert.assertEquals(statusMessage, hm.get("statusMessage"));
		System.out.println("######################################### testFetchAllDispositionIds compleated #########################################");
	}

	@Test(dataProvider = "fetchMetaInfoForGroupsDbValidate", groups = { "sanity", "regression" },description = "Fetch Meta Info For Groups Validator")
	public void testFetchAllDispositionIdsDbValidate(String orderId, List<Integer> expectedDispositionsList, String groupId, String scenario, int group) throws SQLException, IOException, InterruptedException {
		System.out.println("***************************************** testFetchAllDispositionIds with "+scenario+" and with group = "+group+" started *****************************************");

		helper.changeDeliveryPlacingAndVerificationStatusInDB(scenario, orderId);
		String[] params = new String[] { orderId, groupId };
		GameOfThronesService getSession = new GameOfThronesService("oms", "fetchalldispositionids", gameofthrones);

		Processor fetchalldispositionids_response = new Processor(getSession, helper.getHeaders(), null, params);
		JSONArray dispositionsArray = fetchalldispositionids_response.ResponseValidator.GetNodeValueAsJsonArray("$.data.disposition");
		List<Integer> actualDispositionsList = new ArrayList();
		for(int i = 0; i<dispositionsArray.size(); i++) {
			int id = fetchalldispositionids_response.ResponseValidator.GetNodeValueAsInt("$.data.disposition["+i+"].id");
			actualDispositionsList.add(id);
		}
		Assert.assertTrue(ContextualCancellationHelper.listEqualsIgnoreOrder(actualDispositionsList, expectedDispositionsList), "Actual Dispositions did not match with Expected"+"Actual == "+ actualDispositionsList +"Expected == "+expectedDispositionsList + " orderID = " + orderId);
		System.out.println("######################################### testFetchAllDispositionIds with "+scenario+" and with group = "+group+" compleated #########################################");
	}

	@Test(dataProvider = "cancellationApi", groups = { "sanity", "regression" },description = "Cancellation Api Validator")
	public void testCancellationApi(String action, String disposition_id, String sub_disposition_id, String cancellation_fee, String cancellation_fee_applicability, String OrderId, HashMap hm) throws JSONException {
		System.out.println("***************************************** Cancellation Api started *****************************************");
		String[] params = new String[] { action, disposition_id, sub_disposition_id, cancellation_fee, cancellation_fee_applicability };
		GameOfThronesService getSession = new GameOfThronesService("oms", "ffcancellationapi", gameofthrones);
		Processor cancellationApi_response = new Processor(getSession, helper.getHeaders(), params, new String[] { OrderId });
		String resp = cancellationApi_response.ResponseValidator.GetBodyAsText();
		String statusMessage = JsonPath.read(resp, "$.response.message");
		String cancellation_initiated = JsonPath.read(resp, "$.data.cancellation_status.cancellation_initiated").toString();
		if (hm.get("Testname") != "successCase" && hm.get("Testname") != "invalidinvalidCancellationFeeApplicability" && hm.get("Testname") != "invalidCancellationFee") {

			String message = JsonPath.read(resp, "$.response.errors[0].message");
			String error_field = JsonPath.read(resp, "$.response.errors[0].error_field").toString();
			String rejected_value = JsonPath.read(resp, "$.response.errors[0].rejected_value").toString();

			Assert.assertEquals(String.valueOf(cancellationApi_response.ResponseValidator.GetResponseCode()), hm.get("httpStatusCode"));
			Assert.assertEquals(statusMessage, hm.get("statusMessage"));
			Assert.assertEquals(message, hm.get("message"));
			Assert.assertEquals(error_field, hm.get("error_field"));
			Assert.assertEquals(rejected_value, hm.get("rejected_value"));
			Assert.assertEquals(cancellation_initiated, hm.get("cancellation_initiated"));
		}

		Assert.assertEquals(String.valueOf(cancellationApi_response.ResponseValidator.GetResponseCode()), hm.get("httpStatusCode"));
		Assert.assertEquals(statusMessage, hm.get("statusMessage"));
		System.out.println(hm.get("cancellation_initiated").getClass().getName());
		Assert.assertEquals(cancellation_initiated, hm.get("cancellation_initiated"));

		System.out.println("***************************************** Cancellation Api Completed *****************************************");
	}

	@Test(dataProvider = "cancellationApiDbValidate", groups = { "sanity", "regression" },description = "Cancellation Api DB Validator")
	public void testCancellationApiDbValidate(String action, String disposition_id, String sub_disposition_id, String cancellation_fee, String cancellation_fee_applicability, String OrderId) throws JSONException {
		System.out.println("***************************************** Cancellation Api started *****************************************");
		String[] params = new String[] { action, disposition_id, sub_disposition_id, cancellation_fee, cancellation_fee_applicability };
		GameOfThronesService getSession = new GameOfThronesService("oms", "ffcancellationapi", gameofthrones);
		Processor cancellationApi_response = new Processor(getSession, helper.getHeaders(), params, new String[] { OrderId });
		String resp = cancellationApi_response.ResponseValidator.GetBodyAsText();
		List<Map<String, Object>> orderStatus = SystemConfigProvider.getTemplate("checkout").queryForList("SELECT order_status FROM swiggy_order_management.swiggy_orders where id = " + OrderId + ";");
		String statusMessage = JsonPath.read(resp, "$.response.message");
		Assert.assertEquals(String.valueOf(cancellationApi_response.ResponseValidator.GetResponseCode()), "200");
		Assert.assertEquals(statusMessage, "Success");
		Assert.assertEquals(orderStatus.get(0).get("order_status"), "cancelled");
		System.out.println("***************************************** Cancellation Api Completed *****************************************");
	}

	@Test(dataProvider = "fetchMetaInfoForGroupsBadCases", groups = { "sanity", "regression" },description = "Fetch Meta Info For Groups BadCases Validator")
	public void testFetchMetaInfoForGroupsBadCases(String groupId, HashMap hm) {
		System.out.println("***************************************** testFetchMetaInfoForGroupsBadCases started *****************************************");
		String[] params = new String[] { groupId };
		GameOfThronesService getSession = new GameOfThronesService("oms", "fetchmetainfoforgroups", gameofthrones);
		Processor fetchmetainfoforgroups_response = new Processor(getSession, helper.getHeaders(), null, params);
		String resp = fetchmetainfoforgroups_response.ResponseValidator.GetBodyAsText();
		Assert.assertEquals(String.valueOf(fetchmetainfoforgroups_response.ResponseValidator.GetResponseCode()), hm.get("httpStatusCode"));
		System.out.println("######################################### testFetchMetaInfoForGroupsBadCases compleated #########################################");
	}

	@Test(dataProvider = "fetchCancellationFee", groups = { "sanity", "regression" },description = "Fetch Cancellation Fee Validator")
	public void testfetchCancellationFee(String orderId, String dispositionId, String subDispositionId, HashMap hm) {
		System.out.println("***************************************** testfetchCancellationFee started *****************************************");
		String[] params = new String[] { orderId, dispositionId, subDispositionId };
		GameOfThronesService getSession = new GameOfThronesService("oms", "fetchcancellationfee", gameofthrones);
		Processor fetchcancellationfee_response = new Processor(getSession, helper.getHeaders(), null, params);
		String resp = fetchcancellationfee_response.ResponseValidator.GetBodyAsText();
		String statusMessage = JsonPath.read(resp, "$.response.message");
		Assert.assertEquals(String.valueOf(fetchcancellationfee_response.ResponseValidator.GetResponseCode()), hm.get("httpStatusCode"));
		Assert.assertEquals(statusMessage, hm.get("statusMessage"));

		System.out.println("######################################### testfetchCancellationFee compleated #########################################");
	}




	@Test(groups = { "schemaValidator" },description = "Fetch All Cancellation Groups Validator")
	public void testFetchAllCancellationGroupsSchemaValidator() throws ProcessingException, IOException {
		System.out.println("***************************************** testFetchAllCancellationGroupsSchemaValidator started *****************************************");
		GameOfThronesService getSession = new GameOfThronesService("oms", "fetchallcancellationgroups", gameofthrones);
		Processor fetchAllCancellationGroups_response = new Processor(getSession, helper.getHeaders(), null);

		JSONArray jsonArray = fetchAllCancellationGroups_response.ResponseValidator.GetNodeValueAsJsonArray("$.data.cancellation_groups");
		System.out.println("Length of array is ====== "+jsonArray.size());
		int length=jsonArray.size();
		// @test
		List name_of_groups=new ArrayList<String>();
		List id_of_groups=new ArrayList<String>();
		List dispositions_of_groups=new ArrayList();
		List subdispositions_of_groups=new ArrayList();

		List dispositions_of_groups_by_Id=new ArrayList();
		List subdispositions_of_groups_by_id=new ArrayList();
		//  @Helper
		List group=ContextualCancellationHelper.getGroupsName();
		List dispositions_list=ContextualCancellationHelper.getAllDispositionsName();
		List subdispositions_list=ContextualCancellationHelper.getAllSubDispositionsName();
		List dispositions_list_id=ContextualCancellationHelper.getAllDispositionsId();
		List subdispositions_list_id=ContextualCancellationHelper.getAllSubDispositionsId();
		for(int i=0;i<length;i++)
		{
			name_of_groups.add(fetchAllCancellationGroups_response.ResponseValidator.GetNodeValue("$.data.cancellation_groups["+i+"].name"));
			id_of_groups.add(fetchAllCancellationGroups_response.ResponseValidator.GetNodeValueAsInt("$.data.cancellation_groups["+i+"].id"));
			int dispositions=fetchAllCancellationGroups_response.ResponseValidator.GetNodeValueAsJsonArray("$.data.cancellation_groups["+i+"].dispositions").size();
			for(int j=0;j<dispositions;j++)
			{
				dispositions_of_groups.add(fetchAllCancellationGroups_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cancellation_groups["+i+"].dispositions["+j+"].name"));
				System.out.println("Disposition is === "+fetchAllCancellationGroups_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cancellation_groups["+i+"].dispositions["+j+"].id"));
				dispositions_of_groups_by_Id.add(fetchAllCancellationGroups_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cancellation_groups["+i+"].dispositions["+j+"].id"));
				int subDispositions=fetchAllCancellationGroups_response.ResponseValidator.GetNodeValueAsJsonArray("$.data.cancellation_groups["+i+"].dispositions["+j+"].sub_dispositions").size();
				if(subDispositions >0) {
					for(int k =0; k<subDispositions; k++) {
						subdispositions_of_groups.add(fetchAllCancellationGroups_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cancellation_groups["+i+"].dispositions["+j+"].sub_dispositions["+k+"].name"));
						System.out.println("sub_disposition is === "+fetchAllCancellationGroups_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cancellation_groups["+i+"].dispositions["+j+"].sub_dispositions["+k+"].id"));
						subdispositions_of_groups_by_id.add(fetchAllCancellationGroups_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cancellation_groups["+i+"].dispositions["+j+"].sub_dispositions["+k+"].id"));
					}
					System.out.println("subdispositions added in array");
				}
				System.out.println("dispositions added in array");
			}
		}

		Assert.assertTrue(helper.listEqualsIgnoreOrder(group, name_of_groups), "Actual list of dispositions_list did not match expected list");
		Assert.assertTrue(helper.listEqualsIgnoreOrder(dispositions_list, dispositions_of_groups), "Actual list of dispositions_list did not match expected list");
		Assert.assertTrue(helper.listEqualsIgnoreOrder(subdispositions_list, subdispositions_of_groups), "Actual list of dispositions_list did not match expected list"+""+subdispositions_of_groups+"/n"+subdispositions_list);
		Assert.assertTrue(helper.listEqualsIgnoreOrder(subdispositions_list_id, subdispositions_of_groups_by_id), "Actual list of dispositions_list did not match expected list"+""+subdispositions_of_groups+"/n"+subdispositions_list);

		System.out.println("######################################### testFetchAllCancellationGroupsSchemaValidator compleated #########################################");
	}





	@Test(dataProvider = "fetchMetaInfoForGroups", groups = { "schemaValidator" },description = "Fetch Meta Info For Groups Schema Validator")
	public void testFetchMetaInfoForGroupsSchemaValidator(String groupId) throws IOException, ProcessingException {
		System.out.println("***************************************** testFetchMetaInfoForGroupsSchemaValidator started *****************************************");
		String[] params = new String[] { groupId };
		GameOfThronesService getSession = new GameOfThronesService("oms", "fetchmetainfoforgroups", gameofthrones);
		Processor fetchmetainfoforgroups_response = new Processor(getSession, helper.getHeaders(), null, params);
		String resp = fetchmetainfoforgroups_response.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/ff/fetchmetainfoforgroup.txt");
		List missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);;
		Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Schema validation failed for fetchMetaInfoForGroups api");
		boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes=>"+isEmpty);
		Assert.assertEquals( false,isEmpty,"Found Empty Nodes in Response");
		System.out.println("######################################### testFetchMetaInfoForGroupsSchemaValidator compleated #########################################");
	}

	@Test(dataProvider = "fetchAllDispositionIdsSchemaValidator", groups = { "schemaValidator" },description = "Fetch All Disposition Ids Schema Validator")
	public void testFetchAllDispositionIdsSchemaValidator(String order_id, String group_id) throws IOException, ProcessingException {
		System.out.println("***************************************** testFetchAllDispositionIdsSchemaValidator started *****************************************");
		String[] params = new String[] { order_id, group_id };
		GameOfThronesService getSession = new GameOfThronesService("oms", "fetchalldispositionids", gameofthrones);
		Processor fetchalldispositionids_response = new Processor(getSession, helper.getHeaders(), null, params);
		String resp = fetchalldispositionids_response.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/ff/fetchalldispositionidsforgivenorder.txt");;
		List missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);;
		Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Schema validation failed for fetchAllDispositionIds api");
		boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes=>"+isEmpty);
		Assert.assertEquals( false,isEmpty,"Found Empty Nodes in Response");
		System.out.println("######################################### testFetchAllDispositionIdsSchemaValidator compleated #########################################");
	}

	@Test(dataProvider = "fetchCancellationFee",groups = {"schemaValidator"},description = "Fetch Cancellation Fee Schema Validator")
	public void testfetchCancellationFeeSchemaValidator(String orderId, String dispositionId, String subDispositionId, HashMap hm) throws IOException, ProcessingException {
		System.out.println("***************************************** testfetchCancellationFeeSchemaValidator started *****************************************");
		String[] params = new String[] { orderId, dispositionId, subDispositionId };
		GameOfThronesService getSession = new GameOfThronesService("oms", "fetchcancellationfee", gameofthrones);
		Processor fetchcancellationfee_response = new Processor(getSession, helper.getHeaders(), null, params);
		String resp = fetchcancellationfee_response.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/ff/fetchcancellationfee.txt");;
		List missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);;
		Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Schema validation failed for fetchCancellationFee api");
		boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes=>"+isEmpty);
		Assert.assertEquals( false,isEmpty,"Found Empty Nodes in Response");
		System.out.println("######################################### testfetchCancellationFeeSchemaValidator compleated #########################################");
	}

	@Test(dataProvider = "cancellationApiDbValidate", groups = { "schemaValidator" },description = "Fetch Cancellation Api Db Validator")
	public void testCancellationApiSchemaValidator(String action, String disposition_id, String sub_disposition_id, String cancellation_fee, String cancellation_fee_applicability, String OrderId) throws JSONException, IOException, ProcessingException {
		System.out.println("***************************************** testCancellationApiSchemaValidator started *****************************************");
		String[] params = new String[] { action, disposition_id, sub_disposition_id, cancellation_fee, cancellation_fee_applicability };
		GameOfThronesService getSession = new GameOfThronesService("oms", "ffcancellationapi", gameofthrones);
		Processor cancellationApi_response = new Processor(getSession, helper.getHeaders(), params, new String[] { OrderId });
		String resp = cancellationApi_response.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/ff/cancellation.txt");;
		List missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);;
		Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Schema validation failed for cancellation api");
		boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes=>"+isEmpty);
		Assert.assertEquals( false,isEmpty,"Found Empty Nodes in Response");
		System.out.println("######################################### testCancellationApiSchemaValidator compleated #########################################");
	}

}


