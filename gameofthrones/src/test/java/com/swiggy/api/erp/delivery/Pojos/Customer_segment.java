package com.swiggy.api.erp.delivery.Pojos;

import com.swiggy.api.erp.delivery.constants.RTSConstants;

public class Customer_segment {

	private String contact_affinity;

    private String cancellation_probability;

    private String marketing_segment;

    private String churn_probability;

    private String new_user;

    private String user_id;

    private String high_eng_segment;

    private String premium_segment;

    private String value_segment;

    public String getContact_affinity ()
    {
        return contact_affinity;
    }

    public void setContact_affinity (String contact_affinity)
    {
        this.contact_affinity = contact_affinity;
    }

    public String getCancellation_probability ()
    {
        return cancellation_probability;
    }

    public void setCancellation_probability (String cancellation_probability)
    {
        this.cancellation_probability = cancellation_probability;
    }

    public String getMarketing_segment ()
    {
        return marketing_segment;
    }

    public void setMarketing_segment (String marketing_segment)
    {
        this.marketing_segment = marketing_segment;
    }

    public String getChurn_probability ()
    {
        return churn_probability;
    }

    public void setChurn_probability (String churn_probability)
    {
        this.churn_probability = churn_probability;
    }

    public String getNew_user ()
    {
        return new_user;
    }

    public void setNew_user (String new_user)
    {
        this.new_user = new_user;
    }

    public String getUser_id ()
    {
        return user_id;
    }

    public void setUser_id (String user_id)
    {
        this.user_id = user_id;
    }

    public String getHigh_eng_segment ()
    {
        return high_eng_segment;
    }

    public void setHigh_eng_segment (String high_eng_segment)
    {
        this.high_eng_segment = high_eng_segment;
    }

    public String getPremium_segment ()
    {
        return premium_segment;
    }

    public void setPremium_segment (String premium_segment)
    {
        this.premium_segment = premium_segment;
    }

    public String getValue_segment ()
    {
        return value_segment;
    }

    public void setValue_segment (String value_segment)
    {
        this.value_segment = value_segment;
    }

    public void setdefaultresultset()
    {
    	if(this.getCancellation_probability()==null)
    	{
    		this.setCancellation_probability(RTSConstants.default_cancellation_probability);
    	}
    	if(this.getChurn_probability()==null)
    	{
    		this.setChurn_probability(RTSConstants.default_Churn_probability);
    	}
    	if(this.getContact_affinity()==null)
    	{
    		this.setContact_affinity(RTSConstants.default_contact_affinity);
    	}
    	if(this.getHigh_eng_segment()==null)
    	{
    		this.setHigh_eng_segment(RTSConstants.default_high_eng_segment);
    	}
    	if(this.getMarketing_segment()==null)
    	{
    		this.setMarketing_segment(RTSConstants.default_marketing_segmentt);
    	}
    	if(this.getNew_user()==null)
    	{
    		this.setNew_user(RTSConstants.default_new_user);
    	}
    	if(this.getPremium_segment()==null)
    	{
    		this.setPremium_segment(RTSConstants.default_premium_segment);
    	}
    	if(this.getUser_id()==null)
    	{
    		this.setUser_id(RTSConstants.default_user_id);
    	}
    	if(this.getValue_segment()==null)
    	{
    		this.setValue_segment(RTSConstants.default_value_segment);
    	}
    }
    
    
    
    @Override
    public String toString()
    {
        return "ClassPojo [contact_affinity = "+contact_affinity+", cancellation_probability = "+cancellation_probability+", marketing_segment = "+marketing_segment+", churn_probability = "+churn_probability+", new_user = "+new_user+", user_id = "+user_id+", high_eng_segment = "+high_eng_segment+", premium_segment = "+premium_segment+", value_segment = "+value_segment+"]";
    }

}