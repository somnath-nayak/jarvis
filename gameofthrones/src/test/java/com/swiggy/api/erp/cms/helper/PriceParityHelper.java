package com.swiggy.api.erp.cms.helper;

import com.swiggy.api.erp.cms.constants.PriceparityConstants;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.SqlTemplate;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.swiggy.api.erp.cms.helper.BaseServiceHelper.gameofthrones;


public class PriceParityHelper {
    private static Logger log = LoggerFactory.getLogger(PriceParityHelper.class);

    /**
     * DownLoad File From S3
     * @param s3URL
     * @param fileName
     * @throws IOException
     */
    public boolean downLoadFromS3(String s3URL, String fileName) throws IOException {
        File myFile = new File(fileName);
        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet(s3URL);
        HttpResponse response = client.execute(request);
        if(response.getStatusLine().getStatusCode() != 200){
            return false;
        }
        try (FileOutputStream outStream = new FileOutputStream(myFile)) {
            response.getEntity().writeTo(outStream);
        }
        log.debug("Completed Downloading File .. "+fileName);
        return true;
    }

    /**
     * Get Data From Event Queue
     * @param restaurantId
     * @return
     */
    public List<Map<String, Object>> getDataFromEventQueue(int restaurantId){
        List<Map<String, Object>> eventQueueData = SystemConfigProvider.getTemplate("competitive_intelligence").queryForList("select * from event_queue where created_on > now() - interval 2 minute and rest_id="+restaurantId+" order by id desc");
        return eventQueueData;
    }

    public void resetPriceData(){
        SqlTemplate sqlTemplateCMS = SystemConfigProvider.getTemplate("cms");
        SqlTemplate sqlTemplateCI = SystemConfigProvider.getTemplate("competitive_intelligence");
        sqlTemplateCMS.update("update items set price=195, packing_charges=0 where id=5875585");
        sqlTemplateCMS.update("update items set price=220, packing_charges=0 where id=5875589");
        sqlTemplateCMS.update("update items set price=170, packing_charges=0 where id=5875620");
        sqlTemplateCMS.update("update items set price=250, packing_charges=0 where id=5875606");
        sqlTemplateCMS.update("update items set price=360, packing_charges=21.6 where id=5875608");
        sqlTemplateCMS.update("update tickets set state='REJECTED' where state in ('ASSIGNED', 'UNASSIGNED','SUBMITTED') and ticket_type in ('PRICE_PARITY_MANUAL_MAPPING','PRICE_PARITY_MAPPING_VERIFICATION')");

        sqlTemplateCI.execute("delete from event_workflow_map where event_queue_id in (select id from event_queue where rest_id in ("+ PriceparityConstants.restaurantId_pp_basic+"))");
        sqlTemplateCI.execute("delete from event_queue where rest_id in ("+ PriceparityConstants.restaurantId_pp_basic+")");
        sqlTemplateCI.update("update restaurant_price_disparity set disparity_data=null, email_count=0 where rest_id="+PriceparityConstants.restaurantId_pp_basic);
        sqlTemplateCI.update("update restaurant_degradation set disparity_count=0, penalized_count=0, resolved_count=0 where rest_id in ("+ PriceparityConstants.restaurantId_pp_basic +")");

        sqlTemplateCI.execute("delete from competitor_pricing_detail where competitor_item_map_id in (select id from competitor_item_map where rest_id=30648)");
        sqlTemplateCI.execute("delete from competitor_item_map where item_id=5875585");
    }


    /**
     * Get Data From Event WorkFlow Map
     * @param workFlowId
     * @return int
     */
    public long getAgentTicketId(String workFlowId, String ticketType){
        List<Map<String, Object>> agentTicketIdList = SystemConfigProvider.getTemplate("competitive_intelligence").queryForList("select ticket_id from operation_ticket where ticket_type='"+ ticketType+"' and operation_id=(select id from operation_map where workflow_id='"+ workFlowId +"')");
        return (Long) agentTicketIdList.iterator().next().get("ticket_id");
    }

    /**
     * Get Ticket Assignment Details
     * @param ticketId
     * @return
     */
    public Map<String, Object> getTicketAssignmentDetails(long ticketId){
        List<Map<String, Object>> agentTicketIdList = SystemConfigProvider.getTemplate("cms").queryForList("select * from agent_ticket_assignment where ticket_id="+ticketId);
        return agentTicketIdList.iterator().next();
    }

    public void updateAgentStatus(int agentId, String status){
        Map<String, Object> authList = SystemConfigProvider.getTemplate("cms").queryForMap("select * from agents where id="+agentId);

        HashMap<String, String> hm = getGenericHeader();
        hm.put("auth_key", authList.get("auth_key").toString());
        GameOfThronesService service = new GameOfThronesService("cms_agent", "update_agent_available", gameofthrones);
        Processor processor = new Processor(service, hm, null, new String[] {authList.get("auth_user_id").toString(), status});
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
        Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("$.statusCode"), 1);
    }
    /**
     * Get Data From Event WorkFlow map for the list of Items
     * @param list
     * @return
     */
    public List<Map<String, Object>> getDataFromEventWorkFlowMap(List list){
        List<Map<String, Object>> eventQueueData = SystemConfigProvider.getTemplate("competitive_intelligence").queryForList("select * from event_workflow_map where event_queue_id="+list.get(0));
        return eventQueueData;
    }


    public List listMapToArray(List<Map<String, Object>> listMap, String key){
        List<String> arr = new ArrayList();
        for (Map m : listMap){
            arr.add(m.get(key).toString());
        }
        return arr;
    }

    public HashMap<String, String> getGenericHeader(){
        HashMap<String, String> hm = new HashMap<>();
        hm.put("Content-Type", "application/json");
        return hm;
    }

    public HashMap<String, String> getAuthHeader(){
        HashMap<String, String> hm = new HashMap<>();
        hm.put("Content-Type", "application/json");
        hm.put("Authorization", "Basic dmVuZG9yLXN5c3RlbTpzZWxmc2VydmUtc3lzdGVt");
        return hm;
    }

    /**
     * Trigger Event to Schedule Price parity Event
     * @param startTime
     */
    public String scheduleMapping(String startTime, int restId){
        GameOfThronesService service = new GameOfThronesService("cms_inject", "event_schedule", gameofthrones);
        Processor processor = new Processor(service, getGenericHeader(), new String[]{startTime, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm")
                .withZone(ZoneId.systemDefault()).format(Instant.now().plusSeconds(120)).toString()}, null);
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
        Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("$.statusCode"), 1);
        return processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[?(@.rest_id=="+restId+")].workflow_id").replace("[","").replace("]","").replace("\"", "");
    }

    /**
     * Fetch SelfServe Ticket Details by ticket ID
     * @param ticketId
     * @return Processor
     */
    public Processor fetchTicketDetails(long ticketId){
        GameOfThronesService service = new GameOfThronesService("cmsselfserve", "get_ticket_details", gameofthrones);
        Processor processor =  new Processor(service, getAuthHeader(), null, new String[]{""+ticketId});
        return processor;
    }
    /**
     * Upload Mapping File
     * @param workflowId
     * @param filePath
     * @return Processor
     */
    public Processor uploadMappingFile(String workflowId, String filePath, long restId, long compRestId){
        GameOfThronesService service = new GameOfThronesService("cms_mapping", "pp_map_upload", gameofthrones);
        Processor processor =  new Processor(service, setHeadersForUrlEncode(), null, null, setFormDataForPP(filePath, workflowId, ""+restId, ""+compRestId));
        return processor;
    }

    /**
     * Create Auto Map to override the Actual AutoMap
     * @param workflowId
     * @param itemIds
     * @param restId
     * @param compRestId
     * @return
     */
    public Processor createMapping(String workflowId, String itemIds, long restId, long compRestId){
        GameOfThronesService service = new GameOfThronesService("cms_mapping", "pp_map_create", gameofthrones);
        Processor processor =  new Processor(service, getGenericHeader(), new String[]{""+compRestId, itemIds, ""+restId, workflowId}, null);
        return processor;
    }

    /**
     * Upload Mapping Verified File.
     * @param workflowId
     * @param filePath
     * @return Processor
     */
    public Processor uploadVerifiedFile(String workflowId, String filePath, long restId, long compRestId){
        GameOfThronesService service = new GameOfThronesService("cms_mapping", "pp_verification_upload", gameofthrones);
        Processor processor =  new Processor(service, setHeadersForUrlEncode(), null, null, setFormDataForPP(filePath, workflowId, ""+restId, ""+compRestId));
        return processor;
    }

    /**
     * Complete Mapping WorkFlow
     * @param workFlowId
     * @return Processor
     */
    public Processor completeMapping(String workFlowId, String restaurantId){
        GameOfThronesService service = new GameOfThronesService("cms_mapping", "pp_map_complete", gameofthrones);
        Processor processor = new Processor(service, getGenericHeader(), null, new String[]{workFlowId, restaurantId});
        return processor;
    }

    /**
     * Complete Verification Flow.
     * @param workFlowId
     * @return Processor
     */
    public Processor completeVerification(String workFlowId, String competitiveRestId, String competitorType, String restId){
        GameOfThronesService service = new GameOfThronesService("cms_mapping", "pp_verification_complete", gameofthrones);
        Processor processor = new Processor(service, getGenericHeader(), new String[]{competitiveRestId, competitorType, restId, workFlowId}, null);
        return processor;
    }

    /**
     * Fetch Disparity By Restaurant ID
     * @param restaurantId
     * @return
     */
    public Processor fetchDisparityByRestId(int restaurantId){
        GameOfThronesService service = new GameOfThronesService("cms_mapping", "", gameofthrones);
        Processor processor = new Processor(service, getGenericHeader(), null, new String[]{""+restaurantId, "Z"});
        return processor;
    }

    /**
     * Set FormData for Price Parity
     * @param filePath
     * @param workflowId
     * @return
     */
    private HashMap<String, String> setFormDataForPP(String filePath, String workflowId, String restId, String compRestId){
        HashMap<String, String> formData = new HashMap<>();
        formData.put("file", filePath);
        formData.put("workflow-id", workflowId);
        formData.put("rest-id", restId);
        formData.put("comp-rest-id", compRestId);
        return formData;
    }

    private HashMap<String, String> setHeadersForUrlEncode(){
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "multipart/form-data");
        headers.put("Accept", "application/json");
        headers.put("Authorization", "Basic dXNlcjpjaGVjaw==");
        return headers;
    }

}
