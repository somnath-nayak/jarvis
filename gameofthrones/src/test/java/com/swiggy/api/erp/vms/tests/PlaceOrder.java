package com.swiggy.api.erp.vms.tests;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.ff.helper.OMSHelper;
import com.swiggy.api.erp.vms.constants.RMSConstants;
import com.swiggy.api.erp.vms.dp.RMSDp;
import com.swiggy.api.erp.vms.dp.VMSEndToEndOrderFlowsDP;
import com.swiggy.api.erp.vms.helper.RMSCommonHelper;
import com.swiggy.api.erp.vms.helper.RMSHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.pojo.Cart;
import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.snd.helper.SnDHelper;
import com.swiggy.automation.common.utils.APIUtils;

import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;

public class PlaceOrder extends RMSDp {
	RMSHelper helper = new RMSHelper();
	CheckoutHelper checkoutHelper = new CheckoutHelper();
	OMSHelper omsHelper = new OMSHelper();
	RMSHelper rmsHelper = new RMSHelper();
	SnDHelper sndHelper = new SnDHelper();
	SchemaValidatorUtils schemaValidatorUtils = new SchemaValidatorUtils();
	String order_id;
	String updatedNumberber;
	
	public static String[] createOrder(String restaurantId,long consumerAppMobile,String consumerAppPassword,String paymentMode,String comments) throws Exception {
		
		List<Cart> items = new ArrayList<>();
		Processor response = SnDHelper.menuV3RestId(restaurantId);
		ArrayList<Long> itemList = RMSCommonHelper.getInStockItemsFromMenu(response);
		String latLng = RMSCommonHelper.getRestLatLngFromMenu(response);
		Collections.shuffle(itemList);
		items.add(new Cart(null, null, String.valueOf(itemList.get(new Random().nextInt(itemList.size()/2))), 1));
		return RMSCommonHelper.createOrder(consumerAppMobile, consumerAppPassword, latLng,
				items, restaurantId, paymentMode, comments);
	}
	

	@DataProvider(name = "verifyFetchOrdersAtRMS")
	public static Object[][] a() throws Exception {
		HashMap<String, Object> myFetchOrderData = new HashMap<>();
		String restaurantId = "5127";
		String[] orderReponse = createOrder(restaurantId, RMSConstants.consumerAppMobile,
				RMSConstants.consumerAppPassword, RMSConstants.PAYMENTMODE_CASH, RMSConstants.TEST_ORDER_COMMENTS);
		HashMap<String, Object> orderInfo = new HashMap<>();
		orderInfo.put("orderReponse", orderReponse[1]);
		orderInfo.put("orderId", orderReponse[0]);
		myFetchOrderData.put("orderData", orderInfo);
		HashMap<String, Object> restaurantInfo = new HashMap<>();
		restaurantInfo.put("restaurantId", restaurantId);
		myFetchOrderData.put("restaurantData", restaurantInfo);
		myFetchOrderData.put("omsSessionData",
				RMSCommonHelper.getOMSSessionData(RMSConstants.OMS_UAT_USERNAME, RMSConstants.OMS_UAT_PASSWORD));
		myFetchOrderData.put("rmsSessionData",
				RMSCommonHelper.getRMSSessionData(RMSConstants.VENDOR_UAT_USERNAME, RMSConstants.VENDOR_UAT_PASSWORD));
		return new Object[][] { { myFetchOrderData } };
	}

	
//done
	@Test(dataProvider = "verifyFetchOrdersAtRMS", description = "CreateOrder ->Login -> Fetch orders -> Verify order details")
	public void verifyFetchOrdersAtRMS(HashMap<String, HashMap<String, String>> dataSet) throws Exception {
		OMSHelper omsHelper = new OMSHelper();
		HashMap<String, String> orderMap = dataSet.get("orderData");
		HashMap<String, String> omsSessionData = dataSet.get("omsSessionData");
		HashMap<String, String> rmsSessionData = dataSet.get("rmsSessionData");
		HashMap<String, String> restaurantData = dataSet.get("restaurantData");
		if (isOrderAvailableAtFF(orderMap.get("orderId"))) {
			boolean flg = omsHelper.verifyOrder(orderMap.get("orderId"), "010", omsSessionData.get("X-CSRFToken"),
					omsSessionData.get("sessionId"));
			System.out.println("myAccessToken :" + rmsSessionData.get("accessToken"));
			String date = getCurrentDateTimeInRequiredFormat(new Date(), "yyyy-MM-dd'T'HH:mm:ss");
			System.out.println("------------------->" + date);
			Thread.sleep(60000);
			String a = rmsHelper.fetchOrder(restaurantData.get("restaurantId"),
					rmsSessionData.get("accessToken")).ResponseValidator.GetBodyAsText();
			JsonNode nodeObject = APIUtils.convertStringtoJSON(a);
			Assert.assertEquals(nodeObject.get("statusCode").asInt(), 0);
			Assert.assertEquals(nodeObject.get("statusMessage").asText(), "Success");
			String Obj = getMeObjectOrderObjectFromList(orderMap.get("orderId"),
					Long.parseLong(restaurantData.get("restaurantId")), nodeObject);
			Assert.assertNotNull("FetchOrder API doesnt have OrderCreated " + Obj);
		} else {
			Assert.fail("FF issue  .. Order Not Reached FF So This DownStream Dependency Failed For VMS");
		}
	}
	
		
	ArrayList<Long> list = new ArrayList<>();
	@AfterClass
	public void a1() {
		for (Long l : list) {
			RngHelper.disableEnableTD(String.valueOf(l), "false");
		}
	}

}
