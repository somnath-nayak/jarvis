package com.swiggy.api.erp.ff.tests.omtRewrite.omtMiddleware;

import org.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.swiggy.api.erp.ff.dp.omtRewrite.OmtMiddlewareData;
import com.swiggy.api.erp.ff.helper.OmtMiddlewareHelper;
import framework.gameofthrones.JonSnow.Processor;

public class GetCustomerDetailsAPITests extends OmtMiddlewareData {

	OmtMiddlewareHelper OmtMiddlewareHelper = new OmtMiddlewareHelper();

	@Test(dataProvider = "getCustomerDetailsValidOrders", description = "OMT Middleware get customer details API")

	public void getCustomerDetailsAPITestsValidOrders(String orderId, String scenario, int expectedResponse) throws NumberFormatException, JSONException, InterruptedException {
		System.out.println("*get customer details with " + scenario + " started");
		
		Thread.sleep(1000);
		Processor response = OmtMiddlewareHelper.getCustomerDetails(orderId);
		
		Assert.assertEquals(response.ResponseValidator.GetResponseCode(), expectedResponse,"Actual response did not match with the expected response " + orderId);
		SoftAssert softAssertion = new SoftAssert();
		softAssertion.assertEquals(response.ResponseValidator.GetNodeValue("$.statusMessage"), "Success!","Actual Status message did not match with expected " + orderId);
		softAssertion.assertEquals(response.ResponseValidator.GetNodeValueAsInt("data.customer_id"),OmtMiddlewareHelper.getExpectedCustomerId(Long.parseLong(orderId)),"Actual customer Id did not match with Expected     " + orderId);
		softAssertion.assertEquals(response.ResponseValidator.GetNodeValue("data.name"),OmtMiddlewareHelper.getCustomerName(Long.parseLong(orderId)),"Actual name did not match with expected     " + orderId);
		softAssertion.assertEquals(response.ResponseValidator.GetNodeValueAsInt("data.mobile"), OmtMiddlewareHelper.getCustomerMobile(Long.parseLong(orderId)).intValue(),"Actual mobile did not match with expected     " + orderId);
		softAssertion.assertEquals(response.ResponseValidator.GetNodeValue("data.email"),OmtMiddlewareHelper.getCustomerEmail(Long.parseLong(orderId)),"Actual email did not match with expected      " + orderId);
		softAssertion.assertEquals(response.ResponseValidator.GetNodeValue("$.data.line1"),OmtMiddlewareHelper.getCustomerAddressLine(Long.parseLong(orderId)),"Actual addressline  did not match with expected" + orderId);
		softAssertion.assertEquals(response.ResponseValidator.GetNodeValue("data.flat_number"),OmtMiddlewareHelper.getCustomerFlatNumber(Long.parseLong(orderId)),"Actual flat # did not match with expected     " + orderId);
		softAssertion.assertEquals(response.ResponseValidator.GetNodeValue("data.city"),OmtMiddlewareHelper.getCustomerCity(Long.parseLong(orderId)),"Actual city  did not match with expected     " + orderId);
		softAssertion.assertEquals(response.ResponseValidator.GetNodeValue("data.landmark"),OmtMiddlewareHelper.getCustomerLandmark(Long.parseLong(orderId)),"Actual landmark did not match with expected    " + orderId);
		softAssertion.assertEquals(response.ResponseValidator.GetNodeValue("data.area"),OmtMiddlewareHelper.getCustomerArea(Long.parseLong(orderId)),"Actual area did not match with expected    " + orderId);
		softAssertion.assertEquals(response.ResponseValidator.GetNodeValue("data.customer_lat"),OmtMiddlewareHelper.getCustomerlat(Long.parseLong(orderId)),"Actual customer lat did not match with expected    " + orderId);
		softAssertion.assertEquals(response.ResponseValidator.GetNodeValue("data.customer_lng"),OmtMiddlewareHelper.getCustomerLng(Long.parseLong(orderId)),"Actual customer lng  did not match with expected    " + orderId);
		softAssertion.assertAll();

		System.out.println(" getCustomerDetails with " + scenario + " completed....");
	}
	
	@Test(dataProvider = "getCustomerDetailsInvalidOrders", description = "OMT Middleware get customer details API")

	public void getCustomerDetailsAPITestsInvalidOrders(String orderId, String scenario,int expectedResponse) throws NumberFormatException, JSONException {
		System.out.println("*get customer details with " + scenario + " started");
		Processor response = OmtMiddlewareHelper.getCustomerDetails(orderId);
		Assert.assertEquals(response.ResponseValidator.GetResponseCode(), expectedResponse,"Actual response did not match with the expected response " + orderId);
		SoftAssert softAssertion = new SoftAssert();
		softAssertion.assertEquals(response.ResponseValidator.GetNodeValue("$.statusMessage"), "{\"status_code\":404,\"status_message\":\"No entity found for query\",\"data\":null}","Actual Status did not match" + orderId);
		softAssertion.assertAll();

		System.out.println(" getCustomerDetails with " + scenario + " completed....");
	}

	@Test(dataProvider = "getCustomerDetailsDataPostPutDelete", description = "OMT Middle ware get customer details API")
	public void getCustomerDetailsAPITestAsPost(String orderId, String scenario, int expectedResponse)throws NumberFormatException, JSONException {
		
		System.out.println("*get customer details with " + scenario + " started");
		Processor response = OmtMiddlewareHelper.getCustomerDetailsAsPost(orderId);
		Assert.assertEquals(response.ResponseValidator.GetResponseCode(), expectedResponse,"Actual response did not match with the expected response " + orderId);
		System.out.println(" getCustomerDetails with " + scenario + "compleated....");
	}

	@Test(dataProvider = "getCustomerDetailsDataPostPutDelete", description = "OMT Middleware get customer details API")

	public void getCustomerDetailsAPITestAsPut(String orderId, String scenario, int expectedResponse) throws NumberFormatException, JSONException {
		
		System.out.println("*get customer details with " + scenario + " started");
		Processor response = OmtMiddlewareHelper.getCustomerDetailsAsPut(orderId);
		Assert.assertEquals(response.ResponseValidator.GetResponseCode(), expectedResponse, "Actual response did not match with the expected response " + orderId);
		System.out.println(" getCustomerDetails with " + scenario + "compleated....");
	}

	@Test(dataProvider = "getCustomerDetailsDataPostPutDelete", description = "OMT Middle ware get customer details API")

	public void getCustomerDetailsAPITestAsDelete(String orderId, String scenario, int expectedResponse) throws NumberFormatException, JSONException {
		
		System.out.println("*get customer details with " + scenario + " started");
		Processor response = OmtMiddlewareHelper.getCustomerDetailsAsDelete(orderId);
		Assert.assertEquals(response.ResponseValidator.GetResponseCode(), expectedResponse, "Actual response did not match with the expected response " + orderId);
		System.out.println(" getCustomerDetails with " + scenario + "completed....");
	}
}
