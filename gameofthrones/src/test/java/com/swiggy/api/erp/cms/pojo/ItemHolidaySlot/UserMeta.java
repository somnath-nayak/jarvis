package com.swiggy.api.erp.cms.pojo.ItemHolidaySlot;

import org.apache.commons.lang.builder.ToStringBuilder;

public class UserMeta {

    private String source;
    private Meta meta;

    /**
     * No args constructor for use in serialization
     *
     */
    public UserMeta() {
    }

    /**
     *
     * @param source
     * @param meta
     */
    public UserMeta(String source, Meta meta) {
        super();
        this.source = source;
        this.meta = meta;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("source", source).append("meta", meta).toString();
    }

}

