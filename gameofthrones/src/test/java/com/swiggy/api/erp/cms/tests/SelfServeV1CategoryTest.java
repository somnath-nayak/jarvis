package com.swiggy.api.erp.cms.tests;

import framework.gameofthrones.JonSnow.Processor;
import org.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.dp.SelfServeV1Dp;
import com.swiggy.api.erp.cms.helper.SelfServeHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HemaGovinadaraj on 14/2/18.
 */
public class SelfServeV1CategoryTest extends SelfServeV1Dp {
	SelfServeHelper sshelper= new SelfServeHelper();
	String catTicketId,restaurantID;
	public static List<String> createticketId = new ArrayList<String>(), ticketslistnotassigned = new ArrayList<String>();
	public static List<Boolean> expectedstatuscreate = new ArrayList<Boolean>();

	@Test(dataProvider="updateCategory", priority=0, groups={"Sanity_TC", "Smoke_TC", "Regression_TC"},
			description="update category ticket")
	public void updateCategoryTicket(String restId,String catId,String type,String expectedData,String errorMessage,boolean expectedResult)
			throws JSONException, InterruptedException
	{

		String response = sshelper.updateCategory(restId,catId,type,expectedData).ResponseValidator.GetBodyAsText();
		if (errorMessage==null)
		{
			if(type=="Cat")
				catTicketId = JsonPath.read(response, "$.ticket_id").toString();
			restaurantID=restId;
			String responsecatName = JsonPath.read(response, "$.category_name").toString();
			String responseType = JsonPath.read(response, "$.category_type").toString();
			Assert.assertEquals(expectedData, responsecatName, "Success");
			Assert.assertEquals(responseType, type, "Success");
			createticketId.add(JsonPath.read(response, "$.ticket_id").toString().replace("[","").replace("]","").toString());
			expectedstatuscreate.add(expectedResult);


		}
		else
		{
			Assert.assertEquals(errorMessage, response, "Success");
		}


	}

	@Test(dataProvider="getCategoryTicketByTicketID", priority=1, groups={"Sanity_TC", "Smoke_TC", "Regression_TC"},
			description="fetch ticket details of category")
	public void getCategoryByTicketId(String catId,String catUpdateName,boolean status)
			throws JSONException, InterruptedException
	{

		String response;
		if (status==true)
		{
			response = sshelper.getCategoryTicketId(catTicketId,restaurantID).ResponseValidator.GetBodyAsText();
			String ticketId = JsonPath.read(response, "$.ticket_id").toString();
			String catName = JsonPath.read(response, "$.category_name").toString();
			Assert.assertEquals(ticketId, catTicketId, "Success");
			Assert.assertEquals(ticketId, catTicketId, "Success");
			Assert.assertEquals(catName, catUpdateName, "Success");
		}
		else
		{
			response = sshelper.getCategoryTicketId(catId,restaurantID).ResponseValidator.GetBodyAsText();
			Assert.assertEquals(catUpdateName, response, "Success");
		}


	}

	@Test(dataProvider="submitCategoryticket",priority=2,groups={"Sanity_TC", "Smoke_TC", "Regression_TC"},
			description="submit ticket")
	public void submitticket(String payload,String restId,int validationMessage,int status)
			throws JSONException, InterruptedException
	{
		Processor p= sshelper.submitTicket(restId,payload);
		String response =p.ResponseValidator.GetBodyAsText();
		int statuscode=p.ResponseValidator.GetResponseCode();
		int responsearray = Integer.parseInt(JsonPath.read(response, "$.statusCode").toString());
		Assert.assertEquals(validationMessage,responsearray , "Success");
		Assert.assertEquals(statuscode, status, "Success");

	}

	@Test(dataProvider="approveTicketCategory", priority=3,groups={"Sanity_TC", "Smoke_TC", "Regression_TC"},
			description="approve ticket")
	public void approveTicket(String payload,String ticketId,String ticketState,String agentid,int validationMessage,int status)
			throws JSONException, InterruptedException
	{	Processor p;
	    System.out.println("sad");
		p= sshelper.updatecategoryticketstate(ticketId,ticketState,payload,agentid);
		String response =p.ResponseValidator.GetBodyAsText();
		int statuscode=p.ResponseValidator.GetResponseCode();
		int responseitemName =Integer.parseInt(JsonPath.read(response, "$.status").toString());
		Assert.assertEquals(validationMessage, responseitemName, "Success");
		Assert.assertEquals(status, statuscode, "Success");
	}


}
