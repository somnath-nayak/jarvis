package com.swiggy.api.erp.cms.tests.CatalogV2;

        import com.jayway.jsonpath.JsonPath;
        import com.swiggy.api.erp.cms.helper.CatalogV2Helper;
        import framework.gameofthrones.Cersei.ToolBox;
        import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
        import org.json.JSONObject;
        import org.testng.Assert;
        import org.testng.annotations.Test;

        import java.util.List;

public class AddAttributesToCategory {

    SchemaValidatorUtils schemaValidatorUtils = new SchemaValidatorUtils();
    CatalogV2Helper catalogV2Helper = new CatalogV2Helper();
    String catId;
    @Test
    public void AddAttributesToCategory() throws Exception{

        String jsonSchema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/Schemaset/Json/CMS/addattributestocategory.txt");

        String response=catalogV2Helper.createCategoryPojo().ResponseValidator.GetBodyAsText();
        catId= JsonPath.read(response,"$.id");
        Assert.assertNotNull(catId);
        String nameOfTheAttributeInRequest = "test"+catId;

        String responseOfAddAttribute = catalogV2Helper.createPojoForAddAttributesToACategory(nameOfTheAttributeInRequest,catId).ResponseValidator.GetBodyAsText();
        boolean doesNameOfTheAttributeMatches = catalogV2Helper.isNameChangedByAddingAttribute(responseOfAddAttribute,nameOfTheAttributeInRequest);

        Assert.assertTrue(doesNameOfTheAttributeMatches,"The attribute added was not seen in response... Hence failing the test case");

        /*List<String> missingAddedNodeList = schemaValidatorUtils.validateServiceSchema(jsonSchema,responseOfAddAttribute);
        Assert.assertTrue(missingAddedNodeList.isEmpty(),missingAddedNodeList + "Nodes are missing or data type of the object is mismatching");*/




    }

}
