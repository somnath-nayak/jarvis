package com.swiggy.api.erp.cms.tests;

import org.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.dp.Critical_API_dataProvider;
import com.swiggy.api.erp.cms.helper.CriticalAPI_Helper;

import framework.gameofthrones.JonSnow.Processor;

/*
 *   get the item-ds for the respective external-ids provided for a given restaurant
 */

public class ThirdPartyReverseMap extends Critical_API_dataProvider {
	
	CriticalAPI_Helper obj =  new CriticalAPI_Helper();
	
	
	@Test(dataProvider="thirdpartyreversemap_getMultipleItemIds", priority=0, groups={"Sanity_TC", "Smoke_TC", "Regression_TC"}, 
			description="gets multiple item-ids matching with respective external-ids of a restaurant")
	public void getMultipleItemIds(String external_item_ids, int restaurant_id)
			throws JSONException, InterruptedException
	{
		  Processor p1 = obj.getItemIdsWithexternalIds(external_item_ids, restaurant_id);
		  
		  String response = p1.ResponseValidator.GetBodyAsText();
		  
		  System.out.println(response);
		  
		  String item_mapping1 =  JsonPath.read(response, "$.data.item_mapping.99").toString();
		  String item_mapping2 =  JsonPath.read(response, "$.data.item_mapping.98").toString();
		  String item_mapping3 =  JsonPath.read(response, "$.data.item_mapping.97").toString();
			 
		  Assert.assertEquals(item_mapping1, "3821448");
		  Assert.assertEquals(item_mapping2, "3821452");
		  Assert.assertEquals(item_mapping3, "3821454");
			
		  int statusCode = JsonPath.read(response, "$.statusCode");
		  Assert.assertEquals(statusCode, 1);
		  
	}
	
	@Test(dataProvider="thirdpartyreversemap_getSingleItemIds", priority=1, groups={"Sanity_TC", "Smoke_TC", "Regression_TC"}, 
			description="gets single item-id matching with a respective external-id of a restaurant")
	public void getSingleItemId(String external_item_ids, int restaurant_id)
			throws JSONException, InterruptedException
	{
		  Processor p1 = obj.getItemIdsWithexternalIds(external_item_ids, restaurant_id);
		  
		  String response = p1.ResponseValidator.GetBodyAsText();
		  System.out.println(response);
		  
		  String item_mapping1 =  JsonPath.read(response, "$.data.item_mapping.99").toString();
		  Assert.assertEquals(item_mapping1, "3821448");
		  int statusCode = JsonPath.read(response, "$.statusCode");
		  Assert.assertEquals(statusCode, 1);
		  
	}
	
	
	@Test(dataProvider="thirdpartyreversemap_invalidExternalIds", priority=1, groups={"Smoke_TC", "Regression_TC"}, 
			description="gets empty item-ids for invalid external-ids of a restaurant")
	public void invalidExternalIds(String external_item_ids, int restaurant_id)
			throws JSONException, InterruptedException
	{
		  Processor p1 = obj.getItemIdsWithexternalIds(external_item_ids, restaurant_id);
		  
		  String response = p1.ResponseValidator.GetBodyAsText();
		  System.out.println(response);
		  
		  String item_mapping1 =  JsonPath.read(response, "$.data.item_mapping").toString();
		  Assert.assertEquals(item_mapping1, "{}");
		  int statusCode = JsonPath.read(response, "$.statusCode");
		  Assert.assertEquals(statusCode, 1);
		  
	}
	
	
	@Test(dataProvider="thirdpartyreversemap_emptyExternalIds", priority=1, groups={"Sanity_TC", "Smoke_TC", "Regression_TC"}, 
			description="gets empty item-ids for empty external-ids of a restaurant")
	public void emptyExternalIds(String external_item_ids, int restaurant_id)
			throws JSONException, InterruptedException
	{
		  Processor p1 = obj.getItemIdsWithexternalIds(external_item_ids, restaurant_id);
		  
		  String response = p1.ResponseValidator.GetBodyAsText();
		  System.out.println(response);
		  
		  String item_mapping1 =  JsonPath.read(response, "$.data.item_mapping").toString();
		  Assert.assertEquals(item_mapping1, "{}");
		  int statusCode = JsonPath.read(response, "$.statusCode");
		  Assert.assertEquals(statusCode, 1);
		  
	}
	
	@Test(dataProvider="thirdpartyreversemap_duplicateExternalIds", priority=0, groups={"Sanity_TC", "Smoke_TC", "Regression_TC"}, 
			description="gets multiple item-ids matching with respective duplicate external-ids of a restaurant")
	public void duplicateExternalIds(String external_item_ids, int restaurant_id)
			throws JSONException, InterruptedException
	{
		  Processor p1 = obj.getItemIdsWithexternalIds(external_item_ids, restaurant_id);
		  String response = p1.ResponseValidator.GetBodyAsText();
		  
		  System.out.println(response);
		  
		  String item_mapping1 =  JsonPath.read(response, "$.data.item_mapping.99").toString();
		  String item_mapping2 =  JsonPath.read(response, "$.data.item_mapping.98").toString();
		  String item_mapping3 =  JsonPath.read(response, "$.data.item_mapping.97").toString();
			 
		  Assert.assertEquals(item_mapping1, "3821448");
		  Assert.assertEquals(item_mapping2, "3821452");
		  Assert.assertEquals(item_mapping3, "3821454");
		 
		  int statusCode = JsonPath.read(response, "$.statusCode");
		  Assert.assertEquals(statusCode, 1);
		  
	}
	
	
	@Test(dataProvider="thirdpartyreversemap_multipleExternalIdswithoutArray", priority=0, groups={"Smoke_TC", "Regression_TC"}, 
			description="pass multiple valid external-ids without an array")
	public void multipleExternalIdsWithoutArray(String external_item_ids, int restaurant_id)
			throws JSONException, InterruptedException
	{
		  Processor p1 = obj.getItemIdsWithexternalIds(external_item_ids, restaurant_id);
		  
		  String response = p1.ResponseValidator.GetBodyAsText();
		  System.out.println(response);
		  
		  String fieldError = JsonPath.read(response, "$.data.field_validation_errors[0].field");
		  String valueError = JsonPath.read(response, "$.data.field_validation_errors[0].value");
		  String statusMessage = JsonPath.read(response, "$.statusMessage");
		  	
		  Assert.assertEquals(fieldError, "external_item_ids");
		  Assert.assertEquals(valueError, "Invalid value");
		  Assert.assertEquals(statusMessage, "Validation Error");
		 
		  int statusCode = JsonPath.read(response, "$.statusCode");
		  Assert.assertEquals(statusCode, 0);
		  
	}
	
	
	@Test(dataProvider="thirdpartyreversemap_singleExternalIdswithoutArray", priority=0, groups={"Smoke_TC", "Regression_TC"}, 
			description="pass single valid external-id without an array")
	public void singleExternalIdsWithoutArray(String external_item_ids, int restaurant_id)
			throws JSONException, InterruptedException
	{
		  Processor p1 = obj.getItemIdsWithexternalIds(external_item_ids, restaurant_id);

		  String response = p1.ResponseValidator.GetBodyAsText();
		  System.out.println(response);
		  
		  String fieldError = JsonPath.read(response, "$.data.field_validation_errors[0].field");
		  String valueError = JsonPath.read(response, "$.data.field_validation_errors[0].value");
		  String statusMessage = JsonPath.read(response, "$.statusMessage");
		  
		  Assert.assertEquals(fieldError, "external_item_ids");
		  Assert.assertEquals(valueError, "Invalid value");
		  Assert.assertEquals(statusMessage, "Validation Error");
		 
		  int statusCode = JsonPath.read(response, "$.statusCode");
		  Assert.assertEquals(statusCode, 0);
	}
	

	@Test(dataProvider="thirdpartyreversemap_externalIdsWithoutRestaurantId", priority=0, groups={"Regression_TC"}, 
			description="pass multiple valid external-ids without a restaurant id")
	public void multipleExternalIdsWithoutRestaurantId(String external_item_ids, String restaurant_id)
			throws JSONException, InterruptedException
	{
		  Processor p1 = obj.getItemIdswithoutRestId(external_item_ids, restaurant_id);
		  String response = p1.ResponseValidator.GetBodyAsText();
		  System.out.println(response);
		  
		  String fieldError = JsonPath.read(response, "$.data.field_validation_errors[0].field");
		  String valueError = JsonPath.read(response, "$.data.field_validation_errors[0].value");
		  String statusMessage = JsonPath.read(response, "$.statusMessage");

		  Assert.assertEquals(fieldError, "restaurant_id");
		  Assert.assertEquals(valueError, "Invalid value");
		  Assert.assertEquals(statusMessage, "Validation Error");
		 
		  int statusCode = JsonPath.read(response, "$.statusCode");
		  Assert.assertEquals(statusCode, 0);
	}
	
	
	
}
