package com.swiggy.api.erp.delivery.tests;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import org.apache.log4j.FileAppender;
import org.json.JSONException;

import org.json.JSONException;
import org.json.JSONObject;
import framework.gameofthrones.Daenerys.Initializer;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.crm.foodissues.pojo.CCResolution.ResolutionRequestData;
import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.erp.delivery.dp.DeliveryServiceDataProvider;
import com.swiggy.api.erp.delivery.helper.AutoassignHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryDataHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryHelperMethods;
import com.swiggy.api.erp.delivery.helper.DeliveryReferalHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceHelper;
import com.swiggy.api.erp.ff.helper.LOSHelper;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.DBHelper;

public class DeliveryServiceRegressionTest extends DeliveryServiceDataProvider {


	static String header = null, auth, auth1, cycleauth, auth2;
	//Initialize gameofthrones = new Initialize();

	//Initialize gameofthrones = new Initialize();
	Initialize gameofthrones =Initializer.getInitializer();

	DeliveryServiceHelper helper = new DeliveryServiceHelper();
	DeliveryHelperMethods delmeth=new DeliveryHelperMethods();
	AutoassignHelper deauto= new AutoassignHelper();
	DBHelper dbHelper = new DBHelper();
	DeliveryDataHelper deliverydata =new DeliveryDataHelper(); 

	
	@Test(dataProvider = "delogin", description="de login" , groups="sanity")
	public void delogin(String de_id,int statuscode) throws InterruptedException {
		Processor processor=helper.makeDEActive(de_id);
		String resp = processor.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(resp, "$.statusCode");
		Assert.assertEquals(statuscode, statusCode);
		
	}
	
	@Test(testName = "freede", dataProvider = "defree", description="makeing DE free ", groups="sanity")
	public void freedem(String de_id, int statuscode) throws InterruptedException {
		Processor processor=helper.makeDEFree(de_id);
		String resp = processor.ResponseValidator.GetBodyAsText();
		int resp1= processor.ResponseValidator.GetResponseCode();
		if (resp1==200)
		{
			Assert.assertEquals(statuscode, resp1);
		}
		else {
		int status = JsonPath.read(resp, "$.statusCode");
		Assert.assertEquals(statuscode, status);

	}

	}
		@Test(testName = "debusy", dataProvider = "debusy", description="makeing de busy ",groups="sanity")
		public void makedebusy(String de_id, int statuscode) throws InterruptedException {
			Processor processor=helper.makeDEBusy(de_id);
			String resp = processor.ResponseValidator.GetBodyAsText();
			int resp1= processor.ResponseValidator.GetResponseCode();
			if (resp1==200)
			{
				Assert.assertEquals(statuscode, resp1);
			}
			else {
			int status = JsonPath.read(resp, "$.statusCode");
			Assert.assertEquals(statuscode, status);

		}
		}
		

	
  
	@Test(dataProvider = "orderassignn", description="Assigning orders to de ", groups="Login")
	public void orderrassign(String order_id, String delivery_boy_id,
			int status_code) throws Exception {
		
		helper.makeDEFree(delivery_boy_id);	
		Processor processor = helper.orderAssign(order_id, delivery_boy_id);
		String resp = processor.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(resp, "$.statusCode");
		Assert.assertEquals(status_code, statusCode);
		if (statusCode == 200) {
			String statusmessage = JsonPath.read(resp, "$.statusMessage");
			int requestId1 = JsonPath.read(resp, "$.requestId");
			Assert.assertEquals("Success", statusmessage);
			Assert.assertNotNull(requestId1);
		}

	}
	

	@Test(dataProvider = "reassign", description="reassign orders to De ", groups="sanity")
	public void reassign(String order_id, String reassign_status_code,
			String city_id, int status_code) throws Exception {

		Processor processor = helper.reassign(order_id, reassign_status_code,
				city_id);
		String resp = processor.ResponseValidator.GetBodyAsText();
		// Assert.assertEquals(200,processor.ResponseValidator.GetResponseCode());
		int statusCode = JsonPath.read(resp, "$.statusCode");
		Assert.assertEquals(status_code, statusCode);
		if (statusCode == 200) {
			String statusmessage = JsonPath.read(resp, "$.statusMessage");
			int requestId1 = JsonPath.read(resp, "$.requestId");
			// String requestid =
			// processor.ResponseValidator.GetNodeValueAsStringFromJsonArray(".$requestId");
			Assert.assertEquals("Success", statusmessage);
			Assert.assertNotNull(requestId1);
		}

	}
	

	@Test(dataProvider = "unassign",description="Unassign order from De ", groups="sanity")
	public void unassign(String order_id, String delivery_boy_id,
			int status_code) throws Exception {

		Processor processor = helper.unAssign(order_id, delivery_boy_id);
		String resp = processor.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(resp, "$.statusCode");
		Assert.assertEquals(status_code, statusCode);
		if (statusCode == 200) {
			String statusmessage = JsonPath.read(resp, "$.statusMessage");
			int requestId1 = JsonPath.read(resp, "$.requestId");
			Assert.assertEquals("Success", statusmessage);
			Assert.assertNotNull(requestId1);
		}

	}

	@Test(groups = { "regression" }, priority = 4, dataProvider = "OrderCollectData")
	public void orderCollectTest(String de_id, String de_rating,String order_id, String time_stamp) {
		System.out.println("******************************OrderCollectTest is started******************************");
		GameOfThronesService service = new GameOfThronesService(
				"deliveryservice", "OrderCollect", gameofthrones);

		Processor processor = new Processor(service,
				delmeth.doubleheader(), new String[] { de_id, de_rating,
						order_id, time_stamp });
		String response = processor.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(response, "$.statusCode");
		Assert.assertEquals(200, statusCode);
		System.out.println("**********************OrderCollectTest is stopped******************************");

	}

	@Test(groups = { "regression" }, priority = 5, dataProvider = "AddArea_MappedDEData")
	public void deleteArea_MappedDETest(String de_id, String area) {
		System.out.println("*******************AddArea_MappedDETest is started******************************");
		GameOfThronesService service = new GameOfThronesService(
				"deliveryservice", "DeleteArea_MappedDE", gameofthrones);

		String data = helper.addArea_MappedDETest(de_id, area);
		System.out
				.println("******************************AddArea_MappedDETest is stopped******************************");

		System.out
				.println("******************************DeleteArea_MappedDETest is started******************************");
		Processor processor = new Processor(service,
			delmeth.doubleheader(), new String[] { data });
		String response = processor.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(response, "$.statusCode");
		Assert.assertEquals(200, statusCode);
		System.out
				.println("******************************DeleteArea_MappedDETest is stopped******************************");

	}
	
	// update parameter for autoassigment updating deweight
		@Test(dataProvider = "updateDeWeightParam",enabled=true,description ="update parameter for autoassigment updating deweight: updating de weight")
		public void updatedeparam(String City, String zone, String Param_id, String Param_value, String user)
				throws InterruptedException {
			Processor processor = helper.updateParamAuto(City,zone,Param_id,Param_value,user);
			Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
			
		}

		@Test(dataProvider = "orderpay",enabled = true, priority = 3, description ="Updates orders pay")
		public void Orderpay(String order_id, String order_pay,String status_code,String message ) throws Exception {
			HashMap<String, String> requestheaders = new HashMap<String, String>();
				        requestheaders.put("Content-Type", "application/json");
				        requestheaders.put("Authorization", "Basic R0dZU1dJOjIwMTVTVyFHR1k=");
			            GameOfThronesService service = new GameOfThronesService("deliveryservice", "orderpay", gameofthrones);
				        String[] payloadparams = new String[2];
				        payloadparams[0] = order_id;
				        payloadparams[1]= order_pay;
				        Processor processor = new Processor(service,requestheaders,payloadparams,null);
				        String resp=processor.ResponseValidator.GetBodyAsText();
				        System.out.println("Response "+ resp);
				        String statuscod = JsonPath.read(resp,"$.statusCode").toString();
				        System.out.println("status code success"+statuscod);
				        Assert.assertEquals(status_code,statuscod);
				        String msg = JsonPath.read(resp,"$.statusMessage");
				        System.out.println("Status Message"+ msg);
		                Assert.assertEquals(message,msg);
	
			}
		
		
		
		//***********************************Food Issues API*********************************************
		@Test(dataProvider = "FoodIssues", enabled = true, description = "Verify the Food Issues BILL_MISMATCH case",priority=7)
		public void foodIssuesBillMismatch(String orderId, String auth,String inputValue, String inputType, String billProvided, String verifyWithVendor) throws InterruptedException {
			System.out.println("********************************Food Issues regression starts here*************************");
			Thread.sleep(10000);
			this.helper=new DeliveryServiceHelper(auth);
			Processor referral=helper.billCheck(orderId,inputValue,inputType, billProvided,verifyWithVendor);
			JSONObject eligibilityOb=null;
			String eligibilitySt=null;
			try {
				 eligibilityOb=(JSONObject) (new JSONObject(referral.ResponseValidator.GetBodyAsText())).get("data");
				 eligibilitySt=eligibilityOb.getString("issueType").toString();
			} catch (JSONException e) {
				e.printStackTrace();
			}
			if(eligibilitySt.equalsIgnoreCase("BILL_MISMATCH"))
			{
	    		Assert.assertTrue(true);
			}
			else
			{
	    		Assert.assertTrue(false);

			}
		    System.out.println("*#################################################################################*");

			}
		@Test(dataProvider = "FoodIssues", enabled = true, description = "Verify the food issues NO_ISSUE case",priority=8)
		public void foodIssuesNoIssues(String orderId, String auth,String inputValue, String inputType, String billProvided, String verifyWithVendor) throws InterruptedException {
			Thread.sleep(3000);
			this.helper=new DeliveryServiceHelper(auth);
			Processor referral=helper.billCheck(orderId,"411",inputType, billProvided,verifyWithVendor);
			JSONObject eligibilityOb=null;
			String eligibilitySt=null;
			try {
				 eligibilityOb=(JSONObject) (new JSONObject(referral.ResponseValidator.GetBodyAsText())).get("data");
				 eligibilitySt=eligibilityOb.getString("issueType").toString();
			} catch (JSONException e) {
				e.printStackTrace();
			}
			if(eligibilitySt.equalsIgnoreCase("NO_ISSUE"))
			{
	    		Assert.assertTrue(true);
			}
			else
			{
	    		Assert.assertTrue(false);

			}
		    System.out.println("*#################################################################################*");

			}
		
		
		@Test(dataProvider = "FoodIssues", enabled = true, description = "Verify the food issues BILL_NOT_PROVIDED case ",priority=9)
		public void foodIssuesNoBill(String orderId, String auth,String inputValue, String inputType, String billProvided, String verifyWithVendor) throws InterruptedException {
			this.helper=new DeliveryServiceHelper(auth);
			Processor referral=helper.billCheck(orderId,inputValue,inputType, "false",verifyWithVendor);
			JSONObject eligibilityOb=null;
			String eligibilitySt=null;
			try {
				 eligibilityOb=(JSONObject) (new JSONObject(referral.ResponseValidator.GetBodyAsText())).get("data");
				 eligibilitySt=eligibilityOb.getString("issueType").toString();
			} catch (JSONException e) {
				e.printStackTrace();
			}
			if(eligibilitySt.equalsIgnoreCase("BILL_NOT_PROVIDED"))
			{
	    		Assert.assertTrue(true);
			}
			else
			{
	    		Assert.assertTrue(false);

			}
		    System.out.println("*#################################################################################*");

			}
		

		@Test(dataProvider = "FoodIssuesAck", enabled = true, description = "Verify the Food issues acknowledgement api",priority=10)
		public void foodIssuesAck(String orderId, String auth, String billProvided) throws InterruptedException {
			Thread.sleep(3000);
			this.helper=new DeliveryServiceHelper(auth);
			Processor referral=helper.foodIssueAck(orderId, billProvided);
			JSONObject eligibilityOb=null;
			String eligibilitySt=null;
			try {
				 eligibilityOb= new JSONObject(referral.ResponseValidator.GetBodyAsText());
				 eligibilitySt=eligibilityOb.getString("statusMessage").toString();
			} catch (JSONException e) {
				e.printStackTrace();
			}
			if(eligibilitySt.equalsIgnoreCase("success"))
			{
	    		Assert.assertTrue(true);
	    		System.out.println("*******************Food Issues regression stops here************************");
			}
			else
			{
	    		Assert.assertTrue(false);
	    		System.out.println("*******************Food Issues regression stops here************************");

			}
		    System.out.println("*#################################################################################*");

			}

		
@Test(dataProvider = "merge", description="Verify whether the order is mergrd or not", groups="Sanity")
public void mergeorders(String order_id1, String order_id2,
		int status_code) throws Exception {

	Processor processor = helper.mergeorder(order_id1, order_id2);
	String resp = processor.ResponseValidator.GetBodyAsText();
	int statusCode = JsonPath.read(resp, "$.statusCode");
	Assert.assertEquals(status_code, statusCode);
	if (statusCode == 200) {
		String statusmessage = JsonPath.read(resp, "$.statusMessage");
		int requestId1 = JsonPath.read(resp, "$.requestId");
		Assert.assertEquals("Success", statusmessage);
		Assert.assertNotNull(requestId1);
	}

}







@Test(dataProvider = "nextorder", description="pre-order assigment", groups="sanity")
public void nextorder(String order_id, String de_id,
		int status_code) throws Exception {

	Processor processor = helper.nextorder(order_id, de_id);
	String resp = processor.ResponseValidator.GetBodyAsText();
	int statusCode = JsonPath.read(resp, "$.statusCode");
	Assert.assertEquals(status_code, statusCode);

}




@Test(dataProvider = "location",description="Verify the location of the de ",  groups="sanity")
public void location(String lat,String lng,String accuracy, String discardedLocation,
		int status_code,String de_id,String version) throws Exception {

	// DE  Login
	Processor processor = deauto.delogin(de_id, version);
	Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
	String deotp = (String) delmeth.redisconnectget(DeliveryConstant.de_otp, de_id,
			DeliveryConstant.redisdb);
	Processor processor1 = deauto.deotp(de_id, deotp);
   Assert.assertEquals(processor1.ResponseValidator.GetResponseCode(), 200);
	String resp = processor1.ResponseValidator.GetBodyAsText();
	auth = JsonPath.read(resp, "$.data.Authorization").toString();
	//Processor processor2 = deauto.locupdate(lat, lng, auth);
	Processor processor3=deauto.DElocation(lat, lng, accuracy, discardedLocation, auth);
	String resp1 = processor3.ResponseValidator.GetBodyAsText();
	int statusCode = JsonPath.read(resp1, "$.statusCode");
	Assert.assertEquals(status_code, statusCode);
	if (statusCode == 200) {
		String statusmessage = JsonPath.read(resp, "$.statusMessage");
		int requestId1 = JsonPath.read(resp, "$.requestId");
		Assert.assertEquals("Success", statusmessage);
		Assert.assertNotNull(requestId1);
	}

}


@Test(dataProvider = "floatingcash",  description="get all the options of channel", groups ="floating cash")
public void floating(String de_id,String version,int status_code) throws Exception {

   helper.makeDEActive(de_id);
	
	Processor processor = deauto.delogin(de_id, version);
	Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
	String deotp = (String) delmeth.redisconnectget(DeliveryConstant.de_otp, de_id,
			DeliveryConstant.redisdb);
	Processor processor1 = deauto.deotp(de_id, deotp);
	Assert.assertEquals(processor1.ResponseValidator.GetResponseCode(), 200);
	String resp = processor1.ResponseValidator.GetBodyAsText();
	auth = JsonPath.read(resp, "$.data.Authorization").toString();
	Processor processor2=helper.floatingcashChannel(auth);
	Assert.assertEquals(processor2.ResponseValidator.GetResponseCode(), 200);
	if (status_code == 200) {
		String statusmessage = JsonPath.read(resp, "$.statusMessage");
		int requestId1 = JsonPath.read(resp, "$.requestId");
		Assert.assertEquals("Success", statusmessage);
		Assert.assertNotNull(requestId1);
	}
	   

}


@Test(dataProvider = "dynamicCode",  description="To get channnel option details", groups="floating cash")
public void floating1(String de_id,String version,int status_code, String channelid) throws Exception {

	Processor processor = deauto.delogin(de_id, version);
	Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
	String deotp = (String) delmeth.redisconnectget(DeliveryConstant.de_otp, de_id,
			DeliveryConstant.redisdb);
	Processor processor1 = deauto.deotp(de_id, deotp);
	Assert.assertEquals(processor1.ResponseValidator.GetResponseCode(), 200);
	String resp = processor1.ResponseValidator.GetBodyAsText();
	auth = JsonPath.read(resp, "$.data.Authorization").toString();
	Processor processor2 =helper.floatingdynamiccode(auth, channelid);
	Assert.assertEquals(processor2.ResponseValidator.GetResponseCode(), 200);
	if (status_code == 200) {
		String statusmessage = JsonPath.read(resp, "$.statusMessage");
		int requestId1 = JsonPath.read(resp, "$.requestId");
		Assert.assertEquals("Success", statusmessage);
		Assert.assertNotNull(requestId1);
	}
   String text= processor2.ResponseValidator.GetBodyAsText();
	
	String dynamicCode=JsonPath.read(text,"$..dynamic_code").toString().replace("[","").replace("]","" );
	//Assert.assertEquals("dynamiccode", dynamicCode);
    if(dynamicCode == DeliveryConstant.deflotingcashCDM)
    {
    	System.out.println("deposit through ICICI cash deposit mechain"+dynamicCode);
    }
    else if(dynamicCode == DeliveryConstant.deflotingNDP){
    	System.out.println("deposit through Nopay"+dynamicCode);

     }
    else if(dynamicCode == DeliveryConstant.defoatingSDH)
    {	System.out.println("deposit through swiggy hub"+dynamicCode);
    	}
    
    else if(dynamicCode == DeliveryConstant.defloatingcashUPI)
    {
    	System.out.println("transfer through UPI"+dynamicCode);
    }
    else{
    	
    	System.out.println("invalide channel"  +dynamicCode);
    }
    }

@Test(dataProvider = "blocking", description="Verify that De is blocked", groups="floating cash")

public void blockde( String de_id,int status_code) throws InterruptedException
{  
	
Processor processor = helper.blockde(de_id);
//Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
String resp = processor.ResponseValidator.GetBodyAsText();
int statusCode = JsonPath.read(resp, "$.statusCode");
Assert.assertEquals(status_code, statusCode);
String statusmessage = JsonPath.read(resp, "$.statusMessage");
System.out.println("statusmessage"   +  statusmessage);
helper.unblockde(de_id);
}


@Test(dataProvider = "blocking", description="verify that de is unblocked", groups="floating cash")

public void unblockde( String de_id, int Status_code) throws InterruptedException
{  
	
Processor processor = helper.unblockde(de_id);
//Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
String resp = processor.ResponseValidator.GetBodyAsText();
int statusCode = JsonPath.read(resp, "$.statusCode");
Assert.assertEquals(Status_code, statusCode);
String statusmessage = JsonPath.read(resp, "$.statusMessage");
System.out.println("statusmessage"   +  statusmessage);

}



@Test(dataProvider = "bankdetails", description="verify the bank details of de", groups="sanity")
public void bankdetails(String de_id,String version,int status_code) throws Exception {

	Processor processor = deauto.delogin(de_id, version);
	Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
	String deotp = (String) delmeth.redisconnectget(DeliveryConstant.de_otp, de_id,
			DeliveryConstant.redisdb);
	
	Processor processor1 = deauto.deotp(de_id, deotp);
	Assert.assertEquals(processor1.ResponseValidator.GetResponseCode(), 200);
	
	String resp = processor1.ResponseValidator.GetBodyAsText();
	auth = JsonPath.read(resp, "$.data.Authorization").toString();
	Processor processor2=helper.bankdetails(auth);
	String resp1 = processor2.ResponseValidator.GetBodyAsText();

	Assert.assertEquals(processor2.ResponseValidator.GetResponseCode(), 200);
	
	int statusCode = JsonPath.read(resp1, "$.statusCode");
	Assert.assertEquals(status_code, statusCode);
	String statusmessage = JsonPath.read(resp1, "$.statusMessage");
	
	
	String query="Select bank_name from de_info  where de_id="+de_id +";" ;
	Object obj1=  delmeth.dbhelperget(query, "bank_name");
	String bankname =obj1.toString();
	
	String query1="Select bank_ac from de_info  where de_id="+de_id +";" ;
	Object obj2=  delmeth.dbhelperget(query1, "bank_ac");
	String bankaccountno= obj2.toString();
	
	String query2="Select bank_ac_name from de_info  where de_id="+de_id +";" ;
	Object obj3=  delmeth.dbhelperget(query2, "bank_ac_name");
	String bankAccountname= obj3.toString();
	
	String query3="Select bank_ifsc from  de_info where de_id="+de_id +";" ;
	Object obj4=  delmeth.dbhelperget(query3, "bank_ifsc");
	String ifscode= obj4.toString();
	
	String query4="Select pan_card_number from de_info where de_id="+de_id +";" ;
	Object obj5=  delmeth.dbhelperget(query4, "pan_card_number");
	String pancardno= obj5.toString();
	
	String statusmessage1 = JsonPath.read(resp1, "$.statusMessage");
    String banknameR=JsonPath.read(resp1,"$.data.bankName");
    String IfsccodeR=JsonPath.read(resp1,"$.data.ifscCode");
    String pancardnoR=JsonPath.read(resp1," $.data.panCardNo");
    String accountname=JsonPath.read(resp1,"$.data.accountName");
    String accountno=JsonPath.read(resp1,"$.data.accountNumber");

	if (status_code ==0) {
		
		Assert.assertEquals("Success", statusmessage1);
		Assert.assertEquals(bankname, banknameR);
		Assert.assertEquals(ifscode,IfsccodeR);
		Assert.assertEquals(pancardno,pancardnoR);
		Assert.assertEquals(bankAccountname, accountname);
		Assert.assertEquals(bankaccountno, accountno);
      
	}
	   
	System.out.println(" bank name= "+ bankname+"\n" +"ifsccode="+ifscode+"\n"+"pancard no is="+pancardno+"\n"+"bank account no is "+bankaccountno);

}

@Test(dataProvider = "startduty", description = "Make De start duty " ,groups="sanity")
public void startDuty(String de_id,String version,int status_code,String statusmessage) throws Exception {

   helper.makeDEActive(de_id);
	Processor processor = deauto.delogin(de_id, version);
	Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
	String deotp = (String) delmeth.redisconnectget(DeliveryConstant.de_otp, de_id,
			DeliveryConstant.redisdb);
	
	Processor processor1 = deauto.deotp(de_id, deotp);
	Assert.assertEquals(processor1.ResponseValidator.GetResponseCode(), 200);
	
	String resp = processor1.ResponseValidator.GetBodyAsText();
	auth = JsonPath.read(resp, "$.data.Authorization").toString();
	Processor processor2=helper.startDuty(auth);
	String resp1 = processor2.ResponseValidator.GetBodyAsText();

	int statusCode = JsonPath.read(resp1, "$.statusCode");
	String status_message = JsonPath.read(resp1, "$.statusMessage");

	Assert.assertEquals(processor2.ResponseValidator.GetResponseCode(), 200);
	Assert.assertEquals(statusCode,status_code);
	Assert.assertEquals(status_message,statusmessage);
	Assert.assertEquals(processor2.ResponseValidator.GetResponseCode(), 200);
	
}


@Test(dataProvider = "stopduty", description="Make De to stopduty", groups="sanity")
public void stopDuty(String de_id,String version,int status_code,String statusmessage) throws Exception {

   helper.makeDEActive(de_id);
	Processor processor = deauto.delogin(de_id, version);
	Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
	String deotp = (String) delmeth.redisconnectget(DeliveryConstant.de_otp, de_id,
			DeliveryConstant.redisdb);
	
	Processor processor1 = deauto.deotp(de_id, deotp);
	Assert.assertEquals(processor1.ResponseValidator.GetResponseCode(), 200);
	
	String resp = processor1.ResponseValidator.GetBodyAsText();
	auth = JsonPath.read(resp, "$.data.Authorization").toString();
	Processor processor2=helper.stopDuty(auth);
	String resp1 = processor2.ResponseValidator.GetBodyAsText();
	int statusCode = JsonPath.read(resp1, "$.statusCode");
	String status_message = JsonPath.read(resp1, "$.statusMessage");

	Assert.assertEquals(processor2.ResponseValidator.GetResponseCode(), 200);
	Assert.assertEquals(statusCode,status_code);
	Assert.assertEquals(status_message,statusmessage);

}


@Test(dataProvider = "systemreject",description="verifying System order reject",groups="sanity")
public void systemreject( String de_id, String order_id,int Status_code,String statusmessage ) throws InterruptedException
{
	
helper.makeDEActive(de_id);
Thread.sleep(1000);
//Assign to order to delivery boy 
 helper.orderAssign(order_id, de_id);
// reject the order
 Thread.sleep(1000);
Processor processor = helper.systemRejectOrder(de_id, order_id);
Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
Thread.sleep(10000);
//String query= "select de_id from de_rejected_orders where order_id=" + order_id+";";
//Object obj1=  delmeth.dbhelperget(query, "de_id");
//String rejectdeId =obj1.toString();
String query1= "select auto_reject from de_rejected_orders where order_id=" + order_id+";";
Object obj2=  delmeth.dbhelperget(query1, "auto_reject");
String autocheck =obj2.toString();
//String autocheck = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap(query1).get("auto_reject").toString();
//String rejectdeId = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap(query).get("de_id").toString();
Assert.assertEquals(autocheck, "true");
//Assert.assertEquals(rejectdeId, de_id);
String resp = processor.ResponseValidator.GetBodyAsText();
int statusCode = JsonPath.read(resp, "$.statusCode");
String status_message = JsonPath.read(resp, "$.statusMessage");
Assert.assertEquals(Status_code, statusCode);
Assert.assertEquals(statusmessage, status_message);


}


@Test(dataProvider = "deliveryboyfloatingcash",description="to get delivery boy floating cash", groups="sanity")
public void deliveryfloatingcash(String de_id,String version,int status_code, String statusmessage) throws Exception {

	Processor processor = deauto.delogin(de_id, version);
	Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
	String deotp = (String) delmeth.redisconnectget(DeliveryConstant.de_otp, de_id,
			DeliveryConstant.redisdb);
	
	Processor processor1 = deauto.deotp(de_id, deotp);
	Assert.assertEquals(processor1.ResponseValidator.GetResponseCode(), 200);
	
	String resp = processor1.ResponseValidator.GetBodyAsText();
	auth = JsonPath.read(resp, "$.data.Authorization").toString();
	Processor processor2=helper.getDeliveryboyFloatingcash(auth);
	String resp1 = processor2.ResponseValidator.GetBodyAsText();

	Assert.assertEquals(processor2.ResponseValidator.GetResponseCode(), 200);
	
	int statusCode = JsonPath.read(resp1, "$.statusCode");
	Assert.assertEquals(status_code, statusCode);
	String status_message = JsonPath.read(resp1, "$.statusMessage");
	Assert.assertEquals(status_message, statusmessage);
	

}




@Test(dataProvider = "verifydeisfree", description="verify where the de is free", groups="sanity")
public void verifyDEIsFree(String de_id,String version,int status_code, String statusmessage) throws Exception {

	Processor processor = deauto.delogin(de_id, version);
	Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
	String deotp = (String) delmeth.redisconnectget(DeliveryConstant.de_otp, de_id,
			DeliveryConstant.redisdb);
	Processor processor1 = deauto.deotp(de_id, deotp);
	Assert.assertEquals(processor1.ResponseValidator.GetResponseCode(), 200);
	String resp = processor1.ResponseValidator.GetBodyAsText();
	auth = JsonPath.read(resp, "$.data.Authorization").toString();
	Processor processor2=helper.CheckDeisFree(auth);
	String resp1 = processor2.ResponseValidator.GetBodyAsText();
	Assert.assertEquals(processor2.ResponseValidator.GetResponseCode(), 200);
	int statusCode = JsonPath.read(resp1, "$.statusCode");
	Assert.assertEquals(status_code, statusCode);
	String status_message = JsonPath.read(resp1, "$.statusMessage");
	Assert.assertEquals(status_message, statusmessage);
	
}


@Test(dataProvider = "getdelocation", description="get the de location",groups="sanity")
public void getdeliveryboylocation(String de_id,int status_code) throws Exception {
	Processor processor =helper.getdeliveryboylocation(de_id);
	Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
	String resp = processor.ResponseValidator.GetBodyAsText();
	int statusCode = JsonPath.read(resp, "$.statusCode");
	Assert.assertEquals(status_code, statusCode);
//	String latitude =JsonPath.read(resp, "$.data.latitude");
//	String longitude =JsonPath.read(resp, "$.data.longitude");
//
//	String query="Select last_seen_lat from delivery_boys where id="+de_id +";" ;
//	Object obj1=  delmeth.dbhelperget(query, "last_seen_lat");
//	String lat =obj1.toString();
//	
//	String query1="Select last_seen_lng from delivery_boys where id="+de_id +";" ;
//	Object obj2=  delmeth.dbhelperget(query1, "last_seen_lng");
//	String lng =obj2.toString();
//	
//	Assert.assertEquals(latitude, lat);
//	Assert.assertEquals(longitude, lng);
}



@Test(dataProvider = "updatedezone", description="update or add zone to de ",groups="sanity")
public void updatedezone(String de_id,String de_zone_map_id,String de_zone_id,int status_code, String StatusMessage, String data) throws Exception {
    Processor processor =helper.addzonetode(de_id, de_zone_map_id, de_zone_id);
	Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
	String resp = processor.ResponseValidator.GetBodyAsText();
	int statusCode = JsonPath.read(resp, "$.statusCode");
	Assert.assertEquals(status_code, statusCode);
	String status_message = JsonPath.read(resp, "$.statusMessage");
	Assert.assertEquals(status_message, StatusMessage);
	String zonemapid = JsonPath.read(resp, "$.data").toString();
	Assert.assertEquals(data, zonemapid);	
}


@Test(dataProvider = "deletzone", description="update or add zone to de ", groups="sanity")
public void deletzone(String de_id,String de_zone_map_id,String de_zone_id,int status_code, String StatusMessage, String data) throws Exception {
    Processor processor =helper.addzonetode(de_id, de_zone_map_id, de_zone_id);
	Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
	String resp = processor.ResponseValidator.GetBodyAsText();
	int statusCode = JsonPath.read(resp, "$.statusCode");
	Assert.assertEquals(status_code, statusCode);
	String status_message = JsonPath.read(resp, "$.statusMessage");
	Assert.assertEquals(status_message, StatusMessage);
	String zonemapid = JsonPath.read(resp, "$.data").toString();
	Assert.assertEquals(data, zonemapid);	
}


@Test(dataProvider = "deletzone", description="deletezone ", groups="sanity")
public void deletzone(String de_zone_map_id,int status_code, String StatusMessage) throws Exception {
    Processor processor =helper.deletezonemap(de_zone_map_id);
	Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
	String resp = processor.ResponseValidator.GetBodyAsText();
	int statusCode = JsonPath.read(resp, "$.statusCode");
	Assert.assertEquals(status_code, statusCode);
	String status_message = JsonPath.read(resp, "$.statusMessage");
	Assert.assertEquals(status_message, StatusMessage);
		
}


@Test(dataProvider = "orderhistory", description="To check the orderhistory of de ",groups="sanity")
public void getdeorderhistory(String de_id,String version,int status_code, String statusmessage,String date, String order_id) throws Exception {

	Processor processor = deauto.delogin(de_id, version);
	Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
	String deotp = (String) delmeth.redisconnectget(DeliveryConstant.de_otp, de_id,
			DeliveryConstant.redisdb);
	Processor processor1 = deauto.deotp(de_id, deotp);
	Assert.assertEquals(processor1.ResponseValidator.GetResponseCode(), 200);
	String resp = processor1.ResponseValidator.GetBodyAsText();
	auth = JsonPath.read(resp, "$.data.Authorization").toString();
	Processor processor2=helper.orderAssign(order_id, de_id);
	Processor processor3=helper.getdeorderhistory(auth, date);
	String resp1 = processor3.ResponseValidator.GetBodyAsText();
	Assert.assertEquals(processor3.ResponseValidator.GetResponseCode(), 200);
	int statusCode = JsonPath.read(resp1, "$.statusCode");
	Assert.assertEquals(status_code, statusCode);
	String status_message = JsonPath.read(resp1, "$.statusMessage");
	Assert.assertEquals(status_message, statusmessage);
	
}



@Test(dataProvider = "loginhistory", description="To check the loginhistory of de",groups="sanity")
public void loginhistory(String de_id,String version,int status_code, String statusmessage,String date1, String date2) throws Exception {

	Processor processor = deauto.delogin(de_id, version);
	Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
	String deotp = (String) delmeth.redisconnectget(DeliveryConstant.de_otp, de_id,
			DeliveryConstant.redisdb);
	Processor processor1 = deauto.deotp(de_id, deotp);
	Assert.assertEquals(processor1.ResponseValidator.GetResponseCode(), 200);
	String resp = processor1.ResponseValidator.GetBodyAsText();
	auth = JsonPath.read(resp, "$.data.Authorization").toString();
	Processor processor2=helper.deloginhistory(auth, date1, date2);
	String resp1 = processor2.ResponseValidator.GetBodyAsText();
	//Assert.assertEquals(processor2.ResponseValidator.GetResponseCode(), 200);
	int statusCode = JsonPath.read(resp1, "$.statusCode");
	Assert.assertEquals(status_code, statusCode);
	String status_message = JsonPath.read(resp1, "$.statusMessage");
	Assert.assertEquals(status_message, statusmessage);
	
}

@Test(dataProvider = "enablebatchforresturant", description="enable batching for redturant",groups="sanity")
public void enablebatchforresturant(String rest1, String rest2, String update_value,int status_code, String StatusMessage) throws Exception {
    Processor processor =helper.Enablebatchresturant(rest1, rest2, update_value);
	Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
	String resp = processor.ResponseValidator.GetBodyAsText();
	int statusCode = JsonPath.read(resp, "$.statusCode");
	Assert.assertEquals(status_code, statusCode);
	String status_message = JsonPath.read(resp, "$.statusMessage");
	Assert.assertEquals(status_message, StatusMessage);
	  String query = "Select batching_enabled from restaurant where name like 'Meghana Foods' " + ";";
      Object obj = delmeth.dbhelperget(query, "batching_enabled");
       boolean check_value1= (boolean)obj;
       
       String query1 = "Select batching_enabled from restaurant where name like 'Chai Point' " + ";";
       Object obj1 = delmeth.dbhelperget(query1, "batching_enabled");
       boolean check_value2=  (boolean)obj1;
        Assert.assertEquals(check_value1, (!update_value.equalsIgnoreCase("0")));
        Assert.assertEquals(check_value2, !(update_value).equalsIgnoreCase("0"));
		
}


@Test(dataProvider = "pointdetailpage", description="Verifying point detail of DE ",groups="loyality")
public void pointdetailpage(String de_id,String version,int status_code, String statusmessage) throws Exception {

	Processor processor = deauto.delogin(de_id, version);
	Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
	String deotp = (String) delmeth.redisconnectget(DeliveryConstant.de_otp, de_id,
			DeliveryConstant.redisdb);
	Processor processor1 = deauto.deotp(de_id, deotp);
	Assert.assertEquals(processor1.ResponseValidator.GetResponseCode(), 200);
	String resp = processor1.ResponseValidator.GetBodyAsText();
	auth = JsonPath.read(resp, "$.data.Authorization").toString();
	Processor processor2=helper.getPointdetailpage(auth);
	String resp1 = processor2.ResponseValidator.GetBodyAsText();
	Assert.assertEquals(processor2.ResponseValidator.GetResponseCode(), 200);
	int statusCode = JsonPath.read(resp1, "$.statusCode");
	Assert.assertEquals(status_code, statusCode);
	String status_message = JsonPath.read(resp1, "$.statusMessage");
	Assert.assertEquals(status_message, statusmessage);
	
}


@Test(dataProvider = "redeempoints", description="Verify Redeem Points of DE" ,groups="loyality")
public void redeempoints(String de_id,String version,int status_code, String statusmessage) throws Exception {

	Processor processor = deauto.delogin(de_id, version);
	Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
	String deotp = (String) delmeth.redisconnectget(DeliveryConstant.de_otp, de_id,
			DeliveryConstant.redisdb);
	Processor processor1 = deauto.deotp(de_id, deotp);
	Assert.assertEquals(processor1.ResponseValidator.GetResponseCode(), 200);
	String resp = processor1.ResponseValidator.GetBodyAsText();
	auth = JsonPath.read(resp, "$.data.Authorization").toString();
	Processor processor2=helper.getRedeempoints(auth);
	String resp1 = processor2.ResponseValidator.GetBodyAsText();
	Assert.assertEquals(processor2.ResponseValidator.GetResponseCode(), 200);
	int statusCode = JsonPath.read(resp1, "$.statusCode");
	Assert.assertEquals(status_code, statusCode);
	String status_message = JsonPath.read(resp1, "$.statusMessage");
	Assert.assertEquals(status_message, statusmessage);
	
}

@Test(dataProvider = "getTotalpoints", description="Check De total Points of DE",groups="loyality")
public void getTotalpoints(String de_id,String version,int status_code, String statusmessage) throws Exception {

	Processor processor = deauto.delogin(de_id, version);
	Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
	String deotp = (String) delmeth.redisconnectget(DeliveryConstant.de_otp, de_id,
			DeliveryConstant.redisdb);
	Processor processor1 = deauto.deotp(de_id, deotp);
	Assert.assertEquals(processor1.ResponseValidator.GetResponseCode(), 200);
	String resp = processor1.ResponseValidator.GetBodyAsText();
	auth = JsonPath.read(resp, "$.data.Authorization").toString();
	Processor processor2=helper.getTotalpoints(auth);
	String resp1 = processor2.ResponseValidator.GetBodyAsText();
	Assert.assertEquals(processor2.ResponseValidator.GetResponseCode(), 200);
	int statusCode = JsonPath.read(resp1, "$.statusCode");
	Assert.assertEquals(status_code, statusCode);
	String status_message = JsonPath.read(resp1, "$.statusMessage");
	Assert.assertEquals(status_message, statusmessage);
	String sucess = JsonPath.read(resp1, "$.data.success").toString();

	int rewarded = JsonPath.read(resp1, "$.data.rewarded");
	int redeemed = JsonPath.read(resp1, "$.data.redeemed");
	int balance = JsonPath.read(resp1, "$.data.balance");
	System.out.println("sucess message = "+ sucess+ "\n"+" DE rewarded points= " + rewarded +"\n"+ "De Redmeed points="+ redeemed+"\n" +"DE blance  point is ="+ balance);
}


@Test(dataProvider = "getvoucher", description="To check the voucher details ",groups="loyality")
public void getvoucher(String de_id,String version) throws Exception {

	Processor processor = deauto.delogin(de_id, version);
	Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
	String deotp = (String) delmeth.redisconnectget(DeliveryConstant.de_otp, de_id,
			DeliveryConstant.redisdb);
	Processor processor1 = deauto.deotp(de_id, deotp);
	Assert.assertEquals(processor1.ResponseValidator.GetResponseCode(), 200);
	String resp = processor1.ResponseValidator.GetBodyAsText();
	auth = JsonPath.read(resp, "$.data.Authorization").toString();
	Processor processor2=helper.getVouchers(auth);
	String resp1 = processor2.ResponseValidator.GetBodyAsText();
	Assert.assertEquals(processor2.ResponseValidator.GetResponseCode(), 200);
	int statusCode = JsonPath.read(resp1, "$.statusCode");
	String status_message = JsonPath.read(resp1, "$.statusMessage");
	if (statusCode==1)
	{
		Assert.assertEquals(status_message, "Could not fetch vouchers");
		
		System.out.println("No voucher is not created for this DE");
	}
	
	else if(statusCode==0)
	{
		Assert.assertEquals(status_message, "success");
		
		System.out.println("No voucher is not created for this DE");

	}
	
	
}



@Test(dataProvider = "getRewardConfig", description="Verify Reward config of the DE",groups="loyality")
public void getRewardConfig(String de_id,String version,int status_code, String statusmessage) throws Exception {

	Processor processor = deauto.delogin(de_id, version);
	Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
	String deotp = (String) delmeth.redisconnectget(DeliveryConstant.de_otp, de_id,
			DeliveryConstant.redisdb);
	Processor processor1 = deauto.deotp(de_id, deotp);
	Assert.assertEquals(processor1.ResponseValidator.GetResponseCode(), 200);
	String resp = processor1.ResponseValidator.GetBodyAsText();
	auth = JsonPath.read(resp, "$.data.Authorization").toString();
	Processor processor2=helper.getGif(auth);
	String resp1 = processor2.ResponseValidator.GetBodyAsText();
	Assert.assertEquals(processor2.ResponseValidator.GetResponseCode(), 200);
	int statusCode = JsonPath.read(resp1, "$.statusCode");
	Assert.assertEquals(status_code, statusCode);
	String status_message = JsonPath.read(resp1, "$.statusMessage");
	Assert.assertEquals(status_message, statusmessage);
	
}






//@Test(dataProvider = "servicestatusupdate" )
//public void caprdstatusupdate(String order_id, String Status, String bill,String pay,String collect,String de_id,String version,int status_code) throws Exception {
//	helper.assignOrder(order_id);
//  //helper.makeDEActive(de_id);
//	Processor processor = deauto.delogin(de_id, version);
//	Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
//	String deotp = (String) delmeth.redisconnectget(DeliveryConstant.de_otp, de_id,
//			DeliveryConstant.redisdb);
//	Processor processor1 = deauto.deotp(de_id, deotp);
//	Assert.assertEquals(processor1.ResponseValidator.GetResponseCode(), 200);
//	String resp = processor1.ResponseValidator.GetBodyAsText();
//	auth = JsonPath.read(resp, "$.data.Authorization").toString();
//	//helper.makeDEFree(de_id);
//	//helper.makeDEActive(de_id);
//	Thread.sleep(2000);
//	
//	
//	Processor processor2=helper.deliveryboystatusupdate(order_id, Status, bill, pay, collect,auth);
//	String resp1 = processor2.ResponseValidator.GetBodyAsText();
//	int statusCode = JsonPath.read(resp1, "$.statusCode");
//	Assert.assertEquals(status_code, statusCode);
//	if (statusCode == 200) {
//		String statusmessage = JsonPath.read(resp, "$.statusMessage");
//		int requestId1 = JsonPath.read(resp, "$.requestId");
//		Assert.assertEquals("Success", statusmessage);
//		Assert.assertNotNull(requestId1);
//	}
//
//}






//@Test(dataProvider = "makeActiveFree")
//public void geohash(String de_id) 
//{
//helper.makeDEActive(de_id);
//helper.makeDEFree(de_id);
//
//}
//
@Test
public void test() throws IOException, InterruptedException {
		for (int i=0;i<=3;i++) {
	LOSHelper loshelper = new LOSHelper();
	String order_id = loshelper.getAnOrder("pop");
	System.out.println("order is "+order_id);
}
	
}
private static final String FILENAME = "D:\\test\\deidauth.csv";
@Test
public void deidauth() throws IOException
{
    FileWriter fw = new FileWriter(FILENAME); 
   BufferedWriter bw= new BufferedWriter(fw);
	
    fw = new FileWriter(FILENAME);
      bw = new BufferedWriter(fw);
       for (int i=0;i<3;i++)
        {
    String de_id=deliverydata.CreateDE(4);
    deliverydata.delocationupdate("12.934847", "77.61612300000002", de_id, "1.9");
    bw.write(de_id);
    bw.write(",");
    String auth=deliverydata.getDEAuth(de_id, "1.9");
    bw.write(auth+"\n");
        }
    bw.close();
//        
//        
}







/*
@Test(dataProvider = "createorders")
public void createorders(String rest_id, String lat_long,String area_code) throws JSONException, InterruptedException 
{
	DeliveryServiceDataProvider help= new DeliveryServiceDataProvider();
  Object  orderid1= help.createorderjson(rest_id, lat_long, area_code);
	String order_id=orderid1.toString();

}


@Test
public void placeOrders2()
{
	ResolutionRequestData res=new ResolutionRequestData();
	DeliveryDataHelper helper = new DeliveryDataHelper();
    Map<String,String> testValues= new HashMap<>();
    List  <String> finalList= new ArrayList();
    List <Integer> zones=new ArrayList<Integer>(){{
       // add(4);
        //add(1);
        //add(10);
        //add(2);
        add(3);
    }};
    for(Integer i:zones) {
        List<Map<String,Object>> restObject= SystemConfigProvider.getTemplate("deliverydb").queryForList("select id,lat_long from restaurant where enabled=1 and area_code ="+i+" order by 1 limit 10");
        for(Map<String,Object> restMap:restObject)
        {
            testValues.put("restaurant_id",restMap.get("id").toString());
            testValues.put("restaurant_lat_lng",restMap.get("lat_long").toString());
            String lat=restMap.get("lat_long").toString().split(",")[0];
            String lon=restMap.get("lat_long").toString().split(",")[1];
            testValues.put("restaurant_lat_lng",helper.getApproxLatLongAtADistance(Double.parseDouble(lat),Double.parseDouble(lon),Math.random()));
            //testValues.put("restaurant_customer_distance","1.5");
            //testValues.put("is_long_distance","false");
            testValues.put("order_time",helper.getcurrentDateTimefororderTime());
            testValues.put("order_type","regular");
            testValues.put("restaurant_area_code",i.toString());
            String restId = testValues.get("restaurant_id");
           String order_id=helper.getOrderId(restId, lat, lon);
            String finalString=i+"___"+restMap.get("id").toString()+"___"+order_id;
            System.out.println(finalString);
            finalList.add(finalString);
        }

        for(String str: finalList)
        {
            System.out.println(str);

        }

    }}


*/

//@Test(dataProvider = "mergeandassign", description="Verify whether the order is mergrd or not", groups="Sanity")
//public void mergeandassign(String order_id1, String order_id2,
//		String order_id3,String de_id) throws Exception {
//
//     helper.mergeorder(order_id1, order_id2);
//     helper.orderAssign(order_id1, de_id);
//     helper.nextorder(order_id3, de_id);
//	
	//String resp = processor.ResponseValidator.GetBodyAsText();
//	int statusCode = JsonPath.read(resp, "$.statusCode");
//	Assert.assertEquals(status_code, statusCode);
//	if (statusCode == 200) {
//		String statusmessage = JsonPath.read(resp, "$.statusMessage");
//		int requestId1 = JsonPath.read(resp, "$.requestId");
//		Assert.assertEquals("Success", statusmessage);
//		Assert.assertNotNull(requestId1);
//	}

//}
@Test(dataProvider = "cancelorder", description = "verify that order gets canceled " ,groups="sanity")
public void cancelorder(String de_id, String version,String order_id,int status_code,String statusmessage) throws Exception {
   helper.makeDEActive(de_id);
   helper.makeDEFree(de_id);
   helper.orderAssign(order_id, de_id);
   Processor processor=helper.ordercancel(order_id);
	String resp1 = processor.ResponseValidator.GetBodyAsText();
	int statusCode = JsonPath.read(resp1, "$.statusCode");
	String status_message = JsonPath.read(resp1, "$.statusMessage");
	Assert.assertEquals(statusCode,status_code);
	Assert.assertEquals(status_message,statusmessage);
	
}



@Test(dataProvider = "incoming", description = "Updating incoming for an particular order " ,groups="sanity")
public void incoming( String de_id, String order_id,String collect,int status_code,String statusmessage) throws Exception {
  
	helper.Assign(order_id);
   String query = "Select de_id from trips where order_id=" + order_id + ";";
	
		Object obj1 =delmeth.dbhelperget(query, "de_id");
	
        String de_id1 = obj1.toString();
         if(de_id==null)
         {
        	 System.out.println("order is not assigned assigned ");
         }
         else
         {
        	 de_id.equalsIgnoreCase(de_id1);
        	 System.out.println("order is assigned ******************** ");

         }
	Processor processor=helper.incoming(order_id, collect);
	String resp1 = processor.ResponseValidator.GetBodyAsText();
	int statusCode = JsonPath.read(resp1, "$.statusCode");
	String status_message = JsonPath.read(resp1, "$.statusMessage");
	Assert.assertEquals(statusCode,status_code);
	Assert.assertEquals(status_message,statusmessage);
	
}


}
