package com.swiggy.api.erp.cms.pojo.BaseServiceBulkVariantGroup;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.erp.cms.pojo.BaseServiceBulkVariantGroup
 **/

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "delete_by_ids",
        "delete_by_third_party_ids",
        "update_items",
        "variant_groups"
})
public class BulkVariantGroups {

    @JsonProperty("delete_by_ids")
    private List<Integer> delete_by_ids = null;
    @JsonProperty("delete_by_third_party_ids")
    private List<String> delete_by_third_party_ids = null;
    @JsonProperty("update_items")
    private Boolean update_items;
    @JsonProperty("variant_groups")
    private List<Variant_group> variant_groups = null;

    @JsonProperty("delete_by_ids")
    public List<Integer> getDelete_by_ids() {
        return delete_by_ids;
    }

    @JsonProperty("delete_by_ids")
    public void setDelete_by_ids(List<Integer> delete_by_ids) {
        this.delete_by_ids = delete_by_ids;
    }

    @JsonProperty("delete_by_third_party_ids")
    public List<String> getDelete_by_third_party_ids() {
        return delete_by_third_party_ids;
    }

    @JsonProperty("delete_by_third_party_ids")
    public void setDelete_by_third_party_ids(List<String> delete_by_third_party_ids) {
        this.delete_by_third_party_ids = delete_by_third_party_ids;
    }

    @JsonProperty("update_items")
    public Boolean getUpdate_items() {
        return update_items;
    }

    @JsonProperty("update_items")
    public void setUpdate_items(Boolean update_items) {
        this.update_items = update_items;
    }

    @JsonProperty("variant_groups")
    public List<Variant_group> getVariant_groups() {
        return variant_groups;
    }

    @JsonProperty("variant_groups")
    public void setVariant_groups(List<Variant_group> variant_groups) {
        this.variant_groups = variant_groups;
    }

    public void setDefaultValues() {
        List<Variant_group> list = new ArrayList<>();
        if(this.getUpdate_items() == null)
            this.setUpdate_items(true);
        if(this.getVariant_groups() == null)
            this.setVariant_groups(list);
        if(this.getDelete_by_ids() == null)
            this.setDelete_by_ids(new ArrayList<>());
        if(this.getDelete_by_third_party_ids() == null)
            this.setDelete_by_third_party_ids(new ArrayList<>());
    }

    public BulkVariantGroups build() {
        setDefaultValues();
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("delete_by_ids", delete_by_ids).append("delete_by_third_party_ids", delete_by_third_party_ids).append("update_items", update_items).append("variant_groups", variant_groups).toString();
    }

}
