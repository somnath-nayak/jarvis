package com.swiggy.api.erp.ff.dp.omtRewrite;

import com.swiggy.api.erp.cms.helper.CMSHelper;
import com.swiggy.api.erp.ff.helper.LOSHelper;
import com.swiggy.api.erp.ff.helper.OMSHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import org.testng.annotations.DataProvider;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.RabbitMQHelper;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import com.swiggy.api.sf.snd.helper.SnDHelper;
import java.io.IOException;
import java.util.HashMap;
import com.swiggy.api.erp.ff.constants.OmtConstants;

public class OmtMiddlewareData {
	
	String sessionId = null;
	String token = null;
	String tid = null;
	Long serviceableAddress = null;

	LOSHelper losHelper = new LOSHelper();
	Initialize gameofthrones = new Initialize();
	RabbitMQHelper rmqHelper = new RabbitMQHelper();
	OMSHelper omsHelper = new OMSHelper();
	CheckoutHelper checkoutHelper = new CheckoutHelper();
	
	SnDHelper sndHelper = new SnDHelper();
	CMSHelper cmsHelper = new CMSHelper();
	SANDHelper sandHelper = new SANDHelper();

	@DataProvider(name = "getPaymentDetailsData")
	public Object[][] getPaymentDetailsData() throws IOException, InterruptedException {	
		
		String manualOrder = losHelper.getAnOrder("manual");
		String partnerOrder = losHelper.getAnOrder("partner");
		String thirdPartyOrder = losHelper.getAnOrder("thirdparty");
		String longDistanceOrder = losHelper.getAnOrder("longdistance");
		String newUserOrder = losHelper.getAnOrder("newuser");
		String popOrder = losHelper.getAnOrder("pop");
		String swiggyAssuredOrder = losHelper.getAnOrder("swiggyassured");

		HashMap<String, Integer> responseCodeMap = new HashMap<String, Integer>();

		responseCodeMap.put("validOrderIdAndValidSessionId", 200);
		responseCodeMap.put("validOrderIdAndInvalidSessionId", 401);
		responseCodeMap.put("invalidOrderIdAndValidSessionId", 404);
		responseCodeMap.put("invalidOrderIdAndInvalidSessionId", 401);

		return new Object[][] {
				{ manualOrder, "Valid Manual Order and valid sessionId",responseCodeMap.get("validOrderIdAndValidSessionId"), null, null },
				{ partnerOrder, "Valid Partner Order and valid sessionId",responseCodeMap.get("validOrderIdAndValidSessionId"), null, null },
				{ thirdPartyOrder, "Valid Third Party Order and valid sessionId",responseCodeMap.get("validOrderIdAndValidSessionId"), null, null },
				{ longDistanceOrder, "Valid Long Distance Order and valid sessionId",responseCodeMap.get("validOrderIdAndValidSessionId"), null, null },
				{ newUserOrder, "Valid New User Order and valid sessionID",responseCodeMap.get("validOrderIdAndValidSessionId"), null, null },
				{ popOrder, "Valid Swiggy pop Order and valid sessionId",responseCodeMap.get("validOrderIdAndValidSessionId"), null, null },
				{ swiggyAssuredOrder, "Valid Swiggy Assured Order and valid sessionId",responseCodeMap.get("validOrderIdAndValidSessionId"), null, null },

				{ losHelper.getAnOrder("invalid"), "Invalid Order",responseCodeMap.get("invalidOrderIdAndValidSessionId"), null, null },

				{ losHelper.createOrderWithDifferentPaymentMethods("Cash", "com.swiggy-cash"), "Payment method Cash",responseCodeMap.get("validOrderIdAndValidSessionId"), "Cash", "com.swiggy-cash" },
				{ losHelper.createOrderWithDifferentPaymentMethods("Netbanking", "com.swiggy-Netbanking"),"Payment method Netbanking", responseCodeMap.get("validOrderIdAndValidSessionId"), "Netbanking","com.swiggy-Netbanking" },
				{ losHelper.createOrderWithDifferentPaymentMethods("Paytm", "com.swiggy-Paytm"), "Payment method Paytm",responseCodeMap.get("validOrderIdAndValidSessionId"), "Paytm", "com.swiggy-Paytm" }

		};
	}

	// getPaymentDetailsAsPostPutPatchDeleteData

	@DataProvider(name = "getPaymentDetailsAsPostPutPatchDeleteData")
	public Object[][] getPaymentDetailsAsPostPutPatchDeleteData() throws IOException, InterruptedException {
		String manualOrder = losHelper.getAnOrder("manual");
		return new Object[][] { { manualOrder, 405 }
		};
	}

	@DataProvider(name = "getDeliveryDetailsData")
	public Object[][] getDeliveryDetailsData() throws IOException, InterruptedException {
		String manualOrder = losHelper.getAnOrder("manual");
		Thread.sleep(1000);
		String invalidOrder = losHelper.getAnOrder("invalid");
		HashMap<String, Integer> responseCodeMap = new HashMap<String, Integer>();
		responseCodeMap.put("successCase", 200);
		responseCodeMap.put("unauthorizedCase", 401);
		return new Object[][] {
				{ manualOrder, "Valid OrderId", responseCodeMap, "positive" },
				{ invalidOrder, "InvalidId", responseCodeMap, "negative" }
		};
	}

	@DataProvider(name = "getCustomerDetailsValidOrders")
	public Object[][] getCustomerDetailsValidOrder() throws IOException, InterruptedException {
		String manualOrder = losHelper.getAnOrder("manual");
		Thread.sleep(1000);
		String partnerOrder = losHelper.getAnOrder("partner");
		Thread.sleep(1000);
		String thirdPartyOrder = losHelper.getAnOrder("thirdparty");
		Thread.sleep(1000);
		String longDistanceOrder = losHelper.getAnOrder("longdistance");
		Thread.sleep(1000);
		String newUserOrder = losHelper.getAnOrder("newuser");
		Thread.sleep(1000);
		String popOrder = losHelper.getAnOrder("pop");
		Thread.sleep(1000);
		String swiggyAssuredOrder = losHelper.getAnOrder("swiggyassured");
		Thread.sleep(1000);

		HashMap<String, Integer> responseCodeMap = new HashMap<String, Integer>();
		responseCodeMap.put("validOrder", 200);

		return new Object[][] {
				{ manualOrder, "Valid Manual Order", responseCodeMap.get("validOrder") },
				{ partnerOrder, "Valid Partner Order ",responseCodeMap.get("validOrder") },
				{ thirdPartyOrder,"Valid Third Party ",responseCodeMap.get("validOrder") },
				{ longDistanceOrder, "Valid Long Distance Order",responseCodeMap.get("validOrder") },
				{ newUserOrder,  "Valid New User Order",responseCodeMap.get("validOrder") },
				{ popOrder, "Valid Swiggy pop Order",responseCodeMap.get("validOrder") },
				{ swiggyAssuredOrder, "Valid Swiggy Assured Order ",responseCodeMap.get("validOrder") } };
	}

	
	@DataProvider(name = "getCustomerDetailsInvalidOrders")
	public Object[][] getCustomerDetailsInvalidOrders() throws IOException, InterruptedException {
		String invalidOrder = losHelper.getAnOrder("invalid");
		HashMap<String, Integer> responseCodeMap = new HashMap<String, Integer>();
		responseCodeMap.put("invalidOrder", 404);
		return new Object[][] {
				{ invalidOrder,  "Invalid Order", responseCodeMap.get("invalidOrder") },
		};
	}

	// Data provider for post put & delete test cases
	@DataProvider(name = "getCustomerDetailsDataPostPutDelete")
	public Object[][] getCustomerDetailsDataPostPutDelete() throws IOException, InterruptedException {
		String manualOrder = losHelper.getAnOrder("manual");
		HashMap<String, Integer> responseCodeMap = new HashMap<String, Integer>();
		responseCodeMap.put("customerDetailsAsPOST", 405);
		return new Object[][] {
				{ manualOrder, "Get customer details for POST PUT & Delete respectively",responseCodeMap.get("customerDetailsAsPOST") },
		};
	}
	
	/*
	 * Data providers for order details middle ware API
	 * 
	 */
		
	@DataProvider(name = "getOrderDetailsSuperOrder")
	public Object[][] getOrderDetailsSuperOrder() throws Exception {

		Processor order_object = checkoutHelper.placeOrder(OmtConstants.mobile, OmtConstants.password, OmtConstants.itemId, OmtConstants.quantity, OmtConstants.restId);
        String superOrder = String.valueOf(order_object.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.order_id"));

		return new Object[][] {			
				{superOrder, "Swiggy Super Order", 200 }
		};
	}
	
	@DataProvider(name = "getOrderDetailsInvalidOrders")
	public Object[][] getOrderDetailsInvalidOrderss() throws IOException, InterruptedException {
		String invalidOrder = losHelper.getAnOrder("invalid");
		HashMap<String, String> responseCodeMap = new HashMap<String, String>();
		responseCodeMap.put("statusCode", "500");
		responseCodeMap.put("statusMessage", "Invalid Order Id");
		return new Object[][] {
				{ invalidOrder,  "Invalid Order", responseCodeMap },
		};
	}
	
	@DataProvider(name = "getOrderDetailsForPostPutDelete")
	public Object[][] getOrderDetailsForPostPutDelete() throws IOException, InterruptedException {
		String manualOrder = losHelper.getAnOrder("manual");

		return new Object[][] {
				{ manualOrder, "Get order details for POST, PUT & DELETE", 405 },
		};
	}	
	
	@DataProvider(name = "getOrderDetailsNonSuperOrder")
	public Object[][] getOrderDetailsNonSuperOrder() throws Exception {
        Processor order_object = checkoutHelper.placeOrder(OmtConstants.nonSuperMobile, OmtConstants.nonSuperPassword, OmtConstants.itemId, OmtConstants.quantity, OmtConstants.restId);
        String nonSuperOrder = String.valueOf(order_object.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.order_id"));
		Thread.sleep(1000);
		HashMap<String, Integer> responseCodeMap = new HashMap<String, Integer>();
		responseCodeMap.put("NonSuperOrder", 500);
		return new Object[][] {
				{ nonSuperOrder, "Valid Manual Order", responseCodeMap.get("NonSuperOrder") },
		};
	}
}
