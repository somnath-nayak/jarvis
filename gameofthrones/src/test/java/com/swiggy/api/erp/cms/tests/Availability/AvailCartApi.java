package com.swiggy.api.erp.cms.tests.Availability;

import com.swiggy.api.erp.cms.dp.AvailabiltyDP;
import com.swiggy.api.erp.cms.helper.AvailabilityHelper;
import org.testng.annotations.Test;

public class AvailCartApi extends AvailabiltyDP {
    AvailabilityHelper availabilityHelper=new AvailabilityHelper();
    @Test(dataProvider = "availcartapi")
    public void availCart(String menu, String regularItem){
        availabilityHelper.availCartApi(menu,regularItem);
    }
}
