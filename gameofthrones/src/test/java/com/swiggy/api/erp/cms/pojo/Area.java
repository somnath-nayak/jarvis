package com.swiggy.api.erp.cms.pojo;

import com.swiggy.api.erp.cms.constants.AreaConstants;

/**
 * Created by kiran.j on 2/1/18.
 */
public class Area {

    private String name;
    private String slug;
    private int city_id;
    private int enabled;
    private int open_time;
    private int close_time;
    private int is_open;
    private String banner_message;
    private int beef_up_minutes;
    private int back_in_timer;
    private int site_delivery_buffer_count;
    private int delivery_time_offset;
    private int last_mile_cap;
    private float de_area_closed_multiplier;
    private float de_area_open_multiplier;
    private int ops_override;
    private int postal_code;
    private String mail_meta_data;
    private String closed_reason;
    private int surge_fee;
    private int dc_enabled;
    private float osm_multiplier;
    private String cancellation_fee_expression;
    private String city;
    int id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public int getCity_id() {
        return city_id;
    }

    public void setCity_id(int city_id) {
        this.city_id = city_id;
    }

    public int getEnabled() {
        return enabled;
    }

    public void setEnabled(int enabled) {
        this.enabled = enabled;
    }

    public int getOpen_time() {
        return open_time;
    }

    public void setOpen_time(int open_time) {
        this.open_time = open_time;
    }

    public int getClose_time() {
        return close_time;
    }

    public void setClose_time(int close_time) {
        this.close_time = close_time;
    }

    public int getIs_open() {
        return is_open;
    }

    public void setIs_open(int is_open) {
        this.is_open = is_open;
    }

    public String getBanner_message() {
        return banner_message;
    }

    public void setBanner_message(String banner_message) {
        this.banner_message = banner_message;
    }

    public int getBeef_up_minutes() {
        return beef_up_minutes;
    }

    public void setBeef_up_minutes(int beef_up_minutes) {
        this.beef_up_minutes = beef_up_minutes;
    }

    public int getBack_in_timer() {
        return back_in_timer;
    }

    public void setBack_in_timer(int back_in_timer) {
        this.back_in_timer = back_in_timer;
    }

    public int getSite_delivery_buffer_count() {
        return site_delivery_buffer_count;
    }

    public void setSite_delivery_buffer_count(int site_delivery_buffer_count) {
        this.site_delivery_buffer_count = site_delivery_buffer_count;
    }

    public int getDelivery_time_offset() {
        return delivery_time_offset;
    }

    public void setDelivery_time_offset(int delivery_time_offset) {
        this.delivery_time_offset = delivery_time_offset;
    }

    public int getLast_mile_cap() {
        return last_mile_cap;
    }

    public void setLast_mile_cap(int last_mile_cap) {
        this.last_mile_cap = last_mile_cap;
    }

    public float getDe_area_closed_multiplier() {
        return de_area_closed_multiplier;
    }

    public void setDe_area_closed_multiplier(float de_area_closed_multiplier) {
        this.de_area_closed_multiplier = de_area_closed_multiplier;
    }

    public float getDe_area_open_multiplier() {
        return de_area_open_multiplier;
    }

    public void setDe_area_open_multiplier(float de_area_open_multiplier) {
        this.de_area_open_multiplier = de_area_open_multiplier;
    }

    public int getOps_override() {
        return ops_override;
    }

    public void setOps_override(int ops_override) {
        this.ops_override = ops_override;
    }

    public int getPostal_code() {
        return postal_code;
    }

    public void setPostal_code(int postal_code) {
        this.postal_code = postal_code;
    }

    public String getMail_meta_data() {
        return mail_meta_data;
    }

    public void setMail_meta_data(String mail_meta_data) {
        this.mail_meta_data = mail_meta_data;
    }

    public String getClosed_reason() {
        return closed_reason;
    }

    public void setClosed_reason(String closed_reason) {
        this.closed_reason = closed_reason;
    }

    public int getSurge_fee() {
        return surge_fee;
    }

    public void setSurge_fee(int surge_fee) {
        this.surge_fee = surge_fee;
    }

    public int getDc_enabled() {
        return dc_enabled;
    }

    public void setDc_enabled(int dc_enabled) {
        this.dc_enabled = dc_enabled;
    }

    public float getOsm_multiplier() {
        return osm_multiplier;
    }

    public void setOsm_multiplier(float osm_multiplier) {
        this.osm_multiplier = osm_multiplier;
    }

    public String getCancellation_fee_expression() {
        return cancellation_fee_expression;
    }

    public void setCancellation_fee_expression(String cancellation_fee_expression) {
        this.cancellation_fee_expression = cancellation_fee_expression;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Area build() {
        if(this.getName() == null)
            this.setName(AreaConstants.name);
        if(this.getCity_id() == 0)
            this.setCity_id(AreaConstants.city_id);
        if(this.getCity() == null)
            this.setCity(Integer.toString(AreaConstants.city_id));
        if(this.getEnabled() == 0)
            this.setEnabled(AreaConstants.enabled);
        if(this.getClose_time() == 0)
            this.setClose_time(AreaConstants.close_time);
        if(this.getIs_open() == 0)
            this.setIs_open(AreaConstants.is_open);
        if(this.getLast_mile_cap() == 0)
            this.setLast_mile_cap(AreaConstants.last_mile_cap);
        if(this.getDe_area_open_multiplier() == 0)
            this.setDe_area_open_multiplier(AreaConstants.de_open_multiplier);
        if(this.getDe_area_closed_multiplier() == 0)
            this.setDe_area_closed_multiplier(AreaConstants.de_closed_multiplier);
        if(this.getSlug() == null)
            this.setSlug(AreaConstants.slug);
        if(this.getBanner_message() == null)
            this.setBanner_message("Testing banner");

        return this;
    }
}
