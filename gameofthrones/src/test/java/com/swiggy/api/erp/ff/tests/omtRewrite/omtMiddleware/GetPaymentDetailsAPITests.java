package com.swiggy.api.erp.ff.tests.omtRewrite.omtMiddleware;

import com.swiggy.api.erp.ff.dp.omtRewrite.OmtMiddlewareData;
import com.swiggy.api.erp.ff.helper.OmtMiddlewareHelper;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class GetPaymentDetailsAPITests extends OmtMiddlewareData {

    OmtMiddlewareHelper helper = new OmtMiddlewareHelper();

    @Test(dataProvider = "getPaymentDetailsData", description = "tests for OMS-Middleware API to get payment details")
    public void getPaymentDetailsAPITests(String orderId, String scenario, int expectedResponse, String paymentMethod, String txnId) throws InterruptedException {
        System.out.println("***************************************** getPaymentDetailsAPITests with "+scenario+" started *****************************************");

        Processor response =  helper.getPaymentDetails(orderId);
        System.out.println("OrderId is ===" + orderId);

        Assert.assertEquals(response.ResponseValidator.GetResponseCode(), expectedResponse, "Actual respponse did not match with the expected response "+orderId);
        if (paymentMethod != null && txnId != null){
            SoftAssert softAssertion= new SoftAssert();
            softAssertion.assertEquals(response.ResponseValidator.GetNodeValue("$.statusMessage"), "Success!", "Actual Status message did not match with expected "+orderId);
            softAssertion.assertEquals(response.ResponseValidator.GetNodeValue("data.payment_txn_status"), "success", "Actual status in data did not match with expected "+orderId);
            softAssertion.assertEquals(response.ResponseValidator.GetNodeValue("data.payment_method"), paymentMethod, "Actual payment method did not match with expected "+orderId);
            softAssertion.assertEquals(response.ResponseValidator.GetNodeValue("data.payment_txn_id"), txnId, "Actual txnID did not match with expected "+orderId);
            softAssertion.assertAll();
        }

        System.out.println("######################################### getPaymentDetailsAPITests with "+scenario+" compleated #########################################");
    }

    @Test(dataProvider = "getPaymentDetailsAsPostPutPatchDeleteData", description = "tests for OMS-Middleware API to get payment details by hitting it as POST")
    public void getPaymentDetailsAPIAsPOSTTests(String orderId, int expectedResponse){
        System.out.println("***************************************** getPaymentDetailsAPIAsPOSTTests started *****************************************");

        Processor response =  helper.getPaymentDetailsAsPost(orderId);

        Assert.assertEquals(response.ResponseValidator.GetResponseCode(), expectedResponse, "Actual respponse did not match with the expected response");

        System.out.println("######################################### getPaymentDetailsAPIAsPOSTTests compleated #########################################");
    }

    @Test(dataProvider = "getPaymentDetailsAsPostPutPatchDeleteData", description = "tests for OMS-Middleware API to get payment details by hitting it as PUT")
    public void getPaymentDetailsAPIAsPUTTests(String orderId, int expectedResponse){
        System.out.println("***************************************** getPaymentDetailsAPIAsPUTTests started *****************************************");

        Processor response =  helper.getPaymentDetailsAsPut(orderId);

        Assert.assertEquals(response.ResponseValidator.GetResponseCode(), expectedResponse, "Actual respponse did not match with the expected response");

        System.out.println("######################################### getPaymentDetailsAPIAsPUTTests compleated #########################################");
    }

//    @Test(dataProvider = "getPaymentDetailsAsPostPutPatchDeleteData", description = "tests for OMS-Middleware API to get payment details by hitting it as PATCH")
    public void getPaymentDetailsAPIAsPATCHTests(String orderId, int expectedResponse){
        System.out.println("***************************************** getPaymentDetailsAPIAsPATCHTests started *****************************************");

        Processor response =  helper.getPaymentDetailsAsPatch(orderId);

        Assert.assertEquals(response.ResponseValidator.GetResponseCode(), expectedResponse, "Actual respponse did not match with the expected response");

        System.out.println("######################################### getPaymentDetailsAPIAsPATCHTests compleated #########################################");
    }

    @Test(dataProvider = "getPaymentDetailsAsPostPutPatchDeleteData", description = "tests for OMS-Middleware API to get payment details by hitting it as DELETE")
    public void getPaymentDetailsAPIAsDELETETests(String orderId, int expectedResponse){
        System.out.println("***************************************** getPaymentDetailsAPIAsDELETETests started *****************************************");

        Processor response =  helper.getPaymentDetailsAsDelete(orderId);

        Assert.assertEquals(response.ResponseValidator.GetResponseCode(), expectedResponse, "Actual respponse did not match with the expected response");

        System.out.println("######################################### getPaymentDetailsAPIAsDELETETests compleated #########################################");
    }

}
