package com.swiggy.api.erp.ff.tests.assignmentService;

import com.swiggy.api.erp.ff.constants.AssignmentServiceConstants;
import com.swiggy.api.erp.ff.constants.ManualTaskServiceConstants;
import com.swiggy.api.erp.ff.constants.OMSConstants;
import com.swiggy.api.erp.ff.helper.AssignmentServiceHelper;
import com.swiggy.api.erp.ff.helper.LOSHelper;
import com.swiggy.api.erp.ff.helper.ManualTaskServiceHelper;
import com.swiggy.api.erp.ff.helper.OMSHelper;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Tyrion.DBHelper;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.List;
import java.util.Map;

public class AssignmentService_DBChanges {

    AssignmentServiceHelper assignmentServiceHelper = new AssignmentServiceHelper();
    LOSHelper losHelper = new LOSHelper();
    OMSHelper omshelper = new OMSHelper();
    DBHelper dbHelper = new DBHelper();
    SoftAssert Assert = new SoftAssert();
    ManualTaskServiceHelper mts = new ManualTaskServiceHelper();

    @BeforeMethod
    public void cleanUp(){

        omshelper.redisFlushAll(OMSConstants.REDIS);
        SystemConfigProvider.getTemplate(OMSConstants.SERVICE).execute("update oms_orderexecutive set current_order_count=0,enabled=1,start_duty=1 where id="+AssignmentServiceConstants.verifier_id[0]+";");
        SystemConfigProvider.getTemplate(OMSConstants.SERVICE).execute("update oms_orderexecutive set current_order_count=0,enabled=1,start_duty=1 where id="+AssignmentServiceConstants.verifier_id[1]+";");
        SystemConfigProvider.getTemplate(OMSConstants.SERVICE).execute("update oms_orderexecutive set current_order_count=0,enabled=1,start_duty=1 where id="+AssignmentServiceConstants.verifier_id[2]+";");
        SystemConfigProvider.getTemplate(OMSConstants.SERVICE).execute("update oms_orderexecutive set current_order_count=0,enabled=1,start_duty=1 where id="+AssignmentServiceConstants.l1_id[0]+";");
        SystemConfigProvider.getTemplate(OMSConstants.SERVICE).execute("update oms_orderexecutive set current_order_count=0,enabled=1,start_duty=1 where id="+AssignmentServiceConstants.l1_id[1]+";");
        SystemConfigProvider.getTemplate(OMSConstants.SERVICE).execute("update oms_orderexecutive set current_order_count=0,enabled=1,start_duty=1 where id="+AssignmentServiceConstants.l1_id[2]+";");

        SystemConfigProvider.getTemplate(OMSConstants.SERVICE).execute("update oms_orderexecutive set current_order_count=0,enabled=1,start_duty=1 where id="+AssignmentServiceConstants.l2_id[0]+";");
        SystemConfigProvider.getTemplate(OMSConstants.SERVICE).execute("update oms_orderexecutive set current_order_count=0,enabled=1,start_duty=1 where id="+AssignmentServiceConstants.l2_id[1]+";");
        SystemConfigProvider.getTemplate(OMSConstants.SERVICE).execute("update oms_orderexecutive set current_order_count=0,enabled=1,start_duty=1 where id="+AssignmentServiceConstants.l2_id[2]+";");
        SystemConfigProvider.getTemplate(OMSConstants.SERVICE).execute("update oms_orderexecutive set current_order_count=0,enabled=1,start_duty=1 where id="+AssignmentServiceConstants.callde_id[0]+";");
        SystemConfigProvider.getTemplate(OMSConstants.SERVICE).execute("update oms_orderexecutive set current_order_count=0,enabled=1,start_duty=1 where id="+AssignmentServiceConstants.callde_id[1]+";");
        SystemConfigProvider.getTemplate(OMSConstants.SERVICE).execute("update oms_orderexecutive set current_order_count=0,enabled=1,start_duty=1 where id="+AssignmentServiceConstants.callde_id[2]+";");

//
//        SystemConfigProvider.getTemplate(OMSConstants.ASSIGNMENT_DB).execute("delete FROM db_assignment.request_assignment_map;");
//        SystemConfigProvider.getTemplate(OMSConstants.ASSIGNMENT_DB).execute("delete FROM db_assignment.assignment;");
//        SystemConfigProvider.getTemplate(OMSConstants.ASSIGNMENT_DB).execute("delete FROM db_assignment.assignment_request;");

    }

    @Test
    public void manulOrderVerification_WithMonolith() throws Exception {

        //Create Order
        Long orderId1 = Long.parseLong(losHelper.getAnOrder("manual"));

        //Placing 1st manual order
        assignmentServiceHelper.manualOrderRMQ(AssignmentServiceConstants.verifier_assignment_reason_id, orderId1, AssignmentServiceConstants.verifier_role_id, assignmentServiceHelper.getOrderPk(orderId1));

        String s1 = String.valueOf(orderId1);

        //Checking order is present in ordersinqueue table
        Assert.assertTrue(assignmentServiceHelper.checkInOrdersInQueueTable(orderId1, "Verifier"), orderId1 + "==== is not present in OrdersInQueueTable");

        Thread.sleep(5000);

        //checking order in assignment_request table
        List<Map<String,Object>> requestObjects = assignmentServiceHelper.checkInAssignment_RequestTable(orderId1, 6);
        String requestId = requestObjects.get(0).get("id").toString();
        Assert.assertEquals(requestObjects.get(0).get("status").toString(),"open","Status is not open");
        Assert.assertEquals(requestObjects.get(0).get("end_time"),null,"end time  is not null at the time of request raised");



        //Verifier OE login
        assignmentServiceHelper.oeLoginRMQ(AssignmentServiceConstants.verifier_id[0], AssignmentServiceConstants.verifier_role_id);
        Thread.sleep(6000);


        //Checking whether order is assigned to OE in mysql table
        Assert.assertTrue(DBHelper.pollDB(OMSConstants.ASSIGNMENT_DB,"SELECT order_id FROM db_assignment.assignment where order_id = " + orderId1 + " and oe_id = " +  AssignmentServiceConstants.verifier_id[0]+ ";","order_id",s1,5,70),"Orderid is not found in DB");

        //Checking in assigmentTable
        String assignment_id = assignmentServiceHelper.checkInAssignmentDB(orderId1,AssignmentServiceConstants.verifier_id[0]);


        //Checking in request_assignment_map table
        Assert.assertEquals(assignmentServiceHelper.checkInAssignment_Request_MapTable(assignment_id,requestId),"1","is_active is not 1");


        //Verifier - All Good proceed
        omshelper.verifyOrder(""+orderId1, "001");
        Thread.sleep(1000);

        //checking order in assignment_request table after Verification done
        List<Map<String,Object>> requestObjects2 = assignmentServiceHelper.checkInAssignment_RequestTable(orderId1, 6);
        Assert.assertEquals(requestObjects2.get(0).get("status").toString(),"completed","Status is not open");
        Assert.assertNotNull(requestObjects2.get(0).get("end_time"),"end time  is not null at the time of request raised");

        //Checking in request_assignment_map table after Verification done
       Assert.assertEquals(assignmentServiceHelper.checkInAssignment_Request_MapTable(assignment_id,requestId),"0","is_active is still 1");

    }

    @Test
    public void order_L1Assigned_placedbyPartner_withManualTaskService() throws Exception {

        //Create Order
        Long orderId1 = Long.parseLong(losHelper.getAnOrder("manual"));
        String s1 = String.valueOf(orderId1);

        mts.createTask2(s1,"MANUAL_PLACING_ORDER","test","test");


//        //Checking order is present in ordersinqueue table
//        Assert.assertTrue(assignmentServiceHelper.checkInOrdersInQueueTable(orderId1, "Verifier"), orderId1 + "==== is not present in OrdersInQueueTable");
//
//        Thread.sleep(5000);

               //checking order in assignment_request table
        List<Map<String,Object>> requestObjects = assignmentServiceHelper.checkInAssignment_RequestTable(orderId1, 1);
        String requestId = requestObjects.get(0).get("id").toString();
        Assert.assertEquals(requestObjects.get(0).get("status").toString(),"open","Status is not open");
        Assert.assertEquals(requestObjects.get(0).get("end_time"),null,"end time  is not null at the time of request raised");

        //checking in task table
        Assert.assertTrue(DBHelper.pollDB(ManualTaskServiceConstants.MANUAL_TASK_DB_HOST,"SELECT assignment_request_id FROM omt.task where order_id = " + orderId1 + ";","assignment_request_id",requestId,5,70),"requestId is not found in DB");




        //Verifier OE login
        assignmentServiceHelper.oeLoginRMQ(AssignmentServiceConstants.l1_id[0], AssignmentServiceConstants.l1_role_id);
        Thread.sleep(2000);


        //Checking whether order is assigned to OE in mysql table
        Assert.assertTrue(DBHelper.pollDB(OMSConstants.ASSIGNMENT_DB,"SELECT order_id FROM db_assignment.assignment where order_id = " + orderId1 + " and oe_id = " +  AssignmentServiceConstants.l1_id[0]+ ";","order_id",s1,5,70),"Orderid is not found in DB");

        //Checking in assigmentTable
        String assignment_id = assignmentServiceHelper.checkInAssignmentDB(orderId1,AssignmentServiceConstants.l1_id[0]);


        //Checking in request_assignment_map table
        Assert.assertEquals(assignmentServiceHelper.checkInAssignment_Request_MapTable(assignment_id,requestId),"1","is_active is not 1");


        //order placed by partner
        omshelper.changeOrderStatusToPlacedPartner(s1);
        Thread.sleep(1000);

        //checking order in assignment_request table after Verification done
        List<Map<String,Object>> requestObjects2 = assignmentServiceHelper.checkInAssignment_RequestTable(orderId1, 1);
        Assert.assertEquals(requestObjects2.get(0).get("status").toString(),"completed","Status is not open");
        Assert.assertNotNull(requestObjects2.get(0).get("end_time"),"end time  is not null at the time of request raised");

        //Checking in request_assignment_map table after Verification done
        Assert.assertEquals(assignmentServiceHelper.checkInAssignment_Request_MapTable(assignment_id,requestId),"0","is_active is still 1");


    }

    @Test
    public void order_L1Assigned_LoginLogoutScenario_withManualTaskService() throws Exception {

        //Create Order
        Long orderId1 = Long.parseLong(losHelper.getAnOrder("manual"));
        String s1 = String.valueOf(orderId1);



        mts.createTask2(s1,"MANUAL_PLACING_ORDER","test","test");
//        //Checking order is present in ordersinqueue table
//        Assert.assertTrue(assignmentServiceHelper.checkInOrdersInQueueTable(orderId1, "Verifier"), orderId1 + "==== is not present in OrdersInQueueTable");
//
//        Thread.sleep(5000);

        //checking order in assignment_request table
        List<Map<String,Object>> requestObjects = assignmentServiceHelper.checkInAssignment_RequestTable(orderId1, 1);
        String requestId = requestObjects.get(0).get("id").toString();
        Assert.assertEquals(requestObjects.get(0).get("status").toString(),"open","Status is not open");
        Assert.assertEquals(requestObjects.get(0).get("end_time"),null,"end time  is not null at the time of request raised");


        //checking in task table
        Assert.assertTrue(DBHelper.pollDB(ManualTaskServiceConstants.MANUAL_TASK_DB_HOST,"SELECT assignment_request_id FROM omt.task where order_id = " + orderId1 + ";","assignment_request_id",requestId,5,70),"requestId is not found in DB");




        //Verifier OE login
        assignmentServiceHelper.oeLoginRMQ(AssignmentServiceConstants.l1_id[0], AssignmentServiceConstants.l1_role_id);
        Thread.sleep(2000);


        //Checking whether order is assigned to OE in mysql table
        Assert.assertTrue(DBHelper.pollDB(OMSConstants.ASSIGNMENT_DB,"SELECT order_id FROM db_assignment.assignment where order_id = " + orderId1 + " and oe_id = " +  AssignmentServiceConstants.l1_id[0]+ ";","order_id",s1,5,70),"Orderid is not found in DB");

        //Checking in assigmentTable
        String assignment_id = assignmentServiceHelper.checkInAssignmentDB(orderId1,AssignmentServiceConstants.l1_id[0]);


        //Checking in request_assignment_map table
        Assert.assertEquals(assignmentServiceHelper.checkInAssignment_Request_MapTable(assignment_id,requestId),"1","is_active is not 1");

        //OE1 logout
        assignmentServiceHelper.oeLogoutRMQ(AssignmentServiceConstants.l1_id[0],AssignmentServiceConstants.l1_role_id, AssignmentServiceConstants.logout_new_state_id);
        Thread.sleep(2000);

        //checking order in assignment_request table after Verification done
        List<Map<String,Object>> requestObjects2 = assignmentServiceHelper.checkInAssignment_RequestTable(orderId1, 1);
        Assert.assertEquals(requestObjects2.get(0).get("status").toString(),"open","Status is not open");
        Assert.assertEquals(requestObjects2.get(0).get("end_time"),null,"end time  is not null at the time of request raised");

        //Checking in request_assignment_map table after Verification done
        Assert.assertEquals(assignmentServiceHelper.checkInAssignment_Request_MapTable(assignment_id,requestId),"0","is_active is still 1");

        //Verifier OE2 login
        assignmentServiceHelper.oeLoginRMQ(AssignmentServiceConstants.l1_id[1], AssignmentServiceConstants.l1_role_id);
        Thread.sleep(2000);

        //Checking whether order is assigned to OE in mysql table
        Assert.assertTrue(DBHelper.pollDB(OMSConstants.ASSIGNMENT_DB,"SELECT order_id FROM db_assignment.assignment where order_id = " + orderId1 + " and oe_id = " +  AssignmentServiceConstants.l1_id[0]+ ";","order_id",s1,5,70),"Orderid is not found in DB");


          //Checking in assigmentTable
        String assignment_id2 = assignmentServiceHelper.checkInAssignmentDB(orderId1,AssignmentServiceConstants.l1_id[1]);

        //Checking in request_assignment_map table after Verification done
        Assert.assertEquals(assignmentServiceHelper.checkInAssignment_Request_MapTable(assignment_id2,requestId),"1","is_active is 0");



    }

    @Test
    public void order_L1Assigned_MultipleRequest_withManualTaskService() throws Exception {

        //Create Order
        Long orderId1 = Long.parseLong(losHelper.getAnOrder("manual"));
        String s1 = String.valueOf(orderId1);

        mts.createTask2(s1,"MANUAL_PLACING_ORDER","test","test");

        mts.createTask2(s1,"PLACING_DELAY","test","test");




//        //Checking order is present in ordersinqueue table
//        Assert.assertTrue(assignmentServiceHelper.checkInOrdersInQueueTable(orderId1, "Verifier"), orderId1 + "==== is not present in OrdersInQueueTable");
//
//        Thread.sleep(5000);

        //checking order in assignment_request table
        List<Map<String,Object>> requestObjects = assignmentServiceHelper.checkInAssignment_RequestTable(orderId1, 1);
        String requestId = requestObjects.get(0).get("id").toString();
        Assert.assertEquals(requestObjects.get(0).get("status").toString(),"open","Status is not open");
        Assert.assertEquals(requestObjects.get(0).get("end_time"),null,"end time  is not null at the time of request raised");

        //checking in task table
        Assert.assertTrue(DBHelper.pollDB(ManualTaskServiceConstants.MANUAL_TASK_DB_HOST,"SELECT assignment_request_id FROM omt.task where order_id = " + orderId1 + ";","assignment_request_id",requestId,5,70),"requestId is not found in DB");



        //Verifier OE login
        assignmentServiceHelper.oeLoginRMQ(AssignmentServiceConstants.l1_id[0], AssignmentServiceConstants.l1_role_id);
        Thread.sleep(2000);


        //Checking whether order is assigned to OE in mysql table
        Assert.assertTrue(DBHelper.pollDB(OMSConstants.ASSIGNMENT_DB,"SELECT order_id FROM db_assignment.assignment where order_id = " + orderId1 + " and oe_id = " +  AssignmentServiceConstants.l1_id[0]+ ";","order_id",s1,5,70),"Orderid is not found in DB");

        //Checking in assigmentTable
        String assignment_id = assignmentServiceHelper.checkInAssignmentDB(orderId1,AssignmentServiceConstants.l1_id[0]);


        //Checking in request_assignment_map table
        Assert.assertEquals(assignmentServiceHelper.checkInAssignment_Request_MapTable(assignment_id,requestId),"1","is_active is not 1");


        //order placed by partner
        omshelper.changeOrderStatusToPlacedPartner(s1);
        Thread.sleep(1000);

        //checking order in assignment_request table after Verification done
        List<Map<String,Object>> requestObjects2 = assignmentServiceHelper.checkInAssignment_RequestTable(orderId1, 1);
        Assert.assertEquals(requestObjects2.get(0).get("status").toString(),"completed","Status is not open");
        Assert.assertNotNull(requestObjects2.get(0).get("end_time"),"end time  is not null at the time of request raised");

        System.out.println(assignmentServiceHelper.checkInAssignment_Request_MapTable(assignment_id,requestId)+"+++++++++++++++=============++++++++++++");


        //Checking in request_assignment_map table after Verification done
        Assert.assertEquals(assignmentServiceHelper.checkInAssignment_Request_MapTable(assignment_id,requestId),"0","is_active is still 1");


    }

    @Test
    public void order_L2OE_CancellationOrder_withManualTaskService() throws Exception {
        //Create Order
        Long orderId1 = Long.parseLong(losHelper.getAnOrder("manual"));
        String s1 = String.valueOf(orderId1);

        mts.createTask2(s1,"CANCELLATION_REQUIRED","test","test");

        //checking order in assignment_request table
        List<Map<String,Object>> requestObjects = assignmentServiceHelper.checkInAssignment_RequestTable(orderId1, 2);
        String requestId = requestObjects.get(0).get("id").toString();
        Assert.assertEquals(requestObjects.get(0).get("status").toString(),"open","Status is not open");
        Assert.assertEquals(requestObjects.get(0).get("end_time"),null,"end time  is not null at the time of request raised");

        //checking in task table
        Assert.assertTrue(DBHelper.pollDB(ManualTaskServiceConstants.MANUAL_TASK_DB_HOST,"SELECT assignment_request_id FROM omt.task where order_id = " + orderId1 + ";","assignment_request_id",requestId,5,70),"requestId is not found in DB");




        //Verifier OE login
        assignmentServiceHelper.oeLoginRMQ(AssignmentServiceConstants.l2_id[0], AssignmentServiceConstants.l2_role_id);
        Thread.sleep(2000);


        //Checking whether order is assigned to OE in mysql table
        Assert.assertTrue(DBHelper.pollDB(OMSConstants.ASSIGNMENT_DB,"SELECT order_id FROM db_assignment.assignment where order_id = " + orderId1 + " and oe_id = " +  AssignmentServiceConstants.l1_id[0]+ ";","order_id",s1,5,70),"Orderid is not found in DB");

        //Checking in assigmentTable
        String assignment_id = assignmentServiceHelper.checkInAssignmentDB(orderId1,AssignmentServiceConstants.l2_id[0]);

        System.out.println("==========================================="+assignment_id+"-============="+requestId);

        //Checking in request_assignment_map table
        Assert.assertEquals(assignmentServiceHelper.checkInAssignment_Request_MapTable(assignment_id,requestId),"1","is_active is not 1");


        //order placed by partner
        omshelper.changeOrderStatusToPlacedPartner(s1);
        Thread.sleep(1000);

        //checking order in assignment_request table after Verification done
        List<Map<String,Object>> requestObjects2 = assignmentServiceHelper.checkInAssignment_RequestTable(orderId1, 2);
        Assert.assertEquals(requestObjects2.get(0).get("status").toString(),"completed","Status is not open");
        Assert.assertNotNull(requestObjects2.get(0).get("end_time"),"end time  is not null at the time of request raised");

        System.out.println(assignmentServiceHelper.checkInAssignment_Request_MapTable(assignment_id,requestId)+"+++++++++++++++=============++++++++++++");


        //Checking in request_assignment_map table after Verification done
        Assert.assertEquals(assignmentServiceHelper.checkInAssignment_Request_MapTable(assignment_id,requestId),"0","is_active is still 1");

    }

    @Test
    public void order_L2OE_PARTNER_CALL_REQUEST_withManualTaskService() throws Exception {
        //Create Order
        Long orderId1 = Long.parseLong(losHelper.getAnOrder("manual"));
        String s1 = String.valueOf(orderId1);

        mts.createTask2(s1,"PARTNER_CALL_REQUEST","test","test");

        //checking order in assignment_request table
        List<Map<String,Object>> requestObjects = assignmentServiceHelper.checkInAssignment_RequestTable(orderId1, 2);
        String requestId = requestObjects.get(0).get("id").toString();
        Assert.assertEquals(requestObjects.get(0).get("status").toString(),"open","Status is not open");
        Assert.assertEquals(requestObjects.get(0).get("end_time"),null,"end time  is not null at the time of request raised");

        //checking in task table
        Assert.assertTrue(DBHelper.pollDB(ManualTaskServiceConstants.MANUAL_TASK_DB_HOST,"SELECT assignment_request_id FROM omt.task where order_id = " + orderId1 + ";","assignment_request_id",requestId,5,70),"requestId is not found in DB");




        //Verifier OE login
        assignmentServiceHelper.oeLoginRMQ(AssignmentServiceConstants.l2_id[0], AssignmentServiceConstants.l2_role_id);
        Thread.sleep(2000);


        //Checking whether order is assigned to OE in mysql table
        Assert.assertTrue(DBHelper.pollDB(OMSConstants.ASSIGNMENT_DB,"SELECT order_id FROM db_assignment.assignment where order_id = " + orderId1 + " and oe_id = " +  AssignmentServiceConstants.l1_id[0]+ ";","order_id",s1,5,70),"Orderid is not found in DB");

        //Checking in assigmentTable
        String assignment_id = assignmentServiceHelper.checkInAssignmentDB(orderId1,AssignmentServiceConstants.l2_id[0]);

        System.out.println("==========================================="+assignment_id+"-============="+requestId);

        //Checking in request_assignment_map table
        Assert.assertEquals(assignmentServiceHelper.checkInAssignment_Request_MapTable(assignment_id,requestId),"1","is_active is not 1");


        //order placed by partner
        omshelper.changeOrderStatusToPlacedPartner(s1);
        Thread.sleep(1000);

        //checking order in assignment_request table after Verification done
        List<Map<String,Object>> requestObjects2 = assignmentServiceHelper.checkInAssignment_RequestTable(orderId1, 2);
        Assert.assertEquals(requestObjects2.get(0).get("status").toString(),"completed","Status is not open");
        Assert.assertNotNull(requestObjects2.get(0).get("end_time"),"end time  is not null at the time of request raised");

        System.out.println(assignmentServiceHelper.checkInAssignment_Request_MapTable(assignment_id,requestId)+"+++++++++++++++=============++++++++++++");


        //Checking in request_assignment_map table after Verification done
        Assert.assertEquals(assignmentServiceHelper.checkInAssignment_Request_MapTable(assignment_id,requestId),"0","is_active is still 1");

    }

    @Test
    public void order_L2OE_ORDER_CART_EDIT_withManualTaskService() throws Exception {
        //Create Order
        Long orderId1 = Long.parseLong(losHelper.getAnOrder("manual"));
        String s1 = String.valueOf(orderId1);

        mts.createTask2(s1,"PARTNER_CALL_REQUEST","test","test");

        //checking order in assignment_request table
        List<Map<String,Object>> requestObjects = assignmentServiceHelper.checkInAssignment_RequestTable(orderId1, 2);
        String requestId = requestObjects.get(0).get("id").toString();
        Assert.assertEquals(requestObjects.get(0).get("status").toString(),"open","Status is not open");
        Assert.assertEquals(requestObjects.get(0).get("end_time"),null,"end time  is not null at the time of request raised");

        //checking in task table
        Assert.assertTrue(DBHelper.pollDB(ManualTaskServiceConstants.MANUAL_TASK_DB_HOST,"SELECT assignment_request_id FROM omt.task where order_id = " + orderId1 + ";","assignment_request_id",requestId,5,70),"requestId is not found in DB");




        //Verifier OE login
        assignmentServiceHelper.oeLoginRMQ(AssignmentServiceConstants.l2_id[0], AssignmentServiceConstants.l2_role_id);
        Thread.sleep(2000);


        //Checking whether order is assigned to OE in mysql table
        Assert.assertTrue(DBHelper.pollDB(OMSConstants.ASSIGNMENT_DB,"SELECT order_id FROM db_assignment.assignment where order_id = " + orderId1 + " and oe_id = " +  AssignmentServiceConstants.l1_id[0]+ ";","order_id",s1,5,70),"Orderid is not found in DB");

        //Checking in assigmentTable
        String assignment_id = assignmentServiceHelper.checkInAssignmentDB(orderId1,AssignmentServiceConstants.l2_id[0]);

        System.out.println("==========================================="+assignment_id+"-============="+requestId);

        //Checking in request_assignment_map table
        Assert.assertEquals(assignmentServiceHelper.checkInAssignment_Request_MapTable(assignment_id,requestId),"1","is_active is not 1");


        //order placed by partner
        omshelper.changeOrderStatusToPlacedPartner(s1);
        Thread.sleep(1000);

        //checking order in assignment_request table after Verification done
        List<Map<String,Object>> requestObjects2 = assignmentServiceHelper.checkInAssignment_RequestTable(orderId1, 2);
        Assert.assertEquals(requestObjects2.get(0).get("status").toString(),"completed","Status is not open");
        Assert.assertNotNull(requestObjects2.get(0).get("end_time"),"end time  is not null at the time of request raised");

        System.out.println(assignmentServiceHelper.checkInAssignment_Request_MapTable(assignment_id,requestId)+"+++++++++++++++=============++++++++++++");


        //Checking in request_assignment_map table after Verification done
        Assert.assertEquals(assignmentServiceHelper.checkInAssignment_Request_MapTable(assignment_id,requestId),"0","is_active is still 1");

    }

    @Test
    public void order_L2OE_PLACING_STATE_MISMATCH_withManualTaskService() throws Exception {
        //Create Order
        Long orderId1 = Long.parseLong(losHelper.getAnOrder("manual"));
        String s1 = String.valueOf(orderId1);

        mts.createTask2(s1,"PARTNER_CALL_REQUEST","test","test");

        //checking order in assignment_request table
        List<Map<String,Object>> requestObjects = assignmentServiceHelper.checkInAssignment_RequestTable(orderId1, 2);
        String requestId = requestObjects.get(0).get("id").toString();
        Assert.assertEquals(requestObjects.get(0).get("status").toString(),"open","Status is not open");
        Assert.assertEquals(requestObjects.get(0).get("end_time"),null,"end time  is not null at the time of request raised");

        //checking in task table
        Assert.assertTrue(DBHelper.pollDB(ManualTaskServiceConstants.MANUAL_TASK_DB_HOST,"SELECT assignment_request_id FROM omt.task where order_id = " + orderId1 + ";","assignment_request_id",requestId,5,70),"requestId is not found in DB");




        //Verifier OE login
        assignmentServiceHelper.oeLoginRMQ(AssignmentServiceConstants.l2_id[0], AssignmentServiceConstants.l2_role_id);
        Thread.sleep(2000);


        //Checking whether order is assigned to OE in mysql table
        Assert.assertTrue(DBHelper.pollDB(OMSConstants.ASSIGNMENT_DB,"SELECT order_id FROM db_assignment.assignment where order_id = " + orderId1 + " and oe_id = " +  AssignmentServiceConstants.l1_id[0]+ ";","order_id",s1,5,70),"Orderid is not found in DB");

        //Checking in assigmentTable
        String assignment_id = assignmentServiceHelper.checkInAssignmentDB(orderId1,AssignmentServiceConstants.l2_id[0]);

        System.out.println("==========================================="+assignment_id+"-============="+requestId);

        //Checking in request_assignment_map table
        Assert.assertEquals(assignmentServiceHelper.checkInAssignment_Request_MapTable(assignment_id,requestId),"1","is_active is not 1");


        //order placed by partner
        omshelper.changeOrderStatusToPlacedPartner(s1);
        Thread.sleep(1000);

        //checking order in assignment_request table after Verification done
        List<Map<String,Object>> requestObjects2 = assignmentServiceHelper.checkInAssignment_RequestTable(orderId1, 2);
        Assert.assertEquals(requestObjects2.get(0).get("status").toString(),"completed","Status is not open");
        Assert.assertNotNull(requestObjects2.get(0).get("end_time"),"end time  is not null at the time of request raised");

        System.out.println(assignmentServiceHelper.checkInAssignment_Request_MapTable(assignment_id,requestId)+"+++++++++++++++=============++++++++++++");


        //Checking in request_assignment_map table after Verification done
        Assert.assertEquals(assignmentServiceHelper.checkInAssignment_Request_MapTable(assignment_id,requestId),"0","is_active is still 1");

    }

    @Test
    public void order_L2OE_ORDER_DECLINED_BY_RESTAURANT_withManualTaskService() throws Exception {
        //Create Order
        Long orderId1 = Long.parseLong(losHelper.getAnOrder("manual"));
        String s1 = String.valueOf(orderId1);

        mts.createTask2(s1,"PARTNER_CALL_REQUEST","test","test");

        //checking order in assignment_request table
        List<Map<String,Object>> requestObjects = assignmentServiceHelper.checkInAssignment_RequestTable(orderId1, 2);
        String requestId = requestObjects.get(0).get("id").toString();
        Assert.assertEquals(requestObjects.get(0).get("status").toString(),"open","Status is not open");
        Assert.assertEquals(requestObjects.get(0).get("end_time"),null,"end time  is not null at the time of request raised");

        //checking in task table
        Assert.assertTrue(DBHelper.pollDB(ManualTaskServiceConstants.MANUAL_TASK_DB_HOST,"SELECT assignment_request_id FROM omt.task where order_id = " + orderId1 + ";","assignment_request_id",requestId,5,70),"requestId is not found in DB");




        //Verifier OE login
        assignmentServiceHelper.oeLoginRMQ(AssignmentServiceConstants.l2_id[0], AssignmentServiceConstants.l2_role_id);
        Thread.sleep(2000);


        //Checking whether order is assigned to OE in mysql table
        Assert.assertTrue(DBHelper.pollDB(OMSConstants.ASSIGNMENT_DB,"SELECT order_id FROM db_assignment.assignment where order_id = " + orderId1 + " and oe_id = " +  AssignmentServiceConstants.l1_id[0]+ ";","order_id",s1,5,70),"Orderid is not found in DB");

        //Checking in assigmentTable
        String assignment_id = assignmentServiceHelper.checkInAssignmentDB(orderId1,AssignmentServiceConstants.l2_id[0]);

        System.out.println("==========================================="+assignment_id+"-============="+requestId);

        //Checking in request_assignment_map table
        Assert.assertEquals(assignmentServiceHelper.checkInAssignment_Request_MapTable(assignment_id,requestId),"1","is_active is not 1");


        //order placed by partner
        omshelper.changeOrderStatusToPlacedPartner(s1);
        Thread.sleep(1000);

        //checking order in assignment_request table after Verification done
        List<Map<String,Object>> requestObjects2 = assignmentServiceHelper.checkInAssignment_RequestTable(orderId1, 2);
        Assert.assertEquals(requestObjects2.get(0).get("status").toString(),"completed","Status is not open");
        Assert.assertNotNull(requestObjects2.get(0).get("end_time"),"end time  is not null at the time of request raised");

        System.out.println(assignmentServiceHelper.checkInAssignment_Request_MapTable(assignment_id,requestId)+"+++++++++++++++=============++++++++++++");


        //Checking in request_assignment_map table after Verification done
        Assert.assertEquals(assignmentServiceHelper.checkInAssignment_Request_MapTable(assignment_id,requestId),"0","is_active is still 1");

    }


}
