package com.swiggy.api.erp.ff.POJO.CouponFraudCheck;

public class Coupon_details
{
    private String[] coupon_categories;

    public String[] getCoupon_categories ()
    {
        return coupon_categories;
    }

    public void setCoupon_categories (String[] coupon_categories)
    {
        this.coupon_categories = coupon_categories;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [coupon_categories = "+coupon_categories+"]";
    }
}