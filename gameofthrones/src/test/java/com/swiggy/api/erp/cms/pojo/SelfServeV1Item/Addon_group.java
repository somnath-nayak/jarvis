package com.swiggy.api.erp.cms.pojo.SelfServeV1Item;

public class Addon_group {
	private String addon_min_limit;

    private String item_id;

    private String name;

    private String updated_by;

    private String active;

    private String id;

    private String third_party_id;

    private String created_by;

    private String addon_limit;

    private String temp_addon_group_id;

    private String addon_free_limit;

    private String order;

    public String getAddon_min_limit ()
    {
        return addon_min_limit;
    }

    public void setAddon_min_limit (String addon_min_limit)
    {
        this.addon_min_limit = addon_min_limit;
    }

    public String getItem_id ()
    {
        return item_id;
    }

    public void setItem_id (String item_id)
    {
        this.item_id = item_id;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getUpdated_by ()
    {
        return updated_by;
    }

    public void setUpdated_by (String updated_by)
    {
        this.updated_by = updated_by;
    }

    public String getActive ()
    {
        return active;
    }

    public void setActive (String active)
    {
        this.active = active;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getThird_party_id ()
    {
        return third_party_id;
    }

    public void setThird_party_id (String third_party_id)
    {
        this.third_party_id = third_party_id;
    }

    public String getCreated_by ()
    {
        return created_by;
    }

    public void setCreated_by (String created_by)
    {
        this.created_by = created_by;
    }

    public String getAddon_limit ()
    {
        return addon_limit;
    }

    public void setAddon_limit (String addon_limit)
    {
        this.addon_limit = addon_limit;
    }

    public String getTemp_addon_group_id ()
    {
        return temp_addon_group_id;
    }

    public void setTemp_addon_group_id (String temp_addon_group_id)
    {
        this.temp_addon_group_id = temp_addon_group_id;
    }

    public String getAddon_free_limit ()
    {
        return addon_free_limit;
    }

    public void setAddon_free_limit (String addon_free_limit)
    {
        this.addon_free_limit = addon_free_limit;
    }

    public String getOrder ()
    {
        return order;
    }

    public void setOrder (String order)
    {
        this.order = order;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [addon_min_limit = "+addon_min_limit+", item_id = "+item_id+", name = "+name+", updated_by = "+updated_by+", active = "+active+", id = "+id+", third_party_id = "+third_party_id+", created_by = "+created_by+", addon_limit = "+addon_limit+", temp_addon_group_id = "+temp_addon_group_id+", addon_free_limit = "+addon_free_limit+", order = "+order+"]";
    }
}
			
			