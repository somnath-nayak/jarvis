package com.swiggy.api.erp.delivery.helper;

import com.rabbitmq.client.AMQP;
import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import framework.gameofthrones.Tyrion.RabbitMQHelper;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.text.StrSubstitutor;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author preetesh
 * @project swiggy_test
 * @createdon 11/09/18
 */
public class OrderBuilder
{
    private final String restaurant_area_code;
    private final String restaurant_id;
    private final String order_time;
    private final String lat;
    private final String lng;
    private final String order_id;
    private final String restaurant_customer_distance;
    private final String restaurant_lat_lng;
    private final String is_long_distance;
    private final String order_type;
    private final String is_replicated;
    private String is_batching_enabled;
    private Boolean orderPushStatus;

    public String getOrder_id()
    {
        return order_id;
    }
    public String getRestaurant_id() {
        return restaurant_id;
    }

    public Boolean getOrderPushStatus()
    {
        return orderPushStatus;
    }

    public static class Builder
    {
        private final String restaurant_area_code;
        private String restaurant_id;
        private String order_time;
        private String lat;
        private String lng;
        private String order_id;
        private String restaurant_customer_distance;
        private String restaurant_lat_lng;
        private String is_long_distance;
        private String is_batching_enabled;
        private String is_replicated;
        private String order_type;
        private Boolean orderPushStatus;


        public Builder(String restaurant_area_code)
        {
            this.restaurant_area_code=restaurant_area_code;
        }

        public Builder restaurant_id(String val)
        {
            restaurant_id=val;
            return this;
        }

        public Builder order_id(String val)
        {
            order_id=val;
            return this;
        }

        public Builder order_time(String val)
        {
            order_time=val;
            return this;
        }

        public Builder lat(String val)
        {
            lat=val;
            return this;
        }

        public Builder lng(String val)
        {
            lng=val;
            return this;
        }

        public Builder restaurant_customer_distance(String val)
        {
            restaurant_customer_distance=val;
            return this;
        }

        public Builder restaurant_lat_lng(String val)
        {
            restaurant_lat_lng=val;
            return this;
        }

        public Builder is_batching_enabled(String val)
        {
            is_batching_enabled=val;
            return this;
        }



        public Builder is_long_distance(String val)
        {
            is_long_distance=val;
            return this;
        }

        public Builder order_type(String val)
        {
            order_type=val;
            return this;
        }

        public Builder is_replicated(String val)
        {
            is_replicated=val;
            return this;
        }

        public OrderBuilder build()
        {
            //create a map from instance variables
            //substitue values and
            //push to rmqor
            //order push status in object
            if(restaurant_id=="" || restaurant_id==null)
            {
                if((is_batching_enabled==null || is_batching_enabled==""))
                {
                    Integer restid=DeliveryDataBaseUtils.getRandomEnabledRestaurantFromArea(restaurant_area_code);
                    restaurant_id=restid.toString();
                }
                else
                {
                    Integer restid=DeliveryDataBaseUtils.getRandomBatchingEnabledRestaurantId(restaurant_area_code);
                    restaurant_id=restid.toString();
                }

            }
            if (order_time==null|| order_time=="")
            {
                SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date now = new Date();
                String strDate = sdfDate.format(now);
                System.out.printf("Print");
                order_time=strDate;
            }
            if(lat==null||lat =="")
            {
                String latLons=DeliveryDataBaseUtils.getCentroidLatLonFromZone(DeliveryDataBaseUtils.getZoneFromArea(restaurant_area_code).toString());
                lat=latLons.split(",")[0];
            }
            if(lng==null||lng =="")
            {
                String latLons=DeliveryDataBaseUtils.getCentroidLatLonFromZone(DeliveryDataBaseUtils.getZoneFromArea(restaurant_area_code).toString());
                lng=latLons.split(",")[1];
            }

            if(restaurant_customer_distance==null|| restaurant_customer_distance=="")
            {
                restaurant_customer_distance="1";
            }

            if(restaurant_lat_lng==null||restaurant_lat_lng=="")
            {
                if(restaurant_id==null|| restaurant_id=="")
                {
                    Integer restid=DeliveryDataBaseUtils.getRandomEnabledRestaurantFromArea(restaurant_area_code);
                    restaurant_lat_lng=DeliveryDataBaseUtils.getRestaurantLatLong(restid.toString());
                }
                else
                {
                    restaurant_lat_lng=DeliveryDataBaseUtils.getRestaurantLatLong(restaurant_id);
                }

            }

            if(is_batching_enabled==null|| is_batching_enabled=="")
            {
                is_batching_enabled="false";
            }

            if(is_long_distance==null|| is_long_distance=="")
            {
                is_long_distance="false";
            }

            if(order_type==null|| order_type=="")
            {
                order_type="regular";
            }

            if(is_replicated==null || is_replicated=="")
            {
                is_replicated="false";
            }
            Map<String, String> orderDetails= new HashMap<>();
            orderDetails.put("restaurant_id",restaurant_id);
            orderDetails.put("restaurant_lat_lng",restaurant_lat_lng);
            orderDetails.put("restaurant_customer_distance",restaurant_customer_distance);
            orderDetails.put("is_long_distance",is_long_distance);
            orderDetails.put("restaurant_area_code",restaurant_area_code);
            orderDetails.put("order_time",order_time);
            orderDetails.put("order_type",order_type);
            orderDetails.put("lat",lat);
            orderDetails.put("lng",lng);
            orderDetails.put("is_replicated",is_replicated);
            orderDetails.put("prep_time","30");
            if(order_id==null) {
                order_id = DeliveryDataBaseUtils.getOrderIdForOrderGeneration();
            }
            orderDetails.put("order_id",order_id);
            String orderTemplatePath="../Data/Payloads/JSON/OrderJsonFordelivery";
            String orderTemplate=null;
            File file = new File(orderTemplatePath);
            try {
                orderTemplate = FileUtils.readFileToString(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
            StrSubstitutor sub = new StrSubstitutor(orderDetails);
            String resolvedString = sub.replace(orderTemplate);
            RabbitMQHelper rabbitMQHelper=new RabbitMQHelper();
            rabbitMQHelper.pushMessageToExchange(DeliveryConstant.rmq_hostname, DeliveryConstant.push_order_queue, new AMQP.BasicProperties().builder().contentType("application/json"), resolvedString);
            String orderQuery="select order_id from trips where  order_id="+order_id;
            Boolean orderstatusindelivery= DeliveryDataBaseUtils.polldb(DeliveryConstant.databaseName,orderQuery,"order_id",order_id,500,30);
            orderPushStatus=orderstatusindelivery;
            return new OrderBuilder(this);
        }

    }

    private OrderBuilder(Builder builder)
    {
        restaurant_area_code=builder.restaurant_area_code;
        restaurant_id=builder.restaurant_id;
        order_time=builder.order_time;
        lat=builder.lat;
        lng=builder.lng;
        order_id=builder.order_id;
        restaurant_customer_distance=builder.restaurant_customer_distance;
        restaurant_lat_lng=builder.restaurant_lat_lng;
        is_long_distance=builder.is_long_distance;
        is_batching_enabled=builder.is_batching_enabled;
        order_type=builder.order_type;
        is_replicated=builder.is_replicated;
        orderPushStatus=builder.orderPushStatus;
    }


}
