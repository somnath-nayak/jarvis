package com.swiggy.api.erp.ff.tests.CancelAndDeliveredEvents;

import com.swiggy.api.erp.ff.constants.LosConstants;
import com.swiggy.api.erp.ff.dp.CancelAndDeliveredDataProvider;
import com.swiggy.api.erp.ff.helper.CancelAndDeliveredHelper;
import com.swiggy.api.erp.ff.helper.LOSHelper;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class CancelAndDeliveredStatus {

    LOSHelper losHelper = new LOSHelper();
    String orderId, orderId2,order_time;
    CancelAndDeliveredHelper cdh = new CancelAndDeliveredHelper();


    @BeforeClass
    public void deliveredOrderStatusScenarios() throws IOException, InterruptedException {

        orderId = losHelper.getAnOrder("manual");
        orderId2 = losHelper.getAnOrder("manual");
         Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat order_idFormat = new SimpleDateFormat("yyyyMMddHHmmss");
         order_time = dateFormat.format(date);
         losHelper.statusUpdate(orderId,order_time, LosConstants.status[5]);
        cdh.cancelOrder(orderId2);


    }

    @Test(dataProvider="differentDeliveryStatus",dataProviderClass = CancelAndDeliveredDataProvider.class)
    public void sendEventforDeliveredOrderId(String event) throws IOException, InterruptedException {
        losHelper.statusUpdate(orderId,order_time, event);
        Assert.assertEquals(cdh.getOrderStatus(orderId),LosConstants.status[5],"Order status is changed from Devliered");
    }

    @Test(dataProvider="differentDeliveryStatus",dataProviderClass = CancelAndDeliveredDataProvider.class)
    public void sendEventforCancelledOrderId(String event) throws IOException, InterruptedException {
        losHelper.statusUpdate(orderId2,order_time, event);
        Assert.assertEquals(cdh.getOrderStatus(orderId2),LosConstants.unassignStatus,"Order status is changed from cancelled");
    }


}
