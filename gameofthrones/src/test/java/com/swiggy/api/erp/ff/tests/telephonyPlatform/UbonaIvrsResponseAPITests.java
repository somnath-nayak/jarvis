package com.swiggy.api.erp.ff.tests.telephonyPlatform;

import com.swiggy.api.erp.ff.dp.telephonyPlatform.TelephonyPlatformTestData;
import com.swiggy.api.erp.ff.helper.TelephonyPlatformHelper;
import framework.gameofthrones.JonSnow.Processor;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashMap;

/**
 Created by Narendra on 26-Dec-2018.
*/

public class UbonaIvrsResponseAPITests extends TelephonyPlatformTestData{

    TelephonyPlatformHelper helper = new TelephonyPlatformHelper();
    Logger log = Logger.getLogger(UbonaIvrsResponseAPITests.class);

    @Test(dataProvider = "ubonaResponseData", description = "Test telephony ubona IVRS Response API")
    public void ubonaIvrsResponseAPITests(String eventName, String status, HashMap<String, String> expectedResponseMap){
        log.info("***************************************** ubonaIvrsResponseAPITests started *****************************************");

        Processor response = helper.ubonaIvrsResponse(eventName, status);
        log.info(response.ResponseValidator.GetBodyAsText());
        log.info("Response code is == " + response.ResponseValidator.GetResponseCode());

        Assert.assertEquals(response.ResponseValidator.GetResponseCode(), Integer.parseInt(expectedResponseMap.get("responseCode")));

        if(response.ResponseValidator.GetResponseCode() == 400){
            Assert.assertEquals(response.ResponseValidator.GetNodeValue("error"), expectedResponseMap.get("error"));
            Assert.assertTrue(response.ResponseValidator.GetNodeValue("message").contains(expectedResponseMap.get("message")));
        }

        log.info("######################################### ubonaIVRSResponseAPITests completed #########################################");
    }

}
