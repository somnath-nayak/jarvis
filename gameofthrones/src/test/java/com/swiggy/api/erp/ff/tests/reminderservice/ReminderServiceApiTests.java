package com.swiggy.api.erp.ff.tests.reminderservice;

import java.util.Calendar;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.ff.dp.ReminderServiceTestData;
import com.swiggy.api.erp.ff.helper.ReminderServiceHelper;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;

public class ReminderServiceApiTests extends ReminderServiceTestData {

	ReminderServiceHelper rshelper = new ReminderServiceHelper();
	Initialize gameofthrones = new Initialize();
	SchemaValidatorUtils schemaValidatorUtils = new SchemaValidatorUtils();

	@Test(priority = 1, dataProvider = "setreminder", groups = { "sanity", "regression" }, description = "Set Reminder")
	public void setReminder(String to, String from, String msg) {

		Calendar calendar = Calendar.getInstance();
		System.out.println(calendar.getTimeInMillis() / 1000);
		calendar.add(Calendar.MINUTE, 1);
		System.out.println(calendar.getTimeInMillis() / 1000);
		Processor response = rshelper.setReminder("" + calendar.getTimeInMillis() / 1000, to, from, msg);
		String message = JsonPath.read(response.RequestValidator.GetBodyAsText(), "$.msg");
		Assert.assertEquals(String.valueOf(response.ResponseValidator.GetResponseCode()), "200",
				"Response code did not match");
		Assert.assertEquals(message, "" + msg, "Reminder message did not match");
	}
	
	@Test(priority = 2, groups = { "sanity", "regression" }, description = "Nullify Reminder")
	public void nullify() {

		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MINUTE, 1);
		Processor response = rshelper.setReminder("" + calendar.getTimeInMillis() / 1000, "service1", "service2", "test message");
		String reminderId = JsonPath.read(response.ResponseValidator.GetBodyAsText(),"$.id");
		Processor nullifyResponse = rshelper.nullify(reminderId);
		Assert.assertEquals(String.valueOf(nullifyResponse.ResponseValidator.GetResponseCode()), "200",
				"Response code did not match");
	}
	
	@Test(priority = 4, groups = { "sanity", "regression" }, dataProvider="BadRequest", description = "Nullify Reminder")
	public void nullifyBadRequest(String reminderId) {

		Processor nullifyResponse = rshelper.nullify(reminderId);
		Assert.assertEquals(String.valueOf(nullifyResponse.ResponseValidator.GetResponseCode()), "400",
				"Response code did not match");
	}

}
