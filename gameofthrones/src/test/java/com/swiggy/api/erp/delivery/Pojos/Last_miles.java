package com.swiggy.api.erp.delivery.Pojos;

import com.swiggy.api.erp.delivery.constants.RTSConstants;

public class Last_miles {

	 private String id;

	    private String address_lat_long;

	    public String getId ()
	    {
	        return id;
	    }

	    public void setId (String id)
	    {
	        this.id = id;
	    }

	    public String getAddress_lat_long ()
	    {
	        return address_lat_long;
	    }

	    public void setAddress_lat_long (String address_lat_long)
	    {
	        this.address_lat_long = address_lat_long;
	    }

	     
	    public void setdefaultvalues()
	    {
	    	if(this.getAddress_lat_long()==null)
	    	{
	    		this.setAddress_lat_long(RTSConstants.default_lat_long);
	    	}
	    	if(this.getId()==null)
	    	{
	    		this.setId(RTSConstants.default_address_id);
	    	}
			
				    }
	    
	    
	    
	    
	    
	    @Override
	    public String toString()
	    {
	        return "ClassPojo [id = "+id+", address_lat_long = "+address_lat_long+"]";
	    }

}