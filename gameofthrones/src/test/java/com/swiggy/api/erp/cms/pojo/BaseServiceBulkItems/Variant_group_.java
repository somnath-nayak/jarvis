package com.swiggy.api.erp.cms.pojo.BaseServiceBulkItems;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.erp.cms.pojo.BaseServiceBulkItems
 **/

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "external_group_id",
        "group_id",
        "name",
        "order",
        "variations"
})
public class Variant_group_ {

    @JsonProperty("external_group_id")
    private String external_group_id;
    @JsonProperty("group_id")
    private String group_id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("order")
    private Integer order;
    @JsonProperty("variations")
    private List<Variation__> variations = null;

    @JsonProperty("external_group_id")
    public String getExternal_group_id() {
        return external_group_id;
    }

    @JsonProperty("external_group_id")
    public void setExternal_group_id(String external_group_id) {
        this.external_group_id = external_group_id;
    }

    @JsonProperty("group_id")
    public String getGroup_id() {
        return group_id;
    }

    @JsonProperty("group_id")
    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("order")
    public Integer getOrder() {
        return order;
    }

    @JsonProperty("order")
    public void setOrder(Integer order) {
        this.order = order;
    }

    @JsonProperty("variations")
    public List<Variation__> getVariations() {
        return variations;
    }

    @JsonProperty("variations")
    public void setVariations(List<Variation__> variations) {
        this.variations = variations;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("external_group_id", external_group_id).append("group_id", group_id).append("name", name).append("order", order).append("variations", variations).toString();
    }

}

