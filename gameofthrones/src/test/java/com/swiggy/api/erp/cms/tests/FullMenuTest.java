package com.swiggy.api.erp.cms.tests;


import com.swiggy.api.erp.cms.constants.MenuConstants;
import com.swiggy.api.erp.cms.dp.FullMenuDp;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import com.swiggy.api.erp.cms.helper.FullMenuHelper;
import com.swiggy.api.erp.cms.pojo.FullMenu.Items;

import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;

import java.io.IOException;
import java.util.List;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

/**
 * Created by kiran.j on 2/2/18.
 */
public class FullMenuTest extends FullMenuDp {

    @Test(dataProvider = "negativeentity", description = "Verifies Negative Cases for Entity")
    public void negativeEntityCases(String rest_id, String json_payload, MenuConstants.negativejson check) {
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        SoftAssert softAssert = new SoftAssert();
        Processor processor = fullMenuHelper.createFullMenu(rest_id, json_payload);
        softAssert.assertTrue(fullMenuHelper.verifyEntityResponse(processor, check));
        softAssert.assertAll();
    }

    @Test(dataProvider = "negativeitems", description = "Verifies Negative Cases for Items")
    public void negativeItems(String rest_id, String json) throws Exception{
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        SoftAssert softAssert = new SoftAssert();
        Processor processor = fullMenuHelper.createFullMenu(rest_id, json);
        softAssert.assertTrue(fullMenuHelper.verifyZeroItemsResponse(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("errors").replace("[","").replace("]","")));
        softAssert.assertAll();
    }

    @Test(dataProvider = "pricingcombinations", description = "Verifies Different Cases for Pricing Combinations")
    public void pricingCombinations(String rest_id, String json, List<String> ids,Items[] items,boolean flag) throws Exception {
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        SoftAssert softAssert = new SoftAssert();
        Processor processor = fullMenuHelper.createFullMenu(rest_id, json);
        if(flag==true)
        	  softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive1(rest_id, processor, ids,items,flag));
        else
        softAssert.assertTrue(fullMenuHelper.validateResponse(processor, flag));
        softAssert.assertAll();
    }
    
    @Test(dataProvider = "negativeOnlyCat", description = "Verifies Negative Cases for Categories")
    public void negativeOnlyCat(String rest_id, String json_payload, MenuConstants.negativejson check) throws IOException {
     FullMenuHelper fullMenuHelper = new FullMenuHelper();
     SoftAssert softAssert = new SoftAssert();
     Processor processor = fullMenuHelper.createFullMenu(rest_id, json_payload);
     softAssert.assertTrue(fullMenuHelper.verifyEntityResponse(processor, check));
     softAssert.assertAll();
    }
    
    @Test(dataProvider = "NegativeCategoryIDandNameNull", description = "Verifies Invalid Input Cases for Categories")
    public void NegativeCategoryIDandNameNull(String rest_id, String json_payload,MenuConstants.negativejson check) throws IOException {
     FullMenuHelper fullMenuHelper = new FullMenuHelper();
     System.out.println("retaurant_id"+rest_id+"jsonpayload"+json_payload);
     SoftAssert softAssert = new SoftAssert();
     Processor processor = fullMenuHelper.createFullMenu(rest_id, json_payload);
     softAssert.assertTrue(fullMenuHelper.verifyMainCatForNegative(processor,check));
     softAssert.assertAll();
    }
    
   
    @Test(dataProvider = "pricing", description = "Verifies Different Cases for Pricing")
    public void pricing(String rest_id, String json, List<String> ids,Items[] items,boolean flag)throws Exception {
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        SoftAssert softAssert = new SoftAssert();
        Processor processor = fullMenuHelper.createFullMenu(rest_id, json);
        if(flag==true)
    	  softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive1(rest_id, processor, ids,items,flag));
        else
        	softAssert.assertTrue(fullMenuHelper.validatePricingResponse(processor, flag));
        softAssert.assertAll();
    }

	//need to verify 2 test case which fails 10 th and 20th tc (10 error response message is null)
    @Test(dataProvider = "nullvalues", description = "Verifies Negative Cases for Payload")
    public void negativeCases(String rest_id, String json, String[] errors, List<String> ids,Items[] items,boolean ItemPassOrFail) throws Exception {
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        SoftAssert softAssert = new SoftAssert();
        Processor processor = fullMenuHelper.createFullMenu(rest_id, json);
        if(errors.length == 0)
         softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive1(rest_id, processor, ids,items,ItemPassOrFail));
        else
        softAssert.assertTrue(fullMenuHelper.validateErrors(processor, errors));
        softAssert.assertAll();
    }

	// 1 tc fails should debug
    @Test(dataProvider = "combinationprice", description = "Verifies Different Cases for Combination Pricing")
    public void combintaionPricingshadowtower(String rest_id, String json, String[] errors, List<String> ids,Items[] items,boolean ItemPassOrFail) throws Exception {
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        SoftAssert softAssert = new SoftAssert();
        Processor processor = fullMenuHelper.createFullMenu(rest_id, json);
        if(errors.length == 0)
            softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive1(rest_id, processor, ids,items,ItemPassOrFail));
        else
            softAssert.assertTrue(fullMenuHelper.validateErrors(processor, errors));
        softAssert.assertAll();
    }

    @Test(dataProvider = "imageformat", description = "Verifies Different Cases for Image Format")
    public void imageUpload(String rest_id, String json, String[] errors, List<String> ids,Items[] items,boolean ItemPassOrFail) throws Exception {
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        SoftAssert softAssert = new SoftAssert();
        Processor processor = fullMenuHelper.createFullMenu(rest_id, json);
        softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive1(rest_id, processor, ids,items,ItemPassOrFail));
        softAssert.assertAll();
    }

    @Test(dataProvider = "itemslots", description = "Verifies Different Cases for Item Slots")
    public void itemSlots(String rest_id, String json, String[] errors, List<String> ids,Items[] items,boolean ItemPassOrFail) throws Exception {
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        SoftAssert softAssert = new SoftAssert();
        Processor processor = fullMenuHelper.createFullMenu(rest_id, json);
        if(errors.length == 0)
            softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive1(rest_id, processor, ids,items,ItemPassOrFail));
        else
        softAssert.assertTrue(fullMenuHelper.validateErrors(processor, errors));
        softAssert.assertAll();
    }

    @Test(dataProvider = "addoncombinations", description = "Verifies Different Cases for Addons Combinations")
    public void addOnCombinations(String rest_id, String json, String[] errors, List<String> ids,Items[] items,boolean ItemPassOrFail) throws Exception {
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        SoftAssert softAssert = new SoftAssert();
        Processor processor = fullMenuHelper.createFullMenu(rest_id, json);
        if(errors.length == 0)
            softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive1(rest_id, processor, ids,items,ItemPassOrFail));
        else
            softAssert.assertTrue(fullMenuHelper.validateErrors(processor, errors));
        softAssert.assertAll();
    }

    @Test(dataProvider = "gstdp", description = "Verifies Different Cases for Gst Details")
    public void gstCombinations(String rest_id, String json, String[] errors, List<String> ids,Items[] items,boolean ItemPassOrFail) throws Exception {
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        SoftAssert softAssert = new SoftAssert();
        Processor processor = fullMenuHelper.createFullMenu(rest_id, json);
        if(errors.length == 0)
            softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive1(rest_id, processor, ids,items,ItemPassOrFail));
        else
            softAssert.assertTrue(fullMenuHelper.validateErrors(processor, errors));
        softAssert.assertAll();
    }

    @Test(dataProvider = "catcombinations",description = "Verifies Different Cases for Category Combinations")
    public void catCombinations(String rest_id, String json, String[] errors, List<String> ids,Items[] items,boolean ItemPassOrFail) throws Exception {
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        SoftAssert softAssert = new SoftAssert();
        Processor processor = fullMenuHelper.createFullMenu(rest_id, json);
        if(errors.length == 0)
            softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive1(rest_id, processor, ids,items,ItemPassOrFail));
        else
            softAssert.assertTrue(fullMenuHelper.validateErrors(processor, errors));
        softAssert.assertAll();
    }

    @Test(dataProvider = "itemcombinations", description = "Verifies Different Cases for Items Combinations")
    public void itemCombinations(String rest_id, String json, String[] errors, List<String> ids,Items[] items,boolean ItemPassOrFail) throws Exception {
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        SoftAssert softAssert = new SoftAssert();
        Processor processor = fullMenuHelper.createFullMenu(rest_id, json);
        if(errors.length == 0)
            softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive1(rest_id, processor, ids,items,ItemPassOrFail));
        else
            softAssert.assertTrue(fullMenuHelper.validateErrors(processor, errors));
        softAssert.assertAll();
    }
    
    //to be validated
	@Test(dataProvider = "NegativeItemWithVariantsPricingComboWithPricewithoutVariant")
    public void NegativeItemWithVariantsPricingComboWithPricewithoutVariant(String rest_id, String json_payload, String[] errors, List<String> ids,Items[] items,boolean ItemPassOrFail) throws Exception {
		  FullMenuHelper fullMenuHelper = new FullMenuHelper();
	        SoftAssert softAssert = new SoftAssert();
	        Processor processor = fullMenuHelper.createFullMenu(rest_id, json_payload);
	        if(errors.length == 0)
	            softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive1(rest_id, processor, ids,items,ItemPassOrFail));
	        else
	            softAssert.assertTrue(fullMenuHelper.validateErrors(processor, errors));
	        
	        softAssert.assertAll();
	    }
    
	//Bug need to be rectified 
    @Test(dataProvider = "NegativeItemsWithVGwithoutV", description = "Items with variant group without variants(NULL)")
    public void NegativeItemsWithVGwithoutV(String rest_id, String json,String[] errors, List<String> ids,Items[] items,boolean ItemPassOrFail) throws Exception {
    	 FullMenuHelper fullMenuHelper = new FullMenuHelper();
         SoftAssert softAssert = new SoftAssert();
         Processor processor = fullMenuHelper.createFullMenu(rest_id, json);
         if(errors.length == 0)
             softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive1(rest_id, processor, ids,items,ItemPassOrFail));
         else
         softAssert.assertTrue(fullMenuHelper.validateErrors(processor, errors));
         softAssert.assertAll();
    }
  //Bug has been reported should be rectified
	@Test(dataProvider = "NegativeItemsWithAGwithoutA", description = "Items with Addon group without Addons(NULL)")
    public void NegativeItemsWithAGwithoutA(String rest_id, String json,String[] errors, List<String> ids,Items[] items,boolean ItemPassOrFail) throws Exception {
    	 FullMenuHelper fullMenuHelper = new FullMenuHelper();
         SoftAssert softAssert = new SoftAssert();
         Processor processor = fullMenuHelper.createFullMenu(rest_id, json);
         if(errors.length == 0)
             softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive1(rest_id, processor, ids,items,ItemPassOrFail));
         else
         softAssert.assertTrue(fullMenuHelper.validateErrors(processor, errors));
         softAssert.assertAll();
    }

    //error message to be validated
    @Test(dataProvider = "NegativeItemsWithAddonsNullinPc", description = "pricing module with olnly variants and Addons null")
    public void NegativeItemsWithVariantsAndAddonsNullinPc(String rest_id, String json_payload,String[] errors, List<String> ids,Items[] items,boolean ItemPassOrFail) throws Exception {
    	   FullMenuHelper fullMenuHelper = new FullMenuHelper();
           SoftAssert softAssert = new SoftAssert();
           Processor processor = fullMenuHelper.createFullMenu(rest_id, json_payload);
           if(errors.length == 0)
               softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive1(rest_id, processor, ids,items,ItemPassOrFail));
           else
               softAssert.assertTrue(fullMenuHelper.validateError(processor, errors));
           softAssert.assertAll();
    }
    
    
    @Test(dataProvider = "Multiple_variant_cases", description = "Variants default cases and default dependent cases")
    public void Multiple_variant_cases(String rest_id, String json_payload,String[] errors, List<String> ids,Items[] items,boolean ItemPassOrFail) throws Exception {
    	   FullMenuHelper fullMenuHelper = new FullMenuHelper();
           SoftAssert softAssert = new SoftAssert();
           Processor processor = fullMenuHelper.createFullMenu(rest_id, json_payload);
           if(errors.length == 0)
               softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive1(rest_id, processor, ids,items,ItemPassOrFail));
           else
               softAssert.assertTrue(fullMenuHelper.validateErrors(processor, errors));
           softAssert.assertAll();
    }
    
    
    @Test(dataProvider = "Multiple_Addon_cases", description = "Addon default dependent  cases")
    public void Multiple_Addon_cases(String rest_id, String json_payload,String[] errors, List<String> ids,Items[] items,boolean ItemPassOrFail) throws Exception {
    	   FullMenuHelper fullMenuHelper = new FullMenuHelper();
           SoftAssert softAssert = new SoftAssert();
           Processor processor = fullMenuHelper.createFullMenu(rest_id, json_payload);
           if(errors.length == 0)
               softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive1(rest_id, processor, ids,items,ItemPassOrFail));
           else
               softAssert.assertTrue(fullMenuHelper.validateErrors(processor, errors));
           softAssert.assertAll();
    }
  
    @Test
    public void check() throws IOException {
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        JsonHelper jsonHelper = new JsonHelper();
        System.out.println(jsonHelper.getObjectToJSON(fullMenuHelper.createDefaultFullMenuPojo()));
        //System.out.println(jsonHelper.getObjectToJSON(fullMenuHelper.createFullMenuPojo("Cat_id", "Subcat_id", "Item_id",3, 2, 1, 2, 100)));
    }

    @Test
    public void t() {
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        CMSHelper cmsHelper = new CMSHelper();
        System.out.println(cmsHelper.getFullMenuItems("KF0001"));
        System.out.println(cmsHelper.getFullMenuSubcategory("AUTO_CM_id1", "KF0001"));
        System.out.println(cmsHelper.getFullMenuCategory("AUTO_CM_id1", "KF0001"));
    }
}