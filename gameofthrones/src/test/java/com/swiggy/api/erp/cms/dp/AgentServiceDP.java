package com.swiggy.api.erp.cms.dp;

import java.util.Date;

import org.apache.poi.ddf.EscherColorRef.SysIndexSource;
import org.testng.annotations.DataProvider;

import com.swiggy.api.erp.cms.constants.CmsConstants;
import com.swiggy.api.erp.cms.constants.SelfServeConstants;
import com.swiggy.api.erp.cms.helper.SelfServeHelper;


public class AgentServiceDP {
	
	 Date date = new Date();
     String authKey ="AgentAuthKey"+date.getTime();
     
    @DataProvider(name = "AgentServiceDP")
	    public Object[][] agentCreateDP() {
    return new Object[][]{
	        	{SelfServeConstants.agentType,SelfServeConstants.authUserId,authKey,SelfServeConstants.app_secret,1},
	        	{SelfServeConstants.agentType,SelfServeConstants.authUserId,authKey,null,0},
	           	{SelfServeConstants.agentType,0,authKey,null,0},
	        	{null,SelfServeConstants.authUserId,authKey,SelfServeConstants.app_secret,0},
	        	
	  	      
	        //	{SelfServeConstants.agentType,SelfServeConstants.authUserId,null,SelfServeConstants.app_secret,0}
	        };
	    }
    
    @DataProvider(name = "agentAvailabelUnavailable")
    public Object[][] agentAvailabelUavailable() {
     
    	String authUserId=SelfServeHelper.authUerIdforEnableDisable();
     
    	return new Object[][]{
        	{authUserId,true,SelfServeHelper.authAuthKeyforEnableDisable(authUserId),SelfServeConstants.app_secret,1},
     	
       };
    }
}
