package com.swiggy.api.erp.cms.pojo.BaseServiceBulkAddon;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.erp.cms.pojo.BaseServiceBulkAddon
 **/

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.swiggy.api.erp.cms.helper.BaseServiceHelper;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "active",
        "addon_group_ids",
        "created_by",
        "in_stock",
        "inc_price",
        "is_veg",
        "name",
        "order",
        "price",
        "restaurant_id",
        "third_party_id"
})
public class Addon_Bulk {

    BaseServiceHelper baseServiceHelper = new BaseServiceHelper();
    private String timestamp = baseServiceHelper.getTimeStampRandomised();

    @JsonProperty("active")
    private Boolean active;
    @JsonProperty("addon_group_ids")
    private List<Integer> addon_group_ids = null;
    @JsonProperty("created_by")
    private String created_by;
    @JsonProperty("in_stock")
    private Boolean in_stock;
    @JsonProperty("inc_price")
    private Integer inc_price;
    @JsonProperty("is_veg")
    private Boolean is_veg;
    @JsonProperty("name")
    private String name;
    @JsonProperty("order")
    private Integer order;
    @JsonProperty("price")
    private Double price;
    @JsonProperty("restaurant_id")
    private Integer restaurant_id;
    @JsonProperty("third_party_id")
    private String third_party_id;
    @JsonProperty("id")
    private String id;

    @JsonProperty("active")
    public Boolean getActive() {
        return active;
    }

    @JsonProperty("active")
    public void setActive(Boolean active) {
        this.active = active;
    }

    @JsonProperty("addon_group_ids")
    public List<Integer> getAddon_group_ids() {
        return addon_group_ids;
    }

    @JsonProperty("addon_group_ids")
    public void setAddon_group_ids(List<Integer> addon_group_ids) {
        this.addon_group_ids = addon_group_ids;
    }

    @JsonProperty("created_by")
    public String getCreated_by() {
        return created_by;
    }

    @JsonProperty("created_by")
    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    @JsonProperty("in_stock")
    public Boolean getIn_stock() {
        return in_stock;
    }

    @JsonProperty("in_stock")
    public void setIn_stock(Boolean in_stock) {
        this.in_stock = in_stock;
    }

    @JsonProperty("inc_price")
    public Integer getInc_price() {
        return inc_price;
    }

    @JsonProperty("inc_price")
    public void setInc_price(Integer inc_price) {
        this.inc_price = inc_price;
    }

    @JsonProperty("is_veg")
    public Boolean getIs_veg() {
        return is_veg;
    }

    @JsonProperty("is_veg")
    public void setIs_veg(Boolean is_veg) {
        this.is_veg = is_veg;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("order")
    public Integer getOrder() {
        return order;
    }

    @JsonProperty("order")
    public void setOrder(Integer order) {
        this.order = order;
    }

    @JsonProperty("price")
    public Double getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(Double price) {
        this.price = price;
    }

    @JsonProperty("restaurant_id")
    public Integer getRestaurant_id() {
        return restaurant_id;
    }

    @JsonProperty("restaurant_id")
    public void setRestaurant_id(Integer restaurant_id) {
        this.restaurant_id = restaurant_id;
    }

    @JsonProperty("third_party_id")
    public String getThird_party_id() {
        return third_party_id;
    }

    @JsonProperty("third_party_id")
    public void setThird_party_id(String third_party_id) {
        this.third_party_id = third_party_id;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("third_party_id")
    public void setId(String id) {
        this.id = id;
    }


    public void setDefaultValues(int restID) {
        if(this.getIn_stock() == null)
            this.setIn_stock(true);
        if(this.getIs_veg() == null)
            this.setIs_veg(true);
        if(this.getRestaurant_id() == null)
            this.setRestaurant_id(restID);
        if(this.getThird_party_id() == null)
            this.setThird_party_id("Automation_"+timestamp);
        if(this.getName() == null)
            this.setName("Automation_BaseService_"+timestamp);
        if(this.getActive() == null)
            this.setActive(true);
        if(this.getOrder() == null)
            this.setOrder(0);
        if(this.getPrice() == null)
            this.setPrice(100.0);
        if(this.getCreated_by() == null )
            this.setCreated_by("Automation_BaseService_Suite");
        if(this.getAddon_group_ids() == null)
            this.setAddon_group_ids(new ArrayList<>());

    }

    public Addon_Bulk build(int restID) {
        setDefaultValues(restID);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("active", active).append("addon_group_ids", addon_group_ids).append("created_by", created_by).append("in_stock", in_stock).append("inc_price", inc_price).append("is_veg", is_veg).append("name", name).append("order", order).append("price", price).append("restaurant_id", restaurant_id).append("third_party_id", third_party_id).toString();
    }

}
