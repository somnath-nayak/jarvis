package com.swiggy.api.erp.cms.pojo.SelfServeUpdateItem;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.erp.cms.pojo.SelfServeUpdateItem
 **/
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.swiggy.api.erp.cms.helper.BaseServiceHelper;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "description",
        "in_stock",
        "is_perishable",
        "is_spicy",
        "is_veg",
        "name",
        "packing_charges",
        "packing_slab_count",
        "price",
        "serves_how_many",
        "service_charges",
        "gst_details"
})
public class UpdateItem {

    BaseServiceHelper baseServiceHelper = new BaseServiceHelper();

    @JsonProperty("description")
    private String description;
    @JsonProperty("in_stock")
    private Integer in_stock;
    @JsonProperty("is_perishable")
    private Integer is_perishable;
    @JsonProperty("is_spicy")
    private Integer is_spicy;
    @JsonProperty("is_veg")
    private Integer is_veg;
    @JsonProperty("name")
    private String name;
    @JsonProperty("packing_charges")
    private Integer packing_charges;
    @JsonProperty("packing_slab_count")
    private Integer packing_slab_count;
    @JsonProperty("price")
    private Integer price;
    @JsonProperty("serves_how_many")
    private Integer serves_how_many;
    @JsonProperty("service_charges")
    private String service_charges;
    @JsonProperty("gst_details")
    private Gst_details gst_details;

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("in_stock")
    public Integer getIn_stock() {
        return in_stock;
    }

    @JsonProperty("in_stock")
    public void setIn_stock(Integer in_stock) {
        this.in_stock = in_stock;
    }

    @JsonProperty("is_perishable")
    public Integer getIs_perishable() {
        return is_perishable;
    }

    @JsonProperty("is_perishable")
    public void setIs_perishable(Integer is_perishable) {
        this.is_perishable = is_perishable;
    }

    @JsonProperty("is_spicy")
    public Integer getIs_spicy() {
        return is_spicy;
    }

    @JsonProperty("is_spicy")
    public void setIs_spicy(Integer is_spicy) {
        this.is_spicy = is_spicy;
    }

    @JsonProperty("is_veg")
    public Integer getIs_veg() {
        return is_veg;
    }

    @JsonProperty("is_veg")
    public void setIs_veg(Integer is_veg) {
        this.is_veg = is_veg;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("packing_charges")
    public Integer getPacking_charges() {
        return packing_charges;
    }

    @JsonProperty("packing_charges")
    public void setPacking_charges(Integer packing_charges) {
        this.packing_charges = packing_charges;
    }

    @JsonProperty("packing_slab_count")
    public Integer getPacking_slab_count() {
        return packing_slab_count;
    }

    @JsonProperty("packing_slab_count")
    public void setPacking_slab_count(Integer packing_slab_count) {
        this.packing_slab_count = packing_slab_count;
    }

    @JsonProperty("price")
    public Integer getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(Integer price) {
        this.price = price;
    }

    @JsonProperty("serves_how_many")
    public Integer getServes_how_many() {
        return serves_how_many;
    }

    @JsonProperty("serves_how_many")
    public void setServes_how_many(Integer serves_how_many) {
        this.serves_how_many = serves_how_many;
    }

    @JsonProperty("service_charges")
    public String getService_charges() {
        return service_charges;
    }

    @JsonProperty("service_charges")
    public void setService_charges(String service_charges) {
        this.service_charges = service_charges;
    }

    @JsonProperty("gst_details")
    public Gst_details getGst_details() {
        return gst_details;
    }

    @JsonProperty("gst_details")
    public void setGst_details(Gst_details gst_details) {
        this.gst_details = gst_details;
    }

    private void setDefaultValues() {
        Gst_details gst_details = new Gst_details();

        if (this.getDescription() == null)
            this.setDescription("SelfServe Automation Item Update");
        if (this.getIn_stock() == null)
            this.setIn_stock(1);
        if (this.getIs_perishable() == null)
            this.setIs_perishable(0);
        if (this.getIs_spicy() == null)
            this.setIs_spicy(1);
        if (this.getIs_veg() == null)
            this.setIs_veg(1);
        if (this.getName() == null)
            this.setName("New_SelfServe_"+baseServiceHelper.getTimeStampRandomised());
        if (this.getPacking_charges() == null)
            this.setPacking_charges(30);
        if (this.getPacking_slab_count() == null)
            this.setPacking_slab_count(5);
        if (this.getPrice() == null)
            this.setPrice(250);
        if (this.getServes_how_many() ==  null)
            this.setServes_how_many(2);
        if (this.getService_charges() == null)
            this.setService_charges("20");
        if (this.getGst_details() == null)
            this.setGst_details(gst_details.build());
    }

    public UpdateItem build() {
        setDefaultValues();
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("description", description).append("in_stock", in_stock).append("is_perishable", is_perishable).append("is_spicy", is_spicy).append("is_veg", is_veg).append("name", name).append("packing_charges", packing_charges).append("packing_slab_count", packing_slab_count).append("price", price).append("serves_how_many", serves_how_many).append("service_charges", service_charges).append("gst_details", gst_details).toString();
    }
}
