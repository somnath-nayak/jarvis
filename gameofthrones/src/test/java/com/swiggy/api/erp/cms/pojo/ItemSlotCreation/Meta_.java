
package com.swiggy.api.erp.cms.pojo.ItemSlotCreation;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "user"
})
public class Meta_ {

    @JsonProperty("user")
    private String user;


    @JsonProperty("user")
    public String getUser() {
        return user;
    }

    @JsonProperty("user")
    public void setUser(String user) {
        this.user = user;
    }

    public Meta_ build() {
        setDefaultValues();
        return this;
    }

    public void setDefaultValues(){
        if(this.user==null){
            this.user = "CMS-AutomationUserMeta";
        }
    }

}
