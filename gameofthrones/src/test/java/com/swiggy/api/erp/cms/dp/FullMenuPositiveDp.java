package com.swiggy.api.erp.cms.dp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.swiggy.api.erp.cms.helper.CMSHelper;
import com.swiggy.api.erp.cms.helper.FullMenuHelper;

import org.testng.annotations.DataProvider;

import com.swiggy.api.erp.cms.constants.MenuConstants;
import com.swiggy.api.erp.cms.helper.FullMenuHelper;
import com.swiggy.api.erp.cms.pojo.FullMenu.Addon_Groups;
import com.swiggy.api.erp.cms.pojo.FullMenu.Addon_combination;
import com.swiggy.api.erp.cms.pojo.FullMenu.Addons;
import com.swiggy.api.erp.cms.pojo.FullMenu.Entity;
import com.swiggy.api.erp.cms.pojo.FullMenu.FullMenuPojo;
import com.swiggy.api.erp.cms.pojo.FullMenu.Gst_Details;
import com.swiggy.api.erp.cms.pojo.FullMenu.Item_Slots;
import com.swiggy.api.erp.cms.pojo.FullMenu.Items;
import com.swiggy.api.erp.cms.pojo.FullMenu.Main_categories;
import com.swiggy.api.erp.cms.pojo.FullMenu.Pricing_combinations;
import com.swiggy.api.erp.cms.pojo.FullMenu.Sub_categories;
import com.swiggy.api.erp.cms.pojo.FullMenu.Variant_groups;
import com.swiggy.api.erp.cms.pojo.FullMenu.Variants;
import com.swiggy.api.erp.cms.pojo.Items.Gst_details;
import com.swiggy.api.erp.cms.pojo.VariantGroups.VariantGroup;

import framework.gameofthrones.Daenerys.stafdata;
import framework.gameofthrones.Tyrion.JsonHelper;

/**
 * Created by HemaGovinadaraj on 14/2/18.
 */
public class FullMenuPositiveDp {
	@DataProvider(name = "positiveItemsMaptoSubCat")
    public Iterator<Object[]>  positiveItemsMaptoSubCat() throws IOException {
    	 List<Object[]> obj = new ArrayList<>();
      	 FullMenuHelper fullMenuHelper = new FullMenuHelper();
         JsonHelper jsonHelper = new JsonHelper();
         CMSHelper cmsHelper = new CMSHelper();
         FullMenuPojo fullMenuPojo = fullMenuHelper.createDefaultFullMenuPojo();
         String rest_id = MenuConstants.rest_id;
         Entity entity = new Entity();
         Main_categories main_categories = new Main_categories();
        Sub_categories subCategories= new Sub_categories();
        Items items = new Items();
        items.build();
        subCategories.build();
        main_categories.build();  
        Main_categories main_categoriesValuesToArray[] = new Main_categories[1];
        Sub_categories Sub_categoriesValuestoArray[] = new Sub_categories[1];
        Items ItemValuesToArray[] = new Items[1];
        main_categoriesValuesToArray[0] = main_categories;
        ItemValuesToArray[0] = items;
        Sub_categoriesValuestoArray[0] = subCategories;      
        main_categories.setSub_categories(Sub_categoriesValuestoArray);
        entity.setItems(ItemValuesToArray);
        entity.setMain_categories(main_categoriesValuesToArray); 
        
        fullMenuPojo.setEntity(entity);
        
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), new String[]{}, fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});
        
        return obj.iterator();
	
	}
	   
    @DataProvider(name = "positiveItemsMaptoCat")
    public Object[][] positiveItemsMaptoCat() throws IOException {
    	 FullMenuHelper fullMenuHelper = new FullMenuHelper();
        FullMenuPojo fullMenuPojo = new FullMenuPojo();
        Entity entity = new Entity();
        Main_categories main_categories = new Main_categories();
        Items items = new Items();
        main_categories.build();
        items.build();
        Main_categories main_categoriesValuesToArray[] = new Main_categories[1];
        main_categoriesValuesToArray[0] = main_categories;
        entity.setMain_categories(main_categoriesValuesToArray); 
        Items ItemValuesToArray[] = new Items[1];
        ItemValuesToArray[0] = items;
        entity.setItems(ItemValuesToArray);
        fullMenuPojo.setEntity(entity);
        JsonHelper jsonHelper = new JsonHelper();
  
        return new Object[][] {{MenuConstants.rest_id, jsonHelper.getObjectToJSON(fullMenuPojo), fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true}};
    }
    
    
    @DataProvider(name = "positiveItemsWithVarinats")
    public Object[][] positiveItemsWithVarinats() throws IOException {
    	 FullMenuHelper fullMenuHelper = new FullMenuHelper();
    	 FullMenuPojo fullMenuPojo = new FullMenuPojo();
         Entity entity = new Entity();
         Main_categories main_categories = new Main_categories();
         Sub_categories subCategories= new Sub_categories();
         Items items = new Items();
         items.build();
         Variant_groups vg=new Variant_groups();
         Variants variant=new Variants();
         variant.build();
         vg.build();
         subCategories.build();
         main_categories.build();  
         Main_categories main_categoriesValuesToArray[] = new Main_categories[1];
         Sub_categories Sub_categoriesValuestoArray[] = new Sub_categories[1];
         Items ItemValuesToArray[] = new Items[1];
         Variant_groups vgValuesToArray[] = new Variant_groups[1];
         Variants variantValuesToArray[] = new Variants[1];
         
         main_categoriesValuesToArray[0] = main_categories;
         Sub_categoriesValuestoArray[0] = subCategories;   
         ItemValuesToArray[0] = items;
         vgValuesToArray[0]=vg;
         variantValuesToArray[0]=variant;   
        

         vg.setVariants(variantValuesToArray);
         items.setVariant_groups(vgValuesToArray);
         entity.setItems(ItemValuesToArray);
         entity.setMain_categories(main_categoriesValuesToArray);
         main_categories.setSub_categories(Sub_categoriesValuestoArray);
         fullMenuPojo.setEntity(entity);
         JsonHelper jsonHelper = new JsonHelper();
         return new Object[][] {{MenuConstants.rest_id, jsonHelper.getObjectToJSON(fullMenuPojo), fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true}};
    }
    
    @DataProvider(name = "positiveItemsWithAddons")
    public Object[][] positiveItemsWithAddons() throws IOException {
    	 FullMenuHelper fullMenuHelper = new FullMenuHelper();
    	 FullMenuPojo fullMenuPojo = new FullMenuPojo();
         Entity entity = new Entity();
         Main_categories main_categories = new Main_categories();
         Sub_categories subCategories= new Sub_categories();
         Variant_groups Vg=new Variant_groups();
         Pricing_combinations pricingombo=new Pricing_combinations();
         pricingombo.setVariant_combination(null);
         pricingombo.setAddon_combination(null);
         Vg.setVariants(null);
         Items items = new Items();
         items.build();
         subCategories.build();
         main_categories.build();  
         Main_categories main_categoriesValuesToArray[] = new Main_categories[1];
         Sub_categories Sub_categoriesValuestoArray[] = new Sub_categories[1];
         Items ItemValuesToArray[] = new Items[1];
       
         main_categoriesValuesToArray[0] = main_categories;
         Sub_categoriesValuestoArray[0] = subCategories;   
         ItemValuesToArray[0] = items;
      
         entity.setMain_categories(main_categoriesValuesToArray);
         main_categories.setSub_categories(Sub_categoriesValuestoArray);
         entity.setItems(ItemValuesToArray);
         fullMenuPojo.setEntity(entity);
         JsonHelper jsonHelper = new JsonHelper();
         return new Object[][] {{MenuConstants.rest_id, jsonHelper.getObjectToJSON(fullMenuPojo), fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true}};
    }
    
    @DataProvider(name = "positiveItemsWithVariantsAndPricingCombination")
    public Object[][] positiveItemsWithVariantsAndPricingCombination() throws IOException {
    	 FullMenuHelper fullMenuHelper = new FullMenuHelper();
    	FullMenuPojo fullMenuPojo = new FullMenuPojo();
        Entity entity = new Entity();
        Main_categories main_categories = new Main_categories();
        Sub_categories subCategories= new Sub_categories();
        Items items = new Items();
        Addon_Groups ag=new Addon_Groups();
        Pricing_combinations pc= new Pricing_combinations();
        pc.setAddon_combination(null);
        pc.build();
        items.build();
        ag.setAddons(null);
        subCategories.build();
        main_categories.build();  
        Main_categories main_categoriesValuesToArray[] = new Main_categories[1];
        Sub_categories Sub_categoriesValuestoArray[] = new Sub_categories[1];
        Items ItemValuesToArray[] = new Items[1];
        main_categoriesValuesToArray[0] = main_categories;
        Sub_categoriesValuestoArray[0] = subCategories;   
        ItemValuesToArray[0] = items;
        entity.setMain_categories(main_categoriesValuesToArray);
        main_categories.setSub_categories(Sub_categoriesValuestoArray);
        entity.setItems(ItemValuesToArray);
        fullMenuPojo.setEntity(entity);
        JsonHelper jsonHelper = new JsonHelper();
        return new Object[][] {{MenuConstants.rest_id, jsonHelper.getObjectToJSON(fullMenuPojo), fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true}};
    }
    
    
    @DataProvider(name = "positiveItemsWithVariantsAndPCwithAAndV")
    public Object[][] positiveItemsWithVariantsAndPCwithAAndV() throws IOException {
        JsonHelper jsonHelper = new JsonHelper();
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        FullMenuPojo fullMenuPojo = fullMenuHelper.createDefaultFullMenuPojo();
        System.out.println(jsonHelper.getObjectToJSON(fullMenuPojo));
        
        return new Object[][] {{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), fullMenuHelper.getItemIds(fullMenuPojo.getEntity().getItems()),fullMenuPojo.getEntity().getItems(),true}};
    }
    
    @DataProvider(name = "positiveItemsWithGST")
    public Object[][] positiveItemsWithGST() throws IOException {
        JsonHelper jsonHelper = new JsonHelper();
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        FullMenuPojo fullMenuPojo = fullMenuHelper.createDefaultFullMenuPojo();
        Entity entity = fullMenuPojo.getEntity();
        Items items = entity.getItems()[0];
        Gst_Details gst=items.getGst_details();
        gst.build();
        items.setGst_details(gst);
        entity.setItems(new Items[]{items});
        fullMenuPojo.setEntity(entity);
        return new Object[][] {{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true}};
    }
    
    @DataProvider(name = "positiveItemsWithVariantGST")
    public Object[][] positiveItemsWithVariantGST() throws IOException {
        JsonHelper jsonHelper = new JsonHelper();
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        FullMenuPojo fullMenuPojo = fullMenuHelper.createDefaultFullMenuPojo();
        Entity entity = fullMenuPojo.getEntity();
        Items items = entity.getItems()[0];
        Variant_groups Vg=items.getVariant_groups()[0];
        Variants v=Vg.getVariants()[0];
        Gst_Details gst=v.getGst_details();
        gst.setCgst(2.1);
        gst.setIgst(2.1);
        gst.setSgst(2.2);
        gst.setInclusive(true);
        v.setGst_details(gst);
        Vg.setVariants(new Variants[] {v});
        items.setVariant_groups(new Variant_groups[] {Vg});
        entity.setItems(new Items[]{items});
        fullMenuPojo.setEntity(entity);
        return new Object[][] {{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true}};
    }
    

    
    @DataProvider(name = "positiveItemsWithItemSlots")
    public Object[][] positiveItemsWithItemSlots() throws IOException {
        JsonHelper jsonHelper = new JsonHelper();
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        FullMenuPojo fullMenuPojo = fullMenuHelper.createDefaultFullMenuPojo();
        Entity entity = fullMenuPojo.getEntity();
        Items items = entity.getItems()[0];
        Item_Slots iSlots=items.getItem_slots()[0];
        iSlots.setDay_of_week(1);
        iSlots.setOpen_time(400);
        iSlots.setClose_time(900);
        items.setItem_slots(new Item_Slots[] {iSlots});
        entity.setItems(new Items[]{items});
        fullMenuPojo.setEntity(entity);
        return new Object[][] {{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true}};
    }
   
    //to be validated
    @DataProvider(name = "positiveItemsWithVariantsAndAddonsNullinPc")
    public Object[][] positiveItemsWithVariantsAndAddonsNullinPc() throws IOException {
       JsonHelper jsonHelper = new JsonHelper();
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        FullMenuPojo fullMenuPojo = fullMenuHelper.createDefaultFullMenuPojo();
        Entity entity = fullMenuPojo.getEntity();
        Items items = entity.getItems()[0];
        Pricing_combinations pricing_combinations = items.getPricing_combinations()[0];
        pricing_combinations.setAddon_combination(null);
        items.setPricing_combinations(new Pricing_combinations[] {pricing_combinations});
        entity.setItems(new Items[]{items});
        fullMenuPojo.setEntity(entity);
     return new Object[][] {{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo),fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true}};
    }
    

    
    @DataProvider(name = "Cat_SubCat_cases")
    public Iterator<Object[]>  Cat_SubCat_cases() throws IOException {
    	 List<Object[]> obj = new ArrayList<>();
         FullMenuHelper fullMenuHelper = new FullMenuHelper();
         JsonHelper jsonHelper = new JsonHelper();
         FullMenuPojo fullMenuPojo = fullMenuHelper.createDefaultFullMenuPojo();
         Entity entity = fullMenuPojo.getEntity();
         Items items = new Items();
         items.build();
         items.setCategory_id("category_id_differ_frm_menu_categories_cat");
         entity.setItems(new Items[]{items});
         fullMenuPojo.setEntity(entity);
         System.out.println("Detailed level Case Failed");
         obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), new String[]{}, fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),false });

         
         Main_categories main_categories = new Main_categories();
         Sub_categories subCategories= new Sub_categories();
         items = new Items();
         items.build();
         subCategories.build();
         main_categories.build();  
         Main_categories main_categoriesValuesToArray[] = new Main_categories[2];
         Sub_categories Sub_categoriesValuestoArray[] = new Sub_categories[1];
         Items ItemValuesToArray[] = new Items[1];
         main_categories.setId(fullMenuHelper.generateId());
         main_categoriesValuesToArray[0] = main_categories;
         ItemValuesToArray[0] = items;
         
         main_categories=new Main_categories();
         main_categoriesValuesToArray[1] = main_categories;
         main_categories.build();
         subCategories= new Sub_categories();
         subCategories.build();
         subCategories.setId(fullMenuHelper.generateId());
         Sub_categoriesValuestoArray[0] = subCategories;  
         main_categories.setSub_categories(Sub_categoriesValuestoArray);
         entity.setItems(ItemValuesToArray);
         entity.setMain_categories(main_categoriesValuesToArray); 
       
      obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(fullMenuPojo), new String[]{}, fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),false});
         return obj.iterator();
         
         
    }

    
    @DataProvider(name = "PricingCombiantionTestCases")
    public Iterator<Object[]>  PricingCombiantionTestCases() throws IOException {
    	 
    	 List<Object[]> obj = new ArrayList<>();
         FullMenuHelper fullMenuHelper = new FullMenuHelper();
         JsonHelper jsonHelper = new JsonHelper();
         FullMenuPojo fullMenuPojo = fullMenuHelper.createDefaultFullMenuPojo();
         Entity entity = fullMenuPojo.getEntity();
        Items items = new Items();
         items.build();
         Pricing_combinations pc=new Pricing_combinations();
         pc.build();
         pc.setAddon_combination(null);
         items.setPricing_combinations(new Pricing_combinations[] {pc});
         entity.setItems(new Items[]{items});
         fullMenuPojo.setEntity(entity);
         obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo),  MenuConstants.addonFLagNullInPc,fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),false});
        
     	 items = new Items();
         items.build();
         pc=new Pricing_combinations();
         pc.build();
         
         pc.setAddon_combination(null);
         items.setPricing_combinations(new Pricing_combinations[] {pc});
         entity.setItems(new Items[]{items});
         fullMenuPojo.setEntity(entity);
          obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo),  MenuConstants.addonFLagNullInPc,fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),false});
       
              
         fullMenuHelper = new FullMenuHelper();
         fullMenuPojo = fullMenuHelper.createFullMenuPojo("Cat_id", "Subcat_id", "Item_id",2, 2, 2, 3, 100);
         obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo),new String[]{},fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});
         
         
         FullMenuHelper fullMenuHelper2 = new FullMenuHelper();
         fullMenuPojo = fullMenuHelper2.createFullMenuPojo("Cat_id", "Subcat_id", "Item_id",3, 3, 3, 3, 100);
         obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo),new String[]{},fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});
         
         fullMenuHelper2 = new FullMenuHelper();
         fullMenuPojo = fullMenuHelper2.createFullMenuPojo("Cat_id", "Subcat_id", "Item_id",1, 1, 0, 0, 100);
         entity = fullMenuPojo.getEntity();
         items = entity.getItems()[0];
         items.build();
         items.getVariant_groups();
         Variant_groups vg=new Variant_groups();
         vg.build();
         Variants v=new Variants();
         v.build();
         v.setDefault_dependent_variant_group_id(null);
         v.setDefault_dependent_variant_id(null);
         vg.setVariants(new Variants[] {v});
         items.setVariant_groups(new Variant_groups[] {vg});
         entity.setItems(new Items[] {items});
         fullMenuPojo.setEntity(entity);
    //one varinat group one variant without default dependent variant  success
         
         obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo),new String[]{},fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});
         
         fullMenuHelper2 = new FullMenuHelper();
         fullMenuPojo = fullMenuHelper2.createFullMenuPojo("Cat_id", "Subcat_id", "Item_id",1, 3, 0, 0, 100);
         entity = fullMenuPojo.getEntity();
         items = entity.getItems()[0];
         items.build();
         items.getVariant_groups();
         vg=new Variant_groups();
         vg.build();
         v=new Variants();
         v.build();
         v.setDefault_dependent_variant_group_id(null);
         v.setDefault_dependent_variant_id(null);
         vg.setVariants(new Variants[] {v});
         items.setVariant_groups(new Variant_groups[] {vg});
         entity.setItems(new Items[] {items});
         fullMenuPojo.setEntity(entity);
        //one varinat group multiple variant without default dependent variant and default dependent variant group
         
         obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo),new String[]{},fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});
         
         fullMenuHelper2 = new FullMenuHelper();
         fullMenuPojo = fullMenuHelper2.createFullMenuPojo("Cat_id", "Subcat_id", "Item_id",2, 1, 0, 0, 100);
         entity = fullMenuPojo.getEntity();
         items = entity.getItems()[0];
         items.build();
         items.getVariant_groups();
         vg=new Variant_groups();
         vg.build();
         v=new Variants();
         v.build();
         v.setDefault_dependent_variant_group_id(null);
         v.setDefault_dependent_variant_id(null);
         vg.setVariants(new Variants[] {v});
         items.setVariant_groups(new Variant_groups[] {vg});
         entity.setItems(new Items[] {items});
         fullMenuPojo.setEntity(entity);
        //2varinat  group one variant  weach without default dependent variant and default dependent variant group
         
         obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), new String[]{},fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});
         
       return obj.iterator();
         
         
         
         
         
    }
    
    

    @DataProvider(name = "GstTestCases")
    public Iterator<Object[]>  GstTestCases() throws IOException {
    	 
    	 List<Object[]> obj = new ArrayList<>();
         FullMenuHelper fullMenuHelper = new FullMenuHelper();
         JsonHelper jsonHelper = new JsonHelper();
         FullMenuPojo fullMenuPojo = fullMenuHelper.createDefaultFullMenuPojo();
         Entity entity = fullMenuPojo.getEntity();
         Items items = new Items();
         items.build();
         Gst_Details gst=new Gst_Details();
         gst.setCgst(5);
         gst.setSgst(15.4);
         gst.setIgst(5.9);
         items.setGst_details(gst);
         entity.setItems(new Items[]{items});
         fullMenuPojo.setEntity(entity);
      obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), new String[]{}, fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});
        
         items = new Items();
         items.build();
        gst.setCgst(300);
        gst.setSgst(400);
        gst.setIgst(77.56);
        items.setGst_details(gst);
        entity.setItems(new Items[]{items});
        fullMenuPojo.setEntity(entity);
      obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo),MenuConstants.GstError, fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),false});
       
        
        items = new Items();
        items.build();
        Variant_groups vg=new Variant_groups();
        Variants v= new Variants();
        vg.build();
        v.build();
        v.setGst_details(gst);
        vg.setVariants(new Variants[] {v});
        items.setVariant_groups(new Variant_groups [] {vg});
        entity.setItems(new Items[]{items});
        fullMenuPojo.setEntity(entity);
       obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo),MenuConstants.GstErrorVariant, fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),false});
       
        items = new Items();
        items.build();
        Addon_Groups ag=new Addon_Groups();
        Addons a= new Addons();
        ag.build();
        a.build();
        a.setGst_details(gst);
        ag.setAddons(new Addons[] {a});
        items.setAddon_groups(new Addon_Groups[] {ag});
        entity.setItems(new Items[]{items});
        fullMenuPojo.setEntity(entity);
       obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo),MenuConstants.GstErrorAddon, fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),false});
       
      
       items = new Items();
       items.build();
        items.build();
        vg.build();
        v.build();
        gst=new Gst_Details();
        gst.setCgst(5);
        gst.setSgst(15.4);
        gst.setIgst(5.9);
        v.setGst_details(gst);
        vg.setVariants(new Variants[] {v});
        items.setVariant_groups(new Variant_groups [] {vg});
        entity.setItems(new Items[]{items});
        fullMenuPojo.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), new String[]{}, fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});
       
        
        items = new Items();
        items.build();
        ag=new Addon_Groups();
        a= new Addons();
        ag.build();
        a.build();
        gst=new Gst_Details();
        gst.setCgst(5);
        gst.setSgst(15.4);
        gst.setIgst(5.9);
        v.setGst_details(gst);
        a.setGst_details(gst);
        ag.setAddons(new Addons[] {a});
        items.setAddon_groups(new Addon_Groups[] {ag});
        entity.setItems(new Items[]{items});
        fullMenuPojo.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), new String[]{}, fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});
       
        return obj.iterator();
        
        
        
    }
    @DataProvider(name = "Item_related_multiplecase")
    public Iterator<Object[]>  Item_related_multiplecase() throws IOException {
    	 
    	 List<Object[]> obj = new ArrayList<>();
         FullMenuHelper fullMenuHelper = new FullMenuHelper();
         JsonHelper jsonHelper = new JsonHelper();
         FullMenuPojo fullMenuPojo = fullMenuHelper.createDefaultFullMenuPojo();
         Entity entity = fullMenuPojo.getEntity();
         Items items = new Items();
         items.build();
         items.setAddon_groups(null);
         items.setVariant_groups(null);
         entity.setItems(new Items[]{items});
         fullMenuPojo.setEntity(entity);
    obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), MenuConstants.pricingCombowithoutAddonVariant, fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),false});
     
      
      items = new Items();
      items.build();
      items.setAddon_groups(null);
      items.setVariant_groups(null);
      items.setPricing_combinations(null);
      entity.setItems(new Items[]{items});
      fullMenuPojo.setEntity(entity);
  obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo),new String[]{}, fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});
   return obj.iterator();
    }  
    
    @DataProvider(name = "positiveItemsWithVAndPCAdoonEmptyArray")
   public Iterator<Object[]> positiveItemsWithVAndPCAdoonEmptyArray() throws IOException {

       FullMenuHelper fullMenuHelper = new FullMenuHelper();
            List<Object[]> obj = new ArrayList<>();
            JsonHelper jsonHelper = new JsonHelper();
            FullMenuPojo fullMenuPojo = new FullMenuPojo();
            Entity entity = new Entity();
            Main_categories main_categories = new Main_categories();
            Items items = new Items();
            main_categories.build();
            items.build();
            items.setPrice(0);
            items.setAddon_groups(null);
            Pricing_combinations pc=new Pricing_combinations();
            pc.build();
            pc.setAddon_combination(new Addon_combination[] {});
           // pc.setAddon_combination(null);
            
            items.setPricing_combinations(new Pricing_combinations[] {pc});
            entity.setMain_categories(new Main_categories[]{main_categories});
            entity.setItems(new Items[]{items});
            fullMenuPojo.setEntity(entity);
            obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});
            
            return obj.iterator();
        
    }
    
   @DataProvider(name = "categorySwitch")
   public Iterator<Object[]>  CategorySwitch() throws IOException {
   	 List<Object[]> obj = new ArrayList<>();
     	FullMenuHelper fullMenuHelper = new FullMenuHelper();
        JsonHelper jsonHelper = new JsonHelper();
        FullMenuPojo fullMenuPojo = fullMenuHelper.createDefaultFullMenuPojo();
        Entity entity = new Entity();
        Main_categories main_categories = new Main_categories();
        Sub_categories subCategories= new Sub_categories();
        Items items = new Items();
        items.build();
        items.setId(MenuConstants.ItemIdforSwitch[0]);
        items.setName(MenuConstants.ItemIdforName[0]);
        items.setCategory_id(MenuConstants.CatIdforSwitch[0]);
        items.setSub_category_id(MenuConstants.SubCatIdforSwitch[0]);
        subCategories.build();
        subCategories.setId(MenuConstants.SubCatIdforSwitch[0]);
        subCategories.setName(MenuConstants.SubCatNameforSwitch[0]);
        subCategories.setCategory_id(MenuConstants.CatIdforSwitch[0]);
        main_categories.build();  
        main_categories.setId(MenuConstants.CatIdforSwitch[0]);
        main_categories.setName(MenuConstants.CatNameforSwitch[0]);
        Main_categories main_categoriesValuesToArray[] = new Main_categories[1];
        Sub_categories Sub_categoriesValuestoArray[] = new Sub_categories[1];
        Items ItemValuesToArray[] = new Items[1];
        main_categoriesValuesToArray[0] = main_categories;
        ItemValuesToArray[0] = items;
        Sub_categoriesValuestoArray[0] = subCategories;      
        main_categories.setSub_categories(Sub_categoriesValuestoArray);
        entity.setItems(ItemValuesToArray);
        entity.setMain_categories(main_categoriesValuesToArray); 
       
       fullMenuPojo.setEntity(entity);
       
       obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), new String[]{}, fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});
       
     //  System.out.println("Item with same name same id but category and sub category different");
       items.setCategory_id(MenuConstants.CatIdforSwitch[1]);
       items.setSub_category_id(MenuConstants.SubCatIdforSwitch[1]);
       subCategories.setId(MenuConstants.SubCatIdforSwitch[1]);
       subCategories.setName(MenuConstants.SubCatNameforSwitch[1]);
       subCategories.setCategory_id(MenuConstants.CatIdforSwitch[1]);
       main_categories.build();  
       main_categories.setId(MenuConstants.CatIdforSwitch[1]);
       main_categories.setName(MenuConstants.CatNameforSwitch[1]);
       main_categoriesValuesToArray[0] = main_categories;
       ItemValuesToArray[0] = items;
       Sub_categoriesValuestoArray[0] = subCategories;      
       main_categories.setSub_categories(Sub_categoriesValuestoArray);
       entity.setItems(ItemValuesToArray);
       entity.setMain_categories(main_categoriesValuesToArray); 
      
      fullMenuPojo.setEntity(entity);
      
      obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), new String[]{}, fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});


     // createFullMenuPojo(category_id,subcategory_id,item_id,variant_group_count,variant_count,addon_group_count,addon_count,pricing_combinations_count) throws IOException {
    //items with variants	  3 variant groups and 2 variants
      fullMenuPojo=fullMenuHelper.createFullMenuPojo(MenuConstants.CatIdforVariantRemoval,MenuConstants.SubIdforVariantRemoval,MenuConstants.ItemIdforVariantRemoval,3, 2, 1, 2, 100);
      fullMenuPojo.setEntity(entity);
       obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(fullMenuPojo), new String[]{}, fullMenuHelper.getItemIds(entity.getItems()),entity.getItems(),true});


       return obj.iterator();
	
	}
	   
      
    
    
}
