package com.swiggy.api.erp.cms.dp;

import com.swiggy.api.erp.cms.constants.MenuConstants;
import com.swiggy.api.erp.cms.pojo.BulkMainCategory.BulkMainCategory;
import com.swiggy.api.erp.cms.pojo.BulkMainCategory.Entities;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by kiran.j on 2/20/18.
 */
public class BulkMainCategoryDp {


    @DataProvider(name = "bulkmaincategory")
    public Iterator<Object[]> bulkmaincategory() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        BulkMainCategory mainCategory = new BulkMainCategory();
        mainCategory.build();

        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(mainCategory), new String[]{}, MenuConstants.token_id});

        obj.add(new Object[]{"", jsonHelper.getObjectToJSONallowNullValues(mainCategory), new String[]{"404"}, MenuConstants.token_id});

        obj.add(new Object[]{"", jsonHelper.getObjectToJSONallowNullValues(mainCategory), new String[]{"401"}, "123"});


        Entities entities = mainCategory.getEntities()[0];
        entities.setId(null);
        mainCategory.setEntities(new Entities[]{entities});
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(mainCategory), MenuConstants.bulkaddmaincatid, MenuConstants.token_id});

        entities = mainCategory.getEntities()[0];
        entities.setId(null);
        mainCategory.setEntities(new Entities[]{entities});
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(mainCategory), MenuConstants.bulkaddmaincatid, MenuConstants.token_id});

        entities = mainCategory.getEntities()[0];
        entities.setOrder(0);
        mainCategory.setEntities(new Entities[]{entities});
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(mainCategory), MenuConstants.bulkaddmaincatid, MenuConstants.token_id});

        entities = mainCategory.getEntities()[0];
        entities.setOrder(1);
        mainCategory.setEntities(new Entities[]{entities});
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(mainCategory), MenuConstants.bulkaddmaincatid, MenuConstants.token_id});

        entities = mainCategory.getEntities()[0];
        entities.setEnable(0);
        mainCategory.setEntities(new Entities[]{entities});
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(mainCategory), MenuConstants.bulkaddmaincatid, MenuConstants.token_id});

        entities = mainCategory.getEntities()[0];
        entities.setEnable(1);
        mainCategory.setEntities(new Entities[]{entities});
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(mainCategory), MenuConstants.bulkaddmaincatid, MenuConstants.token_id});

        entities = new Entities();
        entities.build();
        Entities[] entities1 = new Entities[2];
        entities1[0] = entities;
        Entities entities2 = new Entities();
        entities2.build();
        entities2.setName(null);
        entities1[1] = entities2;
        mainCategory.setEntities(entities1);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(mainCategory), MenuConstants.bulkaddmaincatname, MenuConstants.token_id});


        return obj.iterator();
    }

    @DataProvider(name = "bulkupdatemaincategory")
    public Iterator<Object[]> bulkupdatemaincategory() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        BulkMainCategory mainCategory = new BulkMainCategory();
        mainCategory.build();

        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(mainCategory), new String[]{}, MenuConstants.token_id, mainCategory.getEntities()[0].getId()});

        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(mainCategory), new String[]{"401"}, "123", mainCategory.getEntities()[0].getId()});

        obj.add(new Object[]{"", jsonHelper.getObjectToJSONallowNullValues(mainCategory), new String[]{"404"}, MenuConstants.token_id, mainCategory.getEntities()[0].getId()});

        Entities entities = mainCategory.getEntities()[0];
        entities.setId(null);
        mainCategory.setEntities(new Entities[]{entities});
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(mainCategory), MenuConstants.bulkaddmaincatid, MenuConstants.token_id, mainCategory.getEntities()[0].getId()});

        entities = mainCategory.getEntities()[0];
        entities.setOrder(0);
        mainCategory.setEntities(new Entities[]{entities});
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(mainCategory), MenuConstants.bulkaddmaincatid, MenuConstants.token_id, mainCategory.getEntities()[0].getId()});

        entities = mainCategory.getEntities()[0];
        entities.setOrder(1);
        mainCategory.setEntities(new Entities[]{entities});
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(mainCategory), MenuConstants.bulkaddmaincatid, MenuConstants.token_id, mainCategory.getEntities()[0].getId()});

        entities = mainCategory.getEntities()[0];
        entities.setEnable(0);
        mainCategory.setEntities(new Entities[]{entities});
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(mainCategory), MenuConstants.bulkaddmaincatid, MenuConstants.token_id, mainCategory.getEntities()[0].getId()});

        entities = mainCategory.getEntities()[0];
        entities.setEnable(1);
        mainCategory.setEntities(new Entities[]{entities});
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(mainCategory), MenuConstants.bulkaddmaincatid, MenuConstants.token_id, mainCategory.getEntities()[0].getId()});

        entities = mainCategory.getEntities()[0];
        entities.setId(null);
        mainCategory.setEntities(new Entities[]{entities});
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(mainCategory), MenuConstants.bulkaddmaincatid, MenuConstants.token_id, mainCategory.getEntities()[0].getId()});

        entities = new Entities();
        entities.build();
        Entities[] entities1 = new Entities[2];
        entities1[0] = entities;
        Entities entities2 = new Entities();
        entities2.build();
        entities2.setName(null);
        entities1[1] = entities2;
        mainCategory.setEntities(entities1);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(mainCategory), new String[]{}, MenuConstants.token_id, mainCategory.getEntities()[0].getId()});


        return obj.iterator();
    }
}
