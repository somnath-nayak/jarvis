package com.swiggy.api.erp.cms.helper;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.swiggy.api.erp.cms.constants.CmsConstants;
import com.swiggy.api.erp.cms.constants.MenuConstants;
import com.swiggy.api.erp.cms.pojo.FullMenu.Items;

import com.swiggy.api.erp.cms.constants.Constants;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.RedisHelper;

public class CategoryInStockOutStockHelper {

	 Initialize gameofthrones = new Initialize();
	 CMSHelper cmsHelper = new CMSHelper();
	 RedisHelper redisHelper = new RedisHelper();
	 public Processor category_OutStock(String restId,String categoryId,String fromTime,String toTime) throws Exception {
	        HashMap<String, String> requestheaders = new HashMap<>();
	        requestheaders.put("Content-Type", "application/json");
	        requestheaders.put("Authorization", "Basic dmVuZG9yLXN5c3RlbTpzZWxmc2VydmUtc3lzdGVt");
	        GameOfThronesService service = new GameOfThronesService("cmsbaseservice", "categoryOutStock", gameofthrones);
	        String[] urlParams=new String[4];
	        urlParams[0]= restId;
	        urlParams[1]= categoryId;
	        urlParams[2]= URLEncoder.encode(fromTime, "UTF-8");
	        urlParams[3]= URLEncoder.encode(toTime, "UTF-8");
	        String[] payloadParams=new String[2];
	        payloadParams[0]=CmsConstants.source;
	        payloadParams[1]=CmsConstants.metaUser;
	        System.out.println("payload param " +payloadParams.toString());
	        Processor processor = new Processor(service, requestheaders, payloadParams, urlParams);
	        return processor;
	    }
 
	    public boolean verifyDB(String restId, String categoryId, String fromTime, String toTime, boolean success) throws Exception {
	    	
	    	List<Map<String, Object>> list = cmsHelper.getItemHolidays(categoryId,fromTime);
	    	 Map<String, Object> itemMenuCount = cmsHelper.getItemMenuMap1(categoryId);
	    	 Map<String, Object> HolidayListCount = cmsHelper.getItemHolidaysSlotCount(categoryId,fromTime);
	    	if(itemMenuCount==HolidayListCount)
	    	success=success && ((int)itemMenuCount.get("count(*)")==(int)HolidayListCount.get("count(*)"));
	    	System.out.println(success);
	    	 for(int i=0;i<list.size();i++) 
	    	 {
	    	       Map<String, Object> map = list.get(i);
	                   success &= ((String)map.get("from_time") == fromTime) && ((String)map.get("to_time") ==toTime);
	         }
			return success;
	    }
 
	 
	 public Processor categoryInStock(String restId,String categoryId) {
	        HashMap<String, String> requestheaders = new HashMap<>();
	        requestheaders.put("content-type", "application/json");
	        requestheaders.put("Authorization", "Basic dmVuZG9yLXN5c3RlbTpzZWxmc2VydmUtc3lzdGVt");
	        GameOfThronesService service = new GameOfThronesService("cmsbaseservice", "categoryInStock", gameofthrones);
	        String[] urlParams=new String[2];
	        urlParams[0]= restId;
	        urlParams[1]= categoryId;
	        		
	        String[] payloadParams=new String[2];
	        payloadParams[0]=CmsConstants.source;
	        payloadParams[1]=CmsConstants.metaUser;
	        Processor processor = new Processor(service, requestheaders, payloadParams, urlParams);
	        return processor;
	    }

	 public Processor itemOutStock(String partner_type,String partner_id,String third_party_item_ids,String source,String in_stock,String fromTime,String toTime) throws Exception {
	        HashMap<String, String> requestheaders = new HashMap<>();
	        requestheaders.put("Content-Type", "application/json");
	        requestheaders.put("Authorization", "Basic dmVuZG9yLXN5c3RlbTpzZWxmc2VydmUtc3lzdGVt");
	        GameOfThronesService service = new GameOfThronesService("cmsbaseservice", "outStockByThirdPartyID", gameofthrones);
	        String[] urlParams=new  String[7] ;
	        urlParams[0]= partner_type;
	        urlParams[1]= partner_id;
	        urlParams[2]= third_party_item_ids;
	        urlParams[3]= source;
	        urlParams[4]= in_stock;
	        urlParams[3]= URLEncoder.encode(fromTime, "UTF-8");
	        urlParams[4]= URLEncoder.encode(toTime, "UTF-8");
	        urlParams[3]= fromTime;
	       
	        Processor processor = new Processor(service, requestheaders, urlParams);
	        return processor;
	    }
	 public Processor itemInStock(String partner_type,String partner_id,String third_party_item_ids,String source,String in_stock) throws Exception {
	        HashMap<String, String> requestheaders = new HashMap<>();
	        requestheaders.put("Content-Type", "application/json");
	        requestheaders.put("Authorization", "Basic dmVuZG9yLXN5c3RlbTpzZWxmc2VydmUtc3lzdGVt");
	        GameOfThronesService service = new GameOfThronesService("cmsbaseservice", "inStockByThirdPartyID", gameofthrones);
	        String[] urlParams=new  String[7] ;
	        urlParams[0]= partner_type;
	        urlParams[1]= partner_id;
	        urlParams[2]= third_party_item_ids;
	        urlParams[3]= source;
	        urlParams[4]= in_stock;
	        Processor processor = new Processor(service, requestheaders, urlParams);
	        return processor;
	    }
	
}




