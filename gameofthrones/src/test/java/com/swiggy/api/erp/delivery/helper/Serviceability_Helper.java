package com.swiggy.api.erp.delivery.helper;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.gson.JsonObject;
import com.sun.corba.se.spi.ior.ObjectKey;
import com.swiggy.api.erp.delivery.constants.ClusterConstants;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.swiggy.api.erp.delivery.constants.Serviceability_Constant;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.annotations.Test;

public class Serviceability_Helper
{
    DeliveryDataHelper dataHelper = new DeliveryDataHelper();
    DeliveryHelperMethods helperMethod = new DeliveryHelperMethods();
    DeliveryHelperMethods delHelper = new DeliveryHelperMethods();
    Initialize gameofthrones = new Initialize();
    private static final Logger LOGGER = Logger.getLogger(Serviceability_Helper.class.getName());


    public String getValueFromDb(String attributeName, String dbName, String refAttributeName, String refAttributeValue)
    {
        String value = SystemConfigProvider.getTemplate("deliverydb")
                .queryForList("select " + attributeName + " from " + dbName + " where " + refAttributeName + "='" + refAttributeValue + "'").get(0).get(attributeName).toString();
        return value;
    }

    public void setValueInDb(String dbName, String attName, String attValue, String refAttName, String refAttValue)
    {
        SystemConfigProvider.getTemplate("deliverydb")
                .update("update " + dbName + " set " + attName + "='" + attValue + "' where " + refAttName + "='" + refAttValue + "'");
        if(!refAttName.equals("id"))
        {
            String id = getValueFromDb("id", dbName, refAttName, refAttValue);
            clearCache(dbName, id);
        }
        else
            clearCache(dbName, refAttValue);
    }


    public Map<String,String> retryCerebroListing(int numretries,int pollint, String[] payload, Map<String, String> argMap )
    {
        Boolean pollflag = false;
        Processor processor = null;
        Map<String,String> mappedresponse = null;
        while (numretries > 0)
        {
            processor = new Serviceability_Helper().listing(payload);
            String response = processor.ResponseValidator.GetBodyAsText();
            System.out.println(response);
            mappedresponse = getServiceabiltyResponseMap(response);

            boolean testflag=true;
            for(String param:argMap.keySet())
            {
                if(mappedresponse.containsKey(param))
                {
                    boolean test=mappedresponse.get(param).equalsIgnoreCase(argMap.get(param));
                    testflag=testflag&test;
                }
                else
                {
                    testflag=false;
                }
            }
            if(testflag)
            {
                break;
            }
            dataHelper.waitIntervalMili(pollint);
            numretries--;
        }
        System.out.println("pollFlag value "+pollflag);
        return mappedresponse;
    }

    ///////jsonobject cart response...
    public Map<String,String> retryCerebroCart(int numretries,int pollint, String[] payload, Map<String, String> argMap )
    {
        Boolean pollflag = false;
        Processor processor = null;
        Map<String,String> mappedresponse = null;
        while (numretries > 0)
        {
            processor = new Serviceability_Helper().cart(payload);
            String response = processor.ResponseValidator.GetBodyAsText();
            System.out.println(response);
            mappedresponse = getServiceabiltyCartResponseMap(response);
            //System.out.println("Servicebaility Cart Map");

            //printMap(mappedresponse);

            boolean testflag=true;
            for(String param:argMap.keySet())
            {
                if(mappedresponse.containsKey(param))
                {
                    boolean test=mappedresponse.get(param).equalsIgnoreCase(argMap.get(param));
                    testflag=testflag&test;
                }
                else
                {
                    testflag=false;
                }
            }
            if(testflag)
            {
                break;
            }
            dataHelper.waitIntervalMili(pollint);
            numretries--;
        }
        System.out.println("pollFlag value "+pollflag);
        return mappedresponse;
    }




    @SafeVarargs
    public final Map<String,String> retryCerebroListingUsingVarargs(int numretries,int pollint, String[] payload, Map<String, String> ...maps )
    {
        Boolean pollflag = false;
        Processor processor = null;
        Map<String,String> mappedresponse = null;
        while (numretries > 0)
        {
            processor = new Serviceability_Helper().listing(payload);
            String response = processor.ResponseValidator.GetBodyAsText();
            System.out.println(response);
            mappedresponse = getServiceabiltyResponseMap(response);
            List<String> actualAttributeKey = new ArrayList<String>();
            List<String> actualAttValueList = new ArrayList<String>();
            List<String> expectedAttValueList = new ArrayList<String>();
            int index = 0;
            for(Map<String, String> map : maps)
            {
                try
                {
                    actualAttValueList.add(mappedresponse.get(map.keySet().toString().replace("[", "").replace("]", "")).toString());
                    actualAttributeKey.add(map.keySet().toString().replace("[", "").replace("]", ""));

                    expectedAttValueList.add(map.get(actualAttributeKey.get(index++)).toString());
                }
                catch(NullPointerException e)
                {
                    System.out.println("*********** " + map.keySet() + " key is absent **************");
                }
            }
            System.out.println("****** actual value **** " + actualAttValueList);
            System.out.println("****** expected value **** " + expectedAttValueList);

            if(actualAttValueList.equals(expectedAttValueList))
            {
                pollflag = true;

                break;
            }

            dataHelper.waitIntervalMili(pollint);
            numretries--;
        }
        System.out.println("pollFlag value "+pollflag);
        return mappedresponse;
    }




    public Map<String,String> retryCerebroListing(int numretries,int pollint,String expectedvalue, String[] payload, String attribute)
    {
        Boolean pollflag = false;
        Processor processor = null;
        Map<String,String> mappedresponse = null;
        while (numretries > 0)
        {
            processor = new Serviceability_Helper().listing(payload);
            String response = processor.ResponseValidator.GetBodyAsText();
            System.out.println(response);
            mappedresponse = getServiceabiltyResponseMap(response);
            //Integer statuscode = processor.ResponseValidator.GetNodeValueAsInt("statusCode");
            //String serviceableStatus = mappedresponse.get("serviceability").toString();
            String serviceableStatus = mappedresponse.get(attribute).toString();
            if(serviceableStatus.equalsIgnoreCase(expectedvalue))
            {
                pollflag = true;
                break;
            }
            System.out.println("*********** UNSUCCESSFUL *********");
            long curtime = System.currentTimeMillis();
            dataHelper.waitIntervalMili(pollint);
            long fintime = System.currentTimeMillis();
            System.out.println(fintime-curtime +" WAITTIME");
            numretries--;
        }
        System.out.println("pollFlag value "+pollflag);
        return mappedresponse;
    }
    public Map<String,String> getServiceabiltyResponseMap(String response)
    {
        JSONObject mappedResponse = null;
        JSONObject mappedResponsecutomerObject = null;
        Map<String,String> responseMap = new HashMap<String,String>();
        try {

            JSONObject resObject = new JSONObject(response);
            System.out.println("Printing response in  getServiceabiltyResponseMap"+response);
            JSONArray serviceableRestArray=(JSONArray)(resObject.get("serviceableRestaurants"));
            if(serviceableRestArray.length()==0) {
                mappedResponse=new JSONObject();
            }
            else {
                mappedResponse = ((JSONArray) (resObject.get("serviceableRestaurants"))).getJSONObject(0).getJSONObject("listingServiceabilityResponse");
            }    		if(resObject.has("customerInfo"))
            {
                mappedResponsecutomerObject = ((JSONObject)(resObject.get("customerInfo")));
                Iterator<?> it = mappedResponsecutomerObject.keys();
                while (it.hasNext())
                {
                    String key = (String)it.next();
                    responseMap.put(key,mappedResponsecutomerObject.getString(key));
                }
            }
            Iterator<?> it = mappedResponse.keys();
            while (it.hasNext())
            {
                String key = (String)it.next();
                responseMap.put(key,mappedResponse.getString(key));
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        return responseMap;
    }

    public Map<String,String> getServiceabiltyCartResponseMap(String response)
    {
        JSONObject mappedResponse = null;
        JSONObject mappedResponseSurge = null;
        JSONObject mappedResponseDegradation=null;
        Map<String,String> responseMap = new HashMap<String,String>();

        try {
            JSONObject resObject = new JSONObject(response);
            JSONArray serviceableRestArray=(JSONArray)(resObject.get("cart"));
            if(serviceableRestArray.length()==0) {
                mappedResponse=new JSONObject();
            }
            else {
                mappedResponse = ((JSONArray) (resObject.get("cart"))).getJSONObject(0);
            }
            if(mappedResponse.has("surge_mode"))//&&(mappedResponse.getJSONObject("surge_mode")!=null || mappedResponse.getJSONObject("surge_mode").toString()!=""))
            //if(resObject.has("surge_mode"))
            {
                mappedResponseSurge = ((JSONObject)(mappedResponse.get("surge_mode")));
                Iterator<?> its = mappedResponseSurge.keys();
                while (its.hasNext())
                {
                    String key = (String)its.next();
                    responseMap.put(key,mappedResponseSurge.getString(key));
                }
            }

            if(mappedResponse.has("degradation"))//&&(mappedResponse.getJSONObject("degradation")!=null || mappedResponse.getJSONObject("degradation").toString()!=""))
            {
                mappedResponseDegradation = ((JSONObject)(mappedResponse.get("degradation")));
                Iterator<?> itd = mappedResponseDegradation.keys();
                while (itd.hasNext())
                {
                    String key = (String)itd.next();
                    responseMap.put(key,mappedResponseDegradation.getString(key));
                }
            }


            Iterator<?> it = mappedResponse.keys();
            while (it.hasNext())
            {
                String key = (String)it.next();
                responseMap.put(key,mappedResponse.getString(key));
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        return responseMap;
    }


    public Map<String,String> getServiceabiltyResponseMapWithoutRestaurantId(String response)
    {
        JSONObject mappedResponse = null;
        JSONObject mappedResponsecutomerObject = null;
        Map<String,String> responseMap = new HashMap<String,String>();
        Map<String, String> map = new LinkedHashMap<>();
        try {
            System.out.println("response = " + response);
            if (!response.isEmpty()) {
                JSONObject resObject = new JSONObject(response);
                JSONArray serviceableRestArray = (JSONArray) (resObject.get("serviceableRestaurants"));
                if (serviceableRestArray.length() == 0) {
                    mappedResponse = new JSONObject();
                } else {
                    int i = 0;
                    System.out.println("******** serviceableRestArray = " + serviceableRestArray.length());

                    while (i < serviceableRestArray.length()) {
                        mappedResponse = ((JSONArray) (resObject.get("serviceableRestaurants"))).getJSONObject(0).getJSONObject("listingServiceabilityResponse");

                        Iterator<?> it = mappedResponse.keys();
                        while (it.hasNext()) {
                            String key = (String) it.next();

                            String longDistanceFlag = serviceableRestArray.getJSONObject(i).getJSONObject("listingServiceabilityResponse").get("long_distance").toString();//responseMap.get("long_distance");
                            String restaurantId = serviceableRestArray.getJSONObject(i).get("restaurantId").toString();
                            map.put(restaurantId, longDistanceFlag);
                        }
                        i++;
                    }
                }
            }
            else
                response = "{}";
        }

        catch(JSONException e)
        {
            e.printStackTrace();
        }

        return map;
    }


    public boolean clearCache(String dbName, String entityId)
    {
        Boolean flag = false;
        String entity;
        switch (dbName)
        {
            case "area":
            {
                entity = "area";
                Processor processor = deleteCache(entity,entityId);
                flag = processor.ResponseValidator.GetResponseCode() == 200;
                break;
            }
            case "city":
            {
                entity = "city";
                Processor processor = deleteCache(entity,entityId);
                flag = processor.ResponseValidator.GetResponseCode() == 200;
                break;
            }
            case "customer_segment":
            {
                entity = "customerSegment";
                Processor processor = deleteCache(entity,entityId);
                flag = processor.ResponseValidator.GetResponseCode() == 200;
                break;
            }
            case "delivery_partners":
            {
                entity = "deliveryPartner";
                Processor processor = deleteCache(entity,entityId);
                flag = processor.ResponseValidator.GetResponseCode() == 200;
                break;
            }
            case "polygon":
            {
                entity = "polygon";
                Processor processor = deleteCache(entity,entityId);
                flag = processor.ResponseValidator.GetResponseCode() == 200;
                break;
            }
            case "pop_polygon":
            {
                entity = "popPolygon";
                Processor processor = deleteCache(entity,entityId);
                flag = processor.ResponseValidator.GetResponseCode() == 200;
                break;
            }
            case "rain_polygon":
            {
                entity = "rainPolygon";
                Processor processor = deleteCache(entity,entityId);
                flag = processor.ResponseValidator.GetResponseCode() == 200;
                break;
            }
            case "restaurant":
            {
                entity = "restaurant";
                Processor processor = deleteCache(entity,entityId);
                flag = processor.ResponseValidator.GetResponseCode() == 200;
                break;
            }
            case "restaurant_polygon_mapping":
            {
                entity = "restaurantPolygon";
                Processor processor = deleteCache(entity,entityId);
                flag = processor.ResponseValidator.GetResponseCode() == 200;
                break;
            }
            case "rain_pop_restaurant_mapping":
            {
                entity = "restaurantRainPopPolygon";
                Processor processor = deleteCache(entity,entityId);
                flag = processor.ResponseValidator.GetResponseCode() == 200;
                break;
            }
            case "black_zone":
            {
                entity = "blackZone";
                Processor processor = deleteCache(entity,entityId);
                flag = processor.ResponseValidator.GetResponseCode() == 200;
                break;
            }
            case "long_distance":
            {
                entity = "longDistance";
                Processor processor = deleteCache(entity,entityId);
                flag = processor.ResponseValidator.GetResponseCode() == 200;
                break;
            }
            case "zone":
            {
                entity = "zone";
                Processor processor = deleteCache(entity,entityId);
                flag = processor.ResponseValidator.GetResponseCode() == 200;
                break;
            }
            case "zoneRain":
            {
                entity = "zone_rain_params";
                Processor processor = deleteCache(entity,entityId);
                flag = processor.ResponseValidator.GetResponseCode() == 200;
                break;
            }
        }
        return flag;
    }


    public Processor deleteCache(String type, String id)
    {
        GameOfThronesService gameOfThronesService = new GameOfThronesService("deliverycerebro","cacheDelete",gameofthrones);
        Processor processor = new Processor(gameOfThronesService, Serviceability_Constant.singleHeader(), null, new String[] {type, id});
        return processor;
    }

    public Processor listing(String[] payload)
    {
        GameOfThronesService gameOfThronesService = new GameOfThronesService("deliverycerebro","listing",gameofthrones);
        Processor processor = new Processor(gameOfThronesService, Serviceability_Constant.singleHeader(), payload);
        return processor;
    }


    public Processor listingCallWithoutRestaurantId(String[] payload)
    {
        GameOfThronesService gameOfThronesService = new GameOfThronesService("deliverycerebro","listingCluster",gameofthrones);
        Processor processor = new Processor(gameOfThronesService, Serviceability_Constant.singleHeader(), payload);
        return processor;
    }


    public Processor cart(String[] payload)
    {
        GameOfThronesService gameOfThronesService = new GameOfThronesService("deliverycerebro","cerebrocartSla",gameofthrones);
        Processor processor = new Processor(gameOfThronesService, Serviceability_Constant.singleHeader(), payload);
        return processor;
    }


    public Processor cartWithSlot(String[] payload)
    {
        GameOfThronesService gameOfThronesService = new GameOfThronesService("deliverycerebro","cerebrocartSlaWithDeliverySlot",gameofthrones);
        Processor processor = new Processor(gameOfThronesService, Serviceability_Constant.singleHeader(), payload);
        return processor;
    }

    public Processor preOrderListing(String[] payload)
    {
        GameOfThronesService gameOfThronesService = new GameOfThronesService("deliverycerebro","preOrderListing",gameofthrones);
        Processor processor = new Processor(gameOfThronesService, Serviceability_Constant.singleHeader(), payload);
        return processor;
    }

    public void setNoRainsAtZone(String zoneId)
    {
        setValueInDb("zone", "rain_mode_type", "3", "id", zoneId);
        clearCache("zone", zoneId);
    }


    public String getRestLatLongFromSolr(String url, String core, String query) {
        SolrDocumentList solrDocuments = dataHelper.getSolrOutput( url,  core,  query);
        Iterator<SolrDocument> iterator=solrDocuments.iterator();
        while(iterator.hasNext())
        {
            System.out.println(iterator.next().toString());
        }
        Pattern pattern= Pattern.compile("\\bplace\\W([0-9]*[.])?[0-9]+[,]([0-9]*[.])?[0-9]+");
        Matcher matcher= pattern.matcher(solrDocuments.toString());
        matcher.find();
        String latlon=matcher.group();
        System.out.println(latlon);
        return latlon.split("=")[1];
    }

    public void printMap(Map<?,?> printMap)
    {
        for(Object str:printMap.keySet())
        {
            System.out.println("**********************************************************************************");
            System.out.println(str+"::::"+printMap.get(str).toString());
        }
    }


    //CART CALL WITH CLUSTER FLAG..
    public Map<String,String> retryCerebroCartViaCluster(int numretries,int pollint, String[] payload, Map<String, String> argMap )
    {
        Boolean pollflag = false;
        Processor processor = null;
        int count = 0;
        Map<String,String> mappedresponse = null;
        while (numretries > 0 || count < 2)
        {
            processor = new Serviceability_Helper().cartWithClusterFlag(payload);
            String response = processor.ResponseValidator.GetBodyAsText().trim();
            if(!response.isEmpty()) {
                mappedresponse = getServiceabiltyCartResponseMap(response);

                boolean testflag = true;
                for (String param : argMap.keySet()) {
                    if (mappedresponse.containsKey(param)) {
                        boolean test = mappedresponse.get(param).equalsIgnoreCase(argMap.get(param));
                        testflag = testflag & test;
                    } else {
                        testflag = false;
                    }
                }
                if (testflag) {
                    break;
                }
            }
            else
            {
                response="{}";
            }
            dataHelper.waitIntervalMili(pollint);
            numretries--;
            count++;
        }
        System.out.println("pollFlag value "+pollflag);
        return mappedresponse;
    }


    public Processor cartWithClusterFlag(String[] payload)
    {
        GameOfThronesService gameOfThronesService = new GameOfThronesService("deliverycerebro","cerebrocartSlaWithClusterFlag",gameofthrones);
        Processor processor = new Processor(gameOfThronesService, Serviceability_Constant.singleHeader(), payload);
        return processor;
    }

    //LISTING CALL WITH CLUSTER FLAG..
    public Map<String,String> retryCerebroListingViaCluster(int numretries,int pollint, String[] payload, Map<String, String> argMap )
    {
        Boolean pollflag = false;
        Processor processor = null;
        Map<String,String> mappedresponse = null;
        int count = 0;
        while (numretries > 0 || count < 3)
        {
            processor = new Serviceability_Helper().listingViaCluster(payload);
            String response = processor.ResponseValidator.GetBodyAsText();

            if(!response.isEmpty()) {
                mappedresponse = getServiceabiltyResponseMap(response);

                boolean testflag = true;
                for (String param : argMap.keySet()) {
                    if (mappedresponse.containsKey(param)) {
                        boolean test = mappedresponse.get(param).equalsIgnoreCase(argMap.get(param));
                        testflag = testflag & test;
                    } else {
                        testflag = false;
                    }
                }
                if (testflag) {
                    break;
                }
            }
            else
            {
                response="{}";
            }
            dataHelper.waitIntervalMili(pollint);
            numretries--;
            count++;
        }
        System.out.println("pollFlag value "+pollflag);
        return mappedresponse;
    }



    public Processor listingViaCluster(String[] payload)
    {
        GameOfThronesService gameOfThronesService = new GameOfThronesService("deliverycerebro","listingViaCluster",gameofthrones);
        Processor processor = new Processor(gameOfThronesService, Serviceability_Constant.singleHeader(), payload);
        return processor;
    }


    //LISTING W/O SERVICEABILITY WITH CLUSTER FLAG..
    public Map<String,String> retryCerebroListingWithoutServCheckViaCluster(int numretries,int pollint, String[] payload, Map<String, String> argMap )
    {
        Boolean pollflag = false;
        Processor processor = null;
        Map<String,String> mappedresponse = null;
        int count = 0;
        while (numretries > 0 || count < 3) {
            processor = new Serviceability_Helper().listingWithoutServiceabilityCheck(payload);
            String response = processor.ResponseValidator.GetBodyAsText().trim();

            if (!response.isEmpty()) {
                mappedresponse = getServiceabiltyResponseMap(response);
                boolean testflag = true;
                for (String param : argMap.keySet()) {
                    if (mappedresponse.containsKey(param)) {
                        boolean test = mappedresponse.get(param).equalsIgnoreCase(argMap.get(param));
                        testflag = testflag & test;
                    } else {
                        testflag = false;
                    }
                }
                if (testflag) {
                    break;
                }

            } else {
                response = "{}";
            }
            dataHelper.waitIntervalMili(pollint);
            numretries--;
            count++;
        }
        System.out.println("pollFlag value "+pollflag);
        return mappedresponse;
    }

    public Processor listingWithoutServiceabilityCheck(String[] payload)
    {
        GameOfThronesService gameOfThronesService = new GameOfThronesService("deliverycerebro","listingWithoutServiceabilityCheck",gameofthrones);
        Processor processor = new Processor(gameOfThronesService, Serviceability_Constant.singleHeader(), payload);
        return processor;
    }


    public void LDFlagInItemclusterTest()
    {
        String response = listingCallWithoutRestaurantId(new String[]{"12.934991", "77.623266", ClusterConstants.cityId, "true"}).ResponseValidator.GetBodyAsText();
        Map<String, String> listingWithCluster = getServiceabiltyResponseMapWithoutRestaurantId(response);
        List<String> LD1inBothPlaces = new ArrayList<>();
        List<String> LD1inResponseAndFalseInDB = new ArrayList<>();
        List<String> LD1inResponseAndNullInDB = new ArrayList<>();
        Map<String, String> myMap = new LinkedHashMap<>();
        for (Map.Entry<String, String> entry : listingWithCluster.entrySet())
        {
            String query = SystemConfigProvider.getTemplate("deliveryclusterdb")
                    .queryForMap("select attributes from items where reference_id='" + entry.getKey() + "'").get("attributes").toString();
            String[] query1 = query.split(",");
            for(String s1: query1) {
                String[] keyValue = s1.split(":");
                myMap.put(keyValue[0].trim().replace("{", "").replace("\"", ""), keyValue[1].trim().replace("}", "").replace("\"", ""));

            }

            System.out.println("******** ID ********* " + myMap.get("name"));
            System.out.println("******** LD flag ********* " + myMap.get("isLongDistanceEnabled"));


            System.out.println(entry.getKey() + "/" + entry.getValue());
            if (entry.getValue().equals("1"))
            {
                if(query.contains("isLongDistanceEnabled")) {
                    for (Map.Entry<String, String> entry1 : myMap.entrySet()) {

                        if (entry1.getKey().equals("isLongDistanceEnabled")) {
                            if (entry1.getValue().toString().equals("true"))
                                LD1inBothPlaces.add(entry.getKey());
                            else if (entry1.getValue().toString().equals("false"))
                                LD1inResponseAndFalseInDB.add(entry.getKey());
                        }
                    }
                }
                else
                    LD1inResponseAndNullInDB.add(entry.getKey());

            }

        }
        System.out.println("********* LD1inBothPlaces ****** " + LD1inBothPlaces);
        System.out.println("********* LD1inBothPlaces size ****** " + LD1inBothPlaces.size());
        System.out.println("********* LD1inResponseAndFalseInDB ****** " + LD1inResponseAndFalseInDB);
        System.out.println("********* LD1inResponseAndFalseInDB size ****** " + LD1inResponseAndFalseInDB.size());
        System.out.println("********* LD1inResponseAndNullInDB ****** " + LD1inResponseAndNullInDB);
        System.out.println("********* LD1inResponseAndNullInDB size****** " + LD1inResponseAndNullInDB.size());

    }


    public void updateSelfDeliveryFlagType() throws Exception
    {
         List<Map<String, Object>> mapList = SystemConfigProvider.getTemplate("deliveryclusterdb")
                .queryForList("select id, attributes from items where attributes like '%\"selfDeliveryOverriden\": 0%' or attributes like '%\"selfDeliveryOverriden\": 1%' or attributes like '%\"serviceable\": 0%' or attributes like '%\"serviceable\": 1%' or attributes like '%\"deliveryPartnerActive\": 0%' or attributes like '%\"deliveryPartnerActive\": 1%'");
        Map<String, String> myMap = new LinkedHashMap<>();
        for (int i = 0; i < mapList.size(); i++)
        {
           // String query = SystemConfigProvider.getTemplate("deliveryclusterdb")
            //        .queryForMap("select attributes from items where id='" + map.get(i).get("id") + "'").get("attributes").toString();

           // System.out.println("attributes ***************** " + query);
           // String[] query1 = query.split(",");
           // System.out.println("attributes array ***************** " + Arrays.toString(query1));
            //for(String s1: query1) {
            //    String[] keyValue = s1.split(":");
             //   myMap.put(keyValue[0].trim().replace("{", "").replace("\"", ""), keyValue[1].trim().replace("}", "").replace("\"", ""));

            //}

            /*
            * iterate over each list Item
            * take attribute from it,
            * check it is boolean or not
            * if not replace 0 to false, 1 to true
            * */

            Map<String, Object> thisMap=mapList.get(i);





            /*System.out.println("my map before *****************" + myMap);
            if (map.get(i).get("attributes").toString().contains("selfDeliveryOverriden"))
            {
                if(myMap.get("selfDeliveryOverriden").equals("0") || myMap.get("selfDeliveryOverriden").equals("1"))
                    myMap.put("selfDeliveryOverriden", myMap.get("selfDeliveryOverriden").equals("1") ? "true" : "false");
            }
            if (map.get(i).get("attributes").toString().contains("serviceable"))
            {

                if(myMap.get("serviceable").equals("0") || myMap.get("serviceable").equals("1"))
                    myMap.put("serviceable", myMap.get("serviceable").equals("1") ? "true" : "false");

            }
            if (map.get(i).get("attributes").toString().contains("deliveryPartnerActive"))
            {


                if(myMap.get("deliveryPartnerActive").equals("0") || myMap.get("deliveryPartnerActive").equals("1"))
                    myMap.put("deliveryPartnerActive", myMap.get("deliveryPartnerActive").equals("1") ? "true" : "false");
            }
            System.out.println("my map after *****************" + myMap);*/

            JSONObject json = updateBool(thisMap);
            try {


                SystemConfigProvider.getTemplate("deliveryclusterdb").update("update items set attributes='" + json + "' where id='" + mapList.get(i).get("id") + "'");
            }catch(org.springframework.jdbc.BadSqlGrammarException ex)
            {
                LOGGER.info("Item id : " + mapList.get(i).get("id").toString());
                System.out.println("************** ID *********** " + mapList.get(i).get("id"));
            }
        }
        System.out.println("**** my map ************ " + myMap);

    }

    public JSONObject updateBool(Map<String, Object> test) throws Exception{
	    String id = test.get("id").toString();
        JSONObject json = new JSONObject(test.get("attributes").toString());

            if (json.has("serviceable") && json.getString("serviceable").equals("1")) {
                json.put("serviceable", true);
            } else if (json.has("serviceable") && json.getString("serviceable").equals("0")) {
                json.put("serviceable", false);
            }
            if (json.has("deliveryPartnerActive") && json.getString("deliveryPartnerActive").equals("1")) {
                json.put("deliveryPartnerActive", true);
            } else if (json.has("deliveryPartnerActive") && json.getString("deliveryPartnerActive").equals("0")) {
                json.put("deliveryPartnerActive", false);
            }
            if (json.has("selfDeliveryOverriden") && json.getString("selfDeliveryOverriden").equals("1")) {
                json.put("selfDeliveryOverriden", true);
            } else if (json.has("selfDeliveryOverriden") && json.getString("selfDeliveryOverriden").equals("0")) {
                json.put("selfDeliveryOverriden", false);
            }

        return json;

    }



}
