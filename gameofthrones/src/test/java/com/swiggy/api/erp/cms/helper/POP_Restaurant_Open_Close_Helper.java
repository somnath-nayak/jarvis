package com.swiggy.api.erp.cms.helper;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/*
 * This class is responsible to open or close a restaurant using the respective Restaurant-ID
 *
 */
public class POP_Restaurant_Open_Close_Helper{

	Initialize gameofthrones = new Initialize();
	
	public HashMap<String, String> setHeaders1() throws JSONException
	{
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		JSONObject json = new JSONObject();
		json.put("source", "cms");
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("user-meta", json.toString());	
		requestheaders.put("Authorization", "Basic dmVuZG9yLXN5c3RlbTpzZWxmc2VydmUtc3lzdGVt");
		return requestheaders;	   
	}

	public String[] setPayload1(int restaurant_id, String from_time, String to_time) throws JSONException
	{
		return new String[]{Integer.toString(restaurant_id),from_time,to_time};
	}


	public Processor restaurant_Close_withRestaurantMenuMap(int restaurant_id, String from_time, String to_time) throws JSONException
	{
		GameOfThronesService service1 = new GameOfThronesService("cmsselfserve","restaurantclose", gameofthrones);
		Processor p1 = new Processor(service1, setHeaders1(), 
				setPayload1(restaurant_id, from_time, to_time),
				new String[]{Integer.toString(restaurant_id)});
		return p1;   
	}


	public Processor restaurant_Close_withoutRestaurantMenuMap(int restaurant_id, String from_time, String to_time) throws JSONException
	{
		GameOfThronesService service1 = new GameOfThronesService("cmsselfserve","restaurantclose", gameofthrones);
		Processor p1 = new Processor(service1,
				setHeaders1(), 
				setPayload1(restaurant_id, from_time, to_time), 
				new String[]{Integer.toString(restaurant_id)});
		return p1;   
	}


	public Processor restaurant_Open_withRestaurantMenuMap(int restaurant_id, String from_time, String to_time) throws JSONException
	{
		GameOfThronesService service1 = new GameOfThronesService("cmsselfserve","restaurantopen", gameofthrones);
		Processor p1 = new Processor(service1,
				setHeaders1(), 
				setPayload1(restaurant_id, from_time, to_time), 
				new String[]{Integer.toString(restaurant_id)});

		return p1; 
	}

	public Processor restaurant_Open_withoutRestaurantMenuMap(int restaurant_id, String from_time, String to_time) throws JSONException
	{
		GameOfThronesService service1 = new GameOfThronesService("cmsselfserve","restaurantopen", gameofthrones);
		Processor p1 = new Processor(service1,
				setHeaders1(), 
				setPayload1(restaurant_id, from_time, to_time), 
				new String[]{Integer.toString(restaurant_id)});
		return p1;
	}

	public Processor restaurant_Open_withoutRestaurantID(int restaurant_id, String from_time, String to_time) throws JSONException
	{
		GameOfThronesService service1 = new GameOfThronesService("cmsselfserve","restaurantopen", gameofthrones);
		Processor p1 = new Processor(service1,
				setHeaders1(), 
				setPayload1(restaurant_id, from_time, to_time), 
				new String[]{""});
		return p1;
	}



}
