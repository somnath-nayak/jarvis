package com.swiggy.api.erp.ff.helper;

import com.swiggy.api.erp.ff.constants.LosConstants;
import framework.gameofthrones.Aegon.SystemConfigProvider;

import java.util.List;
import java.util.Map;

public class CancelAndDeliveredHelper {


    public String getOrderStatus(String orderId) {
        Long orderId1 = Long.parseLong(orderId);
        List<Map<String, Object>> statusIds = SystemConfigProvider.getTemplate(LosConstants.hostName).queryForList("select status_id from oms_order where order_id = "+ orderId1  +";");
        String statusId = statusIds.get(0).get("status_id").toString();
        List<Map<String, Object>> OrderStatus = SystemConfigProvider.getTemplate(LosConstants.hostName).queryForList("select delivery_status from oms_orderstatus where id = "+ statusId  +";");

        System.out.println ("order pk = "+OrderStatus.get(0).get("delivery_status"));
        return OrderStatus.get(0).get("delivery_status").toString();

    }

    public void cancelOrder(String orderId) {
        Long orderId1 = Long.parseLong(orderId);
        List<Map<String, Object>> statusIds = SystemConfigProvider.getTemplate(LosConstants.hostName).queryForList("select status_id from oms_order where order_id = "+ orderId1  +";");
        String statusId = statusIds.get(0).get("status_id").toString();
        SystemConfigProvider.getTemplate(LosConstants.hostName).execute("update oms_orderstatus set order_status ='cancelled',status = 'cancelled' where id = "+ statusId  +";");


    }
}
