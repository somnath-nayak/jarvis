package com.swiggy.api.erp.cms.pojo;

/**
 * Created by kiran.j on 1/23/18.
 */
public class RestaurantTimeSlot {

    private int restaurant_id;
    private String open_time;
    private String close_time;
    private String day;

    public RestaurantTimeSlot(int restaurant_id, String open_time, String close_time, String day) {
        this.restaurant_id = restaurant_id;
        this.open_time = open_time;
        this.close_time = close_time;
        this.day = day;
    }

    public int getRestaurant_id() {
        return restaurant_id;
    }

    public void setRestaurant_id(int restaurant_id) {
        this.restaurant_id = restaurant_id;
    }

    public String getOpen_time() {
        return open_time;
    }

    public void setOpen_time(String open_time) {
        this.open_time = open_time;
    }

    public String getClose_time() {
        return close_time;
    }

    public void setClose_time(String close_time) {
        this.close_time = close_time;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    @Override
    public String toString() {
        return "{" +
                "restaurant_id=" + restaurant_id +
                ", open_time='" + open_time + '\'' +
                ", close_time='" + close_time + '\'' +
                ", day='" + day + '\'' +
                '}';
    }
}
