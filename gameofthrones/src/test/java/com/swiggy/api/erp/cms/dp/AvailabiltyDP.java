package com.swiggy.api.erp.cms.dp;

import com.swiggy.api.erp.cms.helper.CMSHelper;
import org.testng.annotations.DataProvider;

public class AvailabiltyDP {

    CMSHelper cmsHelper= new CMSHelper();

    @DataProvider(name = "restTime")
    public Object[][] restTime() {
        return new Object[][]{{"281"},
                {"273"},{"9990"},{"477"},{"412"},{"793"},{"894"},{"698"},{"1076"},{"1200"},{"1230"}};
    }
    @DataProvider(name = "restTimeInvalid")
    public Object[][] restTimeInvalid() {
        return new Object[][]{{"1"},{"-1"},{"0"},{"null"},{""}};
    }

    @DataProvider(name = "areaIdRestSlotsFailure")
    public Object[][] areaIdRestSlotsFailure() {
        return new Object[][]{{"1"},{"-1"},{"0"},{"null"}};
    }

    @DataProvider(name = "areaIdRestSlotsError")
    public Object[][] areaIdRestSlotsError() {
        return new Object[][]{{""}};
    }

    @DataProvider(name = "cityRestSlotsFailure")
    public Object[][] cityRestSlotsFailure() {
        return new Object[][]{{"1"},{"-1"},{"0"},{"null"},{""}};
    }

    @DataProvider(name = "restTimeNegative")
    public Object[][] restTimeNegative() {
        return new Object[][]{{"null"}};
    }

    @DataProvider(name = "areaSlot")
    public Object[][] areaSlot() {
        return new Object[][]{{"223"},
                {"219"},{"542"},{"535"},{"415"},{"420"},{"1267"}};
    }

    @DataProvider(name = "areaSlotError")
    public Object[][] areaSlotError() {
        return new Object[][]{{""}};
    }

    @DataProvider(name = "areaSlotFailure")
    public Object[][] areaSlotFailure() {
        return new Object[][]{{"null"}};
    }

    @DataProvider(name = "citySlot")
    public Object[][] citySlot() {
        return new Object[][]{{"4"},
                {"7"},{"2"},{"1"},{"-1"},{"0"}};
    }

    @DataProvider(name = "citySlotFailure")
    public Object[][] citySlotFailure() {
        return new Object[][]{{"null"}};
    }


    @DataProvider(name = "citySlotError")
    public Object[][] citySlotError() {
        return new Object[][]{{""}};
    }

    @DataProvider(name = "itemSlot")
    public Object[][] itemSlot() {
        return new Object[][]{{"223"},{"281"},{"273"},{"9990"},{"477"},{"412"},{"793"}};
    }

    @DataProvider(name = "itemSlotError")
    public Object[][] itemSlotError() {
        return new Object[][]{{"2"},{"1"},{"-1"},{"0"}};
    }

    @DataProvider(name = "itemSlotErr")
    public Object[][] itemSlotErr() {
        return new Object[][]{{"null"},{""}};
    }

    @DataProvider(name = "restTypeSlot")
    public Object[][] restTypeSlot() {
        return new Object[][]{{"273", "REGULAR_ITEM"},
                {"281","REGULAR_ITEM"},{"9990", "POP_ITEM"}, {"9990", "REGULAR_ITEM"}};
    }

    @DataProvider(name = "restTypeSlotFailure")
    public Object[][] restTypeSlotFailure() {
        return new Object[][]{{"273", "ITEM"}, {"281", ""},{"9990", "ITEM 1"},{"99900", "ITEM 2"},{"123456", "ITEM4"}};
    }

    @DataProvider(name = "menuHolidaySlots")
    public Object[][] menuHolidaySlots() {
        return new Object[][]{{"3441"},{"4025"},{"1052821"}};
    }

    @DataProvider(name = "menuHolidaySlotsError")
    public Object[][] menuHolidaySlotsError() {
        return new Object[][]{{"406990323"},{"4070003343433"}};
    }

    @DataProvider(name="availcartapi")
    public Object[][] availcartapi(){return new Object[][]{{"443259","443259"}};}
}
