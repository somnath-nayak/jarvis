package com.swiggy.api.erp.cms.tests;

import java.util.List;

import org.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.dp.ThirdPartDP;
import com.swiggy.api.erp.cms.helper.FullMenuHelper;
import com.swiggy.api.erp.cms.helper.ThirdPartyServicesHelper;

import framework.gameofthrones.JonSnow.Processor;

public class ThirdPartyTests extends ThirdPartDP {
	
	
	@Test(dataProvider="FetchDominozMenu", priority=1, groups={"Sanity_TC", "Smoke_TC", "Regression_TC"}, 
			description="Fetch Third Part Menu")
	public void fetchDOMINOSMenu(String restIds, String typeOfPartner, String mealType)
			throws JSONException, InterruptedException
	{
		
		 ThirdPartyServicesHelper helper = new ThirdPartyServicesHelper();
	     SoftAssert softAssert = new SoftAssert();
	     Processor processor = helper.fetchDOMINOSMenu(restIds, typeOfPartner,mealType);
	   
	     softAssert.assertTrue(helper.verifyResponse(processor));
	     softAssert.assertAll();
	     
	     
	     System.out.println("restaurant id"+restIds+typeOfPartner+mealType);
        
	     softAssert.assertAll();
	}
	
	
	@Test(dataProvider="FetchMCDMenu", priority=1, groups={"Sanity_TC", "Smoke_TC", "Regression_TC"}, 
			description="Fetch Third Part Menu")
	public void fetchMCDMenu(String restIds, String typeOfPartner, String mealType)
			throws JSONException, InterruptedException
	{
		
		 ThirdPartyServicesHelper helper = new ThirdPartyServicesHelper();
	     SoftAssert softAssert = new SoftAssert();
	     Processor processor = helper.fetchMCDMenu(restIds, typeOfPartner,mealType);
	   
	     softAssert.assertTrue(helper.verifyResponse(processor));
	     softAssert.assertAll();
	     
	     
	     System.out.println("restaurant id"+restIds+typeOfPartner+mealType);
        
	     softAssert.assertAll();
	}
	
	

}
