package com.swiggy.api.erp.cms.pojo;

import java.io.IOException;

public class ItemSlotBuilder {
    private ItemSlot itemSlot;
    String id;

    public ItemSlotBuilder()
    {
        itemSlot=new ItemSlot();
    }

    public ItemSlotBuilder data(ItemData data) {
        itemSlot.setData(data);
        return this;
    }

    public ItemSlotBuilder userMeta(ItemUserMeta userMeta) {
        itemSlot.setUserMeta(userMeta);
        return this;
    }

    private void defaultItemSlot() throws IOException {
        if(itemSlot.getData()==null){
            itemSlot.setData(new ItemData(22448, 1, 1230,1240));
        }
        if(itemSlot.getUserMeta()==null){
            itemSlot.setUserMeta(new ItemUserMeta("cms",new ItemMeta("cms-tester")));
        }
    }

    public ItemSlot buildItemSlot() throws IOException {
        defaultItemSlot();
        return itemSlot;
    }

}
