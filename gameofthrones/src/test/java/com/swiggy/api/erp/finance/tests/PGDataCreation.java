package com.swiggy.api.erp.finance.tests;

import com.swiggy.api.erp.finance.constants.PgConstants;
import com.swiggy.api.erp.finance.dp.FinanceRegressionDP;
import com.swiggy.api.erp.finance.helper.NodalHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import java.util.HashMap;
import java.util.Random;


public class PGDataCreation extends FinanceRegressionDP {

    Random rn = new Random();
    int min=1;
    int max=9;
    int choice;
    private static Logger log = LoggerFactory.getLogger(FinanceRegression.class);
    NodalHelper nodalHelper=new NodalHelper();

    @BeforeClass
    public void createRestaurants() {
        HashMap<String, String> hm = createAllRestaurants();
        prepaid_restId_old_mou = hm.get("prepaid_old_mou");
        prepaid_restId_new_mou = hm.get("prepaid_new_mou");
        postpaid_restId_new_mou = hm.get("postpaid_new_mou");
        postpaid_restId_old_mou = hm.get("postpaid_old_mou");
        log.info("--RestID_prepaid_old_mou ::: " + prepaid_restId_old_mou);
        log.info("--RestID_prepaid_new_mou ::: " + prepaid_restId_new_mou);
        log.info("--RestID_postpaid_new_mou ::: " + postpaid_restId_new_mou);
        log.info("--RestID_postpaid_old_mou ::: " + postpaid_restId_old_mou);
    }
    @Test(dataProvider = "OrderJsonsDP")
    public void PGOrdersMultiTDDelivered(String orderID, String orderTime){
        System.out.println("current order_time ::: " + orderTime);
        String payment_method= nodalHelper.getOrderTableEntity("payment_method",orderID);
        int choice=rn.nextInt(max - min + 1) + min;
        if(!payment_method.equalsIgnoreCase("cash")){
            nodalHelper.pg_transaction_new_entity_creator(orderID, orderTime,PgConstants.txn_type_forward,choice);
            String order_from_pg=nodalHelper.getOrderFromPGTxnNewTable(orderID,PgConstants.txn_type_forward);
            Assert.assertEquals(order_from_pg,orderID,"order id not present in pg_transactions_new table");
        }
    }

    @Test(dataProvider = "DP_FOR_POP_SUPER_NORMAL")
    public void PGOrderPOPSUPER(String orderID, String orderTime){
        System.out.println("current order_time ::: " + orderTime);
        String payment_method= nodalHelper.getOrderTableEntity("payment_method",orderID);
        int choice=rn.nextInt(max - min + 1) + min;
        if(!payment_method.equalsIgnoreCase("cash")){
            nodalHelper.pg_transaction_new_entity_creator(orderID, orderTime,PgConstants.txn_type_forward,choice);
            String order_from_pg=nodalHelper.getOrderFromPGTxnNewTable(orderID,PgConstants.txn_type_forward);
            Assert.assertEquals(order_from_pg,orderID,"order id not present in pg_transactions_new table");
        }
    }

    @Test(dataProvider = "CancelOrderJsonsDP")
     public  void PGdataForCancelOrder(String orderID, String orderTime){
        System.out.println("current order_time ::: " + orderTime);
        String payment_method= nodalHelper.getOrderTableEntity("payment_method",orderID);
        int choice=rn.nextInt(max - min + 1) + min;
        if(!payment_method.equalsIgnoreCase("cash")){
            nodalHelper.pg_transaction_new_entity_creator(orderID, orderTime,PgConstants.txn_type_forward,choice);
            String order_from_pg=nodalHelper.getOrderFromPGTxnNewTable(orderID,PgConstants.txn_type_forward);
            Assert.assertEquals(order_from_pg,orderID,"order id not present for forward transaction in pg_transactions_new table");
            nodalHelper.pg_transaction_new_entity_creator(orderID, orderTime,PgConstants.txn_type_refund,choice);
            String order_from_pg1=nodalHelper.getOrderFromPGTxnNewTable(orderID,PgConstants.txn_type_refund);
            Assert.assertEquals(order_from_pg1,orderID,"order id not present for refund transaction in pg_transactions_new table");
        }

    }
    @Test(dataProvider = "DP_FOR_CANCEL_POP_SUPER_NORMAL")
    public  void PGdataForCancelOrderSUPERPOP(String orderID, String orderTime){
        System.out.println("current order_time ::: " + orderTime);
        String payment_method= nodalHelper.getOrderTableEntity("payment_method",orderID);
        System.out.println("payment method is "+payment_method);
        int choice=rn.nextInt(max - min + 1) + min;
        if(!payment_method.equalsIgnoreCase("cash")){
            nodalHelper.pg_transaction_new_entity_creator(orderID, orderTime,PgConstants.txn_type_forward,choice);
            String order_from_pg=nodalHelper.getOrderFromPGTxnNewTable(orderID,PgConstants.txn_type_forward);
            Assert.assertEquals(order_from_pg,orderID,"order id not present for forward transaction in pg_transactions_new table");
            nodalHelper.pg_transaction_new_entity_creator(orderID, orderTime,PgConstants.txn_type_refund,choice);
            String order_from_pg1=nodalHelper.getOrderFromPGTxnNewTable(orderID,PgConstants.txn_type_refund);
            Assert.assertEquals(order_from_pg1,orderID,"order id not present for refund transaction in pg_transactions_new table");
        }
    }




}
