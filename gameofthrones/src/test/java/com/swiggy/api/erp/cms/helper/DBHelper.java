package com.swiggy.api.erp.cms.helper;
import com.mongodb.*;


public class DBHelper {
    public static DB db = null;
    public DBHelper(){


    }

    public static DB connectDb() {
        try {
            MongoClient mongo = new MongoClient("cms-u-cuat-catalog2.0.swiggyops.de", 27018);
            db = mongo.getDB("test");

        } catch (Exception e) {
            e.printStackTrace();

        }
        return db;
    }

    /**
     * Get First value of given key
     *
     * @param collectionname
     * @param key
     * @return
     */

    public static Object getCollectionData(String collectionname, String key) {
        DBCollection collection = db.getCollection(collectionname);
        DBObject dbo = collection.findOne();
        Object ob = dbo.get(key);
        return ob;
    }

    /**
     * Get N-number of value of given key
     *
     * @param collectionname
     * @param key
     * @param count
     * @return
     */

    public static void getCollectionData(String collectionname, String key, int count) {
        DBCollection collection = db.getCollection(collectionname);
        BasicDBObject allQuery = new BasicDBObject();
        BasicDBObject fields = new BasicDBObject();
        DBCursor cursor = collection.find(allQuery, fields).limit(count);
        while (cursor.hasNext()) {
            System.out.print(cursor.next().get(key)+",");
        }
    }
        /**
         * Delete whole collection data
         *
         */

        public static void deleteCollectionData (String collectionname){
            db.getCollection("collectionname").remove(new BasicDBObject());
        }

    /**
     * Delete whole collection data
     *
     */

    public static void deleteCollectionDataUsingFeilds (String collectionname, String key, String value){
        BasicDBObject query = new BasicDBObject();
        query.put(key,value);
        db.getCollection("collectionname").remove(query);
    }

    }
