package com.swiggy.api.erp.delivery.dp;

import java.io.IOException;

import org.testng.annotations.DataProvider;

import com.swiggy.api.erp.delivery.constants.RTSConstants;
import com.swiggy.api.erp.delivery.helper.DeliveryHelperMethods;

public class ItemLimiter_DataProvider {

	static DeliveryHelperMethods delmeth=new DeliveryHelperMethods();
	@DataProvider(name = "Item_limit_2")
	public Object[][] IL1() throws IOException, InterruptedException {
		delmeth.dbhelperupdate("Update zone set is_open=1 where id=4");
		delmeth.dbhelperupdate(RTSConstants.set_max_item_cart_less);
		delmeth.dbhelperupdate(RTSConstants.set_max_total_item_cart_more);
		return new Object[][] { {} };
	}
	@DataProvider(name = "Item_limit_3")
	public Object[][] IL3() throws IOException, InterruptedException {
		delmeth.dbhelperupdate(RTSConstants.set_max_item_cart_more);
		delmeth.dbhelperupdate(RTSConstants.set_max_total_item_cart_less);
		return new Object[][] { {} };
	}
	@DataProvider(name = "Item_limit_4")
	public Object[][] IL4() throws IOException, InterruptedException {
		delmeth.dbhelperupdate(RTSConstants.set_max_item_cart_less);
		delmeth.dbhelperupdate(RTSConstants.set_max_total_item_cart_less);
		return new Object[][] { {} };
	}
	@DataProvider(name = "Item_limit_5")
	public Object[][] IL5() throws IOException, InterruptedException {
		delmeth.dbhelperupdate(RTSConstants.delete_max_item_cart_key);
		delmeth.dbhelperupdate(RTSConstants.delete_max_total_item_count_key);
		return new Object[][] { {} };
	}
}