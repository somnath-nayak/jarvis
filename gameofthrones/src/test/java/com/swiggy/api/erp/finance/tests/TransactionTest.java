package com.swiggy.api.erp.finance.tests;

import com.swiggy.api.erp.finance.constants.TransactionConstants;
import com.swiggy.api.erp.finance.dp.TransactionDp;
import com.swiggy.api.erp.finance.helper.CashMgmtHelper;
import com.swiggy.api.erp.finance.helper.DEHelper;
import com.swiggy.api.erp.finance.helper.TransactionsHelper;
import com.swiggy.api.erp.finance.helper.ZoneHelper;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Created by kiran.j on 7/11/18.
 */
public class TransactionTest extends TransactionDp{

    TransactionsHelper helper = new TransactionsHelper();
    CashMgmtHelper cashMgmtHelper = new CashMgmtHelper();
    TransactionDp transactionDp = new TransactionDp();
    ZoneHelper zoneHelper = new ZoneHelper();
    DEHelper deHelper = new DEHelper();

    @Test(dataProvider = "getTransaction", description = "Verifies Created  DE Transactions")
    public void getTransactionTest(String de_id, String session_id, String transaction_id, String[] errors) {
        Processor processor = helper.getTransactions(de_id, session_id, transaction_id);
        Assert.assertTrue(helper.verifyTransactionResponse(de_id, session_id, processor, errors), "Response Validation Failed");
    }

    @Test(dataProvider = "addTransaction", description = "Verifies Add Transaction for DE")
    public void addTransactionTest(String de_id, String description, String order_id, String de_incoming, String de_outgoing, String txn_type) {
        Processor processor = helper.addPocketingAndPayoutTxn(de_id, description, order_id, de_incoming, de_outgoing, txn_type);
        Assert.assertTrue(helper.getTransactions(order_id).size() > 0 , "Transactions is not created");
        Assert.assertTrue(helper.getCashSessions(de_id).get(0).get("floating_cash").toString().equalsIgnoreCase(TransactionConstants.pocketing_de_incoming),"Floating Cash Not updated");
    }

    @Test(dataProvider = "submitRevertTxn", description = "Verifies Revert Existing Transaction")
    public void submitRevertTest(String txn_id, String reason, String user_id, String email, int status_code, boolean check) {
        Processor processor = helper.submitRevertRequest(txn_id, reason, user_id, email);
        if(check)
            Assert.assertTrue((int)helper.getPocketingTxn(txn_id).get(0).get("pocket_revert_status") == TransactionConstants.submit_revert_status);
        else
            Assert.assertTrue(helper.verifyStatusCode(processor, status_code));
    }

    @Test(dataProvider = "acceptRevertTxn", description = "Verifies Accept Revert Existing Transaction")
    public void acceptRevertTest(String txn_id, String reason, String user_id, String email, int status_code, boolean check) {
        Processor processor = helper.acceptRevertRequest(txn_id, reason, user_id, email);
        if(check)
            Assert.assertTrue((int)helper.getPocketingTxn(txn_id).get(0).get("pocket_revert_status") == TransactionConstants.accept_revert_status);
        else
            Assert.assertTrue(helper.verifyStatusCode(processor, status_code));
        Assert.assertTrue(helper.verifyStatusCode(processor, status_code));
    }

    @Test(dataProvider = "denyRevertTxn", description = "Verifies Deny Revert Existing Transaction")
    public void denyRevertTest(String txn_id, String reason, String user_id, String email, int status_code, boolean check) {
        Processor processor = helper.denyRevertRequest(txn_id, reason, user_id, email);
        if(check)
            Assert.assertTrue((int)helper.getPocketingTxn(txn_id).get(0).get("pocket_revert_status") == TransactionConstants.deny_revert_status);
        else
            Assert.assertTrue(helper.verifyStatusCode(processor, status_code));
        Assert.assertTrue(helper.verifyStatusCode(processor, status_code));
    }

    @Test(dataProvider = "getNovoPay", description = "Verify Get Novo Pay Transaction")
    public void getNovoPayTest(String modile_no, String[] status) {
        Processor processor = helper.getNovoPay(modile_no);
        Assert.assertTrue(helper.verifyStatusCode(processor, status));
    }

    @Test(dataProvider = "depositNovoPay", description = "Verify Deposit Novo Pay Transaction")
    public void depositNovoPayTest(String de_id, String mobile_no, String amount, String txn_id, String[] status, int cash) {
        Processor processor = helper.depositNovoPay(mobile_no, amount, txn_id);
        Assert.assertTrue(helper.verifyStatusCode(processor, status));
        if(de_id != null) {
            Assert.assertTrue((helper.getFloatingCash(de_id) + Integer.parseInt(TransactionConstants.amt) == cash), "Transaction was not Deposited");
            cashMgmtHelper.closeSessionHelper(de_id);
        }
    }

    @Test(dataProvider = "revertTxn", description = "Verify revert Transaction")
    public void revertTxnTest(String de_id, String txn_id, String order_id, String[] errors, boolean check) {
        int de_incoming = (int)helper.getTransactions(order_id).get(0).get("de_incoming");
        Processor processor = helper.revertTxnHelper(de_id, txn_id);
        if(check) {
            int updated_de_incoming = (int)helper.getTransactions(order_id).get(0).get("de_incoming");
            Assert.assertTrue(helper.getFloatingCash(de_id) == 0 , "Transaction is Not Reverted");
            Assert.assertTrue(de_incoming > 0 && updated_de_incoming < 0, "Transaction is Not Reverted");
        }
        else
            Assert.assertTrue(helper.verifyRevertStatusCode(processor, errors));
    }

    @Test(dataProvider = "updateTxn", description = "Verify Update Transaction")
    public void updateTxnTest(String de_id, String txn_id, String order_id, String inconsitency, String[] errors, boolean check) {
        Processor processor = helper.updateTxn(de_id, txn_id , inconsitency);
        if(check)
            Assert.assertTrue(helper.deCashAudit(txn_id).size() > 0, "Transaction Not Updated");
        else
            Assert.assertTrue(helper.verifyRevertStatusCode(processor, errors));
    }

    @Test(dataProvider = "manualTransaction", description = "Verify Add Manual Transaction")
    public void addManualTransaction(String de_id, String description, String order_id, String de_incoming, String de_outgoing, String txn_type, String expected_txn_type) {
        Processor processor = helper.addTransactionHelper(de_id, description, order_id, de_incoming, de_outgoing, txn_type);
        Map<String, Object> map = helper.getTransactions(order_id).get(0);
        Assert.assertTrue((map.size() > 0) && map.get("transaction_type").toString().equalsIgnoreCase(expected_txn_type), "Transaction is not Added");
        Assert.assertTrue(helper.getFloatingCash(de_id) > 0, "Transaction is not Added in de_cash_sessions");
    }

    @Test(dataProvider = "addPocketing", description = "Verify Add Pocketing Transaction")
    public void addPocketingTest(String de_id, String city, String zone, String from_date, String to_date, boolean check) {
        Processor processor = helper.addPocketing(de_id, city, zone, from_date, to_date);
        if(!check)
            Assert.assertTrue(helper.verifyTxnList(processor), "Pocketing Transaction is Not Empty for Invalid De Id");
        helper.getPocketingTxns(processor, de_id, from_date, to_date);
        System.out.println(processor.ResponseValidator.GetBodyAsText());
    }

    @Test(dataProvider = "addPocketingOps", description = "Verify Add Pocketing Transaction")
    public void addPocketingOpsTest(String de_id, String city, String zone, String from_date, String to_date, boolean check) {
        Processor processor = helper.addPocketingOps(de_id, city, zone, from_date, to_date);
        if(!check)
            Assert.assertTrue(helper.verifyTxnList(processor), "Pocketing Transaction is Not Empty for Invalid De Id");
        helper.getPocketingTxns(processor, de_id, from_date, to_date);
        System.out.println(processor.ResponseValidator.GetBodyAsText());
    }

    @Test(dataProvider = "cdmDeposit", description = "Verify CDM Deposit Transactions")
    public void cdmDepositTest(String payload, String de_id, String expected_txn_type) throws IOException {
        Processor processor = helper.cdmiciciDeposit(payload);
        System.out.println(processor.ResponseValidator.GetResponseCode());
        Map<String, Object> map = helper.getLastTransactions(de_id).get(0);
        List<Map<String, Object>> l = helper.deCashAuditByDe(de_id);
        Assert.assertTrue((map.size() > 0) && map.get("transaction_source").toString().equalsIgnoreCase(expected_txn_type), "Transaction is not Added");
        Assert.assertTrue(helper.getCashSessions(de_id).size() > 0, "Transaction Not Updated");
        Assert.assertTrue(l.size() > 0 && l.get(0).get("user").toString().equalsIgnoreCase(expected_txn_type), "Transaction is not Added");
    }

    @Test(dataProvider = "cdmValidate", description = "Verify CDM Validate Transactions")
    public void cdmValidateTest(String payload, String de_id, int status_code) throws IOException {
        Processor processor = helper.cdmiciciValidate(payload);
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), status_code, "Transaction Validation Failed");
    }

    @Test(dataProvider = "upiDeposit", description = "Verify UPI Deposit Transactions")
    public void upiDepositTest(String payload, String de_id, String expected_txn_type, boolean valid) throws IOException {
        Processor processor = helper.upiiciciDeposit(payload);
        if(valid) {
            Map<String, Object> map = helper.getLastTransactions(de_id).get(0);
            List<Map<String, Object>> l = helper.deCashAuditByDe(de_id);
            Assert.assertTrue((map.size() > 0) && map.get("transaction_source").toString().equalsIgnoreCase(expected_txn_type), "Transaction is not Added");
            Assert.assertTrue(helper.getCashSessions(de_id).size() > 0, "Transaction Not Updated");
            Assert.assertTrue(l.size() > 0 && l.get(0).get("user").toString().equalsIgnoreCase("ICICI"), "Transaction is not Added");
        } else {
            Assert.assertTrue(processor.ResponseValidator.GetNodeValue("txn_status").equalsIgnoreCase("REJECT"), "Transaction Should not be Added");
        }
    }

    @Test(dataProvider = "createZone", description = "Verify Different Scenarios on Create Zone", enabled = false)
    public void createZoneTest(String zone_id, String area_id, int validate) {
        zoneHelper.createZone(zone_id, area_id);
        Assert.assertEquals(zoneHelper.getZoneDetails(zone_id).size(), validate, "Zone Create Check Failed");
    }

    @Test(dataProvider = "createDe", description = "Verify Different Scenarios on Create Delivery Boy", enabled = false)
    public void createDeTest(String action, String de_id, int validate) {
        deHelper.deAction(action, de_id);
        Assert.assertEquals(deHelper.getDeDetails(de_id).size(), validate, "Delivery Boy Create Check Failed");
    }

    @AfterClass
    public void closeSession() {
        cashMgmtHelper.closeSessionHelper(transactionDp.getMap().get(TransactionConstants.de_id));
    }
}
