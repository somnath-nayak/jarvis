package com.swiggy.api.erp.ff.constants;

public interface LosConstants {
	
	String hostName = "oms";
	String deIncomingDeadLetter = "ff-order-component-updates-dead-letters";
	String createOrdersQueue = "swiggy.orders";
	String deIncomingExchange = "swiggy.order_collect";
	String statusUpdateQueue="swiggy.order_status";
	String rmsUpdatePartnerQueue="swiggy.partner_update";
	String rmsUpdateDeadLetterQueue="ff-rms-status-update-dead-letters";
	String external_order_id="123";
	String wrong_external_order_id="test_123";
	String statusUpdateFailedQueue="ff-order-status-update-dead-letters";
	String order_id = "${order_id}";
	String food_prepared = "${food_prepared}";
	String batch_id = "${batch_id}";
	String time = "${time}";
	String restaurant_bill = "${restaurant_bill}";
	String de_spending = "${de_spending}";
	String de_incoming = "${de_incoming}";
	String[] status = {"assigned","confirmed", "arrived", "pickedup", "reached", "delivered"};
	String cancelStatus = "cancelled";
	String unassignStatus = "unassigned";
	//Verification BCP
	String verification_queue= "swiggy.orders";
	String Verify_force_queue="ff-verification-force-relay-orders-exchange";
	String manualOrder = "swiggy.ff_manual_order";
}
