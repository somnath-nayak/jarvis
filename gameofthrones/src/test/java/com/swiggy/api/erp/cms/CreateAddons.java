package com.swiggy.api.erp.cms;

import com.swiggy.api.erp.cms.constants.MenuConstants;
import com.swiggy.api.erp.cms.pojo.Items.Gst_details;

/**
 * Created by kiran.j on 3/13/18.
 */
public class CreateAddons
{
    private String created_by;

    private boolean in_stock;

    private boolean is_veg;

    private String restaurant_id;

    private String inc_price;

    private String third_party_id;

    private String id;

    private String[] addon_group_ids;

    private String updated_at;

    private String price;

    private int order;

    private String name;

    private Gst_details gst_details;

    private String updated_by;

    private boolean active;

    private String created_at;

    public String getCreated_by ()
    {
        return created_by;
    }

    public void setCreated_by (String created_by)
    {
        this.created_by = created_by;
    }

    public boolean getIn_stock ()
    {
        return in_stock;
    }

    public void setIn_stock (boolean in_stock)
    {
        this.in_stock = in_stock;
    }

    public boolean getIs_veg ()
    {
        return is_veg;
    }

    public void setIs_veg (boolean is_veg)
    {
        this.is_veg = is_veg;
    }

    public String getRestaurant_id ()
    {
        return restaurant_id;
    }

    public void setRestaurant_id (String restaurant_id)
    {
        this.restaurant_id = restaurant_id;
    }

    public String getInc_price ()
    {
        return inc_price;
    }

    public void setInc_price (String inc_price)
    {
        this.inc_price = inc_price;
    }

    public String getThird_party_id ()
    {
        return third_party_id;
    }

    public void setThird_party_id (String third_party_id)
    {
        this.third_party_id = third_party_id;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String[] getAddon_group_ids ()
    {
        return addon_group_ids;
    }

    public void setAddon_group_ids (String[] addon_group_ids)
    {
        this.addon_group_ids = addon_group_ids;
    }

    public String getUpdated_at ()
    {
        return updated_at;
    }

    public void setUpdated_at (String updated_at)
    {
        this.updated_at = updated_at;
    }

    public String getPrice ()
    {
        return price;
    }

    public void setPrice (String price)
    {
        this.price = price;
    }

    public int getOrder ()
    {
        return order;
    }

    public void setOrder (int order)
    {
        this.order = order;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public Gst_details getGst_details ()
    {
        return gst_details;
    }

    public void setGst_details (Gst_details gst_details)
    {
        this.gst_details = gst_details;
    }

    public String getUpdated_by ()
    {
        return updated_by;
    }

    public void setUpdated_by (String updated_by)
    {
        this.updated_by = updated_by;
    }

    public boolean getActive ()
    {
        return active;
    }

    public void setActive (boolean active)
    {
        this.active = active;
    }

    public String getCreated_at ()
    {
        return created_at;
    }

    public void setCreated_at (String created_at)
    {
        this.created_at = created_at;
    }

    public CreateAddons build() {
        Gst_details gst_details = new Gst_details();

        if(this.getName() == null)
            this.setName(MenuConstants.variants_name);

        this.setPrice(Double.toString(MenuConstants.default_price));
        this.setGst_details(gst_details);
        this.setOrder(MenuConstants.enabled);


        return this;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [created_by = "+created_by+", in_stock = "+in_stock+", is_veg = "+is_veg+", restaurant_id = "+restaurant_id+", inc_price = "+inc_price+", third_party_id = "+third_party_id+", id = "+id+", addon_group_ids = "+addon_group_ids+", updated_at = "+updated_at+", price = "+price+", default = "+order+", name = "+name+", gst_details = "+gst_details+", updated_by = "+updated_by+", active = "+active+", created_at = "+created_at+"]";
    }
}