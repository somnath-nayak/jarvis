package com.swiggy.api.erp.vms.td.pojo;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
"id",
"name",
"city",
"area",
"recommended",
"costForTwo",
"deliveryCharge",
"minimumOrder",
"opened",
"deliveryTime",
"tmpClosed",
"postalCode",
"cityDeliveryCharge",
"threshold",
"discountType",
"tradeCampaignHeaders",
"select",
"new",
"categories"
})

/***
 * 
 * @author ramzi
 *
 */

public class Restaurant {

@JsonProperty("id")
private Integer id;
@JsonProperty("name")
private String name;
@JsonProperty("city")
private Integer city;
@JsonProperty("area")
private String area;
@JsonProperty("recommended")
private Integer recommended;
@JsonProperty("costForTwo")
private Integer costForTwo;
@JsonProperty("deliveryCharge")
private Integer deliveryCharge;
@JsonProperty("minimumOrder")
private Integer minimumOrder;
@JsonProperty("opened")
private Integer opened;
@JsonProperty("deliveryTime")
private Integer deliveryTime;
@JsonProperty("tmpClosed")
private Integer tmpClosed;
@JsonProperty("postalCode")
private Integer postalCode;
@JsonProperty("cityDeliveryCharge")
private Integer cityDeliveryCharge;
@JsonProperty("threshold")
private Integer threshold;
@JsonProperty("discountType")
private Integer discountType;
@JsonProperty("tradeCampaignHeaders")
private List<Object> tradeCampaignHeaders = null;
@JsonProperty("select")
private String select;
@JsonProperty("new")
private String _new;
@JsonProperty("categories")
private List<Category> categories = null;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

/**
* No args constructor for use in serialization
*
*/
public Restaurant() {
}

/**
*
* @param select
* @param minimumOrder
* @param tmpClosed
* @param recommended
* @param deliveryCharge
* @param threshold
* @param city
* @param tradeCampaignHeaders
* @param cityDeliveryCharge
* @param id
* @param area
* @param postalCode
* @param discountType
* @param deliveryTime
* @param name
* @param _new
* @param categories
* @param costForTwo
* @param opened
*/
public Restaurant(Integer id, String name, Integer city, String area, Integer recommended, Integer costForTwo, Integer deliveryCharge, Integer minimumOrder, Integer opened, Integer deliveryTime, Integer tmpClosed, Integer postalCode, Integer cityDeliveryCharge, Integer threshold, Integer discountType, List<Object> tradeCampaignHeaders, String select, String _new, List<Category> categories) {
super();
this.id = id;
this.name = name;
this.city = city;
this.area = area;
this.recommended = recommended;
this.costForTwo = costForTwo;
this.deliveryCharge = deliveryCharge;
this.minimumOrder = minimumOrder;
this.opened = opened;
this.deliveryTime = deliveryTime;
this.tmpClosed = tmpClosed;
this.postalCode = postalCode;
this.cityDeliveryCharge = cityDeliveryCharge;
this.threshold = threshold;
this.discountType = discountType;
this.tradeCampaignHeaders = tradeCampaignHeaders;
this.select = select;
this._new = _new;
this.categories = categories;
}

@JsonProperty("id")
public Integer getId() {
return id;
}

@JsonProperty("id")
public void setId(Integer id) {
this.id = id;
}

public Restaurant withId(Integer id) {
this.id = id;
return this;
}

@JsonProperty("name")
public String getName() {
return name;
}

@JsonProperty("name")
public void setName(String name) {
this.name = name;
}

public Restaurant withName(String name) {
this.name = name;
return this;
}

@JsonProperty("city")
public Integer getCity() {
return city;
}

@JsonProperty("city")
public void setCity(Integer city) {
this.city = city;
}

public Restaurant withCity(Integer city) {
this.city = city;
return this;
}

@JsonProperty("area")
public String getArea() {
return area;
}

@JsonProperty("area")
public void setArea(String area) {
this.area = area;
}

public Restaurant withArea(String area) {
this.area = area;
return this;
}

@JsonProperty("recommended")
public Integer getRecommended() {
return recommended;
}

@JsonProperty("recommended")
public void setRecommended(Integer recommended) {
this.recommended = recommended;
}

public Restaurant withRecommended(Integer recommended) {
this.recommended = recommended;
return this;
}

@JsonProperty("costForTwo")
public Integer getCostForTwo() {
return costForTwo;
}

@JsonProperty("costForTwo")
public void setCostForTwo(Integer costForTwo) {
this.costForTwo = costForTwo;
}

public Restaurant withCostForTwo(Integer costForTwo) {
this.costForTwo = costForTwo;
return this;
}

@JsonProperty("deliveryCharge")
public Integer getDeliveryCharge() {
return deliveryCharge;
}

@JsonProperty("deliveryCharge")
public void setDeliveryCharge(Integer deliveryCharge) {
this.deliveryCharge = deliveryCharge;
}

public Restaurant withDeliveryCharge(Integer deliveryCharge) {
this.deliveryCharge = deliveryCharge;
return this;
}

@JsonProperty("minimumOrder")
public Integer getMinimumOrder() {
return minimumOrder;
}

@JsonProperty("minimumOrder")
public void setMinimumOrder(Integer minimumOrder) {
this.minimumOrder = minimumOrder;
}

public Restaurant withMinimumOrder(Integer minimumOrder) {
this.minimumOrder = minimumOrder;
return this;
}

@JsonProperty("opened")
public Integer getOpened() {
return opened;
}

@JsonProperty("opened")
public void setOpened(Integer opened) {
this.opened = opened;
}

public Restaurant withOpened(Integer opened) {
this.opened = opened;
return this;
}

@JsonProperty("deliveryTime")
public Integer getDeliveryTime() {
return deliveryTime;
}

@JsonProperty("deliveryTime")
public void setDeliveryTime(Integer deliveryTime) {
this.deliveryTime = deliveryTime;
}

public Restaurant withDeliveryTime(Integer deliveryTime) {
this.deliveryTime = deliveryTime;
return this;
}

@JsonProperty("tmpClosed")
public Integer getTmpClosed() {
return tmpClosed;
}

@JsonProperty("tmpClosed")
public void setTmpClosed(Integer tmpClosed) {
this.tmpClosed = tmpClosed;
}

public Restaurant withTmpClosed(Integer tmpClosed) {
this.tmpClosed = tmpClosed;
return this;
}

@JsonProperty("postalCode")
public Integer getPostalCode() {
return postalCode;
}

@JsonProperty("postalCode")
public void setPostalCode(Integer postalCode) {
this.postalCode = postalCode;
}

public Restaurant withPostalCode(Integer postalCode) {
this.postalCode = postalCode;
return this;
}

@JsonProperty("cityDeliveryCharge")
public Integer getCityDeliveryCharge() {
return cityDeliveryCharge;
}

@JsonProperty("cityDeliveryCharge")
public void setCityDeliveryCharge(Integer cityDeliveryCharge) {
this.cityDeliveryCharge = cityDeliveryCharge;
}

public Restaurant withCityDeliveryCharge(Integer cityDeliveryCharge) {
this.cityDeliveryCharge = cityDeliveryCharge;
return this;
}

@JsonProperty("threshold")
public Integer getThreshold() {
return threshold;
}

@JsonProperty("threshold")
public void setThreshold(Integer threshold) {
this.threshold = threshold;
}

public Restaurant withThreshold(Integer threshold) {
this.threshold = threshold;
return this;
}

@JsonProperty("discountType")
public Integer getDiscountType() {
return discountType;
}

@JsonProperty("discountType")
public void setDiscountType(Integer discountType) {
this.discountType = discountType;
}

public Restaurant withDiscountType(Integer discountType) {
this.discountType = discountType;
return this;
}

@JsonProperty("tradeCampaignHeaders")
public List<Object> getTradeCampaignHeaders() {
return tradeCampaignHeaders;
}

@JsonProperty("tradeCampaignHeaders")
public void setTradeCampaignHeaders(List<Object> tradeCampaignHeaders) {
this.tradeCampaignHeaders = tradeCampaignHeaders;
}

public Restaurant withTradeCampaignHeaders(List<Object> tradeCampaignHeaders) {
this.tradeCampaignHeaders = tradeCampaignHeaders;
return this;
}

@JsonProperty("select")
public String getSelect() {
return select;
}

@JsonProperty("select")
public void setSelect(String select) {
this.select = select;
}

public Restaurant withSelect(String select) {
this.select = select;
return this;
}

@JsonProperty("new")
public String getNew() {
return _new;
}

@JsonProperty("new")
public void setNew(String _new) {
this._new = _new;
}

public Restaurant withNew(String _new) {
this._new = _new;
return this;
}

@JsonProperty("categories")
public List<Category> getCategories() {
return categories;
}

@JsonProperty("categories")
public void setCategories(List<Category> categories) {
this.categories = categories;
}

public Restaurant withCategories(List<Category> categories) {
this.categories = categories;
return this;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

public Restaurant withAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
return this;
}

@Override
public String toString() {
return new ToStringBuilder(this).append("id", id).append("name", name).append("city", city).append("area", area).append("recommended", recommended).append("costForTwo", costForTwo).append("deliveryCharge", deliveryCharge).append("minimumOrder", minimumOrder).append("opened", opened).append("deliveryTime", deliveryTime).append("tmpClosed", tmpClosed).append("postalCode", postalCode).append("cityDeliveryCharge", cityDeliveryCharge).append("threshold", threshold).append("discountType", discountType).append("tradeCampaignHeaders", tradeCampaignHeaders).append("select", select).append("_new", _new).append("categories", categories).append("additionalProperties", additionalProperties).toString();
}

}