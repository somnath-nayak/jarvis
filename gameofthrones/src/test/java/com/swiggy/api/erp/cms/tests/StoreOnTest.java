package com.swiggy.api.erp.cms.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.dp.SelfServeDP;
import org.testng.annotations.Test;
import framework.gameofthrones.Aegon.Initialize;
import com.swiggy.api.erp.cms.helper.SelfServeHelper;
import org.testng.asserts.SoftAssert;


public class StoreOnTest extends SelfServeDP {

    SelfServeHelper selfServeHelper= new SelfServeHelper();
    Initialize gameofthrones = new Initialize();

   @Test(dataProvider = "storeAvailability")
    public void openCloseStore(int partnerType, String storeId, boolean option, String ontime, String offtime, String flag)
    {
        String response = selfServeHelper.postStoreAvailability(option, storeId, partnerType, ontime, offtime).ResponseValidator.GetBodyAsText().toString();
        System.out.println(response);
        SoftAssert sa = new SoftAssert();sa.assertEquals(storeId, (JsonPath.read(response,"$.store_id")).toString().replace("[","").replace("]",""));
        sa.assertEquals(Integer.toString(partnerType), (JsonPath.read(response,"$.partner_type")).toString().replace("[","").replace("]",""));
        sa.assertEquals(storeId, (JsonPath.read(response,"$.store_id")).toString().replace("[","").replace("]",""));
        sa.assertEquals(Boolean.toString(option), (JsonPath.read(response,"$.open")).toString().replace("[","").replace("]",""));
        if (flag.equals("negative"))
        {
            sa.assertTrue(JsonPath.read(response,"$.error.error_message").toString().length()>3);
        }
        sa.assertAll();
    }
}
