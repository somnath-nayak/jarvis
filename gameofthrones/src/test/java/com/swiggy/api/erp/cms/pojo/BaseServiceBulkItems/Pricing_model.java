package com.swiggy.api.erp.cms.pojo.BaseServiceBulkItems;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.erp.cms.pojo.BaseServiceBulkItems
 **/

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "addon_combinations",
        "price",
        "variations"
})
public class Pricing_model {

    @JsonProperty("addon_combinations")
    private List<Addon_combination> addon_combinations = null;
    @JsonProperty("price")
    private Integer price;
    @JsonProperty("variations")
    private List<Variation_> variations = null;

    @JsonProperty("addon_combinations")
    public List<Addon_combination> getAddon_combinations() {
        return addon_combinations;
    }

    @JsonProperty("addon_combinations")
    public void setAddon_combinations(List<Addon_combination> addon_combinations) {
        this.addon_combinations = addon_combinations;
    }

    @JsonProperty("price")
    public Integer getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(Integer price) {
        this.price = price;
    }

    @JsonProperty("variations")
    public List<Variation_> getVariations() {
        return variations;
    }

    @JsonProperty("variations")
    public void setVariations(List<Variation_> variations) {
        this.variations = variations;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("addon_combinations", addon_combinations).append("price", price).append("variations", variations).toString();
    }

}
