package com.swiggy.api.erp.ff.tests.los;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.swiggy.api.erp.ff.helper.LOSHelper;

import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Tyrion.RabbitMQHelper;
import com.swiggy.api.erp.ff.dp.losDataProvider;
import com.swiggy.api.erp.ff.constants.LosConstants;

public class StatusUpdateTests extends losDataProvider{

	RabbitMQHelper helper = new RabbitMQHelper();
	LOSHelper losHelper = new LOSHelper();
	Logger log = Logger.getLogger(StatusUpdateTests.class);
	
	//	String[] statusList = {"assigned", "confirmed", "arrived", "pickedup", "reached", "delivered"};

	public void statusUpdate(String order_id, String order_time, String status) throws InterruptedException, IOException {
		Assert.assertTrue(losHelper.validateOrderExistInOMSOrderTable(order_id, 10));
		losHelper.statusUpdate(order_id, order_time, status);
		losHelper.validateOrderStatusInOMSDB(order_id, status, 5);
		//log.info("********* Order Status is updated to " + order_status + " in OMS DB successfully *********");
	}

	@Test(dataProvider ="createDataForOrderCreation",groups = {"sanity", "regression"},description = "Create a manual order and traverse all the status updates while asserting the status from the DB")
	public void testStatusUpdateForManualOrder(String order_id,String order_time,String epochTime, String status, boolean check) throws SQLException, IOException, InterruptedException {
		log.info("***************************************** test StatusUpdateForManualOrder started *****************************************");

		//Create a manual order
		if(check)
			losHelper.createManualOrder(order_id,order_time,epochTime);
		statusUpdate(order_id, order_time, status);

		log.info("######################################### test StatusUpdateForManualOrder completed #########################################");
	}

	@Test(dataProvider = "createDataForOrderCreation",groups = {"sanity", "regression"},description = "Create a third party order and traverse all the status updates while asserting the status from the DB")
	public void testStatusUpdateForThirdPartyOrder(String order_id,String order_time,String epochTime, String status, boolean check) throws SQLException, IOException, InterruptedException {
		log.info("***************************************** test StatusUpdateForThirdPartyOrder started *****************************************");

		//Create a third party order
		if(check)
			losHelper.createThirdPartyOrder(order_id,order_time,epochTime);

		//Change the status of the created third party order from assigned to delivered
		statusUpdate(order_id, order_time, status);

		log.info("######################################### test StatusUpdateForThirdPartyOrder completed #########################################");
	}

	@Test(dataProvider = "createDataForOrderCreation",groups = {"sanity", "regression"},description = "Create a partner order and traverse all the status updates while asserting the status from the DB")
	public void testStatusUpdateForPartnerOrder(String order_id,String order_time,String epochTime, String status, boolean check) throws SQLException, IOException, InterruptedException {
		log.info("***************************************** test StatusUpdateForPartnerOrder started *****************************************");

		//Create a with partner order
		if(check)
			losHelper.createOrderWithPartner(order_id,order_time,epochTime);

		//Change the status of the created partner order from assigned to delivered
		statusUpdate(order_id, order_time, status);

		log.info("######################################### test StatusUpdateForPartnerOrder completed #########################################");
	}

	@Test(dataProvider = "createDataForOrderCreation",groups = {"sanity", "regression"},description = "Create a long distance order and traverse all the status updates while asserting the status from the DB")
	public void testStatusUpdateForLongDistanceOrder(String order_id,String order_time,String epochTime, String status, boolean check) throws SQLException, IOException, InterruptedException {
		log.info("***************************************** test StatusUpdateForLongDistanceOrder started *****************************************");

		//Create a long distance order
		if(check)
			losHelper.createLongDistanceOrder(order_id,order_time,epochTime);

		//Change the status of the long distance order from assigned to delivered
		statusUpdate(order_id, order_time, status);

		log.info("######################################### test StatusUpdateForLongDistanceOrder completed #########################################");
	}

	@Test(dataProvider = "createDataForOrderCreation",groups = {"sanity", "regression"},description = "Create a dominos order and traverse all the status updates while asserting the status from the DB")
	public void testStatusUpdateForDominosOrder(String order_id,String order_time,String epochTime, String status, boolean check) throws SQLException, IOException, InterruptedException {
		log.info("***************************************** test StatusUpdateForDominosOrder started *****************************************");

		//Create a Dominos order
		if(check)
			losHelper.createDominosOrder(order_id,order_time,epochTime);

		//Change the status of the Dominos order from assigned to delivered
		statusUpdate(order_id, order_time, status);

		log.info("######################################### test StatusUpdateForDominosOrder completed #########################################");
	}

	@Test(dataProvider = "createDataForOrderCreation",groups = {"sanity", "regression"},description = "Create a first order for new user and traverse all the status updates while asserting the status from the DB")
	public void testStatusUpdateForNewUserFirstOrder(String order_id,String order_time,String epochTime, String status, boolean check) throws SQLException, IOException, InterruptedException {
		log.info("***************************************** test StatusUpdateForNewUserFirstOrder started *****************************************");

		//Create a New User First order
		if(check)
			losHelper.createNewUsersFirstOrder(order_id,order_time,epochTime);

		//Change the status of the New User First order from assigned to delivered
		statusUpdate(order_id, order_time, status);

		log.info("######################################### test StatusUpdateForNewUserFirstOrder completed #########################################");
	}

	@Test(dataProvider = "createDataForOrderCreation",groups = {"sanity", "regression"},description = "Create a pop order and traverse all the status updates while asserting the status from the DB")
	public void testStatusUpdateForPopOrder(String order_id,String order_time,String epochTime, String status, boolean check) throws SQLException, IOException, InterruptedException {
		log.info("***************************************** test StatusUpdateForNewUserFirstOrder started *****************************************");

		//Create a POP order
		losHelper.createPOPOrder(order_id,order_time,epochTime);

		//Change the status of the POP order from assigned to delivered
		statusUpdate(order_id, order_time, status);

		log.info("######################################### test StatusUpdateForNewUserFirstOrder completed #########################################");
	}

	@Test(dataProvider = "createDataForOrderCreation",groups = {"sanity", "regression"},description = "Create a swiggy assured order and traverse all the status updates while asserting the status from the DB")
	public void testStatusUpdateForSwiggyAssuredOrder(String order_id,String order_time,String epochTime, String status, boolean check) throws SQLException, IOException, InterruptedException {
		log.info("***************************************** test StatusUpdateForSwiggyAssuredOrder started *****************************************");

		//Create a Swiggy Assured order
		if(check)
			losHelper.createSwiggyAssuredOrder(order_id,order_time,epochTime);

		//Change the status of the Swiggy Assured order from assigned to delivered
		statusUpdate(order_id, order_time, status);

		log.info("######################################### test StatusUpdateForSwiggyAssuredOrder completed #########################################");
	}


	//Following tests are for negative cases

	//Test to update an order for wrong status name
	@Test(dataProvider = "orderCreationData",groups = {"regression"},description = "Create a manual order and update an invalid status while asserting the status is not updated in the DB")
	public void testStatusUpdateForWrongStatuses(String order_id,String order_time,String epochTime) throws IOException, TimeoutException, InterruptedException {
		log.info("***************************************** test StatusUpdateForWrongStatuses started *****************************************");

		String status="NotAvailable";

		//Create a manual order
		losHelper.createManualOrder(order_id,order_time,epochTime);

		//Change the status of the create order for wrong status as "StatusNotDefined"
		Thread.sleep(5000);
		String msg=losHelper.statusUpdate(order_id, order_time, status);
		Thread.sleep(1000);

		//Verify if the pushed JSON with wrong status has not changed the order status from the DB
		List<Map<String, Object>> orderResult = SystemConfigProvider.getTemplate(LosConstants.hostName).queryForList("select delivery_status from oms_orderstatus where id in (select status_id from oms_order where order_id = " + order_id + ");");
		String order_status = (String) orderResult.get(0).get("delivery_status");
		log.info("The status of the order is "+order_status);
		Assert.assertNotEquals(status, order_status, "Status is wrongly changed in DB");

		log.info("***************************************** test StatusUpdateForWrongStatuses completed *****************************************");
	}

	//Test to update status of a non-existing order
	@Test(dataProvider = "orderCreationData",groups = {"regression"},description = "Update the status of a non-existing order and assert the message goes to status update dead letter queue")
	public void testStatusUpdateForNonExistingOrder(String order_id,String order_time,String epochTime) throws IOException, TimeoutException, InterruptedException {
		log.info("***************************************** test StatusUpdateForNonExistingOrder started *****************************************");

		String status="NotAvailable";

		//Create a manual order
		losHelper.createManualOrder(order_id,order_time,epochTime);

		//Change the status of the create order for wrong status as "StatusNotDefined"
		Thread.sleep(5000);
		String msg=losHelper.statusUpdate(order_id, order_time, status);
		Thread.sleep(1000);

		//Verify if the pushed JSON with wrong status has not changed the order status from the DB
		List<Map<String, Object>> orderResult = SystemConfigProvider.getTemplate(LosConstants.hostName).queryForList("select delivery_status from oms_orderstatus where id in (select status_id from oms_order where order_id = " + order_id + ");");
		String order_status = (String) orderResult.get(0).get("delivery_status");
		log.info("The status of the order is "+order_status);
		Assert.assertNotEquals(status, order_status, "Status is wrongly changed in DB");

		log.info("***************************************** test StatusUpdateForNonExistingOrder completed *****************************************");

	}

	//Test to verify the status update sends the JSOn to dead letter queue when orderId is sent as string
	@Test(dataProvider = "createDataForOrderCreation",groups = {"regression"},description = "Update the status by sending an order with order id as string and assert the message is gone to status update dead letter queue")
	public void testStatusUpdateForOrderIdAsString(String order_id,String order_time,String epochTime, String status, boolean check) throws IOException, TimeoutException, InterruptedException {
		log.info("***************************************** test StatusUpdateForOrderAsString started *****************************************");

		String orderAsString="SatyamTestForWrongOrderAsString";

		//Change the status of the Order
		//Purge the dead letter queue before pushing the JSON with Order As String
		helper.purgeQueue(LosConstants.hostName, LosConstants.statusUpdateFailedQueue);
		Thread.sleep(5000);
		String msg = losHelper.statusUpdate(orderAsString, order_time, status);
		Thread.sleep(12000);

		//Verify if the pushed JSON with Order As String the same as message from dead letter queue
		Assert.assertEquals(helper.getMessage(LosConstants.hostName, LosConstants.statusUpdateFailedQueue), msg, "Order is not present in dead-letter-queue");

		log.info("***************************************** test StatusUpdateForOrderAsString completed *****************************************");

	}
}
