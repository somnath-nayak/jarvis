package com.swiggy.api.erp.ff.tests.verificationService;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.swiggy.api.erp.ff.dp.verificationService.VerificationServiceApiTestData;
import com.swiggy.api.erp.ff.helper.LOSHelper;
import com.swiggy.api.erp.ff.helper.OMSHelper;
import com.swiggy.api.erp.ff.helper.VerificationServiceHelper;


public class VerifyOrdersAfterQueuingForVerifierUsingNewAPITests {
	
	VerificationServiceHelper helper = new VerificationServiceHelper();
	LOSHelper losHelper = new LOSHelper();
	OMSHelper omsHelper = new OMSHelper();
	
	@Test(dataProvider = "queueOrderToAssignVerifier", groups = {"sanity", "regression"},description = "Create a manual order queue it for assigning verifier then verify it", dataProviderClass = VerificationServiceApiTestData.class)
	public void queueManualOrderForAssigningVerifierThenVerifyTest(String channel, String userValidation, String distanceValidation, String duplicateValidation) throws IOException, InterruptedException{
		System.out.println("***************************************** queueManualOrderForAssigningVerifierThenVerifyTest started *****************************************");
		
		String orderId = helper.queueForAssigningVerifierAndReturnOrderId("manual", channel, userValidation, distanceValidation, duplicateValidation);
		Thread.sleep(3000);
		Assert.assertTrue(omsHelper.verifyOrder(orderId, helper.convertCurrentOrderAaction(userValidation, distanceValidation, duplicateValidation)), "Could not verify manual order after queuing it for verifier");
		
		System.out.println("######################################### queueManualOrderForAssigningVerifierThenVerifyTest compleated #########################################");
	}
	
	@Test(dataProvider = "queueOrderToAssignVerifier", groups = {"sanity", "regression"},description = "Create a partner order queue it for assigning verifier then verify it", dataProviderClass = VerificationServiceApiTestData.class)
	public void queuePartnerOrderForAssigningVerifierThenVerifyTest(String channel, String userValidation, String distanceValidation, String duplicateValidation) throws IOException, InterruptedException{
		System.out.println("***************************************** queuePartnerOrderForAssigningVerifierThenVerifyTest started *****************************************");
		
		String orderId = helper.queueForAssigningVerifierAndReturnOrderId("partner", channel, userValidation, distanceValidation, duplicateValidation);
		Thread.sleep(3000);
		Assert.assertTrue(omsHelper.verifyOrder(orderId, helper.convertCurrentOrderAaction(userValidation, distanceValidation, duplicateValidation)), "Could not verify partner order after queuing it for verifier");
		
		System.out.println("######################################### queuePartnerOrderForAssigningVerifierThenVerifyTest compleated #########################################");
	}
	
	@Test(dataProvider = "queueOrderToAssignVerifier", groups = {"sanity", "regression"},description = "Create a third party order queue it for assigning verifier then verify it", dataProviderClass = VerificationServiceApiTestData.class)
	public void queueThirdPartyOrderForAssigningVerifierThenVerifyTest(String channel, String userValidation, String distanceValidation, String duplicateValidation) throws IOException, InterruptedException{
		System.out.println("***************************************** queueThirdPartyOrderForAssigningVerifierThenVerifyTest started *****************************************");
		
		String orderId = helper.queueForAssigningVerifierAndReturnOrderId("thirdparty", channel, userValidation, distanceValidation, duplicateValidation);
		Thread.sleep(3000);
		Assert.assertTrue(omsHelper.verifyOrder(orderId, helper.convertCurrentOrderAaction(userValidation, distanceValidation, duplicateValidation)), "Could not verify third party order after queuing it for verifier");
		
		System.out.println("######################################### queueThirdPartyOrderForAssigningVerifierThenVerifyTest compleated #########################################");
	}
	
	@Test(dataProvider = "queueOrderToAssignVerifier", groups = {"sanity", "regression"},description = "Create a pop order queue it for assigning verifier then verify it", dataProviderClass = VerificationServiceApiTestData.class)
	public void queuePopOrderForAssigningVerifierThenVerifyTest(String channel, String userValidation, String distanceValidation, String duplicateValidation) throws IOException, InterruptedException{
		System.out.println("***************************************** queuePopOrderForAssigningVerifierThenVerifyTest started *****************************************");
		
		String orderId = helper.queueForAssigningVerifierAndReturnOrderId("pop", channel, userValidation, distanceValidation, duplicateValidation);
		Thread.sleep(3000);
		Assert.assertTrue(omsHelper.verifyOrder(orderId, helper.convertCurrentOrderAaction(userValidation, distanceValidation, duplicateValidation)), "Could not verify pop order after queuing it for verifier");
		
		System.out.println("######################################### queuePopOrderForAssigningVerifierThenVerifyTest compleated #########################################");
	}
	
	@Test(dataProvider = "queueOrderToAssignVerifier", groups = {"sanity", "regression"},description = "Create a swiggy assured order queue it for assigning verifier then verify it", dataProviderClass = VerificationServiceApiTestData.class)
	public void queueSwiggyAssuredOrderForAssigningVerifierThenVerifyTest(String channel, String userValidation, String distanceValidation, String duplicateValidation) throws IOException, InterruptedException{
		System.out.println("***************************************** queueSwiggyAssuredOrderForAssigningVerifierThenVerifyTest started *****************************************");
		
		String orderId = helper.queueForAssigningVerifierAndReturnOrderId("swiggyassured", channel, userValidation, distanceValidation, duplicateValidation);
		Thread.sleep(3000);
		Assert.assertTrue(omsHelper.verifyOrder(orderId, helper.convertCurrentOrderAaction(userValidation, distanceValidation, duplicateValidation)), "Could not verify swiggy assured order after queuing it for verifier");
		System.out.println("######################################### queueSwiggyAssuredOrderForAssigningVerifierThenVerifyTest compleated #########################################");
	}
	
	@Test(dataProvider = "queueOrderToAssignVerifier", groups = {"sanity", "regression"},description = "Create a long distance order queue it for assigning verifier then verify it", dataProviderClass = VerificationServiceApiTestData.class)
	public void queueLongDistanceOrderForAssigningVerifierThenVerifyTest(String channel, String userValidation, String distanceValidation, String duplicateValidation) throws IOException, InterruptedException{
		System.out.println("***************************************** queueLongDistanceOrderForAssigningVerifierThenVerifyTest started *****************************************");
		
		String orderId = helper.queueForAssigningVerifierAndReturnOrderId("longdistance", channel, userValidation, distanceValidation, duplicateValidation);
		Thread.sleep(3000);
		
		Assert.assertTrue(omsHelper.verifyOrder(orderId, helper.convertCurrentOrderAaction(userValidation, distanceValidation, duplicateValidation)), "Could not verify long distance order after queuing it for verifier");
		System.out.println("######################################### queueLongDistanceOrderForAssigningVerifierThenVerifyTest compleated #########################################");
	}
	
	@Test(dataProvider = "queueOrderToAssignVerifier", groups = {"sanity", "regression"},description = "Create a new user order queue it for assigning verifier then verify it", dataProviderClass = VerificationServiceApiTestData.class)
	public void queueNewUserOrderForAssigningVerifierThenVerifyTest(String channel, String userValidation, String distanceValidation, String duplicateValidation) throws IOException, InterruptedException{
		System.out.println("***************************************** queueNewUserOrderForAssigningVerifierThenVerifyTest started *****************************************");
		
		String orderId = helper.queueForAssigningVerifierAndReturnOrderId("newuser", channel, userValidation, distanceValidation, duplicateValidation);
		Thread.sleep(3000);
		
		Assert.assertTrue(omsHelper.verifyOrder(orderId, helper.convertCurrentOrderAaction(userValidation, distanceValidation, duplicateValidation)), "Could not verify new user order after queuing it for verifier");
		System.out.println("######################################### queueNewUserOrderForAssigningVerifierThenVerifyTest compleated #########################################");
	}

}
