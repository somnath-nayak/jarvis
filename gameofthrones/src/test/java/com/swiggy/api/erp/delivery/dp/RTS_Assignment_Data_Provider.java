package com.swiggy.api.erp.delivery.dp;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.testng.annotations.DataProvider;

import com.swiggy.api.erp.delivery.constants.RTSConstants;
import com.swiggy.api.erp.delivery.helper.DeliveryDataHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryHelperMethods;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceHelper;

public class RTS_Assignment_Data_Provider {
	static DeliveryHelperMethods delmeth=new DeliveryHelperMethods();
	DeliveryDataHelper deldata=new DeliveryDataHelper();
	DeliveryServiceHelper delserhel=new DeliveryServiceHelper();
	String order_id;
	@DataProvider(name = "RTS_CAS_1")
	public Object[][] CAS_1() throws Exception {
		delmeth.dbhelperupdate(RTSConstants.closeallexisitingbatches);
		delmeth.dbhelperupdate(RTSConstants.set_random_de_id);
		//String query="update cross_zone_assignment_mapping set cross_zone_enable="+RTSConstants.enable_cas_assign+" where from_zone_id="+RTSConstants.cas_De_rest_Area+" and to_zone_id="+RTSConstants.cas_area_code;
		//delmeth.dbhelperupdatee(sqlTemplate, query);
		order_id=createorderjson(RTSConstants.long_distance_batching_rest_id, RTSConstants.long_distance_batching_rest_lat_lng, RTSConstants.long_distance_batching_last_mile_less, RTSConstants.cas_De_rest_Area_string,  RTSConstants.cas_customer_lat, RTSConstants.cas_customer_lng);
		return new Object[][] {

		{ order_id,RTSConstants.de_App_version,RTSConstants.CAS_de_lat,RTSConstants.CAS_de_lng}

		};
	}
	@DataProvider(name = "RTS_CAS_2")
	public Object[][] CAS_2() throws Exception {
		String query="update cross_zone_assignment_mapping set cross_zone_enable="+RTSConstants.disable_cas_assign+" where from_zone_id="+RTSConstants.cas_De_rest_Area+" and to_zone_id="+RTSConstants.cas_area_code;
		delmeth.dbhelperupdate(null, query);
		Map<String, String> defMap= new HashMap<>();
		defMap.put("restaurant_id",RTSConstants.long_distance_batching_rest_id);
		defMap.put("restaurant_lat_lng",RTSConstants.long_distance_batching_rest_lat_lng);
		defMap.put("restaurant_customer_distance", RTSConstants.long_distance_batching_last_mile_less);
		defMap.put("restaurant_area_code", RTSConstants.cas_De_rest_Area_string);
		defMap.put("lat", RTSConstants.cas_customer_lat);
		defMap.put("lng", RTSConstants.cas_customer_lng);
		String order_time=deldata.getcurrentDateTimefororderTime();
		defMap.put("order_time",order_time);
		String orderJson1=deldata.returnOrderJson(defMap);
		boolean order_status1=deldata.pushOrderJsonToRMQAndValidate(orderJson1);
		String order_id1=null;
		if(order_status1)
		{
			order_id1=(new JSONObject(orderJson1)).get("order_id").toString();
		}
		return new Object[][] {

		{ order_id1,RTSConstants.de_App_version,RTSConstants.CAS_de_lat,RTSConstants.CAS_de_lng}

		};
	}
	@DataProvider(name = "RTS_CAS_3")
	public Object[][] CAS_3() throws Exception {
		delmeth.dbhelperupdate(RTSConstants.disable_RTS_CAS_featue);
		String query="update cross_zone_assignment_mapping set cross_zone_enable="+RTSConstants.disable_cas_assign+" where from_zone_id="+RTSConstants.cas_De_rest_Area+" and to_zone_id="+RTSConstants.cas_area_code;
		delmeth.dbhelperupdate(query);
		Map<String, String> defMap= new HashMap<>();
		defMap.put("restaurant_id",RTSConstants.long_distance_batching_rest_id);
		defMap.put("restaurant_lat_lng",RTSConstants.long_distance_batching_rest_lat_lng);
		defMap.put("restaurant_customer_distance", RTSConstants.long_distance_batching_last_mile_less);
		defMap.put("restaurant_area_code", RTSConstants.cas_De_rest_Area_string);
		defMap.put("lat", RTSConstants.cas_customer_lat);
		defMap.put("lng", RTSConstants.cas_customer_lng);
		String order_time=deldata.getcurrentDateTimefororderTime();
		defMap.put("order_time",order_time);
		String orderJson1=deldata.returnOrderJson(defMap);
		boolean order_status1=deldata.pushOrderJsonToRMQAndValidate(orderJson1);
		String order_id1=null;
		if(order_status1)
		{
			order_id1=(new JSONObject(orderJson1)).get("order_id").toString();
		}
		return new Object[][] {

		{ order_id1,RTSConstants.de_App_version,RTSConstants.CAS_de_lat,RTSConstants.CAS_de_lng}

		};
	}
	@DataProvider(name = "RTS_CAS_4")
	public Object[][] CAS_4() throws Exception {
		delmeth.dbhelperupdate(RTSConstants.enable_RTS_CAS_featue);
		String query="update cross_zone_assignment_mapping set cross_zone_enable="+RTSConstants.enable_cas_assign+" where from_zone_id="+RTSConstants.cas_De_rest_Area+" and to_zone_id="+RTSConstants.cas_area_code;
		delmeth.dbhelperupdate(query);
		Map<String, String> defMap= new HashMap<>();
		defMap.put("restaurant_id",RTSConstants.long_distance_batching_rest_id);
		defMap.put("restaurant_lat_lng",RTSConstants.long_distance_batching_rest_lat_lng);
		defMap.put("restaurant_customer_distance", RTSConstants.long_distance_batching_last_mile_less);
		defMap.put("restaurant_area_code", RTSConstants.cas_De_rest_Area_string);
		defMap.put("lat", RTSConstants.cas_customer_lat);
		defMap.put("lng", RTSConstants.cas_customer_lng);
		String order_time=deldata.getcurrentDateTimefororderTime();
		defMap.put("order_time",order_time);
		String orderJson1=deldata.returnOrderJson(defMap);
		boolean order_status1=deldata.pushOrderJsonToRMQAndValidate(orderJson1);
		String order_id1=null;
		if(order_status1)
		{
			order_id1=(new JSONObject(orderJson1)).get("order_id").toString();
		}
		return new Object[][] {

		{ order_id1,RTSConstants.de_App_version,RTSConstants.CAS_de_lat,RTSConstants.CAS_de_lng}

		};
	}
	@DataProvider(name = "RTS_CostAssign_5")
	public Object[][] RTS_CostAssign5() throws Exception {
		delmeth.dbhelperupdate(RTSConstants.disable_RTS_CAS_featue);
		String query="update cross_zone_assignment_mapping set cross_zone_enable="+RTSConstants.disable_cas_assign+" where from_zone_id="+RTSConstants.cas_De_rest_Area+" and to_zone_id="+RTSConstants.cas_area_code;
		delmeth.dbhelperupdate(query);
		delmeth.dbhelperupdate(RTSConstants.enable_RTS_Assignment);
		delmeth.dbhelperupdate(RTSConstants.enable_RTS_Assignment_for_BTM_zone);
		Map<String, String> defMap= new HashMap<>();
		defMap.put("restaurant_id",RTSConstants.long_distance_batching_rest_id);
		defMap.put("restaurant_lat_lng",RTSConstants.long_distance_batching_rest_lat_lng);
		defMap.put("restaurant_customer_distance", RTSConstants.long_distance_batching_last_mile_less);
		defMap.put("restaurant_area_code", RTSConstants.long_distance_batching_rest_area_code);
		String order_time=deldata.getcurrentDateTimefororderTime();
		defMap.put("order_time",order_time);
		String orderJson1=deldata.returnOrderJson(defMap);
		boolean order_status1=deldata.pushOrderJsonToRMQAndValidate(orderJson1);
		String order_id1=null;
		if(order_status1)
		{
			order_id1=(new JSONObject(orderJson1)).get("order_id").toString();
		}
		return new Object[][] {

		{ order_id1,RTSConstants.de_App_version}

		};
	}
	@DataProvider(name = "RTS_CostAssign_6")
	public Object[][] RTS_CostAssign6() throws Exception {
		delmeth.dbhelperupdate(RTSConstants.enable_RTS_Assignment);
		delmeth.dbhelperupdate(RTSConstants.enable_RTS_Assignment_for_BTM_zone);
		Map<String, String> defMap= new HashMap<>();
		defMap.put("restaurant_id",RTSConstants.long_distance_batching_rest_id);
		defMap.put("restaurant_lat_lng",RTSConstants.long_distance_batching_rest_lat_lng);
		defMap.put("restaurant_customer_distance", RTSConstants.long_distance_batching_last_mile_less);
		defMap.put("restaurant_area_code", RTSConstants.cas_De_rest_Area_string);
		String order_time=deldata.getcurrentDateTimefororderTime();
		defMap.put("order_time",order_time);
		String orderJson1=deldata.returnOrderJson(defMap);
		boolean order_status1=deldata.pushOrderJsonToRMQAndValidate(orderJson1);
		String order_id1=null;
		if(order_status1)
		{
			order_id1=(new JSONObject(orderJson1)).get("order_id").toString();
		}
		return new Object[][] {

		{ order_id1,RTSConstants.de_App_version}

		};
	}
	@DataProvider(name = "RTS_CostAssign_7")
	public Object[][] RTS_CostAssign7() throws Exception {
		delmeth.dbhelperupdate(RTSConstants.enable_RTS_Assignment);
		delmeth.dbhelperupdate(RTSConstants.enable_RTS_Assignment_for_BTM_zone);
		String query=RTSConstants.RTS_greedy_objective_function_query+RTSConstants.long_distance_area_code;
		delmeth.dbhelperupdate(query);
		Map<String, String> defMap= new HashMap<>();
		defMap.put("restaurant_id",RTSConstants.long_distance_batching_rest_id);
		defMap.put("restaurant_lat_lng",RTSConstants.long_distance_batching_rest_lat_lng);
		defMap.put("restaurant_customer_distance", RTSConstants.long_distance_batching_last_mile_less);
		defMap.put("restaurant_area_code", RTSConstants.long_distance_batching_rest_area_code);
		String order_time=deldata.getcurrentDateTimefororderTime();
		defMap.put("order_time",order_time);
		String orderJson1=deldata.returnOrderJson(defMap);
		boolean order_status1=deldata.pushOrderJsonToRMQAndValidate(orderJson1);
		String order_id1=null;
		if(order_status1)
		{
			order_id1=(new JSONObject(orderJson1)).get("order_id").toString();
		}
		return new Object[][] {

		{ order_id1,RTSConstants.de_App_version}

		};
	}
	@DataProvider(name = "RTS_CostAssign_8")
	public Object[][] RTS_CostAssign8() throws Exception {
		delmeth.dbhelperupdate(RTSConstants.enable_RTS_Assignment);
		delmeth.dbhelperupdate(RTSConstants.enable_RTS_Assignment_for_BTM_zone);
		String query=RTSConstants.RTS_hungarian_objective_function_query+RTSConstants.long_distance_area_code;
		delmeth.dbhelperupdate(query);
		Map<String, String> defMap= new HashMap<>();
		defMap.put("restaurant_id",RTSConstants.long_distance_batching_rest_id);
		defMap.put("restaurant_lat_lng",RTSConstants.long_distance_batching_rest_lat_lng);
		defMap.put("restaurant_customer_distance", RTSConstants.long_distance_batching_last_mile_less);
		defMap.put("restaurant_area_code", RTSConstants.long_distance_batching_rest_area_code);
		String order_time=deldata.getcurrentDateTimefororderTime();
		defMap.put("order_time",order_time);
		String orderJson1=deldata.returnOrderJson(defMap);
		boolean order_status1=deldata.pushOrderJsonToRMQAndValidate(orderJson1);
		String order_id1=null;
		if(order_status1)
		{
			order_id1=(new JSONObject(orderJson1)).get("order_id").toString();
		}
		return new Object[][] {

		{ order_id1,RTSConstants.de_App_version}

		};
	}
	@DataProvider(name = "RTS_CostAssign_9")
	public Object[][] RTS_CostAssign9() throws Exception {
		delmeth.dbhelperupdate(RTSConstants.enable_RTS_Assignment);
		delmeth.dbhelperupdate(RTSConstants.enable_RTS_Assignment_for_BTM_zone);
		String query=RTSConstants.RTS_hungarian_objective_function_query+RTSConstants.long_distance_area_code;
		delmeth.dbhelperupdate(query);
		Map<String, String> defMap= new HashMap<>();
		defMap.put("restaurant_id",RTSConstants.long_distance_batching_rest_id);
		defMap.put("restaurant_lat_lng",RTSConstants.long_distance_batching_rest_lat_lng);
		defMap.put("restaurant_customer_distance", RTSConstants.long_distance_batching_last_mile_less);
		defMap.put("restaurant_area_code", RTSConstants.long_distance_batching_rest_area_code);
		String order_time=deldata.getcurrentDateTimefororderTime();
		defMap.put("order_time",order_time);
		String orderJson1=deldata.returnOrderJson(defMap);
		boolean order_status1=deldata.pushOrderJsonToRMQAndValidate(orderJson1);
		String order_id1=null;
		if(order_status1)
		{
			order_id1=(new JSONObject(orderJson1)).get("order_id").toString();
		}
		return new Object[][] {

		{ order_id1,RTSConstants.de_App_version}

		};
	}
	
	public String createorderjson(String restaurant_id,String restaurant_lat_lng,String restaurant_customer_distance,String restaurant_area_code,String lat,String lng) throws JSONException
	{
		Map<String, String> defMap= new HashMap<>();
		defMap.put("restaurant_id",restaurant_id);
		defMap.put("restaurant_lat_lng",restaurant_lat_lng);
		defMap.put("restaurant_customer_distance", restaurant_customer_distance);
		defMap.put("restaurant_area_code", restaurant_area_code);
	if(lat!="")
	{
		defMap.put("lat", lat);
	}
	if(lng!="")
	{
		defMap.put("lng",lng);
	}
		String order_time=deldata.getcurrentDateTimefororderTime();
		defMap.put("order_time",order_time);
			
		String orderJson=deldata.returnOrderJson(defMap);
		boolean order_status=deldata.pushOrderJsonToRMQAndValidate(orderJson);
		Object order_id=null;
		System.out.println(orderJson);
		if(order_status)
		{
			order_id=(new JSONObject(orderJson)).get("order_id").toString();
		}
		return order_id.toString();
	}
	
}
