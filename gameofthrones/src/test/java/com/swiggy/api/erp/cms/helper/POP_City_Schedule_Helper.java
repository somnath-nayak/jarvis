package com.swiggy.api.erp.cms.helper;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/*
 * Area scheduling can be created from this class and same details can be updated for area-code=3 (HSR)
 */

public class POP_City_Schedule_Helper  {
	public static int id, cityScheduleid=0;
	Initialize gameofthrones = new Initialize();

	public HashMap<String, String> setHeaders() throws JSONException {
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		JSONObject json = new JSONObject();
		json.put("source", "cms");
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("user_meta", json.toString());
		return requestheaders;
	}

	public String[] setPayload1(String slot_type, int city_id, int openTime, int closeTime, String day,
			String menu_type) throws JSONException {
		return new String[] { slot_type, Integer.toString(city_id), Integer.toString(openTime),
				Integer.toString(closeTime), day, menu_type };
	}

	public Processor createCity_schedule_helper(String slot_type, int city_id, 
			int openTime, int closeTime, String day, String menu_type) throws JSONException {
		GameOfThronesService service1 = new GameOfThronesService("cmsrestaurantservice", "cityschedulecreate", gameofthrones);
		 Processor p = new Processor(service1, 
				 					setHeaders(), 
				 					setPayload1(slot_type, city_id, openTime, closeTime, day, menu_type), 
				 					null);	
		 return p;	
	}
	
	
	
	public Processor updateCity_schedule_helper(String slot_type, int city_id, int openTime, int closeTime, String day,
			String menu_type, int cityScheduleid) throws JSONException {
		//String getId = Long.toString(id);
		GameOfThronesService service1 = new GameOfThronesService("cmsrestaurantservice", "cityscheduleupdate", gameofthrones);
		Processor p1 = new Processor(service1, 
									setHeaders(), 
									setPayload1(slot_type, city_id, openTime, closeTime, day, menu_type),
									new String[] { ""+ cityScheduleid });
		return p1;
	}

	public Processor getCity_schedule_helper(int cityScheduleid) throws JSONException {
		//String getId = Long.toString(id);
		GameOfThronesService service1 = new GameOfThronesService("cmsrestaurantservice", "cityscheduleget", gameofthrones);
		Processor p1 = new Processor(service1, 
									setHeaders(), 
									null, 
									new String[] { ""+ cityScheduleid });
		return p1;
	}

	public Processor deleteCity_schedule_helper(int cityScheduleid) throws JSONException {
		GameOfThronesService service1 = new GameOfThronesService("cmsrestaurantservice", "cityscheduledelete", gameofthrones);
		Processor p1 = new Processor(service1, 
									setHeaders(), 
									null, 
									new String[] {""+ cityScheduleid });
		return p1;
	}

	public Processor create_InvalidCity_schedule_helper(String slot_type, int city_id, int openTime, int closeTime, String day,
			String menu_type) throws JSONException {
		GameOfThronesService service1 = new GameOfThronesService("cmsrestaurantservice", "cityschedulecreate", gameofthrones);
		Processor p1 = new Processor(service1, 
									setHeaders(), 
									setPayload1(slot_type, city_id, openTime, closeTime, day, menu_type), 
									null);
		return p1;
	}

	public Processor create_City_schedule_For_ExistingSlot_helper(String slot_type, int city_id, int openTime, int closeTime,
			String day, String menu_type) throws JSONException {
		GameOfThronesService service1 = new GameOfThronesService("cmsrestaurantservice", "cityschedulecreate", gameofthrones);
		Processor p1 = new Processor(service1, 
									setHeaders(), 
									setPayload1(slot_type, city_id, openTime, closeTime, day, menu_type), 
									null);
		return p1;
	}

	public Processor create_City_schedule_With_Invalid_OR_Empty_DayName_helper(String slot_type, int city_id, int openTime,
			int closeTime, String day, String menu_type) throws JSONException {
		GameOfThronesService service1 = new GameOfThronesService("cmsrestaurantservice", "cityschedulecreate", gameofthrones);
		Processor p1 = new Processor(service1, 
									setHeaders(),
									setPayload1(slot_type, city_id, openTime, closeTime, day, menu_type), 
									null);
		return p1;
	}

}
