package com.swiggy.api.erp.delivery.tests;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.annotations.Test;

import com.swiggy.api.erp.delivery.helper.DeliveryHelperMethods;

public class IncentiveRulesValidation {
	
	DeliveryHelperMethods delmeth=new DeliveryHelperMethods();
	private static final String FILENAME = "D:\\test\\inecntive_data.csv";
	
	BufferedWriter bw = null;
	FileWriter fw = null;
	List<String> batchcountt=new ArrayList<String>();
	List<String> orderrejectcount=new ArrayList<String>();
	List<String> batchtype=new ArrayList<String>();
	List<String> orderweight=new ArrayList<String>();
	List<String> Mglweight=new ArrayList<String>();
	List<String> rattings=new ArrayList<String>();
	List<String> lastmile=new ArrayList<String>();
	List<String> firstmile=new ArrayList<String>();
	List<String> rainmodetype=new ArrayList<String>();
	List<String> preptime=new ArrayList<String>();
	List<String> placementdelay=new ArrayList<String>();
	List<String> dewaittime=new ArrayList<String>();
	List<String> fmtt=new ArrayList<String>();
	List<String> lmtt=new ArrayList<String>();
	List<String> orderedtime=new ArrayList<String>();
	List<String> assignedtime=new ArrayList<String>();
	List<String> confirmationtime=new ArrayList<String>();
	List<String> arrivedtime=new ArrayList<String>();
	List<String> pickeduptime=new ArrayList<String>();
	List<String> reachedtime=new ArrayList<String>();
	List<String> deliveredtime=new ArrayList<String>();
	List<String> receivedtime=new ArrayList<String>();
	List<String> slaa=new ArrayList<String>();
	HashMap<String,String> map1=new HashMap<String,String>();
	
	Object batchcount,Batch_type,Order_reject_count,rain_mode_type,prep_time,order_weight,Mgl_weight,rating,last_mile,first_mile,placement_delay,de_wait_time,fmt,lmt,sla,ordered_time,assigned_time,confirmation_time,arrived_time,pickedup_time,reached_time,delivered_time,received_time;
	 int MILLIS_IN_DAY = 1000 * 60 * 60 * 24;
	
	 
	 @Test
	 public void createfile() throws IOException
	 {
		 fw = new FileWriter(FILENAME);
			bw = new BufferedWriter(fw);
	 }
	 
	 
	@Test(description="incentive data")
	public void test1() throws IOException, JSONException
	{
		
		 SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");  
		    Date date = new Date();  
		    String prevDate = formatter.format(date.getTime() - MILLIS_IN_DAY);
		    String currDate = formatter.format(date.getTime());
		    Calendar cal = Calendar.getInstance();
		    cal.setTimeInMillis(date.getTime()- MILLIS_IN_DAY);
		    cal.set(Calendar.HOUR_OF_DAY, 5);
		    cal.set(Calendar.MINUTE, 30);
		    cal.set(Calendar.SECOND, 0); 
		    long prevdaytime= cal.getTimeInMillis();
		    System.out.println(prevdaytime);
		    cal.setTimeInMillis(date.getTime());
		    cal.set(Calendar.HOUR_OF_DAY, 5);
		    cal.set(Calendar.MINUTE, 30);
		    cal.set(Calendar.SECOND, 0); 
		    long currenttime=cal.getTimeInMillis();
		    System.out.println(currenttime);
		    		    
		String query="select * from alchemist.incentive_order_data where created_at between "+ "'"+prevDate+" 05:30:00"+"' and "+"'"+currDate+" 05:30:00"+"'";
		List<Map<String, Object>> ls=delmeth.dbhelpergetall(query);
		HashMap<String,Object> map=new HashMap<String,Object>();
		for(Map<String, Object> l:ls)
		{
		String s=l.get("incentive_data").toString();
		String order_id=l.get("order_id").toString();
		if(!map.containsKey(order_id))
		{	
		map.put(l.get("order_id").toString(), s);
		JSONObject jsonObj = new JSONObject(s);
		batchcount=jsonObj.get("batch_count");
		Order_reject_count=jsonObj.get("order_reject_count");
		if(jsonObj.has("batch_type")){
		Batch_type=jsonObj.get("batch_type"); 
		}
		else
		{
			Batch_type=null;	
		}
		order_weight=jsonObj.getDouble("order_weight");
		Mgl_weight=jsonObj.getDouble("mgl_weight");
		rating=jsonObj.getDouble("rating");
		last_mile=jsonObj.getDouble("last_mile");
		first_mile=jsonObj.getDouble("first_mile");
		rain_mode_type=jsonObj.getInt("rain_mode_type");
		prep_time=jsonObj.get("prep_time");
		placement_delay=jsonObj.getDouble("placement_delay");
		de_wait_time=jsonObj.getDouble("de_wait_time");
		fmt=jsonObj.getDouble("fmt");
		lmt=jsonObj.getDouble("lmt");
		ordered_time=jsonObj.getLong("ordered_time");
		assigned_time=jsonObj.getLong("assigned_time");
		confirmation_time=jsonObj.getLong("confirmation_time");
		arrived_time=jsonObj.getLong("arrived_time");
		pickedup_time=jsonObj.getLong("pickedup_time");
		reached_time=jsonObj.getLong("reached_time");
		delivered_time=jsonObj.getLong("delivered_time");
		received_time=jsonObj.getLong("received_time");
		sla=jsonObj.getDouble("sla");		
				
		if( batchcount!=null && (Integer.valueOf(batchcount.toString())==0 || Integer.valueOf(batchcount.toString())>5))
		{
			batchcountt.add(l.get("order_id").toString());
	    }
	/*	Long obj1 = new Long((long) ordered_time);
	      Long obj2 = new Long(prevdaytime);
	      Long obj3=new Long(currenttime);
	      int retval1 =  obj1.compareTo(obj2);
	      int retval2 =  obj1.compareTo(obj3);
		if(ordered_time!=null && (retval1<0 || retval2>0))
		{
			orderedtime.add(l.get("order_id").toString());
		}
		if(assigned_time!=null && ((Long.parseLong(assigned_time.toString())<prevdaytime) || (Long.parseLong(assigned_time.toString()) >currenttime)))
		{
			assignedtime.add(l.get("order_id").toString());
		}
		if(confirmation_time!=null && ((Long.parseLong(confirmation_time.toString())<prevdaytime) || (Long.parseLong(confirmation_time.toString()) >currenttime)))
		{
			confirmationtime.add(l.get("order_id").toString());
		}
		if(arrived_time!=null && ((Long.parseLong(arrived_time.toString())<prevdaytime) || (Long.parseLong(arrived_time.toString()) >currenttime)))
		{
			arrivedtime.add(l.get("order_id").toString());
		}
		if(pickedup_time!=null && ((Long.parseLong(pickedup_time.toString())<prevdaytime) || (Long.parseLong(pickedup_time.toString()) >currenttime)))
		{
			pickeduptime.add(l.get("order_id").toString());
		}
		if(reached_time!=null && ((Long.parseLong(reached_time.toString())<prevdaytime) || (Long.parseLong(reached_time.toString()) >currenttime)))
		{
			reachedtime.add(l.get("order_id").toString());
		}
		if(delivered_time!=null && ((Long.parseLong(delivered_time.toString())<prevdaytime) || (Long.parseLong(delivered_time.toString()) >currenttime)))
		{
			deliveredtime.add(l.get("order_id").toString());
		}
		if(delivered_time!=null && ((Long.parseLong(received_time.toString())<prevdaytime) || (Long.parseLong(received_time.toString()) >currenttime)))
		{
			receivedtime.add(l.get("order_id").toString());
		}*/
				
		if(Order_reject_count!=null && (Integer.valueOf(Order_reject_count.toString())<0 || Integer.valueOf(Order_reject_count.toString())>10))
		{
			orderrejectcount.add(l.get("order_id").toString());
		}
		if(Batch_type!=null && (Integer.valueOf(Batch_type.toString())>4))
		{
			batchtype.add(l.get("order_id").toString());
		}
		
		if(order_weight!=null && ((Double.parseDouble(order_weight.toString())<1) || (Double.parseDouble(order_weight.toString())>2.5)))
		{
			orderweight.add(l.get("order_id").toString());
		}
		if(Mgl_weight!=null && ((Double.parseDouble(Mgl_weight.toString())<0.9) || (Double.parseDouble(Mgl_weight.toString())>2.5)))
		{
			Mglweight.add(l.get("order_id").toString());
		}
		if(rating!=null && ((Double.parseDouble(rating.toString())<0.0) || (Double.parseDouble(rating.toString())>5.0)))
		{
			rattings.add(l.get("order_id").toString());
		}
		if(last_mile!=null && ((Double.parseDouble(last_mile.toString())<0.0) || (Double.parseDouble(last_mile.toString())>10.0)))
		{
			lastmile.add(l.get("order_id").toString());
		}
		if(first_mile!=null && ((Double.parseDouble(first_mile.toString())<0.0) || (Double.parseDouble(first_mile.toString())>10.0)))
		{
			firstmile.add(l.get("order_id").toString());
		}
		if(rain_mode_type!=null && (Integer.valueOf(rain_mode_type.toString())>5))
		{
			rainmodetype.add(l.get("order_id").toString());
		}
		if(sla!=null && ((Double.parseDouble(sla.toString())<0.0) || (Double.parseDouble(sla.toString())>120.0)))
		{
			slaa.add(l.get("order_id").toString());
		}
		
	   if(prep_time!=null && ((Double.parseDouble(prep_time.toString())<0.0) || (Double.parseDouble(prep_time.toString())>60.0)))
		{
			preptime.add(l.get("order_id").toString());
		}
		if(placement_delay!=null && ((Double.parseDouble(placement_delay.toString())<0.0) || (Double.parseDouble(placement_delay.toString())>20.0)))
		{
			placementdelay.add(l.get("order_id").toString());
		}
		if( de_wait_time!=null && ((Double.parseDouble(de_wait_time.toString())<0.0) || (Double.parseDouble(de_wait_time.toString())>60.0)))
		{
			dewaittime.add(l.get("order_id").toString());
		}
		if(fmt!=null && ((Double.parseDouble(fmt.toString())<0.0) || (Double.parseDouble(fmt.toString())>60.0)))
		{
			fmtt.add(l.get("order_id").toString());
		}
		if(lmt!=null &&  ((Double.parseDouble(lmt.toString())<0.0) || (Double.parseDouble(lmt.toString())>60.0)))
		{
			lmtt.add(l.get("order_id").toString());
		}
		}
		}
		bw.write("Batch_Count =0 or Batch_Count > 5"+"\n");
		for(int i=0;i<batchcountt.size();i++)
		{
			bw.write(batchcountt.get(i)+"\n");
		}
		bw.write(",");
		bw.write("Ordered_time <1513402721000  or Ordered_time >1766016000"+"\n");
		/*for(int i=0;i<orderedtime.size();i++)
		{
			bw.write(orderedtime.get(i)+"\n");
		}
		bw.write(",");
		bw.write("assigned_time <1513402721000  or assigned_time >1766016000"+"\n");
		for(int i=0;i<assignedtime.size();i++)
		{
			bw.write(assignedtime.get(i)+"\n");
		}
		bw.write(",");
		bw.write("confirmation_time <1513402721000  or confirmation_time >1766016000"+"\n");
		for(int i=0;i<confirmationtime.size();i++)
		{
			bw.write(confirmationtime.get(i)+"\n");
		}
		bw.write(",");
		bw.write("arrived_time <1513402721000  or arrived_time >1766016000"+"\n");
		for(int i=0;i<arrivedtime.size();i++)
		{
			bw.write(arrivedtime.get(i)+"\n");
		}
		bw.write(",");
		bw.write("pickedup_time <1513402721000  or pickedup_time >1766016000"+"\n");
		for(int i=0;i<pickeduptime.size();i++)
		{
			bw.write(pickeduptime.get(i)+"\n");
		}
		bw.write(",");
		bw.write("reached_time <1513402721000  or reached_time >1766016000"+"\n");
		for(int i=0;i<reachedtime.size();i++)
		{
			bw.write(reachedtime.get(i)+"\n");
		}
		bw.write(",");
		bw.write("delivered_time <1513402721000  or delivered_time >1766016000"+"\n");
		for(int i=0;i<deliveredtime.size();i++)
		{
			bw.write(deliveredtime.get(i)+"\n");
		}
		bw.write(",");
		bw.write("received_time <1513402721000  or received_time >1766016000"+"\n");
		for(int i=0;i<receivedtime.size();i++)
		{
			bw.write(receivedtime.get(i)+"\n");
		}*/
		bw.write(",");
		bw.write("Order_reject_count < 0 or Order_reject_count > 10 "+"\n");
		for(int i=0;i<orderrejectcount.size();i++)
		{
			bw.write(orderrejectcount.get(i)+"\n");
		}
		bw.write(",");
		bw.write("Batch_type > 4"+"\n");
		for(int i=0;i<batchtype.size();i++)
		{
			bw.write(batchtype.get(i)+"\n");
		}
		bw.write(",");
		bw.write("Order_weight < 1 or Order_weight > 2.5"+"\n");
		for(int i=0;i<orderweight.size();i++)
		{
			bw.write(orderweight.get(i)+"\n");
		}
		bw.append(",");
		bw.write("Mgl_weight < 0.9 or Mgl_weight > 2.5"+"\n");
		for(int i=0;i<Mglweight.size();i++)
		{
			bw.write(Mglweight.get(i)+"\n");
		}
		bw.write(",");
		bw.write("Rating > 5 or rating < 0"+"\n");
		for(int i=0;i<rattings.size();i++)
		{
			bw.write(rattings.get(i)+"\n");
		}
		bw.write(",");
		bw.write("Last_mile <= 0 or last_mile > 10"+"\n");
		for(int i=0;i<lastmile.size();i++)
		{
			bw.write(lastmile.get(i)+"\n");
		}
		bw.write(",");
		bw.write("First_mile <= 0 or First_mile > 10"+"\n");
		for(int i=0;i<firstmile.size();i++)
		{
			bw.write(firstmile.get(i)+"\n");
		}
		bw.write(",");
		bw.write("Rain_mode_type > 5"+"\n");
		for(int i=0;i<rainmodetype.size();i++)
		{
			bw.write(rainmodetype.get(i)+"\n");
		}
		bw.write(",");
		bw.write("Sla > 120 or sla <= 0"+"\n");
		for(int i=0;i<slaa.size();i++)
		{
			bw.write(slaa.get(i)+"\n");
		}
		bw.write(",");
		bw.write("Prep_time <= 0 or prep_time > 60 "+"\n");
		for(int i=0;i<preptime.size();i++)
		{
			bw.write(preptime.get(i)+"\n");
		}
		bw.write(",");
		bw.write("Placement_delay <= 0 or placement_delay > 20 "+"\n");
		for(int i=0;i<placementdelay.size();i++)
		{
			bw.write(placementdelay.get(i)+"\n");
		}
		bw.write(",");
		bw.write("De_wait_time <= 0 or de_wait_time > 60"+"\n");
		for(int i=0;i<dewaittime.size();i++)
		{
			bw.write(dewaittime.get(i)+"\n");
		}
		bw.write(",");
		bw.write("Fmt <= 0 or fmt > 60 "+"\n");
		for(int i=0;i<fmtt.size();i++)
		{
			bw.write(fmtt.get(i)+"\n");
		}
		bw.write(",");
		bw.write("Lmt <= 0 or lmt > 60"+"\n");
		for(int i=0;i<lmtt.size();i++)
		{
			bw.write(lmtt.get(i)+"\n");
		}
				
		bw.close();

		}
	
	
}