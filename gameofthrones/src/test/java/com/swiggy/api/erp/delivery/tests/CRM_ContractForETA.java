package com.swiggy.api.erp.delivery.tests;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.delivery.helper.DeliveryControllerHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryDataHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceHelper;
import com.swiggy.api.erp.delivery.helper.ETA_ServiceHelper;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.KafkaHelper;
import framework.gameofthrones.Tyrion.RedisHelper;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * 	Created by somnath.nayak on 20/11/18.
 */


public class CRM_ContractForETA {
    ETA_ServiceHelper eta_serviceHelper = new ETA_ServiceHelper();
    DeliveryDataHelper deliveryDataHelper = new DeliveryDataHelper();
    DeliveryServiceHelper deliveryServiceHelper = new DeliveryServiceHelper();
    DeliveryControllerHelper deliveryControllerHelper = new DeliveryControllerHelper();
    RedisHelper redisHelper = new RedisHelper();
    ObjectMapper mapper = new ObjectMapper();

    static String de_id = null;
    static String zone = null;
    static String area = null;
    boolean executeAfterMethod = true;

    @BeforeMethod
    public void DE_Creation(){
        zone =eta_serviceHelper.getRestaurantDetails().get("rest_zone");
        area =eta_serviceHelper.getRestaurantDetails().get("rest_area");
        de_id = deliveryDataHelper.CreateDE(Integer.parseInt(zone));
        deliveryDataHelper.updateDELocationInRedis(de_id);
    }

    @AfterMethod
    public void DE_Deletion(){
        if (!executeAfterMethod) {
            System.out.println("********** Skipping AfterMethod for this test **********");
        }else { deliveryDataHelper.DeleteDE(de_id,area); }
        executeAfterMethod=true;
    }

    @org.testng.annotations.Test(description = "Verify that \"statusCode\":200 and \"statusMessage\": success and \"data\": null")
    public void DS_01() {
        String orderId = eta_serviceHelper.getOrderId();
        if(!orderId.equals(null)) {
            Processor processor = eta_serviceHelper.getServiceOrderTracking(orderId);
            String response = processor.ResponseValidator.GetBodyAsText();
            String statusCode = JsonPath.read(response, "$.statusCode").toString().replace("[", "").replace("]", "");
            String statusMessage = JsonPath.read(response, "$.statusMessage").toString().replace("[", "").replace("]", "");
            Assert.assertEquals(statusCode, "200");
            Assert.assertEquals(statusMessage, "Success");
        }else {
            Assert.fail();
        }
    }

    @org.testng.annotations.Test(description = "Verify that \"delivery_address\" is reflecting properly in api response")
    public void DS_02(){
        String orderId = eta_serviceHelper.getOrderId();
        if(!orderId.equals(null)) {
            deliveryServiceHelper.assignOrder(orderId, de_id);
            Processor processor = eta_serviceHelper.getServiceOrderTracking(orderId);
            String response = processor.ResponseValidator.GetBodyAsText();
            String delivery_address = JsonPath.read(response, "$.data.delivery_address").toString().replace("[", "").replace("]", "");
            String customerLat = eta_serviceHelper.getRestaurantDetails().get("ApproxCustomerLat");
            String customerLng = eta_serviceHelper.getRestaurantDetails().get("ApproxCustomerLng");
            Assert.assertEquals(delivery_address,customerLat+","+customerLng);
        }else {
            Assert.fail();
        }
    }

    @org.testng.annotations.Test(description = "Verify that \"de_location\" is reflecting properly in api response")
    public void DS_03(){
        String orderId = eta_serviceHelper.getOrderId();
        if(!orderId.equals(null)) {
            deliveryServiceHelper.assignOrder(orderId, de_id);
            Processor processor = eta_serviceHelper.getServiceOrderTracking(orderId);
            String response = processor.ResponseValidator.GetBodyAsText();
            String de_lat = JsonPath.read(response, "$.data.lat").toString().replace("[", "").replace("]", "");
            String de_lng = JsonPath.read(response, "$.data.lng").toString().replace("[", "").replace("]", "");
            Object de_details = redisHelper.getValue("deliveryredis",1,"DE_Location_"+de_id);
            String json = "";
            try {
                json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(de_details).replace("\\", "").replace("\"{", "{");
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            String de_lat_redis = JsonPath.read(json, "$.lat").toString().replace("[", "").replace("]", "");
            String de_lng_redis = JsonPath.read(json, "$.lng").toString().replace("[", "").replace("]", "");
            Assert.assertEquals(de_lat,de_lat_redis);
            Assert.assertEquals(de_lng,de_lng_redis);
        }else {
            Assert.fail();
        }
    }

    @org.testng.annotations.Test(description = "Verify that \"de_location\" and de_details are reflecting properly in api response")
    public void DS_04(){
        String orderId = eta_serviceHelper.getOrderId();
        if(!orderId.equals(null)) {
            deliveryServiceHelper.assignOrder(orderId, de_id);
            Processor processor = eta_serviceHelper.getServiceOrderTracking(orderId);
            String response = processor.ResponseValidator.GetBodyAsText();
            String de_mobile = JsonPath.read(response, "$.data.mobile").toString().replace("[", "").replace("]", "");
            String de_name = JsonPath.read(response, "$.data.name").toString().replace("[", "").replace("]", "");
            Object de_details = redisHelper.getValue("deliveryredis",1,"DE_Location_"+de_id);
            String json = "";
            try {
                json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(de_details).replace("\\", "").replace("\"{", "{");
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            String de_mobile_redis = JsonPath.read(json, "$.mobile").toString().replace("[", "").replace("]", "");
            String de_name_redis = JsonPath.read(json, "$.name").toString().replace("[", "").replace("]", "");
            Assert.assertEquals(de_mobile,de_mobile_redis);
            Assert.assertEquals(de_name,de_name_redis);
        }else {
            Assert.fail();
        }
    }

    @org.testng.annotations.Test(description = "Verify that \"delivery_status\" should be \"assigned\" ")
    public void DS_05() {
        String orderId = eta_serviceHelper.getOrderId();
        if (!orderId.equals(null)) {
            deliveryServiceHelper.assignOrder(orderId, de_id);
            Processor processor = eta_serviceHelper.getServiceOrderTracking(orderId);
            String response = processor.ResponseValidator.GetBodyAsText();
            String delivery_status = JsonPath.read(response, "$.data.delivery_status").toString().replace("[", "").replace("]", "");
            Assert.assertEquals(delivery_status,"assigned");
        }else {
            Assert.fail();
        }
    }

    @org.testng.annotations.Test(description = "Verify that \"status\" should be pending of \"PICKUP\" task under \"batched_destinations\" segment")
    public void DS_06() {
        String orderId = eta_serviceHelper.getOrderId();
        if (!orderId.equals(null)) {
            deliveryServiceHelper.assignOrder(orderId, de_id);
            Processor processor = eta_serviceHelper.getServiceOrderTracking(orderId);
            String response = processor.ResponseValidator.GetBodyAsText();
            String batched_destinations = JsonPath.read(response, "$.data.batched_destinations").toString();
            String status = JsonPath.read(batched_destinations, "$..[?(@.task_type==\"pickup\" && @.order_id=="+orderId+")].status").toString().replace("[\"", "").replace("\"]", "");
            Assert.assertEquals(status,"pending");
        }else {
            Assert.fail();
        }
    }

    @org.testng.annotations.Test(description = "Verify that \"lat_long\" of PICKUP task should be restaurant lat_lng")
    public void DS_07() {
        String orderId = eta_serviceHelper.getOrderId();
        if (!orderId.equals(null)) {
            deliveryServiceHelper.assignOrder(orderId, de_id);
            Processor processor = eta_serviceHelper.getServiceOrderTracking(orderId);
            String response = processor.ResponseValidator.GetBodyAsText();
            String batched_destinations = JsonPath.read(response, "$.data.batched_destinations").toString();
            String pickup_lat_lng = JsonPath.read(batched_destinations, "$..[?(@.task_type==\"pickup\" && @.order_id=="+orderId+")].lat_long").toString().replace("[\"", "").replace("\"]", "");
            String restaurantLatLng = eta_serviceHelper.getRestaurantDetails().get("restLatLng");
            Assert.assertEquals(pickup_lat_lng,restaurantLatLng);
        }else {
            Assert.fail();
        }
    }

    @org.testng.annotations.Test(description = "Verify that \"status\" should be pending of \"DROP\" task under \"batched_destinations\" segment")
    public void DS_08() {
        String orderId = eta_serviceHelper.getOrderId();
        if (!orderId.equals(null)) {
            deliveryServiceHelper.assignOrder(orderId, de_id);
            Processor processor = eta_serviceHelper.getServiceOrderTracking(orderId);
            String response = processor.ResponseValidator.GetBodyAsText();
            String batched_destinations = JsonPath.read(response, "$.data.batched_destinations").toString();
            String status = JsonPath.read(batched_destinations, "$..[?(@.task_type==\"drop\" && @.order_id=="+orderId+")].status").toString().replace("[\"", "").replace("\"]", "");
            Assert.assertEquals(status,"pending");
        }else {
            Assert.fail();
        }
    }

    @org.testng.annotations.Test(description = "Verify that \"lat_long\" of DROP task should be customer lat_lng")
    public void DS_09() {
        String orderId = eta_serviceHelper.getOrderId();
        if (!orderId.equals(null)) {
            deliveryServiceHelper.assignOrder(orderId, de_id);
            Processor processor = eta_serviceHelper.getServiceOrderTracking(orderId);
            String response = processor.ResponseValidator.GetBodyAsText();
            String batched_destinations = JsonPath.read(response, "$.data.batched_destinations").toString();
            String drop_lat_lng = JsonPath.read(batched_destinations, "$..[?(@.task_type==\"drop\" && @.order_id=="+orderId+")].lat_long").toString().replace("[\"", "").replace("\"]", "");
            String customerLatLng = eta_serviceHelper.getRestaurantDetails().get("ApproxCustomerLat")+","+eta_serviceHelper.getRestaurantDetails().get("ApproxCustomerLng");
            Assert.assertEquals(drop_lat_lng,customerLatLng);
        }else {
            Assert.fail();
        }
    }

    @org.testng.annotations.Test(description = "Verify that \"delivery_status\" should be \"confirmed\"")
    public void DS_10() {
        String orderId = eta_serviceHelper.getOrderId();
        if (!orderId.equals(null)) {
            deliveryServiceHelper.assignOrder(orderId, de_id);
            eta_serviceHelper.processOrder(orderId,"confirmed");
            Processor processor = eta_serviceHelper.getServiceOrderTracking(orderId);
            String response = processor.ResponseValidator.GetBodyAsText();
            String delivery_status = JsonPath.read(response, "$.data.delivery_status").toString().replace("[", "").replace("]", "");
            Assert.assertEquals(delivery_status,"confirmed");
        }else {
            Assert.fail();
        }
    }

    @org.testng.annotations.Test(description = "Verify that \"status\" should be inprogress of \"PICKUP\" task under \"batched_destinations\" segment")
    public void DS_11() {
        String orderId = eta_serviceHelper.getOrderId();
        if (!orderId.equals(null)) {
            deliveryServiceHelper.assignOrder(orderId, de_id);
            eta_serviceHelper.processOrder(orderId,"confirmed");
            Processor processor = eta_serviceHelper.getServiceOrderTracking(orderId);
            String response = processor.ResponseValidator.GetBodyAsText();
            String batched_destinations = JsonPath.read(response, "$.data.batched_destinations").toString();
            String status = JsonPath.read(batched_destinations, "$..[?(@.task_type==\"pickup\" && @.order_id=="+orderId+")].status").toString().replace("[\"", "").replace("\"]", "");
            Assert.assertEquals(status,"inprogress");
        }else {
            Assert.fail();
        }
    }

    @org.testng.annotations.Test(description = "Verify that \"delivery_status\" should reflect as \"arrived\"")
    public void DS_12() {
        String orderId = eta_serviceHelper.getOrderId();
        if (!orderId.equals(null)) {
            deliveryServiceHelper.assignOrder(orderId, de_id);
            eta_serviceHelper.processOrder(orderId,"arrived");
            Processor processor = eta_serviceHelper.getServiceOrderTracking(orderId);
            String response = processor.ResponseValidator.GetBodyAsText();
            String delivery_status = JsonPath.read(response, "$.data.delivery_status").toString().replace("[", "").replace("]", "");
            Assert.assertEquals(delivery_status,"arrived");
        }else {
            Assert.fail();
        }
    }

    @org.testng.annotations.Test(description = "Verify that \"status\" should be inprogress of \"PICKUP\" task under \"batched_destinations\" segment")
    public void DS_13() {
        String orderId = eta_serviceHelper.getOrderId();
        if (!orderId.equals(null)) {
            deliveryServiceHelper.assignOrder(orderId, de_id);
            eta_serviceHelper.processOrder(orderId,"arrived");
            Processor processor = eta_serviceHelper.getServiceOrderTracking(orderId);
            String response = processor.ResponseValidator.GetBodyAsText();
            String batched_destinations = JsonPath.read(response, "$.data.batched_destinations").toString();
            String status = JsonPath.read(batched_destinations, "$..[?(@.task_type==\"pickup\" && @.order_id=="+orderId+")].status").toString().replace("[\"", "").replace("\"]", "");
            Assert.assertEquals(status,"inprogress");
        }else {
            Assert.fail();
        }
    }

    @org.testng.annotations.Test(description = "Verify that \"delivery_status\" should reflect as \"pickedup\" ")
    public void DS_14() {
        String orderId = eta_serviceHelper.getOrderId();
        if (!orderId.equals(null)) {
            deliveryServiceHelper.assignOrder(orderId, de_id);
            eta_serviceHelper.processOrder(orderId,"pickedUp");
            Processor processor = eta_serviceHelper.getServiceOrderTracking(orderId);
            String response = processor.ResponseValidator.GetBodyAsText();
            String delivery_status = JsonPath.read(response, "$.data.delivery_status").toString().replace("[", "").replace("]", "");
            Assert.assertEquals(delivery_status,"pickedup");
        }else {
            Assert.fail();
        }
    }

    @org.testng.annotations.Test(description = "Verify that \"status\" should be completed of \"PICKUP\" task under \"batched_destinations\" segment")
    public void DS_15() {
        String orderId = eta_serviceHelper.getOrderId();
        if (!orderId.equals(null)) {
            deliveryServiceHelper.assignOrder(orderId, de_id);
            eta_serviceHelper.processOrder(orderId,"pickedUp");
            Processor processor = eta_serviceHelper.getServiceOrderTracking(orderId);
            String response = processor.ResponseValidator.GetBodyAsText();
            String batched_destinations = JsonPath.read(response, "$.data.batched_destinations").toString();
            String status = JsonPath.read(batched_destinations, "$..[?(@.task_type==\"pickup\" && @.order_id=="+orderId+")].status").toString().replace("[\"", "").replace("\"]", "");
            Assert.assertEquals(status,"completed");
        }else {
            Assert.fail();
        }
    }

    @org.testng.annotations.Test(description = "Verify that \"status\" should be inprogress of \"DROP\" task under \"batched_destinations\" segment")
    public void DS_16() {
        String orderId = eta_serviceHelper.getOrderId();
        if (!orderId.equals(null)) {
            deliveryServiceHelper.assignOrder(orderId, de_id);
            eta_serviceHelper.processOrder(orderId,"pickedUp");
            Processor processor = eta_serviceHelper.getServiceOrderTracking(orderId);
            String response = processor.ResponseValidator.GetBodyAsText();
            String batched_destinations = JsonPath.read(response, "$.data.batched_destinations").toString();
            String status = JsonPath.read(batched_destinations, "$..[?(@.task_type==\"drop\" && @.order_id=="+orderId+")].status").toString().replace("[\"", "").replace("\"]", "");
            Assert.assertEquals(status,"inprogress");
        }else {
            Assert.fail();
        }
    }

    @org.testng.annotations.Test(description = "Verify that \"delivery_status\" should reflect as \"reached\" in api response")
    public void DS_17() {
        String orderId = eta_serviceHelper.getOrderId();
        if (!orderId.equals(null)) {
            deliveryServiceHelper.assignOrder(orderId, de_id);
            eta_serviceHelper.processOrder(orderId,"reached");
            Processor processor = eta_serviceHelper.getServiceOrderTracking(orderId);
            String response = processor.ResponseValidator.GetBodyAsText();
            String delivery_status = JsonPath.read(response, "$.data.delivery_status").toString().replace("[", "").replace("]", "");
            Assert.assertEquals(delivery_status,"reached");
        }else {
            Assert.fail();
        }
    }

    @org.testng.annotations.Test(description = "Verify that \"status\" should be inprogress of \"DROP\" task under \"batched_destinations\" segment in api response")
    public void DS_18() {
        String orderId = eta_serviceHelper.getOrderId();
        if (!orderId.equals(null)) {
            deliveryServiceHelper.assignOrder(orderId, de_id);
            eta_serviceHelper.processOrder(orderId,"reached");
            Processor processor = eta_serviceHelper.getServiceOrderTracking(orderId);
            String response = processor.ResponseValidator.GetBodyAsText();
            String batched_destinations = JsonPath.read(response, "$.data.batched_destinations").toString();
            String status = JsonPath.read(batched_destinations, "$..[?(@.task_type==\"drop\" && @.order_id=="+orderId+")].status").toString().replace("[\"", "").replace("\"]", "");
            Assert.assertEquals(status,"inprogress");
        }else {
            Assert.fail();
        }
    }

    @org.testng.annotations.Test(description = "Verify that \"delivery_status\" should reflect as \"delivered \" in api response")
    public void DS_19() {
        String orderId = eta_serviceHelper.getOrderId();
        if (!orderId.equals(null)) {
            deliveryServiceHelper.assignOrder(orderId, de_id);
            eta_serviceHelper.processOrder(orderId,"delivered");
            Processor processor = eta_serviceHelper.getServiceOrderTracking(orderId);
            String response = processor.ResponseValidator.GetBodyAsText();
            String delivery_status = JsonPath.read(response, "$.data.delivery_status").toString().replace("[", "").replace("]", "");
            Assert.assertEquals(delivery_status,"delivered");
        }else {
            Assert.fail();
        }
    }

    @org.testng.annotations.Test(description = "Verify that \"status\" should be completed of \"DROP\" task under \"batched_destinations\" segment in api response")
    public void DS_20() {
        String orderId = eta_serviceHelper.getOrderId();
        if (!orderId.equals(null)) {
            deliveryServiceHelper.assignOrder(orderId, de_id);
            eta_serviceHelper.processOrder(orderId,"delivered");
            Processor processor = eta_serviceHelper.getServiceOrderTracking(orderId);
            String response = processor.ResponseValidator.GetBodyAsText();
            String batched_destinations = JsonPath.read(response, "$.data.batched_destinations").toString();
            String status = JsonPath.read(batched_destinations, "$..[?(@.task_type==\"drop\" && @.order_id=="+orderId+")].status").toString().replace("[\"", "").replace("\"]", "");
            Assert.assertEquals(status,"completed");
        }else {
            Assert.fail();
        }
    }

    @org.testng.annotations.Test(description = "Verify that \"statusCode\":200 and \"statusMessage\": success and \"data\": null")
    public void DS_21(){
        String orderId = eta_serviceHelper.getOrderId();
        if(!orderId.equals(null)) {
            deliveryServiceHelper.assignOrder(orderId, de_id);
            eta_serviceHelper.processOrder(orderId,"deRejected");
            Processor processor = eta_serviceHelper.getServiceOrderTracking(orderId);
            String response = processor.ResponseValidator.GetBodyAsText();
            String statusCode = JsonPath.read(response, "$.statusCode").toString().replace("[", "").replace("]", "");
            String statusMessage = JsonPath.read(response, "$.statusMessage").toString().replace("[", "").replace("]", "");
            String data = JsonPath.read(response, "$.data");
            Assert.assertEquals(statusCode, "200");
            Assert.assertEquals(statusMessage, "Success");
            Assert.assertEquals(data,"null");
        }else {
            Assert.fail();
        }
    }

    @Test(description = "Verify that \"statusCode\":200 and \"statusMessage\": success and \"data\": null")
    public void DS_22() {
        String orderId = eta_serviceHelper.getOrderId();
        if(!orderId.equals(null)) {
            deliveryServiceHelper.assignOrder(orderId, de_id);
            //deliveryServiceHelper.reassign(orderId, DeliveryConstant.reassign_status_code,eta_serviceHelper.getRestaurantLatlng_CityId(restaurant_id).get(1));
            //deliveryControllerHelper.reasssignFromFF(orderId,eta_serviceHelper.getRestaurantLatlng_CityId(restaurant_id).get(1));
            deliveryControllerHelper.reasssignFromFF(orderId,eta_serviceHelper.getRestaurantDetails().get("city_id"));
            Processor processor = eta_serviceHelper.getServiceOrderTracking(orderId);
            String response = processor.ResponseValidator.GetBodyAsText();
            String statusCode = JsonPath.read(response, "$.statusCode").toString().replace("[", "").replace("]", "");
            String statusMessage = JsonPath.read(response, "$.statusMessage").toString().replace("[", "").replace("]", "");
            String data = JsonPath.read(response, "$.data");
            Assert.assertEquals(statusCode, "200");
            Assert.assertEquals(statusMessage, "Success");
            Assert.assertEquals(data,"null");
        }else {
            Assert.fail();
        }
    }

}
