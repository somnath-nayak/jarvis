package com.swiggy.api.erp.vms.tests;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.vms.dp.RMSSelfServeDP;
import com.swiggy.api.erp.vms.helper.RMSCommonHelper;
import com.swiggy.api.erp.vms.helper.RMSSelfServeHelper;
import framework.gameofthrones.Cersei.ToolBox;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import io.advantageous.boon.core.Str;
import io.advantageous.boon.core.Sys;
import jdk.nashorn.internal.parser.JSONParser;
import org.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.io.IOException;
import java.util.List;
import org.json.*;


public class RMSSelfServeTest extends RMSSelfServeDP {
        RMSSelfServeHelper helper = new RMSSelfServeHelper();
        SchemaValidatorUtils schemaValidatorUtils = new SchemaValidatorUtils();


        @Test(dataProvider = "self_Serve_Menu_GET_MENU_DP", groups = { "sanity",
                "smoke" },  description = "GETrestaurant/v1/fetchItemsFromSnd: self_Serve_Menu_GET_MENU")
        public void self_Serve_Menu_GET_MENU(String accessToken, String restaurantId, String SSMM_statusMessage)
                throws IOException, ProcessingException {

            Processor processor = helper.selfServeMenuGETMENU(accessToken, restaurantId);
            String resp = processor.ResponseValidator.GetBodyAsText();

            String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/SchemaSet/Json/VMS/selfServeGetMenu");
            List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
            Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For SelfServeGetMenu API");
            boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
            System.out.println("Contain empty nodes => " + isEmpty);

            Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
            Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), SSMM_statusMessage);
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.name"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.uuid"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.area"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cuisine"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.recommended"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.costForTwo"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.costForTwoString"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.deliveryCharge"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.minimumOrder"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.opened"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.categories.[0].name"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.categories.[0].id"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.categories.[0].menu.[0].id"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.deliveryTime"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.minDeliveryTime"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.maxDeliveryTime"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slugs.restaurant"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slugs.city"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slugs.area"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tmpClosed"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cityState"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.address"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.postalCode"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.latLong"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cityDeliveryCharge"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.locality"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.delivery_fee_type"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.unserviceable"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.metaInfo"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.longDistanceEnabled"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.new"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.favorite"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.veg"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.select"));

        }

        @Test(dataProvider = "self_Serve_Menu_GET_MENU_DP1", groups = {
                "regression" },  description = "restaurant/v1/fetchItemsFromSnd: self_Serve_Menu_GET_MENU_DP regression use cases")
        public void self_Serve_Menu_GET_MENU_DP1(String accessToken, String restaurantId, int expectedStatusCode, String expectedStatusMessage)
                throws IOException, ProcessingException {

            Processor processor = helper.selfServeMenuGETMENU(accessToken, restaurantId);
            Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), expectedStatusCode);
            Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), expectedStatusMessage);
        }

        @Test(dataProvider = "self_Serve_Menu_Calculate_Service_DP", groups = { "sanity",
                "smoke" },  description = "GETitems/v1/itemEdition/caluclateservice: self_Serve_Menu_Calculate_Service")
        public void self_Serve_Menu_Calculate_Service(String accessToken, String packagingCharges, String price, String restaurantid,
                                                      String SSMCS_statusMessage) throws IOException, ProcessingException {

            Processor processor = helper.selfServeMenuCalcualteService(accessToken, packagingCharges, price, restaurantid);
            String resp = processor.ResponseValidator.GetBodyAsText();

            String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/SchemaSet/Json/VMS/selfServeCalculateService");
            List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
            Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For SelfServeGetMenu API");
            boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
            System.out.println("Contain empty nodes => " + isEmpty);

            Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
            Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), SSMCS_statusMessage);
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.price"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.service_charges"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.service_tax"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.vat"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.packaging_charges"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.total"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.igst"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cgst"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.sgst"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.gst_inclusive"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.gst_total"));

        }

        @Test(dataProvider = "self_Serve_Menu_Calculate_Service_DP1", groups = {
                "regression" },  description = "GETitems/v1/itemEdition/caluclateservice: self_Serve_Menu_Calculate_Service regression use cases")
        public void self_Serve_Menu_Calculate_Service_DP1(String accessToken, String packagingCharges, String price, String restaurantid,
                                                          int expectedStatusCode, String expectedStatusMessage) throws IOException, ProcessingException {

            RMSSelfServeHelper helper = new RMSSelfServeHelper();
            Processor processor = helper.selfServeMenuCalcualteService(accessToken, packagingCharges, price, restaurantid);
            Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), expectedStatusCode);
            Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), expectedStatusMessage);
        }

        @Test(dataProvider = "self_Serve_Menu_Revison_History_DP", groups = { "sanity",
                "smoke" },  description = "GET/items/v1/revisionHistory: self_Serve_Menu_Revison_History")
        public void self_Serve_Menu_Revison_History(String accessToken, String restaurantId, String SSMRH_statusMessage)
                throws IOException, ProcessingException {

            Processor processor = helper.selfServeMenuRevisonHistory(accessToken, restaurantId);
            String resp = processor.ResponseValidator.GetBodyAsText();

            String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/SchemaSet/Json/VMS/selfServeRevisionHistory");
            List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
            Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For selfServeRevisionHistory API");
            boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
            System.out.println("Contain empty nodes => " + isEmpty);

            Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
            Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), SSMRH_statusMessage);
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..page"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..size"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..rows_per_page"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..search_data.[0].id"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..search_data.[0].createdAt"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..search_data.[0].restaurantId"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..search_data.[0].uniqueId"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..search_data.[0].data"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..search_data.[0].status"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..search_data.[0].item_name"));
        }

        @Test(dataProvider = "self_Serve_Menu_Revison_History_DP1", groups = {
                "regression" },  description = "GET/items/v1/revisionHistory: self_Serve_Menu_Revison_History regression use cases")
        public void self_Serve_Menu_Revison_History_DP1(String accessToken, String restaurantId, int expectedStatusCode,
                                                        String expectedStatusMessage) throws IOException, ProcessingException {

            RMSSelfServeHelper helper = new RMSSelfServeHelper();

            Processor processor = helper.selfServeMenuRevisonHistory(accessToken, restaurantId);
            Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), expectedStatusCode);
            Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), expectedStatusMessage);
        }

        @Test(dataProvider = "self_Serve_Menu_Item_Ticket_DP", groups = { "sanity",
                "smoke" },  description = "sGET/items/v1/itemTicket: elf_Serve_Menu_ItemTicket")
        public void self_Serve_Menu_Item_Ticket(String accessToken, String restaurantId, String ticketId, String SSMIT_statusMessage)
                throws IOException, ProcessingException {

            Processor processor = helper.selfServeMenuItemTicket(accessToken, restaurantId, ticketId);
            String resp = processor.ResponseValidator.GetBodyAsText();

            String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/SchemaSet/Json/VMS/selfServeItemTicket");
            List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
            Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For selfServeItemTicket API");
            boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
            System.out.println("Contain empty nodes => " + isEmpty);
            Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
            Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), SSMIT_statusMessage);

            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.createdAt"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurantId"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.uniqueId"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.data"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.status"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.updated_at"));
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.item_name"));
        }

        @Test(dataProvider = "self_Serve_Menu_Item_Ticket_DP1", groups = {
                "regression" },  description = "GET/items/v1/itemTicket: self_Serve_Menu_Item_Ticket_DP regression use cases")
        public void self_Serve_Menu_Item_Ticket_DP1(String accessToken, String restaurantId, String ticketId, int expectedStatusCode,
                                                    String expectedStatusMessage) throws IOException, ProcessingException {

            Processor processor = helper.selfServeMenuItemTicket(accessToken, restaurantId, ticketId);
            Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), expectedStatusCode);
            Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), expectedStatusMessage);
        }

        @Test(dataProvider = "Self_Serve_Menu_add_NEW_TEM_DP", groups = { "sanity",
                "smoke" },  description = "POST/items/v1/itemEdition: Self_Serve_Menu_add_NEWITEM")
        public void Self_Serve_Menu_add_NEW_ITEM(String accessToken, String name, String is_veg, String packing_charges, String description,
                                                 String item_change_request, String addons_and_variant_change_request, String restId, String item_Id,
                                                 String markInStock, String s3_image_url, String price, String category_id, String main_category_id,
                                                 String SSMANI_statusMessage) {

            Processor processor = helper.selfServeMenuAddNewItem(accessToken, name, is_veg, packing_charges, description,
                    item_change_request, addons_and_variant_change_request, restId, item_Id, markInStock, s3_image_url,
                    price, category_id, main_category_id);
            /* No need for schema validation */

            Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
            Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), SSMANI_statusMessage);
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));

        }

        @Test(dataProvider = "Self_Serve_Menu_add_NEW_TEM_DP1", groups = {
                "regression" },  description = "POST/items/v1/itemEdition: Self_Serve_Menu_add_NEW_ITEM regression cases")
        public void Self_Serve_Menu_add_NEW_ITEM1(String accessToken, String name, String is_veg, String packing_charges, String description,
                                                  String item_change_request, String addons_and_variant_change_request, String restId, String item_Id,
                                                  String markInStock, String s3_image_url, String price, String category_id, String main_category_id,
                                                  int expectedStatusCode, String expectedStatusMessage) {

            Processor processor = helper.selfServeMenuAddNewItem(accessToken, name, is_veg, packing_charges, description,
                    item_change_request, addons_and_variant_change_request, restId, item_Id, markInStock, s3_image_url,
                    price, category_id, main_category_id);

            Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), expectedStatusCode);
            Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), expectedStatusMessage);

        }

        @Test(dataProvider = "self_Serve_Menu_Edit_ITEM_DP", groups = { "sanity",
                "smoke" },  description = "PUT/items/v1/itemEdition: self_Serve_Menu_Edit_ITEM_DP")
        public void self_Serve_Menu_Edit_ITEM(
                String accessToken, String item_Id, String restaurantId, String description, String seller_tier, String SSMEI_statusMessage) {

            Processor processor = helper.selfServeMenuEditITEM(accessToken, item_Id, restaurantId, description, seller_tier);

            /* No need for schema validation */
            Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
            Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), SSMEI_statusMessage);
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));

        }

        @Test(dataProvider = "self_Serve_Menu_Edit_ITEM_DP1", groups = {
                "regression" },  description = "PUT/items/v1/itemEdition: self_Serve_Menu_Edit_ITEM_DP")
        public void self_Serve_Menu_Edit_ITEM1(String accessToken, String item_Id, String restaurantId, String description, String seller_tier,
                                               int expectedStatusCode, String expectedStatusMessage) {

            Processor processor = helper.selfServeMenuEditITEM(accessToken, item_Id, restaurantId, description, seller_tier);

            Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), expectedStatusCode);
            Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), expectedStatusMessage);

        }

        @Test(dataProvider = "self_Serve_Menu_Cancel_Ticket_DP", groups = { "sanity",
                "smoke" },  description = "DELETE/items/v1/cancelTicket: self_Serve_Menu_Cancel_Ticket")
        public void self_Serve_Menu_Cancel_Ticket(String accessToken, String restaurantId, String SSMCT_statusMessage) {

            String ticketId = helper.selfServeMenuRevisonHistory(accessToken,restaurantId, "PENDING_SUBMISSION").
                    ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.search_data.[0].id");
            System.out.println("TicketIDdddddd" + ticketId);
            Processor processor = helper.selfServeMenuCancelTicket(accessToken, restaurantId, ticketId);

            /* No need for schema validation */
            Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
            Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), SSMCT_statusMessage);
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));

        }

        @Test(dataProvider = "self_Serve_Menu_Cancel_Ticket_DP1", groups = {
                "regression" },  description = "DELETE/items/v1/cancelTicket: self_Serve_Menu_Cancel_Ticket")
        public void self_Serve_Menu_Cancel_Ticket1(String accessToken, String restaurantId, String ticketId, int expectedStatusCode,
                                                   String expectedStatusMessage) {

            Processor processor = helper.selfServeMenuCancelTicket(accessToken, restaurantId, ticketId);

            Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), expectedStatusCode);
            Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), expectedStatusMessage);

        }

        @Test(dataProvider = "self_Serve_Menu_delete_Cart_Image_DP", groups = { "sanity",
                "smoke" },  description = "DELETE/items/v1/deleteCatlgeImage: self_Serve_Menu_delete_Cart_Image_DP")
        public void self_Serve_Menu_delete_Cart_Image(String accessToken, String restaurantId, String imageId, String SSMDCI_statusMessage) {

            Processor processor = helper.selfServeMenudeleteCartImage(accessToken, restaurantId, imageId);

            /* No need for schema validation */
            Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
            Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), SSMDCI_statusMessage);

        }

        @Test(dataProvider = "self_Serve_Menu_delete_Cart_Image_DP1", groups = {
                "regression" },  description = "DELETE/items/v1/deleteCatlgeImage: self_Serve_Menu_delete_Cart_Image_DP1")
        public void self_Serve_Menu_delete_Cart_Image1(String accessToken, String restaurantId, String imageId, int expectedStatusCode,
                                                       String expectedStatusMessage) {

            Processor processor = helper.selfServeMenudeleteCartImage(accessToken, restaurantId, imageId);

            Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), expectedStatusCode);
            Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), expectedStatusMessage);
        }

        @Test(dataProvider = "self_Serve_Menu_Delete_Item_DP", groups = { "sanity",
                "smoke" },  description = "POST/items/v1/deleteItem: self_Serve_Menu_Delete_Item_DP")
        public void self_Serve_Menu_Delete_Item(String accessToken, String restaurantId, String item_id, String SSMDI_statusMessage) {

            Processor processor = helper.selfServeMenuDeleteItem(accessToken, restaurantId, item_id);

            /*No need for schema validation */
            Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
            Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), SSMDI_statusMessage);
            Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));

        }

        @Test(dataProvider = "self_Serve_Menu_Delete_Item_DP1", groups = {
                "regression" },  description = "POST/items/v1/deleteItem: self_Serve_Menu_Delete_Item_DP1")
        public void self_Serve_Menu_Delete_Item1(String accessToken, String restaurantId, String item_id, int expectedStatusCode,
                                                 String expectedStatusMessage) {

            Processor processor = helper.selfServeMenuDeleteItem(accessToken, restaurantId, item_id);

            Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), expectedStatusCode);
            Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), expectedStatusMessage);

        }

        @Test(dataProvider = "self_Serve_Menu_Submit_Pending_Tickets_DP", groups = { "sanity",
                "smoke" },  description = "POSTitems/v1/submitPendingTickets: self_Serve_Menu_Submit_Pending_Tickets_DP")
        public void self_Serve_Menu_Submit_Pending_Tickets(String accessToken, String restaurantId,
                                                           String SSMSPT_statusMessage) throws Exception {
            if(!RMSCommonHelper.getEnv().equalsIgnoreCase("prod")) {
                String ticketId = helper.selfServeMenuRevisonHistory(accessToken, restaurantId, "PENDING_SUBMISSION").
                        ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.search_data.[0].id");
                System.out.println("TicketIDdddddd" + ticketId);
                Processor processor = helper.selfServeMenuSubmitPendingTickets(accessToken, restaurantId, ticketId);
                String resp = processor.ResponseValidator.GetBodyAsText();

                String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir") + "/../Data/SchemaSet/Json/VMS/selfServeSubmitTicket");
                List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
                Assert.assertTrue(missingNodeList.isEmpty(), missingNodeList + " Nodes Are Missing Or Not Matching For selfServeSubmitTicket API");
                boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
                System.out.println("Contain empty nodes => " + isEmpty);

                Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
                Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), SSMSPT_statusMessage);
                Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
            } else {
                System.out.println("This test will not run on production");
            }

        }

        @Test(dataProvider = "self_Serve_Menu_Submit_Pending_Tickets_DP1", groups = { "sanity",
                "smoke" },  description = "POSTitems/v1/submitPendingTickets: self_Serve_Menu_Submit_Pending_Tickets_DP")
        public void self_Serve_Menu_Submit_Pending_Tickets1(String accessToken, String restaurantId, String ticketIds, int expectedStatusCode,
                                                            String expectedStatusMessage) throws Exception {
            if(!RMSCommonHelper.getEnv().equalsIgnoreCase("prod")) {
                Processor processor = helper.selfServeMenuSubmitPendingTickets(accessToken, restaurantId, ticketIds);

                Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), expectedStatusCode);
                Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), expectedStatusMessage);
            } else {
                System.out.println("This test should not run on prod");
            }
        }

    @Test(dataProvider = "self_Serve_Item_Image_Upload_DP", groups = { "sanity",
            "smoke" },  description = "POST/items/v1/itemImage/: self_Serve_Menu_Delete_Item_DP")
    public void self_Serve_Item_Image_Upload(String accessToken, String file, String restaurantId, String statusMessage,
                                             String statusCode, String statusMessage1, String filename) throws IOException, ProcessingException, JSONException {

        Processor processor = helper.selfServeMenuItemImageUpload(accessToken, file, restaurantId);
        String resp = processor.ResponseValidator.GetBodyAsText();

        String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir") + "/../Data/SchemaSet/Json/VMS/selfServeItemImageUpload");
        List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
        Assert.assertTrue(missingNodeList.isEmpty(), missingNodeList + " Nodes Are Missing Or Not Matching For selfServeSubmitTicket API");
        boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
        System.out.println("Contain empty nodes => " + isEmpty);

        Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
        Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
        Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("$.data.statusCode"), 200);
        JSONObject obj = new JSONObject(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.body"));
        Assert.assertEquals(obj.get("statusCode").toString(), statusCode);
        Assert.assertEquals(obj.get("statusMessage").toString(), statusMessage1);
        JSONObject obj1 = new JSONObject(obj.get("data").toString());
        Assert.assertNotNull(obj1.get("id"));
        Assert.assertNotNull(obj1.get("uniqueId"));
        Assert.assertNotNull(obj1.get("sync_to_s3"));
        Assert.assertNotNull(obj1.get("sync_to_cdn"));
        Assert.assertNotNull(obj1.get("hash"));
        Assert.assertNotNull(obj1.get("path"));
        Assert.assertNotNull(obj1.get("s3_url"));
        Assert.assertNotNull(obj1.get("status"));
        Assert.assertNotNull(obj1.get("active"));
        Assert.assertTrue(obj1.get("s3_url").toString().contains(filename));

    }

    @Test(dataProvider = "self_Serve_Item_Image_Upload_Regression", groups = { "regression"},
            description = "POST/items/v1/itemImage/: self_Serve_Menu_Delete_Item_DP")
    public void self_Serve_Item_Image_Upload_Regression(String accessToken, String file, String restaurantId, String statusMessage, String statusCode, String statusMessage1)
            throws IOException, ProcessingException, JSONException {

        Processor processor = helper.selfServeMenuItemImageUpload(accessToken, file, restaurantId);
        Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
        Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
        if (processor.ResponseValidator.DoesNodeExists("$.data.statusCode")) {
            JSONObject obj = new JSONObject(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.body"));
            Assert.assertEquals(obj.get("statusCode").toString(), statusCode);
            Assert.assertEquals(obj.get("statusMessage").toString(), statusMessage1);
        }
    }

    @Test(dataProvider = "self_Serve_Item_Catlog_ImageDP", groups = { "sanity",
            "smoke" },  description = "POST/items/v1/uploadCatlogImages/: self_Serve_Menu_Delete_Item_DP")
    public void self_Serve_Item_Catlog_Image(String accessToken, String file, String restaurantId, String statusMessage) {

        Processor processor = helper.selfServeMenuUploadItemCatalogueImage(accessToken, file, restaurantId);

        Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
        Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);

    }

    @Test(dataProvider = "self_Serve_Item_Catlog_ImageRegression", groups = { "regression"},
            description = "POST/items/v1/uploadCatlogImages/: self_Serve_Menu_Delete_Item_DP")
    public void self_Serve_Item_Catlog_Image_Regression(String accessToken, String file, String restaurantId, String statusMessage) {

        Processor processor = helper.selfServeMenuUploadItemCatalogueImage(accessToken, file, restaurantId);

        Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
        Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);

    }

    @Test(dataProvider = "selfServeCategoryOOSInStock", groups = { "sanity",
            "smoke" },  description = "POST v1/category/outOfStock/  v1/category/inStock/: Mark Category OOS and InStock")
    public void selfServeCategoryOOSInStock(String accessToken, String restaurantId, String categoryId,
                                            List<Integer> subCatList, List<Integer> itemList, List<Integer> variantList,
                                            String oosStatusMessage, String inStockStatusMessage) throws Exception {

            Processor processor = helper.markCategoryOOS(accessToken, restaurantId, categoryId);
            Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
            Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), oosStatusMessage);

            /*Calling fetch items to verify if all categories, subcategories, items and variants are marked OOS*/
            Processor p = helper.fetchItems(accessToken, restaurantId);
            String catStatus = JsonPath.read(p.ResponseValidator.GetBodyAsText(), "$.data.categories[?(@.id == "+categoryId+")].inStock").toString();
            Assert.assertEquals(catStatus, "[0]");

            if(subCatList != null) {
                for (int i = 0; i < subCatList.size(); i++) {
                    System.out.println("SubCatIDDDDD"+ subCatList.get(i));
                    String subCatStatus = JsonPath.read(p.ResponseValidator.GetBodyAsText(), "$.data.categories..subCategories[?(@.id == " + subCatList.get(i) + ")].inStock").toString();
                    Assert.assertEquals(subCatStatus, "[0]");
                    System.out.println("PASSSSS");
                }
            }

            for(int i=0; i<itemList.size(); i++) {
                System.out.println("itemIDDD"+ itemList.get(i));
                String itemStatus = JsonPath.read(p.ResponseValidator.GetBodyAsText(), "$.data.categories..subCategories..menu[?(@.id == "+itemList.get(i)+")].inStock").toString();
                Assert.assertEquals(itemStatus, "[0]");
                System.out.println("PASSSSS");
            }

            if(variantList != null) {
                for (int i = 0; i < variantList.size(); i++) {
                    System.out.println("variantIDDD"+ variantList.get(i));
                    String variantStatus = JsonPath.read(p.ResponseValidator.GetBodyAsText(), "$.data.categories..subCategories..menu..variants_new.variant_groups..variations[?(@.id == " + variantList.get(i) + ")].inStock").toString();
                    Assert.assertEquals(variantStatus, "[0]");
                    System.out.println("PASSSSS");
                }
            }

            /* Now verify In Stock API for the same category we marked OOS above*/

            System.out.println("Now Verifying In Stock API");
            Thread.sleep(10000);
            Processor resp = helper.markCategoryInStock(accessToken, restaurantId, categoryId);
            Assert.assertTrue(helper.aggregateResponseAndStatusCodes(resp));
            Assert.assertEquals(resp.ResponseValidator.GetNodeValue("statusMessage"), inStockStatusMessage);

            Processor p1 = helper.fetchItems(accessToken, restaurantId);
            catStatus = JsonPath.read(p1.ResponseValidator.GetBodyAsText(), "$.data.categories[?(@.id == "+categoryId+")].inStock").toString();
            Assert.assertEquals(catStatus, "[1]");

            if(subCatList != null) {
                for (int i = 0; i < subCatList.size(); i++) {
                    System.out.println("SubCatIDDDDD"+ subCatList.get(i));
                    String subCatStatus = JsonPath.read(p1.ResponseValidator.GetBodyAsText(), "$.data.categories..subCategories[?(@.id == " + subCatList.get(i) + ")].inStock").toString();
                    Assert.assertEquals(subCatStatus, "[1]");
                    System.out.println("PASSSSS");
                }
            }

            for(int i=0; i<itemList.size(); i++) {
                System.out.println("itemIDDD"+ itemList.get(i));
                String itemStatus = JsonPath.read(p1.ResponseValidator.GetBodyAsText(), "$.data.categories..subCategories..menu[?(@.id == "+itemList.get(i)+")].inStock").toString();
                Assert.assertEquals(itemStatus, "[1]");
                System.out.println("PASSSSS");
            }

            if(variantList != null) {
                for (int i = 0; i < variantList.size(); i++) {
                    System.out.println("variantIDDD"+ variantList.get(i));
                    String variantStatus = JsonPath.read(p1.ResponseValidator.GetBodyAsText(), "$.data.categories..subCategories..menu..variants_new.variant_groups..variations[?(@.id == " + variantList.get(i) + ")].inStock").toString();
                    Assert.assertEquals(variantStatus, "[1]");
                    System.out.println("PASSSSS");
                }
            }
        }

    @Test(dataProvider = "selfServeCategoryOOS", groups = { "regression"},
            description = "POST v1/category/outOfStock/: Category InStock Regression")
    public void selfServeCategoryOOS(String accessToken, String restaurantId, String categoryId,
                                     String fromTime, String toTime, String statusMessage) {

        Processor processor = helper.markCategoryOOS(accessToken, restaurantId, categoryId, fromTime, toTime);
        Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
        Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
        helper.markCategoryInStock(accessToken, categoryId, restaurantId);

    }


    @Test(dataProvider = "selfServeCategoryInStockRegression", groups = { "regression"},
            description = "POST v1/category/inStock/: Category InStock Regression")
    public void selfServeCategoryInStockRegression(String accessToken, String restaurantId, String categoryId, String statusMessage) {

        Processor processor = helper.markCategoryInStock(accessToken, restaurantId, categoryId);
        Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
        Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);

    }

}
