
package com.swiggy.api.erp.cms.pojo.Inventory;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.swiggy.api.erp.cms.constants.CatalogV2Constants;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "key",
    "inventory_construct",
    "meta"
})
public class Inventory {

    @JsonProperty("key")
    private String key;
    @JsonProperty("inventory_construct")
    private Inventory_construct inventory_construct;
    @JsonProperty("meta")
    private Meta meta;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("key")
    public String getKey() {
        return key;
    }

    @JsonProperty("key")
    public void setKey(String key) {
        this.key = key;
    }

    public Inventory withKey(String key) {
        this.key = key;
        return this;
    }

    @JsonProperty("inventory_construct")
    public Inventory_construct getInventory_construct() {
        return inventory_construct;
    }

    @JsonProperty("inventory_construct")
    public void setInventory_construct(Inventory_construct inventory_construct) {
        this.inventory_construct = inventory_construct;
    }

    public Inventory withInventory_construct(Inventory_construct inventory_construct) {
        this.inventory_construct = inventory_construct;
        return this;
    }

    @JsonProperty("meta")
    public Meta getMeta() {
        return meta;
    }

    @JsonProperty("meta")
    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public Inventory withMeta(Meta meta) {
        this.meta = meta;
        return this;
    }

    public Inventory setValue(String key,long quantity){
        Attributes at=new Attributes();
        Inventories inv=new Inventories().setValue(quantity);
        Meta m=new Meta().setValue();
        Inventory_construct invc=new Inventory_construct().setValue(inv,at);
        this.withKey(key).withMeta(m).withInventory_construct(invc);
        return this;
    }

}
