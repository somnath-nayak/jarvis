package com.swiggy.api.erp.delivery.tests;

import framework.gameofthrones.Daenerys.Initializer;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.delivery.dp.DeliveryControllerDataProvider;
import com.swiggy.api.erp.delivery.helper.DeliveryControllerHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryHelperMethods;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;

public class DeliveryControllerPartnerRegressionTest extends
		DeliveryControllerDataProvider {

	//Initialize gameofthrones = new Initialize();
	Initialize gameofthrones =Initializer.getInitializer();
	DeliveryControllerHelper helper = new DeliveryControllerHelper();
	DeliveryHelperMethods delmeth=new DeliveryHelperMethods();

	@Test(groups = { "regression" }, dataProvider = "zonepartnerupdate", enabled = true)
	public void partnermappingupdatezone(String zone_id, String partner_id,
			String partner_zone_map_id, int status_code)
			throws InterruptedException {

		Processor processor = helper.partnermappingupdatezone(zone_id,
				partner_id, partner_zone_map_id);
		String resp = processor.ResponseValidator.GetBodyAsText();
		// Assert.assertEquals(200,processor.ResponseValidator.GetResponseCode());
		int statusCode = JsonPath.read(resp, "$.statusCode");
		Assert.assertEquals(status_code, statusCode);
		if (statusCode == 200) {
			String statusmessage = JsonPath.read(resp, "$.statusMessage");
			int statuscode = JsonPath.read(resp, "$.statusCode");
			int data1 = JsonPath.read(resp, "$.data");
			// String requestid =
			// processor.ResponseValidator.GetNodeValueAsStringFromJsonArray(".$requestId");
			Assert.assertEquals("Success", statusmessage);
			Assert.assertEquals(200, statuscode);
			Assert.assertNotNull(data1);
		}

	}

	@Test(groups = { "regression" }, dataProvider = "citypartnerupdate", enabled = true)
	public void partnermappingupdatecity(String city_id, String partner_id,
			String partner_city_map_id, String max_active_orders,
			String city_helpline, int status_code) throws InterruptedException {

		Processor processor = helper.partnermappingupdatecity(city_id,
				partner_id, partner_city_map_id, max_active_orders,
				city_helpline);
		String resp = processor.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(resp, "$.statusCode");
		Assert.assertEquals(status_code, statusCode);

	}

	@Test(groups = { "regression" }, dataProvider = "AddPartnerData")
	public void AddPartnerTest(String isActive, String name) {
		System.out
				.println("******************************AddPartnerTest is started******************************");
		GameOfThronesService service = new GameOfThronesService(
				"deliverycontroller", "AddPartner", gameofthrones);

		Processor processor = new Processor(service,
				delmeth.doubleheader(), new String[] { isActive, name });
		String response = processor.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(response, "$.statusCode");
		Assert.assertEquals(200, statusCode);
		System.out
				.println("******************************AddPartnerTest is stopped******************************");
	}

	@Test(groups = { "regression" }, dataProvider = "ReAssignData")
	public void ReAssignTest(String orderID, String cityID,
			String regressionStatusCode) throws Exception {
		System.out
				.println("******************************OrderPlace is started******************************");
		createOrder();
		Thread.sleep(1200);
		System.out
				.println("******************************OrderPlace is stopped******************************");

		System.out
				.println("******************************ReAssignTest is started******************************");
		GameOfThronesService service = new GameOfThronesService(
				"deliverycontroller", "ReAssign", gameofthrones);

		Processor processor = new Processor(service,
				delmeth.doubleheader(), new String[] { orderID, cityID,
						regressionStatusCode });
		String response = processor.ResponseValidator.GetBodyAsText();
		System.out.println(response);
		System.out
				.println("******************************ReAssignTest is stopped******************************");
	}
	
	 @Test(dataProvider = "zonepartneradd",enabled=true,priority=3)
	    public void partneraddzone(String zone_id, String partner_id, String partner_zone_map_id,int status_code) throws InterruptedException {
	        
	    	
	    	Processor processor=helper.partneraddzone(zone_id,partner_id,partner_zone_map_id);
	        String resp=processor.ResponseValidator.GetBodyAsText();
	       int statusCode=JsonPath.read(resp,"$.statusCode");
	        Assert.assertEquals(status_code,statusCode);
	    	if (statusCode == 200) {
				String statusmessage = JsonPath.read(resp, "$.statusMessage");
				int statuscode = JsonPath.read(resp, "$.statusCode");
				int data1 = JsonPath.read(resp, "$.data");
				// String requestid =
				// processor.ResponseValidator.GetNodeValueAsStringFromJsonArray(".$requestId");
				Assert.assertEquals("Success", statusmessage);
				Assert.assertEquals(200, statuscode);
				Assert.assertNotNull(data1);
	    	}
	                
	       }
	    @Test(dataProvider = "citypartneradd",enabled=true,priority=3)
	    public void partneraddcity(String city_id, String partner_id, String partner_city_map_id,String max_active_orders,String city_helpline,int status_code) throws InterruptedException {
	        
	    	
	    	Processor processor=helper.partneraddcity(city_id,partner_id,partner_city_map_id,max_active_orders,city_helpline);
	        String resp=processor.ResponseValidator.GetBodyAsText();
	       int statusCode=JsonPath.read(resp,"$.statusCode");
	        Assert.assertEquals(status_code,statusCode);
	        if (statusCode == 200) {
				String statusmessage = JsonPath.read(resp, "$.statusMessage");
				int statuscode = JsonPath.read(resp, "$.statusCode");
				int data1 = JsonPath.read(resp, "$.data");
				// String requestid =
				// processor.ResponseValidator.GetNodeValueAsStringFromJsonArray(".$requestId");
				Assert.assertEquals("Success", statusmessage);
				Assert.assertEquals(200, statuscode);
				Assert.assertNotNull(data1);
	        }    
	       }
	    
	    @Test(dataProvider = "zonepartnerdelete",enabled=true,priority=3, dependsOnMethods="partneraddzone")
	    public void partnerdeletezone(String partner_zone_map_id,int status_code) throws InterruptedException {
	        
	    	
	    	Processor processor=helper.partnerdeletezone(partner_zone_map_id);
	        String resp=processor.ResponseValidator.GetBodyAsText();
	        int statusCode=JsonPath.read(resp,"$.statusCode");
	        Assert.assertEquals(status_code,statusCode);
	        if (statusCode == 200) {
				String statusmessage = JsonPath.read(resp, "$.statusMessage");
				int statuscode = JsonPath.read(resp, "$.statusCode");
				int data1 = JsonPath.read(resp, "$.data");
				// String requestid =
				// processor.ResponseValidator.GetNodeValueAsStringFromJsonArray(".$requestId");
				Assert.assertEquals("Success", statusmessage);
				Assert.assertEquals(200, statuscode);
				Assert.assertNotNull(data1);
	        }      
	       }
	    @Test(dataProvider = "citypartnerdelete",enabled=true,priority=3, dependsOnMethods="partneraddcity")
	    public void partnerdeletecity(String partner_city_map_id,int status_code) throws InterruptedException {
	        
	    	
	    	Processor processor=helper.partnerdeletecity(partner_city_map_id);
	        String resp=processor.ResponseValidator.GetBodyAsText();
	       int statusCode=JsonPath.read(resp,"$.statusCode");
	        Assert.assertEquals(status_code,statusCode);
	        if (statusCode == 200) {
				String statusmessage = JsonPath.read(resp, "$.statusMessage");
				int statuscode = JsonPath.read(resp, "$.statusCode");
				int data1 = JsonPath.read(resp, "$.data");
				// String requestid =
				// processor.ResponseValidator.GetNodeValueAsStringFromJsonArray(".$requestId");
				Assert.assertEquals("Success", statusmessage);
				Assert.assertEquals(200, statuscode);
				Assert.assertNotNull(data1);    
	       }
	    }


}
