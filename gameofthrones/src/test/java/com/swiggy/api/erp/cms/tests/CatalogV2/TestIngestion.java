package com.swiggy.api.erp.cms.tests.CatalogV2;

import com.jayway.jsonpath.JsonPath;
import com.mongodb.DB;
import com.swiggy.api.erp.cms.constants.CatalogV2Constants;
import com.swiggy.api.erp.cms.helper.CatalogV2Helper;
import com.swiggy.api.erp.cms.helper.DBHelper;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

public class TestIngestion {
    CatalogV2Helper cms=new CatalogV2Helper();

    @Test(description = "This test will Ingest the data")
    public void ingestData(){
        String response=cms.ingestFile(CatalogV2Constants.s3url).ResponseValidator.GetBodyAsText();
        String status= JsonPath.read(response,"$.response_status").toString().replace("[","").replace("]","");
        Assert.assertEquals(status,"SUCCESS");
    }

    @AfterTest
    public void deleteData(){
       DBHelper.connectDb();
       DBHelper.deleteCollectionDataUsingFeilds("spins","category_id","5c7fafe9-9057-42b9-a406-15a8151b7f9c");
       DBHelper.deleteCollectionDataUsingFeilds("sku_spin_category_lookup","category_id","5c7fafe9-9057-42b9-a406-15a8151b7f9c");
       DBHelper.deleteCollectionData("sku_testintergration");


    }

}
