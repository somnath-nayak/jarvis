package com.swiggy.api.erp.cms.tests.PreOrder;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.dp.PreOrderDP;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import org.testng.Assert;
import org.testng.annotations.Test;


public class EnablePreOrderforRestaurant extends PreOrderDP {
    CMSHelper cmsHelper= new CMSHelper();
    @Test(dataProvider = "preorderenable",description = "Enable preorder in dashboard")
    public void preOrderEnable(String restId, String attribute, String value){
        String response=cmsHelper.preOrderEnable(restId,attribute,value).ResponseValidator.GetBodyAsText();
        int status=JsonPath.read(response,"$.status");
        Assert.assertEquals(status,1);
    }

}
