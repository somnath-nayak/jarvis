package com.swiggy.api.erp.cms.pojo.FullMenu;

import com.swiggy.api.erp.cms.constants.MenuConstants;

/**
 * Created by kiran.j on 2/2/18.
 */
public class Variant_groups
{
    private String id;

    private int order;

    private String name;

    private Variants[] variants;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public int getOrder ()
    {
        return order;
    }

    public void setOrder (int order)
    {
        this.order = order;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public Variants[] getVariants ()
    {
        return variants;
    }

    public void setVariants (Variants[] variants)
    {
        this.variants = variants;
    }

    public Variant_groups build() {
        setDefaultValues();
        return this;
    }

    public void setDefaultValues() {
        Variants variants = new Variants();
        variants.build();
        if(this.getId() == null)
            this.setId(MenuConstants.variant_groups_id);
        if(this.getName() == null)
            this.setName(MenuConstants.variant_groups_name);
        this.setOrder(MenuConstants.variant_groups_order);
        this.setVariants(new Variants[]{variants});
    }

    @Override
    public String toString()
    {
        return "{id = "+id+", order = "+order+", name = "+name+", variants = "+variants+"}";
    }
}
