package com.swiggy.api.erp.ff.dp;

import org.testng.annotations.DataProvider;

import com.swiggy.api.erp.ff.helper.LOSHelper;
import com.swiggy.api.erp.ff.helper.OMSHelper;

public class RCCDP {
	
	LOSHelper los = new LOSHelper();
	OMSHelper oms = new OMSHelper();
	
	@DataProvider(name = "orderAtRiskPlacedOrder")
	public Object[][] orderAtRiskPlacedOrder() throws Exception {
		String order_id = los.getAnOrder("manual");
		oms.verifyOrder(order_id, "111");
		oms.changeOrderStatusToPlaced(order_id);
		return new Object[][] {
			
			{"{\"restaurant_id\":\"212\",\"is_order_at_risk\":true,\"order_id\":"+order_id+",\"reason\":[\"test\"]}",order_id}
		};
	}
	
	@DataProvider(name = "orderAtRiskNotPlaced")
	public Object[][] orderAtRiskNotPlaced() throws Exception {
		String order_id = los.getAnOrder("manual");
		oms.verifyOrder(order_id, "111");
		Thread.sleep(300000);
		return new Object[][] {
			
			{"{\"restaurant_id\":\"212\",\"is_order_at_risk\":true,\"order_id\":"+order_id+",\"reason\":[\"test\"]}",order_id,"1","40"}
		};
	}

	@DataProvider(name = "rccCancelledOrder")
	public Object[][] rccCancelledOrder() throws Exception {
		String order_id_1 = los.getAnOrder("manual");
		Thread.sleep(100);
		String order_id_2 = los.getAnOrder("manual");
		Thread.sleep(100);
		String order_id_3 = los.getAnOrder("manual");
		oms.verifyOrder(order_id_1, "111");
		
		return new Object[][] {
			
			{order_id_1,order_id_2,order_id_3}
		};
	}
	
}
