package com.swiggy.api.erp.cms.pojo.Items;

/**
 * Created by kiran.j on 2/9/18.
 */
public class Gst_details
{
    private boolean inclusive;

    private double igst;

    private double cgst;

    private double sgst;

    public boolean getInclusive ()
    {
        return inclusive;
    }

    public void setInclusive (boolean inclusive)
    {
        this.inclusive = inclusive;
    }

    public double getIgst ()
    {
        return igst;
    }

    public void setIgst (double igst)
    {
        this.igst = igst;
    }

    public double getCgst ()
    {
        return cgst;
    }

    public void setCgst (double cgst)
    {
        this.cgst = cgst;
    }

    public double getSgst ()
    {
        return sgst;
    }

    public void setSgst (double sgst)
    {
        this.sgst = sgst;
    }

    @Override
    public String toString()
    {
        return "{ inclusive = "+inclusive+", igst = "+igst+", cgst = "+cgst+", sgst = "+sgst+"}";
    }
}