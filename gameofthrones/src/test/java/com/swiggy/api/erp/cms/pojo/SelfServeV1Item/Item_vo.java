package com.swiggy.api.erp.cms.pojo.SelfServeV1Item;

import com.swiggy.api.erp.cms.catalogV2.pojos.CatalogV2PricingPojo;
import com.swiggy.api.erp.cms.catalogV2.pojos.SkuAttributes;
import com.swiggy.api.erp.cms.constants.SelfServeConstants;
import com.swiggy.api.erp.cms.helper.SelfServeHelper;

public class Item_vo {
	 private Item item;

	    private String sub_category_third_party_id;

	    private String temp_main_category_id;

	    private String main_category_order;

	    private Item_slot[] item_slot;

	    private String main_category_third_party_id;

	    private String[] delete_addon_group_by_ids;

	    private String main_category_id;

	    private Variant_groups_vo[] variant_groups_vo;

	    private Addon_groups_vo[] addon_groups_vo;

	    private String[] delete_variant_group_by_ids;

	    private String main_category_name;

	    private String sub_category_id;

	    private String sub_category_name;

	    private String temp_sub_category_id;

	    private String sub_category_order;

	    private String status;

	    public Item getItem ()
	    {
	        return item;
	    }

	    public void setItem (Item item)
	    {
	        this.item = item;
	    }

	    public String getSub_category_third_party_id ()
	    {
	        return sub_category_third_party_id;
	    }

	    public void setSub_category_third_party_id (String sub_category_third_party_id)
	    {
	        this.sub_category_third_party_id = sub_category_third_party_id;
	    }

	    public String getTemp_main_category_id ()
	    {
	        return temp_main_category_id;
	    }

	    public void setTemp_main_category_id (String temp_main_category_id)
	    {
	        this.temp_main_category_id = temp_main_category_id;
	    }

	    public String getMain_category_order ()
	    {
	        return main_category_order;
	    }

	    public void setMain_category_order (String main_category_order)
	    {
	        this.main_category_order = main_category_order;
	    }

	    public Item_slot[] getItem_slot ()
	    {
	        return item_slot;
	    }

	    public void setItem_slot (Item_slot[] item_slot)
	    {
	        this.item_slot = item_slot;
	    }

	    public String getMain_category_third_party_id ()
	    {
	        return main_category_third_party_id;
	    }

	    public void setMain_category_third_party_id (String main_category_third_party_id)
	    {
	        this.main_category_third_party_id = main_category_third_party_id;
	    }

	    public String[] getDelete_addon_group_by_ids ()
	    {
	        return delete_addon_group_by_ids;
	    }

	    public void setDelete_addon_group_by_ids (String[] delete_addon_group_by_ids)
	    {
	        this.delete_addon_group_by_ids = delete_addon_group_by_ids;
	    }

	    public String getMain_category_id ()
	    {
	        return main_category_id;
	    }

	    public void setMain_category_id (String main_category_id)
	    {
	        this.main_category_id = main_category_id;
	    }

	    public Variant_groups_vo[] getVariant_groups_vo ()
	    {
	        return variant_groups_vo;
	    }

	    public void setVariant_groups_vo (Variant_groups_vo[] variant_groups_vo)
	    {
	        this.variant_groups_vo = variant_groups_vo;
	    }

	    public Addon_groups_vo[] getAddon_groups_vo ()
	    {
	        return addon_groups_vo;
	    }

	    public void setAddon_groups_vo (Addon_groups_vo[] addon_groups_vo)
	    {
	        this.addon_groups_vo = addon_groups_vo;
	    }

	    public String[] getDelete_variant_group_by_ids ()
	    {
	        return delete_variant_group_by_ids;
	    }

	    public void setDelete_variant_group_by_ids (String[] delete_variant_group_by_ids)
	    {
	        this.delete_variant_group_by_ids = delete_variant_group_by_ids;
	    }

	    public String getMain_category_name ()
	    {
	        return main_category_name;
	    }

	    public void setMain_category_name (String main_category_name)
	    {
	        this.main_category_name = main_category_name;
	    }

	    public String getSub_category_id ()
	    {
	        return sub_category_id;
	    }

	    public void setSub_category_id (String sub_category_id)
	    {
	        this.sub_category_id = sub_category_id;
	    }

	    public String getSub_category_name ()
	    {
	        return sub_category_name;
	    }

	    public void setSub_category_name (String sub_category_name)
	    {
	        this.sub_category_name = sub_category_name;
	    }

	    public String getTemp_sub_category_id ()
	    {
	        return temp_sub_category_id;
	    }

	    public void setTemp_sub_category_id (String temp_sub_category_id)
	    {
	        this.temp_sub_category_id = temp_sub_category_id;
	    }

	    public String getSub_category_order ()
	    {
	        return sub_category_order;
	    }

	    public void setSub_category_order (String sub_category_order)
	    {
	        this.sub_category_order = sub_category_order;
	    }

	    public String getStatus ()
	    {
	        return status;
	    }

	    public void setStatus (String status)
	    {
	        this.status = status;
	    }

	    @Override
	    public String toString()
	    {
	        return "ClassPojo [item = "+item+", sub_category_third_party_id = "+sub_category_third_party_id+", temp_main_category_id = "+temp_main_category_id+", main_category_order = "+main_category_order+", item_slot = "+item_slot+", main_category_third_party_id = "+main_category_third_party_id+", delete_addon_group_by_ids = "+delete_addon_group_by_ids+", main_category_id = "+main_category_id+", variant_groups_vo = "+variant_groups_vo+", addon_groups_vo = "+addon_groups_vo+", delete_variant_group_by_ids = "+delete_variant_group_by_ids+", main_category_name = "+main_category_name+", sub_category_id = "+sub_category_id+", sub_category_name = "+sub_category_name+", temp_sub_category_id = "+temp_sub_category_id+", sub_category_order = "+sub_category_order+", status = "+status+"]";
	    }

		public Item_vo build() {
			setDefaultValues();
			return this;
		}

		private void setDefaultValues() {
            Item item=new Item();
            item.build();
           if(this.getMain_category_id()==null)
    			this.setMain_category_id(SelfServeConstants.catId);
           if(this.getSub_category_id()==null)
   			this.setSub_category_id(SelfServeConstants.subCatId);
   			}
		public Item_vo build(String itemId) {
			setDefaultValuesupdateItem(itemId);
			return this;
		}

		private void setDefaultValuesupdateItem(String itemId) {
            Item item=new Item();
            String[] catSubcatIdforItem=SelfServeHelper.getCatIdforItem(itemId);
			this.setMain_category_id(catSubcatIdforItem[0]);
			this.setSub_category_id(catSubcatIdforItem[1]);
            item.build(itemId);

   			}
	}
				
				
