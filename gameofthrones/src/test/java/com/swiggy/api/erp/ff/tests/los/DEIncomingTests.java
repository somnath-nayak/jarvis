package com.swiggy.api.erp.ff.tests.los;

import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.rabbitmq.client.AMQP;
import com.swiggy.api.erp.ff.constants.LosConstants;
import com.swiggy.api.erp.ff.dp.losDp.DEIncomingUpdateData;
import com.swiggy.api.erp.ff.helper.LOSHelper;

import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Tyrion.RabbitMQHelper;

public class DEIncomingTests extends DEIncomingUpdateData {
	RabbitMQHelper rmqhelper=new RabbitMQHelper();
	LOSHelper losHelper = new LOSHelper();
	Logger log = Logger.getLogger(DEIncomingTests.class);
	
	@Test(groups = {"sanity","regression"}, dataProvider = "updateIncomingwithValidData")
	public void testDEIncomingConsumerWithValidData(String order_type, String incoming) throws IOException, SQLException, InterruptedException{
		log.info("***************************************** testDEIncomingConsumerWithValidData started *****************************************");
		
		Date date = new Date();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		DateFormat order_idFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		String order_id = order_idFormat.format(date);
		String order_time = dateFormat.format(date);
		log.info("Order time = "+order_time);
		String epochTime = String.valueOf(Instant.now().getEpochSecond());
		log.info("Epoch time = " +epochTime);
		losHelper.deIncoming(order_type, order_id, incoming, order_time, epochTime);
		List<Map<String, Object>> orders = SystemConfigProvider.getTemplate(LosConstants.hostName).queryForList("select incoming_entered_by_de from oms_order where order_id = "+ order_id  +";");
		Thread.sleep(3000);
		Assert.assertEquals(String.valueOf(orders.get(0).get("incoming_entered_by_de")), incoming);
		log.info(orders.get(0).get("incoming_entered_by_de"));
		log.info("######################################### testDEIncomingConsumerWithValidData completed #########################################");
		
	}
	
	@Test(groups = {"sanity","regression"}, dataProvider = "deIncomingwithInValidData")
	public void testDEIncomingDeadLetterQueue(String invalidJson) throws IOException, TimeoutException, InterruptedException {
		rmqhelper.purgeQueue(LosConstants.hostName,LosConstants.deIncomingDeadLetter);
		rmqhelper.pushMessageToExchange(LosConstants.hostName, LosConstants.deIncomingExchange, new AMQP.BasicProperties().builder().contentType("application/json"), invalidJson);
		Thread.sleep(6000);
		String message = rmqhelper.getMessage(LosConstants.hostName, LosConstants.deIncomingDeadLetter);
		Assert.assertEquals(message, invalidJson);
		//log.info(message);
	}

}
