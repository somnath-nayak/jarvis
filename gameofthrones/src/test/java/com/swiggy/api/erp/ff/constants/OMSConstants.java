package com.swiggy.api.erp.ff.constants;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Daenerys.Initializer;

import javax.ws.rs.core.Cookie;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public interface OMSConstants {
	
	//For placing order from backend
	String mobile = "7899772315";
	String password = "swiggy";
	String itemId_withpartner = "550554";
	String itemId_manual = "550554";
	String quantity = "2";
	String restId_withpartner = "3252";
	String restId_manual = "3252";

    String SERVICE = "oms";
    String USERNAME = "shashank";
    String PASSSWORD = "Shash@ank1";
    String E2EUSERPROD= "jitender.kumar@swiggy.in";
   	String E2EPASSWORDPROD= "Abhijit@123";
	String E2EUSER= "murthy@swiggy.in";
 	String E2EPASSWORD= "Swiggy@1234";

    String ASSIGNMENT_LOGIN_QUEUE = "swiggy.oe_login_queue";
    String ASSIGNMENT_LOGOUT_QUEUE = "swiggy.oe_logout_queue";
    String MANUAL_ORDER_QUEUE = "swiggy.ff_manual_order";
    String ASSIGNMENT_ORDER_STATUS = "swiggy.order_status_update_queue";
    String ASSIGNMENT_DB = "db_assignment";
	String omt_DB = "omt";
    String ASSIGNMENTQUEUE = "testassignment";
    String RMQHOSTNAME = "oms";
    String REDIS = "ffredis";
    String query= "select order_id from oms_order where status_id in (select id from oms_orderstatus where delivery_status != 'delivered' or status!= 'cancelled') limit 10 ;";
    String query1= "select * from oms_order limit 10";
	String mobileNumberSearchQuery="select o.order_id from oms_order o join oms_deliveryaddress d on o.delivery_address_id = d.id join oms_orderstatus s on o.status_id = s.id where o.payment_txn_status not in ('failed', 'pending') and s.order_status not in ('delivered', 'cancelled') and d.mobile = ";
	String restIdSearchQuery = "select o.order_id from oms_order o join oms_restaurantdetails r on o.restaurant_details_id = r.id join oms_orderstatus s on o.status_id = s.id where o.payment_txn_status not in ('failed', 'pending') and s.order_status not in ('delivered', 'cancelled') and r.restaurant_id = ";
	String WATCHDOG_DB = "watchdog";
	String SWIGGYCAFEQUEUE="swiggy-cafe-watchdog-testautomation";
	String SWIGGYCAFEDEADLETTER="ff-watchdog-order-exchange-dead-letters-testautomation";
	String OMSKAFKA="omskafka";
	String ORDER_AT_RISK ="order-at-risk";
	String WATCHDOG_CANCELLED_ORDER = "ff-watchdog-cancel-order";
	String ONP_ORDERS="ff-watchdog-delayed-order-to-delivery-test";
	String OrderInsert="INSERT INTO `swiggy_orders` (`user_id`, `created_on`, `order_status`, `payment_method`, `payment_txn_status`, `order_details`, `updated_on`)\n" +
			"VALUES\n" +
			"\t(7862213, '2019-02-18 15:37:33', 'processing', 'Cash', 'success', '{\\\"restaurantId\\\":757,\\\"addressId\\\":29724841,\\\"sessionId\\\":\\\"fd5c8d21-1afd-4e37-a229-9b699e2a9a53\\\",\\\"orderType\\\":\\\"regular\\\",\\\"sharedOrder\\\":false,\\\"cartDetails\\\":{\\\"items\\\":[{\\\"itemId\\\":613982,\\\"externalItemId\\\":\\\"\\\",\\\"quantity\\\":2,\\\"isVeg\\\":1,\\\"packingCharges\\\":0.0,\\\"subTotal\\\":2.0,\\\"total\\\":2.0,\\\"name\\\":\\\"Dry Fruit Ice cream Double Scoop\\\",\\\"imageId\\\":\\\"i7rfrwuhsnkjkhwws91b\\\",\\\"category\\\":\\\"Ice Creams\\\",\\\"subCategory\\\":\\\"Double Scoops\\\",\\\"globalMainCategory\\\":\\\"DESSERTS\\\",\\\"globalSubCategory\\\":\\\"ICE CREAMS\\\",\\\"addedByUserId\\\":-1,\\\"addedByUserName\\\":\\\"\\\",\\\"groupUserItemMap\\\":{},\\\"addons\\\":[],\\\"variants\\\":[],\\\"itemCharges\\\":{\\\"vat\\\":\\\"0\\\",\\\"serviceCharge\\\":\\\"0\\\",\\\"serviceTax\\\":\\\"0\\\",\\\"packingCharge\\\":\\\"0\\\",\\\"gst\\\":\\\"0\\\"},\\\"itemTaxPercentages\\\":{\\\"vat\\\":\\\"0\\\",\\\"serviceTax\\\":\\\"0\\\",\\\"serviceCharge\\\":\\\"0\\\",\\\"swatchBharatCess\\\":null,\\\"krishiKalyanCess\\\":null,\\\"gstInclusive\\\":false,\\\"cgst\\\":null,\\\"sgst\\\":null,\\\"igst\\\":null},\\\"swiggyDiscountHit\\\":0.0,\\\"restaurantDiscountHit\\\":0.0,\\\"gstDetails\\\":{\\\"SGST\\\":0.0,\\\"CGST\\\":0.0,\\\"IGST\\\":0.0},\\\"commissionExpression\\\":null,\\\"slotId\\\":0,\\\"rewardType\\\":null,\\\"singleVariant\\\":false,\\\"mealQuantity\\\":1,\\\"catalogAttributes\\\":null,\\\"inStock\\\":1}],\\\"mealItems\\\":[],\\\"subscriptionItems\\\":[],\\\"charges\\\":{\\\"vat\\\":\\\"0\\\",\\\"serviceCharge\\\":\\\"0\\\",\\\"serviceTax\\\":\\\"0\\\",\\\"deliveryCharge\\\":\\\"0\\\",\\\"packingCharge\\\":\\\"0\\\",\\\"convenienceCharge\\\":\\\"0\\\",\\\"cancellationFeeCollected\\\":\\\"0\\\",\\\"gst\\\":\\\"0\\\"},\\\"isCouponApplied\\\":false,\\\"subscriptionTax\\\":0.0,\\\"subscriptionTotalWithoutTax\\\":0.0,\\\"subscriptionTotal\\\":0.0,\\\"cartTotal\\\":2,\\\"orderTotal\\\":2,\\\"nonRoundedOrderTotal\\\":2.0,\\\"swiggyMoneyUsed\\\":0.0,\\\"totalTax\\\":0.0,\\\"freeShipping\\\":0,\\\"cartDiscount\\\":0.0,\\\"deliveryCharge\\\":0.0,\\\"restaurantPackingCharges\\\":0.0,\\\"totalPackingCharges\\\":0.0,\\\"swiggyDiscountHit\\\":0.0,\\\"restaurantDiscountHit\\\":0.0,\\\"tradeDiscountRewardList\\\":[],\\\"tradeDiscountBreakup\\\":[],\\\"freeDeliveryDiscountHit\\\":0.0,\\\"restaurantFreebieDiscountHit\\\":0.0,\\\"originalOrderTotal\\\":2,\\\"cashbackAmount\\\":0.0,\\\"swiggyMoneyReverted\\\":false,\\\"taxPercentages\\\":{\\\"vat\\\":\\\"[CART_SUBTOTAL]*0\\\",\\\"serviceTax\\\":\\\"[CART_SUBTOTAL]*0\\\",\\\"serviceCharge\\\":\\\"[CART_SUBTOTAL]*0\\\",\\\"swatchBharatCess\\\":null,\\\"krishiKalyanCess\\\":null,\\\"itemGSTInclusive\\\":false,\\\"packagingCGST\\\":null,\\\"packagingIGST\\\":null,\\\"packagingSGST\\\":null,\\\"packagingGSTInclusive\\\":false,\\\"serviceChargeCGST\\\":null,\\\"serviceChargeIGST\\\":null,\\\"serviceChargeSGST\\\":null,\\\"serviceChargeGSTInclusive\\\":false,\\\"cgst\\\":null,\\\"sgst\\\":null,\\\"igst\\\":null},\\\"commissionOnFullBill\\\":true,\\\"cancellationFeeCollected\\\":0.0,\\\"originalSwiggyMoneyUsed\\\":0.0,\\\"pricingModelKey\\\":\\\"29c040a9-0bba-42a0-a821-918a3e01579e\\\",\\\"cartId\\\":188396,\\\"thresholdFee\\\":0.0,\\\"distanceFee\\\":0.0,\\\"timeFee\\\":0.0,\\\"specialFee\\\":0.0,\\\"cartType\\\":\\\"REGULAR\\\",\\\"cartShared\\\":false,\\\"tradeDiscountMeta\\\":[],\\\"cartCod\\\":{\\\"codEnabled\\\":true,\\\"codDisabledMessage\\\":\\\"\\\",\\\"codDisabledReason\\\":\\\"\\\",\\\"defaultCodCheck\\\":false,\\\"abvariant\\\":\\\"A\\\"},\\\"rainFee\\\":false,\\\"assured\\\":false},\\\"financeDetails\\\":{\\\"orderIncoming\\\":2.0,\\\"orderSpending\\\":0.0,\\\"orderRestaurantBill\\\":2.0,\\\"orderCommission\\\":0.10000000149011612,\\\"serviceTaxOnCommission\\\":0.0,\\\"nodalSpending\\\":1.861999997973442,\\\"gstOnCommission\\\":{\\\"CGST\\\":0.009000000491738325,\\\"SGST\\\":0.009000000491738325},\\\"gstOnCommissionExpressions\\\":{\\\"CGST\\\":\\\"0.09\\\",\\\"IGST\\\":\\\"0\\\",\\\"SGST\\\":\\\"0.09\\\"},\\\"tcsOnBill\\\":{\\\"CTCS\\\":0.009999999776482582,\\\"STCS\\\":0.009999999776482582},\\\"tcsOnBillExpressions\\\":{\\\"CTCS\\\":\\\"0.005\\\",\\\"ITCS\\\":\\\"0\\\",\\\"STCS\\\":\\\"0.005\\\"},\\\"gstOnDeliveryFee\\\":{},\\\"gstOnConvenienceFee\\\":{},\\\"gstOnCancellationFee\\\":{},\\\"gstOnSubscription\\\":{},\\\"gstOnDeliveryFeeExpressions\\\":{\\\"CGST\\\":\\\"0.09\\\",\\\"IGST\\\":\\\"0.0\\\",\\\"SGST\\\":\\\"0.09\\\"},\\\"gstOnConvenienceFeeExpressions\\\":{\\\"CGST\\\":\\\"0.09\\\",\\\"IGST\\\":\\\"0.0\\\",\\\"SGST\\\":\\\"0.09\\\"},\\\"gstOnCancellationFeeExpressions\\\":{\\\"CGST\\\":\\\"0.09\\\",\\\"IGST\\\":\\\"0.0\\\",\\\"SGST\\\":\\\"0.09\\\"},\\\"gstOnSubscriptionExpressions\\\":{\\\"CGST\\\":\\\"0.09\\\",\\\"IGST\\\":\\\"0.0\\\",\\\"SGST\\\":\\\"0.09\\\"},\\\"actualDeliveryFee\\\":0.0,\\\"actualConvenienceFee\\\":0.0,\\\"actualCancellationFee\\\":0.0,\\\"waiveOffAmount\\\":0.0,\\\"editRefundAmount\\\":0.0,\\\"actualSubscriptionTotal\\\":0.0,\\\"orderPgCharges\\\":0.0},\\\"deviceDetails\\\":{\\\"userAgent\\\":\\\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36\\\",\\\"customerIP\\\":\\\"10.0.22.83\\\",\\\"appVersionCode\\\":\\\"320\\\",\\\"lat\\\":null,\\\"lng\\\":null,\\\"defaultingType\\\":null,\\\"defaultingLat\\\":null,\\\"defaultingLng\\\":null},\\\"deliveryDetails\\\":{\\\"deliveryStatus\\\":\\\"unassigned\\\",\\\"orderPlacementStatus\\\":\\\"0\\\"},\\\"paymentDetails\\\":{\\\"paymentMethod\\\":\\\"Cash\\\",\\\"paymentTxnStatus\\\":\\\"success\\\",\\\"paymentConfirmationChannel\\\":\\\"api\\\",\\\"pgResponseTime\\\":\\\"2019-02-18 15:37:32\\\",\\\"refundInitiated\\\":false,\\\"refundRequestCount\\\":0},\\\"additionalInfo\\\":{\\\"convertToCod\\\":false,\\\"orderSla\\\":52,\\\"orderSlaMin\\\":52,\\\"orderSlaMax\\\":57,\\\"rainMode\\\":0,\\\"orderDistanceCalculationMethod\\\":\\\"HAVERSINE_LUCIFER\\\",\\\"restaurantCustomerDistance\\\":3.4,\\\"hasRating\\\":\\\"0\\\",\\\"customerCareNumberVisible\\\":true,\\\"displayNumberFlag\\\":\\\"3\\\",\\\"assuredRefund\\\":0,\\\"onTime\\\":false,\\\"firstOrder\\\":false,\\\"slaDifference\\\":0,\\\"actualOrderSla\\\":0,\\\"userLatLong\\\":{\\\"lat\\\":\\\"12.928088\\\",\\\"lng\\\":\\\"77.626793\\\"},\\\"cancellationFeeInfo\\\":{\\\"cancellationFeeCollectedTotal\\\":0.0,\\\"cancellationFeeDisabled\\\":false},\\\"assignmentDelayPred\\\":3.0,\\\"prepTimePred\\\":14.0,\\\"fmPred\\\":7.0,\\\"placementDelayPred\\\":0.0,\\\"promiseId\\\":\\\"33559651303864689364650225\\\",\\\"initiationSource\\\":0,\\\"lmtimePred\\\":24.914999},\\\"analyticsInfo\\\":{\\\"sid\\\":\\\"ef30e450-1346-48d4-8a9d-d49bb8760491\\\",\\\"deviceId\\\":\\\"1b8b4b1c-d3d1-444c-ad4e-326b76a2573a\\\",\\\"swuid\\\":\\\"1b8b4b1c-d3d1-444c-ad4e-326b76a2573a\\\"},\\\"userDetails\\\":{\\\"userName\\\":\\\"narendra\\\",\\\"phoneNumber\\\":\\\"7760999789\\\",\\\"email\\\":\\\"narendra.guntaka@swiggy.in\\\"},\\\"orderTagsList\\\":[],\\\"restaurantDetails\\\":{\\\"name\\\":\\\"Corner House Ice Creamm\\\",\\\"address\\\":\\\"67-7-61, 1st A Main, 7th Block, Koramangala\\\",\\\"locality\\\":\\\"7th Block\\\",\\\"type\\\":\\\"D\\\",\\\"latLong\\\":\\\"12.936358,77.61514599999998\\\",\\\"ordersNotifyMobile\\\":\\\"\\\",\\\"ordersNotifyEmail\\\":\\\"cornerhousekml@gmail.com\\\",\\\"payBySystemValue\\\":true,\\\"newSlug\\\":\\\"corner-house-ice-cream-7th-block-koramangala\\\",\\\"prepTimePeak\\\":16,\\\"commissionExp\\\":\\\"[bill_without_taxes]*0.05\\\",\\\"phoneNumbers\\\":\\\"9845923734\\\",\\\"imageId\\\":\\\"b6qvoop4fpdv2zjhuu6j\\\",\\\"areaCode\\\":1,\\\"cuisine\\\":\\\"Ice Cream,Desserts\\\",\\\"cityCode\\\":1,\\\"agreementType\\\":\\\"1\\\",\\\"assured\\\":true,\\\"select\\\":true,\\\"metaInfo\\\":\\\"{\\\\\\\"configuration\\\\\\\":{\\\\\\\"menu\\\\\\\":{},\\\\\\\"cart\\\\\\\":{},\\\\\\\"payment\\\\\\\":{},\\\\\\\"tracking\\\\\\\":{},\\\\\\\"order\\\\\\\":{},\\\\\\\"delivery\\\\\\\":{},\\\\\\\"analytics\\\\\\\":{},\\\\\\\"assured\\\\\\\":{\\\\\\\"distance\\\\\\\":\\\\\\\"2.7\\\\\\\"}},\\\\\\\"messages\\\\\\\":{}}\\\",\\\"partnerEnable\\\":true,\\\"codVerificationLimit\\\":1000},\\\"addressDetails\\\":{\\\"id\\\":29724841,\\\"name\\\":\\\"test automation\\\",\\\"mobile\\\":\\\"7760999789\\\",\\\"address\\\":\\\"199, Near Sai Baba Temple, 7th Block, Koramangala\\\",\\\"landmark\\\":\\\"Swiggy office Land Mark\\\",\\\"area\\\":\\\"Swiggy Test\\\",\\\"lat\\\":12.925375,\\\"lng\\\":77.586044,\\\"annotation\\\":\\\"HOME\\\",\\\"flatNo\\\":\\\"4321\\\",\\\"city\\\":\\\"Bangalore\\\"},\\\"cityDetails\\\":{\\\"name\\\":\\\"Bangalore\\\",\\\"customerCareNumber\\\":\\\"08067466792\\\"},\\\"areaDetails\\\":{\\\"slug\\\":\\\"koramangala\\\",\\\"name\\\":\\\"Koramangala\\\"},\\\"partnerDetails\\\":{\\\"partnerId\\\":\\\"757\\\",\\\"typeOfPartner\\\":3,\\\"enabled\\\":true},\\\"orderKey\\\":\\\"24D9UA\\\",\\\"itemOos\\\":false}', '2019-02-18 15:37:33');\n";

	String OrderQuery="select id from swiggy_orders order by 1 desc limit 1";

	Initialize init = Initializer.getInitializer();

    static HashMap<String, String> getOMSHeader(String csrfToken){
        HashMap<String, String> omsHeader = new HashMap<String, String>();
        omsHeader.put("Content-Type", "application/json");
        omsHeader.put("X-CSRFToken", csrfToken);
        return omsHeader;
    }

    default String getOrdersNotInTerminalStateByMobileNumber(String mobile)
	{
		return mobileNumberSearchQuery+mobile;

	}

	default String getOrdersNotInTerminalStateByRestId(String restId)
	{
		return restIdSearchQuery+restId;

	}
    static List<Cookie> getOMSCookies(String csrfToken){
        List<Cookie> cookies = new ArrayList<>();
        cookies.add(new Cookie("csrftoken", csrfToken, "/", init.EnvironmentDetails.setup.getEnvironmentData()
                .getServices().GetServiceDetails(OMSConstants.SERVICE).getBaseurl().replace("http://", "")));
        cookies.add(new Cookie("sessionid", "036cji9thl6kvmobgjhgcfnsmej0z6ut", "/", init.EnvironmentDetails.setup.getEnvironmentData()
                .getServices().GetServiceDetails(OMSConstants.SERVICE).getBaseurl().replace("http://", "")));
        return cookies;
    }

	static List<Cookie> getOMSCookiesProd(String csrfToken, String sessionId){
		List<Cookie> cookies = new ArrayList<>();
		cookies.add(new Cookie("csrftoken", csrfToken, "/", init.EnvironmentDetails.setup.getEnvironmentData()
				.getServices().GetServiceDetails(OMSConstants.SERVICE).getBaseurl().replace("http://", "")));
		cookies.add(new Cookie("sessionid", sessionId, "/", init.EnvironmentDetails.setup.getEnvironmentData()
				.getServices().GetServiceDetails(OMSConstants.SERVICE).getBaseurl().replace("http://", "")));
		return cookies;
	}
    
    default String queryToClearOrdersInQueue(String waitingFor){
		if(!waitingFor.equalsIgnoreCase("all")){
			return "delete from oms_ordersinqueue where waiting_for = '"+waitingFor+"';";
		}else {
			return "delete from oms_ordersinqueue;";
		}
	}
    
    default String queryToClearOrdersInQueue(String waitingFor, String orderId){
    	if(!waitingFor.equalsIgnoreCase("all")){
    		return "delete from oms_ordersinqueue where waiting_for = 'L1Placer' and order_id not in ("+orderId+");";
    	}else {
			return "select from oms_ordersinqueue where order_id not in ("+orderId+");";
		}
    }

	default String queryToclearoms_orderexecutivemapping_orders(){
		return "delete from oms_orderexecutivemapping_orders;";
	}

	default String queryToClearoms_orderexecutivemapping(){
		return "delete from oms_orderexecutivemapping;";
	}

	default String forceLogoutQuery(String userName){
		if(userName.equalsIgnoreCase("all")){
			return "update oms_orderexecutive set enabled = 0;";
		}
		return "update oms_orderexecutive set enabled = 0 where user_id = (select id from auth_user where username = '"+userName+"');";
	}


}
