package com.swiggy.api.erp.cms.pojo.FullMenu;

/**
 * Created by kiran.j on 2/2/18.
 */
public class FullMenuPojo
{
    public FullMenuPojo(){}

    private Entity entity;

    public Entity getEntity ()
    {
        return entity;
    }

    public void setEntity (Entity entity)
    {
        this.entity = entity;
    }

    @Override
    public String toString()
    {
        return "{entity:"+entity+"}";
    }
}



