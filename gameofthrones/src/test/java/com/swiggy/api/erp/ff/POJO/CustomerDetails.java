package com.swiggy.api.erp.ff.POJO;


import org.codehaus.jackson.annotate.JsonProperty;

public class CustomerDetails {

    @JsonProperty("customer_id")
    private Integer customerId;
    @JsonProperty("pending_cancellation_fee")
    private Integer pendingCancellationFee;
    @JsonProperty("swiggy_money_used")
    private double swiggyMoneyUsed;
    @JsonProperty("mobile_no")
    private double mobileNo;

    public double getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(double mobileNo) {
        this.mobileNo = mobileNo;
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public CustomerDetails() {
    }

    /**
     *
     * @param customerId
     * @param pendingCancellationFee
     * @param swiggyMoneyUsed
     */
    public CustomerDetails(Integer customerId,double mobileNo, Integer pendingCancellationFee, double swiggyMoneyUsed) {
        super();
        this.customerId = customerId;
        this.pendingCancellationFee = pendingCancellationFee;
        this.swiggyMoneyUsed = swiggyMoneyUsed;
        this.mobileNo=mobileNo;
    }

    @JsonProperty("customer_id")
    public Integer getCustomerId() {
        return customerId;
    }

    @JsonProperty("customer_id")
    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    @JsonProperty("pending_cancellation_fee")
    public Integer getPendingCancellationFee() {
        return pendingCancellationFee;
    }

    @JsonProperty("pending_cancellation_fee")
    public void setPendingCancellationFee(Integer pendingCancellationFee) {
        this.pendingCancellationFee = pendingCancellationFee;
    }

    @JsonProperty("swiggy_money_used")
    public double getSwiggyMoneyUsed() {
        return swiggyMoneyUsed;
    }

    @JsonProperty("swiggy_money_used")
    public void setSwiggyMoneyUsed(double swiggyMoneyUsed) {
        this.swiggyMoneyUsed = swiggyMoneyUsed;
    }

}