package com.swiggy.api.erp.delivery.constants;

import com.swiggy.api.erp.delivery.helper.SolrHelper;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public interface Serviceability_Constant 
{
	String CITY_ID = "1";
	String AREA_ID = "5";
	String ZONE_ID = "5";
	String RESTAURANT_ID = "282";
	String RESTAURANT_LAT = "12.976042";
	String RESTAURANT_LONG = "77.72689300000002";
	String LISTING_RESPONSE_PATH = "$.serviceableRestaurants..[?(@.restaurantId==" + RESTAURANT_ID + ")].listingServiceabilityResponse.";
	String LISTING_ITEM_COUNT = "0";
	String TOTAL_ITEM_COUNT = "1";
	String RESTAURANT_NAME = getDetails("name", RESTAURANT_ID);
	//String RESTAURANT_LAT_LONG = getRestaurantLatLong();
	String START_TIME = "";

	SolrHelper solrHelper = new SolrHelper();

	static String getRestLatLongFromSolr(String url, String core, String query) {
		SolrDocumentList solrDocuments = getSolrOutput( url,  core,  query);
		Iterator<SolrDocument> iterator=solrDocuments.iterator();
		while(iterator.hasNext())
		{
			System.out.println(iterator.next().toString());
		}
		Pattern pattern= Pattern.compile("\\bplace\\W([0-9]*[.])?[0-9]+[,]([0-9]*[.])?[0-9]+");
		Matcher matcher= pattern.matcher(solrDocuments.toString());
		matcher.find();
		String latlon=matcher.group();
		System.out.println(latlon);
		return latlon.split("=")[1];
	}


	static SolrDocumentList getSolrOutput(String url, String core, String query)
	{
		//SolrClient client = new HttpSolrClient(url);
		SolrClient client = new HttpSolrClient.Builder(url).build();
		SolrDocumentList solrDocuments=null;
		try{
			SolrQuery query1=new SolrQuery(query);
			query1.setParam("wt", "json");
			solrDocuments= (client.query(core, query1).getResults());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return solrDocuments;
	}


	/*static String getRestaurantLatLong()
	{
		String urlString = solrHelper.getSolrUrl();//"http://solr-u-cuat-03.swiggyops.de:8983/solr/";
		String latlong =getRestLatLongFromSolr(urlString,"core_listing","id:" + RESTAURANT_ID);
		System.out.println("******** RESTAURANT LAT LONG *********** " + latlong);
		return latlong;
	}*/


// Cerebro Enhancements Params
	String city_id = "1";
	String area_id = "6";
	String zone_id = "6";
	String restaurant_id = "361";

	static String getDetails(String attributeName, String restaurantId)
	{
		String name = SystemConfigProvider.getTemplate("deliverydb").queryForMap("select " + attributeName + " from restaurant where id='" + restaurantId + "'").get(attributeName).toString();
		return name;
	}

	static List<String> getStartTime()
	{
		Date date = new Date();
		Date date1 = new Date();
		int dt = date.getDate();
		date.setDate(dt + 1);
		String startTimeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(date);
		int dt1 = date1.getDate();
		date1.setDate(dt1 + 1);
		int min = date1.getMinutes();
		date1.setMinutes(min+5);
		String endTimeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(date1);

		System.out.println("****** START TIME SLOT ******* " + startTimeStamp + " *** END TIME SLOT *** " + endTimeStamp);
		return Arrays.asList(startTimeStamp, endTimeStamp);

	}

	static HashMap<String, String> singleHeader(){
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("content-type", "application/json");
		requestheaders.put("Cache-Control", "no-cache");
		return requestheaders;
	}
}
