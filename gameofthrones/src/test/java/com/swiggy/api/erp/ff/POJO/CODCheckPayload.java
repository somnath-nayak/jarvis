package com.swiggy.api.erp.ff.POJO;


import org.codehaus.jackson.annotate.JsonProperty;

public class CODCheckPayload {

    @JsonProperty("request_id")
    private String requestId;
    @JsonProperty("customer_details")
    private CustomerDetails customerDetails;
    @JsonProperty("session_details")
    private SessionDetails sessionDetails;
    @JsonProperty("delivery_details")
    private DeliveryDetails deliveryDetails;
    @JsonProperty("cart_details")
    private CartDetails cartDetails;

    /**
     * No args constructor for use in serialization
     *
     */
    /**
     *
     * @param deliveryDetails
     * @param requestId
     * @param customerDetails
     * @param sessionDetails
     * @param cartDetails
     */
    public CODCheckPayload(String requestId, CustomerDetails customerDetails, SessionDetails sessionDetails, DeliveryDetails deliveryDetails, CartDetails cartDetails) {

        this.requestId = requestId;
        this.customerDetails = customerDetails;
        this.sessionDetails = sessionDetails;
        this.deliveryDetails = deliveryDetails;
        this.cartDetails = cartDetails;
    }

    @JsonProperty("request_id")
    public String getRequestId() {
        return requestId;
    }

    @JsonProperty("request_id")
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    @JsonProperty("customer_details")
    public CustomerDetails getCustomerDetails() {
        return customerDetails;
    }

    @JsonProperty("customer_details")
    public void setCustomerDetails(CustomerDetails customerDetails) {
        this.customerDetails = customerDetails;
    }

    @JsonProperty("session_details")
    public SessionDetails getSessionDetails() {
        return sessionDetails;
    }

    @JsonProperty("session_details")
    public void setSessionDetails(SessionDetails sessionDetails) {
        this.sessionDetails = sessionDetails;
    }

    @JsonProperty("delivery_details")
    public DeliveryDetails getDeliveryDetails() {
        return deliveryDetails;
    }

    @JsonProperty("delivery_details")
    public void setDeliveryDetails(DeliveryDetails deliveryDetails) {
        this.deliveryDetails = deliveryDetails;
    }

    @JsonProperty("cart_details")
    public CartDetails getCartDetails() {
        return cartDetails;
    }

    @JsonProperty("cart_details")
    public void setCartDetails(CartDetails cartDetails) {
        this.cartDetails = cartDetails;
    }

}