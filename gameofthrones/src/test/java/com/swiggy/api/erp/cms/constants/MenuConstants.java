package com.swiggy.api.erp.cms.constants;

/**
 * Created by kiran.j on 2/2/18.
 */
public interface MenuConstants {

    enum negativejson {
        empty,
        entity,
        maincategory,
        items
        
    }

    String Success_message = "Your request is accepted. We will notify you soon.";
    double SuccessStatusCode = 202;
    String Successcode = "SUCCESS";

    int failure_status_code = 400;
    String statusMessage = "Basic validation failed";
    String failure_code = "CAE014";
    String failure_pricing_combinations = "menu -> items no. 1 -> pricing_combinations no. 1 -> price";
    String failure_pricing_message = "Price should be in between 1 to 10000";
    String content_type = "application/json";
    String token_id2 ="8bjfe8s5752he2w6";
    String token_id = "1ksue6a7593ks7s2";
    String token_id3 = "8kdme3b9583eg4f1";
    String auth = "Basic dXNlcjpjaGVjaw==";
    String code = failure_code;
    String main_categories_err = "menu -> main_categories";
    String items_err = "menu -> items";
    String rest_id2= "LT001";
    String rest_id = "KF0001";
    String rest_id3 = "FM8211";
    String entity = "entity";
    String entities_error_message = "Entities cannot be blank";
    String main_category_err = "menu -> main_categories";
    String main_category_message = "Number of Main Categories should be in between 1 to 50";
    String item_errm= "menu -> items";
    String item_message = "Number of Items should be in between 1 to 400";
    String items_zeroerror = "menu -> items no. 1 -> price";
    String items_rejected_value = "0.0";
    String items_zero_message = "Item Price cannot be 0 if pricing combination is empty ";
    String dependant_variantid = "xx";
    String dependant_variantgroupid = "yy";

    String main_category_id = "AUTO_CM_CatId1";
    String main_category_name = "AUTO_CM_CatName";
    String main_category_order = "1";

    String sub_category_id = "AUTO_CM_Subid1";
    String sub_category_name = "AUTO_CM_SubName";
    String subcat_desc = "AUTO_CM_desc";
    int subcat_order = 0;
    String subcat_catid = main_category_id;
    
    String items_id = "AUTO_CM_id1";
    String items_category_id =main_category_id;
    String items_subcat_id = sub_category_id;
    String items_name = "AUTO_CM_name";
    boolean items_isveg = true;
    boolean items_enable = true;
    String items_description = "AUTO_CM_desc";
    double zero_price = 0;
    double default_price = 10;
    boolean items_instock = true;
    String items_image_url = "https://swiggy-rest-item-image-upload.s3.amazonaws.com/item_image/011775da-6b10-11e8-a1fe-06e944becd6dFriday_08_June_2018__17_05id9179213.jpg";
    String image_id="nnomtt0pnqhwmnxjnut2";
    
    String variant_groups_id = "AUTO_CM_VdId1"; 
    String variant_groups_name ="AUTO_CM_name";
    int variant_groups_order = 1;

    String addonid = "AUTO_CM_AddonId1";
    String addon_group_id = "AUTO_CM_AgId1";

    String variants_id = "AUTO_CM_VariantId1";
    String variants_name = "AUTO_CM_VariantName";
    int variants_order = 1;
    boolean variants_isveg = true;
    boolean variants_instock = true;
    String variants_image_url = "http://kfc-uat-4902398.jpg";

    String addons_groups_id = addon_group_id;
    String addons_groups_name = "AUTO_CM_AgName";
    int addons_groups_order = 1;


    int itemLevelAddon_free_limit=-1;
    int itemLevelAddon_limit=-1;
    /*int cat_id=1337458;
    int subcat_id=1337459;
    int restau_id=43343;*/
    int cat_id=71414;
    int subcat_id=71415;
    int restau_id=223;
    int price=100;
    int veg=0;
    String vat="tVat";
    String serviceTax="sTax";
    String serviceCharge="servCharge";
    int addlimit=0;
    int perishable=0;
    String descr="test";


    int addon_limit = 1;
    int addon_min_limit = 0;
    int addon_free_limit = 1;
    String[] addonlimit = {"menu -> items no. 1 -> addon_groups no. 1 -> addon_min_limit", "Addon Min Limit should be in between 0 to 50 "};
    String[] addonfreelimit = {"menu -> items no. 1 -> addon_groups no. 1 -> addon_free_limit", "Addon free limit should in between -1 to 50 "};
    String[] addongroup_id = {"menu -> items no. 1 -> pricing_combinations","Addon Group Ids and corresponding Addon Ids in addon_combination should exist in the Addon Groups","menu -> items no. 1 -> addon_groups no. 1 -> id", "Addon Group ID cannot be blank "};
    String[] pricing_addon_groupid = {"menu -> items no. 1 -> pricing_combinations no. 1 -> addon_combination no. 1 -> addon_group_id", "Addon Group ID cannot be blank "};
    String[] pricing_addon_id = {"menu -> items no. 1 -> pricing_combinations no. 1 -> addon_combination no. 1 -> addon_id", "Addon ID cannot be blank"};
    String[] pricing_price = {"menu -> items no. 1 -> pricing_combinations no. 1 -> price", "Price should be in between 1 to 10000"};
    String[] pricing_addon = {"menu -> items no. 1 -> pricing_combinations no. 1 -> addon_combination", "addon_combination cannot be null"};
    String[] pricing_variant = {"menu -> items no. 1 -> pricing_combinations", "size of variant_combinations in pricing_combinations should be equal to size of variant groups","menu -> items no. 1 -> pricing_combinations no. 1 -> variant_combination","variant_combination cannot be null"};
    String[] addongroup_name = {"menu -> items no. 1 -> addon_groups no. 1 -> name", "Addon Group Name cannot be blank "};
    String[] addon_id = {"menu -> items no. 1 -> pricing_combinations","Addon Group Ids and corresponding Addon Ids in addon_combination should exist in the Addon Groups",null,"menu -> items no. 1 -> addon_groups no. 1 -> addons no. 1 -> id"};
    String[] addon_name = {"menu -> items no. 1 -> addon_groups no. 1 -> addons no. 1 -> name", "Addon Name cannot be blank"};
    String[] item_id = {"menu -> items no. 1 -> id", "Item ID cannot be blank "};
    String[] item_name = {"menu -> items no. 1 -> name", "Item Name cannot be blank "};
    //String[] variant_id = {"menu -> items no. 1 -> pricing_combinations no. 1 -> variant_combination no. 1 -> variant_id","Variant ID cannot be blank"};
    String[] variant_id = {"menu -> items no. 1 -> pricing_combinations","Variant Group Ids and corresponding Variant Ids in variant_combination should exist in the Variant Groups","menu -> items no. 1 -> pricing_combinations no. 1 -> variant_combination no. 1 -> variant_id","Variant ID cannot be blank"};
    	       
    String[] variantgroup_id = {"menu -> items no. 1 -> pricing_combinations no. 1 -> variant_combination no. 1 -> variant_group_id", "Variant Group ID cannot be blank "};
    String[] addonid_pricing = {"menu -> items no. 1 -> pricing_combinations","Addon Group Ids and corresponding Addon Ids in addon_combination should exist in the Addon Groups","menu -> items no. 1 -> pricing_combinations no. 1 -> addon_combination no. 1 -> addon_id", "Addon ID cannot be blank"};
    String[] addongroupid_pricing = {"menu -> items no. 1 -> pricing_combinations","Addon Group Ids and corresponding Addon Ids in addon_combination should exist in the Addon Groups","menu -> items no. 1 -> pricing_combinations no. 1 -> addon_combination no. 1 -> addon_group_id","Addon Group ID cannot be blank "};
    String[] variantgroupname = {"menu -> items no. 1 -> variant_groups no. 1 -> name", "Variant Group Name cannot be blank "};
    String[] variantgroupid = {"menu -> items no. 1 -> pricing_combinations","ordering of variant_combination in pricing_combinations should be equal to ordering of variant groups","menu -> items no. 1 -> variant_groups no. 1 -> id","Variant Group ID cannot be blank "};
    String[] variantid = { "menu -> items no. 1 -> pricing_combinations","ordering of variant_combination in pricing_combinations should be equal to ordering of variant groups","menu -> items no. 1 -> variant_groups no. 1 -> id", "Variant Group ID cannot be blank "};
    String[] variantname = {"menu -> items no. 1 -> variant_groups no. 1 -> name", "Variant Group Name cannot be blank "};
 //   String[] variantDefaultFlagNull = {"menu -> items no. 1 -> variant_groups","default key is mandatory in one of the variants in first variant group."};
    String[] variantDefaultFlagNull = {"menu -> items no. 1 -> variant_groups","default key is mandatory in one of the variants in first variant group."};
    String[] variantId_inPCMissMatch = {"menu -> items no. 1 -> pricing_combinations","Variant Group Ids and corresponding Variant Ids in variant_combination should exist in the Variant Groups"};
    String[] variantgroupId_inPCMissMatch = {"menu -> items no. 1 -> pricing_combinations",  "ordering of variant_combination in pricing_combinations should be equal to ordering of variant groups"};
    String[] addongroupId_inPCMissMatch = {"menu -> items no. 1 -> pricing_combinations","Addon Group Ids and corresponding Addon Ids in addon_combination should exist in the Addon Groups"};
    String[] addonId_inPCMissMatch = {"menu -> items no. 1 -> pricing_combinations","Addon Group Ids and corresponding Addon Ids in addon_combination should exist in the Addon Groups"};
    
    String[] addonFLagNullInPc=    {"menu -> items no. 1 -> pricing_combinations no. 1 -> addon_combination","addon_combination cannot be null"};
    String[] GstError= {"menu -> items no. 1 -> gst_details -> igst","GST should be in between 0 to 40","menu -> items no. 1 -> gst_details -> sgst","GST should be in between 0 to 40","menu -> items no. 1 -> gst_details -> cgst","GST should be in between 0 to 40"};
    String[] GstErrorVariant= { "menu -> items no. 1 -> variant_groups no. 1 -> variants no. 1 -> gst_details -> igst", "GST should be in between 0 to 40","menu -> items no. 1 -> variant_groups no. 1 -> variants no. 1 -> gst_details -> sgst","GST should be in between 0 to 40","menu -> items no. 1 -> variant_groups no. 1 -> variants no. 1 -> gst_details -> cgst","300.0","GST should be in between 0 to 40"};
    String[] GstErrorAddon= {"menu -> items no. 1 -> addon_groups no. 1 -> addons no. 1 -> gst_details -> igst","GST should be in between 0 to 40","menu -> items no. 1 -> addon_groups no. 1 -> addons no. 1 -> gst_details -> sgst","GST should be in between 0 to 40","menu -> items no. 1 -> addon_groups no. 1 -> addons no. 1 -> gst_details -> cgst","GST should be in between 0 to 40"};

    String[] variantNullInVG= {"menu -> items no. 1 -> variant_groups no. 1 -> variants", "Variants cannot be blank "};
   
    String[] AddonNullInAG = {"menu -> items no. 1 -> addon_groups no. 1 -> addons","Addons cannot be blank "};
    String[]  VandAdoonsNull= {"menu -> items no. 1 -> pricing_combinations no. 1 -> addon_combination","addon_combination cannot be null"};
    String[] variantPriceerror = {"menu -> items no. 1 -> pricing_combinations","size of variant_combinations in pricing_combinations should be equal to size of variant groups"};
  
    String[] pricingCombowithoutAddonVariant = {"menu -> items no. 1 -> variant_groups","variant_groups is mandatory when pricing combinations are present"};
    
    String[] gst = {"menu -> items no. 1 -> gst_details -> igst", "GST should be in between 0 to 40"};
    String[] catid = {"menu -> main_categories no. 1 -> id", "Category ID cannot be blank "};
    String[] catname = {"menu -> main_categories no. 1 -> name", "Category Name cannot be blank "};
    String[] subcatid = {"menu -> main_categories no. 1 -> sub_categories no. 1 -> id", "Sub-category ID cannot be blank "};
    String[] subcatname = {"menu -> main_categories no. 1 -> sub_categories no. 1 -> name", "Sub-category Name cannot be blank "};

    String[] pCWithoutVariantion = {"menu -> items no. 1 -> pricing_combinations no. 1 -> variant_combination","variant_combination cannot be null"};
    String[] pCWithPrriceWithoutVariantion = {"menu -> items no. 1 -> pricing_combinations no. 1 -> price","price should be null if variant combination is blank or empty"};

    String[] maincatid = {"category -> id", "Category ID cannot be blank "};
    String[] maincatname = {"category -> name", "Category Name cannot be blank "};
    String[] maincatid_update = {"category -> id", "Category id length should be in between 1 to 50"};
    String[] maincatname_update = {"category -> name", "Category Name length should in between 1 to 50 "};
    String[] addsubcatid = {"sub_category -> id", "Sub-category ID cannot be blank "};
    String[] addsubcatname = {"sub_category -> name", "Sub-category Name cannot be blank "};
    String[] addsubcatname_update = {"sub_category -> name", "Sub-category Name length should be in between 1 and 50 "};
    String[] addsubcatid_update = {"sub_category -> id", "Sub-Category id length should be in between 1 to 50"};
    String[] bulkaddmaincatid = {"category no. 1 -> id", "Category ID cannot be blank "};
    String[] bulkaddmaincatname = {"category no. 2 -> name", "Category Name cannot be blank "};
    String[] bulkaddvariantgroup_id = {"variant_group no. 1 -> id", "Variant Group ID cannot be blank "};
    String[] getBulkaddvariantgroup_itemid = {"variant_group no. 1 -> item_id", "Item ID cannot be blank "};
    String[] addvariant_id = {"variant -> id", "Variant ID cannot be blank"};
    String[] addvariant_price = {"variant -> price", "Variant Price should be in between 0 to 1000 "};
    String[] variant_cgst = {"variant -> gst_details -> cgst", "GST should be in between 0 to 40"};
    String[] variant_igst = {"variant -> gst_details -> igst", "GST should be in between 0 to 40"};
    String[] variant_sgst = {"variant -> gst_details -> sgst", "GST should be in between 0 to 40"};
    String[] updatetvariant_id = {"variant -> id", "Variant id length should be in between 1 to 50"};
    String[] addvariant_name = {"variant -> name", "Variant Name cannot be blank "};
    String[] updatevariant_name = {"variant -> name", "Variant Name length cannot be greater then 50 "};
    String[] addvariant_groupid = {"variant -> variant_group_id", "Variant Group ID cannot be blank "};
    String[] createaddongroup_id = {"addon_group -> id", "Addon Group ID cannot be blank  "};
    String[] createaddongroup_itemid = {"addon_group -> item_id", "Item ID cannot be blank "};
    String[] createaddongroup_name = {"addon_group -> name", "Addon Group Name cannot be blank  "};
    String[] updateaddongroup_id = {"addon_group -> id", "Addon Group id length should be in between 1 to 50"};
    String[] addbulkaddongroup_id = {"addon_group no. 2 -> id", "Addon Group ID cannot be blank  "};
    String[] addbulkaddongroup_itemid = {"addon_group no. 2 -> item_id", "Item ID cannot be blank "};
    String[] addbulkaddongroup_name = {"addon_group no. 2 -> name", "Addon Group Name cannot be blank  "};
    String[] updatebulkaddongroup_addonminlimit = {"addon_group no. 2 -> addon_min_limit", "Minimum Addon limit should in between 0 to 20"};
    String[] bulkvariant_id = {"variant no. 1 -> id", "Variant ID cannot be blank"};
    String[] bulkvariant_name = {"variant no. 1 -> name", "Variant Name cannot be blank "};
    String[] bulkvariant_groupdid = {"variant no. 1 -> variant_group_id", "Variant Group ID cannot be blank "};
    String[] bulkvariant_price = {"variant no. 1 -> price", "Variant Price should be in between 0 to 1000 "};
    String[] bulkvariant_cgst = {"variant no. 1 -> gst_details -> cgst", "GST should be in between 0 to 40"};
    String[] bulkvariant_igst = {"variant no. 1 -> gst_details -> igst", "GST should be in between 0 to 40"};
    String[] bulkvariant_sgst = {"variant no. 1 -> gst_details -> sgst", "GST should be in between 0 to 40"};
    String[] updatebulkvariant_name = {"variant no. 1 -> name", "Variant Name length cannot be greater then 50 "};
    String[] addon_groupid = {"addon -> addon_group_id", "Addon Group ID cannot be blank"};
    String[] createaddon_id = {"addon -> id","Addon ID cannot be blank"};
    String[] createaddon_name = {"addon -> name", "Addon Name cannot be blank"};
    String[] createaddon_price = {"addon -> price", "Addon Price should in between 0 to 500 "};
    String[] bulkaddon_id = {"addon no. 1 -> id", "Addon ID cannot be blank"};
    String[] bulkaddon_group_id = {"addon no. 2 -> addon_group_id", "Addon Group ID cannot be blank"};
    String[] bulkaddon_name = {"addon no. 2 -> name", "Addon Name cannot be blank"};
    String[] bulkaddon_price = {"addon no. 2 -> price", "Addon Price should in between 0 to 500 "};
    String[] additem_id = {"item -> id", "Item ID cannot be blank "};
    String[] additem_name = {"item -> name", "Item Name cannot be blank "};
    String[] additem_subcategory = {"item -> sub_category_id", "Item Sub Category ID length cannot be greater then 50 "};
    String[] additem_price = {"item -> price", "Item Price should be in between 1 to 5000 "};
    String[] additem_packaging = {"item -> packing_charge", "Item Packing Charge should be in between 0 to 1000"};
    String[] additem_sgst = {"item -> gst_details -> sgst", "GST should be in between 0 to 40"};
    String[] additem_cgst = {"item -> gst_details -> cgst","GST should be in between 0 to 40"};
    String[] additem_igst = {"item -> gst_details -> igst", "GST should be in between 0 to 40"};
    String[] updateitem_name = {"item -> name", "Item Name length cannot be greater then 50 "};
    String[] bulkadditem_categoryid = {"item no. 2 -> category_id", "Category ID of a Item cannot be blank "};
    String[] bulkadditem_id = {"item no. 2 -> id", "Item ID cannot be blank "};
    String[] bulkadditem_name = {"item no. 2 -> name", "Item Name cannot be blank "};
    String[] bulkadditem_price = {"item no. 2 -> price","Item Price should be in between 1 to 5000 "};
    String[] bulkadditem_packagingprice = {"item no. 2 -> packing_charge","Item Packing Charge should be in between 0 to 1000"};
    String[] bulkadditem_cgst = {"item no. 2 -> gst_details -> cgst", "GST should be in between 0 to 40"};
    String[] bulkadditem_igst = {"item no. 2 -> gst_details -> igst", "GST should be in between 0 to 40"};
    String[] bulkadditem_sgst = {"item no. 2 -> gst_details -> sgst", "GST should be in between 0 to 40"};
    String[] bulkupdateitem_name = {"item no. 2 -> name", "Item Name length cannot be greater then 50 "};
    String[] bulksubcategory_categoryid = {"sub_category no. 2 -> category_id","Category ID of Sub-category cannot be blank "};
    String[] bulksubcategory_id = {"sub_category no. 2 -> id", "Sub-category ID cannot be blank "};
    String[] bulksubcategory_name = {"sub_category no. 2 -> name", "Sub-category Name cannot be blank "};
    String[] catIdBlank = {"menu -> main_categories no. 1 -> id", "Category ID cannot be blank "};
    int defaultpojo_price = 10;

    int[] limitcombinations = {7,5
                                ,5,7
                                ,5,5};
    String[] variant_group_itemid = {"variant_group -> item_id", "Item ID cannot be blank "};
    String[] variant_group_name = {"variant_group -> name", "Variant Group Name length should be in between 1 to 50 "};
    String[] variant_group_id = {"variant_group -> id", "Variant Group ID cannot be blank "};

    String jpg = "https://swiggy-rest-item-image-upload.s3.amazonaws.com/item_image/629f496c-fa8d-11e6-bd5f-06f9086dbe2bFriday_24_February_2017__18_03id22912.jpg";
    String jpeg = "https://swiggy-rest-item-image-upload.s3.amazonaws.com/item_image/e97774fc-aa71-11e7-bd5f-06f9086dbe2bFriday_06_October_2017__14_09id2518909.jpeg";
    String png = "https://s3-ap-southeast-1.amazonaws.com/swiggy-rest-item-image-upload/item/upload/278a9c85-03e5-49dc-8ddc-c120fa779f42-crunchy-chocolate-nutty-rocks-12-pcs1501325569%20(1).png";

    int open_time = 0000;
    int close_time = 1200;
    int day_of_week = 1;
    int[] invalid_day_week = {-5, 0, 10};
    String[] invalid_day_errors = {"menu -> items no. 1 -> time_slots no. 1 -> day_of_week","Day of week should be in between 1 to 7 "};
    String[] greateropentime = {"menu -> items no. 1 -> time_slots no. 1 -> open_time", "Item Slot open close time not valid"};
    String[] invalid_slot = { "menu -> items no. 1 -> time_slots no. 1 -> close_time","Item Slot open close time not valid"};
    String[] invalid_slot1 =  {"menu -> items no. 1 -> time_slots no. 1 -> open_time","Item Slot open time not valid", "menu -> items no. 1 -> time_slots no. 1 -> close_time","Item Slot close time not valid "};

    String[] packaging_Charges = {"menu -> items no. 1 -> packing_charge", "Item Packing Charge should be in between 0 to 1000"};

    
    int[] is_veg = {0,1,2,6};
    boolean[] enable = {true,false};
    boolean[] in_stock = {true, false};
    int[] open_close = {0000, 2400, 2360, 0001, 2359};

    double cgst = 0.02;
    double sgst = 0.02;
    double igst = 0.02;

    double invalid_cgst = 60;
    double invalid_sgst = 60;
    double invalid_igst = 60;

    double negative_cgst = -5;
    double negative_sgst = -5;
    double negative_igst = -5;

    int addon_groups_limit = -1;
    int disabled = 0;
    int enabled = 1;
    String regular_item = "REGULAR_ITEM";
    String pop_item = "POP_ITEM";
    String[] rests = {"FM8001", "FS001", "KF0001"};
	String[] ItemIdforSwitch = {"Auto_ItemIDFrCatSwitchId001","Auto_ItemIDFrCatSwitchId002"};
	String[] ItemIdforName = {"Auto_ItemNameFrCatSwitch001","Auto_ItemNameFrCatSwitch002"};
	String[] CatIdforSwitch = {"Auto_CatIDfrCatSwitchId001","Auto_CatIDfrCatSwitchId002"};
	String[] CatNameforSwitch= {"Auto_CatNamefrCatSwitch001","Auto_CatNamefrCatSwitch002"};
	String[] SubCatIdforSwitch = {"Auto_SubCatIDfrCatISwitch001","Auto_SubCatIDfrCatISwitch002"};
	String[] SubCatNameforSwitch ={"Auto_SubCatNamefrCatSwitch001","Auto_SubCatNamefrCatSwitch002"};
	String ItemIdforVariantRemoval="Auto_ItemIDFrVariantRemoval001";
	String CatIdforVariantRemoval="Auto_CatIDFrVariantRemoval001";
	String SubIdforVariantRemoval="Auto_SubIDFrVariantRemoval001";
    
    public static String Item_OOS_Invalid_Item_id="7875548698899";
    public static String Item_OOS_from_time="2018-10-24 19:00:00";
    public static String Item_OOS_to_time="2018-10-25 18:00:00";
    

}
