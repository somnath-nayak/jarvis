package com.swiggy.api.erp.cms.constants;


public interface AttributeConstants {
    String unit = "ml";
    int value = 10;
   String cutlery="Spoon";
   String packaging="Aluminium Can";
    String prep_style="Steaming";
    int serves_how_many=1;
    String spice_level="NONSPICY";
   String veg_classifier="VEG";
   String[] accompaniments={"Chutney","Raita"};

}
