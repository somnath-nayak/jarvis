package com.swiggy.api.erp.crm.tests.foodissues;

import com.swiggy.api.erp.crm.constants.CRMConstants;
import com.swiggy.api.erp.crm.dp.foodissues.GratificationDP;
import com.swiggy.api.erp.crm.foodissues.pojo.CCResolution.CCResolutionPOJO;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

public class Gratification {

    Initialize gameofthrones = new Initialize();
    GratificationDP gratificationDP = new GratificationDP();
    CCResolutionPOJO po = new CCResolutionPOJO();


    
    public String itemId = CRMConstants.itemId;
    public String restId = CRMConstants.restId;
    public String userId = CRMConstants.userID;

    public int cloneCheck(String orderId, String itemId, String restId) {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("Content-Type", "application/json");
        GameOfThronesService cloneTest = new GameOfThronesService("cconeview", "cloneCheck", gameofthrones);
        String[] payload = new String[]{orderId, restId, itemId};

        Processor cloneTestResponse = new Processor(cloneTest, requestheaders, payload);
        int res = cloneTestResponse.ResponseValidator.GetNodeValueAsInt("statusCode");
        return res;

    }

    @Test(priority = 0, enabled = true, dataProviderClass = GratificationDP.class, dataProvider = "couponRequest", description = "Gratification -  Provide Coupon")
    public void gratificationCoupon(String Payload) {
        
        HashMap<String, String> requestheaders_gratificationCoupon = new HashMap<String, String>();
        requestheaders_gratificationCoupon.put("Content-Type", "application/json");
        requestheaders_gratificationCoupon.put("initiator-id", "212");
        requestheaders_gratificationCoupon.put("initiator-type", "BOT");
     
        System.out.println("request headers    " +requestheaders_gratificationCoupon);

        GameOfThronesService gratificationCoupon = new GameOfThronesService("cconeview", "gratification", gameofthrones);

        String[] payload1 = new String[]{Payload};
        Processor gratificationCoupon_response = new Processor(gratificationCoupon, requestheaders_gratificationCoupon, payload1);
        
        System.out.println("coupond response   " +gratificationCoupon_response);

        String userid = CRMConstants.userIDForCoupon;
        
        Assert.assertNotNull(gratificationCoupon_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data..isResolved"));
        Assert.assertEquals(gratificationCoupon_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("status"), "SUCCESS");
        Assert.assertEquals(gratificationCoupon_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data..type"), "[\""+"COUPON"+"\"]");
        Assert.assertEquals(gratificationCoupon_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data..data.userId"), "["+userid+"]");

    }
        
    @Test(priority = 1, enabled = true, dataProviderClass = GratificationDP.class, dataProvider = "refundRequest", description = "Gratification -  Award Refund")
    public void gratificationRefund(String Payload) throws Exception {

        String orderId = CRMConstants.regularPostPaidOrder;

		HashMap<String, String> requestheaders_gratificationRefund = new HashMap<String, String>();
        requestheaders_gratificationRefund.put("Content-Type", "application/json");
        requestheaders_gratificationRefund.put("initiator-id", "212");
        requestheaders_gratificationRefund.put("initiator-type", "BOT");
     
        System.out.println("request headers    " +requestheaders_gratificationRefund);

        GameOfThronesService gratificationRefund = new GameOfThronesService("cconeview", "gratificationRefund", gameofthrones);

        String[] payload1 = new String[]{Payload};

        Processor gratificationRefund_response = new Processor(gratificationRefund, requestheaders_gratificationRefund, payload1);

        System.out.println("refund resolution    " +gratificationRefund_response);
        
        Assert.assertEquals(gratificationRefund_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("status"), "SUCCESS"); 
        Assert.assertEquals(gratificationRefund_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data..type"), "[\"REFUND\"]");
        Assert.assertEquals(gratificationRefund_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data..orderId"), "["+orderId+"]");
        Assert.assertEquals(gratificationRefund_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data..amount"), "[1.0]");
        Assert.assertEquals(gratificationRefund_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data..reason"), "[\"test refund\"]");
        Assert.assertEquals(gratificationRefund_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data..source"), "[\"PG\"]");  
    }

    @Test(enabled = true, priority = 2, dataProviderClass = GratificationDP.class, dataProvider = "replicateRequest", description = "Gratification - Replicate order")
    public void gratificationReplicate(String Payload) {

        String orderId = CRMConstants.regularPostPaidOrder;

        //Get order details api objects

        GameOfThronesService getOrderDetail = new GameOfThronesService("crmff", "orderstatusff", gameofthrones);
        HashMap<String, String> requestheaders1 = new HashMap<>();
        requestheaders1.put("Content-Type", "application/json");

        //First check the order can be replicated, clone check api
        int clonePossible = cloneCheck(orderId, itemId, restId);
        System.out.println(clonePossible);

        //Payload for the replicate api
        String[] payload=new String[]{Payload};

        //Headers for resolution api
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("Content-Type", "application/json");
        requestheaders.put("initiator-id", "212");
        requestheaders.put("initiator-type", "AGENT");

        //Replicate the order
        GameOfThronesService replicateOrder = new GameOfThronesService("cconeview", "gratification", gameofthrones);

        Processor replicateOrderResponse = new Processor(replicateOrder, requestheaders, payload);
        System.out.println("Replicate order response  " + replicateOrderResponse);

        //Validate the status, isResolved Flag and baseOrderId
        Assert.assertEquals(replicateOrderResponse.ResponseValidator.GetNodeValue("status"), "SUCCESS");
        Assert.assertEquals(replicateOrderResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data..isResolved").replace("[", "").replace("]", ""), "true");
        Assert.assertEquals(replicateOrderResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data..data.orderItems..quantity").replace("[", "").replace("]", ""), "1");

        String baseOrderId = replicateOrderResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data..data.baseOrderId").replace("[", "").replace("]", "");
        Assert.assertEquals(baseOrderId, orderId);



        //Fetch the replicated order id from the response

        String replicatedOrderId = replicateOrderResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data..data.orderId").replace("[", "").replace("]", "");

        //Validate the replicated order has the base order id in the order details response
        Processor getOrderDetailResponse = new Processor(getOrderDetail, requestheaders1, null, new String[]{replicatedOrderId});

        Assert.assertEquals(getOrderDetailResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("objects..base_order_id").replace("[", "").replace("]", ""), ("\"" + orderId + "\""));
    }

    @Test(priority = 3, enabled = true, description = "Gratification - Refund and Coupon")
    public void gratificationRefundCoupon() {

        String orderId = CRMConstants.regularPostPaidOrder;

		HashMap<String, String> requestheaders_igcccoupon = new HashMap<String, String>();
        requestheaders_igcccoupon.put("Content-Type", "application/json");
             
        GameOfThronesService igcccoupon = new GameOfThronesService("crmigcc", "coupon", gameofthrones);

    	Date date = new Date();
        DateFormat coupondFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        String coupon = coupondFormat.format(date);
        
        String[] payload = new String[]{"igcc"+coupon+"", "IGCC"+coupon+""};

        Processor igcccoupon_response = new Processor(igcccoupon, requestheaders_igcccoupon, payload);
        System.out.println("igcc coupon response  " + igcccoupon_response);

        Assert.assertEquals(igcccoupon_response.ResponseValidator.GetNodeValueAsInt("statusCode"), 1);
        Assert.assertNotNull(igcccoupon_response.ResponseValidator.GetNodeValueAsInt("data.id"));

        int couponId = igcccoupon_response.ResponseValidator.GetNodeValueAsInt("data.id");
        String couponCode = igcccoupon_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.code");
        String couponDescription = igcccoupon_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.description");     
        int couponAmount = igcccoupon_response.ResponseValidator.GetNodeValueAsInt("data.discount_amount");
		
		HashMap<String, String> requestheaders_gratificationRefundCoupon = new HashMap<String, String>();
        requestheaders_gratificationRefundCoupon.put("Content-Type", "application/json");
        requestheaders_gratificationRefundCoupon.put("initiator-id", "212");
        requestheaders_gratificationRefundCoupon.put("initiator-type", "BOT");
     
        System.out.println("request headers    " +requestheaders_gratificationRefundCoupon);

        GameOfThronesService gratificationRefundCoupon = new GameOfThronesService("cconeview", "gratificationRefundCoupon", gameofthrones);

        String[] payload1 = new String[]{userId, orderId, "1", "1", "test refund", "SM", couponCode, ""+couponId+"", ""+couponAmount+"", couponDescription};
        
        Processor gratificationRefundCoupon_response = new Processor(gratificationRefundCoupon, requestheaders_gratificationRefundCoupon, payload1);

        System.out.println("refund resolution    " +gratificationRefundCoupon_response);
        
    }


    @Test(enabled = true, priority = 4, dataProviderClass = GratificationDP.class, dataProvider = "getNonDelivered_CancelledOrder", description = "Gratification - Replicate order, for a failed clone check")
    public void gratificationReplicateCloneCheckFailed(String Payload) {

        String orderId = CRMConstants.regularPostPaidOrder;
        //First check the order can be replicated, clone check api
        int clonePossible = cloneCheck(orderId, itemId, restId);
        System.out.println(clonePossible);

        //Payload for the replicate api
        String[] payload=new String[]{Payload};

        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("Content-Type", "application/json");
        requestheaders.put("initiator-id", "212");
        requestheaders.put("initiator-type", "AGENT");

        //Try Replicate the order
        GameOfThronesService replicateOrder = new GameOfThronesService("cconeview", "gratification", gameofthrones);

        Processor replicateOrderResponse = new Processor(replicateOrder, requestheaders, payload);
        System.out.println("Replicate order response  " + replicateOrderResponse);

        //Validate the status, isResolved Flag and baseOrderId
        Assert.assertEquals(replicateOrderResponse.ResponseValidator.GetNodeValue("status"), "SUCCESS");
        Assert.assertEquals(replicateOrderResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data..isResolved").replace("[", "").replace("]", ""), "false");
        Assert.assertEquals(replicateOrderResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data..data.orderItems..quantity").replace("[", "").replace("]", ""), "1");

    }
}

