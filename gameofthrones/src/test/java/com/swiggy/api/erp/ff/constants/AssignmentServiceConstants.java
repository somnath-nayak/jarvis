package com.swiggy.api.erp.ff.constants;

public interface AssignmentServiceConstants {
	
	String SERVICE = "omsassignmentservice";

    int[] verifier_id = {46,47,48};
    int verifier_role_id = 6;
    int logout_new_state_id = 4;
    int verifier_assignment_reason_id = 2;
    int verifier_city_id = 1;

    int[] l1_id = {40,41,42};
    int l1_role_id = 1;
    int l1_assignment_reason_id = 6;
    int l1_city_id = 1;

    int[] l2_id = {43,44,45};
    int l2_role_id = 2;
    int l2_assignment_reason_id = 8;
    int l2_city_id = 1;

    int[] callde_id = {49,50,51};
    int callde_role_id = 7;
    int callde_assignment_reason_id = 9;
    int callde_city_id = 1;

    String Verifier = "Verifier";
    String L1Placer = "L1Placer";
    String L2Placer = "L2Placer";
    String Callde = "CallDE_OE";
 String badOrder = "BAD_ORDER_FOLLOWUP";

    int[] badOrderFollowup_id = {52,53};
    int badOrderFollowup_role_id = 40;
    int badOrderFollowup_assignment_reason_id = 12;
    int badOrderFollowup_city_id = 1;
    String reason_PlacingDelay = "PLACING_DELAY";
    int Bad_logout_new_state_id = 4;
    String subreason_RESTAURANT_POTENTIALLY_CLOSED = "RESTAURANT_POTENTIALLY_CLOSED";
    String reason_CANCELLATION_REQUIRED = "CANCELLATION_REQUIRED";
            String subreason_RESTAURANT_CLOSED = "RESTAURANT_CLOSED";
            int expectedCode=201;

}

