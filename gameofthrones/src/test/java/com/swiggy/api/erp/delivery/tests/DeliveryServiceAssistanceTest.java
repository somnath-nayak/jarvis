package com.swiggy.api.erp.delivery.tests;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.erp.delivery.dp.DeliveryServiceAssiatanceDataProvider;
import com.swiggy.api.erp.delivery.dp.DeliveryServiceDataProvider;
import com.swiggy.api.erp.delivery.helper.AutoassignHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryDataHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryHelperMethods;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceAssiatanceHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceHelper;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Daenerys.Initializer;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.DBHelper;

public class DeliveryServiceAssistanceTest  extends DeliveryServiceAssiatanceDataProvider{
	
	
	
	
	static String header = null, auth, auth1, cycleauth, auth2;
	//Initialize gameofthrones = new Initialize();

	//Initialize gameofthrones = new Initialize();
	Initialize gameofthrones =Initializer.getInitializer();

	DeliveryServiceHelper helper = new DeliveryServiceHelper();
	DeliveryHelperMethods delmeth=new DeliveryHelperMethods();
	AutoassignHelper deauto= new AutoassignHelper();
	DBHelper dbHelper = new DBHelper();
	DeliveryDataHelper deliverydata =new DeliveryDataHelper(); 
	DeliveryServiceAssiatanceHelper asshelp= new DeliveryServiceAssiatanceHelper();

	
	@Test(dataProvider = "DeAssistance", description="DE Assistance" , groups="sanity")
	public void DEAssisrtance( String de_id, String version ,String order_id , String action, String reason,
			int statuscode) throws InterruptedException {
		helper.makeDEActive(de_id);
		helper.makeDEFree(de_id);
		helper.Assign(order_id);
		Processor processor = deauto.delogin(de_id, version);
		Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
		String deotp = (String) delmeth.redisconnectget(DeliveryConstant.de_otp, de_id,
				DeliveryConstant.redisdb);
		Processor processor1 = deauto.deotp(de_id, deotp);
		Assert.assertEquals(processor1.ResponseValidator.GetResponseCode(), 200);
		
		String resp = processor1.ResponseValidator.GetBodyAsText();
		auth = JsonPath.read(resp, "$.data.Authorization").toString();
	    Processor processor3 = asshelp.deAssistance(auth,order_id, action, reason);
	    String resp1 = processor.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(resp1, "$.statusCode");
		Assert.assertEquals(statuscode, statusCode);
	}
	
}




