package com.swiggy.api.erp.ff.constants;

public interface ContextualCancellationConstants {
		
		String hostName = "oms";

		String[] delivery_status= {"unassigned","assigned","confirmed","arrived","pickedup","reached","delivered"};
		String[] placing_status= {"unplaced","placed","ordered","call_partner","parked","with_partner","with_de","partner_delay","delivered"};
		
		int[] groups= {1,2,3,5};
		int[] verificationStatus= {0,1,2};
		
		String[] verification_status_in_string= {"none","pending","completed"};
		String basic_auth = "Basic cmFuZ2FuYXRoOlN3aWdneUAxMjM=";
}
