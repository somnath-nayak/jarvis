package com.swiggy.api.erp.crm.foodissues.pojo.CCResolution;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class resolutionRequestBuilder {

    public static Initialize gameofthrones = new Initialize();

    public static Integer getIssueId(String orderId) throws Exception{
        Integer issueId=null;
        String[] payloadData=new String[]{orderId, "true", "123", "", "", "", "", "", ""};

        // Create Food Issues
        HashMap<String, String> requestheaders_cccreatefoodissues = new HashMap<String, String>();
        requestheaders_cccreatefoodissues.put("Content-Type", "application/json");
        GameOfThronesService cccreatefoodissues = new GameOfThronesService("cconeview", "cccreatefoodissues", gameofthrones);

        String[] orderArray = new String[] { orderId };
        String[] queryparam = orderArray;

        Processor cccreatefoodissues_response = new Processor(cccreatefoodissues, requestheaders_cccreatefoodissues,
                payloadData, queryparam);
        issueId = cccreatefoodissues_response.ResponseValidator.GetNodeValueAsInt("data.id");

        return issueId;
    }


    public String replicateRequest(Integer orderId,Integer userId,String restId, List<CartItem> cartItems) throws Exception
    {
        Data data = new Data();
        Resolution resolution=new Resolution();
        ResolutionRequestData resolutionRequestData=new ResolutionRequestData();

        Integer issueId=getIssueId(orderId.toString());
        data.setCartItems(cartItems);
        data.setOrderIncoming(0);
        data.setRestaurantId(restId);

        resolutionRequestData.setIssueType("FOOD_ISSUES");
        resolutionRequestData.setOrderId(orderId);
        resolutionRequestData.setUserId(userId);
        resolutionRequestData.setIssueId(issueId);

        resolution.setType("REPLICATE_ORDER");
        resolution.setData(data);

        List<Resolution> resolutions = new ArrayList<Resolution>();
        resolutions.add(resolution);
        CCResolutionPOJO obj = new CCResolutionPOJO(resolutionRequestData, resolutions);
        JsonHelper helper = new JsonHelper();
        return helper.getObjectToJSON(obj);

    }

    public String couponRequest(Integer orderId,Integer userId,String restId, String couponCode, String couponDesc, Integer couponId, Integer couponAmount) throws Exception
    {
        Data data = new Data();
        Resolution resolution=new Resolution();
        ResolutionRequestData resolutionRequestData=new ResolutionRequestData();

        data.setCouponAmount(couponAmount);
        data.setCouponId(couponId);
        data.setCouponCode(couponCode);
        data.setCouponDescription(couponDesc);
        data.setOrderIncoming(0);
        data.setRestaurantId(restId);

        resolutionRequestData.setIssueType("FOOD_ISSUES");
        resolutionRequestData.setOrderId(orderId);
        resolutionRequestData.setUserId(userId);
        resolutionRequestData.setIssueId(getIssueId(orderId.toString()));

        resolution.setType("COUPON");
        resolution.setData(data);

        List<Resolution> resolutions = new ArrayList<Resolution>();
        resolutions.add(resolution);
        CCResolutionPOJO obj = new CCResolutionPOJO(resolutionRequestData, resolutions);
        JsonHelper helper = new JsonHelper();
        return helper.getObjectToJSON(obj);

    }

    public String refundRequest(Integer orderId,Integer userId,String restId, String source, String reason, Integer amount) throws Exception
    {
        Data data = new Data();
        Resolution resolution=new Resolution();
        ResolutionRequestData resolutionRequestData=new ResolutionRequestData();

        data.setAmount(amount);
        data.setReason(reason);
        data.setSource(source);
        data.setOrderIncoming(0);
        data.setRestaurantId(restId);

        resolutionRequestData.setIssueType("FOOD_ISSUES");
        resolutionRequestData.setOrderId(orderId);
        resolutionRequestData.setUserId(userId);
        resolutionRequestData.setIssueId(getIssueId(orderId.toString()));

        resolution.setType("REFUND");
        resolution.setData(data);

        List<Resolution> resolutions = new ArrayList<Resolution>();
        resolutions.add(resolution);
        CCResolutionPOJO obj = new CCResolutionPOJO(resolutionRequestData, resolutions);
        JsonHelper helper = new JsonHelper();
        return helper.getObjectToJSON(obj);

    }
}
