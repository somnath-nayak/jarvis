package com.swiggy.api.erp.vms.helper;

import com.swiggy.api.erp.vms.constants.PrepTimeConstants;
import com.swiggy.api.erp.vms.constants.RMSConstants;
import com.swiggy.api.sf.checkout.helper.CommonAPIHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Daenerys.Initializer;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import org.codehaus.jackson.JsonNode;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by kiran.j on 4/24/18.
 */
public class PrepTimeServiceHelper {

    String slotIDs;
    Initialize initializer = Initializer.getInitializer();
    RMSHelper rmsHelper = new RMSHelper();
    HashMap<String, String> auth_map = new HashMap<>();
    public String valid_auth = null;

    public HashMap<String, String> getAuth_map() {
        return auth_map;
    }

    public void setAuth_map(HashMap<String, String> auth_map) {
        this.auth_map = auth_map;
    }

    public Processor getPrepTimeDayWise(String prepTimeRestaurantId, String auth){
        GameOfThronesService services=new GameOfThronesService("rms1", "daywisepreptime", initializer);
        HashMap<String, String> requestheaders =  getDefaultHeaders();
        requestheaders.put("Authorization", auth);
        Processor processor = new Processor(services, requestheaders, null, new String[] { prepTimeRestaurantId });
        return processor;

    }

    public String[] fetchSlotWiseId(String restId) {
        String[] slotidarray = null;
        try {
            GameOfThronesService services = new GameOfThronesService("rms1", "fetchpreptimemultirest", initializer);
            HashMap<String, String> requestheaders =  getPrepTimeHeaders();
            Processor processor = new Processor(services, requestheaders, new String[] { restId }, null);
            JsonNode json = CommonAPIHelper.convertStringtoJSON(
                    processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("data..prepTimes"));
            System.out.println(json.size());
            slotIDs = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..[?(@.type=='SLOTWISE')].id");

            System.out.println("***************"+slotIDs);
            slotIDs= slotIDs.replaceAll("\\[", "").replaceAll("\\]", "");

            System.out.println(slotIDs);
            slotidarray = slotIDs.split(",");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return slotidarray;
    }

    public String[] fetchSlotWiseId(String restId, String slot_type) {
        String[] slotidarray = null;
        try {
            GameOfThronesService services = new GameOfThronesService("rms1", "fetchpreptimemultirest", initializer);
            HashMap<String, String> requestheaders =  getPrepTimeHeaders();
            Processor processor = new Processor(services, requestheaders, new String[] { restId }, null);
            JsonNode json = CommonAPIHelper.convertStringtoJSON(
                    processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("data..prepTimes"));
            System.out.println(json.size());
            slotIDs = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..[?(@.type=='"+slot_type+"')].id");

            System.out.println("***************"+slotIDs);
            slotIDs= slotIDs.replaceAll("\\[", "").replaceAll("\\]", "");

            System.out.println(slotIDs);
            slotidarray = slotIDs.split(",");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return slotidarray;
    }

    public String[] fetchDayWiseId()
    {
        String[] dayidarray = null;
        try{
            GameOfThronesService services=new GameOfThronesService("rms1", "fetchdaypreptimemulti", initializer);
            HashMap<String, String> requestheaders =  getPrepTimeHeaders();
            Processor processor= new Processor(services,requestheaders,new String[] {RMSConstants.prepTimeRestaurantId},null);
            slotIDs = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..[*].id");
            System.out.println("***************"+slotIDs);
            slotIDs= slotIDs.replaceAll("\\[", "").replaceAll("\\]", "");
            System.out.println(slotIDs);
            dayidarray = slotIDs.split(",");
            int len = dayidarray.length;
            System.out.println(len);
            for(int i = 0;i<len;i++){
                System.out.println(dayidarray[i]);
            }
            //		String[] slotidarray = slotIDs.to
            System.out.println("#############################################################################");
            System.out.println("tokeeeeeeennnnnnnnnnnnn" +slotIDs);
        } catch(Exception e){
            e.printStackTrace();
        }
        return dayidarray;
    }

    public Processor fetchPrepTimeMultiRestaurants(String prepTimeRestaurantId, String auth) {
        GameOfThronesService services = new GameOfThronesService("rms1", "fetchpreptimemultirest", initializer);
        HashMap<String, String> requestheaders =  getDefaultHeaders();
        requestheaders.put("Authorization", auth);
        Processor processor = new Processor(services, requestheaders, new String[] { prepTimeRestaurantId }, new String[] { prepTimeRestaurantId });
        return processor;
    }

    public Processor delPreptime(String prepTimeRestaurantId){
        GameOfThronesService services=new GameOfThronesService("rms1", "deletepreptime", initializer);
        HashMap<String, String> requestheaders =  getPrepTimeHeaders();
        Processor processor = new Processor(services, requestheaders, null, new String[] { prepTimeRestaurantId });
        return processor;
    }

    public Processor savingPreptime(String prepTimeRestaurantId, String from, String to, String prep_time, String prep_type, String days, String auth){
        GameOfThronesService services=new GameOfThronesService("rms1", "savepreptime", initializer);
        HashMap<String, String> requestheaders =  getDefaultHeaders();
        requestheaders.put("Authorization", auth);
        Processor processor = new Processor(services, requestheaders,
                new String[] { from, to, prep_time, prep_type, days }, new String[] { prepTimeRestaurantId });
        return processor;
    }

    public Processor savingPreptimeMulti(String prepTimeRestaurantId, String from, String to, String prep_time,
                                         String prep_type, String days, String auth) {
        GameOfThronesService services = new GameOfThronesService("rms1", "savepreptimemulti", initializer);
        HashMap<String, String> requestheaders =  getDefaultHeaders();
        requestheaders.put("Authorization", auth);
        Processor processor = new Processor(services, requestheaders,
                new String[] { prepTimeRestaurantId, from, to, prep_time, prep_type, days },
                new String[] { prepTimeRestaurantId });
        return processor;
    }

    public Processor fetchingDayPreptimeMulti(String prepTimeRestaurantId, String auth) {
        GameOfThronesService services = new GameOfThronesService("rms1", "fetchdaypreptimemulti", initializer);
        HashMap<String, String> requestheaders =  getDefaultHeaders();
        requestheaders.put("Authorization", auth);
        Processor processor = new Processor(services, requestheaders, new String[] { prepTimeRestaurantId }, new String[] { prepTimeRestaurantId });
        return processor;
    }

    public Processor index(String auth){
        GameOfThronesService services=new GameOfThronesService("rms1", "index", initializer);
        HashMap<String, String> requestheaders =  getDefaultHeaders();
        requestheaders.put("Authorization", auth);
        Processor processor = new Processor(services, requestheaders, null, null);
        return processor;
    }

    public void deletePrepTimes(String restId) {
        String[] slots = fetchSlotWiseId(restId);
        for(int i=0;i<slots.length;i++) {
            delPreptime(slots[i]);
        }
    }

    public void createPrepTimeForRest(String RestId, String auth) {
        String slots[] = fetchSlotWiseId(RestId);
        if(slots.length <= 1) {
            String fromDate, toDate;
            Date date = new Date();
            DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
            fromDate = dateFormat.format(-1);
            toDate = dateFormat.format(date);
            savingPreptime(RestId,fromDate, toDate, RMSConstants.prep_time, RMSConstants.PrepType.GLOBAL.toString(), "\"" + RMSConstants.days[0] + "\"", auth);
            savingPreptime(RestId,fromDate, toDate, RMSConstants.prep_time, RMSConstants.PrepType.SLOTWISE.toString(), "\"" + RMSConstants.days[0] + "\"", auth);
        }
    }

    public Processor getPrepTimeSlotWise(String prepTimeRestaurantId, String auth) {
        GameOfThronesService services = new GameOfThronesService("rms1", "slotwisepreptime", initializer);
        HashMap<String, String> requestheaders =  getDefaultHeaders();
        requestheaders.put("Authorization", auth);
        Processor processor = new Processor(services, requestheaders, null, new String[] { prepTimeRestaurantId });
        return processor;

    }

    public boolean aggregateResponseAndStatusCodes(Processor response) {
        boolean success = false;

        System.out.println("SuccessresponseCode :" + response.ResponseValidator.GetResponseCode());
        System.out.println("SuccessstatusCode : " + response.ResponseValidator.GetNodeValueAsInt("statusCode"));

        if ((RMSConstants.successresponseCode == response.ResponseValidator.GetResponseCode())
                && RMSConstants.successstatusCode == response.ResponseValidator.GetNodeValueAsInt("statusCode")) {
            System.out.println("1..inside response");
            success = true;
        } else if ((RMSConstants.successresponseCode == response.ResponseValidator.GetResponseCode())
                && RMSConstants.failedstatusCode == response.ResponseValidator.GetNodeValueAsInt("statusCode")) {
            System.out.println("2..inside response");
            System.out.println("SuccessresponseCode: " + response.ResponseValidator.GetResponseCode());
            System.out.println("SuccessstatusCode :  " + response.ResponseValidator.GetNodeValueAsInt("statusCode"));
            success = true;
        } else if ((RMSConstants.successresponseCode == response.ResponseValidator.GetResponseCode())
                && RMSConstants.failedstatusCode_invalidSession == response.ResponseValidator
                .GetNodeValueAsInt("statusCode")) {
            System.out.println("3..inside response");
            System.out.println("SuccessresponseCode: " + response.ResponseValidator.GetResponseCode());
            System.out.println("SuccessstatusCode :  " + response.ResponseValidator.GetNodeValueAsInt("statusCode"));
            success = true;
        }

        else if((RMSConstants.successresponseCode == response.ResponseValidator.GetResponseCode()) &&
                RMSConstants.failedstatusCode_alreadyExist == response.ResponseValidator.GetNodeValueAsInt("statusCode")){
            System.out.println("4..inside response");
            System.out.println("SuccessresponseCode: " + response.ResponseValidator.GetResponseCode());
            System.out.println("SuccessstatusCode :  " + response.ResponseValidator.GetNodeValueAsInt("statusCode"));
            success = true;
        }
        return success;
    }

    public Processor updatePrepTime(String from, String to, String prep_time, String prep_type, String days,
                                    String prep_id) {
        GameOfThronesService services = new GameOfThronesService("rms1", "updatepreptime", initializer);
        return new Processor(services, getPrepTimeHeaders(), new String[] { from, to, prep_time, prep_type, days },
                new String[] { prep_id });
    }

    public HashMap<String, String> getPrepTimeHeaders() {
        HashMap<String, String> header = new HashMap<String, String>();
        header.put("Content-Type", "application/json");
        header.put("Authorization", RMSConstants.prep_auth);
        return header;
    }

    public HashMap<String, String> getDefaultHeaders() {
        HashMap<String, String> header = new HashMap<String, String>();
        header.put("Content-Type", "application/json");
        return header;
    }

    public Processor healthCheck(String auth){
        GameOfThronesService services=new GameOfThronesService("rms1", "healthcheck", initializer);
        HashMap<String, String> requestheaders = getDefaultHeaders();
        requestheaders.put("Authorization", auth);
        Processor processor = new Processor(services, requestheaders, null, null);
        return processor;
    }

    public boolean verifySaveResponse(String from, String to, String preptime, String type, String days, Processor processor, boolean ignore) throws IOException {
        boolean success = false;
        success = (processor.ResponseValidator.GetNodeValue("data."+PrepTimeConstants.from_time).toString().equalsIgnoreCase(from)) &&
                  (processor.ResponseValidator.GetNodeValue("data."+PrepTimeConstants.end_time).toString().equalsIgnoreCase(to)) &&
                  (processor.ResponseValidator.GetNodeValueAsInt("data."+PrepTimeConstants.prep_time) == Integer.parseInt(preptime)) &&
                  (processor.ResponseValidator.GetNodeValue("data."+PrepTimeConstants.type).toString().equalsIgnoreCase(type));
        if(ignore)
            success &= (processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("data."+PrepTimeConstants.days).replace("[","").replace("]","").equalsIgnoreCase(days));
        return success;
    }

    public boolean verifyNegativeResponse(Processor processor) {
        boolean success = false;
        success = (processor.ResponseValidator.GetNodeValueAsInt("statusCode") == PrepTimeConstants.negative_status) &&
                  (processor.ResponseValidator.GetNodeValue("statusMessage").equalsIgnoreCase(PrepTimeConstants.negative_message));
        return success;
    }

    public void createDefaultSlot(String rest_id) {
        String days = "";
        for(int j=0;j<RMSConstants.days.length;j++) {
            days += "\"" + RMSConstants.days[j] + "\",";
        }
        System.out.println(days);
        Calendar from = Calendar.getInstance();
        Calendar to = Calendar.getInstance();
        from.add(Calendar.HOUR, -1);
        to.add(Calendar.HOUR, 5);
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        System.out.println(dateFormat.format(from.getTime()).toString()+"asdasdasd"+ dateFormat.format(to.getTime()));
        savingPreptime(rest_id, dateFormat.format(from.getTime()).toString(), dateFormat.format(to.getTime()).toString(), "15", RMSConstants.PrepType.SLOTWISE.toString(), days, RMSConstants.prep_auth);
    }

    public void load_auth_map() {
        String[] rests = RMSConstants.restaurantIds;
        if(getAuth_map().isEmpty()) {
            HashMap<String, String> map = new HashMap<>();
            for(int i=0;i< rests.length ;i++) {
                valid_auth = rmsHelper.getLoginToken(rests[i], PrepTimeConstants.auth);
                map.put(rests[i], rmsHelper.getLoginToken(rests[i], PrepTimeConstants.auth));
            }
            setAuth_map(map);
        }
    }
}
