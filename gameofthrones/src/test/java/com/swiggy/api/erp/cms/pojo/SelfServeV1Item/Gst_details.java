package com.swiggy.api.erp.cms.pojo.SelfServeV1Item;

public class Gst_details {
	private String sgst;

    private String inclusive;

    private String cgst;

    private String igst;

    private String uniqueId;

    public String getSgst ()
    {
        return sgst;
    }

    public void setSgst (String sgst)
    {
        this.sgst = sgst;
    }

    public String getInclusive ()
    {
        return inclusive;
    }

    public void setInclusive (String inclusive)
    {
        this.inclusive = inclusive;
    }

    public String getCgst ()
    {
        return cgst;
    }

    public void setCgst (String cgst)
    {
        this.cgst = cgst;
    }

    public String getIgst ()
    {
        return igst;
    }

    public void setIgst (String igst)
    {
        this.igst = igst;
    }

    public String getUniqueId ()
    {
        return uniqueId;
    }

    public void setUniqueId (String uniqueId)
    {
        this.uniqueId = uniqueId;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [sgst = "+sgst+", inclusive = "+inclusive+", cgst = "+cgst+", igst = "+igst+", uniqueId = "+uniqueId+"]";
    }
}
			
			
