package com.swiggy.api.erp.cms.pojo.SelfServeV1Item;

import com.swiggy.api.erp.cms.constants.SelfServeConstants;

public class SelfServeV1ItemsPojo {
	private String requested_by;

    private String[] delete_by_ids;

    private Item_vo item_vo;

    private String comment;

    public String getRequested_by ()
    {
        return requested_by;
    }

    public void setRequested_by (String requested_by)
    {
        this.requested_by = requested_by;
    }

    public String[] getDelete_by_ids ()
    {
        return delete_by_ids;
    }

    public void setDelete_by_ids (String[] delete_by_ids)
    {
        this.delete_by_ids = delete_by_ids;
    }

    public Item_vo getItem_vo ()
    {
        return item_vo;
    }

    public void setItem_vo (Item_vo item_vo)
    {
        this.item_vo = item_vo;
    }

    public String getComment ()
    {
        return comment;
    }

    public void setComment (String comment)
    {
        this.comment = comment;
    }


	public SelfServeV1ItemsPojo build() {
		setDefaultValues();
		return this;
	}

	private void setDefaultValues() {
		Item_vo itemdetail =new Item_vo();
		itemdetail.build();
		if(this.getRequested_by()==null){
			this.setRequested_by(SelfServeConstants.creatdOrUpdatedBy[0]);
		}
		if(this.getRequested_by()==null){
			this.setRequested_by(SelfServeConstants.creatdOrUpdatedBy[0]);
		}
	}

    @Override
    public String toString()
    {
        return "ClassPojo [requested_by = "+requested_by+", delete_by_ids = "+delete_by_ids+", item_vo = "+item_vo+", comment = "+comment+"]";
    }
}
			
			