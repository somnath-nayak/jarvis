package com.swiggy.api.erp.cms.helper;

import com.swiggy.api.erp.cms.constants.CmsConstants;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.SqlTemplate;
import org.json.JSONException;
import org.testng.Assert;

import java.util.*;

public class Grasshopper_Helper {
	Initialize gameofthrones = new Initialize();

	public HashMap<String, String> setFormData() throws JSONException {
		HashMap<String, String> formData = new HashMap<>(); 
		formData.put("file", CmsConstants.FILE_PATH);
		formData.put("entityType", "ITEM");
		formData.put("userId", CmsConstants.USER_ID);
		formData.put("fileName", "testUpload");
		formData.put("userName", "cms-tester");
		formData.put("source", "CMS");
		return formData;
	}
	
	public HashMap<String, String> setHeaders1() throws JSONException {
		
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/x-www-form-urlencoded");
	    headers.put("Accept", "application/json");
		return headers;
	}
	
	

	public Processor uploadFile() throws JSONException {
	        GameOfThronesService service = new GameOfThronesService("cmsgrasshopper", "uploadfile", gameofthrones);
	        Processor p =  new Processor(service, setHeaders1(), null, null, setFormData());
	        return p;
	    }
	 
	 public Processor uploadFileWithoutFilePath() throws JSONException {
	        GameOfThronesService service = new GameOfThronesService("cmsgrasshopper", "uploadfile", gameofthrones);
	        HashMap<String, String> frmDataWithoutFile = setFormData();
	        frmDataWithoutFile.remove("file");
	        Processor p =  new Processor(service, setHeaders1(), null, null, frmDataWithoutFile);
	        return p;
	    }
	 
	 public Processor uploadFileWithoutEntityType() throws JSONException {
	        GameOfThronesService service = new GameOfThronesService("cmsgrasshopper", "uploadfile", gameofthrones);
	        HashMap<String, String> frmDataWithoutEntityType = setFormData();
	        frmDataWithoutEntityType.remove("entityType");
	        Processor p =  new Processor(service, setHeaders1(), null, null, frmDataWithoutEntityType);
	        return p;
	    }
	 
	 public Processor uploadFileWithoutUserId() throws JSONException {
	        GameOfThronesService service = new GameOfThronesService("cmsgrasshopper", "uploadfile", gameofthrones);
	        HashMap<String, String> frmDataWithoutUserId = setFormData();
	        frmDataWithoutUserId.remove("userId");
	        Processor p =  new Processor(service, setHeaders1(), null, null, frmDataWithoutUserId);
	        return p;
	    }
	 
	 public Processor uploadFileWithoutFileName() throws JSONException {
	        GameOfThronesService service = new GameOfThronesService("cmsgrasshopper", "uploadfile", gameofthrones);
	        HashMap<String, String> frmDataWithoutFileName = setFormData();
	        frmDataWithoutFileName.remove("fileName");
	        Processor p =  new Processor(service, setHeaders1(), null, null, frmDataWithoutFileName);
	        return p;
	    }
	 
	 public Processor uploadFileWithoutUserName() throws JSONException {
	        GameOfThronesService service = new GameOfThronesService("cmsgrasshopper", "uploadfile", gameofthrones);
	        HashMap<String, String> frmDataWithoutUserName = setFormData();
	        frmDataWithoutUserName.remove("userName");
	        Processor p =  new Processor(service, setHeaders1(), null, null, frmDataWithoutUserName);
	        return p;
	    }
	 
	 public Processor uploadFileWithoutSource() throws JSONException {
	        GameOfThronesService service = new GameOfThronesService("cmsgrasshopper", "uploadfile", gameofthrones);
	        HashMap<String, String> frmDataWithoutSource = setFormData();
	        frmDataWithoutSource.remove("source");
	        Processor p =  new Processor(service, setHeaders1(), null, null, frmDataWithoutSource);
	        return p;
	    }
	 
	//DB Validation Methods
	 public void getFileDetailsFromDB(String fileId) {
	        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
	        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.getFileId_query+ fileId);
	        
	        Map<String,Object> rowNum = list.get(0);
	        
	        Assert.assertEquals( rowNum.get("id").toString(), fileId);
	        Assert.assertEquals( rowNum.get("entity_type").toString(), "ITEM");
	        Assert.assertTrue( (Integer)rowNum.get("total_record_count")>=0);
	        Assert.assertTrue( (Integer)rowNum.get("records_failed")>=0);
	        Assert.assertTrue( (Integer)rowNum.get("records_passed")>=0);
	          
	        Assert.assertTrue( rowNum.get("status").equals("FINISHED") || rowNum.get("status").equals("GRASSHOPPER_SUCCESS") || 
	        					rowNum.get("status").equals("FILE_SENT_FOR_PARSING"));
	        
	        Assert.assertNotNull(rowNum.get("created_at"));      
			Assert.assertNotNull(rowNum.get("created_by"));      
			Assert.assertNotNull(rowNum.get("updated_at"));      
			Assert.assertNotNull(rowNum.get("source"));      
			Assert.assertNotNull(rowNum.get("file_name"));      
	        
			Assert.assertNotNull(rowNum.get("uploaded_file_s3_url"));      
			Assert.assertNotNull(rowNum.get("response_file_s3_url"));      
	        
			System.out.println("FILE_ID: "+fileId + " successfully created in DB");
	        System.out.println("Validation Success for FILE_ID: "+fileId);
	        
	    }
	 

	 public Object getMenuIdFromDB(String restaurant_id) {
		 	 SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
	        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.getMenuId_query+ restaurant_id);   
			if (list == null || list.isEmpty()) {
				return null;
			} else {
				return list.get(0).get("menu_id");
			}
		}
	 
	 
	 public String[] restIds = {"19718", "19718", "19719"};
	 String[] menuIds = new String[restIds.length];
	 
	 public String[] getMenuIdsHelper()
	 {
		 for(int i=0;i<restIds.length;i++)
		 {
			 SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
		     List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.restMenuMap_query+restIds[i]);
		     System.out.println("REST_MENU_MAP LIST: "+ list);
		   //  System.out.println("LIST MENU1 ID: "+ list.get(i).get("menu_id"));
		     menuIds[i] = list.get(0).get("menu_id").toString();
		     
		     //String catQuery = "Select * from swiggy.menu_taxonomy where parent="+menuId;     
		 }
		return menuIds;
	 }

	 public String[] validateCategoriesfromDB()
	 {
		 String[] menuIds = getMenuIdsHelper();
		 String[] catNames = {"MainCat1", "MainCat1", "MainCat2"};
		 String[] catIds=new String[catNames.length];
		 int k = 0;
		 for(int i=0;i<menuIds.length;i++)
		 {
			 	SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
		        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.getCatSubcat_query+menuIds[i]);  
		        for(int j=0;j<list.size();j++) {
		        System.out.println("CATEGORY LIST: "+ list);
		        catIds[k++] = list.get(j).get("id").toString();
		        Assert.assertEquals( list.get(j).get("name"), catNames[i]);
		        Assert.assertEquals( list.get(j).get("active"), 1);
		        }
		 }
		 
		 System.out.println(Arrays.toString(catIds));
		 return catIds;
		 
	 }
	 
	 
	 public String[] validateSubCategoriesfromDB()
	 {
		 String[] subcatNames = {"SubCat1", "SubCat1",  "SubCat2"};
		 String[] catIds= validateCategoriesfromDB();
		 String[] subCatIds=new String[subcatNames.length];
			
		 int p = 0;
		 for(int i=0;i<catIds.length;i++)
		 {
			 	SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
		        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.getCatSubcat_query+catIds[i]);  
		        for(int j=0;j<list.size();j++) {
		        System.out.println("SUB-CATEGORY LIST: "+ list);
		        subCatIds[p++] = list.get(j).get("id").toString();
		        Assert.assertEquals( list.get(j).get("name"), subcatNames[i]);
		        Assert.assertEquals( list.get(j).get("active"), 1);
		        }
		 }
		 
		 System.out.println(Arrays.toString(subCatIds));
		 return subCatIds;
		 
	 }
	 
	 
	 public String[] validateItemMenuMapfromDB()
	 {
		 String[] subCatIds = validateSubCategoriesfromDB();
		 System.out.println("SubCAT-IDs: "+ Arrays.toString(subCatIds) );
		 String[] itemIds = new String[100];
		 int m=0;
		 for(int i=0;i<subCatIds.length;i++)
		 {
			 	SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
		        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.getItemMenuMap_query+subCatIds[i]);  
		        for(int j=0;j<list.size();j++) {
		        System.out.println("ITEM-MENU-MAP LIST: "+ list);
		        itemIds[m++] = list.get(j).get("item_id").toString();
		  //      Assert.assertNotNull( list.get(j).get("item_id"));
		        Assert.assertEquals( list.get(j).get("active"), 1);
		        }
		 }
		 
		 String[] allItemIds = Arrays.copyOfRange(itemIds, 0,m);
		 System.out.println(Arrays.toString(allItemIds));
		 return allItemIds;
	 }
	 
	 public String[] validateItemsfromDB()
	 {
		 String[] itemNames = {"TestItem1","TestItem2","TestItem3"};
		 String[] arr2 = validateItemMenuMapfromDB();
		 
		 Set<String> mySet = new HashSet<String>(Arrays.asList(arr2));
		 String arr1 = Arrays.toString((mySet.toArray())).replaceAll("[\\[\\](){}]","");
		 String[] itemIds = arr1.split(",");
		 

		 // [8973498,  8973499,  8973500]
		 System.out.println("All Item ids: "+Arrays.toString(itemIds));
		 for(int i=0;i<itemIds.length;i++)
		 {
			 	System.out.println("Item ids of i"+itemIds[i]);
			 	SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
		        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.getItems_query+itemIds[i]);  
		        for(int j=0;j<list.size();j++) {
		        System.out.println("ITEMS DETAILS LIST: "+ list);
		        Assert.assertEquals( list.get(j).get("id").toString(), itemIds[i].toString().trim());
		        Assert.assertEquals( list.get(j).get("active"), 1);
		        Assert.assertEquals( list.get(j).get("name"), itemNames[i]);
		        Assert.assertNotNull(list.get(j).get("price"));
		        Assert.assertEquals(list.get(j).get("restaurant_id").toString(), restIds[i].toString());
		        }
		 }
		 return itemIds;
	 }
	 
	 public void deleteAllItemsFromDB() {
		 		String itemids1 = Arrays.toString(validateItemsfromDB()).toString();
		 		String result = itemids1.substring(1,itemids1.length()-1);
 		
		 		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
		 		sqlTemplate.execute(CmsConstants.deleteItems_query+"("+result+")");
		 		
		 		 List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.getMultipleItems_query+"("+result+")");
		 		 int rowNum = list.size();
		 		 Assert.assertEquals( rowNum, 0);
	    }
	 
	 public void deleteAllItemMenuMapFromDB() {
		 	String[] arr2 = validateSubCategoriesfromDB();
		 
		 	Set<String> mySet = new HashSet<String>(Arrays.asList(arr2));
		 	String arr1 = Arrays.toString((mySet.toArray())).replaceAll("[\\[\\](){}]","");
		 	String[] subcatIds = arr1.split(",");
		 
		 	System.out.println("Subcat Ids: "+ Arrays.toString(subcatIds));
		 	
		 	String subcatIds1 = Arrays.toString(subcatIds).toString();
	 		String result = subcatIds1.substring(1,subcatIds1.length()-1);
	
	 		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
	 		sqlTemplate.execute(CmsConstants.deleteItemMenuMap_query+"("+result+")");
	 		
	 		 List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.getMultipleItemMenuMap_query+"("+result+")");
	 		 int rowNum = list.size();
	 		 Assert.assertEquals( rowNum, 0);
 }

	 public void deleteAllSubcatsFromDB() {
		 	String[] arr2 = validateCategoriesfromDB();
		 
		 	Set<String> mySet = new HashSet<String>(Arrays.asList(arr2));
		 	String arr1 = Arrays.toString((mySet.toArray())).replaceAll("[\\[\\](){}]","");
		 	String[] catIds = arr1.split(",");
		 
		 	System.out.println("Cat Ids: "+ Arrays.toString(catIds));
		 	
		 	String catIds1 = Arrays.toString(catIds).toString();
	 		String result = catIds1.substring(1,catIds1.length()-1);
	
	 		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
	 		sqlTemplate.execute(CmsConstants.deleteCatSubcat_query+"("+result+")");
	 		
	 		 List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.getMultipleCatSubcat_query+"("+result+")");
	 		 int rowNum = list.size();
	 		 Assert.assertEquals( rowNum, 0);
}
	
	 public void deleteAllCatsFromDB() {
		 	String[] arr2 = getMenuIdsHelper();
		 
		 	Set<String> mySet = new HashSet<String>(Arrays.asList(arr2));
		 	String arr1 = Arrays.toString((mySet.toArray())).replaceAll("[\\[\\](){}]","");
		 	String[] menuIds = arr1.split(",");
		 
		 	System.out.println("Menu Ids: "+ Arrays.toString(menuIds));
		 	
		 	String menuIds1 = Arrays.toString(menuIds).toString();
	 		String result = menuIds1.substring(1,menuIds1.length()-1);
	
	 		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
	 		sqlTemplate.execute(CmsConstants.deleteCatSubcat_query+"("+result+")");
	 		
	 		 List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.getMultipleCatSubcat_query+"("+result+")");
	 		 int rowNum = list.size();
	 		 Assert.assertEquals( rowNum, 0);
}
	 
	 
	 public void deleteFileDetailsFromDB(String fileId) {
	        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
	        sqlTemplate.execute(CmsConstants.deleteFileId_query+ fileId);
	        
	        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.getFileId_query+ fileId);
	        int rowNum = list.size();
	        Assert.assertEquals( rowNum, 0);
	       
	        System.out.println("FILE_ID: "+fileId + " successfully deleted from DB");
	    }
	 

	public Processor getFileByFileId(String fileId) throws JSONException {
		GameOfThronesService service1 = new GameOfThronesService("cmsgrasshopper", "getfilebyfileid", gameofthrones);
		Processor p = new Processor(service1, 
				 					setHeaders1(), 
				 					new String[]{""}, 
				 					new String[] {fileId});	
		 return p;	
	}
	
	
	public Processor getFileByUserId(String userId) throws JSONException {
		GameOfThronesService service1 = new GameOfThronesService("cmsgrasshopper", "getfilebyuserid", gameofthrones);
		 Processor p = new Processor(service1, 
				 					setHeaders1(), 
				 					new String[]{""}, 
				 					new String[] {userId});	
		 return p;	
	}

}
