package com.swiggy.api.erp.delivery.dp;

import java.io.IOException;

import org.testng.annotations.DataProvider;

import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.erp.delivery.constants.RTSConstants;
import com.swiggy.api.erp.delivery.helper.DeliveryHelperMethods;

public class LastMileandPrepTimePredicitionDataProvider {
	DeliveryHelperMethods delmeth=new DeliveryHelperMethods();
	@DataProvider(name = "LMPT_1")
	public Object[][] LMPT1() throws IOException, InterruptedException {

		delmeth.dbhelperupdate(RTSConstants.setlastmiletimepredict_false,  DeliveryConstant.databaseName);
		delmeth.dbhelperupdate(RTSConstants.setpreptimepredict_false,  DeliveryConstant.databaseName);
		delmeth.dbhelperupdate(RTSConstants.set_prep_time_predictions_zone_disable,  DeliveryConstant.databaseName);
		delmeth.dbhelperupdate(RTSConstants.set_last_mile_prediction_zone_disable,  DeliveryConstant.databaseName);
		return new Object[][] { {} };
	}
	@DataProvider(name = "LMPT_2")
	public Object[][] LMPT2() throws IOException, InterruptedException {

		delmeth.dbhelperupdate(RTSConstants.setlastmiletimepredict_true,  DeliveryConstant.databaseName);
		delmeth.dbhelperupdate(RTSConstants.setpreptimepredict_false,  DeliveryConstant.databaseName);
		delmeth.dbhelperupdate(RTSConstants.set_last_mile_prediction_zone_enable,  DeliveryConstant.databaseName);
		delmeth.dbhelperupdate(RTSConstants.set_prep_time_predictions_zone_disable,  DeliveryConstant.databaseName);
		return new Object[][] { {} };
	}
	@DataProvider(name = "LMPT_3")
	public Object[][] LMPT3() throws IOException, InterruptedException {

		delmeth.dbhelperupdate(RTSConstants.setlastmiletimepredict_false,  DeliveryConstant.databaseName);
		delmeth.dbhelperupdate(RTSConstants.setpreptimepredict_true,  DeliveryConstant.databaseName);
		delmeth.dbhelperupdate(RTSConstants.set_last_mile_prediction_zone_disable,  DeliveryConstant.databaseName);
		delmeth.dbhelperupdate(RTSConstants.set_prep_time_predictions_zone_enable,  DeliveryConstant.databaseName);
		return new Object[][] { {} };
	}
	@DataProvider(name = "LMPT_4")
	public Object[][] LMPT4() throws IOException, InterruptedException {

		delmeth.dbhelperupdate(RTSConstants.setlastmiletimepredict_true,  DeliveryConstant.databaseName);
		delmeth.dbhelperupdate(RTSConstants.setpreptimepredict_true,  DeliveryConstant.databaseName);
		delmeth.dbhelperupdate(RTSConstants.set_last_mile_prediction_zone_enable,  DeliveryConstant.databaseName);
		delmeth.dbhelperupdate(RTSConstants.set_prep_time_predictions_zone_enable,  DeliveryConstant.databaseName);
		return new Object[][] { {} };
	}
	@DataProvider(name = "LMPT_5")
	public Object[][] LMPT5() throws IOException, InterruptedException {

		delmeth.dbhelperupdate(RTSConstants.setlastmiletimepredict_true,  DeliveryConstant.databaseName);
		delmeth.dbhelperupdate(RTSConstants.setpreptimepredict_true,  DeliveryConstant.databaseName);
		Thread.sleep(10000);		
		return new Object[][] { {} };
	}
	@DataProvider(name = "LMPT_6")
	public Object[][] LMPT6() throws IOException, InterruptedException {

		delmeth.dbhelperupdate(RTSConstants.setlastmiletimepredict_true,  DeliveryConstant.databaseName);
		delmeth.dbhelperupdate(RTSConstants.setpreptimepredict_true,  DeliveryConstant.databaseName);
		Thread.sleep(10000);		
		return new Object[][] { {} };
	}
	@DataProvider(name = "LMPT_7")
	public Object[][] LMPT7() throws IOException, InterruptedException {

		delmeth.dbhelperupdate(RTSConstants.setlastmiletimepredict_true,  DeliveryConstant.databaseName);
		delmeth.dbhelperupdate(RTSConstants.setpreptimepredict_true,  DeliveryConstant.databaseName);
		delmeth.dbhelperupdate(RTSConstants.set_max_Last_mile_more,  DeliveryConstant.databaseName);
		Thread.sleep(10000);		
		return new Object[][] { {} };
	}
	@DataProvider(name = "LMPT_8")
	public Object[][] LMPT8() throws IOException, InterruptedException {

		delmeth.dbhelperupdate(RTSConstants.setlastmiletimepredict_true,  DeliveryConstant.databaseName);
		delmeth.dbhelperupdate(RTSConstants.setpreptimepredict_true,  DeliveryConstant.databaseName);
		delmeth.dbhelperupdate(RTSConstants.set_max_Last_mile_less,  DeliveryConstant.databaseName);
		Thread.sleep(10000);		
		return new Object[][] { {} };
	}
	@DataProvider(name = "LMPT_9")
	public Object[][] LMPT9() throws IOException, InterruptedException {

		delmeth.dbhelperupdate(RTSConstants.setlastmiletimepredict_true,  DeliveryConstant.databaseName);
		delmeth.dbhelperupdate(RTSConstants.setpreptimepredict_true,  DeliveryConstant.databaseName);
		delmeth.dbhelperupdate(RTSConstants.set_max_prep_time_more,  DeliveryConstant.databaseName);
		Thread.sleep(10000);		
		return new Object[][] { {} };
	}
	@DataProvider(name = "LMPT_10")
	public Object[][] LMPT10() throws IOException, InterruptedException {

		delmeth.dbhelperupdate(RTSConstants.setlastmiletimepredict_true,  DeliveryConstant.databaseName);
		delmeth.dbhelperupdate(RTSConstants.setpreptimepredict_true,  DeliveryConstant.databaseName);
		delmeth.dbhelperupdate(RTSConstants.set_max_prep_time_less,  DeliveryConstant.databaseName);
		Thread.sleep(10000);		
		return new Object[][] { {} };
	}
}