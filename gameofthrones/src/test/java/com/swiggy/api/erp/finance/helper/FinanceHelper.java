package com.swiggy.api.erp.finance.helper;

import com.rabbitmq.client.AMQP;
import com.swiggy.api.erp.cms.constants.CmsConstants;
import com.swiggy.api.erp.cms.helper.RestaurantHelper;
import com.swiggy.api.erp.cms.pojo.Restaurant;
import com.swiggy.api.erp.ff.constants.LosConstants;
import com.swiggy.api.erp.finance.constants.FinanceConstants;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.DateHelper;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.DBHelper;
import framework.gameofthrones.Tyrion.RabbitMQHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;
import org.apache.commons.io.FileUtils;
import org.testng.Assert;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;

public class FinanceHelper implements FinanceConstants {

    RabbitMQHelper rmqHelper = new RabbitMQHelper();
    public static Initialize gameofthrones = new Initialize();
    public static RestaurantHelper restaurantHelper = new RestaurantHelper();
    DateHelper dateHelper = new DateHelper();

    public void assertReconOrderAndStatus(String orderId,String orderstatus) throws Exception{
        GameOfThronesService reconOrder = new GameOfThronesService("fin","getReconOrder",gameofthrones);
        HashMap<String,String> headers = new HashMap<String, String>();
        headers.put("Content-Type","application/json");
        Processor reconOrderresponse = new Processor(reconOrder,headers,null,new String[]{orderId});
        Assert.assertEquals(reconOrderresponse.ResponseValidator.GetNodeValue("order_id"),orderId);
        Assert.assertEquals(reconOrderresponse.ResponseValidator.GetNodeValue("order_status"),orderstatus);
    }

    public HashMap<String, String> getDefaultHeaders() {
        HashMap<String, String> header = new HashMap<String, String>();
        header.put("Content-Type", "application/json");
        return header;
    }

    public String createOrderFromSelectedRestaurant(String restType) throws IOException {
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat order_idFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        String orderId = order_idFormat.format(date);
        String orderTime = dateFormat.format(date);
        System.out.println("Order time = " + orderTime);
        String epochTime = String.valueOf(Instant.now().getEpochSecond());
        System.out.println("Epoch time = " + epochTime);

        String restId = null;
        if(restType.equals("newmouprepaid")){
            restId= FinanceConstants.rest_id_prepaid_newmou;
        }
        if(restType.equals("newmoupostpaid")){
            restId = FinanceConstants.rest_id_postpaid_newmou;
        }
        if(restType.equals("oldmouprepaid")){
            restId = FinanceConstants.rest_id_prepaid_oldmou;
        }
        if(restType.equals("oldmoupostpaid")){
            restId = FinanceConstants.rest_id_postpaid_oldmou;

        }

        File file = new File("../Data/Payloads/JSON/fin_createOrder.json");
        String createOrder = FileUtils.readFileToString(file);
        createOrder = createOrder.replace("${orderID}", orderId).replace("${order_time}", orderTime).replace("${ordered_time_in_seconds}", epochTime).replace("${restaurant_id}", restId);
        rmqHelper.pushMessageToExchange(FinanceConstants.hostName, FinanceConstants.createOrdersQueue, new AMQP.BasicProperties().builder().contentType("application/json"), createOrder);
        System.out.println("Order Id ----- " + orderId);
        return orderId;
    }



    public String getCurrentEpochtime(){
        String epochTime = String.valueOf(Instant.now().getEpochSecond());
        System.out.println("Epoch time = " + epochTime);
        return epochTime;
    }

    public String cancelOrderTests(String orderId, String foodPrepared, String responsibleId) throws IOException {

        String cancellationTime = String.valueOf(dateHelper.getCurrentEpochTimeInMillis());
        File file = new File("../Data/Payloads/JSON/fin_cancellation.json");
        String cancelOrder = FileUtils.readFileToString(file);
        cancelOrder = cancelOrder.replace("${orderID}", orderId).replace("${responsibleId}", responsibleId).replace("${foodPrep}", foodPrepared).replace("${cancellationTime}",cancellationTime);
        rmqHelper.pushMessageToExchange(FinanceConstants.hostName, FinanceConstants.cancelOrdersQueue, new AMQP.BasicProperties().builder().contentType("application/json"), cancelOrder);
        System.out.println("Order Id ----- " + orderId);
        return cancelOrder;
    }

    public boolean validateOrderAvailableInReconRawOrderFromDB(String orderID) {
        List<Map<String, Object>> list;
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(FinanceConstants.finDB);
        String q = FinanceConstants.get_order_details_from_recon_raw_orders + orderID;
        System.out.println("--Query ::: " + q);
        list = sqlTemplate.queryForList(q);
        if (list.size() == 0)
            return false;
        else {
            String restID = list.get(0).get("restaurant_id").toString();
            System.out.println("--ORDERID ::: "+orderID+" , --RESTID ::: "+restID);
            return true;
        }
    }

    public boolean validateOrderEntryInReconDeDmEntriesFromDB(String orderID) {
        List<Map<String, Object>> list;
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(FinanceConstants.finDB);
        String q = FinanceConstants.get_details_from_recon_de_dm_entries + orderID;
        System.out.println("--Query ::: " + q);
        list = sqlTemplate.queryForList(q);
        if (list.size() == 0)
            return false;
        else {
            System.out.println("--ORDERID ::: "+orderID);
            return true;
        }
    }

    public boolean validateOrderAvailableInReconRawOrderDetailsFromDB(String orderID) {
        List<Map<String, Object>> list;
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(FinanceConstants.finDB);
        String q = FinanceConstants.get_order_details_from_recon_raw_order_details + orderID;
        System.out.println("--Query ::: " + q);
        list = sqlTemplate.queryForList(q);
        if (list.size() == 0)
            return false;
        else {
            System.out.println("--ORDERID ::: "+orderID);
            return true;
        }
    }

    public boolean validateCashReconStatus(String orderID) {
        List<Map<String, Object>> list;
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(FinanceConstants.finDB);
        String q = FinanceConstants.get_recon_reconciliation_status + orderID;
        System.out.println("--Query ::: " + q);
        list = sqlTemplate.queryForList(q);
        String cashreconstatus = list.get(0).get("cash_recon_status").toString();
        if (cashreconstatus.equalsIgnoreCase("false"))
            return false;
        else {

            return true;
        }
    }
    public boolean validateBillReconStatus(String orderID) {
        List<Map<String, Object>> list;
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(FinanceConstants.finDB);
        String q = FinanceConstants.get_recon_reconciliation_status + orderID;
        System.out.println("--Query ::: " + q);
        list = sqlTemplate.queryForList(q);
        String billreconstatus = list.get(0).get("bill_recon_status").toString();
        if (billreconstatus.equalsIgnoreCase("false"))
            return false;
        else {

            return true;
        }
    }
    public String getAutoReconStatusFromDB(String orderID) {
        List<Map<String, Object>> list;
        String autoreconstatus = "";
        String q = FinanceConstants.get_order_details_from_recon_raw_orders + orderID;
        boolean status = DBHelper.pollDB(FinanceConstants.finDB,q,"order_id",orderID,2,90);
        if (status) {
            String q1 = get_auto_recon_status + orderID;
            SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(FinanceConstants.finDB);
            System.out.println("--Query ::: " + q1);
            list = sqlTemplate.queryForList(q1);
            autoreconstatus = list.get(0).get("auto_recon_status").toString();
        } else {
            System.out.println("--Could not find the order ID ::: "+orderID);
            autoreconstatus = "-1";
        }
        return autoreconstatus;
    }


    public boolean getReconciliationStatusFromDB (String orderID) {
        String reconciliationStatus = "";
        List<Map<String, Object>> list;
        String q = FinanceConstants.get_recon_reconciliation_status + orderID;
        System.out.println("--Query ::: " + q);
        boolean status = DBHelper.pollDB(FinanceConstants.finDB,q,"order_id",orderID,2,90);
        if (status) {
            String q1 = get_recon_reconciliation_status + orderID;
            SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(FinanceConstants.finDB);
            System.out.println("--Query ::: " + q1);
            list = sqlTemplate.queryForList(q1);
            reconciliationStatus = list.get(0).get("reconciliation_status").toString();
            return Boolean.parseBoolean(reconciliationStatus);
        } else {
            System.out.println("--Could not find reconciliation_status for order ID ::: "+orderID);
            return false;
        }
    }

    public boolean validateOrderAvailableInReconOrderActualsFromDB (String orderID) {
        String q = FinanceConstants.get_recon_order_actuals + orderID;
        System.out.println("--Query ::: " + q);
        boolean status = DBHelper.pollDB(FinanceConstants.finDB,q,"order_id",orderID,2,60);
        return status;
    }

    public boolean validateEntryInRestuarantCurrentPayoutDetailsFromDB(String payoutID) {
        List<Map<String, Object>> list;
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(FinanceConstants.finDB);
        String q = FinanceConstants.get_details_restaurant_current_payout_details +payoutID;
        System.out.println("--Query ::: " + q);
        list = sqlTemplate.queryForList(q);
        return list.size() != 0;
    }

    public boolean validateEntryInRestuarantCurrentPayoutFromDB (String taskID) {
        List<Map<String, Object>> list;
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(FinanceConstants.finDB);
        String q = FinanceConstants.get_details_restaurant_current_payout +taskID;
        System.out.println("--Query ::: " + q);
        list = sqlTemplate.queryForList(q);
        return list.size() != 0;
    }

    public String validateStatusFromReconCurrentPayoutJobsFromDB(String id) {
        List<Map<String, Object>> list;
        String status;
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(FinanceConstants.finDB);
        String q = FinanceConstants.get_details_recon_current_payout_jobs +id;
        System.out.println("--Query ::: " + q);
        list = sqlTemplate.queryForList(q);
        if(!(list.size() == 0))
            status = list.get(0).get("status").toString();
        else
            status = "-1";
        return status;
    }

    public HashMap<String,String> getCommissionAndRestaurantEarningReconOrderActualsFromDB(String orderID) {
        HashMap<String, String> hm = new HashMap<>();
        List<Map<String, Object>> list;
        String q = FinanceConstants.get_recon_order_actuals + orderID;
        System.out.println("--Query ::: " + q);
        boolean status = DBHelper.pollDB(FinanceConstants.finDB,q,"order_id",orderID,2,60);
        if (status) {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            String q1 = FinanceConstants.get_commission_rest_earnings + orderID;
            SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(FinanceConstants.finDB);
            System.out.println("--Query ::: " + q1);
            list = sqlTemplate.queryForList(q1);
            System.out.println("--commission ::: "+list.get(0).get("commission").toString());
            System.out.println("--restaurant_earnings ::: "+ list.get(0).get("restaurant_earnings").toString());
            hm.put("commission",list.get(0).get("commission").toString());
            hm.put("restaurant_earnings",list.get(0).get("restaurant_earnings").toString());
        }
        return hm;
    }

    public HashMap<String, String> getCommissionAndRestaurantEarningMasterFromDB(String orderID) {
        HashMap<String, String> hm = new HashMap<>();
        List<Map<String, Object>> list;
        String q = FinanceConstants.get_details_from_master + orderID;
        System.out.println("--Query ::: " + q);
        boolean status = DBHelper.pollDB(FinanceConstants.finDB,q,"order_id",orderID,2,60);
        if (status) {
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            String q1 = FinanceConstants.get_details_from_master + orderID;
            SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(FinanceConstants.finDB);
            System.out.println("--Query ::: " + q1);
            list = sqlTemplate.queryForList(q1);
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            hm.put("swiggy_commission",list.get(0).get("swiggy_commission").toString());
            hm.put("restaurant_earnings",list.get(0).get("restaurant_earnings").toString());
        }
        return hm;
    }

    public HashMap<String, String> getCommissionAndRestaurantEarningOrdersDB(String orderID) {
        HashMap<String, String> hm = new HashMap<>();
        List<Map<String, Object>> list;
        String q = FinanceConstants.get_details_from_orders + orderID;
        System.out.println("--Query ::: " + q);
        boolean status = DBHelper.pollDB(FinanceConstants.finDB,q,"order_id",orderID,2,60);
        if (status) {
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            String q1 = FinanceConstants.get_details_from_orders + orderID;
            SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(FinanceConstants.finDB);
            System.out.println("--Query ::: " + q1);
            list = sqlTemplate.queryForList(q1);
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            hm.put("swiggy_commission",list.get(0).get("swiggy_commission").toString());
            hm.put("rest_earnings",list.get(0).get("rest_earnings").toString());
            hm.put("nodal_rest_earnings",list.get(0).get("nodal_rest_earnings").toString());

        }
        return hm;
    }
    public String createRestaurantOfGivenAgreementType (String agreementType) {
        Restaurant restaurant = new Restaurant();
        restaurant.build();
        restaurant.setAgreement_type(agreementType);
        Processor processor = restaurantHelper.createRestaurant(restaurant);
       if(processor.ResponseValidator.GetNodeValueAsInt(CmsConstants.code) == CmsConstants.rest_status_code) {
            int restId = processor.ResponseValidator.GetNodeValueAsInt(CmsConstants.swiggy_id);
            return String.valueOf(restId);
        }
        System.out.println("--RestID creation did not work properly ::: ");
        return FinanceConstants.rest_id_prepaid_oldmou;
    }

    public String createManualOrderForGivenRestaurantOfGivenAgreementType(String orderId, String orderTime, String epochTime, String restID, String rest_agreement_type_string, String rest_agreement_type) throws IOException {
        File file = new File("../Data/Payloads/JSON/losCreateManualOrderForGivenRestAndAgreementType.json");
        String createOrder = FileUtils.readFileToString(file);
        createOrder = createOrder.replace("${orderID}", orderId).replace("${order_time}",orderTime).replace("${ordered_time_in_seconds}", epochTime).replace("${agreement_type_string}",rest_agreement_type_string).replace("${agreement_type}", rest_agreement_type).replace("${restaurant_id}",restID);
        rmqHelper.pushMessageToExchange(LosConstants.hostName, LosConstants.createOrdersQueue, new AMQP.BasicProperties().builder().contentType("application/json"), createOrder);
        System.out.println("Order Id ----- "+ orderId);
        return createOrder;
    }
    public String createManualOrderForGivenRestaurantOfGivenAgreementType(String orderId, String orderTime, String epochTime, String restID, String rest_agreement_type_string, String rest_agreement_type, String replicate,String bill) throws IOException {
        File file = new File("../Data/Payloads/JSON/losCreateManualOrderForGivenRestAndAgreementType.json");
        String createOrder = FileUtils.readFileToString(file);       
        createOrder = createOrder.replace("${orderID}", orderId).replace("${order_time}",orderTime).replace("${ordered_time_in_seconds}", epochTime).replace("${agreement_type_string}",rest_agreement_type_string).replace("${agreement_type}", rest_agreement_type).replace("${restaurant_id}",restID).replace("${replicate}",replicate).replace("${bill}",bill);
        rmqHelper.pushMessageToExchange(LosConstants.hostName, LosConstants.createOrdersQueue, new AMQP.BasicProperties().builder().contentType("application/json"), createOrder);
        System.out.println("Order Id ----- "+ orderId);
        return createOrder;
    }
    public String createOtherOrder(String orderId, String orderTime, String epochTime, String restID, String rest_agreement_type_string, String rest_agreement_type, String filePath) throws IOException {
        File file = new File(filePath);
        String createOrder = FileUtils.readFileToString(file);
        createOrder = createOrder.replace("${orderID}", orderId).replace("${order_time}",orderTime).replace("${ordered_time_in_seconds}", epochTime).replace("${agreement_type_string}",rest_agreement_type_string).replace("${agreement_type}", rest_agreement_type).replace("${restaurant_id}",restID);
        rmqHelper.pushMessageToExchange(LosConstants.hostName, LosConstants.createOrdersQueue, new AMQP.BasicProperties().builder().contentType("application/json"), createOrder);
        System.out.println("Order Id ----- "+ orderId);
        return createOrder;
    }


    public String createOtherOrderFin(String orderId, String orderTime, String epochTime, String restID, String rest_agreement_type_string, String rest_agreement_type,String bill_exp,String fd_share_type,String fd_share_value,String levy,String filePath) throws IOException, InterruptedException {
        File file = new File(filePath);
        String createOrder = FileUtils.readFileToString(file);
        createOrder = createOrder.replace("${orderID}", orderId).replace("${order_time}",orderTime).replace("${ordered_time_in_seconds}", epochTime).replace("${agreement_type_string}",rest_agreement_type_string).replace("${agreement_type}", rest_agreement_type).replace("${restaurant_id}",restID).replace("${bill}",bill_exp).replace("${fd_share_type}",fd_share_type).replace("${fd_share_value}",fd_share_value).replace("${levy}",levy);
        rmqHelper.pushMessageToExchange(LosConstants.hostName, LosConstants.createOrdersQueue, new AMQP.BasicProperties().builder().contentType("application/json"), createOrder);
        System.out.println("Order Id ----- "+ orderId);
        return createOrder;
    }

    public String createOtherOrderFinPOPSuperNormal(String orderId, String orderTime, String epochTime, String restID, String rest_agreement_type_string, String rest_agreement_type,String bill_exp,String filePath) throws IOException, InterruptedException {
        File file = new File(filePath);
        String createOrder = FileUtils.readFileToString(file);
        createOrder = createOrder.replace("${orderID}", orderId).replace("${order_time}", orderTime).replace("${ordered_time_in_seconds}", epochTime).replace("${agreement_type_string}", rest_agreement_type_string).replace("${agreement_type}", rest_agreement_type).replace("${restaurant_id}", restID).replace("${bill}", bill_exp);
        rmqHelper.pushMessageToExchange(LosConstants.hostName, LosConstants.createOrdersQueue, new AMQP.BasicProperties().builder().contentType("application/json"), createOrder);
        System.out.println("Order Id ----- " + orderId);
        return createOrder;

    }

    public String createZombieOrder(String orderId, String orderTime, String epochTime, String restID, String rest_agreement_type_string, String rest_agreement_type,String bill_exp,String filePath) throws IOException, InterruptedException {
        File file = new File(filePath);
        String createOrder = FileUtils.readFileToString(file);
        createOrder = createOrder.replace("${orderID}", orderId).replace("${order_time}", orderTime).replace("${ordered_time_in_seconds}", epochTime).replace("${agreement_type_string}", rest_agreement_type_string).replace("${agreement_type}", rest_agreement_type).replace("${restaurant_id}", restID).replace("${bill}", bill_exp);
        rmqHelper.pushMessageToExchange(LosConstants.hostName, LosConstants.createOrdersQueue, new AMQP.BasicProperties().builder().contentType("application/json"), createOrder);
        //Thread.sleep(5000);
        System.out.println("Order Id ----- " + orderId);
        return createOrder;

    }


    public String createOnlineOrder(String orderId, String orderTime, String epochTime, String restID, String rest_agreement_type_string, String rest_agreement_type, String filePath, String bill) throws IOException {
        File file = new File(filePath);
        String createOrder = FileUtils.readFileToString(file);
        createOrder = createOrder.replace("${orderID}", orderId).replace("${order_time}",orderTime).replace("${ordered_time_in_seconds}", epochTime).replace("${agreement_type_string}",rest_agreement_type_string).replace("${agreement_type}", rest_agreement_type).replace("${restaurant_id}",restID).replace("${bill}",bill);
        rmqHelper.pushMessageToExchange(LosConstants.hostName, LosConstants.createOrdersQueue, new AMQP.BasicProperties().builder().contentType("application/json"), createOrder);
        System.out.println("Order Id ----- "+ orderId);
        return createOrder;
    }
    
    public String createReplicatedOrder(String orderId,String orderId2, String filePath) throws IOException {
        File file = new File(filePath);
        String createOrder = FileUtils.readFileToString(file);
        createOrder = createOrder.replace("${orderID}", orderId).replace("${orderID2}", orderId2);
        rmqHelper.pushMessageToExchange(LosConstants.hostName, "swiggy.reconcile.igcc_info", new AMQP.BasicProperties().builder().contentType("application/json"), createOrder);
        System.out.println("Order Id ----- "+ orderId);
        return createOrder;
    }

    public String getAgreementTypeFromAgreementTypeString(String agreementTypeString) {
        switch (agreementTypeString) {
            case "Postpaid":
                return String.valueOf(1);
            //TODO: FIll in the value for this
            case "Prepaid":
                return String.valueOf(0);
            default:
                return "";
        }
    }


    public String[] createOrderOfAGivenType (String restID,String agreementType) {
        String[] id_time = new String[2];
        String get_orderID_val = dateHelper.getCurrentDateTimeIn_yyyyMMddHHmmss();
        String epochTime = String.valueOf(dateHelper.getCurrentEpochTimeInSeconds());
        String orderTime = String.valueOf(dateHelper.getCurrentDateTimeIn_yyyy_mm_dd_hh_mm_ss());
        Random random = new Random();
        int x = random.nextInt(9) + 1;
        get_orderID_val = get_orderID_val + String.valueOf(x);
        String orderID = "";
        try {
            orderID = createManualOrderForGivenRestaurantOfGivenAgreementType(get_orderID_val,orderTime,epochTime,restID,agreementType,getAgreementTypeFromAgreementTypeString(agreementType));
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("--ORDERID for agreeementType: "+agreementType+ " is:"+get_orderID_val);
        id_time[0] = get_orderID_val;
        id_time[1] = orderTime;
        return id_time;
    }


    public String[] createOrderOfAGivenType (String restID,String agreementType, String replicate, String bill) {
        String[] id_time = new String[2];
        String get_orderID_val = dateHelper.getCurrentDateTimeIn_yyyyMMddHHmmss();
        String epochTime = String.valueOf(dateHelper.getCurrentEpochTimeInSeconds());
        String orderTime = String.valueOf(dateHelper.getCurrentDateTimeIn_yyyy_mm_dd_hh_mm_ss());
        Random random = new Random();
        int x = random.nextInt(9) + 1;
        get_orderID_val = get_orderID_val + String.valueOf(x);
        String orderID = "";
        try {
            orderID = createManualOrderForGivenRestaurantOfGivenAgreementType(get_orderID_val,orderTime,epochTime,restID,agreementType,getAgreementTypeFromAgreementTypeString(agreementType), replicate,bill);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("--ORDERID for agreeementType: "+agreementType+ " is:"+get_orderID_val);
        id_time[0] = get_orderID_val;
        id_time[1] = orderTime;
        return id_time;
    }
    

    public String[] createOtherOrders (String restID,String agreementType, String filePath) {
        String[] id_time = new String[2];
        String get_orderID_val = dateHelper.getCurrentDateTimeIn_yyyyMMddHHmmss();
        String epochTime = String.valueOf(dateHelper.getCurrentEpochTimeInSeconds());
        String orderTime = String.valueOf(dateHelper.getCurrentDateTimeIn_yyyy_mm_dd_hh_mm_ss());
        Random random = new Random();
        int x = random.nextInt(9) + 1;
        get_orderID_val = get_orderID_val + String.valueOf(x);
        String orderID = "";
        try {
            orderID = createOtherOrder(get_orderID_val,orderTime,epochTime,restID,agreementType,getAgreementTypeFromAgreementTypeString(agreementType), filePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("--ORDERID for agreeementType: "+agreementType+ " is:"+get_orderID_val);
        id_time[0] = get_orderID_val;
        id_time[1] = orderTime;
        return id_time;
    }

    public String[] createOrderFinanceHelper(String restID,String agreementType,String bill_exp,String fd_share_type,String fd_share_value,String levy,String filePath) throws IOException, InterruptedException {
        String[] id_time = new String[2];
        String get_orderID_val = dateHelper.getCurrentDateTimeIn_yyyyMMddHHmmss();
        String epochTime = String.valueOf(dateHelper.getCurrentEpochTimeInSeconds());
        String orderTime = String.valueOf(dateHelper.getCurrentDateTimeIn_yyyy_mm_dd_hh_mm_ss());
        Random random = new Random();
        int x = random.nextInt(9) + 1;
        get_orderID_val = get_orderID_val + String.valueOf(x);
        String orderID = "";
        try {
            orderID = createOtherOrderFin(get_orderID_val,orderTime,epochTime,restID,agreementType,getAgreementTypeFromAgreementTypeString(agreementType),bill_exp,fd_share_type,fd_share_value,levy, filePath);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
            Thread.sleep(1000);
            pushPlacedJson(get_orderID_val, FinanceConstants.placed_json);
            Thread.sleep(500);

            pushPickeupDeliverjson(get_orderID_val, FinanceConstants.pickedup_json);
            Thread.sleep(500);
            pushPickeupDeliverjson(get_orderID_val, FinanceConstants.delivered_json);
            Thread.sleep(500);

        System.out.println("--ORDERID for agreeementType: "+agreementType+ " is:"+get_orderID_val);
        id_time[0] = get_orderID_val;
        id_time[1] = orderTime;
        return id_time;

    }

    public String[] createAndCancelOrderFinanceHelper(String restID,String agreementType,String bill_exp,String fd_share_type,String fd_share_value,String levy,String filePath,String rule) throws IOException, InterruptedException {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(FinanceConstants.finDB);

        if (rule.equalsIgnoreCase(FinanceConstants.rule15)) {
            String q1 = FinanceConstants.updateMfrEnabledWithoutFallback + restID;
            System.out.println("--Query ::: " + q1);
            sqlTemplate.update(q1);
        }
         if (rule.equalsIgnoreCase(FinanceConstants.rule16)) {
             String q1 = FinanceConstants.updateMfrEnabledWithFallbackNewMou + restID;
             System.out.println("--Query ::: " + q1);
             sqlTemplate.update(q1);
         }
        if (rule.equalsIgnoreCase(FinanceConstants.rule17)) {
            String q1 = FinanceConstants.updateMfrEnabledWithFallbackOldMou + restID;
            System.out.println("--Query ::: " + q1);
            sqlTemplate.update(q1);
        }
            String[] id_time = new String[2];
            String get_orderID_val = dateHelper.getCurrentDateTimeIn_yyyyMMddHHmmss();
            String epochTime = String.valueOf(dateHelper.getCurrentEpochTimeInSeconds());
            String orderTime = String.valueOf(dateHelper.getCurrentDateTimeIn_yyyy_mm_dd_hh_mm_ss());
            Random random = new Random();
            int x = random.nextInt(9) + 1;
            get_orderID_val = get_orderID_val + String.valueOf(x);
            String orderID = "";
            try {
                orderID = createOtherOrderFin(get_orderID_val, orderTime, epochTime, restID, agreementType, getAgreementTypeFromAgreementTypeString(agreementType), bill_exp, fd_share_type, fd_share_value, levy, filePath);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            switch (rule) {

                case FinanceConstants.rule1:
                    pushPlacedJson(get_orderID_val, FinanceConstants.placed_json);
                    Thread.sleep(500);
                    cancelOrderTests(get_orderID_val, "true", "1");
                    break;
                case FinanceConstants.rule2:
                    pushPlacedJson(get_orderID_val, FinanceConstants.placed_json);
                    Thread.sleep(500);
                    cancelOrderTests(get_orderID_val, "true", "2");
                    break;
                case FinanceConstants.rule3:
                    pushPlacedJson(get_orderID_val, FinanceConstants.placed_json);
                    Thread.sleep(500);
                    cancelOrderTests(get_orderID_val, "true", "3");
                    break;
                case FinanceConstants.rule4:
                    pushPlacedJson(get_orderID_val, FinanceConstants.placed_json);
                    Thread.sleep(500);
                    pushPickeupDeliverjson(get_orderID_val, FinanceConstants.pickedup_json);
                    Thread.sleep(500);
                    cancelOrderTests(get_orderID_val, "true", "1");
                    break;
                case FinanceConstants.rule5:
                    pushPlacedJson(get_orderID_val, FinanceConstants.placed_json);
                    Thread.sleep(500);
                    pushPickeupDeliverjson(get_orderID_val, FinanceConstants.pickedup_json);
                    Thread.sleep(500);
                    cancelOrderTests(get_orderID_val, "true", "2");
                    break;
                case FinanceConstants.rule6:
                    pushPlacedJson(get_orderID_val, FinanceConstants.placed_json);
                    Thread.sleep(500);
                    pushPickeupDeliverjson(get_orderID_val, FinanceConstants.pickedup_json);
                    Thread.sleep(500);
                    cancelOrderTests(get_orderID_val, "true", "3");
                    break;
                case FinanceConstants.rule7:
                    pushPlacedJson(get_orderID_val, FinanceConstants.placed_json);
                    Thread.sleep(500);
                    cancelOrderTests(get_orderID_val, "false", "1");
                    break;
                case FinanceConstants.rule8:
                    pushPlacedJson(get_orderID_val, FinanceConstants.placed_json);
                    Thread.sleep(500);
                    cancelOrderTests(get_orderID_val, "false", "2");
                    break;
                case FinanceConstants.rule9:
                    pushPlacedJson(get_orderID_val, FinanceConstants.placed_json);
                    Thread.sleep(500);
                    cancelOrderTests(get_orderID_val, "false", "3");
                    break;
                case FinanceConstants.rule10:
                    pushPlacedJson(get_orderID_val, FinanceConstants.placed_json);
                    Thread.sleep(500);
                    pushPickeupDeliverjson(get_orderID_val, FinanceConstants.pickedup_json);
                    Thread.sleep(500);
                    cancelOrderTests(get_orderID_val, "false", "1");
                    break;
                case FinanceConstants.rule11:
                    pushPlacedJson(get_orderID_val, FinanceConstants.placed_json);
                    Thread.sleep(500);
                    pushPickeupDeliverjson(get_orderID_val, FinanceConstants.pickedup_json);
                    Thread.sleep(500);
                    cancelOrderTests(get_orderID_val, "false", "2");
                    break;
                case FinanceConstants.rule12:
                    pushPlacedJson(get_orderID_val, FinanceConstants.placed_json);
                    Thread.sleep(500);
                    pushPickeupDeliverjson(get_orderID_val, FinanceConstants.pickedup_json);
                    Thread.sleep(500);
                    cancelOrderTests(get_orderID_val, "false", "3");
                case FinanceConstants.rule13:
                    pushPickeupDeliverjson(get_orderID_val, FinanceConstants.pickedup_json);
                    Thread.sleep(500);
                    cancelOrderTests(get_orderID_val, "false", "3");
                    break;
                case FinanceConstants.rule14:
                    cancelOrderTests(get_orderID_val, "false", "3");
                    break;
                case FinanceConstants.rule15:
                    pushPlacedJson(get_orderID_val, FinanceConstants.placed_json);
                    Thread.sleep(500);
                    pushPickeupDeliverjson(get_orderID_val, FinanceConstants.pickedup_json);
                    Thread.sleep(500);
                    cancelOrderTests(get_orderID_val, "true", "3");
                    break;
                case FinanceConstants.rule16:
                    pushPlacedJson(get_orderID_val, FinanceConstants.placed_json);
                    Thread.sleep(500);
                    pushPickeupDeliverjson(get_orderID_val, FinanceConstants.pickedup_json);
                    Thread.sleep(500);
                    cancelOrderTests(get_orderID_val, "true", "3");
                    break;
                case FinanceConstants.rule17:
                    pushPlacedJson(get_orderID_val, FinanceConstants.placed_json);
                    Thread.sleep(500);
                    pushPickeupDeliverjson(get_orderID_val, FinanceConstants.pickedup_json);
                    Thread.sleep(500);
                    cancelOrderTests(get_orderID_val, "true", "3");
                    break;
                default:
                    break;


            }


            System.out.println("--ORDERID for agreeementType: " + agreementType + " is:" + get_orderID_val);
            id_time[0] = get_orderID_val;
            id_time[1] = orderTime;
            return id_time;

        }





    public String[] createOrderFinanceHelperPOPSuperNormal(String restID,String agreementType,String bill_exp,String filePath) throws IOException, InterruptedException {
        String[] id_time = new String[2];
        String get_orderID_val = dateHelper.getCurrentDateTimeIn_yyyyMMddHHmmss();
        String epochTime = String.valueOf(dateHelper.getCurrentEpochTimeInSeconds());
        String orderTime = String.valueOf(dateHelper.getCurrentDateTimeIn_yyyy_mm_dd_hh_mm_ss());
        Random random = new Random();
        int x = random.nextInt(9) + 1;
        get_orderID_val = get_orderID_val + String.valueOf(x);
        String orderID = "";
        try {
            orderID = createOtherOrderFinPOPSuperNormal(get_orderID_val,orderTime,epochTime,restID,agreementType,getAgreementTypeFromAgreementTypeString(agreementType),bill_exp,filePath);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Thread.sleep(1000);
        pushPlacedJson(get_orderID_val, FinanceConstants.placed_json);
        Thread.sleep(500);

        pushPickeupDeliverjson(get_orderID_val, FinanceConstants.pickedup_json);
        Thread.sleep(500);
        pushPickeupDeliverjson(get_orderID_val, FinanceConstants.delivered_json);
        Thread.sleep(500);


        System.out.println("--ORDERID for agreeementType: "+agreementType+ " is:"+get_orderID_val);
        id_time[0] = get_orderID_val;
        id_time[1] = orderTime;
        return id_time;

    }

    public String[] createCancelOrderFinanceHelperPOPSuperNormal(String restID,String agreementType,String bill_exp,String filePath, String rule) throws IOException, InterruptedException {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(FinanceConstants.finDB);
        if (rule.equalsIgnoreCase(FinanceConstants.rule15)) {
            String q1 = FinanceConstants.updateMfrEnabledWithoutFallback + restID;
            System.out.println("--Query ::: " + q1);
            sqlTemplate.update(q1);
        }
        if (rule.equalsIgnoreCase(FinanceConstants.rule16)) {
            String q1 = FinanceConstants.updateMfrEnabledWithFallbackNewMou + restID;
            System.out.println("--Query ::: " + q1);
            sqlTemplate.update(q1);
        }
        if (rule.equalsIgnoreCase(FinanceConstants.rule17)) {
            String q1 = FinanceConstants.updateMfrEnabledWithFallbackOldMou + restID;
            System.out.println("--Query ::: " + q1);
            sqlTemplate.update(q1);
        }
        String[] id_time = new String[2];
        String get_orderID_val = dateHelper.getCurrentDateTimeIn_yyyyMMddHHmmss();
        String epochTime = String.valueOf(dateHelper.getCurrentEpochTimeInSeconds());
        String orderTime = String.valueOf(dateHelper.getCurrentDateTimeIn_yyyy_mm_dd_hh_mm_ss());
        Random random = new Random();
        int x = random.nextInt(9) + 1;
        get_orderID_val = get_orderID_val + String.valueOf(x);
        String orderID = "";
        try {
            orderID = createOtherOrderFinPOPSuperNormal(get_orderID_val,orderTime,epochTime,restID,agreementType,getAgreementTypeFromAgreementTypeString(agreementType),bill_exp,filePath);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        switch(rule)
        {

            case FinanceConstants.rule1:
                pushPlacedJson(get_orderID_val, FinanceConstants.placed_json);
                Thread.sleep(500);
                cancelOrderTests(get_orderID_val,"true", "1");
                break;
            case FinanceConstants.rule2:
                pushPlacedJson(get_orderID_val, FinanceConstants.placed_json);
                Thread.sleep(500);
                cancelOrderTests(get_orderID_val,"true", "2");
                break;
            case FinanceConstants.rule3:
                pushPlacedJson(get_orderID_val, FinanceConstants.placed_json);
                Thread.sleep(500);
                cancelOrderTests(get_orderID_val,"true", "3");
                break;
            case FinanceConstants.rule4:
                pushPlacedJson(get_orderID_val, FinanceConstants.placed_json);
                Thread.sleep(500);
                pushPickeupDeliverjson(get_orderID_val, FinanceConstants.pickedup_json);
                Thread.sleep(500);
                cancelOrderTests(get_orderID_val,"true", "1");
                break;
            case FinanceConstants.rule5:
                pushPlacedJson(get_orderID_val, FinanceConstants.placed_json);
                Thread.sleep(500);
                pushPickeupDeliverjson(get_orderID_val, FinanceConstants.pickedup_json);
                Thread.sleep(500);
                cancelOrderTests(get_orderID_val,"true", "2");
                break;
            case FinanceConstants.rule6:
                pushPlacedJson(get_orderID_val, FinanceConstants.placed_json);
                Thread.sleep(500);
                pushPickeupDeliverjson(get_orderID_val, FinanceConstants.pickedup_json);
                Thread.sleep(500);
                cancelOrderTests(get_orderID_val,"true", "3");
                break;
            case FinanceConstants.rule7:
                pushPlacedJson(get_orderID_val, FinanceConstants.placed_json);
                Thread.sleep(500);
                cancelOrderTests(get_orderID_val,"false", "1");
                break;
            case FinanceConstants.rule8:
                pushPlacedJson(get_orderID_val, FinanceConstants.placed_json);
                Thread.sleep(500);
                cancelOrderTests(get_orderID_val,"false", "2");
                break;
            case FinanceConstants.rule9:
                pushPlacedJson(get_orderID_val, FinanceConstants.placed_json);
                Thread.sleep(500);
                cancelOrderTests(get_orderID_val,"false", "3");
                break;
            case FinanceConstants.rule10:
                pushPlacedJson(get_orderID_val, FinanceConstants.placed_json);
                Thread.sleep(500);
                pushPickeupDeliverjson(get_orderID_val, FinanceConstants.pickedup_json);
                Thread.sleep(500);
                cancelOrderTests(get_orderID_val,"false", "1");
                break;
            case FinanceConstants.rule11:
                pushPlacedJson(get_orderID_val, FinanceConstants.placed_json);
                Thread.sleep(500);
                pushPickeupDeliverjson(get_orderID_val, FinanceConstants.pickedup_json);
                Thread.sleep(500);
                cancelOrderTests(get_orderID_val,"false", "2");
                break;
            case FinanceConstants.rule12:
                pushPlacedJson(get_orderID_val, FinanceConstants.placed_json);
                Thread.sleep(500);
                pushPickeupDeliverjson(get_orderID_val, FinanceConstants.pickedup_json);
                Thread.sleep(500);
                cancelOrderTests(get_orderID_val,"false", "3");
            case FinanceConstants.rule13:
                pushPickeupDeliverjson(get_orderID_val, FinanceConstants.pickedup_json);
                Thread.sleep(500);
                cancelOrderTests(get_orderID_val, "false", "3");
                break;
            case FinanceConstants.rule14:
                cancelOrderTests(get_orderID_val, "false", "3");
                break;
            case FinanceConstants.rule15:
                pushPlacedJson(get_orderID_val, FinanceConstants.placed_json);
                Thread.sleep(500);
                pushPickeupDeliverjson(get_orderID_val, FinanceConstants.pickedup_json);
                Thread.sleep(500);
                cancelOrderTests(get_orderID_val, "true", "3");
                break;
            case FinanceConstants.rule16:
                pushPlacedJson(get_orderID_val, FinanceConstants.placed_json);
                Thread.sleep(500);
                pushPickeupDeliverjson(get_orderID_val, FinanceConstants.pickedup_json);
                Thread.sleep(500);
                cancelOrderTests(get_orderID_val, "true", "3");
                break;
            case FinanceConstants.rule17:
                pushPlacedJson(get_orderID_val, FinanceConstants.placed_json);
                Thread.sleep(500);
                pushPickeupDeliverjson(get_orderID_val, FinanceConstants.pickedup_json);
                Thread.sleep(500);
                cancelOrderTests(get_orderID_val, "true", "3");
                break;
            default:
                break;



        }

        System.out.println("--ORDERID for agreeementType: "+agreementType+ " is:"+get_orderID_val);
        id_time[0] = get_orderID_val;
        id_time[1] = orderTime;
        return id_time;

    }

    public String[] createZombieOrderHelper(String restID,String agreementType,String bill_exp,String filePath) throws IOException, InterruptedException {
        String[] id_time = new String[2];
        String get_orderID_val = dateHelper.getCurrentDateTimeIn_yyyyMMddHHmmss();
        String epochTime = String.valueOf(dateHelper.getCurrentEpochTimeInSeconds());
        String orderTime = String.valueOf(dateHelper.getCurrentDateTimeIn_yyyy_mm_dd_hh_mm_ss());
        Random random = new Random();
        int x = random.nextInt(9) + 1;
        get_orderID_val = get_orderID_val + String.valueOf(x);
        String orderID = "";
        try {
            orderID = createZombieOrder(get_orderID_val,orderTime,epochTime,restID,agreementType,getAgreementTypeFromAgreementTypeString(agreementType),bill_exp,filePath);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if(Long.parseLong(get_orderID_val)%2==0) {
            pushPlacedJson(get_orderID_val, FinanceConstants.placed_json);
            Thread.sleep(1000);
        }
        else {
            pushPickeupDeliverjson(get_orderID_val, FinanceConstants.pickedup_json);
            Thread.sleep(1000);
        }
        System.out.println("--ORDERID for agreeementType: "+agreementType+ " is:"+get_orderID_val);

        id_time[0] = get_orderID_val;
        id_time[1] = orderTime;
        return id_time;

    }


    public String[] createOnlineOrders (String restID,String agreementType, String filePath, String bill) {
        String[] id_time = new String[2];
        String get_orderID_val = dateHelper.getCurrentDateTimeIn_yyyyMMddHHmmss();
        String epochTime = String.valueOf(dateHelper.getCurrentEpochTimeInSeconds());
        String orderTime = String.valueOf(dateHelper.getCurrentDateTimeIn_yyyy_mm_dd_hh_mm_ss());
        Random random = new Random();
        int x = random.nextInt(9) + 1;
        get_orderID_val = get_orderID_val + String.valueOf(x);
        String orderID = "";
        try {
            orderID = createOnlineOrder(get_orderID_val,orderTime,epochTime,restID,agreementType,getAgreementTypeFromAgreementTypeString(agreementType), filePath,bill);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("--ORDERID for agreeementType: "+agreementType+ " is:"+get_orderID_val);
        id_time[0] = get_orderID_val;
        id_time[1] = orderTime;
        return id_time;
    }

    public void makeRestaurantNewMou (String restID) {
        List<Map<String, Object>> list;
        String q = FinanceConstants.insert_details_in_deduction_configurations.replace("${RESTID}",restID);
        System.out.println("--Query ::: " + q);
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(FinanceConstants.finDB);
        sqlTemplate.execute(q);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String q1 = FinanceConstants.update_restaurant_to_new_mou + restID;
        System.out.println("--Query ::: " + q1);
        sqlTemplate.update(q1);
        System.out.println("--RESTID of NEW_MOU ::: "+restID);
    }

    public String getTaskIdFromReconCurrentPayout (String from, String to ) {
        List<Map<String, Object>> list;
        String q = FinanceConstants.get_task_id_from_recon_current_payout_job.replace("${FROM_DATETIME}",from).replace("${TO_DATETIME}",to);
        System.out.println("--Query ::: " + q);
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(FinanceConstants.finDB);
        list = sqlTemplate.queryForList(q);
        return list.get(0).get("id").toString();
    }

    public boolean pollStatusFromReconCurrentPayout (String from, String to, String validateStatus) {
        List<Map<String, Object>> list;
        String q = FinanceConstants.get_task_id_from_recon_current_payout_job.replace("${FROM_DATETIME}",from).replace("${TO_DATETIME}",to);
        System.out.println("--Query ::: " + q);
        boolean status = DBHelper.pollDB(FinanceConstants.finDB,q,"status",validateStatus,2,150);
        return status;
    }


    public String getPayoutIdFromRestaurantCurrentPayouts(String taskID) {
        List<Map<String, Object>> list;
        String q = FinanceConstants.get_payout_id_from_restaurant_current_payouts + taskID;
        System.out.println("--Query ::: " + q);
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(FinanceConstants.finDB);
        list = sqlTemplate.queryForList(q);
        return list.get(0).get("id").toString();
    }

    public String getIdFromRestaurantCurrentPayoutDetails (String payoutID) {
        List<Map<String, Object>> list;
        String q = FinanceConstants.get_id_from_restaurant_current_payout_details + payoutID;
        System.out.println("--Query ::: " + q);
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(FinanceConstants.finDB);
        list = sqlTemplate.queryForList(q);
        return list.get(0).get("id").toString();
    }

    public Processor createInvoiceJob(String from, String to, String user_id, String payoutTaskId, String cityID) {
        GameOfThronesService service = new GameOfThronesService("fin","createinvoicejob",gameofthrones);
        return new Processor(service, getDefaultHeaders(), new String[]{from, to, user_id,payoutTaskId,cityID});
    }

    public String getInvoiceTaskIdFromReconInvoiceJobs() {
        List<Map<String, Object>> list;
        String q = FinanceConstants.get_task_id_from_recon_invoice_jobs;
        System.out.println("--Query ::: " + q);
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(FinanceConstants.finDB);
        list = sqlTemplate.queryForList(q);
        return list.get(0).get("id").toString();
    }

    public String getStatusFromReconInvoiceJobs(String taskID) {
        List<Map<String, Object>> list;
        String q = FinanceConstants.get_status_from_recon_invoice_jobs + taskID;
        System.out.println("--Query ::: " + q);
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(FinanceConstants.finDB);
        list = sqlTemplate.queryForList(q);
        return list.get(0).get("status").toString();
    }

    public boolean pollDBForStatusFromReconInvoiceJobs(String taskID, String statusVal) {
        String q = FinanceConstants.get_status_from_recon_invoice_jobs + taskID;
        System.out.println("--Query ::: " + q);
        boolean returnVal = DBHelper.pollDB(FinanceConstants.finDB,q,"status",statusVal,2,150);
        return returnVal;
    }

    public List<Map<String, Object>> getDetailsFromSubtask(String taskID) {
        List<Map<String, Object>> list;
        String q = FinanceConstants.get_details_from_subtask + taskID;
        System.out.println("--Query ::: " + q);
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(FinanceConstants.finDB);
        list = sqlTemplate.queryForList(q);
        return list;
    }

    public List<Map<String, Object>> getDetailsFromReconInvoices(String taskID) {
        List<Map<String, Object>> list;
        String q = FinanceConstants.get_details_from_recon_invoices + taskID;
        System.out.println("--Query ::: " + q);
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(FinanceConstants.finDB);
        list = sqlTemplate.queryForList(q);
        return list;
    }

    public String getIDFromMasterFileJob() {
        List<Map<String, Object>> list;
        String q = FinanceConstants.get_id_from_recon_master_file_job;
        System.out.println("--Query ::: " + q);
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(FinanceConstants.finDB);
        list = sqlTemplate.queryForList(q);
        //System.out.println("=======list "+list);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (list.size() > 0) {
            System.out.println("--==ID ::: " + list.get(0).get("id").toString());
            String id = list.get(0).get("id").toString();
            return id;
        } else
            return "-1";

    }

    public Processor generateMasterFile(String from, String to, String cityID, String userId, String type) {
        GameOfThronesService service = new GameOfThronesService("fin","generatemasterfilenew",gameofthrones);
        return new Processor(service, getDefaultHeaders(), new String[]{from, to, cityID, userId, type});
    }

    public HashMap<String, List<String>> getInvoiceUrlAnnexureUrlFromReconInvoices(String taskID) {
        HashMap<String, List<String>> hm = new HashMap<>();
        List<String> invoices = new ArrayList<>();
        List<String> annexures = new ArrayList<>();
        List<Map<String, Object>> list;
        String q = FinanceConstants.get_details_from_recon_invoices + taskID;
        System.out.println("--Query ::: " + q);
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(FinanceConstants.finDB);
        list = sqlTemplate.queryForList(q);
        for (int i = 0; i < list.size() ; i++) {
            invoices.add(list.get(i).get("invoice_url").toString());
            annexures.add(list.get(i).get("annexure_url").toString());
            System.out.println("--ANNEXURE URL :::: "+ list.get(i).get("annexure_url").toString());
        }
        hm.put("invoice_url",invoices);
        hm.put("annexure_url",annexures);
        return hm;
    }

    public List<Map<String, Object>> getDetailsFromMasterFileJob(String id) {
        List<Map<String, Object>> list;
        String q = FinanceConstants.get_details_from_recon_master_file_job + id;
        System.out.println("--Query ::: " + q);
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(FinanceConstants.finDB);
        list = sqlTemplate.queryForList(q);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (list.size() > 0) {
            System.out.println("--===URL ::: " + list.get(0).get("file_url").toString());
        }
        return list;
    }

    public HashMap<String, String[]> parseCSVFileToHashMap(String path) throws IOException {
        File file = new File(path);
        List<String> list = new ArrayList<>();
        HashMap<String, String[]> hm = new HashMap<>();
        BufferedReader in = null;
        try {
            in = new BufferedReader(new FileReader(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        String st;
        while ((st = in.readLine()) != null) {
            list.add(st);
            //debug
            System.out.println("--Line ::: " + st);
        }
        System.out.println("--Size of list in parseTxtFileToHashMap ::: " + list.size());
        for (int i = 0; i < list.size(); i++) {
            String[] vals = list.get(i).split(",");
            hm.put(vals[1], vals);
        }
        return hm;
    }

    public void placedRMSUpdate(String orderID) throws IOException {
        File file = new File("../Data/Payloads/JSON/fin_placedStatusUpdate.json");
        String placedRMSOrder = FileUtils.readFileToString(file);
        placedRMSOrder = placedRMSOrder.replace("${orderID}", orderID);

        rmqHelper.pushMessageToExchange(FinanceConstants.hostName, FinanceConstants.placedOrdersQueue, new AMQP.BasicProperties().builder().contentType("application/json"), placedRMSOrder);
        //System.out.println("Order Id ----- " + orderId);
        //return placedRMSOrder;
    }

    
    public String validateOrderStatusInReconRawOrderFromDB(String orderID) {
        List<Map<String, Object>> list;
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(FinanceConstants.finDB);
        String q = FinanceConstants.get_order_details_from_recon_raw_orders + orderID;
        System.out.println("--Query ::: " + q);
        list = sqlTemplate.queryForList(q);
        if (list.size() == 0)
            return "0";
        else {
            String status = list.get(0).get("status").toString();
            System.out.println("--ORDERID ::: "+orderID+" , --Status ::: "+status);
            return status;
        }
    }
    
    public String validateOrderStatusInMasterFromDB(String orderID) {
        List<Map<String, Object>> list;
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(FinanceConstants.finDB);
        String q = FinanceConstants.get_order_details_from_master + orderID;
        System.out.println("--Query ::: " + q);
        list = sqlTemplate.queryForList(q);
        if (list.size() == 0)
            return "0";
        else {
            String status = list.get(0).get("order_status").toString();
            System.out.println("--ORDERID ::: "+orderID+" , --Master Status ::: "+status);
            return status;
        }

    }

    public  String getMasterEntityFromDB(String master_entity,String orderId){
        List<Map<String, Object>> list;
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(FinanceConstants.finDB);
        String q = FinanceConstants.get_master_entities_from_master.replace("${*}",master_entity).replace("${order_id}",orderId);
        System.out.println("--Query ::: " + q);
        list = sqlTemplate.queryForList(q);
        if (list.size() == 0)
            return "0";
        else {
            String entity = list.get(0).get(master_entity).toString();
            System.out.println("--ORDERID ::: "+orderId+" , --Master ::: "+entity);
            return entity;
        }


    }
    public  String getMasterEntityTINYFromDB(String master_entity,String orderId) {
        List<Map<String, Object>> list;
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(FinanceConstants.finDB);
        String q = FinanceConstants.get_master_entities_from_master_tinyint.replace("${*}", master_entity).replace("${order_id}", orderId);
        System.out.println("--Query ::: " + q);
        list = sqlTemplate.queryForList(q);
        if (list.size() == 0)
            return "0";
        else {
            String entity = list.get(0).get(master_entity).toString();
            System.out.println("--ORDERID ::: " + orderId + " , --Master ::: " + entity);
            return entity;
        }

    }

    public void pushPlacedJson(String order_id,String filePath) throws IOException {
        String time= getTimeStampForPlacedOrder();

        File file = new File(filePath);
        String placedJson = FileUtils.readFileToString(file);
        placedJson=placedJson.replace("${order_id}",order_id).replace("${time}",time);

        rmqHelper.pushMessageToExchange(LosConstants.hostName,FinanceConstants.placedOrdersQueue, new AMQP.BasicProperties().builder().contentType("application/json"), placedJson);
        //Thread.sleep(1000);
        System.out.println("placed status order Id ----- "+ order_id);


    }
    public void pushPickeupDeliverjson(String order_id,String filePath) throws IOException {
        String time=getCurrentTimeFinance();
        File file = new File(filePath);
        String pickedupJson = FileUtils.readFileToString(file);
        pickedupJson=pickedupJson.replace("${order_id}",order_id).replace("${time}",time);
        rmqHelper.pushMessageToExchange(LosConstants.hostName,LosConstants.statusUpdateQueue, new AMQP.BasicProperties().builder().contentType("application/json"), pickedupJson);
        //Thread.sleep(1000);
        System.out.println("status order Id ----- "+ order_id);
    }




   public String getTimeStampForPlacedOrder(){
       Date date=new Date();
       DateFormat dateform1=new SimpleDateFormat("yyyy-MM-dd");
       DateFormat dateform2=new SimpleDateFormat("kk:mm:ss");
       String formatteddate1=dateform1.format(date);
       String formatteddate2=dateform2.format(date);
       return  formatteddate1+"T"+formatteddate2;

   }

   public String getCurrentTimeFinance(){
       Date date=new Date();
       DateFormat dateform=new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
       String formatteddate=dateform.format(date);
       return formatteddate;


   }

}
