package com.swiggy.api.erp.vms.dp;

import com.swiggy.api.erp.vms.constants.PrepTimeConstants;
import com.swiggy.api.erp.vms.constants.RMSConstants;
import com.swiggy.api.erp.vms.helper.RMSHelper;
import com.swiggy.api.erp.vms.helper.RMSPrepTimeHelper;
import org.testng.annotations.DataProvider;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created by kiran.j on 8/2/18.
 */
public class RMSPrepTimeDP {

    RMSHelper rmsHelper = new RMSHelper();
    private static RMSPrepTimeHelper helper = new RMSPrepTimeHelper();


    @DataProvider(name = "slotWisePrepTime")
    public Iterator<Object[]> slotWisePrepTime() {
        List<Object[]> obj = new ArrayList<>();
        String auth = rmsHelper.getLoginToken(RMSConstants.prepTimeRestaurantId, RMSConstants.auth);
        obj.add(new Object[]{ RMSConstants.prepTimeRestaurantId, PrepTimeConstants.fetch_prep, auth, true});
        obj.add(new Object[]{RMSConstants.invalidprepid, PrepTimeConstants.invalid_session, auth, true});
        obj.add(new Object[]{"",PrepTimeConstants.invalid_input, auth, false});

        return obj.iterator();
    }

    @DataProvider(name = "dayWisePrepTime")
    public Iterator<Object[]> dayWisePrepTime() {
        List<Object[]> obj = new ArrayList<>();
        String auth = rmsHelper.getLoginToken(RMSConstants.prepTimeRestaurantId, RMSConstants.auth);
        obj.add(new Object[]{ RMSConstants.prepTimeRestaurantId,PrepTimeConstants.fetch_multi_prep, auth, true});
        obj.add(new Object[]{RMSConstants.invalidprepid, PrepTimeConstants.invalid_session, auth, true});
        obj.add(new Object[]{"",PrepTimeConstants.fail_fetch_prep, auth, false});

        return obj.iterator();
    }

    @DataProvider(name = "savePrepTime")
    public Iterator<Object[]> savepreptime() {
        String toDate, fromDate, days="";
        Date date = new Date();
        String auth = rmsHelper.getLoginToken(RMSConstants.restaurantId, RMSConstants.auth);
        String[] rests = RMSConstants.restaurantIds;

        //Delete Restaurant Prep Time Slots before Creating new
        for(int i=0;i<rests.length;i++)
            helper.delPreptime(rests[i], auth, rests[i]);

        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        fromDate = dateFormat.format(-1);
        toDate = dateFormat.format(date);
        List<Object[]> obj = new ArrayList<>();
        for(int j=0;j<RMSConstants.restaurantIds.length;j++) {
            days = "\"" + RMSConstants.days[0] + "\"";
//            auth = rmsHelper.getLoginToken(RMSConstants.restaurantIds[j], RMSConstants.auth);
            for (int i = 1; i < RMSConstants.days.length; i++) {
                days += "," + "\"" + RMSConstants.days[i] + "\"";
                obj.add(new Object[]{RMSConstants.restaurantIds[j], fromDate.toString(), toDate.toString(), RMSConstants.prep_time, RMSConstants.PrepType.GLOBAL.toString(), days,"Saved prep time for a restaurant Successfully",auth,  true});
                obj.add(new Object[]{RMSConstants.restaurantIds[j], fromDate.toString(), toDate.toString(), RMSConstants.prep_time, RMSConstants.PrepType.SLOTWISE.toString(), days, "Saved prep time for a restaurant Successfully",auth, true});
                obj.add(new Object[]{RMSConstants.restaurantIds[j], fromDate.toString(), toDate.toString(), RMSConstants.prep_time, RMSConstants.PrepType.TEST.toString(), days, RMSConstants.errorstatus,auth, false});
            }
        }
        obj.add(new Object[]{RMSConstants.restaurantIds[0], fromDate.toString(), toDate.toString(), RMSConstants.prep_time, RMSConstants.PrepType.TEST.toString(), days, RMSConstants.auth_error,RMSConstants.invalid_auth, false});
        obj.add(new Object[]{RMSConstants.restaurantIds[0], fromDate.toString(), toDate.toString(), RMSConstants.invalid_preptime, RMSConstants.PrepType.SLOTWISE.toString(), days, RMSConstants.bad_request,RMSConstants.prep_auth, false});
        obj.add(new Object[]{RMSConstants.restaurantIds[0], fromDate.toString(), toDate.toString(), RMSConstants.prep_time, RMSConstants.PrepType.SLOTWISE.toString(), days + RMSConstants.invalid_day, RMSConstants.bad_request,RMSConstants.prep_auth, false});
        return obj.iterator();

    }

    @DataProvider(name = "deleteSlotPrepTime")
    public Iterator<Object[]> deleteSlotPrepTime() {
        String auth = rmsHelper.getLoginToken(RMSConstants.restaurantId, RMSConstants.auth);
        helper.createPrepTimeForRest(RMSConstants.restaurantId, auth);
        String[] slots = helper.fetchSlotWiseId(RMSConstants.restaurantId, auth);
        List<Object[]> obj = new ArrayList<>();
        for(int i=0;i<slots.length;i++)
            obj.add(new Object[]{slots[i],"Deleted prep time Successfully", auth, true, RMSConstants.restaurantId});
        obj.add(new Object[]{RMSConstants.invalidprepid, RMSConstants.invalid_prep_message, auth, false, RMSConstants.restaurantId});
        return obj.iterator();
    }

    @DataProvider(name = "updatepreptime")
    public Iterator<Object[]> updatepreptime() {
        String toDate, fromDate, days="", rests;
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        fromDate = dateFormat.format(-1);
        toDate = dateFormat.format(date);
        String auth = rmsHelper.getLoginToken(RMSConstants.restaurantId, RMSConstants.auth);
        helper.createPrepTimeForRest(RMSConstants.restaurantId, auth);
        String[] slotwise_slots = helper.fetchSlotWiseId(RMSConstants.restaurantId, auth);
        String[] global_slots = helper.fetchSlotWiseId(RMSConstants.restaurantId, auth);
        List<Object[]> obj = new ArrayList<>();
        days = "\"" + RMSConstants.days[0] + "\"";
        for (int j = 1; j < RMSConstants.days.length; j++) {
            days += "," + "\"" + RMSConstants.days[j] + "\"";
            obj.add(new Object[]{fromDate.toString(), toDate.toString(), RMSConstants.prep_time, RMSConstants.PrepType.SLOTWISE.toString(), days, slotwise_slots[0], "Updated prep time Successfully",auth, true, RMSConstants.restaurantId});
        }
        for (int j = 1; j < RMSConstants.days.length; j++) {
            days += "," + "\"" + RMSConstants.days[j] + "\"";
            obj.add(new Object[]{fromDate.toString(), toDate.toString(), RMSConstants.prep_time, RMSConstants.PrepType.GLOBAL.toString(), days, global_slots[0], "Updated prep time Successfully",auth, true, RMSConstants.restaurantId});
        }
        obj.add(new Object[]{fromDate.toString(), toDate.toString(), RMSConstants.prep_time, RMSConstants.PrepType.GLOBAL.toString(), days, RMSConstants.invalidprepid, "Failed to update prep time",auth, false, RMSConstants.restaurantId});
        obj.add(new Object[]{fromDate.toString(), toDate.toString(), RMSConstants.prep_time, RMSConstants.PrepType.SLOTWISE.toString(), days, RMSConstants.invalidprepid, "Failed to update prep time", auth, false, RMSConstants.restaurantId});
        return obj.iterator();
    }

    @DataProvider(name = "savePrepTimeMulti")
    public Iterator<Object[]> savePrepTimeMulti() {
        String toDate, fromDate, days = "", rests;
        Date date = new Date();

        String[] rest = RMSConstants.restaurantIds;
        String auth = rmsHelper.getLoginToken(RMSConstants.restaurantId, RMSConstants.auth);
        //Delete Restaurant Prep Time Slots before Creating new
        for(int i=0;i<rest.length;i++)
            helper.delPreptime(rest[i], auth, rest[i]);

        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        fromDate = dateFormat.format(-1);
        toDate = dateFormat.format(date);
        List<Object[]> obj = new ArrayList<>();
        rests = RMSConstants.restaurantIds[0];
        for(int j=0;j<RMSConstants.restaurantIds.length;j++) {
            days = "\"" + RMSConstants.days[0] + "\"";
            for (int i = 1; i < RMSConstants.days.length; i++) {
                days += "," + "\"" + RMSConstants.days[i] + "\"";
                obj.add(new Object[]{rests, fromDate.toString(), toDate.toString(), RMSConstants.prep_time, RMSConstants.PrepType.GLOBAL.toString(), days, RMSConstants.preptimeMessage,RMSConstants.prep_auth, true,true });
                obj.add(new Object[]{rests, fromDate.toString(), toDate.toString(), RMSConstants.prep_time, RMSConstants.PrepType.SLOTWISE.toString(), days, RMSConstants.preptimeMessage,RMSConstants.prep_auth, true,true});
                obj.add(new Object[]{rests, fromDate.toString(), toDate.toString(), RMSConstants.prep_time, RMSConstants.PrepType.TEST.toString(), days, RMSConstants.errorstatus,RMSConstants.prep_auth, true,false});
            }
            if(j < RMSConstants.restaurantIds.length - 1)
                rests += "," + RMSConstants.restaurantIds[j+1];
        }
        obj.add(new Object[]{rests, fromDate.toString(), toDate.toString(), RMSConstants.prep_time, RMSConstants.PrepType.TEST.toString(), days, RMSConstants.auth_error,RMSConstants.invalid_auth, true, false});
        obj.add(new Object[]{rests, fromDate.toString(), toDate.toString(), RMSConstants.invalid_preptime, RMSConstants.PrepType.SLOTWISE.toString(), days, RMSConstants.bad_request,RMSConstants.prep_auth, true, false});
        obj.add(new Object[]{rests, fromDate.toString(), toDate.toString(), RMSConstants.prep_time, RMSConstants.PrepType.SLOTWISE.toString(), days + RMSConstants.invalid_day, RMSConstants.bad_request,RMSConstants.prep_auth, true, false});


        return obj.iterator();

    }
}
