package com.swiggy.api.erp.ff.tests.telephonyPlatform;

import com.swiggy.api.erp.ff.dp.telephonyPlatform.TelephonyPlatformTestData;
import com.swiggy.api.erp.ff.helper.TelephonyPlatformHelper;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.annotations.Test;

public class ExotelCallbackAPITests extends TelephonyPlatformTestData{

    TelephonyPlatformHelper helper = new TelephonyPlatformHelper();

    @Test(dataProvider = "exotelKeypressCallbackData", description = "Test telephony keypress callback API")
    public void exotelKeypressCallbackAPITests(String appId, String digits){
        System.out.println("***************************************** exotelKeypressCallbackAPITests started *****************************************");

        Processor response = helper.exotelKeypressCallback(appId, digits);
        System.out.println(response.ResponseValidator.GetBodyAsText());
        System.out.println("Response code is == " + response.ResponseValidator.GetResponseCode());

        System.out.println("######################################### exotelKeypressCallbackAPITests compleated #########################################");
    }

    @Test(dataProvider = "exotelStatusCallbackData", description = "Test telephony status callback API")
    public void exotelStatusCallbackAPITests(String appId, String status){
        System.out.println("***************************************** exotelStatusCallbackAPITests started *****************************************");

        Processor response = helper.exotelStatusCallback(appId, status);
        System.out.println(response.ResponseValidator.GetBodyAsText());
        System.out.println("Response code is == " + response.ResponseValidator.GetResponseCode());

        System.out.println("######################################### exotelStatusCallbackAPITests compleated #########################################");
    }
}
