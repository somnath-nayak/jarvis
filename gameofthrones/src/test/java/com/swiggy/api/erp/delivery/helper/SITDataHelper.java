package com.swiggy.api.erp.delivery.helper;

import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by preetesh.sharma on 30/03/18.
 */
public class SITDataHelper {

    Initialize init = new Initialize();
    DeliveryServiceHelper deliveryServiceHelper= new DeliveryServiceHelper();
    DeliveryHelperMethods del= new DeliveryHelperMethods();
    AutoassignHelper autoassignHelper= new AutoassignHelper();
    DeliveryDataHelper deliveryDataHelper= new DeliveryDataHelper();

    public boolean addPolygonZone(String payloadjson)
    {
        Boolean polygonstatus= false;
        GameOfThronesService service = new GameOfThronesService(
                DeliveryConstant.delivery_Service, "addpolygonzone", init);
        HashMap<String, String> requestheaders = del.doubleheader();
        String[] payloadparams = new String[] { payloadjson};
        Processor processor=new Processor(service,requestheaders,payloadparams);
        int statuscode=processor.ResponseValidator.GetResponseCode();
        if(statuscode==200)
        {
            polygonstatus= true;
        }

        return polygonstatus;
    }

    public void placede(int zone_id, String appversion, String location)
    {
        String areaquery="select id from area where zone_id="+zone_id;
        String areaid=SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForList(areaquery).get(0).get("id").toString();
        String latlonarr[]= location.split(",");
        Map<String, String> requestheaders= new HashMap<>();
        String deauth= (deliveryDataHelper.getDEAuth(deliveryDataHelper.CreateDE(zone_id,Integer.parseInt(areaid)),appversion));
        autoassignHelper.locupdate(latlonarr[0],latlonarr[1],deauth);
    }

    //update params

    public boolean updateRainParamsForCity(Integer city_id)
    {
        Boolean updateparamstatus=false;
        String updaterainparams = "INSERT INTO `city_rain_params` (`city_id`, `surge_fee`, `bf_stop_factor`, `bf_start_factor`, `beef_up_minutes`, `surge_multiplier`, `surge_minutes`, `banner_message`, `last_mile_cap`, `max_sla`)\n" +
                "VALUES (" + city_id + ",20,1,0.9,20,1.4,30,'Due to rains,  a surge fee of Rs 20 is being levied which will be passed on to delivery executives.',3.50,63);";
        SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).execute(updaterainparams);
        List<Map<String, Object>> checklist = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForList("select * from city_rain_params where city_id=" + city_id);
        if (checklist.size() > 0)
        {
            updateparamstatus=true;
        }
        return updateparamstatus;
    }

    public boolean updateZoneParams(int zone_id)
    {
        Boolean zoneParamstatus=false;
        float rain_open_bf=0.2f;
        float rain_close_bf=0.2f;
        float conversion_factor=0.2f;
        String zoneparamsupdatequery="update zone set rain_open_bf="+rain_open_bf+ ", rain_close_bf="+rain_close_bf+", conversion_factor="+conversion_factor+" where id="+zone_id;
        int updatestatus=SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(zoneparamsupdatequery);
        if(updatestatus==1)
        {
            zoneParamstatus=true;
        }
        return zoneParamstatus;
    }

    public String getApproxLatLongAtADistance(Double lat, Double lon, Double distance)
    {
        String text = Double.toString(Math.abs(lat));
        int integerPlaces = text.indexOf('.');
        int decimalPlaces = text.length() - integerPlaces - 1;
        Double r_earth=6731.0;
        Double newLat=lat +( distance / r_earth) * (180 / Math.PI);
        Double newLon=lon+(distance/r_earth)*(180/Math.PI)/Math.cos(lat* Math.PI/180);
        BigDecimal bigDecimalLat=new BigDecimal(newLat);
        BigDecimal roundedWithScale1=bigDecimalLat.setScale(decimalPlaces,BigDecimal.ROUND_HALF_EVEN);
        BigDecimal bigDecimalLon=new BigDecimal(newLon);
        BigDecimal roundedWithScale2=bigDecimalLon.setScale(decimalPlaces,BigDecimal.ROUND_HALF_EVEN);
        System.out.println(roundedWithScale1+","+roundedWithScale2);
        return roundedWithScale1+","+roundedWithScale2;
    }

    public  List<String> getZoneBoundayCoordinates(String zone_id)
    {
        //Get latlong list fromgiven zone ID
        //Split it in to two and get centrioid of each of them
        String pathquery="select path from zone where id ="+zone_id;
        String latlonglist= SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForList(pathquery).get(0).get("path").toString();
        // "13.060659,77.524917 13.047866,77.540453 13.022279,77.519252 13.047532,77.492387";
        final String[] latlonarray= latlonglist.split(" ");
        List<String> boundaryPointList=new ArrayList<String>();
        for(int i=0;i<latlonarray.length-2;i++)
        {
            List<String> lineList= new ArrayList<String>();
            lineList.add(latlonarray[i]);
            lineList.add(latlonarray[i+1]);
            String boundaryLatlon=getCentroid(lineList);
            boundaryPointList.add(boundaryLatlon);
        }
        boundaryPointList.add(getCentroid(new ArrayList<String>()
        {{add(latlonarray[0]);
            add(latlonarray[latlonarray.length-1]);
        }}));
        return boundaryPointList;
    }


    public  String getCentroid(List<String> path) {

        double sumX = 0, sumY = 0, sumZ = 0;
        int counts = 0;
        for (String latLong : path) {
            sumX += (Math.cos(Math.toRadians(Double.parseDouble(latLong.split(",")[0]))) * Math.cos(
                    Math.toRadians(Double.parseDouble(latLong.split(",")[1]))));
            sumY += (Math.cos(Math.toRadians(Double.parseDouble(latLong.split(",")[0])))) * Math.sin(
                    Math.toRadians(Double.parseDouble(latLong.split(",")[1])));
            sumZ += Math.sin(Math.toRadians(Double.parseDouble(latLong.split(",")[0])));
            counts++;
        }
        double avgX = sumX / counts, avgY = sumY / counts, avgZ = sumZ / counts;

        double lng = Math.atan2(avgY, avgX);
        double hyp = Math.sqrt(avgX * avgX + avgY * avgY);
        double lat = Math.atan2(avgZ, hyp);
        lng = lng * (180 / Math.PI);
        lat = lat * (180 / Math.PI);
        lng = (double) Math.round(lng * 10000000d) / 10000000d;
        lat = (double) Math.round(lat * 10000000d) / 10000000d;
        return lat+","+lng;
    }
}