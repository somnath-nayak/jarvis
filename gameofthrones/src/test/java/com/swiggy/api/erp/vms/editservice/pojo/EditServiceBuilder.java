package com.swiggy.api.erp.vms.editservice.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EditServiceBuilder {

	EditServiceEntry editserviceEntry;

	public EditServiceBuilder() {
		editserviceEntry = new EditServiceEntry();
	}

	@JsonProperty("order_id")
	public EditServiceBuilder setOrderId(long orderId) {
		editserviceEntry.setOrder_id(orderId);
		return this;
	}

	@JsonProperty("edit_reason")
	public EditServiceBuilder setEditReason(String editReason) {
		editserviceEntry.setEdit_reason(editReason);
		return this;
	}

	@JsonProperty("edit_data")
	public EditServiceBuilder setEditData(Edit_data editData) {
		editserviceEntry.setEdit_data(editData);
		return this;
	}

	@JsonProperty("meta_data")
	public EditServiceBuilder setMetaData(Meta_data metaData) {
		editserviceEntry.setMeta_data(metaData);
		return this;
	}
	
	public EditServiceEntry build() {
		return editserviceEntry;
	}

}
