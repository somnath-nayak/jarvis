package com.swiggy.api.erp.cms.tests;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.Test;
import framework.gameofthrones.JonSnow.Processor;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.dp.SelfServeV1Dp;
import com.swiggy.api.erp.cms.helper.SelfServeHelper;

/**
 * Created by HemaGovinadaraj on 14/2/18.
 */
public class SelfServeV1ItemTest extends SelfServeV1Dp{
	SelfServeHelper sshelper= new SelfServeHelper();
	String restaurantId=null;
	public static List<String> updateticketId= new ArrayList<String>(),createticketId = new ArrayList<String>(),ticketslistnotassigned=new ArrayList<String>();
	public static List<Boolean> expectedstatuscreate= new ArrayList<Boolean>(),expectedstatusupdate= new ArrayList<Boolean>();


	@Test(dataProvider="createItem", priority=5, groups={"Sanity_TC", "Smoke_TC", "Regression_TC"},
			description="create item ticket" )
	public void createItem(String payload,String restId,String validationMessage,int status,boolean expectedstate)
			throws JSONException, InterruptedException
	{	

		restaurantId=restId;
		Processor p= sshelper.addSelfServeV1Item(restId,payload);
		String response =p.ResponseValidator.GetBodyAsText();
		int statuscode=p.ResponseValidator.GetResponseCode();

		if (validationMessage!=null)
		{
			String responseitemName = JsonPath.read(response, "$.item_vo.item.name").toString();
			Assert.assertEquals(responseitemName, validationMessage, "Success");
		}
		else
		{
			if(status==400)
				Assert.assertEquals(statuscode, status, "Success");
			else
				Assert.assertEquals(validationMessage, response, "Success");
		}
		if(statuscode==200)
		{
			createticketId.add(JsonPath.read(response, "$.ticket_id").toString().replace("[","").replace("]","").toString());
			expectedstatuscreate.add(expectedstate);
		}


	}

		@Test(dataProvider="updateItem",priority=6, groups={"Sanity_TC", "Smoke_TC", "Regression_TC"},
				description="update item ticket")
		public void updateItem(String payload,String restId,String validationMessage,String itemId,int status,boolean expectedstate)
				throws JSONException, InterruptedException
		{

			Processor p= sshelper.updateItemSelfServeV1(restId,itemId,payload);
			String response = p.ResponseValidator.GetBodyAsText();
			int statuscode=p.ResponseValidator.GetResponseCode();
			if (validationMessage!=null)
			{
				String responseitemName = JsonPath.read(response, "$.item_vo.item.id").toString();
				Assert.assertEquals(itemId, responseitemName, "Success");

			}
			else
			{
				if(status==400)
					Assert.assertEquals(statuscode, status, "Success");
				else
					Assert.assertEquals(validationMessage, response, "Success");
			}
			if(statuscode==200)
			{
				updateticketId.add(JsonPath.read(response, "$.ticket_id").toString().replace("[","").replace("]","").toString());
				expectedstatusupdate.add(expectedstate);
			}

		}
	
	@Test(dataProvider="submitticket",priority=7,groups={"Sanity_TC", "Smoke_TC", "Regression_TC"},
			description="create item ticket")
	public void submitticket(String payload,String restId,String validationMessage,int status)
			throws JSONException, InterruptedException
	{	
		Processor p= sshelper.submitTicket(restId,payload);
		String response =p.ResponseValidator.GetBodyAsText();
		int statuscode=p.ResponseValidator.GetResponseCode();
		String[] responsearray = JsonPath.read(response, "$.data[*].ticket_reason").toString().replace("[","").replace("]","").replace("\"","").split(",");
		for(int i=0;i<responsearray.length;i++)
			Assert.assertEquals(validationMessage,responsearray[i] , "Success");
		Assert.assertEquals(statuscode, status, "Success");

	}

	@Test(dataProvider="approveTicket", priority=8,groups={"Sanity_TC", "Smoke_TC", "Regression_TC"},
			description="create item ticket")
	public void approveTicket(String payload,String ticketId,String ticketState,String agentid,int validationMessage,int status)
			throws JSONException, InterruptedException
	{	Processor p;
	p= sshelper.updateitemticketstate(ticketId,ticketState,payload,agentid);
	String response =p.ResponseValidator.GetBodyAsText();
	int statuscode=p.ResponseValidator.GetResponseCode();
	int responseitemName =Integer.parseInt(JsonPath.read(response, "$.status").toString());
	Assert.assertEquals(validationMessage, responseitemName, "Success");
	Assert.assertEquals(status, statuscode, "Success");
	if(validationMessage==0)
		{
			p= sshelper.updateitemticketstate(ticketId,"REJECTED",payload,agentid);
			Assert.assertEquals(status, statuscode, "Success");
		}

	}

	@Test(dataProvider="approveTicketUpdate", priority=9, groups={"Sanity_TC", "Smoke_TC", "Regression_TC"},
			description="create item ticket")
	public void approveTicketUpdate(String payload,String ticketId,String ticketState,String agentid,int validationMessage,int status)
			throws JSONException, InterruptedException
	{	Processor p;
	p= sshelper.updateitemticketstate(ticketId,ticketState,payload,agentid);
	String response =p.ResponseValidator.GetBodyAsText();
	int statuscode=p.ResponseValidator.GetResponseCode();
	int responseitemName =Integer.parseInt(JsonPath.read(response, "$.status").toString());
	Assert.assertEquals(validationMessage, responseitemName, "Success");
	Assert.assertEquals(status, statuscode, "Success");
	if(validationMessage==0)
	{
		p= sshelper.updateitemticketstate(ticketId,"REJECTED",payload,agentid);
	}

	}

//cancleTicketnonApprovedItem

}