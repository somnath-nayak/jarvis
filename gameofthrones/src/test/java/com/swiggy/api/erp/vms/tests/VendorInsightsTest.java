package com.swiggy.api.erp.vms.tests;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.swiggy.api.erp.vms.constants.RMSConstants;
import com.swiggy.api.erp.vms.dp.VendorInsightsDP;
import com.swiggy.api.erp.vms.helper.RMSHelper;
import com.swiggy.api.erp.vms.helper.VendorInsightsHelper;
import framework.gameofthrones.Cersei.ToolBox;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.io.IOException;
import java.util.List;


public class VendorInsightsTest extends VendorInsightsDP{
	RMSHelper helper=new RMSHelper();
	VendorInsightsHelper VIhelper=new VendorInsightsHelper();
	SchemaValidatorUtils schemaValidatorUtils = new SchemaValidatorUtils();

	
	@Test(dataProvider="getRestaurant_VI",groups={"sanity","smoke"},
			description="POST insights/v2/restaurants/ Fetches all the restaurants for given user id")
	public void getRestaurantVI(String accessToken, String username_VI,String rest_ids,String start_date,String end_date,String group_by,String statusMessage, boolean valid) throws IOException, ProcessingException
	{
		Processor processor=VIhelper.getRestaurants(accessToken, username_VI, rest_ids, start_date, end_date, group_by);
		if (valid) {
			String resp = processor.ResponseValidator.GetBodyAsText();
			String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/getRestaurant_VI");
			List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
			Assert.assertTrue(missingNodeList.isEmpty(), missingNodeList + " Nodes Are Missing Or Not Matching API");
			boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
			System.out.println("Contain empty nodes => " + isEmpty);
		}
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"),statusMessage);
		//Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"),statusCode);

	}

	@Test(dataProvider="orderForRest_VI",groups={"sanity","smoke"},
			description="POST insights/v2/restaurants/revenue-details Fetches all orders on daily basis for given time period and given restaurant id")
	public void orderForRestVI(String accessToken, String user_id,String rest_id,String start_date,String end_date,String group_by,String statusMessage,boolean valid) throws IOException, ProcessingException
	{
		Processor processor=VIhelper.orderForRest(user_id, rest_id, start_date, end_date, group_by,accessToken);
		if(valid) {
			String resp = processor.ResponseValidator.GetBodyAsText();
			String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/orderForRestaurant_VI");
			List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
			Assert.assertTrue(missingNodeList.isEmpty(), missingNodeList + " Nodes Are Missing Or Not Matching  API");
			boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
			System.out.println("Contain empty nodes => " + isEmpty);
		}
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		System.out.println("Valid Message: "+processor.ResponseValidator.GetNodeValue("statusMessage"));
		
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"),statusMessage);
	}

	@Test(dataProvider="helpContext_VI",groups={"sanity","smoke"},
			description="POST insights/v2/help Fetches the help context for given restaurant id")
	public void helpContextVI(String accessToken, String rest_id,String user_id,String statusMessage) throws IOException, ProcessingException
	{
		Processor processor=VIhelper.helpContext(accessToken, rest_id,user_id);
		String resp=processor.ResponseValidator.GetBodyAsText();			
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/helpContext_VI");
		List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
		Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching API");
		boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), RMSConstants.helpContextValidMessage);  
	}

	@Test(dataProvider="orderFeedback_VI",groups={"sanity","smoke"},
			description="POST insights/v2/order-feedback Gives the detail feedback about orders for particular rest id ")
	public void orderFeedbackVI(String accessToken, String order_id,String rest_id,String user_id,String statusMessage) throws IOException, ProcessingException
	{
		Processor processor=VIhelper.orderFeedback(accessToken, order_id, rest_id, user_id);
		String resp=processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/orderFeedback_VI");
		List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
		Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching API");
		boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), RMSConstants.orderFeedbackValidMessage);  
	}

	@Test(dataProvider="lastUpdatedTime_VI",groups={"sanity","smoke"},
			description="POST insights/v2/last-updated-time fetches the lastest updated time for given restaurant id ")
	public void lastUpdatedTimeVI(String accessToken, String rest_id,String statusMessage) throws IOException, ProcessingException
	{
		Processor processor=VIhelper.lastUpdatedTime(accessToken, rest_id);
		String resp=processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/lastUpdatedTime_VI");
		List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
		Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For API");
		boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), RMSConstants.lastUpdatedTimeValidMessage);  
	}


	@Test(dataProvider="confirmationTime_VI",groups={"sanity","smoke"},
			description="POST insights/v2/confirmation-time Fetches the data related to confirmation time for particular  restaurant id")
	public void confirmationTimeVI(String accessToken, String start_date,String end_date,String rest_id,String user_id,String group_by1,String statusMessage) throws IOException, ProcessingException
	{
		Processor processor=VIhelper.confirmationTime(accessToken, start_date,end_date,rest_id,user_id, group_by1);
		String resp=processor.ResponseValidator.GetBodyAsText();
		
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/confirmationTime_VI");
		List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
		Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For API");
		boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		System.out.println("Valid Message: "+processor.ResponseValidator.GetNodeValue("statusMessage"));
		//Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), RMSConstants.orderForRestValidMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), RMSConstants.confirmationTimeValidMessage);
	}


	@Test(dataProvider="orderForRestWeekly_VI",groups={"sanity","smoke"},
			description="POST POST insights/v2/restaurants/revenue-details Fetch the orders on weekly basis for given restaurant id")
	public void orderForRestWeekly_VI(String accessToken, String user_id,String rest_id2,String start_date_weekly,String end_date_weekly,
									  String group_by_weekly,String statusMessage, boolean valid) throws IOException, ProcessingException
	{
		Processor processor=VIhelper.orderForRest(user_id, rest_id2, start_date_weekly, end_date_weekly, group_by_weekly,accessToken);
		if(valid) {
			String resp = processor.ResponseValidator.GetBodyAsText();
			String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/orderForRestaurantWeekly_VI");
			List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
			Assert.assertTrue(missingNodeList.isEmpty(), missingNodeList + " Nodes Are Missing Or Not Matching  API");
			boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
			System.out.println("Contain empty nodes => " + isEmpty);
		}
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		System.out.println("Valid Message: "+processor.ResponseValidator.GetNodeValue("statusMessage"));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
	}

	@Test(dataProvider="orderForRestMonthly_VI",groups={"sanity","smoke"},
			description="POST POST insights/v2/restaurants/revenue-details Fetches total number of orders on Monthly basis for given restaurant id")
	public void orderForRestMonthly_VI(String accessToken, String user_id,String rest_id,String start_date_monthly,String end_date_monthly,
									   String group_by_monthly,String statusMessage, boolean valid) throws IOException, ProcessingException
	{
		Processor processor=VIhelper.orderForRest(user_id, rest_id, start_date_monthly, end_date_monthly, group_by_monthly,accessToken);
		if(valid) {
			String resp = processor.ResponseValidator.GetBodyAsText();
			String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/orderForRestMonthly_VI");
			List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
			Assert.assertTrue(missingNodeList.isEmpty(), missingNodeList + " Nodes Are Missing Or Not Matching  API");
			boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
			System.out.println("Contain empty nodes => " + isEmpty);
		}
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		System.out.println("Valid Message: "+processor.ResponseValidator.GetNodeValue("statusMessage"));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
	}

	@Test(dataProvider="metricsForRest_VI",groups={"sanity","smoke"},
			description="POST insights/v2/restaurants/ops-metrics Gives bussiness metrics detail about particular restaurant id")
	public void metricsForRest_VI(String accessToken, String user_id,String rest_id,String start_date,String end_date,
								  String group_by,String statusMessage, boolean valid) throws IOException, ProcessingException
	{
		Processor processor=VIhelper.metricsForRest(user_id, rest_id, start_date, end_date, group_by,accessToken);
		if(valid) {
			String resp = processor.ResponseValidator.GetBodyAsText();
			String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/metricsForRest_VI");
			List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
			Assert.assertTrue(missingNodeList.isEmpty(), missingNodeList + " Nodes Are Missing Or Not Matching  API");
			boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
			System.out.println("Contain empty nodes => " + isEmpty);
		}
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		System.out.println("Valid Message: "+processor.ResponseValidator.GetNodeValue("statusMessage"));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
	}

	@Test(dataProvider="ratingsForRest_VI",groups={"sanity","smoke"},
			description="POST insights/v2/ratings Fetches the ratings given for particular restaurant id")
	public void ratingsForRest_VI(String accessToken, String start_date,String end_date,String rest_id,String statusMessage, boolean valid) throws IOException, ProcessingException
	{
		Processor processor=VIhelper.ratingsForRest(start_date, end_date, rest_id,accessToken);
		if(valid) {
			String resp = processor.ResponseValidator.GetBodyAsText();
			String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/ratingsForRest_VI");
			List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
			Assert.assertTrue(missingNodeList.isEmpty(), missingNodeList + " Nodes Are Missing Or Not Matching  API");
			boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
			System.out.println("Contain empty nodes => " + isEmpty);
			Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		}
		System.out.println("Valid Message: "+processor.ResponseValidator.GetNodeValue("statusMessage"));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
	}
	
	@Test(dataProvider="ordersForRatings_VI",groups={"sanity","smoke"},
			description="GET insights/v2/ratings/orders Fetches the orders of the particular restaurant which has ratings or comments ")
	public void ordersForRatings_VI(String accessToken, String rest_id,String in_ratings,String user_id,String statusMessage, boolean valid) throws IOException, ProcessingException
	{
		Processor processor=VIhelper.ordersForRatings(rest_id, in_ratings, user_id,accessToken);
		if(valid) {
			String resp = processor.ResponseValidator.GetBodyAsText();
			String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/ordersForRatings_VI");
			List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
			Assert.assertTrue(missingNodeList.isEmpty(), missingNodeList + " Nodes Are Missing Or Not Matching  API");
			boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
			System.out.println("Contain empty nodes => " + isEmpty);
		}
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		System.out.println("Valid Message: "+processor.ResponseValidator.GetNodeValue("statusMessage"));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
	}
	
	@Test(dataProvider="ordersForItems_VI",groups={"sanity","smoke"},
			description="POST insights/v2/items/orders It gives top delivered items for particular restaurant id")
	public void ordersForItems_VI(String accessToken, String start_date,String end_date,String rest_id,String user_id,String statusMessage, boolean valid) throws IOException, ProcessingException
	{
		Processor processor=VIhelper.ordersForItems(start_date, end_date, rest_id, user_id,accessToken);
		if(valid) {
			String resp = processor.ResponseValidator.GetBodyAsText();
			String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/ordersForItems_VI");
			List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
			Assert.assertTrue(missingNodeList.isEmpty(), missingNodeList + " Nodes Are Missing Or Not Matching  API");
			boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
			System.out.println("Contain empty nodes => " + isEmpty);
		}
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		System.out.println("Valid Message: "+processor.ResponseValidator.GetNodeValue("statusMessage"));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
	}
	
	@Test(dataProvider="ordersForCategories_VI",groups={"sanity","smoke"},
			description="POST insights/v2/categories/orders It gives orders according to categories for particular restaurant id")
	public void ordersForCategories_VI(String accessToken, String start_date,String end_date,String rest_id,String user_id,String statusMessage, boolean valid) throws IOException, ProcessingException
	{
		Processor processor=VIhelper.ordersForCategories(start_date, end_date, rest_id, user_id,accessToken);
		if(valid) {
			String resp = processor.ResponseValidator.GetBodyAsText();
			String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/ordersForCategories_VI");
			List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
			Assert.assertTrue(missingNodeList.isEmpty(), missingNodeList + " Nodes Are Missing Or Not Matching  API");
			boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
			System.out.println("Contain empty nodes => " + isEmpty);
		}
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		System.out.println("Valid Message: "+processor.ResponseValidator.GetNodeValue("statusMessage"));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
	}
	
	@Test(dataProvider="cancellationReasons_VI",groups={"sanity","smoke"},
			description="POST insights/v2/cancellation-reasons It gives no of cancellation reason for particular restaurant id")
	public void cancellationReasons_VI(String accessToken, String start_date,String end_date,String rest_id,String user_id,String statusMessage, boolean valid) throws IOException, ProcessingException
	{
		Processor processor=VIhelper.cancellationReasons(start_date, end_date, rest_id, user_id,accessToken);
		if(valid) {
			String resp = processor.ResponseValidator.GetBodyAsText();
			String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/cancellationReasons_VI");
			List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
			Assert.assertTrue(missingNodeList.isEmpty(), missingNodeList + " Nodes Are Missing Or Not Matching  API");
			boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
			System.out.println("Contain empty nodes => " + isEmpty);
		}
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		System.out.println("Valid Message: "+processor.ResponseValidator.GetNodeValue("statusMessage"));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
	}
	
	@Test(dataProvider="oosOrdersAndReasons_VI",groups={"sanity","smoke"},
			description="POST insights/v2/oos-reasons It gives no of outof stock orders with reasons for particular restaurant id")
	public void oosOrdersAndReasons_VI(String accessToken, String start_date,String end_date,String rest_id,String user_id,String group_by_weekly,String statusMessage, boolean valid) throws IOException, ProcessingException
	{
		
		Processor processor=VIhelper.oosOrdersAndReasons(start_date, end_date, rest_id, user_id,group_by_weekly,accessToken);
		if(valid) {
			String resp = processor.ResponseValidator.GetBodyAsText();
			String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/oosOrdersAndReasons_VI");
			List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
			Assert.assertTrue(missingNodeList.isEmpty(), missingNodeList + " Nodes Are Missing Or Not Matching  API");
			boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
			System.out.println("Contain empty nodes => " + isEmpty);
		}
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		System.out.println("Valid Message: "+processor.ResponseValidator.GetNodeValue("statusMessage"));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
	}

	@Test(dataProvider="sellerTieringVI",groups={"sanity","smoke"},
			description="POST insights/v2/seller-tiering It gives seller tiering info")
	public void sellerTieringVI(String accessToken, String start_date, String end_date, String rest_id, String user_id, String statusMessage, boolean valid) throws IOException, ProcessingException
	{

		Processor processor=VIhelper.sellerTieringVI(start_date, end_date, rest_id, user_id, accessToken);
		if(valid) {
			String resp = processor.ResponseValidator.GetBodyAsText();
			String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/sellerTieringVI");
			List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
			Assert.assertTrue(missingNodeList.isEmpty(), missingNodeList + " Nodes Are Missing Or Not Matching  API");
			boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
			System.out.println("Contain empty nodes => " + isEmpty);
		}
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);

	}


	@Test(dataProvider="tdMetricsVI",groups={"sanity","smoke"},
			description="POST insights/v1/td/metrics It gives td metrics for given TD Id")
	public void tdMetricsVI(String accessToken, String start_date, String end_date, String rest_id, String type, String offerId, String statusMessage, boolean valid) throws IOException, ProcessingException
	{

		Processor processor=VIhelper.tdMetricsVI(start_date, end_date, rest_id, type, offerId, accessToken);
		if(valid) {
			String resp = processor.ResponseValidator.GetBodyAsText();
			String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/tdMetricsVI");
			List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
			Assert.assertTrue(missingNodeList.isEmpty(), missingNodeList + " Nodes Are Missing Or Not Matching  API");
			boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
			System.out.println("Contain empty nodes => " + isEmpty);
		}
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);

	}

	@Test(dataProvider = "getOverview", groups = { "sanity",
			"smoke" }, description = "get the overview for business Matrix")
	public void getOverview(String accessToken, String restId, int statusCode, String statusMessage) throws Exception {

		Processor processor = VIhelper.getOverview(accessToken, restId);
		String resp = processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/getOverview");
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		Assert.assertTrue(missingNodeList.isEmpty(), missingNodeList + " Nodes Are Missing Or Not Matching for API");
		boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
		Assert.assertNotNull(
				processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.data.data..day"));
		Assert.assertNotNull(
				processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.data.data..date"));
	}

	@Test(dataProvider = "getOverview_regression", groups = {
			"regression" }, description = "Regression Test Cases to get the overview for business Matrix")
	public void getOverview_regression(String accessToken, String restId, int statusCode, String statusMessage)
			throws Exception {
		Processor processor = VIhelper.getOverview(accessToken, restId);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
	}


	@Test(dataProvider = "getRevenue", groups = { "smoke",
			"sanity" }, description = "get the revenue for business Matrix")
	public void getRevenue(String accessToken, String restId, String startdate, String enddate, String group_by_weekly,
						   int statusCode, String statusMessage) throws Exception {
		Processor processor = VIhelper.getRevenue(accessToken, restId, startdate, enddate, group_by_weekly);
		String resp = processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/getRevenue");
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		Assert.assertTrue(missingNodeList.isEmpty(), missingNodeList + " Nodes Are Missing Or Not Matching for API");
		boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
		Assert.assertNotNull(
				processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..data..overview"));
		Assert.assertNotNull(processor.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data..data..overview.['avg_order_value']"));
	}

	@Test(dataProvider = "getRevenue_regression", groups = {
			"regression" }, description = "Regression Test Cases to get the revenue for business Matrix")
	public void getRevenue_regression(String accessToken, String restId, String startdate, String enddate,
									  String group_by_weekly, int statusCode, String statusMessage) {

			Processor processor = VIhelper.getRevenue(accessToken, restId, startdate, enddate, group_by_weekly);
			Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
			Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
	}

	@Test(dataProvider = "getOperations", groups = { "sanity",
			"smoke" }, description = "get the operations for business Matrix")
	public void getOperations(String accessToken, String restId, String startdate, String enddate,
							  String group_by_weekly, int statusCode, String statusMessage)
			throws Exception {
		Processor processor = VIhelper.getOperations(accessToken, restId, startdate, enddate, group_by_weekly);
		String resp = processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/getOperations");
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		Assert.assertTrue(missingNodeList.isEmpty(), missingNodeList + " Nodes Are Missing Or Not Matching for API");
		boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
		Assert.assertNotNull(
				processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..data..overview"));

	}

	@Test(dataProvider = "getOperations_regression", groups = {
			"regression" }, description = "Regression Test cases to get the operations for business Matrix")
	public void getOperations_regression(String accessToken, String restId, String startdate, String enddate,
										 String group_by_weekly, int statusCode, String statusMessage) {
		Processor processor = VIhelper.getOperations(accessToken, restId, startdate, enddate, group_by_weekly);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);

	}

	@Test(dataProvider="getRatings",groups={"sanity","smoke"},description="get the ratings for given restuarant id")
	public void getRatings(String accessToken, String restId, String startdate, String enddate, String group_by_weekly,
						   int statusCode, String statusMessage) throws IOException, ProcessingException {
		Processor processor = VIhelper.getRatings(accessToken, restId, startdate, enddate, group_by_weekly);
		String resp = processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/getRatings");
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		Assert.assertTrue(missingNodeList.isEmpty(), missingNodeList + " Nodes Are Missing Or Not Matching for API");
		boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
		Assert.assertNotNull(
				processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.model_based_rating"));
		Assert.assertNotNull(
				processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..['reasons']..['reason']"));
	}

	@Test(dataProvider="getRatings_regression",groups={"regression"},
			description="Regression Test Cases to get the ratings for given restuarant id")
	public void getRatings_regression(String accessToken, String restId, String startdate, String enddate,
									  String group_by_weekly, int statusCode, String statusMessage) {
		Processor processor = VIhelper.getRatings(accessToken, restId, startdate, enddate, group_by_weekly);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
	}

	@Test(dataProvider="getMfrReport",groups={"sanity"},
			description="Get MFR daily report for a given restaurant id")
	public void getMfrReport(String restId, String startdate, String enddate, int statusCode, String statusMessage) {
		Processor processor = VIhelper.getMfrReport(restId, startdate, enddate);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsInt("$.data.mfr_pressed_correctly"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsInt("$.data.mfr_not_pressed"));
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsInt("$.data.mfr_pressed_early"));
	}

	@Test(dataProvider="getMfrReportRegression",groups={"regression"},
			description="Get MFR daily report for a given restaurant id")
	public void getMfrReportRegression(String restId, String startdate, String enddate, int statusCode, String statusMessage) {
		Processor processor = VIhelper.getMfrReport(restId, startdate, enddate);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
	}
}