
package com.swiggy.api.erp.ff.tests.assignmentService;

import com.swiggy.api.erp.ff.constants.AssignmentServiceConstants;
import com.swiggy.api.erp.ff.constants.LosConstants;
import com.swiggy.api.erp.ff.constants.OMSConstants;
import com.swiggy.api.erp.ff.helper.AssignmentServiceHelper;
import com.swiggy.api.erp.ff.helper.LOSHelper;
import com.swiggy.api.erp.ff.helper.OMSHelper;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Tyrion.DBHelper;
import framework.gameofthrones.Tyrion.RabbitMQHelper;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.TimeoutException;


public class AssignmentServiceEndtoEndTests implements AssignmentServiceConstants {

    AssignmentServiceHelper assignmentServiceHelper = new AssignmentServiceHelper();
    LOSHelper losHelper = new LOSHelper();
    OMSHelper omshelper = new OMSHelper();
    DBHelper dbHelper = new DBHelper();
    RabbitMQHelper rmqhelper = new RabbitMQHelper();
    Logger log = Logger.getLogger(AssignmentServiceEndtoEndTests.class);

    Long orderId[] = new Long[3];
    String s[] = new String[3];
    String[] V = new String[3];
    String[] L = new String[3];

    @BeforeMethod
    public void cleanUp() throws IOException, TimeoutException{

        rmqhelper.purgeQueue(LosConstants.hostName,LosConstants.manualOrder);
        omshelper.redisFlushAll(OMSConstants.REDIS);

        for (int i = 0; i < 3; i++) {

            SystemConfigProvider.getTemplate(OMSConstants.SERVICE).execute("update oms_orderexecutive set current_order_count=0,enabled=1,start_duty=1 where id=" + AssignmentServiceConstants.verifier_id[i] + ";");
            SystemConfigProvider.getTemplate(OMSConstants.SERVICE).execute("update oms_orderexecutive set current_order_count=0,enabled=1,start_duty=1 where id=" + AssignmentServiceConstants.l1_id[i] + ";");
            SystemConfigProvider.getTemplate(OMSConstants.SERVICE).execute("update oms_orderexecutive set current_order_count=0,enabled=1,start_duty=1 where id=" + AssignmentServiceConstants.l2_id[i] + ";");
            SystemConfigProvider.getTemplate(OMSConstants.SERVICE).execute("update oms_orderexecutive set current_order_count=0,enabled=1,start_duty=1 where id=" + AssignmentServiceConstants.callde_id[i] + ";");

        }

    }

    @Test(groups = {"sanity", "regression"}, description = "Assignment for mulitple Verifier OE with multiple orders ")
    public void testCreateMultipleOrdersLogInOEAndAssignmentVerifier() throws Exception  {
        // 3 Create Orders , 2 Verifier logged in - Assignment validations

        for (int i = 0; i <= 1; i++) {
            //Create Order
            orderId[i] = Long.parseLong(losHelper.getAnOrder("manual"));
            Thread.sleep(5000);
            //Placing manual order
            assignmentServiceHelper.manualOrderRMQ(AssignmentServiceConstants.verifier_assignment_reason_id, orderId[i], AssignmentServiceConstants.verifier_role_id, assignmentServiceHelper.getOrderPk(orderId[i]));
            s[i] = String.valueOf(orderId[i]);
            //Checking order is present in ordersinqueue table
            Assert.assertTrue(assignmentServiceHelper.checkInOrdersInQueueTable(orderId[i], "Verifier"), orderId[i] + "==== is not present in OrdersInQueueTable");
            System.out.println(i+"***"+s[i]);
        }

        for (int i = 0; i < 2; i++) {

            assignmentServiceHelper.oeLoginRMQ(AssignmentServiceConstants.verifier_id[i], AssignmentServiceConstants.verifier_role_id);
            Thread.sleep(6000);
            //Checking whether order is assigned to OE in mysql table
            Assert.assertTrue(dbHelper.pollDB(OMSConstants.ASSIGNMENT_DB, "SELECT order_id FROM db_assignment.assignment where order_id = " + orderId[i] + " and oe_id = " + verifier_id[i] + ";", "order_id", s[i], 5, 70), + orderId[i] +"===Orderid is not found in DB");

        }

    }

    @Test(groups = {"sanity", "regression"}, description = "Multiple OE login with Multiple Orders")
    public void testLoginMultipleOEsCreateMultipleOrdersAndAssignmentVerifier() throws Exception {
        // 3 Verifier logged in , 3 Create Orders and Assignment Validations

        for (int i = 0; i <= 2; i++) {
            assignmentServiceHelper.oeLoginRMQ(AssignmentServiceConstants.verifier_id[i], AssignmentServiceConstants.verifier_role_id);
            Thread.sleep(3000);

        }

        for (int i = 0; i <= 2; i++) {

            orderId[i] = Long.parseLong(losHelper.getAnOrder("manual"));
            assignmentServiceHelper.manualOrderRMQ(AssignmentServiceConstants.verifier_assignment_reason_id, orderId[i], AssignmentServiceConstants.verifier_role_id, assignmentServiceHelper.getOrderPk(orderId[i]));

            s[i] = String.valueOf(orderId[i]);

            //Checking order is present in ordersinqueue table
            Assert.assertTrue(assignmentServiceHelper.checkInOrdersInQueueTable(orderId[i], "Verifier"), orderId[i] + "==== is not present in OrdersInQueueTable");

        }
        Thread.sleep(10000);

        for (int i = 0; i <= 2; i++) {
            Assert.assertTrue(dbHelper.pollDB(OMSConstants.ASSIGNMENT_DB, "SELECT order_id FROM db_assignment.assignment where order_id = " + orderId[i] + " and oe_id = " + verifier_id[i] + ";", "order_id", s[i], 5, 70), + orderId[i] +"===Orderid is not found in DB");
        }

    }

    @Test(groups = {"sanity", "regression"}, description = "Multiple Order logins and Multiple L1 OE")
    public void testCreateMultipleOrdersLogInOEAndAssignmentL1() throws Exception {
        // 3 Create Orders , 2 L1 logged in - Assignment validations
        for (int i = 0; i <= 2; i++) {
            //Placing manual order
            orderId[i] = Long.parseLong(losHelper.getAnOrder("manual"));
            assignmentServiceHelper.ivrsNotEnabled(AssignmentServiceConstants.l1_assignment_reason_id, orderId[i], AssignmentServiceConstants.l1_role_id, assignmentServiceHelper.getOrderPk(orderId[i]));

            //Verifier - All Good proceed
            omshelper.verifyOrder("" + orderId[i], "001");

            Thread.sleep(3000);

            s[i] = String.valueOf(orderId[i]);
            Assert.assertTrue(assignmentServiceHelper.checkInOrdersInQueueTable(orderId[i], L1Placer), orderId[i] + "==== is not present in OrdersInQueueTable");
            System.out.println(i+"***"+s[i]);
        }


        for (int i = 0; i <= 2; i++) {
            //1st OE login
            assignmentServiceHelper.oeLoginRMQ(AssignmentServiceConstants.l1_id[i], AssignmentServiceConstants.l1_role_id);
            Assert.assertTrue(dbHelper.pollDB(OMSConstants.ASSIGNMENT_DB, "SELECT order_id FROM db_assignment.assignment where order_id = " + orderId[i] + " and oe_id = " + l1_id[i] + ";", "order_id", s[i], 10, 90), + orderId[i] +"===Orderid is not found in DB");
        }

    }

    @Test(groups = {"sanity", "regression"}, description = "Multiple OE L1 logins and Multiple orders")
    public void testLoginMultipleOEsCreateMultipleOrdersAndAssignmentL1() throws Exception {
        // 3 Login L1 , 3 Create Orders - Assignment validations

        for (int i = 0; i <= 2; i++) {
            //OE login
            assignmentServiceHelper.oeLoginRMQ(AssignmentServiceConstants.l1_id[i], AssignmentServiceConstants.l1_role_id);
            Thread.sleep(3000);
        }

        for (int i = 0; i <= 2; i++) {
            //Placing 1st manual order
            orderId[i] = Long.parseLong(losHelper.getAnOrder("manual"));
            assignmentServiceHelper.ivrsNotEnabled(AssignmentServiceConstants.l1_assignment_reason_id, orderId[i], AssignmentServiceConstants.l1_role_id, assignmentServiceHelper.getOrderPk(orderId[i]));

            s[i] = String.valueOf(orderId[i]);

            //Verify Order
            omshelper.verifyOrder("" + orderId[i], "001");

        }
        Thread.sleep(10000);

        for (int i = 0; i <= 2; i++) {
            Assert.assertTrue(dbHelper.pollDB(OMSConstants.ASSIGNMENT_DB, "SELECT order_id FROM db_assignment.assignment where order_id = " + orderId[i] + " and oe_id = " + l1_id[i] + ";", "order_id", s[i], 5, 70), + orderId[i] +"===Orderid is not found in DB");
        }

    }

    @Test(groups = {"sanity", "regression"}, description = "Multiple Order and Multiple login")
    public void testCreateMultipleOrdersLogInOEAndAssignmentL2() throws Exception {
        // 3 Create Orders , 2 L2 logged in - Assignment validations
        for (int i = 0; i <= 2; i++) {
            //Placing manual order
            orderId[i] = Long.parseLong(losHelper.getAnOrder("manual"));
            Thread.sleep(1000);
            assignmentServiceHelper.rNr(l2_assignment_reason_id, orderId[i], l2_role_id, assignmentServiceHelper.getOrderPk(orderId[i]));

            //Verifier - All Good proceed
            omshelper.verifyOrder("" + orderId[i], "001");
            Thread.sleep(3000);

            //Reassign placer - L2
            omshelper.reassignPlacer("" + orderId[i]);
            Thread.sleep(3000);

            s[i] = String.valueOf(orderId[i]);

            //Checking order is present in ordersinqueue table
            Assert.assertTrue(assignmentServiceHelper.checkInOrdersInQueueTable(orderId[i], "L2Placer"), orderId[i] + "==== is not present in OrdersInQueueTable");

        }


        for (int i = 0; i <= 1; i++) {
            //OE login
            assignmentServiceHelper.oeLoginRMQ(AssignmentServiceConstants.l2_id[i], l2_role_id);

            Thread.sleep(1000);

            //Checking whether order is assigned to OE in mysql table
            log.info(orderId[i] + "=======================" + l2_id[i]);
            Assert.assertTrue(dbHelper.pollDB(OMSConstants.ASSIGNMENT_DB, "SELECT order_id FROM db_assignment.assignment where order_id = " + orderId[i] + " and oe_id = " + l2_id[i] + ";", "order_id", s[i], 5, 70), + orderId[i] +"===Orderid is not found in DB");

        }
    }

    @Test(groups = {"sanity", "regression"}, description = "Multiple L2 logins and Multiple orders")
    public void testLoginMultipleOEsCreateMultipleOrdersAndAssignmentL2() throws Exception {
        // 3 L2 Logged in , 3 Create orders - Assignment validations
        for (int i = 0; i <= 2; i++) {
            //1st OE login
            assignmentServiceHelper.oeLoginRMQ(AssignmentServiceConstants.l2_id[i], AssignmentServiceConstants.l2_role_id);
            Thread.sleep(3000);

        }

        for (int i = 0; i <= 2; i++) {
            orderId[i] = Long.parseLong(losHelper.getAnOrder("manual"));
            Thread.sleep(1000);
            assignmentServiceHelper.rNr(AssignmentServiceConstants.l2_assignment_reason_id, orderId[i], AssignmentServiceConstants.l2_role_id, assignmentServiceHelper.getOrderPk(orderId[i]));
            //Verifier - All Good proceed
            omshelper.verifyOrder("" + orderId[i], "001");
            Thread.sleep(1000);

            //Reassign placer - L2
            omshelper.reassignPlacer("" + orderId[i]);
            Thread.sleep(1000);

            s[i] = String.valueOf(orderId[i]);

            //Checking order is present in ordersinqueue table
            Assert.assertTrue(assignmentServiceHelper.checkInOrdersInQueueTable(orderId[i], L2Placer), orderId[i] + "==== is not present in OrdersInQueueTable");

        }
        Thread.sleep(10000);
        for(int i=0;i<=2;i++)
        {
            Assert.assertTrue(dbHelper.pollDB(OMSConstants.ASSIGNMENT_DB, "SELECT order_id FROM db_assignment.assignment where order_id = " + orderId[i] + " and oe_id = " + l2_id[i] + ";", "order_id", s[i], 10, 90), + orderId[i] +"===Orderid is not found in DB");
        }
    }

    @Test(groups = {"sanity", "regression"}, description = "Multiple login with multiple order")
    public void testLoginMultipleOEsCreateMultipleOrdersAndAssignmentCallde() throws Exception {
        // 3 Callde Login , 3 Orders creation - Assignment validations

        for (int i = 0; i <= 2; i++) {

            //1st OE login
            assignmentServiceHelper.oeLoginRMQ(AssignmentServiceConstants.callde_id[i], AssignmentServiceConstants.callde_role_id);
            Thread.sleep(6000);
        }

        for (int i = 0; i <= 2; i++) {
            //Placing manual order
            orderId[i] = Long.parseLong(losHelper.getAnOrder("manual"));
            Thread.sleep(10000);

            assignmentServiceHelper.rNr(AssignmentServiceConstants.callde_assignment_reason_id, orderId[i], AssignmentServiceConstants.callde_role_id, assignmentServiceHelper.getOrderPk(orderId[i]));

            //Verifier - All Good proceed
            omshelper.verifyOrder("" + orderId[i], "001");
            Thread.sleep(1000);

            //Reassign placer - L2
            omshelper.reassignPlacer("" + orderId[i]);
            Thread.sleep(1000);

            //Assign CallDE for an order
            omshelper.deAssistance("" + orderId[i], "CNR");
            Thread.sleep(1000);
            s[i] = String.valueOf(orderId[i]);

            Assert.assertTrue(assignmentServiceHelper.checkInOrdersInQueueTable(orderId[i], L2Placer), orderId[i]+"==== is not present in OrdersInQueueTable");
        }

        for (int i = 0; i <= 2; i++) {
            Assert.assertTrue(dbHelper.pollDB(OMSConstants.ASSIGNMENT_DB, "SELECT order_id FROM db_assignment.assignment where order_id = " + orderId[i] + " and oe_id = " + callde_id[i] + ";", "order_id", s[i], 10, 90), + orderId[i] + "===Orderid is not found in Assignment DB");
        }
    }

    @Test(groups = {"sanity", "regression"}, description = "Swiggy assured and Regular order with One OE logged in")
    public void testCreateOrdersSwiggyassuredRegularOrdersLogInOEAndAssignmentL1() throws Exception {

        // 2 Create Orders , 2 L1 logged in - Assignment validations

        //placing 1st manual order
        Long orderId1 = Long.parseLong(losHelper.getAnOrder("manual"));
        Thread.sleep(30000);

        assignmentServiceHelper.ivrsNotEnabled(AssignmentServiceConstants.l1_assignment_reason_id, orderId1, AssignmentServiceConstants.l1_role_id, assignmentServiceHelper.getOrderPk(orderId1));

        //Verifier - All Good proceed
        omshelper.verifyOrder("" + orderId1, "001");
        Thread.sleep(1000);

        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long orderId = Instant.now().getEpochSecond();
        String s1 = String.valueOf(orderId1);
        String s2 = String.valueOf(orderId);

        //Placing 2nd manual order
        losHelper.createSwiggyAssuredOrder("" + orderId, dateFormat.format(date), "" + orderId);
        Thread.sleep(3000);

        assignmentServiceHelper.ivrsNotEnabled(AssignmentServiceConstants.l1_assignment_reason_id, orderId, AssignmentServiceConstants.l1_role_id, assignmentServiceHelper.getOrderPk(orderId));

        //Verifier - All Good proceed
        omshelper.verifyOrder("" + orderId, "001");
        Thread.sleep(1000);

        log.info(orderId1 + "=======================" + orderId);

        //1st OE login
        assignmentServiceHelper.oeLoginRMQ(AssignmentServiceConstants.l1_id[0], AssignmentServiceConstants.l1_role_id);
        Thread.sleep(10000);


        //2nd OE Login
        assignmentServiceHelper.oeLoginRMQ(AssignmentServiceConstants.l1_id[1], AssignmentServiceConstants.l1_role_id);
        Thread.sleep(3000);

        Assert.assertTrue(dbHelper.pollDB(LosConstants.hostName, "select order_id,waiting_for from oms_ordersinqueue where order_id = "+ orderId  +";", "waiting_for", L1Placer, 10, 90), + orderId  +"===Orderid is not found in DB");
        Assert.assertTrue(dbHelper.pollDB(LosConstants.hostName, "select order_id,waiting_for from oms_ordersinqueue where order_id = "+ orderId1  +";", "waiting_for", L1Placer, 10, 90), + orderId1  +"===Orderid1 is not found in DB");

        //Checking whether order is assigned to OE in mysql table

        log.info(orderId1 + "=======================" + l1_id[0]);

        Assert.assertTrue(dbHelper.pollDB(OMSConstants.ASSIGNMENT_DB, "SELECT order_id FROM db_assignment.assignment where order_id = " + orderId1 + " and oe_id = " + l1_id[0] + ";", "order_id", s1, 10, 90), "Orderid1 is not found in DB.."+ orderId1);
        log.info(orderId + "=======================" + l1_id[1]);
        Assert.assertTrue(dbHelper.pollDB(OMSConstants.ASSIGNMENT_DB, "SELECT order_id FROM db_assignment.assignment where order_id = " + orderId + " and oe_id = " + l1_id[1] + ";", "order_id", s2, 10, 90),  "Orderid is not found in DB.."+ orderId1);

    }


    @Test(groups = {"sanity", "regression"}, description = "Multiple Order with logout OE Scenario")
    public void testMultipleOrdersAssignedLogoutOE() throws Exception {
        // 3 Create Orders , 2 Verifier logged in - Assignment validations and Logout - Unassignment validations

        Thread.sleep(2000);
        for (int i = 0; i <= 2; i++) {

            //Placing manual order
            orderId[i] = Long.parseLong(losHelper.getAnOrder("manual"));
            Thread.sleep(1000);
            assignmentServiceHelper.manualOrderRMQ(AssignmentServiceConstants.verifier_assignment_reason_id, orderId[i], AssignmentServiceConstants.verifier_role_id, assignmentServiceHelper.getOrderPk(orderId[i]));

            s[i] = String.valueOf(orderId[i]);
            log.info("SortedListOrder Contains Orderid.." + orderId[i]);

            //Checking order is present in ordersinqueue table
            Assert.assertTrue(assignmentServiceHelper.checkInOrdersInQueueTable(orderId[i], "Verifier"), orderId[i] + "==== is not present in OrdersInQueueTable");

            Thread.sleep(1000);
            log.info("Order Id..." + orderId[i]);
        }

        for (int i = 0; i <= 2; i++) {

            //1st OE login
            assignmentServiceHelper.oeLoginRMQ(AssignmentServiceConstants.verifier_id[i], AssignmentServiceConstants.verifier_role_id);
            //Checking whether order is assigned to OE in mysql table
            log.info(orderId[i] + "=======================" + verifier_id[i]);
            Assert.assertTrue(dbHelper.pollDB(OMSConstants.ASSIGNMENT_DB, "SELECT order_id FROM db_assignment.assignment where order_id = " + orderId[i] + " and oe_id = " + verifier_id[i] + ";", "order_id", s[i], 5, 70), + orderId[i] +"===Orderid is not found in DB");

        }

        for (int i = 0; i <= 2; i++) {
            //OE1 logout
            assignmentServiceHelper.oeLogoutRMQ(verifier_id[i], verifier_role_id, logout_new_state_id);
            Thread.sleep(6000);
            //Checking whether order is unassigned to OE in mysql table
            Assert.assertTrue(dbHelper.pollDB(OMSConstants.ASSIGNMENT_DB, "SELECT reasonForUnassignment_id FROM db_assignment.assignment where order_id = " + orderId[i] + "  and oe_id = " + verifier_id[i] + ";", "reasonForUnassignment_id", "20", 5, 70), + orderId[i] +"===Orderid is not found in DB");

            //Checking order1 is present back in ordersinqueue table
            Assert.assertTrue(assignmentServiceHelper.checkInOrdersInQueueTable(orderId[i], "Verifier"), orderId[i] + "==== is not present in OrdersInQueueTable");

        }

    }

    @Test(groups = {"sanity", "regression"}, description = "Multiple order with OE at startStopDuty")
    public void testCreateMultipleOrdersLogInOEBucketFullStopStartDuty() throws Exception {
        // 3 Create Orders , 2 Verifier logged in - Assignment validations
        rmqhelper.purgeQueue(LosConstants.hostName,LosConstants.manualOrder);
        omshelper.redisFlushAll(OMSConstants.REDIS);

        Thread.sleep(2000);
        for (int i = 0; i <= 2; i++) {
            //Placing manual order
            orderId[i] = Long.parseLong(losHelper.getAnOrder("manual"));
            Thread.sleep(1000);

            assignmentServiceHelper.manualOrderRMQ(AssignmentServiceConstants.verifier_assignment_reason_id, orderId[i], AssignmentServiceConstants.verifier_role_id, assignmentServiceHelper.getOrderPk(orderId[i]));
            s[i] = String.valueOf(orderId[i]);
            Thread.sleep(1000);

            System.out.println(i+"***"+s[i]);
            Assert.assertTrue(assignmentServiceHelper.checkInOrdersInQueueTable(orderId[i], "Verifier"), orderId[i] + "==== is not present in OrdersInQueueTable");

        }

        //1st OE login
        assignmentServiceHelper.oeLoginRMQ(AssignmentServiceConstants.verifier_id[0], AssignmentServiceConstants.verifier_role_id);

        log.info(orderId[0] + "=======================" + verifier_id[0]);
        Assert.assertTrue(dbHelper.pollDB(OMSConstants.ASSIGNMENT_DB, "SELECT order_id FROM db_assignment.assignment where order_id = " + orderId[0] + " and oe_id = " + verifier_id[0] + ";", "order_id", s[0], 10, 90), + orderId[0] +"===Orderid is not found in DB");

        //Stop duty - Needs to check do we have separate queue for Stop and Start duty or same as login/logout queue
        assignmentServiceHelper.oeLogoutRMQ(verifier_id[0], verifier_role_id, logout_new_state_id);
        Thread.sleep(5000);
        log.info(orderId[0] + "=======================" + verifier_id[0]);
        //Checking whether order is unassigned to OE in mysql table
        Assert.assertTrue(dbHelper.pollDB(OMSConstants.ASSIGNMENT_DB, "SELECT reasonForUnassignment_id FROM db_assignment.assignment where order_id = " + orderId[0] + "  and oe_id = " + verifier_id[0] + ";", "reasonForUnassignment_id", "20", 10, 90), orderId[0]+"===Orderid is not found in DB");

    }

    @Test(groups = {"sanity", "regression"}, description = "Multiple Order with Multple Call DE")
    public void testCreateMultipleOrdersLogInOEAndAssignmentCallDe() throws Exception {
        // 3 Orders creation , 2 Callde Login - Assignment validations
        rmqhelper.purgeQueue(LosConstants.hostName,LosConstants.manualOrder);
        omshelper.redisFlushAll(OMSConstants.REDIS);

        Thread.sleep(2000);
        for (int i = 0; i <= 1; i++) {

            //Placing 1st manual order
            orderId[i] = Long.parseLong(losHelper.getAnOrder("manual"));
            Thread.sleep(10000);

            assignmentServiceHelper.rNr(AssignmentServiceConstants.callde_assignment_reason_id, orderId[i], AssignmentServiceConstants.callde_role_id, assignmentServiceHelper.getOrderPk(orderId[i]));

            // Assign CallDE for an order
            omshelper.deAssistance("" + orderId[i], "CNR");
            Thread.sleep(1000);

            s[i] = String.valueOf(orderId[i]);
            System.out.println(i+"***"+s[i]);

            //Checking order is present in ordersinqueue table
            Assert.assertTrue(assignmentServiceHelper.checkInOrdersInQueueTable(orderId[i], "CallDE_OE"), orderId[i] + "==== is not present in OrdersInQueueTable");

        }

        for (int i = 0; i <= 1; i++) {
            Thread.sleep(5000);
            //1st OE login
            assignmentServiceHelper.oeLoginRMQ(AssignmentServiceConstants.callde_id[i], AssignmentServiceConstants.callde_role_id);
            Thread.sleep(6000);

            //Checking whether order is assigned to OE in mysql table
            log.info(orderId[i] + "=======================" + callde_id[i]);
            Assert.assertTrue(dbHelper.pollDB(OMSConstants.ASSIGNMENT_DB, "SELECT order_id FROM db_assignment.assignment where order_id = " + orderId[i] + " and oe_id = " + callde_id[i] + ";", "order_id", s[i], 10, 90), orderId[i]+ " Order "+i+ " th is not found in DB");

        }

    }

}

