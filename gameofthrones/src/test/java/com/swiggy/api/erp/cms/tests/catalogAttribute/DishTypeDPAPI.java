package com.swiggy.api.erp.cms.tests.catalogAttribute;

import com.swiggy.api.erp.cms.dp.itemDP;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import framework.gameofthrones.Aegon.Initialize;
import org.testng.Assert;
import org.testng.annotations.Test;

public class DishTypeDPAPI extends itemDP {
    Initialize gameofthrones = new Initialize();
    CMSHelper cmsHelper= new CMSHelper();
    @Test(dataProvider = "createitempj",description = "Dish type prediction DP API")
    public void dpDishType(String name){
        int responsecode=cmsHelper.dpAPIDishType(name).ResponseValidator.GetResponseCode();
        Assert.assertEquals(200,responsecode,"Response code is not 200");
    }
}
