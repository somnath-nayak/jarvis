package com.swiggy.api.erp.cms.helper;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ThirdPartyServicesHelper {

	 Initialize gameofthrones = new Initialize();
	 
	 public Processor fetchDOMINOSMenu(String restIds, String typeOfPartner, String mealType) {
		 HashMap<String,String> headers = new HashMap<>();
		 headers.put("Content-Type","application/json");
		 headers.put("Authorization","Basic dXNlcjpjaGVjaw==");
		 
		 GameOfThronesService service = new GameOfThronesService("cmstransformation", "fetchDOMINOSmenu", gameofthrones);
		// List<Integer> restaurantIds=new ArrayList<>();
		 String payload[] = {restIds,typeOfPartner,mealType};
		 Processor processor = new Processor(service,headers, payload);
		return processor;
		 
	 }
	 
	 public Processor fetchMCDMenu(String restIds, String typeOfPartner, String mealType) {
		 HashMap<String,String> headers = new HashMap<>();
		 headers.put("Content-Type","application/json");
		 headers.put("Authorization","Basic dXNlcjpjaGVjaw==");
		 
		 GameOfThronesService service = new GameOfThronesService("cmstransformation", "fetchMCDmenu", gameofthrones);
		 List<Integer> restaurantIds=new ArrayList<>();
		 String payload[] = {restIds,typeOfPartner,mealType};
		 Processor processor = new Processor(service,headers, payload);
		return processor;
		 
	 }
	 
	 public boolean verifyResponse(Processor processor) {
		boolean Status=false;
		Status= processor.ResponseValidator.GetNodeValueAsInt("statusCode")==1;
		return Status;
		 
	 }



}
