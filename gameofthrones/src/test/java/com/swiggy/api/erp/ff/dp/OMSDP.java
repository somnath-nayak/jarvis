package com.swiggy.api.erp.ff.dp;

import org.testng.annotations.DataProvider;

public class OMSDP {
    @DataProvider(name = "session")
    public static Object[][] session() {

        return new Object[][] {
                { "murthy@swiggy.in", "Swiggy@1234"},
                {"rahul.sharma_l1", "Swiggy@123"},
                { "rahul.sharma_l2", "Swiggy@123"},
                {"V1", "Swiggy@123"},
                {"shashank", "Shash@ank1"}

        };
    }
}
