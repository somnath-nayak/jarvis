package com.swiggy.api.erp.ff.dp;

import java.io.IOException;

import org.testng.annotations.DataProvider;

import com.swiggy.api.erp.ff.helper.AssignmentServiceHelper;
import com.swiggy.api.erp.ff.helper.LOSHelper;

public class assignmentService {
	
	AssignmentServiceHelper asHelper = new AssignmentServiceHelper();
	LOSHelper losHelper = new LOSHelper();
	
	@DataProvider (name = "assignmentValidation")
	public Object[][] assignmentValidation() {
		return new Object[][] {
			{35,6,1},
			
		};
	}

	@DataProvider (name = "assignmentValidationUnassignment")
	public Object[][] assignmentValidationUnassignment() {
		return new Object[][] {
				{35,6,1,4}

		};
	}
	
	@DataProvider (name = "L1_L2")
	public Object[][] L1_L2() {
		return new Object[][] {
			{35,6,1},
			
			
		};
	}
	
	@DataProvider (name = "orderStatus")
	public Object[][] orderStatus() {
		return new Object[][] {
			{35,6,1,0},
			
		};
	}
	
	@DataProvider (name = "orderStatusCancelled")
	public Object[][] orderStatusCancelled() {
		return new Object[][] {
			{35,6,1,8},
			
		};
	}
	
	@DataProvider (name = "orderStatusCNR")
	public Object[][] orderStatusCNR() {
		return new Object[][] {
			{35,6,1,6},
			
		};
	}
	
	@DataProvider (name = "orderStatusPlaced")
	public Object[][] orderStatusPlaced() {
		return new Object[][] {
			{35,6,1,1},
			
		};
	}
	
	@DataProvider (name = "E2E")
	public Object[][] E2E() {
		return new Object[][] {
			// verifier, verifier_role_id,  city_id, status_id,  L1,  l1_role_id, L2,  l2_role_id, callDE,  callDE_role_id
			{35,6,1,1,25,1,38,2,31,7}			
		};
	}
	
	@DataProvider (name = "stopDutyValidation")
	public Object[][] stopDutyValidation() {
		return new Object[][] {
			{35,6,1,2},
			{25,1,1,2},
			{38,2,1,2},
			{31,7,1,2}
		};
	}
	
	@DataProvider(name = "oeIds")
	public Object[][] oeIds(){
		asHelper.oeLoginRMQ(35, 6);
		asHelper.oeLoginRMQ(25, 1);
		asHelper.oeLoginRMQ(38, 2);
		asHelper.oeLoginRMQ(31, 7);
//		asHelper.oeLogoutRMQ(35, 6, 4);
		asHelper.oeLogoutRMQ(28, 1, 4);
		asHelper.oeLogoutRMQ(24, 2, 4);
//		asHelper.oeLogoutRMQ(31, 7, 4);
		return new Object[][]{
			{"35", "verifier", "isLoggedIn"},
//			{"35", "verifier", "isLoggedOut"},
			{"25", "L1Placer", "isLoggedIn"},
			{"28", "L1Placer", "isLoggedOut"},
			{"38", "L2Placer", "isLoggedIn"},
			{"24", "L2Placer", "isLoggedOut"},
			{"31","Call-DE", "isLoggedIn"},
//			{"31","Call-DE", "isLoggedOut"},
//			{"incorrect", "incorrect", "incorrect"}
		};
	}
	
	@DataProvider(name = "incorrectDataForCurrentOrderApi")
	public Object[][] incorrectDataForCurrentOrderApi(){
		return new Object[][]{
			{"124231542135"},
			{"incorrect"}
		};
	}
	
	@DataProvider (name = "orderIdsForAssignment")
	public Object[][] orderIdsForAssignment() throws IOException, InterruptedException {
		return new Object[][]{
			{losHelper.getAnOrder("manual"), "6", "Verifier", "35"},
			{losHelper.getAnOrder("manual"), "1", "L1Placer", "25"},
//			{losHelper.getAnOrder("manual"), "2", "L2Placer", "38"},
//			{losHelper.getAnOrder("manual"), "7", "CallDE_OE", "31"}
		};
	}
	
	@DataProvider (name = "orderStarvation")
	public Object[][] orderStarvation() {
		return new Object[][] {
			{35,6,1},
			
		};
	}

}
