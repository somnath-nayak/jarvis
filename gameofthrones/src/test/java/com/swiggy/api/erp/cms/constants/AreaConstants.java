package com.swiggy.api.erp.cms.constants;

/**
 * Created by kiran.j on 3/6/18.
 */
public interface AreaConstants {

    String name = "Test-Area";
    int city_id = 1;
    int enabled = 1;
    int is_open = 1;
    int open_time = 0;
    int close_time = 2359;
    String banner_message = "Test Banner Message";
    String insert_into = "INSERT INTO ";
    String values = " VALUES ";
    String area_table = "area";
    int last_mile_cap = 4;
    float de_closed_multiplier = (float)1.40;
    float de_open_multiplier = (float)1.20;
    String json = "{\"action\": $action, \"model\": $area, \"data\": $data}";
    String insert = "insert";
    String area = "area";
    String area_name = "Area";
    String area_id = "select * from swiggy.area where name like '%";
    String like_end = "%'";
    String id = "id";
    String slug = "bangalore";
    String update = "update";

}
