package com.swiggy.api.erp.cms.dp;

import com.swiggy.api.erp.cms.constants.MenuConstants;
import com.swiggy.api.erp.cms.helper.FullMenuHelper;
import com.swiggy.api.erp.cms.pojo.MainCategory.Entity;
import com.swiggy.api.erp.cms.pojo.MainCategory.MainCategory;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by kiran.j on 2/19/18.
 */
public class MainCategoryDp {

    FullMenuHelper fullMenuHelper = new FullMenuHelper();

    @DataProvider(name = "maincategory")
    public Iterator<Object[]> maincategory() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        MainCategory mainCategory = new MainCategory();
        mainCategory.build();

        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(mainCategory), new String[]{}, MenuConstants.token_id});

        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(mainCategory), new String[]{"401"}, "123"});

        obj.add(new Object[]{"", jsonHelper.getObjectToJSONallowNullValues(mainCategory), new String[]{"404"}, MenuConstants.token_id});

        Entity entity = mainCategory.getEntity();
        entity.setId(null);
        mainCategory.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(mainCategory), MenuConstants.maincatid, MenuConstants.token_id});

        entity = mainCategory.getEntity();
        entity.build();
        entity.setOrder(0);
        mainCategory.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(mainCategory), new String[]{}, MenuConstants.token_id});

        entity = mainCategory.getEntity();
        entity.build();
        entity.setOrder(1);
        mainCategory.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(mainCategory), new String[]{}, MenuConstants.token_id});

        entity = mainCategory.getEntity();
        entity.build();
        entity.setName("");
        mainCategory.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(mainCategory), MenuConstants.maincatname, MenuConstants.token_id});

        entity = mainCategory.getEntity();
        entity.build();
        entity.setId(null);
        mainCategory.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(mainCategory), MenuConstants.maincatid, MenuConstants.token_id});

        entity = mainCategory.getEntity();
        entity.build();
        entity.setName(null);
        mainCategory.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(mainCategory), MenuConstants.maincatname, MenuConstants.token_id});



        return obj.iterator();
    }

    @DataProvider(name = "updatemaincategory")
    public Iterator<Object[]> updatemaincategory() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        MainCategory mainCategory = new MainCategory();
        mainCategory.build();

        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(mainCategory), new String[]{}, MenuConstants.token_id, mainCategory.getEntity().getId()});

        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(mainCategory), new String[]{"401"}, "123", mainCategory.getEntity().getId()});

        obj.add(new Object[]{"", jsonHelper.getObjectToJSONallowNullValues(mainCategory), new String[]{"404"}, MenuConstants.token_id, mainCategory.getEntity().getId()});

        Entity entity = mainCategory.getEntity();
        entity.setId(null);
        mainCategory.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(mainCategory), MenuConstants.maincatid_update, MenuConstants.token_id, MenuConstants.main_category_id});

        entity = mainCategory.getEntity();
        entity.build();
        entity.setOrder(0);
        mainCategory.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(mainCategory), new String[]{}, MenuConstants.token_id, MenuConstants.main_category_id});

        entity = mainCategory.getEntity();
        entity.build();
        entity.setOrder(1);
        mainCategory.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(mainCategory), new String[]{}, MenuConstants.token_id, MenuConstants.main_category_id});

        entity = mainCategory.getEntity();
        entity.build();
        entity.setName("");
        mainCategory.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(mainCategory), MenuConstants.maincatname_update, MenuConstants.token_id, mainCategory.getEntity().getId()});

        entity = mainCategory.getEntity();
        entity.build();
        entity.setId(null);
        mainCategory.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(mainCategory), MenuConstants.maincatid_update, MenuConstants.token_id, MenuConstants.main_category_id});

        entity = mainCategory.getEntity();
        entity.build();
        entity.setName(null);
        mainCategory.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(mainCategory), new String[]{}, MenuConstants.token_id, mainCategory.getEntity().getId()});



        return obj.iterator();
    }
}
