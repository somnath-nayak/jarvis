package com.swiggy.api.erp.ff.tests.telephonyPlatform;

import com.swiggy.api.erp.ff.dp.telephonyPlatform.TelephonyPlatformTestData;
import com.swiggy.api.erp.ff.helper.TelephonyPlatformHelper;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.WireMockHelper;
import org.testng.annotations.*;

import java.io.IOException;
import java.util.HashMap;

public class IvrsRequestAPITests extends TelephonyPlatformTestData{

    TelephonyPlatformHelper helper = new TelephonyPlatformHelper();
    WireMockHelper wireMockHelper = new WireMockHelper();

    @BeforeClass
//    public void startMockServer(){
//        wireMockHelper.startMockServer(6666);
//    }

    @AfterClass
    public void stopMockServer() throws InterruptedException {
//        wireMockHelper.stopMockServer();
//        Thread.sleep(3000);
    }

    @Test(dataProvider = "ivrsRequestData", description = "Generate IVRS calls using telephony platform API")
    public void ivrsRequestAPITests(String clientId,String requestId, String callerReasonId, String calleeId, String appId, HashMap<String, String> expectedResponseMap){
        System.out.println("***************************************** ivrsRequestAPITests started *****************************************");
        Processor response = helper.ivrsRequest(clientId, requestId, callerReasonId, calleeId, appId);
        helper.assertAPIResponses(response, expectedResponseMap);

        if (response.ResponseValidator.GetResponseCode() == 200){
            helper.assertDataInDBAfterHittingAPI(response.ResponseValidator.GetNodeValueAsInt("id"));
        }

        System.out.println("######################################### ivrsRequestAPITests compleated #########################################");
    }

    @Test(dataProvider = "ivrsRequestHysterixData", description = "Test Hysterix functionality if exotel response it delayed")
    public void ivrsRequestAPIHysterixTests(String clientId,String requestId, String callerReasonId, String calleeId, String appId, HashMap<String, String> expectedResponseMap) throws IOException, InterruptedException {
        System.out.println("***************************************** ivrsRequestAPIHysterixTests with  started *****************************************");

        helper.stubExotelCreateIVRSAPIResponse();
        Processor response = helper.ivrsRequest(clientId, requestId, callerReasonId, calleeId, appId);
        System.out.println(response.ResponseValidator.GetBodyAsText());
        Thread.sleep(5000);

        System.out.println("######################################### ivrsRequestAPIHysterixTests with  compleated #########################################");
    }
}
