package com.swiggy.api.erp.delivery.helper;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;

public class DSListingHelper {

	Initialize gameofthrones = new Initialize();
	
	public Processor maxOrderVolumeResolve() throws InterruptedException {
		GameOfThronesService service = new GameOfThronesService(
				"deliverycerebro", "maxordervolumeresolve", gameofthrones);
			Processor processor = new Processor(service);
		return processor;
	}
	public Processor maxOrderCapacityResolve() throws InterruptedException {
		GameOfThronesService service = new GameOfThronesService(
				"deliverycerebro", "maxordercapacityresolve", gameofthrones);
			Processor processor = new Processor(service);
		return processor;
	}

}
