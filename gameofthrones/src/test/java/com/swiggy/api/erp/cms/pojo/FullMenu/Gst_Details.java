package com.swiggy.api.erp.cms.pojo.FullMenu;

import com.swiggy.api.erp.cms.constants.MenuConstants;

/**
 * Created by kiran.j on 2/13/18.
 */
public class Gst_Details {

    private double igst;

    private double sgst;

    private double cgst;

    private boolean inclusive;

    public double getIgst() {
        return igst;
    }

    public void setIgst(double igst) {
        this.igst = igst;
    }

    public double getSgst() {
        return sgst;
    }

    public void setSgst(double sgst) {
        this.sgst = sgst;
    }

    public double getCgst() {
        return cgst;
    }

    public void setCgst(double cgst) {
        this.cgst = cgst;
    }

    public boolean isInclusive() {
        return inclusive;
    }

    public void setInclusive(boolean inclusive) {
        this.inclusive = inclusive;
    }

    public Gst_Details build() {
        setDefaultValues();
        return this;
    }

    public void setDefaultValues() {
       	 this.setCgst(MenuConstants.cgst);
       	 this.setCgst(MenuConstants.sgst);
       	 this.setCgst(MenuConstants.igst);
       
    }

    @Override
    public String toString() {
        return "gst_details{" +
                "igst=" + igst +
                ", sgst=" + sgst +
                ", cgst=" + cgst +
                ", inclusive=" + inclusive +
                '}';
    }
}
