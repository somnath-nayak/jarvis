package com.swiggy.api.erp.cms.pojo.AttributeItem;


import com.swiggy.api.erp.cms.constants.AttributeConstants;


public class CatalogAttributes {
private String[] accompaniments;

private String cutlery;
private String packaging;
private String prep_style;
private Quantity quantity;

private int serves_how_many;
private String spice_level;
private String veg_classifier;

    public String[] getAccompaniments() {
        return accompaniments;
    }

    public void setAccompaniments(String[] accompaniments) {
        this.accompaniments = accompaniments;
    }

    public String getCutlery() {
        return cutlery;
    }

    public void setCutlery(String cutlery) {
        this.cutlery = cutlery;
    }

    public String getPackaging() {
        return packaging;
    }

    public void setPackaging(String packaging) {
        this.packaging = packaging;
    }

    public String getPrep_style() {
        return prep_style;
    }

    public void setPrep_style(String prep_style) {
        this.prep_style = prep_style;
    }

    public Quantity getQuantity() {
        return quantity;
    }

    public void setQuantity(Quantity quantity) {
        this.quantity = quantity;
    }

    public int getServes_how_many() {
        return serves_how_many;
    }

    public void setServes_how_many(int serves_how_many) {
        this.serves_how_many = serves_how_many;
    }

    public String getSpice_level() {
        return spice_level;
    }

    public void setSpice_level(String spice_level) {
        this.spice_level = spice_level;
    }

    public String getVeg_classifier() {
        return veg_classifier;
    }

    public void setVeg_classifier(String veg_classifier) {
        this.veg_classifier = veg_classifier;
    }

    public CatalogAttributes build(){
        setDefaultValues();
        return this;
    }

    public void setDefaultValues(){
        Quantity quantity=new Quantity();
        quantity.build();
        if(this.getAccompaniments()==null){
           this.setAccompaniments(AttributeConstants.accompaniments);
           //new String[]{"Chutney"}
        }
        if(this.getQuantity()==null){
            this.setQuantity(quantity);
        }

        if(this.getServes_how_many()==0){
            this.setServes_how_many(AttributeConstants.serves_how_many);
        }
        if(this.getCutlery()==null){
            this.setCutlery(AttributeConstants.cutlery);

        }
        if(this.getPackaging()==null){
            this.setPackaging(AttributeConstants.packaging);
        }
        if(this.getPrep_style()==null){
            this.setPrep_style(AttributeConstants.prep_style);
        }
        if(this.getVeg_classifier()==null){
            this.setVeg_classifier(AttributeConstants.veg_classifier);
        }

        if(this.getSpice_level()==null){
            this.setSpice_level(AttributeConstants.spice_level);
        }

    }

    @Override
    public String toString()
    {
        return "ClassPojo [accompaniments = "+accompaniments+",cutlery="+cutlery+",packaging="+packaging+",prep_style="+prep_style+",quantity="+quantity+",serves_how_many="+serves_how_many+",spice_level="+spice_level+",veg_classifier = "+veg_classifier+"]";
    }


}
