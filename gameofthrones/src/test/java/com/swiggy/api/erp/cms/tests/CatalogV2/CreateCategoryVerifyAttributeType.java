package com.swiggy.api.erp.cms.tests.CatalogV2;
import com.swiggy.api.erp.cms.helper.CatalogV2Helper;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CreateCategoryVerifyAttributeType {
    CatalogV2Helper catalogV2Helper=new CatalogV2Helper();
    @Test(description = "Verify the attribute type with allowed values")
    public void verifyAttributeType(){
        String categoryresponse=catalogV2Helper.createCategoryAttributeType().ResponseValidator.GetBodyAsText();
        Assert.assertEquals(categoryresponse,"STRING allowed values Should have type as STRING ");
    }
}
