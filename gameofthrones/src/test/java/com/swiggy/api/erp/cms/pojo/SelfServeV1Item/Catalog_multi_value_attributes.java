package com.swiggy.api.erp.cms.pojo.SelfServeV1Item;

public class Catalog_multi_value_attributes {
	private String attribute_type;

    private String[] attributes;

    private String category;

    public String getAttribute_type ()
    {
        return attribute_type;
    }

    public void setAttribute_type (String attribute_type)
    {
        this.attribute_type = attribute_type;
    }

    public String[] getAttributes ()
    {
        return attributes;
    }

    public void setAttributes (String[] attributes)
    {
        this.attributes = attributes;
    }

    public String getCategory ()
    {
        return category;
    }

    public void setCategory (String category)
    {
        this.category = category;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [attribute_type = "+attribute_type+", attributes = "+attributes+", category = "+category+"]";
    }
}
			
			