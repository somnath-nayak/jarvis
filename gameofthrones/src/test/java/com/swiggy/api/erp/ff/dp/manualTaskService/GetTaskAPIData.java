package com.swiggy.api.erp.ff.dp.manualTaskService;

import com.swiggy.api.erp.ff.constants.ManualTaskServiceConstants;
import com.swiggy.api.erp.ff.helper.LOSHelper;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.util.HashMap;

public class GetTaskAPIData implements ManualTaskServiceConstants {

    LOSHelper losHelper = new LOSHelper();

    @DataProvider(name = "getTaskData")
    public Object[][] getTaskData() throws IOException, InterruptedException {

        HashMap<String, String> successCase = new HashMap<>();
        successCase.put("responseCode", "200");
        successCase.put("statusCode", "0");
        successCase.put("statusMessage", "Success");
        successCase.put("status", "active");

        HashMap<String, String> badRequestCase = new HashMap<>();
        successCase.put("expectedResponse", "200");
        successCase.put("statusCode", "0");
        successCase.put("statusMessage", "message");
        successCase.put("status", "active");

        return new Object[][]{
                {losHelper.getAnOrder("manual"), REASON_MANUAL_PLACING_ORDER_L1, "null", "TestSource", successCase},
                {losHelper.getAnOrder("partner"), REASON_MANUAL_PLACING_ORDER_L1, "null", "TestSource", successCase},
                {losHelper.getAnOrder("thirdparty"), REASON_MANUAL_PLACING_ORDER_L1, "null", "TestSource", successCase}
                
                

        };
    }
}
