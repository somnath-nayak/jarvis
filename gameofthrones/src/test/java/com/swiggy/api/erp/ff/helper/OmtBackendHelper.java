package com.swiggy.api.erp.ff.helper;

import com.swiggy.api.erp.ff.constants.OmtConstants;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;

public class OmtBackendHelper {

    Initialize gameofthrones = new Initialize();

    public Processor oldCustomerNewArea(String email, String mobile, String area){
        GameOfThronesService service = new GameOfThronesService(OmtConstants.SERVICE_BACKEND, "oldcustomernewarea", gameofthrones);
        Processor processor = new Processor(null, null, new String[]{email, mobile, area}, null, service);
        return processor;
    }
    
	public Processor filterOrders(String restaurantId){
    	String[] urlParams = {restaurantId};
    	GameOfThronesService service = new GameOfThronesService(OmtConstants.SERVICE_BACKEND, "filterorder", gameofthrones);
    	Processor processor = new Processor(null, null, urlParams, null, service);
		return processor;
	}

}
