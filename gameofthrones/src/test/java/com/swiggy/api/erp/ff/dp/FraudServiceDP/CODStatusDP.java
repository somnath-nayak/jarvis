package com.swiggy.api.erp.ff.dp.FraudServiceDP;

import com.swiggy.api.erp.ff.POJO.CartDetails;
import com.swiggy.api.erp.ff.POJO.CustomerDetails;
import com.swiggy.api.erp.ff.POJO.DeliveryDetails;
import com.swiggy.api.erp.ff.POJO.SessionDetails;
import com.swiggy.api.erp.ff.constants.FraudServiceConstants;
import com.swiggy.api.erp.ff.helper.FraudServiceHelper;
import framework.gameofthrones.Tyrion.WireMockHelper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CODStatusDP {




        CustomerDetails cd,cd4,cd2,cd3;
        SessionDetails sd;
        DeliveryDetails dd,dd2,dd3,dd4;
        CartDetails cartd,cartd2,cartd3;
        String rId;

        WireMockHelper wireMockHelper = new WireMockHelper();
        FraudServiceHelper fsh = new FraudServiceHelper();

        Object[][] getPendingCancellationFeeData() {


            try {
                fsh.mockDeliveryBoyResponse(FraudServiceConstants.statusCode);
                fsh.mockDSResponse(FraudServiceConstants.statusCode, FraudServiceConstants.predication_0,FraudServiceConstants.status_1);
            } catch (IOException e) {
                e.printStackTrace();
            }




            rId = "Testing";
            List<String> requestIds = new ArrayList<>();

            requestIds.add(rId);

            cd = new CustomerDetails();
            cd.setCustomerId(1);
            cd.setPendingCancellationFee(0);
            cd.setSwiggyMoneyUsed(0.0);
            cd.setMobileNo(123456789);

            cd2 = new CustomerDetails();
            cd2.setCustomerId(12);
            cd2.setPendingCancellationFee(20);
            cd2.setSwiggyMoneyUsed(0.0);
            cd2.setMobileNo(123456789);

            cd3 = new CustomerDetails();
            cd3.setCustomerId(123);
            cd3.setPendingCancellationFee(20);
            cd3.setSwiggyMoneyUsed(30.0);
            cd3.setMobileNo(123456789);

            cd4 = new CustomerDetails();
            cd4.setCustomerId(1234);
            cd4.setPendingCancellationFee(20);
            cd4.setSwiggyMoneyUsed(10.0);
            cd4.setMobileNo(123456789);

            List<CustomerDetails> customerDetails = new ArrayList<>();
            customerDetails.add(cd);
            customerDetails.add(cd2);
            customerDetails.add(cd3);
            customerDetails.add(cd4);

            sd = new SessionDetails();
            sd.setDeviceId("abcde-fghij-klmno");
            sd.setTimestamp("2018-07-03 13:56:00");
            sd.setUserAgent("Swiggy-Android");

            List<SessionDetails> sessionDetails = new ArrayList<>();
            sessionDetails.add(sd);

            dd = new DeliveryDetails();
            dd.setAreaId(2);
            dd.setCityId(1);
            dd.setCustomerGeohash("abcde");
            dd.setRestaurantCustomerDistanceKms("1.45");

            List<DeliveryDetails> deliveryDetails = new ArrayList<>();
            deliveryDetails.add(dd);

            cartd = new CartDetails();
            cartd.setBillAmount(345);
            cartd.setItemCount(3);
            cartd.setItemQuantity(10);
            cartd.setRestaurantId("12345");

            List<CartDetails> cartDetails = new ArrayList<>();
            cartDetails.add(cartd);

            return dataProviderGenerator.generatevariants2(requestIds,customerDetails,sessionDetails,deliveryDetails,cartDetails);

        }


        Object[][] getCityConfiguration() {


            try {
                fsh.mockDeliveryBoyResponse(FraudServiceConstants.statusCode);
                fsh.mockDSResponse(FraudServiceConstants.statusCode, FraudServiceConstants.predication_0,FraudServiceConstants.status_1);
            } catch (IOException e) {
                e.printStackTrace();
            }





            rId = "Testing2";
            List<String> requestIds = new ArrayList<>();

            requestIds.add(rId);

            cd = new CustomerDetails();
            cd.setCustomerId(123456);
            cd.setPendingCancellationFee(0);
            cd.setSwiggyMoneyUsed(0.0);
            cd.setMobileNo(123456789);

            List<CustomerDetails> customerDetails = new ArrayList<>();
            customerDetails.add(cd);


            sd = new SessionDetails();
            sd.setDeviceId("abcde-fghij-klmno");
            sd.setTimestamp("2018-07-03 13:56:00");
            sd.setUserAgent("Swiggy-Android");

            List<SessionDetails> sessionDetails = new ArrayList<>();
            sessionDetails.add(sd);

            dd = new DeliveryDetails();
            dd.setAreaId(2);
            dd.setCityId(1);
            dd.setCustomerGeohash("abcde");
            dd.setRestaurantCustomerDistanceKms("1.45");

            dd2 = new DeliveryDetails();
            dd2.setAreaId(2);
            dd2.setCityId(5);
            dd2.setCustomerGeohash("abcde");
            dd2.setRestaurantCustomerDistanceKms("1.45");


            dd3 = new DeliveryDetails();
            dd3.setAreaId(2);
            dd3.setCityId(6);
            dd3.setCustomerGeohash("abcde");
            dd3.setRestaurantCustomerDistanceKms("1.45");

            dd4 = new DeliveryDetails();
            dd4.setAreaId(2);
            dd4.setCityId(7);
            dd4.setCustomerGeohash("abcde");
            dd4.setRestaurantCustomerDistanceKms("1.45");

            List<DeliveryDetails> deliveryDetails = new ArrayList<>();
            deliveryDetails.add(dd);
            deliveryDetails.add(dd2);
            deliveryDetails.add(dd3);
            deliveryDetails.add(dd4);

            cartd = new CartDetails();
            cartd.setBillAmount(300);
            cartd.setItemCount(3);
            cartd.setItemQuantity(10);
            cartd.setRestaurantId("12345");

            cartd2 = new CartDetails();
            cartd2.setBillAmount(500);
            cartd2.setItemCount(3);
            cartd2.setItemQuantity(10);
            cartd2.setRestaurantId("12345");

            cartd3 = new CartDetails();
            cartd3.setBillAmount(200);
            cartd3.setItemCount(3);
            cartd3.setItemQuantity(10);
            cartd3.setRestaurantId("12345");


            List<CartDetails> cartDetails = new ArrayList<>();
            cartDetails.add(cartd);
            cartDetails.add(cartd2);
            cartDetails.add(cartd3);

            return dataProviderGenerator.generatevariants2(requestIds,customerDetails,sessionDetails,deliveryDetails,cartDetails);

        }


        Object[][] getPendingFeeCityConfiguration() {


            try {
                fsh.mockDeliveryBoyResponse(FraudServiceConstants.statusCode);
                fsh.mockDSResponse(FraudServiceConstants.statusCode, FraudServiceConstants.predication_1,FraudServiceConstants.status_1);
            } catch (IOException e) {
                e.printStackTrace();
            }




            rId = "Testing2";
            List<String> requestIds = new ArrayList<>();

            requestIds.add(rId);

            cd = new CustomerDetails();
            cd.setCustomerId(12345);
            cd.setPendingCancellationFee(10);
            cd.setSwiggyMoneyUsed(10.0);
            cd.setMobileNo(123456789);
            List<CustomerDetails> customerDetails = new ArrayList<>();
            customerDetails.add(cd);


            sd = new SessionDetails();
            sd.setDeviceId("abcde-fghij-klmno");
            sd.setTimestamp("2018-07-03 13:56:00");
            sd.setUserAgent("Swiggy-Android");

            List<SessionDetails> sessionDetails = new ArrayList<>();
            sessionDetails.add(sd);

            dd2 = new DeliveryDetails();
            dd2.setAreaId(2);
            dd2.setCityId(5);
            dd2.setCustomerGeohash("abcde");
            dd2.setRestaurantCustomerDistanceKms("1.45");


            List<DeliveryDetails> deliveryDetails = new ArrayList<>();

            deliveryDetails.add(dd2);

            cartd = new CartDetails();
            cartd.setBillAmount(400);
            cartd.setItemCount(3);
            cartd.setItemQuantity(10);
            cartd.setRestaurantId("12345");


            List<CartDetails> cartDetails = new ArrayList<>();
            cartDetails.add(cartd);

            return dataProviderGenerator.generatevariants2(requestIds,customerDetails,sessionDetails,deliveryDetails,cartDetails);

        }





        Object[][] getExclustionListData() {

            try {
                fsh.mockDeliveryBoyResponse(FraudServiceConstants.statusCode);
                fsh.mockDSResponse(FraudServiceConstants.statusCode, FraudServiceConstants.predication_1,FraudServiceConstants.status_1);
            } catch (IOException e) {
                e.printStackTrace();
            }



            rId = "Testing";
            List<String> requestIds = new ArrayList<>();

            requestIds.add(rId);

            cd = new CustomerDetails();
            cd.setCustomerId(292929);
            cd.setPendingCancellationFee(10);
            cd.setSwiggyMoneyUsed(0.0);
            cd.setMobileNo(123456789);




            List<CustomerDetails> customerDetails = new ArrayList<>();
            customerDetails.add(cd);


            sd = new SessionDetails();
            sd.setDeviceId("abcde-fghij-klmno");
            sd.setTimestamp("2018-07-03 13:56:00");
            sd.setUserAgent("Swiggy-Android");

            List<SessionDetails> sessionDetails = new ArrayList<>();
            sessionDetails.add(sd);

            dd = new DeliveryDetails();
            dd.setAreaId(2);
            dd.setCityId(5);
            dd.setCustomerGeohash("abcde");
            dd.setRestaurantCustomerDistanceKms("1.45");


            List<DeliveryDetails> deliveryDetails = new ArrayList<>();
            deliveryDetails.add(dd);

            cartd = new CartDetails();
            cartd.setBillAmount(345);
            cartd.setItemCount(3);
            cartd.setItemQuantity(10);
            cartd.setRestaurantId("12345");

            List<CartDetails> cartDetails = new ArrayList<>();
            cartDetails.add(cartd);

            return dataProviderGenerator.generatevariants2(requestIds,customerDetails,sessionDetails,deliveryDetails,cartDetails);

        }


        Object[][] getDSCheckData() {
            rId = "Testing";
            List<String> requestIds = new ArrayList<>();

            requestIds.add(rId);

            cd = new CustomerDetails();
            cd.setCustomerId(1234);
            cd.setPendingCancellationFee(0);
            cd.setSwiggyMoneyUsed(0.0);
            cd.setMobileNo(123456789);



            List<CustomerDetails> customerDetails = new ArrayList<>();
            customerDetails.add(cd);


            sd = new SessionDetails();
            sd.setDeviceId("abcde-fghij-klmno");
            sd.setTimestamp("2018-07-03 13:56:00");
            sd.setUserAgent("Swiggy-Android");

            List<SessionDetails> sessionDetails = new ArrayList<>();
            sessionDetails.add(sd);

            dd = new DeliveryDetails();
            dd.setAreaId(2);
            dd.setCityId(1);
            dd.setCustomerGeohash("abcde");
            dd.setRestaurantCustomerDistanceKms("1.45");

            List<DeliveryDetails> deliveryDetails = new ArrayList<>();
            deliveryDetails.add(dd);

            cartd = new CartDetails();
            cartd.setBillAmount(345);
            cartd.setItemCount(3);
            cartd.setItemQuantity(10);
            cartd.setRestaurantId("12345");

            List<CartDetails> cartDetails = new ArrayList<>();
            cartDetails.add(cartd);

            return dataProviderGenerator.generatevariants2(requestIds,customerDetails,sessionDetails,deliveryDetails,cartDetails);

        }


        Object[][] getFieldLevelData() {
       List<String>customerIds = new ArrayList<>();
       customerIds.add("");
       customerIds.add("null");
       customerIds.add("12.12");
       customerIds.add("abac");
       customerIds.add("7878787878");
       return dataProviderGenerator.generatevariants2(customerIds);

    }


}