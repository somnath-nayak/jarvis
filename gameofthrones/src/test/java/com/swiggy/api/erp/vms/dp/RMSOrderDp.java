package com.swiggy.api.erp.vms.dp;

import com.swiggy.api.erp.vms.constants.RMSConstants;
import com.swiggy.api.erp.vms.helper.RMSCommonHelper;
import org.testng.annotations.DataProvider;
import java.io.IOException;
import java.util.*;

public class RMSOrderDp extends RMSCommonHelper {

	static HashMap<String, String> m = new HashMap<>();
	static String restaurantId = null;
	static String restaurantId1 = null;
	static String cities = null;
	public static String VENDOR_PASSWORD = null;
	public static String VENDOR_USERNAME = null;
	public static String OMS_PASSWORD = null;
	public static String OMS_USERNAME = null;
	public static String de_id = null;

	static {
		try {
			if (getEnv().equalsIgnoreCase("stage1")) {
				VENDOR_PASSWORD = "El3ment@ry";
				VENDOR_USERNAME = "5271";
				restaurantId = "5271";
				restaurantId1 = "9990";
				cities = "7";
				OMS_PASSWORD = "Test@12345";
				OMS_USERNAME = "ramzi@swiggy.in";
				de_id = "276767";
			} else if (getEnv().equalsIgnoreCase("stage2")) {
				VENDOR_PASSWORD = "imran@123";
				VENDOR_USERNAME = "9990";
				restaurantId = "5271";
				restaurantId1 = "9990";
				cities = "7";
				OMS_PASSWORD = "Admin@123";
				OMS_USERNAME = "murthy@swiggy.in";
				de_id = "276767";
			} else if (getEnv().equalsIgnoreCase("stage3")) {
				VENDOR_PASSWORD = "El3ment@ry";
				VENDOR_USERNAME = "5271";
				restaurantId = "5271";
				restaurantId1 = "9990";
				cities = "7";
				//OMS_PASSWORD = "Admin@123";
				//OMS_USERNAME = "murthy@swiggy.in";
				OMS_PASSWORD = "Swiggy@123";
				OMS_USERNAME = "ranganath";
				de_id = "359900";
			} else if (getEnv().equalsIgnoreCase("prod")) {
				VENDOR_PASSWORD = "$w199y@zolb";
				VENDOR_USERNAME = "9738948943";
				restaurantId = "9990";
				restaurantId1 = "4993";
				cities = "7";
				OMS_PASSWORD = "Sha@shank1";
				OMS_USERNAME = "shashank";
				de_id = "276767";
			}
			m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@DataProvider(name = "fetchOrder")
	public static Object[][] fetchOrder() throws Exception {

		String OrderId = RMSCommonHelper.createOrder(OMS_USERNAME, OMS_PASSWORD);
		return new Object[][]{{m.get("accessToken"), restaurantId, "Success", 0, OrderId}};
	}

	@DataProvider(name = "fetchOrder_regression")
	public static Object[][] fetchOrder_regression() {

		return new Object[][] { { m.get("accessToken"), "223", "don't have permission for requested restaurants", -5 },
				{ m.get("accessToken"), "5272", "don't have permission for requested restaurants", -5 } };
	}

	@DataProvider(name = "getOldOrderHistory")
	public static Object[][] getOldOrderdHistory() throws Exception {

		return new Object[][] { { m.get("accessToken"), restaurantId, 0, "Order History Fetched Successfully" }};
	}

	@DataProvider(name = "getOldOrderdHistory_regression")
	public static Object[][] getOldOrderdHistory_regression() {

		return new Object[][] { { m.get("accessToken"), "123", -3, "Invalid Session" } };
	}

	@DataProvider(name = "getOrderedHistory")
	public static Object[][] getOrderedHistory() throws Exception {
		return new Object[][] {
				{ m.get("accessToken"), restaurantId, "20", "0", RMSConstants.startDate(-90),
						RMSConstants.endDate(-1), "null", 0, "Order History Fetched Successfully" },
				{ m.get("accessToken"), restaurantId, "10", "0", RMSConstants.startDate(-90),
						RMSConstants.endDate(-1), "null", 0, "Order History Fetched Successfully" } };
	}

	@DataProvider(name = "getOrderedHistory_regression")
	public static Object[][] getOrderedHistory_regression() {

		return new Object[][] {
				{ m.get("accessToken"), "123", "20", "0", RMSConstants.startDate(-366), RMSConstants.endDate(-367),
						"null", -3, "Invalid Session" },
				{ m.get("accessToken"), "5271", "1", "1", RMSConstants.startDate(-90), RMSConstants.endDate(-7),
						"null", 0, "Order History Fetched Successfully" },
				{ m.get("accessToken"), "5271", "20", "0", RMSConstants.startDate(-30), RMSConstants.endDate(-0),
						"null", 0, "Order History Fetched Successfully" },
				{ m.get("accessToken"), "5271", "20", "0", RMSConstants.startDate(-60), RMSConstants.endDate(-1),
						"2", 0, "Order History Fetched Successfully" },
				{ m.get("accessToken"), "123", "1", "3", RMSConstants.startDate(-370), RMSConstants.endDate(-367), "3",
						-3, "Invalid Session" } };
	}

	@DataProvider(name = "getOrderHistoryCSV")
	public static Object[][] getOrderHistoryCSV() throws Exception {
		return new Object[][] {
				{ m.get("accessToken"), restaurantId, "false", RMSConstants.startDate(-60) + "T" + "20:00:00",
						RMSConstants.startDate(-1) + "T" + "20:00:00", RMSConstants.startDate(-0) + "T" + "20:37:00",
						0, "FF successfully called to generate CSV" },
				{ m.get("accessToken"), restaurantId, "true", RMSConstants.startDate(-60) + "T" + "20:00:00",
						RMSConstants.startDate(-1) + "T" + "20:00:00", RMSConstants.startDate(-0) + "T" + "20:37:00",
						0, "FF successfully called to generate CSV" }
		};

	}

	@DataProvider(name = "getOrderHistoryCSV_regression")
	public static Object[][] getOrderHistoryCSV_regression() {

		return new Object[][] {
				{ m.get("accessToken"), "123", "false", RMSConstants.startDate(-60) + "T" + "20:00:00",
						RMSConstants.startDate(-59) + "T" + "20:00:00", RMSConstants.startDate(-59) + "T" + "20:37:00",
						-3, "Invalid Session" },
				{ m.get("accessToken"), "123", "true", RMSConstants.startDate(-50) + "T" + "20:00:00",
						RMSConstants.startDate(-59) + "T" + "20:00:00", RMSConstants.startDate(-60) + "T" + "00:37:00",
						-3, "Invalid Session" } };
	}

	@DataProvider(name = "getorderHistoryMultiChain")
	public static Object[][] getorderHistoryMultiChain() throws Exception {

		ArrayList<String> lst1 = new ArrayList<String>();
		lst1.add(restaurantId);

		ArrayList<String> lst2 = new ArrayList<String>();
		lst2.add(restaurantId);
		lst2.add(restaurantId);

		return new Object[][] {
				{ m.get("accessToken"), lst1, 0, "Order History Fetched Successfully" },
				{ m.get("accessToken"), lst2, 0, "Order History Fetched Successfully" }
		};
	}

	@DataProvider(name = "getorderHistoryMultiChain_regression")
	public static Object[][] getorderHistoryMultiChain_regression() {

		ArrayList<String> lst1 = new ArrayList<String>();
		lst1.add("123");
		lst1.add("9990");
		ArrayList<String> lst3 = new ArrayList<String>();
		lst3.addAll(lst1);
		lst3.add(restaurantId);
		lst3.add("9990");
		lst3.add("7158");
		ArrayList<String> lst2 = new ArrayList<String>();
		lst2.add("0");

		return new Object[][] { { m.get("accessToken"), lst1, -3, "Invalid Session" },
				{ m.get("accessToken"), lst3, -3, "Invalid Session" },
				{ m.get("accessToken"), lst2, -3, "Invalid Session" } };

	}

	@DataProvider(name = "callback")
	public static Object[][] callback() throws Exception {

		String OrderId = RMSCommonHelper.createOrder(OMS_USERNAME, OMS_PASSWORD);
		return new Object[][] { { m.get("accessToken"), restaurantId, OrderId, 0, "Call Requested Successfully" } };
	}

	@DataProvider(name = "callback_regression")
	public static Object[][] callback_regression() throws Exception {

		return new Object[][] { { m.get("accessToken"), restaurantId1, "14383177365", -2, "Order does not belong to this restaurant" },
				{ m.get("accessToken"), "9990", "14383177365", -2, "Order does not belong to this restaurant" },
				{ m.get("accessToken"), "0", "0", -1, "Invalid Input" } };
	}

	@DataProvider(name = "dispatch")
	public static Object[][] dispatch() throws Exception {

		String OrderId = RMSCommonHelper.createOrder(OMS_USERNAME, OMS_PASSWORD);
		return new Object[][] { { m.get("accessToken"), restaurantId, OrderId, 0, "Status Updated Successfully" } };
	}

	@DataProvider(name = "confirmOrder")
	public Object[][] confirmOrder() throws  Exception {

		String OrderId = RMSCommonHelper.createOrder(OMS_USERNAME, OMS_PASSWORD);
		return new Object[][] { { m.get("accessToken"), restaurantId, OrderId,  "Success", 0 } };
	}

	@DataProvider(name = "confirmOrderWithPrepTime")
	public Object[][] confirmOrderWithPrepTime() throws  Exception {

		String OrderId = RMSCommonHelper.createOrder(OMS_USERNAME, OMS_PASSWORD);
		return new Object[][] { { m.get("accessToken"), restaurantId, OrderId, "5" , "Placing Event captured successfully", 0 } };
	}

	@DataProvider(name = "foodPrepared")
	public Object[][] foodPrepared() throws Exception{

		String OrderId = RMSCommonHelper.createOrder(OMS_USERNAME, OMS_PASSWORD);
		return new Object[][] {{ m.get("accessToken"), OrderId, restaurantId, "Marked food prepared" } };

	}

	@DataProvider(name = "foodPreparedRegression")
	public Object[][] foodPreparedRegression() throws Exception{

		return new Object[][] {{ "Yhdvhjgq-==wdn", "1234", restaurantId, "Order does not belong to this restaurant" }};
	}

	@DataProvider(name = "dscallout_Dp")
	public static Object[][] dscallout_Dp() throws Exception{

		String OrderId = RMSCommonHelper.createOrder(OMS_USERNAME, OMS_PASSWORD);
		return new Object[][] {{ "5271", OrderId, RMSConstants.DS_statusMessage } };
	}

	@DataProvider(name = "dscallout_Dp1")
	public static Object[][] dscallout_Dp1() {

		return new Object[][] {
				{ "9990", "12345678", -2, "Order does not belong to this restaurant" },
				{ "", "12345678", -1, "Invalid Input" },
				{ "9990", "", -1, "Invalid Input" },
				{ "", "", -1, "Invalid Input" }
		};
	}

	@DataProvider(name = "trackOrder")
	public Object[][] trackOrder() throws Exception {

		String OrderId = RMSCommonHelper.createOrder(OMS_USERNAME, OMS_PASSWORD);
		return new Object[][] { { m.get("accessToken"), restaurantId, OrderId,  "Done Successfully"} };
	}

	@DataProvider(name = "trackOrderRegression")
	public Object[][] trackOrderRegression() throws Exception {

		return new Object[][] {
				{ m.get("accessToken"), "", "1234567890",  "Done Successfully"},
				{ m.get("accessToken"), restaurantId, "",  "Done Successfully"},
				{ m.get("accessToken"), restaurantId, "abcd",  "Done Successfully"},
				{ "Ygvdquy2t==web", restaurantId, "1234567890",  "Invalid Session"}
		};
	}

	@DataProvider(name = "locateOrderRegression")
	public Object[][] locateOrderRegression() throws Exception {

		return new Object[][] {
				{ m.get("accessToken"), "", "1234567890",  "Invalid input"},
				{ m.get("accessToken"), restaurantId, "abcd",  "Unable to locate order"},
				{ "Y671etd2n-==-ewns", restaurantId, "1234567890",  "Invalid Session"}
		};
	}

	@DataProvider(name = "getDispositions")
	public static Object[][] getDispositions() {

		return new Object[][] {{ m.get("accessToken"), cities, restaurantId, "success"}};
	}

	@DataProvider(name = "getDispositionsRegression")
	public static Object[][] getDispositionsRegression() {

		return new Object[][] {
				{ "Y389nHD2hd2id=nd4u", cities, restaurantId, -3, "Invalid Session"},
				{ m.get("accessToken"), "", restaurantId, -1, "Invalid Input"},
				{ m.get("accessToken"), cities, "", -1, "Invalid Input"},
				{ m.get("accessToken"), "1009", restaurantId, -4, "Unable to fetch disposition list"}
		};
	}

	@DataProvider(name = "getActiveOrders")
	public Object[][] getActiveOrders() throws Exception {

		String OrderId = RMSCommonHelper.createOrder(OMS_USERNAME, OMS_PASSWORD);
		return new Object[][] { { OrderId,  "Done Successfully"} };
	}

	@DataProvider(name = "withPartner")
	public Object[][] withPartner() throws Exception{

		String OrderId = RMSCommonHelper.createOrder(OMS_USERNAME, OMS_PASSWORD);
		return new Object[][] {{ OrderId, restaurantId, "Successfully updated withPartnerTimestamp" }};

	}

	@DataProvider(name = "withPartnerRegression")
	public Object[][] withPartnerRegression() throws Exception{

		return new Object[][] {
				{ "", restaurantId, "Invalid Input" },
				{ "1234567890", "", "Invalid Input" }
		};

	}

	@DataProvider(name = "firstAcceptance")
	public Object[][] firstAcceptance() throws Exception{

		String OrderId = RMSCommonHelper.createOrder(OMS_USERNAME, OMS_PASSWORD);
		return new Object[][] {{ OrderId, restaurantId, "CONFIRM", "Attempted to update firstAcceptanceTimestamp" }};

	}

	@DataProvider(name = "firstAcceptanceRegression")
	public Object[][] firstAcceptanceRegression() throws Exception{

		return new Object[][] {
				{ "", restaurantId, "CONFIRM", "Invalid Input" },
				{ "1234", "", "CONFIRM", "Invalid Input" },
				{ "1234", restaurantId, "", "Invalid Input" }
		};
	}

	@DataProvider(name = "markFoodPrepared")
	public Object[][] markFoodPrepared() throws Exception{

		String OrderId = RMSCommonHelper.createOrder(OMS_USERNAME, OMS_PASSWORD);
		return new Object[][] {{ OrderId, restaurantId, "Marked food prepared" }};
	}

	@DataProvider(name = "markFoodPreparedRegression")
	public Object[][] markFoodPreparedRegression() throws Exception{

		return new Object[][] {
				{ "", restaurantId, "Invalid Input" },
				{ "1234", "", "Invalid Input" }
		};
	}

	@DataProvider(name = "trackAllOrders")
	public Object[][] trackAllOrders() throws Exception {
		String OrderId = RMSCommonHelper.createOrder(OMS_USERNAME, OMS_PASSWORD);
		return new Object[][] { {OrderId,  "Done Successfully"} };
	}

	@DataProvider(name = "orderCAPRD")
	public Object[][] orderCAPRD() throws Exception {

		String OrderId = RMSCommonHelper.createOrder(OMS_USERNAME, OMS_PASSWORD);
		return new Object[][]{{m.get("accessToken"), restaurantId, OrderId}};
	}

}
