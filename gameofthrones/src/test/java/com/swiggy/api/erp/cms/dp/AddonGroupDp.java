package com.swiggy.api.erp.cms.dp;

import com.swiggy.api.erp.cms.constants.MenuConstants;
import com.swiggy.api.erp.cms.helper.FullMenuHelper;
import com.swiggy.api.erp.cms.pojo.AddonGroup.AddonGroup;
import com.swiggy.api.erp.cms.pojo.AddonGroup.Entity;
import com.swiggy.api.erp.cms.pojo.BulkAddonGroup.BulkAddonGroup;
import com.swiggy.api.erp.cms.pojo.BulkAddonGroup.Entities;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by kiran.j on 2/21/18.
 */
public class AddonGroupDp {


    @DataProvider(name = "createaddongroup")
    public Iterator<Object[]> createaddongroup() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        AddonGroup addonGroup = new AddonGroup();
        addonGroup.build();

        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(addonGroup), new String[]{}, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2)});

        obj.add(new Object[]{"", jsonHelper.getObjectToJSON(addonGroup), new String[]{"404"}, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2)});

        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(addonGroup), new String[]{"400"}, MenuConstants.token_id});

        Entity entity = new Entity();
        entity.build();
        entity.setId("");
        addonGroup.setEntity(entity);

        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(addonGroup), MenuConstants.createaddongroup_id, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2)});

        entity = new Entity();
        entity.build();
        entity.setItem_id("");
        addonGroup.setEntity(entity);

        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(addonGroup), MenuConstants.createaddongroup_itemid, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2)});

        entity = new Entity();
        entity.build();
        entity.setName("");
        addonGroup.setEntity(entity);

        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(addonGroup), MenuConstants.createaddongroup_name, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2)});

        entity = new Entity();
        entity.build();
        entity.setAddon_min_limit(0);
        addonGroup.setEntity(entity);

        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(addonGroup), new String[]{}, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2)});

        return obj.iterator();
    }

    @DataProvider(name = "updateaddongroup")
    public Iterator<Object[]> updateaddongroup() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        AddonGroup addonGroup = new AddonGroup();
        addonGroup.build();

        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(addonGroup), new String[]{}, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2), addonGroup.getEntity().getId()});

        obj.add(new Object[]{"", jsonHelper.getObjectToJSON(addonGroup), new String[]{"404"}, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2), addonGroup.getEntity().getId()});

        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(addonGroup), new String[]{"400"}, MenuConstants.token_id, addonGroup.getEntity().getId()});

        Entity entity = new Entity();
        entity.build();
        entity.setId("");
        addonGroup.setEntity(entity);

        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(addonGroup), MenuConstants.updateaddongroup_id, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2), "123"});

        entity = new Entity();
        entity.build();
        entity.setItem_id("");
        addonGroup.setEntity(entity);

        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(addonGroup), MenuConstants.createaddongroup_itemid, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2), addonGroup.getEntity().getId()});

        entity = new Entity();
        entity.build();
        entity.setName("");
        addonGroup.setEntity(entity);

        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(addonGroup), new String[]{}, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2), addonGroup.getEntity().getId()});

        entity = new Entity();
        entity.build();
        entity.setAddon_min_limit(0);
        addonGroup.setEntity(entity);

        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(addonGroup), new String[]{}, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2), addonGroup.getEntity().getId()});

        return obj.iterator();
    }

    @DataProvider(name = "createbulkaddongroup")
    public Iterator<Object[]> createbulkaddongroup() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        BulkAddonGroup addonGroup = new BulkAddonGroup();
        addonGroup.build();

        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(addonGroup), new String[]{}, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2)});

        obj.add(new Object[]{"", jsonHelper.getObjectToJSON(addonGroup), new String[]{"404"}, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2)});

        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(addonGroup), new String[]{"400"}, MenuConstants.token_id});

        Entities entity = new Entities();
        Entities entity2 = new Entities();
        entity2.build();
        entity.build();
        entity.setId("");
        addonGroup.setEntities(new Entities[]{entity2, entity});

        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(addonGroup), MenuConstants.addbulkaddongroup_id, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2)});

        entity = new Entities();
        entity2 = new Entities();
        entity2.build();
        entity.build();
        entity.setItem_id("");
        addonGroup.setEntities(new Entities[]{entity2, entity});

        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(addonGroup), MenuConstants.addbulkaddongroup_itemid, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2)});

        entity = new Entities();
        entity2 = new Entities();
        entity2.build();
        entity.build();
        entity.setName("");
        addonGroup.setEntities(new Entities[]{entity2, entity});

        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(addonGroup), MenuConstants.addbulkaddongroup_name, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2)});

        entity = new Entities();
        entity2 = new Entities();
        entity2.build();
        entity.build();
        entity.setAddon_min_limit(0);
        addonGroup.setEntities(new Entities[]{entity2, entity});

        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(addonGroup), new String[]{}, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2)});

        return obj.iterator();
    }

    @DataProvider(name = "updatebulkaddongroup")
    public Iterator<Object[]> updatebulkaddongroup() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        BulkAddonGroup addonGroup = new BulkAddonGroup();
        addonGroup.build();

        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(addonGroup), new String[]{}, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2)});

        obj.add(new Object[]{"", jsonHelper.getObjectToJSON(addonGroup), new String[]{"404"}, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2)});

        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(addonGroup), new String[]{"400"}, MenuConstants.token_id});

        Entities entity = new Entities();
        Entities entity2 = new Entities();
        entity2.build();
        entity.build();
        entity.setId("");
        addonGroup.setEntities(new Entities[]{entity2, entity});

        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(addonGroup), MenuConstants.addbulkaddongroup_id, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2)});

        entity = new Entities();
        entity2 = new Entities();
        entity2.build();
        entity.build();
        entity.setItem_id("");
        addonGroup.setEntities(new Entities[]{entity2, entity});

        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(addonGroup), MenuConstants.addbulkaddongroup_itemid, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2)});

        entity = new Entities();
        entity2 = new Entities();
        entity2.build();
        entity.build();
        entity.setName("");
        addonGroup.setEntities(new Entities[]{entity2, entity});

        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(addonGroup), new String[]{}, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2)});

        entity = new Entities();
        entity2 = new Entities();
        entity2.build();
        entity.build();
        entity.setAddon_limit(-1);
        addonGroup.setEntities(new Entities[]{entity2, entity});

        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(addonGroup), new String[]{}, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2)});

        entity = new Entities();
        entity2 = new Entities();
        entity2.build();
        entity.build();
        entity.setAddon_min_limit(-1);
        addonGroup.setEntities(new Entities[]{entity2, entity});

        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(addonGroup), MenuConstants.updatebulkaddongroup_addonminlimit, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2)});

        entity = new Entities();
        entity2 = new Entities();
        entity2.build();
        entity.build();
        entity.setAddon_min_limit(0);
        addonGroup.setEntities(new Entities[]{entity2, entity});

        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(addonGroup), new String[]{}, FullMenuHelper.getToken_ids().get(MenuConstants.rest_id2)});

        return obj.iterator();
    }
}
