package com.swiggy.api.erp.delivery.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.swiggy.api.erp.delivery.tests.ServiceabilityCartTest;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.common.SolrDocumentList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.noggit.JSONUtil;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.delivery.constants.DeliveryConstant;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Daenerys.Initializer;
import org.testng.annotations.Test;

public class SolrHelper {
    Initialize init = Initializer.getInitializer();
    DeliveryDataHelper deliveryDataHelper = new DeliveryDataHelper();
    Serviceability_Helper serviceablility_Helper = new Serviceability_Helper();


    public String solrUrl, lDrestLatLon, lDrestaurantId;

    public Double lDrestLat, lDrestLon;
    Double LdDistance = 6.5;
    private static final String core = "core_listing";
    public Integer lDPolygonId;
    public List<String> LdCoordinates;


    public String getSolrUrl() {
        String environment = init.EnvironmentDetails.setup.getEnvironmentData().getName().toLowerCase();
        String solrUrl = null;
        switch (environment){
            case "stage1":
                solrUrl = DeliveryConstant.stage1_solrUrl;
                break;
            case "stage2":
                solrUrl = DeliveryConstant.stage2_solrUrl;
                break;
            case "stage3":
                solrUrl = DeliveryConstant.stage3_solrUrl;
                break;
            case "daily":
                solrUrl = DeliveryConstant.u2_listing_solrUrl;
                break;
            default:
                System.out.println("***** Environment not found *****");
        }
        return solrUrl;
    }
    public String getSolrUrl(String type) {
        String environment = init.EnvironmentDetails.setup.getEnvironmentData().getName().toLowerCase();
        String solrUrl = null;
        switch (environment){
            case "stage1":
                solrUrl = DeliveryConstant.stage1_solrUrl;
                break;
            case "stage2":
                solrUrl = DeliveryConstant.stage2_solrUrl;
                break;
            case "stage3":
                solrUrl = DeliveryConstant.stage3_solrUrl;
                break;
            case "daily":
                if(type.equalsIgnoreCase("listing"))
                {
                    solrUrl = DeliveryConstant.u2_listing_solrUrl;
                }else
                {
                    solrUrl = DeliveryConstant.u2_polygon_solrUrl;
                }
                break;
            default:
                System.out.println("***** Environment not found *****");
        }
        return solrUrl;
    }

    public SolrDocumentList getSolrOutput(String url, String core, String query,Integer numrows)
    {
        //SolrClient client = new HttpSolrClient(url);
        SolrClient client = new HttpSolrClient.Builder(url).build();
        SolrDocumentList solrDocuments=null;
        try{
            SolrQuery query1=new SolrQuery(query);
            query1.setParam("wt", "json");
            query1.setRows(numrows);
            solrDocuments= (client.query(core, query1).getResults());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return solrDocuments;
    }


    public List<Integer> GetMatchingPolygonId(String area_id, String city_id, double restLat, double restLon)
    {
        List<Integer> polygonIdList = new ArrayList<>();
        solrUrl = getSolrUrl();
        System.out.println(solrUrl + "Printing solr URL");
        // String polygonCore="polygon";
        String solrquery = "area_id:" + area_id + " AND is_long_distance_enabled:true AND city_id:" + city_id;
        SolrDocumentList solrDocuments = getSolrOutput(solrUrl,core,solrquery,100);
        System.out.println(solrDocuments);
        JSONObject restObject = null;
        try {
            restObject = new JSONArray(JSONUtil.toJSON(solrDocuments)).getJSONObject(0);
            lDrestLatLon = restObject.get("place").toString();
            lDrestLat = Double.parseDouble(lDrestLatLon.split(",")[0]);
            lDrestLon = Double.parseDouble(lDrestLatLon.split(",")[1]);
            lDrestaurantId = restObject.get("id").toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Double angle = 0.0;
        Integer polygonId = 0;
        for(int i = 0; i < 72; i++)
        {
            Double[] latlonarray = deliveryDataHelper.getLatLng(restLat,restLon,angle,LdDistance);
            Integer id = checkIFPointIsInLDPloygon(latlonarray[0],latlonarray[1]);

            if(id != 0)
            {
                polygonId = id;
                polygonIdList.add(id);
                System.out.println("found Polygon ID " + id);

            }
            angle = angle+5.0;

        }
        System.out.println("Printing Polygon Id List");
        printList(polygonIdList);
        return polygonIdList;
        //get all Ls
        //find coordinates at 5 degree angle

    }


    public void printList(List<?> somelist)
    {
        for(Object o:somelist)
        {
            System.out.println("List Values" +o.toString());
        }

    }

    public Integer checkIFPointIsInLDPloygon(Double lat,Double lon)
    {
        String solrUrl = getSolrUrl();
        SolrDocumentList solrDocuments = getSolrOutput(solrUrl,"polygon","polygon:\"Intersects("+lat+" "+lon+")\"",100);
        String jsonDoc = JSONUtil.toJSON(solrDocuments);
        System.out.println(jsonDoc);
        JSONArray polygonArray = null;
        String polygon_id = null;

        try
        {
            polygonArray = new JSONArray(JSONUtil.toJSON(solrDocuments));
            System.out.println(polygonArray.toString());
            Integer len = polygonArray.length();
            for(int i = 0; i < len; i++) {
                JSONObject polygonObject = polygonArray.getJSONObject(i);
                String polygonid = polygonObject.getString("id").toString();
                String maxLastMile = null;
                System.out.println(polygonObject.get("tags")+"  Getting Tags");
                System.out.println(polygonObject.get("tags").toString().contains("LONG_DISTANCE"));
                System.out.println(polygonObject.get("enabled").toString().contains("true"));

                if (polygonObject.get("tags").toString().contains("LONG_DISTANCE")&&polygonObject.get("enabled").toString().contains("true"))
                {
                    String polygonQuery = "select polygon_id from long_distance where polygon_id=" + polygonid;
                    List<Map<String,Object>> polygonIdList = SystemConfigProvider.getTemplate(DeliveryConstant.dbname).queryForList(polygonQuery);
                    if(polygonIdList.size() > 0)
                    {
                        return Integer.parseInt(polygonObject.get("id").toString());
                    }


                }
            }}
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        return 0;
    }



    public List<String> findPointInsideLDPolygonLatLon(String area_id, String city_id, double restLat, double restLon)
    {
        List<Integer> matchingPolygonIds = GetMatchingPolygonId(area_id, city_id, restLat, restLon);
        List<Map<Integer,String>> finalMapList = getCentroidlatLongFromPolygonId(matchingPolygonIds);
        Map<Integer,String> centroidMap = finalMapList.get(0);
        Map<Integer,String> LMMap = finalMapList.get(1);

        System.out.println("Printing Centroid Map");
        printMap(centroidMap);
        System.out.println("Printing LM Map");
        printMap(LMMap);

        List<String> ldLatLonList = new ArrayList<>();
        Integer ldArea = Integer.parseInt(SystemConfigProvider.getTemplate(DeliveryConstant.dbname).queryForMap("select area_code from restaurant where id="+lDrestaurantId).get("area_code").toString());
        SystemConfigProvider.getTemplate(DeliveryConstant.dbname).update("update restaurant set max_second_mile=4 where id="+lDrestaurantId);
        serviceablility_Helper.clearCache("restaurant", lDrestaurantId);
        SystemConfigProvider.getTemplate(DeliveryConstant.dbname).update("update area set last_mile_cap=4 where id="+ldArea);
        serviceablility_Helper.clearCache("area", String.valueOf(ldArea));

        Double lastMileRestaurantCap = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.dbname).
                queryForMap("select a.last_mile_cap from restaurant r inner join area a on a.id=r.area_code  where r.id="+lDrestaurantId).get("last_mile_cap").toString());
        Double lastMileAreaCap = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.dbname).
                queryForMap("select max_second_mile from restaurant where id="+lDrestaurantId).get("max_second_mile").toString());
        Double lm_area_cap = Math.min(lastMileAreaCap,lastMileRestaurantCap);

        System.out.println("lastMileRestaurantCap"+ lastMileRestaurantCap);
        System.out.println("lastMileAreaCap"+ lastMileAreaCap);
        System.out.println("lm_area_cap"+ lm_area_cap);
		/*        //generate random_value between max size of centroid map and 0
        Random r = new Random();
        int Low = 0;
        int High = centroidMap.size()-1;
        int randInt = r.nextInt(High-Low) + Low;
        List<Integer> test=new ArrayList<>(centroidMap.keySet());
        System.out.println(test.size());
        String centroidLatLon=centroidMap.get(test.get(randInt));
        Double fromLat=Double.parseDouble(centroidLatLon.split(",")[0]);
        Double fromLon=Double.parseDouble(centroidLatLon.split(",")[1]);
        String zoneid=getCustomerZoneFromLatLon(fromLat,fromLon);
        if(zoneid=="null"||zoneid=="")
        {
            continue;
        }*/
        for(Integer polygonId : centroidMap.keySet())
        {
            String centroidLatLon = centroidMap.get(polygonId);
            Double fromLat = Double.parseDouble(centroidLatLon.split(",")[0]);
            Double fromLon = Double.parseDouble(centroidLatLon.split(",")[1]);
            String zoneid = getCustomerZoneFromLatLon(fromLat,fromLon);
            System.out.println("***** zone ID **** " + zoneid);
            if(zoneid == null || zoneid == "")
            {
                continue;
            }
            System.out.println();
            Integer distance = deliveryDataHelper.getDistance(lDrestLat,lDrestLon,fromLat,fromLon);
            Double coordDistance =Double.parseDouble(distance.toString())/1000;
            System.out.println("found LM area cap as "+ lm_area_cap+"found distance from centroid as "+coordDistance+"found LM in polygon Core as "+Double.parseDouble(LMMap.get(polygonId)));
            //lm_area_cap is set to a value
            //now update lmarea_cap set value to coordinate distance +1
            String id = SystemConfigProvider.getTemplate(DeliveryConstant.dbname).queryForMap("select id from long_distance where polygon_id='" + polygonId + "'").get("id").toString();
            SystemConfigProvider.getTemplate(DeliveryConstant.dbname).update("update long_distance set max_last_mile="+(coordDistance+1.0)+",max_sla=200 where polygon_id="+polygonId);
            serviceablility_Helper.clearCache("long_distance", id);
            //hit the cache invalidate API here
			/*           if(coordDistance>lm_area_cap && coordDistance<Double.parseDouble(LMMap.get(polygonId)))
           {*/
            System.out.println("found the lat long ");
            ldLatLonList.add(fromLat.toString()+","+fromLon.toString());
            // }

            lDPolygonId=polygonId;
            break;
            //get customer zone from latlong
            //get mastmile from restaurant and latlon
            //checkwether sitance is greater than latmile normal and less than lstmile LD
        }
        System.out.println("Printing final CoordinateList");
        printList(ldLatLonList);
        return ldLatLonList;
    }


    public Integer checkIFPointIsNotInLDPloygon(Double lat,Double lon)
    {
        String solrUrl = getSolrUrl();
        SolrDocumentList solrDocuments = getSolrOutput(solrUrl,"polygon","polygon:\"Intersects("+lat+" "+lon+")\"",100);
        String jsonDoc = JSONUtil.toJSON(solrDocuments);
        System.out.println(jsonDoc);
        JSONArray polygonArray = null;
        String polygon_id = null;

        try
        {
            polygonArray = new JSONArray(JSONUtil.toJSON(solrDocuments));
            System.out.println(polygonArray.toString());
            Integer len = polygonArray.length();
            for(int i = 0; i < len; i++) {
                JSONObject polygonObject = polygonArray.getJSONObject(i);
                String polygonid = polygonObject.getString("id").toString();
                String maxLastMile = null;
                System.out.println(polygonObject.get("tags")+"  Getting Tags");
                System.out.println(polygonObject.get("tags").toString().contains("LONG_DISTANCE"));
                System.out.println(polygonObject.get("enabled").toString().contains("true"));

                if (!polygonObject.get("tags").toString().contains("LONG_DISTANCE")&&polygonObject.get("enabled").toString().contains("true"))
                {
                    String polygonQuery = "select polygon_id from long_distance where polygon_id=" + polygonid;
                    List<Map<String,Object>> polygonIdList = SystemConfigProvider.getTemplate(DeliveryConstant.dbname).queryForList(polygonQuery);
                    if(polygonIdList.size() > 0)
                    {
                        return Integer.parseInt(polygonObject.get("id").toString());
                    }


                }
            }}
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        return 0;
    }

    public List<Map<Integer,String>>  getCentroidlatLongFromPolygonId(List<Integer> poygonIdList)
    {
        Map<Integer,String> allPolygonLatLonMap=new HashMap<>();
        Map<Integer,String> allPolygonLastMileMap=new HashMap<>();
        List<Map<Integer,String>> mapList=new ArrayList<>();
        solrUrl = getSolrUrl();
        System.out.println(solrUrl+"Printing solr URL");
        for(Integer polygonId:poygonIdList) {
            String solrquery = "id:" + polygonId;
            //String solrquery="id:2727";
            SolrDocumentList solrDocuments = getSolrOutput(solrUrl, "polygon", solrquery, 1);
            System.out.println(solrDocuments);
            JSONObject polygonObject = null;
            List<String> pathList = new ArrayList<>();
            try {
                System.out.println(JSONUtil.toJSON(solrDocuments) + "XXXXXXX");
                polygonObject = (JSONObject) (new JSONArray(JSONUtil.toJSON(solrDocuments)).get(0));
                String polygonlatlons = polygonObject.getString("latLongs").replace("[", "").replace("]", "").replace("\"", "");
                String lastMile=polygonObject.getString("maxLastMile");
                allPolygonLastMileMap.put(polygonId,lastMile);
                String[] latlonarrray = polygonlatlons.split("\\s*,\\s*");
                for (String str : latlonarrray) {
                    pathList.add(str.split(" ")[0] + "," + str.split(" ")[1]);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String polygoncentre = deliveryDataHelper.getCentroid(pathList);
            System.out.println(polygoncentre);
            allPolygonLatLonMap.put(polygonId,polygoncentre);
        }
        mapList.add(allPolygonLatLonMap);
        mapList.add(allPolygonLastMileMap);
        return mapList;
    }


    public void printMap(Map<?,?> printMap)
    {
        for(Object str:printMap.keySet())
        {
            System.out.println(str+"::::"+printMap.get(str).toString());
        }
    }


    public String getCustomerZoneFromLatLon(Double lat,Double lon)
    {
        solrUrl = getSolrUrl();
        SolrDocumentList solrDocuments = deliveryDataHelper.getSolrOutput(solrUrl,"user_zone","polygon:\"Intersects(" + lat.toString() + "  " + lon.toString() + ")\"");
        String jsonDoc = JSONUtil.toJSON(solrDocuments);
        System.out.println("**** json doc **** " + jsonDoc);
        String customerZoneId=null;
        try{
            customerZoneId =((JSONObject)((new JSONArray(jsonDoc)).get(0))).get("id").toString();
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        return customerZoneId;
    }

    public Map<String, String> getCustomerZoneDetailsFromLatLon(Double lat,Double lon, String ... attr)
    {
        solrUrl = getSolrUrl();
        SolrDocumentList solrDocuments = deliveryDataHelper.getSolrOutput(solrUrl,"user_zone","polygon:\"Intersects(" + lat.toString() + "  " + lon.toString() + ")\"");
        String jsonDoc = JSONUtil.toJSON(solrDocuments);
        System.out.println("**** json doc **** " + jsonDoc);
        Map<String, String> solrUserZoneMap = new LinkedHashMap<>();
        for(String attributeName : attr) {
            String value = null;
            try {
                value = ((JSONObject) ((new JSONArray(jsonDoc)).get(0))).get(attributeName).toString();
                solrUserZoneMap.put(attributeName, value);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return solrUserZoneMap;
    }

    public Map<String, String> getRestaurantDetailsFromLatLon(String restId, String ... attr)
    {
        solrUrl = getSolrUrl();
        SolrDocumentList solrDocuments = deliveryDataHelper.getSolrOutput(solrUrl,"core_listing","id:" + restId);
        String jsonDoc = JSONUtil.toJSON(solrDocuments);
        System.out.println("**** json doc **** " + jsonDoc);
        Map<String, String> solrRestMap = new LinkedHashMap<>();
        for(String attributeName : attr) {
            String value = null;
            try {
                value = ((JSONObject) ((new JSONArray(jsonDoc)).get(0))).get(attributeName).toString();
                solrRestMap.put(attributeName, value);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return solrRestMap;
    }


    public String getCityIdByLatLon(Double lat,Double lon)
    {
        SolrDocumentList solrDocuments = deliveryDataHelper.getSolrOutput(solrUrl,"user_zone","polygon:\"Intersects(" + lat.toString() + "  " + lon.toString() + ")\"");
        String jsonDoc = JSONUtil.toJSON(solrDocuments);
        System.out.println("**** json doc **** " + jsonDoc);
        String cityId=null;
        try{
            cityId =((JSONObject)((new JSONArray(jsonDoc)).get(0))).get("cityId").toString();
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        return cityId;
    }


    public Map<String, String> getLdDisabledRestaurantInfo(String area_id, String city_id)
    {
        solrUrl = getSolrUrl();
        String solrquery = "area_id:" + area_id + " AND is_long_distance_enabled:false AND enabled:true AND city_id:" + city_id;
        SolrDocumentList solrDocuments = getSolrOutput(solrUrl,core,solrquery,100);
        System.out.println(solrDocuments);
        Map<String, String> map = new LinkedHashMap<String, String>();
        JSONObject restObject = null;
        try {
            restObject = new JSONArray(JSONUtil.toJSON(solrDocuments)).getJSONObject(0);
            String ldDisabledrestLatLon = restObject.get("place").toString();
            map.put("ldDisabledrestLat", ldDisabledrestLatLon.split(",")[0]);
            map.put("ldDisabledrestLong", ldDisabledrestLatLon.split(",")[1]);
            map.put("ldDisabledrestaurantId", restObject.get("id").toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return map;
    }


    public String getAttributeValueFromSolr(String core, String query, String attribute) {
        SolrDocumentList solrDocuments = deliveryDataHelper.getSolrOutput( getSolrUrl(),  core,  query);
        String jsonDoc = JSONUtil.toJSON(solrDocuments);
        System.out.println("******** JSON DOC ******* " + jsonDoc);
        String ldLastMile = JsonPath.read(jsonDoc, "$.." + attribute).toString().replace("[", "").replace("]", "");
        return ldLastMile;
    }
    public boolean checkIfDailyPolygonCreated(int polygonId)
    {
        String solrUrl = getSolrUrl();
        SolrDocumentList solrDocuments = getSolrOutput(solrUrl,"polygon","id: "+polygonId,100);
        String jsonDoc = JSONUtil.toJSON(solrDocuments);
        System.out.println(jsonDoc);
        JSONArray polygonArray = null;
        boolean result = false;

        try
        {
            polygonArray = new JSONArray(JSONUtil.toJSON(solrDocuments));
            System.out.println(polygonArray.toString());
            Integer len = polygonArray.length();
            JSONObject polygonObject = polygonArray.getJSONObject(0);
            String polygonid = polygonObject.getString("id").toString();
            String maxLastMile = null;
            System.out.println(polygonObject.get("tags")+"  Getting Tags");
            System.out.println(polygonObject.get("tags").toString().contains("DAILY"));
            System.out.println(polygonObject.get("enabled").toString().contains("true"));
            if (polygonObject.get("tags").toString().contains("DAILY")&&polygonObject.get("enabled").toString().contains("true"))
            {
                String polygonName = "select name from polygon where id=" + polygonid;
                String polygonEnabled = "select enabled from polygon where id=" + polygonid;
                String polygonTags = "select tags from polygon where id=" + polygonid;
                String polygonCityId = "select city_id from polygon where id=" + polygonid;
                String polygonParameters = "select parameters from polygon where id=" + polygonid;
                //todo
                //verify entries are there in DB
                Map<String,Object> polygonTag = SystemConfigProvider.getTemplate(DeliveryConstant.dbname).queryForMap(polygonTags);
                Map<String,Object> polygonStatus = SystemConfigProvider.getTemplate(DeliveryConstant.dbname).queryForMap(polygonEnabled);
                if(polygonTag.get("tags").toString().equalsIgnoreCase("DAILY") &&
                        polygonStatus.get("enabled").toString().equalsIgnoreCase("true"))
                {
                    result = true;
                }

            }
            return result;
        }

        catch (JSONException e)
        {
            e.printStackTrace();
        }
        return result;
    }
}
