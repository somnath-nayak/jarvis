package com.swiggy.api.erp.cms.tests;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.*;
import org.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.dp.POP_dataProvider;
import com.swiggy.api.erp.cms.helper.POP_Item_Schedule_Mapping_By_City_Helper;

/*
 * This class depends on the pop_area_schedule class
 *   Please delete the schedule from uat02 for respective city-code=5 as mentioned in POP_Area_Schedule class
 *   New item scheduling can be done from this class for same area-code and inventory and display-sequence 
 *   can be updated for the same
 */

public class POP_Item_Schedule_Mapping_By_City extends POP_dataProvider {
	Initialize gameofthrones = new Initialize();
	POP_Item_Schedule_Mapping_By_City_Helper obj = new POP_Item_Schedule_Mapping_By_City_Helper();
	int newlyCreatedItemScheduleIdByCity=0;
	@Test(dataProvider = "itemScheduleCityCREATEData", priority = 0, groups = { "Sanity_TC", "Smoke_TC", "Regression_TC" },
			description="Creating a new item-schedule-mapping for new city-schedule slot")
	public void createNewItemScheduleByNewCitySchedule(int item_id, String date, int display_sequence, int inventory,
			String slot_type, int area_id, int openTime, int closeTime, String day, String menu_type)
			throws JSONException {
		Processor p1 = obj.createItemScheduleByCity_helper(item_id, date, display_sequence, inventory, slot_type, area_id, openTime,
				closeTime, day, menu_type);
	
		String response = p1.ResponseValidator.GetBodyAsText();
		newlyCreatedItemScheduleIdByCity = JsonPath.read(response, "$.data.id");
		
		int statusCode = JsonPath.read(response, "$.statusCode");
		Assert.assertEquals(statusCode, 1);
	}

/*	 @Test(dataProvider = "itemScheduleCREATEWithExistingCitySchedule", priority = 0, groups = { "Sanity_TC", "Smoke_TC", "Regression_TC" },
			description="Creating a new item-schedule-mapping for existing city-schedule slot")
	public void createNewItemScheduleByExistingCitySchedule(int item_id, int area_schedule_id, String date,
			int display_sequence, int inventory) throws JSONException {
		Processor p1 = obj.createItemScheduleByExistingCity_helper(item_id, area_schedule_id, date, display_sequence,
				inventory);
		String response = p1.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(response, "$.statusCode");
		Assert.assertEquals(statusCode, 1);
	}
*/

	@Test(dataProvider = "itemScheduleCityUPDATEData", priority = 1, groups = { "Sanity_TC", "Smoke_TC", "Regression_TC" },
			description="Updating the created item-schedule-mapping by city")
	public void updateValidItemScheduleByCity(int display_sequence, int inventory) throws JSONException {
		Processor p1 = obj.updateItemScheduleByCity_helper(display_sequence, inventory, newlyCreatedItemScheduleIdByCity);
		String response = p1.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(response, "$.statusCode");
		Assert.assertEquals(statusCode, 1);
	}

	@Test(priority = 2, groups = { "Sanity_TC", "Smoke_TC", "Regression_TC" },
			description="Retrieving the created item-schedule-mapping by city")
	public void getValidItemScheduleByCity() throws JSONException {
		Processor p1 = obj.getItemScheduleByCity_helper(newlyCreatedItemScheduleIdByCity);
		String response = p1.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(response, "$.statusCode");
		Assert.assertEquals(statusCode, 1);
	}

	@Test(priority = 3, groups = { "Sanity_TC", "Smoke_TC", "Regression_TC" },
			description="Deleting the created item-schedule-mapping by city")
	public void deleteValidItemScheduleByCity() throws JSONException {
		Processor p1 = obj.deleteItemScheduleByCity_helper(newlyCreatedItemScheduleIdByCity);
		String response = p1.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(response, "$.statusCode");
		Assert.assertEquals(statusCode, 1);	
		obj.deleteCityScheduleHelper();
	}
}
