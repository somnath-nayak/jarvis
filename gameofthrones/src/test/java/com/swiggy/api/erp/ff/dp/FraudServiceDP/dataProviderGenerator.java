package com.swiggy.api.erp.ff.dp.FraudServiceDP;

import java.util.Arrays;
import java.util.List;

public class dataProviderGenerator {

    private static Object[][] objectifieddata;

    public static Object[][] generatevariants2(List<?>... lists) {

        int[] size = new int[lists.length];
        int counter = 0;

        for (List lst : lists) {
            size[counter] = lst.size();
            counter++;

        }
        int arraylength = 1;
        for (int i = 0; i < size.length; i++) {
            arraylength = arraylength * size[i];
        }

        int[] resetthreshhold = new int[lists.length];
        int newarrlength = 1;

        int temp = 1;
        for (int i = 0; i < lists.length; i++) {
            //System.out.println("size[i] = " + size[i]);
            newarrlength = newarrlength * size[i];
            resetthreshhold[i] = arraylength / newarrlength;
        }
        //System.out.println("Size values = " + Arrays.toString(size));
        //System.out.println("Threshhold values = " + Arrays.toString(resetthreshhold));


        Object[][] returnval = new Object[arraylength][lists.length];
        int column = 0, row = 0, itr = 0, itemindex = 0, counter1 = 0, index = 0;
        Object value = "";
        int totallists = lists.length;
        System.out.println("totallists = " + totallists);
        for (List lst : lists) {
            counter1 = 0;
            index = 0;
            itr = resetthreshhold[itemindex];
            //System.out.println("itr = " + itr);
            for (int i = 0; i < arraylength; i++) {
                if (counter1 == itr) {
                    counter1 = 0;
                    index++;

                }
                if (index >= lst.size()) {
                    index = 0;
                }
                //System.out.println("lstsize ="+lst.size()+", index = " + index+": counter = "+counter1);
                value = lst.get(index);
                //System.out.println("row = " + i+",column = "+column+" :: value ="+value);

                returnval[i][column] = value;
                counter1++;
                //index++;
            }
            column++;
            itemindex++;
        }
        objectifieddata = returnval;
        printarray(objectifieddata);
        return returnval;
    }

    private static void printarray(Object[][] objectarray)
    {
        for (Object[] arr : objectarray) {
            System.out.println(Arrays.toString(arr));
        }
    }






}
