package com.swiggy.api.erp.delivery.dp;

import org.testng.annotations.DataProvider;

public class ETA_ServiceDataProvider {

    @DataProvider(name = "DP")                      // test_case : DS_01
    public Object [][] DataProvider(){
        return new Object[][] { { "361", "12.956542", "77.701255" } };        // rest_id,customer_lat,customer_lng
    }
}
