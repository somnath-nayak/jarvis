package com.swiggy.api.erp.vms.tests;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.swiggy.api.erp.vms.constants.RMSConstants;
import com.swiggy.api.erp.vms.dp.PrepTimeServicesDP;
import com.swiggy.api.erp.vms.helper.PrepTimeServiceHelper;
import framework.gameofthrones.Cersei.ToolBox;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import org.apache.commons.collections.CollectionUtils;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.io.IOException;
import java.util.List;

public class PrepTimeServices extends PrepTimeServicesDP {

    PrepTimeServiceHelper helper = new PrepTimeServiceHelper();
    SchemaValidatorUtils schemaValidatorUtils = new SchemaValidatorUtils();

    @Test(dataProvider="slotWisePrepTime",groups={"sanity","smoke","regression"}, description = "Fetches Slot Wise Prep Time")
    public void SlotWisePrepTime(String prepTimeRestaurantId,String statusMessage, String auth,  boolean valid) throws IOException, ProcessingException
    {
        SoftAssert softAssert = new SoftAssert();
        Processor processor=helper.getPrepTimeSlotWise(prepTimeRestaurantId, auth);
        if(valid) {
            softAssert.assertTrue(schemaValidatorUtils.validateSchema(processor, "/../Data/SchemaSet/Json/VMS/daywisePreptime.txt"),"Nodes Are Missing Or Not Matching For Slotwise Prep Time API");
            softAssert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
            softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
        } else {
            softAssert.assertEquals(processor.ResponseValidator.GetResponseCode(), Integer.parseInt(statusMessage), "Status Message doesn't Match");
        }
        softAssert.assertAll();
    }

    @Test(dataProvider="dayWisePrepTime",groups={"sanity","smoke","regression"}, description = "Fetches Day Wise Prep Time")
    public void DayWisePrepTime(String prepTimeRestaurantId,String statusMessage, String auth,  boolean valid) throws IOException, ProcessingException
    {
        SoftAssert softAssert = new SoftAssert();
        Processor processor=helper.getPrepTimeDayWise(prepTimeRestaurantId, auth);
        if(valid) {
            softAssert.assertTrue(schemaValidatorUtils.validateSchema(processor, "/../Data/SchemaSet/Json/VMS/daywisePreptime.txt"),"Nodes Are Missing Or Not Matching For Daywise Prep Time API");
            softAssert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
            softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
        } else {
            softAssert.assertEquals(processor.ResponseValidator.GetResponseCode(), Integer.parseInt(statusMessage), "Status Message doesn't Match");
        }
        softAssert.assertAll();
    }

    @Test(dataProvider="multiRestPrepTime",groups={"sanity","smoke","regression"}, description = "Verifies Prep Times for Multiple Restaurants")
    public void MultiRestPrepTime(String prepTimeRestaurantId,String statusMessage, String auth, boolean check) throws IOException, ProcessingException
    {
        SoftAssert softAssert = new SoftAssert();
        Processor processor=helper.fetchPrepTimeMultiRestaurants(prepTimeRestaurantId, auth);
        softAssert.assertTrue(schemaValidatorUtils.validateSchema(processor, "/../Data/SchemaSet/Json/VMS/multiRestPreptime.txt"),"Nodes Are Missing Or Not Matching For Multiwise Prep Time API");
        softAssert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
        softAssert.assertAll();
    }

    @Test(dataProvider="deleteSlotPrepTime",groups={"sanity","smoke","regression"}, description = "Verifies Delete Prep Time Slots")
    public void DelPrepTime(String prepTimeRestaurantId,String statusMessage, boolean valid) throws IOException, ProcessingException
    {
        SoftAssert softAssert = new SoftAssert();
        Processor processor=helper.delPreptime(prepTimeRestaurantId);
        softAssert.assertTrue(schemaValidatorUtils.validateSchema(processor, "/../Data/SchemaSet/Json/VMS/deletePreptime.txt"),"Nodes Are Missing Or Not Matching For Delete Prep Time API");
        Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
        if(valid)
            softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
        else
            softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("data"), statusMessage);
        softAssert.assertAll();
    }

    @Test(dataProvider="deleteDayPrepTime",groups={"sanity","smoke","regression"}, description = "Verifies Delete Day Prep Time Slots")
    public void DelDayPrepTime(String prepTimeRestaurantId,String statusMessage, boolean valid) throws IOException, ProcessingException
    {
        SoftAssert softAssert = new SoftAssert();
        Processor processor=helper.delPreptime(prepTimeRestaurantId);
        softAssert.assertTrue(schemaValidatorUtils.validateSchema(processor, "/../Data/SchemaSet/Json/VMS/deletePreptime.txt"),"Nodes Are Missing Or Not Matching For Delete Day Prep Time API");
        Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
        if(valid)
            softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
        else
            softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("data"), statusMessage);
        softAssert.assertAll();
    }

    @Test(dataProvider="savePrepTime",groups={"sanity","smoke","regression"}, description = "Creates New Prep Time for a Restaurant(SLOTWISE or GLOBAL)")
    public void SavePrepTime(String prepTimeRestaurantId,String from, String to, String prep_time, String prep_type, String days, String statusMessage, String auth, boolean check) throws IOException, ProcessingException
    {
        SoftAssert softAssert = new SoftAssert();
        Processor processor=helper.savingPreptime(prepTimeRestaurantId, from, to,prep_time, prep_type, days, auth);
        if(check) {
            softAssert.assertTrue(schemaValidatorUtils.validateSchema(processor, "/../Data/SchemaSet/Json/VMS/savePreptime.txt"),"Nodes Are Missing Or Not Matching For Save Prep Time API");
            if(prep_type.equalsIgnoreCase(RMSConstants.PrepType.GLOBAL.toString())) {
                String[] global_slots = helper.fetchSlotWiseId(RMSConstants.restaurantId, RMSConstants.PrepType.GLOBAL.toString());
                if(global_slots.length >= 1)
                    softAssert.assertTrue(helper.verifyNegativeResponse(processor));
            }
            else {
                softAssert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
                softAssert.assertTrue(helper.verifySaveResponse(from, to, prep_time, prep_type, days, processor, false));
                softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
                String slotID = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[*].id");
                softAssert.assertNotEquals(slotID, null, "SlotID id is null");
            }
        } else
            softAssert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("status"), Integer.parseInt(statusMessage));
        softAssert.assertAll();
    }


    @Test(dataProvider="updatepreptime",groups={"sanity","smoke","regression"}, description = "Verifies Update Existing Prep Time")
    public void updatePrepTime(String from, String to, String prep_time, String prep_type, String days, String prep_id, String statusMessage, boolean valid) throws IOException, ProcessingException
    {
        SoftAssert softAssert = new SoftAssert();
        Processor processor=helper.updatePrepTime(from, to,prep_time, prep_type, days, prep_id);
        softAssert.assertTrue(schemaValidatorUtils.validateSchema(processor, "/../Data/SchemaSet/Json/VMS/savePreptime.txt"),"Nodes Are Missing Or Not Matching For Update Prep Time API");
        if(valid) {
            softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
            softAssert.assertTrue(helper.verifySaveResponse(from, to, prep_time, prep_type, days, processor, false));
        }
        else
            softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
        softAssert.assertAll();
    }


    @Test(dataProvider="savePrepTimeMulti",groups={"sanity","smoke","regression"}, description = "Verifies Save Prep Time for Multiple Restaurant")
    public void SavePrepTimeMulti(String prepTimeRestaurantId,String from, String to, String prep_time, String prep_type, String days, String statusMessage, String auth, boolean ignore, boolean check) throws IOException, ProcessingException
    {
        SoftAssert softAssert = new SoftAssert();
        Processor processor=helper.savingPreptimeMulti(prepTimeRestaurantId, from, to, prep_time, prep_type, days, auth);
        if(check) {
            softAssert.assertTrue(schemaValidatorUtils.validateSchema(processor, "/../Data/SchemaSet/Json/VMS/savePreptimeMulti.txt"),"Nodes Are Missing Or Not Matching For Save Multi Restaurant Prep Time API");
            if(prep_type.equalsIgnoreCase(RMSConstants.PrepType.GLOBAL.toString())) {
                String[] global_slots = helper.fetchSlotWiseId(RMSConstants.restaurantId, RMSConstants.PrepType.GLOBAL.toString());
                if(global_slots.length >= 1)
                    softAssert.assertTrue(helper.verifyNegativeResponse(processor));
            }
            else {
                softAssert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
                softAssert.assertTrue(helper.verifySaveResponse(from, to, prep_time, prep_type, days, processor, ignore));
                softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
            }
        } else
            softAssert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("status"), Integer.parseInt(statusMessage));

        softAssert.assertAll();
    }

    @Test(dataProvider="fetchDayPrepTimeMulti",groups={"sanity","smoke","regression"}, description = "Verifies Fetch Day Prep Time for Multiple Restaurant")
    public void FetchDayPrepTimeMulti(String prepTimeRestaurantId,String statusMessage, String auth, boolean check) throws IOException, ProcessingException
    {
        SoftAssert softAssert = new SoftAssert();
        Processor processor=helper.fetchingDayPreptimeMulti(prepTimeRestaurantId, auth);
        if(check) {
            softAssert.assertTrue(schemaValidatorUtils.validateSchema(processor, "/../Data/SchemaSet/Json/VMS/savePreptimeMulti.txt"),"Nodes Are Missing Or Not Matching For Save Multi Restaurant Prep Time API");
            softAssert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
            softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
        } else {
            softAssert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("status"), Integer.parseInt(statusMessage));
        }
        softAssert.assertAll();
    }

    @Test(dataProvider="fetchSlotPrepTimeMulti",groups={"sanity","smoke","regression"}, description = "Verifies Fetch Slot Prep Time for Multiple Restaurant")
    public void FetchSlotPrepTimeMulti(String prepTimeRestaurantId,String statusMessage, String auth, boolean check) throws IOException, ProcessingException
    {
        SoftAssert softAssert = new SoftAssert();
        Processor processor=helper.fetchPrepTimeMultiRestaurants(prepTimeRestaurantId, auth);
        if(check) {
            String resp = processor.ResponseValidator.GetBodyAsText();
            String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/fetchSlotPreptimeMulti.txt");
            List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
            Assert.assertTrue(CollectionUtils.isEmpty(missingNodeList),missingNodeList + " Nodes Are Missing Or Not Matching For Fetch Slot Prep Time API");
            softAssert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
            softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
        } else {
            softAssert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("status"), Integer.parseInt(statusMessage));
        }
        softAssert.assertAll();
    }

    @Test(dataProvider="index",groups={"sanity","smoke","regression"})
    public void index(String statusMessage, String auth, boolean check) throws IOException, ProcessingException {
        SoftAssert softAssert = new SoftAssert();
        Processor processor = helper.index(auth);
        if (check) {
            softAssert.assertTrue(schemaValidatorUtils.validateSchema(processor, "/../Data/SchemaSet/Json/VMS/index.txt"),"Nodes Are Missing Or Not Matching For index API");
            softAssert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
            softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
        } else {
            softAssert.assertEquals(processor.ResponseValidator.GetResponseCode(), Integer.parseInt(statusMessage), "Status Message doesn't Match");
            softAssert.assertAll();
        }
    }

    @Test(dataProvider="healthCheck",groups={"sanity","smoke","regression"}, description = "Verifies the HealthCheck for PrepTime Service")
    public void healthCheck(String statusMessage, String auth, boolean check) throws IOException, ProcessingException
    {
        SoftAssert softAssert = new SoftAssert();
        Processor processor=helper.healthCheck(auth);
        if(check) {
            softAssert.assertTrue(schemaValidatorUtils.validateSchema(processor, "/../Data/SchemaSet/Json/VMS/healthCheck.txt"),"Nodes Are Missing Or Not Matching For HealthCheck API");
            softAssert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
            softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
        } else {
            softAssert.assertEquals(processor.ResponseValidator.GetResponseCode(), Integer.parseInt(statusMessage) , "Status Message doesn't Match");
        }
        softAssert.assertAll();
    }

}
