package com.swiggy.api.erp.cms.pojo.BulkVariant;

import com.swiggy.api.erp.cms.constants.MenuConstants;
import com.swiggy.api.erp.cms.pojo.Items.Gst_details;

/**
 * Created by kiran.j on 2/22/18.
 */
public class Entities
{
    private String id;

    private String default_dependent_variant_id;

    private double price;

    private String variant_group_id;

    private int order;

    private boolean in_stock;

    private String item_id;

    private boolean is_veg;

    private String name;

    private Gst_details gst_details;

    private String default_dependent_variant_group_id;

    private boolean is_default;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getDefault_dependent_variant_id ()
    {
        return default_dependent_variant_id;
    }

    public void setDefault_dependent_variant_id (String default_dependent_variant_id)
    {
        this.default_dependent_variant_id = default_dependent_variant_id;
    }

    public double getPrice ()
    {
        return price;
    }

    public void setPrice (double price)
    {
        this.price = price;
    }

    public String getVariant_group_id ()
    {
        return variant_group_id;
    }

    public void setVariant_group_id (String variant_group_id)
    {
        this.variant_group_id = variant_group_id;
    }

    public int getOrder ()
    {
        return order;
    }

    public void setOrder (int order)
    {
        this.order = order;
    }

    public boolean getIn_stock ()
    {
        return in_stock;
    }

    public void setIn_stock (boolean in_stock)
    {
        this.in_stock = in_stock;
    }

    public String getItem_id ()
    {
        return item_id;
    }

    public void setItem_id (String item_id)
    {
        this.item_id = item_id;
    }

    public boolean getIs_veg ()
    {
        return is_veg;
    }

    public void setIs_veg (boolean is_veg)
    {
        this.is_veg = is_veg;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public Gst_details getGst_details ()
    {
        return gst_details;
    }

    public void setGst_details (Gst_details gst_details)
    {
        this.gst_details = gst_details;
    }

    public String getDefault_dependent_variant_group_id ()
    {
        return default_dependent_variant_group_id;
    }

    public void setDefault_dependent_variant_group_id (String default_dependent_variant_group_id)
    {
        this.default_dependent_variant_group_id = default_dependent_variant_group_id;
    }

    public boolean getIs_default ()
    {
        return is_default;
    }

    public void setIs_default (boolean is_default)
    {
        this.is_default = is_default;
    }

    public Entities build() {
        Gst_details gst_details = new Gst_details();
        if(this.getId() == null)
            this.setId(MenuConstants.variants_id);
        if(this.getVariant_group_id() == null)
            this.setVariant_group_id(MenuConstants.variant_groups_id);
        if(this.getName() == null)
            this.setName(MenuConstants.variants_name);
        if(this.getDefault_dependent_variant_id() == null)
            this.setDefault_dependent_variant_id(MenuConstants.dependant_variantid);
        if(this.getDefault_dependent_variant_group_id() == null)
            this.setDefault_dependent_variant_group_id(MenuConstants.dependant_variantgroupid);
        if(this.getItem_id() == null)
            this.setItem_id(MenuConstants.items_id);
        if(this.getPrice() == 0)
            this.setPrice(MenuConstants.default_price);

        this.setGst_details(gst_details);
        return this;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", default_dependent_variant_id = "+default_dependent_variant_id+", price = "+price+", variant_group_id = "+variant_group_id+", order = "+order+", in_stock = "+in_stock+", item_id = "+item_id+", is_veg = "+is_veg+", name = "+name+", gst_details = "+gst_details+", default_dependent_variant_group_id = "+default_dependent_variant_group_id+", is_default = "+is_default+"]";
    }
}
