package com.swiggy.api.erp.crm.tests.IntegrationServicesTests;

import com.swiggy.api.erp.crm.constants.FlowMapper;
import com.swiggy.api.erp.crm.dp.MockServiceDP;
import com.swiggy.api.erp.crm.helper.CRMUpstreamValidation;
import com.swiggy.api.sf.snd.helper.SnDHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.RedisHelper;
import io.advantageous.boon.core.Sys;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static org.testng.Assert.assertTrue;

public class IntegrationDashWorkFlows extends MockServiceDP {

    FlowMapper flowMapper = new FlowMapper();
    Initialize gameofthrones = new Initialize();

    @Test(dataProvider = "mockDashWorkFlows", groups = {"regression"}, description = "End to end tests for Dash Workflows")
    public void integrationDashFlow(HashMap<String, String> flowDetails) throws IOException, InterruptedException {
        String flowName = flowDetails.get("flow").toString();
        String orderStatus = flowDetails.get("orderStatus").toString();
        String[] flow = flowMapper.setFlow().get(flowName);
        String order_job_id ,order_id,nodeId= null;
        String customer_id = "123456";
        String payment_Method = "PAYTM";
        String[] createOrderPaylaodparam = new String[]{customer_id, payment_Method};
        String queryparam[] = new String[1];
        String updateStatusQueryParam[] = new String[1];
        String mobile = "7507220659";
        String password = "Test@2211";

        Processor loginResponse = SnDHelper.consumerLogin(mobile, password);
        String tid = loginResponse.ResponseValidator.GetNodeValue("tid");
        String token = loginResponse.ResponseValidator.GetNodeValue("data.token");

        HashMap<String, String> createOrderRequestHeaders_inbound = new HashMap<String, String>();
        createOrderRequestHeaders_inbound.put("Content-Type", "application/json");
        createOrderRequestHeaders_inbound.put("Authorization", "Basic QmEkRU9tJDoyMDE4U1chR0dZ");
        createOrderRequestHeaders_inbound.put("tid",tid);
        createOrderRequestHeaders_inbound.put("token", token);
        System.out.println("requestheaders_inbound "+createOrderRequestHeaders_inbound.get("Authorization"));


        // Create Order
        GameOfThronesService createOrder = new GameOfThronesService("dash", "createbaseOMS", gameofthrones);
        Processor createOrderResponse = new Processor(createOrder, createOrderRequestHeaders_inbound,createOrderPaylaodparam);
        Assert.assertEquals(createOrderResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("statusMsg"),
                "Successful", "Actual status code doesnt match with actual status code ");
        order_job_id = createOrderResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("body.orders[*].order_jobs[*].order_job_id").replace("[", "").replace("]", "");
        System.out.println("order_job_id "+order_job_id);
        updateStatusQueryParam[0] = order_job_id;
        order_id = createOrderResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("body.orders[*].order_id").toString().replace("[", "").replace("]", "");
        System.out.println("order_id "+order_id);
        System.out.println("order Status "+orderStatus);

        Assert.assertNotNull(order_job_id, "Order job id not found");
        String[] updateOrderJobStatusPayload = new String[]{orderStatus};
        // Update Order Status
        HashMap<String, String> updateOrderRequestHeaders_inbound = new HashMap<String, String>();
        createOrderRequestHeaders_inbound.put("Content-Type", "application/json");
        GameOfThronesService updateOrderJobStatus = new GameOfThronesService("dash", "updatedOrderJobStatus", gameofthrones);
        Processor updateOrderJobStatusPayloadResponse = new Processor(updateOrderJobStatus, updateOrderRequestHeaders_inbound, updateOrderJobStatusPayload,updateStatusQueryParam);

        Assert.assertEquals(updateOrderJobStatusPayloadResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("statusMsg"),
                "Successful", "Actual status code doesnt match with actual status code ");

        for (int i = 0; i < flow.length - 1; i++) {
            nodeId = flow[i];
            if(nodeId == "1" ){
                continue;
            } else {
                queryparam[0] = nodeId;
                String conversationID = "677yduhcfdd-a2dj-4c3-9djhgjhgfdfgfc7-208758";
                String[] validChildPaylaodparam = new String[]{conversationID, customer_id, order_id};
                HashMap<String, String> requestheaders_cancellationinbound = new HashMap<String, String>();
                requestheaders_cancellationinbound.put("Content-Type", "application/json");
                GameOfThronesService cancellationinbound = new GameOfThronesService("crm", "cancelFlow1", gameofthrones);

                Processor cancellationinbound_response = new Processor(cancellationinbound,
                        requestheaders_cancellationinbound, validChildPaylaodparam, queryparam);

                Assert.assertEquals(cancellationinbound_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("status"), "SUCCESS");

                String addOnGroupList = cancellationinbound_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.childrenNodes..id").toString().replace("[", "").replace("]", "");
                List<String> addOnlist = Arrays.asList(addOnGroupList.split(","));

                System.out.println("addonlist" + addOnlist);
                System.out.println("flowname[i+1]" + flow[i + 1]);

                assertTrue(addOnlist.contains(flow[i + 1]));
                int index = addOnlist.indexOf(flow[i + 1]);

                if (i < (flow.length - 2)) {
                    Assert.assertEquals(cancellationinbound_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.childrenNodes.[" + index + "].isLeaf"), "false");
                } else {
                    Assert.assertEquals(cancellationinbound_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.childrenNodes.[" + index + "].isLeaf"), "true");
                }
            }
        }



    }

}
