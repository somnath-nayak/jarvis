package com.swiggy.api.erp.cms.pojo.EntityTag;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class EntityTagMapBulk {
    @JsonProperty("entity_tag_map")
    private List<EntityTagMap> entityTagMap = null;
    @JsonProperty("delete_by_ids")
    private List<DeleteById> deleteByIds = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public EntityTagMapBulk() {
    }

    /**
     *
     * @param deleteByIds
     * @param entityTagMap
     */
    public EntityTagMapBulk(List<EntityTagMap> entityTagMap, List<DeleteById> deleteByIds) {
        super();
        this.entityTagMap = entityTagMap;
        this.deleteByIds = deleteByIds;
    }

    public List<EntityTagMap> getEntityTagMap() {
        return entityTagMap;
    }

    public void setEntityTagMap(List<EntityTagMap> entityTagMap) {
        this.entityTagMap = entityTagMap;
    }

    public List<DeleteById> getDeleteByIds() {
        return deleteByIds;
    }

    public void setDeleteByIds(List<DeleteById> deleteByIds) {
        this.deleteByIds = deleteByIds;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("entityTagMap", entityTagMap).append("deleteByIds", deleteByIds).toString();
    }

}