package com.swiggy.api.erp.ff.dp.FraudServiceDP;

import com.swiggy.api.erp.ff.POJO.CartDetails;
import com.swiggy.api.erp.ff.POJO.CustomerDetails;
import com.swiggy.api.erp.ff.POJO.DeliveryDetails;
import com.swiggy.api.erp.ff.POJO.SessionDetails;
import com.swiggy.api.erp.ff.constants.FraudServiceConstants;
import com.swiggy.api.erp.ff.helper.FraudServiceHelper;
import framework.gameofthrones.Tyrion.WireMockHelper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FraudService {


    CustomerDetails cd, cd4, cd2, cd3;
    SessionDetails sd, sd1, sd2, sd3, sd4, sd5, sd6, sd7, sd8, sd9, sd10, sd11, sd12, sd13, sd14, sd15;
    DeliveryDetails dd, dd2, dd3, dd4;
    CartDetails cartd, cartd2, cartd3,cartd4,cartd5;
    String rId;

    WireMockHelper wireMockHelper = new WireMockHelper();
    FraudServiceHelper fsh = new FraudServiceHelper();

    Object[][] getPendingCancellationFeeData() {


        try {
            fsh.mockDeliveryBoyResponse(FraudServiceConstants.statusCode);
            fsh.mockDSResponse(FraudServiceConstants.statusCode, FraudServiceConstants.predication_0,"1");
        } catch (IOException e) {
            e.printStackTrace();
        }


        rId = "Testing";
        List<String> requestIds = new ArrayList<>();

        requestIds.add(rId);

        cd = new CustomerDetails();
        cd.setCustomerId(12345);
        cd.setPendingCancellationFee(0);
        cd.setSwiggyMoneyUsed(0.0);
        cd.setMobileNo(123456789);

        cd2 = new CustomerDetails();
        cd2.setCustomerId(12345);
        cd2.setPendingCancellationFee(20);
        cd2.setSwiggyMoneyUsed(0.0);
        cd2.setMobileNo(123456789);

        cd3 = new CustomerDetails();
        cd3.setCustomerId(12345);
        cd3.setPendingCancellationFee(20);
        cd3.setSwiggyMoneyUsed(30.0);
        cd3.setMobileNo(123456789);

        cd4 = new CustomerDetails();
        cd4.setCustomerId(12345);
        cd4.setPendingCancellationFee(20);
        cd4.setSwiggyMoneyUsed(10.0);
        cd4.setMobileNo(123456789);

        List<CustomerDetails> customerDetails = new ArrayList<>();
        customerDetails.add(cd);
        customerDetails.add(cd2);
        customerDetails.add(cd3);
        customerDetails.add(cd4);

        sd = new SessionDetails();
        sd.setDeviceId("abcde-fghij-klmno");
        sd.setTimestamp("2018-07-03 13:56:00");
        sd.setUserAgent("Swiggy-Android");

        List<SessionDetails> sessionDetails = new ArrayList<>();
        sessionDetails.add(sd);

        dd = new DeliveryDetails();
        dd.setAreaId(2);
        dd.setCityId(1);
        dd.setCustomerGeohash("abcde");
        dd.setRestaurantCustomerDistanceKms("1.45");

        List<DeliveryDetails> deliveryDetails = new ArrayList<>();
        deliveryDetails.add(dd);

        cartd = new CartDetails();
        cartd.setBillAmount(345);
        cartd.setItemCount(3);
        cartd.setItemQuantity(10);
        cartd.setRestaurantId("12345");

        List<CartDetails> cartDetails = new ArrayList<>();
        cartDetails.add(cartd);

        return dataProviderGenerator.generatevariants2(requestIds, customerDetails, sessionDetails, deliveryDetails, cartDetails);

    }


    Object[][] getCityConfiguration() {

        try {
            fsh.mockDeliveryBoyResponse(FraudServiceConstants.statusCode);
            fsh.mockDSResponse(FraudServiceConstants.statusCode, FraudServiceConstants.predication_0,"1");
        } catch (IOException e) {
            e.printStackTrace();
        }


        rId = "Testing2";
        List<String> requestIds = new ArrayList<>();

        requestIds.add(rId);

        cd = new CustomerDetails();
        cd.setCustomerId(123456);
        cd.setPendingCancellationFee(0);
        cd.setSwiggyMoneyUsed(0.0);
        cd.setMobileNo(123456789);
        List<CustomerDetails> customerDetails = new ArrayList<>();
        customerDetails.add(cd);


        sd = new SessionDetails();
        sd.setDeviceId("abcde-fghij-klmno");
        sd.setTimestamp("2018-07-03 13:56:00");
        sd.setUserAgent("Swiggy-Android");

        List<SessionDetails> sessionDetails = new ArrayList<>();
        sessionDetails.add(sd);

        dd = new DeliveryDetails();
        dd.setAreaId(2);
        dd.setCityId(1);
        dd.setCustomerGeohash("abcde");
        dd.setRestaurantCustomerDistanceKms("1.45");

        dd2 = new DeliveryDetails();
        dd2.setAreaId(2);
        dd2.setCityId(5);
        dd2.setCustomerGeohash("abcde");
        dd2.setRestaurantCustomerDistanceKms("1.45");


        dd3 = new DeliveryDetails();
        dd3.setAreaId(2);
        dd3.setCityId(6);
        dd3.setCustomerGeohash("abcde");
        dd3.setRestaurantCustomerDistanceKms("1.45");

        List<DeliveryDetails> deliveryDetails = new ArrayList<>();
        deliveryDetails.add(dd);
        deliveryDetails.add(dd2);
        deliveryDetails.add(dd3);

        cartd = new CartDetails();
        cartd.setBillAmount(300);
        cartd.setItemCount(3);
        cartd.setItemQuantity(10);
        cartd.setRestaurantId("12345");

        cartd2 = new CartDetails();
        cartd2.setBillAmount(500);
        cartd2.setItemCount(3);
        cartd2.setItemQuantity(10);
        cartd2.setRestaurantId("12345");

        cartd3 = new CartDetails();
        cartd3.setBillAmount(200);
        cartd3.setItemCount(3);
        cartd3.setItemQuantity(10);
        cartd3.setRestaurantId("12345");


        List<CartDetails> cartDetails = new ArrayList<>();
        cartDetails.add(cartd);
        cartDetails.add(cartd2);
        cartDetails.add(cartd3);

        return dataProviderGenerator.generatevariants2(requestIds, customerDetails, sessionDetails, deliveryDetails, cartDetails);

    }


    Object[][] getCityConfigurationBasedTime() {


        try {
            fsh.mockDeliveryBoyResponse(FraudServiceConstants.statusCode);
            fsh.mockDSResponse(FraudServiceConstants.statusCode, FraudServiceConstants.predication_0,"1");
        } catch (IOException e) {
            e.printStackTrace();
        }


        rId = "Testing2";
        List<String> requestIds = new ArrayList<>();

        requestIds.add(rId);

        cd = new CustomerDetails();
        cd.setCustomerId(343434);
        cd.setPendingCancellationFee(0);
        cd.setSwiggyMoneyUsed(0.0);
        cd.setMobileNo(123456789);


        sd = new SessionDetails();
        sd.setDeviceId("abcde-fghij-klmno");
        sd.setTimestamp("2018-07-03 08:56:00");
        sd.setUserAgent("Swiggy-Android");

        sd1 = new SessionDetails();
        sd1.setDeviceId("abcde-fghij-klmno");
        sd1.setTimestamp("2018-07-04 09:59:00");
        sd1.setUserAgent("Swiggy-Android");

        sd2 = new SessionDetails();
        sd2.setDeviceId("abcde-fghij-klmno");
        sd2.setTimestamp("2018-07-05 10:00:00");
        sd2.setUserAgent("Swiggy-Android");

        sd3 = new SessionDetails();
        sd3.setDeviceId("abcde-fghij-klmno");
        sd3.setTimestamp("2018-07-06 10:00:01");
        sd3.setUserAgent("Swiggy-Android");


        sd4 = new SessionDetails();
        sd4.setDeviceId("abcde-fghij-klmno");
        sd4.setTimestamp("2018-07-07 14:59:00");
        sd4.setUserAgent("Swiggy-Android");

        sd5 = new SessionDetails();
        sd5.setDeviceId("abcde-fghij-klmno");
        sd5.setTimestamp("2018-07-08 15:00:00");
        sd5.setUserAgent("Swiggy-Android");

        sd6 = new SessionDetails();
        sd6.setDeviceId("abcde-fghij-klmno");
        sd6.setTimestamp("2018-07-09 15:00:01");
        sd6.setUserAgent("Swiggy-Android");


        sd7 = new SessionDetails();
        sd7.setDeviceId("abcde-fghij-klmno");
        sd7.setTimestamp("2018-07-10 16:00:01");
        sd7.setUserAgent("Swiggy-Android");


        sd8 = new SessionDetails();
        sd8.setDeviceId("abcde-fghij-klmno");
        sd8.setTimestamp("2018-07-03 20:56:00");
        sd8.setUserAgent("Swiggy-Android");

        sd9 = new SessionDetails();
        sd9.setDeviceId("abcde-fghij-klmno");
        sd9.setTimestamp("2018-07-04 21:59:00");
        sd9.setUserAgent("Swiggy-Android");

        sd10 = new SessionDetails();
        sd10.setDeviceId("abcde-fghij-klmno");
        sd10.setTimestamp("2018-07-05 22:00:00");
        sd10.setUserAgent("Swiggy-Android");

        sd11 = new SessionDetails();
        sd11.setDeviceId("abcde-fghij-klmno");
        sd11.setTimestamp("2018-07-06 22:00:01");
        sd11.setUserAgent("Swiggy-Android");


        sd12 = new SessionDetails();
        sd12.setDeviceId("abcde-fghij-klmno");
        sd12.setTimestamp("2018-07-07 04:59:00");
        sd12.setUserAgent("Swiggy-Android");

        sd13 = new SessionDetails();
        sd13.setDeviceId("abcde-fghij-klmno");
        sd13.setTimestamp("2018-07-08 05:00:00");
        sd13.setUserAgent("Swiggy-Android");

        sd14 = new SessionDetails();
        sd14.setDeviceId("abcde-fghij-klmno");
        sd14.setTimestamp("2018-07-09 05:00:01");
        sd14.setUserAgent("Swiggy-Android");


        sd15 = new SessionDetails();
        sd15.setDeviceId("abcde-fghij-klmno");
        sd15.setTimestamp("2018-07-10 06:00:01");
        sd15.setUserAgent("Swiggy-Android");


        dd = new DeliveryDetails();
        dd.setAreaId(2);
        dd.setCityId(8);
        dd.setCustomerGeohash("abcde");
        dd.setRestaurantCustomerDistanceKms("1.45");


        dd2 = new DeliveryDetails();
        dd2.setAreaId(2);
        dd2.setCityId(9);
        dd2.setCustomerGeohash("abcde");
        dd2.setRestaurantCustomerDistanceKms("1.45");


        cartd = new CartDetails();
        cartd.setBillAmount(11);
        cartd.setItemCount(3);
        cartd.setItemQuantity(10);
        cartd.setRestaurantId("12345");

        cartd2 = new CartDetails();
        cartd2.setBillAmount(10);
        cartd2.setItemCount(3);
        cartd2.setItemQuantity(10);
        cartd2.setRestaurantId("12345");


        cartd3 = new CartDetails();
        cartd3.setBillAmount(9);
        cartd3.setItemCount(3);
        cartd3.setItemQuantity(10);
        cartd3.setRestaurantId("12345");

        return new Object[][]{
                {rId, cd, sd, dd, cartd, true}, {rId, cd, sd1, dd, cartd, true}, {rId, cd, sd2, dd, cartd, false}, {rId, cd, sd3, dd, cartd, false},
                {rId, cd, sd4, dd, cartd, false}, {rId, cd, sd5, dd, cartd, true}, {rId, cd, sd6, dd, cartd, true}, {rId, cd, sd7, dd, cartd, true},
                {rId, cd, sd, dd, cartd2, true}, {rId, cd, sd1, dd, cartd2, true}, {rId, cd, sd2, dd, cartd2, true}, {rId, cd, sd3, dd, cartd2, true},
                {rId, cd, sd4, dd, cartd2, true}, {rId, cd, sd5, dd, cartd2, true}, {rId, cd, sd6, dd, cartd2, true}, {rId, cd, sd7, dd, cartd2, true},
                {rId, cd, sd, dd, cartd3, true}, {rId, cd, sd1, dd, cartd3, true}, {rId, cd, sd2, dd, cartd3, true}, {rId, cd, sd3, dd, cartd3, true},
                {rId, cd, sd4, dd, cartd3, true}, {rId, cd, sd5, dd, cartd3, true}, {rId, cd, sd6, dd, cartd3, true}, {rId, cd, sd7, dd, cartd3, true},

                {rId, cd, sd8, dd2, cartd, true}, {rId, cd, sd9, dd2, cartd, true}, {rId, cd, sd10, dd2, cartd, false}, {rId, cd, sd11, dd2, cartd, false},
                {rId, cd, sd12, dd2, cartd, false}, {rId, cd, sd13, dd2, cartd, true}, {rId, cd, sd14, dd2, cartd, true}, {rId, cd, sd15, dd2, cartd, true},
                {rId, cd, sd8, dd2, cartd2, true}, {rId, cd, sd9, dd2, cartd2, true}, {rId, cd, sd10, dd2, cartd2, true}, {rId, cd, sd11, dd2, cartd2, true},
                {rId, cd, sd12, dd2, cartd2, true}, {rId, cd, sd13, dd2, cartd2, true}, {rId, cd, sd14, dd2, cartd2, true}, {rId, cd, sd15, dd2, cartd2, true},
                {rId, cd, sd8, dd2, cartd3, true}, {rId, cd, sd9, dd2, cartd3, true}, {rId, cd, sd10, dd2, cartd3, true}, {rId, cd, sd11, dd2, cartd3, true},
                {rId, cd, sd12, dd2, cartd3, true}, {rId, cd, sd13, dd2, cartd3, true}, {rId, cd, sd14, dd2, cartd3, true}, {rId, cd, sd15, dd2, cartd3, true},


        };
    }


    Object[][] getPendingFeeCityConfiguration() {


        try {
            fsh.mockDeliveryBoyResponse(FraudServiceConstants.statusCode);
            fsh.mockDSResponse(FraudServiceConstants.statusCode, FraudServiceConstants.predication_0,"1");
        } catch (IOException e) {
            e.printStackTrace();
        }


        rId = "Testing2";
        List<String> requestIds = new ArrayList<>();

        requestIds.add(rId);

        cd = new CustomerDetails();
        cd.setCustomerId(12345);
        cd.setPendingCancellationFee(10);
        cd.setSwiggyMoneyUsed(10.0);
        cd.setMobileNo(123456789);
        List<CustomerDetails> customerDetails = new ArrayList<>();
        customerDetails.add(cd);


        sd = new SessionDetails();
        sd.setDeviceId("abcde-fghij-klmno");
        sd.setTimestamp("2018-07-03 13:56:00");
        sd.setUserAgent("Swiggy-Android");

        List<SessionDetails> sessionDetails = new ArrayList<>();
        sessionDetails.add(sd);

        dd2 = new DeliveryDetails();
        dd2.setAreaId(2);
        dd2.setCityId(5);
        dd2.setCustomerGeohash("abcde");
        dd2.setRestaurantCustomerDistanceKms("1.45");


        List<DeliveryDetails> deliveryDetails = new ArrayList<>();

        deliveryDetails.add(dd2);

        cartd = new CartDetails();
        cartd.setBillAmount(400);
        cartd.setItemCount(3);
        cartd.setItemQuantity(10);
        cartd.setRestaurantId("12345");


        List<CartDetails> cartDetails = new ArrayList<>();
        cartDetails.add(cartd);

        return dataProviderGenerator.generatevariants2(requestIds, customerDetails, sessionDetails, deliveryDetails, cartDetails);

    }


    Object[][] getExclustionListData() {

        try {
            fsh.mockDeliveryBoyResponse(FraudServiceConstants.statusCode);
            fsh.mockDSResponse(FraudServiceConstants.statusCode, FraudServiceConstants.predication_1,"1");
        } catch (IOException e) {
            e.printStackTrace();
        }


        rId = "Testing";
        List<String> requestIds = new ArrayList<>();

        requestIds.add(rId);

        cd = new CustomerDetails();
        cd.setCustomerId(12345);
        cd.setPendingCancellationFee(10);
        cd.setSwiggyMoneyUsed(0.0);
        cd.setMobileNo(123456789);

        cd2 = new CustomerDetails();
        cd2.setCustomerId(6789);
        cd2.setPendingCancellationFee(10);
        cd2.setSwiggyMoneyUsed(0.0);
        cd2.setMobileNo(123456789);

        List<CustomerDetails> customerDetails = new ArrayList<>();
        customerDetails.add(cd);
        customerDetails.add(cd2);


        sd = new SessionDetails();
        sd.setDeviceId("abcde-fghij-klmno");
        sd.setTimestamp("2018-07-03 13:56:00");
        sd.setUserAgent("Swiggy-Android");

        List<SessionDetails> sessionDetails = new ArrayList<>();
        sessionDetails.add(sd);

        dd = new DeliveryDetails();
        dd.setAreaId(2);
        dd.setCityId(5);
        dd.setCustomerGeohash("abcde");
        dd.setRestaurantCustomerDistanceKms("1.45");

        List<DeliveryDetails> deliveryDetails = new ArrayList<>();
        deliveryDetails.add(dd);

        cartd = new CartDetails();
        cartd.setBillAmount(345);
        cartd.setItemCount(3);
        cartd.setItemQuantity(10);
        cartd.setRestaurantId("12345");

        List<CartDetails> cartDetails = new ArrayList<>();
        cartDetails.add(cartd);

        return dataProviderGenerator.generatevariants2(requestIds, customerDetails, sessionDetails, deliveryDetails, cartDetails);

    }


    Object[][] getDSCheckData() {
        rId = "Testing";
        List<String> requestIds = new ArrayList<>();

        requestIds.add(rId);

        cd = new CustomerDetails();
        cd.setCustomerId(12345);
        cd.setPendingCancellationFee(0);
        cd.setSwiggyMoneyUsed(0.0);
        cd.setMobileNo(123456789);


        List<CustomerDetails> customerDetails = new ArrayList<>();
        customerDetails.add(cd);


        sd = new SessionDetails();
        sd.setDeviceId("abcde-fghij-klmno");
        sd.setTimestamp("2018-07-03 13:56:00");
        sd.setUserAgent("Swiggy-Android");

        List<SessionDetails> sessionDetails = new ArrayList<>();
        sessionDetails.add(sd);

        dd = new DeliveryDetails();
        dd.setAreaId(2);
        dd.setCityId(1);
        dd.setCustomerGeohash("abcde");
        dd.setRestaurantCustomerDistanceKms("1.45");

        List<DeliveryDetails> deliveryDetails = new ArrayList<>();
        deliveryDetails.add(dd);

        cartd = new CartDetails();
        cartd.setBillAmount(345);
        cartd.setItemCount(3);
        cartd.setItemQuantity(10);
        cartd.setRestaurantId("12345");

        List<CartDetails> cartDetails = new ArrayList<>();
        cartDetails.add(cartd);

        return dataProviderGenerator.generatevariants2(requestIds, customerDetails, sessionDetails, deliveryDetails, cartDetails);

    }


    Object[][] getCityDefaultCheck() {
        rId = "Testing";
        List<String> requestIds = new ArrayList<>();

        requestIds.add(rId);

        cd = new CustomerDetails();
        cd.setCustomerId(12345);
        cd.setPendingCancellationFee(0);
        cd.setSwiggyMoneyUsed(0.0);
        cd.setMobileNo(123456789);


        List<CustomerDetails> customerDetails = new ArrayList<>();
        customerDetails.add(cd);


        sd = new SessionDetails();
        sd.setDeviceId("abcde-fghij-klmno");
        sd.setTimestamp("2018-07-03 13:56:00");
        sd.setUserAgent("Swiggy-Android");

        List<SessionDetails> sessionDetails = new ArrayList<>();
        sessionDetails.add(sd);

        dd = new DeliveryDetails();
        dd.setAreaId(2);
        dd.setCityId(1);
        dd.setCustomerGeohash("abcde");
        dd.setRestaurantCustomerDistanceKms("1.45");

        dd3 = new DeliveryDetails();
        dd3.setAreaId(2);
        dd3.setCityId(2);
        dd3.setCustomerGeohash("abcde");
        dd3.setRestaurantCustomerDistanceKms("1.45");

        dd2 = new DeliveryDetails();
        dd2.setAreaId(2);
        dd2.setCityId(3);
        dd2.setCustomerGeohash("abcde");
        dd2.setRestaurantCustomerDistanceKms("1.45");

        dd4 = new DeliveryDetails();
        dd4.setAreaId(2);
        dd4.setCityId(4);
        dd4.setCustomerGeohash("abcde");
        dd4.setRestaurantCustomerDistanceKms("1.45");

        List<DeliveryDetails> deliveryDetails = new ArrayList<>();
        deliveryDetails.add(dd);
        deliveryDetails.add(dd2);
        deliveryDetails.add(dd3);
        deliveryDetails.add(dd4);

        cartd = new CartDetails();
        cartd.setBillAmount(50);
        cartd.setItemCount(3);
        cartd.setItemQuantity(10);
        cartd.setRestaurantId("12345");

        cartd2 = new CartDetails();
        cartd2.setBillAmount(200);
        cartd2.setItemCount(3);
        cartd2.setItemQuantity(10);
        cartd2.setRestaurantId("12345");

        cartd3 = new CartDetails();
        cartd3.setBillAmount(345);
        cartd3.setItemCount(3);
        cartd3.setItemQuantity(10);
        cartd3.setRestaurantId("12345");


        cartd4 = new CartDetails();
        cartd4.setBillAmount(500);
        cartd4.setItemCount(3);
        cartd4.setItemQuantity(10);
        cartd4.setRestaurantId("12345");


        cartd5 = new CartDetails();
        cartd5.setBillAmount(1001);
        cartd5.setItemCount(3);
        cartd5.setItemQuantity(10);
        cartd5.setRestaurantId("12345");


        List<CartDetails> cartDetails = new ArrayList<>();
        cartDetails.add(cartd);
        cartDetails.add(cartd2);
        cartDetails.add(cartd3);
        cartDetails.add(cartd4);
        cartDetails.add(cartd5);

        return dataProviderGenerator.generatevariants2(requestIds, customerDetails, sessionDetails, deliveryDetails, cartDetails);


    }

    Object[][] getDefaultDSPData() {


        try {
            fsh.mockDeliveryBoyResponse(FraudServiceConstants.statusCode);
            fsh.mockDSResponse(FraudServiceConstants.statusCode, FraudServiceConstants.predication_0,"1");
        } catch (IOException e) {
            e.printStackTrace();
        }


        rId = "Testing";
        List<String> requestIds = new ArrayList<>();

        requestIds.add(rId);

        cd = new CustomerDetails();
        cd.setCustomerId(12345);
        cd.setPendingCancellationFee(0);
        cd.setSwiggyMoneyUsed(0.0);
        cd.setMobileNo(123456789);

        cd2 = new CustomerDetails();
        cd2.setCustomerId(12345);
        cd2.setPendingCancellationFee(10);
        cd2.setSwiggyMoneyUsed(10.0);
        cd2.setMobileNo(123456789);

        cd3 = new CustomerDetails();
        cd3.setCustomerId(12345);
        cd3.setPendingCancellationFee(20);
        cd3.setSwiggyMoneyUsed(30.0);
        cd3.setMobileNo(123456789);

        List<CustomerDetails> customerDetails = new ArrayList<>();
        customerDetails.add(cd);
        customerDetails.add(cd2);
        customerDetails.add(cd3);

        sd = new SessionDetails();
        sd.setDeviceId("abcde-fghij-klmno");
        sd.setTimestamp("2018-07-03 13:56:00");
        sd.setUserAgent("Swiggy-Android");

        List<SessionDetails> sessionDetails = new ArrayList<>();
        sessionDetails.add(sd);

        dd = new DeliveryDetails();
        dd.setAreaId(2);
        dd.setCityId(1);
        dd.setCustomerGeohash("abcde");
        dd.setRestaurantCustomerDistanceKms("1.45");

        List<DeliveryDetails> deliveryDetails = new ArrayList<>();
        deliveryDetails.add(dd);

        cartd = new CartDetails();
        cartd.setBillAmount(345);
        cartd.setItemCount(3);
        cartd.setItemQuantity(10);
        cartd.setRestaurantId("12345");

        List<CartDetails> cartDetails = new ArrayList<>();
        cartDetails.add(cartd);

        return dataProviderGenerator.generatevariants2(requestIds, customerDetails, sessionDetails, deliveryDetails, cartDetails);

    }


}
