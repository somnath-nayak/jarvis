package com.swiggy.api.erp.cms.tests;

import com.swiggy.api.erp.cms.dp.AddonGroupDp;
import com.swiggy.api.erp.cms.helper.FullMenuHelper;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

/**
 * Created by kiran.j on 2/21/18.
 */
public class AddonGroupTests extends AddonGroupDp {

    @Test(dataProvider = "createaddongroup", description = "Verifies Different Scenarios for Creating Addon Groups")
    public void createAddonGroup(String rest_id, String json, String[] errors, String token_id) {
        System.out.println(json);
        SoftAssert softAssert = new SoftAssert();
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        Processor processor = fullMenuHelper.createAddonGroup(rest_id, json, token_id);
        if(errors.length == 0) {
            softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive(processor));
            System.out.println(fullMenuHelper.getRequestTracker(processor.ResponseValidator.GetNodeValue("request_id"), token_id));
        }
        else if(errors.length == 1)
            softAssert.assertTrue(processor.ResponseValidator.GetResponseCode() == Integer.parseInt(errors[0]));
        else
            softAssert.assertTrue(fullMenuHelper.validateErrors(processor, errors));
        softAssert.assertAll();
    }

    @Test(dataProvider = "updateaddongroup", description = "Verifies Different Scenarios for Updating Addon Groups")
    public void updateAddonGroup(String rest_id, String json, String[] errors, String token_id, String id) {
        System.out.println(json);
        SoftAssert softAssert = new SoftAssert();
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        Processor processor = fullMenuHelper.updateAddonGroup(rest_id, json, token_id, id);
        if(errors.length == 0)
            softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive(processor));
        else if(errors.length == 1)
            softAssert.assertTrue(processor.ResponseValidator.GetResponseCode() == Integer.parseInt(errors[0]));
        else
            softAssert.assertTrue(fullMenuHelper.validateError(processor, errors));
        softAssert.assertAll();
    }

    @Test(dataProvider = "createbulkaddongroup", description = "Verifies Different Scenarios for Creating Bulk Addon Groups")
    public void createBulkAddonGroup(String rest_id, String json, String[] errors, String token_id) {
        System.out.println(json);
        SoftAssert softAssert = new SoftAssert();
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        Processor processor = fullMenuHelper.createBulkAddonGroup(rest_id, json, token_id);
        if(errors.length == 0)
            softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive(processor));
        else if(errors.length == 1)
            softAssert.assertTrue(processor.ResponseValidator.GetResponseCode() == Integer.parseInt(errors[0]));
        else
            softAssert.assertTrue(fullMenuHelper.validateErrors(processor, errors));
        softAssert.assertAll();
    }

    @Test(dataProvider = "updatebulkaddongroup", description = "Verifies Different Scenarios for Updating Bulk Addon Groups")
    public void updateBulkAddonGroup(String rest_id, String json, String[] errors, String token_id) {
        System.out.println(json);
        SoftAssert softAssert = new SoftAssert();
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        Processor processor = fullMenuHelper.updateBulkAddonGroup(rest_id, json, token_id);
        if(errors.length == 0)
            softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive(processor));
        else if(errors.length == 1)
            softAssert.assertTrue(processor.ResponseValidator.GetResponseCode() == Integer.parseInt(errors[0]));
        else
            softAssert.assertTrue(fullMenuHelper.validateErrors(processor, errors));
        softAssert.assertAll();
    }


}
