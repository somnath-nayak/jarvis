package com.swiggy.api.erp.cms.pojo.ItemHolidaySlot;
import org.apache.commons.lang.time.DateUtils;

import java.util.Date;

public class ItemHolidaySlotBuilder {
    private ItemHolidaySlot itemHolidaySlot;

    public ItemHolidaySlotBuilder()
    {
        itemHolidaySlot=new ItemHolidaySlot();
    }
     public ItemHolidaySlotBuilder Data(Data data)
    {
        itemHolidaySlot.setData(data);
        return this;
    }

    public ItemHolidaySlotBuilder userMeta(UserMeta userMeta)
    {
        itemHolidaySlot.setUserMeta(userMeta);
        return this;
    }

    private void defaultItemHolidaySlot() {
        if(itemHolidaySlot.getData()==null){
            itemHolidaySlot.setData(new Data(228943,String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000),String.valueOf(DateUtils.addHours(new Date(), 10).toInstant().getEpochSecond() * 1000)));
        }
        if(itemHolidaySlot.getUserMeta()==null){
            itemHolidaySlot.setUserMeta(new UserMeta("cms", new Meta("cms-tester")));
        }
    }

    public ItemHolidaySlot buildItemHolidaySlot() {
        defaultItemHolidaySlot();
        return itemHolidaySlot;
    }
}
