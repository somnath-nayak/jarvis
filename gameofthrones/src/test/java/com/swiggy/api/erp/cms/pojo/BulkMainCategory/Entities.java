package com.swiggy.api.erp.cms.pojo.BulkMainCategory;

import com.swiggy.api.erp.cms.constants.MenuConstants;

/**
 * Created by kiran.j on 2/20/18.
 */
public class Entities
{
    private String id;

    private int order;

    private String description;

    private String name;

    private int enable;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public int getOrder ()
    {
        return order;
    }

    public void setOrder (int order)
    {
        this.order = order;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public int getEnable ()
    {
        return enable;
    }

    public void setEnable (int enable)
    {
        this.enable = enable;
    }

    public Entities build() {
        setDefaultValues();
        return this;
    }

    public void setDefaultValues() {
        if(this.getId() == null)
            this.setId(MenuConstants.main_category_id);
        if(this.getName() == null)
            this.setName(MenuConstants.main_category_name);
        if(this.getDescription() == null)
            this.setDescription(MenuConstants.items_description);
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", order = "+order+", description = "+description+", name = "+name+", enable = "+enable+"]";
    }
}
