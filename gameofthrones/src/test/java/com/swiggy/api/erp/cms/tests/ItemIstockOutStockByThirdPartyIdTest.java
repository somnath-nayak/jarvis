package com.swiggy.api.erp.cms.tests;

import org.hibernate.dialect.identity.SybaseAnywhereIdentityColumnSupport;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.constants.Constants;
import com.swiggy.api.erp.cms.dp.CategoryInStockOutStockDp;
import com.swiggy.api.erp.cms.helper.CategoryInStockOutStockHelper;
import com.swiggy.api.erp.cms.helper.SelfServeHelper;

public class ItemIstockOutStockByThirdPartyIdTest extends CategoryInStockOutStockDp {
	CategoryInStockOutStockHelper instockOutstockHelper = new CategoryInStockOutStockHelper();

	@Test(dataProvider = "ItemInstock", priority = 1, description = "item instock out of stock status")
	public void ItemInstock(String partner_type,String partner_id,String third_party_item_ids,String source,String in_stock,boolean success)
			throws Exception{
		
		String response =instockOutstockHelper.itemInStock(partner_type,partner_id,third_party_item_ids,source,in_stock).ResponseValidator.GetBodyAsText();
		String partnerType=JsonPath.read(response, "$.partner_type").toString();
		String partnerId=JsonPath.read(response, "$.partner_id").toString();
		
		if (success == true) {
			{
				String ItemId = JsonPath.read(response, "$.successful_third_party_item_ids[0]").toString();
			Assert.assertEquals(ItemId, third_party_item_ids, "success");
			}
		} else
		{
			String errroMessage=JsonPath.read(response, "$.failed_items_error_map["+third_party_item_ids+"].error_message").toString();
			if (errroMessage!=null)
				Assert.assertEquals(partner_type, partnerType, "success");
			   Assert.assertEquals(partner_id, partnerId, "success");
		}
				
	    }
		
		
		@Test(dataProvider = "ItemOutSTock", priority = 2, description = "item instock out of stock status")
  	public void ItemOutSTock(String partner_type,String partner_id,String third_party_item_ids,String source,String in_stock,String fromTime,String toTime,boolean success)
				throws Exception{
		
			String response =instockOutstockHelper.itemOutStock(partner_type,partner_id,third_party_item_ids,in_stock,source,fromTime,toTime).ResponseValidator.GetBodyAsText();
		
			if (success == true) {
				{
					String ItemId = JsonPath.read(response, "$.successful_third_party_item_ids[0]").toString();
				Assert.assertEquals(ItemId, third_party_item_ids, "success");
			}
			} else
			{
				String response1[]=JsonPath.read(response, "$[*].id").toString().replace("["," ").replace("]","").split(",");
				System.out.println(response1[0]);
			}
		
}
}






