package com.swiggy.api.erp.delivery.tests;

import com.swiggy.api.erp.delivery.helper.*;
import org.testng.annotations.Test;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.DataProvider;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class DEAlchemistTest{
    DeliveryServiceApiHelper deliveryServiceApiHelper=null;
    HashMap<String,String> ruleMap=null;
    AlchemistCommands alchemistCommands=null;
    Map<String,Command> runCommand=null;
    Boolean ReadRulesFromDBFlag=false;
    Boolean SendPayoutToDE;

    @BeforeSuite
    public void AlchemistActiveRules(){
        deliveryServiceApiHelper=new DeliveryServiceApiHelper();
        if(!ReadRulesFromDBFlag)
            deliveryServiceApiHelper.disableAllActiveRules();
    }

    @AfterSuite
    public void AlchemistInActiveRules(){
        if(!ReadRulesFromDBFlag)
            deliveryServiceApiHelper.enableAllInactiveRules();
        deliveryServiceApiHelper.printTestSuiteResultJSON();
    }

    @AfterMethod
    public void AlchemistDisableRules(){
        if(!ReadRulesFromDBFlag)
            deliveryServiceApiHelper.disableRules();
    }

    @DataProvider(name="GoldenRules")
    public Object[][] data(){
        Object[][] objectData= DeliveryUtils.getTestsInSheet("goldenset");
        LinkedHashMap<String,String> map=null;
        int numberOfTestCasesEnabled=0;
        for(int counter=0;counter<objectData.length;counter++){
            map= (LinkedHashMap<String, String>) objectData[counter][1];
            if(Boolean.parseBoolean(map.get("enabled"))){
                numberOfTestCasesEnabled++;
            }
        }
        Object[][] testCases=new Object[numberOfTestCasesEnabled][objectData[0].length];
        int index=0;
        for(int counter=0;counter<objectData.length;counter++){
            map= (LinkedHashMap<String, String>) objectData[counter][1];
            if(Boolean.parseBoolean(map.get("enabled"))){
                testCases[index][1]=objectData[counter][1];
                index++;
            }
        }
        return testCases;
    }

    @Test(dataProvider = "GoldenRules")
    public void verifyPayoutfortheGoldenSetOfRules(String TC_ID,LinkedHashMap<String,String> objectData)
    {
        System.out.println("**************** Test Case Id "+TC_ID+" STARTS HERE****************");
        System.out.println("Test Description "+objectData.get("TC_Description"));
        String zone,expression,ComputeOn,setCAPRD_To_state;
        Boolean CancelOrder;
        zone=objectData.get("zoneID");
        Double a= Double.valueOf((zone));
        int zoneId=a.intValue();
        expression=objectData.get("expression");
        ComputeOn=objectData.get("computeOn");
        setCAPRD_To_state=objectData.get("setCAPRDToState");
        CancelOrder=Boolean.parseBoolean(objectData.get("cancelOrder"));
        Boolean SendPayoutToDE= Boolean.valueOf(objectData.get("sendPayoutToDE"));

        if(ReadRulesFromDBFlag)
            ruleMap=deliveryServiceApiHelper.verifyActiveRuleInDB(expression);
        else
            ruleMap=deliveryServiceApiHelper.createRules(String.valueOf(zoneId),expression,ComputeOn);

        ruleMap.put("SendPayoutToDE", String.valueOf(SendPayoutToDE));
        alchemistCommands=new AlchemistCommands(ruleMap);
        if(setCAPRD_To_state !="NULL"){
            alchemistCommands.setCAPRD_To_state(setCAPRD_To_state);
        }
        alchemistCommands.setOrdermapper();
        runCommand=alchemistCommands.AllAlchemistCommandMap;
        runCommand.get("create_new_de").runCommand();
        runCommand.get("pre_order_checks").runCommand();
        runCommand.get("assign").runCommand();
        runCommand.get("docaprd").runCommand();
        if(CancelOrder)
            runCommand.get("cancelOrder").runCommand();
        runCommand.get("doValidate").runCommand();
        deliveryServiceApiHelper.convertTestCaseResultstoJSON();
        System.out.println("**************** Test Case Id "+TC_ID+" ENDS HERE ****************");
    }

    @Test(description = "Verify Payout is successful for batch count>1 single expression", enabled = false)
    public void verifyPayoutforBatchCountGreaterThan1ExpressionRule()
    {
        if(ReadRulesFromDBFlag)
            ruleMap=deliveryServiceApiHelper.verifyActiveRuleInDB("batch_count>1");
        else
            ruleMap=deliveryServiceApiHelper.createRules("1","de_wait_time<15","delivered");
        SendPayoutToDE=true;
        ruleMap.put("SendPayoutToDE", String.valueOf(SendPayoutToDE));
        alchemistCommands=new AlchemistCommands(ruleMap);
        alchemistCommands.setOrdermapper();
        runCommand=alchemistCommands.AllAlchemistCommandMap;
        runCommand.get("create_new_de").runCommand();
        runCommand.get("pre_order_checks").runCommand();
        runCommand.get("assign").runCommand();
        runCommand.get("docaprd").runCommand();
        runCommand.get("doValidate").runCommand();
        deliveryServiceApiHelper.convertTestCaseResultstoJSON();
    }

    @Test(description = "Verify Payout is successful for batch count=1 single expression", enabled = false)
    public void verifyPayoutforBatchCountEqualTo1ExpressionRule()
    {
        if(ReadRulesFromDBFlag)
            ruleMap=deliveryServiceApiHelper.verifyActiveRuleInDB("batch_count=1");
        else
            ruleMap=deliveryServiceApiHelper.createRules("4","batch_count=1","delivered");
        SendPayoutToDE=true;
        ruleMap.put("SendPayoutToDE", String.valueOf(SendPayoutToDE));
        alchemistCommands=new AlchemistCommands(ruleMap);
        alchemistCommands.setOrdermapper();
        runCommand=alchemistCommands.AllAlchemistCommandMap;
        runCommand.get("create_new_de").runCommand();
        runCommand.get("pre_order_checks").runCommand();
        runCommand.get("assign").runCommand();
        runCommand.get("docaprd").runCommand();
        runCommand.get("doValidate").runCommand();
    }

    @Test(description = "Verify Payout is successful for Zone id single expression", enabled = false)
    public void verifyPayoutforZoneSpecificAndBatchCountExpressionRuleSet1()
    {
        ruleMap=deliveryServiceApiHelper.createRules("4","zone_id=4 and batch_count>1","delivered");
        SendPayoutToDE=true;
        ruleMap.put("SendPayoutToDE", String.valueOf(SendPayoutToDE));
        alchemistCommands=new AlchemistCommands(ruleMap);
        alchemistCommands.setOrdermapper();
        runCommand=alchemistCommands.AllAlchemistCommandMap;
        runCommand.get("create_new_de").runCommand();
        runCommand.get("pre_order_checks").runCommand();
        runCommand.get("assign").runCommand();
        runCommand.get("docaprd").runCommand();
        runCommand.get("doValidate").runCommand();
    }

    @Test(description = "Verify Payout is successful for Zone id single expression", enabled = false)
    public void verifyPayoutforZoneSpecificAndBatchCountExpressionRuleSet2()
    {
        ruleMap=deliveryServiceApiHelper.createRules("1","zone_id=1 and batch_count=1","delivered");
        SendPayoutToDE=true;
        ruleMap.put("SendPayoutToDE", String.valueOf(SendPayoutToDE));
        alchemistCommands=new AlchemistCommands(ruleMap);
        alchemistCommands.setOrdermapper();
        runCommand=alchemistCommands.AllAlchemistCommandMap;
        runCommand.get("create_new_de").runCommand();
        runCommand.get("pre_order_checks").runCommand();
        runCommand.get("assign").runCommand();
        runCommand.get("docaprd").runCommand();
        runCommand.get("doValidate").runCommand();
    }

    @Test(description = "Verify Payout is successful for First Mile single expression", enabled = false)
    public void verifyPayoutforFirstMileGreaterThanRule()
    {
        ruleMap=deliveryServiceApiHelper.createRules("4","first_mile>5","delivered");
        SendPayoutToDE=true;
        ruleMap.put("SendPayoutToDE", String.valueOf(SendPayoutToDE));
        alchemistCommands=new AlchemistCommands(ruleMap);
        alchemistCommands.setOrdermapper();
        runCommand=alchemistCommands.AllAlchemistCommandMap;
        runCommand.get("create_new_de").runCommand();
        runCommand.get("pre_order_checks").runCommand();
        runCommand.get("assign").runCommand();
        runCommand.get("docaprd").runCommand();
        runCommand.get("doValidate").runCommand();
    }

    @Test(description = "Verify Payout is successful for First Mile single expression", enabled = false)
    public void verifyPayoutforFirstMileLessThanRule()
    {
        ruleMap=deliveryServiceApiHelper.createRules("4","first_mile<5","delivered");
        SendPayoutToDE=true;
        ruleMap.put("SendPayoutToDE", String.valueOf(SendPayoutToDE));
        alchemistCommands=new AlchemistCommands(ruleMap);
        alchemistCommands.setOrdermapper();
        runCommand=alchemistCommands.AllAlchemistCommandMap;
        runCommand.get("create_new_de").runCommand();
        runCommand.get("pre_order_checks").runCommand();
        runCommand.get("assign").runCommand();
        runCommand.get("docaprd").runCommand();
        runCommand.get("doValidate").runCommand();
    }

    @Test(description = "Verify Payout is successful for Last Mile single expression", enabled = false)
    public void verifyPayoutforLastMileLessThanRule()
    {
        ruleMap=deliveryServiceApiHelper.createRules("4","last_mile<5","delivered");
        SendPayoutToDE=true;
        ruleMap.put("SendPayoutToDE", String.valueOf(SendPayoutToDE));
        alchemistCommands=new AlchemistCommands(ruleMap);
        alchemistCommands.setOrdermapper();
        runCommand=alchemistCommands.AllAlchemistCommandMap;
        runCommand.get("create_new_de").runCommand();
        runCommand.get("pre_order_checks").runCommand();
        runCommand.get("assign").runCommand();
        runCommand.get("docaprd").runCommand();
        runCommand.get("doValidate").runCommand();
    }

    @Test(description = "Verify Payout is successful for Last Mile single expression", enabled = false)
    public void verifyPayoutforLastMileGreaterThanRule()
    {
        ruleMap=deliveryServiceApiHelper.createRules("4","last_mile>5","delivered");
        SendPayoutToDE=true;
        ruleMap.put("SendPayoutToDE", String.valueOf(SendPayoutToDE));
        alchemistCommands=new AlchemistCommands(ruleMap);
        alchemistCommands.setOrdermapper();
        runCommand=alchemistCommands.AllAlchemistCommandMap;
        runCommand.get("create_new_de").runCommand();
        runCommand.get("pre_order_checks").runCommand();
        runCommand.get("assign").runCommand();
        runCommand.get("docaprd").runCommand();
        runCommand.get("doValidate").runCommand();
    }

    @Test(description = "Verify Payout is successful for First Mile time single expression", enabled = false)
    public void verifyPayoutforFirstMileTimeGreaterThanRule() {
        ruleMap=deliveryServiceApiHelper.createRules("4","fmt>15","delivered");
        SendPayoutToDE=true;
        ruleMap.put("SendPayoutToDE", String.valueOf(SendPayoutToDE));
        alchemistCommands=new AlchemistCommands(ruleMap);
        alchemistCommands.setOrdermapper();
        runCommand=alchemistCommands.AllAlchemistCommandMap;
        runCommand.get("create_new_de").runCommand();
        runCommand.get("pre_order_checks").runCommand();
        runCommand.get("assign").runCommand();
        runCommand.get("docaprd").runCommand();
        runCommand.get("doValidate").runCommand();
    }

    @Test(description = "Verify Payout is successful for First Mile time single expression", enabled = false)
    public void verifyPayoutforFirstMileTimeLessThanRule() {
        ruleMap=deliveryServiceApiHelper.createRules("4","fmt<15","delivered");
        SendPayoutToDE=true;
        ruleMap.put("SendPayoutToDE", String.valueOf(SendPayoutToDE));
        alchemistCommands=new AlchemistCommands(ruleMap);
        alchemistCommands.setOrdermapper();
        runCommand=alchemistCommands.AllAlchemistCommandMap;
        runCommand.get("create_new_de").runCommand();
        runCommand.get("pre_order_checks").runCommand();
        runCommand.get("assign").runCommand();
        runCommand.get("docaprd").runCommand();
        runCommand.get("doValidate").runCommand();
    }

    @Test(description = "Verify Payout is successful for Last Mile Time single expression", enabled = false)
    public void verifyPayoutforLastMileTimeLessThanRule() {
        ruleMap=deliveryServiceApiHelper.createRules("4","lmt<15","delivered");
        SendPayoutToDE=true;
        ruleMap.put("SendPayoutToDE", String.valueOf(SendPayoutToDE));
        alchemistCommands=new AlchemistCommands(ruleMap);
        alchemistCommands.setOrdermapper();
        runCommand=alchemistCommands.AllAlchemistCommandMap;
        runCommand.get("create_new_de").runCommand();
        runCommand.get("pre_order_checks").runCommand();
        runCommand.get("assign").runCommand();
        runCommand.get("docaprd").runCommand();
        runCommand.get("doValidate").runCommand();
    }

    @Test(description = "Verify Payout is successful for Last Mile Time single expression", enabled = false)
    public void verifyPayoutforLastMileTimeGreaterThanRule() {
        ruleMap=deliveryServiceApiHelper.createRules("4","lmt>15","delivered");
        SendPayoutToDE=true;
        ruleMap.put("SendPayoutToDE", String.valueOf(SendPayoutToDE));
        alchemistCommands=new AlchemistCommands(ruleMap);
        alchemistCommands.setOrdermapper();
        runCommand=alchemistCommands.AllAlchemistCommandMap;
        runCommand.get("create_new_de").runCommand();
        runCommand.get("pre_order_checks").runCommand();
        runCommand.get("assign").runCommand();
        runCommand.get("docaprd").runCommand();
        runCommand.get("doValidate").runCommand();
    }

    @Test(description = "Verify Payout is successful for first Mile and batch count multiple expression", enabled = false)
    public void verifyPayoutforFirstMileandBatchCountRuleSet1() {
        ruleMap=deliveryServiceApiHelper.createRules("4","first_mile>5 and batch_count<2","delivered");
        SendPayoutToDE=true;
        ruleMap.put("SendPayoutToDE", String.valueOf(SendPayoutToDE));
        alchemistCommands=new AlchemistCommands(ruleMap);
        alchemistCommands.setOrdermapper();
        runCommand=alchemistCommands.AllAlchemistCommandMap;
        runCommand.get("create_new_de").runCommand();
        runCommand.get("pre_order_checks").runCommand();
        runCommand.get("assign").runCommand();
        runCommand.get("docaprd").runCommand();
        runCommand.get("doValidate").runCommand();
    }

    @Test(description = "Verify Payout is successful for first Mile and batch count multiple expression", enabled = false)
    public void verifyPayoutforFirstMileandBatchCountRuleSet2() {
        ruleMap=deliveryServiceApiHelper.createRules("4","first_mile<5 and batch_count<2","delivered");
        SendPayoutToDE=true;
        ruleMap.put("SendPayoutToDE", String.valueOf(SendPayoutToDE));
        alchemistCommands=new AlchemistCommands(ruleMap);
        alchemistCommands.setOrdermapper();
        runCommand=alchemistCommands.AllAlchemistCommandMap;
        runCommand.get("create_new_de").runCommand();
        runCommand.get("pre_order_checks").runCommand();
        runCommand.get("assign").runCommand();
        runCommand.get("docaprd").runCommand();
        runCommand.get("doValidate").runCommand();
    }

    @Test(description = "Verify Payout is successful for last Mile and batch count multiple expression", enabled = false)
    public void verifyPayoutforLastMileandBatchCountRule() {
        ruleMap=deliveryServiceApiHelper.createRules("4","last_mile>5 and batch_count<2","delivered");
        SendPayoutToDE=true;
        ruleMap.put("SendPayoutToDE", String.valueOf(SendPayoutToDE));
        alchemistCommands=new AlchemistCommands(ruleMap);
        alchemistCommands.setOrdermapper();
        runCommand=alchemistCommands.AllAlchemistCommandMap;
        runCommand.get("create_new_de").runCommand();
        runCommand.get("pre_order_checks").runCommand();
        runCommand.get("assign").runCommand();
        runCommand.get("docaprd").runCommand();
        runCommand.get("doValidate").runCommand();
    }

    @Test(description = "Verify Payout is successful for Last Mile Time and batch count multiple expression", enabled = false)
    public void verifyPayoutforLastMilTimeeandBatchCountRule() {
        ruleMap=deliveryServiceApiHelper.createRules("4","lmt<15 and batch_count<2","delivered");
        SendPayoutToDE=true;
        ruleMap.put("SendPayoutToDE", String.valueOf(SendPayoutToDE));
        alchemistCommands=new AlchemistCommands(ruleMap);
        alchemistCommands.setOrdermapper();
        runCommand=alchemistCommands.AllAlchemistCommandMap;
        runCommand.get("create_new_de").runCommand();
        runCommand.get("pre_order_checks").runCommand();
        runCommand.get("assign").runCommand();
        runCommand.get("docaprd").runCommand();
        runCommand.get("doValidate").runCommand();
    }

    @Test(description = "Verify Payout is successful for Last Mile Time and batch count multiple expression", enabled = false)
    public void testLastMileTimeAndBatchCount1MultiRuleSet2() {
        ruleMap=deliveryServiceApiHelper.createRules("4","lmt>15 and batch_count<2","delivered");
        SendPayoutToDE=true;
        ruleMap.put("SendPayoutToDE", String.valueOf(SendPayoutToDE));
        alchemistCommands=new AlchemistCommands(ruleMap);
        alchemistCommands.setOrdermapper();
        runCommand=alchemistCommands.AllAlchemistCommandMap;
        runCommand.get("create_new_de").runCommand();
        runCommand.get("pre_order_checks").runCommand();
        runCommand.get("assign").runCommand();
        runCommand.get("docaprd").runCommand();
        runCommand.get("doValidate").runCommand();
    }

    @Test(description = "Verify Payout is successful for first Mile Time and batch count multiple expression", enabled = false)
    public void verifyPayoutforFirstMileTimeandBatchCountRuleSet1() {
        ruleMap=deliveryServiceApiHelper.createRules("4","fmt>15 and batch_count<2","delivered");
        SendPayoutToDE=true;
        ruleMap.put("SendPayoutToDE", String.valueOf(SendPayoutToDE));
        alchemistCommands=new AlchemistCommands(ruleMap);
        alchemistCommands.setOrdermapper();
        runCommand=alchemistCommands.AllAlchemistCommandMap;
        runCommand.get("create_new_de").runCommand();
        runCommand.get("pre_order_checks").runCommand();
        runCommand.get("assign").runCommand();
        runCommand.get("docaprd").runCommand();
        runCommand.get("doValidate").runCommand();
    }

    @Test(description = "Verify Payout is successful for first Mile Time and batch count multiple expression", enabled = false)
    public void verifyPayoutforFirstMileTimeandBatchCountRuleSet2() {
        ruleMap=deliveryServiceApiHelper.createRules("4","fmt<15 and batch_count<2","delivered");
        SendPayoutToDE=true;
        ruleMap.put("SendPayoutToDE", String.valueOf(SendPayoutToDE));
        alchemistCommands=new AlchemistCommands(ruleMap);
        alchemistCommands.setOrdermapper();
        runCommand=alchemistCommands.AllAlchemistCommandMap;
        runCommand.get("create_new_de").runCommand();
        runCommand.get("pre_order_checks").runCommand();
        runCommand.get("assign").runCommand();
        runCommand.get("docaprd").runCommand();
        runCommand.get("doValidate").runCommand();
    }

    @Test(description = "Verify Payout is successful for enabled Rain Mode rule",enabled = false)
    public void verifyPayoutforRainModeRuleSet1() {
        ruleMap=deliveryServiceApiHelper.createRules("4","batch_count>1","delivered");
        SendPayoutToDE=true;

        ruleMap.put("SendPayoutToDE", String.valueOf(SendPayoutToDE));
        alchemistCommands=new AlchemistCommands(ruleMap);
        alchemistCommands.setOrdermapper();
        runCommand=alchemistCommands.AllAlchemistCommandMap;
        runCommand.get("create_new_de").runCommand();
        runCommand.get("pre_order_checks").runCommand();
        runCommand.get("assign").runCommand();
        runCommand.get("docaprd").runCommand();
        runCommand.get("doValidate").runCommand();
    }

    @Test(enabled = false)
    public void testCancelAfterPickUp()
    {
        if(ReadRulesFromDBFlag)
            ruleMap=deliveryServiceApiHelper.verifyActiveRuleInDB("batch_count<2");
        else
            ruleMap=deliveryServiceApiHelper.createRules("4","batch_count>1","pickedup");
        SendPayoutToDE=true;
        ruleMap.put("SendPayoutToDE", String.valueOf(SendPayoutToDE));
        alchemistCommands=new AlchemistCommands(ruleMap);
        alchemistCommands.setOrdermapper();
        alchemistCommands.setCAPRD_To_state("pickedup");
        runCommand=alchemistCommands.AllAlchemistCommandMap;
        runCommand.get("create_new_de").runCommand();
        runCommand.get("pre_order_checks").runCommand();
        runCommand.get("assign").runCommand();
        runCommand.get("docaprd").runCommand();
        runCommand.get("cancelOrder").runCommand();
        runCommand.get("doValidate").runCommand();
    }
}
