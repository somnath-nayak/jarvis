package com.swiggy.api.erp.cms.tests.PreOrder;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.dp.PreOrderDP;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import org.testng.Assert;
import org.testng.annotations.Test;

public class PreOrderAreaScheduleJenkins extends PreOrderDP {
    CMSHelper cmsHelper= new CMSHelper();
   @Test(dataProvider = "areaschedulecreatejenkins",description = "Create pre order area schedule")
    public void preOrderAreaSchedule(String areaId,String menuType, String day, String slotType, String openTime,String closeTime){
        String response=cmsHelper.preOrderAreaScheduleCreate(areaId, menuType, day, slotType, openTime,closeTime).ResponseValidator.GetBodyAsText();
        int status=JsonPath.read(response,"$.status");
        Assert.assertEquals(status,1);


    }
}
