package com.swiggy.api.erp.delivery.tests;


import com.mysql.jdbc.AssertionFailedException;
import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.erp.delivery.dp.BatchingDataProvider;
import com.swiggy.api.erp.delivery.helper.*;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import gherkin.lexer.Th;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.HashMap;


public class BatchingTestCapacity extends BatchingDataProvider {

    RTSHelper rtshelp = new RTSHelper();
    DeliveryHelperMethods deliveryHelperMethods = new DeliveryHelperMethods();
    DeliveryDataHelper deliveryDataHelper = new DeliveryDataHelper();
    DeliveryHelperMethods delmeth = new DeliveryHelperMethods();
    AutoassignHelper autoassignHelper = new AutoassignHelper();
    DeliveryControllerHelper deliveryControllerHelper = new DeliveryControllerHelper();
    DeliveryServiceHelper deliveryServiceHelperhelper = new DeliveryServiceHelper();
    DeliveryServiceApiWrapperHelper deliveryServiceApiHelper = new DeliveryServiceApiWrapperHelper();
    DeliveryDataHelper deldatahelp=new DeliveryDataHelper();
    DeliveryServiceHelper deliveryServiceHelper = new DeliveryServiceHelper();
    HashMap<String,String> map = new HashMap<String,String>();
    String de_id;
    int zone_id = 6;
    String order_id1 = null,order_id2 = null;
    OrderBuilder o1 = null, o2 = null;


    @BeforeClass
    public void enableBatching() {
        String queryToEnableBatching = "update zone set batching_enabled=1;";
        SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(queryToEnableBatching);
        String queryToEnableSingleRestBatch = "update zone set batching_version=1;";
        SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(queryToEnableSingleRestBatch);
    }



    @BeforeMethod
    public void closeBatchesAndCreateDE() {
        String queryToCloseOpenBatches = "update batch set open=0 where zone_id=6;";
        SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(queryToCloseOpenBatches);
        de_id = deliveryDataHelper.CreateDE(6);
        deliveryServiceHelper.makeDEActive(de_id);
        System.out.println("DE ID: "+ de_id);
        String query = "update delivery_boys set enabled=1 where id="+de_id;
        SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(query);

    }

    @AfterMethod
    public void cancelOrdersAndConfigureValuesInDB()
    {
        cancelOrderThroughDB(order_id1);
        cancelOrderThroughDB(order_id2);
        deliveryServiceHelper.makeDEFree(de_id);
        deliveryServiceHelper.markDEInactive(de_id,"0");
        String queryToUpdateMaxOrdersInBatch = "update zone set max_orders_in_batch=4 where id="+zone_id;
        SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(queryToUpdateMaxOrdersInBatch);
        String queryToUpdateMaxSla = "update zone set max_sla_of_batch=100 where id="+zone_id;
        SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(queryToUpdateMaxSla);
        String queryToUpdateMaxItem = "update zone set max_items_in_batch=4 where id="+zone_id;
        SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(queryToUpdateMaxItem);
        String queryToUpdateMaxCustEdge = "update zone set max_cust_edge=100 where id="+zone_id;
        SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(queryToUpdateMaxCustEdge);
        String queryToUpdateMaxOrderDiff = "update zone set max_order_diff=500 where id="+zone_id;
        SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(queryToUpdateMaxOrderDiff);
        String queryToUpdateMaxElapsedTime = "update zone set max_batch_elapsed_time=20 where id="+zone_id;
        SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(queryToUpdateMaxElapsedTime);
        String queryToUpdateOrders = "update trips set cancelled=1 where zone_id=6";
        SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(queryToUpdateOrders);

    }

     @AfterClass
    public void disableDE()
    {
        String queryToDisableDEs = DeliveryConstant.update_De_status+zone_id;
        SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(queryToDisableDEs);

    }

    public void updateLocationOFDEWithOrderID(String order_id)
    {
        String getRestaurantlatlon1 = "select r.lat_long from trips t inner join restaurant r on t.restaurant_id=r.id where t.order_id=" + order_id;
        String restaurantlatlon1 = delmeth.dbhelperget(getRestaurantlatlon1, "lat_long").toString();
        String[] latlong1 = restaurantlatlon1.split(",");
        deldatahelp.updateDELocationInRedis(de_id,latlong1[0],latlong1[1]);
    }


    public void cancelOrderThroughDB(String order_id)
    {
        String queryToCancelOrder = DeliveryConstant.disable_order_for_batching+"'"+order_id+"'";
        SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(queryToCancelOrder);
    }


    @Test(enabled = true, groups = {"batchingregression"},priority = 0,description = "Verify that if the order is a replicated order than it's not considered for batching and also, there is no delay added for this order")
    public void noBatchingIfOrderReplicated() {

              String expectedTag = "is_replicated";
               o1 = new OrderBuilder.Builder("6").is_batching_enabled("1").is_replicated("1").build();
               o2 = new OrderBuilder.Builder("6").is_batching_enabled("1").is_replicated("1").build();
              order_id1 = o1.getOrder_id();
              order_id2 = o2.getOrder_id();
              updateLocationOFDEWithOrderID(order_id1);
              deliveryControllerHelper.waitTill(20);
              autoassignHelper.runAutoAssign("1");
              String batch_id1 = DeliveryDataBaseUtils.getBatchIdFromOrderId(order_id1);
              String batch_id2 = DeliveryDataBaseUtils.getBatchIdFromOrderId(order_id2);
              System.out.println("_________________________________________ Batch ID 1: " + batch_id1);
              System.out.println("_________________________________________ Batch ID 2: " + batch_id2);
              String queryToSelect1 = DeliveryConstant.select_order_tag + "'" + order_id1 + "'";
              Object orderTag1 = delmeth.dbhelperget(queryToSelect1, "order_tags");
              Assert.assertEquals(orderTag1, expectedTag);
              String queryToSelect2 = DeliveryConstant.select_order_tag + "'" + order_id2 + "'";
              Object orderTag2 = delmeth.dbhelperget(queryToSelect2, "order_tags");
              Assert.assertEquals(orderTag2, expectedTag);
              Assert.assertNotEquals(batch_id1, batch_id2);

    }



   //@Test(enabled = true, groups = {"batchingregression"}, priortiy =1, description = "Verify whether two orders from same restaurants under same zone are batched only if both of these orders are non with_de.")
    public void batchingDoneIfSameRestaurant() {
          boolean expectedWith_DE = false;
           o1 = new OrderBuilder.Builder("6").is_batching_enabled("1").build();
           o2 = new OrderBuilder.Builder("6").is_batching_enabled("1").build();
          order_id1 = o1.getOrder_id();
          order_id2 = o2.getOrder_id();
          updateLocationOFDEWithOrderID(order_id1);
          updateLocationOFDEWithOrderID(order_id2);
          String batch_id1 = DeliveryDataBaseUtils.getBatchIdFromOrderId(order_id1);
          String batch_id2 = DeliveryDataBaseUtils.getBatchIdFromOrderId(order_id2);
          deliveryControllerHelper.waitTill(20);
          autoassignHelper.runAutoAssign("1");
          String queryToUpdate1 = DeliveryConstant.update_with_de_to_false + "'" + order_id1 + "'";
          SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(queryToUpdate1);
          String queryToSelect1 = DeliveryConstant.select_with_de_value + "'" + order_id1 + "'";
          Object with_deValue1 = delmeth.dbhelperget(queryToSelect1, "with_de");
          Assert.assertEquals(with_deValue1, expectedWith_DE);
          String queryToUpdate2 = DeliveryConstant.update_with_de_to_false + "'" + order_id2 + "'";
          SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(queryToUpdate2);
          String queryToSelect2 = DeliveryConstant.select_with_de_value + "'" + order_id2 + "'";
          Object with_deValue2 = delmeth.dbhelperget(queryToSelect2, "with_de");
          Assert.assertEquals(with_deValue2, expectedWith_DE);
          Assert.assertEquals(batch_id1, batch_id2);

    }

    @Test(enabled = true, groups = {"batchingregression"}, priority = 2,description = "Verify whether two orders from different restaurants are not batched if batching version is set to 1.")
    public void noBatchingIfBatchingVersionIsOne() {
               String queryToUpdateBatchVersion = DeliveryConstant.update_batch_version + zone_id;
               SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(queryToUpdateBatchVersion);
                o1 = new OrderBuilder.Builder("6").restaurant_id("361").is_batching_enabled("1").build();
                o2 = new OrderBuilder.Builder("6").restaurant_id("362").is_batching_enabled("1").build();
               order_id1 = o1.getOrder_id();
               order_id2 = o2.getOrder_id();
               updateLocationOFDEWithOrderID(order_id1);
               updateLocationOFDEWithOrderID(order_id2);
               System.out.println("____________________________________________ Order ID 1: " + order_id1);
               System.out.println("____________________________________________ Order ID 2: " + order_id2);
               deliveryControllerHelper.waitTill(20);
               autoassignHelper.runAutoAssign("1");
               String batch_id1 = DeliveryDataBaseUtils.getBatchIdFromOrderId(order_id1);
               String batch_id2 = DeliveryDataBaseUtils.getBatchIdFromOrderId(order_id2);
               System.out.println("_________________________________________ Batch ID 1: " + batch_id1);
               System.out.println("_________________________________________ Batch ID 2: " + batch_id2);
               Assert.assertNotEquals(batch_id1, batch_id2);
    }


    @Test(enabled = true, groups = {"batchingregression"},priority = 3, description = "Verify orders are not getting merged if SLA exceeds max sla of batch configured at zone level")
    public void noBatchingIfSlAExceedsConfigValue() {
              int expectedSla = 20;
              String queryToUpdateMaxSLA = DeliveryConstant.update_max_Sla + zone_id;
              SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(queryToUpdateMaxSLA);
              String queryToSelectMaxSLA = DeliveryConstant.select_max_sla + zone_id;
              Object sla_of_batch = delmeth.dbhelperget(queryToSelectMaxSLA, "max_sla_of_batch");
              Assert.assertEquals(expectedSla, sla_of_batch);
               o1 = new OrderBuilder.Builder("6").is_batching_enabled("1").build();
               o2 = new OrderBuilder.Builder("6").is_batching_enabled("1").build();
              order_id1 = o1.getOrder_id();
              String sla1 = deliveryControllerHelper.updateAndSelectSLA("90", order_id1 );
              System.out.println("SLA OF ORDER 1 : "+ sla1);
              order_id2 = o2.getOrder_id();
              deliveryControllerHelper.updateAndSelectSLA("90", order_id2);
              updateLocationOFDEWithOrderID(order_id1);
              updateLocationOFDEWithOrderID(order_id2);
              deliveryControllerHelper.waitTill(20);
              autoassignHelper.runAutoAssign("1");
              String batch_id1 = DeliveryDataBaseUtils.getBatchIdFromOrderId(order_id1);
              String batch_id2 = DeliveryDataBaseUtils.getBatchIdFromOrderId(order_id2);
              System.out.println("_________________________________________ Batch ID 1: " + batch_id1);
              System.out.println("_________________________________________ Batch ID 2: " + batch_id2);
              Assert.assertNotEquals(batch_id1, batch_id2);
    }

    @Test(enabled = true, groups = {"batchingregression"},priority = 4, description = "Verify orders are not getting merged if number of items exceeds max item in batch configured at zone level")
    public void noBatchingIfItemCountExceedsConfigValue() throws InterruptedException {
            // String expectedNumberOfItem = "1";
            String queryToUpdateItem = DeliveryConstant.update_max_item_in_batch + zone_id;
            SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(queryToUpdateItem);

            /*conversion issue from framework, status awaiting*/

        /*String queryToSelectItem =  DeliveryConstant.select_max_item_in_batch+zone_id;
        Object item_in_batch = delmeth.dbhelperget(queryToSelectItem,"max_items_in_batch");
        System.out.print("_______________________________________________________________________--- MAX ITEMS : "+item_in_batch);
        Assert.assertEquals(expectedNumberOfItem,item_in_batch);*/
             o1 = new OrderBuilder.Builder("6").is_batching_enabled("1").build();
             o2 = new OrderBuilder.Builder("6").is_batching_enabled("1").build();
            order_id1 = o1.getOrder_id();
            deliveryControllerHelper.updateAndSelectMaxItems("9", order_id1);
            order_id2 = o2.getOrder_id();
            deliveryControllerHelper.updateAndSelectMaxItems("9", order_id2);
            updateLocationOFDEWithOrderID(order_id1);
            updateLocationOFDEWithOrderID(order_id2);
            deliveryControllerHelper.waitTill(20);
            autoassignHelper.runAutoAssign("1");
            String batch_id1 = DeliveryDataBaseUtils.getBatchIdFromOrderId(order_id1);
            String batch_id2 = DeliveryDataBaseUtils.getBatchIdFromOrderId(order_id2);
            System.out.println("_________________________________________ Batch ID 1: " + batch_id1);
            System.out.println("_________________________________________ Batch ID 2: " + batch_id2);
            Assert.assertNotEquals(batch_id1, batch_id2);
    }


    @Test(enabled = true, groups = {"batchingregression"},priority = 5, description = "Verify orders are not getting merged if distance between customers exceeds max customer distance in batch configured at zone level")
    public void noBatchingIfCustDistanceExceedsConfigValue()  {
                int expectedMaxCustEdge = 10;
                String queryToUpdateEdge = DeliveryConstant.update_max_cust_edge + zone_id;
                SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(queryToUpdateEdge);
                String queryToSelectEdge = DeliveryConstant.select_max_cust_edge + zone_id;
                Object cust_edge = delmeth.dbhelperget(queryToSelectEdge, "max_cust_edge");
                Assert.assertEquals(expectedMaxCustEdge, cust_edge);
                 o1 = new OrderBuilder.Builder("6").lat("12.960514").lng("77.711992").is_batching_enabled("1").build();
                 o2 = new OrderBuilder.Builder("6").lat("12.9596406").lng("77.711659").is_batching_enabled("1").build();
                order_id1 = o1.getOrder_id();
                order_id2 = o2.getOrder_id();
                updateLocationOFDEWithOrderID(order_id1);
                updateLocationOFDEWithOrderID(order_id2);
                deliveryControllerHelper.waitTill(20);
                autoassignHelper.runAutoAssign("1");
                String batch_id1 = DeliveryDataBaseUtils.getBatchIdFromOrderId(order_id1);
                String batch_id2 = DeliveryDataBaseUtils.getBatchIdFromOrderId(order_id2);
                System.out.println("_________________________________________ Batch ID 1: " + batch_id1);
                System.out.println("_________________________________________ Batch ID 2: " + batch_id2);
                Assert.assertNotEquals(batch_id1, batch_id2);
    }

    @Test(enabled = true, groups = {"batchingregression"},priority = 6, description = "Verify orders are not getting merged if max order difference between orders exceeds max orders diff in batch configured at zone level")
    public void noBatchingIfOrderDiffTimeExceedsConfigValue()
    {
             int expectedOrderDiff = 50;
             String queryToUpdateOrderDiff = DeliveryConstant.update_max_order_diff + zone_id;
             SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(queryToUpdateOrderDiff);
             String queryToSelectOrderDiff = DeliveryConstant.select_max_order_diff + zone_id;
             Object order_diff = delmeth.dbhelperget(queryToSelectOrderDiff, "max_order_diff");
             Assert.assertEquals(expectedOrderDiff, order_diff);
              o1 = new OrderBuilder.Builder("6").is_batching_enabled("1").order_time("2018-10-10 16:49:00").build();
              o2 = new OrderBuilder.Builder("6").is_batching_enabled("1").order_time("2018-10-10 18:49:00").build();
             order_id1 = o1.getOrder_id();
             order_id2 = o2.getOrder_id();
             updateLocationOFDEWithOrderID(order_id1);
             updateLocationOFDEWithOrderID(order_id2);
             deliveryControllerHelper.waitTill(20);
             autoassignHelper.runAutoAssign("1");
             String batch_id1 = DeliveryDataBaseUtils.getBatchIdFromOrderId(order_id1);
             String batch_id2 = DeliveryDataBaseUtils.getBatchIdFromOrderId(order_id2);
             System.out.println("_________________________________________ Batch ID 1: " + batch_id1);
             System.out.println("_________________________________________ Batch ID 2: " + batch_id2);
             Assert.assertNotEquals(batch_id1, batch_id2);
    }

    @Test(enabled = true, groups = {"batchingregression"},priority = 8, description = "Verify order are not eligble for merging if order elapsed time of any of the order exceeds max batch elapsed Time configured at zone level")
    public void noBatchingIfElapsedTimeExceedsConfigValue()
    {
              int expectedElapsedTime = 5;
              String queryToUpdateElapsedTime = DeliveryConstant.update_max_elapsed_time + zone_id;
              SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(queryToUpdateElapsedTime);
              String queryToSelectElapsedTime = DeliveryConstant.select_max_elapsed_time + zone_id;
              Object elapsed_time = delmeth.dbhelperget(queryToSelectElapsedTime, "max_batch_elapsed_time");
              Assert.assertEquals(expectedElapsedTime, elapsed_time);
               o1 = new OrderBuilder.Builder("6").is_batching_enabled("1").order_time("2018-10-10 16:49:00").build();
               o2 = new OrderBuilder.Builder("6").is_batching_enabled("1").order_time("2018-10-10 18:49:00").build();
              order_id1 = o1.getOrder_id();
              order_id2 = o2.getOrder_id();
              updateLocationOFDEWithOrderID(order_id1);
              updateLocationOFDEWithOrderID(order_id2);
              deliveryControllerHelper.waitTill(20);
              autoassignHelper.runAutoAssign("1");
              String batch_id1 = DeliveryDataBaseUtils.getBatchIdFromOrderId(order_id1);
              String batch_id2 = DeliveryDataBaseUtils.getBatchIdFromOrderId(order_id2);
              System.out.println("_________________________________________ Batch ID 1: " + batch_id1);
              System.out.println("_________________________________________ Batch ID 2: " + batch_id2);
              Assert.assertNotEquals(batch_id1, batch_id2);
    }

    @Test(enabled = true,groups = "batchingregression",priority = 7,description = "Verify order is not getting merged with already exisiting batch consisting of two orders if maximum orders in batch is configured as two at zone level")
    public void noBatchingIfBatchExceedsConfigValue()
    {
              int expectedMaxOrdersInBatch = 2;
              String queryToUpdateMaxOrders = DeliveryConstant.update_max_orders + zone_id;
              SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(queryToUpdateMaxOrders);
              String queryToSelectMaxOrders = DeliveryConstant.select_max_orders + zone_id;
              Object orders_in_batch = delmeth.dbhelperget(queryToSelectMaxOrders, "max_orders_in_batch");
              Assert.assertEquals(expectedMaxOrdersInBatch, orders_in_batch);
               o1 = new OrderBuilder.Builder("6").is_batching_enabled("1").build();
               o2 = new OrderBuilder.Builder("6").is_batching_enabled("1").build();
              order_id1 = o1.getOrder_id();
              order_id2 = o2.getOrder_id();
              updateLocationOFDEWithOrderID(order_id1);
              updateLocationOFDEWithOrderID(order_id2);
              System.out.println("____________________________________________ Order ID 1: " + order_id1);
              System.out.println("____________________________________________ Order ID 2: " + order_id2);
              deliveryControllerHelper.waitTill(20);
              autoassignHelper.runAutoAssign("1");
              String query1 = "select batch_id from trips where order_id=" + order_id1;
              String batch_id1 = SystemConfigProvider.getTemplate(DeliveryConstant.dbname).queryForMap(query1).get("batch_id").toString();
              String query2 = "select batch_id from trips where order_id=" + order_id2;
              String batch_id2 = SystemConfigProvider.getTemplate(DeliveryConstant.dbname).queryForMap(query2).get("batch_id").toString();
              System.out.println("_________________________________________ Batch ID 1: " + batch_id1);
              System.out.println("_________________________________________ Batch ID 2: " + batch_id2);
              Assert.assertEquals(batch_id1, batch_id2);
              OrderBuilder o3 = new OrderBuilder.Builder("6").is_batching_enabled("1").build();
              String order_id3 = o3.getOrder_id();
              String batch_id3 = DeliveryDataBaseUtils.getBatchIdFromOrderId(order_id3);
              String batchIdToMerge = batch_id1;
              System.out.println("Batch ID 3 : " + batch_id3);
              deliveryControllerHelper.waitTill(20);
              autoassignHelper.runAutoAssign("1");
              Assert.assertNotEquals(batch_id1, batch_id3);
    }

    @Test(enabled = true,groups = "batchingregression",priority = 9,description = "Verify Order is merged with exisitng order assigned to some DE if order status is not marked pickedup")
    public void orderMergedWithExistingOrderIfNotPickedup()
    {
              String queryToUpdateOrders = "update trips set cancelled=1 where zone_id=6";
              SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(queryToUpdateOrders);
               o1 = new OrderBuilder.Builder("6").is_batching_enabled("1").build();
              order_id1 = o1.getOrder_id();
              System.out.println("____________________________________________ Order ID 1: " + order_id1);
              updateLocationOFDEWithOrderID(order_id1);
              deliveryControllerHelper.waitTill(50);
              autoassignHelper.runAutoAssign("1");
              boolean stateChangedToArrived = deliveryDataHelper.doCAPRD(order_id1, de_id, "confirmed", "arrived");
              Assert.assertTrue(stateChangedToArrived);
              deliveryControllerHelper.waitTill(50);
               o2 = new OrderBuilder.Builder("6").is_batching_enabled("1").build();
              order_id2 = o2.getOrder_id();
              System.out.println("____________________________________________ Order ID 2: " + order_id2);
              updateLocationOFDEWithOrderID(order_id2);
              deliveryControllerHelper.waitTill(50);
              autoassignHelper.runAutoAssign("1");
              String queryToSelectBatch1 = DeliveryConstant.select_batch_id + order_id1;
              String batch_id1 = SystemConfigProvider.getTemplate(DeliveryConstant.dbname).queryForMap(queryToSelectBatch1).get("batch_id").toString();
              System.out.println("Batch ID 1 ---- " + batch_id1);
              String queryToSelectBatch2 = DeliveryConstant.select_batch_id + order_id2;
              String batch_id2 = SystemConfigProvider.getTemplate(DeliveryConstant.dbname).queryForMap(queryToSelectBatch2).get("batch_id").toString();
              System.out.println("Batch ID 2 ---- " + batch_id2);
              Assert.assertEquals(batch_id1, batch_id2);

    }

}
