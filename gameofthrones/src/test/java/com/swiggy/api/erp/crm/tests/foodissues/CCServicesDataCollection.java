package com.swiggy.api.erp.crm.tests.foodissues;

import java.util.ArrayList;
import java.util.HashMap;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.swiggy.api.erp.crm.dp.foodissues.CCServicesDP;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.DBHelper;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

public class CCServicesDataCollection {

	Initialize gameofthrones = new Initialize();
	DBHelper dbHelper = new DBHelper();

    ArrayList arrayList = new ArrayList();
	Properties properties = new Properties();

	@Test(enabled = true, description = "CC Services - Ping")
	public void ccPing() throws Exception {

		HashMap<String, String> requestheaders_ccping = new HashMap<String, String>();
		requestheaders_ccping.put("Content-Type", "application/json");
		GameOfThronesService ccping = new GameOfThronesService("cconeview", "ccping", gameofthrones);
		Processor ccping_response = new Processor(ccping, requestheaders_ccping);

		Assert.assertEquals(ccping_response.ResponseValidator.GetResponseCode(), 200);
	}

	@Test(enabled = true, description = "CC Services - HealthCheck",groups = "sanity")
	public void ccHealthCheck() throws Exception {

		HashMap<String, String> requestheaders_cchealthcheck = new HashMap<String, String>();
		requestheaders_cchealthcheck.put("Content-Type", "application/json");
		GameOfThronesService cchealthcheck = new GameOfThronesService("cconeview", "cchealthcheck", gameofthrones);
		Processor cchealthcheck_response = new Processor(cchealthcheck, requestheaders_cchealthcheck);

		Boolean expected = true;

		Assert.assertEquals(cchealthcheck_response.ResponseValidator.GetResponseCode(), 200);

		Boolean mysql = Boolean.parseBoolean(
				cchealthcheck_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("healths.mysql.healthy"));
		
		Assert.assertEquals(mysql, expected);
	}

	@Test(enabled = true, description = "CC Services -Version",groups = "sanity")
	public void ccVersion() throws Exception {

		HashMap<String, String> requestheaders_ccversion = new HashMap<String, String>();
		requestheaders_ccversion.put("Content-Type", "application/json");
		GameOfThronesService ccversion = new GameOfThronesService("cconeview", "ccversion", gameofthrones);
		Processor ccversion_response = new Processor(ccversion, requestheaders_ccversion);

		Assert.assertEquals(ccversion_response.ResponseValidator.GetResponseCode(), 200);
        Assert.assertEquals(ccversion_response.ResponseValidator.GetNodeValue("version"),"0.1-snapshot");
	}

	@Test(enabled = true, dataProviderClass = CCServicesDP.class, dataProvider = "createfoodissuesSanity", groups = "sanity",description = "Create Food Issues - CC Services")
	public void ccCreateFoodIssuesSanity(String key, String[] payloadData) throws Exception {

		// Create Food Issues
		HashMap<String, String> requestheaders_cccreatefoodissues = new HashMap<String, String>();
		requestheaders_cccreatefoodissues.put("Content-Type", "application/json");
		GameOfThronesService cccreatefoodissues = new GameOfThronesService("cconeview", "cccreatefoodissues", gameofthrones);

		String[] orderArray = new String[] { payloadData[0] };
		String[] queryparam = orderArray;

		Processor cccreatefoodissues_response = new Processor(cccreatefoodissues, requestheaders_cccreatefoodissues,
				payloadData, queryparam);

		Assert.assertEquals(cccreatefoodissues_response.ResponseValidator.GetNodeValue("status"), "SUCCESS");
		Assert.assertNotNull(cccreatefoodissues_response.ResponseValidator.GetNodeValueAsInt("data.id"));

	}

	@Test(enabled = true, dataProviderClass = CCServicesDP.class, dataProvider = "createfoodissues", description = "Create Food Issues - CC Services")
	public void ccCreateFoodIssues(String key, String[] payloadData) throws Exception {

		// Create Food Issues
		HashMap<String, String> requestheaders_cccreatefoodissues = new HashMap<String, String>();
		requestheaders_cccreatefoodissues.put("Content-Type", "application/json");
		GameOfThronesService cccreatefoodissues = new GameOfThronesService("cconeview", "cccreatefoodissues", gameofthrones);

		String[] orderArray = new String[] { payloadData[0] };
		String[] queryparam = orderArray;

		Processor cccreatefoodissues_response = new Processor(cccreatefoodissues, requestheaders_cccreatefoodissues,
				payloadData, queryparam);
		
		if (key.contains("CPOSITIVE")) {
			Assert.assertEquals(cccreatefoodissues_response.ResponseValidator.GetNodeValue("status"), "SUCCESS");
			Assert.assertNotNull(cccreatefoodissues_response.ResponseValidator.GetNodeValueAsInt("data.id"));

			int foodIssueId = cccreatefoodissues_response.ResponseValidator.GetNodeValueAsInt("data.id");
			String foodId = Integer.toString(foodIssueId);

		    arrayList.add(foodId);
		    
		} else if (key.contains("CNEGATIVE")) {
			Assert.assertEquals(cccreatefoodissues_response.ResponseValidator.GetNodeValue("status"), "FAILURE");
		}		
	}
	
	@Test(enabled = true, description = "CC Services - based upon food issue id")
	public void getFiId() throws Exception {

	    int ordersize = arrayList.size();

	    for (int i=0; i<ordersize; i++)
	    {
	    	String foodIssueId = (String) arrayList.get(i);
		String[] foodArray = new String[] {foodIssueId};
		String[] foodqueryparam = foodArray;

		HashMap<String, String> requestheaders_ccgetfiId = new HashMap<String, String>();
		requestheaders_ccgetfiId.put("Content-Type", "application/json");
		GameOfThronesService ccgetfiId = new GameOfThronesService("cconeview", "ccgetfiId", gameofthrones);

		Processor ccgetfiId_response = new Processor(ccgetfiId, requestheaders_ccgetfiId, null, foodqueryparam);

		Assert.assertEquals(ccgetfiId_response.ResponseValidator.GetNodeValue("status"), "SUCCESS");
		Assert.assertNotNull(ccgetfiId_response.ResponseValidator.GetNodeValueAsInt("data.id"));
		Assert.assertEquals(ccgetfiId_response.ResponseValidator.GetNodeValueAsInt("data.id"), foodIssueId);
	    }
	}

	@Test(enabled = true,  dataProviderClass = CCServicesDP.class,dataProvider = "updatefoodissues",description = "CC Services - create food issues")
	public void ccUpdateFoodIssues(String key, String[] payloadData) throws Exception {

         		// Create Food Issues
				HashMap<String, String> requestheaders_cccreatefoodissues = new HashMap<String, String>();
				requestheaders_cccreatefoodissues.put("Content-Type", "application/json");
				GameOfThronesService cccreatefoodissues = new GameOfThronesService("cconeview", "cccreatefoodissues", gameofthrones);
        String[] payloadDataCreateIssue = new String[] { "20180612143226","true","123","","","","","","" };

				Processor cccreatefoodissues_response = new Processor(cccreatefoodissues, requestheaders_cccreatefoodissues,
						payloadDataCreateIssue);

				Assert.assertEquals(cccreatefoodissues_response.ResponseValidator.GetNodeValue("status"), "SUCCESS");
					Assert.assertNotNull(cccreatefoodissues_response.ResponseValidator.GetNodeValueAsInt("data.id"));

					int foodIssueId = cccreatefoodissues_response.ResponseValidator.GetNodeValueAsInt("data.id");
					String foodId = Integer.toString(foodIssueId);

                // Update Food Issues
                HashMap<String, String> requestheaders_ccUpdatefoodissues = new HashMap<String, String>();
                requestheaders_ccUpdatefoodissues.put("Content-Type", "application/json");
                GameOfThronesService ccUpdatefoodissues = new GameOfThronesService("cconeview", "ccupdatefoodissues", gameofthrones);

                String[] issueQueryParam = new String[]{foodId};

                Processor ccUpdatefoodissues_response = new Processor(ccUpdatefoodissues,requestheaders_ccUpdatefoodissues,payloadData,issueQueryParam);

                Assert.assertEquals(ccUpdatefoodissues_response.ResponseValidator.GetNodeValue("status"), "SUCCESS");
                Assert.assertEquals(ccUpdatefoodissues_response.ResponseValidator.GetNodeValueAsInt("statusCode"), 0);


	}

}
