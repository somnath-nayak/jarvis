package com.swiggy.api.erp.cms.tests;

import org.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.dp.POP_dataProvider;
import com.swiggy.api.erp.cms.helper.POP_City_Schedule_Helper;

import framework.gameofthrones.JonSnow.Processor;

/*
 * City scheduling can be created from this class and same details can be updated for city-code=5 (Mumbai)
 */

public class POP_City_Schedule extends POP_dataProvider {
	int newCityScheduleId=0;
	POP_City_Schedule_Helper obj = new POP_City_Schedule_Helper();
	
	@Test(dataProvider="cityScheduleCREATEData", priority=0, groups={"Sanity_TC", "Smoke_TC", "Regression_TC"},
			description="Creates a city schedule")
	public void createCitySchedule(String slot_type, int city_id, int openTime,
			int closeTime, String day, String menu_type) throws JSONException, InterruptedException
	{
		  Processor p1 = obj.createCity_schedule_helper(slot_type, city_id, openTime, closeTime, day, menu_type);
		  String response = p1.ResponseValidator.GetBodyAsText();
		  newCityScheduleId = JsonPath.read(response, "$.data.id");
		  System.out.println("Newly created City-Schedule-id  = " + newCityScheduleId);
		  int statusCode = JsonPath.read(response, "$.status");
		  Assert.assertEquals(statusCode, 1);
	}

	@Test(dataProvider="cityScheduleUPDATEData", priority=1, groups={"Sanity_TC", "Smoke_TC", "Regression_TC"},
			description="Updates the created city schedule")
	public void updateCitySchedule(String slot_type, int city_id, int openTime,
			int closeTime, String day, String menu_type) throws JSONException
	{
		Processor p1 = obj.updateCity_schedule_helper(slot_type, city_id, openTime, closeTime, day, menu_type, newCityScheduleId );
		
		String response = p1.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(response, "$.status");
		Assert.assertEquals(statusCode, 1);
	}

	@Test(priority=2, groups={"Sanity_TC", "Smoke_TC", "Regression_TC"},
			description="Retrieves the created city schedule")
	public void getCitySchedule() throws JSONException
	{
		Processor p1 = obj.getCity_schedule_helper(newCityScheduleId);
		String response = p1.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(response, "$.status");
		Assert.assertEquals(statusCode, 1);
	}

	@Test(priority=3, groups={"Sanity_TC", "Smoke_TC", "Regression_TC"},
			description="Deletes the created city schedule")
	public void deleteCitySchedule() throws JSONException
	{
		Processor p1 = obj.deleteCity_schedule_helper(newCityScheduleId);
		String response = p1.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(response, "$.status");
		Assert.assertEquals(statusCode, 1);
		System.out.println("Deleted id successfully = ");
	}

	@Test(dataProvider="invalidcityScheduleCREATEData", priority=4, groups={"Regression_TC", "Smoke_TC"},
			description="Validating while creating city schedule for invalid/non-existence city id")
	public void createInvalidCitySchedule(String slot_type, int city_id, int openTime,
			int closeTime, String day, String menu_type) throws JSONException
	{
		Processor p1 = obj.create_InvalidCity_schedule_helper(slot_type, city_id, openTime, closeTime, day, menu_type);
		
		String response = p1.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(response, "$.status");
		String message = JsonPath.read(response, "$.message");
		String entity_id = JsonPath.read(response, "$.data.field_validation_errors[0].value");
		
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(message, "Validation Error");
		Assert.assertEquals(entity_id, "Entity doesn't exist.");
	}

	@Test(dataProvider="cityScheduleWithExistingSlot", priority=5, groups={"Regression_TC"},
			description="Validating for creating a city-schedule with existing city-schedule slot")
	public void createDuplicateCitySchedule(String slot_type, int city_id, int openTime,
			int closeTime, String day, String menu_type) throws JSONException
	{
		Processor p1 = obj.create_City_schedule_For_ExistingSlot_helper(slot_type, city_id, openTime, closeTime, day, menu_type);
		
		String response = p1.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(response, "$.status");
		String message = JsonPath.read(response, "$.message");
		String openTimeValue = JsonPath.read(response, "$.data.field_validation_errors[0].value");
		String closeTimeValue = JsonPath.read(response, "$.data.field_validation_errors[1].value");

		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(message, "Validation Error");
		Assert.assertEquals(openTimeValue, "Open time shouldn't lie between existing slots.");
		Assert.assertEquals(closeTimeValue, "Schedule shouldn't cover existing schedule.");
	}

	@Test(dataProvider="cityScheduleWithInvalidDayName", priority=6, groups={"Regression_TC"},
			description="Validating for creating a city-schedule with invalid/full Day Name")
	public void createCityScheduleWithInvalidOREmptyDayName(String slot_type, int city_id, int openTime,
			int closeTime, String day, String menu_type) throws JSONException
	{
		Processor p1 = obj.create_City_schedule_With_Invalid_OR_Empty_DayName_helper(slot_type, city_id, openTime, closeTime, day, menu_type);
		String response = p1.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(response, "$.status");
		String errorMsg = JsonPath.read(response, "$.error");
		Assert.assertEquals(statusCode, 400);
		Assert.assertEquals(errorMsg, "Bad Request");
	}


}
