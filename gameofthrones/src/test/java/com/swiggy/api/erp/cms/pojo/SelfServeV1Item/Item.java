package com.swiggy.api.erp.cms.pojo.SelfServeV1Item;

import com.swiggy.api.erp.cms.constants.SelfServeConstants;

public class Item {

	private String addons;

	private String third_party_id;

	private Variants variants;

	private int in_stock;

	private String type;

	private Batch_info batch_info;

	private double price;

	private String id;

	private String eligible_for_long_distance;

	private String addon_limit;

	private String order;

	private String is_spicy;

	private String image_url;

	private String service_charges;

	private String vat;

	private String active;

	private String created_by;

	private int is_veg;

	private String is_perishable;

	private String service_tax;

	private String name;

	private String updated_by;

	private String image_id;

	private String pop_usage_type;

	private String uniqueId;

	private String serves_how_many;

	private String third_party_category_name;

	private String third_party_category_order;

	private String third_party_sub_category_order;

	private String restaurant_id;

	private String description;

	private String is_discoverable;

	private String enabled;

	private String addon_free_limit;

	private Item_sub_info item_sub_info;

	private String preparation_style;

	private Variants_v2 variants_v2;

	private Catalog_attributes catalog_attributes;

	private String category_id;

	private String sub_category_id;

	private String commission;

	private String addon_description;

	private Catalog_multi_value_attributes[] catalog_multi_value_attributes;

	private String packing_charges;

	private String packing_slab_count;

	private Gst_details gst_details;

	private String s3_image_url;

	private String recommended;

	private String crop_choices;

	private String third_party_sub_category_name;

	private String comment;

	private String variant_description;

	private String temp_item_id;

	public String getAddons ()
	{
		return addons;
	}

	public void setAddons (String addons)
	{
		this.addons = addons;
	}

	public String getThird_party_id ()
	{
		return third_party_id;
	}

	public void setThird_party_id (String third_party_id)
	{
		this.third_party_id = third_party_id;
	}

	public Variants getVariants ()
	{
		return variants;
	}

	public void setVariants (Variants variants)
	{
		this.variants = variants;
	}

	public int getIn_stock ()
	{
		return in_stock;
	}

	public void setIn_stock (int isveg)
	{
		this.in_stock = isveg;
	}

	public String getType ()
	{
		return type;
	}

	public void setType (String type)
	{
		this.type = type;
	}

	public Batch_info getBatch_info ()
	{
		return batch_info;
	}

	public void setBatch_info (Batch_info batch_info)
	{
		this.batch_info = batch_info;
	}

	public double getPrice ()
	{
		return price;
	}

	public void setPrice (double price)
	{
		this.price = price;
	}

	public String getId ()
	{
		return id;
	}

	public void setId (String id)
	{
		this.id = id;
	}

	public String getEligible_for_long_distance ()
	{
		return eligible_for_long_distance;
	}

	public void setEligible_for_long_distance (String eligible_for_long_distance)
	{
		this.eligible_for_long_distance = eligible_for_long_distance;
	}

	public String getAddon_limit ()
	{
		return addon_limit;
	}

	public void setAddon_limit (String addon_limit)
	{
		this.addon_limit = addon_limit;
	}

	public String getOrder ()
	{
		return order;
	}

	public void setOrder (String order)
	{
		this.order = order;
	}

	public String getIs_spicy ()
	{
		return is_spicy;
	}

	public void setIs_spicy (String is_spicy)
	{
		this.is_spicy = is_spicy;
	}

	public String getImage_url ()
	{
		return image_url;
	}

	public void setImage_url (String image_url)
	{
		this.image_url = image_url;
	}

	public String getService_charges ()
	{
		return service_charges;
	}

	public void setService_charges (String service_charges)
	{
		this.service_charges = service_charges;
	}

	public String getVat ()
	{
		return vat;
	}

	public void setVat (String vat)
	{
		this.vat = vat;
	}

	public String getActive ()
	{
		return active;
	}

	public void setActive (String active)
	{
		this.active = active;
	}

	public String getCreated_by ()
	{
		return created_by;
	}

	public void setCreated_by (String created_by)
	{
		this.created_by = created_by;
	}

	public int getIs_veg ()
	{
		return is_veg;
	}

	public void setIs_veg (int is_veg)
	{
		this.is_veg = is_veg;
	}

	public String getIs_perishable ()
	{
		return is_perishable;
	}

	public void setIs_perishable (String is_perishable)
	{
		this.is_perishable = is_perishable;
	}

	public String getService_tax ()
	{
		return service_tax;
	}

	public void setService_tax (String service_tax)
	{
		this.service_tax = service_tax;
	}

	public String getName ()
	{
		return name;
	}

	public void setName (String name)
	{
		this.name = name;
	}

	public String getUpdated_by ()
	{
		return updated_by;
	}

	public void setUpdated_by (String updated_by)
	{
		this.updated_by = updated_by;
	}

	public String getImage_id ()
	{
		return image_id;
	}

	public void setImage_id (String image_id)
	{
		this.image_id = image_id;
	}

	public String getPop_usage_type ()
	{
		return pop_usage_type;
	}

	public void setPop_usage_type (String pop_usage_type)
	{
		this.pop_usage_type = pop_usage_type;
	}

	public String getUniqueId ()
	{
		return uniqueId;
	}

	public void setUniqueId (String uniqueId)
	{
		this.uniqueId = uniqueId;
	}

	public String getServes_how_many ()
	{
		return serves_how_many;
	}

	public void setServes_how_many (String serves_how_many)
	{
		this.serves_how_many = serves_how_many;
	}

	public String getThird_party_category_name ()
	{
		return third_party_category_name;
	}

	public void setThird_party_category_name (String third_party_category_name)
	{
		this.third_party_category_name = third_party_category_name;
	}

	public String getThird_party_category_order ()
	{
		return third_party_category_order;
	}

	public void setThird_party_category_order (String third_party_category_order)
	{
		this.third_party_category_order = third_party_category_order;
	}

	public String getThird_party_sub_category_order ()
	{
		return third_party_sub_category_order;
	}

	public void setThird_party_sub_category_order (String third_party_sub_category_order)
	{
		this.third_party_sub_category_order = third_party_sub_category_order;
	}

	public String getRestaurant_id ()
	{
		return restaurant_id;
	}

	public void setRestaurant_id (String restaurant_id)
	{
		this.restaurant_id = restaurant_id;
	}

	public String getDescription ()
	{
		return description;
	}

	public void setDescription (String description)
	{
		this.description = description;
	}

	public String getIs_discoverable ()
	{
		return is_discoverable;
	}

	public void setIs_discoverable (String is_discoverable)
	{
		this.is_discoverable = is_discoverable;
	}

	public String getEnabled ()
	{
		return enabled;
	}

	public void setEnabled (String enabled)
	{
		this.enabled = enabled;
	}

	public String getAddon_free_limit ()
	{
		return addon_free_limit;
	}

	public void setAddon_free_limit (String addon_free_limit)
	{
		this.addon_free_limit = addon_free_limit;
	}

	public Item_sub_info getItem_sub_info ()
	{
		return item_sub_info;
	}

	public void setItem_sub_info (Item_sub_info item_sub_info)
	{
		this.item_sub_info = item_sub_info;
	}

	public String getPreparation_style ()
	{
		return preparation_style;
	}

	public void setPreparation_style (String preparation_style)
	{
		this.preparation_style = preparation_style;
	}

	public Variants_v2 getVariants_v2 ()
	{
		return variants_v2;
	}

	public void setVariants_v2 (Variants_v2 variants_v2)
	{
		this.variants_v2 = variants_v2;
	}

	public Catalog_attributes getCatalog_attributes ()
	{
		return catalog_attributes;
	}

	public void setCatalog_attributes (Catalog_attributes catalog_attributes)
	{
		this.catalog_attributes = catalog_attributes;
	}

	public String getCategory_id ()
	{
		return category_id;
	}

	public void setCategory_id (String category_id)
	{
		this.category_id = category_id;
	}

	public String getSub_category_id ()
	{
		return sub_category_id;
	}

	public void setSub_category_id (String sub_category_id)
	{
		this.sub_category_id = sub_category_id;
	}

	public String getCommission ()
	{
		return commission;
	}

	public void setCommission (String commission)
	{
		this.commission = commission;
	}

	public String getAddon_description ()
	{
		return addon_description;
	}

	public void setAddon_description (String addon_description)
	{
		this.addon_description = addon_description;
	}

	public Catalog_multi_value_attributes[] getCatalog_multi_value_attributes ()
	{
		return catalog_multi_value_attributes;
	}

	public void setCatalog_multi_value_attributes (Catalog_multi_value_attributes[] catalog_multi_value_attributes)
	{
		this.catalog_multi_value_attributes = catalog_multi_value_attributes;
	}

	public String getPacking_charges ()
	{
		return packing_charges;
	}

	public void setPacking_charges (String packing_charges)
	{
		this.packing_charges = packing_charges;
	}

	public String getPacking_slab_count ()
	{
		return packing_slab_count;
	}

	public void setPacking_slab_count (String packing_slab_count)
	{
		this.packing_slab_count = packing_slab_count;
	}

	public Gst_details getGst_details ()
	{
		return gst_details;
	}

	public void setGst_details (Gst_details gst_details)
	{
		this.gst_details = gst_details;
	}

	public String getS3_image_url ()
	{
		return s3_image_url;
	}

	public void setS3_image_url (String s3_image_url)
	{
		this.s3_image_url = s3_image_url;
	}

	public String getRecommended ()
	{
		return recommended;
	}

	public void setRecommended (String recommended)
	{
		this.recommended = recommended;
	}

	public String getCrop_choices ()
	{
		return crop_choices;
	}

	public void setCrop_choices (String crop_choices)
	{
		this.crop_choices = crop_choices;
	}

	public String getThird_party_sub_category_name ()
	{
		return third_party_sub_category_name;
	}

	public void setThird_party_sub_category_name (String third_party_sub_category_name)
	{
		this.third_party_sub_category_name = third_party_sub_category_name;
	}

	public String getComment ()
	{
		return comment;
	}

	public void setComment (String comment)
	{
		this.comment = comment;
	}

	public String getVariant_description ()
	{
		return variant_description;
	}

	public void setVariant_description (String variant_description)
	{
		this.variant_description = variant_description;
	}

	public String getTemp_item_id ()
	{
		return temp_item_id;
	}

	public void setTemp_item_id (String temp_item_id)
	{
		this.temp_item_id = temp_item_id;
	}

	public Item build() {
		setDefaultValues();
		return this;

	}
	public Item build(String itemId) {
		setDefaultValuesforUpdateItem();
		return this;

	}

	private void setDefaultValues() {
		if(this.getRestaurant_id()==null)
			this.setRestaurant_id(SelfServeConstants.restId[0]);
		if(this.getIs_veg()==0)
			this.setIs_veg(SelfServeConstants.isVeg[0]);
		if(this.getIn_stock()==0)
			this.setIn_stock(SelfServeConstants.isVeg[0]);
		if(this.getPrice()==0)
			this.setPrice(SelfServeConstants.price);
		if(this.getName()==null)
			this.setName(SelfServeConstants.itemName);
		
	}

	private void setDefaultValuesforUpdateItem() {
		if(this.getIs_veg()==0)
			this.setIs_veg(SelfServeConstants.isVeg[0]);
		if(this.getIn_stock()==0)
			this.setIn_stock(SelfServeConstants.isVeg[0]);
		if(this.getPrice()==0)
			this.setPrice(SelfServeConstants.price);
		if(this.getName()==null)
			this.setName(SelfServeConstants.itemName);

	}
	@Override
	public String toString()
	{
		return "ClassPojo [addons = "+addons+", third_party_id = "+third_party_id+", variants = "+variants+", in_stock = "+in_stock+", type = "+type+", batch_info = "+batch_info+", price = "+price+", id = "+id+", eligible_for_long_distance = "+eligible_for_long_distance+", addon_limit = "+addon_limit+", order = "+order+", is_spicy = "+is_spicy+", image_url = "+image_url+", service_charges = "+service_charges+", vat = "+vat+", active = "+active+", created_by = "+created_by+", is_veg = "+is_veg+", is_perishable = "+is_perishable+", service_tax = "+service_tax+", name = "+name+", updated_by = "+updated_by+", image_id = "+image_id+", pop_usage_type = "+pop_usage_type+", uniqueId = "+uniqueId+", serves_how_many = "+serves_how_many+", third_party_category_name = "+third_party_category_name+", third_party_category_order = "+third_party_category_order+", third_party_sub_category_order = "+third_party_sub_category_order+", restaurant_id = "+restaurant_id+", description = "+description+", is_discoverable = "+is_discoverable+", enabled = "+enabled+", addon_free_limit = "+addon_free_limit+", item_sub_info = "+item_sub_info+", preparation_style = "+preparation_style+", variants_v2 = "+variants_v2+", catalog_attributes = "+catalog_attributes+", category_id = "+category_id+", sub_category_id = "+sub_category_id+", commission = "+commission+", addon_description = "+addon_description+", catalog_multi_value_attributes = "+catalog_multi_value_attributes+", packing_charges = "+packing_charges+", packing_slab_count = "+packing_slab_count+", gst_details = "+gst_details+", s3_image_url = "+s3_image_url+", recommended = "+recommended+", crop_choices = "+crop_choices+", third_party_sub_category_name = "+third_party_sub_category_name+", comment = "+comment+", variant_description = "+variant_description+", temp_item_id = "+temp_item_id+"]";
	}

}

