package com.swiggy.api.erp.delivery.tests;

import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.erp.delivery.dp.CAPRDFromExotelDataProvider;
import com.swiggy.api.erp.delivery.helper.AutoassignHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryHelperMethods;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceHelper;

import framework.gameofthrones.JonSnow.Processor;

public class CAPRDFlowsExotel extends CAPRDFromExotelDataProvider {
	
	DeliveryServiceHelper helpdel = new DeliveryServiceHelper();
	AutoassignHelper hepauto = new AutoassignHelper();
	DeliveryHelperMethods delmeth = new DeliveryHelperMethods();

	public CAPRDFlowsExotel()
	{
		super(DeliveryConstant.caprdrestaurantid,DeliveryConstant.caprdrestaurantLat,DeliveryConstant.getCaprdrestaurantLon);
	}

	@Test(dataProvider = "QE-349", groups = "CAPRDregressionMayur", description = "Verify order is marked confirmed through exotel when order is assigned to DE")
	public void CAPRDFROMExotel349(String order_id,String de_id, String version)
			throws InterruptedException {
		Assert.assertNotNull(order_id,"Order is not created through checkout helper");
		hepauto.delogin(de_id, version);
		String deotp= null;
		try {
			deotp = (new JSONObject(helpdel.getOtp(de_id.toString()).ResponseValidator.GetBodyAsText())).get("data").toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		//String deotp = delmeth.redisconnectget(DeliveryConstant.de_otp, de_id, DeliveryConstant.redisdb);
		hepauto.deotp(de_id, deotp);
		helpdel.makeDEFree(de_id);
		Thread.sleep(10000);
		helpdel.assignOrder(order_id, de_id);
		Processor processor=helpdel.exotelConfirmed(DeliveryConstant.DEmobile, DeliveryConstant.exotelConfirm);
		if((processor.ResponseValidator.GetResponseCode())==200)
		{
		String query = "Select * from trips where order_id=" + order_id + ";";
		Object confirmed = delmeth.dbhelperget(query, "confirmed_time");
		if (null == confirmed) {
			helpdel.makeDEFree(de_id);
			Assert.assertTrue(false);

		} else {
		     helpdel.makeDEFree(de_id);
		Assert.assertTrue(true);

		}
		}
	}
	@Test(dataProvider = "QE-350", groups = "CAPRDregressionMayur", description = "Verify order is not marked confirmed through exotel when order is not assigned to DE")
	public void CAPRDFROMExotel350(String order_id,String de_id, String version)
			throws InterruptedException {
		Assert.assertNotNull(order_id,"Order is not created through checkout helper");
		hepauto.delogin(de_id, version);
		String deotp= null;
		try {
			deotp = (new JSONObject(helpdel.getOtp(de_id.toString()).ResponseValidator.GetBodyAsText())).get("data").toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		//String deotp = delmeth.redisconnectget(DeliveryConstant.de_otp, de_id, DeliveryConstant.redisdb);
		hepauto.deotp(de_id, deotp);
		helpdel.makeDEFree(de_id);
		Thread.sleep(10000);
		Processor processor=helpdel.exotelConfirmed(DeliveryConstant.DEmobile, DeliveryConstant.exotelConfirm);
		if((processor.ResponseValidator.GetResponseCode())==200)
		{
		String query = "Select * from trips where order_id=" + order_id + ";";
		Object confirmed = delmeth.dbhelperget(query, "confirmed_time");
		if (null == confirmed) {
			helpdel.makeDEFree(de_id);
			Assert.assertTrue(true);

		} else {
		     helpdel.makeDEFree(de_id);
		Assert.assertTrue(false);

		}
		}
	}

	@Test(dataProvider = "QE-351", groups = "CAPRDregressionMayur", description = "Verify order is marked arrived through exotel when order status is confirmed")
	public void CAPRDFROMExotel351(String order_id,String de_id, String version)
			throws InterruptedException {
		Assert.assertNotNull(order_id,"Order is not created through checkout helper");
		hepauto.delogin(de_id, version);
		String deotp= null;
		try {
			deotp = (new JSONObject(helpdel.getOtp(de_id.toString()).ResponseValidator.GetBodyAsText())).get("data").toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		//String deotp = delmeth.redisconnectget(DeliveryConstant.de_otp, de_id, DeliveryConstant.redisdb);
		hepauto.deotp(de_id, deotp);
		helpdel.makeDEFree(de_id);
		Thread.sleep(10000);
		helpdel.assignOrder(order_id, de_id);
		Thread.sleep(5000);
		helpdel.zipDialConfirmDE(order_id);
		Thread.sleep(5000);
		Processor processor=helpdel.exotelArrived(DeliveryConstant.DEmobile, DeliveryConstant.exotelArrived);
		if((processor.ResponseValidator.GetResponseCode())==200)
		{
		String query = "Select * from trips where order_id=" + order_id + ";";
		Object arrived = delmeth.dbhelperget(query, "arrived_time");
		if (null == arrived) {
			helpdel.makeDEFree(de_id);
			Assert.assertTrue(false);

		} else {
		     helpdel.makeDEFree(de_id);
		Assert.assertTrue(true);

		}
		}
	}
	
	@Test(dataProvider = "QE-352", groups = "CAPRDregressionMayur", description = "Verify order is not marked arrived through exotel when order status is assigned")
	public void CAPRDFROMExotel352(String order_id,String de_id, String version)
			throws InterruptedException {
		Assert.assertNotNull(order_id,"Order is not created through checkout helper");
		hepauto.delogin(de_id, version);
		String deotp= null;
		try {
			deotp = (new JSONObject(helpdel.getOtp(de_id.toString()).ResponseValidator.GetBodyAsText())).get("data").toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		//String deotp = delmeth.redisconnectget(DeliveryConstant.de_otp, de_id,DeliveryConstant.redisdb);
		hepauto.deotp(de_id, deotp);
		helpdel.makeDEFree(de_id);
		Thread.sleep(10000);
		helpdel.assignOrder(order_id, de_id);
		Thread.sleep(5000);
		Processor processor=helpdel.exotelArrived(DeliveryConstant.DEmobile, DeliveryConstant.exotelArrived);
		if((processor.ResponseValidator.GetResponseCode())==200)
		{
		String query = "Select * from trips where order_id=" + order_id + ";";
		Object arrived = delmeth.dbhelperget(query, "arrived_time");
		if (null == arrived) {
			helpdel.makeDEFree(de_id);
			Assert.assertTrue(true);

		} else {
		     helpdel.makeDEFree(de_id);
		Assert.assertTrue(false);

		}
		}
	}
	@Test(dataProvider = "QE-353", groups = "CAPRDregressionMayur", description = "Verify order is marked pickedup through exotel when order status is arrived")
	public void CAPRDFROMExotel353(String order_id,String de_id, String version)
			throws InterruptedException {
		Assert.assertNotNull(order_id,"Order is not created through checkout helper");
		hepauto.delogin(de_id, version);
		String deotp= null;
		try {
			deotp = (new JSONObject(helpdel.getOtp(de_id.toString()).ResponseValidator.GetBodyAsText())).get("data").toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		//String deotp = delmeth.redisconnectget(DeliveryConstant.de_otp, de_id,DeliveryConstant.redisdb);
		hepauto.deotp(de_id, deotp);
		helpdel.makeDEFree(de_id);
		Thread.sleep(10000);
		helpdel.assignOrder(order_id, de_id);
		Thread.sleep(5000);
		helpdel.zipDialConfirmDE(order_id);
		Thread.sleep(5000);
		helpdel.zipDialArrivedDE(order_id);
		Thread.sleep(5000);
		Processor processor=helpdel.exotelPickedup(DeliveryConstant.DEmobile, DeliveryConstant.exotelPickedup);
		if((processor.ResponseValidator.GetResponseCode())==200)
		{
		String query = "Select * from trips where order_id=" + order_id + ";";
		Object arrived = delmeth.dbhelperget(query, "pickedup_time");
		if (null == arrived) {
			helpdel.makeDEFree(de_id);
			Assert.assertTrue(false);

		} else {
		     helpdel.makeDEFree(de_id);
		Assert.assertTrue(true);

		}
		}
	}
	
	@Test(dataProvider = "QE-354", groups = "CAPRDregressionMayur", description = "Verify order is not marked pickedup through exotel when order status is confirmed")
	public void CAPRDFROMExotel354(String order_id,String de_id, String version)
			throws InterruptedException {
		Assert.assertNotNull(order_id,"Order is not created through checkout helper");
		hepauto.delogin(de_id, version);
		String deotp= null;
		try {
			deotp = (new JSONObject(helpdel.getOtp(de_id.toString()).ResponseValidator.GetBodyAsText())).get("data").toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		//String deotp = delmeth.redisconnectget(DeliveryConstant.de_otp, de_id,DeliveryConstant.redisdb);
		hepauto.deotp(de_id, deotp);
		helpdel.makeDEFree(de_id);
		Thread.sleep(10000);
		helpdel.assignOrder(order_id, de_id);
		Thread.sleep(5000);
		helpdel.zipDialConfirmDE(order_id);
		Thread.sleep(5000);
		Processor processor=helpdel.exotelPickedup(DeliveryConstant.DEmobile, DeliveryConstant.exotelPickedup);
		if((processor.ResponseValidator.GetResponseCode())==200)
		{
		String query = "Select * from trips where order_id=" + order_id + ";";
		Object arrived = delmeth.dbhelperget(query, "pickedup_time");
		if (null == arrived) {
			helpdel.makeDEFree(de_id);
			Assert.assertTrue(true);

		} else {
		     helpdel.makeDEFree(de_id);
		Assert.assertTrue(false);

		}
		}
	}
	@Test(dataProvider = "QE-355", groups = "CAPRDregressionMayur", description = "Verify order is marked reached through exotel when order status is pickedup")
	public void CAPRDFROMExotel355(String order_id,String de_id, String version)
			throws InterruptedException {
		Assert.assertNotNull(order_id,"Order is not created through checkout helper");
		hepauto.delogin(de_id, version);
		String deotp= null;
		try {
			deotp = (new JSONObject(helpdel.getOtp(de_id.toString()).ResponseValidator.GetBodyAsText())).get("data").toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		//String deotp = delmeth.redisconnectget(DeliveryConstant.de_otp, de_id, DeliveryConstant.redisdb);
		hepauto.deotp(de_id, deotp);
		helpdel.makeDEFree(de_id);
		Thread.sleep(10000);
		helpdel.assignOrder(order_id, de_id);
		Thread.sleep(5000);
		helpdel.zipDialConfirmDE(order_id);
		Thread.sleep(5000);
		helpdel.zipDialArrivedDE(order_id);
		Thread.sleep(5000);
		helpdel.zipDialPickedUpDE(order_id);
		Processor processor=helpdel.exotelReached(DeliveryConstant.DEmobile, DeliveryConstant.exotelReached);
		if((processor.ResponseValidator.GetResponseCode())==200)
		{
		String query = "Select * from trips where order_id=" + order_id + ";";
		Object arrived = delmeth.dbhelperget(query, "reached_time");
		if (null == arrived) {
			helpdel.makeDEFree(de_id);
			Assert.assertTrue(false);

		} else {
		     helpdel.makeDEFree(de_id);
		Assert.assertTrue(true);

		}
		}
	}
	
	@Test(dataProvider = "QE-356", groups = "CAPRDregressionMayur", description = "Verify order is not marked reached through exotel when order status is arrived")
	public void CAPRDFROMExotel356(String order_id,String de_id, String version)
			throws InterruptedException {
		Assert.assertNotNull(order_id,"Order is not created through checkout helper");
		hepauto.delogin(de_id, version);
		String deotp= null;
		try {
			deotp = (new JSONObject(helpdel.getOtp(de_id.toString()).ResponseValidator.GetBodyAsText())).get("data").toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		//String deotp = delmeth.redisconnectget(DeliveryConstant.de_otp, de_id, DeliveryConstant.redisdb);
		hepauto.deotp(de_id, deotp);
		helpdel.makeDEFree(de_id);
		Thread.sleep(10000);
		helpdel.assignOrder(order_id, de_id);
		Thread.sleep(5000);
		helpdel.zipDialConfirmDE(order_id);
		Thread.sleep(5000);
		helpdel.zipDialArrivedDE(order_id);
		Processor processor=helpdel.exotelReached(DeliveryConstant.DEmobile, DeliveryConstant.exotelReached);
		if((processor.ResponseValidator.GetResponseCode())==200)
		{
		String query = "Select * from trips where order_id=" + order_id + ";";
		Object arrived = delmeth.dbhelperget(query, "reached_time");
		if (null == arrived) {
			helpdel.makeDEFree(de_id);
			Assert.assertTrue(true);

		} else {
		     helpdel.makeDEFree(de_id);
		Assert.assertTrue(false);

		}
		}
	}
	@Test(dataProvider = "QE-357", groups = "CAPRDregressionMayur", description = "Verify order is marked delivered through exotel when order status is reached")
	public void CAPRDFROMExotel357(String order_id,String de_id, String version)
			throws InterruptedException {
		Assert.assertNotNull(order_id,"Order is not created through checkout helper");
		hepauto.delogin(de_id, version);
		String deotp= null;
		try {
			deotp = (new JSONObject(helpdel.getOtp(de_id.toString()).ResponseValidator.GetBodyAsText())).get("data").toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		//String deotp = delmeth.redisconnectget(DeliveryConstant.de_otp, de_id, DeliveryConstant.redisdb);
		hepauto.deotp(de_id, deotp);
		helpdel.makeDEFree(de_id);
		Thread.sleep(10000);
		helpdel.assignOrder(order_id, de_id);
		Thread.sleep(5000);
		helpdel.zipDialConfirmDE(order_id);
		Thread.sleep(5000);
		helpdel.zipDialArrivedDE(order_id);
		Thread.sleep(5000);
		helpdel.zipDialPickedUpDE(order_id);
		Thread.sleep(5000);
		helpdel.zipDialReachedDE(order_id);
		Processor processor=helpdel.exotelDelivered(DeliveryConstant.DEmobile, DeliveryConstant.exotelDelivered);
		if((processor.ResponseValidator.GetResponseCode())==200)
		{
		String query = "Select * from trips where order_id=" + order_id + ";";
		Object arrived = delmeth.dbhelperget(query, "delivered_time");
		if (null == arrived) {
			helpdel.makeDEFree(de_id);
			Assert.assertTrue(false);

		} else {
		     helpdel.makeDEFree(de_id);
		Assert.assertTrue(true);

		}
		}
	}
	@Test(dataProvider = "QE-358", groups = "CAPRDregressionMayur", description = "Verify order is not marked delivered through exotel when order status is pickedup")
	public void CAPRDFROMExotel358(String order_id,String de_id, String version)
			throws InterruptedException {
		Assert.assertNotNull(order_id,"Order is not created through checkout helper");
		hepauto.delogin(de_id, version);
		String deotp= null;
		try {
			deotp = (new JSONObject(helpdel.getOtp(de_id.toString()).ResponseValidator.GetBodyAsText())).get("data").toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		//String deotp = delmeth.redisconnectget(DeliveryConstant.de_otp, de_id, DeliveryConstant.redisdb);
		hepauto.deotp(de_id, deotp);
		helpdel.makeDEFree(de_id);
		Thread.sleep(10000);
		helpdel.assignOrder(order_id, de_id);
		Thread.sleep(5000);
		helpdel.zipDialConfirmDE(order_id);
		Thread.sleep(5000);
		helpdel.zipDialArrivedDE(order_id);
		Thread.sleep(5000);
		helpdel.zipDialPickedUpDE(order_id);
		Processor processor=helpdel.exotelDelivered(DeliveryConstant.DEmobile, DeliveryConstant.exotelDelivered);
		if((processor.ResponseValidator.GetResponseCode())==200)
		{
		String query = "Select * from trips where order_id=" + order_id + ";";
		Object arrived = delmeth.dbhelperget(query, "delivered_time");
		if (null == arrived) {
			helpdel.makeDEFree(de_id);
			Assert.assertTrue(true);

		} else {
		     helpdel.makeDEFree(de_id);
		Assert.assertTrue(false);

		}
		}
	}
}
