package com.swiggy.api.erp.delivery.helper;

import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Daenerys.Initializer;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.RedisHelper;

import java.util.HashMap;

public class AutoassignHelper {
	Initialize gameofthrones;
	DeliveryHelperMethods delmeth;
	DeliveryServiceHelper helpdel;
	//Initialize gameofthrones = new Initialize();
	

	public AutoassignHelper()
	{
		gameofthrones = Initializer.getInitializer();
		delmeth = new DeliveryHelperMethods();
		helpdel = new DeliveryServiceHelper();
	}
	
	public Processor delogin(String de_id, String version) {
		GameOfThronesService service = new GameOfThronesService(
				"deliveryservice", "delogin", gameofthrones);
		HashMap<String, String> headers = delmeth.formdataheaders();
		HashMap<String, String> formcontent = new HashMap<String, String>();
		formcontent.put("id", de_id);
		formcontent.put("version", version);
		Processor response = new Processor(service, headers, null, null,
				formcontent);
		return response;

	}

	public Processor deotp(String de_id, String otp) {
		GameOfThronesService service = new GameOfThronesService(
				"deliveryservice", "deotp", gameofthrones);
		HashMap<String, String> headers = delmeth.formdataheaders();
		HashMap<String, String> formcontent = new HashMap<String, String>();
		formcontent.put("id", de_id);
		formcontent.put("otp", otp);
		Processor response = new Processor(service, headers, null, null,
				formcontent);
		return response;
	}

	public Processor locupdate(String lat, String lng, String header) {
		HashMap<String, String> requestheaders = delmeth.singleheader();
		requestheaders.put("Authorization", header);
		GameOfThronesService service = new GameOfThronesService(
				"deliverylocation", "updatedelocation", gameofthrones);
		String[] payloadparams = new String[] { lat, lng };
		Processor processor = new Processor(service, requestheaders,
				payloadparams);
		return processor;
	}

	
	// new location moved to golang
	
	public Processor DElocation(String lat, String lng, String accuracy,String discardlocation,String header)
			throws InterruptedException {
		GameOfThronesService service = new GameOfThronesService(
				"deliverylocation", "Delocation", gameofthrones);
		HashMap<String, String> requestheaders = delmeth.singleheader();
		requestheaders.put("Authorization", header);
		HashMap<String, String> headers = delmeth.formdataheaders();
		HashMap<String, String> formcontent = new HashMap<String, String>();
		formcontent.put("lat", lat);
		formcontent.put("lng", lng);
		formcontent.put("accuracy", accuracy);
		formcontent.put("discardedLocation", discardlocation);
		
      Processor response = new Processor(service, headers, null, null,
				formcontent);
		return response;
	}

	
	
	
	// update parameter for autoassigment
	public Processor updateParamAuto(String City, String zone, String Param_id, String Param_value, String user,String Param_name)
			{
		HashMap<String, String> requestheaders = AutoassignHelper.singleheader();
		GameOfThronesService service = new GameOfThronesService(
				"deliveryservice", "autoassignupdateparam", gameofthrones);
		String[] payloadparams = new String[]{City,zone,Param_id,Param_value,user,Param_name};
		Processor processor = new Processor(service, requestheaders,
				payloadparams);
		return processor;
	}

	public Boolean runAutoAssign(String city_id)
	{
		HashMap<String, String> requestheaders = new HashMap<>();//AutoassignHelper.singleheader();
		GameOfThronesService service = new GameOfThronesService("assignmentrun", "runassignment", gameofthrones);
		System.out.println("city_id is "+city_id);
		String[] payloadparams = new String[] {city_id};
		Processor processor = new Processor(service, requestheaders,null,payloadparams);
		//Processor p1=						new  Processor(service, requestheaders, null, new String[]{de_id});
		return(processor.ResponseValidator.GetBodyAsText().equalsIgnoreCase("OK"));
	}


	public static HashMap<String, String> doubleheader() {
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("content-type", "application/json");
		requestheaders.put("Authorization", "Basic R0dZU1dJOjIwMTVTVyFHR1k=");
		return requestheaders;
	}

	public static HashMap<String, String> singleheader() {
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("content-type", "application/json");
		return requestheaders;
	}

	public static HashMap<String, String> formdataheaders() {
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("authorization", DeliveryConstant.basicauth);
		requestheaders.put("content-type", "application/form-data");
		return requestheaders;
	}

	public String redisconnect(String keyinitial,String entity,int redisdb)
	{
		RedisHelper red = new RedisHelper();
		String s = keyinitial;
		String key = s.concat(entity);
		String value = (String) red.getValue("deliveryredis",redisdb, key);
		return value;

	}
}