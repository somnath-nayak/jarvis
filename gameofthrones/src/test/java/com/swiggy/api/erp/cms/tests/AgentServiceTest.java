package com.swiggy.api.erp.cms.tests;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.dp.AgentServiceDP;
import com.swiggy.api.erp.cms.helper.AgentHelper;

public class AgentServiceTest extends AgentServiceDP {
	AgentHelper agentHelper= new AgentHelper();
	
	@Test(dataProvider = "AgentServiceDP", priority = 1, description = "Creating Agent")
    public void agentCreate(String agent_type,String auth_user_id,String auth_key,String appSecret,int expectedCode)
    {
        String response= agentHelper.createAgent(agent_type,auth_user_id,auth_key,appSecret).ResponseValidator.GetBodyAsText();
        int actualCode = JsonPath.read(response, "$.statusCode");
	    Assert.assertEquals(actualCode,expectedCode,"Successful");
	
    }
	



}
