package com.swiggy.api.erp.cms.pojo.BaseServiceBulkVariant;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.erp.cms.pojo.BaseServiceBulkVariant
 **/
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.swiggy.api.erp.cms.helper.BaseServiceHelper;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "active",
        "created_at",
        "created_by",
        "default",
        "gst_details",
        "id",
        "in_stock",
        "is_veg",
        "name",
        "order",
        "price",
        "third_party_id",
        "updated_at",
        "updated_by",
        "variant_group_id"
})
public class Variant {

    BaseServiceHelper baseServiceHelper = new BaseServiceHelper();

    @JsonProperty("active")
    private Boolean active;
    @JsonProperty("created_at")
    private String created_at;
    @JsonProperty("created_by")
    private String created_by;
    @JsonProperty("default")
    private Integer _default;
    @JsonProperty("gst_details")
    private Gst_details gst_details;
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("in_stock")
    private Integer in_stock;
    @JsonProperty("is_veg")
    private Integer is_veg;
    @JsonProperty("name")
    private String name;
    @JsonProperty("order")
    private Integer order;
    @JsonProperty("price")
    private Double price;
    @JsonProperty("third_party_id")
    private String third_party_id;
    @JsonProperty("updated_at")
    private String updated_at;
    @JsonProperty("updated_by")
    private String updated_by;
    @JsonProperty("variant_group_id")
    private Integer variant_group_id;

    @JsonProperty("active")
    public Boolean getActive() {
        return active;
    }

    @JsonProperty("active")
    public void setActive(Boolean active) {
        this.active = active;
    }

    @JsonProperty("created_at")
    public String getCreated_at() {
        return created_at;
    }

    @JsonProperty("created_at")
    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    @JsonProperty("created_by")
    public String getCreated_by() {
        return created_by;
    }

    @JsonProperty("created_by")
    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    @JsonProperty("default")
    public Integer getDefault() {
        return _default;
    }

    @JsonProperty("default")
    public void setDefault(Integer _default) {
        this._default = _default;
    }

    @JsonProperty("gst_details")
    public Gst_details getGst_details() {
        return gst_details;
    }

    @JsonProperty("gst_details")
    public void setGst_details(Gst_details gst_details) {
        this.gst_details = gst_details;
    }

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("in_stock")
    public Integer getIn_stock() {
        return in_stock;
    }

    @JsonProperty("in_stock")
    public void setIn_stock(Integer in_stock) {
        this.in_stock = in_stock;
    }

    @JsonProperty("is_veg")
    public Integer getIs_veg() {
        return is_veg;
    }

    @JsonProperty("is_veg")
    public void setIs_veg(Integer is_veg) {
        this.is_veg = is_veg;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("order")
    public Integer getOrder() {
        return order;
    }

    @JsonProperty("order")
    public void setOrder(Integer order) {
        this.order = order;
    }

    @JsonProperty("price")
    public Double getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(Double price) {
        this.price = price;
    }

    @JsonProperty("third_party_id")
    public String getThird_party_id() {
        return third_party_id;
    }

    @JsonProperty("third_party_id")
    public void setThird_party_id(String third_party_id) {
        this.third_party_id = third_party_id;
    }

    @JsonProperty("updated_at")
    public String getUpdated_at() {
        return updated_at;
    }

    @JsonProperty("updated_at")
    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    @JsonProperty("updated_by")
    public String getUpdated_by() {
        return updated_by;
    }

    @JsonProperty("updated_by")
    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    @JsonProperty("variant_group_id")
    public Integer getVariant_group_id() {
        return variant_group_id;
    }

    @JsonProperty("variant_group_id")
    public void setVariant_group_id(Integer variant_group_id) {
        this.variant_group_id = variant_group_id;
    }

    public void setDefaultValues() {
        String timestamp = baseServiceHelper.getTimeStampRandomised();
        Gst_details gst_details = new Gst_details();
        if(getDefault() == null)
            this.setDefault(1);
        if(getActive() == null)
            this.setActive(true);
        if(this.getCreated_at() == null)
            this.setCreated_by("Automation_BaseService_Suite");
        if(this.getCreated_at() == null)
            this.setCreated_at("2018-04-23T08:02:21.654Z");
        if(this.getName() == null)
            this.setName("Automation_BaseService_"+timestamp);
        if(this.getOrder() == null)
            this.setOrder(0);
        if(this.getThird_party_id() == null)
            this.setThird_party_id("Automation_"+timestamp);
        if(this.getUpdated_by() == null)
            this.setUpdated_by("Automation_BaseService");
        if(this.getUpdated_at() == null)
            this.setUpdated_at("2018-04-23T08:02:21.654Z");
        if(this.getPrice() == null)
            this.setPrice(100.0);
        if(this.getIs_veg() == null)
            this.setIs_veg(1);
        if(this.getIn_stock() == null)
            this.setIn_stock(1);
        if(this.getVariant_group_id() == null)
            this.setVariant_group_id(0);
        this.setGst_details(gst_details.build());
    }

    public Variant build() {
        setDefaultValues();
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("active", active).append("created_at", created_at).append("created_by", created_by).append("_default", _default).append("gst_details", gst_details).append("id", id).append("in_stock", in_stock).append("is_veg", is_veg).append("name", name).append("order", order).append("price", price).append("third_party_id", third_party_id).append("updated_at", updated_at).append("updated_by", updated_by).append("variant_group_id", variant_group_id).toString();
    }

}
