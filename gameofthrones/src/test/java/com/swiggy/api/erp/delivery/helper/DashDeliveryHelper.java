package com.swiggy.api.erp.delivery.helper;

import com.swiggy.api.castleblack.CastleBlackManager;
import com.swiggy.automation.utils.HeaderUtils;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Daenerys.Initializer;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.DBHelper;

import java.util.HashMap;

public class DashDeliveryHelper extends DeliveryServiceHelper {

    Initialize gameofthrones = Initializer.getInitializer();
    DeliveryHelperMethods del = new DeliveryHelperMethods();
    CastleBlackManager castleBlackManager= new CastleBlackManager();
    EndToEndHelp endToEndHelp= new EndToEndHelp();
    DBHelper dbHelper = new DBHelper();
    boolean b;
    HashMap<String, String> requestheaders ;
    // TODO Auto-generated constructor stub
    public DashDeliveryHelper()
    {}

    public Processor delogin(String de_id, String version) {
        GameOfThronesService service = new GameOfThronesService(
                "deliveryservice", "delogin", gameofthrones);

        Processor processor = new Processor(service, null, null,
                new String[]{version,de_id});
        return processor;

    }
    public Processor validateOtp(String de_id, String otp) {
        GameOfThronesService service = new GameOfThronesService(
                "deliveryservice", "validateotp", gameofthrones);

        Processor processor = new Processor(service, null, null,
                new String[]{otp,de_id});
        return processor;

    }

    public Processor getEarningHistory(String de_id,String auth){
        GameOfThronesService service = new GameOfThronesService(
                "deliveryservice", "earninghistory", gameofthrones);
        Processor processor = new Processor(service, HeaderUtils.getHeaderForDeApp(auth));
        return processor;
    }

    public Processor getEarninghistoryOfWeek(String de_id,String auth,int week){
        GameOfThronesService service = new GameOfThronesService(
                "deliveryservice", "earninghistorybyweek", gameofthrones);
        Processor processor = new Processor(service, HeaderUtils.getHeaderForDeApp(auth));
        return processor;
    }

    public Processor getAllChannelOptions(String auth){
        Processor processor=null;
        try {
            GameOfThronesService gots = new GameOfThronesService("deliveryservice", "allchannel", gameofthrones);
            processor = new Processor(gots, HeaderUtils.getHeaderForDeApp(auth));
        }
        catch(Exception e){
            return null;
        }
        return processor;

    }
    public Processor getSpecificChannelDetail(String auth,String channelId){
        Processor processor=null;
        try {
            GameOfThronesService gots = new GameOfThronesService("deliveryservice", "getspecificchannel", gameofthrones);
            String[] payloadparam={channelId};
            processor = new Processor(gots, HeaderUtils.getHeaderForDeApp(auth),null,new String[]{channelId});
        }
        catch(Exception e){
            return null;
        }
        return processor;

    }
}
