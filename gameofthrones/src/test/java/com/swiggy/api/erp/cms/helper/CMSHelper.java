package com.swiggy.api.erp.cms.helper;


//import com.google.gdata.data.dublincore.Date;

import com.google.gson.Gson;
import com.jayway.jsonpath.JsonPath;
import com.rabbitmq.client.AMQP;
import com.swiggy.api.erp.cms.constants.CmsConstants;
import com.swiggy.api.erp.cms.constants.RestaurantConstants;
import com.swiggy.api.erp.cms.pojo.AddItemToRest.AddItemToRestaurant;
import com.swiggy.api.erp.cms.pojo.AddItemToRest.RestBuilder;
import com.swiggy.api.erp.cms.pojo.AttributeItem.ItemAttribute;
import com.swiggy.api.erp.cms.pojo.Category;
import com.swiggy.api.erp.cms.pojo.ItemsCat;
import com.swiggy.api.erp.cms.pojo.PartnerUpdate;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Daenerys.Initializer;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import framework.gameofthrones.Tyrion.RabbitMQHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;
import org.apache.commons.io.FileUtils;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CMSHelper {


    //Initialize gameofthrones = new Initialize();
    Initialize gameofthrones = Initializer.getInitializer();
    RabbitMQHelper rmqHelper = new RabbitMQHelper();

    public List<Map<String, Object>> getItemsCreated2(String store_id, String external_item_id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.getItems_fullmenu1 + store_id
                + CmsConstants.getItems_fullmenuMiddle1 + external_item_id + CmsConstants.getItems_end);
        return list;
    }

    public Map<String, Object> getItemsCreated1(String store_id, String external_item_id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.getItems_fullmenu1 + external_item_id
                + CmsConstants.getItems_fullmenuMiddle1 + store_id + CmsConstants.getItems_end);
        // Map<String, Object> map=list.get(0);
        return (list.size() > 0) ? list.get(0) : null;
    }

    public List<Map<String, Object>> getVariantGroupsCreated(String store_id, String external_item_id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.getItems_VariantGroup + external_item_id
                + CmsConstants.getItems_VariantGroup1 + store_id + CmsConstants.getItems_VariantGroup2);
        return list;
    }

    public List<Map<String, Object>> getVariantCreated(String store_id, String external_item_id,
                                                       String variant_GroupID) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate
                .queryForList(CmsConstants.getItems_Variant + external_item_id + CmsConstants.getItems_Variant1
                        + store_id + CmsConstants.getItems_Variant2 + variant_GroupID + CmsConstants.getItems_Variant3);
        System.out.println("test");
        return list;
    }

    public List<Map<String, Object>> getAddonGroupCreated(String store_id, String external_item_id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.getItems_AddonGroup + external_item_id
                + CmsConstants.getItems_AddonGroup1 + store_id + CmsConstants.getItems_AddonGroup2);
        System.out.println("test");
        return list;
    }

    public List<Map<String, Object>> getaddonCreated(String store_id, String external_item_id, String addon_ExternalID,
                                                     String addon_group_externalID) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.getItems_addons + addon_ExternalID
                + CmsConstants.getItems_addons1 + external_item_id + CmsConstants.getItems_addons2 + store_id
                + CmsConstants.getItems_addons3 + addon_group_externalID + CmsConstants.getItems_addons4);
        System.out.println("test");
        return list;
    }

    public List<Map<String, Object>> getItemSlot(String store_id, String external_item_id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.getItemsSlot + external_item_id
                + CmsConstants.getItemsSlot1 + store_id + CmsConstants.getItemsSlot2);
        System.out.println("test");
        return list;
    }

    public List<Map<String, Object>> getCatlogGstDetails(String external_item_id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate
                .queryForList(CmsConstants.getCatlogGst + external_item_id + CmsConstants.getCatlogGst1);
        System.out.println("test");
        return list;
    }


    public List<Map<String, Object>> getRestaurantIdByName(String restName) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.restaurantdIdByName + restName + CmsConstants.endMySqlLike);
        return list;
    }

    public List<Map<String, Object>> getRestaurantIdByNameExact(String restName) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.restaurantdIdByNameExact + restName + CmsConstants.endquotes);
        return list;
    }

    public int getRestaurantOpenTime(String restaurantId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.restaurantOpenTime + restaurantId);
        return (list.size() > 0) ? (Integer) (list.get(0).get(CmsConstants.opentime)) : 0;
    }

    public int getAreaOpenTime(String areaId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.areaOpenTime + areaId);
        return (list.size() > 0) ? (Integer) (list.get(0).get(CmsConstants.opentime)) : 0;
    }

    public int getCityOpenTime(String cityId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.cityOpenTime + cityId);
        return (list.size() > 0) ? (Integer) (list.get(0).get(CmsConstants.opentime)) : 0;
    }

    public int getAreaCloseTime(String areaId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.areaCloseTime + areaId);
        return (list.size() > 0) ? (Integer) (list.get(0).get(CmsConstants.closetime)) : 0;
    }

    public int getCityCloseTime(String cityId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.cityCloseTime + cityId);
        return (list.size() > 0) ? (Integer) (list.get(0).get(CmsConstants.closetime)) : 0;
    }

    public int getRestaurantCloseTime(String restaurantId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.restaurantClosedTime + restaurantId);
        return (list.size() > 0) ? (Integer) (list.get(0).get(CmsConstants.closetime)) : 0;
    }

    public void setAreaOpenTime(String areaId, int value) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.updateAreaOpenTime + value + CmsConstants.whereId + areaId);
    }

    public void setAreaCloseTime(String areaId, int value) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.updateAreaCloseTime + value + CmsConstants.whereId + areaId);
    }

    public void setCityOpenTime(String cityId, int value) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.updateCityOpenTime + value + CmsConstants.whereId + cityId);
    }

    public void setCityCloseTime(String cityId, int value) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.updateCityCloseTime + value + CmsConstants.whereId + cityId);
    }

    public void setRestaurantCloseTime(String restaurantId, int value) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.updateRestaurantCloseTime + value + CmsConstants.whereId + restaurantId);
    }

    public void setRestaurantOpenTime(String restaurantId, int value) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.updateRestaurantOpenTime + value + CmsConstants.whereId + restaurantId);
    }

    public int getAreaEnabled(String areaId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.areaEnabled + areaId);
        return (list.size() > 0) ? (Integer) (list.get(0).get(CmsConstants.enabled)) : 0;
    }

    public int getCityEnabled(String cityId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.cityEnabled + cityId);
        return (list.size() > 0) ? (Integer) (list.get(0).get(CmsConstants.enabled)) : 0;
    }

    public int getRestaurantEnabled(String restaurantId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.restaurantEnabled + restaurantId);
        return (list.size() > 0) ? (Integer) (list.get(0).get(CmsConstants.enabled)) : 0;
    }

    public void setRestaurantEnabled(String restaurantId, int value) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.updateRestaurantEnabled + value + CmsConstants.whereId + restaurantId);
    }

    public void setCityEnabled(String cityId, int value) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.updateCityEnabled + value + CmsConstants.whereId + cityId);
    }

    public void setAreaEnabled(String areaId, int value) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.updateAreaEnabled + value + CmsConstants.whereId + areaId);
    }

    public String getCityBannerMessage(String cityId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.cityBanner + cityId);
        return (list.size() > 0) ? list.get(0).get(CmsConstants.bannermessage).toString() : "Not Found";
    }

    public String getAreaBannerMessage(String areaId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.areaBanner + areaId);
        return (list.size() > 0) ? list.get(0).get(CmsConstants.bannermessage).toString() : "Not Found";
    }


    public void setAreaBanner(String areaId, String value) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.updateAreaBanner + value + CmsConstants.whereIdString + areaId);
    }

    public void setCityBanner(String cityId, String value) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.updateCityBanner + value + CmsConstants.whereIdString + cityId);
    }

    public int getAreaOpen(String areaId) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.areaOpen + areaId);
        return (list.size() > 0) ? (Integer) (list.get(0).get(CmsConstants.isopen)) : 0;
    }

    public int getCityOpen(String cityId) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.cityOpen + cityId);
        return (list.size() > 0) ? (Integer) (list.get(0).get(CmsConstants.isopen)) : 0;
    }

    public void setCityOpen(String cityId, int value) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.updateCityOpen + value + CmsConstants.whereId + cityId);
    }

    public void setAreaOpen(String areaId, int value) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.updateAreaOpen + value + CmsConstants.whereId + areaId);
    }

    public int getAreaSlotOpenHour(String areaId) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.areaslotsOpenHour + areaId);
        return (list.size() > 0) ? (Integer) (list.get(0).get(CmsConstants.opentime)) : 0;
    }

    public int getAreaSlotCloseHour(String areaId) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.areaslotsCloseHour + areaId);
        return (list.size() > 0) ? (Integer) (list.get(0).get(CmsConstants.closetime)) : 0;
    }

    public int getCitySlotOpenHour(String cityId) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.cityslotsOpenHour + cityId);
        return (list.size() > 0) ? (Integer) (list.get(0).get(CmsConstants.opentime)) : 0;
    }

    public int getCitySlotCloseHour(String cityId) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.cityslotCloseHour + cityId);
        return (list.size() > 0) ? (Integer) (list.get(0).get(CmsConstants.closetime)) : 0;
    }

    public void setAreaslotsOpenHour(String areaId, int value) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.updateAreaslotOpenHour + value + CmsConstants.whereId + areaId);
    }

    public void setAreaslotsCloseHour(String areaId, int value) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.updateAreaslotCloseHour + value + CmsConstants.whereId + areaId);
    }

    public void setCityslotsOpenHour(String cityId, int value) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.updateCityslotOpenHour + value + CmsConstants.whereId + cityId);
    }

    public void setCityslotsCloseHour(String cityId, int value) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.updateCityslotCloseHour + value + CmsConstants.whereId + cityId);
    }

    public int getItemisVeg(String itemId) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.getisveg + itemId);
        return (list.size() > 0) ? (Integer) (list.get(0).get(CmsConstants.isveg)) : 0;
    }

    public void setItemisVeg(String itemId, int value) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.updateisveg + value + CmsConstants.whereId + itemId);
    }

    public int getItemisInStock(String itemId) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.getinstock + itemId);
        return (list.size() > 0) ? (Integer) (list.get(0).get(CmsConstants.instock)) : 0;
    }

    public void setItemInStock(String itemId, int value) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.updateinstock + value + CmsConstants.whereId + itemId);
    }

    public boolean getIsLongDistanceEnabled(String restaurantId) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.islongdistance + restaurantId);
        return (list.size() > 0) ? (boolean) (list.get(0).get(CmsConstants.longdistance)) : false;
    }

    public void setIsLongDistanceEnabled(String restaurantId, int value) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.updatelongdistance + value + CmsConstants.whereId + restaurantId);
    }

    public boolean getIsSwiggyAssured(String restaurantId) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.isswiggyassured + restaurantId);
        return (list.size() > 0) ? (boolean) (list.get(0).get(CmsConstants.assured)) : false;
    }

    public void setIsSwiggyAssured(String restaurantId, int value) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.setisassured + value + CmsConstants.whereId + restaurantId);
    }

    public boolean getIsSwiggySelect(String restaurantId) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.isselect + restaurantId);
        return (list.size() > 0) ? (boolean) (list.get(0).get(CmsConstants.swiggyselect)) : false;
    }

    public void setIsSwiggySelect(String restaurantId, int value) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.setisselect + value + CmsConstants.whereId + restaurantId);
    }

    public int getItemRecommended(String itemId) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.itemrecommend + itemId);
        return (list.size() > 0) ? (int) (list.get(0).get(CmsConstants.recommended)) : 0;
    }

    public void setItemRecommended(String itemId, int value) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.updaterecommend + value + CmsConstants.whereId + itemId);
    }

    public boolean getItemEligibleForLongDistance(String itemId) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.eligiblelongdistance + itemId);
        return (list.size() > 0) ? (boolean) (list.get(0).get(CmsConstants.eligiblelong)) : false;
    }

    public void setItemEligibleForLongDistance(String itemId, int value) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.updateeligiblelongdistance + value + CmsConstants.whereId + itemId);
    }

    public int getItemIsEnabled(String itemId) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.itemenable + itemId);
        return (list.size() > 0) ? (int) (list.get(0).get(CmsConstants.enabled)) : 0;
    }

    public void setItemIsEnabled(String itemId, int value) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.updateitemenable + value + CmsConstants.whereId + itemId);
    }

    public List<Map<String, Object>> getAreaHolidaySlot(String areaId) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.areaholiday + areaId);
        return list;
    }

    public List<Map<String, Object>> getCityHolidaySlot(String cityId) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.cityholiday + cityId);
        return list;
    }

    public String getWpOptionsValue(String option_name) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.getwpoptions + option_name + CmsConstants.endquotes);
        return (list.size() > 0) ? (list.get(0).get(CmsConstants.optionvalue)).toString() : "Key Not Found";
    }


    public Processor getRestaurantHolidaySlot(String restaurantId) {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers = getDefaultHeaders();
        String[] urlparams = new String[1];
        urlparams[0] = restaurantId;
        GameOfThronesService service = new GameOfThronesService("cms", "restaurantholidayslot", gameofthrones);
        Processor response = new Processor(service, headers, null, urlparams);
        return response;
    }

    public Processor setRestaurantHolidaySlot(String restaurantId, String fromtime, String totime) {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers = getDefaultHeaders();
        headers.put(CmsConstants.usermetakey, CmsConstants.usermetavalue);
        String[] payloadparams = new String[3];
        payloadparams[0] = restaurantId;
        payloadparams[1] = fromtime;
        payloadparams[2] = totime;
        GameOfThronesService service = new GameOfThronesService("cms", "updaterestaurantholidayslot", gameofthrones);
        return new Processor(service, headers, payloadparams);
    }

    public Processor getMenuHolidaySlot(String menuId) {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers = getDefaultHeaders();
        String[] urlparams = new String[1];
        urlparams[0] = menuId;
        GameOfThronesService service = new GameOfThronesService("cms_menu", "getmenuholidayslot", gameofthrones);
        Processor response = new Processor(service, headers, null, urlparams);
        return response;
    }

    public Processor setMenuHolidaySlot(String menuId, String fromtime, String totime) {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers = getDefaultHeaders();
        headers.put(CmsConstants.usermetakey2, CmsConstants.usermetavalue);
        String[] payloadparams = new String[3];
        payloadparams[0] = menuId;
        payloadparams[1] = fromtime;
        payloadparams[2] = totime;
        GameOfThronesService service = new GameOfThronesService("cms_menu", "updatemenuholidayslot", gameofthrones);
        return new Processor(service, headers, payloadparams);
    }

    public Processor getItemSlot(String itemId) {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers = getDefaultHeaders();
        String[] urlparams = new String[1];
        urlparams[0] = itemId;
        GameOfThronesService service = new GameOfThronesService("cms_menu", "getitemslot", gameofthrones);
        Processor response = new Processor(service, headers, null, urlparams);
        return response;
    }

    public Processor setItemSlot(String itemId, String dayOfWeek, String fromtime, String totime) {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers = getDefaultHeaders();
        headers.put(CmsConstants.usermetakey2, CmsConstants.usermetavalue2);
        String[] payloadparams = new String[5];
        payloadparams[0] = itemId;
        payloadparams[1] = dayOfWeek;
        payloadparams[2] = fromtime;
        payloadparams[3] = totime;
        payloadparams[4] = CmsConstants.usermetavalue2;
        GameOfThronesService service = new GameOfThronesService("cms_menu", "updateitemslot", gameofthrones);
        return new Processor(service, headers, payloadparams);
    }

    public Processor getItemHolidaySlot(String itemId) {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers = getDefaultHeaders();
        String[] urlparams = new String[1];
        urlparams[0] = itemId;
        GameOfThronesService service = new GameOfThronesService("cms_menu", "getitemholidaylot", gameofthrones);
        Processor response = new Processor(service, headers, null, urlparams);
        return response;
    }

    public Processor setItemHolidaySlot(String itemId, String fromtime, String totime) {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers = getDefaultHeaders();
        headers.put(CmsConstants.usermetakey2, CmsConstants.usermetavalue);
        String[] payloadparams = new String[3];
        payloadparams[0] = itemId;
        payloadparams[1] = fromtime;
        payloadparams[2] = totime;
        GameOfThronesService service = new GameOfThronesService("cms_menu", "updateitemholidayslot", gameofthrones);
        return new Processor(service, headers, payloadparams);
    }

    public HashMap<String, String> getDefaultHeaders() {
        HashMap<String, String> header = new HashMap<String, String>();
        header.put(CmsConstants.usermetakey, CmsConstants.usermetavalue);
        header.put("Content-Type", "application/json");
        return header;
    }


    public Processor getPopItemForRestaurant(String restaurantId) {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers = getDefaultHeaders();
        String[] urlparams = new String[2];
        urlparams[0] = restaurantId;
        urlparams[1] = CmsConstants.poptype;
        GameOfThronesService service = new GameOfThronesService("cms_menu", "getpopitem", gameofthrones);
        return new Processor(service, headers, null, urlparams);
    }

    public List<Map<String, Object>> getItemDetails(String restaurant_id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.getItems + restaurant_id + CmsConstants.getItemsEnd);
        return list;
    }

    public List<String> getCategoriesDetails(String restaurant_id) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.getCategories + restaurant_id + CmsConstants.getItemsEnd);
        return (List<String>) list.get(0).get("cat_id");
    }

    public List<Map<String, Object>> getCategories(String restaurant_id) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.getcategories + restaurant_id + CmsConstants.getcategoriesend);
        return list;
    }

    public List<Map<String, Object>> getPrice(String restaurant_id) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.getPrice + restaurant_id + ";");
        return list;
    }

    public List<Map<String, Object>> getSubcategory(String restaurant_id, String category_id) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        String query = CmsConstants.getsubcategory + restaurant_id + CmsConstants.categoryid + category_id + CmsConstants.getsubcategoryend;
        System.out.println(query);
        List<Map<String, Object>> list = sqlTemplate.queryForList(query);
        return list;
    }

    public List<Map<String, Object>> getItem(String restaurant_id, String category_id, String subcategory_id) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        String query = CmsConstants.getitem.replace(CmsConstants.rest, restaurant_id).replace(CmsConstants.category, category_id).replace(CmsConstants.subcategory, subcategory_id);
        List<Map<String, Object>> list = sqlTemplate.queryForList(query);
        return list;
    }

    public List<Map<String, Object>> getItemsFromRest(String restaurant_id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.getValidRestItem + restaurant_id);
        return list;
    }

    public List<String> getSubCategoriesDetails(String restaurant_id) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.getCategories + restaurant_id + CmsConstants.getItemsEnd);
        return (List<String>) list.get(0).get("cat_id");
    }

    public Processor setRestaurantSlot(String restaurant_id, String open_time, String close_time, String day) {
        HashMap<String, String> headers = getDefaultHeaders();
        //headers = getDefaultHeaders();
        //headers.put(CmsConstants.usermetakey2,CmsConstants.usermetavalue2);
        //headers.put("Content-Type", "application/json");
        //headers.put("user_meta","{\"source\": \"CMS\", \"user\":\"cms-tester\"}");
        String[] payloadparams = new String[]{restaurant_id, open_time, close_time, day};

        GameOfThronesService service = new GameOfThronesService("cmsavailability", "updaterestaurantlot", gameofthrones);
        return new Processor(service, headers, payloadparams);
    }

    public Processor getRestaurantTimeSlot(String restaurantId) {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers = getDefaultHeaders();
        String[] urlparams = new String[1];
        urlparams[0] = restaurantId;
        GameOfThronesService service = new GameOfThronesService("cmsavailability", "getrestaurantslot", gameofthrones);
        Processor response = new Processor(service, headers, null, urlparams);
        return response;
    }

    /**
     * Added By Shaswat
     * Start ************************************************************************************************
     */

    public List<Map<String, Object>> getExclusiveRestaurant(int isExclusive) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        return sqlTemplate.queryForList(CmsConstants.getExclusiveRestaurants1 + isExclusive + CmsConstants.getExclusiveRestaurants2);
    }

    public List<Map<String, Object>> getNewArrivalRestaurant() {

        SANDHelper sandHelper = new SANDHelper();
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        return sqlTemplate.queryForList(CmsConstants.getNewArrivalRestaurants1 + sandHelper.getDateForNewArrivalRestaurant() + CmsConstants.getNewArrivalRestaurants2);
    }

    public List<Map<String, Object>> getNonNewArrivalRestaurant() {

        SANDHelper sandHelper = new SANDHelper();
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        return sqlTemplate.queryForList(CmsConstants.getNonNewArrivalRestaurants1 + sandHelper.getDateForNewArrivalRestaurant() + CmsConstants.getNonNewArrivalRestaurants2);
    }

    public String getLatLngByRestID(String restId) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        return String.valueOf(sqlTemplate.queryForList(CmsConstants.getLatLongByRest + restId).get(0).get("lat_long"));
    }

    public List<Map<String, Object>> getCatAndSubCat() {

        SANDHelper sandHelper = new SANDHelper();
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        return sqlTemplate.queryForList(CmsConstants.getcatandsubcat);
    }

    public List<Map<String, Object>> getRestListTD() {

        SANDHelper sandHelper = new SANDHelper();
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        return sqlTemplate.queryForList(CmsConstants.getrestlist);
    }

    public List<Map<String, Object>> getRestaurantItemDetails(String restId) {

        SANDHelper sandHelper = new SANDHelper();
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        return sqlTemplate.queryForList(CmsConstants.getitemdetails + restId + CmsConstants.getItemsEnd);
    }

    /**
     * Shaswat Ends #########################################################################################
     */

    public Processor deleteRestaurantTimeSlot(String restaurantId) {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers = getDefaultHeaders();
        String[] urlparams = new String[1];
        urlparams[0] = restaurantId;
        GameOfThronesService service = new GameOfThronesService("cmsavailability", "restaurantholidayslot", gameofthrones);
        Processor response = new Processor(service, headers, null, urlparams);
        return response;
    }

    public Processor PutRestaurantTimeSlot(String restaurantId) {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers = getDefaultHeaders();
        String[] urlparams = new String[1];
        urlparams[0] = restaurantId;
        GameOfThronesService service = new GameOfThronesService("cmsavailability", "restaurantholidayslot", gameofthrones);
        Processor response = new Processor(service, headers, null, urlparams);
        return response;
    }

    public List<Map<String, Object>> getMenuMap(String restId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        return sqlTemplate.queryForList(CmsConstants.menumap_query + restId);
    }

    public List<Map<String, Object>> getMenuTaxonomy(String parentId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        return sqlTemplate.queryForList(CmsConstants.menutaxonomy_query + parentId);
    }

    public List<Map<String, Object>> getItemMenuMap(String menuId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        return sqlTemplate.queryForList(CmsConstants.item_menu_map + menuId);
    }

    public Processor createCategory(Category categories) {
        Processor processor = null;
        try {
            HashMap<String, String> headers = new HashMap<String, String>();
            JsonHelper jsonHelper = new JsonHelper();
            headers = getDefaultHeaders();
            GameOfThronesService service = new GameOfThronesService("cms_menu", "CMS_BULK_CATEGORY_UPLOAD", gameofthrones);
            processor = new Processor(service, headers, new String[]{jsonHelper.getObjectToJSON(categories)}, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return processor;
    }

    public Processor createCategoryForResturant(Category categories) {
        Processor processor = null;
        try {
            HashMap<String, String> headers = new HashMap<String, String>();
            JsonHelper jsonHelper = new JsonHelper();
            headers = getDefaultHeaders();
            GameOfThronesService service = new GameOfThronesService("cmsbaseservice", "CMS_CREATE_CATEGORY", gameofthrones);
            processor = new Processor(service, headers, new String[]{jsonHelper.getObjectToJSON(categories)}, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return processor;
    }

    public Processor createItem(AddItemToRestaurant addItemToRestaurant) {
        Processor processor = null;
        try {
            HashMap<String, String> headers = new HashMap<String, String>();
            JsonHelper jsonHelper = new JsonHelper();
            headers = getDefaultHeaders();
            GameOfThronesService service = new GameOfThronesService("cmsbaseservice", "CMS_CREATE_ITEM", gameofthrones);
            processor = new Processor(service, headers, new String[]{jsonHelper.getObjectToJSON(addItemToRestaurant)}, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return processor;
    }

    public int updateCuisine(String restId, String value) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        return sqlTemplate.update(CmsConstants.updatecuisine + value + CmsConstants.whereIdString + restId);
    }

    public List<Map<String, Object>> getCategoriesByStoreId(String store_id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.getCategory_storeId_start + store_id + CmsConstants.getCategory_storeId_end);
        return list;
    }

    public int getCategoryActive(String store_id, String external_id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        System.out.println(CmsConstants.getCategory_storeId_start + store_id + CmsConstants.getCategory_storeId_end + CmsConstants.getCatActive + external_id);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.getCategory_storeId_start + store_id + CmsConstants.getCategory_wo_active + CmsConstants.getCatActive + external_id + CmsConstants.endquotes);
        return (int) list.get(0).get("active");
    }

    public List<Map<String, Object>> getFullMenuItems(String store_id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.getfullmenu_items + store_id + CmsConstants.close_braces);
        return list;
    }

    public List<Map<String, Object>> getFullMenuSubcategory(String external_item_id, String store_id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.getSubcategory_fullmenu + external_item_id + CmsConstants.getSubCategory_middle + store_id + CmsConstants.getSubcategory_end);
        return list;
    }

    public List<Map<String, Object>> getFullMenuCategory(String external_subcategory_id, String store_id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.getCategory_fullmenu + external_subcategory_id + CmsConstants.getCategory_middle + store_id + CmsConstants.getCategory_end);
        return list;
    }

    public List<Map<String, Object>> getItemsCreated(String store_id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.getItems_fullmenu + store_id + CmsConstants.getItems_end);
        return list;
    }

    public String getTokenForRestaurant(String rest_id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.token_query + rest_id + CmsConstants.endquotes);
        return list.get(0).get(CmsConstants.api_token).toString();
    }


    public Processor createItem(String name) {
        Processor processor = null;
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("Authorization", "Basic dXNlcjpjaGVjaw==");
        JsonHelper jsonHelper = new JsonHelper();
        ItemsCat itm = new ItemsCat();
        itm.build();
        itm.setName(name);

        GameOfThronesService got = new GameOfThronesService("cmsbaseservice", "verifycatalog", gameofthrones);
        try {
            processor = new Processor(got, headers, new String[]{jsonHelper.getObjectToJSON(itm)}, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return processor;
    }

    public Processor updateItem(String comment, String description, String name, String itemId) {
        Processor processor = null;
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        JsonHelper jsonHelper = new JsonHelper();
        /*String[] urlparams = new String[1];
        urlparams[0] = itemId;*/
        String[] urlparams = {itemId};
        String[] payloadparams = new String[]{comment, description, name};

        GameOfThronesService got = new GameOfThronesService("cmsbaseservice", "verifycatalogfeedback", gameofthrones);
        try {
            processor = new Processor(got, headers, payloadparams, urlparams);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return processor;
    }

    public Processor dpAPICuisine(String itmName) {
        Processor processor = null;
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        String[] payload = new String[]{itmName};
        GameOfThronesService got = new GameOfThronesService("cmsdpapi", "dpapicuisine", gameofthrones);
        processor = new Processor(got, headers, payload, null);
        return processor;
    }

    public Processor dpAPICategory(String itmName) {
        Processor processor = null;
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        String[] payload = new String[]{itmName};
        GameOfThronesService got = new GameOfThronesService("cmsdpapi", "dpapicategory", gameofthrones);
        processor = new Processor(got, headers, payload, null);
        return processor;
    }

    public Processor dpAPIDishType(String itmName) {
        Processor processor = null;
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        String[] payload = new String[]{itmName};
        GameOfThronesService got = new GameOfThronesService("cmsdpapi", "dpapidishfamily", gameofthrones);
        processor = new Processor(got, headers, payload, null);
        return processor;
    }

    public Processor preOrderEnable(String restId, String attribute, String value) {
        Processor processor = null;
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        String[] payload = new String[]{restId, attribute, value};
        GameOfThronesService got = new GameOfThronesService("cmsrestaurantservice", "preorderenable", gameofthrones);
        processor = new Processor(got, headers, payload, null);
        return processor;
    }


    public void preorderSlots(String rest_id, String day, String openTime, String closeTime) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String current_time = dateFormat.format(date);
        sqlTemplate.execute("delete from `restaurants_schedule` where `rest_id`=" + rest_id + "");
        sqlTemplate.execute("INSERT INTO `restaurants_schedule` (`rest_id`,`day`,`open_time`,`close_time`,type,`created_on`,`updated_on`) VALUES ('" + rest_id + "','" + day + "','" + openTime + "','" + closeTime + "','PREORDER'" + ",'" + current_time + "','" + current_time + "')");

    }


    public void longDistanceEnabled(String rest_id) {

    }


    public List<Map<String, Object>> getRegularItems(String rest_id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.regular_items + rest_id);
        return list;
    }

    public void setLiveDate(String rest_id, String time) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.execute(CmsConstants.live_date_query_date + time + CmsConstants.live_date_cond + rest_id + CmsConstants.endquotes);
    }


    public void setExclusiveRestaurant(String rest_id, int is_exclusive) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.set_exclusive + is_exclusive + CmsConstants.set_exclusive_end + rest_id);

    }

    public void createWpOptions(String option_name, String option_value, String autoload) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.execute(CmsConstants.insert_wp_options + option_name + "','" + option_value + "'," + "'" + autoload + CmsConstants.wp_options_end);
    }

    public boolean setWpOptions(String option_name, String option_value, String autoload) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        boolean success = (sqlTemplate.update(CmsConstants.update_wp_options + option_value + CmsConstants.update_wp_options2 + autoload + CmsConstants.update_wp_options3 + option_name + CmsConstants.update_wp_options_end) > 0) ? true : false;
        return success;
    }

    public void deleteWpOptions(String option_name) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.execute(CmsConstants.delete_wp_options + option_name + "'");
    }

    public int getCityId(String city_name) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = (sqlTemplate.queryForList(CmsConstants.getcity_id + city_name + CmsConstants.endquotes));
        return (list.size() > 0) ? (Integer) list.get(0).get("id") : 0;
    }

    public int getAreaId(String area_name) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = (sqlTemplate.queryForList(CmsConstants.getarea_id + area_name + CmsConstants.endquotes));
        return (list.size() > 0) ? (Integer) list.get(0).get("id") : 0;
    }

    public Map<String, Object> getCityDetails(String city_id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.city_details + city_id);
        return list.get(0);
    }

    public Map<String, Object> getAreaDetails(String area_id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.area_details + area_id);
        return list.get(0);
    }


    public Processor preOrderAreaScheduleCreate(String areaId, String menuType, String day, String slotType, String openTime, String closeTime) {
        Processor processor = null;
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.execute("delete  from `area_schedule` where `area_id`=" + areaId + " and type='PRE_ORDER' and day='" + day + "'");
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("user_meta", "{\"source\":\"VENDOR\"}");
        String[] payload = new String[]{areaId, menuType, day, slotType, openTime, closeTime};
        GameOfThronesService got = new GameOfThronesService("cmsrestaurantservice", "preorderareaschedulecreate", gameofthrones);
        processor = new Processor(got, headers, payload, null);
        return processor;
    }


    public Map<String, Object> getRestaurantDetails(String rest_id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        return sqlTemplate.queryForList(CmsConstants.rest_details + rest_id).get(0);
    }


    public List<Map<String, Object>> getPopMenuMap(String restId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        return sqlTemplate.queryForList(CmsConstants.popmap_query + restId);
    }

    public void insertCuisine(int rest_id, int cuisine_id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.insert_cuisine + rest_id + CmsConstants.comma + cuisine_id + CmsConstants.close_insert);
    }

    public void setLiveDate(int rest_id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.execute(CmsConstants.live_date_query + rest_id + CmsConstants.endquotes);
    }

    public void setLatLong(String lat_lng, int rest_id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.update_latlng + lat_lng + CmsConstants.whereIdString + rest_id);
    }

    public boolean isCityPresent(String city_name) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.city_present + city_name + CmsConstants.endquotes);
        return (list.size() > 0) ? true : false;

    }

    public boolean isAreaPresent(String area_name) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.area_present + area_name + CmsConstants.endquotes);
        return (list.size() > 0) ? true : false;
    }

    public boolean isAreaPresent(int area_id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.area_present + area_id + CmsConstants.endquotes);
        return (list.size() > 0) ? true : false;
    }

    public int getPolygonId(int city_id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = (sqlTemplate.queryForList(CmsConstants.polygon_id + city_id));
        return (list.size() > 0) ? (Integer) list.get(0).get("id") : 0;
    }

    public long getPolygonIdFromName(String polygon_name) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = (sqlTemplate.queryForList(CmsConstants.polygon_name + polygon_name));
//        return (list.size() > 0) ? (BigInteger) list.get(0).get("id") : 0;
        if (list.size() > 0) {
            BigInteger big = (BigInteger) list.get(0).get("id");
            return big.longValue();
        } else return 0;
    }

    public void partnerMapping(String rest_id, String partner_id, String type_of_partner, String otp) throws IOException, JSONException {
        insertPartnerMap(rest_id, partner_id, type_of_partner, otp);
        PartnerUpdate partnerUpdate = new PartnerUpdate();
        partnerUpdate.build();
        partnerUpdate.setPartner_id(partner_id);
        partnerUpdate.setType_of_partner(Integer.parseInt(type_of_partner));
        partnerUpdate.setOtp(otp);
    }

    public void rmsPartnerUpdate(String rest_id, String partner_id) throws IOException {
        File file = new File("../Data/Payloads/JSON/rms_items");
        String queue = FileUtils.readFileToString(file);
        queue = queue.replace("${action}", RestaurantConstants.update).replace("${partner_id}", partner_id).replace("${type}", RestaurantConstants.rest).replace("${rest_id}", rest_id);
        System.out.println(queue);
        rmqHelper.pushMessageToExchange("oms", "swiggy.items_to_rms", new AMQP.BasicProperties().builder().contentType("application/json"), queue);
    }

    public void insertPartnerMap(String rest_id, String partner_id, String type_of_partner, String otp) throws IOException, JSONException {
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String value = "(";
        value += "'" + rest_id + "',";
        value += "'" + partner_id + "',";
        value += "'" + type_of_partner + "',";
        value += "'" + otp + "',";
        value += "'" + RestaurantConstants.enabled + "',";
        value += "'" + dateFormat.format(date) + "',";
        value += "'" + dateFormat.format(date) + "',";
        value += "'" + RestaurantConstants.enabled + "',";
        value += "'" + dateFormat.format(date) + "',";
        value += "'" + "NULL" + "',";
        value += "'" + dateFormat.format(date) + "',";
        value += "'" + "NULL" + "',";
        value += "'" + " " + "',";
        String query = RestaurantConstants.partner_map_insert;
        query += value + ")";
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.execute(query);
        System.out.println(query);
        partnerMapUpdate(rest_id);
        rmsPartnerUpdate(rest_id, partner_id);
        updateRestaurantPartnerMap(rest_id, type_of_partner);
    }

    public Map<String, Object> getPartnerMapData(String rest_id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        return sqlTemplate.queryForList(CmsConstants.partner_map_data + rest_id).get(0);
    }

    public void partnerMapUpdate(String rest_id) throws IOException {
        Gson gson = new Gson();
        String json = gson.toJson(getPartnerMapData(rest_id));
        System.out.println(json);
        File file = new File("../Data/Payloads/JSON/partner_update");
        String queue = FileUtils.readFileToString(file);
        queue = queue.replace("${0}", RestaurantConstants.update).replace("${1}", json);
        System.out.println(queue);
        rmqHelper.pushMessageToExchange("oms", "swiggy.partner_map_update", new AMQP.BasicProperties().builder().contentType("application/json"), queue);
    }

    public void updateRestaurantPartnerMap(String rest_id, String type_of_partner) throws JSONException {
        CMSHelper cmsHelper = new CMSHelper();
        Gson gson = new Gson();
        String json = gson.toJson(cmsHelper.getRestaurantDetails(rest_id));
        JSONObject jsonObject = new JSONObject(json);
        jsonObject.put("type_of_partner", type_of_partner);
        //jsonObject.put("brand_id", null);
        jsonObject.put("swiggy_assured", jsonObject.get("is_assured"));
        jsonObject.put("swiggy_select", jsonObject.get("is_select"));
        jsonObject.put("long_distance_order_enabled", jsonObject.get("is_long_distance_enabled"));
        System.out.println(jsonObject);
    }

    public List<Map<String, Object>> getItemIds(String rest_id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.getItemsforRest + rest_id);
        return list;
    }

    public boolean isUserPresent(String mobile_no) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.check_user_exists + mobile_no);
        return (list.size() > 0) ? true : false;
    }

    public int getCategoryIdByName(String rest_id, String category_name) {
        List<Map<String, Object>> map = getCategories(rest_id);
        for (Map<String, Object> m : map) {
            if (m.get("cat_name").toString().equalsIgnoreCase(category_name)) {
                BigInteger cat_id = (BigInteger) m.get("cat_id");
                return cat_id.intValue();
            }
        }
        return 0;
    }

    public int getSubCategoryIdByName(String rest_id, String cat_id, String subcategory_name) {
        List<Map<String, Object>> map = getSubcategory(rest_id, cat_id);
        for (Map<String, Object> m : map) {
            if (m.get("subcat_name").toString().equalsIgnoreCase(subcategory_name)) {
                BigInteger subcat_id = (BigInteger) m.get("subcat_id");
                return subcat_id.intValue();
            }
        }
        return 0;
    }

    public List<Map<String, Object>> getItemsForRestaurant(String rest_id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.item_details + rest_id);
        return list;
    }

    public int getItemIdByName(String rest_id, String item_name) {
        List<Map<String, Object>> map = getItemsForRestaurant(rest_id);
        for (Map<String, Object> m : map) {
            if (m.get("name").toString().equalsIgnoreCase(item_name)) {
                BigInteger item_id = (BigInteger) m.get("id");
                return item_id.intValue();
            }
        }
        return 0;
    }


    /**
     * Added By Shaswat Start
     * ************************************************************************************************
     */


    /**
     * Shaswat Ends
     * #########################################################################################
     */

    public List<Map<String, Object>> areaSlotForPop(int AreaId, String day) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.AreaIdSchedule + AreaId
                + CmsConstants.AreaIdSchedule1 + day + CmsConstants.CategoryIdforPop1);
        System.out.println(CmsConstants.AreaIdSchedule + AreaId + CmsConstants.AreaIdSchedule1 + day
                + CmsConstants.CategoryIdforPop1);
        return list;
    }

    public List<Map<String, Object>> scheduledItem(int areaScheduleId, String cusine, String dishtype) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate
                .queryForList(CmsConstants.ItemSchedulePriority + areaScheduleId + CmsConstants.ItemSchedulePriority1
                        + cusine + CmsConstants.ItemSchedulePriority2 + dishtype + CmsConstants.CategoryIdforPop1);
        return list;
    }

    public List<Map<String, Object>> getMenuIdForPop(int RestaurantId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate
                .queryForList(CmsConstants.MenuIdforPop + RestaurantId + CmsConstants.MenuIdforPop1);
        return list;
    }

    public List<Map<String, Object>> getcatIdForPop(int menuId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate
                .queryForList(CmsConstants.CategoryIdforPop + menuId + CmsConstants.CategoryIdforPop1);
        return list;
    }

    public Map<String, Object> getAreaIdbasedOnItem(String ItemId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate
                .queryForList(CmsConstants.areaIdBasedOnItem + ItemId + CmsConstants.areaIdBasedOnItem1);
        return list.get(0);
    }

    public Processor createItemAttributes(String name) {
        Processor processor = null;
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("Authorization", "Basic dXNlcjpjaGVjaw==");
        JsonHelper jsonHelper = new JsonHelper();
        ItemAttribute itm = new ItemAttribute();
        itm.build();
        itm.setName(name);
        GameOfThronesService got = new GameOfThronesService("cmsbaseservice", "itemattributes", gameofthrones);
        try {
            processor = new Processor(got, headers, new String[]{jsonHelper.getObjectToJSON(itm)}, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return processor;
    }

	public List<Map<String, Object>> getItemHolidays(String categoryId, String fromTime) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
		List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.catInactiveitemHolidayslot + categoryId+ CmsConstants.catInactiveitemHolidayslot1 +fromTime+CmsConstants.catInactiveitemHolidayslot2 );
		return list;
	}

	public Map<String, Object> getItemMenuMap1(String categoryId) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
		List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.VerifyItemMenuMapTab + categoryId+ CmsConstants.VerifyItemMenuMapTab);
		return list.get(0);
	}
    
	public Map<String, Object> getItemHolidaysSlotCount(String categoryId, String fromTime) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
		List<Map<String, Object>> list = sqlTemplate.queryForList(CmsConstants.VerifyItemCountimHolidayslotTab + categoryId+ CmsConstants.VerifyItemCountimHolidayslotTab1+fromTime+CmsConstants.VerifyItemCountimHolidayslotTab2);
		return list.get(0);
	}

    public Processor availabilityRestaurant() {
        Processor processor = null;
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        JsonHelper jsonHelper = new JsonHelper();
        GameOfThronesService got = new GameOfThronesService("availabilityapi", "availabilityrestaurant", gameofthrones);
        processor = new Processor(got, headers, null, null);
        return processor;
    }

    public Processor availabilityMenu() {
        Processor processor = null;
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        JsonHelper jsonHelper = new JsonHelper();
        GameOfThronesService got = new GameOfThronesService("availabilityapi", "availabilitymenu", gameofthrones);
        processor = new Processor(got, headers, null, null);
        return processor;
    }

    public Processor availabilityRegularItem() {
        Processor processor = null;
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        JsonHelper jsonHelper = new JsonHelper();
        GameOfThronesService got = new GameOfThronesService("availabilityapi", "availabilityregularitem", gameofthrones);
        processor = new Processor(got, headers, null, null);
        return processor;
    }

    public Processor availabilityPopItem() {
        Processor processor = null;
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        JsonHelper jsonHelper = new JsonHelper();
        GameOfThronesService got = new GameOfThronesService("availabilityapi", "availabilitypopitem", gameofthrones);
        processor = new Processor(got, headers, null, null);
        return processor;
    }

    public String getSubCategoryIdForItem(String itemId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        return String.valueOf(sqlTemplate.queryForList(CmsConstants.getSubcategoryId0 + itemId).get(0).get("menu_id"));
    }

    public String getCategoryIdForItem(String itemId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        return String.valueOf(sqlTemplate.queryForList(CmsConstants.getCategoryId0 + itemId + CmsConstants.getCategoryId1).get(0).get("parent"));
    }


    public Processor variantInStock(String varid, String vargid) {
        Processor processor = null;
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("Authorization", "Basic dmVuZG9yLXN5c3RlbTpzZWxmc2VydmUtc3lzdGVt");
        headers.put("user-meta", "{\"source\":\"VENDOR\", \"meta\":{\"user\":\"test\"}}");
        GameOfThronesService got = new GameOfThronesService("cmsbaseservice", "variantinstock", gameofthrones);
        String[] queryParams = {varid, vargid};
        processor = new Processor(got, headers, null, queryParams);
        return processor;
    }

    public Processor variantOutOfStock(String varid, String vargid) {
        Processor processor = null;
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("Authorization", "Basic dmVuZG9yLXN5c3RlbTpzZWxmc2VydmUtc3lzdGVt");
        headers.put("user-meta", "{\"source\":\"VENDOR\", \"meta\":{\"user\":\"test\"}}");
        GameOfThronesService got = new GameOfThronesService("cmsbaseservice", "variantoutofstock", gameofthrones);
        String[] queryParams = {varid, vargid};
        processor = new Processor(got, headers, null, queryParams);
        return processor;

    }
        public Processor smartSuggestion (String itemId){
            Processor processor = null;
            HashMap<String, String> headers = new HashMap<>();
            headers.put("Content-Type", "application/json");
            SqlTemplate sql = SystemConfigProvider.getTemplate("cms");
            String[] urlparams = new String[1];
            urlparams[0] = itemId;
            List<Map<String, Object>> hm = sql.queryForList("select item_id from item_suggestion where item_id=" + itemId);
            System.out.println(hm.size());

            if (hm.size() == 0) {
                sql.execute(CmsConstants.ExecuteQuery(itemId));
                GameOfThronesService got = new GameOfThronesService("cmsmenuservice", "menubuildcache", gameofthrones);

                processor = new Processor(got, headers, null, urlparams);

            }
            return processor;

        }

    public String getActiveItem(){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        return String.valueOf(sqlTemplate.queryForList(CmsConstants.activeItemId).get(0).get("id"));
    }
    
    public Object dbhelperget(String query, String entity) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
		List<Map<String, Object>> list = sqlTemplate.queryForList(query);
		if (list == null || list.isEmpty()) {
			return null;
		} else {
			return list.get(0).get(entity);
		}
	}


    public String getSmartItem(){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        return String.valueOf(sqlTemplate.queryForList(CmsConstants.suggItem).get(0).get("item_id"));
    }

    public Processor restaurantSlotsCreate(String day, String restId, String openTime, String closeTime) {
        Processor processor = null;
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.execute("delete from `restaurant_slots` where `rest_id`="+restId+" and `day`='"+day+"'");
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("user_meta", "{\"source\":\"CMS Dashboard\",\"meta\":{\"user\":\"cms@swiggy.in\"}}");
        String[] payload = new String[]{day, restId,openTime,closeTime};
        GameOfThronesService got = new GameOfThronesService("cmsrestaurantservice", "restslotscreate", gameofthrones);
        processor = new Processor(got, headers, payload, null);
        return processor;
    }
    public Processor restaurantSlotsCreate1(String day, String restId, String openTime, String closeTime) {
        Processor processor = null;
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.execute("delete from `restaurant_slots` where `rest_id` in "+"("+restId+")" + "and `day`in "+"('"+day+"')");
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("user_meta", "{\"source\":\"CMS Dashboard\",\"meta\":{\"user\":\"cms@swiggy.in\"}}");
        String[] payload = new String[]{day, restId,openTime,closeTime};
        GameOfThronesService got = new GameOfThronesService("cmsrestaurantservice", "restslotscreate", gameofthrones);
        processor = new Processor(got, headers, payload, null);
        return processor;
    }


    public Processor restaurantSlotsDelete(String slotId) {
        Processor processor = null;
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("user_meta", "{\"source\":\"CMS Dashboard\",\"meta\":{\"user\":\"cms@swiggy.in\"}}");
        String[] urlParams = new String[]{slotId};
        GameOfThronesService got = new GameOfThronesService("cmsrestaurantservice", "restslotsdelete", gameofthrones);
        processor = new Processor(got, headers,null, urlParams);
        return processor;
    }

    
	public static String getAuthUser() {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
	    return String.valueOf(sqlTemplate.queryForList(CmsConstants.authUserId).get(0).get("id"));
	}

	public static String authUerIdforEnableDisable() {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
	    return String.valueOf(sqlTemplate.queryForList(CmsConstants.authUserId).get(0).get("id"));
	}


    public String getEnableAndActiveItemID(String restaurant_id,int stock) throws IOException
    {
    	String item_id;
    	String query=CmsConstants.getItem_id_query+restaurant_id+" limit 1";
    	Object item=dbhelperget(query,"id");
    	if(item==null)
    	{
    		RestBuilder restBuilder = new RestBuilder();
    		AddItemToRestaurant additem=restBuilder.buildItemSlot();
    		additem.setInStock(stock);
    		String response=createItem(additem).ResponseValidator.GetBodyAsText();
    		item_id = JsonPath.read(response, "$.data.uniqueId").toString();
    	}
    	else
    	{
    		item_id=item.toString();
    	}
		return item_id;
    	
    }
    
    public String getcurrentdaytime()
    {
    	String pattern = "yyyy-MM-dd HH:mm:ss";
    	SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
    	String date = simpleDateFormat.format(new Date());
    	return date;
    }
    public String getcurrentDayTimeMinusMins(int timetominus)
    {
      	long date=System.currentTimeMillis()+timetominus*60*1000;
    	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    	String timeminus=dateFormat.format(date);
		return timeminus;
    }
    public String nextDayDateTime()
    {
    	Calendar calendar = Calendar.getInstance();
      	calendar.add(Calendar.DAY_OF_YEAR, 1);
    	Date tomorrow = calendar.getTime();
    	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    	String next_Day = dateFormat.format(tomorrow);
    	return next_Day;
    }

    public Processor buildItemCache(String itemId) {
        Processor processor = null;
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        String[] urlparams = new String[1];
        urlparams[0] = itemId;
        GameOfThronesService got = new GameOfThronesService("cmsmenuservice", "menubuildcache", gameofthrones);
        processor = new Processor(got, headers, null, urlparams);
        return processor;
    }

}

