package com.swiggy.api.erp.delivery.helper;


public interface Command {
    void runCommand();
}