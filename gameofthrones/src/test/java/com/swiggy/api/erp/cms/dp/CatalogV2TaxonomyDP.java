package com.swiggy.api.erp.cms.dp;

import com.swiggy.api.erp.cms.helper.CatalogV2Helper;
import com.swiggy.api.erp.cms.pojo.Taxonomy.Taxonomy;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.Reporter;
import org.testng.annotations.DataProvider;

import java.time.Instant;

public class CatalogV2TaxonomyDP {
    CatalogV2Helper helper=new CatalogV2Helper();
    Taxonomy tax=new Taxonomy();
    JsonHelper help=new JsonHelper();

    @DataProvider(name = "createtaxonomydp")
    public Object[][] taxonomy() throws Exception{
        String payload = help.getObjectToJSON(tax);
        Reporter.log(payload,true);
        System.out.println(payload);
        return new Object[][]{{payload,"100"}};
    }

    @DataProvider(name = "updatetaxonomydp")
    public Object[][] uploadtaxonomy() throws Exception{
        tax.setPriority(5l);
        String payload = help.getObjectToJSON(tax);
        Reporter.log(payload,true);
        System.out.println(payload);
        return new Object[][]{{payload}};
    }
}
