package com.swiggy.api.erp.cms.pojo.AddonGroup;

import com.swiggy.api.erp.cms.constants.MenuConstants;

/**
 * Created by kiran.j on 2/21/18.
 */
public class Entity
{
    private String id;

    private int order;

    private int addon_limit;

    private String item_id;

    private String name;

    private int addon_free_limit;

    private int addon_min_limit;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public int getOrder ()
    {
        return order;
    }

    public void setOrder (int order)
    {
        this.order = order;
    }

    public int getAddon_limit ()
    {
        return addon_limit;
    }

    public void setAddon_limit (int addon_limit)
    {
        this.addon_limit = addon_limit;
    }

    public String getItem_id ()
    {
        return item_id;
    }

    public void setItem_id (String item_id)
    {
        this.item_id = item_id;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public int getAddon_free_limit ()
    {
        return addon_free_limit;
    }

    public void setAddon_free_limit (int addon_free_limit)
    {
        this.addon_free_limit = addon_free_limit;
    }

    public int getAddon_min_limit ()
    {
        return addon_min_limit;
    }

    public void setAddon_min_limit (int addon_min_limit)
    {
        this.addon_min_limit = addon_min_limit;
    }

    public Entity build() {
        if(this.getId() == null)
            this.setId(MenuConstants.addons_groups_id);
        if(this.getName() == null)
            this.setName(MenuConstants.addons_groups_name);
        if(this.getItem_id() == null)
            this.setItem_id(MenuConstants.items_id);

        this.setAddon_free_limit(MenuConstants.addon_groups_limit);
        this.setAddon_limit(MenuConstants.addon_groups_limit);

        return this;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", order = "+order+", addon_limit = "+addon_limit+", item_id = "+item_id+", name = "+name+", addon_free_limit = "+addon_free_limit+", addon_min_limit = "+addon_min_limit+"]";
    }
}
