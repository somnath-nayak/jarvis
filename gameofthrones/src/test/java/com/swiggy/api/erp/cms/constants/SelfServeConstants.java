package com.swiggy.api.erp.cms.constants;

import com.swiggy.api.erp.cms.helper.SelfServeHelper;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.erp.cms.constants
 **/
public interface SelfServeConstants {

	String message_success = "success";
	int statusOne = 1;
	int statusZero = 0;
	String test_auth_key = "test";
	String message_null = "null";
	String msg_agentDisabled = "Agent disabled!";
	String msg_agentAlreadyAvailable = "Agent already available";
	String agent_authorization_key = "Basic YWdlbnQtZGFzaGJvYXJkOnNlbGZzZXJ2ZS1zeXN0ZW0=";
	String vendor_authorization_key = "Basic dmVuZG9yLXN5c3RlbTpzZWxmc2VydmUtc3lzdGVt";
	String app_secret = "02fse!@02fe!";
	String get_active_agent_id = "SELECT * FROM swiggy.agents WHERE active=1 LIMIT 1";
	String get_auth_user_id_of_agent = "SELECT * FROM swiggy.agents WHERE active=1 AND id=";
	String get_agent_authKey = "SELECT * FROM swiggy.agents WHERE active=1 and id=";
	String get_created_ticket = "SELECT * FROM swiggy.tickets WHERE parent_resc_id=${PARENT_RESC_ID} AND state IN ('NEW') AND subject='${NAME}'";
	String get_submitted_ticket = "SELECT * FROM swiggy.tickets WHERE parent_resc_id=${PARENT_RESC_ID} AND state IN ('SUBMITTED') AND subject='${NAME}'";
	String get_assigned_ticket = "SELECT * FROM swiggy.tickets WHERE parent_resc_id=${PARENT_RESC_ID} AND state IN ('ASSIGNED') AND subject='${NAME}'";
	String get_all_available_agents = "SELECT * FROM swiggy.agents WHERE is_available=1";
	String get_all_active_agents = "SELECT * FROM swiggy.agents WHERE active=1";
	String get_all_active_enabled_available_agents = "SELECT * FROM swiggy.agents WHERE is_available=1 AND is_enabled=1 AND active=1";
	String get_ticket_group_id = "SELECT * FROM swiggy.tickets WHERE parent_resc_id=${PARENT_RESC_ID} AND subject=\"${NAME}\"";
	String test_auth_key1 = "select a.auth_key from agents a inner join tickets t where t.assigned_to=a.id and t.id='${ticketId}'";
	String agentType="SELFSERVE";
	String authUserId=SelfServeHelper.getAuthUser();
	String authUerIdforEnableDisable=SelfServeHelper.authUerIdforEnableDisable();
	public static String authUserIdForEnableDisable="select auth_user_id from agents where auth_user_id in(select id from auth_user where is_active=1) and is_enabled=1";
	public static String authKeyEnableDisable="select auth_key from agents where auth_user_id='${authUserId}'";
	public static String catTicketNoFuond="no ticket found for id:";
	public static String typeMissmatch="This ticket is of different type ";
	public static String catUpdateName="Automation CatName"+System.currentTimeMillis();
	public static String notaSubCat="nota";
	public static String subCatUpdateName="Automation SubCatName"+System.currentTimeMillis();
	String[] restId= {"9990","223"}; 
	String[] type= {"Cat","SubCat"};
	String catNotFound="Category Not found";
	String[] creatdOrUpdatedBy= {"HemaAutomation"};
	int[] isVeg= {0,1,2};
	int[] instock= {0,1};
	String catId=SelfServeHelper.getCatOrSubCatIdHelper(restId[0],"Cat");
	String subCatId=SelfServeHelper.getCatOrSubCatIdHelper(catId,"SubCat");
	float price=100;
	String itemName="itemNameSelfServe"+System.currentTimeMillis();
	int open_time = 0000;
	int close_time = 1200;
	String[] day_of_week = {"MON","TUE","WED","THU","FRI","SAT","SUN"};
	int[] invalid_day_week = {-5, 0, 10};
    String items_image_url = "https://swiggy-rest-item-image-upload.s3.amazonaws.com/item_image/011775da-6b10-11e8-a1fe-06e944becd6dFriday_08_June_2018__17_05id9179213.jpg";
	String phoneno="1234567890" ;
	String authUsername = "hemamalini.g@swiggy.in";

	String[] errormessage={"Category type is incorrect."};

}
