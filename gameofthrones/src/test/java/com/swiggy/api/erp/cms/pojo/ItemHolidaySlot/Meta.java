package com.swiggy.api.erp.cms.pojo.ItemHolidaySlot;

import org.apache.commons.lang.builder.ToStringBuilder;

public class Meta {

    private String user;

    /**
     * No args constructor for use in serialization
     *
     */
    public Meta() {
    }

    /**
     *
     * @param user
     */
    public Meta(String user) {
        super();
        this.user = user;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("user", user).toString();
    }

}
