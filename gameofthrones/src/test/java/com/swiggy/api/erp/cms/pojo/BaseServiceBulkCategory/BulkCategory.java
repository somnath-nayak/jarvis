package com.swiggy.api.erp.cms.pojo.BaseServiceBulkCategory;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.erp.cms.pojo.BaseServiceBulkCategory
 **/

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "categories",
        "delete_by_ids",
        "delete_by_third_party_ids"
})
public class BulkCategory {

    @JsonProperty("categories")
    private List<Category> categories = null;
    @JsonProperty("delete_by_ids")
    private List<Integer> delete_by_ids = null;
    @JsonProperty("delete_by_third_party_ids")
    private List<String> delete_by_third_party_ids = null;

    @JsonProperty("categories")
    public List<Category> getCategories() {
        return categories;
    }

    @JsonProperty("categories")
    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    @JsonProperty("delete_by_ids")
    public List<Integer> getDelete_by_ids() {
        return delete_by_ids;
    }

    @JsonProperty("delete_by_ids")
    public void setDelete_by_ids(List<Integer> delete_by_ids) {
        this.delete_by_ids = delete_by_ids;
    }

    @JsonProperty("delete_by_third_party_ids")
    public List<String> getDelete_by_third_party_ids() {
        return delete_by_third_party_ids;
    }

    @JsonProperty("delete_by_third_party_ids")
    public void setDelete_by_third_party_ids(List<String> delete_by_third_party_ids) {
        this.delete_by_third_party_ids = delete_by_third_party_ids;
    }

    public void setDefaultValues() {
        if (this.getCategories() == null)
            this.setCategories(new ArrayList<>());
        if (this.getDelete_by_ids() == null)
            this.setDelete_by_ids(new ArrayList<>());
        if (this.getDelete_by_third_party_ids() == null)
            this.setDelete_by_third_party_ids(new ArrayList<>());
    }

    public BulkCategory build() {
        setDefaultValues();
        return this;
        }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("categories", categories).append("delete_by_ids", delete_by_ids).append("delete_by_third_party_ids", delete_by_third_party_ids).toString();
    }

}
