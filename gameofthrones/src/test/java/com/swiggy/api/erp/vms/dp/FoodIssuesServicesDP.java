package com.swiggy.api.erp.vms.dp;

import java.util.ArrayList;
import java.util.HashMap;

import org.testng.annotations.DataProvider;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.ff.helper.OMSHelper;
import com.swiggy.api.erp.vms.constants.RMSConstants;
import com.swiggy.api.erp.vms.helper.RMSCommonHelper;
import com.swiggy.api.erp.vms.helper.RMSHelper;

import framework.gameofthrones.JonSnow.Processor;

/**
  Created by Imran Khan.
 */

public class FoodIssuesServicesDP {
	
	
	

	@DataProvider(name = "foodIssuesGetConfig_DP")
	public static Object[][] foodIssuesGetConfig_DP() {
		
		ArrayList<String> expectedFlowConfigList=new ArrayList<>();
		expectedFlowConfigList.add("NEW_BLOCKER");
		expectedFlowConfigList.add("NEW_NON_BLOCKER");
		
		return new Object[][] { { RMSConstants.FIGC_restaurantId, RMSConstants.FFIGC_statusMessage,expectedFlowConfigList }};
		
	}
	
	@DataProvider(name = "foodIssuesGetConfig_DP1")
	public static Object[][] foodIssuesGetConfig_DP1() {

		return new Object[][] 
			{
				{ "0", 1,"failed to fetch config" },
				{ "-0", 1,"failed to fetch config" },
				{ "8997687996576786879", 1,"failed to fetch config"},
				{ "", 1,"invalid input params"},
			};
		
	}
	
	@DataProvider(name = "foodIssuesVerifyBill_DP")
	public static Object[][] foodIssuesVerifyBill_DP() {
		
		return new Object[][] 
			{ 
		
				{  RMSConstants.FIVB_inputType,RMSConstants.FIVB_billProvided , RMSConstants.FIVB_statusMessage },
				{ RMSConstants.FIVB_inputType,"False" , RMSConstants.FIVB_statusMessage },
				
			};
	}
	@DataProvider(name = "foodIssuesVerifyBill_DP1")
	public static Object[][] foodIssuesVerifyBill_DP1() throws Exception {
//		RMSHelper helper=new RMSHelper();
//		String[] response=RMSCommonHelper.createOrder(RMSConstants.restaurantId, RMSConstants.consumerAppMobile, RMSConstants.consumerAppPassword, RMSConstants.PAYMENTMODE_CASH, RMSConstants.TEST_ORDER_COMMENTS);
//		
//		Thread.sleep(10000);
//		OMSHelper omsHellper = new OMSHelper();
//		String orderInfo = omsHellper.getOrderInfoFromOMS(response[0]);
//		System.out.println(orderInfo);
//	    String updatedSource= JsonPath.read(orderInfo, "$.order_restaurant_bill").toString();
		
		return new Object[][]
			{ 	
				//{  response[0],updatedSource,"BILL","true" ,1, "invalid input params"},
			
			//	{ "8997687996576786879",updatedSource,"BILL","true" ,1, "invalid input params"},
			//	{ response[0],"8997687996576786879","BILL","true" ,1, "invalid input params"},			
				{ "8997687996576786879", "8997687996576786879", "BILL","true" ,1, "invalid input params"},
				{ "-1", "8997687996576786879", "BILL","true" ,1, "invalid input params"},
				{ "0", "8997687996576786879", "BILL","true" ,1, "invalid input params"},
			
				
				{ "8997687996576786879", "-1", "BILL","true" ,1, "invalid input params"},
				{ "-1", "8997687996576786879", "BILL","true" ,1, "invalid input params"},
				{ "0", "0", "BILL","true" ,1, "invalid input params"},
				{ "-1", "-1", "BILL","true" ,1, "invalid input params"},
				
				{ "8997687996576786879", "0.87675766", "BILL","true" ,1, "invalid input params"},
				{ "-1", "0.87675766", "BILL","true" ,1, "invalid input params"},
				{ "0", "0.87675766", "BILL","true" ,1, "invalid input params"},
				
				{ "8997687996576786879", "8997687996576786879", "BILL","false" ,1, "invalid input params"},
				{ "-1", "8997687996576786879", "BILL","false" ,1, "invalid input params"},
				{ "0", "8997687996576786879", "BILL","false" ,1, "invalid input params"},
	
				
				
				{ "8997687996576786879", "-1", "BILL","false" ,1, "invalid input params"},
				{ "-1", "8997687996576786879", "BILL","false" ,1, "invalid input params"},
				{ "0", "0", "BILL","false" ,1, "invalid input params"},
				{ "-1", "-1", "BILL","false" ,1, "invalid input params"},
				
				{ "8997687996576786879", "0.87675766", "BILL","false" ,1, "invalid input params"},
				{ "-1", "0.87675766", "BILL","false" ,1, "invalid input params"},
				{ "0", "0.87675766", "BILL","false" ,1, "invalid input params"},
				{ "0", "0", "BILL","false" ,1, "invalid input params"},
				{ "0", "0", "BILL","true" ,1, "invalid input params"},
				
			
			};

	}
	
	@DataProvider(name = "foodIssuesSaveFinalBill_DP")
	public static Object[][] foodIssuesSaveFinalBill_DP() {
		return new Object[][] {
				{ RMSConstants.FISFB_issueId, RMSConstants.FISFB_orderId, RMSConstants.FISFB_finalBillValue ,
				 RMSConstants.FISFB_statusMessage } };
	}
	
	@DataProvider(name = "foodIssuesSaveFinalBill_DP1")
	public static Object[][] foodIssuesSaveFinalBill_DP1() {
		
		return new Object[][]
				{
					{ "0", "-1", "33323" , 1,"invalid input params" },
					{ "-1", "0", "33323" , 1,"invalid input params" },
					{ "8997687996576786879", "-1", "33323" , 1,"invalid input params" },
					{ "8997687996576786879", "0", "33323" , 1,"invalid input params" },
					
					{ "0", "8997687996576786879", "33323" , 1,"invalid input params" },
					{ "-1", "8997687996576786879", "33323" , 1,"invalid input params" },
					{ RMSConstants.FISFB_issueId, "-1", "33323" , 1,"invalid input params" },
					{ RMSConstants.FISFB_issueId, "0", "33323" , 1,"invalid input params" },
					
					{ RMSConstants.FISFB_issueId, "8997687996576786879", "33323" , 1,"invalid input params" },
					{ "-1", RMSConstants.FISFB_orderId, "33323" , 1,"invalid input params" },
					{ "0", RMSConstants.FISFB_orderId, "33323" , 1,"invalid input params" },
					{"8997687996576786879" , RMSConstants.FISFB_orderId, "33323" , 1,"invalid input params" },	
					
					{ "", "-1", "33323" , 1,"invalid input params" },
					{ "", "0", "33323" , 1,"invalid input params" },
					{ "", "8997687996576786879", "33323" , 1,"invalid input params" },
					{ RMSConstants.FISFB_issueId, "0", "33323" , 1,"invalid input params" },
					
					{ RMSConstants.FISFB_issueId, "-1", "33323" , 1,"invalid input params" },
					{ RMSConstants.FISFB_issueId, "8997687996576786879", "33323" , 1,"invalid input params" },
					{ "0", "0", "33323" , 1,"invalid input params" },
					{ "-1", "-1", "33323" , 1,"invalid input params" },
					{ "8997687996576786879", "8997687996576786879", "33323" , 1,"invalid input params" },					
				};
	}

	@DataProvider(name = "foodIssuesRectifybill_DP")
	public static Object[][] foodIssuesRectifybill_DP() {
		
		ArrayList<String> expectedresolutionList=new ArrayList<>();
		expectedresolutionList.add("ITEM_ADDED");
		expectedresolutionList.add("WRONG_BILL");
		expectedresolutionList.add("DE_LEFT");
		expectedresolutionList.add("BOXES_CORRECTED");
		expectedresolutionList.add("UNRESOLVED");
		expectedresolutionList.add("ITEM_MISSING");
		expectedresolutionList.add("DIFFERENT_PARCEL");
		
		return new Object[][] { { RMSConstants.FIRB_issueId, RMSConstants.FIRB_resolution,RMSConstants.FIRB_FI_statusMessage,expectedresolutionList ,} };
	}
	
	@DataProvider(name = "foodIssuesRectifybill_DP1")
	public static Object[][] foodIssuesRectifybill_DP1() {

		return new Object[][] 
			{ 
				{ "0", "WRONG_BILL",1,"invalid input params" },
				{ "-1", "WRONG_BILL",1,"invalid input params" },
				{ "8997687996576786879", "WRONG_BILL",1,"invalid input params" },
				
				{ "0", "UNRESOLVED",1,"invalid input params" },
				{ "-1", "UNRESOLVED",1,"invalid input params" },
				{ "8997687996576786879", "UNRESOLVED",1,"invalid input params" },
				
				{ "0", "BOXES_CORRECTED",1,"invalid input params" },
				{ "-1", "BOXES_CORRECTED",1,"invalid input params" },
				{ "8997687996576786879", "BOXES_CORRECTED",1,"invalid input params" },
				
				{ "0", "DE_LEFT",1,"invalid input params" },
				{ "-1", "DE_LEFT",1,"invalid input params" },
				{ "8997687996576786879", "DE_LEFT",1,"invalid input params" },
				
				{ "0", "ITEM_MISSING",1,"invalid input params" },
				{ "-1", "ITEM_MISSING",1,"invalid input params" },
				{ "8997687996576786879", "ITEM_MISSING",1,"invalid input params" },
				
				{ "0", "DIFFERENT_PARCEL",1,"invalid input params" },
				{ "-1", "DIFFERENT_PARCEL",1,"invalid input params" },
				{ "8997687996576786879", "DIFFERENT_PARCEL",1,"invalid input params" },
				
				{ "0", "BILL_GIVEN",1,"invalid input params" },
				{ "-1", "BILL_GIVEN",1,"invalid input params" },
				{ "8997687996576786879", "BILL_GIVEN",1,"invalid input params" },
				{ "", "BILL_GIVEN",1,"invalid input params" },
			};	
	}

}
