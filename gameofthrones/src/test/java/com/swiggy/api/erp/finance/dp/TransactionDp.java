package com.swiggy.api.erp.finance.dp;

import com.swiggy.api.erp.ff.helper.LOSHelper;
import com.swiggy.api.erp.finance.constants.FinanceConstants;
import com.swiggy.api.erp.finance.constants.TransactionConstants;
import com.swiggy.api.erp.finance.helper.*;
import framework.gameofthrones.JonSnow.Processor;
import org.apache.commons.io.FileUtils;
import org.testng.annotations.DataProvider;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;

/**
 * Created by kiran.j on 7/12/18.
 */
public class TransactionDp {

    public static HashMap<String, String> map = new HashMap<>();
    CashMgmtHelper cashHelper = new CashMgmtHelper();
    TransactionsHelper helper = new TransactionsHelper();
    ZoneHelper zoneHelper = new ZoneHelper();
    DEHelper deHelper = new DEHelper();
    LOSHelper losHelper=new LOSHelper();

    public HashMap<String, String> getMap() {
        return map;
    }

    public void setMap(HashMap<String, String> map) {
        TransactionDp.map = map;
    }

    public String getOrderForPocketing() throws IOException {
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat order_idFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        String order_id = order_idFormat.format(date);
        String order_time = dateFormat.format(date);
        String epochTime = String.valueOf(Instant.now().getEpochSecond());
        losHelper.createManualOrder(order_id,order_time,epochTime);
        return order_id;
    }

    @DataProvider(name = "getTransaction")
    public Iterator<Object[]> getTransaction() throws IOException {
        createData();
        String de_id = getMap().get(TransactionConstants.de_id);
        cashHelper.closeSessionHelper(de_id);
        cashHelper.createSessionHelper(de_id);
        String order_id=getOrderForPocketing();
        helper.addPocketingAndPayoutTxn(de_id, TransactionConstants.pocketing_desc, order_id, TransactionConstants.pocketing_de_incoming, TransactionConstants.pocketing_de_outgoing, TransactionConstants.pocketing_txn);
        Processor processor = cashHelper.getSessions(de_id);
        String curr_session_id = cashHelper.getCurrentSessionId(processor);
        Processor txn_processor = cashHelper.getSessionDetails(de_id, curr_session_id);
        String txn_id = cashHelper.getTransactions(txn_processor);
        List<Object[]> data = new ArrayList<>();
        data.add(new Object[]{TransactionConstants.invalid_de_id, TransactionConstants.invalid_session_id, TransactionConstants.invalid_transaction_id, TransactionConstants.invalid_de_responses});
        data.add(new Object[]{de_id, TransactionConstants.invalid_session_id, TransactionConstants.invalid_transaction_id, TransactionConstants.invalid_de_session});
        data.add(new Object[]{de_id, curr_session_id, TransactionConstants.invalid_transaction_id, TransactionConstants.invalid_transaction});
        data.add(new Object[]{de_id, curr_session_id, txn_id, new String[]{}});

        return data.iterator();
    }

    public void createData() {
        if(getMap().size() == 0) {
            finHelper finHelper = new finHelper();
            setMap(finHelper.createZoneAndDE());
        }
    }

    @DataProvider(name = "submitRevertTxn")
    public Iterator<Object[]> submitRevertTxn() throws IOException {
        createData();
        List<Object[]> data = new ArrayList<>();
        String de_id = getMap().get(TransactionConstants.de_id);
        cashHelper.closeSessionHelper(de_id);
        cashHelper.createSessionHelper(de_id);
        String order_id=getOrderForPocketing();
        helper.addPocketingAndPayoutTxn(de_id, TransactionConstants.pocketing_desc, order_id, TransactionConstants.pocketing_de_incoming, TransactionConstants.pocketing_de_outgoing, TransactionConstants.pocketing_txn);
        String txn_id = helper.getTransactions(order_id).get(0).get("id").toString();
        data.add(new Object[]{TransactionConstants.invalid_transaction_id, TransactionConstants.reason, TransactionConstants.user_id, TransactionConstants.email,TransactionConstants.error_code, false});
        data.add(new Object[]{txn_id, TransactionConstants.reason, TransactionConstants.user_id, TransactionConstants.email, TransactionConstants.success_code, true});
        return data.iterator();
    }

    @DataProvider(name = "acceptRevertTxn")
    public Iterator<Object[]> acceptRevertTxn() throws IOException {
        createData();
        String de_id = getMap().get(TransactionConstants.de_id);
        cashHelper.closeSessionHelper(de_id);
        cashHelper.createSessionHelper(de_id);
        String order_id=getOrderForPocketing();
        helper.addPocketingAndPayoutTxn(de_id, TransactionConstants.pocketing_desc, order_id, TransactionConstants.pocketing_de_incoming, TransactionConstants.pocketing_de_outgoing, TransactionConstants.pocketing_txn);
        String txn_id = helper.getTransactions(order_id).get(0).get("id").toString();
        helper.submitRevertRequest(txn_id, TransactionConstants.reason, TransactionConstants.user_id, TransactionConstants.email);
        List<Object[]> data = new ArrayList<>();
        data.add(new Object[]{TransactionConstants.invalid_transaction_id, TransactionConstants.reason, TransactionConstants.user_id, TransactionConstants.email,TransactionConstants.error_code, false});
        data.add(new Object[]{txn_id, TransactionConstants.reason, TransactionConstants.user_id, TransactionConstants.email, TransactionConstants.success_code, true});
        return data.iterator();
    }

    @DataProvider(name = "denyRevertTxn")
    public Iterator<Object[]> denyRevertTxn() throws IOException {
        createData();
        String de_id = getMap().get(TransactionConstants.de_id);
        cashHelper.closeSessionHelper(de_id);
        cashHelper.createSessionHelper(de_id);
        String order_id=getOrderForPocketing();
        helper.addPocketingAndPayoutTxn(de_id, TransactionConstants.pocketing_desc, order_id, TransactionConstants.pocketing_de_incoming, TransactionConstants.pocketing_de_outgoing, TransactionConstants.pocketing_txn);
        String txn_id = helper.getTransactions(order_id).get(0).get("id").toString();
        helper.submitRevertRequest(txn_id, TransactionConstants.reason, TransactionConstants.user_id, TransactionConstants.email);
        helper.acceptRevertRequest(txn_id, TransactionConstants.reason, TransactionConstants.user_id, TransactionConstants.email);
        List<Object[]> data = new ArrayList<>();
        data.add(new Object[]{TransactionConstants.invalid_transaction_id, TransactionConstants.reason, TransactionConstants.user_id, TransactionConstants.email,TransactionConstants.error_code, false});
        data.add(new Object[]{txn_id, TransactionConstants.reason, TransactionConstants.user_id, TransactionConstants.email,TransactionConstants.success_code, true});
        return data.iterator();
    }

    @DataProvider(name = "getNovoPay")
    public Iterator<Object[]> getNovoPay() {
        List<Object[]> data = new ArrayList<>();
        data.add(new Object[]{TransactionConstants.invalid_mobno, TransactionConstants.invalid_de});
        data.add(new Object[]{helper.getInvalidDeMob(), TransactionConstants.invalid_de});
        data.add(new Object[]{helper.getValidDeMob(), TransactionConstants.valid_de});
        return data.iterator();
    }

    @DataProvider(name = "depositNovoPay")
    public Iterator<Object[]> depositNovoPay() {
        List<Object[]> data = new ArrayList<>();
        Map<String, Object> map = helper.getValidDedetails();
        String de_id = map.get("id").toString();
        String mobile_no = map.get("mobile").toString();
        cashHelper.closeSessionHelper(de_id);
        cashHelper.createSessionHelper(de_id);
        data.add(new Object[]{null, TransactionConstants.invalid_mobno, TransactionConstants.amt, TransactionConstants.invalid_np_txn_id, TransactionConstants.deposit_invalid_de, 0});
        data.add(new Object[]{de_id, mobile_no, TransactionConstants.amt, Long.toString(Instant.now().getEpochSecond()), TransactionConstants.deposit_valid_de, helper.getFloatingCash(de_id)});
        return data.iterator();
    }

    @DataProvider(name = "addTransaction")
    public Iterator<Object[]> addTransaction() throws IOException {
        List<Object[]> data = new ArrayList<>();
        createData();
        String de_id = getMap().get(TransactionConstants.de_id);
        cashHelper.closeSessionHelper(de_id);
        cashHelper.createSessionHelper(de_id);
        String order_id=getOrderForPocketing();
        data.add(new Object[]{de_id, TransactionConstants.pocketing_desc,order_id, TransactionConstants.pocketing_de_incoming, TransactionConstants.pocketing_de_outgoing, TransactionConstants.pocketing_txn});
        return data.iterator();
    }

    @DataProvider(name = "revertTxn")
    public Iterator<Object[]> revertTxn() throws IOException {
        List<Object[]> data = new ArrayList<>();
        createData();
        String de_id = getMap().get(TransactionConstants.de_id);
        cashHelper.closeSessionHelper(de_id);
        cashHelper.createSessionHelper(de_id);
        String order_id=getOrderForPocketing();
        helper.addPocketingAndPayoutTxn(de_id, TransactionConstants.pocketing_desc, order_id, TransactionConstants.pocketing_de_incoming, TransactionConstants.pocketing_de_outgoing, TransactionConstants.pocketing_txn);
        String txn_id = helper.getTransactions(order_id).get(0).get("id").toString();
        data.add(new Object[]{TransactionConstants.invalid_de_id, txn_id, order_id, TransactionConstants.invalid_de_responses, false});
        data.add(new Object[]{de_id, txn_id, order_id, new String[]{}, true});
        return data.iterator();
    }

    @DataProvider(name = "updateTxn")
    public Iterator<Object[]> updateTxn() throws IOException, InterruptedException {
        List<Object[]> data = new ArrayList<>();
        createData();
        String de_id = getMap().get(TransactionConstants.de_id);
        cashHelper.closeSessionHelper(de_id);
        cashHelper.createSessionHelper(de_id);
        String order_id=getOrderForPocketing();
        Thread.sleep(1000);
        helper.addPocketingAndPayoutTxn(de_id, TransactionConstants.pocketing_desc, order_id, TransactionConstants.pocketing_de_incoming, TransactionConstants.pocketing_de_outgoing, TransactionConstants.pocketing_txn);
        String txn_id = helper.getTransactions(order_id).get(0).get("id").toString();
        data.add(new Object[]{TransactionConstants.invalid_de_id, txn_id, order_id, TransactionConstants.inconsistency, TransactionConstants.invalid_de_responses, false});
        data.add(new Object[]{de_id, TransactionConstants.invalid_np_txn_id, order_id, TransactionConstants.inconsistency,TransactionConstants.update_errors,  false});
        data.add(new Object[]{de_id, txn_id, order_id, TransactionConstants.inconsistency, TransactionConstants.update_errors, true});
        return data.iterator();
    }

    @DataProvider(name = "manualTransaction")
    public Iterator<Object[]> manualTransaction() throws IOException {
        List<Object[]> data = new ArrayList<>();
        createData();
        String de_id = getMap().get(TransactionConstants.de_id);
        cashHelper.closeSessionHelper(de_id);
        cashHelper.createSessionHelper(de_id);
        String order_id = Long.toString(Instant.now().getEpochSecond());
        data.add(new Object[]{de_id, TransactionConstants.pocketing_desc, order_id, TransactionConstants.pocketing_de_incoming, TransactionConstants.pocketing_de_outgoing, TransactionConstants.invalid_txn_type, TransactionConstants.cash_txn});
        try {
            Thread.sleep(1000);
            order_id = Long.toString(Instant.now().getEpochSecond());
            data.add(new Object[]{de_id, TransactionConstants.pocketing_desc, order_id, TransactionConstants.pocketing_de_incoming, TransactionConstants.pocketing_de_outgoing, TransactionConstants.disburse_txn_input, TransactionConstants.disburse_txn_output});

            Thread.sleep(1000);
            order_id = Long.toString(Instant.now().getEpochSecond());
            data.add(new Object[]{de_id, TransactionConstants.pocketing_desc, order_id, TransactionConstants.pocketing_de_incoming, TransactionConstants.pocketing_de_outgoing, TransactionConstants.collect_cash_input, TransactionConstants.collect_cash_output});

            Thread.sleep(1000);
            order_id=getOrderForPocketing();
            data.add(new Object[]{de_id, TransactionConstants.pocketing_desc, order_id, TransactionConstants.pocketing_de_incoming, TransactionConstants.pocketing_de_outgoing, TransactionConstants.order_txn_input, TransactionConstants.order_txn_output});

            Thread.sleep(1000);
            order_id = Long.toString(Instant.now().getEpochSecond());
            data.add(new Object[]{de_id, TransactionConstants.pocketing_desc, order_id, TransactionConstants.pocketing_de_incoming, TransactionConstants.pocketing_de_outgoing, TransactionConstants.cash_incoming_input, TransactionConstants.cash_incoming_output});

            Thread.sleep(1000);
            order_id = Long.toString(Instant.now().getEpochSecond());
            data.add(new Object[]{de_id, TransactionConstants.pocketing_desc, order_id, TransactionConstants.pocketing_de_incoming, TransactionConstants.pocketing_de_outgoing, TransactionConstants.cash_spending_input, TransactionConstants.cash_spending_output});

            Thread.sleep(1000);
            order_id=getOrderForPocketing();
            data.add(new Object[]{de_id, TransactionConstants.pocketing_desc, order_id, TransactionConstants.pocketing_de_incoming, TransactionConstants.pocketing_de_outgoing, TransactionConstants.pocket_txn_input, TransactionConstants.pocket_txn_output});

            Thread.sleep(1000);
            order_id = Long.toString(Instant.now().getEpochSecond());
            data.add(new Object[]{de_id, TransactionConstants.pocketing_desc, "15124219017", TransactionConstants.pocketing_de_incoming, TransactionConstants.pocketing_de_outgoing, TransactionConstants.pocket_txn_input, TransactionConstants.pocket_txn_output});

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return data.iterator();
    }

    @DataProvider(name = "addPocketing")
    public Iterator<Object[]> addPocketing() {
        List<Object[]> data = new ArrayList<>();
        createData();
        String de_id = getMap().get(TransactionConstants.de_id);
        String zone_id = getMap().get(TransactionConstants.zone_id);
        cashHelper.closeSessionHelper(de_id);
        cashHelper.createSessionHelper(de_id);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE , -1);
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String from = dateFormat.format(cal.getTime());
        String to = dateFormat.format(date);
        data.add(new Object[]{TransactionConstants.invalid_de_id, TransactionConstants.city_id, zone_id, from, to, false});
        data.add(new Object[]{de_id, TransactionConstants.city_id, zone_id, from, to, false});
        return data.iterator();
    }

    @DataProvider(name = "addPocketingOps")
    public Iterator<Object[]> addPocketingOps() {
        List<Object[]> data = new ArrayList<>();
        createData();
        String de_id = getMap().get(TransactionConstants.de_id);
        String zone_id = getMap().get(TransactionConstants.zone_id);
        cashHelper.closeSessionHelper(de_id);
        cashHelper.createSessionHelper(de_id);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE , -1);
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String from = dateFormat.format(cal.getTime());
        String to = dateFormat.format(date);
        data.add(new Object[]{TransactionConstants.invalid_de_id, TransactionConstants.city_id, zone_id, from, to, false});
        data.add(new Object[]{de_id, TransactionConstants.city_id, zone_id, from, to, false});
        return data.iterator();
    }

    @DataProvider(name = "cdmDeposit")
    public Iterator<Object[]> cdmDeposit() throws Exception {
        List<Object[]> data = new ArrayList<>();
        createData();
        String de_id = getMap().get(TransactionConstants.de_id);
        de_id = String.format("%06d",Integer.parseInt(de_id));
        System.out.println("de_id"+de_id);
        String rrn_no = Integer.toString(helper.getRandomNo(0000, 9999));
        String payer_acc_no = Integer.toString(helper.getRandomNo(0000000, 999999));
        File file = new File("../Data/Payloads/JSON/deposit_cdm_api");
        String create_payload = FileUtils.readFileToString(file);
        create_payload= create_payload.replace("${txn_amt}", TransactionConstants.cdm_txn_amt).replace("${trace_no}",TransactionConstants.cdm_trace_no)
                .replace("${txn_competion_time}", TransactionConstants.cdm_txn_comp_time).replace("${rrn_no}", rrn_no).replace("${status_code}", TransactionConstants.cdm_status_code)
                .replace("${payer_acno}", "999911"+de_id).replace("${source}", TransactionConstants.cdm_source);
        System.out.println("Payload is  "+create_payload);
        String payload = TransactionsHelper.encrypt(helper.getPublicKey(), create_payload);
        data.add(new Object[]{payload, de_id, TransactionConstants.cdm_txn});
       // create_payload = FileUtils.readFileToString(file).replace("${payer_acno}", payer_acc_no + de_id);
        create_payload = create_payload.replace("${payer_acno}", payer_acc_no + de_id);
        payload = TransactionsHelper.encrypt(helper.getPublicKey(), create_payload);
        data.add(new Object[]{payload, de_id, TransactionConstants.cdm_txn});
        return data.iterator();
    }

    @DataProvider(name = "cdmValidate")
    public Iterator<Object[]> cdmValidate() throws Exception {
        List<Object[]> data = new ArrayList<>();
        createData();
        String de_id = getMap().get(TransactionConstants.de_id);
        de_id = String.format("%06d",Integer.parseInt(de_id));
        System.out.println("de_id"+de_id);
        String rrn_no = Integer.toString(helper.getRandomNo(0000, 9999));
        String payer_acc_no = Integer.toString(helper.getRandomNo(0000000, 999999));
        File file = new File("../Data/Payloads/JSON/validate_cdm_api");
        String create_payload = FileUtils.readFileToString(file);
        create_payload = create_payload.replace("${trace_no}", "000029")
                .replace("${txt_time}", TransactionConstants.cdm_txn_comp_time).replace("${rrn_no}", rrn_no)
                .replace("${payer_acno}", "999911"+de_id).replace("${source}", TransactionConstants.cdm_source);
        System.out.println("payload is "+ create_payload);
        String payload = TransactionsHelper.encrypt(helper.getPublicKey(), create_payload);
        data.add(new Object[]{payload, de_id, TransactionConstants.success_code});

        create_payload = FileUtils.readFileToString(file).replace("${payer_acno}", payer_acc_no + de_id);
        payload = TransactionsHelper.encrypt(helper.getPublicKey(), create_payload);
        data.add(new Object[]{payload, de_id, TransactionConstants.bad_error_code});

        create_payload = FileUtils.readFileToString(file).replace("${payer_acno}", payer_acc_no + payer_acc_no);
        payload = TransactionsHelper.encrypt(helper.getPublicKey(), create_payload);
        data.add(new Object[]{payload, de_id, TransactionConstants.bad_error_code});

        create_payload = FileUtils.readFileToString(file).replace("${payer_acno}", payer_acc_no + payer_acc_no+payer_acc_no);
        payload = TransactionsHelper.encrypt(helper.getPublicKey(), create_payload);
        data.add(new Object[]{payload, de_id, TransactionConstants.bad_error_code});

        return data.iterator();
    }


    @DataProvider(name = "upiDeposit")
    public Iterator<Object[]> upiDeposit() throws Exception {
        List<Object[]> data = new ArrayList<>();
        createData();
        String de_id = getMap().get(TransactionConstants.de_id);
        de_id = String.format("%06d",Integer.parseInt(de_id));
        System.out.println("de_id"+de_id);
        String rrn_no = Integer.toString(helper.getRandomNo(0000, 9999));
        String payer_acc_no = Integer.toString(helper.getRandomNo(0000000, 999999));
        String bank_rrn = Long.toString(generateRandom(12));
        File file = new File("../Data/Payloads/JSON/deposit_upi_api");
        String create_payload = FileUtils.readFileToString(file);
        create_payload = create_payload.replace("${merchant_id}", TransactionConstants.upi_merchant_id).replace("${submerchant_id}",TransactionConstants.upi_merchant_id)
                .replace("${bank_rrn}", bank_rrn).replace("${merchant_txn_id}", de_id).replace("${payer_name}", TransactionConstants.upi_name)
                .replace("${payer_mobile}", TransactionConstants.upi_mobile).replace("${payer_va}", TransactionConstants.cdm_payer).replace("${payer_amt}", TransactionConstants.upi_amt)
                .replace("${status}", TransactionConstants.upi_status).replace("${txn_init}", TransactionConstants.upi_txn);
        System.out.println("payload is" + create_payload);
        String payload = TransactionsHelper.encrypt(helper.getPublicKey(), create_payload);
        data.add(new Object[]{payload, de_id, TransactionConstants.upi_txn_enum, true});

        create_payload = FileUtils.readFileToString(file).replace("${merchant_txn_id}", payer_acc_no);
        payload = TransactionsHelper.encrypt(helper.getPublicKey(), create_payload);
        data.add(new Object[]{payload, de_id, TransactionConstants.upi_txn_enum, false});

        rrn_no = Integer.toString(helper.getRandomNo(0000, 9999));
        create_payload = FileUtils.readFileToString(file).replace("${merchant_txn_id}", payer_acc_no + payer_acc_no).replace("${bank_rrn}", bank_rrn);
        payload = TransactionsHelper.encrypt(helper.getPublicKey(), create_payload);
        data.add(new Object[]{payload, de_id, TransactionConstants.upi_txn_enum, false});


        return data.iterator();
    }

    public static long generateRandom(int length) {
        Random random = new Random();
        char[] digits = new char[length];
        digits[0] = (char) (random.nextInt(9) + '1');
        for (int i = 1; i < length; i++) {
            digits[i] = (char) (random.nextInt(10) + '0');
        }
        return Long.parseLong(new String(digits));
    }

    @DataProvider(name = "createZone")
    public Iterator<Object[]> createZone() {
        List<Object[]> data = new ArrayList<>();
        String id = Integer.toString(new Random().nextInt(10001));
        String zone_id = zoneHelper.getZoneFromDb().get(0).get("id").toString();
        String area_id = zoneHelper.getAreaFromDb().get(0).get("id").toString();
        data.add(new Object[]{zone_id, id, 0});
        data.add(new Object[]{id, area_id, 1});
        data.add(new Object[]{id, id, 0});
        return data.iterator();
    }

    @DataProvider(name = "createDe")
    public Iterator<Object[]> createDe() {
        List<Object[]> data = new ArrayList<>();
        String id = Integer.toString(new Random().nextInt(10001));
        String de_id = deHelper.getDEFromDb().get(0).get("id").toString();
        data.add(new Object[]{FinanceConstants.create, de_id, 0});
        data.add(new Object[]{FinanceConstants.create, id, 1});
        return data.iterator();
    }
}
