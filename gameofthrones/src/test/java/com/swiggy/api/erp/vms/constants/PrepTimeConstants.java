package com.swiggy.api.erp.vms.constants;

/**
 * Created by kiran.j on 4/26/18.
 */
public interface PrepTimeConstants {

    String from_time = "startTime";
    String end_time = "endTime";
    String prep_time = "prepTime";
    String type = "type";
    String days = "days";
    int negative_status = -1;
    String negative_message = "Unable to save prep time for the restaurant.";
    String auth_password = "El3ment@ry";
    String auth = "$w199y@zolb";
    String fetch_prep = "Fetched prep time for a restaurant  slot wise successfully";
    String login_fail = "Login failed";
    String invalid_input = "Invalid Input";
    String invalid_session = "Invalid Session";
    String fetch_multi_prep = "Fetched prep time for multiple restaurants successfully";
    String fail_fetch_prep = "Failed to fetch prep time for multiple restaurants";
}
