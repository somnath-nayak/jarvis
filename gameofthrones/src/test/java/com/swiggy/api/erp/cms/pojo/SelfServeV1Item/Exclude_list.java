package com.swiggy.api.erp.cms.pojo.SelfServeV1Item;

public class Exclude_list {
	private String third_party_variation_id;

    private String group_id;

    private String variation_id;

    private String third_party_group_id;

    public String getThird_party_variation_id ()
    {
        return third_party_variation_id;
    }

    public void setThird_party_variation_id (String third_party_variation_id)
    {
        this.third_party_variation_id = third_party_variation_id;
    }

    public String getGroup_id ()
    {
        return group_id;
    }

    public void setGroup_id (String group_id)
    {
        this.group_id = group_id;
    }

    public String getVariation_id ()
    {
        return variation_id;
    }

    public void setVariation_id (String variation_id)
    {
        this.variation_id = variation_id;
    }

    public String getThird_party_group_id ()
    {
        return third_party_group_id;
    }

    public void setThird_party_group_id (String third_party_group_id)
    {
        this.third_party_group_id = third_party_group_id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [third_party_variation_id = "+third_party_variation_id+", group_id = "+group_id+", variation_id = "+variation_id+", third_party_group_id = "+third_party_group_id+"]";
    }
}
			
			