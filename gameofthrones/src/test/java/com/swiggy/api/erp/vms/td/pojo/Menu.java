package com.swiggy.api.erp.vms.td.pojo;

import org.apache.commons.lang.builder.ToStringBuilder;

/***
 * 
 * @author rachana
 *
 */

public class Menu {

    private String id;
    private String name;

    /**
     * No args constructor for use in serialization
     *
     */
    public Menu() {
    }

    /**
     *
     * @param id
     * @param name
     */
    public Menu(String id, String name) {
        super();
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("name", name).toString();
    }


}