package com.swiggy.api.erp.ff.tests.telephonyPlatform;

import com.swiggy.api.erp.ff.constants.TelephonyPlatformConstants;
import com.swiggy.api.erp.ff.dp.telephonyPlatform.TelephonyPlatformTestData;
import com.swiggy.api.erp.ff.helper.TelephonyPlatformHelper;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.WireMockHelper;
import org.testng.annotations.*;

import java.io.IOException;
import java.util.HashMap;

public class WebHookTests extends TelephonyPlatformTestData implements TelephonyPlatformConstants{

    TelephonyPlatformHelper helper = new TelephonyPlatformHelper();
    WireMockHelper wireMockHelper = new WireMockHelper();
    String clientId, appId, callSid;

    @BeforeClass
    public void startMockServer() throws InterruptedException, IOException {
        HashMap<String, String> validHeader = new HashMap<>();
        validHeader.put("AuthKey", "tpadmin@2018");
        validHeader.put("Content-Type", "application/json");
//        wireMockHelper.startMockServer(6666);
        helper.stubExotelCreateIVRSAPIResponse();
        Processor createIvrsAppResponse = helper.createIvrsApp(validHeader, testAppName, testPodName, testAppUrl, testCompletedCallbackUrl, testFailedCallbackUrl, testKeyPressCallbackUrl);
        clientId = String.valueOf(createIvrsAppResponse.ResponseValidator.GetNodeValueAsInt("clientId"));
        appId = String.valueOf(createIvrsAppResponse.ResponseValidator.GetNodeValueAsInt("appId"));
        Processor createIvrsCallResponse = helper.ivrsRequest(clientId, helper.getUniqueRequestId(), testCallerRegionId, testCalleeId, appId);
        System.out.println("Create call response is == " + createIvrsCallResponse.ResponseValidator.GetBodyAsText());
        int callId = createIvrsCallResponse.ResponseValidator.GetNodeValueAsInt("id");
        callSid = String.valueOf(helper.getCallSidFromDB(callId));
        helper.stubClientAPIForCompletedCallback(String.valueOf(callId));
        helper.stubClientAPIForFailedCallback(String.valueOf(callId));
        helper.stubClientAPIForKeypressCallback(String.valueOf(callId), digit);
    }

    @AfterClass
    public void stopMockServer() throws InterruptedException {
//        Thread.sleep(1000);
//        wireMockHelper.stopMockServer();
    }

    @Test(description = "Test Telephony Webhook to client")
    public void webhookTests() {
        System.out.println("***************************************** webhook started *****************************************");


        helper.exotelStatusCallback(callSid, statusCompleted);
        helper.exotelStatusCallback(callSid, statusFailed);
        helper.exotelKeypressCallback(callSid, digit);

        System.out.println("######################################### webhook compleated #########################################");
    }
}
