package com.swiggy.api.erp.delivery.helper;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.delivery.Pojos.DE;
import com.swiggy.api.erp.delivery.constants.DEAlchemistConstants;
import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.Processor;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

public class DeliveryServiceApiHelper {
    DeliveryServiceApiWrapperHelper api_helper = new DeliveryServiceApiWrapperHelper();
    DeliveryHelperMethods del = new DeliveryHelperMethods();
    DeliveryDataHelper DH = new DeliveryDataHelper();
    Processor processor = null;
    ArrayList<String> ruleIds=new ArrayList<>();
    HashMap<String,String> ruleDetailsMap=null;
    List<String> allActiveRules = new ArrayList<>();
    static JSONObject masterconfigMapJSON = new JSONObject();
    static int TC_Counter=0;


    public String createNewOrder(HashMap<String, String> order_defaultMap ) {
        String order_id = "";
        String orderJson = DH.returnOrderJson(order_defaultMap);
        Boolean orderstatus = DH.pushOrderJsonToRMQAndValidate(orderJson);
        if (orderstatus) {
            order_id = DH.getOrderIdFromOrderJson(orderJson);
            System.out.println("Order id created -----" + order_id);
            return order_id;
        } else {
            System.out.println("Unable to place order");
            return null;
        }
    }

    public String createNewOrderSLD(HashMap<String, String> order_defaultMap ) {
        String order_id = "";
        String orderJson = DH.returnOrderJson(order_defaultMap);
        Boolean orderstatus = DH.pushOrderToDelivery_Orders(orderJson);
        if (orderstatus) {
            order_id = DH.getOrderIdFromOrderJson(orderJson);
            System.out.println("Order id created -----" + order_id);
            return order_id;
        } else {
            System.out.println("Unable to place order");
            return null;
        }
    }

    public String createNewOrderIDGenerator(HashMap<String, String> order_defaultMap ) {
        String order_id = "";
        String orderJson = DH.returnOrderJson(order_defaultMap);
            order_id = DH.getOrderIdFromOrderJson(orderJson);
            System.out.println("Order id created -----" + order_id);
            return order_id;
    }

    public void rejectOrder(String order_id, String de_id, int RejectCounter){
        for (int i = 0; i < RejectCounter; i++) {
            processor = api_helper.rejectOrder(order_id);
            if (processor.ResponseValidator.GetResponseCode() == 200)
                System.out.println("Order id " + order_id + " rejected successfully");
            else
                System.out.println("Unable to reject Order id " + order_id);
        }
    }


    public void mergeOrders(String order_id1, String order_id2) {
        String batch_id1 = null;
        String batch_id2 = null;
        String query1 = "Select * from trips where order_id=" + order_id1 + ";";
        Object obj1 = del.dbhelperget(query1, "batch_id");
        String query2 = "Select * from trips where order_id=" + order_id2 + ";";
        Object obj2 = del.dbhelperget(query2, "batch_id");
        if (obj1 == null && obj2 == null) {
            System.out.println("Unable to find batch id for orders");
        } else {
            batch_id1 = obj1.toString();
            batch_id2 = obj2.toString();
        }
        System.out.println("batch id 1 is " + batch_id1);
        System.out.println("batch id 2 is " + batch_id2);
        processor = api_helper.mergeAndBatch(batch_id1, batch_id2);
        String resp = processor.ResponseValidator.GetBodyAsText();
        int statusCode = JsonPath.read(resp, "$.statusCode");
        String statusmessage = "";
        if (statusCode == 0) {
            statusmessage = JsonPath.read(resp, "$.statusMessage");
            Assert.assertEquals("Success", statusmessage);
            System.out.println("Order ids batched : " + order_id1 + " ---- " + order_id2);
        }
        else {
            System.out.println("Unable to batch Order ids: " + order_id1 + " ---- " + order_id2);
        }
    }

    public String getBatchID(String order_id){
        String batch_id = null;
        String query1 = "Select * from trips where order_id=" + order_id + ";";
        Object obj1 = del.dbhelperget(query1, "batch_id");
        if (obj1 == null) {
            System.out.println("Unable to find batch id for orders");
            return null;
        }
        else {
            batch_id = obj1.toString();
        }
        System.out.println("batch id 1 is " + batch_id);
        return batch_id;
    }

    public void assignDE(String order_id, String de_id) {
        String batch_id = getBatchID(order_id);
        processor = api_helper.DeAssignment(batch_id, de_id);
        String resp = processor.ResponseValidator.GetBodyAsText();
        int statusCode = JsonPath.read(resp, "$.statusCode");
        String statusmessage = "";
        if (statusCode == 0) {
            statusmessage = JsonPath.read(resp, "$.statusMessage");
            System.out.println(order_id + " assigned to DE id : " + de_id + statusmessage);
        } else {
            System.out.println("Unable to assign DE");
        }
    }

    public String createDEandmakeDEActive(int zoneid) {
        String areaid = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForList("select id from area where zone_id=" + zoneid).get(0).get("id").toString();
        DE de = new DE(zoneid, Integer.parseInt(areaid));
        de.build();
        ObjectMapper om = new ObjectMapper();
        String json = null;
        try {
            json = om.writeValueAsString(de);
        } catch (IOException e) {
            e.printStackTrace();
        }
        processor = api_helper.createDE(json);
        String response = processor.ResponseValidator.GetBodyAsText();
        System.out.println(response);
        String de_id = JsonPath.read(response, "$.data").toString();
        System.out.println(de_id);
        SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update("update delivery_boys set enabled =1 where id= " + de_id);
        return de_id;
    }

    public void markDEAvailable(String de_id) {
        processor = api_helper.markDEFree(de_id);
        if (processor.ResponseValidator.GetResponseCode() == 200) {
            System.out.println("DE id " + de_id + " is made free");
        }
        else {
            System.out.println("Unable to make " + de_id + " free");
        }
    }

    public HashMap<String,String> verifyActiveRuleInDB(String expression){
        ruleDetailsMap=new HashMap<>();
        String IncentiveRulesQuery="select * from incentive_rules where active=1 and expression like '%";
        String RuleId = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).queryForList(IncentiveRulesQuery+expression+"%'").get(0).get("id").toString();
        String RuleExpression = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).queryForList(IncentiveRulesQuery+expression+"%'").get(0).get("expression").toString();
        String ZoneIdFetchQuery="select * from rule_zone_mapping where rule_id=";
        String zoneId=SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).queryForList(ZoneIdFetchQuery+RuleId).get(0).get("zone_id").toString();
        String CityFetchQuery="select * from area where id=";
        String cityId=SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForList(CityFetchQuery+zoneId).get(0).get("city_id").toString();
        String computeOn = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).queryForList(IncentiveRulesQuery+expression+"%'").get(0).get("compute_on").toString();
        ruleIds.add(RuleId);
        ruleDetailsMap.put("ruleid",RuleId);
        ruleDetailsMap.put("city",cityId);
        ruleDetailsMap.put("zone",zoneId);
        ruleDetailsMap.put("event",computeOn);
        ruleDetailsMap.put("expression", RuleExpression);

        System.out.println(cityId);
        System.out.println(RuleId);
        System.out.println(zoneId);
        System.out.println(computeOn);
        System.out.println(RuleExpression);

        return ruleDetailsMap;
    }

    public HashMap<String,String> createRules(String zone,String expression,String computeOn){
        TC_Counter++;
        String incentive_group= DEAlchemistConstants.IncentiveGroup;
        this.ruleDetailsMap= new HashMap<>();
        Random random=new Random();
        String bonus= String.valueOf((random.nextInt(15)));
        String ruleNameInDB = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).queryForList("select * from incentive_rules where rule_name like '%AutomatedRules%' order by id desc").get(0).get("rule_name").toString();
        int counter= Integer.parseInt(ruleNameInDB.replaceAll("[^0-9]", ""));
        counter++;
        String ruleName="AutomatedRules"+counter;
        String zoneName = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForList("select * from zone where id=" + zone).get(0).get("name").toString();
        processor = api_helper.createRule(ruleName,bonus,incentive_group,zoneName,expression,computeOn);
        if (processor.ResponseValidator.GetResponseCode() == 200) {
            String ruleId = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).queryForList("select * from incentive_rules where rule_name='" + ruleName +"'").get(0).get("id").toString();
            String CityfetchQuery="select * from area where id=";
            String cityId=SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForList(CityfetchQuery+zone).get(0).get("city_id").toString();
            System.out.println("Rule id " + ruleId + " is created successfully");
            ruleIds.add(ruleId);
            ruleDetailsMap.put("ruleid",ruleId);
            ruleDetailsMap.put("zone",zone);
            ruleDetailsMap.put("event",computeOn);
            ruleDetailsMap.put("expression", expression);
            ruleDetailsMap.put("city",cityId);
            return ruleDetailsMap;
        }
        else{
            System.out.println(processor.ResponseValidator.GetBodyAsText());
            return null;
        }
    }

    public void disableRules(){
        String allRules=String.join(",",ruleIds);
        String disableRulesQuery="update incentive_rules set active=0 where id IN (";
        SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).update(disableRulesQuery+allRules+")");
    }

    public boolean disableAllActiveRules(){
        List<Map<String, Object>> activeRules =null;
        try {
            activeRules = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).queryForList("select * from incentive_rules where active=1");
        }
        catch(NullPointerException np){
            System.out.println("Currently no rules are active");
            return true;
        }
        for (int i=0;i<activeRules.size();i++){
            allActiveRules.add(activeRules.get(i).get("id").toString());
        }
        String RulesList = String.join(",", allActiveRules);
        System.out.println("All pre Active Rules Before Suite run are deactivated: "+ RulesList);
        String disableActiveRulesQuery="update incentive_rules set active=0 where id IN (";
        if(allActiveRules.size()>0)
            SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).update(disableActiveRulesQuery +RulesList+")");
        return true;
    }

    public List<String> getActiveRules(){
        if(allActiveRules.size()>0)
            return allActiveRules;
        else {
            System.out.println("Currently no rules are active");
            return Collections.emptyList();
        }
    }

    public void enableAllInactiveRules(){
        String enableActiveRulesQuery="update incentive_rules set active=1 where id IN (";
        String RulesList = String.join(",", allActiveRules);
        if(allActiveRules.size()>0)
            SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).update(enableActiveRulesQuery +RulesList+")");
        System.out.println("All pre Active Rules Before Suite run are Activated again: "+ RulesList);
    }

    public void batchCountDBUpdates(String orderCount, String parent_batch_id, String child_batch_id) {
        String orderCountQuery = "update batch set order_count =" + orderCount + " where id= " + parent_batch_id + ";";
        SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(orderCountQuery);

        int counter = Integer.valueOf(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForList("select * from batch_audit order by id desc;").get(0).get("id").toString());
        counter++;

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
        Date cur_time = new Date();
        String date = formatter.format(cur_time);

        String batchAuditQuery1 = "insert into batch_audit values(" + counter + "," + parent_batch_id + "," + parent_batch_id + ",'" + date + "');";
        SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(batchAuditQuery1);
        counter++;

        String batchAuditQuery2 = "insert into batch_audit values(" + counter + "," + parent_batch_id + "," + child_batch_id + ",'" + date + "');";
        SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(batchAuditQuery2);
    }

    public void enableRainMode(String zone_id) {
        SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(DeliveryConstant.enableHeavyRainMode + zone_id);
    }

    public void disableRainMode(String zone_id) {
        SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update(DeliveryConstant.disableHeavyRainMode + zone_id);
    }

    public boolean cancelOrder(String order_id){
        processor = api_helper.cancelOrder(order_id);
        if (processor.ResponseValidator.GetResponseCode() == 200) {
            System.out.println("Order cancelled successfully " + order_id);
            return true;
        }
        else {
            System.out.println("Unable to cancel Order");
            return false;
        }
    }

    public void calculateWaitTime(String order_id) {
//        String getPlacementTime = "select pickedup_time - arrived_time from trips where order_id =" + order_id + ";";
//        String waitTime = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForList(waitTimeQuery).get(0).get("pickedup_time - arrived_time").toString();
//        //double deWaitTime = getPrepTimeInMinutes() + incentiveOrderPojo.getPlacementTime() - ((incentiveOrderPojo.getAssignedTime() - incentiveOrderPojo.getReceivedTime()) / 1000) / 60 - incentiveOrderPojo.getFirstMileTimeInMinutes();
//        int deWaitTime= 30+
//        return Integer.parseInt(waitTime);
    }

    public int expectedBonus(String expression) {
        String expectedBonusQuery = "select * from incentive_rules where id in (select rule_id from rule_zone_mapping where zone_id =4) and active =1 and now() between `validity_start_time` and `validity_end_time` and expression like '%" + expression + "%';";
        String expectedBonus = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName2).queryForList(expectedBonusQuery).get(0).get("bonus").toString();
        System.out.println("Expected Bonus is" + expectedBonus);
        return Integer.parseInt(expectedBonus);
    }

    public void waitIntervalMili(int pollinterval)
    {
        try {
            Thread.sleep(pollinterval);
        }
        catch(InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void convertTestCaseResultstoJSON(){
        JSONObject configMapJSON = null;

        try {
            configMapJSON = new JSONObject(ruleDetailsMap);
            masterconfigMapJSON.put("TCID_"+TC_Counter,configMapJSON);
            System.out.println("Test Case Result JSON :"+ configMapJSON.toString() );
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void printTestSuiteResultJSON(){
        System.out.println("Test Suite Result JSON :"+  masterconfigMapJSON.toString());
    }

    public JSONObject mergeJSONObjects(JSONObject json1, JSONObject json2) {
        JSONObject mergedJSON = new JSONObject();
        try {
            mergedJSON = new JSONObject(json1, JSONObject.getNames(json1));
            for (String Key : JSONObject.getNames(json2)) {
                mergedJSON.put(Key, json2.get(Key));
            }

        } catch (JSONException e) {
            throw new RuntimeException("JSON Exception" + e);
        }
        return mergedJSON;
    }

    @Test
    public void Testme(){
//        String de_id=createDEandmakeDEActive(4);
      String de_id="216";
        DeliveryDataHelper dataHelper=new DeliveryDataHelper();
        dataHelper.loginDEandUpdateLocation(de_id,"1.8","12.9554712","77.5850857");
        HashMap<String,String> orderjson= new HashMap<>();
        String order_id=createNewOrder(orderjson);
        assignDE(order_id,de_id);
        dataHelper.doCAPRD(order_id,de_id,"confirmed","delivered");
    }

}