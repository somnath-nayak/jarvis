package com.swiggy.api.erp.cms.pojo.FullMenu;

/**
 * Created by kiran.j on 2/2/18.
 */
public class Entity
{
    public Entity(){
    }

    private Items[] items;

    private Main_categories[] main_categories;

    public Items[] getItems ()
    {
        return items;
    }

    public Entity setItems (Items[] items)
    {
        this.items = items;
        return this;
    }

    public Main_categories[] getMain_categories ()
    {
        return main_categories;
    }

    public void setMain_categories (Main_categories[] main_categories)
    {
        this.main_categories = main_categories;
    }

    public Entity build() {
        setDefaultValues();
        return this;
    }

    public void setDefaultValues() {

    }

    @Override
    public String toString()
    {
        return "{items = "+items+", main_categories = "+main_categories+"}";
    }
}
