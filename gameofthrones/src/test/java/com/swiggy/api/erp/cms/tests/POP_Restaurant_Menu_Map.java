package com.swiggy.api.erp.cms.tests;

import org.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.dp.POP_dataProvider;
import com.swiggy.api.erp.cms.helper.POP_Restaurant_Menu_Map_Helper;

import framework.gameofthrones.JonSnow.Processor;

/*
 *   Create restaurant menu map for restaurant-id=4551
 */

public class POP_Restaurant_Menu_Map extends POP_dataProvider {
	int restaurantMenuMapRegularId=0, restaurantMenuMapPopId=0;
	POP_Restaurant_Menu_Map_Helper obj = new POP_Restaurant_Menu_Map_Helper();
	
	@Test(dataProvider="restaurantMenuMapRegular", priority=0, groups={"Sanity_TC", "Smoke_TC", "Regression_TC"},
			description="Create Restaurant-Menu-Map for REGULAR menu type")
	public void createRestaurantMenuMapWithRegularMenuType(int restaurant_id, int menu_id, String menu_type) 
			throws JSONException, InterruptedException
	{
		Processor p1 = obj.createRestaurantMenuMapRegular(restaurant_id, menu_id, menu_type);
		String response = p1.ResponseValidator.GetBodyAsText();
		restaurantMenuMapRegularId = JsonPath.read(response, "$.data.id");
		int statusCode = JsonPath.read(response, "$.statusCode");
		Assert.assertEquals(statusCode, 1);
	}

	@Test(priority=1, groups={"Sanity_TC", "Smoke_TC", "Regression_TC"},
			description="Retrieve the created Restaurant-Menu-Map for REGULAR menu type")
	public void getRestaurantMenuMapWithRegularMenuType() 
			throws JSONException, InterruptedException
	{
		Processor p = obj.getRestaurantMenuMapRegular(restaurantMenuMapRegularId);
		String response = p.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(response, "$.statusCode");
		Assert.assertEquals(statusCode, 1);
	}

	@Test(priority=2, groups={"Sanity_TC", "Smoke_TC", "Regression_TC"},
			description="Delete the created Restaurant-Menu-Map for REGULAR menu type")
	public void deleteRestaurantMenuMapWithRegularMenuType() 
			throws JSONException, InterruptedException
	{
		Processor p = obj.deleteRestaurantMenuMapRegular(restaurantMenuMapRegularId);
		String response = p.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(response, "$.statusCode");
		Assert.assertEquals(statusCode, 1);
	}
	
	@Test(dataProvider="restaurantMenuMapPop", priority=3, groups={"Sanity_TC", "Smoke_TC", "Regression_TC"},
	description="Create Restaurant-Menu-Map for POP menu type")
	public void createRestaurantMenuMapWithPopMenuType(int restaurant_id, int menu_id, String menu_type) 
			throws JSONException, InterruptedException
	{
		Processor p1 = obj.createRestaurantMenuMapPop(restaurant_id, menu_id, menu_type);
		String response = p1.ResponseValidator.GetBodyAsText();
		restaurantMenuMapRegularId = JsonPath.read(response, "$.data.id");
		int statusCode = JsonPath.read(response, "$.statusCode");
		Assert.assertEquals(statusCode, 1);
	}

	@Test(priority=4, groups={"Sanity_TC", "Smoke_TC", "Regression_TC"},
			description="Retrieve the created Restaurant-Menu-Map for POP menu type")
	public void getRestaurantMenuMapWithPopMenuType() 
			throws JSONException, InterruptedException
	{
		Processor p = obj.getRestaurantMenuMapPop(restaurantMenuMapRegularId);
		String response = p.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(response, "$.statusCode");
		Assert.assertEquals(statusCode, 1);
	}

	@Test(priority=5, groups={"Sanity_TC", "Smoke_TC", "Regression_TC"},
			description="Delete the created Restaurant-Menu-Map for POP menu type")
	public void deleteRestaurantMenuMapWithPopMenuType() 
			throws JSONException, InterruptedException
	{
		Processor p = obj.deleteRestaurantMenuMapPop(restaurantMenuMapRegularId);
		String response = p.ResponseValidator.GetBodyAsText();
		int statusCode = JsonPath.read(response, "$.statusCode");
		Assert.assertEquals(statusCode, 1);
	}

}
