package com.swiggy.api.erp.cms.dp;

import org.testng.annotations.DataProvider;

public class POP_dataProvider {
	
	@DataProvider(name = "areaScheduleCREATEData")
	public Object[][] createAreaScheduledata() {
		
	return new Object[][]{{"LUNCH",44,1410,1510,"SUN","POP_MENU"}};
	}
	
	@DataProvider(name = "invalidareaScheduleCREATEData")
	public Object[][] createInvalidAreaScheduledata() {
	return new Object[][]{{"LUNCH",0,1410,1510,"SUN","POP_MENU"}};
	}
	
	@DataProvider(name = "areaScheduleWithExistingSlot")
	public Object[][] createAreaScheduleWithExistingSlot() {
	return new Object[][]{{"LUNCH",10,1600,1700,"WED","POP_MENU"}};
	}
	
	@DataProvider(name = "areaScheduleWithInvalidDayName")
	public Object[][] createAreaScheduleWithInvalidDayName() {
	return new Object[][]{{"LUNCH",10,1600,1700,"MONDAY","POP_MENU"}};
	}
	
	@DataProvider(name = "areaScheduleUPDATEData")
	public Object[][] updateAreaScheduledata() {
		return new Object[][]{{"BREAKFAST",44,1410,1510,"THU","POP_MENU"}};
	}
	
	@DataProvider(name = "cityScheduleCREATEData")
	public Object[][] createCityScheduledata() {
	return new Object[][]{{"BREAKFAST",5,800,1100,"SUN","POP_MENU"}};
	}
	
	@DataProvider(name = "invalidcityScheduleCREATEData")
	public Object[][] createInvalidCityScheduledata() {
	return new Object[][]{{"LUNCH",0,1410,1510,"SUN","POP_MENU"}};
	}
	
	@DataProvider(name = "cityScheduleWithExistingSlot")
	public Object[][] createCityScheduleWithExistingSlot() {
	return new Object[][]{{"LUNCH",10,1700,1800,"MON","POP_MENU"}};
	}
	
	@DataProvider(name = "cityScheduleWithInvalidDayName")
	public Object[][] createCityScheduleWithInvalidDayName() {
	return new Object[][]{{"LUNCH",10,1600,1700,"MONDAY","POP_MENU"}};
	}
	
	@DataProvider(name = "cityScheduleUPDATEData")
	public Object[][] updateCityScheduledata() {
	return new Object[][]{{"BREAKFAST",5,1410,1510,"THU","POP_MENU"}};
	}
	
	@DataProvider(name = "itemScheduleAreaCREATEData")
	public Object[][] createItemScheduledataByNewAreaSchedule() {
	return new Object[][]{{1766423,"2019-01-14",10,1, "LUNCH",44,1410,1510,"SUN","POP_MENU" }};
	}
	
	@DataProvider(name = "itemScheduleCREATEWithExistingAreaSchedule")
	public Object[][] createItemScheduledataByExistingAreaSchedule() {
	return new Object[][]{{1766424, 630973, "2019-01-14",10,1}};
	}
	
	@DataProvider(name = "itemScheduleAreaUPDATEData")
	public Object[][] updateItemScheduledataByArea() {
	return new Object[][]{{15,1}};
	}
	
	@DataProvider(name = "itemScheduleCityCREATEData")
	public Object[][] createItemScheduledataByCity() {
	return new Object[][]{{1766423,"2019-01-14",10,1, "BREAKFAST",5,800,1100,"SUN","POP_MENU" }};
	}
	
	@DataProvider(name = "itemScheduleCREATEWithExistingCitySchedule")
	public Object[][] createItemScheduledataByExistingCitySchedule() {
	return new Object[][]{{1766424, 629965, "2019-01-14",10,1}};
	}
	
	@DataProvider(name = "itemScheduleCityUPDATEData")
	public Object[][] updateItemScheduledataByCity() {
	return new Object[][]{{15,2}};
	}
	
	@DataProvider(name = "openRestaurantWithMenuMap")
	public Object[][] openRestaurantWithMenuMap() {
	return new Object[][]{{4553,"2019-01-11T13:00:31","2019-01-11T14:30:31"}};
	}
	
	@DataProvider(name = "closeRestaurantWithMenuMap")
	public Object[][] closeRestaurantWithMenuMap() {
	return new Object[][]{{4553,"2019-01-11T13:00:31","2019-01-11T14:30:31"}};
	}
	
	@DataProvider(name = "openRestaurantWithoutMenuMap")
	public Object[][] openRestaurantWithoutMenuMap() {
	return new Object[][]{{1973,"2019-01-11T13:00:31","2019-01-11T14:30:31"}};
	}
	
	@DataProvider(name = "closeRestaurantWithoutMenuMap")
	public Object[][] closeRestaurantWithoutMenuMap() {
	return new Object[][]{{1973,"2019-01-11T13:00:31","2019-01-11T14:30:31"}};
	}
	
	@DataProvider(name = "restaurantMenuMapRegular")
	public Object[][] restaurantMenuMapRegular() {
	return new Object[][]{{4551, 102186, "REGULAR_MENU"}};
	}
	
	@DataProvider(name = "restaurantMenuMapPop")
	public Object[][] restaurantMenuMapPop() {
	return new Object[][]{{4551, 102186, "POP_MENU"}};
	}
	
	
}
