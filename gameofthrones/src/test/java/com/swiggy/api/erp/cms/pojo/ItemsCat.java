package com.swiggy.api.erp.cms.pojo;
import com.swiggy.api.erp.cms.constants.MenuConstants;

public class ItemsCat
{
    private String eligible_for_long_distance;

    private int in_stock;

    private int is_veg;

    private String vat;

    private String service_charges;

    private String recommended;

    private String service_tax;

    private String type;

    private int serves_how_many;

    private String order;

    private String description;

    private int addon_limit;

    private String name;

    private String updated_by;

    private String packing_charges;

    private String variant_description;

    private int packing_slab_count;

    private String created_by;

    private int is_perishable;

    private int enabled;

    private int addon_free_limit;

    private int restaurant_id;

    private int category_id;

    private String updated_at;

    private int price;

    private int sub_category_id;

    private int is_spicy;

    private String updated_on;

    private String active;

    private String is_discoverable;

    private String addon_description;

    private String preparation_style;

    public String getEligible_for_long_distance ()
    {
        return eligible_for_long_distance;
    }

    public void setEligible_for_long_distance (String eligible_for_long_distance)
    {
        this.eligible_for_long_distance = eligible_for_long_distance;
    }

    public int getIn_stock ()
    {
        return in_stock;
    }

    public void setIn_stock (int in_stock)
    {
        this.in_stock = in_stock;
    }

    public int getIs_veg ()
    {
        return is_veg;
    }

    public void setIs_veg (int is_veg)
    {
        this.is_veg = is_veg;
    }

    public String getVat ()
    {
        return vat;
    }

    public void setVat (String vat)
    {
        this.vat = vat;
    }

    public String getService_charges ()
    {
        return service_charges;
    }

    public void setService_charges (String service_charges)
    {
        this.service_charges = service_charges;
    }

    public String getRecommended ()
    {
        return recommended;
    }

    public void setRecommended (String recommended)
    {
        this.recommended = recommended;
    }

    public String getService_tax ()
    {
        return service_tax;
    }

    public void setService_tax (String service_tax)
    {
        this.service_tax = service_tax;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    public int getServes_how_many ()
    {
        return serves_how_many;
    }

    public void setServes_how_many (int serves_how_many)
    {
        this.serves_how_many = serves_how_many;
    }

    public String getOrder ()
    {
        return order;
    }

    public void setOrder (String order)
    {
        this.order = order;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public int getAddon_limit ()
    {
        return addon_limit;
    }

    public void setAddon_limit (int addon_limit)
    {
        this.addon_limit = addon_limit;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getUpdated_by ()
    {
        return updated_by;
    }

    public void setUpdated_by (String updated_by)
    {
        this.updated_by = updated_by;
    }

    public String getPacking_charges ()
    {
        return packing_charges;
    }

    public void setPacking_charges (String packing_charges)
    {
        this.packing_charges = packing_charges;
    }

    public String getVariant_description ()
    {
        return variant_description;
    }

    public void setVariant_description (String variant_description)
    {
        this.variant_description = variant_description;
    }

    public int getPacking_slab_count ()
    {
        return packing_slab_count;
    }

    public void setPacking_slab_count (int packing_slab_count)
    {
        this.packing_slab_count = packing_slab_count;
    }

    public String getCreated_by ()
    {
        return created_by;
    }

    public void setCreated_by (String created_by)
    {
        this.created_by = created_by;
    }

    public int getIs_perishable ()
    {
        return is_perishable;
    }

    public void setIs_perishable (int is_perishable)
    {
        this.is_perishable = is_perishable;
    }

    public int getEnabled ()
    {
        return enabled;
    }

    public void setEnabled (int enabled)
    {
        this.enabled = enabled;
    }

    public int getAddon_free_limit ()
    {
        return addon_free_limit;
    }

    public void setAddon_free_limit (int addon_free_limit)
    {
        this.addon_free_limit = addon_free_limit;
    }

    public int getRestaurant_id ()
    {
        return restaurant_id;
    }

    public void setRestaurant_id (int restaurant_id)
    {
        this.restaurant_id = restaurant_id;
    }

    public int getCategory_id ()
    {
        return category_id;
    }

    public void setCategory_id (int category_id)
    {
        this.category_id = category_id;
    }

    public String getUpdated_at ()
    {
        return updated_at;
    }

    public void setUpdated_at (String updated_at)
    {
        this.updated_at = updated_at;
    }

    public int getPrice ()
    {
        return price;
    }

    public void setPrice (int price)
    {
        this.price = price;
    }

    public int getSub_category_id ()
    {
        return sub_category_id;
    }

    public void setSub_category_id (int sub_category_id)
    {
        this.sub_category_id = sub_category_id;
    }

    public int getIs_spicy ()
    {
        return is_spicy;
    }

    public void setIs_spicy (int is_spicy)
    {
        this.is_spicy = is_spicy;
    }

    public String getUpdated_on ()
    {
        return updated_on;
    }

    public void setUpdated_on (String updated_on)
    {
        this.updated_on = updated_on;
    }

    public String getActive ()
    {
        return active;
    }

    public void setActive (String active)
    {
        this.active = active;
    }

    public String getIs_discoverable ()
    {
        return is_discoverable;
    }

    public void setIs_discoverable (String is_discoverable)
    {
        this.is_discoverable = is_discoverable;
    }

    public String getAddon_description ()
    {
        return addon_description;
    }

    public void setAddon_description (String addon_description)
    {
        this.addon_description = addon_description;
    }

    public String getPreparation_style ()
    {
        return preparation_style;
    }

    public void setPreparation_style (String preparation_style)
    {
        this.preparation_style = preparation_style;
    }

    public ItemsCat build() {
        setDefaultValues();
        return this;
    }

    public void setDefaultValues() {
        if(this.getCategory_id() == 0)
            this.setCategory_id(MenuConstants.cat_id);
        if(this.getSub_category_id()==0)
            this.setSub_category_id(MenuConstants.subcat_id);
        if(this.getRestaurant_id()==0)
            this.setRestaurant_id(MenuConstants.restau_id);
        if(this.getIn_stock()==0)
            this.setIn_stock(1);
        if(this.getPrice()==0)
            this.setPrice(MenuConstants.price);
        if(this.getVat()==null)
            this.setVat(MenuConstants.vat);
        if(this.getService_tax()==null)
            this.setService_tax(MenuConstants.serviceTax);
        if(this.getService_charges()==null)
            this.setService_charges(MenuConstants.serviceCharge);
        if(this.getIs_perishable()==0)
            this.setIs_perishable(MenuConstants.perishable);
        if(this.getAddon_free_limit()==0)
            this.setAddon_free_limit(MenuConstants.addlimit);
        if(this.getEnabled()==0)
            this.setEnabled(1);
        if(this.getServes_how_many()==0)
            this.setServes_how_many(1);
        if(this.getIs_spicy()==0)
            this.setIs_spicy(1);
        if(this.getDescription()==null)
            this.setDescription(MenuConstants.descr);






    }


    @Override
    public String toString()
    {
        return "ClassPojo [eligible_for_long_distance = "+eligible_for_long_distance+", in_stock = "+in_stock+", is_veg = "+is_veg+", vat = "+vat+", service_charges = "+service_charges+", recommended = "+recommended+", service_tax = "+service_tax+", type = "+type+", serves_how_many = "+serves_how_many+", order = "+order+", description = "+description+", addon_limit = "+addon_limit+", name = "+name+", updated_by = "+updated_by+", packing_charges = "+packing_charges+", variant_description = "+variant_description+", packing_slab_count = "+packing_slab_count+", created_by = "+created_by+", is_perishable = "+is_perishable+", enabled = "+enabled+", addon_free_limit = "+addon_free_limit+", restaurant_id = "+restaurant_id+", category_id = "+category_id+", updated_at = "+updated_at+", price = "+price+", sub_category_id = "+sub_category_id+", is_spicy = "+is_spicy+", updated_on = "+updated_on+", active = "+active+", is_discoverable = "+is_discoverable+", addon_description = "+addon_description+", preparation_style = "+preparation_style+"]";
    }
}

