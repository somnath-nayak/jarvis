package com.swiggy.api.erp.cms.pojo.BaseServiceBulkItems;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.erp.cms.pojo.BaseServiceBulkItems
 **/
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "default",
        "default_dependent_variant",
        "external_variant_id",
        "gst_details",
        "id",
        "in_stock",
        "is_veg",
        "name",
        "order",
        "price",
        "variant_group_id"
})
public class Variation__ {

    @JsonProperty("default")
    private Integer _default;
    @JsonProperty("default_dependent_variant")
    private Default_dependent_variant default_dependent_variant;
    @JsonProperty("external_variant_id")
    private String external_variant_id;
    @JsonProperty("gst_details")
    private Gst_details gst_details;
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("in_stock")
    private Integer in_stock;
    @JsonProperty("is_veg")
    private Integer is_veg;
    @JsonProperty("name")
    private String name;
    @JsonProperty("order")
    private Integer order;
    @JsonProperty("price")
    private Integer price;
    @JsonProperty("variant_group_id")
    private Integer variant_group_id;

    @JsonProperty("default")
    public Integer getDefault() {
        return _default;
    }

    @JsonProperty("default")
    public void setDefault(Integer _default) {
        this._default = _default;
    }

    @JsonProperty("default_dependent_variant")
    public Default_dependent_variant getDefault_dependent_variant() {
        return default_dependent_variant;
    }

    @JsonProperty("default_dependent_variant")
    public void setDefault_dependent_variant(Default_dependent_variant default_dependent_variant) {
        this.default_dependent_variant = default_dependent_variant;
    }

    @JsonProperty("external_variant_id")
    public String getExternal_variant_id() {
        return external_variant_id;
    }

    @JsonProperty("external_variant_id")
    public void setExternal_variant_id(String external_variant_id) {
        this.external_variant_id = external_variant_id;
    }

    @JsonProperty("gst_details")
    public Gst_details getGst_details() {
        return gst_details;
    }

    @JsonProperty("gst_details")
    public void setGst_details(Gst_details gst_details) {
        this.gst_details = gst_details;
    }

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("in_stock")
    public Integer getIn_stock() {
        return in_stock;
    }

    @JsonProperty("in_stock")
    public void setIn_stock(Integer in_stock) {
        this.in_stock = in_stock;
    }

    @JsonProperty("is_veg")
    public Integer getIs_veg() {
        return is_veg;
    }

    @JsonProperty("is_veg")
    public void setIs_veg(Integer is_veg) {
        this.is_veg = is_veg;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("order")
    public Integer getOrder() {
        return order;
    }

    @JsonProperty("order")
    public void setOrder(Integer order) {
        this.order = order;
    }

    @JsonProperty("price")
    public Integer getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(Integer price) {
        this.price = price;
    }

    @JsonProperty("variant_group_id")
    public Integer getVariant_group_id() {
        return variant_group_id;
    }

    @JsonProperty("variant_group_id")
    public void setVariant_group_id(Integer variant_group_id) {
        this.variant_group_id = variant_group_id;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("_default", _default).append("default_dependent_variant", default_dependent_variant).append("external_variant_id", external_variant_id).append("gst_details", gst_details).append("id", id).append("in_stock", in_stock).append("is_veg", is_veg).append("name", name).append("order", order).append("price", price).append("variant_group_id", variant_group_id).toString();
    }

}

