package com.swiggy.api.erp.cms.pojo.BaseServiceCategory;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.erp.cms.pojo.BaseServiceCategory
 **/

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.swiggy.api.erp.cms.helper.BaseServiceHelper;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "active",
        "created_at",
        "created_by",
        "created_on",
        "description",
        "menu_type",
        "name",
        "order",
        "parent_category_id",
        "restaurant_id",
        "third_party_id",
        "type",
        "updated_at",
        "updated_by",
        "updated_on"
})

public class Category {
    BaseServiceHelper baseServiceHelper = new BaseServiceHelper();
    private String timestamp = baseServiceHelper.getTimeStampRandomised();

    @JsonProperty("active")
    private Boolean active;
    @JsonProperty("created_at")
    private String created_at;
    @JsonProperty("created_by")
    private String created_by;
    @JsonProperty("created_on")
    private String created_on;
    @JsonProperty("description")
    private String description;
    @JsonProperty("menu_type")
    private String menu_type;
    @JsonProperty("name")
    private String name;
    @JsonProperty("order")
    private Integer order;
    @JsonProperty("parent_category_id")
    private Integer parent_category_id;
    @JsonProperty("restaurant_id")
    private Integer restaurant_id;
    @JsonProperty("third_party_id")
    private String third_party_id;
    @JsonProperty("type")
    private String type;
    @JsonProperty("updated_at")
    private String updated_at;
    @JsonProperty("updated_by")
    private String updated_by;
    @JsonProperty("updated_on")
    private String updated_on;

    @JsonProperty("active")
    public Boolean getActive() {
        return active;
    }

    @JsonProperty("active")
    public void setActive(Boolean active) {
        this.active = active;
    }

    @JsonProperty("created_at")
    public String getCreated_at() {
        return created_at;
    }

    @JsonProperty("created_at")
    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    @JsonProperty("created_by")
    public String getCreated_by() {
        return created_by;
    }

    @JsonProperty("created_by")
    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    @JsonProperty("created_on")
    public String getCreated_on() {
        return created_on;
    }

    @JsonProperty("created_on")
    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("menu_type")
    public String getMenu_type() {
        return menu_type;
    }

    @JsonProperty("menu_type")
    public void setMenu_type(String menu_type) {
        this.menu_type = menu_type;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("order")
    public Integer getOrder() {
        return order;
    }

    @JsonProperty("order")
    public void setOrder(Integer order) {
        this.order = order;
    }

    @JsonProperty("parent_category_id")
    public Integer getParent_category_id() {
        return parent_category_id;
    }

    @JsonProperty("parent_category_id")
    public void setParent_category_id(Integer parent_category_id) {
        this.parent_category_id = parent_category_id;
    }

    @JsonProperty("restaurant_id")
    public Integer getRestaurant_id() {
        return restaurant_id;
    }

    @JsonProperty("restaurant_id")
    public void setRestaurant_id(Integer restaurant_id) {
        this.restaurant_id = restaurant_id;
    }

    @JsonProperty("third_party_id")
    public String getThird_party_id() {
        return third_party_id;
    }

    @JsonProperty("third_party_id")
    public void setThird_party_id(String third_party_id) {
        this.third_party_id = third_party_id;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("updated_at")
    public String getUpdated_at() {
        return updated_at;
    }

    @JsonProperty("updated_at")
    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    @JsonProperty("updated_by")
    public String getUpdated_by() {
        return updated_by;
    }

    @JsonProperty("updated_by")
    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    @JsonProperty("updated_on")
    public String getUpdated_on() {
        return updated_on;
    }

    @JsonProperty("updated_on")
    public void setUpdated_on(String updated_on) {
        this.updated_on = updated_on;
    }


    public void setDefaultValues(int restID, int menuId) {
        if(this.getActive() == null)
            this.setActive(true);
        if(this.getCreated_at() == null)
            this.setCreated_at("2018-04-17T13:20:53.670Z");
        if(this.getCreated_by() == null)
            this.setCreated_by("Automation_BaseService_Suite");
        if(this.getCreated_on() == null)
            this.setCreated_on("2018-04-17T13:20:53.670Z");
        if(this.getDescription() == null)
            this.setDescription("Testing Category");
        if(this.getMenu_type() == null)
            this.setMenu_type("REGULAR_MENU");
        if(this.getName() == null)
            this.setName("Automation_BaseService_"+timestamp);
        if(this.getOrder() == null)
            this.setOrder(0);
        if(this.getParent_category_id() == null)
            this.setParent_category_id(menuId);
        if(this.getRestaurant_id() == null)
            this.setRestaurant_id(restID);
        if(this.getThird_party_id() == null)
            this.setThird_party_id("Automation_"+timestamp);
        if(this.getType() == null)
            this.setType("Cat");
        if(this.getUpdated_at() == null)
            this.setUpdated_at("2018-04-17T13:20:53.670Z");
        if(this.getUpdated_by() ==  null)
            this.setUpdated_by("Automation_baseService");
        if(this.getUpdated_on() == null)
            this.setUpdated_on("2018-04-17T13:20:53.670Z");
    }

    public Category build(int restID, int menuId){
        setDefaultValues(restID,menuId);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("active", active).append("created_at", created_at).append("created_by", created_by).append("created_on", created_on).append("description", description).append("menu_type", menu_type).append("name", name).append("order", order).append("parent_category_id", parent_category_id).append("restaurant_id", restaurant_id).append("third_party_id", third_party_id).append("type", type).append("updated_at", updated_at).append("updated_by", updated_by).append("updated_on", updated_on).toString();
    }
}
