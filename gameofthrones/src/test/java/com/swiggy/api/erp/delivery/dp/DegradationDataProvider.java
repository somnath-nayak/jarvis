package com.swiggy.api.erp.delivery.dp;

import java.util.Map;

import org.testng.annotations.DataProvider;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.delivery.constants.DegradationConstant;
import com.swiggy.api.erp.delivery.helper.DegradationHelper;

import framework.gameofthrones.Aegon.SystemConfigProvider;


public class DegradationDataProvider 
{
	DegradationHelper degradationHelper = new DegradationHelper();
	
	public String[] entityZeroTWPayload = {"BLR_BTM", "0", "4"};
	
	
	@DataProvider(name="TC_GD_15")
	public Object[][] TransitionAfterTransitionWindow() throws Exception 
	{
		String[] updateEntityPayload = {"BLR_BTM", "2", "4"};
		return new Object[][] { { updateEntityPayload }};
	}
	
	
	@DataProvider(name="TC_GD_16")
	public Object[][] uniqueZindex() throws Exception 
	{
		String active_machine = JsonPath.read(degradationHelper.getActiveMachinesForEntity(DegradationConstant.ENTITY_ID).ResponseValidator.GetBodyAsText(), 
				"$..machineId").toString().replace("[", "").replace("]", "");
		
		Map<String, Object> map = SystemConfigProvider.getTemplate("degradation")
				.queryForMap("select name, from_state, to_state, z_index from edges where machine_id=" + active_machine + " limit 1");
		
		String[] addEdgeBtwnStatesPayload = {map.get("from_state").toString(), active_machine, map.get("name").toString(), 
				map.get("to_state").toString(), map.get("z_index").toString()};
		
		return new Object[][] { { addEdgeBtwnStatesPayload }};
	}
	
	
	@DataProvider(name="TC_GD_17")
	public Object[][] uniDirectionEdge() throws Exception 
	{
		String active_machine = JsonPath.read(degradationHelper.getActiveMachinesForEntity(DegradationConstant.ENTITY_ID).ResponseValidator.GetBodyAsText(), 
				"$..machineId").toString().replace("[", "").replace("]", "");
		
		Map<String, Object> map = SystemConfigProvider.getTemplate("degradation")
				.queryForMap("select name, from_state, to_state, z_index from edges where machine_id=" + active_machine + " limit 1");
		
		//reversed the from & to states of edge..
		String[] addEdgeBtwnStatesPayload = {map.get("to_state").toString(), active_machine, map.get("name").toString(), 
				map.get("from_state").toString(), map.get("z_index").toString()};
		
		return new Object[][] { { addEdgeBtwnStatesPayload }};
	}
	
	
	@DataProvider(name="TC_GD_21_22")
	public Object[][] manualTransition() throws Exception 
	{
		String[] updateEntityPayload = {"BLR_BTM", "2", "4"};
		return new Object[][] { { updateEntityPayload }};
	}
}
