package com.swiggy.api.erp.cms.pojo.BaseServiceBulkItems;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.erp.cms.pojo.BaseServiceBulkItems
 **/

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "exclude_list",
        "variant_groups"
})
public class Variants {

    @JsonProperty("exclude_list")
    private List<List<Exclude_list>> exclude_list = null;
    @JsonProperty("variant_groups")
    private List<Variant_group> variant_groups = null;

    @JsonProperty("exclude_list")
    public List<List<Exclude_list>> getExclude_list() {
        return exclude_list;
    }

    @JsonProperty("exclude_list")
    public void setExclude_list(List<List<Exclude_list>> exclude_list) {
        this.exclude_list = exclude_list;
    }

    @JsonProperty("variant_groups")
    public List<Variant_group> getVariant_groups() {
        return variant_groups;
    }

    @JsonProperty("variant_groups")
    public void setVariant_groups(List<Variant_group> variant_groups) {
        this.variant_groups = variant_groups;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("exclude_list", exclude_list).append("variant_groups", variant_groups).toString();
    }

}
