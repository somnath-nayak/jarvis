package com.swiggy.api.erp.cms.tests.CatalogV2;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.helper.CatalogV2Helper;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CreateProductCategoryWithoutRule {
    CatalogV2Helper catalogV2Helper=new CatalogV2Helper();
    String catId;
    @Test(description = "Creates product category without rules and with allowed values")
    public void createProductCategoryWithoutRule(){
        String response=catalogV2Helper.createCategoryWithoutRule().ResponseValidator.GetBodyAsText();
        catId=JsonPath.read(response,"$.id");
        Assert.assertNotNull(catId);
    }
}
