package com.swiggy.api.erp.crm.dp;

import com.swiggy.api.erp.delivery.helper.OrderStatus;
import io.advantageous.boon.core.Sys;
import org.testng.annotations.DataProvider;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import com.swiggy.api.erp.crm.constants.CRMConstants;

public class MockServiceDP{

    public ArrayList<Object[]> fetchIssueDetails(String orderType, String dispositionType) throws Exception
    {
        String ffStatus = null, disposition = null, sla = null;
        String[] deliveryStatus = null;
        String[] dispositionList = new String[0];
        ArrayList<Object[]> flowList = new ArrayList<Object[]>();
        String[] validFlows = null;
        String restaurant_type = "TAKEAWAY";

        String[] allType = null;

        if (dispositionType.equalsIgnoreCase("mockFlows")) {
            dispositionList = new String[]
                    {
                            "ChangedMyMindOrderNotPlacedNotOnTrack",
                            "ChangedMyMindOrderNotPlacedOnTrack",
                            "ChangedMyMindOrderPlacedNotOnTrack",
                            "ChangedMyMindOrderPlacedOnTrack",
                            "ChangedMyMindOrderPickedupOnTrack",
                            "ChangedMyMindOrderPickedupNotOnTrack",
                            "IncorrectAddressOrderPlacedNotOnTrack",
                            "IncorrectAddressOrderNotPlacedNotOnTrack",
                            "IncorrectAddressOrderNotPlacedOnTrack",
                            "IncorrectAddressOrderPlacedOnTrack",
                            "IncorrectAddressOrderPickedupNotOnTrack",
                            "IncorrectAddressOrderPickedupOnTrack",
                            "PlacedByMistakeOrderPlacedNotOnTrack",
                            "PlacedByMistakeOrderPlacedOnTrack",
                            "PlacedByMistakeOrderNotPlacedNotOnTrack",
                            "PlacedByMistakeOrderNotPlacedOnTrack",
                            "PlacedByMistakeOrderPickedupNotOnTrack",
                            "PlacedByMistakeOrderPickedupOnTrack",
                            "OrderDelayedOrderPlacedNotOnTrack",
                            "OrderDelayedOrderNotPlacedNotOnTrack",
                            "OrderDelayedOrderPickedupNotOnTrack",
                            "ExpectedFasterOrderPlacedOnTrack",
                            "ExpectedFasterOrderNotPlacedOnTrack",
                            "ExpectedFasterOrderPickedupOnTrack"
                    };
        } else if (dispositionType.equalsIgnoreCase("integrationFlows"))
        {
            dispositionList = new String[]{

                    "PlacedByMistakeOrderPlacedOnTrack",
                    "ChangedMyMindOrderPlacedOnTrack",
                    "IncorrectAddressOrderPlacedOnTrack",
                    "ExpectedFasterOrderPlacedOnTrack",

                    "IncorrectAddressOrderPickedupOnTrack",
                    "ChangedMyMindOrderPickedupOnTrack",
                    "PlacedByMistakeOrderPickedupOnTrack",
                    "ExpectedFasterOrderPickedupOnTrack",

                    "ChangedMyMindOrderNotPlacedOnTrack",
                    "IncorrectAddressOrderNotPlacedOnTrack",
                    "PlacedByMistakeOrderNotPlacedOnTrack",
                    "ExpectedFasterOrderNotPlacedOnTrack"
            };
        } else if (dispositionType.equalsIgnoreCase("takeAwayFlows")) {
            dispositionList = new String[]{

                    "TakeawayChangedMyMind",
                    "TakeawayUnableToPickup",
                    "TakeawayPlacedByMistake",
                    "TakeawayOrderDelayed"
            };
        } else
            System.out.println("disposition Type is not defined");

        int dispositionSize = dispositionList.length;

        for (int k=0;k<dispositionSize;k++) {

            if (dispositionList[k].equalsIgnoreCase("TakeawayChangedMyMind")) {
                validFlows = new String[]{"flow170", "flow171", "flow172", "flow173","flow174", "flow175"};
                ffStatus = "unplaced";
                sla = "onTrack";
                disposition = dispositionList[k];
                deliveryStatus = new String[]{ "assigned", "confirmed", "arrived"};
            } else if (dispositionList[k].equalsIgnoreCase("TakeawayUnableToPickup")) {
                validFlows = new String[]{"flow176", "flow177", "flow178", "flow179", "flow180", "flow181"};
                ffStatus = "unplaced";
                sla = "onTrack";
                disposition = dispositionList[k];
                deliveryStatus = new String[]{ "assigned", "confirmed", "arrived"};
            } else if (dispositionList[k].equalsIgnoreCase("TakeawayPlacedByMistake")) {
                validFlows = new String[]{"flow182", "flow183", "flow184", "flow185","flow186", "flow187"};
                ffStatus = "unplaced";
                sla = "onTrack";
                disposition = dispositionList[k];
                deliveryStatus = new String[]{ "assigned", "confirmed", "arrived"};
            } else if (dispositionList[k].equalsIgnoreCase("TakeawayOrderDelayed")) {
                validFlows = new String[]{"flow188", "flow189", "flow190", "flow191","flow192", "flow193"};
                ffStatus = "unplaced";
                sla = "onTrack";
                disposition = dispositionList[k];
                deliveryStatus = new String[]{ "assigned", "confirmed", "arrived"};
            } else if (dispositionList[k].equalsIgnoreCase("IncorrectAddressOrderPlacedOnTrack")) {
                validFlows = new String[]{"flow106", "flow107", "flow108", "flow109",
                        "flow110", "flow111", "flow112", "flow113", "flow114"};
                ffStatus = "placed";
                sla = "onTrack";
                disposition = dispositionList[k];
                deliveryStatus = new String[]{ "assigned", "confirmed", "arrived"};
            }
            else if (dispositionList[k].equalsIgnoreCase("PlacedByMistakeOrderPlacedOnTrack")) {
                validFlows = new String[]{"flow115", "flow116", "flow117", "flow118",
                        "flow119", "flow120", "flow121", "flow122"};
                ffStatus = "placed";
                sla = "onTrack";
                disposition = dispositionList[k];
                deliveryStatus = new String[]{ "assigned", "confirmed", "arrived"};
            }
            else if (dispositionList[k].equalsIgnoreCase("ExpectedFasterOrderPlacedOnTrack")) {
                validFlows = new String[]{"flow123", "flow124", "flow125", "flow126",
                        "flow127", "flow128", "flow129", "flow130"};
                ffStatus = "placed";
                sla = "onTrack";
                disposition = dispositionList[k];
                deliveryStatus = new String[]{ "assigned", "confirmed", "arrived"};
            }
            else if (dispositionList[k].equalsIgnoreCase("IncorrectAddressOrderPickedupOnTrack")) {
                validFlows = new String[]{"flow131", "flow132", "flow133", "flow134",
                        "flow135", "flow136", "flow137", "flow138"};
                ffStatus = "placed";
                sla = "onTrack";
                disposition = dispositionList[k];
                deliveryStatus = new String[]{ "pickedup"};
            }

            else if (dispositionList[k].equalsIgnoreCase("PlacedByMistakeOrderPickedupOnTrack")) {
                validFlows = new String[]{"flow139", "flow140", "flow141", "flow142",
                        "flow143", "flow144", "flow145", "flow146"};
                ffStatus = "placed";
                sla = "onTrack";
                disposition = dispositionList[k];
                deliveryStatus = new String[]{ "pickedup"};
            }

            else if (dispositionList[k].equalsIgnoreCase("ExpectedFasterOrderPickedupOnTrack")) {
                validFlows = new String[]{"flow147", "flow148", "flow149", "flow150",
                        "flow151", "flow152", "flow153", "flow154"};
                ffStatus = "placed";
                sla = "onTrack";
                disposition = dispositionList[k];
                deliveryStatus = new String[]{ "pickedup"};
            }

            else if (dispositionList[k].equalsIgnoreCase("ExpectedFasterOrderNotPlacedOnTrack")) {
                validFlows = new String[]{"flow155", "flow156", "flow157", "flow158",
                        "flow159", "flow160", "flow161"};
                ffStatus = "unplaced";
                sla = "onTrack";
                disposition = dispositionList[k];
                deliveryStatus = new String[]{ "assigned", "confirmed", "arrived"};
            }
            else if (dispositionList[k].equalsIgnoreCase("ChangedMyMindOrderNotPlacedNotOnTrack")) {
                validFlows = new String[]{"flow1", "flow2", "flow3", "flow4", "flow5", "flow6", "flow7"};
                ffStatus = "unplaced";
                sla = "notOnTrack";
                disposition = dispositionList[k];
                deliveryStatus = new String[]{ "assigned", "confirmed", "arrived"};
            }

            else if (dispositionList[k].equalsIgnoreCase("ChangedMyMindOrderNotPlacedOnTrack"))
            {
                validFlows = new String[]{"flow8", "flow9", "flow10", "flow11", "flow12", "flow13", "flow14"};
                ffStatus = "unplaced";
                sla = "onTrack";
                disposition = dispositionList[k];
                deliveryStatus = new String[]{ "assigned", "confirmed", "arrived"};
            }

            else if (dispositionList[k].equalsIgnoreCase("ChangedMyMindOrderPlacedNotOnTrack"))
            {
                validFlows = new String[]{"flow15", "flow16", "flow17", "flow18", "flow19", "flow20", "flow21"};
                ffStatus = "placed";
                sla = "notOnTrack";
                disposition = dispositionList[k];
                deliveryStatus = new String[]{"assigned", "confirmed", "arrived", "pickedup","reached"};
            }

            else if (dispositionList[k].equalsIgnoreCase("ChangedMyMindOrderPlacedOnTrack"))
            {
                validFlows = new String[]{"flow22", "flow23", "flow24", "flow25"};
                ffStatus = "placed";
                sla = "onTrack";
                disposition = dispositionList[k];
                deliveryStatus = new String[]{"assigned", "confirmed", "arrived", "pickedup","reached"};
            }

            else if (dispositionList[k].equalsIgnoreCase("IncorrectAddressOrderPlacedNotOnTrack"))
            {
                validFlows = new String[]{"flow57", "flow58", "flow59", "flow60", "flow61", "flow62", "flow63"};
                ffStatus = "placed";
                sla = "notOnTrack";
                disposition = dispositionList[k];
                deliveryStatus = new String[]{"assigned", "confirmed", "arrived", "pickedup","reached"};
            }

            else if (dispositionList[k].equalsIgnoreCase("IncorrectAddressOrderNotPlacedNotOnTrack"))
            {
                validFlows = new String[]{"flow43", "flow44", "flow45", "flow46", "flow47", "flow48", "flow49"};
                ffStatus = "unplaced";
                sla = "notOnTrack";
                disposition = dispositionList[k];
                deliveryStatus = new String[]{"assigned", "confirmed", "arrived"};
            }

            else if (dispositionList[k].equalsIgnoreCase("IncorrectAddressOrderNotPlacedOnTrack"))
            {
                validFlows = new String[]{"flow50", "flow51", "flow52", "flow53", "flow54", "flow55", "flow56"};
                ffStatus = "unplaced";
                sla = "onTrack";
                disposition = dispositionList[k];
                deliveryStatus = new String[]{"assigned", "confirmed", "arrived"};
            }

            else if (dispositionList[k].equalsIgnoreCase("PlacedByMistakeOrderPlacedNotOnTrack")) {
                validFlows = new String[]{"flow78", "flow79", "flow80", "flow81", "flow82", "flow83", "flow84"};
                ffStatus = "placed";
                sla = "notOnTrack";
                disposition = dispositionList[k];
                deliveryStatus = new String[]{"assigned", "confirmed", "arrived", "pickedup","reached"};
            }

            else if (dispositionList[k].equalsIgnoreCase("PlacedByMistakeOrderNotPlacedNotOnTrack")) {
                validFlows = new String[]{"flow64", "flow65", "flow66", "flow67", "flow68", "flow69", "flow70"};
                ffStatus = "unplaced";
                sla = "notOnTrack";
                disposition = dispositionList[k];
                deliveryStatus = new String[]{"assigned", "confirmed", "arrived"};
            }

            else if (dispositionList[k].equalsIgnoreCase("PlacedByMistakeOrderNotPlacedOnTrack")) {
                validFlows = new String[]{"flow71", "flow72", "flow73", "flow74", "flow75", "flow76", "flow77"};
                ffStatus = "unplaced";
                sla = "onTrack";
                disposition = dispositionList[k];
                deliveryStatus = new String[]{"assigned", "confirmed", "arrived"};
            }

            else if (dispositionList[k].equalsIgnoreCase("OrderDelayedOrderPlacedNotOnTrack")) {
                validFlows = new String[]{"flow92", "flow93", "flow94", "flow95", "flow96", "flow97", "flow98"};
                ffStatus = "placed";
                sla = "notOnTrack";
                disposition = dispositionList[k];
                deliveryStatus = new String[]{"assigned", "confirmed", "arrived", "pickedup","reached"};
            }

            else if (dispositionList[k].equalsIgnoreCase("OrderDelayedOrderNotPlacedNotOnTrack")) {
                validFlows = new String[]{"flow85", "flow86", "flow87", "flow88", "flow89", "flow90", "flow91"};
                ffStatus = "unplaced";
                sla = "notOnTrack";
                disposition = dispositionList[k];
                deliveryStatus = new String[]{"assigned", "confirmed", "arrived"};
            }

            else if (dispositionList[k].equalsIgnoreCase("ChangedMyMindOrderPickedupOnTrack")) {
                validFlows = new String[]{"flow100", "flow101", "flow102"};
                ffStatus = "placed";
                sla = "onTrack";
                disposition = dispositionList[k];
                deliveryStatus = new String[]{"pickedup","reached"};
            }

            else if (dispositionList[k].equalsIgnoreCase("ChangedMyMindOrderPickedupNotOnTrack")) {
                validFlows = new String[]{"flow103", "flow104", "flow105"};
                ffStatus = "placed";
                sla = "notOnTrack";
                disposition = dispositionList[k];
                deliveryStatus = new String[]{"pickedup","reached"};
            }

            int flowSize = validFlows.length;
            int statusSize = deliveryStatus.length;

            for (int i = 0; i < statusSize; i++) {
                for (int j = 0; j < flowSize; j++) {
                    HashMap<String, String> flowData = new HashMap<>();
                    flowData.put("ffStatus", ffStatus);
                    flowData.put("disposition", disposition);
                    flowData.put("orderType", orderType);
                    flowData.put("sla", sla);
                    flowData.put("deliveryStatus", deliveryStatus[i]);
                    flowData.put("restaurant_type",restaurant_type);
                    flowData.put("flow", validFlows[j]);
                    flowList.add(new Object[]{flowData});
                }
            }
        }
        return flowList;
    }

    public ArrayList<Object []> fetchCodBlockFlows(String orderType) throws Exception {
        String ffStatus = "placed", disposition = "ChangedMyMindOrderNotPlacedNotOnTrack", sla = "onTrack";
        String[] deliveryStatus = new String[] {"assigned","delivered"};
      //  String[] orders = new String[] {"12586338", "1055952078"};
        String cancellationFees[] = null;
        String[] rule_id = new String[0];
        ArrayList<Object[]> flowList = new ArrayList<Object[]>();
        String[] validFlows = null;

        String[] allType = null;

        rule_id = new String[] {
                "CheckCodEnabled",
                "CheckCodEnabledAfterPlacingOrder"
        };

        int dispositionSize = rule_id.length;

        for (int k=0;k<dispositionSize;k++) {
            String ruleId =new String();

            if (rule_id[k].equalsIgnoreCase("CheckCodEnabledAfterPlacingOrder")) {
                validFlows = new String[]{"cod_flow1", "cod_flow2", "cod_flow3"};
                ffStatus = "placed";
                sla = "OnTrack";
                deliveryStatus = new String[]{"assigned","delivered"};
                cancellationFees = new String[]{"10", "0"};
                ruleId = rule_id[k];

            }
            else if (rule_id[k].equalsIgnoreCase("CheckCodEnabled")) {
                validFlows = new String[]{"cod_flow4", "cod_flow5", "cod_flow6"};
                ffStatus = "placed";
                sla = "OnTrack";
                deliveryStatus = new String[]{"assigned","delivered"};
                cancellationFees = new String[]{"10", "0"};
                ruleId = rule_id[k];

            }
            int flowSize = validFlows.length;
            int statusSize = deliveryStatus.length;
            for(int z=0 ; z<cancellationFees.length;z++) {
                for (int i = 0; i < statusSize; i++) {
                    for (int j = 0; j < flowSize; j++) {
                        HashMap<String, String> flowData = new HashMap<>();
                        flowData.put("ffStatus", ffStatus);
                        flowData.put("disposition", disposition);
                        flowData.put("orderType", orderType);
                     //   flowData.put("order", orders[i]);
                        flowData.put("sla", sla);
                        flowData.put("deliveryStatus", deliveryStatus[i]);
                        flowData.put("cancellationFee",cancellationFees[z]);
                        flowData.put("flow", validFlows[j]);
                        System.out.println("flowData " + flowData);
                        flowList.add(new Object[]{flowData});
                    }
                }
            }
        }
        System.out.println("flowList "+flowList);
        return flowList;
    }

    public ArrayList<Object[]> editOrderFlow(String orderType) throws Exception
    {
        String ffStatus = "placed", disposition = "ChangedMyMindOrderNotPlacedNotOnTrack", sla = "onTrack";
        String[] deliveryStatus = null;
        String[] initiatorType = new String[0];
        ArrayList<Object[]> flowList = new ArrayList<Object[]>();
        String[] validFlows = null;
        String portalResponse = null,orderEditable=null,itemAdditionAllowed=null;

        initiatorType = new String[]{
                "restaurantInitiatedSuccess",
                "restaurantInitiatedClosed",
                "customerInitiatedBeforeArrivedEditPossible",
                "customerInitiatedBeforeArrivedEditNotPossible",
                "customerInitiatedBeforeArrivedItemAddNotAllowed",
                "customerInitiatedArrived"
        };

        int initiatorTypeSize = initiatorType.length;

        for (int k=0;k<initiatorTypeSize;k++)

        {
            {
                if (initiatorType[k].equalsIgnoreCase("restaurantInitiatedSuccess")) {
                    validFlows = new String[]{"edit20", "edit21","edit22"};
                    deliveryStatus = new String[]{"unassigned", "assigned", "confirmed", "arrived"};
                    portalResponse="success";
                    orderEditable="0";
                    itemAdditionAllowed="true";
                }
                else if (initiatorType[k].equalsIgnoreCase("restaurantInitiatedClosed")) {
                    validFlows = new String[]{"edit11", "edit12", "edit13", "edit14", "edit15", "edit16", "edit17", "edit18", "edit19",
                            "edit23", "edit24", "edit25", "edit26", "edit27", "edit28", "edit29"};
                    deliveryStatus = new String[]{"unassigned", "assigned", "confirmed", "arrived"};
                    portalResponse="closed";
                    orderEditable="0";
                    itemAdditionAllowed="true";
                }
                else if (initiatorType[k].equalsIgnoreCase("customerInitiatedBeforeArrivedEditPossible")) {
                    validFlows = new String[]{"edit1", "edit2", "edit3", "edit4", "edit5"};
                    deliveryStatus = new String[]{"unassigned", "assigned", "confirmed"};
                    portalResponse="success";
                    orderEditable="0";
                    itemAdditionAllowed="true";
                }
                else if (initiatorType[k].equalsIgnoreCase("customerInitiatedBeforeArrivedEditNotPossible")) {
                    validFlows = new String[]{"edit6", "edit7", "edit8"};
                    deliveryStatus = new String[]{"unassigned", "assigned", "confirmed"};
                    portalResponse="success";
                    orderEditable="1";
                    itemAdditionAllowed="true";
                }
                else if (initiatorType[k].equalsIgnoreCase("customerInitiatedBeforeArrivedItemAddNotAllowed")) {
                    validFlows = new String[]{"edit6", "edit7", "edit8"};
                    deliveryStatus = new String[]{"unassigned", "assigned", "confirmed"};
                    portalResponse="success";
                    orderEditable="0";
                    itemAdditionAllowed="false";
                }
                else if (initiatorType[k].equalsIgnoreCase("customerInitiatedArrived")) {
                    validFlows = new String[]{"edit9", "edit10"};
                    deliveryStatus = new String[]{"arrived", "pickedup", "reached"};
                    portalResponse="success";
                    orderEditable="0";
                    itemAdditionAllowed="true";
                }


                int flowSize = validFlows.length;
                int statusSize = deliveryStatus.length;

                for (int i = 0; i < statusSize; i++) {
                    for (int j = 0; j < flowSize; j++) {
                        HashMap<String, String> flowData = new HashMap<>();
                        flowData.put("ffStatus", ffStatus);
                        flowData.put("disposition", disposition);
                        flowData.put("orderType", orderType);
                        flowData.put("sla", sla);
                        flowData.put("deliveryStatus", deliveryStatus[i]);
                        flowData.put("portalResponse",portalResponse);
                        flowData.put("flow", validFlows[j]);
                        flowData.put("orderEditable",orderEditable);
                        flowData.put("itemAdditionAllowed",itemAdditionAllowed);
                        flowList.add(new Object[]{flowData});
                    }
                }
            }
        }
        return flowList;
    }

    public ArrayList<Object []> fetchPaymentAndRefundFlow(String orderType) throws Exception {
        String ffStatus="placed", disposition = "ChangedMyMindOrderNotPlacedNotOnTrack", sla = "onTrack";
        String [] deliveryStatus = null;
        String[] paymentModeArray = new String[]{ "Cash", "NONCOD"};
        ArrayList<Object[]> flowList = new ArrayList<Object[]>();
        String[] validFlows = null;
        String test = null , paymentmode = null;
        String orderId = new String();

        for (int k=0; k <  paymentModeArray.length; k++)
        {
                if (paymentModeArray[k].equalsIgnoreCase("Cash")) {
                    validFlows = new String[]{"refund1", "refund5", "refund6", "refund7", "refund11", "refund12", "refund13"};
                    paymentmode = paymentModeArray[k];
                 //   orderId = "1055960037";
                    test = "positive";
                    deliveryStatus = new String[]{OrderStatus.ASSIGNED, OrderStatus.CONFIRMED, OrderStatus.ARRIVED, OrderStatus.PICKEDUP};
                } else if (paymentModeArray[k].equalsIgnoreCase("NONCOD")) {
                    validFlows = new String[]{"refund1", "refund2", "refund3", "refund4", "refund8", "refund9", "refund10", "refund11", "refund12", "refund13"};
                    paymentmode = paymentModeArray[k];
                 //   orderId = "1055956000";
                      //  test = "positive";
                    deliveryStatus = new String[]{OrderStatus.ASSIGNED, OrderStatus.CONFIRMED, OrderStatus.ARRIVED, OrderStatus.PICKEDUP};
                }

                int flowSize = validFlows.length;
                for(int j = 0; j < deliveryStatus.length; j++) {
                    for (int i = 0; i < flowSize; i++) {
                        HashMap<String, String> flowData = new HashMap<>();
                        flowData.put("ffStatus", ffStatus);
                        flowData.put("disposition", disposition);
                        flowData.put("delivery_status", deliveryStatus[j]);
                        flowData.put("orderType", orderType);
                        flowData.put("paymentMode", paymentmode);
                    //    flowData.put("orderId",orderId);
                        flowData.put("test", test);
                        flowData.put("flow", validFlows[i]);
                        System.out.println("flowData " + flowData);
                        flowList.add(new Object[]{flowData});
                    }
                }
        }

        String [] negativeDeliveryStatus = new String[]{OrderStatus.DELIVERED,"cancelled"};
        for(int i = 0; i < negativeDeliveryStatus.length; i++){
            test = "negative";
            HashMap<String, String> flowData = new HashMap<>();
            flowData.put("ffStatus", ffStatus);
            flowData.put("test", test);
            flowData.put("disposition", disposition);
            flowData.put("delivery_status", negativeDeliveryStatus[i]);
            if(negativeDeliveryStatus[i]== "delivered")
                flowData.put("orderId","1055956012");
            else if (negativeDeliveryStatus[i] == "cancelled")
                flowData.put("orderId","1055959407");
            flowData.put("orderType", orderType);
            flowData.put("paymentMode", paymentmode);
            flowData.put("flow", "refundNegative");
            System.out.println("flowData " + flowData);
            flowList.add(new Object[]{flowData});

        }
        return flowList;
    }

    @DataProvider(name ="mockRegularFlows", parallel = false)
    public Iterator<Object[]> mockRegularFlows() throws Exception {

        ArrayList flowList = fetchIssueDetails("regular", "mockFlows");

        return flowList.iterator();
    }

    @DataProvider(name ="mockTakeAwayFlows", parallel = false)
    public Iterator<Object[]> mockTakeAwayFlows() throws Exception {
        ArrayList flowList = fetchIssueDetails("cafe", "takeAwayFlows");

        return flowList.iterator();
    }

    @DataProvider(name ="mockCodFlow", parallel = false)
    public Iterator<Object[]> mockCodFlows() throws Exception {

        ArrayList flowList = fetchCodBlockFlows("regular");

        return flowList.iterator();
    }

    @DataProvider(name ="mockPaymentAndRefundFlow", parallel = false)
    public Iterator<Object[]> mockPaymentAndRefundFlow() throws Exception {

        ArrayList flowList = fetchPaymentAndRefundFlow("regular");
//        ArrayList flowList1 = fetchPaymentAndRefundFlow("pop");
//        ArrayList flowList2 = fetchPaymentAndRefundFlow("self-delivery");

//        flowList.addAll(flowList1);
//        flowList.addAll(flowList2);

        return flowList.iterator();
    }

    @DataProvider (name = "integrationFlows", parallel = false)
    public Iterator<Object[]> integrationFlows() throws Exception {

        ArrayList flowList = fetchIssueDetails("regular", "integrationFlows");

        return flowList.iterator();
    }

    @DataProvider(name ="mockPOPFlows", parallel = false)
    public Iterator<Object[]> mockPOPFlows() throws Exception {

        ArrayList flowList = fetchIssueDetails("pop", "mockFlows");

        return flowList.iterator();
    }


    @DataProvider(name ="editFlows", parallel = false)
    public Iterator<Object[]> editFlows() throws Exception {

        ArrayList flowList = editOrderFlow("regular");
        ArrayList flowList1 = editOrderFlow("pop");
        flowList.addAll(flowList1);

        return flowList.iterator();
    }

    @DataProvider(name ="mockSelfServeFlows", parallel = false)
    public Object[][] mockSelfServeFlows() throws Exception {

        return new Object[][]{
                {"dominos1","ordered","unplaced","unassigned"},
                {"dominos2","ordered","unplaced","unassigned"},
                {"dominos3","ordered","unplaced","unassigned"},
                {"dominos1","ordered","placed","unassigned"},
                {"dominos2","ordered","placed","unassigned"},
                {"dominos3","ordered","placed","unassigned"},

        };

    }

    @DataProvider(name ="mockCafeFlows", parallel = false)
    public Object[][] mockCafeFlows() throws Exception {

        return new Object[][]{
                {"cafe1","ordered","unplaced"},
                {"cafe2","ordered","unplaced"},
                {"cafe3","ordered","unplaced"},
                {"cafe4","ordered","unplaced"},
                {"cafe5","ordered","unplaced"},
                {"cafe6","ordered","unplaced"},
                {"cafe7","ordered","unplaced"},
                {"cafe8","ordered","unplaced"},
                {"cafe9","ordered","unplaced"},
                {"cafe10","ordered","unplaced"},
                {"cafe11","delivered","placed"},
                {"cafe12","delivered","placed"},
                {"cafe13","delivered","placed"},
                {"cafe14","ordered","unplaced"},
                {"cafe15","ordered","placed"},
                {"cafe16","ordered","unplaced"}

        };
    }

    @DataProvider(name ="mockDashWorkFlows", parallel = false)
    public Iterator<Object[]> mockDashWorkFlows() throws Exception {
        ArrayList flowList = fetchDashIssueDetails("regular", "Dash");

        return flowList.iterator();
    }

    public ArrayList<Object[]> fetchDashIssueDetails(String orderType, String Type) throws Exception
    {
        String[] orderStatus = null;
        String[] dispositionList = new String[0];
        ArrayList<Object[]> flowList = new ArrayList<Object[]>();
        String[] validFlows = null;
        dispositionList = new String[]{
                "orderTrack",
                "cancelOrder",
                "orderEdit",
                "orderEditPostDE",
                "paymentAndRefund",
                "coupons",
                "billRelatedIssues",
                "orderMarkedIncorrectly",
                "orderDamage",
                "MissingOrder",
                "ModifyStore" };

    int dispositionSize = dispositionList.length;

        for (int k=0;k<dispositionSize;k++) {
            String dataType = dispositionList[k];
            switch (dataType) {
                case "orderTrack":
                    validFlows = new String[]{"DOrderTrack1", "DOrderTrack2"};
                    orderStatus = new String[]{CRMConstants.PLACED,CRMConstants.CONFIRMED,CRMConstants.PAYMENTPENDING,CRMConstants.DELIVERYCREATED,
                            CRMConstants.DELIVERYASSIGNED, CRMConstants.DELIVERYCONFIRMED, CRMConstants.DELIVERYPAY, CRMConstants.ITEMCOMFIRMATIONREQUESTED,
                            CRMConstants.ITEMCONFIRMED, CRMConstants.CARTCONFIRMATIONREQUESTED, CRMConstants.CARTCONFIRMED, CRMConstants.DELIVERYARRIVED,
                            CRMConstants.DELIVERYPICKEDUP, CRMConstants.DELIVERYREACHED};
                    break;

                case "cancelOrder":
                    validFlows = new String[]{"DorderCancel1", "DorderCancel2"};
                    orderStatus = new String[]{CRMConstants.PLACED,CRMConstants.CONFIRMED,CRMConstants.PAYMENTPENDING,CRMConstants.DELIVERYCREATED,
                            CRMConstants.DELIVERYASSIGNED, CRMConstants.DELIVERYCONFIRMED, CRMConstants.DELIVERYPAY, CRMConstants.ITEMCOMFIRMATIONREQUESTED,
                            CRMConstants.ITEMCONFIRMED, CRMConstants.CARTCONFIRMATIONREQUESTED, CRMConstants.CARTCONFIRMED, CRMConstants.DELIVERYARRIVED,
                            CRMConstants.DELIVERYPICKEDUP, CRMConstants.DELIVERYREACHED};
                    break;

                case "orderEdit":
                    validFlows = new String[]{"DOEdit1", "DOEdit2"};
                    orderStatus = new String[]{CRMConstants.PLACED,CRMConstants.CONFIRMED,CRMConstants.PAYMENTPENDING,CRMConstants.DELIVERYCREATED,
                            CRMConstants.DELIVERYASSIGNED, CRMConstants.DELIVERYCONFIRMED, CRMConstants.DELIVERYPAY, CRMConstants.ITEMCOMFIRMATIONREQUESTED,
                            CRMConstants.ITEMCONFIRMED, CRMConstants.CARTCONFIRMATIONREQUESTED, CRMConstants.CARTCONFIRMED};
                    break;

                case "orderEditPostDE":
                    validFlows = new String[]{"DOEditPostDE1", "DOEditPostDE2"};
                    orderStatus = new String[]{CRMConstants.DELIVERYARRIVED, CRMConstants.DELIVERYPICKEDUP, CRMConstants.DELIVERYREACHED};
                    break;

                case "paymentAndRefund":
                    validFlows = new String[]{"DOPayments1", "DOPayments2"};
                    orderStatus = new String[]{CRMConstants.PLACED,CRMConstants.CONFIRMED,CRMConstants.PAYMENTPENDING,CRMConstants.DELIVERYCREATED,
                            CRMConstants.DELIVERYASSIGNED, CRMConstants.DELIVERYCONFIRMED, CRMConstants.DELIVERYPAY, CRMConstants.ITEMCOMFIRMATIONREQUESTED,
                            CRMConstants.ITEMCONFIRMED, CRMConstants.CARTCONFIRMATIONREQUESTED, CRMConstants.CARTCONFIRMED, CRMConstants.DELIVERYARRIVED,
                            CRMConstants.DELIVERYPICKEDUP, CRMConstants.DELIVERYREACHED, CRMConstants.DELIVERYDELIVERED, CRMConstants.CANCELLIED,
                            CRMConstants.FAILED};
                    break;

                case "coupons":
                    validFlows = new String[]{"DOCoupons1", "DOCoupons2"};
                    orderStatus = new String[]{CRMConstants.PLACED,CRMConstants.CONFIRMED,CRMConstants.PAYMENTPENDING,CRMConstants.DELIVERYCREATED,
                            CRMConstants.DELIVERYASSIGNED, CRMConstants.DELIVERYCONFIRMED, CRMConstants.DELIVERYPAY, CRMConstants.ITEMCOMFIRMATIONREQUESTED,
                            CRMConstants.ITEMCONFIRMED, CRMConstants.CARTCONFIRMATIONREQUESTED, CRMConstants.CARTCONFIRMED, CRMConstants.DELIVERYARRIVED,
                            CRMConstants.DELIVERYPICKEDUP, CRMConstants.DELIVERYREACHED, CRMConstants.DELIVERYDELIVERED, CRMConstants.CANCELLIED,
                            CRMConstants.FAILED};
                    break;

                case "billRelatedIssues":
                    validFlows = new String[]{"DOBill1", "DOBill2"};
                    orderStatus = new String[]{CRMConstants.DELIVERYREACHED, CRMConstants.DELIVERYDELIVERED, CRMConstants.CANCELLIED, CRMConstants.FAILED};
                    break;

                case "orderMarkedIncorrectly":
                    validFlows = new String[]{CRMConstants.DELIVERYDELIVERED};
                    break;

                case "orderDamage":
                    validFlows = new String[]{"DOdamage1", "DOdamage2"};
                    orderStatus = new String[]{CRMConstants.DELIVERYDELIVERED};
                    break;

                case "MissingOrder":
                    validFlows = new String[]{"DOmissing1", "DOmissing2"};
                    orderStatus = new String[]{CRMConstants.DELIVERYDELIVERED};
                    break;

                case "ModifyStore":
                    validFlows = new String[]{"DOModifyStore1", "DOModifyStore2"};
                    orderStatus = new String[]{CRMConstants.PLACED,CRMConstants.CONFIRMED,CRMConstants.PAYMENTPENDING,CRMConstants.DELIVERYCREATED,
                            CRMConstants.DELIVERYASSIGNED, CRMConstants.DELIVERYCONFIRMED, CRMConstants.DELIVERYPAY, CRMConstants.ITEMCOMFIRMATIONREQUESTED,
                            CRMConstants.ITEMCONFIRMED, CRMConstants.CARTCONFIRMATIONREQUESTED, CRMConstants.CARTCONFIRMED, CRMConstants.DELIVERYARRIVED,
                            CRMConstants.DELIVERYPICKEDUP, CRMConstants.DELIVERYREACHED};
                    break;
            }
                for (int i = 0; i < orderStatus.length; i++) {
                    for (int j = 0; j < validFlows.length; j++) {
                        HashMap<String, String> flowData = new HashMap<>();;
                        flowData.put("orderType", orderType);
                        flowData.put("orderStatus", orderStatus[i]);
                        flowData.put("flow", validFlows[j]);
                        flowList.add(new Object[]{flowData});
                    }
                }
        }
        return flowList;
    }

    @DataProvider(name ="deliveryInstructionFlows", parallel = false)
    public Iterator<Object[]> deliveryInstructionFlows() throws Exception {

        ArrayList flowList = fetchDeliveryInstructionFlows("regular");

        return flowList.iterator();
    }

    public ArrayList<Object []> fetchDeliveryInstructionFlows(String orderType) throws Exception {
        String ffStatus="placed", sla = "onTrack";
        String [] deliveryStatus = null;
        String[] paymentModeArray = new String[]{ "Cash", "NONCOD"};
        String tests[] = new String[] {"positive", "negative"};
        String test = new String();
        ArrayList<Object[]> flowList = new ArrayList<Object[]>();
        String[] validFlows = null;
        String paymentmode = null;
        String orderId=null;


        for (int k = 0; k < paymentModeArray.length; k++) {
            if (paymentModeArray[k].equalsIgnoreCase("Cash")) {
                validFlows = new String[]{"DEInstr07", "DEInstr08", "DEInstr09", "DEInstr10", "DEInstr11", "DEInstr12", "DEInstr13", "DEInstr14"};
                paymentmode = paymentModeArray[k];
                test = "positive";
                orderId = "1128232750";
                deliveryStatus = new String[]{OrderStatus.ASSIGNED, OrderStatus.CONFIRMED, OrderStatus.ARRIVED, OrderStatus.PICKEDUP};
            } else if (paymentModeArray[k].equalsIgnoreCase("NONCOD")) {
                validFlows = new String[]{"DEInstr01", "DEInstr02", "DEInstr03", "DEInstr04", "DEInstr05", "DEInstr06", "DEInstr07", "DEInstr08",
                        "DEInstr09", "DEInstr10", "DEInstr11", "DEInstr12", "DEInstr13", "DEInstr14"};
                paymentmode = paymentModeArray[k];
                deliveryStatus = new String[]{OrderStatus.ASSIGNED, OrderStatus.CONFIRMED, OrderStatus.ARRIVED, OrderStatus.PICKEDUP};
                orderId = "1128232843";
                test = "positive";
            }

            int flowSize = validFlows.length;
            for (int j = 0; j < deliveryStatus.length; j++) {
                for (int i = 0; i < flowSize; i++) {
                    HashMap<String, String> flowData = new HashMap<>();
                    flowData.put("ffStatus", ffStatus);
                    flowData.put("delivery_status", deliveryStatus[j]);
                    flowData.put("orderType", orderType);
                    flowData.put("paymentMode", paymentmode);
                    flowData.put("test", test);
                    flowData.put("orderId", orderId);
                    flowData.put("flow", validFlows[i]);
                    System.out.println("flowData " + flowData);
                    flowList.add(new Object[]{flowData});
                }
            }
        }
        HashMap<String, String> flowData = new HashMap<>();
        flowData.put("ffStatus", ffStatus);
        flowData.put("delivery_status", OrderStatus.DELIVERED);
        flowData.put("orderType", "regular");
        flowData.put("paymentMode", "NONCOD");
        flowData.put("test", "negative");
        flowData.put("orderId", "1128229581");
        flowData.put("flow", "DEInstrNegative");
        System.out.println("flowData " + flowData);
        flowList.add(new Object[]{flowData});
        return flowList;
    }

    public ArrayList<Object []> fetchTrackingScreenTestDataOrderTypes() throws Exception {
        ArrayList<Object[]> flowList = new ArrayList<Object[]>();
        String ffStatus = "placed", sla = "onTrack";
        String[] deliveryStatus = new String[]{OrderStatus.ASSIGNED, OrderStatus.CONFIRMED, OrderStatus.ARRIVED, OrderStatus.PICKEDUP,
                OrderStatus.REACHED, OrderStatus.DELIVERED};
        String[] orderTypes = new String[]{"manual", "pop", "dominos"};
        String order = null;
        for (int k = 0; k < orderTypes.length; k++) {
            for(int i = 0; i< deliveryStatus.length; i++) {
                order = orderTypes[k];
                HashMap<String, String> flowData = new HashMap<>();
                flowData.put("ffStatus", ffStatus);
                flowData.put("orderType", order);
                flowData.put("paymentMode", "COD");
                flowData.put("deliveryStatus", deliveryStatus[i]);
                System.out.println("flowData " + flowData);
                flowList.add(new Object[]{flowData});
            }
        }
        return flowList;
    }

    @DataProvider(name ="fetchTrackingScreenData", parallel = false)
    public Iterator<Object[]> fetchTrackingScreenData() throws Exception {

        ArrayList flowList = fetchTrackingScreenTestDataOrderTypes();
        System.out.println("flowList "+flowList);
        return flowList.iterator();
    }

    public ArrayList<Object []> fetchValidWorkflowsMultiTenant() {
        ArrayList<Object[]> flowList = new ArrayList<Object[]>();
        String ffStatus = "placed", sla = "onTrack";
        String deliveryStatus = OrderStatus.ASSIGNED;
        String[] workflowsType = new String[]{CRMConstants.ORDERISSUETYPE, CRMConstants.GENERALISSUETYPE, CRMConstants.LEGALISSUETYPE,
                CRMConstants.SUPERFAQISSUETYPE, CRMConstants.FAQISSUETYPE, CRMConstants.DASHORDERISSUETYPE};
        String workFlowType = null;
        String[] validFlows = null;
        for (int k = 0; k < workflowsType.length; k++) {
            switch (workflowsType[k]) {
                case CRMConstants.ORDERISSUETYPE:
                    validFlows = new String[]{"orderWorkflow", "deliveredOrderWorkflow"};
                    workFlowType = CRMConstants.ORDERISSUETYPE;
                    break;
                case CRMConstants.GENERALISSUETYPE:
                    validFlows = new String[]{"generalWorkflow"};
                    workFlowType = CRMConstants.GENERALISSUETYPE;
                    break;
                case CRMConstants.LEGALISSUETYPE:
                    validFlows = new String[]{"legalWorkflow"};
                    workFlowType = CRMConstants.LEGALISSUETYPE;
                    break;
                case CRMConstants.SUPERFAQISSUETYPE:
                    validFlows = new String[]{"superfaqWorkflow"};
                    workFlowType = CRMConstants.SUPERFAQISSUETYPE;
                    break;
                case CRMConstants.FAQISSUETYPE:
                    validFlows = new String[]{"faqWorkflow"};
                    workFlowType = CRMConstants.FAQISSUETYPE;
                    break;
                case CRMConstants.DASHORDERISSUETYPE:
                    validFlows = new String[]{""};
                    workFlowType = CRMConstants.DASHORDERISSUETYPE;
                    break;
            }
            HashMap<String, String> flowData = new HashMap<>();
            for(int i=0; i<validFlows.length; i++){
                if(validFlows[i] == "deliveredOrderWorkflow")
                    deliveryStatus = OrderStatus.DELIVERED;
                else
                    deliveryStatus = OrderStatus.ASSIGNED;
                flowData.put("ffStatus", ffStatus);
                flowData.put("delivery_status", deliveryStatus);
                flowData.put("workflow_type", workFlowType);
                flowData.put("flow", validFlows[i]);
                System.out.println("flowData " + flowData);
            }
            flowList.add(new Object[]{flowData});
        }

        return flowList;
    }

    @DataProvider(name ="fetchWorFlowsMT", parallel = false)
    public Iterator<Object[]> fetchWorFlowsMT() throws Exception {
        ArrayList flowList = fetchValidWorkflowsMultiTenant();
        System.out.println("flowList "+flowList);
        return flowList.iterator();
    }

    @DataProvider(name ="dailyPreOrderWorkFlows", parallel = false)
    public Iterator<Object[]> dailyPreOrderWorkFlows() throws Exception {
        ArrayList flowList = fetchDailyFlowsDetails("preorder");
        System.out.println("flowList "+flowList);
        return flowList.iterator();
    }

    @DataProvider(name ="dailySubMealWorkFlows", parallel = false)
    public Iterator<Object[]> dailySubMealWorkFlows() throws Exception {
        ArrayList flowList = fetchDailyFlowsDetails("subMeal");
        System.out.println("flowList "+flowList);
        return flowList.iterator();
    }

    public ArrayList<Object[]> fetchDailyFlowsDetails(String orderType) throws Exception
    {
        String[] orderStatus = null;
        String[] dispositionList = new String[0];
        ArrayList<Object[]> flowList = new ArrayList<Object[]>();
        String[] validFlows = null;

            dispositionList = new String[]{
                    "orderTrack",
                    "cancelOrder",
                    "changeDeliverySlot",
                    "skipMyMeal",
                    "addModify",
                    "changeOrSwapMeal",
                    "foodPreInstruction",
                    "deliveryInstruction",
                    "paymentAndRefund",
                    "orderMarkedIncorrectly",
                    "MissingOrder",
                    "incorrectMealProvided",
                    "badQualityFood",
                    "mealPackagingIssue",
                    "myReasonNotListing",
                    "orderTrackPostCut"};

        int dispositionSize = dispositionList.length;

        for (int k=0;k<dispositionSize;k++) {
            String dataType = dispositionList[k];
            switch (dataType) {
                case "orderTrackPreOrder":
                    if(orderType == "preorder")
                         validFlows = new String[]{"OTPreOrder"};
                    else if(orderType == "subMeal")
                        validFlows = new String[]{"OTSubMeal"};
                    orderStatus = new String[]{CRMConstants.POSTSCHEDULECUTOFF, CRMConstants.CONFIRMED, CRMConstants.DELIVERYCREATED,
                            CRMConstants.DELIVERYASSIGNED, CRMConstants.DELIVERYCONFIRMED, CRMConstants.DELIVERYPAY, CRMConstants.DELIVERYARRIVED,
                            CRMConstants.DELIVERYUNASSIGN, CRMConstants.DELIVERYPICKEDUP, CRMConstants.DELIVERYREACHED,
                            CRMConstants.POSTDELIVERYSLOT};
                    break;

                case "cancelOrder":
                    if(orderType == "preorder")
                        validFlows = new String[]{"cancelPreOrder1", "cancelPreOrder2","cancelPreOrder3","cancelPreOrder4",
                        "cancelPreOrder5","cancelPreOrder6","cancelPreOrder7","cancelPreOrder8","cancelPreOrder9","cancelPreOrder10",
                        "cancelPreOrder11","cancelPreOrder12","cancelPreOrder13","cancelPreOrder14","cancelPreOrder15","cancelPreOrder16"};
                    else if(orderType == "subMeal")
                        validFlows = new String[]{"cancelSubMeal1","cancelSubMeal2","cancelSubMeal3","cancelSubMeal4","cancelSubMeal5",
                        "cancelSubMeal6","cancelSubMeal7","cancelPreOrder8","cancelPreOrder9","cancelPreOrder10","cancelPreOrder11",
                        "cancelPreOrder12","cancelPreOrder13","cancelPreOrder14","cancelPreOrder15","cancelPreOrder16"};
                    orderStatus = new String[]{CRMConstants.POSTSCHEDULECUTOFF, CRMConstants.CONFIRMED, CRMConstants.DELIVERYCREATED,
                            CRMConstants.DELIVERYASSIGNED, CRMConstants.DELIVERYCONFIRMED, CRMConstants.DELIVERYPAY, CRMConstants.DELIVERYARRIVED,
                            CRMConstants.DELIVERYUNASSIGN, CRMConstants.DELIVERYPICKEDUP, CRMConstants.DELIVERYREACHED,
                            CRMConstants.POSTDELIVERYSLOT};
                    break;

                case "changeDeliverySlot":
                    if(orderType == "preorder")
                        validFlows = new String[]{"changeDeliveryPreOrder"};
                    else if(orderType == "subMeal")
                        validFlows = new String[]{"changeDeliverySubMeal"};
                    orderStatus = new String[]{CRMConstants.PRESCHEDULECUTOFF};
                    break;

                case "skipMyMeal":
                    if(orderType == "preorder")
                        validFlows = new String[]{"skipOrderPreOrder"};
                    else if(orderType == "subMeal")
                        validFlows = new String[]{"skipOrderSubMeal"};
                    orderStatus = new String[]{CRMConstants.PRESCHEDULECUTOFF};
                    break;

                case "addModify": if(orderType == "preorder")
                    validFlows = new String[]{"addModifyPreOrder"};
                else if(orderType == "subMeal")
                    validFlows = new String[]{"addModifySubMeal"};
                    orderStatus = new String[]{CRMConstants.PRESCHEDULECUTOFF, CRMConstants.POSTSCHEDULECUTOFF, CRMConstants.CONFIRMED,
                            CRMConstants.DELIVERYCREATED, CRMConstants.DELIVERYASSIGNED, CRMConstants.DELIVERYCONFIRMED, CRMConstants.DELIVERYPAY,
                            CRMConstants.DELIVERYARRIVED, CRMConstants.DELIVERYUNASSIGN};
                    break;

                case "changeOrSwapMeal":
                    if(orderType == "preorder")
                        validFlows = new String[]{"changeSwapPreOrder"};
                    else if(orderType == "subMeal")
                        validFlows = new String[]{"changeSwapSubMeal"};
                    orderStatus = new String[]{CRMConstants.PRESCHEDULECUTOFF};
                    break;

                case "foodPreInstruction":
                    if(orderType == "preorder")
                        validFlows = new String[]{"foodInstPreOrder"};
                    else if(orderType == "subMeal")
                        validFlows = new String[]{"foodInstSubMeal"};
                    orderStatus = new String[]{CRMConstants.PRESCHEDULECUTOFF, CRMConstants.POSTSCHEDULECUTOFF, CRMConstants.CONFIRMED,
                            CRMConstants.DELIVERYCREATED, CRMConstants.DELIVERYASSIGNED, CRMConstants.DELIVERYCONFIRMED,
                            CRMConstants.DELIVERYPAY, CRMConstants.DELIVERYARRIVED, CRMConstants.DELIVERYUNASSIGN };
                    break;

                case "deliveryInstruction":
                    if(orderType == "preorder")
                        validFlows = new String[]{"deInstrPreOrder"};
                    else if(orderType == "subMeal")
                        validFlows = new String[]{"deInstrSubMeal"};
                    orderStatus = new String[]{CRMConstants.PRESCHEDULECUTOFF, CRMConstants.POSTSCHEDULECUTOFF, CRMConstants.CONFIRMED,
                            CRMConstants.DELIVERYCREATED, CRMConstants.DELIVERYASSIGNED, CRMConstants.DELIVERYCONFIRMED,
                            CRMConstants.DELIVERYPAY, CRMConstants.DELIVERYARRIVED, CRMConstants.DELIVERYUNASSIGN,
                            CRMConstants.DELIVERYPICKEDUP, CRMConstants.DELIVERYREACHED};
                    break;

                case "paymentAndRefund":
                    if(orderType == "preorder")
                        validFlows = new String[]{"paymentPreOrder"};
                    else if(orderType == "subMeal")
                        validFlows = new String[]{"paymentSubMeal"};
                    orderStatus = new String[]{CRMConstants.PRESCHEDULECUTOFF, CRMConstants.POSTSCHEDULECUTOFF, CRMConstants.CONFIRMED,
                            CRMConstants.DELIVERYCREATED, CRMConstants.DELIVERYASSIGNED, CRMConstants.DELIVERYCONFIRMED,
                            CRMConstants.DELIVERYPAY, CRMConstants.DELIVERYARRIVED, CRMConstants.DELIVERYUNASSIGN,
                            CRMConstants.DELIVERYPICKEDUP, CRMConstants.DELIVERYREACHED, CRMConstants.DELIVERYDELIVERED,
                            CRMConstants.CANCELLIED, CRMConstants.FAILED};
                    break;

                case "orderMarkedIncorrectly":
                    if(orderType == "preorder")
                        validFlows = new String[]{"markIncorrectPreOrder"};
                    else if(orderType == "subMeal")
                        validFlows = new String[]{"markIncorrectSubMeal"};
                    orderStatus = new String[]{CRMConstants.DELIVERYDELIVERED};
                    break;

                case "MissingOrder":
                    if(orderType == "preorder")
                        validFlows = new String[]{"missingItemPreOrder"};
                    else if(orderType == "subMeal")
                        validFlows = new String[]{"MissingItemSubMeal"};
                    orderStatus = new String[]{CRMConstants.DELIVERYDELIVERED};
                    break;

                case "incorrectMealProvided":
                    if(orderType == "preorder")
                        validFlows = new String[]{"IncorectMealPreOrder"};
                    else if(orderType == "subMeal")
                        validFlows = new String[]{"IncorrectSubMeal"};
                    orderStatus = new String[]{CRMConstants.DELIVERYDELIVERED };
                    break;

                case "badQualityFood":
                    if(orderType == "preorder")
                        validFlows = new String[]{"BadQualityPreOrder"};
                    else if(orderType == "subMeal")
                        validFlows = new String[]{"BadQualitySubMeal"};
                    orderStatus = new String[]{CRMConstants.DELIVERYDELIVERED };
                    break;

                case "mealPackagingIssue":
                    if(orderType == "preorder")
                        validFlows = new String[]{"packagingPreOrder"};
                    else if(orderType == "subMeal")
                        validFlows = new String[]{"packingSubMeal"};
                    orderStatus = new String[]{CRMConstants.DELIVERYDELIVERED };
                    break;

                case "myReasonNotListing":
                    if(orderType == "preorder")
                        validFlows = new String[]{"reasonNotPreMeal"};
                    else if(orderType == "subMeal")
                        validFlows = new String[]{"reasonNotSubMeal"};
                    orderStatus = new String[]{CRMConstants.PRESCHEDULECUTOFF, CRMConstants.POSTSCHEDULECUTOFF, CRMConstants.CONFIRMED,
                            CRMConstants.DELIVERYCREATED, CRMConstants.DELIVERYASSIGNED, CRMConstants.DELIVERYCONFIRMED,
                            CRMConstants.DELIVERYPAY, CRMConstants.DELIVERYARRIVED, CRMConstants.DELIVERYUNASSIGN,
                            CRMConstants.DELIVERYPICKEDUP, CRMConstants.DELIVERYREACHED, CRMConstants.DELIVERYDELIVERED,
                            CRMConstants.CANCELLIED, CRMConstants.FAILED};
                    break;

                case "orderTrackPostCut":
                    if(orderType == "preorder")
                        validFlows = new String[]{"otPostCutPreOrder"};
                    else if(orderType == "subMeal")
                        validFlows = new String[]{"otPostCutSubMeal"};
                    orderStatus = new String[]{CRMConstants.POSTDELIVERYSLOT};
                    break;
            }
            for (int i = 0; i < orderStatus.length; i++) {
                for (int j = 0; j < validFlows.length; j++) {
                    HashMap<String, String> flowData = new HashMap<>();;
                    flowData.put("orderStatus", orderStatus[i]);
                    flowData.put("flow", validFlows[j]);
                    flowList.add(new Object[]{flowData});
                }
            }
        }
        return flowList;
    }



}
