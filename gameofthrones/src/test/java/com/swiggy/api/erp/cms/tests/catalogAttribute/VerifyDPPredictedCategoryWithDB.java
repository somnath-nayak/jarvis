package com.swiggy.api.erp.cms.tests.catalogAttribute;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.dp.itemDP;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Tyrion.RabbitMQHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.*;

public class VerifyDPPredictedCategoryWithDB extends itemDP {
    Initialize gameofthrones = new Initialize();
    RabbitMQHelper helper=new RabbitMQHelper();
    CMSHelper cmsHelper= new CMSHelper();
    SqlTemplate sqlTemplateCI = SystemConfigProvider.getTemplate("cms");

    String itemName;
    int itmId;
    String predCategory;
    @Test(dataProvider="createitempj",description = "Create Item API")
    public void createItem(String name){
        String response=cmsHelper.createItem(name).ResponseValidator.GetBodyAsText();
        itemName=JsonPath.read(response,"$.data.name");
        itmId=JsonPath.read(response,"$.data.id");
        Assert.assertNotNull(itemName, "Item Name is Null");

    }

    @Test(dependsOnMethods = "createItem",description = "Verify category got from db is matching with dp api")
    public void predCategory() throws InterruptedException{
        String response=cmsHelper.dpAPICategory(itemName).ResponseValidator.GetBodyAsText();
        Double predScore=JsonPath.read(response,"$.result.prediction_score");
        predCategory=JsonPath.read(response,"$.result.predicted_category");
        //Double predScore=((BigDecimal)JsonPath.read(response,"$.result.prediction_score")).doubleValue();
        if(predScore>=0.7){
            sqlTemplateCI = SystemConfigProvider.getTemplate("cms");
            //List<Map<String, Object>> entitytagId = sqlTemplateCI.queryForList("select tag_id from `entity_tag_map` where `entity_id`="+itmId);
            Thread.sleep(5000);
            //System.out.println(entitytagId);
            List<Map<String, Object>> predCatName = sqlTemplateCI.queryForList("select tc.name as cat_name ,tn.name as tag_name from tags_category tc, tags_new tn, entity_tag_map etm where tn.`id`=etm.`tag_id`and etm.entity_id="+itmId+" and tn.parent=tc.id");
            HashMap<String,String> hm=new HashMap<>();
          for (int i = 0; i < predCatName.size(); i++) {
              String catName=predCatName.get(i).get("cat_name").toString();

                String tagName=predCatName.get(i).get("tag_name").toString();
                hm.put(catName,tagName);
            }

            String expCategory=(hm.get("category"));
          Assert.assertEquals(expCategory,predCategory);

        }

        else{
            Assert.fail("Prediction score/Confidence for item is less than 0.7");
        }

    }
}
