package com.swiggy.api.erp.vms.editservice.pojo;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"group_id",
"choices"
})
public class Addon {

@JsonProperty("group_id")
private long group_id;
@JsonProperty("choices")
private List<Long> choices = null;

/**
* No args constructor for use in serialization
* 
*/
public Addon() {
}

/**
* 
* @param choices
* @param group_id
*/
public Addon(long group_id, List<Long> choices) {
super();
this.group_id = group_id;
this.choices = choices;
}

@JsonProperty("group_id")
public long getGroup_id() {
return group_id;
}

@JsonProperty("group_id")
public void setGroup_id(long group_id) {
this.group_id = group_id;
}

public Addon withGroup_id(long group_id) {
this.group_id = group_id;
return this;
}

@JsonProperty("choices")
public List<Long> getChoices() {
return choices;
}

@JsonProperty("choices")
public void setChoices(List<Long> choices) {
this.choices = choices;
}

public Addon withChoices(List<Long> choices) {
this.choices = choices;
return this;
}

@Override
public String toString() {
return new ToStringBuilder(this).append("group_id", group_id).append("choices", choices).toString();
}

@Override
public int hashCode() {
return new HashCodeBuilder().append(choices).append(group_id).toHashCode();
}

@Override
public boolean equals(Object other) {
if (other == this) {
return true;
}
if ((other instanceof Addon) == false) {
return false;
}
Addon rhs = ((Addon) other);
return new EqualsBuilder().append(choices, rhs.choices).append(group_id, rhs.group_id).isEquals();
}

}