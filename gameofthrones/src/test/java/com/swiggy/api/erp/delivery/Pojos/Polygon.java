package com.swiggy.api.erp.delivery.Pojos;

import java.util.List;

public class Polygon
{
    private List<Coordinate> coordinates;

    public List<Coordinate> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(List<Coordinate> coordinates) {
        this.coordinates = coordinates;
    }
}
