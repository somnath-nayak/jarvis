package com.swiggy.api.erp.cms.dp;

import com.swiggy.api.erp.cms.constants.MenuConstants;
import com.swiggy.api.erp.cms.pojo.BulkSubCategory.BulkSubCategory;
import com.swiggy.api.erp.cms.pojo.BulkSubCategory.Entities;
import com.swiggy.api.erp.cms.pojo.SubCategory.Entity;
import com.swiggy.api.erp.cms.pojo.SubCategory.SubCategory;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by kiran.j on 2/19/18.
 */
public class SubCategoryDp {


    @DataProvider(name = "subcategory")
    public Iterator<Object[]> subcategory() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        SubCategory subCategory = new SubCategory();
        subCategory.build();
        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(subCategory), new String[]{}, MenuConstants.token_id2});

        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(subCategory), new String[]{}, MenuConstants.token_id});

        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(subCategory), new String[]{"401"}, "123"});

        obj.add(new Object[]{"", jsonHelper.getObjectToJSONallowNullValues(subCategory), new String[]{"404"}, MenuConstants.token_id});

        Entity entity = subCategory.getEntity();
        entity.setId(null);
        subCategory.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(subCategory), MenuConstants.addsubcatid, MenuConstants.token_id});

        entity = subCategory.getEntity();
        entity.build();
        entity.setOrder(0);
        subCategory.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(subCategory), new String[]{}, MenuConstants.token_id});

        entity = subCategory.getEntity();
        entity.build();
        entity.setOrder(1);
        subCategory.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(subCategory), new String[]{}, MenuConstants.token_id});


        entity = subCategory.getEntity();
        entity.build();
        entity.setEnable(1);
        subCategory.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(subCategory), new String[]{}, MenuConstants.token_id});

        entity = subCategory.getEntity();
        entity.build();
        entity.setEnable(0);
        subCategory.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(subCategory), new String[]{}, MenuConstants.token_id});

        entity = subCategory.getEntity();
        entity.build();
        entity.setName("");
        subCategory.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(subCategory), MenuConstants.addsubcatname, MenuConstants.token_id});

        entity = subCategory.getEntity();
        entity.build();
        entity.setId(null);
        subCategory.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(subCategory), MenuConstants.addsubcatid, MenuConstants.token_id});

        entity = subCategory.getEntity();
        entity.build();
        entity.setName(null);
        subCategory.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(subCategory), MenuConstants.addsubcatname, MenuConstants.token_id});

        return obj.iterator();
    }


    @DataProvider(name = "updatesubcategory")
    public Iterator<Object[]> updatesubcategory() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();
        SubCategory mainCategory = new SubCategory();
        mainCategory.build();

        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(mainCategory), new String[]{}, MenuConstants.token_id, mainCategory.getEntity().getId()});

        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(mainCategory), new String[]{"401"}, "123", mainCategory.getEntity().getId()});

        obj.add(new Object[]{"", jsonHelper.getObjectToJSONallowNullValues(mainCategory), new String[]{"404"}, MenuConstants.token_id, mainCategory.getEntity().getId()});

        Entity entity = mainCategory.getEntity();
        entity.setId(null);
        mainCategory.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(mainCategory), MenuConstants.addsubcatid_update, MenuConstants.token_id, MenuConstants.sub_category_id});

        entity = mainCategory.getEntity();
        entity.build();
        entity.setOrder(0);
        mainCategory.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(mainCategory), new String[]{}, MenuConstants.token_id, MenuConstants.sub_category_id});

        entity = mainCategory.getEntity();
        entity.build();
        entity.setOrder(1);
        mainCategory.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(mainCategory), new String[]{}, MenuConstants.token_id, MenuConstants.sub_category_id});


        entity = mainCategory.getEntity();
        entity.build();
        entity.setEnable(0);
        mainCategory.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(mainCategory), new String[]{}, MenuConstants.token_id, MenuConstants.sub_category_id});

        entity = mainCategory.getEntity();
        entity.build();
        entity.setEnable(1);
        mainCategory.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(mainCategory), new String[]{}, MenuConstants.token_id, MenuConstants.sub_category_id});

        entity = mainCategory.getEntity();
        entity.build();
        entity.setName("");
        mainCategory.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSONallowNullValues(mainCategory), MenuConstants.addsubcatname_update, MenuConstants.token_id, mainCategory.getEntity().getId()});

        entity = mainCategory.getEntity();
        entity.build();
        entity.setId(null);
        mainCategory.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(mainCategory), MenuConstants.addsubcatid_update, MenuConstants.token_id, MenuConstants.main_category_id});

        entity = mainCategory.getEntity();
        entity.build();
        entity.setName(null);
        mainCategory.setEntity(entity);
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(mainCategory), new String[]{}, MenuConstants.token_id, mainCategory.getEntity().getId()});

        return obj.iterator();
    }

    @DataProvider(name = "createbulksubcategory")
    public Iterator<Object[]> createbulksubcategory() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();

        BulkSubCategory bulkSubCategory = new BulkSubCategory();
        bulkSubCategory.build();
        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(bulkSubCategory), new String[]{}, MenuConstants.token_id2});

        obj.add(new Object[]{"", jsonHelper.getObjectToJSON(bulkSubCategory), new String[]{"404"}, MenuConstants.token_id2});

        obj.add(new Object[]{MenuConstants.rest_id, jsonHelper.getObjectToJSON(bulkSubCategory), new String[]{"400"}, MenuConstants.token_id});

        Entities entities2 = new Entities();
        entities2.build();
        Entities entities1 = bulkSubCategory.getEntities()[0];
        entities2.setCategory_id(null);
        bulkSubCategory.setEntities(new Entities[]{entities1, entities2});
        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(bulkSubCategory), MenuConstants.bulksubcategory_categoryid, MenuConstants.token_id2});

        entities2 = new Entities();
        entities2.build();
        entities1 = bulkSubCategory.getEntities()[0];
        entities2.setId(null);
        bulkSubCategory.setEntities(new Entities[]{entities1, entities2});
        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(bulkSubCategory), MenuConstants.bulksubcategory_id, MenuConstants.token_id2});

        entities2 = new Entities();
        entities2.build();
        entities1 = bulkSubCategory.getEntities()[0];
        entities2.setName(null);
        bulkSubCategory.setEntities(new Entities[]{entities1, entities2});
        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(bulkSubCategory), MenuConstants.bulksubcategory_name, MenuConstants.token_id2});

        entities2 = new Entities();
        entities2.build();
        entities1 = bulkSubCategory.getEntities()[0];
        entities2.setEnable(1);
        bulkSubCategory.setEntities(new Entities[]{entities1, entities2});
        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(bulkSubCategory), new String[]{}, MenuConstants.token_id2});

        entities2 = new Entities();
        entities2.build();
        entities1 = bulkSubCategory.getEntities()[0];
        entities2.setEnable(0);
        bulkSubCategory.setEntities(new Entities[]{entities1, entities2});
        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(bulkSubCategory), new String[]{}, MenuConstants.token_id2});


        return obj.iterator();
    }

    @DataProvider(name = "updatebulksubcategory")
    public Iterator<Object[]> updatebulksubcategory() throws IOException {
        List<Object[]> obj = new ArrayList<>();
        JsonHelper jsonHelper = new JsonHelper();

        BulkSubCategory bulkSubCategory = new BulkSubCategory();
        bulkSubCategory.build();
        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(bulkSubCategory), new String[]{}, MenuConstants.token_id2});

        obj.add(new Object[]{"", jsonHelper.getObjectToJSON(bulkSubCategory), new String[]{"404"}, MenuConstants.token_id2});

        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(bulkSubCategory), new String[]{"400"}, MenuConstants.token_id});

        Entities entities2 = new Entities();
        entities2.build();
        Entities entities1 = bulkSubCategory.getEntities()[0];
        entities2.setCategory_id(null);
        bulkSubCategory.setEntities(new Entities[]{entities1, entities2});
        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(bulkSubCategory), MenuConstants.bulksubcategory_categoryid, MenuConstants.token_id2});

        entities2 = new Entities();
        entities2.build();
        entities1 = bulkSubCategory.getEntities()[0];
        entities2.setId(null);
        bulkSubCategory.setEntities(new Entities[]{entities1, entities2});
        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(bulkSubCategory), MenuConstants.bulksubcategory_id, MenuConstants.token_id2});

        entities2 = new Entities();
        entities2.build();
        entities1 = bulkSubCategory.getEntities()[0];
        entities2.setName(null);
        bulkSubCategory.setEntities(new Entities[]{entities1, entities2});
        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(bulkSubCategory), new String[]{}, MenuConstants.token_id2});

        entities2 = new Entities();
        entities2.build();
        entities1 = bulkSubCategory.getEntities()[0];
        entities2.setEnable(1);
        bulkSubCategory.setEntities(new Entities[]{entities1, entities2});
        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(bulkSubCategory), new String[]{}, MenuConstants.token_id2});

        entities2 = new Entities();
        entities2.build();
        entities1 = bulkSubCategory.getEntities()[0];
        entities2.setEnable(0);
        bulkSubCategory.setEntities(new Entities[]{entities1, entities2});
        obj.add(new Object[]{MenuConstants.rest_id2, jsonHelper.getObjectToJSON(bulkSubCategory), new String[]{}, MenuConstants.token_id2});

        return obj.iterator();
    }
}
