package com.swiggy.api.erp.vms.helper;

import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;
import com.swiggy.api.erp.vms.constants.RMSConstants;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Daenerys.Initializer;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.RedisHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;

import java.math.BigInteger;
import java.time.Instant;
import java.util.*;

//import edu.emory.mathcs.backport.java.util.Arrays;

public class RMSHelper extends RMSConstants {

	CheckoutHelper checkoutHelper = new CheckoutHelper();
	Long order_id = -1l;
	Initialize initializer = Initializer.getInitializer();
	String token;
	String updatedNumberber = null;

	public Processor getLogin(String userName, String password) {

		GameOfThronesService services = new GameOfThronesService("rms", "rmslogin", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();

		requestheaders.put("Content-Type", "application/json");
		Processor processor = new Processor(services, requestheaders, new String[] { userName, password }, null);

		return processor;
	}

	public Processor fetchOrder(String restaurantId, String accessToken) {

		GameOfThronesService services = new GameOfThronesService("rms", "orderpolling", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		Processor processor = new Processor(services, requestheaders, new String[] { restaurantId });

		return processor;
	}

	public String getLoginToken(String userName, String password) {
		GameOfThronesService services = new GameOfThronesService("rms", "rmslogin", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		Processor processor = new Processor(services, requestheaders, new String[] { userName, password });
		token = processor.ResponseValidator.GetNodeValue("$.data.access_token");
		System.out.println("tokeeeeeeennnnnnnnnnnnn" + token);
		return token;
	}

	public Processor getOrders(String accessToken, String restaurantId) {
		GameOfThronesService services = new GameOfThronesService("rms", "orderpolling", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		Processor processor = new Processor(services, requestheaders, new String[] { restaurantId });
		return processor;
	}

	public Long getAnOrder() throws Exception {

		Processor processor = checkoutHelper.placeOrder(RMSConstants.portalUser, RMSConstants.portalPassword,
				RMSConstants.itemId, RMSConstants.quantity, RMSConstants.restaurantId);
		Long orderId = (long) processor.ResponseValidator.GetNodeValueAsInt("data.order_id");
		return order_id;
	}

	// GET HealthCheck
	public Processor healthCheck() {
		GameOfThronesService services = new GameOfThronesService("rms", "healthcheck", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Authorization", "Basic Y2hlY2s6Y2hlY2s=");
		Processor processor = new Processor(services, requestheaders, null, null);
		return processor;
	}

	// Rachana
	// Get HealthCheck
	public Processor getHealthCheck() {
		GameOfThronesService services = new GameOfThronesService("rms", "healthCheck1", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		// requestheaders.put("Authorization", "Basic Y2hlY2s6Y2hlY2s=");
		Processor processor = new Processor(services, requestheaders);
		return processor;
	}

	// Get Contact Reason
	public Processor getContactReasons(String accessToken) {
		GameOfThronesService services = new GameOfThronesService("rms", "contactReasons", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		Processor processor = new Processor(services, requestheaders);
		return processor;

	}

	// POST Contact US Ticket
	public Processor contactUsTicket(String accessToken, String restaurantId, String subject, String message,
			String category, String city) {
		GameOfThronesService services = new GameOfThronesService("rms", "contactUsTicket", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		// requestheaders.put("Content-Type","application/json");
		Processor processor = new Processor(services, requestheaders,
				new String[] { restaurantId, subject, message, category, city });
		return processor;
	}

	// GET :- Find Partner Type
	public Processor findPartnerType(String accessToken, String order_id_finpartnertype,
			String rest_id_finpartnertype) {
		GameOfThronesService services = new GameOfThronesService("rms", "findPartnerType", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		Processor processor = new Processor(services, requestheaders, null,
				new String[] { rest_id_finpartnertype, order_id_finpartnertype });
		return processor;

	}

	public Processor getPartnerType(String accessToken, String order_id_finpartnertype,
									 String rest_id_finpartnertype) {
		GameOfThronesService services = new GameOfThronesService("rms", "getPartnerType", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		Processor processor = new Processor(services, requestheaders, null,
				new String[] { rest_id_finpartnertype, order_id_finpartnertype });
		return processor;

	}

	public Processor restaurantOnOff(String accessToken, String rest_id11, String isRestaurantOnOff, String source) {
		GameOfThronesService services = new GameOfThronesService("rms", "restauranttoggleonoff", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		requestheaders.put("Content-Type", "application/json");
		Processor processor = new Processor(services, requestheaders,
				new String[] { rest_id11, isRestaurantOnOff, source });
		return processor;

	}

	public Processor restaurantOnOff(String accessToken, String rest_id11, String isRestaurantOnOff, String source,
			String endDate) {
		GameOfThronesService services = new GameOfThronesService("rms", "restauranttogglewithdate", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		requestheaders.put("Content-Type", "application/json");
		Processor processor = new Processor(services, requestheaders,
				new String[] { rest_id11, isRestaurantOnOff, source, endDate });
		return processor;

	}

	public Processor getRestaurantHolidaySlots(String accessToken, String restaurantId) {
		GameOfThronesService services = new GameOfThronesService("rms", "getRestaurantHolidaySlots", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		requestheaders.put("Content-Type", "application/json");
		Processor processor = new Processor(services, requestheaders, null, new String[] { restaurantId });

		return processor;

	}

	public Processor createRestaurantHolidaySlots(String accessToken, String restaurantId, String endDate,
			String isRequestedToOpen) {
		GameOfThronesService services = new GameOfThronesService("rms", "createRestaurantHolidaySlots", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		requestheaders.put("Content-Type", "application/json");
		Processor processor = new Processor(services, requestheaders,
				new String[] { restaurantId, endDate, isRequestedToOpen });

		return processor;

	}

	public Processor deleteRestaurantHolidaySlots(String accessToken, String restaurantId, String slotId) {
		GameOfThronesService services = new GameOfThronesService("rms", "deleteRestaurantHolidaySlots", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		requestheaders.put("Content-Type", "application/json");
		Processor processor = new Processor(services, requestheaders, new String[] { restaurantId, slotId });
		return processor;

	}

	public Processor fetchFeatureGate(String accessToken, String restId) {
		GameOfThronesService services = new GameOfThronesService("rms", "fetchFeatureGate", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		requestheaders.put("Content-Type", "application/json");
		Processor processor = new Processor(services, requestheaders, new String[] { restId });
		return processor;

	}

	public Processor foodIssuesGetConfig(String restaurantId) {
		GameOfThronesService services = new GameOfThronesService("rms2", "getConfig", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		// requestheaders.put("Authorization", "Basic Y2hlY2s6Y2hlY2s=");
		Processor processor = new Processor(services, requestheaders, null, new String[] { restaurantId });
		return processor;
	}

	public Processor foodIssuesVerifyBill(String orderId, String inputValue, String inputType, String billProvided) {
		GameOfThronesService services = new GameOfThronesService("rms2", "verifyBill", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		// requestheaders.put("Authorization", "Basic Y2hlY2s6Y2hlY2s=");
		Processor processor = new Processor(services, requestheaders,
				new String[] { orderId, inputValue, inputType, billProvided }, null);
		return processor;

	}

	public Processor foodIssuesSaveFinalBill(String issueId, String orderId, String finalBillValue) {
		GameOfThronesService services = new GameOfThronesService("rms2", "saveFinalBill", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		// requestheaders.put("Authorization", "Basic Y2hlY2s6Y2hlY2s=");
		Processor processor = new Processor(services, requestheaders, new String[] { issueId, orderId, finalBillValue },
				null);
		return processor;

	}

	public Processor foodIssuesRectifybill(String issueId, String resolution) {
		GameOfThronesService services = new GameOfThronesService("rms2", "rectifyBill", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		// requestheaders.put("Authorization", "Basic Y2hlY2s6Y2hlY2s=");
		Processor processor = new Processor(services, requestheaders, new String[] { issueId, resolution }, null);
		return processor;
	}

	public Processor restaurantTierMerticsOfRestaurant(String accessToken, String restaurantId) {
		GameOfThronesService services = new GameOfThronesService("rms", "merticsOfRestaurant", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);

		Processor processor = new Processor(services, requestheaders, null, new String[] { restaurantId });
		return processor;
	}

	public Processor restaurantTierBenifitsForAllTiers(String accessToken, String cityId, String restId) {
		GameOfThronesService services = new GameOfThronesService("rms", "benifitsForAllTiers", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);

		Processor processor = new Processor(services, requestheaders, null, new String[] { cityId, restId });
		return processor;
	}

	public Processor dscallout(String restaurantId, String orderId) {
		GameOfThronesService services = new GameOfThronesService("rms", "dsCallout", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("Authorization", "Basic MXFERjE5UENVNzpoeVQ0dnZIMjVPMDNjeHF0U1VXNWxJejFFOUZ0ZGp6aA==");

		Processor processor= new Processor(services,requestheaders,new String[]{orderId, restaurantId},null);
		return processor;
	}

	public Processor fetchBroadcastInfo(String accessToken, String restaurantId) {
		GameOfThronesService services = new GameOfThronesService("rms", "fetchBroadcastInfo", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);

		Processor processor = new Processor(services, requestheaders, null, new String[] { restaurantId });
		return processor;
	}

	public Processor changepassword(String accessToken, String username, String oldpass, String newpass) {
		GameOfThronesService services = new GameOfThronesService("rms", "changepassword", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		requestheaders.put("Content-Type", "application/json");
		Processor processor = new Processor(services, requestheaders, new String[] { username, oldpass, newpass });

		return processor;
	}

	public Processor logout(String username, String accessToken) {
		GameOfThronesService services = new GameOfThronesService("rms", "logout", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		requestheaders.put("Content-Type", "application/json");
		Processor processor = new Processor(services, requestheaders, new String[] { username });

		return processor;
	}

	public Processor notify(String restaurantId, String orderId, String status, String accessToken) {
		GameOfThronesService services = new GameOfThronesService("rms", "notify", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		requestheaders.put("Content-Type", "application/json");
		Processor processor = new Processor(services, requestheaders, new String[] { restaurantId, orderId, status });

		return processor;
	}

	public Processor dispatch(String accessToken, String restaurantId, String orderId) {
		GameOfThronesService services = new GameOfThronesService("rms", "dispatch", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		requestheaders.put("Content-Type", "application/json");
		Processor processor = new Processor(services, requestheaders, new String[] { restaurantId, orderId });

		return processor;
	}

	public Processor confirm(String accessToken, String orderId, String restaurantId) {
		GameOfThronesService services = new GameOfThronesService("rms", "confirm", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		requestheaders.put("Content-Type", "application/json");
		Processor processor = new Processor(services, requestheaders, new String[] { restaurantId, orderId });

		return processor;
	}

	public Processor confirm(String accessToken, String orderId, String restaurantId, String prepTime) {
		GameOfThronesService services = new GameOfThronesService("rms", "confirmWithPrep", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		requestheaders.put("Content-Type", "application/json");
		Processor processor = new Processor(services, requestheaders, new String[] { restaurantId, orderId, prepTime });

		return processor;
	}

	public Processor markReady(String accessToken, String orderId, String restaurantId) {
		GameOfThronesService services = new GameOfThronesService("rms", "markReady", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		requestheaders.put("Content-Type", "application/json");
		Processor processor = new Processor(services, requestheaders, new String[] { restaurantId, orderId });

		return processor;
	}

	public Processor callback(String accessToken, String restaurantId, String orderId) {
		GameOfThronesService services = new GameOfThronesService("rms", "callback", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		requestheaders.put("Content-Type", "application/json");
		Processor processor = new Processor(services, requestheaders, new String[] { restaurantId, orderId });

		return processor;
	}

	public Processor toggleitemsoos(String accessToken, String markInStock, String item_id, String restaurantId,
			String fromTime, String toTime, String EOD, String hours) {
		GameOfThronesService services = new GameOfThronesService("rms", "toggleitemsoos", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		requestheaders.put("Content-Type", "application/json");
		Processor processor = new Processor(services, requestheaders,
				new String[] { markInStock, item_id, restaurantId, fromTime, toTime, EOD, hours });

		return processor;
	}

	public Processor markItemOOS(String orderId, String itemId, String alternatives, String fromTime,
			String restaurantId, String accessToken) {
		System.out.println(alternatives);
		GameOfThronesService services = new GameOfThronesService("rms", "markitemoos", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		requestheaders.put("Content-Type", "application/json");
		Processor processor = new Processor(services, requestheaders,
				new String[] { orderId, itemId, alternatives, fromTime, restaurantId });

		return processor;

	}

	public Processor updateorderHistoryCsvResponse(String restaurantId, String sessionId, String reporURL,
			String accessToken) {
		GameOfThronesService services = new GameOfThronesService("rms", "orderhistorycsvresponse", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		requestheaders.put("Content-Type", "application/json");
		Processor processor = new Processor(services, requestheaders,
				new String[] { restaurantId, sessionId, reporURL });

		return processor;

	}

	public Processor getOldOrderdHistory(String accessToken, String restaurantId) {
		GameOfThronesService services = new GameOfThronesService("rms", "orderoldhistory", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		requestheaders.put("Content-Type", "application/json");
		Processor processor = new Processor(services, requestheaders, null, new String[] { restaurantId });

		return processor;

	}

	public Processor getOrderdHistory(String accessToken, String restaurantId, String limit, String offset,
			String ordered_time__gte, String ordered_time__lte, String filterReq) {
		GameOfThronesService services = new GameOfThronesService("rms", "orderhistory", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		requestheaders.put("Content-Type", "application/json");
		Processor processor = new Processor(services, requestheaders, null,
				new String[] { restaurantId, limit, offset, ordered_time__gte, ordered_time__lte, filterReq });

		return processor;

	}

	public Processor getorderHistoryMultiChain(String accessToken, ArrayList<String> restaurantIds) {

		System.out.println(restaurantIds);
		System.out.println(String.valueOf(restaurantIds).replace('[', ' ').replace(']', ' ').trim());
		GameOfThronesService services = new GameOfThronesService("rms", "orderhistorymultichain", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		requestheaders.put("Content-Type", "application/json");
		Processor processor = new Processor(services, requestheaders, null, new String[] {
				String.valueOf(restaurantIds).replace('[', ' ').replace(']', ' ').replace(", ", ",").trim() });

		return processor;

	}

	public Processor getOrderHistoryCSV(String accessToken, String restaurantId, String isPoll, String orderedTimeGte,
			String orderedTimeLte, String requestedTimeStamp) {
		GameOfThronesService services = new GameOfThronesService("rms", "orderhistorycsv", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		requestheaders.put("Content-Type", "application/json");
		Processor processor = new Processor(services, requestheaders, null,
				new String[] { restaurantId, isPoll, orderedTimeGte, orderedTimeLte, requestedTimeStamp });

		return processor;

	}

	public Processor getRestUsers(String accessToken, String rest_id) {
		GameOfThronesService services = new GameOfThronesService("rms", "getRestUsers", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		Processor processor = new Processor(services, requestheaders, null, new String[] { rest_id });
		return processor;
	}

	public Processor getUserInfo(String accessToken) {
		GameOfThronesService services = new GameOfThronesService("rms", "getUserInfo", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		Processor processor = new Processor(services, requestheaders, null);
		return processor;
	}

	public Processor getRestInfo(String accessToken, String restaurantId) {
		GameOfThronesService services = new GameOfThronesService("rms", "getRestInfo", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		Processor processor = new Processor(services, requestheaders, null, new String[] { restaurantId });
		return processor;
	}

	public Processor createUser(String accessToken, int role_id, String userPhone_no, String name, String email,
			String[] permissions, String[] restaurantId) {
		boolean flag = false;
		// long mobile_number = null;
		GameOfThronesService services = new GameOfThronesService("rms", "createUser", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		Processor processor;
		processor = new Processor(services, requestheaders, new String[] { String.valueOf(role_id), userPhone_no, name,
				email + "@email.com", Arrays.toString(permissions), Arrays.toString(restaurantId) }, null);
		return processor;

	}

	public String getNextPhoneNumber(String mobilenumber) {
		long a = Long.parseLong(mobilenumber);
		return String.valueOf(++a);

	}

	public Processor createUser2(String accessToken, int role_id, String userPhone_no, String name, String email,
			String[] permissions, String[] restaurantId) {
		GameOfThronesService services = new GameOfThronesService("rms", "createUser", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		Processor processor = new Processor(services, requestheaders,
				new String[] { String.valueOf(role_id), String.valueOf(userPhone_no), name, email,
						Arrays.toString(permissions), Arrays.toString(restaurantId) },
				null);

		return processor;

	}

	public Processor updateUser(String accessToken, int updatedRole_id, String role_name, String phone_no,
			String updatedEmail, String updatedName, String[] permissions, String[] restaurantId, int role_id,
			String email, String[] old_permissions, String[] old_restaurants, String registration_complete,
			String isActive) {
		GameOfThronesService services = new GameOfThronesService("rms", "updateUser", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();

		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		Processor processor = new Processor(services, requestheaders,
				new String[] { String.valueOf(updatedRole_id), role_name, phone_no, updatedEmail, updatedName,
						Arrays.toString(permissions), Arrays.toString(restaurantId), String.valueOf(role_id), email,
						Arrays.toString(old_permissions), Arrays.toString(old_restaurants), registration_complete,
						isActive },
				null);
		return processor;
	}

	public Processor disableUser(String accessToken, String phone_no, String is_enable) {
		GameOfThronesService services = new GameOfThronesService("rms", "disableUser", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		Processor processor = new Processor(services, requestheaders, new String[] { phone_no, is_enable }, null);
		return processor;
	}

	public Processor fetchItems(String accessToken, String restaurantId) {
		GameOfThronesService services = new GameOfThronesService("rms", "fetchItems", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		Processor processor = new Processor(services, requestheaders, null, new String[] { restaurantId });
		return processor;
	}

	public Processor itemOOS(String accessToken, int markOOStock, String itemId, String restaurantId, String hours) {
		GameOfThronesService services = new GameOfThronesService("rms", "itemOOS", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		Processor processor = new Processor(services, requestheaders,
				new String[] { String.valueOf(markOOStock), itemId, restaurantId, hours }, null);
		return processor;
	}


	public Processor itemOOSV2(String accessToken, int markOOStock, String itemId, String restaurantId, String hours ) {
		GameOfThronesService services = new GameOfThronesService("rms", "itemOOSV2", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("Cookie",
				"Swiggy_Session-alpha=" + accessToken);
		Processor processor = new Processor(services, requestheaders, new String[] { String.valueOf(markOOStock), itemId, restaurantId, hours }, null);
		return processor;
	}

	public Processor itemInstock(String accessToken,String markInStock, String itemId, String restaurantId, String hours) {
		GameOfThronesService services = new GameOfThronesService("rms", "itemInstock", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		Processor processor = new Processor(services, requestheaders,
				new String[] { markInStock, itemId, restaurantId, hours }, null);
		return processor;
	}

	public Processor variantOOS(String accessToken, int markOOStock, String variantItemIdOos, String variantId,
			String variantGroupId, String restaurantId) {
		GameOfThronesService services = new GameOfThronesService("rms", "variantOOS", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		Processor processor = new Processor(services, requestheaders,
				new String[] { String.valueOf(markOOStock), variantItemIdOos, variantId, variantGroupId, restaurantId },
				null);
		return processor;
	}

	public Processor variantOOSV2(String accessToken,int markOOStock, String variantItemIdOos, String variantId, String variantGroupId, String restaurantId) {
		GameOfThronesService services = new GameOfThronesService("rms", "variantOOSV2", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("Cookie",
				"Swiggy_Session-alpha=" + accessToken);
		Processor processor = new Processor(services, requestheaders, new String[] { String.valueOf(markOOStock), variantItemIdOos, variantId, variantGroupId, restaurantId }, null);
		return processor;
	}
	
	public Processor variantInStock(String accessToken,String markInStock, String variantItemIdOos, String variantId, String variantGroupId, String restaurantId) {
		GameOfThronesService services = new GameOfThronesService("rms", "variantInStock", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		Processor processor = new Processor(services, requestheaders,
				new String[] { markInStock, variantItemIdOos, variantId, variantGroupId, restaurantId }, null);
		return processor;
	}

	public Processor createNewUser(String accessToken, int role_id, String userPhone_no, String name, String email,
			String[] permissions, String[] restaurantId) {

		GameOfThronesService services = new GameOfThronesService("rms", "createUser", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		Processor processor = new Processor(services, requestheaders,
				new String[] { String.valueOf(role_id), String.valueOf(userPhone_no), name, email,
						Arrays.toString(permissions), Arrays.toString(restaurantId) },
				null);
		return processor;

	}

	public Processor registerUser(String accessToken, String mobile, String email, String name, String password,
			String confirmPassword) {

		GameOfThronesService services = new GameOfThronesService("rms", "rmssignup", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		Processor processor = new Processor(services, requestheaders,
				new String[] { mobile, email, name, password, confirmPassword }, null);
		return processor;

	}

	public String getOTP(String mobile) {

		String key = "otp_" + mobile;
		RedisHelper redisHelper = new RedisHelper();
		// RedisTemplate rtemplate = redisHelper.getRedisTemplate("vmsredisstage",8);
		// Object o = rtemplate;
		System.out.println("redis value is" + redisHelper.getValue("vmsredisstage", 8, key).toString());
		// System.out.println("tid in passed to VMShelper getOTP method "+o.toString());
		// return o.toString();
		return redisHelper.getValue("vmsredisstage", 8, key).toString();
	}

	public Processor confirmOTP(String mobile, String otp) {

		GameOfThronesService services = new GameOfThronesService("rms", "confirmotp", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");

		Processor processor = new Processor(services, requestheaders, new String[] { mobile, otp }, null);
		return processor;

	}

	public boolean aggregateResponseAndStatusCodes(Processor response) {
		boolean success = false;

		System.out.println("SuccessresponseCode :" + response.ResponseValidator.GetResponseCode());
		System.out.println("SuccessstatusCode : " + response.ResponseValidator.GetNodeValueAsInt("statusCode"));

		if ((RMSConstants.successresponseCode == response.ResponseValidator.GetResponseCode())
				&& RMSConstants.successstatusCode == response.ResponseValidator.GetNodeValueAsInt("statusCode")) {
			System.out.println("1..inside response");
			success = true;
		} else if ((RMSConstants.successresponseCode == response.ResponseValidator.GetResponseCode())
				&& RMSConstants.failedstatusCode == response.ResponseValidator.GetNodeValueAsInt("statusCode")) {
			System.out.println("2..inside response");
			System.out.println("SuccessresponseCode: " + response.ResponseValidator.GetResponseCode());
			System.out.println("SuccessstatusCode :  " + response.ResponseValidator.GetNodeValueAsInt("statusCode"));
			success = true;
		} else if ((RMSConstants.successresponseCode == response.ResponseValidator.GetResponseCode())
				&& RMSConstants.failedstatusCode_invalidSession == response.ResponseValidator
						.GetNodeValueAsInt("statusCode")) {
			System.out.println("3..inside response");
			System.out.println("SuccessresponseCode: " + response.ResponseValidator.GetResponseCode());
			System.out.println("SuccessstatusCode :  " + response.ResponseValidator.GetNodeValueAsInt("statusCode"));
			success = true;
		}

		else if ((RMSConstants.successresponseCode == response.ResponseValidator.GetResponseCode())
				&& RMSConstants.failedstatusCode_alreadyExist == response.ResponseValidator
						.GetNodeValueAsInt("statusCode")) {
			System.out.println("4..inside response");
			System.out.println("SuccessresponseCode: " + response.ResponseValidator.GetResponseCode());
			System.out.println("SuccessstatusCode :  " + response.ResponseValidator.GetNodeValueAsInt("statusCode"));
			success = true;
		}
		return success;
	}

	public Processor getBillMasking(String token) {
		GameOfThronesService services = new GameOfThronesService("rms", "billMasking", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + token);
		Processor processor = new Processor(services, requestheaders, null, null);
		return processor;
	}

	public String getUserByName(String name) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RMSConstants.rms_db);
		name = name.split("-")[1];
		List<Map<String, Object>> list = sqlTemplate
				.queryForList(RMSConstants.user_by_name + name + RMSConstants.end_like);
		if (list.size() > 0) {
			BigInteger id = (BigInteger) list.get(0).get("id");
			return id.toString();
		}
		return "";
	}

	public void deleteUserIds(String user_id) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RMSConstants.rms_db);
		sqlTemplate.execute(RMSConstants.delete_user_pass + user_id);
		sqlTemplate.execute(RMSConstants.delete_user + user_id);
	}

	public String getUniqueId() {
		Long order_id = Instant.now().getEpochSecond();
		return String.valueOf(order_id);
	}

	public Processor registration(String mobileno) {

		GameOfThronesService services = new GameOfThronesService("rms", "registration", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		Processor processor = new Processor(services, requestheaders, null, new String[] { mobileno });
		return processor;
	}

	public Processor registrationV2(String userId) {

		GameOfThronesService services = new GameOfThronesService("rms", "registrationV2", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		Processor processor = new Processor(services, requestheaders, null, new String[] { userId });
		return processor;
	}

	public Processor getVariantV2(String accessToken, String variantId, String variantGroupId, String itemId,
			String restaurantId) {
		GameOfThronesService services = new GameOfThronesService("rms", "getVariantV2", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);

		Processor processor = new Processor(services, requestheaders, null,
				new String[] { variantId, variantGroupId, itemId, restaurantId });
		return processor;
	}

	public Processor getVariant(String accessToken, String variantId, String variantGroupId, String itemId,
								  String restaurantId) {
		GameOfThronesService services = new GameOfThronesService("rms", "getVariant", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);

		Processor processor = new Processor(services, requestheaders, null,
				new String[] { variantId, variantGroupId, itemId, restaurantId });
		return processor;
	}

	public Processor getAlternatives(String accessToken, String itemId, String restaurantId) {
		GameOfThronesService services = new GameOfThronesService("rms", "getAlternatives", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);

		Processor processor = new Processor(services, requestheaders, null, new String[] { itemId, restaurantId });
		return processor;
	}

	public Processor getAlternativesV2(String accessToken, String restaurantId) {
		GameOfThronesService services = new GameOfThronesService("rms", "getAlternativesV2", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);

		Processor processor = new Processor(services, requestheaders, null, new String[] { restaurantId });
		return processor;
	}

	public List<Integer> getItems(Processor p) {
		return (List<Integer>) (JsonPath.read(p.ResponseValidator.GetBodyAsText(),
				"$.data.categories..subCategories..menu[?(@.inStock == 0)].id"));
	}

	public List<String> getVariantGroupIds(Processor p, String item_id) {
		return (List<String>) (JsonPath.read(p.ResponseValidator.GetBodyAsText(),
				"$.data.categories..subCategories..menu[?(@.id == " + item_id
						+ ")].variants_new.variant_groups..group_id"));
	}

	public List<Integer> getVariantId(Processor p, String variant_groupId) {
		return (List<Integer>) (JsonPath.read(p.ResponseValidator.GetBodyAsText(),
				"$.data.categories..subCategories..menu..variants_new.variant_groups[?(@.group_id == " + variant_groupId
						+ ")].variations..id"));
	}

	public Processor getInvoiceDocument(String accessToken, String restaurantId, String fromDate, String toDate) {
		GameOfThronesService services = new GameOfThronesService("rms", "getInvoiceDocuments", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);

		Processor processor = new Processor(services, requestheaders, null, new String[]{restaurantId, fromDate, toDate});
		return processor;
	}

	public Processor getNonPollingRestaurants(String interval) {
		GameOfThronesService services = new GameOfThronesService("rms", "getNonPollingRestaurants", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Authorization", "Basic YWtqZGRoN2hkZDpkamtzaGp3OTIzZGpkamg4MzM4c2hqZDIxNzE4Mm4=");

		Processor processor = new Processor(services, requestheaders, null, new String[]{interval});
		return processor;
	}

	public Processor trackOrder(String accessToken, String restaurantId, String orderId){

		GameOfThronesService services=new GameOfThronesService("rms", "trackOrder", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("Cookie", "Swiggy_Session-alpha="+accessToken);
		Processor processor= new Processor(services,requestheaders,new String[]{restaurantId, orderId}, null);

		return processor;
	}

	public Processor locateOrder(String accessToken, String restaurantId, String orderId) {
		GameOfThronesService services = new GameOfThronesService("rms", "locateOrder", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);

		Processor processor = new Processor(services, requestheaders, null, new String[]{orderId, restaurantId});
		return processor;
	}

	public Processor getDispositions(String accessToken, String cities, String restaurantId) {
		GameOfThronesService services = new GameOfThronesService("rms", "getDispositions", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);

		Processor processor = new Processor(services, requestheaders, null, new String[]{cities, restaurantId});
		return processor;
	}

	public Processor getActiveOrders(){

		GameOfThronesService services=new GameOfThronesService("rms", "activeOrders", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("Authorization", "Basic R0dZU1dJOlJEM2NQTVJtN2VtcEFKZzM=");
		Processor processor= new Processor(services,requestheaders, null);

		return processor;
	}

	public String getWithPartnerTime(String orderId) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RMSConstants.rms_db);
		List<Map<String, Object>> list = sqlTemplate.queryForList(RMSConstants.order_by_id + orderId);
		if(list.size() > 0) {
			String with_partner_timestamp = list.get(0).get("with_partner_timestamp").toString();
			return with_partner_timestamp;
		}
		return "";
	}

	public Processor withPartner(String orderId, String restaurantId){

		GameOfThronesService services=new GameOfThronesService("rms", "withPartner", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("Authorization", "Basic Y1Bqd0pXUlhzODppTloxR2dQVk05WGt4QmdOWGh1RWZ4cVZNUm14QW4wYg==");
		Processor processor= new Processor(services,requestheaders,new String[]{orderId, restaurantId}, null);

		return processor;
	}

	public Processor firstAcceptance(String orderId, String restaurantId, String event){

		GameOfThronesService services=new GameOfThronesService("rms", "firstAcceptance", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("Authorization", "Basic Y1Bqd0pXUlhzODppTloxR2dQVk05WGt4QmdOWGh1RWZ4cVZNUm14QW4wYg==");
		Processor processor= new Processor(services,requestheaders,new String[]{orderId, restaurantId, event}, null);

		return processor;
	}

	public List<Map<String, Object>> firstAcceptanceEvent(String orderId) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RMSConstants.rms_db);
		List<Map<String, Object>> list = sqlTemplate.queryForList(RMSConstants.firstAcceptance + orderId);
		if(list.size() > 0) {
			return list;
		}
		return null;
	}

	public Processor markFoodPrepared(String orderId, String restaurantId){

		GameOfThronesService services=new GameOfThronesService("rms", "markFoodPrepared", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("Authorization", "Basic Y1Bqd0pXUlhzODppTloxR2dQVk05WGt4QmdOWGh1RWZ4cVZNUm14QW4wYg==");
		Processor processor= new Processor(services,requestheaders,new String[]{orderId, restaurantId}, null);

		return processor;
	}

	public Processor trackAllOrders(String orderId) {

		GameOfThronesService services = new GameOfThronesService("rms", "trackAllOrders", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("Authorization", "Basic R0dZU1dJOlJEM2NQTVJtN2VtcEFKZzM=");
		Processor processor = new Processor(services, requestheaders, new String[]{orderId});

		return processor;
	}

	public static long getOrderIdMaxValue() {
		String str = "select max(order_id) as order_id from order_edit_request";
		Map<String, Object> maxVal = SystemConfigProvider.getTemplate("ordereditservice").queryForMap(str);
		System.out.println(maxVal);
		long ss=(long)maxVal.get("order_id");
		System.out.println(ss);
		return ss;
	}

	public Processor getTDEnabledCities(String accessToken, String restaurantId) {
		GameOfThronesService services = new GameOfThronesService("rms", "getTDEnabledCities", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		requestheaders.put("Content-Type", "application/json");

		Processor processor = new Processor(services, requestheaders, new String[] {restaurantId}, null);
		return processor;
	}

	public Processor getRestInfoById(String accessToken, String restaurantId) {
		GameOfThronesService services = new GameOfThronesService("rms", "getRestInfoById", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		requestheaders.put("Content-Type", "application/json");

		Processor processor = new Processor(services, requestheaders, new String[] {restaurantId}, null);
		return processor;
	}

	public Processor getGSTFeatureFlag(String accessToken) {
		GameOfThronesService services = new GameOfThronesService("rms", "getGSTFeatureFlag", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);

		Processor processor = new Processor(services, requestheaders, null);
		return processor;
	}

	public Processor getGSTStaticInfo(String accessToken) {
		GameOfThronesService services = new GameOfThronesService("rms", "getGSTStaticInfo", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);

		Processor processor = new Processor(services, requestheaders, null);
		return processor;
	}

	public Processor getGSTDetails(String accessToken, String restaurantId) {
		GameOfThronesService services = new GameOfThronesService("rms", "getGSTDetails", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);

		Processor processor = new Processor(services, requestheaders, null, new String[]{restaurantId});
		return processor;
	}

	public Processor getRestaurantInfo(String accessToken, String restaurantId) {
		GameOfThronesService services = new GameOfThronesService("rms", "getRestaurantInfo", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);
		requestheaders.put("Content-Type", "application/json");

		Processor processor = new Processor(services, requestheaders, new String[] {restaurantId}, null);
		return processor;
	}

	public String deStatus(Processor processor, String orderId) {

		String deStatus = JsonPath
				.read(processor.ResponseValidator.GetBodyAsText(),
						"$.restaurantData..[?(@.order_id=='" + orderId + "')].status.delivery_status")
				.toString().replace("[\"", "").replace("\"]", "").trim();
		System.out.println("Delivery Status:::::" + deStatus);

		return deStatus;
	}

	public Processor getUsers(String rest_id, String user_id, String email,
							 String mobile_no, String name, String limit) {
		GameOfThronesService services = new GameOfThronesService("rms", "getUsers", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Authorization", "Basic WUdHSVNXOlNXSUdHWUAyMDE3");

		Processor processor = new Processor(services, requestheaders, null,
				new String[] {rest_id, user_id, email, mobile_no, name, limit});
		return processor;
	}

	public Processor getUserByRest(String rest_id, String role_id, String partner_type, String mapping_type) {
		GameOfThronesService services = new GameOfThronesService("rms", "getUserByRest", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");

		Processor processor = new Processor(services, requestheaders, new String[] {rest_id, role_id, partner_type, mapping_type}, null);
		return processor;
	}

	public Processor getDispFromFF(String orderId) {
		GameOfThronesService services = new GameOfThronesService("rms", "getDispFromFF", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("Authorization", "Basic S1dyYkY3eXBOZjo3WG5LOTQzcnRaZEJCTE02");

		Processor processor = new Processor(services, requestheaders, null, new String[] {orderId});
		return processor;
	}

	public String orderWithDisp() {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RMSConstants.rms_db);
		List<Map<String, Object>> list = sqlTemplate.queryForList(RMSConstants.disp);
		if(list.size() > 0) {
			String orderId = list.get(0).get("order_id").toString();
			return orderId;
		}
		return "1111";

	}

	public Processor updateUserFromROP(String name, String mobileNumber, String email, String restaurantId, String role,
									   String isPrimary, String action, String updatedBy, String source) {
		GameOfThronesService services = new GameOfThronesService("rms", "updateUserFromROP", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Authorization", "Basic WUdHSVNXOlNXSUdHWUAyMDE3");
		requestheaders.put("Content-Type", "application/json");

		Processor processor = new Processor(services, requestheaders, new String[] {name, mobileNumber, email,
				restaurantId, role, isPrimary, action, updatedBy, source}, null);
		return processor;
	}

	public Processor replaceUserFromROP(String name, String mobileNumber, String email, String restaurantId, String role,
									   String isPrimary, String action, String updatedBy, String userToReplaceId) {
		GameOfThronesService services = new GameOfThronesService("rms", "replaceUserFromROP", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Authorization", "Basic WUdHSVNXOlNXSUdHWUAyMDE3");
		requestheaders.put("Content-Type", "application/json");

		Processor processor = new Processor(services, requestheaders, new String[] {name, mobileNumber, email,
				restaurantId, role, isPrimary, action, updatedBy, userToReplaceId}, null);
		return processor;
	}

	public void deleteUserDataFromUsersAndMap(String rest_id, String user_id, String email,
							  String mobile_no, String name, String limit) throws PathNotFoundException {
		Processor p = getUsers(rest_id, user_id, email, mobile_no, name, limit);
		String Id = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.users.[0].id");
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RMSConstants.rms_db);
		sqlTemplate.execute(RMSConstants.delete_user_map + Id);
		sqlTemplate.execute(RMSConstants.delete_user_pass + Id);
		sqlTemplate.execute(RMSConstants.delete_user + Id);

	}

	public String getIsPrimaryFromUserMap(String rest_id, String user_id, String email,
											  String mobile_no, String name, String limit) {
		Processor p = getUsers(rest_id, user_id, email, mobile_no, name, limit);
		String Id = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.users.[0].id");
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RMSConstants.rms_db);
		List<Map<String, Object>> list = sqlTemplate.queryForList(RMSConstants.userMap_by_id + Id);
		System.out.println("ISPRIMARYYYYYYY" + list);
		if(list.size() > 0) {
			String is_Primary = list.get(0).get("is_Primary").toString();
			return is_Primary;
		}

		return null;

	}

	public String getUserId(String rest_id, String user_id, String email,
										  String mobile_no, String name, String limit) throws PathNotFoundException {
		Processor p = getUsers(rest_id, user_id, email, mobile_no, name, limit);
		String Id = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.users.[0].id");
		return Id;

	}

	public void setRegisterdFlagForUser(String rest_id, String user_id, String email,
										  String mobile_no, String name, String limit) {
		Processor p = getUsers(rest_id, user_id, email, mobile_no, name, limit);
		String Id = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.users.[0].id");
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RMSConstants.rms_db);
		sqlTemplate.execute(RMSConstants.setRegisTrue + Id);
	}

	public Processor getRoles() {
		GameOfThronesService services = new GameOfThronesService("rms", "getRoles", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("Authorization", "Basic WUdHSVNXOlNXSUdHWUAyMDE3");

		Processor processor = new Processor(services, requestheaders, null, null);
		return processor;
	}

	public Processor getRoleByID(String Id) {
		GameOfThronesService services = new GameOfThronesService("rms", "getRoleByID", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("Authorization", "Basic WUdHSVNXOlNXSUdHWUAyMDE3");

		Processor processor = new Processor(services, requestheaders, null, new String[] {Id});
		return processor;
	}

	public Processor getAllCities(String accessToken, String restaurantId) {
		GameOfThronesService services = new GameOfThronesService("rms", "getAllCities", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("Cookie", "Swiggy_Session-alpha="+accessToken);

		Processor processor = new Processor(services, requestheaders, null, new String[] {restaurantId});
		return processor;
	}

	public Processor getCities(String accessToken, String restaurantId, String cityId) {
		GameOfThronesService services = new GameOfThronesService("rms", "getCities", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("Cookie", "Swiggy_Session-alpha="+accessToken);

		Processor processor = new Processor(services, requestheaders, null, new String[] {restaurantId, cityId});
		return processor;
	}

	public Processor getTC(String accessToken, String restaurantId) {
		GameOfThronesService services = new GameOfThronesService("rms", "getTC", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("Cookie", "Swiggy_Session-alpha="+accessToken);

		Processor processor = new Processor(services, requestheaders, null, new String[] {restaurantId});
		return processor;
	}

	public Processor getTCAcceptance(String accessToken, String restaurantId) {
		GameOfThronesService services = new GameOfThronesService("rms", "getTCAcceptance", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("Cookie", "Swiggy_Session-alpha="+accessToken);

		Processor processor = new Processor(services, requestheaders, null, new String[] {restaurantId});
		return processor;
	}

	public Processor getTCLastAcceptance(String accessToken, String restaurantId) {
		GameOfThronesService services = new GameOfThronesService("rms", "getTCLastAcceptance", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("Cookie", "Swiggy_Session-alpha="+accessToken);

		Processor processor = new Processor(services, requestheaders, null, new String[] {restaurantId});
		return processor;
	}

	public Processor acceptTC(String accessToken, String restaurantId) {
		GameOfThronesService services = new GameOfThronesService("rms", "acceptTC", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("Cookie", "Swiggy_Session-alpha="+accessToken);

		Processor processor = new Processor(services, requestheaders, new String[] {restaurantId}, null);
		return processor;
	}

	public Processor enabledWebviewTC(String accessToken, String restaurantId) {
		GameOfThronesService services = new GameOfThronesService("rms", "enabledWebviewTC", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("Cookie", "Swiggy_Session-alpha="+accessToken);

		Processor processor = new Processor(services, requestheaders, new String[] {restaurantId}, null);
		return processor;
	}

	public Processor acceptWebviewTC(String accessToken, String restaurantId) {
		GameOfThronesService services = new GameOfThronesService("rms", "acceptWebviewTC", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("Cookie", "Swiggy_Session-alpha="+accessToken);

		Processor processor = new Processor(services, requestheaders, new String[] {restaurantId}, null);
		return processor;
	}

	public Processor getMfrReport(String accessToken, String rest_id, String startdate, String enddate) {
		GameOfThronesService services = new GameOfThronesService("rms1", "getMfrReport", initializer);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("Cookie", "Swiggy_Session-alpha="+accessToken);
		Processor processor = new Processor(services, requestheaders, null,
				new String[] {rest_id, startdate, enddate});
		return processor;

	}
}
