package com.swiggy.api.erp.cms.pojo.getMultipleItemsUsingItemId;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "group_id",
        "group_name",
        "order",
        "minAddons",
        "maxFreeAddons",
        "maxAddons",
        "choices"
})
public class Addon {

    @JsonProperty("group_id")
    private Integer groupId;
    @JsonProperty("group_name")
    private String groupName;
    @JsonProperty("order")
    private Integer order;
    @JsonProperty("minAddons")
    private Integer minAddons;
    @JsonProperty("maxFreeAddons")
    private Integer maxFreeAddons;
    @JsonProperty("maxAddons")
    private Integer maxAddons;
    @JsonProperty("choices")
    private Map<String, Choices> choiceMap;

    /**
     * No args constructor for use in serialization
     *
     */
    public Addon() {
    }

    /**
     *
     * @param minAddons
     * @param groupId
     * @param groupName
     * @param order
     * @param maxAddons
     * @param choiceMap
     * @param maxFreeAddons
     */
    public Addon(Integer groupId, String groupName, Integer order, Integer minAddons, Integer maxFreeAddons, Integer maxAddons, Map<String, Choices> choiceMap) {
        super();
        this.groupId = groupId;
        this.groupName = groupName;
        this.order = order;
        this.minAddons = minAddons;
        this.maxFreeAddons = maxFreeAddons;
        this.maxAddons = maxAddons;
        this.choiceMap = choiceMap;
    }

    @JsonProperty("group_id")
    public Integer getGroupId() {
        return groupId;
    }

    @JsonProperty("group_id")
    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    @JsonProperty("group_name")
    public String getGroupName() {
        return groupName;
    }

    @JsonProperty("group_name")
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    @JsonProperty("order")
    public Integer getOrder() {
        return order;
    }

    @JsonProperty("order")
    public void setOrder(Integer order) {
        this.order = order;
    }

    @JsonProperty("minAddons")
    public Integer getMinAddons() {
        return minAddons;
    }

    @JsonProperty("minAddons")
    public void setMinAddons(Integer minAddons) {
        this.minAddons = minAddons;
    }

    @JsonProperty("maxFreeAddons")
    public Integer getMaxFreeAddons() {
        return maxFreeAddons;
    }

    @JsonProperty("maxFreeAddons")
    public void setMaxFreeAddons(Integer maxFreeAddons) {
        this.maxFreeAddons = maxFreeAddons;
    }

    @JsonProperty("maxAddons")
    public Integer getMaxAddons() {
        return maxAddons;
    }

    @JsonProperty("maxAddons")
    public void setMaxAddons(Integer maxAddons) {
        this.maxAddons = maxAddons;
    }

    @JsonProperty("choices")
    public Map<String, Choices> getChoices() {
        return choiceMap;
    }

    @JsonProperty("choices")
    public void setChoices(Map<String, Choices> choiceMap) {
        this.choiceMap = choiceMap;
    }

}