package com.swiggy.api.erp.crm.tests.ChatMiddleware;

import com.swiggy.api.erp.crm.dp.cancellationflows.CreateData;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.DBHelper;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import framework.gameofthrones.JonSnow.XMLValidator;


public class CmAuthorization{
	
	Initialize gameofthrones = new Initialize();
	DBHelper dbHelper = new DBHelper();
	
	HashMap<String,String> layerUserIdToken = new HashMap<String,String>();
	HashMap<String,String> layerUserConversation= new HashMap<String,String>();
	
	@Test(dataProviderClass=CreateData.class, dataProvider = "user credentials", enabled=true,description = "Test whether the given agent ois authorised")
	public void authenticateUser(HashMap<Integer,String[]> data)
	{
		String[] userdata;

		HashMap<String, String> requestheaders_authentication = new HashMap<String, String>();
		
		for(int i=1;i<=data.size();i++)
		{	
		
		userdata=data.get(i);
		
		String layerUserId,layerSessionToken;
		requestheaders_authentication.put("Content-Type", "application/json");
		requestheaders_authentication.put("userId", userdata[0]);
		requestheaders_authentication.put("username", userdata[1]);
    	
        GameOfThronesService authenticate_user = new GameOfThronesService("chatmiddlewear", "authenticate", gameofthrones);
        Processor authenticate_response = new Processor(authenticate_user, requestheaders_authentication);
       
        
        int http_status=authenticate_response.ResponseValidator.GetResponseCode();
        
        Assert.assertEquals(http_status, 200);
        layerUserId=authenticate_response.ResponseValidator.GetNodeValue("profile.layerUserId");

        
    	if(layerUserId!=null)
    	{
    		layerSessionToken = authenticate_response.ResponseValidator.GetNodeValue("profile.layerSessionToken");
    		
    		
    		if(layerSessionToken!=null)
    		{
    			layerUserIdToken.put(layerUserId, layerSessionToken);        			
    		}
       	}
    	
    	else
    		System.out.println("layer User Id not found");
    	     
		}	 
		
	}
	
}
