package com.swiggy.api.erp.finance.tests;

import com.swiggy.api.erp.cms.helper.PriceParityHelper;
import com.swiggy.api.erp.ff.helper.LOSHelper;
import com.swiggy.api.erp.finance.constants.FinanceConstants;
import com.swiggy.api.erp.finance.dp.FinanceRegressionDP;
import com.swiggy.api.erp.finance.helper.CashMgmtRegressionHelper;
import com.swiggy.api.erp.finance.helper.FinanceHelper;
import com.swiggy.api.erp.finance.helper.finHelper;
import framework.gameofthrones.JonSnow.DateHelper;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.ExcelHelper;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.erp.finance.tests
 **/
public class FinanceRegression extends FinanceRegressionDP {
    FinanceHelper helper = new FinanceHelper();
    finHelper finHelper =  new finHelper();
    LOSHelper losHelper = new LOSHelper();
    DateHelper dateHelper = new DateHelper();
    CashMgmtRegressionHelper cashMgmtRegressionHelper = new CashMgmtRegressionHelper();
    PriceParityHelper priceParityHelper = new PriceParityHelper();
    ExcelHelper excelHelper = new ExcelHelper();
    private static Logger log = LoggerFactory.getLogger(FinanceRegression.class);


    @BeforeClass
    public void createRestaurants() {
        HashMap<String, String> hm = createAllRestaurants();
        prepaid_restId_old_mou = hm.get("prepaid_old_mou");
        prepaid_restId_new_mou = hm.get("prepaid_new_mou");
        postpaid_restId_new_mou = hm.get("postpaid_new_mou");
        postpaid_restId_old_mou = hm.get("postpaid_old_mou");
        log.info("--RestID_prepaid_old_mou ::: "+prepaid_restId_old_mou);
        log.info("--RestID_prepaid_new_mou ::: "+prepaid_restId_new_mou);
        log.info("--RestID_postpaid_new_mou ::: "+postpaid_restId_new_mou);
        log.info("--RestID_postpaid_old_mou ::: "+postpaid_restId_old_mou);
    }

    @Test(invocationCount = 1, dataProvider = "autoReconciliationDeliveredOrderDP",groups = {"Finance", "FinanceRegression", "Fin"},description = "Auto Reconciliation - Delivered Order")
    public void validateAutoReconciliationDeliveredOrder(String orderID, String orderTime) throws IOException, InterruptedException {
        log.info("--OrderID ::: "+orderID+" && OrderTime ::: "+orderTime);
        SoftAssert softAssert = new SoftAssert();
        Processor p = finHelper.getReconOrder(orderID);
        String get_orderStatus = p.ResponseValidator.GetNodeValue("$.order_status");
        if (get_orderStatus.equals("processing") || get_orderStatus.equals("relayed"))
            softAssert.assertTrue(true);
        else
            softAssert.assertTrue(false, "order status not in relayed or processing");
        for (int i = 0; i < 6 ; i++) {
            losHelper.statusUpdate(orderID, orderTime, statusList[i]);

            //::to do order status update in db for each update

            log.info("--Order Status ::: "+statusList[i]);
        }
       boolean recon_raw_order = helper.validateOrderAvailableInReconRawOrderFromDB(orderID);
        softAssert.assertEquals(recon_raw_order,true, "recon_raw_order not true");
        boolean recon_raw_order_details = helper.validateOrderAvailableInReconRawOrderDetailsFromDB(orderID);
        softAssert.assertEquals(recon_raw_order_details,true,"recon_raw_order_details not true");
        boolean recon_De_dm_entries = helper.validateOrderEntryInReconDeDmEntriesFromDB(orderID);
        softAssert.assertEquals(recon_De_dm_entries, true, "recon_De_dm_entries not true");
        String autoreconStatus0 = helper.getAutoReconStatusFromDB(orderID);
        softAssert.assertEquals(autoreconStatus0, "1","autoreconStatus not 0");
        boolean reconcilitionStatus = helper.getReconciliationStatusFromDB(orderID);
        softAssert.assertEquals(reconcilitionStatus,true,"reconcilitionStatus not equals true");
        String autoreconstatus3 = helper.getAutoReconStatusFromDB(orderID);
        softAssert.assertEquals(autoreconstatus3,"1","autoreconstatus3 not equal to 3");
        boolean reconOrderActuals = helper.validateOrderAvailableInReconOrderActualsFromDB(orderID);
        softAssert.assertEquals(reconOrderActuals,true,"reconOrderActuals not true" );
        Processor p1 = finHelper.getReconOrder(orderID);
        String get_recon_Status = p1.ResponseValidator.GetNodeValue("$.recon_status.status");
        String orderStatus = p1.ResponseValidator.GetNodeValue("$.order_status");
        softAssert.assertEquals(get_recon_Status, FinanceConstants.recon_status_auto_reconciled,"get_recon_status not equal to autoreconciled");
        softAssert.assertEquals(orderStatus,"delivered", "order status is not delivered");
        softAssert.assertAll();
    }



    @Test(dataProvider ="OrderJsonsDP",groups = {"Finance", "FinanceRegression", "Fin"},description = "Auto Reconciliation - Delivered Order")
    public void createdDeliveredOrderFinance(String orderID, String orderTime) throws IOException, InterruptedException {
        log.info("--OrderID ::: "+orderID+" && OrderTime ::: "+orderTime);
        SoftAssert softAssert = new SoftAssert();

        Processor p = finHelper.getReconOrder(orderID);
        String get_orderStatus = p.ResponseValidator.GetNodeValue("$.order_status");
        boolean recon_raw_order = helper.validateOrderAvailableInReconRawOrderFromDB(orderID);
        softAssert.assertEquals(recon_raw_order,true, "recon_raw_order not true");
        String order_in_master=helper.validateOrderStatusInMasterFromDB(orderID);
        Assert.assertNotEquals(order_in_master,"0","order id is not present in master");
        softAssert.assertEquals(get_orderStatus,"delivered", "order status is not delivered");

        softAssert.assertAll();

    }

    @Test(dataProvider ="CancelOrderJsonsDP",groups = {"Finance", "FinanceRegression", "Fin"},description = "Auto Reconciliation - Delivered Order")
    public void createdCancelledOrderFinance(String orderID, String orderTime) throws IOException, InterruptedException {
        log.info("--OrderID ::: "+orderID+" && OrderTime ::: "+orderTime);
        SoftAssert softAssert = new SoftAssert();
        Thread.sleep(10000);
        Processor p = finHelper.getReconOrder(orderID);
        String get_orderStatus = p.ResponseValidator.GetNodeValue("$.order_status");
        boolean recon_raw_order = helper.validateOrderAvailableInReconRawOrderFromDB(orderID);
        softAssert.assertEquals(recon_raw_order,true, "recon_raw_order not true");
        String order_in_master=helper.validateOrderStatusInMasterFromDB(orderID);
        Assert.assertNotEquals(order_in_master,"0","order id is not present in master");

        softAssert.assertEquals(get_orderStatus,"cancelled", "order status is not Cancelled");
     /*   String autoreconStatus0 = helper.getAutoReconStatusFromDB(orderID);
        softAssert.assertEquals(autoreconStatus0, "1","autoreconStatus not 0");*/
        softAssert.assertAll();

    }

    @Test(dataProvider ="DP_FOR_POP_SUPER_NORMAL",groups = {"Finance", "FinanceRegression", "Fin"},description = "Auto Reconciliation - Delivered Order")
    public void createdDeliveredOrderFinancePOPSuperNormal(String orderID, String orderTime) throws IOException, InterruptedException {
        log.info("--OrderID ::: "+orderID+" && OrderTime ::: "+orderTime);
        SoftAssert softAssert = new SoftAssert();
        Thread.sleep(4000);
        Processor p = finHelper.getReconOrder(orderID);
        String get_orderStatus = p.ResponseValidator.GetNodeValue("$.order_status");
        boolean recon_raw_order = helper.validateOrderAvailableInReconRawOrderFromDB(orderID);
        softAssert.assertEquals(recon_raw_order,true, "recon_raw_order not true");
        String order_in_master=helper.validateOrderStatusInMasterFromDB(orderID);
        Assert.assertNotEquals(order_in_master,"0","order id is not present in master");
        softAssert.assertEquals(get_orderStatus,"delivered", "order status is not delivered");
        softAssert.assertAll();

    }

    @Test(dataProvider ="DP_FOR_CANCEL_POP_SUPER_NORMAL",groups = {"Finance", "FinanceRegression", "Fin"},description = "Auto Reconciliation - Delivered Order")
    public void createdCancelledOrderFinancePOPSuperNormal(String orderID, String orderTime) throws IOException, InterruptedException {
        log.info("--OrderID ::: "+orderID+" && OrderTime ::: "+orderTime);
        SoftAssert softAssert = new SoftAssert();
        Thread.sleep(4000);
        Processor p = finHelper.getReconOrder(orderID);
        String get_orderStatus = p.ResponseValidator.GetNodeValue("$.order_status");
        boolean recon_raw_order = helper.validateOrderAvailableInReconRawOrderFromDB(orderID);
        softAssert.assertEquals(recon_raw_order,true, "recon_raw_order not true");
        String order_in_master=helper.validateOrderStatusInMasterFromDB(orderID);
        Assert.assertNotEquals(order_in_master,"0","order id is not present in master");
        softAssert.assertEquals(get_orderStatus,"cancelled", "order status is not Cancelled");
        softAssert.assertAll();

    }


    @Test(invocationCount = 1, dataProvider = "autoReconciliationCancelledOrderNewMouDP", groups = {"Finance", "FinanceRegression", "Fin"},description = "Auto Reconciliation Food Prepared True - Cancelled Order After Placed - New MOU")
    public void validateAutoReconciliationCancelledOrderNewMOU(String orderID, String orderTime) throws IOException, InterruptedException {
        SoftAssert softAssert = new SoftAssert();
        log.info("--OrderID ::: "+orderID+" && OrderTime ::: "+orderTime);
        helper.placedRMSUpdate(orderID);
        Processor p = finHelper.getReconOrder(orderID);
        String get_orderStatus = p.ResponseValidator.GetNodeValue("$.order_status");
        softAssert.assertEquals(get_orderStatus,"placed", "order status not placed");
        helper.cancelOrderTests(orderID,"true",FinanceConstants.responsible_id_customer);
        Thread.sleep(10000);
        Processor p1 = finHelper.getReconOrder(orderID);
        String orderStatus = p1.ResponseValidator.GetNodeValue("$.order_status");
        softAssert.assertEquals(orderStatus,"cancelled", "order status not cancelled");
        boolean recon_raw_order = helper.validateOrderAvailableInReconRawOrderFromDB(orderID);
       softAssert.assertEquals(recon_raw_order,true, "recon_raw_order not true");
        boolean recon_raw_order_details = helper.validateOrderAvailableInReconRawOrderDetailsFromDB(orderID);
        softAssert.assertEquals(recon_raw_order_details,true,"recon_raw_order_details not true");
        boolean reconcilitionStatus = helper.getReconciliationStatusFromDB(orderID);
        if(helper.validateCashReconStatus(orderID)&&helper.validateBillReconStatus(orderID)) {
            softAssert.assertEquals(reconcilitionStatus, true, "reconcilitionStatus not equals true");
        }
        else
        {
            softAssert.assertEquals(reconcilitionStatus, false, "reconcilitionStatus not equals false");
        }
        String autoreconStatus1 = helper.getAutoReconStatusFromDB(orderID);
        softAssert.assertEquals(autoreconStatus1, "1","autoreconStatus not 1");
        boolean reconOrderActuals = helper.validateOrderAvailableInReconOrderActualsFromDB(orderID);
        softAssert.assertEquals(reconOrderActuals,true,"reconOrderActuals not true" );
        softAssert.assertAll();
    }


    @Test(invocationCount = 1, dataProvider = "autoReconciliationCancelledOrderOldMouDP",groups = {"Finance", "FinanceRegression", "Fin"},description = "Auto Reconciliation food prepared true - Cancelled Order Once Placed - Old MOU")
    public void validateAutoReconciliationCancelledOrderForOldMOU(String orderID, String orderTime) throws IOException, InterruptedException {
        SoftAssert softAssert = new SoftAssert();
        log.info("--OrderID ::: "+orderID+" && OrderTime ::: "+orderTime);
        helper.placedRMSUpdate(orderID);
        Processor p = finHelper.getReconOrder(orderID);
        String get_orderStatus = p.ResponseValidator.GetNodeValue("$.order_status");
        softAssert.assertEquals(get_orderStatus,"placed", "order status not placed");
        helper.cancelOrderTests(orderID,"true",FinanceConstants.responsible_id_swiggy);
        Thread.sleep(10000);
        Processor p1 = finHelper.getReconOrder(orderID);
        String orderStatus = p1.ResponseValidator.GetNodeValue("$.order_status");
        softAssert.assertEquals(orderStatus,"cancelled", "order status not cancelled");
        boolean recon_raw_order = helper.validateOrderAvailableInReconRawOrderFromDB(orderID);
        softAssert.assertEquals(recon_raw_order,true, "recon_raw_order not true");
        boolean recon_raw_order_details = helper.validateOrderAvailableInReconRawOrderDetailsFromDB(orderID);
        softAssert.assertEquals(recon_raw_order_details,true,"recon_raw_order_details not true");
        boolean reconcilitionStatus = helper.getReconciliationStatusFromDB(orderID);
        if(helper.validateCashReconStatus(orderID)&&helper.validateBillReconStatus(orderID)) {
            softAssert.assertEquals(reconcilitionStatus, true, "reconcilitionStatus not equals true");
        }
        else
        {
            softAssert.assertEquals(reconcilitionStatus, false, "reconcilitionStatus not equals false");
        }
        String autoreconStatus1 = helper.getAutoReconStatusFromDB(orderID);
        softAssert.assertEquals(autoreconStatus1, "1","autoreconStatus not 1");
        boolean reconOrderActuals = helper.validateOrderAvailableInReconOrderActualsFromDB(orderID);
        softAssert.assertEquals(reconOrderActuals,true,"reconOrderActuals not true" );
        softAssert.assertAll();
    }

    @Test(dataProvider = "payoutDP",groups = {"Finance, FinanceRegression", "Fin"},description = "validate Payout")
    public void validatePayout(String orderID, String orderTime) {
        SoftAssert softAssert = new SoftAssert();
        log.info("--OrderID ::: "+orderID+" && OrderTime ::: "+orderTime);
        String payoutID = "";
        String id = "";
        String to = dateHelper.getDateTimePlusOrMinusCurrentDate_yyyy_mm_dd_hh_mm_ss(0);
        String from = dateHelper.getDateTimePlusOrMinusCurrentDate_yyyy_mm_dd_hh_mm_ss(-3);
        log.info("--From ::: "+from+" --To ::: "+to);
        Processor p = finHelper.getCurrentPayout(from,to,FinanceConstants.user_id,FinanceConstants.city_id);
        int get_status = p.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String get_message = p.ResponseValidator.GetNodeValue("$.statusMessage");
        softAssert.assertEquals(get_status,FinanceConstants.statusOne, "Status not one");
        softAssert.assertEquals(get_message,FinanceConstants.msg_job_created, "msg_job_created not same");
        String taskID = helper.getTaskIdFromReconCurrentPayout(from,to);
        boolean restaurant_current_payout = helper.validateEntryInRestuarantCurrentPayoutFromDB(taskID);
        softAssert.assertEquals(restaurant_current_payout,false, "entry in restaurant_current_payout is not false");
        String recon_current_payout_jobs = helper.validateStatusFromReconCurrentPayoutJobsFromDB(taskID);
        if (recon_current_payout_jobs.equals("0") || recon_current_payout_jobs.equals("1"))
            softAssert.assertTrue(true);
        else
            softAssert.assertTrue(false, "recon_current_payout_jobs status not 0 or 1");
        boolean pollstatus = helper.pollStatusFromReconCurrentPayout(from,to,"2");
        if (pollstatus == true) {
            payoutID = helper.getPayoutIdFromRestaurantCurrentPayouts(taskID);
            id = helper.getIdFromRestaurantCurrentPayoutDetails(payoutID);
        } else
            softAssert.assertTrue(false, "Status in recon invoice jobs was not 2 (status=done)");
        log.info("--taskID ::: "+taskID);
        log.info("--payoutID ::: "+payoutID);
        log.info("--id ::: "+id);
        boolean restaurant_current_payout_detail = helper.validateEntryInRestuarantCurrentPayoutDetailsFromDB(payoutID);
        softAssert.assertEquals(restaurant_current_payout_detail,true, "entry in restaurant_current_payout_detail is not false");
        HashMap<String, String> hm = helper.getCommissionAndRestaurantEarningReconOrderActualsFromDB(orderID);
        HashMap<String, String> hm_master = helper.getCommissionAndRestaurantEarningMasterFromDB(orderID);
        softAssert.assertNotEquals(hm.get("commission"),"","commission value does not exists");
        softAssert.assertNotEquals(hm_master.get("swiggy_commission"),"","swiggy_commission value does not exits");
        softAssert.assertNotEquals(hm.get("restaurant_earnings"),"","commission value does not exists");
        softAssert.assertNotEquals(hm_master.get("restaurant_earnings"),"","swiggy_commission value does not exits");
        boolean restaurant_current_payouts = helper.validateEntryInRestuarantCurrentPayoutFromDB(taskID);
        softAssert.assertEquals(restaurant_current_payouts,true, "restaurant_current_payouts not true");
        boolean restaurant_current_payouts_details = helper.validateEntryInRestuarantCurrentPayoutDetailsFromDB(payoutID);
        softAssert.assertEquals(restaurant_current_payouts_details,true, "restaurant_current_payouts_details not true");
        String recon_current_payout_job = helper.validateStatusFromReconCurrentPayoutJobsFromDB(taskID);
        softAssert.assertEquals(recon_current_payout_job,"2","recon_current_payout_job not equal to 2");


        softAssert.assertAll();
    }

    @Test(dataProvider = "invoiceDP",groups = {"Finance", "FinanceRegression", "Fin"},description = "validate Invoice")
    public void validateInvoice(String orderID, String restID) throws IOException, InvalidFormatException {
        SoftAssert softAssert = new SoftAssert();
        List<Map<String,Object>> list, list1;
        log.info("--OrderID ::: "+orderID);
        boolean flag = false;
        String payoutID = "";
        String id="";
        String to = dateHelper.getDateTimePlusOrMinusCurrentDate_yyyy_mm_dd_hh_mm_ss(0);
        String from = dateHelper.getDateTimePlusOrMinusCurrentDate_yyyy_mm_dd_hh_mm_ss(-1);
        log.info("--From ::: "+from+" --To ::: "+to);
        Processor p = finHelper.getCurrentPayout(from,to,FinanceConstants.user_id,FinanceConstants.city_id);
        int get_status = p.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        softAssert.assertEquals(get_status,FinanceConstants.statusOne, "Status not one");
        String taskID = helper.getTaskIdFromReconCurrentPayout(from,to);
        boolean pollstatus = helper.pollStatusFromReconCurrentPayout(from,to,"2");
        if (pollstatus == true) {
            payoutID = helper.getPayoutIdFromRestaurantCurrentPayouts(taskID);
            id = helper.getIdFromRestaurantCurrentPayoutDetails(payoutID);
        } else
            softAssert.assertTrue(false, "Status in recon invoice jobs was not 2 (status=done)");
        log.info("--taskID ::: "+taskID);
        log.info("--payoutID ::: "+payoutID);
        Processor p1 = helper.createInvoiceJob(from,to,FinanceConstants.user_id,taskID,FinanceConstants.city_id);
        int get_status1 = p1.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String get_message = p1.ResponseValidator.GetNodeValue("$.statusMessage");
        softAssert.assertEquals(get_status1,FinanceConstants.statusOne, "Status not one");
        softAssert.assertEquals(get_message,FinanceConstants.msg_invoice_job_created, "msg_invoice_job_created not same");
        String invoice_main_taskID = helper.getInvoiceTaskIdFromReconInvoiceJobs();
        log.info("--invoice_main_taskID ::: "+invoice_main_taskID);
        //String invoice_main_taskID = "9";
        String invoice_status = helper.getStatusFromReconInvoiceJobs(invoice_main_taskID);
        System.out.println("=======================invoice status: "+invoice_status);
        softAssert.assertEquals(invoice_status,"0", "invoice_status not equal to 0 i.e. pending");
        list = helper.getDetailsFromSubtask(invoice_main_taskID);
        System.out.println(list);
        for (int i = 0; i < list.size() ; i++) {
            if (list.get(i).get("status").toString().equals("0") || list.get(i).get("status").toString().equals("1") || list.get(i).get("status").toString().equals("2"))
                softAssert.assertTrue(true);
            else
            softAssert.assertTrue(false, "subtask status is not in state 0/1/2 for i:"+i+" & id:"+list.get(i).get("id").toString());
        }
        helper.pollDBForStatusFromReconInvoiceJobs(invoice_main_taskID,"2");
        list1 = helper.getDetailsFromReconInvoices(invoice_main_taskID);
//        softAssert.assertEquals(list1.size(),list.size(), "size of sub task not equal to size of recon invoices");
//        for (int i = 0; i < list1.size() ; i++) {
//            softAssert.assertEquals(list.get(i).get("id").toString(),list1.get(i).get("id").toString(),"recon invoice id value not same for id:"+list1.get(i).get("id").toString());
//        }
        log.info("--===> list(subtask): "+list.size() + " --===>list1(recon_invoice): "+list1.size());
        softAssert.assertNotEquals(list.size(),0,"list(subtask) has size 0");
        softAssert.assertNotEquals(list1.size(),0,"list1(recon_invoice) has size 0");
        HashMap<String, List<String>> hm = helper.getInvoiceUrlAnnexureUrlFromReconInvoices(invoice_main_taskID);
        for (int i = 0; i < hm.get("invoice_url").size(); i++) {
            System.out.println("=================> yay I got the URLs");
            softAssert.assertNotEquals(hm.get("invoice_url").get(i),"", "invoice_url is empty at i:"+i);
            priceParityHelper.downLoadFromS3(hm.get("annexure_url").get(i),FinanceConstants.file_path_xlsx);
            List<Map<String, String>> parsedExcel = excelHelper.getExcelList(FinanceConstants.file_path_xlsx,"Annexure A",1,15,80);
            int count = 0 ;
            log.info("--OrderID ::: "+orderID);
            log.info("--RestID ::: "+restID);
            while (count < (parsedExcel.size())) {
                log.info("count: "+count+"  ------> "+parsedExcel.get(count));
                if ((parsedExcel.get(count).get("Restaurant Id").equals(restID)) && parsedExcel.get(count).get("Order No").equals(orderID)) {
                    flag = true;
                    softAssert.assertEquals(parsedExcel.get(count).get("Order No"), orderID, "RestId found but corresponding order ID not found");
                    log.info("--Rest ID from xlsx ::: "+ parsedExcel.get(count).get("Restaurant Id")+ "  --OrderID from xlsx ::: "+parsedExcel.get(count).get("Order No"));
                    log.info("--Found the value in file ::: "+hm.get("annexure_url").get(i));
                }
                ++count;
            }
            log.info ("--count ::: "+count);
        }
        if (flag)
            softAssert.assertTrue(true);
        else
            softAssert.assertTrue(false,"Could not find the order no. & rest id in all the downloaded files");
        cashMgmtRegressionHelper.deleteAllCreatedTmpFiles(FinanceConstants.file_path_xlsx);
        boolean invoice_status1 = helper.pollDBForStatusFromReconInvoiceJobs(invoice_main_taskID,"2");
        softAssert.assertTrue(invoice_status1, "invoice_status1 not equal to 2, poll db false");
        cashMgmtRegressionHelper.deleteAllCreatedTmpFiles(FinanceConstants.file_path_xlsx);
        softAssert.assertAll();

    }

    @Test(dataProvider = "masterFileDP",groups = {"Finance", "FinanceRegression", "Fin"},description = "validate Master File")
    public void validateMasterFile(String from, String to, String orderID) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        log.info("--OrderID ::: "+orderID);
        String payoutID = "";
        String to_payout = dateHelper.getDateTimePlusOrMinusCurrentDate_yyyy_mm_dd_hh_mm_ss(0);
        String from_payout = dateHelper.getDateTimePlusOrMinusCurrentDate_yyyy_mm_dd_hh_mm_ss(-1);
        Processor p1 = finHelper.getCurrentPayout(from_payout,to_payout,FinanceConstants.user_id,FinanceConstants.city_id);
        int get_status = p1.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        softAssert.assertEquals(get_status,FinanceConstants.statusOne, "Status not one");
        String taskID = helper.getTaskIdFromReconCurrentPayout(from_payout,to_payout);
        boolean pollstatus = helper.pollStatusFromReconCurrentPayout(from_payout,to_payout,"2");
        if (pollstatus == true) {
            payoutID = helper.getPayoutIdFromRestaurantCurrentPayouts(taskID);
        } else
            softAssert.assertTrue(false, "Status in recon current payout was not 2 (status=done)");
        log.info("--taskID ::: "+taskID);
        log.info("--payoutID ::: "+payoutID);
        Processor p = helper.generateMasterFile(from,to,FinanceConstants.city_id,FinanceConstants.user_id,"0");
        int status = p.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String msg = p.ResponseValidator.GetNodeValue("$.statusMessage");
        softAssert.assertEquals(status,FinanceConstants.statusZero, "Status is not 0");
        softAssert.assertEquals(msg,FinanceConstants.msg_successfully_added_job, "msg_successfully_added_job not as expected");
        HashMap<String, String> hashmap = helper.getCommissionAndRestaurantEarningMasterFromDB(orderID);
        String swiggy_commission = hashmap.get("swiggy_commission");
        log.info("--swiggy_commission ::: "+swiggy_commission);
        String id = helper.getIDFromMasterFileJob();
        softAssert.assertNotEquals(id, "-1", "list is empty so returned value is -1, it should be valid id");
        List<Map<String, Object>> list1 = helper.getDetailsFromMasterFileJob(id);
        softAssert.assertEquals(list1.get(0).get("status").toString(),"2", "master recon job status is not 2");
        String url = list1.get(0).get("file_url").toString();
        priceParityHelper.downLoadFromS3(url,FinanceConstants.file_path_csv);
        HashMap<String, String[]> hm = helper.parseCSVFileToHashMap(FinanceConstants.file_path_csv);
        String[] vals = hm.get(orderID);
        log.info("--swiggy commission in csv file ::: "+vals[49]);
        softAssert.assertEquals(vals[49],swiggy_commission, "swiggy_commission not same in the csv file for the order id:"+orderID);
        cashMgmtRegressionHelper.deleteAllCreatedTmpFiles(FinanceConstants.file_path_csv);
        softAssert.assertAll();

    }
    
    @Test(dataProvider = "autoReconciliationDeliveredOrderDP",groups = {"Finance", "FinanceRegression", "Fin"},description = "Auto Reconciliation - DB assert for Delivered Order")
    public void assertOrderStatusInDB(String orderID, String orderTime) throws IOException, InterruptedException
    {
    	 log.info("--OrderID ::: "+orderID+" && OrderTime ::: "+orderTime);
         SoftAssert softAssert = new SoftAssert();
         Processor p = finHelper.getReconOrder(orderID);
         String get_orderStatus = p.ResponseValidator.GetNodeValue("$.order_status");
         if (get_orderStatus.equals("processing") || get_orderStatus.equals("relayed"))
             softAssert.assertTrue(true);
         else
             softAssert.assertTrue(false, "order status not in relayed or processing");
         for (int i = 0; i <statusList.length  ; i++) {
             losHelper.statusUpdate(orderID, orderTime, statusList[i]); //update status till status = picked
             log.info("--Order Status ::: "+statusList[i]);
             String status=helper.validateOrderStatusInReconRawOrderFromDB(orderID);
             String status2=helper.validateOrderStatusInMasterFromDB(orderID);

             softAssert.assertEquals(statusList[i],status, "status update failed");
             softAssert.assertEquals(statusList[i],status2, "status update failed");    
         }  
    	
    }
    
    
    
    @Test(dataProvider = "autoReconciliationDeliveredOrderDP",groups = {"Finance", "FinanceRegression", "Fin"},description = "Auto Reconciliation - DB assert for Delivered Order")
    public void assertPayoutsInDB(String orderID, String orderTime) throws IOException, InterruptedException
    {
    	 log.info("--OrderID ::: "+orderID+" && OrderTime ::: "+orderTime);
         SoftAssert softAssert = new SoftAssert();
         Processor p = finHelper.getReconOrder(orderID);
         String get_orderStatus = p.ResponseValidator.GetNodeValue("$.order_status");
         if (get_orderStatus.equals("processing") || get_orderStatus.equals("relayed"))
             softAssert.assertTrue(true);
         else
             softAssert.assertTrue(false, "order status not in relayed or processing");
         for (int i = 0; i <statusList.length  ; i++) {
             losHelper.statusUpdate(orderID, orderTime, statusList[i]); //update status till status = picked
             log.info("--Order Status ::: "+statusList[i]);
             String status=helper.validateOrderStatusInReconRawOrderFromDB(orderID);
             String status2=helper.validateOrderStatusInMasterFromDB(orderID);

             softAssert.assertEquals(statusList[i],status, "status update failed");
             softAssert.assertEquals(statusList[i],status2, "status update failed");    
         }  
    }

    
    @Test(invocationCount = 1, dataProvider = "replicatedOrder", groups = {"Finance", "FinanceRegression", "Fin"},description = "Auto Reconciliation Replicated Order")
    public void validateAutoReconciliationReplicatedOrder(String orderID,String orderID2) throws IOException, InterruptedException {
    	System.out.println("order1"+orderID +"Order2"+orderID2);
        SoftAssert softAssert = new SoftAssert();
        String filePath="../Data/Payloads/JSON/fin_replicated.json";
        String orderTime="";
        log.info("--OrderID ::: "+orderID+" && OrderTime ::: "+orderTime);
        Processor p = finHelper.getReconOrder(orderID);
        Thread.sleep(1000);
        helper.cancelOrderTests(orderID,"true",FinanceConstants.responsible_id_swiggy);
        Processor p2 = finHelper.getReconOrder(orderID2);
        for (int i = 0; i < 6 ; i++) {
            losHelper.statusUpdate(orderID2, orderTime, statusList[i]); //update status till status = picked
            log.info("--Order Status ::: "+statusList[i]);
        }
        String test= helper.createReplicatedOrder(orderID,orderID2,filePath);
        boolean recon_raw_order = helper.validateOrderAvailableInReconRawOrderFromDB(orderID2);
        softAssert.assertEquals(recon_raw_order,true, "recon_raw_order not true");
        boolean recon_raw_order_details = helper.validateOrderAvailableInReconRawOrderDetailsFromDB(orderID2);
        softAssert.assertEquals(recon_raw_order_details,true,"recon_raw_order_details not true");
        boolean recon_De_dm_entries = helper.validateOrderEntryInReconDeDmEntriesFromDB(orderID2);
        softAssert.assertEquals(recon_De_dm_entries, true, "recon_De_dm_entries not true");
        String autoreconStatus1 = helper.getAutoReconStatusFromDB(orderID2);
        softAssert.assertEquals(autoreconStatus1, "1","autoreconStatus not 1");
        boolean reconcilitionStatus = helper.getReconciliationStatusFromDB(orderID2);
        softAssert.assertEquals(reconcilitionStatus,true,"reconcilitionStatus not equals true");
        String autoreconstatus3 = helper.getAutoReconStatusFromDB(orderID);
        softAssert.assertEquals(autoreconstatus3,"1","autoreconstatus3 not equal to 3");
        boolean reconOrderActuals = helper.validateOrderAvailableInReconOrderActualsFromDB(orderID2);
        softAssert.assertEquals(reconOrderActuals,true,"reconOrderActuals not true" );
        Processor p1 = finHelper.getReconOrder(orderID2);
        String get_recon_Status = p1.ResponseValidator.GetNodeValue("$.recon_status.status");
        String orderStatus = p1.ResponseValidator.GetNodeValue("$.order_status");
        softAssert.assertEquals(get_recon_Status, FinanceConstants.recon_status_auto_reconciled,"get_recon_status not equal to autoreconciled");
        softAssert.assertEquals(orderStatus,"delivered", "order status is not delivered");
       //softAssert.assertAll();
    
    }

    @Test(dataProvider = "zombieOrderDP")
    public void  zombieOrderCreation(String orderID, String orderTime) throws IOException, InterruptedException {
        log.info("--OrderID ::: "+orderID+" && OrderTime ::: "+orderTime);

        SoftAssert softAssert = new SoftAssert();
        Processor p = finHelper.getReconOrder(orderID);
        String get_orderStatus = p.ResponseValidator.GetNodeValue("$.order_status");
        if(Long.parseLong(orderID)%2==0)
            Assert.assertEquals("placed", get_orderStatus, "order status update fail");

        else
            Assert.assertEquals("pickedup", get_orderStatus, "order status update fail");
        String orderExist = helper.getMasterEntityFromDB("order_id",orderID);
        Assert.assertEquals("0",orderExist,"test fails ,order should not exist in master");



    }



   /* @AfterClass
    public void cleanUpAnyCreatedFile(){
        cashMgmtRegressionHelper.deleteAllCreatedTmpFiles(FinanceConstants.file_path_xlsx);
        cashMgmtRegressionHelper.deleteAllCreatedTmpFiles(FinanceConstants.file_path_csv);
    }*/
    


    


}
