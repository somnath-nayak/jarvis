package com.swiggy.api.erp.ff.helper;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;

import java.util.HashMap;

public class ReminderServiceHelper {
	
	Initialize gameofthrones = new Initialize();

	public Processor setReminder(String sendAt,String to, String from, String msg){
		HashMap<String, String> requestheaders = new HashMap<>();
		requestheaders.put("content-type", "application/json");
    	String[] payloadParams = {sendAt,to,from,msg};
    	GameOfThronesService service = new GameOfThronesService("omsreminderservice", "reminder", gameofthrones);
    	Processor processor = new Processor(requestheaders, payloadParams, null, null, service);
		return processor;
	}

	public Processor setBulkReminder(String sendAt,String to, String from, String msg){
		HashMap<String, String> requestheaders = new HashMap<>();
		requestheaders.put("content-type", "application/json");
    	String[] payloadParams = {sendAt,to,from,msg};
    	GameOfThronesService service = new GameOfThronesService("omsreminderservice", "bulkreminder", gameofthrones);
    	Processor processor = new Processor(requestheaders, payloadParams, null, null, service);
		return processor;
	}
	
	public Processor nullify(String reminderId){
		HashMap<String, String> requestheaders = new HashMap<>();
		requestheaders.put("content-type", "application/json");
    	String[] payloadParams = {reminderId};
    	GameOfThronesService service = new GameOfThronesService("omsreminderservice", "nullify", gameofthrones);
    	Processor processor = new Processor(requestheaders, payloadParams, null, null, service);
		return processor;
	}
	
}