package com.swiggy.api.erp.finance.tests;

import com.swiggy.api.erp.finance.dp.FinanceRegressionDP;
import com.swiggy.api.erp.finance.helper.NodalHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.HashMap;

public class NodalOrderCreation extends FinanceRegressionDP {
    NodalHelper nodalHelper=new NodalHelper();
    private static Logger log = LoggerFactory.getLogger(FinanceRegression.class);
    @BeforeClass

    public void createRestaurants() {
        HashMap<String, String> hm = createAllRestaurants();
        prepaid_restId_old_mou = hm.get("prepaid_old_mou");
        prepaid_restId_new_mou = hm.get("prepaid_new_mou");
        postpaid_restId_new_mou = hm.get("postpaid_new_mou");
        postpaid_restId_old_mou = hm.get("postpaid_old_mou");
        log.info("--RestID_prepaid_old_mou ::: "+prepaid_restId_old_mou);
        log.info("--RestID_prepaid_new_mou ::: "+prepaid_restId_new_mou);
        log.info("--RestID_postpaid_new_mou ::: "+postpaid_restId_new_mou);
        log.info("--RestID_postpaid_old_mou ::: "+postpaid_restId_old_mou);
    }

    @Test(dataProvider = "OrderJsonsDP")
    public void nodalOrderCreationWithMultiTDDelivered(String orderID, String orderTime) {
        System.out.println("current order_time ::: "+orderTime);
        String order_status = nodalHelper.getOrderStatusFromOrderTable("order_status", orderID);
        System.out.println("received order status is "+order_status);
        Assert.assertEquals(order_status, "3", "order status not matched");
    }
    @Test(dataProvider = "DP_FOR_POP_SUPER_NORMAL")
    public void nodalOrderCreationForSuperPOPOnlineDelivered(String orderID, String orderTime){
        System.out.println("current order_time ::: "+orderTime);
        String order_status = nodalHelper.getOrderStatusFromOrderTable("order_status", orderID);
        System.out.println("received order status is "+order_status);
        Assert.assertEquals(order_status, "3", "order status not matched");
    }

    @Test(dataProvider = "CancelOrderJsonsDP")
    public void nodalOrderCreationMultiTDCancelled(String orderID, String orderTime){
        System.out.println("current order_time ::: "+orderTime);
        String order_status = nodalHelper.getOrderStatusFromOrderTable("order_status", orderID);
        System.out.println("received order status is "+order_status);
        Assert.assertEquals(order_status, "4", "order status not matched");

    }
    @Test(dataProvider = "DP_FOR_CANCEL_POP_SUPER_NORMAL")
    public void nodalOrderCreationForSuperPOPOnlineCancelled(String orderID, String orderTime){
        System.out.println("current order_time ::: "+orderTime);
        String order_status = nodalHelper.getOrderStatusFromOrderTable("order_status", orderID);
        System.out.println("received order status is "+order_status);
        Assert.assertEquals(order_status, "4", "order status not matched");
    }





}
