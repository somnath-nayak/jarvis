package com.swiggy.api.erp.delivery.tests;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.erp.delivery.constants.RTSConstants;
import com.swiggy.api.erp.delivery.dp.Long_Distance_Batching_Data_Provider;
import com.swiggy.api.erp.delivery.helper.DeliveryDataHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryHelperMethods;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceHelper;
import com.swiggy.api.erp.delivery.helper.RTSHelper;

public class Long_Distance_Batching extends Long_Distance_Batching_Data_Provider {
	DeliveryHelperMethods delmeth = new DeliveryHelperMethods();
	DeliveryDataHelper deldata = new DeliveryDataHelper();
	DeliveryServiceHelper delserhel=new DeliveryServiceHelper();
	RTSHelper rtshelp = new RTSHelper();
	@BeforeClass
	public void enablelongdistancebatching() {
		delmeth.dbhelperupdate(RTSConstants.long_distance_batching_more_last_mile);
		delmeth.dbhelperupdate(RTSConstants.long_distance_batching_enabled_zone);
		delmeth.dbhelperupdate(RTSConstants.long_distance_batching_batch_enabled);
		String query = RTSConstants.enable_resturant_batching
				+ RTSConstants.long_distance_batching_rest_id;
		delmeth.dbhelperupdate(query);
		delmeth.dbhelperupdate(RTSConstants.set_random_de_id);
	}
	@AfterMethod
	public void closeallbatches()
	{
		delmeth.dbhelperupdate(RTSConstants.closeallexisitingbatches);
		delmeth.dbhelperupdate(RTSConstants.set_random_de_id);
	}
	
	@Test(dataProvider = "LDB_1", priority = 37, groups = "LDB", description = "Verify long distance orders are batched if it is satisfying other conditions and last mile of both orders is within last mile config")
	public void LDB_1(String order_id1, String order_id2)
			throws InterruptedException {
	validatequals(order_id1, order_id2);
	}
	@Test(dataProvider = "LDB_2", priority = 38, groups = "LDB", description = "Verify long distance orders are not batched if it is satisfying all other conditions and last mile of both orders is exceeding last mile config")
	public void LDB_2(String order_id1, String order_id2)
			throws InterruptedException {
		validatenotequals(order_id1, order_id2);
	}
	@Test(dataProvider = "LDB_3", priority = 39, groups = "LDB", description = "Verify long distance order is not batched with normal order even if all conditions are staisfied")
	public void LDB_3(String order_id1, String order_id2)
			throws InterruptedException {
         validatenotequals(order_id1, order_id2);
	}
	@Test(dataProvider = "LDB_4", priority = 40, groups = "LDB", description = "Verify long distance order is batched with exisitng long distance order to whom DE is assigned and DE has confirmed/arrived the order")
	public void LDB_4(String order_id1, String order_id2)
			throws InterruptedException {
		validatequals(order_id1, order_id2);
	}
	@Test(dataProvider = "LDB_5", priority = 41, groups = "LDB", description = "Verify long distance order is not batched with normal order to whom DE is assigned and DE has confirmed/arrived the order")
	public void LDB_5(String order_id1, String order_id2)
			throws InterruptedException {
		validatenotequals(order_id1, order_id2);
	}
	@Test(dataProvider = "LDB_6", priority = 42, groups = "LDB", description = "Verify long distance orders are not batched if it is satisfying all other conditions and last mile of one of the orders is exceeding last mile config")
	public void LDB_6(String order_id1, String order_id2)
			throws InterruptedException {
		validatenotequals(order_id1, order_id2);
	}
	@Test(dataProvider = "LDB_7", priority = 43, groups = "LDB", description = "Verify two long distance orders are not batched again if they were previuosly batched and batch was broken")
	public void LDB_7(String order_id1, String order_id2)
			throws InterruptedException {
		Assert.assertNotNull(order_id1,
				"Order is not created through pushing Order Json");
		Assert.assertNotNull(order_id2,
				"Order is not created through pushing Order Json");
		String query1 = DeliveryConstant.batch_id + order_id1 + ";";
		String query2 = DeliveryConstant.batch_id + order_id2 + ";";
		Object batch_id1 = delmeth.dbhelperget(query1, "batch_id");
		Object batch_id2 = delmeth.dbhelperget(query2, "batch_id");
		rtshelp.runassignment(RTSConstants.cityid);
		if (batch_id1 != null && batch_id2 != null) {
			Object batch_id3 = delmeth.dbhelperget(query1, "batch_id");
			Object batch_id4 = delmeth.dbhelperget(query2, "batch_id");
			if (((batch_id3.toString()).equalsIgnoreCase(batch_id4.toString()))) 
			{
				delserhel.assignOrder(order_id1, RTSConstants.de_id1);
				delserhel.systemRejectOrder(RTSConstants.de_id1, order_id1);
				rtshelp.runassignment(RTSConstants.cityid);
				Thread.sleep(10000);
				Object batch_id5 = delmeth.dbhelperget(query1, "batch_id");
				Object batch_id6 = delmeth.dbhelperget(query2, "batch_id");
				Assert.assertNotEquals(batch_id5.toString(), batch_id6.toString());
			} else {

				Assert.assertTrue(false);
			}
		}

		else {
			System.out.println("Batch id not present in Batch table");
			Assert.assertTrue(false);
		}
	}
	public void validatequals(Object order_id1,Object order_id2)
	{
		Assert.assertNotNull(order_id1,
				"Order is not created through pushing Order Json");
		Assert.assertNotNull(order_id2,
				"Order is not created through pushing Order Json");
		String query1 = DeliveryConstant.batch_id + order_id1 + ";";
		String query2 = DeliveryConstant.batch_id + order_id2 + ";";
		Object batch_id1 = delmeth.dbhelperget(query1, "batch_id");
		Object batch_id2 = delmeth.dbhelperget(query2, "batch_id");
		rtshelp.runassignment(RTSConstants.cityid);
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (batch_id1 != null && batch_id2 != null) {
			Object batch_id3 = delmeth.dbhelperget(query1, "batch_id");
			Object batch_id4 = delmeth.dbhelperget(query2, "batch_id");
			Assert.assertEquals(batch_id3.toString(), batch_id4.toString());
			}
		else {
			System.out.println("Batch id not present in Batch table");
			Assert.assertTrue(false);
		}
	}
		public void validatenotequals(Object order_id1,Object order_id2)
		{
			Assert.assertNotNull(order_id1,
					"Order is not created through pushing Order Json");
			Assert.assertNotNull(order_id2,
					"Order is not created through pushing Order Json");
			String query1 = DeliveryConstant.batch_id + order_id1 + ";";
			String query2 = DeliveryConstant.batch_id + order_id2 + ";";
			Object batch_id1 = delmeth.dbhelperget(query1, "batch_id");
			Object batch_id2 = delmeth.dbhelperget(query2, "batch_id");
			rtshelp.runassignment(RTSConstants.cityid);
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (batch_id1 != null && batch_id2 != null) {
				Object batch_id3 = delmeth.dbhelperget(query1, "batch_id");
				Object batch_id4 = delmeth.dbhelperget(query2, "batch_id");
				Assert.assertNotEquals(batch_id3.toString(), batch_id4.toString());
				}
			else {
				System.out.println("Batch id not present in Batch table");
				Assert.assertTrue(false);
			}
	}
	@AfterClass
	public void disablelongdistanceBatching() {
		delmeth.dbhelperupdate(RTSConstants.long_distance_batching_disabled_zone);
		delmeth.dbhelperupdate(RTSConstants.long_distance_batching_batch_disabled);
		String query = RTSConstants.disable_resturant_batching
				+ RTSConstants.long_distance_batching_rest_id;
		delmeth.dbhelperupdate(query);
		delmeth.dbhelperupdate(RTSConstants.set_random_de_id);
	}
}
