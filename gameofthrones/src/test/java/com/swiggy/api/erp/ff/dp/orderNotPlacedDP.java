package com.swiggy.api.erp.ff.dp;

import com.swiggy.api.erp.ff.constants.OMSConstants;
import com.swiggy.api.erp.ff.helper.LOSHelper;
import com.swiggy.api.erp.ff.helper.OMSHelper;
import framework.gameofthrones.Tyrion.RabbitMQHelper;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class orderNotPlacedDP {

    LOSHelper los = new LOSHelper();
    OMSHelper oms = new OMSHelper();
    RabbitMQHelper rmqhelper = new RabbitMQHelper();

    @DataProvider(name = "createOrderONP")
    public Object[][] createOrderONP() {
        String manual_order_id = null;
        try {
            rmqhelper.purgeQueue(OMSConstants.RMQHOSTNAME,OMSConstants.ONP_ORDERS);
             manual_order_id = los.getAnOrder("manual");
            oms.verifyOrder(manual_order_id, "111");
            Thread.sleep(2000);
        } catch(NullPointerException|TimeoutException|IOException|InterruptedException e){
            e.printStackTrace();
        }
        return new Object[][] {
                {manual_order_id}
        };
    }

    @DataProvider(name = "NormalOrderFlow")

    public Object[][] NormalOrderFlow() {
        String pop_order_id = null;

        try {
            rmqhelper.purgeQueue(OMSConstants.RMQHOSTNAME,OMSConstants.ONP_ORDERS);
            pop_order_id = los.getAnOrder("pop");
            oms.verifyOrder(pop_order_id, "111");
        } catch(NullPointerException|TimeoutException|IOException|InterruptedException e){
            //This catch block catches all the exceptions
            e.printStackTrace();
        }
        return new Object[][] {

                {pop_order_id}
        };
    }

    @DataProvider(name = "PartnerOrderFlow")
    public Object[][] PartnerOrderFlow()  {
        String partner_order_id = null;
        try {
            rmqhelper.purgeQueue(OMSConstants.RMQHOSTNAME,OMSConstants.ONP_ORDERS);
            partner_order_id = los.getAnOrder("partner");
            oms.verifyOrder(partner_order_id, "111");
        } catch(NullPointerException|TimeoutException|IOException|InterruptedException e){
            //This catch block catches all the exceptions
            e.printStackTrace();
        }
        return new Object[][] {
                {partner_order_id}
        };
    }
}
