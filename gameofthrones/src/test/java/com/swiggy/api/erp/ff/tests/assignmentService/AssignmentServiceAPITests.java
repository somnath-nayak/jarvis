package com.swiggy.api.erp.ff.tests.assignmentService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.swiggy.api.erp.ff.constants.OMSConstants;
import com.swiggy.api.erp.ff.dp.assignmentService;
import com.swiggy.api.erp.ff.helper.AssignmentServiceHelper;
import com.swiggy.api.erp.ff.helper.LOSHelper;
import com.swiggy.api.erp.ff.helper.OMSHelper;

import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.Processor;

public class AssignmentServiceAPITests extends assignmentService {

	OMSHelper omsHelper = new OMSHelper();
	AssignmentServiceHelper helper = new AssignmentServiceHelper();
	LOSHelper losHelper = new LOSHelper();

	public List<Map<String, Object>> getAssignedOrderToOEFromDB(String oeId){
		return SystemConfigProvider.getTemplate(OMSConstants.ASSIGNMENT_DB).queryForList("select order_id from assignment where time_of_unassignment is NULL and oe_id = "+oeId+";");
	}

	@BeforeClass
	public void print(){
		System.out.println("**********************************************************************");
		System.out.println("*     ____          _                                                *");
		System.out.println("*    /____|_      _(_) __ _  __ _ _   _                              *");
		System.out.println("*    \\___ \\ \\ /\\ / / |/ _` |/ _` | | | |                             *");
		System.out.println("*     ___) \\ V  V /| | (_| | (_| | |_| |                             *");
		System.out.println("*    |____/ \\_/\\_/ |_|\\__, |\\__, |\\__, |                             *");
		System.out.println("*                      |___/ |___/ |___/                             *");
		System.out.println("*                                                                    *");
		System.out.println("*    Starting Assignment Service API tests                           *");
		System.out.println("**********************************************************************");
	}


	@Test(dataProvider = "oeIds")
	public void testCurrentOrderForOEApi(String oeId, String role, String loginStatus){
		System.out.println("***************************************** testCurrentOrderForOEApi started *****************************************");

		List<String> ordersFromDB = new ArrayList<>();
		String fromApi = helper.getCurrentOrdesForOE(oeId).ResponseValidator.GetBodyAsText();
		List<Map<String, Object>> fromDb = getAssignedOrderToOEFromDB(oeId);
		fromApi = fromApi.replaceAll("\"", "");
		System.out.println("FROM API ============="+fromApi);
		for(int i=fromDb.size()-1;i>=0;i--){
			ordersFromDB.add(fromDb.get(i).get("order_id").toString());
		}
		String fromDB = ordersFromDB.toString().replaceAll(" ", "");
		System.out.println("FROM DB =============="+fromDB);
		Assert.assertEquals(fromApi, fromDB, "Output from API did not match with the output from the DB, Failed for "+role+" with oeId = "+oeId+" when login status = "+loginStatus);
		System.out.println("Passed for "+role+" with oeId = "+oeId+" when login status = "+loginStatus);

		System.out.println("######################################### testCurrentOrderForOEApi completed #########################################");
	}


	@Test(dataProvider = "incorrectDataForCurrentOrderApi")
	public void testCurrentOrderForOEApiWithInvalidData(String oeId){
		System.out.println("***************************************** testCurrentOrderForOEApiWithInvalidData started *****************************************");

		Processor process = helper.getCurrentOrdesForOE(oeId);
		System.out.println("Incorrect data test ============="+process.ResponseValidator.GetBodyAsText());
		System.out.println("Response code is ============="+process.ResponseValidator.GetResponseCode());

		System.out.println("######################################### testCurrentOrderForOEApiWithInvalidData completed #########################################");
	}

	@Test(dataProvider = "orderIdsForAssignment")
	public void testAssignOEToOrderApi(String orderId, String roleId, String role, String oeId) throws IOException, InterruptedException{
		System.out.println("***************************************** testAssignOEToOrderApi started *****************************************");

		if(!role.equals("Verifier")){
			omsHelper.verifyOrder(orderId, "001");
		}
		omsHelper.clearOrdersInQueueExceptOrder(role, orderId);
		helper.oeLoginRMQ(Integer.parseInt(oeId), Integer.parseInt(roleId));
		Thread.sleep(5000);
		Processor response = helper.assignOEToOrder(orderId, roleId);
		Assert.assertEquals(response.ResponseValidator.GetResponseCode(), 200, "Response Code is not 200");
		Assert.assertEquals(response.ResponseValidator.GetNodeValue("message"), "Agent assigned successfully");
		Assert.assertTrue(response.ResponseValidator.GetNodeValueAsStringFromJsonArray("assignment").contains(orderId), "assignOEToOrder api response does not contain expected order ID");
		Assert.assertTrue(helper.getCurrentOrdesForOE(oeId).ResponseValidator.GetBodyAsText().contains(orderId), "OrderId is not present in getCurrentOrdersForOE API's response");
		Assert.assertTrue(getAssignedOrderToOEFromDB(oeId).toString().contains(orderId), "Assigned order id is not present in assignment DB=="+getAssignedOrderToOEFromDB(oeId).toString());

		System.out.println("######################################### testAssignOEToOrderApi completed #########################################");
	}
	
	
//	               **********************************             Negative Tests             **********************************

	
	@Test(dataProvider = "orderIdsForAssignment")
	public void testAssignOEToOrderAPIWithoutOELogin(String orderId, String roleId, String role, String oeId) throws InterruptedException{
		System.out.println("***************************************** testNegativeCasesOfAssignOEToOrderAPI started *****************************************");
		omsHelper.forceLogout("all");
		Thread.sleep(2000);
		Processor response = helper.assignOEToOrder(orderId, roleId);
		Assert.assertEquals(response.ResponseValidator.GetResponseCode(), 200, "Response Code is not 200");
//		Assert.assertEquals(response.ResponseValidator.GetNodeValue("message"), "Agent assigned successfully");
//		Assert.assertTrue(response.ResponseValidator.GetNodeValueAsStringFromJsonArray("assignment").contains(orderId), "assignOEToOrder api response does not contain expected order ID");
//		Assert.assertFalse(helper.getCurrentOrdesForOE(oeId).ResponseValidator.GetBodyAsText().contains(orderId), "Placed order got assigned to the OE without OE login");
		List<Map<String, Object>> nullCheck = getAssignedOrderToOEFromDB(oeId);
		if(nullCheck != null){
			Assert.assertFalse(nullCheck.toString().contains(orderId), "Assigned order id is not present in assignment DB");
		}else {
			Assert.assertTrue(true);
		}

		System.out.println("######################################### testNegativeCasesOfAssignOEToOrderAPI completed #########################################");

	}

}
