package com.swiggy.api.erp.delivery.dp;

import com.swiggy.api.erp.cms.helper.CMSHelper;
import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.erp.delivery.helper.*;
import com.swiggy.api.erp.delivery.tests.DeliveryControllerRegression;
import framework.gameofthrones.JonSnow.Processor;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import org.testng.annotations.Test;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class DeliveryControllerRegressionDataProvider {

    public Integer zoneID = 1;
    public String cityId = "1";
    public String areaCode = "1";
    AutoassignHelper autoassignHelper = new AutoassignHelper();
    DeliveryServiceHelper deliveryServiceHelper = new DeliveryServiceHelper();
    DeliveryHelperMethods delmeth = new DeliveryHelperMethods();
    DeliveryDataHelper deldatahelp=new DeliveryDataHelper();
    DeliveryServiceApiWrapperHelper deliveryServiceApiWrapperHelper = new DeliveryServiceApiWrapperHelper();
    String paramIdForMaxFirstMileDistance = "select id from auto_assignment_params where `parameter`=\"max_first_mile_distance\"";
    String paramIdForWithDE = "select id from auto_assignment_params where `parameter`=\"weight_with_de\"";
    String paramIDForWeightOrderDelay = "select id from auto_assignment_params where `parameter`=\"weight_order_delay\"";
    String paramIDForOrderDelayStepFactor = "select id from auto_assignment_params where `parameter`=\"order_delay_step_factor\"";
    String paramIDForWithinShiftTime = "select id from auto_assignment_params where `parameter`=\"weight_within_shift\"";
    String paramIDForWeightCycleDe = "select id from auto_assignment_params where `parameter`=\"weight_cycle_de\"";
    String paramIDForMaxFirstMileCycle = "select id from auto_assignment_params where `parameter`=\"max_first_mile_cycle_distance\"";
    String paramIDForMaxLastMileCycle = "select id from auto_assignment_params where `parameter`=\"max_last_mile_cycle_distance\"";
    String paramIDForminJitTime = "select id from auto_assignment_params where `parameter`=\"min_jit_delay\"";
    String paramIDForDelayDynamicFirstMile = "select id from auto_assignment_params where `parameter`=\"delay_dynamic_first_mile\"";
    String paramIDForStepFactorDelay = "select id from auto_assignment_params where `parameter`=\"step_dynamic_first_mile\"";
    String paramIDForOrderDelayPanicTime = "select id from auto_assignment_params where `parameter`=\"order_delay_panic_time\"";
    String paramIDForOrderDelayPanicFactor = "select id from auto_assignment_params where `parameter`=\"order_delay_panic_factor\"";
    String getFirstRowOFAutoAssign = "select * from `auto_assign_audit` where zone_id = 1 limit 1";
    String getSecondRowOFAutoAssign = "select * from `auto_assign_audit` where zone_id = 1 limit 2";
    public String updateDeliveryBoy = "update delivery_boys set enabled=0 where id between 80000 and 98000";



    @DataProvider(name = "verifyOrderIsAssignedIfFMIsLess")
    public Object[][] verifyOrderIsAssignedIfFMIsLess01(){
        String paramId = delmeth.dbhelperget(paramIdForMaxFirstMileDistance, "id").toString();
        autoassignHelper.updateParamAuto(cityId,zoneID.toString(),paramId,DeliveryConstant.autoassign_maxfirstmiledistance,"Test","max_first_mile_distance");
        delmeth.dbhelperupdate(DeliveryConstant.updatebatch);
        String orderId = new OrderBuilder.Builder(areaCode).build().getOrder_id();
        String order_time = "select ordered_time from trips where order_id="+orderId;
        String ordered_time = delmeth.dbhelperget(order_time,"ordered_time").toString();
        String getRestaurantlatlon = "select r.lat_long from trips t inner join restaurant r on t.restaurant_id=r.id where t.order_id=" + orderId;
        String restaurantlatlon = delmeth.dbhelperget(getRestaurantlatlon, "lat_long").toString();
        String[] latlong = restaurantlatlon.split(",");
        deldatahelp.updateDELocationInRedis(DeliveryControllerRegression.deID,latlong[0],latlong[1]);
        deliveryServiceHelper.makeDEActive(DeliveryControllerRegression.deID);
        deldatahelp.addDEzonemap(DeliveryControllerRegression.deID,zoneID);
        System.out.println("***************************************************"+orderId);
        return new Object[][] {
                { orderId,ordered_time}
        };

    }

    @DataProvider(name = "verifyOrderNotAssignedIfDEDifferntZone")
    public Object[][] verifyOrderNotAssignedIfDEDifferntZone02(){
        deliveryServiceHelper.makeDEFree(DeliveryControllerRegression.deID);
        String updateDEZONEQuery = "update delivery_boys set zone_id = 35 where id="+DeliveryControllerRegression.deID;
        delmeth.dbhelperupdate(updateDEZONEQuery);
        String getRestaurantlatlon = "select lat_long from `restaurant` where area_code=35 limit 1;";
        String restaurantlatlon = delmeth.dbhelperget(getRestaurantlatlon, "lat_long").toString();
        String[] latlong = restaurantlatlon.split(",");
        deldatahelp.updateDELocationInRedis(DeliveryControllerRegression.deID,latlong[0],latlong[1]);
        deliveryServiceHelper.makeDEActive(DeliveryControllerRegression.deID);
        String orderId = new OrderBuilder.Builder(areaCode).build().getOrder_id();
        System.out.println("***************************************************"+orderId);
        return new Object[][] {
                {orderId}
        };
    }

    @DataProvider(name = "verifyOrderIsRejectedIfDENotConfirmed")
    public Object[][] verifyOrderIsRejectedIfDENotConfirmed03(){
        deliveryServiceHelper.makeDEFree(DeliveryControllerRegression.deID);
        String orderId = new OrderBuilder.Builder(areaCode).build().getOrder_id();
        String order_time = "select ordered_time from trips where order_id="+orderId;
        String ordered_time = delmeth.dbhelperget(order_time,"ordered_time").toString();
        String getRestaurantlatlon = "select r.lat_long from trips t inner join restaurant r on t.restaurant_id=r.id where t.order_id=" + orderId;
        String restaurantlatlon = delmeth.dbhelperget(getRestaurantlatlon, "lat_long").toString();
        String[] latlong = restaurantlatlon.split(",");
        deldatahelp.updateDELocationInRedis(DeliveryControllerRegression.deID,latlong[0],latlong[1]);
        deliveryServiceHelper.makeDEActive(DeliveryControllerRegression.deID);
        deldatahelp.addDEzonemap(DeliveryControllerRegression.deID,zoneID);
        System.out.println("***************************************************"+orderId);
        return new Object[][] {
                { orderId,ordered_time}
        };

    }

    @DataProvider(name = "verifyOrderNotAssignedToSameDEIfRejected")
    public Object[][] verifyOrderNotAssignedToSameDEIfRejected04(){
        deliveryServiceHelper.makeDEFree(DeliveryControllerRegression.deID);
        String orderId = new OrderBuilder.Builder(areaCode).build().getOrder_id();
        String getRestaurantlatlon = "select r.lat_long from trips t inner join restaurant r on t.restaurant_id=r.id where t.order_id=" + orderId;
        String restaurantlatlon = delmeth.dbhelperget(getRestaurantlatlon, "lat_long").toString();
        String[] latlong = restaurantlatlon.split(",");
        deldatahelp.updateDELocationInRedis(DeliveryControllerRegression.deID,latlong[0],latlong[1]);
        deliveryServiceHelper.makeDEActive(DeliveryControllerRegression.deID);
        deldatahelp.addDEzonemap(DeliveryControllerRegression.deID,zoneID);
        System.out.println("***************************************************"+orderId);
        return new Object[][] {
                { orderId}
        };
    }

    @DataProvider(name = "verifyDEScoreAddedForWithDE")
    public Object[][] verifyDEScoreAddedForWithDE05(){
        deliveryServiceHelper.makeDEFree(DeliveryControllerRegression.deID);
        String paramId = delmeth.dbhelperget(paramIdForWithDE, "id").toString();
        autoassignHelper.updateParamAuto(cityId,zoneID.toString(),paramId,DeliveryConstant.with_de_value,"Test",DeliveryConstant.autoasign_weightwithde);
        String firstOrderId = new OrderBuilder.Builder(areaCode).build().getOrder_id();
        String updateWithDE = "update `trips` set `with_de` = 1 where `order_id` like " + firstOrderId;
        delmeth.dbhelperupdate(updateWithDE);
        System.out.println("************************First Order ID with DE  "+firstOrderId );
        String secondOrderId = new OrderBuilder.Builder(areaCode).build().getOrder_id();
        System.out.println("************************Second Order ID not with DE  "+secondOrderId );
        String query = "select batch_id from trips where order_id="+firstOrderId;
        String getBatchId1 = delmeth.dbhelperget(query,"batch_id").toString();
        String query2 = "select batch_id from trips where order_id="+secondOrderId;
        String getBatchId2 = delmeth.dbhelperget(query2,"batch_id").toString();
        String getRestaurantlatlon = "select r.lat_long from trips t inner join restaurant r on t.restaurant_id=r.id where t.order_id=" + firstOrderId;
        String restaurantlatlon = delmeth.dbhelperget(getRestaurantlatlon, "lat_long").toString();
        String[] latlong = restaurantlatlon.split(",");
        deldatahelp.updateDELocationInRedis(DeliveryControllerRegression.deID,latlong[0],latlong[1]);
        deliveryServiceHelper.makeDEActive(DeliveryControllerRegression.deID);
        deldatahelp.addDEzonemap(DeliveryControllerRegression.deID,zoneID);
        return new Object[][]{
                {firstOrderId,secondOrderId,getBatchId1,getBatchId2}
        };
    }

    @DataProvider(name = "verifyOrderDelayAddedToOrderAssignment")
    public Object[][] verifyOrderDelayAddedToOrderAssignment06() throws InterruptedException{
        deliveryServiceHelper.makeDEFree(DeliveryControllerRegression.deID);
        String paramId = delmeth.dbhelperget(paramIDForWeightOrderDelay, "id").toString();
        String paramIdOfDelayStepFactor = delmeth.dbhelperget(paramIDForOrderDelayStepFactor, "id").toString();
        autoassignHelper.updateParamAuto(cityId,zoneID.toString(),paramId,DeliveryConstant.weight_of_order_delay_value,"Test",DeliveryConstant.autoassign_weightorderdelayname);
        autoassignHelper.updateParamAuto(cityId,zoneID.toString(),paramIdOfDelayStepFactor,DeliveryConstant.step_factor_delay_value,"Test",DeliveryConstant.autoassign_orderdelaystepfactorname);
        String firstOrderId = new OrderBuilder.Builder(areaCode).build().getOrder_id();
        Thread.sleep(120000);
        System.out.println("************************First Order ID 1 min delay  "+firstOrderId );
        String secondOrderId = new OrderBuilder.Builder(areaCode).build().getOrder_id();
        System.out.println("************************Second Order ID with out delay "+firstOrderId );
        String getRestaurantlatlon = "select r.lat_long from trips t inner join restaurant r on t.restaurant_id=r.id where t.order_id=" + firstOrderId;
        String restaurantlatlon = delmeth.dbhelperget(getRestaurantlatlon, "lat_long").toString();
        String[] latlong = restaurantlatlon.split(",");
        deldatahelp.updateDELocationInRedis(DeliveryControllerRegression.deID,latlong[0],latlong[1]);
        deliveryServiceHelper.makeDEActive(DeliveryControllerRegression.deID);
        deldatahelp.addDEzonemap(DeliveryControllerRegression.deID,zoneID);
        return new Object[][]{
                {firstOrderId, secondOrderId}
        };
    }

    @DataProvider(name="verifyWithinSiftWeightAdded")
    public Object[][] verifyWithinSiftWeightAdded08(){
        deliveryServiceHelper.makeDEFree(DeliveryControllerRegression.deID);
        String paramId = delmeth.dbhelperget(paramIDForWithinShiftTime, "id").toString();
        autoassignHelper.updateParamAuto(cityId,zoneID.toString(),paramId,DeliveryConstant.autoassign_withinshiftmore,"Test",DeliveryConstant.autoassign_withinshiftname);
        String deID1 = deldatahelp.CreateDE(zoneID);
        System.out.println("********************************************DE id is "+deID1);
        String deID2 = deldatahelp.CreateDE(zoneID);
        System.out.println("********************************************DE id is "+deID2);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String deIDFirst = delmeth.dbhelperget(getFirstRowOFAutoAssign, "id").toString();
        String query = "update auto_assign_audit set id ="+deID1+" , parameter_id ="+paramId+" , zone_id = "+zoneID+" , value = 2000 , user = 'test' , timestamp = '"+timestamp+"' where id =" +deIDFirst;
        delmeth.dbhelperupdate(query);
        String deIDSecond = delmeth.dbhelperget(getSecondRowOFAutoAssign, "id").toString();
        String query1 = "update auto_assign_audit set id ="+deID2+" , parameter_id ="+paramId+" , zone_id = "+zoneID+" , value = 1 , user = 'test' , timestamp = '"+timestamp+"' where id =" +deIDSecond;
        delmeth.dbhelperupdate(query1);
        System.out.println(timestamp);
        String orderId = new OrderBuilder.Builder(areaCode).build().getOrder_id();
        String getRestaurantlatlon = "select r.lat_long from trips t inner join restaurant r on t.restaurant_id=r.id where t.order_id=" + orderId;
        String restaurantlatlon = delmeth.dbhelperget(getRestaurantlatlon, "lat_long").toString();
        String[] latlong = restaurantlatlon.split(",");
        deldatahelp.updateDELocationInRedis(deID1,latlong[0],latlong[1]);
        deldatahelp.updateDELocationInRedis(deID2,latlong[0],latlong[1]);
        deliveryServiceHelper.makeDEActive(deID1);
        deliveryServiceHelper.makeDEActive(deID2);
        deldatahelp.addDEzonemap(deID1,zoneID);
        deldatahelp.addDEzonemap(deID2,zoneID);
        return new Object[][]{
                {deID1,deID2,orderId}
        };
    }

    @DataProvider(name="verifyWeightOfCycleDEIsAdded")
    public Object[][] verifyWeightOfCycleDEIsAdded09(){
        String paramId = delmeth.dbhelperget(paramIDForWeightCycleDe, "id").toString();
        autoassignHelper.updateParamAuto(cityId,zoneID.toString(),paramId,DeliveryConstant.autoassign_weight_cycle_de_paramValue,"Test",DeliveryConstant.autoassign_weight_cycle_de_name);
        String deID1 = deldatahelp.CreateDE(zoneID);
        System.out.println("********************************************DE id is "+deID1);
        String deID2 = deldatahelp.CreateDE(zoneID);
        System.out.println("********************************************DE id is "+deID2);
        String query = "update delivery_boys set `bicycle` = 1 where id ="+deID1;
        delmeth.dbhelperupdate(query);
        String orderId = new OrderBuilder.Builder(areaCode).build().getOrder_id();
        String getRestaurantlatlon = "select r.lat_long from trips t inner join restaurant r on t.restaurant_id=r.id where t.order_id=" + orderId;
        String restaurantlatlon = delmeth.dbhelperget(getRestaurantlatlon, "lat_long").toString();
        String[] latlong = restaurantlatlon.split(",");
        deldatahelp.updateDELocationInRedis(deID1,latlong[0],latlong[1]);
        deldatahelp.updateDELocationInRedis(deID2,latlong[0],latlong[1]);
        deliveryServiceHelper.makeDEActive(deID1);
        deliveryServiceHelper.makeDEActive(deID2);
        deldatahelp.addDEzonemap(deID1,zoneID);
        deldatahelp.addDEzonemap(deID2,zoneID);
        return new Object[][]{
                {deID1,deID2,orderId}
        };
    }

    @DataProvider(name="verifyOrderIsAssignedToCycleDE")
    public Object[][] verifyOrderIsAssignedToCycleDE10(){
        String paramIdOfFirstMile = delmeth.dbhelperget(paramIDForMaxFirstMileCycle, "id").toString();
        autoassignHelper.updateParamAuto(cityId,zoneID.toString(),paramIdOfFirstMile,DeliveryConstant.max_first_mile_cycle_value,"Test",DeliveryConstant.autoassign_maxfirstmileparamname);
        String deID2 = deldatahelp.CreateDE(zoneID);
        System.out.println("********************************************DE id is "+deID2);
        String query = "update delivery_boys set `bicycle` = 1 where id ="+deID2;
        delmeth.dbhelperupdate(query);
        deldatahelp.updateDELocationInRedis(deID2,"12.9000897","77.6036");
        deliveryServiceHelper.makeDEActive(deID2);
        deldatahelp.addDEzonemap(deID2,zoneID);
        String orderId = new OrderBuilder.Builder(areaCode).build().getOrder_id();
        System.out.println("**************************************************"+ orderId);
        return new Object[][]{
                {deID2,orderId}
        };
    }

    @DataProvider(name="verifyOrderAssignedIfLMIsLess")
    public Object[][] verifyOrderAssignedIfLMIsLess11(){
        String paramIdOfLastMile = delmeth.dbhelperget(paramIDForMaxLastMileCycle, "id").toString();
        autoassignHelper.updateParamAuto(cityId,zoneID.toString(),paramIdOfLastMile,DeliveryConstant.max_last_mile_cycle_value,"Test",DeliveryConstant.autoassign_cyclelastmilename);
        String deID1 = deldatahelp.CreateDE(zoneID);
        System.out.println("********************************************DE id is "+deID1);
        String query = "update delivery_boys set `bicycle` = 1 where id ="+deID1;
        delmeth.dbhelperupdate(query);
        String orderId = new OrderBuilder.Builder(areaCode).build().getOrder_id();
        String getRestaurantlatlon = "select r.lat_long from trips t inner join restaurant r on t.restaurant_id=r.id where t.order_id=" + orderId;
        String restaurantlatlon = delmeth.dbhelperget(getRestaurantlatlon, "lat_long").toString();
        String[] latlong = restaurantlatlon.split(",");
        deldatahelp.updateDELocationInRedis(deID1,latlong[0],latlong[1]);
        deliveryServiceHelper.makeDEActive(deID1);
        deldatahelp.addDEzonemap(deID1,zoneID);
        return new Object[][]{
                {deID1,orderId}
        };
    }

    @DataProvider(name="verifyOrderAssignedToDEIfHaltedXmins")
    public Object[][] verifyOrderAssignedToDEIfHaltedXmins12(){
        deliveryServiceHelper.makeDEFree(DeliveryControllerRegression.deID);
        String paramIdOfMinJit = delmeth.dbhelperget(paramIDForminJitTime, "id").toString();
        autoassignHelper.updateParamAuto(cityId,zoneID.toString(),paramIdOfMinJit,DeliveryConstant.min_jit_value,"Test",DeliveryConstant.autoassign_minjitparamidname);
        String orderId = new OrderBuilder.Builder(areaCode).build().getOrder_id();
        String restaurantId ="select `restaurant_id` from trips where order_id="+orderId;
        String getRestaurantId = delmeth.dbhelperget(restaurantId, "restaurant_id").toString();
        String enableJitForRestaurant = "update restaurant set `jit_enabled` = 1 where id ="+getRestaurantId;
        String disableJitForRestaurant = "update restaurant set `jit_enabled` = 0 where id ="+getRestaurantId;
        delmeth.dbhelperupdate(enableJitForRestaurant);
        String getRestaurantlatlon = "select r.lat_long from trips t inner join restaurant r on t.restaurant_id=r.id where t.order_id=" + orderId;
        String restaurantlatlon = delmeth.dbhelperget(getRestaurantlatlon, "lat_long").toString();
        String[] latlong = restaurantlatlon.split(",");
        deldatahelp.updateDELocationInRedis(DeliveryControllerRegression.deID,latlong[0],latlong[1]);
        deliveryServiceHelper.makeDEActive(DeliveryControllerRegression.deID);
        deldatahelp.addDEzonemap(DeliveryControllerRegression.deID,zoneID);
        return new Object[][]{
                {orderId,disableJitForRestaurant}
        };
    }

    @DataProvider(name="verifyFMIsIncreasedByStepFactor")
    public Object[][] verifyFMIsIncreasedByStepFactor13(){
        deliveryServiceHelper.makeDEFree(DeliveryControllerRegression.deID);
        String paramIdOfMaxFirstMileDistance = delmeth.dbhelperget(paramIdForMaxFirstMileDistance, "id").toString();
        autoassignHelper.updateParamAuto(cityId,zoneID.toString(),paramIdOfMaxFirstMileDistance,DeliveryConstant.Max_First_Mile_Distance_Value,"Test",DeliveryConstant.maxFirstMileDistanceName);
        deldatahelp.updateDELocationInRedis(DeliveryControllerRegression.deID,"12.9726","77.6036");
        deliveryServiceHelper.makeDEActive(DeliveryControllerRegression.deID);
        deldatahelp.addDEzonemap(DeliveryControllerRegression.deID,zoneID);
        String paramIdOfDelayDynamicFirstMile = delmeth.dbhelperget(paramIDForDelayDynamicFirstMile, "id").toString();
        autoassignHelper.updateParamAuto(cityId,zoneID.toString(),paramIdOfDelayDynamicFirstMile,DeliveryConstant.delayDynamicFirstMileValue,"Test",DeliveryConstant.delayDynamicFirstMileName);
        String paramIdOfStepFactorDelay = delmeth.dbhelperget(paramIDForStepFactorDelay, "id").toString();
        autoassignHelper.updateParamAuto(cityId,zoneID.toString(),paramIdOfStepFactorDelay,DeliveryConstant.autoassignStepfactorDynamicfirstMilemore,"Test",DeliveryConstant.autoassignStepfactorDynamicfirstMilename);
        String orderId = new OrderBuilder.Builder(areaCode).build().getOrder_id();
        return new Object[][]{
                {orderId}
        };
    }

    @DataProvider(name="verifyDEOfOtherServiceZoneIsConsider")
    public Object[][] verifyDEOfOtherServiceZoneIsConsider14(){
        String paramId = delmeth.dbhelperget(paramIdForMaxFirstMileDistance, "id").toString();
        autoassignHelper.updateParamAuto(cityId,zoneID.toString(),paramId,DeliveryConstant.autoassign_maxfirstmiledistance,"Test","max_first_mile_distance");
        delmeth.dbhelperupdate(DeliveryConstant.updatebatch);
        String orderId = new OrderBuilder.Builder(areaCode).build().getOrder_id();
        String getRestaurantlatlon = "select r.lat_long from trips t inner join restaurant r on t.restaurant_id=r.id where t.order_id=" + orderId;
        String restaurantlatlon = delmeth.dbhelperget(getRestaurantlatlon, "lat_long").toString();
        String[] latlong = restaurantlatlon.split(",");
        deldatahelp.updateDELocationInRedis(DeliveryControllerRegression.deID,latlong[0],latlong[1]);
        deliveryServiceHelper.makeDEActive(DeliveryControllerRegression.deID);
        deldatahelp.addDEzonemap(DeliveryControllerRegression.deID,zoneID);
        System.out.println("***************************************************"+orderId);
        return new Object[][] {
                { orderId}
        };
    }

    @DataProvider(name="verifyBusyDENotConsiderForAssignment")
    public Object[][] verifyBusyDENotConsiderForAssignment15(){
        boolean assignstatus = false;
        boolean cronStatus = false;
        deliveryServiceHelper.makeDEFree(DeliveryControllerRegression.deID);
        String orderId = new OrderBuilder.Builder(areaCode).build().getOrder_id();
        System.out.println("********************************************Order ID :" + orderId);
        String getRestaurantlatlon = "select r.lat_long from trips t inner join restaurant r on t.restaurant_id=r.id where t.order_id=" + orderId;
        String restaurantlatlon = delmeth.dbhelperget(getRestaurantlatlon, "lat_long").toString();
        String[] latlong = restaurantlatlon.split(",");
        deldatahelp.updateDELocationInRedis(DeliveryControllerRegression.deID,latlong[0],latlong[1]);
        deliveryServiceHelper.makeDEActive(DeliveryControllerRegression.deID);
        deldatahelp.addDEzonemap(DeliveryControllerRegression.deID,zoneID);
        cronStatus = autoassignHelper.runAutoAssign("1");
        Assert.assertTrue(cronStatus,"Cron Status is : "+cronStatus);
        String query = "select de_id from trips where order_id=" + orderId;
        assignstatus = deldatahelp.polldb("deliverydb", query, "de_id", DeliveryControllerRegression.deID, 2, 60);
        System.out.println(assignstatus);
        Assert.assertTrue(assignstatus,"Order assigned status is  : "+assignstatus);
        deldatahelp.doCAPRD(orderId,DeliveryControllerRegression.deID,"confirmed","pickedup");
        String secondOrderId = new OrderBuilder.Builder(areaCode).build().getOrder_id();
        System.out.println("**************************************"+secondOrderId);
        String batchId = "select batch_id from trips where order_id=" + orderId;
        String batchID = delmeth.dbhelperget(batchId, "batch_id").toString();
        Processor p = deliveryServiceApiWrapperHelper.DeAssignment(batchID,DeliveryControllerRegression.deID);
        String statusMessage =  p.ResponseValidator.GetNodeValue("$.statusMessage");
        System.out.println("***********************************************"+statusMessage);
        return new Object[][]{
                {DeliveryControllerRegression.deID,statusMessage}
        };
    }

    @DataProvider(name="verifyWeightOfOrderDelayCalculated")
    public Object[][] verifyWeightOfOrderDelayCalculated16() throws InterruptedException {
        deliveryServiceHelper.makeDEFree(DeliveryControllerRegression.deID);
        String paramIdOfOrderDelayPanicTime = delmeth.dbhelperget(paramIDForOrderDelayPanicTime, "id").toString();
        autoassignHelper.updateParamAuto(cityId,zoneID.toString(),paramIdOfOrderDelayPanicTime,DeliveryConstant.OrderPanicTimeValue,"Test",DeliveryConstant.autoassignorderpanictimename);
        String paramIdOfOrderDelayPanicFactor = delmeth.dbhelperget(paramIDForOrderDelayPanicFactor, "id").toString();
        autoassignHelper.updateParamAuto(cityId,zoneID.toString(),paramIdOfOrderDelayPanicFactor,DeliveryConstant.OrderPanicFactorValue,"Test",DeliveryConstant.autoassignorderpanicfactorname);
        String orderId = new OrderBuilder.Builder(areaCode).build().getOrder_id();
        System.out.println("********************************************First Order ID :" + orderId);
        String getRestaurantlatlon = "select r.lat_long from trips t inner join restaurant r on t.restaurant_id=r.id where t.order_id=" + orderId;
        String restaurantlatlon = delmeth.dbhelperget(getRestaurantlatlon, "lat_long").toString();
        String[] latlong = restaurantlatlon.split(",");
        deldatahelp.updateDELocationInRedis(DeliveryControllerRegression.deID,latlong[0],latlong[1]);
        deliveryServiceHelper.makeDEActive(DeliveryControllerRegression.deID);
        deldatahelp.addDEzonemap(DeliveryControllerRegression.deID,zoneID);
        Thread.sleep(120000);
        String secondOrderId = new OrderBuilder.Builder(areaCode).build().getOrder_id();
        System.out.println("**************************************"+secondOrderId);
        return new Object[][]{
                {orderId}
        };
    }

    @DataProvider(name="verifyCODOrderNotAssignedToTempDE")
    public Object[][] verifyCODOrderNotAssignedToTempDE17(){
        deliveryServiceHelper.makeDEFree(DeliveryControllerRegression.deID);
        String orderId = new OrderBuilder.Builder(areaCode).build().getOrder_id();
        System.out.println("********************************************First Order ID :" + orderId);
        String getRestaurantlatlon = "select r.lat_long from trips t inner join restaurant r on t.restaurant_id=r.id where t.order_id =" + orderId;
        String restaurantlatlon = delmeth.dbhelperget(getRestaurantlatlon, "lat_long").toString();
        String[] latlong = restaurantlatlon.split(",");
        deldatahelp.updateDELocationInRedis(DeliveryControllerRegression.deID,latlong[0],latlong[1]);
        deliveryServiceHelper.makeDEActive(DeliveryControllerRegression.deID);
        deldatahelp.addDEzonemap(DeliveryControllerRegression.deID,zoneID);
        String updateTempDE = "update de_info set de_type = 1 where de_id = "+DeliveryControllerRegression.deID;
        String updateCollectValue = "update trips set collect = 200 where order_id like "+orderId;
        delmeth.dbhelperupdate(updateTempDE);
        delmeth.dbhelperupdate(updateCollectValue);
        return new Object[][]{
                {orderId}
        };
    }

    @DataProvider(name="verifyInActiveDENotConsiderForAssignment")
    public Object[][] verifyInActiveDENotConsiderForAssignment18(){
        deliveryServiceHelper.makeDEFree(DeliveryControllerRegression.deID);
        String orderId = new OrderBuilder.Builder(areaCode).build().getOrder_id();
        System.out.println("********************************************First Order ID :" + orderId);
        String getRestaurantlatlon = "select r.lat_long from trips t inner join restaurant r on t.restaurant_id=r.id where t.order_id =" + orderId;
        String restaurantlatlon = delmeth.dbhelperget(getRestaurantlatlon, "lat_long").toString();
        String[] latlong = restaurantlatlon.split(",");
        deldatahelp.updateDELocationInRedis(DeliveryControllerRegression.deID,latlong[0],latlong[1]);
        deliveryServiceHelper.markDEInActive(DeliveryControllerRegression.deID);
        String batchId = "select batch_id from trips where order_id=" + orderId;
        String batchID = delmeth.dbhelperget(batchId, "batch_id").toString();
        Processor p = deliveryServiceApiWrapperHelper.DeAssignment(batchID,DeliveryControllerRegression.deID);
        String statusMessage =  p.ResponseValidator.GetNodeValue("$.statusMessage");
        System.out.println("***********************************************"+statusMessage);
        return new Object[][]{
                {statusMessage,orderId}
        };

    }

    @DataProvider(name="verifyOrderISAssignedToDifferentDEIfRejected")
    public Object[][] verifyOrderISAssignedToDifferentDEIfRejected19(){
        deliveryServiceHelper.makeDEFree(DeliveryControllerRegression.deID);
        String orderId = new OrderBuilder.Builder(areaCode).build().getOrder_id();
        System.out.println("********************************************Order ID :" + orderId);
        String getRestaurantlatlon = "select r.lat_long from trips t inner join restaurant r on t.restaurant_id=r.id where t.order_id =" + orderId;
        String restaurantlatlon = delmeth.dbhelperget(getRestaurantlatlon, "lat_long").toString();
        String[] latlong = restaurantlatlon.split(",");
        deldatahelp.updateDELocationInRedis(DeliveryControllerRegression.deID,latlong[0],latlong[1]);
        deliveryServiceHelper.makeDEActive(DeliveryControllerRegression.deID);
        deldatahelp.addDEzonemap(DeliveryControllerRegression.deID,zoneID);
        String deID1 = deldatahelp.CreateDE(zoneID);
        System.out.println("********************************************DE id 1 is "+deID1);
        deldatahelp.updateDELocationInRedis(deID1,latlong[0],latlong[1]);
        deliveryServiceHelper.makeDEActive(deID1);
        deldatahelp.addDEzonemap(deID1,zoneID);
        return new Object[][]{
                {orderId,DeliveryControllerRegression.deID,deID1}
        };
    }


    @DataProvider(name="verifyCancelledOrderNotConsiderForAssignment")
    public Object[][] verifyCancelledOrderNotConsiderForAssignment20(){
        deliveryServiceHelper.makeDEFree(DeliveryControllerRegression.deID);
        String orderId = new OrderBuilder.Builder(areaCode).build().getOrder_id();
        System.out.println("********************************************Order ID :" + orderId);
        String getRestaurantlatlon = "select r.lat_long from trips t inner join restaurant r on t.restaurant_id=r.id where t.order_id=" + orderId;
        String restaurantlatlon = delmeth.dbhelperget(getRestaurantlatlon, "lat_long").toString();
        String[] latlong = restaurantlatlon.split(",");
        deldatahelp.updateDELocationInRedis(DeliveryControllerRegression.deID,latlong[0],latlong[1]);
        deliveryServiceHelper.ordercancel(orderId);
        String batchId = "select batch_id from trips where order_id=" + orderId;
        String batchID = delmeth.dbhelperget(batchId, "batch_id").toString();
        Processor p = deliveryServiceApiWrapperHelper.DeAssignment(batchID,DeliveryControllerRegression.deID);
        String statusMessage =  p.ResponseValidator.GetNodeValue("$.statusMessage");
        System.out.println("***********************************************"+statusMessage);
        return new Object[][]{
                {statusMessage,orderId}
        };
    }

}
