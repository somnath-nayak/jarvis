package com.swiggy.api.erp.ff.tests.omtRewrite.omtMiddleware;

import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.erp.delivery.helper.DeliveryControllerHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryDataHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceHelper;
import com.swiggy.api.erp.ff.dp.omtRewrite.OmtMiddlewareData;
import com.swiggy.api.erp.ff.helper.OMSHelper;
import com.swiggy.api.erp.ff.helper.OmtMiddlewareHelper;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashMap;

public class GetDeliveryDetailsAPITest extends OmtMiddlewareData {

    OmtMiddlewareHelper helper = new OmtMiddlewareHelper();
    OMSHelper omsHelper = new OMSHelper();

    DeliveryServiceHelper deliveryServiceHelper = new DeliveryServiceHelper();
    DeliveryControllerHelper deliveryControllerHelper = new DeliveryControllerHelper();
    DeliveryDataHelper deliveryDataHelper= new DeliveryDataHelper();

    @Test(dataProvider = "getDeliveryDetailsData", description = "tests for OMS-Middleware API to get delivery details")
    public void getDeliveryDetailsAPITests(String orderId, String scenario, HashMap<String, Integer> expectedResponseMap, String testScenario) throws InterruptedException {
        System.out.println("***************************************** getDeliveryDetailsAPITests with "+scenario+" started *****************************************");

        if(testScenario.equals("positive")){

            helper.verifyDeliveryDetailsAfterOrderPlacing(orderId, expectedResponseMap.get("successCase"));
            omsHelper.verifyOrder(orderId, "111");
            deliveryDataHelper.updateDELocationInRedis(DeliveryConstant.snded_id);
            boolean assigned = deliveryServiceHelper.assignOrder(orderId);

            if(assigned){
                helper.verifyDeliveryDetailsAfterAssigned(orderId, expectedResponseMap.get("successCase"));
            }
            else {
                Assert.fail("DE could not be assigned to order_id = "+orderId);
            }

            Thread.sleep(5000);
            boolean confirmed = deliveryServiceHelper.zipDialConfirm(orderId);

            if(confirmed){
                helper.verifyDeliveryDetailsAfterConfirmed(orderId, expectedResponseMap.get("successCase"));
            }
            else {
                Assert.fail("Status could not be changed to confirmed for order_id = "+orderId);
            }
            Thread.sleep(2000);
            boolean arrived = deliveryServiceHelper.zipDialArrived(orderId);

            if(arrived){
                helper.verifyDeliveryDetailsAfterArived(orderId, expectedResponseMap.get("successCase"));
            }
            else {
                Assert.fail("Status could not be changed to Arrived for order_id = "+orderId);
            }

            Thread.sleep(2000);
            boolean pickedup = deliveryServiceHelper.zipDialPickedUp(orderId);

            if(pickedup){
                helper.verifyDeliveryDetailsAfterPickedup(orderId, expectedResponseMap.get("successCase"));
            }
            else {
                Assert.fail("Status could not be changed to pickedup for order_id = "+orderId);
            }

            Thread.sleep(2000);
            boolean reached = deliveryServiceHelper.zipDialReached(orderId);

            if(reached){
                helper.verifyDeliveryDetailsAfterReached(orderId, expectedResponseMap.get("successCase"));
            }
            else {
                Assert.fail("Status could not be changed to reached for order_id = "+orderId);
            }

            Thread.sleep(2000);
            boolean delivered = deliveryServiceHelper.zipDialDelivered(orderId);

            if(delivered){
                helper.verifyDeliveryDetailsAfterDelivered(orderId, expectedResponseMap.get("successCase"));
            }
            else {
                Assert.fail("Status could not be changed to delivered for order_id = "+orderId);
            }
        }else if (testScenario.equals("negative")){

            Processor response = helper.getDeliveryDetails(orderId);

            int unAuthorized = expectedResponseMap.get("unauthorizedCase");
            int success = expectedResponseMap.get("successCase");

            if (scenario.contains("Valid Order and invalid sessionId")){
                Assert.assertEquals(response.ResponseValidator.GetResponseCode(), unAuthorized);
                Assert.assertEquals(response.ResponseValidator.GetNodeValue("statusMessage"), "Not Authorized", "Expected status message did not match with Actual");

            }else if(scenario.contains("Invalid Order and valid sessionId")){
                Assert.assertEquals(response.ResponseValidator.GetResponseCode(), success);
                Assert.assertEquals(response.ResponseValidator.GetNodeValue("statusMessage"), "Success!", "Expected status message did not match with Actual");
                Assert.assertSame(response.ResponseValidator.GetNodeValue("data"), null, "Expected data did not match with Actual");

            }else if (scenario.contains("Invalid Order and invalid sessionId")){
                Assert.assertEquals(response.ResponseValidator.GetResponseCode(), unAuthorized);
                Assert.assertEquals(response.ResponseValidator.GetNodeValue("statusMessage"), "Not Authorized", "Expected status message did not match with Actual");

            }
        }

        System.out.println("######################################### getDeliveryDetailsAPITests with "+scenario+" compleated #########################################");
    }

    @Test(dataProvider = "getPaymentDetailsAsPostPutPatchDeleteData", description = "tests for OMS-Middleware API to get delivery details by hitting it as POST")
    public void getDeliveryDetailsAPIAsPOSTTests(String orderId, int expectedResponse){
        System.out.println("***************************************** getDeliveryDetailsAPIAsPOSTTests started *****************************************");

        Processor response =  helper.getDeliveryDetailsAsPost(orderId);

        Assert.assertEquals(response.ResponseValidator.GetResponseCode(), expectedResponse, "Actual respponse did not match with the expected response");

        System.out.println("######################################### getDeliveryDetailsAPIAsPOSTTests compleated #########################################");
    }

    @Test(dataProvider = "getPaymentDetailsAsPostPutPatchDeleteData", description = "tests for OMS-Middleware API to get delivery details by hitting it as PUT")
    public void getDeliveryDetailsAPIAsPUTTests(String orderId, int expectedResponse){
        System.out.println("***************************************** getDeliveryDetailsAPIAsPOSTTests started *****************************************");

        Processor response =  helper.getDeliveryDetailsAsPut(orderId);

        Assert.assertEquals(response.ResponseValidator.GetResponseCode(), expectedResponse, "Actual respponse did not match with the expected response");

        System.out.println("######################################### getDeliveryDetailsAPIAsPOSTTests compleated #########################################");
    }

    @Test(dataProvider = "getPaymentDetailsAsPostPutPatchDeleteData", description = "tests for OMS-Middleware API to get delivery details by hitting it as PATCH")
    public void getDeliveryDetailsAPIAsPATCHTests(String orderId, int expectedResponse){
        System.out.println("***************************************** getDeliveryDetailsAPIAsPOSTTests started *****************************************");

        Processor response =  helper.getDeliveryDetailsAsPatch(orderId);

        Assert.assertEquals(response.ResponseValidator.GetResponseCode(), expectedResponse, "Actual respponse did not match with the expected response");

        System.out.println("######################################### getDeliveryDetailsAPIAsPOSTTests compleated #########################################");
    }

    @Test(dataProvider = "getPaymentDetailsAsPostPutPatchDeleteData", description = "tests for OMS-Middleware API to get delivery details by hitting it as DELETE")
    public void getDeliveryDetailsAPIAsDELETETests(String orderId, int expectedResponse){
        System.out.println("***************************************** getDeliveryDetailsAPIAsPOSTTests started *****************************************");

        Processor response =  helper.getDeliveryDetailsAsDelete(orderId);

        Assert.assertEquals(response.ResponseValidator.GetResponseCode(), expectedResponse, "Actual respponse did not match with the expected response");

        System.out.println("######################################### getDeliveryDetailsAPIAsPOSTTests compleated #########################################");
    }

}
