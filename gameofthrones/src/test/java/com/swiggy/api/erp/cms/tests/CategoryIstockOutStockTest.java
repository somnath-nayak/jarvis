package com.swiggy.api.erp.cms.tests;

import org.codehaus.jettison.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.constants.Constants;
import com.swiggy.api.erp.cms.dp.CategoryInStockOutStockDp;
import com.swiggy.api.erp.cms.helper.CategoryInStockOutStockHelper;
import com.swiggy.api.sf.snd.constants.SANDConstants;

import framework.gameofthrones.Aegon.Initialize;

public class CategoryIstockOutStockTest extends CategoryInStockOutStockDp {
	CategoryInStockOutStockHelper catInOutStock = new CategoryInStockOutStockHelper();
	Initialize gameofthrones = new Initialize();

	@Test(dataProvider = "CategoryOutOfStock", priority = 0, description = "CategoryOutOfStock marks all items outof stock by applying holiday slots")
	public void CategoryOutOfStock(String restId, String categoryId, String fromTime, String toTime, boolean success)
			throws Exception {

		String response = catInOutStock.category_OutStock(restId, categoryId, fromTime, toTime).ResponseValidator
				.GetBodyAsText();
		int StatusCode = Integer.parseInt(JsonPath.read(response, "$.statusCode").toString());
		if (success == true) {
			String status = JsonPath.read(response, "$.data.category_in_stock_status").toString().replace("[", "")
					.replace("]", "");
			Assert.assertEquals(StatusCode, Constants.statusOne, "success");
			Assert.assertEquals(status, Constants.categoryInStockStatusFalse, "Success");
	
		} else
			Assert.assertEquals(StatusCode, Constants.statusZero, "Expecting valid input data");
	}

	@Test(dataProvider = "CategoryInStock", priority = 0, description = "CategoryOutOfStock marks all items variants and addons outof stock")
	public void CategoryInStock(String restId, String categoryId,boolean success)
			throws Exception{
		String response = catInOutStock.categoryInStock(restId, categoryId).ResponseValidator
				.GetBodyAsText();
		int StatusCode = Integer.parseInt(JsonPath.read(response, "$.statusCode").toString());
		if (success == true) {
			String status = JsonPath.read(response, "$.data.category_in_stock_status").toString().replace("[", "")
					.replace("]", "");
			Assert.assertEquals(StatusCode, Constants.statusOne, "success");
			Assert.assertEquals(status, Constants.categoryInStockStatustrue, "Success");
		} else
			Assert.assertEquals(StatusCode, Constants.statusZero, "Expecting valid input data");
	}
}
