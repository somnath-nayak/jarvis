package com.swiggy.api.erp.ff.tests.omtRewrite.omtBackend;

import com.rabbitmq.client.AMQP;
import com.swiggy.api.erp.ff.constants.LosConstants;
import com.swiggy.api.erp.ff.constants.OmtConstants;
import com.swiggy.api.erp.ff.helper.OMSHelper;
import com.swiggy.api.erp.ff.helper.PlacingServiceHelper;
import framework.gameofthrones.Tyrion.DBHelper;
import framework.gameofthrones.Tyrion.RabbitMQHelper;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;

import com.swiggy.api.erp.ff.helper.LOSHelper;
import com.swiggy.api.erp.ff.dp.losDataProvider;


public class OmtBackendAPITests extends losDataProvider{

    DBHelper dbHelper = new DBHelper();
    Logger log = Logger.getLogger(OmtBackendAPITests.class);
    LOSHelper losHelper = new LOSHelper();
    OMSHelper omshelper = new OMSHelper();
    RabbitMQHelper rmqHelper = new RabbitMQHelper();


    @Test(dataProvider ="orderCreationData",groups = {"sanity", "regression"},description = "Verify Manual Restaurant With Partner Status order consumption from Placing Service ")
    public void testManualRestaurantWithRelayerEvent(String order_id,String order_time,String epochTime) throws Exception {

        log.info("***************************************** test testManualRestaurantWithRelayerEvent WithPartnerStatusUpdateConsumptionFromPlacingService started *****************************************");
        losHelper.createOrderWithPartner(order_id, order_time, epochTime);
        Assert.assertTrue(losHelper.validateOrderExistInOMSOrderTable(order_id, 12), "Order is not present in oms_order table");
        omshelper.verifyOrder("" + order_id, "001");
        Assert.assertTrue(dbHelper.pollDB(LosConstants.hostName, "select placed_status from oms_orderstatus where id = (select status_id from oms_order where order_id="+order_id+");", "placed_status", "with_partner", 10, 60), "Orderid is not found in DB");
        log.info("***************************************** test testManualRestaurantWithRelayerEvent WithPartnerStatusUpdateConsumptionFromPlacingService Completed *****************************************");

    }

    @Test(dataProvider ="orderCreationData",groups = {"sanity", "regression"},description = "Verify ThirdParty Restaurant With Partner Status order consumption from Placing Service ")
    public void testThirdPartyRestaurantRelayedEvent(String order_id,String order_time,String epochTime) throws IOException{
        log.info("***************************************** test ThirdPartyRestaurantRelayedEvent WithPartnerStatusUpdateConsumptionFromPlacingService started *****************************************");
        losHelper.createThirdPartyOrder(order_id, order_time, epochTime);
        Assert.assertTrue(losHelper.validateOrderExistInOMSOrderTable(order_id, 12), "Order is not present in oms_order table");
        omshelper.verifyOrder("" + order_id, "001");

        File file = new File("../Data/Payloads/JSON/ff_thirdPartyRelayedEventFromPlacingService.json");
        String message = FileUtils.readFileToString(file);
        PlacingServiceHelper helper = new PlacingServiceHelper();
        String timeStamp = helper.getTimeStampForPlacingServiceMessage();
        message = message.replace("${timeStamp}", timeStamp).replace("${orderId}", order_id);
        rmqHelper.pushMessageToExchange(LosConstants.hostName, OmtConstants.FF_VENDOR_PLACING_SERVICE_QUEUE, new AMQP.BasicProperties().builder().contentType("application/json"), message);

        Assert.assertTrue(dbHelper.pollDB(LosConstants.hostName, "select placed_status from oms_orderstatus where id = (select status_id from oms_order where order_id="+order_id+");", "placed_status", "with_partner", 10, 60), "Orderid is not found in DB");
        log.info("***************************************** test ThirdPartyRestaurantRelayedEvent WithPartnerStatusUpdateConsumptionFromPlacingService completed *****************************************");
    }
}
