package com.swiggy.api.erp.ff.helper;

import com.swiggy.api.erp.delivery.helper.DeliveryServiceHelper;
import com.swiggy.api.erp.ff.constants.OmtConstants;
import com.swiggy.api.sf.checkout.helper.AddressHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.helper.edvo.helper.CartValidator;
import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import com.swiggy.api.sf.snd.helper.SnDHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import framework.gameofthrones.Tyrion.JsonHelper;
import framework.gameofthrones.Tyrion.RabbitMQHelper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import java.util.List;
import java.util.Map;

public class OmtMiddlewareHelper implements OmtConstants {

    Initialize gameofthrones = new Initialize();
	LOSHelper losHelper = new LOSHelper();
	RabbitMQHelper rmqHelper = new RabbitMQHelper();
	OMSHelper omsHelper = new OMSHelper();
    CheckoutHelper helper= new CheckoutHelper();
    SnDHelper sndHelper= new SnDHelper();
    SANDHelper sandHelper= new SANDHelper();
    CartValidator cartValidator =new CartValidator();
    SchemaValidatorUtils schemaValidatorUtils=new SchemaValidatorUtils();
    RngHelper rngHelper = new RngHelper();
    JsonHelper jsonHelper = new JsonHelper();
    String order_id=null;
    String order_key=null;
    String order=null;
	
    DeliveryServiceHelper deliveryServiceHelper = new DeliveryServiceHelper();
    AddressHelper addressHelper=new AddressHelper();

    public Processor getPaymentDetails(String orderId) {
        GameOfThronesService service = new GameOfThronesService(OmtConstants.SERVICE_MIDDLEWARE, "getpaymentdetails", gameofthrones);
        Processor processor = new Processor(null, null, new String[]{orderId}, null, service);
        return processor;
    }

    public Processor getPaymentDetailsAsPost(String orderId) {
        GameOfThronesService service = new GameOfThronesService(OmtConstants.SERVICE_MIDDLEWARE, "getpaymentdetailsaspost", gameofthrones);
        Processor processor = new Processor(null, null, new String[]{orderId}, null, service);
        return processor;
    }

    public Processor getPaymentDetailsAsPut(String orderId) {
        GameOfThronesService service = new GameOfThronesService(OmtConstants.SERVICE_MIDDLEWARE, "getpaymentdetailsasput", gameofthrones);
        Processor processor = new Processor(null, null, new String[]{orderId}, null, service);
        return processor;
    }

    public Processor getPaymentDetailsAsDelete(String orderId) {
        GameOfThronesService service = new GameOfThronesService(OmtConstants.SERVICE_MIDDLEWARE, "getpaymentdetailsasdelete", gameofthrones);
        Processor processor = new Processor(null, null, new String[]{orderId}, null, service);
        return processor;
    }

    public Processor getPaymentDetailsAsPatch(String orderId) {
        GameOfThronesService service = new GameOfThronesService(OmtConstants.SERVICE_MIDDLEWARE, "getpaymentdetailsaspatch", gameofthrones);
        Processor processor = new Processor(null, null, new String[]{orderId}, null, service);
        return processor;
    }

    public Processor getDeliveryDetails(String orderId) {
        GameOfThronesService service = new GameOfThronesService(OmtConstants.SERVICE_MIDDLEWARE, "getdeliverydetails", gameofthrones);
        Processor processor = new Processor(service, null, null, new String[]{orderId});
        return processor;
    }

    public Processor getDeliveryDetailsAsPost(String orderId) {
        GameOfThronesService service = new GameOfThronesService(OmtConstants.SERVICE_MIDDLEWARE, "getdeliverydetailsaspost", gameofthrones);
        Processor processor = new Processor(service,null, null, new String[]{orderId});
        return processor;
    }

    public Processor getDeliveryDetailsAsPut(String orderId) {
        GameOfThronesService service = new GameOfThronesService(OmtConstants.SERVICE_MIDDLEWARE, "getdeliverydetailsasput", gameofthrones);
        Processor processor = new Processor(service, null, null, new String[]{orderId});
        return processor;
    }

    public Processor getDeliveryDetailsAsDelete(String orderId) {
        GameOfThronesService service = new GameOfThronesService(OmtConstants.SERVICE_MIDDLEWARE, "getdeliverydetailsasdelete", gameofthrones);
        Processor processor = new Processor(service,null, null, new String[]{orderId});
        return processor;
    }

    public Processor getDeliveryDetailsAsPatch(String orderId) {
        GameOfThronesService service = new GameOfThronesService(OmtConstants.SERVICE_MIDDLEWARE, "getdeliverydetailsaspatch", gameofthrones);
        Processor processor = new Processor(null, null, new String[]{orderId}, null, service);
        return processor;
    }

    public void verifyDeliveryDetailsAfterOrderPlacing(String orderId, int expectedResponse) {

        Processor response = getDeliveryDetails(orderId);
//        Processor deliveryResponse = deliveryServiceHelper.getOrderStatus(orderId);

        Assert.assertEquals(response.ResponseValidator.GetResponseCode(), expectedResponse, "Actual respponse did not match with the expected response " + orderId);

        SoftAssert softAssertion = new SoftAssert();
        softAssertion.assertEquals(response.ResponseValidator.GetNodeValue("$.statusMessage"), "Success!", "Actual Status message did not match with expected " + orderId);
        softAssertion.assertEquals(response.ResponseValidator.GetNodeValue("data.status"), "unassigned", "Actual status in data did not match with expected " + orderId);
        softAssertion.assertEquals(response.ResponseValidator.GetNodeValue("data.order_id"), orderId, "Actual payment method did not match with expected " + orderId);
        softAssertion.assertAll();
    }

    public void verifyDeliveryDetailsAfterAssigned(String orderId, int expectedResponse){

        Processor response = getDeliveryDetails(orderId);
//        Processor deliveryResponse = deliveryServiceHelper.getOrderStatus(orderId);

        Assert.assertEquals(response.ResponseValidator.GetResponseCode(), expectedResponse, "Actual respponse did not match with the expected response " + orderId);

        SoftAssert softAssertion = new SoftAssert();
        softAssertion.assertEquals(response.ResponseValidator.GetNodeValue("$.statusMessage"), "Success!", "Actual Status message did not match with expected " + orderId);
        softAssertion.assertEquals(response.ResponseValidator.GetNodeValue("data.status"), "assigned", "Actual status in data did not match with expected " + orderId);
        softAssertion.assertEquals(response.ResponseValidator.GetNodeValue("data.order_id"), orderId, "Actual payment method did not match with expected " + orderId);
        softAssertion.assertFalse(response.ResponseValidator.GetNodeValue("data.deDetails.mobileNumber").equals(null));
        softAssertion.assertFalse(response.ResponseValidator.GetNodeValue("data.deDetails.nick").equals(null));
        softAssertion.assertFalse(response.ResponseValidator.GetNodeValue("data.deDetails.id").equals(null));
        softAssertion.assertFalse(response.ResponseValidator.GetNodeValue("data.deDetails.altMobile").equals(null));
        softAssertion.assertFalse(response.ResponseValidator.GetNodeValue("data.deDetails.name").equals(null));
        softAssertion.assertFalse(response.ResponseValidator.GetNodeValue("data.timestamps.assigned").equals(null));
        softAssertion.assertAll();

    }

    public void verifyDeliveryDetailsAfterConfirmed(String orderId, int expectedResponse){

        Processor response = getDeliveryDetails(orderId);
//        Processor deliveryResponse = deliveryServiceHelper.getOrderStatus(orderId);

        Assert.assertEquals(response.ResponseValidator.GetResponseCode(), expectedResponse, "Actual respponse did not match with the expected response " + orderId);
        SoftAssert softAssertion = new SoftAssert();
        softAssertion.assertEquals(response.ResponseValidator.GetNodeValue("$.statusMessage"), "Success!", "Actual Status message did not match with expected " + orderId);
        softAssertion.assertEquals(response.ResponseValidator.GetNodeValue("data.status"), "confirmed", "Actual status in data did not match with expected " + orderId);
        softAssertion.assertEquals(response.ResponseValidator.GetNodeValue("data.order_id"), orderId, "Actual payment method did not match with expected " + orderId);
        softAssertion.assertFalse(response.ResponseValidator.GetNodeValue("data.deDetails.mobileNumber").equals(null));
        softAssertion.assertFalse(response.ResponseValidator.GetNodeValue("data.deDetails.nick").equals(null));
        softAssertion.assertFalse(response.ResponseValidator.GetNodeValue("data.deDetails.id").equals(null));
        softAssertion.assertFalse(response.ResponseValidator.GetNodeValue("data.deDetails.altMobile").equals(null));
        softAssertion.assertFalse(response.ResponseValidator.GetNodeValue("data.deDetails.name").equals(null));
        softAssertion.assertFalse(response.ResponseValidator.GetNodeValue("data.timestamps.confirmed").equals(null));
        softAssertion.assertAll();

    }

    public void verifyDeliveryDetailsAfterArived(String orderId, int expectedResponse){

        Processor response = getDeliveryDetails(orderId);
//        Processor deliveryResponse = deliveryServiceHelper.getOrderStatus(orderId);
//        System.out.println(deliveryResponse);

        Assert.assertEquals(response.ResponseValidator.GetResponseCode(), expectedResponse, "Actual respponse did not match with the expected response " + orderId);
        SoftAssert softAssertion = new SoftAssert();
        softAssertion.assertEquals(response.ResponseValidator.GetNodeValue("$.statusMessage"), "Success!", "Actual Status message did not match with expected " + orderId);
        softAssertion.assertEquals(response.ResponseValidator.GetNodeValue("data.status"), "arrived", "Actual status in data did not match with expected " + orderId);
        softAssertion.assertEquals(response.ResponseValidator.GetNodeValue("data.order_id"), orderId, "Actual payment method did not match with expected " + orderId);
        softAssertion.assertFalse(response.ResponseValidator.GetNodeValue("data.deDetails.mobileNumber").equals(null));
        softAssertion.assertFalse(response.ResponseValidator.GetNodeValue("data.deDetails.nick").equals(null));
        softAssertion.assertFalse(response.ResponseValidator.GetNodeValue("data.deDetails.id").equals(null));
        softAssertion.assertFalse(response.ResponseValidator.GetNodeValue("data.deDetails.altMobile").equals(null));
        softAssertion.assertFalse(response.ResponseValidator.GetNodeValue("data.deDetails.name").equals(null));
        softAssertion.assertFalse(response.ResponseValidator.GetNodeValue("data.timestamps.arrived").equals(null));
        softAssertion.assertAll();

    }

    public void verifyDeliveryDetailsAfterPickedup(String orderId, int expectedResponse){

        Processor response = getDeliveryDetails(orderId);
//        Processor deliveryResponse = deliveryServiceHelper.getOrderStatus(orderId);

        Assert.assertEquals(response.ResponseValidator.GetResponseCode(), expectedResponse, "Actual respponse did not match with the expected response " + orderId);

        SoftAssert softAssertion = new SoftAssert();
        softAssertion.assertEquals(response.ResponseValidator.GetNodeValue("$.statusMessage"), "Success!", "Actual Status message did not match with expected " + orderId);
        softAssertion.assertEquals(response.ResponseValidator.GetNodeValue("data.status"), "pickedup", "Actual status in data did not match with expected " + orderId);
        softAssertion.assertEquals(response.ResponseValidator.GetNodeValue("data.order_id"), orderId, "Actual payment method did not match with expected " + orderId);
        softAssertion.assertFalse(response.ResponseValidator.GetNodeValue("data.deDetails.mobileNumber").equals(null));
        softAssertion.assertFalse(response.ResponseValidator.GetNodeValue("data.deDetails.nick").equals(null));
        softAssertion.assertFalse(response.ResponseValidator.GetNodeValue("data.deDetails.id").equals(null));
        softAssertion.assertFalse(response.ResponseValidator.GetNodeValue("data.deDetails.altMobile").equals(null));
        softAssertion.assertFalse(response.ResponseValidator.GetNodeValue("data.deDetails.name").equals(null));
        softAssertion.assertFalse(response.ResponseValidator.GetNodeValue("data.timestamps.pickedup").equals(null));
        softAssertion.assertAll();

    }

    public void verifyDeliveryDetailsAfterReached(String orderId, int expectedResponse){

        Processor response = getDeliveryDetails(orderId);
//        Processor deliveryResponse = deliveryServiceHelper.getOrderStatus(orderId);

        Assert.assertEquals(response.ResponseValidator.GetResponseCode(), expectedResponse, "Actual respponse did not match with the expected response " + orderId);
        SoftAssert softAssertion = new SoftAssert();
        softAssertion.assertEquals(response.ResponseValidator.GetNodeValue("$.statusMessage"), "Success!", "Actual Status message did not match with expected " + orderId);
        softAssertion.assertEquals(response.ResponseValidator.GetNodeValue("data.status"), "reached", "Actual status in data did not match with expected " + orderId);
        softAssertion.assertEquals(response.ResponseValidator.GetNodeValue("data.order_id"), orderId, "Actual payment method did not match with expected " + orderId);
        softAssertion.assertFalse(response.ResponseValidator.GetNodeValue("data.deDetails.mobileNumber").equals(null));
        softAssertion.assertFalse(response.ResponseValidator.GetNodeValue("data.deDetails.nick").equals(null));
        softAssertion.assertFalse(response.ResponseValidator.GetNodeValue("data.deDetails.id").equals(null));
        softAssertion.assertFalse(response.ResponseValidator.GetNodeValue("data.deDetails.altMobile").equals(null));
        softAssertion.assertFalse(response.ResponseValidator.GetNodeValue("data.deDetails.name").equals(null));
        softAssertion.assertFalse(response.ResponseValidator.GetNodeValue("data.timestamps.reached").equals(null));
        softAssertion.assertAll();
    }

    public void verifyDeliveryDetailsAfterDelivered(String orderId, int expectedResponse){
        Processor response = getDeliveryDetails(orderId);
//        Processor deliveryResponse = deliveryServiceHelper.getOrderStatus(orderId);

        Assert.assertEquals(response.ResponseValidator.GetResponseCode(), expectedResponse, "Actual respponse did not match with the expected response " + orderId);
        SoftAssert softAssertion = new SoftAssert();
        softAssertion.assertEquals(response.ResponseValidator.GetNodeValue("$.statusMessage"), "Success!", "Actual Status message did not match with expected " + orderId);
        softAssertion.assertEquals(response.ResponseValidator.GetNodeValue("data.status"), "delivered", "Actual status in data did not match with expected " + orderId);
        softAssertion.assertEquals(response.ResponseValidator.GetNodeValue("data.order_id"), orderId, "Actual payment method did not match with expected " + orderId);
        softAssertion.assertFalse(response.ResponseValidator.GetNodeValue("data.deDetails.mobileNumber").equals(null));
        softAssertion.assertFalse(response.ResponseValidator.GetNodeValue("data.deDetails.nick").equals(null));
        softAssertion.assertFalse(response.ResponseValidator.GetNodeValue("data.deDetails.id").equals(null));
        softAssertion.assertFalse(response.ResponseValidator.GetNodeValue("data.deDetails.altMobile").equals(null));
        softAssertion.assertFalse(response.ResponseValidator.GetNodeValue("data.deDetails.name").equals(null));
        softAssertion.assertFalse(response.ResponseValidator.GetNodeValue("data.timestamps.delivered").equals(null));
        softAssertion.assertAll();
    }

    /**
     *  helper functions for customer details API
     */
       
    public Processor getCustomerDetails(String orderId) {
        GameOfThronesService service = new GameOfThronesService(OmtConstants.SERVICE_MIDDLEWARE, "getcustomerdetails",gameofthrones);
        Processor processor = new Processor(null, null, new String[] { orderId }, null, service);
        return processor;
    }
    
    public long getExpectedCustomerId(long order_id) throws JSONException{
        List<Map<String, Object>> orderDetailsList = SystemConfigProvider.getTemplate(omsDBHost).queryForList("select * from oms_rawdata where order_id = "+ order_id  +";");
        String payload = (String)orderDetailsList.get(0).get("payload");
        JSONObject jsonObj = new JSONObject(payload.toString());
        return jsonObj.getLong("customer_id");      
    }

    public String getCustomerName(long order_id) throws JSONException{
        List<Map<String, Object>> customerDetailsList = SystemConfigProvider.getTemplate(omsDBHost).queryForList("select * from oms_rawdata where order_id = "+ order_id  +";");
        String payload = (String)customerDetailsList.get(0).get("payload");
        JSONObject jsonObj = new JSONObject(payload.toString());
        JSONObject obj1 = new JSONObject();
        obj1 = jsonObj.getJSONObject("delivery_address");    
        return obj1.getString("name");
    }
    
    public Long getCustomerMobile(long order_id) throws JSONException{
        List<Map<String, Object>> customerDetailsList = SystemConfigProvider.getTemplate(omsDBHost).queryForList("select * from oms_rawdata where order_id = "+ order_id  +";");
        String payload = (String)customerDetailsList.get(0).get("payload");
        JSONObject jsonObj = new JSONObject(payload.toString());
        JSONObject obj1 = new JSONObject();
        obj1 = jsonObj.getJSONObject("delivery_address");   
        return obj1.getLong("mobile");
    }

    public String getCustomerEmail(long order_id) throws JSONException{
        List<Map<String, Object>> customerDetailsList = SystemConfigProvider.getTemplate(omsDBHost).queryForList("select * from oms_rawdata where order_id = "+ order_id  +";");
        String payload = (String)customerDetailsList.get(0).get("payload");
        JSONObject jsonObj = new JSONObject(payload.toString());
        JSONObject obj1 = new JSONObject();
        obj1 = jsonObj.getJSONObject("delivery_address");    
        return obj1.getString("email");
    }
    
    public String getCustomerAddressLine(long order_id) throws JSONException{
        List<Map<String, Object>> customerDetailsList = SystemConfigProvider.getTemplate(omsDBHost).queryForList("select * from oms_rawdata where order_id = "+ order_id  +";");
        String payload = (String)customerDetailsList.get(0).get("payload");
        JSONObject jsonObj = new JSONObject(payload.toString());
        JSONObject obj1 = new JSONObject();
        obj1 = jsonObj.getJSONObject("delivery_address");     
        return obj1.getString("address");
    }
    
    public String getCustomerFlatNumber(long order_id) throws JSONException{
        List<Map<String, Object>> customerDetailsList = SystemConfigProvider.getTemplate(omsDBHost).queryForList("select * from oms_rawdata where order_id = "+ order_id  +";");
        String payload = (String)customerDetailsList.get(0).get("payload");
        JSONObject jsonObj = new JSONObject(payload.toString());
        JSONObject obj1 = new JSONObject();
        obj1 = jsonObj.getJSONObject("delivery_address");     
        return obj1.getString("flat_no");
    }
    
    public String getCustomerCity(long order_id) throws JSONException{
        List<Map<String, Object>> customerDetailsList = SystemConfigProvider.getTemplate(omsDBHost).queryForList("select * from oms_rawdata where order_id = "+ order_id  +";");
        String payload = (String)customerDetailsList.get(0).get("payload");
        JSONObject jsonObj = new JSONObject(payload.toString());
        JSONObject obj1 = new JSONObject();
        obj1 = jsonObj.getJSONObject("delivery_address");    
        return obj1.getString("city");
    }
    
    public String getCustomerLandmark(long order_id) throws JSONException{
        List<Map<String, Object>> customerDetailsList = SystemConfigProvider.getTemplate(omsDBHost).queryForList("select * from oms_rawdata where order_id = "+ order_id  +";");
        String payload = (String)customerDetailsList.get(0).get("payload");
        JSONObject jsonObj = new JSONObject(payload.toString());
        JSONObject obj1 = new JSONObject();
        obj1 = jsonObj.getJSONObject("delivery_address");     
        return obj1.getString("landmark");
    }
    
    public String getCustomerArea(long order_id) throws JSONException{
        List<Map<String, Object>> customerDetailsList = SystemConfigProvider.getTemplate(omsDBHost).queryForList("select * from oms_rawdata where order_id = "+ order_id  +";");
        String payload = (String)customerDetailsList.get(0).get("payload");
        JSONObject jsonObj = new JSONObject(payload.toString());
        JSONObject obj1 = new JSONObject();
        obj1 = jsonObj.getJSONObject("delivery_address");      
        return obj1.getString("area");
    }
        
    public String getCustomerlat(long order_id) throws JSONException{
        List<Map<String, Object>> customerDetailsList = SystemConfigProvider.getTemplate(omsDBHost).queryForList("select * from oms_rawdata where order_id = "+ order_id  +";");
        String payload = (String)customerDetailsList.get(0).get("payload");
        JSONObject jsonObj = new JSONObject(payload.toString());
        JSONObject obj1 = new JSONObject();
        obj1 = jsonObj.getJSONObject("delivery_address");     
        return obj1.getString("lat");
    }
    
    public String getCustomerLng(long order_id) throws JSONException{
        List<Map<String, Object>> customerDetailsList = SystemConfigProvider.getTemplate(omsDBHost).queryForList("select * from oms_rawdata where order_id = "+ order_id  +";");
        String payload = (String)customerDetailsList.get(0).get("payload");
        JSONObject jsonObj = new JSONObject(payload.toString());
        JSONObject obj1 = new JSONObject();
        obj1 = jsonObj.getJSONObject("delivery_address");     
        return obj1.getString("lng");
    }

    public Processor getCustomerDetailsAsPost(String orderId) {
        GameOfThronesService service = new GameOfThronesService(OmtConstants.SERVICE_MIDDLEWARE, "getcustomerdetailsaspost", gameofthrones);
        Processor processor = new Processor(null, null, new String[] { orderId }, null, service);
        return processor;
    }

    public Processor getCustomerDetailsAsPut(String orderId) {
        GameOfThronesService service = new GameOfThronesService(OmtConstants.SERVICE_MIDDLEWARE,"getcustomerdetailsasput", gameofthrones);
        Processor processor = new Processor(null, null, new String[] { orderId }, null, service);
        return processor;
    }

    public Processor getCustomerDetailsAsDelete(String orderId) {
        GameOfThronesService service = new GameOfThronesService(OmtConstants.SERVICE_MIDDLEWARE,"getcustomerdetailsasdelete", gameofthrones);
        Processor processor = new Processor(null, null, new String[] { orderId }, null, service);
        return processor;
    }
    
    /*
     * helper functions to order details API for swiggy super big bet
     * 
     */

    public Processor getOrderDetails(String orderId) {
        GameOfThronesService service = new GameOfThronesService(OmtConstants.SERVICE_MIDDLEWARE, "getorderdetails", gameofthrones);
        Processor processor = new Processor(null, null, new String[] { orderId }, null, service);
        return processor;
    }
    
    public JSONArray getOrderTags(long order_id) throws JSONException{
        List<Map<String, Object>> orderDetailsList = SystemConfigProvider.getTemplate(omsDBHost).queryForList("select * from oms_rawdata where order_id = "+ order_id  +";");
        String payload = (String)orderDetailsList.get(0).get("payload");
        JSONObject jsonObj = new JSONObject(payload.toString());
        return jsonObj.getJSONArray("order_tags");
    }
        
    public Processor getOrderDetailsAsPost(String orderId) {
        GameOfThronesService service = new GameOfThronesService(OmtConstants.SERVICE_MIDDLEWARE, "getorderdetailsaspost", gameofthrones);
        Processor processor = new Processor(null, null, new String[] { orderId }, null, service);
        return processor;
    }

    public Processor getOrderDetailsAsPut(String orderId) {
        GameOfThronesService service = new GameOfThronesService(OmtConstants.SERVICE_MIDDLEWARE,"getorderdetailsasput", gameofthrones);
        Processor processor = new Processor(null, null, new String[] { orderId }, null, service);
        return processor;
    }

    public Processor getOrderDetailsAsDelete(String orderId) {
        GameOfThronesService service = new GameOfThronesService(OmtConstants.SERVICE_MIDDLEWARE,"getorderdetailsasdelete", gameofthrones);
        Processor processor = new Processor(null, null, new String[] { orderId }, null, service);
        return processor;
    }
}