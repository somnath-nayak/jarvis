package com.swiggy.api.erp.cms.pojo.SelfServeV1Item;

public class Variants_vo {
	 private Variant variant;

	    public Variant getVariant ()
	    {
	        return variant;
	    }

	    public void setVariant (Variant variant)
	    {
	        this.variant = variant;
	    }

	    @Override
	    public String toString()
	    {
	        return "ClassPojo [variant = "+variant+"]";
	    }
	}
				
				