package com.swiggy.api.erp.cms.pojo;

import com.swiggy.api.erp.cms.constants.MenuConstants;
import com.swiggy.api.erp.cms.pojo.Items.Gst_details;

/**
 * Created by kiran.j on 3/12/18.
 */
public class CreateVariant
{
    private String created_by;

    private int in_stock;

    private int is_veg;

    private String id;

    private String third_party_id;

    private String variant_group_id;

    private String updated_at;

    private String price;

    private int order;

    private String name;

    private Gst_details gst_details;

    private String updated_by;

    private String active;

    private String created_at;

    private int Default;

    public String getCreated_by ()
    {
        return created_by;
    }

    public void setCreated_by (String created_by)
    {
        this.created_by = created_by;
    }

    public int getIn_stock ()
    {
        return in_stock;
    }

    public void setIn_stock (int in_stock)
    {
        this.in_stock = in_stock;
    }

    public int getDefault() {
        return Default;
    }

    public void setDefault(int aDefault) {
        Default = aDefault;
    }

    public int getIs_veg ()
    {
        return is_veg;
    }

    public void setIs_veg (int is_veg)
    {
        this.is_veg = is_veg;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getThird_party_id ()
    {
        return third_party_id;
    }

    public void setThird_party_id (String third_party_id)
    {
        this.third_party_id = third_party_id;
    }

    public String getVariant_group_id ()
    {
        return variant_group_id;
    }

    public void setVariant_group_id (String variant_group_id)
    {
        this.variant_group_id = variant_group_id;
    }

    public String getUpdated_at ()
    {
        return updated_at;
    }

    public void setUpdated_at (String updated_at)
    {
        this.updated_at = updated_at;
    }

    public String getPrice ()
    {
        return price;
    }

    public void setPrice (String price)
    {
        this.price = price;
    }

    public int getOrder ()
    {
        return order;
    }

    public void setOrder (int order)
    {
        this.order = order;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public Gst_details getGst_details ()
    {
        return gst_details;
    }

    public void setGst_details (Gst_details gst_details)
    {
        this.gst_details = gst_details;
    }

    public String getUpdated_by ()
    {
        return updated_by;
    }

    public void setUpdated_by (String updated_by)
    {
        this.updated_by = updated_by;
    }

    public String getActive ()
    {
        return active;
    }

    public void setActive (String active)
    {
        this.active = active;
    }

    public String getCreated_at ()
    {
        return created_at;
    }

    public void setCreated_at (String created_at)
    {
        this.created_at = created_at;
    }

    public CreateVariant build() {

        this.setIn_stock(MenuConstants.enabled);
        this.setOrder(MenuConstants.enabled);
        return this;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [created_by = "+created_by+", in_stock = "+in_stock+", is_veg = "+is_veg+", id = "+id+", third_party_id = "+third_party_id+", variant_group_id = "+variant_group_id+", updated_at = "+updated_at+", price = "+price+", order = "+order+", name = "+name+", gst_details = "+gst_details+", updated_by = "+updated_by+", active = "+active+", created_at = "+created_at+"]";
    }
}