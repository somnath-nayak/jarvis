package com.swiggy.api.erp.cms.tests.catalogAttribute;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.dp.itemDP;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Tyrion.RabbitMQHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public class VerifyFeedbackCuisine extends itemDP {

    Initialize gameofthrones = new Initialize();
    RabbitMQHelper helper=new RabbitMQHelper();
    CMSHelper cmsHelper= new CMSHelper();
    SqlTemplate sqlTemplateCI = SystemConfigProvider.getTemplate("cms");

    String itemName;
    int itmId;
    @Test(dataProvider="updateitm",description = "Update Item API")
    public void updateItem(String comment, String description, String name,String itemId){
        String response=cmsHelper.updateItem(comment, description, name,itemId).ResponseValidator.GetBodyAsText();
        itmId=JsonPath.read(response,"$.data.id");
        itemName=JsonPath.read(response,"$.data.name");
    }

    @Test(dependsOnMethods = "updateItem",description = "Verify Item id is present cuisine feedback table")
    public void predScoreLesser(){
        String response=cmsHelper.dpAPICuisine(itemName).ResponseValidator.GetBodyAsText();
        //Double predScore=((BigDecimal)JsonPath.read(response,"$.result.prediction_score")).doubleValue();
        Double predScore=JsonPath.read(response,"$.result.prediction_score");
        if(predScore<=0.7){
            sqlTemplateCI = SystemConfigProvider.getTemplate("cms");
            List<Map<String, Object>> feedback = sqlTemplateCI.queryForList("select item_id,confidence from `cuisine_feedback_table` where `item_id`="+itmId);
            Assert.assertTrue(!feedback.isEmpty(), "Item id not present cuisine feedback table");

        }

        else{
            Assert.fail("Prediction score/Confidence for item is less than 0.7");
        }

    }
}

