package com.swiggy.api.erp.crm.dp.pos;

import org.testng.annotations.DataProvider;

import java.util.HashMap;

public class CreateDataPos {

    @DataProvider(name = "issue data")
    public static Object[][] issueData() {
        HashMap<Integer, String[]> issuedata = new HashMap<Integer, String[]>();

        issuedata.put(0, new String[] { "472", "faq", "Can I edit my order?" });// issueId, issueType, title
        issuedata.put(1, new String[] { "3", "general queries", "General Queries issue" });
        issuedata.put(2, new String[] { "4", "legal", "Legal issues" });
        issuedata.put(3, new String[] { "5", "order", "Order related issues" });

        return new Object[][] { { issuedata } };
    }

    @DataProvider(name = "action data")
    public static Object[][] actionData() {
        HashMap<String, String[]> issuedata = new HashMap<String, String[]>();
        HashMap<String, String[]> action = new HashMap<String, String[]>();

        issuedata.put("conversation00", new String[] { "472", "faq", "Can I edit my order?" });// issueId, issueType, title
        issuedata.put("conversation01", new String[] { "3", "general queries", "General Queries issue" });
        issuedata.put("conversation02", new String[] { "4", "legal", "Legal issues" });
        issuedata.put("conversation03", new String[] { "5", "order", "Order related issues" });
        action.put("action", new String[] {"reopen"});//,"feedback","assignAgent",});

        return new Object[][] { { issuedata, action } };
    }
}
