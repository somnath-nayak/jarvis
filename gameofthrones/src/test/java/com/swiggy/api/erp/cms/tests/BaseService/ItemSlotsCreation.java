package com.swiggy.api.erp.cms.tests.BaseService;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.dp.SelfServeDP;
import com.swiggy.api.erp.cms.helper.BaseServiceHelper;
import com.swiggy.api.erp.cms.helper.SelfServeHelper;
import framework.gameofthrones.Cersei.ToolBox;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class ItemSlotsCreation extends SelfServeDP {
SelfServeHelper selfServeHelper = new SelfServeHelper();
BaseServiceHelper baseServiceHelper = new BaseServiceHelper();
SchemaValidatorUtils schemaValidatorUtils = new SchemaValidatorUtils();

    @Test(dataProvider = "restaurantId", description = "Adding itemSlot to a restaurant")
    public void itemSlotCreation(String restId)throws Exception{
    String restaurantMenuResponse = selfServeHelper.processMenuService(restId).ResponseValidator.GetBodyAsText();
        System.out.println(restaurantMenuResponse);
    Assert.assertNotNull(restaurantMenuResponse);
    int itemId = selfServeHelper.getItemIdMenu(restaurantMenuResponse);

    String jsonSchema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/Schemaset/Json/CMS/createanitemslot.txt");
    String addingItemSlotResponse = baseServiceHelper.createAnItemSlot(itemId).ResponseValidator.GetBodyAsText();

        System.out.println(addingItemSlotResponse);
        List<String> missingAddedNodeList = schemaValidatorUtils.validateServiceSchema(jsonSchema,addingItemSlotResponse);
        Assert.assertTrue(missingAddedNodeList.isEmpty(),missingAddedNodeList + "Nodes are missing or data type of the object is mismatching");

    }
}
