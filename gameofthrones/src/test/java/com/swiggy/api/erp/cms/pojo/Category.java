package com.swiggy.api.erp.cms.pojo;

import com.swiggy.api.erp.cms.constants.CmsConstants;
import com.swiggy.api.sf.checkout.helper.CommonAPIHelper;

/**
 * Created by kiran.j on 1/23/18.
 */
public class Category {

    private int order;
    private boolean active;
    private String name;
    private String description;
    private int parent_category_id;
    private String type;
    private String third_party_id;
    private int restaurant_id;

    public Category() {
    }

    public Category(int order, boolean active, String name, String description, int parent_category_id, String type, String third_party_id, int restaurant_id) {
        this.order = order;
        this.active = active;
        this.name = name;
        this.description = description;
        this.parent_category_id = parent_category_id;
        this.type = type;
        this.third_party_id = third_party_id;
        this.restaurant_id = restaurant_id;
    }

    public int getOrder() {
        return order;
    }

    public Category setOrder(int order) {
        this.order = order;
        return this;
    }

    public boolean isActive() {
        return active;
    }

    public Category setActive(boolean active) {
        this.active = active;
        return this;
    }

    public String getName() {
        return name;
    }

    public Category setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Category setDescription(String description) {
        this.description = description;
        return this;
    }

    public int getParent_category_id() {
        return parent_category_id;
    }

    public Category setParent_category_id(int parent_category_id) {
        this.parent_category_id = parent_category_id;
        return this;
    }

    public String getType() {
        return type;
    }

    public Category setType(String type) {
        this.type = type;
        return this;
    }

    public String getThird_party_id() {
        return third_party_id;
    }

    public Category setThird_party_id(String third_party_id) {
        this.third_party_id = third_party_id;
        return this;
    }

    public int getRestaurant_id() {
        return restaurant_id;
    }

    public Category setRestaurant_id(int restaurant_id) {
        this.restaurant_id = restaurant_id;
        return this;
    }

    public Category build() {
        setDefaultValues();
        return this;
    }

    public void setDefaultValues() {
        CommonAPIHelper commonAPIHelper = new CommonAPIHelper();
        String random = commonAPIHelper.getTimeStamp();
        if(this.isActive() == false)
            this.setActive(true);
        if(this.getThird_party_id() == null)
            this.setThird_party_id("");
        if(this.getName() == null && this.getType() == CmsConstants.cat)
            this.setName(CmsConstants.testcategory_name+random);
        if(this.getName() == null && this.getType() == CmsConstants.subcat)
            this.setName(CmsConstants.testsubcategory_name+random);
        if(this.getDescription() == null)
            this.setDescription("");
    }

//    @Override
//    public String toString() {
//        return "{" +
//                "order=" + order +
//                ", active=" + active +
//                ", name='" + name + '\'' +
//                ", description='" + description + '\'' +
//                ", parent_category_id='" + parent_category_id + '\'' +
//                ", type='" + type + '\'' +
//                ", third_party_id='" + third_party_id + '\'' +
//                ", restaurant_id=" + restaurant_id +
//                '}';
//    }
}
