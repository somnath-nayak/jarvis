package com.swiggy.api.erp.cms.dp;


import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.swiggy.api.erp.cms.constants.CmsConstants;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Tyrion.SqlTemplate;
import org.testng.annotations.DataProvider;


public class Critical_API_dataProvider {


	@DataProvider(name = "menuHolidaySlotCreate")
	public Object[][] createMenuHolidaySlot() {
		return new Object[][]{{3150, "2019-02-05T14:25:43.511", "2019-02-05T15:25:43.511"}};
		//					 {menu-id,	 	"from_time",			 "to_time"}
	}

	@DataProvider(name = "createMenuHolidaySlot_InvalidMenuId")
	public Object[][] createMenuHolidaySlot_InvalidMenuId() {
		return new Object[][]{{3149, "2019-02-05T14:25:43.511", "2019-02-05T15:25:43.511"}};
		//					 {menu-id,	 	"from_time",			 "to_time"}
	}

	@DataProvider(name = "createMenuHolidaySlot_MenuIdWithoutCat")
	public Object[][] createMenuHolidaySlot_MenuIdWithoutCat() {
		return new Object[][]{{6725, "2019-02-05T14:25:43.511", "2019-02-05T15:25:43.511"}};
		//					 {menu-id,	 	"from_time",			 "to_time"}
	}

	@DataProvider(name = "menuHolidaySlotUpdate")
	public Object[][] updateMenuHolidaySlot() {
		return new Object[][]{{3150, "2019-02-05T15:25:43.511", "2019-02-05T16:25:43.511"}};
	}

	@DataProvider(name = "menuHolidaySlotCreate_greaterOpenTime")
	public Object[][] createMenuHolidaySlot_greaterOpenTime() {
		return new Object[][]{{3150, "2019-03-08T15:25:43.511", "2019-03-05T16:25:43.511"}};
	}

	@DataProvider(name = "menuHolidaySlotCreate_sameOpenCloseTime")
	public Object[][] createMenuHolidaySlot_sameOpenCloseTime() {
		return new Object[][]{{3150, "2019-03-05T15:25:43.511", "2019-03-05T15:25:43.511"}};
	}

	@DataProvider(name = "menuHolidaySlotCreate_pastTime")
	public Object[][] createMenuHolidaySlot_pastTime() {
		return new Object[][]{{3150, "2018-01-01T15:25:43.511", "2018-01-01T16:25:43.511"}};
	}


	@DataProvider(name = "thirdpartyreversemap_getMultipleItemIds")
	public Object[][] thirdpartyreversemap_getmultipleItemIds() {
		String[] str = {"99", "98", "97"};
		return new Object[][]{{Arrays.toString(str), 4551}};
//								[external-ids], restaurant-id
	}

	@DataProvider(name = "thirdpartyreversemap_getSingleItemIds")
	public Object[][] thirdpartyreversemap_getSingleItemIds() {
		String[] str = {"99"};
		return new Object[][]{{Arrays.toString(str), 4551}};
	}

	@DataProvider(name = "thirdpartyreversemap_invalidExternalIds")
	public Object[][] thirdpartyreversemap_invalidExternalIds() {
		String[] str = {"12"};
		return new Object[][]{{Arrays.toString(str), 4551}};
	}

	@DataProvider(name = "thirdpartyreversemap_emptyExternalIds")
	public Object[][] thirdpartyreversemap_emptyExternalIds() {
		String[] str = {"12"};
		return new Object[][]{{Arrays.toString(str), 4551}};
	}

	@DataProvider(name = "thirdpartyreversemap_duplicateExternalIds")
	public Object[][] thirdpartyreversemap_duplicateExternalIds() {
		String[] str = {"99", "98", "98", "99", "97"};
		return new Object[][]{{Arrays.toString(str), 4551}};
	}

	@DataProvider(name = "thirdpartyreversemap_multipleExternalIdswithoutArray")
	public Object[][] thirdpartyreversemap_multipleExternalIdswithoutArray() {
		String str = "99,98,97";
		return new Object[][]{{str, 4551}};
	}

	@DataProvider(name = "thirdpartyreversemap_singleExternalIdswithoutArray")
	public Object[][] thirdpartyreversemap_singleExternalIdswithoutArray() {
		String str = "99";
		return new Object[][]{{str, 4551}};
	}

	@DataProvider(name = "thirdpartyreversemap_externalIdsWithoutRestaurantId")
	public Object[][] thirdpartyreversemap_externalIdsWithoutRestaurantId() {
		String[] str = {"99", "98", "97"};
		int[] rest_id = {4551, 4553};
		return new Object[][]{{Arrays.toString(str), Arrays.toString(rest_id)}};
	}

	//Hema Starts
	@DataProvider(name = "getRestaurantMenu")
	public Object[][] getRestaurantMenu() {
		return new Object[][]{{"9990"}};
	}


	@DataProvider(name = "iteminstock")
	public Object[][] test() {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
		List<Map<String, Object>> data=sqlTemplate.queryForList("select variant_group_id,count(variant_group_id),id from variants where active=1 group by variant_group_id having count(variant_group_id)=1 ORDER BY RAND()");
		List<Map<String, Object>> datainactive=sqlTemplate.queryForList("select variant_group_id,count(variant_group_id),id from variants where active=0 group by variant_group_id having count(variant_group_id)=1 ORDER BY RAND()");
		String vgid=data.get(0).get("variant_group_id").toString();
		String vid=data.get(0).get("id").toString();
		String ivgid=datainactive.get(0).get("variant_group_id").toString();
		String ivid=datainactive.get(0).get("id").toString();
		return new Object[][]{{"4376", "1983", "Variant Marked InStock Successfully"},
				{"4377", "1983", "Variant Marked InStock Successfully"},
				{"4386", "1988", "Variant Marked InStock Successfully"},
				{"", "", "The given id must not be null!; nested exception is java.lang.IllegalArgumentException: The given id must not be null!"},
				{"123456789", "123456789", "Invalid Variant Id"},//Invalid vid,gvid
				{"4386", "123456789", "Variant does not belong to given Variant Group Id"},//Valid Vid,Invalid Gvid
				{"1234567891", "1988", "Invalid Variant Id"},//Invalid Vid,Valid Gvid
				{vid, vgid, "Variant Marked InStock Successfully. Congrats Item is also Marked Instock"},
				{ivid, ivgid, "Invalid Variant Id"}//Inactive variant id



		};
	}

	@DataProvider(name = "itemoutofstock")
	public Object[][] test1() {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
		List<Map<String, Object>> data=sqlTemplate.queryForList("select variant_group_id,count(variant_group_id),id from variants where active=1 group by variant_group_id having count(variant_group_id)=1 ORDER BY RAND()");
		List<Map<String, Object>> datainactive=sqlTemplate.queryForList("select variant_group_id,count(variant_group_id),id from variants where active=0 group by variant_group_id having count(variant_group_id)=1 ORDER BY RAND()");
		String vgid= data.get(1).get("variant_group_id").toString();
		String vid=data.get(1).get("id").toString();
		String ivgid=datainactive.get(0).get("variant_group_id").toString();
		String ivid=datainactive.get(0).get("id").toString();
		return new Object[][]{{"4376", "1983", "Variant Marked Out Of Stock Successfully"},
				{"4377", "1983", "Variant Marked Out Of Stock Successfully"},
				{"4386", "1988", "Variant Marked Out Of Stock Successfully"},
				{"", "", "The given id must not be null!; nested exception is java.lang.IllegalArgumentException: The given id must not be null!"},
				{"123456789", "123456789", "Invalid Variant Id"},
				{"4386", "123456789", "Variant does not belong to given Variant Group Id"},//Valid Vid,Invalid Gvid
				{"1234567891", "1988", "Invalid Variant Id"},//Invalid Vid,Valid Gvid
				{vid, vgid, "Variant Marked Out Of Stock Successfully. Item is also Marked Out Of Stock."},
				{ivid, ivgid, "Invalid Variant Id"}//Inactive variant id

		};


	}
}