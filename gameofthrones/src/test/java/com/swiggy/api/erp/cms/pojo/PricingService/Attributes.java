
package com.swiggy.api.erp.cms.pojo.PricingService;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.swiggy.api.erp.cms.constants.CatalogV2Constants;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "sku_cgst",
    "sku_igst",
    "sku_inclusive_of_taxes",
    "sku_packaging_charges",
    "sku_sgst_utgst"
})
public class Attributes {

    @JsonProperty("sku_cgst")
    private String sku_cgst;
    @JsonProperty("sku_igst")
    private String sku_igst;
    @JsonProperty("sku_inclusive_of_taxes")
    private Boolean sku_inclusive_of_taxes;
    @JsonProperty("sku_packaging_charges")
    private String sku_packaging_charges;
    @JsonProperty("sku_sgst_utgst")
    private String sku_sgst_utgst;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();


    @JsonProperty("sku_cgst")
    public String getSku_cgst() {
        return sku_cgst;
    }

    @JsonProperty("sku_cgst")
    public void setSku_cgst(String sku_cgst) {
        this.sku_cgst = sku_cgst;
    }

    public Attributes withSku_cgst(String sku_cgst) {
        this.sku_cgst = sku_cgst;
        return this;
    }

    @JsonProperty("sku_igst")
    public String getSku_igst() {
        return sku_igst;
    }

    @JsonProperty("sku_igst")
    public void setSku_igst(String sku_igst) {
        this.sku_igst = sku_igst;
    }

    public Attributes withSku_igst(String sku_igst) {
        this.sku_igst = sku_igst;
        return this;
    }

    @JsonProperty("sku_inclusive_of_taxes")
    public Boolean getSku_inclusive_of_taxes() {
        return sku_inclusive_of_taxes;
    }

    @JsonProperty("sku_inclusive_of_taxes")
    public void setSku_inclusive_of_taxes(Boolean sku_inclusive_of_taxes) {
        this.sku_inclusive_of_taxes = sku_inclusive_of_taxes;
    }

    public Attributes withSku_inclusive_of_taxes(Boolean sku_inclusive_of_taxes) {
        this.sku_inclusive_of_taxes = sku_inclusive_of_taxes;
        return this;
    }

    @JsonProperty("sku_packaging_charges")
    public String getSku_packaging_charges() {
        return sku_packaging_charges;
    }

    @JsonProperty("sku_packaging_charges")
    public void setSku_packaging_charges(String sku_packaging_charges) {
        this.sku_packaging_charges = sku_packaging_charges;
    }

    public Attributes withSku_packaging_charges(String sku_packaging_charges) {
        this.sku_packaging_charges = sku_packaging_charges;
        return this;
    }

    @JsonProperty("sku_sgst_utgst")
    public String getSku_sgst_utgst() {
        return sku_sgst_utgst;
    }

    @JsonProperty("sku_sgst_utgst")
    public void setSku_sgst_utgst(String sku_sgst_utgst) {
        this.sku_sgst_utgst = sku_sgst_utgst;
    }

    public Attributes withSku_sgst_utgst(String sku_sgst_utgst) {
        this.sku_sgst_utgst = sku_sgst_utgst;
        return this;
    }
    public Attributes setDefaultValue(){
        this.withSku_cgst(CatalogV2Constants.sku_cgst).withSku_igst(CatalogV2Constants.sku_igst).
                withSku_inclusive_of_taxes(true).withSku_packaging_charges(CatalogV2Constants.sku_packaging_charges).
                withSku_sgst_utgst(CatalogV2Constants.sku_sgst_utgst);
        return this;

    }


}
