package com.swiggy.api.erp.cms.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.constants.Constants;
import com.swiggy.api.erp.cms.dp.TaggingDP;
import com.swiggy.api.erp.cms.helper.TaggingHelper;
import com.swiggy.api.erp.cms.pojo.EntityTag.EntityTagMapBulk;
import com.swiggy.api.erp.cms.pojo.EntityTag.EntityTagMapBulkUploadBuilder;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class TaggingTest extends TaggingDP {
    Initialize gameofthrones = new Initialize();
    TaggingHelper taggingHelper= new TaggingHelper();
    Random random= new Random();

    @Test(dataProvider = "createTagCategory")
    public void createTagCategory(String enabled, String name, String update){
        String response= taggingHelper.createTagCategory(enabled,name, update).ResponseValidator.GetBodyAsText();
        String statusCode= JsonPath.read(response,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.status, statusCode,"statusCode is not 1");
        String id= JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertNotNull(id,"id is null");
        String updateBy= JsonPath.read(response,"$.data.updated_by").toString().replace("[","").replace("]","");
        Assert.assertEquals(update, updateBy,"statusCode is not 1");
        String getResponse= taggingHelper.getTagById(id).ResponseValidator.GetBodyAsText();
        String statusCode1= JsonPath.read(getResponse,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.status, statusCode1,"statusCode is not 1");
        String tagIds= JsonPath.read(getResponse,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertEquals(tagIds,id,"tags id is null");
    }

    @Test(dataProvider = "createTagCategory", description = "Not able to create duplicate tag category")
    public void duplicateTagCategory(String enabled, String name, String update){
        String response= taggingHelper.createTagCategory(enabled,name, update).ResponseValidator.GetBodyAsText();
        String statusCode= JsonPath.read(response,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.status, statusCode,"statusCode is not 1");
        String id= JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertNotNull(id,"id is null");
        String updateBy= JsonPath.read(response,"$.data.updated_by").toString().replace("[","").replace("]","");
        Assert.assertEquals(update, updateBy,"statusCode is not 1");
        String getResponse= taggingHelper.getTagById(id).ResponseValidator.GetBodyAsText();
        String statusCode1= JsonPath.read(getResponse,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.status, statusCode1,"statusCode is not 1");
        String tagIds= JsonPath.read(getResponse,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertEquals(tagIds,id,"tags id is null");

        String response2= taggingHelper.createTagCategory(enabled,name, update).ResponseValidator.GetBodyAsText();
        String statusCode2= JsonPath.read(response2,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.status0, statusCode2,"statusCode is not 1");
        String errorCode= JsonPath.read(response2,"$.error.errorCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(errorCode,Constants.badRequest,"id is null");
        String message= JsonPath.read(response2,"$.error..apiFieldErrors..message").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(Constants.errorMessage, message,"updated by different users");
    }

    @Test(dataProvider = "createTagCategoryNegative", description = "checking for null, zero, or negative values")
    public void createTagCategoryNegative(String enabled, String name, String update){
        String response= taggingHelper.createTagCategory(enabled,name, update).ResponseValidator.GetBodyAsText();
        String statusCode2= JsonPath.read(response,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.status0, statusCode2,"statusCode is not 1");
        String errorCode= JsonPath.read(response,"$.error.errorCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(errorCode,Constants.badRequest,"id is null");
        String message= JsonPath.read(response,"$.error..apiFieldErrors..message").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertNotEquals(Constants.errorMessage, message,"updated by different users");
    }

    @Test(dataProvider = "createTagCategory")
    public void getAllTagCategory(String enabled, String name, String update){
        String response= taggingHelper.createTagCategory(enabled,name, update).ResponseValidator.GetBodyAsText();
        String statusCode= JsonPath.read(response,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.status, statusCode,"statusCode is not 1");
        String id= JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertNotNull(id,"id is null");
        String updateBy= JsonPath.read(response,"$.data.updated_by").toString().replace("[","").replace("]","");
        Assert.assertEquals(update, updateBy,"statusCode is not 1");
        String getAllResponse= taggingHelper.getAllTagCategory().ResponseValidator.GetBodyAsText();
        String statusCode1= JsonPath.read(getAllResponse,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.status, statusCode1,"statusCode is not 1");
        String[] tagIds= JsonPath.read(getAllResponse,"$.data..id").toString().replace("[","").replace("]","").
                split(",");
        Assert.assertNotNull(tagIds,"tags array is null");
    }

    @Test(dataProvider = "updateTagCategory")
    public void updateTagCategory(String enabled, String name, String update, String updateName){
        String response= taggingHelper.createTagCategory(enabled,name, update).ResponseValidator.GetBodyAsText();
        statusCode(response);
        String id= JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertNotNull(id,"id is null");
        String updateBy= JsonPath.read(response,"$.data.updated_by").toString().replace("[","").replace("]","");
        Assert.assertEquals(update, updateBy,"statusCode is not 1");
        String getResponse= taggingHelper.getTagById(id).ResponseValidator.GetBodyAsText();
        statusCode(getResponse);
        String tagIds= JsonPath.read(getResponse,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertEquals(tagIds,id,"tags id is null");

        String updateTagCategory= taggingHelper.updateTagCategory(enabled,id,updateName,update).ResponseValidator.GetBodyAsText();
        statusCode(updateTagCategory);
        String updateId= JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertEquals(updateId,id,"id is null");
        String updatedBy= JsonPath.read(response,"$.data.updated_by").toString().replace("[","").replace("]","");
        Assert.assertEquals(update, updatedBy,"statusCode is not 1");
        String getResponseAfterUpdate= taggingHelper.getTagById(id).ResponseValidator.GetBodyAsText();
        statusCode(getResponseAfterUpdate);
        String tagId= JsonPath.read(getResponse,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertEquals(tagId,id,"tags id is null");
    }


    @Test(dataProvider = "updateTagCategory")
    public void deleteUpdateTagCategory(String enabled, String name, String update, String updateName){
        String response= taggingHelper.createTagCategory(enabled,name, update).ResponseValidator.GetBodyAsText();
        statusCode(response);
        String id= JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertNotNull(id,"id is null");
        String updateBy= JsonPath.read(response,"$.data.updated_by").toString().replace("[","").replace("]","");
        Assert.assertEquals(update, updateBy,"statusCode is not 1");
        String getResponse= taggingHelper.getTagById(id).ResponseValidator.GetBodyAsText();
        statusCode(getResponse);
        String tagIds= JsonPath.read(getResponse,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertEquals(tagIds,id,"tags id is null");

        String updateTagCategory= taggingHelper.updateTagCategory(enabled,id,updateName,update).ResponseValidator.GetBodyAsText();
        statusCode(updateTagCategory);
        String updateId= JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertEquals(updateId,id,"id is null");
        String updatedBy= JsonPath.read(response,"$.data.updated_by").toString().replace("[","").replace("]","");
        Assert.assertEquals(update, updatedBy,"statusCode is not 1");
        String getResponseAfterUpdate= taggingHelper.getTagById(id).ResponseValidator.GetBodyAsText();
        statusCode(getResponseAfterUpdate);
        String tagId= JsonPath.read(getResponse,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertEquals(tagId,id,"tags id is null");

        String deleteTAgaCategory= taggingHelper.deleteTagCategory(id).ResponseValidator.GetBodyAsText();
        String deleteStatusCode= JsonPath.read(deleteTAgaCategory,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.status, deleteStatusCode,"statusCode is not 1");
    }


    @Test(dataProvider = "createTagCategory")
    public void deleteTagCategory(String enabled, String name, String update){
        String response= taggingHelper.createTagCategory(enabled,name, update).ResponseValidator.GetBodyAsText();
        String statusCode= JsonPath.read(response,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.status, statusCode,"statusCode is not 1");
        String id= JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertNotNull(id,"id is null");
        String updateBy= JsonPath.read(response,"$.data.updated_by").toString().replace("[","").replace("]","");
        Assert.assertEquals(update, updateBy,"statusCode is not 1");
        String getResponse= taggingHelper.getTagById(id).ResponseValidator.GetBodyAsText();
        String statusCode1= JsonPath.read(getResponse,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.status, statusCode1,"statusCode is not 1");
        String tagIds= JsonPath.read(getResponse,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertEquals(tagIds,id,"tags id is null");

        String deleteTAgaCategory= taggingHelper.deleteTagCategory(id).ResponseValidator.GetBodyAsText();
        String deleteStatusCode= JsonPath.read(deleteTAgaCategory,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.status, deleteStatusCode,"statusCode is not 1");
    }

    @Test(dataProvider = "deleteTagCategoryNegative", description = "delete tag category which is not present")
    public void deleteTagCategoryNotPresent(String tagId){
        String deleteTag= taggingHelper.deleteTagCategory(tagId).ResponseValidator.GetBodyAsText();
        String statusCode= JsonPath.read(deleteTag,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.status0, statusCode,"statusCode is not 0");
        String errorCode= JsonPath.read(deleteTag,"$.error.errorCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.badRequest, errorCode,"errorCode is not 400");
        String errorMesssage= JsonPath.read(deleteTag,"$.error.errorMessage").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.error, errorMesssage,"No error Message");
    }


    @Test(dataProvider = "deleteTagCategoryNegative", description = "get tag category which is not present")
    public void getTagCategoryNotPresent(String tagId){
        String deleteTag= taggingHelper.getTagById(tagId).ResponseValidator.GetBodyAsText();
        String statusCode= JsonPath.read(deleteTag,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.status0, statusCode,"statusCode is not 0");
        String errorCode= JsonPath.read(deleteTag,"$.error.errorCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.badRequest, errorCode,"errorCode is not 400");
        String errorMesssage= JsonPath.read(deleteTag,"$.error.errorMessage").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.error, errorMesssage,"No error Message");
    }

    @Test(dataProvider = "updateTagCategory")
    public void updateAndDeleteTagCategory(String enabled, String name, String update, String updateName){
        String response= taggingHelper.createTagCategory(enabled,name, update).ResponseValidator.GetBodyAsText();
        statusCode(response);
        String id= JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertNotNull(id,"id is null");
        String updateBy= JsonPath.read(response,"$.data.updated_by").toString().replace("[","").replace("]","");
        Assert.assertEquals(update, updateBy,"statusCode is not 1");
        String getResponse= taggingHelper.getTagById(id).ResponseValidator.GetBodyAsText();
        statusCode(getResponse);
        String tagIds= JsonPath.read(getResponse,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertEquals(tagIds,id,"tags id is null");

        String updateTagCategory= taggingHelper.updateTagCategory(enabled,id,updateName,update).ResponseValidator.GetBodyAsText();
        statusCode(updateTagCategory);
        String updateId= JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertEquals(updateId,id,"id is null");
        String updatedBy= JsonPath.read(response,"$.data.updated_by").toString().replace("[","").replace("]","");
        Assert.assertEquals(update, updatedBy,"statusCode is not 1");
        String getResponseAfterUpdate= taggingHelper.getTagById(id).ResponseValidator.GetBodyAsText();
        statusCode(getResponseAfterUpdate);
        String tagId= JsonPath.read(getResponse,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertEquals(tagId,id,"tags id is null");

        String deleteTAgCategory= taggingHelper.deleteTagCategory(updateId).ResponseValidator.GetBodyAsText();
        statusCode(deleteTAgCategory);
    }

    @Test(dataProvider = "createTag", description = "tag category and tag both are true")
    public void createTag(String enabled, String categoryName, String categoryUpdate, String tagName){
        String response= taggingHelper.createTagCategory(enabled,categoryName, categoryUpdate).ResponseValidator.GetBodyAsText();
        statusCode(response);
        String id= JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertNotNull(id,"id is null");
        String updateBy= JsonPath.read(response,"$.data.updated_by").toString().replace("[","").replace("]","");
        Assert.assertEquals(categoryUpdate, updateBy,"statusCode is not 1");
        String getResponse= taggingHelper.getTagById(id).ResponseValidator.GetBodyAsText();
        statusCode(getResponse);
        String tagIds= JsonPath.read(getResponse,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertEquals(tagIds,id,"tags id is null");

        String tagResponse= taggingHelper.createTag(enabled,tagName,id,categoryUpdate).ResponseValidator.GetBodyAsText();
        statusCode(tagResponse);
        String parentTagCategory= JsonPath.read(tagResponse,"$.data.parent").toString().replace("[","").replace("]","");
        Assert.assertEquals(id, parentTagCategory,"parent field is not same");
    }

    @Test(dataProvider = "multipleTag", description = "adding multiple tags under one tag category")
    public void multipleTagUnderOneCategory(String enabled, String categoryName, String categoryUpdate, String tagName, String tagName2){
        String response= taggingHelper.createTagCategory(enabled,categoryName, categoryUpdate).ResponseValidator.GetBodyAsText();
        statusCode(response);
        String id= JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertNotNull(id,"id is null");
        String updateBy= JsonPath.read(response,"$.data.updated_by").toString().replace("[","").replace("]","");
        Assert.assertEquals(categoryUpdate, updateBy,"statusCode is not 1");
        String getResponse= taggingHelper.getTagById(id).ResponseValidator.GetBodyAsText();
        statusCode(getResponse);
        String tagIds= JsonPath.read(getResponse,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertEquals(tagIds,id,"tags id is null");

        String tagResponse= taggingHelper.createTag(enabled,tagName,id,categoryUpdate).ResponseValidator.GetBodyAsText();
        statusCode(tagResponse);
        String parentTagCategory= JsonPath.read(tagResponse,"$.data.parent").toString().replace("[","").replace("]","");
        Assert.assertEquals(id, parentTagCategory,"parent field is not same");

        String tagResponse1= taggingHelper.createTag(enabled,tagName2,id,categoryUpdate).ResponseValidator.GetBodyAsText();
        statusCode(tagResponse1);
        String parentTagCategory1= JsonPath.read(tagResponse,"$.data.parent").toString().replace("[","").replace("]","");
        Assert.assertEquals(id, parentTagCategory1,"parent field is not same");
    }

    @Test(dataProvider = "createTagFalse", description = "tag category and tag both are false")
    public void createTagEnabledFalse(String enabled, String categoryName, String categoryUpdate, String tagName){
        String response= taggingHelper.createTagCategory(enabled,categoryName, categoryUpdate).ResponseValidator.GetBodyAsText();
        statusCode(response);
        String id= JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertNotNull(id,"id is null");
        String updateBy= JsonPath.read(response,"$.data.updated_by").toString().replace("[","").replace("]","");
        Assert.assertEquals(categoryUpdate, updateBy,"statusCode is not 1");
        String getResponse= taggingHelper.getTagById(id).ResponseValidator.GetBodyAsText();
        statusCode(getResponse);
        String tagIds= JsonPath.read(getResponse,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertEquals(tagIds,id,"tags id is null");

        String tagResponse= taggingHelper.createTag(enabled,tagName,id,categoryUpdate).ResponseValidator.GetBodyAsText();
        String tagStatusCode= JsonPath.read(tagResponse,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.status0, tagStatusCode,"statusCode is not 0");
        String errorCode= JsonPath.read(tagResponse,"$.error.errorCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.badRequest, errorCode,"It is not bad request");
    }

    @Test(dataProvider = "createTagCategoryTrue", description = "tag category is true but tag is false")
    public void TagCategoryEnabledTrueTagFalse(String enabled, String categoryName, String categoryUpdate,String tagEnabled ,String tagName){
        String response= taggingHelper.createTagCategory(enabled,categoryName, categoryUpdate).ResponseValidator.GetBodyAsText();
        statusCode(response);
        String id= JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");

        Assert.assertNotNull(id,"id is null");
        String updateBy= JsonPath.read(response,"$.data.updated_by").toString().replace("[","").replace("]","");
        Assert.assertEquals(categoryUpdate, updateBy,"statusCode is not 1");
        String getResponse= taggingHelper.getTagById(id).ResponseValidator.GetBodyAsText();
        statusCode(getResponse);
        String tagIds= JsonPath.read(getResponse,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertEquals(tagIds,id,"tags id is null");

        String tagResponse= taggingHelper.createTag(tagEnabled,tagName,id,categoryUpdate).ResponseValidator.GetBodyAsText();
        statusCode(tagResponse);
        String parentTagCategory= JsonPath.read(tagResponse,"$.data.parent").toString().replace("[","").replace("]","");
        Assert.assertEquals(id, parentTagCategory,"parent field is not same");
    }

    @Test(dataProvider = "createTagCategoryFalse", description = "tag category is false but tag is true")
    public void TagCategoryEnabledFalseTagTrue(String enabled, String categoryName, String categoryUpdate,String tagEnabled ,String tagName){
        String response= taggingHelper.createTagCategory(enabled,categoryName, categoryUpdate).ResponseValidator.GetBodyAsText();
        statusCode(response);
        String id= JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertNotNull(id,"id is null");
        String updateBy= JsonPath.read(response,"$.data.updated_by").toString().replace("[","").replace("]","");
        Assert.assertEquals(categoryUpdate, updateBy,"statusCode is not 1");
        String getResponse= taggingHelper.getTagById(id).ResponseValidator.GetBodyAsText();
        statusCode(getResponse);
        String tagIds= JsonPath.read(getResponse,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertEquals(tagIds,id,"tags id is null");

        String tagResponse= taggingHelper.createTag(tagEnabled,tagName,id,categoryUpdate).ResponseValidator.GetBodyAsText();
        String tagStatusCode= JsonPath.read(tagResponse,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.status0, tagStatusCode,"statusCode is not 0");
        String errorCode= JsonPath.read(tagResponse,"$.error.errorCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.badRequest, errorCode,"It is not bad request");
    }

    @Test(dataProvider = "createTagNoTagCategory", description = "Create Tag for a particular tag category which is not present in db")
    public void createTagNoTagCategory(String enabled, String id, String categoryUpdate,String tagName){

        String tagResponse= taggingHelper.createTag(enabled,tagName,id,categoryUpdate).ResponseValidator.GetBodyAsText();
        String tagStatusCode= JsonPath.read(tagResponse,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.status0, tagStatusCode,"statusCode is not 0");
        String errorCode= JsonPath.read(tagResponse,"$.error.errorCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.badRequest, errorCode,"It is not bad request");
    }

    @Test(dataProvider = "createTag", description = "duplicate tag under same tag category")
    public void duplicateTagInSameTagCategory(String enabled, String categoryName, String categoryUpdate ,String tagName){
        String response= taggingHelper.createTagCategory(enabled,categoryName, categoryUpdate).ResponseValidator.GetBodyAsText();
        statusCode(response);
        String id= JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertNotNull(id,"id is null");
        String updateBy= JsonPath.read(response,"$.data.updated_by").toString().replace("[","").replace("]","");
        Assert.assertEquals(categoryUpdate, updateBy,"statusCode is not 1");
        String getResponse= taggingHelper.getTagById(id).ResponseValidator.GetBodyAsText();
        statusCode(getResponse);
        String tagIds= JsonPath.read(getResponse,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertEquals(tagIds,id,"tags id is null");

        String tagResponse= taggingHelper.createTag(enabled,tagName,id,categoryUpdate).ResponseValidator.GetBodyAsText();
        statusCode(tagResponse);

        String tagResponse1= taggingHelper.createTag(enabled,tagName,id,categoryUpdate).ResponseValidator.GetBodyAsText();
        String tagStatusCode1= JsonPath.read(tagResponse1,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.status0, tagStatusCode1,"statusCode is not 0");
        String errorCode= JsonPath.read(tagResponse1,"$.error.errorCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.badRequest, errorCode,"It is not a bad request");
    }


    @Test(dataProvider = "createTagNegative", description = "name is null")
    public void CreateTagNegative(String enabled, String categoryName, String categoryUpdate ,String tagName){
        String response= taggingHelper.createTagCategory(enabled,categoryName, categoryUpdate).ResponseValidator.GetBodyAsText();
        statusCode(response);
        String id= JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertNotNull(id,"id is null");
        String updateBy= JsonPath.read(response,"$.data.updated_by").toString().replace("[","").replace("]","");
        Assert.assertEquals(categoryUpdate, updateBy,"statusCode is not 1");
        String getResponse= taggingHelper.getTagById(id).ResponseValidator.GetBodyAsText();
        statusCode(getResponse);
        String tagIds= JsonPath.read(getResponse,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertEquals(tagIds,id,"tags id is null");

        String tagResponse1= taggingHelper.createTag(enabled,tagName,id,categoryUpdate).ResponseValidator.GetBodyAsText();
        String tagStatusCode1= JsonPath.read(tagResponse1,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.status0, tagStatusCode1,"statusCode is not 0");
        String errorCode= JsonPath.read(tagResponse1,"$.error.errorCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.badRequest, errorCode,"It is not a bad request");
    }



    @Test(dataProvider = "createTag", description = "checking all the tags ")
    public void getAllTag(String enabled, String categoryName, String categoryUpdate,String tagName) {
        String response= taggingHelper.createTagCategory(enabled,categoryName, categoryUpdate).ResponseValidator.GetBodyAsText();
        statusCode(response);
        String id= JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertNotNull(id,"id is null");
        String updateBy= JsonPath.read(response,"$.data.updated_by").toString().replace("[","").replace("]","");
        Assert.assertEquals(categoryUpdate, updateBy,"statusCode is not 1");
        String getResponse= taggingHelper.getTagById(id).ResponseValidator.GetBodyAsText();
        statusCode(getResponse);
        String tagIds= JsonPath.read(getResponse,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertEquals(tagIds,id,"tags id is null");

        String tagResponse= taggingHelper.createTag(enabled,tagName,id,categoryUpdate).ResponseValidator.GetBodyAsText();
        statusCode(tagResponse);
        String parentTagCategory= JsonPath.read(tagResponse,"$.data.parent").toString().replace("[","").replace("]","");
        Assert.assertEquals(id, parentTagCategory,"parent field is not same");

        String getAllresponse = taggingHelper.getAllTag().ResponseValidator.GetBodyAsText();
        statusCode(getAllresponse);
    }

    @Test(dataProvider = "updateTag", description = "update tag of a tag category")
    public void updateTag(String enabled, String categoryName, String categoryUpdate,String tagEnabled ,String tagName, String updateTagName){
        String response= taggingHelper.createTagCategory(enabled,categoryName, categoryUpdate).ResponseValidator.GetBodyAsText();
        statusCode(response);
        String id= JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertNotNull(id,"id is null");
        String updateBy= JsonPath.read(response,"$.data.updated_by").toString().replace("[","").replace("]","");
        Assert.assertEquals(categoryUpdate, updateBy,"updated by different users");
        String getResponse= taggingHelper.getTagById(id).ResponseValidator.GetBodyAsText();
        statusCode(getResponse);
        String tagIds= JsonPath.read(getResponse,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertEquals(tagIds,id,"tags id is null");

        String tagResponse= taggingHelper.createTag(tagEnabled,tagName,id,categoryUpdate).ResponseValidator.GetBodyAsText();
        String tagStatusCode= JsonPath.read(tagResponse,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.status1, tagStatusCode,"statusCode is not 1");
        String createTagId= JsonPath.read(tagResponse,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertNotNull(createTagId,"id is null");

        String getTagResponse= taggingHelper.getTag(createTagId).ResponseValidator.GetBodyAsText();
        statusCode(getTagResponse);

        String updateTagResponse= taggingHelper.updateTag(tagEnabled,createTagId,updateTagName,id,categoryUpdate).ResponseValidator.GetBodyAsText();
        statusCode(updateTagResponse);
        String updatedTagName= JsonPath.read(updateTagResponse,"$.data.name").toString().replace("[","").replace("]","");
        Assert.assertEquals(updateTagName, updatedTagName,"updated by different users");
        String updateParentCategory= JsonPath.read(updateTagResponse,"$.data.parent").toString().replace("[","").replace("]","");
        Assert.assertEquals(id, updateParentCategory,"Parent Tag Category is not same");
    }

    @Test(dataProvider = "updateTag", description = "Change parent id during updation ofa tag under tag category")
    public void updateTagChangeParent(String enabled, String categoryName, String categoryUpdate,String tagEnabled ,String tagName, String updateTagName){
        String response= taggingHelper.createTagCategory(enabled,categoryName, categoryUpdate).ResponseValidator.GetBodyAsText();
        statusCode(response);
        String id= JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertNotNull(id,"id is null");
        String updateBy= JsonPath.read(response,"$.data.updated_by").toString().replace("[","").replace("]","");
        Assert.assertEquals(categoryUpdate, updateBy,"updated by different users");
        String getResponse= taggingHelper.getTagById(id).ResponseValidator.GetBodyAsText();
        statusCode(getResponse);
        String tagIds= JsonPath.read(getResponse,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertEquals(tagIds,id,"tags id is null");

        String tagResponse= taggingHelper.createTag(tagEnabled,tagName,id,categoryUpdate).ResponseValidator.GetBodyAsText();
        String tagStatusCode= JsonPath.read(tagResponse,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.status1, tagStatusCode,"statusCode is not 1");
        String createTagId= JsonPath.read(tagResponse,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertNotNull(createTagId,"id is null");

        String getTagResponse= taggingHelper.getTag(createTagId).ResponseValidator.GetBodyAsText();
        statusCode(getTagResponse);

        List<Map<String, Object>> parentId= taggingHelper.getParent();
        int number= random.nextInt(parentId.size());
        String parentTagId= taggingHelper.getParent().get(number).get("parent").toString();

        String updateTagResponse= taggingHelper.updateTag(tagEnabled,createTagId,updateTagName,parentTagId,categoryUpdate).ResponseValidator.GetBodyAsText();
        statusCode(updateTagResponse);
        String updatedTagName= JsonPath.read(updateTagResponse,"$.data.name").toString().replace("[","").replace("]","");
        Assert.assertEquals(updateTagName, updatedTagName,"updated by different users");
        String updateParentCategory= JsonPath.read(updateTagResponse,"$.data.parent").toString().replace("[","").replace("]","");
        Assert.assertNotEquals(id, updateParentCategory,"Parent Tag Category is same");
    }

    @Test(dataProvider = "updateTag", description = "delete updated tag of a tag category")
    public void deleteUpdateTag(String enabled, String categoryName, String categoryUpdate,String tagEnabled ,String tagName, String updateTagName){
        String response= taggingHelper.createTagCategory(enabled,categoryName, categoryUpdate).ResponseValidator.GetBodyAsText();
        statusCode(response);
        String id= JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertNotNull(id,"id is null");
        String updateBy= JsonPath.read(response,"$.data.updated_by").toString().replace("[","").replace("]","");
        Assert.assertEquals(categoryUpdate, updateBy,"updated by different users");
        String getResponse= taggingHelper.getTagById(id).ResponseValidator.GetBodyAsText();
        statusCode(getResponse);
        String tagIds= JsonPath.read(getResponse,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertEquals(tagIds,id,"tags id is null");

        String tagResponse= taggingHelper.createTag(tagEnabled,tagName,id,categoryUpdate).ResponseValidator.GetBodyAsText();
        String tagStatusCode= JsonPath.read(tagResponse,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.status1, tagStatusCode,"statusCode is not 1");
        String createTagId= JsonPath.read(tagResponse,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertNotNull(createTagId,"id is null");

        String getTagResponse= taggingHelper.getTag(createTagId).ResponseValidator.GetBodyAsText();
        statusCode(getTagResponse);

        String updateTagResponse= taggingHelper.updateTag(tagEnabled,createTagId,updateTagName,id,categoryUpdate).ResponseValidator.GetBodyAsText();
        statusCode(updateTagResponse);
        String updatedTagName= JsonPath.read(updateTagResponse,"$.data.name").toString().replace("[","").replace("]","");
        Assert.assertEquals(updateTagName, updatedTagName,"updated by different users");
        String updateParentCategory= JsonPath.read(updateTagResponse,"$.data.parent").toString().replace("[","").replace("]","");
        Assert.assertEquals(id, updateParentCategory,"Parent Tag Category is not same");

        String deleteTag= taggingHelper.deleteTag(createTagId).ResponseValidator.GetBodyAsText();
        statusCode(deleteTag);
    }

    @Test(dataProvider = "updateTag1", description = "update enable field of tag api from true to false")
    public void updateTagEnableFalse(String enabled, String categoryName, String categoryUpdate,String tagEnabled ,String tagName, String updateTagName){
        String response= taggingHelper.createTagCategory(enabled,categoryName, categoryUpdate).ResponseValidator.GetBodyAsText();
        statusCode(response);
        String id= JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertNotNull(id,"id is null");
        String updateBy= JsonPath.read(response,"$.data.updated_by").toString().replace("[","").replace("]","");
        Assert.assertEquals(categoryUpdate, updateBy,"updated by different users");
        String getResponse= taggingHelper.getTagById(id).ResponseValidator.GetBodyAsText();
        statusCode(getResponse);
        String tagIds= JsonPath.read(getResponse,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertEquals(tagIds,id,"tags id is null");

        String tagResponse= taggingHelper.createTag(enabled,tagName,id,categoryUpdate).ResponseValidator.GetBodyAsText();
        String tagStatusCode= JsonPath.read(tagResponse,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.status1, tagStatusCode,"statusCode is not 1");
        String createTagId= JsonPath.read(tagResponse,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertNotNull(createTagId,"id is null");

        String getTagResponse= taggingHelper.getTag(createTagId).ResponseValidator.GetBodyAsText();
        statusCode(getTagResponse);

        String updateTagResponse= taggingHelper.updateTag(tagEnabled,createTagId,updateTagName,id,categoryUpdate).ResponseValidator.GetBodyAsText();
        statusCode(updateTagResponse);
        String updatedTagName= JsonPath.read(updateTagResponse,"$.data.name").toString().replace("[","").replace("]","");
        Assert.assertEquals(updateTagName, updatedTagName,"updated by different users");
        String updateParentCategory= JsonPath.read(updateTagResponse,"$.data.parent").toString().replace("[","").replace("]","");
        Assert.assertEquals(id, updateParentCategory,"Parent Tag Category is not same");
    }

    @Test(dataProvider = "createTag", description = "delete tag from tag category")
    public void deleteTag(String enabled, String categoryName, String categoryUpdate, String tagName){
        String response= taggingHelper.createTagCategory(enabled,categoryName, categoryUpdate).ResponseValidator.GetBodyAsText();
        statusCode(response);
        String id= JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertNotNull(id,"id is null");
        String updateBy= JsonPath.read(response,"$.data.updated_by").toString().replace("[","").replace("]","");
        Assert.assertEquals(categoryUpdate, updateBy,"Updated by diff user");
        String getResponse= taggingHelper.getTagById(id).ResponseValidator.GetBodyAsText();
        statusCode(getResponse);
        String tagIds= JsonPath.read(getResponse,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertEquals(tagIds,id,"tags id is null");

        String tagResponse= taggingHelper.createTag(enabled,tagName,id,categoryUpdate).ResponseValidator.GetBodyAsText();
        statusCode(tagResponse);
        String parentTagCategory= JsonPath.read(tagResponse,"$.data.parent").toString().replace("[","").replace("]","");
        Assert.assertEquals(id, parentTagCategory,"parent field is not same");
        String createTagId= JsonPath.read(tagResponse,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertNotNull(createTagId,"id is null");

        String getTagResponse= taggingHelper.getTag(createTagId).ResponseValidator.GetBodyAsText();
        statusCode(getTagResponse);

        String deleteTag= taggingHelper.deleteTag(createTagId).ResponseValidator.GetBodyAsText();
        statusCode(deleteTag);
    }

    @Test(dataProvider = "deleteTagNegative", description = "delete tag from tag category")
    public void deleteTagNotPresent(String tagId){
        String deleteTag= taggingHelper.deleteTag(tagId).ResponseValidator.GetBodyAsText();
        String statusCode= JsonPath.read(deleteTag,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.status0, statusCode,"statusCode is not 0");
        String errorCode= JsonPath.read(deleteTag,"$.error.errorCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.badRequest, errorCode,"errorCode is not 400");
        String errorMesssage= JsonPath.read(deleteTag,"$.error.errorMessage").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.error, errorMesssage,"No error Message");
    }

    @Test(dataProvider = "deleteTagNegative", description = "get tag which is not present")
    public void getTagNotPresent(String tagId){
        String deleteTag= taggingHelper.getTag(tagId).ResponseValidator.GetBodyAsText();
        String statusCode= JsonPath.read(deleteTag,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.status0, statusCode,"statusCode is not 0");
        String errorCode= JsonPath.read(deleteTag,"$.error.errorCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.badRequest, errorCode,"errorCode is not 400");
        String errorMesssage= JsonPath.read(deleteTag,"$.error.errorMessage").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.error, errorMesssage,"No error Message");
    }

    @Test(dataProvider = "createTag", description = "get tag from tag category")
    public void getTag(String enabled, String categoryName, String categoryUpdate, String tagName) {
        String response = taggingHelper.createTagCategory(enabled, categoryName, categoryUpdate).ResponseValidator.GetBodyAsText();
        statusCode(response);
        String id = JsonPath.read(response, "$.data.id").toString().replace("[", "").replace("]", "");
        Assert.assertNotNull(id, "id is null");
        String updateBy = JsonPath.read(response, "$.data.updated_by").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(categoryUpdate, updateBy, "Updated by diff user");
        String getResponse = taggingHelper.getTagById(id).ResponseValidator.GetBodyAsText();
        statusCode(getResponse);
        String tagIds = JsonPath.read(getResponse, "$.data.id").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(tagIds, id, "tags id is null");

        String tagResponse = taggingHelper.createTag(enabled, tagName, id, categoryUpdate).ResponseValidator.GetBodyAsText();
        statusCode(tagResponse);
        String parentTagCategory = JsonPath.read(tagResponse, "$.data.parent").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(id, parentTagCategory, "parent field is not same");
        String createTagId = JsonPath.read(tagResponse, "$.data.id").toString().replace("[", "").replace("]", "");
        Assert.assertNotNull(createTagId, "id is null");
    }

    @Test( description = "get tag by tag category or parent")
    public void getTagByParentId() {
        List<Map<String, Object>> parentId= taggingHelper.getParent();
        int number= random.nextInt(parentId.size());
        String tagId= parentId.get(number).get("parent").toString();
        String response = taggingHelper.getTagByParentId(tagId).ResponseValidator.GetBodyAsText();
        statusCode(response);
    }

    @Test(dataProvider = "createTagConfig")
    public void createTagConfig(String enabled, String name,String update, String entity_type, String multi, String position)
    {
        String response= taggingHelper.createTagCategory(enabled,name, update).ResponseValidator.GetBodyAsText();
        String statusCode= JsonPath.read(response,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.status, statusCode,"statusCode is not 1");
        String id= JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertNotNull(id,"id is null");
        String updateBy= JsonPath.read(response,"$.data.updated_by").toString().replace("[","").replace("]","");
        Assert.assertEquals(update, updateBy,"statusCode is not 1");
        String createTagConfigRsponse= taggingHelper.createTagConfig(id,entity_type,multi,position).ResponseValidator.GetBodyAsText();
        statusCode(createTagConfigRsponse);
        String categoryId= JsonPath.read(createTagConfigRsponse,"$.data.tag_category_id").toString().replace("[","").replace("]","");
        Assert.assertEquals(id, categoryId,"id is not same");
        String multiSelect=JsonPath.read(createTagConfigRsponse,"$.data.is_multi_select").toString().replace("[","").replace("]","");
        Assert.assertEquals(multi, multiSelect,"multi select values is different");
    }

    @Test(dataProvider = "createTagConfigTag", description = "Multiple Tags under a Tag Category, Create A Tag Config")
    public void createTagConfigAndCreateTag(String enabled, String name,String update, String entity_type, String multi, String position, String tagName,String tagName2)
    {
        String response= taggingHelper.createTagCategory(enabled,name, update).ResponseValidator.GetBodyAsText();
        String statusCode= JsonPath.read(response,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.status, statusCode,"statusCode is not 1");
        String id= JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertNotNull(id,"id is null");
        String updateBy= JsonPath.read(response,"$.data.updated_by").toString().replace("[","").replace("]","");
        Assert.assertEquals(update, updateBy,"statusCode is not 1");

        String tagResponse= taggingHelper.createTag(enabled,tagName,id,update).ResponseValidator.GetBodyAsText();
        statusCode(tagResponse);
        String parentTagCategory= JsonPath.read(tagResponse,"$.data.parent").toString().replace("[","").replace("]","");
        Assert.assertEquals(id, parentTagCategory,"parent field is not same");

        String tagResponse1= taggingHelper.createTag(enabled,tagName2,id,update).ResponseValidator.GetBodyAsText();
        statusCode(tagResponse1);
        String parentTagCategory1= JsonPath.read(tagResponse,"$.data.parent").toString().replace("[","").replace("]","");
        Assert.assertEquals(id, parentTagCategory1,"parent field is not same");

        String createTagConfigRsponse= taggingHelper.createTagConfig(id,entity_type,multi,position).ResponseValidator.GetBodyAsText();
        statusCode(createTagConfigRsponse);
        String categoryId= JsonPath.read(createTagConfigRsponse,"$.data.tag_category_id").toString().replace("[","").replace("]","");
        Assert.assertEquals(id, categoryId,"id is not same");
        String multiSelect=JsonPath.read(createTagConfigRsponse,"$.data.is_multi_select").toString().replace("[","").replace("]","");
        Assert.assertEquals(multi, multiSelect,"multi select values is different");
    }

    @Test(dataProvider = "createTagConfig")
    public void getTagConfig(String enabled, String name,String update, String entity_type, String multi, String position)
    {
        String response= taggingHelper.createTagCategory(enabled,name, update).ResponseValidator.GetBodyAsText();
        String statusCode= JsonPath.read(response,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.status, statusCode,"statusCode is not 1");
        String id= JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertNotNull(id,"id is null");
        String updateBy= JsonPath.read(response,"$.data.updated_by").toString().replace("[","").replace("]","");
        Assert.assertEquals(update, updateBy,"statusCode is not 1");
        String createTagConfigRsponse= taggingHelper.createTagConfig(id,entity_type,multi,position).ResponseValidator.GetBodyAsText();
        statusCode(createTagConfigRsponse);
        String categoryId= JsonPath.read(createTagConfigRsponse,"$.data.tag_category_id").toString().replace("[","").replace("]","");
        Assert.assertEquals(id, categoryId,"id is not same");
        String multiSelect=JsonPath.read(createTagConfigRsponse,"$.data.is_multi_select").toString().replace("[","").replace("]","");
        Assert.assertEquals(multi, multiSelect,"multi select values is different");
        String EntityType= entity_type.substring(0, 1).toUpperCase()+entity_type.substring(1);
        String getResponse= taggingHelper.getTagConfigforItemOrRest(entity_type,id).ResponseValidator.GetBodyAsText();
        statusCode(getResponse);
        String categoryId1= JsonPath.read(createTagConfigRsponse,"$.data.tag_category_id").toString().replace("[","").replace("]","");
        Assert.assertEquals(id, categoryId1,"id is not same");
        String multiSelect1=JsonPath.read(createTagConfigRsponse,"$.data.is_multi_select").toString().replace("[","").replace("]","");
        Assert.assertEquals(multi, multiSelect1,"multi select values is different");
    }

    @Test(description = "get all tag config")
    public void getAllTagConfig()
    {
        String response= taggingHelper.getAllTagConfig().ResponseValidator.GetBodyAsText();
        statusCode(response);
        String categoryId= JsonPath.read(response,"$.data..tag_category_id").toString().replace("[","").replace("]","");
        Assert.assertNotNull(categoryId,"category id is null");
    }

    @Test(dataProvider = "createTagConfig", description = "Create a TAg Category, Create A Tag Config, Delete the Tag Config")
    public void deleteTagConfig(String enabled, String name,String update, String entity_type, String multi, String position)
    {
        String response= taggingHelper.createTagCategory(enabled,name, update).ResponseValidator.GetBodyAsText();
        String statusCode= JsonPath.read(response,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.status, statusCode,"statusCode is not 1");
        String id= JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertNotNull(id,"id is null");
        String updateBy= JsonPath.read(response,"$.data.updated_by").toString().replace("[","").replace("]","");
        Assert.assertEquals(update, updateBy,"statusCode is not 1");
        String createTagConfigRsponse= taggingHelper.createTagConfig(id,entity_type,multi,position).ResponseValidator.GetBodyAsText();
        statusCode(createTagConfigRsponse);
        String categoryId= JsonPath.read(createTagConfigRsponse,"$.data.tag_category_id").toString().replace("[","").replace("]","");
        Assert.assertEquals(id, categoryId,"id is not same");
        String multiSelect=JsonPath.read(createTagConfigRsponse,"$.data.is_multi_select").toString().replace("[","").replace("]","");
        Assert.assertEquals(multi, multiSelect,"multi select values is different");
        String EntityType= entity_type.substring(0, 1).toUpperCase()+entity_type.substring(1);
        String getResponse= taggingHelper.getTagConfigforItemOrRest(entity_type,id).ResponseValidator.GetBodyAsText();
        statusCode(getResponse);
        String categoryId1= JsonPath.read(createTagConfigRsponse,"$.data.tag_category_id").toString().replace("[","").replace("]","");
        Assert.assertEquals(id, categoryId1,"id is not same");
        String multiSelect1=JsonPath.read(createTagConfigRsponse,"$.data.is_multi_select").toString().replace("[","").replace("]","");
        Assert.assertEquals(multi, multiSelect1,"multi select values is different");

        String deleteRsponse= taggingHelper.deleteTagConfigForItemOrRest(entity_type,id).ResponseValidator.GetBodyAsText();
        statusCode(deleteRsponse);
    }

    @Test(dataProvider = "createTagConfigTag", description = "Multiple Tag under a TAg Category, Create A Tag Config, Delete the Tag Config")
    public void createTagConfigAndMultipleTagAndDelete(String enabled, String name,String update, String entity_type, String multi, String position, String tagName,String tagName2)
    {
        String response= taggingHelper.createTagCategory(enabled,name, update).ResponseValidator.GetBodyAsText();
        String statusCode= JsonPath.read(response,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.status, statusCode,"statusCode is not 1");
        String id= JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertNotNull(id,"id is null");
        String updateBy= JsonPath.read(response,"$.data.updated_by").toString().replace("[","").replace("]","");
        Assert.assertEquals(update, updateBy,"statusCode is not 1");

        String tagResponse= taggingHelper.createTag(enabled,tagName,id,update).ResponseValidator.GetBodyAsText();
        statusCode(tagResponse);
        String parentTagCategory= JsonPath.read(tagResponse,"$.data.parent").toString().replace("[","").replace("]","");
        Assert.assertEquals(id, parentTagCategory,"parent field is not same");

        String tagResponse1= taggingHelper.createTag(enabled,tagName2,id,update).ResponseValidator.GetBodyAsText();
        statusCode(tagResponse1);
        String parentTagCategory1= JsonPath.read(tagResponse,"$.data.parent").toString().replace("[","").replace("]","");
        Assert.assertEquals(id, parentTagCategory1,"parent field is not same");

        String createTagConfigRsponse= taggingHelper.createTagConfig(id,entity_type,multi,position).ResponseValidator.GetBodyAsText();
        statusCode(createTagConfigRsponse);
        String categoryId= JsonPath.read(createTagConfigRsponse,"$.data.tag_category_id").toString().replace("[","").replace("]","");
        Assert.assertEquals(id, categoryId,"id is not same");
        String multiSelect=JsonPath.read(createTagConfigRsponse,"$.data.is_multi_select").toString().replace("[","").replace("]","");
        Assert.assertEquals(multi, multiSelect,"multi select values is different");
        String EntityType= entity_type.substring(0, 1).toUpperCase()+entity_type.substring(1);

        String deleteRsponse= taggingHelper.deleteTagConfigForItemOrRest(entity_type,id).ResponseValidator.GetBodyAsText();
        statusCode(deleteRsponse);
    }

    @Test(dataProvider = "createTagConfigItem", description = "Item should not be multi select")
    public void createTagConfigForItem(String enabled, String name,String update, String entity_type, String multi, String position, String tagName,String tagName2) {
        String response = taggingHelper.createTagCategory(enabled, name, update).ResponseValidator.GetBodyAsText();
        String statusCode = JsonPath.read(response, "$.statusCode").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(Constants.status, statusCode, "statusCode is not 1");
        String id = JsonPath.read(response, "$.data.id").toString().replace("[", "").replace("]", "");
        Assert.assertNotNull(id, "id is null");
        String updateBy = JsonPath.read(response, "$.data.updated_by").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(update, updateBy, "statusCode is not 1");

        String tagResponse = taggingHelper.createTag(enabled, tagName, id, update).ResponseValidator.GetBodyAsText();
        statusCode(tagResponse);
        String parentTagCategory = JsonPath.read(tagResponse, "$.data.parent").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(id, parentTagCategory, "parent field is not same");

        String tagResponse1 = taggingHelper.createTag(enabled, tagName2, id, update).ResponseValidator.GetBodyAsText();
        statusCode(tagResponse1);
        String parentTagCategory1 = JsonPath.read(tagResponse, "$.data.parent").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(id, parentTagCategory1, "parent field is not same");

        String createTagConfigRsponse = taggingHelper.createTagConfig(id, entity_type, multi, position).ResponseValidator.GetBodyAsText();
        statusCode(createTagConfigRsponse);
        String categoryId = JsonPath.read(createTagConfigRsponse, "$.data.tag_category_id").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(id, categoryId, "id is not same");
        String multiSelect = JsonPath.read(createTagConfigRsponse, "$.data.is_multi_select").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(multi, multiSelect, "multi select values is different");
    }

    @Test(dataProvider = "createTagConfigSingleTag", description = "Single Tag under a TAg Category, Create A Tag Config, Delete the Tag Config")
    public void createTagConfigAndSingleTagAndDelete(String enabled, String name,String update, String entity_type, String multi, String position, String tagName)
    {
        String response= taggingHelper.createTagCategory(enabled,name, update).ResponseValidator.GetBodyAsText();
        String statusCode= JsonPath.read(response,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.status, statusCode,"statusCode is not 1");
        String id= JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertNotNull(id,"id is null");
        String updateBy= JsonPath.read(response,"$.data.updated_by").toString().replace("[","").replace("]","");
        Assert.assertEquals(update, updateBy,"statusCode is not 1");

        String tagResponse= taggingHelper.createTag(enabled,tagName,id,update).ResponseValidator.GetBodyAsText();
        statusCode(tagResponse);
        String parentTagCategory= JsonPath.read(tagResponse,"$.data.parent").toString().replace("[","").replace("]","");
        Assert.assertEquals(id, parentTagCategory,"parent field is not same");

        String createTagConfigRsponse= taggingHelper.createTagConfig(id,entity_type,multi,position).ResponseValidator.GetBodyAsText();
        statusCode(createTagConfigRsponse);
        String categoryId= JsonPath.read(createTagConfigRsponse,"$.data.tag_category_id").toString().replace("[","").replace("]","");
        Assert.assertEquals(id, categoryId,"id is not same");
        String multiSelect=JsonPath.read(createTagConfigRsponse,"$.data.is_multi_select").toString().replace("[","").replace("]","");
        Assert.assertEquals(multi, multiSelect,"multi select values is different");
        String EntityType= entity_type.substring(0, 1).toUpperCase()+entity_type.substring(1);

        String deleteRsponse= taggingHelper.deleteTagConfigForItemOrRest(entity_type,id).ResponseValidator.GetBodyAsText();
        statusCode(deleteRsponse);
    }

    @Test(dataProvider = "updateTagConfig", description = "Create a TAg Category, Create A Tag Config, update the Tag Config")
    public void updateTagConfigUpdateMultiSelect(String enabled, String name,String update, String entity_type, String multi,String multiFalse, String position) {
        String response = taggingHelper.createTagCategory(enabled, name, update).ResponseValidator.GetBodyAsText();
        statusCode(response);
        String id = JsonPath.read(response, "$.data.id").toString().replace("[", "").replace("]", "");
        Assert.assertNotNull(id, "id is null");
        String updateBy = JsonPath.read(response, "$.data.updated_by").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(update, updateBy, "statusCode is not 1");
        String createTagConfigRsponse = taggingHelper.createTagConfig(id, entity_type, multi, position).ResponseValidator.GetBodyAsText();
        statusCode(createTagConfigRsponse);
        String categoryId = JsonPath.read(createTagConfigRsponse, "$.data.tag_category_id").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(id, categoryId, "id is not same");
        String multiSelect = JsonPath.read(createTagConfigRsponse, "$.data.is_multi_select").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(multi, multiSelect, "multi select values is different");
        String getResponse = taggingHelper.getTagConfigforItemOrRest(entity_type, id).ResponseValidator.GetBodyAsText();
        statusCode(getResponse);
        String categoryId1 = JsonPath.read(createTagConfigRsponse, "$.data.tag_category_id").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(id, categoryId1, "id is not same");
        String multiSelect1 = JsonPath.read(createTagConfigRsponse, "$.data.is_multi_select").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(multi, multiSelect1, "multi select values is different");

        String updateTagResponse= taggingHelper.updateTagConfig(id,entity_type,multiFalse,position).ResponseValidator.GetBodyAsText();
        statusCode(updateTagResponse);
        String categoryId2 = JsonPath.read(updateTagResponse, "$.data.tag_category_id").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(id, categoryId2, "id is not same");
        String multiSelect2 = JsonPath.read(updateTagResponse, "$.data.is_multi_select").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(multiFalse, multiSelect2, "multi select values is different");
    }

    @Test(dataProvider = "updateTagConfigEntityType", description = "Create a TAg Category, Create A Tag Config, update the Tag Config")
    public void updateTagConfigUpdateEntityType(String enabled, String name,String update, String entity_type, String multi,String entityChange, String position) {
        String response = taggingHelper.createTagCategory(enabled, name, update).ResponseValidator.GetBodyAsText();
        statusCode(response);
        String id = JsonPath.read(response, "$.data.id").toString().replace("[", "").replace("]", "");
        Assert.assertNotNull(id, "id is null");
        String updateBy = JsonPath.read(response, "$.data.updated_by").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(update, updateBy, "statusCode is not 1");
        String createTagConfigRsponse = taggingHelper.createTagConfig(id, entity_type, multi, position).ResponseValidator.GetBodyAsText();
        statusCode(createTagConfigRsponse);
        String categoryId = JsonPath.read(createTagConfigRsponse, "$.data.tag_category_id").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(id, categoryId, "id is not same");
        String multiSelect = JsonPath.read(createTagConfigRsponse, "$.data.is_multi_select").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(multi, multiSelect, "multi select values is different");
        String getResponse = taggingHelper.getTagConfigforItemOrRest(entity_type, id).ResponseValidator.GetBodyAsText();
        statusCode(getResponse);
        String categoryId1 = JsonPath.read(createTagConfigRsponse, "$.data.tag_category_id").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(id, categoryId1, "id is not same");
        String multiSelect1 = JsonPath.read(createTagConfigRsponse, "$.data.is_multi_select").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(multi, multiSelect1, "multi select values is different");

        String updateTagResponse= taggingHelper.updateTagConfig(id,entityChange,multi,position).ResponseValidator.GetBodyAsText();
        String statusCode= JsonPath.read(updateTagResponse,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.status0, statusCode,"statusCode is not 0");
        String errorCode= JsonPath.read(updateTagResponse,"$.error.errorCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.badRequest, errorCode,"errorCode is not 400");

        String entity= taggingHelper.tagConfig(categoryId1).get(0).get("entity_type").toString();
        Assert.assertEquals(entity_type,entity,"Entity is not same");
    }


    @Test(dataProvider = "createEntityTagMap", description = "Create a category, create a tag under category, createtag config for the tags, create Entity Tag Map")
    public void createEntityTag(String enabled, String name,String update, String entity_type, String multi, String position, String tagName,String tagName2,
                                String category, String entity_id, String partition_id)
    {
        String response= taggingHelper.createTagCategory(enabled,name, update).ResponseValidator.GetBodyAsText();
        String statusCode= JsonPath.read(response,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.status, statusCode,"statusCode is not 1");
        String id= JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertNotNull(id,"id is null");
        String updateBy= JsonPath.read(response,"$.data.updated_by").toString().replace("[","").replace("]","");
        Assert.assertEquals(update, updateBy,"statusCode is not 1");

        String tagResponse= taggingHelper.createTag(enabled,tagName,id,update).ResponseValidator.GetBodyAsText();
        statusCode(tagResponse);
        String parentTagCategory= JsonPath.read(tagResponse,"$.data.parent").toString().replace("[","").replace("]","");
        Assert.assertEquals(id, parentTagCategory,"parent field is not same");
        String tagId= JsonPath.read(tagResponse,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertNotNull(id,"id is null");

        String tagResponse1= taggingHelper.createTag(enabled,tagName2,id,update).ResponseValidator.GetBodyAsText();
        statusCode(tagResponse1);
        String parentTagCategory1= JsonPath.read(tagResponse,"$.data.parent").toString().replace("[","").replace("]","");
        Assert.assertEquals(id, parentTagCategory1,"parent field is not same");

        String createTagConfigRsponse= taggingHelper.createTagConfig(id,entity_type,multi,position).ResponseValidator.GetBodyAsText();
        statusCode(createTagConfigRsponse);
        String categoryId= JsonPath.read(createTagConfigRsponse,"$.data.tag_category_id").toString().replace("[","").replace("]","");
        Assert.assertEquals(id, categoryId,"id is not same");
        String multiSelect=JsonPath.read(createTagConfigRsponse,"$.data.is_multi_select").toString().replace("[","").replace("]","");
        Assert.assertEquals(multi, multiSelect,"multi select values is different");

        String entityTagMap= taggingHelper.createEntityTagMap(category,entity_id,entity_type,partition_id,position,tagId,update).ResponseValidator.GetBodyAsText();
        statusCode(entityTagMap);
        String tag_Name= JsonPath.read(entityTagMap,"$.data.tag_name").toString().replace("[","").replace("]","");
        String name1= taggingHelper.tagConfigDetails(tagId).get(0).get("name").toString();
        Assert.assertEquals(tag_Name,name1,"tagName is not same");
    }

    @Test(dataProvider = "createEntityTagMapRest", description = "Create a category, create a tag under category, createtag config for the tags, create Entity Tag Map")
    public void createEntityTagRest(String enabled, String name,String update, String entity_type, String multi, String position, String tagName,String tagName2,
                                String category, String entity_id, String partition_id)
    {
        String response= taggingHelper.createTagCategory(enabled,name, update).ResponseValidator.GetBodyAsText();
        String statusCode= JsonPath.read(response,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.status, statusCode,"statusCode is not 1");
        String id= JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertNotNull(id,"id is null");
        String updateBy= JsonPath.read(response,"$.data.updated_by").toString().replace("[","").replace("]","");
        Assert.assertEquals(update, updateBy,"statusCode is not 1");

        String tagResponse= taggingHelper.createTag(enabled,tagName,id,update).ResponseValidator.GetBodyAsText();
        statusCode(tagResponse);
        String parentTagCategory= JsonPath.read(tagResponse,"$.data.parent").toString().replace("[","").replace("]","");
        Assert.assertEquals(id, parentTagCategory,"parent field is not same");
        String tagId= JsonPath.read(tagResponse,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertNotNull(id,"id is null");

        String tagResponse1= taggingHelper.createTag(enabled,tagName2,id,update).ResponseValidator.GetBodyAsText();
        statusCode(tagResponse1);
        String parentTagCategory1= JsonPath.read(tagResponse,"$.data.parent").toString().replace("[","").replace("]","");
        Assert.assertEquals(id, parentTagCategory1,"parent field is not same");

        String createTagConfigRsponse= taggingHelper.createTagConfig(id,entity_type,multi,position).ResponseValidator.GetBodyAsText();
        statusCode(createTagConfigRsponse);
        String categoryId= JsonPath.read(createTagConfigRsponse,"$.data.tag_category_id").toString().replace("[","").replace("]","");
        Assert.assertEquals(id, categoryId,"id is not same");
        String multiSelect=JsonPath.read(createTagConfigRsponse,"$.data.is_multi_select").toString().replace("[","").replace("]","");
        Assert.assertEquals(multi, multiSelect,"multi select values is different");

        String entityTagMap= taggingHelper.createEntityTagMap(category,entity_id,entity_type,partition_id,position,tagId,update).ResponseValidator.GetBodyAsText();
        statusCode(entityTagMap);
        String tag_Name= JsonPath.read(entityTagMap,"$.data.tag_name").toString().replace("[","").replace("]","");
        String name1= taggingHelper.tagConfigDetails(tagId).get(0).get("name").toString();
        Assert.assertEquals(tag_Name,name1,"tagName is not same");
    }

    @Test(dataProvider = "updateEntityTagMapCategory", description = "Create a category, create a tag under category, createtag config for the tags, create Entity Tag Map, update the entity tag map")
    public void updateEntityTagChangeCategory(String enabled, String name,String update, String entity_type, String multi, String position, String tagName,String tagName2,
                                    String category, String entity_id, String partition_id, String change_category)
    {
        String response= taggingHelper.createTagCategory(enabled,name, update).ResponseValidator.GetBodyAsText();
        String statusCode= JsonPath.read(response,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.status, statusCode,"statusCode is not 1");
        String id= JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertNotNull(id,"id is null");
        String updateBy= JsonPath.read(response,"$.data.updated_by").toString().replace("[","").replace("]","");
        Assert.assertEquals(update, updateBy,"statusCode is not 1");

        String tagResponse= taggingHelper.createTag(enabled,tagName,id,update).ResponseValidator.GetBodyAsText();
        statusCode(tagResponse);
        String parentTagCategory= JsonPath.read(tagResponse,"$.data.parent").toString().replace("[","").replace("]","");
        Assert.assertEquals(id, parentTagCategory,"parent field is not same");
        String tagId= JsonPath.read(tagResponse,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertNotNull(id,"id is null");

        String tagResponse1= taggingHelper.createTag(enabled,tagName2,id,update).ResponseValidator.GetBodyAsText();
        statusCode(tagResponse1);
        String parentTagCategory1= JsonPath.read(tagResponse,"$.data.parent").toString().replace("[","").replace("]","");
        Assert.assertEquals(id, parentTagCategory1,"parent field is not same");

        String createTagConfigRsponse= taggingHelper.createTagConfig(id,entity_type,multi,position).ResponseValidator.GetBodyAsText();
        statusCode(createTagConfigRsponse);
        String categoryId= JsonPath.read(createTagConfigRsponse,"$.data.tag_category_id").toString().replace("[","").replace("]","");
        Assert.assertEquals(id, categoryId,"id is not same");
        String multiSelect=JsonPath.read(createTagConfigRsponse,"$.data.is_multi_select").toString().replace("[","").replace("]","");
        Assert.assertEquals(multi, multiSelect,"multi select values is different");

        String entityTagMap= taggingHelper.createEntityTagMap(category,entity_id,entity_type,partition_id,position,tagId,update).ResponseValidator.GetBodyAsText();
        statusCode(entityTagMap);
        String tag_Name= JsonPath.read(entityTagMap,"$.data.tag_name").toString().replace("[","").replace("]","");
        String name1= taggingHelper.tagConfigDetails(tagId).get(0).get("name").toString();
        Assert.assertEquals(tag_Name,name1,"tagName is not same");

        String updateEntityTagMap=taggingHelper.updateEntityTag(change_category,entity_id,entity_type,partition_id,position,tagId,update).ResponseValidator.GetBodyAsText();
        statusCode(updateEntityTagMap);
        String updated_tag_Name= JsonPath.read(entityTagMap,"$.data.tag_name").toString().replace("[","").replace("]","");
        String dbname1= taggingHelper.tagConfigDetails(tagId).get(0).get("name").toString();
        Assert.assertEquals(updated_tag_Name,dbname1,"tagName is not same");
    }

    @Test(dataProvider = "updateEntityTagMapRest", description = "Create a category, create a tag under category, createtag config for the tags, create Entity Tag Map, update the entity tag map")
    public void updateEntityTagRest(String enabled, String name,String update, String entity_type, String multi, String position, String tagName,String tagName2,
                                    String category, String entity_id, String partition_id, String change_entity_type)
    {
        String response= taggingHelper.createTagCategory(enabled,name, update).ResponseValidator.GetBodyAsText();
        String statusCode= JsonPath.read(response,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.status, statusCode,"statusCode is not 1");
        String id= JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertNotNull(id,"id is null");
        String updateBy= JsonPath.read(response,"$.data.updated_by").toString().replace("[","").replace("]","");
        Assert.assertEquals(update, updateBy,"statusCode is not 1");

        String tagResponse= taggingHelper.createTag(enabled,tagName,id,update).ResponseValidator.GetBodyAsText();
        statusCode(tagResponse);
        String parentTagCategory= JsonPath.read(tagResponse,"$.data.parent").toString().replace("[","").replace("]","");
        Assert.assertEquals(id, parentTagCategory,"parent field is not same");
        String tagId= JsonPath.read(tagResponse,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertNotNull(id,"id is null");

        String tagResponse1= taggingHelper.createTag(enabled,tagName2,id,update).ResponseValidator.GetBodyAsText();
        statusCode(tagResponse1);
        String parentTagCategory1= JsonPath.read(tagResponse,"$.data.parent").toString().replace("[","").replace("]","");
        Assert.assertEquals(id, parentTagCategory1,"parent field is not same");

        String createTagConfigRsponse= taggingHelper.createTagConfig(id,entity_type,multi,position).ResponseValidator.GetBodyAsText();
        statusCode(createTagConfigRsponse);
        String categoryId= JsonPath.read(createTagConfigRsponse,"$.data.tag_category_id").toString().replace("[","").replace("]","");
        Assert.assertEquals(id, categoryId,"id is not same");
        String multiSelect=JsonPath.read(createTagConfigRsponse,"$.data.is_multi_select").toString().replace("[","").replace("]","");
        Assert.assertEquals(multi, multiSelect,"multi select values is different");

        String entityTagMap= taggingHelper.createEntityTagMap(category,entity_id,entity_type,partition_id,position,tagId,update).ResponseValidator.GetBodyAsText();
        statusCode(entityTagMap);
        String tag_Name= JsonPath.read(entityTagMap,"$.data.tag_name").toString().replace("[","").replace("]","");
        String name1= taggingHelper.tagConfigDetails(tagId).get(0).get("name").toString();
        Assert.assertEquals(tag_Name,name1,"tagName is not same");

        String updateEntityTagMap=taggingHelper.updateEntityTag(category,entity_id,change_entity_type,partition_id,position,tagId,update).ResponseValidator.GetBodyAsText();
        String updateEntityStatusCode= JsonPath.read(updateEntityTagMap,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.status0, updateEntityStatusCode,"statusCode is not 0");
        String updatedErrorCode= JsonPath.read(updateEntityTagMap,"$.error.errorCode").toString().replace("[","").replace("]","");

        Assert.assertEquals(updatedErrorCode,Constants.badRequest,"tagName is not same");
    }

    @Test(dataProvider = "updateEntityTagMapItem", description = "Create a category, create a tag under category, createtag config for the tags, create Entity Tag Map, update the entity tag map")
    public void updateEntityTagItem(String enabled, String name,String update, String entity_type, String multi, String position, String tagName,String tagName2,
                                    String category, String entity_id, String partition_id, String change_entity_type)
    {
        String response= taggingHelper.createTagCategory(enabled,name, update).ResponseValidator.GetBodyAsText();
        String statusCode= JsonPath.read(response,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.status, statusCode,"statusCode is not 1");
        String id= JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertNotNull(id,"id is null");
        String updateBy= JsonPath.read(response,"$.data.updated_by").toString().replace("[","").replace("]","");
        Assert.assertEquals(update, updateBy,"statusCode is not 1");

        String tagResponse= taggingHelper.createTag(enabled,tagName,id,update).ResponseValidator.GetBodyAsText();
        statusCode(tagResponse);
        String parentTagCategory= JsonPath.read(tagResponse,"$.data.parent").toString().replace("[","").replace("]","");
        Assert.assertEquals(id, parentTagCategory,"parent field is not same");
        String tagId= JsonPath.read(tagResponse,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertNotNull(id,"id is null");

        String tagResponse1= taggingHelper.createTag(enabled,tagName2,id,update).ResponseValidator.GetBodyAsText();
        statusCode(tagResponse1);
        String parentTagCategory1= JsonPath.read(tagResponse,"$.data.parent").toString().replace("[","").replace("]","");
        Assert.assertEquals(id, parentTagCategory1,"parent field is not same");

        String createTagConfigRsponse= taggingHelper.createTagConfig(id,entity_type,multi,position).ResponseValidator.GetBodyAsText();
        statusCode(createTagConfigRsponse);
        String categoryId= JsonPath.read(createTagConfigRsponse,"$.data.tag_category_id").toString().replace("[","").replace("]","");
        Assert.assertEquals(id, categoryId,"id is not same");
        String multiSelect=JsonPath.read(createTagConfigRsponse,"$.data.is_multi_select").toString().replace("[","").replace("]","");
        Assert.assertEquals(multi, multiSelect,"multi select values is different");

        String entityTagMap= taggingHelper.createEntityTagMap(category,entity_id,entity_type,partition_id,position,tagId,update).ResponseValidator.GetBodyAsText();
        statusCode(entityTagMap);
        String tag_Name= JsonPath.read(entityTagMap,"$.data.tag_name").toString().replace("[","").replace("]","");
        String name1= taggingHelper.tagConfigDetails(tagId).get(0).get("name").toString();
        Assert.assertEquals(tag_Name,name1,"tagName is not same");

        String updateEntityTagMap=taggingHelper.updateEntityTag(category,entity_id,change_entity_type,partition_id,position,tagId,update).ResponseValidator.GetBodyAsText();
        String updateEntityStatusCode= JsonPath.read(updateEntityTagMap,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(updateEntityStatusCode,Constants.status0,"statusCode is not 0");
        String updatedErrorCode= JsonPath.read(updateEntityTagMap,"$.error.errorCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(updatedErrorCode,Constants.badRequest,"tagName is not same");
    }

    @Test(dataProvider = "getEntityTagMapEntity", description = "Create a category, create a tag under category, createtag config for the tags, create Entity Tag Map, update the entity tag map")
    public void getAllEntityTagEntity(String enabled, String name,String update, String entity_type, String multi, String position, String tagName,String tagName2,
                                    String category, String entity_id, String partition_id) {
        String response = taggingHelper.createTagCategory(enabled, name, update).ResponseValidator.GetBodyAsText();
        String statusCode = JsonPath.read(response, "$.statusCode").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(Constants.status, statusCode, "statusCode is not 1");
        String id = JsonPath.read(response, "$.data.id").toString().replace("[", "").replace("]", "");
        Assert.assertNotNull(id, "id is null");
        String updateBy = JsonPath.read(response, "$.data.updated_by").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(update, updateBy, "statusCode is not 1");

        String tagResponse = taggingHelper.createTag(enabled, tagName, id, update).ResponseValidator.GetBodyAsText();
        statusCode(tagResponse);
        String parentTagCategory = JsonPath.read(tagResponse, "$.data.parent").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(id, parentTagCategory, "parent field is not same");
        String tagId = JsonPath.read(tagResponse, "$.data.id").toString().replace("[", "").replace("]", "");
        Assert.assertNotNull(id, "id is null");

        String tagResponse1 = taggingHelper.createTag(enabled, tagName2, id, update).ResponseValidator.GetBodyAsText();
        statusCode(tagResponse1);
        String parentTagCategory1 = JsonPath.read(tagResponse, "$.data.parent").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(id, parentTagCategory1, "parent field is not same");

        String createTagConfigRsponse = taggingHelper.createTagConfig(id, entity_type, multi, position).ResponseValidator.GetBodyAsText();
        statusCode(createTagConfigRsponse);
        String categoryId = JsonPath.read(createTagConfigRsponse, "$.data.tag_category_id").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(id, categoryId, "id is not same");
        String multiSelect = JsonPath.read(createTagConfigRsponse, "$.data.is_multi_select").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(multi, multiSelect, "multi select values is different");

        String entityTagMap = taggingHelper.createEntityTagMap(category, entity_id, entity_type, partition_id, position, tagId, update).ResponseValidator.GetBodyAsText();
        statusCode(entityTagMap);
        String tag_Name = JsonPath.read(entityTagMap, "$.data.tag_name").toString().replace("[", "").replace("]", "");
        String name1 = taggingHelper.tagConfigDetails(tagId).get(0).get("name").toString();
        Assert.assertEquals(tag_Name, name1, "tagName is not same");
        String entityMapByEntity= taggingHelper.getAllByEntityIdEntitytype(entity_id,entity_type,partition_id).ResponseValidator.GetBodyAsText();
        statusCode(entityMapByEntity);
    }

    @Test(dataProvider = "getEntityTagMap", description = "check all tags , entityType in get Entity tag Map")
    public void getTagIdWithEntityType(String enabled, String name,String update, String entity_type, String multi, String position, String tagName,String tagName2,
                                          String category, String entity_id, String partition_id, String page, String rows)
    {
        String response = taggingHelper.createTagCategory(enabled, name, update).ResponseValidator.GetBodyAsText();
        String statusCode = JsonPath.read(response, "$.statusCode").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(Constants.status, statusCode, "statusCode is not 1");
        String id = JsonPath.read(response, "$.data.id").toString().replace("[", "").replace("]", "");
        Assert.assertNotNull(id, "id is null");
        String updateBy = JsonPath.read(response, "$.data.updated_by").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(update, updateBy, "statusCode is not 1");

        String tagResponse = taggingHelper.createTag(enabled, tagName, id, update).ResponseValidator.GetBodyAsText();
        statusCode(tagResponse);
        String parentTagCategory = JsonPath.read(tagResponse, "$.data.parent").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(id, parentTagCategory, "parent field is not same");
        String tagId = JsonPath.read(tagResponse, "$.data.id").toString().replace("[", "").replace("]", "");
        Assert.assertNotNull(id, "id is null");

        String tagResponse1 = taggingHelper.createTag(enabled, tagName2, id, update).ResponseValidator.GetBodyAsText();
        statusCode(tagResponse1);
        String parentTagCategory1 = JsonPath.read(tagResponse, "$.data.parent").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(id, parentTagCategory1, "parent field is not same");

        String createTagConfigRsponse = taggingHelper.createTagConfig(id, entity_type, multi, position).ResponseValidator.GetBodyAsText();
        statusCode(createTagConfigRsponse);
        String categoryId = JsonPath.read(createTagConfigRsponse, "$.data.tag_category_id").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(id, categoryId, "id is not same");
        String multiSelect = JsonPath.read(createTagConfigRsponse, "$.data.is_multi_select").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(multi, multiSelect, "multi select values is different");

        String entityTagMap = taggingHelper.createEntityTagMap(category, entity_id, entity_type, partition_id, position, tagId, update).ResponseValidator.GetBodyAsText();
        statusCode(entityTagMap);
        String tag_Name = JsonPath.read(entityTagMap, "$.data.tag_name").toString().replace("[", "").replace("]", "");
        String name1 = taggingHelper.tagConfigDetails(tagId).get(0).get("name").toString();
        Assert.assertEquals(tag_Name, name1, "tagName is not same");

        String getEntityResponse= taggingHelper.getEntityTagMap(tagId,entity_type,page,rows).ResponseValidator.GetBodyAsText();
        statusCode(getEntityResponse);
        String get_entity_type = JsonPath.read(getEntityResponse, "$.data..entity_type").toString().replace("[", "").replace("]", "").replace("\"","");
        String get_tag_name = JsonPath.read(getEntityResponse, "$.data..tag_name").toString().replace("[", "").replace("]", "").replace("\"","");
        String get_cateogory = JsonPath.read(getEntityResponse, "$.data..category").toString().replace("[", "").replace("]", "").replace("\"","");
        String get_entity_id = JsonPath.read(getEntityResponse, "$.data..entity_id").toString().replace("[", "").replace("]", "").replace("\"","");

        Assert.assertEquals(get_entity_type, entity_type, "EntityType is not same");
        Assert.assertEquals(get_tag_name,tag_Name,"tag name is not same");
        Assert.assertEquals(get_cateogory, category, "category is not same");
        Assert.assertEquals(get_entity_id,entity_id,"entityId is not same");
    }

    @Test(dataProvider = "getEntityTagMap", description = "check all tags , entityType in get Entity tag Map")
    public void deleteTagIdWithEntityType(String enabled, String name,String update, String entity_type, String multi, String position, String tagName,String tagName2,
                                          String category, String entity_id, String partition_id, String page, String rows)
    {
        String response = taggingHelper.createTagCategory(enabled, name, update).ResponseValidator.GetBodyAsText();
        String statusCode = JsonPath.read(response, "$.statusCode").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(Constants.status, statusCode, "statusCode is not 1");
        String id = JsonPath.read(response, "$.data.id").toString().replace("[", "").replace("]", "");
        Assert.assertNotNull(id, "id is null");
        String updateBy = JsonPath.read(response, "$.data.updated_by").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(update, updateBy, "statusCode is not 1");

        String tagResponse = taggingHelper.createTag(enabled, tagName, id, update).ResponseValidator.GetBodyAsText();
        statusCode(tagResponse);
        String parentTagCategory = JsonPath.read(tagResponse, "$.data.parent").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(id, parentTagCategory, "parent field is not same");
        String tagId = JsonPath.read(tagResponse, "$.data.id").toString().replace("[", "").replace("]", "");
        Assert.assertNotNull(id, "id is null");

        String tagResponse1 = taggingHelper.createTag(enabled, tagName2, id, update).ResponseValidator.GetBodyAsText();
        statusCode(tagResponse1);
        String parentTagCategory1 = JsonPath.read(tagResponse, "$.data.parent").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(id, parentTagCategory1, "parent field is not same");

        String createTagConfigRsponse = taggingHelper.createTagConfig(id, entity_type, multi, position).ResponseValidator.GetBodyAsText();
        statusCode(createTagConfigRsponse);
        String categoryId = JsonPath.read(createTagConfigRsponse, "$.data.tag_category_id").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(id, categoryId, "id is not same");
        String multiSelect = JsonPath.read(createTagConfigRsponse, "$.data.is_multi_select").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(multi, multiSelect, "multi select values is different");

        String entityTagMap = taggingHelper.createEntityTagMap(category, entity_id, entity_type, partition_id, position, tagId, update).ResponseValidator.GetBodyAsText();
        statusCode(entityTagMap);
        String tag_Name = JsonPath.read(entityTagMap, "$.data.tag_name").toString().replace("[", "").replace("]", "");
        String name1 = taggingHelper.tagConfigDetails(tagId).get(0).get("name").toString();
        Assert.assertEquals(tag_Name, name1, "tagName is not same");

        String getEntityResponse= taggingHelper.getEntityTagMap(tagId,entity_type,page,rows).ResponseValidator.GetBodyAsText();
        statusCode(getEntityResponse);
        String get_entity_type = JsonPath.read(getEntityResponse, "$.data..entity_type").toString().replace("[", "").replace("]", "").replace("\"","");
        String get_tag_name = JsonPath.read(getEntityResponse, "$.data..tag_name").toString().replace("[", "").replace("]", "").replace("\"","");
        String get_cateogory = JsonPath.read(getEntityResponse, "$.data..category").toString().replace("[", "").replace("]", "").replace("\"","");
        String get_entity_id = JsonPath.read(getEntityResponse, "$.data..entity_id").toString().replace("[", "").replace("]", "").replace("\"","");

        Assert.assertEquals(get_entity_type, entity_type, "EntityType is not same");
        Assert.assertEquals(get_tag_name,tag_Name,"tag name is not same");
        Assert.assertEquals(get_cateogory, category, "category is not same");
        Assert.assertEquals(get_entity_id,entity_id,"entityId is not same");

        String deleteEntityTagMap= taggingHelper.deleteEntityMap(get_entity_type,get_entity_id,tagId,partition_id).ResponseValidator.GetBodyAsText();
        statusCode(deleteEntityTagMap);
        String deleteData = JsonPath.read(deleteEntityTagMap, "$.data").toString().replace("[", "").replace("]", "").replace("\"","");
        Assert.assertEquals(deleteData, Constants.dataKey, "Data key is not same");
    }

   /* @Test(dataProvider = "getEntit")
    public void t(String payload)
    {
        String r= taggingHelper.entityTagMapBulk(payload).ResponseValidator.GetBodyAsText();
        System.out.println(r);
    }


    @Test(dataProvider = "getEntityAdd")
    public void EntityBulkUpload(String payload)
    {
        String r= taggingHelper.entityTagMapBulk(payload).ResponseValidator.GetBodyAsText();
    }*/

    @Test(dataProvider = "createEntityTagMapRest", description = "Create a category, create a tag under category, createtag config for the tags, create Entity Tag Map")
    public void createEntityTagBulkRest(String enabled, String name,String update, String entity_type, String multi, String position, String tagName,String tagName2,
                                    String category, String entity_id, String partition_id)
    {
        String response= taggingHelper.createTagCategory(enabled,name, update).ResponseValidator.GetBodyAsText();
        String statusCode= JsonPath.read(response,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.status, statusCode,"statusCode is not 1");
        String id= JsonPath.read(response,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertNotNull(id,"id is null");
        String updateBy= JsonPath.read(response,"$.data.updated_by").toString().replace("[","").replace("]","");
        Assert.assertEquals(update, updateBy,"statusCode is not 1");

        String tagResponse= taggingHelper.createTag(enabled,tagName,id,update).ResponseValidator.GetBodyAsText();
        statusCode(tagResponse);
        String parentTagCategory= JsonPath.read(tagResponse,"$.data.parent").toString().replace("[","").replace("]","");
        Assert.assertEquals(id, parentTagCategory,"parent field is not same");
        String tagId= JsonPath.read(tagResponse,"$.data.id").toString().replace("[","").replace("]","");
        Assert.assertNotNull(id,"id is null");

        String tagResponse1= taggingHelper.createTag(enabled,tagName2,id,update).ResponseValidator.GetBodyAsText();
        statusCode(tagResponse1);
        String parentTagCategory1= JsonPath.read(tagResponse,"$.data.parent").toString().replace("[","").replace("]","");
        Assert.assertEquals(id, parentTagCategory1,"parent field is not same");

        String createTagConfigRsponse= taggingHelper.createTagConfig(id,entity_type,multi,position).ResponseValidator.GetBodyAsText();
        statusCode(createTagConfigRsponse);
        String categoryId= JsonPath.read(createTagConfigRsponse,"$.data.tag_category_id").toString().replace("[","").replace("]","");
        Assert.assertEquals(id, categoryId,"id is not same");
        String multiSelect=JsonPath.read(createTagConfigRsponse,"$.data.is_multi_select").toString().replace("[","").replace("]","");
        Assert.assertEquals(multi, multiSelect,"multi select values is different");

        String entityTagMap= taggingHelper.createEntityTagMap(category,entity_id,entity_type,partition_id,position,tagId,update).ResponseValidator.GetBodyAsText();
        statusCode(entityTagMap);
        String tag_Name= JsonPath.read(entityTagMap,"$.data.tag_name").toString().replace("[","").replace("]","");
        String name1= taggingHelper.tagConfigDetails(tagId).get(0).get("name").toString();
        Assert.assertEquals(tag_Name,name1,"tagName is not same");
    }

    @Test(dataProvider = "getEntityAddSingleMultipleTag")
    public void EntityCreateBulkSingle(String payload)
    {
        String response= taggingHelper.entityTagMapBulk(payload).ResponseValidator.GetBodyAsText();
        statusCode(response);
        String category= JsonPath.read(response,"$.data.entity_tag_map..category").toString().replace("[","").replace("]","").replace("\"","");
        String tagId= JsonPath.read(response,"$.data.entity_tag_map..tag_id").toString().replace("[","").replace("]","").replace("\"","");
        String tagName=JsonPath.read(response,"$.data.entity_tag_map..tag_name").toString().replace("[","").replace("]","").replace("\"","");
        String updated=JsonPath.read(response,"$.data.entity_tag_map..updated_by").toString().replace("[","").replace("]","").replace("\"","");
        String entityType=JsonPath.read(response,"$.data.entity_tag_map..entity_type").toString().replace("[","").replace("]","").replace("\"","");
        List<Map<String, Object>> entityTag= taggingHelper.entityTagMapDetails(tagId);
        Assert.assertEquals(entityType,entityTag.get(0).get("entity_type").toString(),"entity type is not same");
        Assert.assertEquals(tagId,entityTag.get(0).get("tag_id").toString(),"tag id is not same");
        Assert.assertEquals(tagName,entityTag.get(0).get("name").toString(),"tag name is not same");
        Assert.assertEquals(updated,entityTag.get(0).get("updated_by").toString(),"tag name is not same");
        Assert.assertEquals(category,entityTag.get(0).get("category").toString(),"tag name is not same");
    }

    @Test(dataProvider = "getEntityAddMultipleTag")
    public void EntityCreateBulkMultiple(String payload)
    {
        String response= taggingHelper.entityTagMapBulk(payload).ResponseValidator.GetBodyAsText();
        statusCode(response);
        String[] category= JsonPath.read(response,"$.data.entity_tag_map..category").toString().replace("[","").replace("]","")
                .replace("\"","").split(",");
        String[] tagId= JsonPath.read(response,"$.data.entity_tag_map..tag_id").toString().replace("[","").replace("]","")
                .replace("\"","").split(",");
        String[] tagName=JsonPath.read(response,"$.data.entity_tag_map..tag_name").toString().replace("[","").replace("]","")
                .replace("\"","").split(",");
        String[] updated=JsonPath.read(response,"$.data.entity_tag_map..updated_by").toString().replace("[","").replace("]","")
                .replace("\"","").split(",");
        String[] entityType=JsonPath.read(response,"$.data.entity_tag_map..entity_type").toString().replace("[","").replace("]","")
                .replace("\"","").split(",");
        for(int i=0; i<tagId.length; i++)
        {
            List<Map<String, Object>> entityTag = taggingHelper.entityTagMapDetails(tagId[i]);
            System.out.println("-->"+entityTag.get(0).get("entity_type").toString());
            Assert.assertEquals(entityType[i], entityTag.get(0).get("entity_type").toString(), "entity type is not same");
            Assert.assertEquals(tagId[i], entityTag.get(0).get("tag_id").toString(), "tag id is not same");
            Assert.assertEquals(tagName[i], entityTag.get(0).get("name").toString(), "tag name is not same");
            Assert.assertEquals(updated[i], entityTag.get(0).get("updated_by").toString(), "tag name is not same");
            Assert.assertEquals(category[i], entityTag.get(0).get("category").toString(), "tag name is not same");
        }
    }

    @Test(dataProvider = "getEntityAndDeleteMultipleTag")
    public void EntityCreateAndDeleteBulkMultiple(String payload)
    {
        String response= taggingHelper.entityTagMapBulk(payload).ResponseValidator.GetBodyAsText();
        statusCode(response);
        String[] category= JsonPath.read(response,"$.data.entity_tag_map..category").toString().replace("[","").replace("]","")
                .replace("\"","").split(",");
        String[] tagId= JsonPath.read(response,"$.data.entity_tag_map..tag_id").toString().replace("[","").replace("]","")
                .replace("\"","").split(",");
        String[] tagName=JsonPath.read(response,"$.data.entity_tag_map..tag_name").toString().replace("[","").replace("]","")
                .replace("\"","").split(",");
        String[] updated=JsonPath.read(response,"$.data.entity_tag_map..updated_by").toString().replace("[","").replace("]","")
                .replace("\"","").split(",");
        String[] entityType=JsonPath.read(response,"$.data.entity_tag_map..entity_type").toString().replace("[","").replace("]","")
                .replace("\"","").split(",");
        for(int i=0; i<tagId.length; i++)
        {
            List<Map<String, Object>> entityTag = taggingHelper.entityTagMapDetails(tagId[i]);
            System.out.println("-->"+entityTag.get(0).get("entity_type").toString());
            Assert.assertEquals(entityType[i], entityTag.get(0).get("entity_type").toString(), "entity type is not same");
            Assert.assertEquals(tagId[i], entityTag.get(0).get("tag_id").toString(), "tag id is not same");
            Assert.assertEquals(tagName[i], entityTag.get(0).get("name").toString(), "tag name is not same");
            Assert.assertEquals(updated[i], entityTag.get(0).get("updated_by").toString(), "tag name is not same");
            Assert.assertEquals(category[i], entityTag.get(0).get("category").toString(), "tag name is not same");
        }
    }

    @Test(dataProvider = "getEntityAndDeleteMultipleOneTag")
    public void EntityCreateAndDeleteBulkMultipleOne(String payload)
    {
        String response= taggingHelper.entityTagMapBulk(payload).ResponseValidator.GetBodyAsText();
        statusCode(response);
        String[] category= JsonPath.read(response,"$.data.entity_tag_map..category").toString().replace("[","").replace("]","")
                .replace("\"","").split(",");
        String[] tagId= JsonPath.read(response,"$.data.entity_tag_map..tag_id").toString().replace("[","").replace("]","")
                .replace("\"","").split(",");
        String[] tagName=JsonPath.read(response,"$.data.entity_tag_map..tag_name").toString().replace("[","").replace("]","")
                .replace("\"","").split(",");
        String[] updated=JsonPath.read(response,"$.data.entity_tag_map..updated_by").toString().replace("[","").replace("]","")
                .replace("\"","").split(",");
        String[] entityType=JsonPath.read(response,"$.data.entity_tag_map..entity_type").toString().replace("[","").replace("]","")
                .replace("\"","").split(",");
        for(int i=0; i<tagId.length; i++)
        {
            List<Map<String, Object>> entityTag = taggingHelper.entityTagMapDetails(tagId[i]);
            System.out.println("-->"+entityTag.get(0).get("entity_type").toString());
            Assert.assertEquals(entityType[i], entityTag.get(0).get("entity_type").toString(), "entity type is not same");
            Assert.assertEquals(tagId[i], entityTag.get(0).get("tag_id").toString(), "tag id is not same");
            Assert.assertEquals(tagName[i], entityTag.get(0).get("name").toString(), "tag name is not same");
            Assert.assertEquals(updated[i], entityTag.get(0).get("updated_by").toString(), "tag name is not same");
            Assert.assertEquals(category[i], entityTag.get(0).get("category").toString(), "tag name is not same");
        }
    }

    @Test(dataProvider = "getEntityAndUpdateMultipleTag")
    public void EntityCreateAndUpdateBulk(String payload)
    {
        String response= taggingHelper.entityTagMapBulk(payload).ResponseValidator.GetBodyAsText();
        statusCode(response);
        String[] category= JsonPath.read(response,"$.data.entity_tag_map..category").toString().replace("[","").replace("]","")
                .replace("\"","").split(",");
        String[] tagId= JsonPath.read(response,"$.data.entity_tag_map..tag_id").toString().replace("[","").replace("]","")
                .replace("\"","").split(",");
        String[] tagName=JsonPath.read(response,"$.data.entity_tag_map..tag_name").toString().replace("[","").replace("]","")
                .replace("\"","").split(",");
        String[] updated=JsonPath.read(response,"$.data.entity_tag_map..updated_by").toString().replace("[","").replace("]","")
                .replace("\"","").split(",");
        String[] entityType=JsonPath.read(response,"$.data.entity_tag_map..entity_type").toString().replace("[","").replace("]","")
                .replace("\"","").split(",");
        for(int i=0; i<tagId.length; i++)
        {
            List<Map<String, Object>> entityTag = taggingHelper.entityTagMapDetails(tagId[i]);
            System.out.println("-->"+entityTag.get(0).get("entity_type").toString());
            Assert.assertEquals(entityType[i], entityTag.get(0).get("entity_type").toString(), "entity type is not same");
            Assert.assertEquals(tagId[i], entityTag.get(0).get("tag_id").toString(), "tag id is not same");
            Assert.assertEquals(tagName[i], entityTag.get(0).get("name").toString(), "tag name is not same");
            Assert.assertEquals(updated[i], entityTag.get(0).get("updated_by").toString(), "tag name is not same");
            System.out.println("00000"+category[i]);
            Assert.assertEquals(category[i], entityTag.get(0).get("category").toString(), "category name is not same");
        }
    }

    @Test(dataProvider = "getEntityAndUpdateAndDeleteMultipleTag")
    public void EntityCreateAndUpdateAndDeleteBulk(String payload)
    {
        String response= taggingHelper.entityTagMapBulk(payload).ResponseValidator.GetBodyAsText();
        statusCode(response);
        String[] category= JsonPath.read(response,"$.data.entity_tag_map..category").toString().replace("[","").replace("]","")
                .replace("\"","").split(",");
        String[] tagId= JsonPath.read(response,"$.data.entity_tag_map..tag_id").toString().replace("[","").replace("]","")
                .replace("\"","").split(",");
        String[] tagName=JsonPath.read(response,"$.data.entity_tag_map..tag_name").toString().replace("[","").replace("]","")
                .replace("\"","").split(",");
        String[] updated=JsonPath.read(response,"$.data.entity_tag_map..updated_by").toString().replace("[","").replace("]","")
                .replace("\"","").split(",");
        String[] entityType=JsonPath.read(response,"$.data.entity_tag_map..entity_type").toString().replace("[","").replace("]","")
                .replace("\"","").split(",");
        for(int i=0; i<tagId.length; i++)
        {
            List<Map<String, Object>> entityTag = taggingHelper.entityTagMapDetails(tagId[i]);
            System.out.println("-->"+entityTag.get(0).get("entity_type").toString());
            Assert.assertEquals(entityType[i], entityTag.get(0).get("entity_type").toString(), "entity type is not same");
            Assert.assertEquals(tagId[i], entityTag.get(0).get("tag_id").toString(), "tag id is not same");
            Assert.assertEquals(tagName[i], entityTag.get(0).get("name").toString(), "tag name is not same");
            Assert.assertEquals(updated[i], entityTag.get(0).get("updated_by").toString(), "tag name is not same");
            System.out.println("00000"+category[i]);
            Assert.assertEquals(category[i], entityTag.get(0).get("category").toString(), "category name is not same");
        }
    }



    public void statusCode(String response)
    {
        String statusCode= JsonPath.read(response,"$.statusCode").toString().replace("[","").replace("]","");
        Assert.assertEquals(Constants.status, statusCode,"statusCode is not 1");
    }

}
