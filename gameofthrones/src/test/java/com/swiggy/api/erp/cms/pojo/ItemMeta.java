package com.swiggy.api.erp.cms.pojo;

import org.apache.commons.lang.builder.ToStringBuilder;

public class ItemMeta {

    private String user;

    /**
     * No args constructor for use in serialization
     *
     */
    public ItemMeta() {
    }

    /**
     *
     * @param user
     */
    public ItemMeta(String user) {
        super();
        this.user = user;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("user", user).toString();
    }

}
