package com.swiggy.api.erp.delivery.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.castleblack.CastleBlackHelper;
import com.swiggy.api.erp.delivery.dp.EndtoEndDP;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceHelper;
import com.swiggy.api.erp.delivery.helper.EndToEndHelp;
import com.swiggy.api.erp.delivery.helper.OrderStatus;
import com.swiggy.api.erp.ff.helper.OMSHelper;
import com.swiggy.api.sf.checkout.pojo.CreateMenuEntry;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Daenerys.Initializer;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

public class EndToEndProdCAPRD extends EndtoEndDP {
    CastleBlackHelper helper= new CastleBlackHelper();
    EndToEndHelp endToEndHelp= new EndToEndHelp();
    DeliveryServiceHelper delhelp=new DeliveryServiceHelper();
    //Initialize gameofthrones= new Initialize();
    Initialize gameofthrones =Initializer.getInitializer();
    OMSHelper omsHelper= new OMSHelper();

    @Test(dataProvider = "endtoendprod")
    public void createOrderProd(CreateMenuEntry payload, String de_id) throws IOException, InterruptedException {
        String order_id=endToEndHelp.createOrder(payload);
        delhelp.processOrderInDeliveryProd(order_id, OrderStatus.DELIVERED, de_id);
        String cancellation= omsHelper.OrderCancellationInProd("65","151", "0", "false",order_id).ResponseValidator.GetBodyAsText();
        String cancellationStatus= JsonPath.read(cancellation, "$.data.cancellation_status.cancellation_initiated").toString().replace("[","").replace("]","");
        Assert.assertEquals(cancellationStatus,"true", "cancellation status is false");
        String message= JsonPath.read(cancellation, "$.response.message").toString().replace("[","").replace("]","");
        Assert.assertEquals(message,"Success", "cancellation failure");
    }
}
