package com.swiggy.api.erp.crm.dp.foodissues;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.swiggy.api.erp.crm.helper.ReadGoogleSheet;
import java.util.concurrent.atomic.AtomicInteger;


public class FoodIssuesRules {

	 public void nodeId() throws IOException {
	 setigccRules();
	 }

	public static Multimap<String, String[]> setigccRules() throws IOException {

		int lengthDisposition = 0;
		String key = "igccRules";
		Multimap<String, String[]> rulesMap = ArrayListMultimap.create();

		 String spreadsheetId = "18kFXJPc6iVUdWHOnKwLzjzIudVpv7FsmBvdktC67Sno";
	     String range = "Final Rules";
	        
		List<List<Object>> values = ReadGoogleSheet.sheetData(spreadsheetId, range);

		System.out.println("no. of rows    " + values.size() + "\n");
		if (values == null || values.size() == 0) {
			System.out.println("No data found.");
		} else {
			System.out.println("Name, Major");
			for (List row : values) {

				String l1 = (String) row.get(0), l2 = (String) row.get(1), segment = (String) row.get(2),
						fraud = (String) row.get(3), orderType = (String) row.get(4), time = (String) row.get(5),
						action = (String) row.get(6), refund = (String) row.get(7), coupon = (String) row.get(8);

				String[] expDisposition = null;

				if (l1.equalsIgnoreCase("Quality") && l2.equalsIgnoreCase("All")) {
					expDisposition = new String[] { "Spicy", "Tasty", "Cold", "Stale", "Burnt", "Foreign Particles",
							"Melted" };
					lengthDisposition = expDisposition.length;

				} else if (l2.equalsIgnoreCase("Spicy/Tasty/Cold/Stale")) {
					expDisposition = new String[] { "Spicy", "Tasty", "Cold", "Stale" };
					lengthDisposition = expDisposition.length;

				} else if (l2.equalsIgnoreCase("Burnt/Foreign Particles/Melted")) {
					expDisposition = new String[] { "Burnt", "Foreign Particles", "Melted" };
					lengthDisposition = expDisposition.length;
				}

				
				if (segment.equalsIgnoreCase("All")) {
					String[] expSegment = { "H", "M", "L", "New" };
					int length = expSegment.length;
					for (int i = 0; i < length; i++) {
						if (fraud.equalsIgnoreCase("All")) {
							String[] expFraud = { "Good", "H", "M", "Null" };
							int fraudLength = expFraud.length;
							for (int k = 0; k < fraudLength; k++) {
								if (l1.equalsIgnoreCase("Quality")) {
									for (int m = 0; m < lengthDisposition; m++) {
										rulesMap.put(key, new String[] { l1, expDisposition[m], expSegment[i],
												expFraud[k], orderType, time, action, refund, coupon });
									}
								} else {
									rulesMap.put(key, new String[] { l1, l2, expSegment[i], expFraud[k], orderType,
											time, action, refund, coupon });
								}
							}
						} else {

							if (l1.equalsIgnoreCase("Quality")) {
								for (int m = 0; m < lengthDisposition; m++) {
									rulesMap.put(key, new String[] { l1, expDisposition[m], expSegment[i], fraud,
											orderType, time, action, refund, coupon });
								}
							} else
								rulesMap.put(key, new String[] { l1, l2, expSegment[i], fraud, orderType, time, action,
										refund, coupon });
						}
					}
				}

				else if (segment.equalsIgnoreCase("M,L,New")) {
					String[] expSeg = { "M", "L", "New" };
					int len = expSeg.length;
					for (int j = 0; j < len; j++) {
						if (fraud.equalsIgnoreCase("All")) {
							String[] expFra = { "Good", "H", "M", "Null" };
							int fraudLen = expFra.length;
							for (int l = 0; l < fraudLen; l++) {
								if (l1.equalsIgnoreCase("Quality")) {
									for (int m = 0; m < lengthDisposition; m++) {
										rulesMap.put(key, new String[] { l1, expDisposition[m], expSeg[j], expFra[l],
												orderType, time, action, refund, coupon });
									}
								} else {
									rulesMap.put(key, new String[] { l1, l2, expSeg[j], expFra[l], orderType, time,
											action, refund, coupon });
								}
							}
						} else {
							if (l1.equalsIgnoreCase("Quality")) {
								for (int m = 0; m < lengthDisposition; m++) {
									rulesMap.put(key, new String[] { l1, expDisposition[m], expSeg[j], fraud, orderType,
											time, action, refund, coupon });
								}
							} else {
								rulesMap.put(key, new String[] { l1, l2, expSeg[j], fraud, orderType, time, action,
										refund, coupon });
							}
						}
					}
				}

				else {
					rulesMap.put(key, new String[] {l1, l2, segment, fraud, orderType, time, action, refund, coupon});
				}
			}
		}

		System.out.println("size of multimap" + rulesMap.size() + "\n");

		return rulesMap;
	}
}
