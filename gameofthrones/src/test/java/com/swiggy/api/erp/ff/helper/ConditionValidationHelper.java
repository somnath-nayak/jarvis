package com.swiggy.api.erp.ff.helper;

import com.swiggy.api.erp.ff.constants.WatchDogConstants;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.SqlTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ConditionValidationHelper {

    Processor processor;

    GameOfThronesService service;
    Initialize  initialize = new Initialize();


    public Processor getRegisterCondtionProcessor(String condition )
    {
        service = new GameOfThronesService("watchdog","registercondition",initialize);
        HashMap header = new HashMap();
        header.put("Content-Type","application/json");
        String[] payload = new String[]{condition};

        return new Processor(service,header,payload);


    }

    public Processor getEvaluateCondtionProcessor(String conditionId,String condition)
    {
        service = new GameOfThronesService("watchdog","evaluatecondition",initialize);
        HashMap header = new HashMap();
        header.put("Content-Type","application/json");
        String[] urlParam = new String[]{conditionId};
        String[] payload = new String[]{condition};

        return new Processor(service,header,payload,urlParam);


    }


    public Processor getECAProcessor(String groupname,String state,String conditionId, String Condition, String ac)
    {
        service = new GameOfThronesService("watchdog","eca",initialize);
        HashMap header = new HashMap();
        header.put("Content-Type","application/json");
        String[] payload = new String[]{groupname,state,conditionId,Condition,ac};

        return new Processor(service,header,payload);


    }

    public boolean dbValidtion(String conditionId)
    {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(WatchDogConstants.omsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(WatchDogConstants.getCondtionsById+conditionId);
        return list.size() != 0;

    }

    public List<Map<String, Object>> getWatchDogDB(String OrderId)
    {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(WatchDogConstants.hostName);
        List<Map<String, Object>> list = sqlTemplate.queryForList(WatchDogConstants.getEcaLogs+OrderId);
return list;
    }

}
