package com.swiggy.api.erp.cms.tests.CatalogV2;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.constants.CatalogV2Constants;
import com.swiggy.api.erp.cms.helper.CatalogV2Helper;
import org.junit.Assert;
import org.testng.annotations.Test;

/**
 * Created-By:Ashiwani
 */

public class TestListing {

    CatalogV2Helper cms=new CatalogV2Helper();

    @Test
    public void getListing(){
        String response=cms.getListing(CatalogV2Constants.store_id).ResponseValidator.GetBodyAsText();
        Assert.assertNotNull(JsonPath.read(response,"$.taxonomy..nodes"));
    }
}
