package com.swiggy.api.erp.ff.tests.fraudService;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.ff.POJO.*;
import com.swiggy.api.erp.ff.constants.FraudServiceConstants;
import com.swiggy.api.erp.ff.dp.FraudServiceDP.FraudServiceData;
import com.swiggy.api.erp.ff.helper.FraudServiceHelper;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import framework.gameofthrones.Tyrion.WireMockHelper;
import org.testng.Assert;
import org.testng.annotations.*;

import java.io.IOException;


public class CODFallback {

    CODCheckPayload ch;
    FraudServiceHelper fsh = new FraudServiceHelper();
    JsonHelper j = new JsonHelper();
    Processor processor;
    WireMockHelper wireMockHelper = new WireMockHelper();

//
//    @BeforeClass
//    public void mockServerStart() {
//        fsh.clearRedisKeys(FraudServiceConstants.CityId_1);
//        fsh.clearRedisKeys(FraudServiceConstants.CityId_2);
//        fsh.clearRedisKeys(FraudServiceConstants.CityId_3);
//        fsh.clearRedisKeys(FraudServiceConstants.CityId_4);
//        wireMockHelper.startMockServer(FraudServiceConstants.mockPort);
//        fsh.AddCityDefaultConfigurations(FraudServiceConstants.CityId_1, FraudServiceConstants.Cityfee_300);
//        fsh.AddCityDefaultConfigurations(FraudServiceConstants.CityId_2, FraudServiceConstants.Cityfee_100);
//        fsh.AddCityDefaultConfigurations(FraudServiceConstants.CityId_3, FraudServiceConstants.Cityfee_0);
//
//    }

    @Test(dataProvider = "cityDefaultCheck", dataProviderClass = FraudServiceData.class, description = "Defalut Fall Back mechanism for Fraud Service")
    public void cityDefaultCheck(String requestId, CustomerDetails customerDetails, SessionDetails sessionDetails, DeliveryDetails deliveryDetails, CartDetails cartDetails) throws IOException {
        fsh.mockDeliveryBoyResponse(FraudServiceConstants.statusCode);
        fsh.mockDSResponse(FraudServiceConstants.statusCode, FraudServiceConstants.predication_1,FraudServiceConstants.status_1,FraudServiceConstants.delay);
        ch = new CODCheckPayload(requestId, customerDetails, sessionDetails, deliveryDetails, cartDetails);
        String body = j.getObjectToJSON(ch);
        processor = fsh.getCODCheckServiceProcessor(body);
        fsh.statusCheck(processor);
        String response = processor.ResponseValidator.GetBodyAsText();
        String CODStatus =   JsonPath.read(response, "$.cod_enabled").toString();
        String CODMessage =   JsonPath.read(response, "$.reason").toString();
        String cityID = deliveryDetails.getCityId().toString();
        double amount = (double) cartDetails.getBillAmount();
        checkAccordingToDefaltCity(CODStatus, cityID, amount,CODMessage,FraudServiceConstants.CODExpectedFailureMessage);


    }

    @Test(dataProvider = "cityDefaultCheck", dataProviderClass = FraudServiceData.class, description = "DSP Predication 2 validations")
    public void DSPUnpredicatbleCheck(String requestId, CustomerDetails customerDetails, SessionDetails sessionDetails, DeliveryDetails deliveryDetails, CartDetails cartDetails) throws IOException {
        fsh.mockDeliveryBoyResponse(FraudServiceConstants.statusCode);
        fsh.mockDSResponse(FraudServiceConstants.statusCode, FraudServiceConstants.predication_2,FraudServiceConstants.status_1);
        ch = new CODCheckPayload(requestId, customerDetails, sessionDetails, deliveryDetails, cartDetails);
        String body = j.getObjectToJSON(ch);
        processor = fsh.getCODCheckServiceProcessor(body);
        fsh.statusCheck(processor);
        String response = processor.ResponseValidator.GetBodyAsText();
        String CODMessage =   JsonPath.read(response, "$.reason").toString();
        String CODStatus =   JsonPath.read(response, "$.cod_enabled").toString();
        String cityID = deliveryDetails.getCityId().toString();
        double amount = (double) cartDetails.getBillAmount();
        checkAccordingToDefaltCity(CODStatus, cityID, amount,CODMessage,FraudServiceConstants.CODExpectedLowConfidenceMessage);


    }


    @Test(dataProvider = "cityDefaultCheck", dataProviderClass = FraudServiceData.class,description = "COD check for DS false Condition")
    public void codCheckForDSPCallFailed(String requestId, CustomerDetails customerDetails, SessionDetails sessionDetails, DeliveryDetails deliveryDetails, CartDetails cartDetails) throws IOException, InterruptedException {


        fsh.mockDeliveryBoyResponse(FraudServiceConstants.statusCode);
        fsh.mockDSResponse(FraudServiceConstants.statusCode, FraudServiceConstants.predication_1,FraudServiceConstants.status_0);


        Processor delivery = fsh.getMockDeliveryProcessor();
        Processor dsp = fsh.getMockDspProcessor();

        System.out.println("DeliveryResposne ======================"+delivery.ResponseValidator.GetBodyAsText());
        System.out.println("DSPResposne ======================"+dsp.ResponseValidator.GetBodyAsText());

        ch = new CODCheckPayload(requestId, customerDetails, sessionDetails, deliveryDetails, cartDetails);
        String body = j.getObjectToJSON(ch);
        processor = fsh.getCODCheckServiceProcessor(body);
        fsh.statusCheck(processor);
        String response = processor.ResponseValidator.GetBodyAsText();
        String CODMessage =   JsonPath.read(response, "$.reason").toString();
        String CODStatus =   JsonPath.read(response, "$.cod_enabled").toString();
        String cityID = deliveryDetails.getCityId().toString();
        double amount = (double) cartDetails.getBillAmount();
        checkAccordingToDefaltCity(CODStatus, cityID, amount,CODMessage,FraudServiceConstants.CODModelFailure);


    }


    public void checkAccordingToDefaltCity(String CODStatus,String cityID, double amount,String CODMessage,String expectedMessage) {
        switch (cityID) {
            case "1":
                if (amount > FraudServiceConstants.CityFee_1_300) {
                    Assert.assertEquals(CODStatus, "false", "Amount is more than configure city level defalut so need to fail");
                    Assert.assertEquals(CODMessage, expectedMessage, "Wrong mesage is coming");
                }else {
                    Assert.assertEquals(CODStatus,"true","Amount is less than configured city amount so it should pass");
                    Assert.assertEquals(CODMessage, expectedMessage, "Wrong mesage is coming");

                }
              break;
            case "2":
                if (amount > FraudServiceConstants.CityFee_2_100) {
                    Assert.assertEquals(CODStatus, "false", "Amount is more than configure city level defalut so need to fail");
                    Assert.assertEquals(CODMessage, expectedMessage, "Wrong mesage is coming");

                }else {
                    Assert.assertEquals(CODStatus,"true","Amount is less than configured city amount so it should pass");
                    Assert.assertEquals(CODMessage, expectedMessage, "Wrong mesage is coming");

                }
                break;
            case "3":
                if (amount > FraudServiceConstants.CityFee_3_0){
                    Assert.assertEquals(CODStatus,"false","Amount is more than configure city level defalut so need to fail");
                    Assert.assertEquals(CODMessage, expectedMessage, "Wrong mesage is coming");

                }
                else {
                    Assert.assertEquals(CODStatus,"true","Amount is less than configured city amount so it should pass");
                    Assert.assertEquals(CODMessage, expectedMessage, "Wrong mesage is coming");

                }
                break;
            case "4":
            if (amount > FraudServiceConstants.default_1000){
                Assert.assertEquals(CODStatus,"false","Amount is more than configure city level defalut so need to fail");
                Assert.assertEquals(CODMessage, expectedMessage, "Wrong mesage is coming");

            }
            else {
                Assert.assertEquals(CODStatus,"true","Amount is less than configured city amount so it should pass");
                Assert.assertEquals(CODMessage, expectedMessage, "Wrong mesage is coming");

            }
                break;
        }

    }



//
//    @AfterClass
//    public void mockServerStop() {
////
////        try {
////            Thread.sleep(1000000000);
////        } catch (InterruptedException e) {
////            e.printStackTrace();
////        }
//
//        wireMockHelper.stopMockServer();
//
//    }

    }
