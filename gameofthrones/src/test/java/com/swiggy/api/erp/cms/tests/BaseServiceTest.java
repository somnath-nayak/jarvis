package com.swiggy.api.erp.cms.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.constants.CmsConstants;
import com.swiggy.api.erp.cms.constants.Constants;
import com.swiggy.api.erp.cms.dp.BaseServiceDP;
import com.swiggy.api.erp.cms.helper.BaseServiceHelper;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import com.swiggy.api.erp.cms.helper.RestaurantHelper;
import com.swiggy.api.erp.cms.constants.Constants;
import framework.gameofthrones.Tyrion.JsonHelper;
import junit.framework.Assert;
import framework.gameofthrones.Cersei.ToolBox;
import framework.gameofthrones.JonSnow.Processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.asserts.SoftAssert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.*;

public class BaseServiceTest extends BaseServiceDP {

    BaseServiceHelper baseServiceHelper = new BaseServiceHelper();
    private static Logger log = LoggerFactory.getLogger(BaseServiceTest.class);


    public int cat_id = baseServiceHelper.createCategoryIdAndReturnInt(restaurantId);
    public int sub_cat_id = baseServiceHelper.createSubCategoryAndReturnInt(restaurantId,cat_id);
    public String item_id, variant_group_id, variant_id, timeStamp, g_third_party_id, g_name;
    public ArrayList<String> g_item_id = new ArrayList<>();
    public ArrayList<String> g_addonGroup_id = new ArrayList<>();
    boolean once = false, flag = false;


    @Test(dataProvider = "createVariantGroupDP", priority = 1,groups = {"BaseService", "aviral"},enabled = true)
    public void createVariantGroup(String order, String third_party) {
        SoftAssert softAssert = new SoftAssert();
        cat_id = baseServiceHelper.createCategoryIdAndReturnInt(restaurantId);
        sub_cat_id = baseServiceHelper.createSubCategoryAndReturnInt(restaurantId,cat_id);
        item_id = baseServiceHelper.createItemIdAndReturnString(restaurantId,cat_id,sub_cat_id);
        //debugger
        log.info("--catID::: " + cat_id + " --subCatID::: "+sub_cat_id+" --itemId::: "+item_id);
        timeStamp = baseServiceHelper.getTimeStamp();
        String name = "AutomationTest_BaseService_"+timeStamp;
        String body = baseServiceHelper.createVariantGroup(item_id,name, order, third_party).ResponseValidator.GetBodyAsText();
        int status = JsonPath.read(body,"$.statusCode");
        String data_field = JsonPath.read(body, "$..data").toString().replace("[","").replace("]","").replace("{","").replace("}","").trim();
        variant_group_id = JsonPath.read(body, "$..data..id").toString().replace("[","").replace("]","").trim();
        String resp_item_Id = JsonPath.read(body, "$..data..item_id").toString().replace("[","").replace("]","").trim();
        String active_state = JsonPath.read(body,"$..data..active").toString().replace("[","").replace("]","").trim();
        String unique_id = JsonPath.read(body,"$..data..uniqueId").toString().replace("[","").replace("]","").replace("\"","").trim();
        //Asserts
        softAssert.assertEquals(Constants.statusOne,status, "Status not Zero");
        softAssert.assertNotNull(data_field);
        softAssert.assertEquals(resp_item_Id,item_id, "Item id not similar");
        softAssert.assertEquals(active_state, "true", "active != true");
        softAssert.assertEquals(unique_id,variant_group_id);
        softAssert.assertAll();
    }

    @Test(dataProvider = "createVariantGroupDP",dependsOnMethods = "createVariantGroup", priority = 2, groups = {"BaseService", "aviral"}, enabled = true)
    public void duplicateCreateVariantGroup(String order, String third_party) {
        SoftAssert softAssert = new SoftAssert();
        //debugger
        log.info("For Duplicate : --catID::: " + cat_id + " --subCatID::: "+sub_cat_id+" --itemId::: "+item_id);
        String name = "AutomationTest_BaseService_"+timeStamp;
        String body = baseServiceHelper.createVariantGroup(item_id,name,order,third_party).ResponseValidator.GetBodyAsText();
        int status = JsonPath.read(body,"$.statusCode");
        String data_field = JsonPath.read(body, "$..data").toString().replace("[","").replace("]","").replace("{","").replace("}","").trim();
        String statusMessage = JsonPath.read(body, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        //debug
        softAssert.assertEquals(status, Constants.statusZero,"Status not zero");
        softAssert.assertEquals(data_field, "null");
        softAssert.assertEquals(statusMessage, "Duplicate 'VariantGroup' with id null and name " + "\'" +name+"\'"+ " found." , "Status message not validated" );
        softAssert.assertAll();
    }

    @Test(dataProvider = "createVariantGroupDP",priority = 3, groups = {"BaseService", "aviral"}, enabled = true)
    public void updateInvalidItemId(String order, String third_party) {
        SoftAssert softAssert = new SoftAssert();
        String name = baseServiceHelper.getName();
        String itemID = "1";
        String body = baseServiceHelper.createVariantGroup(itemID,name,order,third_party).ResponseValidator.GetBodyAsText();
        int status = JsonPath.read(body,"$.statusCode");
        String data_field = JsonPath.read(body, "$..data").toString().replace("[","").replace("]","").replace("{","").replace("}","").trim();
        String statusMessage = JsonPath.read(body, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        String statusMessage_to_validate = "item is not present for item id:"+itemID;
        softAssert.assertEquals(status,Constants.statusZero, "Status not 0");
        softAssert.assertEquals(data_field,"null");
        softAssert.assertEquals(statusMessage,statusMessage_to_validate,"Status message not similar");
        softAssert.assertAll();
    }

    @Test(dataProvider = "negativeCreateVariantGroupDP", priority = 4, groups = {"BaseService", "aviral"},description = "Name=Null & Order = (-)ve & duplication for third_party_id", enabled = true)
    public void  negetiveScenariosCreateVariantGroup(String name, String order, String third_party, String statusMessage_to_validate) throws InterruptedException {
        SoftAssert softAssert = new SoftAssert();
        //debugger
        log.info("For (-)ve Scenarios : --catID::: " + cat_id + " --subCatID::: "+sub_cat_id+" --itemId::: "+item_id);
        String body = baseServiceHelper.createVariantGroup(item_id,name,order,third_party).ResponseValidator.GetBodyAsText();
        int status = JsonPath.read(body,"$.statusCode");
        String statusMessage = JsonPath.read(body, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        String data_field = JsonPath.read(body, "$..data").toString().replace("[","").replace("]","").replace("{","").replace("}","").trim();
        if (data_field.equals("null"))
            softAssert.assertEquals(status,Constants.statusZero, "Status non zero");
        else {
            softAssert.assertEquals(status, Constants.statusOne, "Status not one");
            softAssert.assertNotNull(data_field);
        }

        if (!third_party.equals("null") && !once){
            String third_party_id = JsonPath.read(body,"$..data..third_party_id").toString().replace("[","").replace("]","").replace("\"","").trim();
            softAssert.assertEquals(third_party_id,third_party, "Third Party Id not same");
            softAssert.assertEquals(statusMessage,"null");

        }
        once = true;
        if(statusMessage_to_validate.contains("${NAME}"))
            statusMessage_to_validate = statusMessage_to_validate.replace("${NAME}",name).replace("${THIRD_PARTY}",third_party.replace("\"",""));
        softAssert.assertEquals(statusMessage,statusMessage_to_validate, "Status Message to Validate is not same as Status message");
        softAssert.assertAll();
    }

    @Test(groups = {"BaseService", "aviral"},priority = 5, enabled = true)
    public void getVariantGroupById() {
        SoftAssert softAssert = new SoftAssert();
        String body = baseServiceHelper.getVariantGroupByID(variant_group_id).ResponseValidator.GetBodyAsText();
        //debugger
        log.info("variant_group_id :: "+variant_group_id);
        int status = JsonPath.read(body,"$.statusCode");
        String statusMessage = JsonPath.read(body, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        String data_field = JsonPath.read(body, "$..data").toString().replace("[","").replace("]","").replace("{","").replace("}","").trim();
        String unique_id = JsonPath.read(body,"$..data..uniqueId").toString().replace("[","").replace("]","").replace("\"","").trim();
        String active_state = JsonPath.read(body,"$..data..active").toString().replace("[","").replace("]","").trim();
        String item_id_to_validate = JsonPath.read(body,"$..data..item_id").toString().replace("[","").replace("]","").replace("\"","").trim();
        softAssert.assertEquals(status,Constants.statusOne, "Status not 0");
        softAssert.assertNotNull(data_field);
        softAssert.assertEquals(statusMessage, "null");
        softAssert.assertEquals(unique_id,variant_group_id);
        softAssert.assertEquals(active_state, "true");
        softAssert.assertEquals(item_id_to_validate,item_id);
        softAssert.assertAll();
    }

    @Test(dataProvider = "updateVariantGroupDP",dependsOnMethods = "createVariantGroup", priority = 6, groups = {"BaseService", "aviral"},description = "Update Variant Group", enabled = true)
    public void updateVariantGroup (String q_variant_group_id,String item, String name, String order, String third_party_id,String updated_by, String status_message_validate, String status) {
        SoftAssert softAssert = new SoftAssert();
        log.info("variant_group_id ::: "+variant_group_id);
        String oldName = "AutomationTest_BaseService_"+timeStamp;
        log.info("OLD NAME ::: "+ oldName);
        log.info("NEW NAME ::: "+ name);
        String p_variant_group_id="";
        if (q_variant_group_id.equals("1234")) {
            p_variant_group_id = variant_group_id;
            q_variant_group_id = variant_group_id;
        }
        else {
            p_variant_group_id = variant_group_id;
        }
        if (item.equals("0"))
            item = item_id;
        else
            item = "123";
        String body = baseServiceHelper.updateVariantGroup(p_variant_group_id,q_variant_group_id,item,name,order,third_party_id,updated_by).ResponseValidator.GetBodyAsText();
        int statusCode = JsonPath.read(body,"$.statusCode");
        String statusMessage = JsonPath.read(body, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        String data_field = JsonPath.read(body, "$..data").toString().replace("[","").replace("]","").replace("{","").replace("}","").trim();
        String item_id_to_validate = JsonPath.read(body,"$..data..item_id").toString().replace("[","").replace("]","").replace("\"","").trim();
        String get_new_name = JsonPath.read(body,"$..data..name").toString().replace("[","").replace("]","").replace("\"","").trim();
        String third_party = JsonPath.read(body,"$..data..third_party_id").toString().replace("[","").replace("]","").replace("\"","").trim();
        String updatedby_name = JsonPath.read(body,"$..data..updated_by").toString().replace("[","").replace("]","").replace("\"","").trim();
        String active_state = JsonPath.read(body,"$..data..active").toString().replace("[","").replace("]","").trim();
        String unique_id = JsonPath.read(body,"$..data..uniqueId").toString().replace("[","").replace("]","").replace("\"","").trim();
        String get_id = JsonPath.read(body,"$..data..id").toString().replace("[","").replace("]","").replace("\"","").trim();

        if (status.equals("0")) {
            softAssert.assertEquals(statusCode,Constants.statusZero, "Status not 0");
            softAssert.assertEquals(statusMessage,status_message_validate, "Status message not same");
            softAssert.assertEquals(data_field,"null");
        }
        else if (status.equals("1")){
            softAssert.assertEquals(statusCode, Constants.statusOne, "Status not 1");
            softAssert.assertNotNull(data_field);
            softAssert.assertEquals(item_id_to_validate, item_id);
            softAssert.assertEquals(get_new_name, name, "name did not update");
            softAssert.assertEquals(statusMessage,status_message_validate, "Status message not same");
            softAssert.assertEquals(third_party,third_party_id, "Third_party_Id not same");
            softAssert.assertEquals(updatedby_name,updated_by, "Update_by value not same");
            softAssert.assertEquals(active_state,"true");
            softAssert.assertEquals(unique_id,q_variant_group_id);
            softAssert.assertEquals(get_id,q_variant_group_id);
        }
        softAssert.assertAll();
    }


    @Test(groups = {"BaseService", "aviral"}, priority = 7, dependsOnMethods = "createVariantGroup", enabled = true)
    public void deleteVariantGroup() {
        SoftAssert softAssert = new SoftAssert();
        String body = baseServiceHelper.deleteVariantGroupByID(variant_group_id).ResponseValidator.GetBodyAsText();
        int statusCode = JsonPath.read(body,"$.statusCode");
        String statusMessage = JsonPath.read(body, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        String data_field = JsonPath.read(body, "$..data").toString().replace("[","").replace("]","").replace("{","").replace("}","").trim();
        softAssert.assertEquals(statusCode,Constants.statusOne);
        softAssert.assertEquals(data_field,"null");
        softAssert.assertEquals(statusMessage,"Variant Group deleted successfully");
        //validate using get api if variant group id is deleted
        String body_getApi = baseServiceHelper.getVariantGroupByID(variant_group_id).ResponseValidator.GetBodyAsText();
        String statusMessage_getApi = JsonPath.read(body_getApi, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        int statusCode_getApi = JsonPath.read(body_getApi,"$.statusCode");
        softAssert.assertEquals(statusCode_getApi,Constants.statusZero);
        softAssert.assertEquals(statusMessage_getApi,"Not Found");
        //invalid case
        String invalid_body = baseServiceHelper.deleteVariantGroupByID("123346879").ResponseValidator.GetBodyAsText();
        String inv_statusMessage = JsonPath.read(invalid_body, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        int inv_statusCode = JsonPath.read(invalid_body,"$.statusCode");
        softAssert.assertEquals(inv_statusCode,Constants.statusZero);
        softAssert.assertEquals(inv_statusMessage,"null");
        softAssert.assertAll();
    }

    @Test(dataProvider = "createVariantGroupDP", priority = 8,groups = {"BaseService", "aviral"}, enabled = true)
    public void recreateVariantGroup(String order, String third_party) {
        SoftAssert softAssert = new SoftAssert();
        log.info("OLD VARIANT GROUP ID :::"+variant_group_id);
        createVariantGroup(order, third_party);
        log.info("NEW VARIANT GROUP ID :::"+variant_group_id);
        softAssert.assertAll();
    }

    @Test(dataProvider = "createVariantDP",priority = 9,groups = {"BaseService", "aviral"}, enabled = true)
    public void createVariant(String default_val,String cgst,String igst,String inclusive,String sgst,String in_stock,String is_veg,String name,String order,String price,String third_party,String new_variant_group_id, String status, String message) {
        SoftAssert softAssert = new SoftAssert();
        // recreate a variant group
        if (new_variant_group_id.equals("default_variant_group"))
            new_variant_group_id = variant_group_id;

        String body = baseServiceHelper.createVariant(default_val,cgst,igst,inclusive,sgst,in_stock,is_veg,name,order,price,third_party,new_variant_group_id).ResponseValidator.GetBodyAsText();
        int get_status = JsonPath.read(body,"$.statusCode");
        String get_statusMessage = JsonPath.read(body, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        String get_data_field = JsonPath.read(body, "$..data").toString().replace("[","").replace("]","").replace("{","").replace("}","").trim();
        String get_name = JsonPath.read(body, "$..data..name").toString().replace("[","").replace("]","").replace("\"","").trim();
        String get_third_party_id = JsonPath.read(body, "$..data..third_party_id").toString().replace("[","").replace("]","").replace("\"","").trim();
        String get_variant_group_id = JsonPath.read(body, "$..data..variant_group_id").toString().replace("[","").replace("]","").replace("\"","").trim();
        String get_in_stock = JsonPath.read(body, "$..data..in_stock").toString().replace("[","").replace("]","").replace("\"","").trim();
        String get_is_veg = JsonPath.read(body, "$..data..is_veg").toString().replace("[","").replace("]","").replace("\"","").trim();
        String get_uniqueId = JsonPath.read(body, "$..data..uniqueId").toString().replace("[","").replace("]","").replace("\"","").trim();
        String get_default = JsonPath.read(body, "$..data..default").toString().replace("[","").replace("]","").replace("\"","").trim();

        if (status.equals("0")){
            if (is_veg.equals("-1") && in_stock.equals("-1") && order.equals("-1")) {
                int flag = 0;
                String[] combination_status = {
                        "isVeg - must be greater than or equal to 0, order - must be greater than or equal to 0, inStock - must be greater than or equal to 0",
                        "order - must be greater than or equal to 0, isVeg - must be greater than or equal to 0, inStock - must be greater than or equal to 0",
                        "inStock - must be greater than or equal to 0, order - must be greater than or equal to 0, isVeg - must be greater than or equal to 0",
                        "order - must be greater than or equal to 0, inStock - must be greater than or equal to 0, isVeg - must be greater than or equal to 0",
                        "inStock - must be greater than or equal to 0, isVeg - must be greater than or equal to 0, order - must be greater than or equal to 0",
                        "isVeg - must be greater than or equal to 0, inStock - must be greater than or equal to 0, order - must be greater than or equal to 0"
                };
                for (int i = 0; i < combination_status.length; i++) {
                    if (combination_status[i].equals(get_statusMessage)) {
                        //debugger
                        log.info("--is a match --- "+i+" --- "+combination_status[i]);
                        flag =1;
                        break;
                    }
                }
                softAssert.assertEquals(flag,1, "could not find combination_status so flag is 0");
            }
            else if (message.contains("${VARIANTID}")) {
                message = message.replace("${VARIANTID}",variant_id);
                log.info("Message: "+message);
                softAssert.assertEquals(get_statusMessage,message);
            }
            else
                softAssert.assertEquals(get_statusMessage,message);
            softAssert.assertEquals(get_status,Constants.statusZero);
            softAssert.assertEquals(get_data_field,"null");
        }
        else if (status.equals("1")) {
            variant_id = get_uniqueId;
            g_third_party_id = get_third_party_id;
            g_name = get_name;
            log.info("Variant_Unique_Id ::: "+variant_id);
            softAssert.assertEquals(get_status,Constants.statusOne);
            softAssert.assertEquals(get_statusMessage,message);
            softAssert.assertEquals(get_name,name , "Name different");
            softAssert.assertEquals(get_third_party_id,third_party ,"Third party Id different");
            softAssert.assertEquals(get_variant_group_id, new_variant_group_id, "variant group id different");
            softAssert.assertEquals(get_in_stock,in_stock,"in_stock different");
            softAssert.assertEquals(get_is_veg,is_veg, "is_veg different");
            softAssert.assertEquals(get_default,default_val, "default different");

        }
        softAssert.assertAll();
    }

    @Test(priority = 10,groups = {"BaseService", "aviral"}, enabled = true)
    public void getVariantById() {
        SoftAssert softAssert = new SoftAssert();
        //create variant with default values
        String create_name = "Automation_baseService"+baseServiceHelper.getTimeStampRandomised();
        String create_thirdParty = "Automation_"+baseServiceHelper.getTimeStampRandomised();
        String bodyVariant = baseServiceHelper.createVariant("1","[VARIANT_PRICE]*0.2","[VARIANT_PRICE]*0.2","false","[VARIANT_PRICE]*0.2","0","0",create_name,"0","100",create_thirdParty,variant_group_id).ResponseValidator.GetBodyAsText();
        String variant_id= JsonPath.read(bodyVariant, "$..data..uniqueId").toString().replace("[","").replace("]","").replace("\"","").trim();
        log.info("variant_id ::: "+variant_id);
        String body = baseServiceHelper.getVariantByID(variant_id).ResponseValidator.GetBodyAsText();
        int status = JsonPath.read(body,"$.statusCode");
        String statusMessage = JsonPath.read(body, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        String data_field = JsonPath.read(body, "$..data").toString().replace("[","").replace("]","").replace("{","").replace("}","").trim();
        String unique_id = JsonPath.read(body,"$..data..uniqueId").toString().replace("[","").replace("]","").replace("\"","").trim();
        String active_state = JsonPath.read(body,"$..data..active").toString().replace("[","").replace("]","").trim();
        String id = JsonPath.read(body,"$..data..id").toString().replace("[","").replace("]","").replace("\"","").trim();
        String name = JsonPath.read(body, "$..data..name").toString().replace("[","").replace("]","").replace("\"","").trim();
        String third_party_id = JsonPath.read(body, "$..data..third_party_id").toString().replace("[","").replace("]","").replace("\"","").trim();
        softAssert.assertEquals(status,Constants.statusOne, "Status not 1");
        softAssert.assertNotNull(data_field);
        softAssert.assertEquals(statusMessage, "null");
        softAssert.assertEquals(unique_id,variant_id);
        softAssert.assertEquals(active_state, "true");
        softAssert.assertEquals(id,variant_id);
        softAssert.assertEquals(name,create_name.replace("\"",""));
        softAssert.assertEquals(third_party_id,create_thirdParty.replace("\"",""));
        //invalid case
        String body_invalid = baseServiceHelper.getVariantByID("11").ResponseValidator.GetBodyAsText();
        int invalid_status = JsonPath.read(body_invalid,"$.statusCode");
        String invalid_statusMessage = JsonPath.read(body_invalid, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        String invalid_data_field = JsonPath.read(body_invalid, "$..data").toString().replace("[","").replace("]","").replace("{","").replace("}","").trim();
        softAssert.assertEquals(invalid_status,Constants.statusZero, "Status not 0");
        softAssert.assertEquals(invalid_data_field,"null");
        softAssert.assertEquals(invalid_statusMessage, "Not Found");
        softAssert.assertAll();
    }


    @Test(dataProvider = "updateVariantDP",priority = 11,groups = {"BaseService", "aviral"}, enabled = true)
    public void updateVariant(String p_variant_id, String default_val,String cgst,String igst,String inclusive,String sgst,String q_variant_id,String in_stock,String is_veg,String name,String order,String price,String third_party,String updated_by, String variant_group , String status, String status_message) {
        SoftAssert softAssert = new SoftAssert();
        String create_name = "Automation_" + baseServiceHelper.getTimeStampRandomised();
        String bodyVariant = baseServiceHelper.createVariant("1", cgst, igst, "false", sgst, "0", "0", create_name, "0", "100", "", variant_group_id).ResponseValidator.GetBodyAsText();
        String variant_id = JsonPath.read(bodyVariant, "$..data..uniqueId").toString().replace("[", "").replace("]", "").replace("\"", "").trim();
        String variant_name = JsonPath.read(bodyVariant, "$..data..name").toString().replace("[", "").replace("]", "").trim();
        String variant_tpi = JsonPath.read(bodyVariant, "$..data..third_party_id").toString().replace("[", "").replace("]", "").replace("\"", "").trim();
        log.info("variant_group_id ::: " + variant_group_id);
        log.info("Variant ID ::: " + variant_id);
        flag = true;
        //String p_variant_id="";
        if (q_variant_id.equals("default_variant_id"))
            q_variant_id = variant_id;

        if (p_variant_id.equals("default_variant_id"))
            p_variant_id = variant_id;

        if (variant_group.equals("default_variant_group"))
            variant_group = variant_group_id;

        if(status_message.contains("${VARIANTID}"))
            status_message = status_message.replace("${VARIANTID}",variant_id);

        String body = baseServiceHelper.updateVariant(q_variant_id,default_val,cgst,igst,inclusive,sgst,p_variant_id,in_stock,is_veg,name,order,price,third_party.replace("\"",""),updated_by,variant_group).ResponseValidator.GetBodyAsText();
        int get_status = JsonPath.read(body,"$.statusCode");
        String get_statusMessage = JsonPath.read(body, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        String get_data_field = JsonPath.read(body, "$..data").toString().replace("[","").replace("]","").replace("{","").replace("}","").trim();
        String get_name = JsonPath.read(body, "$..data.name").toString().replace("[","").replace("]","").trim();
        String get_third_party_id = JsonPath.read(body, "$..data..third_party_id").toString().replace("[","").replace("]","").replace("\"","").trim();
        String get_variant_group_id = JsonPath.read(body, "$..data..variant_group_id").toString().replace("[","").replace("]","").replace("\"","").trim();
        String get_in_stock = JsonPath.read(body, "$..data..in_stock").toString().replace("[","").replace("]","").replace("\"","").trim();
        String get_is_veg = JsonPath.read(body, "$..data..is_veg").toString().replace("[","").replace("]","").replace("\"","").trim();
        String get_uniqueId = JsonPath.read(body, "$..data..uniqueId").toString().replace("[","").replace("]","").replace("\"","").trim();
        String get_default = JsonPath.read(body, "$..data..default").toString().replace("[","").replace("]","").replace("\"","").trim();
        String get_updatedby = JsonPath.read(body,"$..data..updated_by").toString().replace("[","").replace("]","").replace("\"","").trim();
        String get_id = JsonPath.read(body,"$..data..id").toString().replace("[","").replace("]","").replace("\"","").trim();

        if (status.equals("0")) {
            softAssert.assertEquals(get_status,Constants.statusZero, "Status not 0");
            softAssert.assertEquals(get_statusMessage,status_message, "Status message not same");
            softAssert.assertEquals(get_data_field,"null");
        }
        else if (status.equals("1")) {
            softAssert.assertEquals(get_status, Constants.statusOne);
            softAssert.assertEquals(get_statusMessage, status_message);

            if (name.equals("\"\"") || name.equals("\"null\"") || name.equals("null"))
                softAssert.assertEquals(get_name,variant_name,"Variant Name issue");
            else {
                g_name = name;
                softAssert.assertEquals(get_name, name);
            }
            if (third_party.equals("") || third_party.equals("\"null\"") || third_party.equals("null"))
                softAssert.assertEquals(get_third_party_id,"","invalid third party id");
            else {
                g_third_party_id = third_party;
                softAssert.assertEquals(get_third_party_id, third_party);
            }

            softAssert.assertEquals(get_is_veg,is_veg, "is_veg different");
            softAssert.assertEquals(get_default,default_val, "default different");
            softAssert.assertEquals(get_in_stock,in_stock,"in_stock different");
            softAssert.assertEquals(get_uniqueId,q_variant_id);
            softAssert.assertEquals(get_id,q_variant_id);
            softAssert.assertEquals(get_variant_group_id,variant_group);
            softAssert.assertEquals(get_updatedby,updated_by);
        }
        softAssert.assertAll();
    }

    @Test(dataProvider = "searchVariantDP",priority = 12,groups = {"BaseService", "aviral"}, enabled = true)
    public void searchVariant(String variantName, String status) {
        SoftAssert softAssert = new SoftAssert();
        if (variantName.equals("global_name"))
            variantName = g_name;
        variantName = variantName.replace("\"","");
        log.info("Global Variant Name ::: "+g_name);
        log.info("Variant Name ::: "+variantName);
        String body = baseServiceHelper.searchVariant(variantName).ResponseValidator.GetBodyAsText();
        int get_status = JsonPath.read(body,"$.statusCode");
        String get_statusMessage = JsonPath.read(body, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        String get_pageable = JsonPath.read(body, "$..data..pageable").toString().replace("[","").replace("]","").replace("\"","").trim();
        String get_search_data = JsonPath.read(body, "$..data..search_data[*]").toString().replace("[","").replace("]","").replace("\"","").trim();
        String get_pageable_page = JsonPath.read(body, "$..data..pageable..page").toString().replace("[","").replace("]","").replace("\"","").trim();
        String get_pageable_size = JsonPath.read(body, "$..data..pageable..page").toString().replace("[","").replace("]","").replace("\"","").trim();
        String[] get_search_data_id = JsonPath.read(body, "$..data..search_data[*]..id").toString().replace("[","").replace("]","").replace("\"","").trim().split(",");

        if (variantName.equals(null) || variantName.equals("\"\"")) {
            softAssert.assertEquals(get_status,Constants.statusOne);
            softAssert.assertEquals(get_pageable, "null");
            softAssert.assertEquals(get_search_data,"","pageable not empty");
            softAssert.assertEquals(get_statusMessage,"null");
        }
        else if (variantName.equals("") || variantName.equals("Automation")) {
            int search_data_len = get_search_data_id.length;
            softAssert.assertEquals(get_status, Constants.statusOne);
            softAssert.assertEquals(get_statusMessage,"null");
            softAssert.assertNotNull(get_pageable);
            softAssert.assertNotNull(get_search_data);
            softAssert.assertNotNull(get_pageable_size);
            softAssert.assertEquals(get_pageable_page, "1", "pageable page not 1");
            softAssert.assertTrue((search_data_len >= 1),"search data length not greater than 1");
        }
        softAssert.assertAll();
    }

    @Test(priority = 13,groups = {"BaseService", "aviral"}, timeOut=2500, enabled = true)
    public void deleteVariant() {
        SoftAssert softAssert = new SoftAssert();
        String body = baseServiceHelper.deleteVariantHelper(variant_id).ResponseValidator.GetBodyAsText();
        int statusCode = JsonPath.read(body,"$.statusCode");
        String statusMessage = JsonPath.read(body, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        String data_field = JsonPath.read(body, "$..data").toString().replace("[","").replace("]","").replace("{","").replace("}","").trim();
        softAssert.assertEquals(statusCode,Constants.statusOne);
        softAssert.assertEquals(data_field,"null");
        //softAssert.assertEquals(statusMessage,"Variant deleted successfully");
        //validate using get api if variant group id is deleted
        String body_getApi = baseServiceHelper.getVariantByID(variant_id).ResponseValidator.GetBodyAsText();
        String statusMessage_getApi = JsonPath.read(body_getApi, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        int statusCode_getApi = JsonPath.read(body_getApi,"$.statusCode");
        softAssert.assertEquals(statusCode_getApi,Constants.statusZero);
        softAssert.assertEquals(statusMessage_getApi,"Not Found");
        //invalid case
        String invalid_body = baseServiceHelper.deleteVariantHelper("123446879").ResponseValidator.GetBodyAsText();
        String inv_statusMessage = JsonPath.read(invalid_body, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        int inv_statusCode = JsonPath.read(invalid_body,"$.statusCode");
        softAssert.assertEquals(inv_statusCode,Constants.statusZero);
        softAssert.assertEquals(inv_statusMessage,"null");
        softAssert.assertAll();
    }


    @Test(dataProvider = "createAddonGroupDP",priority = 14,groups = {"BaseService", "aviral"}, enabled = true)
    public void createAddonGroup (ArrayList<String> arrayList, String message_to_validate, String statusCode) {
        SoftAssert softAssert = new SoftAssert();
        log.info("ARRAY ---> "+arrayList);
        String[] payload = arrayList.toArray(new String[0]);
        if(payload[4].equals("${ITEMID}") && payload[1].equals("-1") && payload[2].equals("-1")) {
            payload = createAddonGroupAndAddItemIdHelper(payload,"50","50");
        }
        else if (payload[4].equals("${ITEMID}") && payload[2].equals("50") && payload[1].equals("-1")) {
            payload = createAddonGroupAndAddItemIdHelper(payload,"50","50");
        }
        else if (payload[4].equals("${ITEMID}") && payload[2].equals("-1") && payload[1].equals("50")) {
            payload = createAddonGroupAndAddItemIdHelper(payload,"50","50");
        }
        else if (payload[4].equals("${ITEMID}") && payload[2].equals("-1") && payload[1].equals("50")) {
            payload = createAddonGroupAndAddItemIdHelper(payload,"50","50");
        }
        else if (payload[4].equals("${ITEMID_LEVEL}")) {
            payload = createAddonGroupAndAddItemIdHelper(payload,"50","-1");
        }
        else if (payload[4].equals("${ITEMID_LEVEL}") && payload[2].equals("-1") && payload[1].equals("50")) {
            payload = createAddonGroupAndAddItemIdHelper(payload,"-1","50");
        }

        String body = baseServiceHelper.createAddonGroupHelper(payload).ResponseValidator.GetBodyAsText();
        int get_statusCode = JsonPath.read(body, "$.statusCode");
        String get_statusMessage = JsonPath.read(body, "$..statusMessage").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_data_field = JsonPath.read(body, "$..data").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_addon_id = JsonPath.read(body, "$..data..id").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_addon_unique_id = JsonPath.read(body, "$..data..uniqueId").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_item_id = JsonPath.read(body, "$..data..item_id").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_name = JsonPath.read(body, "$..data..name").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_addon_limit = JsonPath.read(body, "$..data..addon_limit").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_addon_free_limit = JsonPath.read(body, "$..data..addon_free_limit").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_addon_min_limit = JsonPath.read(body, "$..data..addon_min_limit").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();

        if(!get_addon_id.equals("") && !get_addon_id.equals("0") && !get_addon_id.equals("null") && !get_addon_id.equals(" ")) {
            g_addonGroup_id.add(get_addon_id);
            log.info("G_AddonGroup_ID ----> "+g_addonGroup_id);
        }

        if (statusCode.equals("0")) {
            softAssert.assertEquals(get_statusCode, Constants.statusZero, "Status not 0");
            softAssert.assertEquals(get_data_field, "null");
            softAssert.assertEquals(get_statusMessage, message_to_validate, "Status message issue");
        } else if (statusCode.equals("1")) {
            softAssert.assertEquals(get_statusCode, Constants.statusOne, "Status not 1");
            softAssert.assertNotEquals(get_data_field, "null");
            softAssert.assertEquals(get_statusMessage, message_to_validate, "Status message issue");
            softAssert.assertNotNull(get_addon_id);
            softAssert.assertEquals(get_name, payload[5]);
            softAssert.assertEquals(get_addon_id,get_addon_unique_id);
            softAssert.assertEquals(get_item_id,payload[4]);
            softAssert.assertEquals(get_addon_limit,payload[2]);
            softAssert.assertEquals(get_addon_free_limit,payload[1]);
            softAssert.assertEquals(get_addon_min_limit,payload[3]);
        }
        softAssert.assertAll();
    }

    @Test(dataProvider = "updateAddonGroupDP",priority = 15,groups = {"BaseService", "aviral"}, enabled = true)
    public void updateAddonGroup (ArrayList<String> arrayList, String message_to_validate, String statusCode) {
        SoftAssert softAssert = new SoftAssert();
        log.info("ARRAY ---> "+arrayList);
        String[] payload = arrayList.toArray(new String[0]);
        String addonGroupID = g_addonGroup_id.get((g_addonGroup_id.size()-1));
        if(payload[4].equals("${ITEMID}") && payload[1].equals("10") && payload[2].equals("10")) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("addon_free_limit", "5");
            hm.put("addon_limit", "5");
            String[] tmp_payload = createSingleItemDP(hm);
            String body = baseServiceHelper.createItemHelper(tmp_payload).ResponseValidator.GetBodyAsText();
            String get_itemId = JsonPath.read(body, "$..data..id").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
            payload[4] = get_itemId;
            String bodyAddon = baseServiceHelper.createAddonGroupHelper(payload).ResponseValidator.GetBodyAsText();
            String get_addon_id = JsonPath.read(bodyAddon, "$..data..id").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
            addonGroupID = get_addon_id;
        }

        else if(payload[4].equals("${INVALIDID}")) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("addon_free_limit", "5");
            String[] tmp_payload = createSingleItemDP(hm);
            String body = baseServiceHelper.createItemHelper(tmp_payload).ResponseValidator.GetBodyAsText();
            String get_itemId = JsonPath.read(body, "$..data..id").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
            payload[4] = get_itemId;
            addonGroupID = "123456879";
        }

        else if(payload[4].equals("${ITEMID}") && payload[2].equals("10")) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("addon_free_limit", "5");
            String[] tmp_payload = createSingleItemDP(hm);
            String body = baseServiceHelper.createItemHelper(tmp_payload).ResponseValidator.GetBodyAsText();
            String get_itemId = JsonPath.read(body, "$..data..id").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
            payload[4] = get_itemId;
            String bodyAddon = baseServiceHelper.createAddonGroupHelper(payload).ResponseValidator.GetBodyAsText();
            String get_addon_id = JsonPath.read(bodyAddon, "$..data..id").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
            addonGroupID = get_addon_id;
        }

        String body = baseServiceHelper.updateAddonGroupHelper(payload, addonGroupID).ResponseValidator.GetBodyAsText();
        int get_statusCode = JsonPath.read(body, "$.statusCode");
        String get_statusMessage = JsonPath.read(body, "$..statusMessage").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_data_field = JsonPath.read(body, "$..data").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_addon_id = JsonPath.read(body, "$..data..id").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_addon_unique_id = JsonPath.read(body, "$..data..uniqueId").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_item_id = JsonPath.read(body, "$..data..item_id").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_name = JsonPath.read(body, "$..data..name").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_addon_limit = JsonPath.read(body, "$..data..addon_limit").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_addon_free_limit = JsonPath.read(body, "$..data..addon_free_limit").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_addon_min_limit = JsonPath.read(body, "$..data..addon_min_limit").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        if (statusCode.equals("0")) {
            softAssert.assertEquals(get_statusCode, Constants.statusZero, "Status not 0");
            softAssert.assertEquals(get_data_field, "null");
            softAssert.assertEquals(get_statusMessage, message_to_validate, "Status message issue");
        } else if (statusCode.equals("1")) {
            softAssert.assertEquals(get_statusCode, Constants.statusOne, "Status not 1");
            softAssert.assertNotEquals(get_data_field, "null");
            softAssert.assertEquals(get_statusMessage, message_to_validate, "Status message issue");
            softAssert.assertNotNull(get_addon_id);
            softAssert.assertEquals(get_name, payload[5]);
            softAssert.assertEquals(get_addon_id,get_addon_unique_id);
            softAssert.assertEquals(get_item_id,payload[4]);
            softAssert.assertEquals(get_addon_limit,payload[2]);
            softAssert.assertEquals(get_addon_free_limit,payload[1]);
            softAssert.assertEquals(get_addon_min_limit,payload[3]);
        }
        softAssert.assertAll();
    }

    @Test(dataProvider = "getAddonGroupByIdDP",priority = 16,groups = {"BaseService", "aviral"}, enabled = true)
    public void getAddonGroupById (String addon_group_id) {
        SoftAssert softAssert = new SoftAssert();
        log.info("ADDON_GROUP_ID ::: "+g_addonGroup_id.get((g_addonGroup_id.size()-1)));
        if (addon_group_id.equals("${VALID_ADDONGROUP}"))
            addon_group_id = g_addonGroup_id.get((g_addonGroup_id.size()-1));
        String body = baseServiceHelper.getAddonGroupByIdHelper(addon_group_id).ResponseValidator.GetBodyAsText();
        int status = JsonPath.read(body,"$.statusCode");
        String statusMessage = JsonPath.read(body, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        String data_field = JsonPath.read(body, "$..data").toString().replace("[","").replace("]","").replace("{","").replace("}","").trim();
        String unique_id = JsonPath.read(body,"$..data..uniqueId").toString().replace("[","").replace("]","").replace("\"","").trim();
        String active_state = JsonPath.read(body,"$..data..active").toString().replace("[","").replace("]","").trim();
        String id = JsonPath.read(body,"$..data..id").toString().replace("[","").replace("]","").replace("\"","").trim();
        if (status == 1) {
            softAssert.assertEquals(status, Constants.statusOne);
            if (data_field.equals("null")) {
                softAssert.assertEquals(statusMessage, "null");
            } else {
                softAssert.assertEquals(statusMessage, "null");
                softAssert.assertEquals(unique_id, id);
                softAssert.assertEquals(id, addon_group_id);
                softAssert.assertEquals(active_state, "true");
            }
        }
        else if (status == 0) {
            softAssert.assertEquals(status, Constants.statusZero);
            softAssert.assertEquals(statusMessage, "Not Found");
            softAssert.assertEquals(data_field,"null");
        }
        softAssert.assertAll();
    }

    @Test(dataProvider = "searchAddonGroupDP",priority = 17,groups = {"BaseService", "aviral"}, enabled = true)
    public void searchAddonGroupByName (String name, String third_party, String itemID, Boolean flag) {
        SoftAssert softAssert = new SoftAssert();
        log.info(" NAME of Addon Created ::: "+name);
        log.info(" THIRD_PARTY_ID of Addon Created ::: "+third_party);
        String body = baseServiceHelper.searchAddonGroupByName(name).ResponseValidator.GetBodyAsText();
        int get_status = JsonPath.read(body,"$.statusCode");
        String get_statusMessage = JsonPath.read(body, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        String get_pageable = JsonPath.read(body, "$..data..pageable").toString().replace("[","").replace("]","").replace("\"","").trim();
        String get_search_data = JsonPath.read(body, "$..data..search_data[*]").toString().replace("[","").replace("]","").replace("\"","").trim();
        String get_name = JsonPath.read(body, "$..data..search_data[0].name").toString().replace("[","").replace("]","").replace("\"","").trim();
       //String get_name = JsonPath.read(body, "$..data..search_data[*]..name").toString().replace("[","").replace("]","").replace("\"","").trim();
        String get_third_part = JsonPath.read(body, "$..data..search_data[*]..third_party_id").toString().replace("[","").replace("]","").replace("\"","").trim();
        String get_item = JsonPath.read(body, "$..data..search_data[0].id").toString().replace("[","").replace("]","").replace("\"","").trim();
        //String get_item = JsonPath.read(body, "$..data..search_data[*]..item_id").toString().replace("[","").replace("]","").replace("\"","").trim();
        String get_pageable_page = JsonPath.read(body, "$..data..pageable..page").toString().replace("[","").replace("]","").replace("\"","").trim();
        String get_pageable_size = JsonPath.read(body, "$..data..pageable..page").toString().replace("[","").replace("]","").replace("\"","").trim();
        String[] get_search_data_id = JsonPath.read(body, "$..data..search_data[*]..id").toString().replace("[","").replace("]","").replace("\"","").trim().split(",");

//$..data..search_data[0].name
        softAssert.assertEquals(get_status,Constants.statusOne);
        softAssert.assertEquals(get_statusMessage,"null");
        if(!flag){
            softAssert.assertEquals(get_pageable,"null");
            softAssert.assertEquals(get_search_data,"");
        }
        else if (flag && !name.equals("null") && !name.equals("")) {
            softAssert.assertEquals(get_name,name);
            //softAssert.assertEquals(get_third_part,third_party);
            softAssert.assertEquals(get_item,itemID);
        }
        else if(flag && (name.equals("null") || name.equals(""))){
            int search_data_len = get_search_data_id.length;
            softAssert.assertNotNull(get_pageable);
            softAssert.assertNotNull(get_search_data);
            softAssert.assertNotNull(get_pageable_size);
            softAssert.assertTrue((search_data_len >= 1),"search data length not greater than 1");

        }
        softAssert.assertAll();
    }

    @Test(priority = 18,groups = {"BaseService", "aviral"}, enabled = true)
    public void deleteAddonGroup() {
        SoftAssert softAssert = new SoftAssert();
        HashMap<String, String> hm = new HashMap<>();
        String itemID = baseServiceHelper.createItemIdAndReturnString(restaurantId,cat_id,sub_cat_id);
        hm.put("name","AddonToBeDeleted"+timestampRandomiser());
        hm.put("item_id",itemID);
        String[] tmp_payload = createSingleAddonGroup(hm);
        String bodyCreateAddonGroup = baseServiceHelper.createAddonGroupHelper(tmp_payload).ResponseValidator.GetBodyAsText();
        String addon_id = JsonPath.read(bodyCreateAddonGroup, "$..data..id").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        int status = JsonPath.read(bodyCreateAddonGroup,"$.statusCode");
        softAssert.assertEquals(status,Constants.statusOne, "createAddonGroup status is not 1");
        String body = baseServiceHelper.deleteAddonGroupHelper(addon_id).ResponseValidator.GetBodyAsText();
        String statusMessage = JsonPath.read(body, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        int statusCode = JsonPath.read(body,"$.statusCode");
        softAssert.assertEquals(statusCode,Constants.statusOne);
        softAssert.assertEquals(statusMessage,"Addon Group deleted Successfully");
        //validate
        String body_getApi = baseServiceHelper.getAddonGroupByIdHelper(addon_id).ResponseValidator.GetBodyAsText();
        String statusMessage_getApi = JsonPath.read(body_getApi, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        int statusCode_getApi = JsonPath.read(body_getApi,"$.statusCode");
        softAssert.assertEquals(statusCode_getApi,Constants.statusOne);
        softAssert.assertEquals(statusMessage_getApi,"null");
        //invalid case
        String invalid_body = baseServiceHelper.deleteAddonGroupHelper("123456879").ResponseValidator.GetBodyAsText();
        String inv_statusMessage = JsonPath.read(invalid_body, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        int inv_statusCode = JsonPath.read(invalid_body,"$.statusCode");
        softAssert.assertEquals(inv_statusCode,Constants.statusZero);
        softAssert.assertEquals(inv_statusMessage,"null");
        softAssert.assertAll();
    }

    @Test(dataProvider = "createAddonDP",priority = 19,groups = {"BaseService", "aviral"}, enabled = true)
    public void createAddon (String json, int[] addongroupid, String message_to_validate, String statusCode) {
        SoftAssert softAssert = new SoftAssert();
        log.info("JSON ---> "+json);
        int[] addonGroup = new int[addongroupid.length];
        String body = baseServiceHelper.createAddonHelper(json).ResponseValidator.GetBodyAsText();
        int get_statusCode = JsonPath.read(body,"$.statusCode");
        String get_statusMessage = JsonPath.read(body,"$..statusMessage").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String data_field = JsonPath.read(body, "$..data").toString().replace("[","").replace("]","").replace("{","").replace("}","").trim();
        String get_addon_id = JsonPath.read(body,"$..data..id").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_unique_id = JsonPath.read(body,"$..data..uniqueId").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_addon_group_ids = JsonPath.read(body,"$..data..addon_group_ids[*]").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();

        if(statusCode.equals("0")) {
            softAssert.assertEquals(get_statusCode, Constants.statusZero);
            softAssert.assertEquals(get_statusMessage, message_to_validate);
            softAssert.assertEquals(data_field, "null");
        }
        else if (statusCode.equals("1")) {
            softAssert.assertEquals(get_statusCode, Constants.statusOne);
            softAssert.assertEquals(get_statusMessage, message_to_validate);
            softAssert.assertEquals(get_addon_id,get_unique_id);
            softAssert.assertEquals(addonGroup.length,addongroupid.length,"No. of addon Group IDs associated are incorrect");
            //debug
            log.info("----> "+get_addon_group_ids);
            String[] addon_groupIds_to_Validate = get_addon_group_ids.split(",");
            for(int i = 0; i < addon_groupIds_to_Validate.length; i++){
                addonGroup[i] = Integer.parseInt(addon_groupIds_to_Validate[i]);
            }
            Arrays.sort(addongroupid);
            for (int agID:addonGroup) {
                log.info("ADDONGROUPID :::" +agID);
                softAssert.assertTrue((Arrays.binarySearch(addongroupid,agID)>=0),"AddonGroupID not found");
            }
        }
        softAssert.assertAll();
    }

    @Test(dataProvider = "updateAddonDP",priority = 20,groups = {"BaseService", "aviral"}, enabled = true)
    public void updateAddon (String json, String addonID, String message_to_validate, String statusCode) {
        SoftAssert softAssert = new SoftAssert();
        log.info("JSON ---> "+json);
        String body = baseServiceHelper.updateAddonHelper(json,addonID).ResponseValidator.GetBodyAsText();
        int get_statusCode = JsonPath.read(body,"$.statusCode");
        String get_statusMessage = JsonPath.read(body,"$..statusMessage").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String data_field = JsonPath.read(body, "$..data").toString().replace("[","").replace("]","").replace("{","").replace("}","").trim();
        String get_addon_id = JsonPath.read(body,"$..data..id").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_unique_id = JsonPath.read(body,"$..data..uniqueId").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_addon_group_ids = JsonPath.read(body,"$..data..addon_group_ids[*]").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        if(statusCode.equals("0")) {
            softAssert.assertEquals(get_statusCode, Constants.statusZero);
            softAssert.assertEquals(get_statusMessage, message_to_validate);
            softAssert.assertEquals(data_field, "null");
        }
        else if (statusCode.equals("1")) {
            softAssert.assertEquals(get_statusCode, Constants.statusOne);
            softAssert.assertEquals(get_statusMessage, message_to_validate);
            softAssert.assertEquals(get_addon_id, get_unique_id);
        }
        softAssert.assertAll();
    }

    @Test(dataProvider = "getAddonByIdDP",priority = 21,groups = {"BaseService", "aviral"}, enabled = true)
    public void getAddonById (String addon_id, String message_to_validate, String statusCode) {
        SoftAssert softAssert = new SoftAssert();
        String body = baseServiceHelper.getAddonByIdHelper(addon_id).ResponseValidator.GetBodyAsText();
        int get_statusCode = JsonPath.read(body,"$.statusCode");
        String get_statusMessage = JsonPath.read(body, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        String data_field = JsonPath.read(body, "$..data").toString().replace("[","").replace("]","").replace("{","").replace("}","").trim();
        String get_addon_id = JsonPath.read(body, "$..data..id").toString().replace("[","").replace("]","").replace("{","").replace("}","").trim();
        String get_pageable = JsonPath.read(body, "$..data..pageable").toString().replace("[","").replace("]","").replace("\"","").trim();
        String get_search_data = JsonPath.read(body, "$..data..search_data[*]").toString().replace("[","").replace("]","").replace("\"","").trim();
        log.info("ADDON ID TO FIND : "+addon_id);
        if(statusCode.equals("0")) {
            softAssert.assertEquals(get_statusCode, Constants.statusZero);
            softAssert.assertEquals(get_statusMessage, message_to_validate);
            softAssert.assertEquals(data_field, "null");
        }
        else if (statusCode.equals("1")) {
            if(addon_id.equals("123456879")){
                softAssert.assertEquals(data_field, "null");
            }
            else
                softAssert.assertEquals(get_addon_id, addon_id);
            softAssert.assertEquals(get_statusCode, Constants.statusOne);
            softAssert.assertEquals(get_statusMessage, message_to_validate);

        }
        softAssert.assertAll();
    }

    @Test(dataProvider = "searchAddonDP",priority = 22,groups = {"BaseService", "aviral"}, enabled = true)
    public void searchAddonByName (String addon_id, String name, String addon_group_id, String message_to_validate, boolean flag) {
        //TODO: TO BE FIXED ONCE BUGS ARE RESOLVED
        SoftAssert softAssert = new SoftAssert();
        log.info(" NAME of Addon To Validate ::: " + name);
        //log.info(" THIRD_PARTY_ID of Addon Created ::: " + third_party);

        String body = baseServiceHelper.searchAddonByName(name).ResponseValidator.GetBodyAsText();
        int get_statusCode = JsonPath.read(body,"$.statusCode");
        String get_statusMessage = JsonPath.read(body,"$..statusMessage").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String data_field = JsonPath.read(body, "$..data").toString().replace("[","").replace("]","").replace("{","").replace("}","").trim();
        String[] get_addon_id = JsonPath.read(body, "$..search_data[*]..id").toString().replace("[","").replace("]","").replace("{","").replace("}","").trim().split(",");
        String[] get_name = JsonPath.read(body, "$..data..search_data[*]..name").toString().replace("[","").replace("]","").replace("\"","").trim().split(",");
        String get_pageable_size = JsonPath.read(body, "$..data..pageable..page").toString().replace("[","").replace("]","").replace("\"","").trim();
        String[] get_search_data_id = JsonPath.read(body, "$..data..search_data[*]..id").toString().replace("[","").replace("]","").replace("\"","").trim().split(",");
        String get_pageable = JsonPath.read(body, "$..data..pageable").toString().replace("[","").replace("]","").replace("\"","").trim();
        String get_search_data = JsonPath.read(body, "$..data..search_data[*]").toString().replace("[","").replace("]","").replace("\"","").trim();
        //TODO: Fix once bugs are resolved
        softAssert.assertEquals(get_statusCode,Constants.statusOne);
        softAssert.assertEquals(get_statusMessage,message_to_validate);
        //softAssert.assertEquals(data_field,"null");
        if(!flag){
            softAssert.assertEquals(get_pageable,"null");
            softAssert.assertEquals(get_search_data,"");
        }
        else if (flag && !name.equals("null") && !name.equals("")) {
            softAssert.assertTrue((get_name.length>=1));
            softAssert.assertTrue((get_addon_id.length>=1));

        }
        else if(flag && (name.equals("null") || name.equals(""))){
            int search_data_len = get_search_data_id.length;
            softAssert.assertNotNull(get_pageable);
            softAssert.assertNotNull(get_search_data);
            softAssert.assertNotNull(get_pageable_size);
            softAssert.assertTrue((search_data_len >= 1),"search data length not greater than 1");

        }
        softAssert.assertAll();
    }

    @Test(priority = 23,groups = {"BaseService", "aviral"}, enabled = true)
    public void deleteAddon() throws IOException {
        //TODO: TO BE FIXED ONCE BUGS ARE RESOLVED
        SoftAssert softAssert = new SoftAssert();
        HashMap<String, String> hm = new HashMap<>();
        String itemID = baseServiceHelper.createItemIdAndReturnString(restaurantId,cat_id,sub_cat_id);
        hm.put("name","AddonToBeDeleted"+timestampRandomiser());
        hm.put("item_id",itemID);
        String[] tmp_payload = createSingleAddonGroup(hm);
        String bodyCreateAddonGroup = baseServiceHelper.createAddonGroupHelper(tmp_payload).ResponseValidator.GetBodyAsText();
        String addon_group_id = JsonPath.read(bodyCreateAddonGroup, "$..data..id").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        int status = JsonPath.read(bodyCreateAddonGroup,"$.statusCode");
        softAssert.assertEquals(status,Constants.statusOne, "createAddonGroup status is not 1");
        String addon_id = createSingleAddon(addon_group_id);
        String body = baseServiceHelper.deleteAddonHelper(addon_id).ResponseValidator.GetBodyAsText();
        String statusMessage = JsonPath.read(body, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        int statusCode = JsonPath.read(body,"$.statusCode");
        softAssert.assertEquals(statusCode,Constants.statusOne);
        softAssert.assertEquals(statusMessage,"Addon has been deleted successfully");
        //validate
        String body_getApi = baseServiceHelper.getAddonByIdHelper(addon_id).ResponseValidator.GetBodyAsText();
        String statusMessage_getApi = JsonPath.read(body_getApi, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        int statusCode_getApi = JsonPath.read(body_getApi,"$.statusCode");
        softAssert.assertEquals(statusCode_getApi,Constants.statusOne);
        softAssert.assertEquals(statusMessage_getApi,"null");
        //invalid case
        String invalid_body = baseServiceHelper.deleteAddonHelper("12345687901234").ResponseValidator.GetBodyAsText();
        String inv_statusMessage = JsonPath.read(invalid_body, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        int inv_statusCode = JsonPath.read(invalid_body,"$.statusCode");
        softAssert.assertEquals(inv_statusCode,Constants.statusZero);
        softAssert.assertEquals(inv_statusMessage,"No Addon Found For Id : 12345687901234");
        softAssert.assertAll();
    }



    @Test(dataProvider = "createItemDP",priority = 24,groups = {"BaseService", "aviral"}, enabled = true)
    public void createItem (ArrayList<String> array, String message_to_validate, String statusCode) {
        SoftAssert softAssert = new SoftAssert();
        log.info("ARRAY ---> "+array);
        String[] payload = array.toArray(new String[array.size()]);
        String body = baseServiceHelper.createItemHelper(payload).ResponseValidator.GetBodyAsText();
        int get_statusCode = JsonPath.read(body,"$.statusCode");
        String get_statusMessage = JsonPath.read(body,"$..statusMessage").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_data_field = JsonPath.read(body,"$..data").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_item_id = JsonPath.read(body, "$..data.id").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_name = JsonPath.read(body, "$..data..name").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_third_party_id = JsonPath.read(body, "$..data..third_party_id").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_cat_id = JsonPath.read(body, "$..data..category_id").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_sub_cat_id = JsonPath.read(body, "$..data..sub_category_id").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_is_veg= JsonPath.read(body, "$..data..is_veg").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_price= JsonPath.read(body, "$..data..price").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_service_charges= JsonPath.read(body, "$..data..service_charges").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_service_tax= JsonPath.read(body, "$..data..service_tax").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_vat= JsonPath.read(body, "$..data..vat").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_packing_charges= JsonPath.read(body, "$..data..packing_charges").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_packing_slab_count= JsonPath.read(body, "$..data..packing_slab_count").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_is_spicy= JsonPath.read(body, "$..data..is_spicy").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_serves_how_many= JsonPath.read(body, "$..data..serves_how_many").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_enabled= JsonPath.read(body, "$..data..enabled").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_in_stock= JsonPath.read(body, "$..data..in_stock").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_addon_free_limit= JsonPath.read(body, "$..data..addon_free_limit").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_addon_limit= JsonPath.read(body, "$..data..addon_limit").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_is_perishable= JsonPath.read(body, "$..data..is_perishable").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_is_type= JsonPath.read(body, "$..data..type").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_restaurant_id= JsonPath.read(body, "$..data..restaurant_id").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_order= JsonPath.read(body, "$..data..order").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        if(!get_item_id.equals("") && !get_item_id.equals("0") && !get_item_id.equals(null) && !get_item_id.equals(" ")) {
            g_item_id.add(get_item_id);
            log.info("G_ITEM_ID ----> "+g_item_id);
        }
        if (message_to_validate.contains("${ITEM_ID}")) {
            int len = g_item_id.size();
            message_to_validate = message_to_validate.replace("${ITEM_ID}", g_item_id.get(len-1));
            //log.info("----->"+message_to_validate);
        }
        if (statusCode.equals("0")){
            softAssert.assertEquals(get_statusCode,Constants.statusZero, "Status not 0");
            softAssert.assertEquals(get_data_field,"null");
            softAssert.assertEquals(get_statusMessage,message_to_validate, "Status message issue");
        }
        else if (statusCode.equals("1")) {
            softAssert.assertEquals(get_statusCode,Constants.statusOne, "Status not 1");
            softAssert.assertNotEquals(get_data_field,"null");
            softAssert.assertEquals(get_statusMessage,message_to_validate, "Status message issue");
            softAssert.assertEquals(get_name,payload[12].replace("\"","").trim());
            softAssert.assertEquals(get_third_party_id,payload[24].replace("\"","").trim());
            softAssert.assertEquals(get_cat_id,payload[4].replace("\"","").trim());
            softAssert.assertEquals(get_sub_cat_id,payload[22].replace("\"","").trim());
            softAssert.assertEquals(get_is_veg,payload[11].replace("\"","").trim());
            softAssert.assertEquals(get_price,payload[16].replace("\"","").trim());
            softAssert.assertEquals(get_service_charges,payload[20].replace("\"","").trim());
            softAssert.assertEquals(get_service_tax,payload[21].replace("\"","").trim());
            softAssert.assertEquals(get_vat,payload[26].replace("\"","").trim());
            softAssert.assertEquals(get_packing_charges,payload[14].replace("\"","").trim());
            softAssert.assertEquals(get_packing_slab_count,payload[15].replace("\"","").trim());
            softAssert.assertEquals(get_is_spicy,payload[10].replace("\"","").trim());
            softAssert.assertEquals(get_serves_how_many,payload[19].replace("\"","").trim());
            softAssert.assertEquals(get_enabled,payload[6].replace("\"","").trim());
            softAssert.assertEquals(get_in_stock,payload[7].replace("\"","").trim());
            softAssert.assertEquals(get_addon_free_limit,payload[2].replace("\"","").trim());
            softAssert.assertEquals(get_addon_limit,payload[3].replace("\"","").trim());
            softAssert.assertEquals(get_is_perishable,payload[9].replace("\"","").trim());
            softAssert.assertEquals(get_is_type,payload[23].replace("\"","").trim());
            softAssert.assertEquals(get_restaurant_id,payload[18].replace("\"","").trim());
            softAssert.assertEquals(get_order,payload[13].replace("\"","").trim());

        }
        softAssert.assertAll();
    }

    @Test(dataProvider = "updateItemDP",priority = 25,groups = {"BaseService", "aviral"}, enabled = true)
    public void updateItem (String json, String itemID, String name, String thirdParty, int addonFreeLimit, int addonLimit, double price, int catID, String message_to_validate, String status) {
        SoftAssert softAssert = new SoftAssert();
        log.info("JSON ---> "+json);
        String body = baseServiceHelper.updateItemsHelper(json,itemID).ResponseValidator.GetBodyAsText();
        int get_statusCode = JsonPath.read(body,"$.statusCode");
        String get_statusMessage = JsonPath.read(body,"$..statusMessage").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_data_field = JsonPath.read(body,"$..data").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_item_id = JsonPath.read(body, "$..data.id").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_name = JsonPath.read(body, "$..data..name").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_third_party_id = JsonPath.read(body, "$..data..third_party_id").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_cat_id = JsonPath.read(body, "$..data..category_id").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_price= JsonPath.read(body, "$..data..price").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_addon_free_limit= JsonPath.read(body, "$..data..addon_free_limit").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_addon_limit= JsonPath.read(body, "$..data..addon_limit").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();

        if(status.equals("0")){
            softAssert.assertEquals(get_statusCode,Constants.statusZero, "Status not zero");
            softAssert.assertEquals(get_statusMessage,message_to_validate,"Message not same");
            softAssert.assertEquals(get_data_field,"null", "data field not null");

        }
        else if (status.equals("1")) {
            softAssert.assertEquals(get_statusCode,Constants.statusOne, "Status not zero");
            softAssert.assertEquals(get_statusMessage,message_to_validate,"Message not same");
            softAssert.assertEquals(get_item_id,itemID, "itemID not equal");
            softAssert.assertEquals(get_name,name, "name not equal");
            softAssert.assertEquals(get_third_party_id,thirdParty, "third party not equal");
            softAssert.assertEquals(Integer.parseInt(get_cat_id),catID,"subcat not equal");
            softAssert.assertEquals(Double.parseDouble(get_price),price,"price not equal");
            softAssert.assertEquals(Integer.parseInt(get_addon_free_limit),addonFreeLimit,"addon free limit not equal");
            softAssert.assertEquals(Integer.parseInt(get_addon_limit),addonLimit,"addon limit not equal");
            //softAssert.assertEquals();
        }

        softAssert.assertAll();

    }


    @Test(dataProvider = "getItemByIdDP",priority = 26,groups = {"BaseService", "aviral"}, enabled = true)
    public void getItemById (String itemID, ArrayList<String> to_validate, int catID, int subCatID, List<String> variantGroups, List<String> variants, List<String> addon_group_id, List<String> addon_id, String message_to_validate, String status) throws IOException {
        ToolBox toolBox = new ToolBox();
        log.info("to_validate :::: " + to_validate);
        log.info("ITEMID :::" + itemID);
        log.info("CATID :::" + catID);
        log.info("SUBCATID ::: " + subCatID);
        log.info("variantGroup :::: " + variantGroups);
        log.info("varaints ::: " + variants);
        log.info("addongroup ::: " + addon_group_id);
        log.info("addon_id ::::" + addon_id);
        SoftAssert softAssert = new SoftAssert();
        //log.info("JSON ---> "+json);
        JsonHelper jsonHelper = new JsonHelper();
        String body = baseServiceHelper.getItemByIdHelper(itemID).ResponseValidator.GetBodyAsText();
        int get_statusCode = JsonPath.read(body,"$.statusCode");
        String get_statusMessage = JsonPath.read(body,"$..statusMessage").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_data_field = JsonPath.read(body,"$..data").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        // String get_sub_cat_id = JsonPath.read(body, "$.data.sub_category_id").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        List<Map<String, Object>> list_vg = baseServiceHelper.getVariantGroupsFromDB(Constants.q_get_variantGroup_by_itemId,itemID);
        List<Map<String, Object>> list_v = baseServiceHelper.getVariantsFromDB(Constants.q_get_variant_by_itemId,itemID);
        List<Map<String, Object>> list_ag = baseServiceHelper.getVariantGroupsFromDB(Constants.q_get_addonGroup_by_itemId,itemID);
        List<Map<String, Object>> list_a = baseServiceHelper.getVariantsFromDB(Constants.q_get_addon_by_itemId,itemID);
        List<String> v_db = new ArrayList<>();
        List<String> vg_db = new ArrayList<>();
        List<String> a_db = new ArrayList<>();
        List<String> ag_db = new ArrayList<>();
        System.out.println("list_vg--->"+list_vg);
        System.out.println("list_v--->"+list_v);
        if (list_vg.size() == 0){
            vg_db.add("null");
            v_db.add("null");
        }
        else {
            for (int i = 0; i < list_vg.size(); i++){
                vg_db.add(list_vg.get(i).get("id").toString());
            }
            String[] get_variant_db = JsonPath.read(list_v.get(0).get("variants").toString(),"$.variant_groups[*].variations[*].id").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim().split(",");
            v_db = Arrays.asList(get_variant_db);
        }
        if(list_ag.size() == 0) {
            ag_db.add("null");
            a_db.add("null");
        }
        else {
            for (int i = 0; i < list_ag.size(); i++){
                ag_db.add(list_ag.get(i).get("id").toString());
            }
            log.info("a_json ::: "+list_a.get(0).get("addons"));
            //log.info("======================="+JsonPath.read(list_a.get(0).get("addons").toString(),"$.[*].choices[*].id"));
            String[] get_addons_db = JsonPath.read(list_a.get(0).get("addons").toString(),"$.[*].choices[*].id").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim().split(",");
            a_db = Arrays.asList(get_addons_db);
        }
        //debugger
        System.out.println("V_DB ::: "+v_db);
        System.out.println("A_DB ::: "+a_db);
        System.out.println("VG_DB ::: "+vg_db);
        System.out.println("AG_DB ::: "+ag_db);
        System.out.println("variantGroups ::: "+ variantGroups);
        System.out.println("variants ::: "+variants);
        System.out.println("addon_group_id ::: "+addon_group_id);
        System.out.println("addon_id ::: "+addon_id);

        if (status.equals("1")) {
            String get_item_id = JsonPath.read(body, "$.data.id").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
            String get_name = JsonPath.read(body, "$.data.name").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
            String get_third_party_id = JsonPath.read(body, "$.data.third_party_id").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
            String get_cat_id = JsonPath.read(body, "$.data.category_id").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
            String get_price= JsonPath.read(body, "$.data.price").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
            String get_addon_free_limit= JsonPath.read(body, "$.data.addon_free_limit").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
            String get_addon_limit= JsonPath.read(body, "$.data.addon_limit").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
            String get_service_charges = JsonPath.read(body, "$.data.service_charges").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
            String get_type = JsonPath.read(body, "$.data.type").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
            String get_serves_how_many = JsonPath.read(body, "$.data.serves_how_many").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
            String get_rest_id = JsonPath.read(body, "$.data.restaurant_id").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();

            softAssert.assertEquals(get_statusCode, Constants.statusOne, "Status not one");
            //softAssert.assertEquals(get_statusMessage, message_to_validate, "Message not same");
            softAssert.assertEquals(get_item_id,itemID,"ItemID not same");
            softAssert.assertEquals(get_name,to_validate.get(1), "name not equal");
            softAssert.assertEquals(get_third_party_id,to_validate.get(2), "third party not equal");
            softAssert.assertEquals(get_addon_free_limit,to_validate.get(3),"addon free limit not the same");
            softAssert.assertEquals(get_addon_limit,to_validate.get(4),"addon limit not the same");
            softAssert.assertEquals(get_price,to_validate.get(5),"Price not the same");
            softAssert.assertEquals(get_service_charges,to_validate.get(6),"service charges not the same");
            softAssert.assertEquals(get_type,to_validate.get(7),"type not the same");
            softAssert.assertEquals(get_serves_how_many, to_validate.get(8),"serves how many is not the same");
            softAssert.assertEquals(get_rest_id,to_validate.get(9),"rest id not the same");
            softAssert.assertEquals(get_cat_id,String.valueOf(catID),"category id not same");
            //softAssert.assertEquals(get_sub_cat_id,String.valueOf(subCatID),"sub category id not same");
            if(addon_group_id.get(0).equals("null") && addon_id.get(0).equals("null") && variantGroups.get(0).equals("null") && variants.get(0).equals("null")){
                softAssert.assertEquals(addon_group_id.get(0),ag_db.get(0),"addon group values in db are not same");
                softAssert.assertEquals(addon_id.get(0),a_db.get(0),"addon values not same from the db");
                softAssert.assertEquals(variantGroups.get(0),vg_db.get(0),"variant group values not same from the db");
                softAssert.assertEquals(variants.get(0),v_db.get(0),"variant values not same from the db");
            }
            else if(addon_group_id.get(0).equals("null") && addon_id.get(0).equals("null")) {
                softAssert.assertEquals(variantGroups.size(),vg_db.size(),"value size and db value size not equal");
                softAssert.assertTrue(toolBox.compareLists(variantGroups,vg_db),"variant group lists are not same");
                softAssert.assertEquals(variants.size(),v_db.size(),"size of variants lists not same from db");
                softAssert.assertTrue(toolBox.compareLists(variants,v_db),"variants list are not same");
            }
            else if(variantGroups.get(0).equals("null") && variants.get(0).equals("null")) {
                softAssert.assertEquals(addon_group_id.size(),ag_db.size(),"value of size and db value size not equal");
                softAssert.assertTrue(toolBox.compareLists(addon_group_id,ag_db));
                softAssert.assertEquals(addon_id.size(),a_db.size(),"size of addon lists not same from db");
                softAssert.assertTrue(toolBox.compareLists(addon_id,a_db),"addon list are not same");
            }
        }
        else if (status.equals("0")){
            softAssert.assertEquals(get_statusCode, Constants.statusZero, "Status not zero");
            softAssert.assertEquals(get_statusMessage,message_to_validate,"Message not same");
            softAssert.assertEquals(get_data_field,"null", "data field not null");
        }
        else {
            softAssert.assertTrue(false,"Status code not 1 || 0 || 400");
        }
        softAssert.assertAll();
    }

    @Test(dataProvider = "searchItemDP",priority = 27,groups = {"BaseService", "aviral"}, enabled = true)
    public void searchItem(ArrayList<String> to_validate, String message_to_validate, boolean flag) {
        SoftAssert softAssert = new SoftAssert();
        String name = to_validate.get(1);
        String body = baseServiceHelper.searchItemByNameHelper(name).ResponseValidator.GetBodyAsText();
        int get_statusCode = JsonPath.read(body,"$.statusCode");
        String get_statusMessage = JsonPath.read(body,"$..statusMessage").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String data_field = JsonPath.read(body, "$..data").toString().replace("[","").replace("]","").replace("{","").replace("}","").trim();
        String[] get_item_id = JsonPath.read(body, "$..search_data[*]..id").toString().replace("[","").replace("]","").replace("{","").replace("}","").trim().split(",");
        String[] get_name = JsonPath.read(body, "$..data..search_data[*]..name").toString().replace("[","").replace("]","").replace("\"","").trim().split(",");
        String[] get_thirdParty = JsonPath.read(body, "$..data..search_data[*]..third_party_id").toString().replace("[","").replace("]","").replace("\"","").trim().split(",");
        String get_pageable_size = JsonPath.read(body, "$..data..pageable..page").toString().replace("[","").replace("]","").replace("\"","").trim();
        String[] get_search_data_id = JsonPath.read(body, "$..data..search_data[*]..id").toString().replace("[","").replace("]","").replace("\"","").trim().split(",");
        String get_pageable = JsonPath.read(body, "$..data..pageable").toString().replace("[","").replace("]","").replace("\"","").trim();
        String get_search_data = JsonPath.read(body, "$..data..search_data[*]").toString().replace("[","").replace("]","").replace("\"","").trim();
        String get_price= JsonPath.read(body, "$.data.search_data[*].price").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_addon_free_limit= JsonPath.read(body, "$.data.search_data[*].addon_free_limit").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_addon_limit= JsonPath.read(body, "$.data.search_data[*].addon_limit").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_service_charges = JsonPath.read(body, "$.data.search_data[*].service_charges").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_type = JsonPath.read(body, "$.data.search_data[*].type").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_serves_how_many = JsonPath.read(body, "$.data.search_data[*].serves_how_many").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_rest_id = JsonPath.read(body, "$.data.search_data[*].restaurant_id").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();

        softAssert.assertEquals(get_statusCode,Constants.statusOne);
        softAssert.assertEquals(get_statusMessage,message_to_validate,"message not equal");
        //softAssert.assertEquals(data_field,"null");
        if(!flag){
            softAssert.assertEquals(get_pageable,"null");
            softAssert.assertEquals(get_search_data,"");
        }
        else if (flag && !name.equals("null") && !name.equals("")) {
            softAssert.assertTrue((get_name.length>=1));
            softAssert.assertTrue((get_item_id.length>=1));
            softAssert.assertEquals(get_name,name, "Name not same");
            softAssert.assertEquals(get_thirdParty,to_validate.get(2), "Third party ID not same");
            softAssert.assertEquals(get_item_id,to_validate.get(0), "Item id not same");
            softAssert.assertEquals(get_addon_free_limit,to_validate.get(3),"addon free limit not the same");
            softAssert.assertEquals(get_addon_limit,to_validate.get(4),"addon limit not the same");
            softAssert.assertEquals(get_price,to_validate.get(5),"Price not the same");
            softAssert.assertEquals(get_service_charges,to_validate.get(6),"service charges not the same");
            softAssert.assertEquals(get_type,to_validate.get(7),"type not the same");
            softAssert.assertEquals(get_serves_how_many, to_validate.get(8),"serves how many is not the same");
            softAssert.assertEquals(get_rest_id,to_validate.get(9),"rest id not the same");

        }
        else if(flag && (name.equals("null") || name.equals(""))){
            softAssert.assertNotNull(get_pageable);
            softAssert.assertEquals(get_pageable,"null","pageable not null");
            softAssert.assertEquals(get_search_data,"");
        }

    }

    @Test(priority = 28,groups = {"BaseService", "aviral"}, enabled = true)
    public void deleteItem() throws IOException {
        SoftAssert softAssert = new SoftAssert();
        int catID = baseServiceHelper.createCategoryIdAndReturnInt(restaurantId);
        int subCatID = baseServiceHelper.createSubCategoryAndReturnInt(restaurantId,catID);
        String itemID = createSingleItem(catID,subCatID,50,50);
        String body = baseServiceHelper.deleteItemHelper(itemID).ResponseValidator.GetBodyAsText();
        String statusMessage = JsonPath.read(body, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        int statusCode = JsonPath.read(body,"$.statusCode");
        softAssert.assertEquals(statusCode,Constants.statusOne);
        softAssert.assertEquals(statusMessage,"Item deleted successfully.");
        //validate
        String body_getApi = baseServiceHelper.getItemByIdHelper(itemID).ResponseValidator.GetBodyAsText();
        String statusMessage_getApi = JsonPath.read(body_getApi, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        int statusCode_getApi = JsonPath.read(body_getApi,"$.statusCode");
        softAssert.assertEquals(statusCode_getApi,Constants.statusZero);
        softAssert.assertEquals(statusMessage_getApi,"No Item Found For id :"+itemID);
        //invalid case
        String invalid_body = baseServiceHelper.deleteItemHelper("123456879012345938").ResponseValidator.GetBodyAsText();
        String inv_statusMessage = JsonPath.read(invalid_body, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        int inv_statusCode = JsonPath.read(invalid_body,"$.statusCode");
        softAssert.assertEquals(inv_statusCode,Constants.statusZero);
        softAssert.assertEquals(inv_statusMessage,"No Item Found For id 123456879012345938");
        softAssert.assertAll();
    }


    @Test(dataProvider = "createCategoryDP",priority = 29,groups = {"BaseService", "aviral"}, enabled = true)
    public void createCategory(String json, ArrayList<String> to_validate, String message_to_validate, String status) {
        SoftAssert softAssert = new SoftAssert();
        log.info("JSON ---> "+json);
        String body = baseServiceHelper.createCategoryHelper(json).ResponseValidator.GetBodyAsText();
        if(status.equals("400")) {
            String get_status = JsonPath.read(body,"$..status").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
            String get_error = JsonPath.read(body,"$..error").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
            softAssert.assertEquals(get_status,status, "Status not 400");
            softAssert.assertEquals(get_error,"Bad Request", "error not same");
        }
        else {
            int get_statusCode = JsonPath.read(body, "$.statusCode");
            String get_statusMessage = JsonPath.read(body, "$..statusMessage").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
            String get_data_field = JsonPath.read(body, "$..data").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").trim();

            if (status.equals("0")) {
                softAssert.assertEquals(get_statusCode, Constants.statusZero, "Status not zero");
                softAssert.assertEquals(get_statusMessage, message_to_validate, "Message not same");
                softAssert.assertEquals(get_data_field, "null", "data field not null");

            } else if (status.equals("1")) {
                String get_cat_id = JsonPath.read(body, "$.data.id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                String get_name = JsonPath.read(body, "$.data.name").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                String get_third_party_id = JsonPath.read(body, "$.data.third_party_id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                String get_type = JsonPath.read(body, "$.data.type").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                String get_parent_category_id = JsonPath.read(body, "$.data.parent_category_id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                String get_rest_id = JsonPath.read(body, "$.data.restaurant_id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                String get_active = JsonPath.read(body, "$.data.active").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();

                softAssert.assertEquals(get_statusCode, Constants.statusOne, "Status not zero");
                softAssert.assertEquals(get_statusMessage, message_to_validate, "Message not same");
                softAssert.assertEquals(get_third_party_id, to_validate.get(1), "Third party ID not same");
                softAssert.assertEquals(get_name, to_validate.get(0), "name not the same");
                softAssert.assertEquals(get_active, to_validate.get(5), "is active not the same");
                softAssert.assertEquals(get_type, to_validate.get(4), "type not the same");
                softAssert.assertEquals(get_rest_id, to_validate.get(2), "rest id not the same");
                softAssert.assertEquals(get_parent_category_id, to_validate.get(3), "parent cat id now same not the same");
            }
            else {
                softAssert.assertTrue(false, "Status code not 0 || 1 || 400");
            }
        }
        softAssert.assertAll();
    }


    @Test(dataProvider = "updateCategoryDP",priority = 30,groups = {"BaseService", "aviral"}, enabled = true)
    public void updateCategory(String json, String catID, ArrayList<String> to_validate, String message_to_validate, String status) {
        SoftAssert softAssert = new SoftAssert();
        log.info("JSON ---> "+json);
        String body = baseServiceHelper.updateCategoryHelper(json,catID).ResponseValidator.GetBodyAsText();
        if(status.equals("400")) {
            String get_status = JsonPath.read(body,"$..status").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
            String get_error = JsonPath.read(body,"$..error").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
            softAssert.assertEquals(get_status,status, "Status not 400");
            softAssert.assertEquals(get_error,"Bad Request", "error not same");
        }
        else {
            int get_statusCode = JsonPath.read(body, "$.statusCode");
            String get_statusMessage = JsonPath.read(body, "$..statusMessage").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
            String get_data_field = JsonPath.read(body, "$..data").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").trim();

            if (status.equals("0")) {
                softAssert.assertEquals(get_statusCode, Constants.statusZero, "Status not zero");
                softAssert.assertEquals(get_statusMessage, message_to_validate, "Message not same");
                softAssert.assertEquals(get_data_field, "null", "data field not null");

            } else if (status.equals("1")) {
                String get_cat_id = JsonPath.read(body, "$.data.id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                String get_name = JsonPath.read(body, "$.data.name").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                String get_third_party_id = JsonPath.read(body, "$.data.third_party_id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                String get_type = JsonPath.read(body, "$.data.type").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                String get_parent_category_id = JsonPath.read(body, "$.data.parent_category_id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                //String get_rest_id = JsonPath.read(body, "$.data.restaurant_id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                String get_active = JsonPath.read(body, "$.data.active").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();

                softAssert.assertEquals(get_cat_id,catID,"CatID not same");
                softAssert.assertEquals(get_statusCode, Constants.statusOne, "Status not zero");
                softAssert.assertEquals(get_statusMessage, message_to_validate, "Message not same");
                softAssert.assertEquals(get_third_party_id, to_validate.get(1), "Third party ID not same");
                softAssert.assertEquals(get_name, to_validate.get(0), "name not the same");
                softAssert.assertEquals(get_active, to_validate.get(5), "is active not the same");
                softAssert.assertEquals(get_type, to_validate.get(4), "type not the same");
                //softAssert.assertEquals(get_rest_id, to_validate.get(2), "rest id not the same");
                softAssert.assertEquals(get_parent_category_id, to_validate.get(3), "parent cat id now same not the same");
            }
            else {
                softAssert.assertTrue(false ,"Status code not 0 || 1 || 400");
            }
        }
        softAssert.assertAll();
    }


    @Test(dataProvider = "getCategoryByIdDP",priority = 31,groups = {"BaseService", "aviral"}, enabled = true)
    public void getCategoryById(ArrayList<String> to_validate, String message_to_validate, String status) {
        SoftAssert softAssert = new SoftAssert();
        String catID = to_validate.get(0);
        String body = baseServiceHelper.getCategoryByIdHelper(catID).ResponseValidator.GetBodyAsText();
        int get_statusCode = JsonPath.read(body,"$.statusCode");
        String get_statusMessage = JsonPath.read(body,"$..statusMessage").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();
        String get_data_field = JsonPath.read(body,"$..data").toString().replace("[","").replace("]","").replace("{","").replace("}","").replace("\"","").trim();

        if(status.equals("1")) {
            String get_cat_id = JsonPath.read(body, "$.data.id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
            String get_name = JsonPath.read(body, "$.data.name").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
            String get_third_party_id = JsonPath.read(body, "$.data.third_party_id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
            String get_type = JsonPath.read(body, "$.data.type").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
            String get_parent_category_id = JsonPath.read(body, "$.data.parent_category_id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
            String get_active = JsonPath.read(body, "$.data.active").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();

            softAssert.assertEquals(get_cat_id, catID, "CatID not same");
            softAssert.assertEquals(get_statusCode, Constants.statusOne, "Status not zero");
            softAssert.assertEquals(get_statusMessage, message_to_validate, "Message not same");
            softAssert.assertEquals(get_third_party_id, to_validate.get(2), "Third party ID not same");
            softAssert.assertEquals(get_name, to_validate.get(1), "name not the same");
            softAssert.assertEquals(get_active, to_validate.get(4), "is active not the same");
            softAssert.assertEquals(get_type, to_validate.get(5), "type not the same");
            //softAssert.assertEquals(get_rest_id, to_validate.get(2), "rest id not the same");
            softAssert.assertEquals(get_parent_category_id, to_validate.get(4), "parent cat id now same not the same");
        }
        else if (status.equals("0")) {
            softAssert.assertEquals(get_statusCode, Constants.statusZero, "Status not zero");
            softAssert.assertEquals(get_statusMessage, message_to_validate, "Message not same");
            softAssert.assertEquals(get_data_field, "null", "data field not null");

        }
        else {
            softAssert.assertTrue(false ,"Status code not 0 || 1 || 400");
        }
    }

    @Test(dataProvider = "getCategoryByRestIdDP",priority = 32,groups = {"BaseService", "aviral"}, enabled = true)
    public void getCategoryByRestId(String restId, String message_to_validate, String status) {
        SoftAssert softAssert = new SoftAssert();
        String body = baseServiceHelper.getCategoryByRestIdHelper(restId).ResponseValidator.GetBodyAsText();
        String get_statusMessage = JsonPath.read(body, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        int statusCode = JsonPath.read(body,"$.statusCode");
        String get_data_field = JsonPath.read(body, "$..data").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").trim();

        if(status.equals("1")) {
            String[] get_cat_id = JsonPath.read(body, "$..data..id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").trim().split(",");

            softAssert.assertEquals(statusCode,Constants.statusOne, "Status not 1");
            softAssert.assertEquals(get_statusMessage, message_to_validate, "Message not same");
            softAssert.assertTrue((get_cat_id.length > 0),"id length not more than 0");
        }
        else if(status.equals("0")) {
            softAssert.assertEquals(statusCode,Constants.statusZero, "Status not 0");
            softAssert.assertEquals(get_statusMessage, message_to_validate, "Message not same");
            softAssert.assertEquals(get_data_field, "null", "data field not null");
        }
        else {
            softAssert.assertTrue(false,"Status not 0 || 1");
        }
        softAssert.assertAll();
    }

    @Test(dataProvider = "getSubCategoryByCatIdDP",priority = 33,groups = {"BaseService", "aviral"}, enabled = true)
    public void getSubCategoryByCatId(String catID, String[] subcatID,String[] subcatName, String message_to_validate, String status) {
        SoftAssert softAssert = new SoftAssert();
        String body = baseServiceHelper.getSubCategoryByCatIdHelper(catID).ResponseValidator.GetBodyAsText();
        String get_statusMessage = JsonPath.read(body, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        int statusCode = JsonPath.read(body,"$.statusCode");
        String get_data_field = JsonPath.read(body, "$..data").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"","").trim();

        if(status.equals("1")) {
            String[] get_subcat_id = JsonPath.read(body, "$..data..id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"","").trim().split(",");
            String[] get_subcat_name = JsonPath.read(body, "$..data..name").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"","").trim().split(",");
            softAssert.assertEquals(statusCode,Constants.statusOne, "Status not 1");
            softAssert.assertEquals(get_statusMessage, message_to_validate, "Message not same");
            softAssert.assertTrue((get_subcat_id.length > 0),"id length not more than 0");
            Arrays.sort(subcatID);
            for(String s:get_subcat_id){
                log.info("SUBCAT ID FOUND: "+s);
                softAssert.assertTrue((Arrays.binarySearch(subcatID,s) >= 0),"sub cat ID not found");

            }
            Arrays.sort(subcatName);
            for(String s:get_subcat_name){
                log.info("SUBCAT NAME FOUND: "+s);
                softAssert.assertTrue((Arrays.binarySearch(subcatName,s) >= 0),"sub cat Name not found");

            }
        }
        else if(status.equals("0")) {
            softAssert.assertEquals(statusCode,Constants.statusZero, "Status not 0");
            softAssert.assertEquals(get_statusMessage, message_to_validate, "Message not same");
            softAssert.assertEquals(get_data_field, "", "data field not empty");
        }
        else {
            softAssert.assertTrue(false,"Status not 0 || 1");
        }
        softAssert.assertAll();
    }

    @Test(dataProvider = "searchCategoryDP",priority = 34,groups = {"BaseService", "aviral"}, enabled = true)
    public void searchCategory(String restID, String name, ArrayList<String> to_validate, String message_to_validate, String status) {
        //TODO: FIX invalid case messages once bug is fixed
        SoftAssert softAssert = new SoftAssert();
        String body = baseServiceHelper.searchCategoryHelper(restID,name).ResponseValidator.GetBodyAsText();
        String get_statusMessage = JsonPath.read(body, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        int statusCode = JsonPath.read(body,"$.statusCode");
        String get_search_data_field = JsonPath.read(body, "$..data..search_data").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"","").trim();
        if(status.equals("1")) {
            String get_cat_id = JsonPath.read(body, "$..data..search_data..id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
            String get_name = JsonPath.read(body, "$..data..search_data..name").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
            String get_third_party_id = JsonPath.read(body, "$..data..search_data..third_party_id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
            String get_type = JsonPath.read(body, "$..data..search_data..type").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
            String get_parent_category_id = JsonPath.read(body, "$..data..search_data..parent_category_id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
            String get_active = JsonPath.read(body, "$..data..search_data..active").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
            softAssert.assertEquals(get_cat_id,to_validate.get(0));
            softAssert.assertEquals(statusCode,Constants.statusOne, "Status not 0");
            softAssert.assertEquals(get_statusMessage, message_to_validate, "Message not same");
            softAssert.assertEquals(get_third_party_id, to_validate.get(2), "Third party ID not same");
            softAssert.assertEquals(get_name, to_validate.get(1), "name not the same");
            softAssert.assertEquals(get_active, to_validate.get(6), "is active not the same");
            softAssert.assertEquals(get_type, to_validate.get(5), "type not the same");
            softAssert.assertEquals(get_parent_category_id, to_validate.get(4), "parent cat id now same not the same");
        }
        else if(status.equals("0")) {
            softAssert.assertEquals(statusCode,Constants.statusZero, "Status not 0");
            softAssert.assertEquals(get_statusMessage, message_to_validate, "Message not same");
            softAssert.assertEquals(get_search_data_field, "", "data field not empty");
        }
        else {
            softAssert.assertTrue(false,"Status not 0 || 1");
        }
        softAssert.assertAll();
    }



    @Test(priority = 35,groups = {"BaseService", "aviral"}, enabled = true)
    public void deleteCategory() throws IOException {
        SoftAssert softAssert = new SoftAssert();
        ArrayList<String> getCatDetails = createSingleCategoryWithReturnData(0,0,"REGULAR_MENU","Cat");
        String catID = getCatDetails.get(0);
        String body = baseServiceHelper.deleteCategoryHelper(catID).ResponseValidator.GetBodyAsText();
        String statusMessage = JsonPath.read(body, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        int statusCode = JsonPath.read(body,"$.statusCode");
        softAssert.assertEquals(statusCode,Constants.statusOne);
        softAssert.assertEquals(statusMessage,"Category deleted successfully","delete message not same");
        //invalid case
        String invalid_body = baseServiceHelper.deleteCategoryHelper("123416879012").ResponseValidator.GetBodyAsText();
        String inv_statusMessage = JsonPath.read(invalid_body, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        int inv_statusCode = JsonPath.read(invalid_body,"$.statusCode");
        softAssert.assertEquals(inv_statusCode,Constants.statusZero);
        softAssert.assertEquals(inv_statusMessage,"null","invalid delete message not same");
        softAssert.assertAll();
    }

    @Test(dataProvider = "bulkAddonGroupsDP",priority = 36,groups = {"BaseService", "aviral"}, enabled = true)
    public void bulkAddonGroups(String json,ArrayList<Integer> delete_by_id, ArrayList<String> delete_by_third_party,ArrayList<String> names,ArrayList<String> thirdParty, String message_to_validate, String create_error,String update_error,String delete_error) throws IOException {
        log.info("JSON ---> "+json);
        SoftAssert softAssert = new SoftAssert();
        String body = baseServiceHelper.bulkAddonGroupHelper(json).ResponseValidator.GetBodyAsText();
        String get_statusMessage = JsonPath.read(body, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        int statusCode = JsonPath.read(body,"$.statusCode");
        if(statusCode == 1) {
            String get_failed_delete_by_ids = JsonPath.read(body, "$..data..failed_delete_by_ids").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
            String get_failed_delete_by_third_party_ids = JsonPath.read(body, "$..data..failed_delete_by_third_party_ids").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
            String get_is_failed = JsonPath.read(body, "$..data..is_failed").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
            softAssert.assertEquals(statusCode,Constants.statusOne, "Status not 0");
            softAssert.assertEquals(get_statusMessage, message_to_validate, "Message not same");

            if(!names.isEmpty() && create_error.equals("false") && update_error.equals("false")){
                String[] get_names = JsonPath.read(body, "$..data..addon_groups..name").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                log.info("List of names --> "+names);
                for (int i = 0; i < get_names.length ; i++) {
                    log.info("search name --> "+get_names[i]);
                    softAssert.assertTrue((names.contains(get_names[i])),"does not contain the name");
                }
            }
            else if (!names.isEmpty() && (create_error.equals("true") || update_error.equals("true"))) {
                String[] get_failed_names = JsonPath.read(body, "$..data..failed_addon_groups..name").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                log.info("List of names --> "+names);
                for (int i = 0; i < get_failed_names.length ; i++) {
                    log.info("search failed name --> "+get_failed_names[i]);
                    softAssert.assertTrue((names.contains(get_failed_names[i])),"does not contain the failed name");
                }
            }
            if(!thirdParty.isEmpty() && create_error.equals("false") && update_error.equals("false")){
                String[] get_tpi = JsonPath.read(body, "$..data..addon_groups..third_party_id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                log.info("List of TPI --> "+thirdParty);
                for (int i = 0; i < get_tpi.length ; i++) {
                    log.info("search tpi --> "+get_tpi[i]);
                    softAssert.assertTrue((thirdParty.contains(get_tpi[i])),"does not contain the thirdparty id");
                }
            }
            else if (!thirdParty.isEmpty() && (create_error.equals("true") || update_error.equals("true"))) {
                String[] get_failed_tpi = JsonPath.read(body, "$..data..failed_addon_groups..third_party_id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                log.info("List of TPI --> "+thirdParty);
                for (int i = 0; i < get_failed_tpi.length ; i++) {
                    log.info("search failed tpi --> "+get_failed_tpi[i]);
                    softAssert.assertTrue((thirdParty.contains(get_failed_tpi[i])),"does not contain the failed thirdparty id");
                }
            }
            if(!delete_by_id.isEmpty() && delete_error.equals("false")){
                log.info("List of delete_by_id --> "+delete_by_id);
                softAssert.assertEquals(get_failed_delete_by_ids, "","get_failed_delete_by_ids not empty");
                String[] get_delete_by_id = JsonPath.read(body, "$..data.delete_by_ids").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                for (int i = 0; i < get_delete_by_id.length ; i++) {
                    log.info("search delete_by_id --> "+Integer.parseInt(get_delete_by_id[i]));
                    softAssert.assertTrue(delete_by_id.contains(Integer.parseInt(get_delete_by_id[i])),"does not contain the get_delete_by_id");
                }
            }
            else if (!delete_by_id.isEmpty() && delete_error.equals("true")) {
                log.info("List of delete_by_id --> "+delete_by_id);
                String[] get_failed_delete_by_id = JsonPath.read(body, "$..data..failed_delete_by_ids[*]").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                for (int i = 0; i < get_failed_delete_by_id.length ; i++) {
                    //handle empty case
                    if(!get_failed_delete_by_id[i].equals("")) {
                        log.info("search failed delete_by_id --> " + Integer.parseInt(get_failed_delete_by_id[i]));
                        softAssert.assertTrue(delete_by_id.contains(Integer.parseInt(get_failed_delete_by_id[i])), "does not contain the failed_delete_by_id");
                    }
                    else
                        softAssert.assertTrue(false,"get_failed_delete_by_id is empty");
                }
            }
            if(!delete_by_third_party.isEmpty() && delete_error.equals("false")){
                log.info("List of delete_by_third_party --> "+delete_by_third_party);
                softAssert.assertEquals(get_failed_delete_by_third_party_ids, "","get_failed_delete_by_third_party_ids not empty");
                String[] get_delete_by_tpi = JsonPath.read(body, "$..data.delete_by_third_party_ids").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                for (int i = 0; i < get_delete_by_tpi.length ; i++) {
                    log.info("search delete_by_third_party --> "+get_delete_by_tpi[i]);
                    softAssert.assertTrue((delete_by_third_party.contains(get_delete_by_tpi[i].trim())),"does not contain the get_delete_by_tpi");
                }
            }
            else if (!delete_by_third_party.isEmpty() && delete_error.equals("true")) {
                log.info("List of delete_by_third_party --> "+delete_by_third_party);
                String[] get_failed_delete_by_tpi = JsonPath.read(body, "$..data..failed_delete_by_third_party_ids[*]").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                for (int i = 0; i < get_failed_delete_by_tpi.length ; i++) {
                    log.info("search delete_by_third_party --> "+get_failed_delete_by_tpi[i]);
                    softAssert.assertTrue((delete_by_third_party.contains(get_failed_delete_by_tpi[i].trim())),"does not contain the failed_delete_by_tpi");
                }
            }

            if(create_error.equals("true")){
                String get_created_error_map = JsonPath.read(body, "$..data..created_error_map").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                String get_created_error_message = JsonPath.read(body, "$..data..created_error_map[*]").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                softAssert.assertNotNull(get_created_error_map);
                softAssert.assertTrue(baseServiceHelper.validateBulkErrorMessages(get_created_error_message),"created_error_map msg not same");
                softAssert.assertEquals(get_is_failed, "true","get_is_failed not true");

            }
            else if (create_error.equals("false") && update_error.equals("false") && delete_error.equals("false"))
                softAssert.assertEquals(get_is_failed, "false","get_is_failed not false");

            //TODO:FIX error message for updated when bug is fixed
            if(update_error.equals("true")) {
                String get_updated_error_map = JsonPath.read(body, "$..data..updated_error_map").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                String get_updated_error_message = JsonPath.read(body, "$..data..updated_error_map[*]").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                softAssert.assertNotNull(get_updated_error_map);
                softAssert.assertTrue(baseServiceHelper.validateBulkErrorMessages(get_updated_error_message),"updated_error_map msg not same");
                softAssert.assertEquals(get_is_failed, "true","get_is_failed not true");
            }
            else if(create_error.equals("false") && update_error.equals("false") && delete_error.equals("false"))
                softAssert.assertEquals(get_is_failed, "false","get_is_failed not false");

            if(delete_error.equals("true")) {
                softAssert.assertEquals(get_is_failed, "true","get_is_failed not true");
            }
            else if (create_error.equals("false") && update_error.equals("false") && delete_error.equals("false"))
                softAssert.assertEquals(get_is_failed, "false","get_is_failed not false");

        }
        else {
            softAssert.assertTrue(false,"StatusCode not  1");
        }

        softAssert.assertAll();
    }

    @Test(dataProvider = "bulkAddonsDP",priority = 37,groups = {"BaseService", "aviral"}, enabled = true)
    public void bulkAddons(String json,ArrayList<Integer> delete_by_id, ArrayList<String> delete_by_third_party,ArrayList<String> names,ArrayList<String> thirdParty, String message_to_validate, String create_error,String update_error,String delete_error) throws IOException {
        log.info("JSON ---> "+json);
        SoftAssert softAssert = new SoftAssert();
        String body = baseServiceHelper.bulkAddonHelper(json).ResponseValidator.GetBodyAsText();
        String get_statusMessage = JsonPath.read(body, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        int statusCode = JsonPath.read(body,"$.statusCode");
        if(statusCode == 1) {
            String get_failed_delete_by_ids = JsonPath.read(body, "$..data..failed_delete_by_ids").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
            String get_failed_delete_by_third_party_ids = JsonPath.read(body, "$..data..failed_delete_by_third_party_ids").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
            String get_is_failed = JsonPath.read(body, "$..data..is_failed").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
            softAssert.assertEquals(statusCode,Constants.statusOne, "Status not 0");
            softAssert.assertEquals(get_statusMessage, message_to_validate, "Message not same");

            if(!names.isEmpty() && create_error.equals("false") && update_error.equals("false")){
                String[] get_names = JsonPath.read(body, "$..data..addons..name").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                log.info("List of names --> "+names);
                for (int i = 0; i < get_names.length ; i++) {
                    log.info("search name --> "+get_names[i]);
                    softAssert.assertTrue((names.contains(get_names[i])),"does not contain the name");
                }
            }
            else if (!names.isEmpty() && (create_error.equals("true") || update_error.equals("true"))) {
                String[] get_failed_names = JsonPath.read(body, "$..data..failed_addons..name").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                log.info("List of names --> "+names);
                for (int i = 0; i < get_failed_names.length ; i++) {
                    log.info("search failed name --> "+get_failed_names[i]);
                    softAssert.assertTrue((names.contains(get_failed_names[i])),"does not contain the failed name");
                }
            }
            if(!thirdParty.isEmpty() && create_error.equals("false") && update_error.equals("false")){
                String[] get_tpi = JsonPath.read(body, "$..data..addons..third_party_id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                log.info("List of TPI --> "+thirdParty);
                for (int i = 0; i < get_tpi.length ; i++) {
                    log.info("search tpi --> "+get_tpi[i]);
                    softAssert.assertTrue((thirdParty.contains(get_tpi[i])),"does not contain the thirdparty id");
                }
            }
            else if (!thirdParty.isEmpty() && (create_error.equals("true") || update_error.equals("true"))) {
                String[] get_failed_tpi = JsonPath.read(body, "$..data..failed_addons..third_party_id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                log.info("List of TPI --> "+thirdParty);
                for (int i = 0; i < get_failed_tpi.length ; i++) {
                    log.info("search failed tpi --> "+get_failed_tpi[i]);
                    softAssert.assertTrue((thirdParty.contains(get_failed_tpi[i])),"does not contain the failed thirdparty id");
                }
            }
            if(!delete_by_id.isEmpty() && delete_error.equals("false")){
                log.info("List of delete_by_id --> "+delete_by_id);
                softAssert.assertEquals(get_failed_delete_by_ids, "","get_failed_delete_by_ids not empty");
                String[] get_delete_by_id = JsonPath.read(body, "$..data.delete_by_ids").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                for (int i = 0; i < get_delete_by_id.length ; i++) {
                    log.info("search delete_by_id --> "+Integer.parseInt(get_delete_by_id[i]));
                    softAssert.assertTrue(delete_by_id.contains(Integer.parseInt(get_delete_by_id[i])),"does not contain the get_delete_by_id");
                }
            }
            else if (!delete_by_id.isEmpty() && delete_error.equals("true")) {
                log.info("List of delete_by_id --> "+delete_by_id);
                String[] get_failed_delete_by_id = JsonPath.read(body, "$..data..failed_delete_by_ids[*]").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                for (int i = 0; i < get_failed_delete_by_id.length ; i++) {
                    //handle empty case
                    if(!get_failed_delete_by_id[i].equals("")) {
                        log.info("search failed delete_by_id --> " + Integer.parseInt(get_failed_delete_by_id[i]));
                        softAssert.assertTrue(delete_by_id.contains(Integer.parseInt(get_failed_delete_by_id[i])), "does not contain the failed_delete_by_id");
                    }
                    else
                        softAssert.assertTrue(false,"get_failed_delete_by_id is empty");
                }
            }
            if(!delete_by_third_party.isEmpty() && delete_error.equals("false")){
                log.info("List of delete_by_third_party --> "+delete_by_third_party);
                softAssert.assertEquals(get_failed_delete_by_third_party_ids, "","get_failed_delete_by_third_party_ids not empty");
                String[] get_delete_by_tpi = JsonPath.read(body, "$..data.delete_by_third_party_ids").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                for (int i = 0; i < get_delete_by_tpi.length ; i++) {
                    log.info("search delete_by_third_party --> "+get_delete_by_tpi[i]);
                    softAssert.assertTrue((delete_by_third_party.contains(get_delete_by_tpi[i].trim())),"does not contain the get_delete_by_tpi");
                }
            }
            else if (!delete_by_third_party.isEmpty() && delete_error.equals("true")) {
                log.info("List of delete_by_third_party --> "+delete_by_third_party);
                String[] get_failed_delete_by_tpi = JsonPath.read(body, "$..data..failed_delete_by_third_party_ids[*]").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                for (int i = 0; i < get_failed_delete_by_tpi.length ; i++) {
                    log.info("search delete_by_third_party --> "+get_failed_delete_by_tpi[i]);
                    softAssert.assertTrue((delete_by_third_party.contains(get_failed_delete_by_tpi[i].trim())),"does not contain the failed_delete_by_tpi");
                }
            }

            if(create_error.equals("true")){
                String get_created_error_map = JsonPath.read(body, "$..data..created_error_map").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                String get_created_error_message = JsonPath.read(body, "$..data..created_error_map[*]").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                softAssert.assertNotNull(get_created_error_map);
                softAssert.assertTrue(baseServiceHelper.validateBulkErrorMessages(get_created_error_message),"created_error_map msg not same");
                softAssert.assertEquals(get_is_failed, "true","get_is_failed not true");

            }
            else if (create_error.equals("false") && update_error.equals("false") && delete_error.equals("false"))
                softAssert.assertEquals(get_is_failed, "false","get_is_failed not false");

            //TODO:FIX error message for updated when bug is fixed
            if(update_error.equals("true")) {
                String get_updated_error_map = JsonPath.read(body, "$..data..updated_error_map").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                String get_updated_error_message = JsonPath.read(body, "$..data..updated_error_map[*]").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                softAssert.assertNotNull(get_updated_error_map);
                softAssert.assertTrue(baseServiceHelper.validateBulkErrorMessages(get_updated_error_message),"updated_error_map msg not same");
                softAssert.assertEquals(get_is_failed, "true","get_is_failed not true");
            }
            else if(create_error.equals("false") && update_error.equals("false") && delete_error.equals("false"))
                softAssert.assertEquals(get_is_failed, "false","get_is_failed not false");

            if(delete_error.equals("true")) {
                softAssert.assertEquals(get_is_failed, "true","get_is_failed not true");
            }
            else if (create_error.equals("false") && update_error.equals("false") && delete_error.equals("false"))
                softAssert.assertEquals(get_is_failed, "false","get_is_failed not false");

        }
        else {
            softAssert.assertTrue(false,"StatusCode not 1");
        }


        softAssert.assertAll();
    }

    @Test(dataProvider = "bulkVariantGroupsDP",priority = 38,groups = {"BaseService", "aviral"}, enabled = true)
    public void bulkVariantGroups(String json,ArrayList<Integer> delete_by_id, ArrayList<String> delete_by_third_party,ArrayList<String> names,ArrayList<String> thirdParty, String message_to_validate, String create_error,String update_error,String delete_error) throws IOException {
        log.info("JSON ---> "+json);
        SoftAssert softAssert = new SoftAssert();
        String body = baseServiceHelper.bulkVariantGroupHelper(json).ResponseValidator.GetBodyAsText();
        String get_statusMessage = JsonPath.read(body, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        int statusCode = JsonPath.read(body,"$.statusCode");
        if(statusCode == 1) {
            String get_failed_delete_by_ids = JsonPath.read(body, "$..data..failed_delete_by_ids").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
            String get_failed_delete_by_third_party_ids = JsonPath.read(body, "$..data..failed_delete_by_third_party_ids").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
            String get_is_failed = JsonPath.read(body, "$..data..is_failed").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
            softAssert.assertEquals(statusCode,Constants.statusOne, "Status not 0");
            softAssert.assertEquals(get_statusMessage, message_to_validate, "Message not same");

            if(!names.isEmpty() && create_error.equals("false") && update_error.equals("false")){
                String[] get_names = JsonPath.read(body, "$..data..variant_groups..name").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                log.info("List of names --> "+names);
                for (int i = 0; i < get_names.length ; i++) {
                    log.info("search name --> "+get_names[i]);
                    softAssert.assertTrue((names.contains(get_names[i])),"does not contain the name");
                }
            }
            else if (!names.isEmpty() && (create_error.equals("true") || update_error.equals("true"))) {
                String[] get_failed_names = JsonPath.read(body, "$..data..failed_variant_groups..name").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                log.info("List of names --> "+names);
                for (int i = 0; i < get_failed_names.length ; i++) {
                    log.info("search failed name --> "+get_failed_names[i]);
                    softAssert.assertTrue((names.contains(get_failed_names[i])),"does not contain the failed name");
                }
            }
            if(!thirdParty.isEmpty() && create_error.equals("false") && update_error.equals("false")){
                String[] get_tpi = JsonPath.read(body, "$..data..variant_groups..third_party_id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                log.info("List of TPI --> "+thirdParty);
                for (int i = 0; i < get_tpi.length ; i++) {
                    log.info("search tpi --> "+get_tpi[i]);
                    softAssert.assertTrue((thirdParty.contains(get_tpi[i])),"does not contain the thirdparty id");
                }
            }
            else if (!thirdParty.isEmpty() && (create_error.equals("true") || update_error.equals("true"))) {
                String[] get_failed_tpi = JsonPath.read(body, "$..data..failed_variant_groups..third_party_id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                log.info("List of TPI --> "+thirdParty);
                for (int i = 0; i < get_failed_tpi.length ; i++) {
                    log.info("search failed tpi --> "+get_failed_tpi[i]);
                    softAssert.assertTrue((thirdParty.contains(get_failed_tpi[i])),"does not contain the failed thirdparty id");
                }
            }
            if(!delete_by_id.isEmpty() && delete_error.equals("false")){
                log.info("List of delete_by_id --> "+delete_by_id);
                softAssert.assertEquals(get_failed_delete_by_ids, "","get_failed_delete_by_ids not empty");
                String[] get_delete_by_id = JsonPath.read(body, "$..data.delete_by_ids").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                for (int i = 0; i < get_delete_by_id.length ; i++) {
                    log.info("search delete_by_id --> "+Integer.parseInt(get_delete_by_id[i]));
                    softAssert.assertTrue(delete_by_id.contains(Integer.parseInt(get_delete_by_id[i])),"does not contain the get_delete_by_id");
                }
            }
            else if (!delete_by_id.isEmpty() && delete_error.equals("true")) {
                log.info("List of delete_by_id --> "+delete_by_id);
                String[] get_failed_delete_by_id = JsonPath.read(body, "$..data..failed_delete_by_ids[*]").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                for (int i = 0; i < get_failed_delete_by_id.length ; i++) {
                    //handle empty case
                    if(!get_failed_delete_by_id[i].equals("")) {
                        log.info("search failed delete_by_id --> " + Integer.parseInt(get_failed_delete_by_id[i]));
                        softAssert.assertTrue(delete_by_id.contains(Integer.parseInt(get_failed_delete_by_id[i])), "does not contain the failed_delete_by_id");
                    }
                    else
                        softAssert.assertTrue(false,"get_failed_delete_by_id is empty");
                }
            }
            if(!delete_by_third_party.isEmpty() && delete_error.equals("false")){
                log.info("List of delete_by_third_party --> "+delete_by_third_party);
                softAssert.assertEquals(get_failed_delete_by_third_party_ids, "","get_failed_delete_by_third_party_ids not empty");
                String[] get_delete_by_tpi = JsonPath.read(body, "$..data.delete_by_third_party_ids").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                for (int i = 0; i < get_delete_by_tpi.length ; i++) {
                    log.info("search delete_by_third_party --> "+get_delete_by_tpi[i]);
                    softAssert.assertTrue((delete_by_third_party.contains(get_delete_by_tpi[i].trim())),"does not contain the get_delete_by_tpi");
                }
            }
            else if (!delete_by_third_party.isEmpty() && delete_error.equals("true")) {
                log.info("List of delete_by_third_party --> "+delete_by_third_party);
                String[] get_failed_delete_by_tpi = JsonPath.read(body, "$..data..failed_delete_by_third_party_ids[*]").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                for (int i = 0; i < get_failed_delete_by_tpi.length ; i++) {
                    log.info("search delete_by_third_party --> "+get_failed_delete_by_tpi[i]);
                    softAssert.assertTrue((delete_by_third_party.contains(get_failed_delete_by_tpi[i].trim())),"does not contain the failed_delete_by_tpi");
                }
            }

            if(create_error.equals("true")){
                String get_created_error_map = JsonPath.read(body, "$..data..created_error_map").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                String get_created_error_message = JsonPath.read(body, "$..data..created_error_map[*]").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                softAssert.assertNotNull(get_created_error_map);
                softAssert.assertTrue(baseServiceHelper.validateBulkErrorMessages(get_created_error_message),"created_error_map msg not same");
                softAssert.assertEquals(get_is_failed, "true","get_is_failed not true");

            }
            else if (create_error.equals("false") && update_error.equals("false") && delete_error.equals("false"))
                softAssert.assertEquals(get_is_failed, "false","get_is_failed not false");

            //TODO:FIX error message for updated when bug is fixed
            if(update_error.equals("true")) {
                String get_updated_error_map = JsonPath.read(body, "$..data..updated_error_map").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                String get_updated_error_message = JsonPath.read(body, "$..data..updated_error_map[*]").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                softAssert.assertNotNull(get_updated_error_map);
                softAssert.assertTrue(baseServiceHelper.validateBulkErrorMessages(get_updated_error_message),"updated_error_map msg not same");
                softAssert.assertEquals(get_is_failed, "true","get_is_failed not true");
            }
            else if(create_error.equals("false") && update_error.equals("false") && delete_error.equals("false"))
                softAssert.assertEquals(get_is_failed, "false","get_is_failed not false");

            if(delete_error.equals("true")) {
                softAssert.assertEquals(get_is_failed, "true","get_is_failed not true");
            }
            else if (create_error.equals("false") && update_error.equals("false") && delete_error.equals("false"))
                softAssert.assertEquals(get_is_failed, "false","get_is_failed not false");

        }
        else {
            softAssert.assertTrue(false,"StatusCode not 0 || 1");
        }


        softAssert.assertAll();
    }

    @Test(dataProvider = "bulkVariantsDP",priority = 39,groups = {"BaseService", "aviral"}, enabled = true)
    public void bulkVariants(String json,ArrayList<Integer> delete_by_id, ArrayList<String> delete_by_third_party,ArrayList<String> names,ArrayList<String> thirdParty, String message_to_validate, String create_error,String update_error,String delete_error) throws IOException {
        log.info("JSON ---> "+json);
        SoftAssert softAssert = new SoftAssert();
        String body = baseServiceHelper.bulkVariantHelper(json).ResponseValidator.GetBodyAsText();
        String get_statusMessage = JsonPath.read(body, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        int statusCode = JsonPath.read(body,"$.statusCode");
        if(statusCode == 1) {
            String get_failed_delete_by_ids = JsonPath.read(body, "$..data..failed_delete_by_ids").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
            String get_failed_delete_by_third_party_ids = JsonPath.read(body, "$..data..failed_delete_by_third_party_ids").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
            String get_is_failed = JsonPath.read(body, "$..data..is_failed").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
            softAssert.assertEquals(statusCode,Constants.statusOne, "Status not 0");
            softAssert.assertEquals(get_statusMessage, message_to_validate, "Message not same");

            if(!names.isEmpty() && create_error.equals("false") && update_error.equals("false")){
                String[] get_names = JsonPath.read(body, "$..data..variants..name").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                log.info("List of names --> "+names);
                for (int i = 0; i < get_names.length ; i++) {
                    log.info("search name --> "+get_names[i]);
                    softAssert.assertTrue((names.contains(get_names[i])),"does not contain the name");
                }
            }
            else if (!names.isEmpty() && (create_error.equals("true") || update_error.equals("true"))) {
                String[] get_failed_names = JsonPath.read(body, "$..data..failed_variants..name").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                log.info("List of names --> "+names);
                for (int i = 0; i < get_failed_names.length ; i++) {
                    log.info("search failed name --> "+get_failed_names[i]);
                    softAssert.assertTrue((names.contains(get_failed_names[i])),"does not contain the failed name");
                }
            }
            if(!thirdParty.isEmpty() && create_error.equals("false") && update_error.equals("false")){
                String[] get_tpi = JsonPath.read(body, "$..data..variants..third_party_id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                log.info("List of TPI --> "+thirdParty);
                for (int i = 0; i < get_tpi.length ; i++) {
                    log.info("search tpi --> "+get_tpi[i]);
                    softAssert.assertTrue((thirdParty.contains(get_tpi[i])),"does not contain the thirdparty id");
                }
            }
            else if (!thirdParty.isEmpty() && (create_error.equals("true") || update_error.equals("true"))) {
                String[] get_failed_tpi = JsonPath.read(body, "$..data..failed_variants..third_party_id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                log.info("List of TPI --> "+thirdParty);
                for (int i = 0; i < get_failed_tpi.length ; i++) {
                    log.info("search failed tpi --> "+get_failed_tpi[i]);
                    softAssert.assertTrue((thirdParty.contains(get_failed_tpi[i])),"does not contain the failed thirdparty id");
                }
            }
            if(!delete_by_id.isEmpty() && delete_error.equals("false")){
                log.info("List of delete_by_id --> "+delete_by_id);
                softAssert.assertEquals(get_failed_delete_by_ids, "","get_failed_delete_by_ids not empty");
                String[] get_delete_by_id = JsonPath.read(body, "$..data.delete_by_ids").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                for (int i = 0; i < get_delete_by_id.length ; i++) {
                    log.info("search delete_by_id --> "+Integer.parseInt(get_delete_by_id[i]));
                    softAssert.assertTrue(delete_by_id.contains(Integer.parseInt(get_delete_by_id[i])),"does not contain the get_delete_by_id");
                }
            }
            else if (!delete_by_id.isEmpty() && delete_error.equals("true")) {
                log.info("List of delete_by_id --> "+delete_by_id);
                String[] get_failed_delete_by_id = JsonPath.read(body, "$..data..failed_delete_by_ids[*]").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                for (int i = 0; i < get_failed_delete_by_id.length ; i++) {
                    //handle empty case
                    if(!get_failed_delete_by_id[i].equals("")) {
                        log.info("search failed delete_by_id --> " + Integer.parseInt(get_failed_delete_by_id[i]));
                        softAssert.assertTrue(delete_by_id.contains(Integer.parseInt(get_failed_delete_by_id[i])), "does not contain the failed_delete_by_id");
                    }
                    else
                        softAssert.assertTrue(false,"get_failed_delete_by_id is empty");
                }
            }
            if(!delete_by_third_party.isEmpty() && delete_error.equals("false")){
                log.info("List of delete_by_third_party --> "+delete_by_third_party);
                softAssert.assertEquals(get_failed_delete_by_third_party_ids, "","get_failed_delete_by_third_party_ids not empty");
                String[] get_delete_by_tpi = JsonPath.read(body, "$..data.delete_by_third_party_ids").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                for (int i = 0; i < get_delete_by_tpi.length ; i++) {
                    log.info("search delete_by_third_party --> "+get_delete_by_tpi[i]);
                    softAssert.assertTrue((delete_by_third_party.contains(get_delete_by_tpi[i].trim())),"does not contain the get_delete_by_tpi");
                }
            }
            else if (!delete_by_third_party.isEmpty() && delete_error.equals("true")) {
                log.info("List of delete_by_third_party --> "+delete_by_third_party);
                String[] get_failed_delete_by_tpi = JsonPath.read(body, "$..data..failed_delete_by_third_party_ids[*]").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                for (int i = 0; i < get_failed_delete_by_tpi.length ; i++) {
                    log.info("search delete_by_third_party --> "+get_failed_delete_by_tpi[i]);
                    softAssert.assertTrue((delete_by_third_party.contains(get_failed_delete_by_tpi[i].trim())),"does not contain the failed_delete_by_tpi");
                }
            }

            if(create_error.equals("true")){
                String get_created_error_map = JsonPath.read(body, "$..data..created_error_map").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                String get_created_error_message = JsonPath.read(body, "$..data..created_error_map[*]").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                softAssert.assertNotNull(get_created_error_map);
                softAssert.assertTrue(baseServiceHelper.validateBulkErrorMessages(get_created_error_message),"created_error_map msg not same");
                softAssert.assertEquals(get_is_failed, "true","get_is_failed not true");

            }
            else if (create_error.equals("false") && update_error.equals("false") && delete_error.equals("false"))
                softAssert.assertEquals(get_is_failed, "false","get_is_failed not false");

            //TODO:FIX error message for updated when bug is fixed
            if(update_error.equals("true")) {
                String get_updated_error_map = JsonPath.read(body, "$..data..updated_error_map").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                String get_updated_error_message = JsonPath.read(body, "$..data..updated_error_map[*]").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                softAssert.assertNotNull(get_updated_error_map);
                softAssert.assertTrue(baseServiceHelper.validateBulkErrorMessages(get_updated_error_message),"updated_error_map msg not same");
                softAssert.assertEquals(get_is_failed, "true","get_is_failed not true");
            }
            else if(create_error.equals("false") && update_error.equals("false") && delete_error.equals("false"))
                softAssert.assertEquals(get_is_failed, "false","get_is_failed not false");

            if(delete_error.equals("true")) {
                softAssert.assertEquals(get_is_failed, "true","get_is_failed not true");
            }
            else if (create_error.equals("false") && update_error.equals("false") && delete_error.equals("false"))
                softAssert.assertEquals(get_is_failed, "false","get_is_failed not false");

        }
        else {
            softAssert.assertTrue(false, "StatusCode not 1");
        }
        softAssert.assertAll();
    }

    @Test(dataProvider = "bulkCategoryDP",priority = 39,groups = {"BaseService", "aviral"}, enabled = true)
    public void bulkCategories(String json,ArrayList<Integer> delete_by_id, ArrayList<String> delete_by_third_party,ArrayList<String> names,ArrayList<String> thirdParty, String message_to_validate, String create_error,String update_error,String delete_error) throws IOException {
        log.info("JSON ---> "+json);
        SoftAssert softAssert = new SoftAssert();
        String body = baseServiceHelper.bulkCategoriesHelper(json).ResponseValidator.GetBodyAsText();
        String get_statusMessage = JsonPath.read(body, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        int statusCode = JsonPath.read(body,"$.statusCode");
        if(statusCode == 1) {
            String get_failed_delete_by_ids = JsonPath.read(body, "$..data..failed_delete_by_ids").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
            String get_failed_delete_by_third_party_ids = JsonPath.read(body, "$..data..failed_delete_by_third_party_ids").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
            String get_is_failed = JsonPath.read(body, "$..data..is_failed").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
            softAssert.assertEquals(statusCode,Constants.statusOne, "Status not 0");
            softAssert.assertEquals(get_statusMessage, message_to_validate, "Message not same");

            if(!names.isEmpty() && create_error.equals("false") && update_error.equals("false")){
                String[] get_names = JsonPath.read(body, "$..data..categories..name").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                log.info("List of names --> "+names);
                for (int i = 0; i < get_names.length ; i++) {
                    log.info("search name --> "+get_names[i]);
                    softAssert.assertTrue((names.contains(get_names[i])),"does not contain the name");
                }
            }
            else if (!names.isEmpty() && (create_error.equals("true") || update_error.equals("true"))) {
                String[] get_failed_names = JsonPath.read(body, "$..data..failed_categories..name").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                log.info("List of names --> "+names);
                for (int i = 0; i < get_failed_names.length ; i++) {
                    log.info("search failed name --> "+get_failed_names[i]);
                    softAssert.assertTrue((names.contains(get_failed_names[i])),"does not contain the failed name");
                }
            }
            if(!thirdParty.isEmpty() && create_error.equals("false") && update_error.equals("false")){
                String[] get_tpi = JsonPath.read(body, "$..data..categories..third_party_id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                log.info("List of TPI --> "+thirdParty);
                for (int i = 0; i < get_tpi.length ; i++) {
                    log.info("search tpi --> "+get_tpi[i]);
                    softAssert.assertTrue((thirdParty.contains(get_tpi[i])),"does not contain the thirdparty id");
                }
            }
            else if (!thirdParty.isEmpty() && (create_error.equals("true") || update_error.equals("true"))) {
                String[] get_failed_tpi = JsonPath.read(body, "$..data..failed_categories..third_party_id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                log.info("List of TPI --> "+thirdParty);
                for (int i = 0; i < get_failed_tpi.length ; i++) {
                    log.info("search failed tpi --> "+get_failed_tpi[i]);
                    softAssert.assertTrue((thirdParty.contains(get_failed_tpi[i])),"does not contain the failed thirdparty id");
                }
            }
            if(!delete_by_id.isEmpty() && delete_error.equals("false")){
                log.info("List of delete_by_id --> "+delete_by_id);
                softAssert.assertEquals(get_failed_delete_by_ids, "","get_failed_delete_by_ids not empty");
                String[] get_delete_by_id = JsonPath.read(body, "$..data.delete_by_ids").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                for (int i = 0; i < get_delete_by_id.length ; i++) {
                    log.info("search delete_by_id --> "+Integer.parseInt(get_delete_by_id[i]));
                    softAssert.assertTrue(delete_by_id.contains(Integer.parseInt(get_delete_by_id[i])),"does not contain the get_delete_by_id");
                }
            }
            else if (!delete_by_id.isEmpty() && delete_error.equals("true")) {
                log.info("List of delete_by_id --> "+delete_by_id);
                String[] get_failed_delete_by_id = JsonPath.read(body, "$..data..failed_delete_by_ids[*]").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                for (int i = 0; i < get_failed_delete_by_id.length ; i++) {
                    //handle empty case
                    if(!get_failed_delete_by_id[i].equals("")) {
                        log.info("search failed delete_by_id --> " + Integer.parseInt(get_failed_delete_by_id[i]));
                        softAssert.assertTrue(delete_by_id.contains(Integer.parseInt(get_failed_delete_by_id[i])), "does not contain the failed_delete_by_id");
                    }
                    else
                        softAssert.assertTrue(false,"get_failed_delete_by_id is empty");
                }
            }
            if(!delete_by_third_party.isEmpty() && delete_error.equals("false")){
                log.info("List of delete_by_third_party --> "+delete_by_third_party);
                softAssert.assertEquals(get_failed_delete_by_third_party_ids, "","get_failed_delete_by_third_party_ids not empty");
                String[] get_delete_by_tpi = JsonPath.read(body, "$..data.delete_by_third_party_ids").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                for (int i = 0; i < get_delete_by_tpi.length ; i++) {
                    log.info("search delete_by_third_party --> "+get_delete_by_tpi[i]);
                    softAssert.assertTrue((delete_by_third_party.contains(get_delete_by_tpi[i].trim())),"does not contain the get_delete_by_tpi");
                }
            }
            else if (!delete_by_third_party.isEmpty() && delete_error.equals("true")) {
                log.info("List of delete_by_third_party --> "+delete_by_third_party);
                String[] get_failed_delete_by_tpi = JsonPath.read(body, "$..data..failed_delete_by_third_party_ids[*]").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                for (int i = 0; i < get_failed_delete_by_tpi.length ; i++) {
                    log.info("search delete_by_third_party --> "+get_failed_delete_by_tpi[i]);
                    softAssert.assertTrue((delete_by_third_party.contains(get_failed_delete_by_tpi[i].trim())),"does not contain the failed_delete_by_tpi");
                }
            }

            if(create_error.equals("true")){
                String get_created_error_map = JsonPath.read(body, "$..data..created_error_map").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                String get_created_error_message = JsonPath.read(body, "$..data..created_error_map[*]").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                softAssert.assertNotNull(get_created_error_map);
                softAssert.assertTrue(baseServiceHelper.validateBulkErrorMessages(get_created_error_message),"created_error_map msg not same");
                softAssert.assertEquals(get_is_failed, "true","get_is_failed not true");

            }
            else if (create_error.equals("false") && update_error.equals("false") && delete_error.equals("false"))
                softAssert.assertEquals(get_is_failed, "false","get_is_failed not false");

            //TODO:FIX error message for updated when bug is fixed
            if(update_error.equals("true")) {
                String get_updated_error_map = JsonPath.read(body, "$..data..updated_error_map").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                String get_updated_error_message = JsonPath.read(body, "$..data..updated_error_map[*]").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                softAssert.assertNotNull(get_updated_error_map);
                softAssert.assertTrue(baseServiceHelper.validateBulkErrorMessages(get_updated_error_message),"updated_error_map msg not same");
                softAssert.assertEquals(get_is_failed, "true","get_is_failed not true");
            }
            else if(create_error.equals("false") && update_error.equals("false") && delete_error.equals("false"))
                softAssert.assertEquals(get_is_failed, "false","get_is_failed not false");

            if(delete_error.equals("true")) {
                softAssert.assertEquals(get_is_failed, "true","get_is_failed not true");
            }
            else if (create_error.equals("false") && update_error.equals("false") && delete_error.equals("false"))
                softAssert.assertEquals(get_is_failed, "false","get_is_failed not false");

        }
        else {
            softAssert.assertTrue(false, "StatusCode not 1");
        }
        softAssert.assertAll();
    }

    @Test(dataProvider = "bulkItemsDP",priority = 40,groups = {"BaseService", "aviral"}, enabled = true)
    public void bulkItems(String json,ArrayList<Integer> delete_by_id, ArrayList<String> delete_by_third_party,ArrayList<String> names,ArrayList<String> thirdParty, String message_to_validate, String create_error,String update_error,String delete_error) throws IOException {
        log.info("JSON ---> "+json);
        SoftAssert softAssert = new SoftAssert();
        String body = baseServiceHelper.bulkItemsHelper(json).ResponseValidator.GetBodyAsText();
        String get_statusMessage = JsonPath.read(body, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        int statusCode = JsonPath.read(body,"$.statusCode");
        if(statusCode == 1) {
            String get_failed_delete_by_ids = JsonPath.read(body, "$..data..failed_delete_by_ids").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
            String get_failed_delete_by_third_party_ids = JsonPath.read(body, "$..data..failed_delete_by_third_party_ids").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
            String get_is_failed = JsonPath.read(body, "$..data..is_failed").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
            softAssert.assertEquals(statusCode,Constants.statusOne, "Status not 0");
            softAssert.assertEquals(get_statusMessage, message_to_validate, "Message not same");

            if(!names.isEmpty() && create_error.equals("false") && update_error.equals("false")){
                String[] get_names = JsonPath.read(body, "$..data..items..name").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                log.info("List of names --> "+names);
                for (int i = 0; i < get_names.length ; i++) {
                    log.info("search name --> "+get_names[i]);
                    softAssert.assertTrue((names.contains(get_names[i])),"does not contain the name");
                }
            }
            else if (!names.isEmpty() && (create_error.equals("true") || update_error.equals("true"))) {
                String[] get_failed_names = JsonPath.read(body, "$..data..failed_items..name").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                log.info("List of names --> "+names);
                for (int i = 0; i < get_failed_names.length ; i++) {
                    log.info("search failed name --> "+get_failed_names[i]);
                    softAssert.assertTrue((names.contains(get_failed_names[i])),"does not contain the failed name");
                }
            }
            if(!thirdParty.isEmpty() && create_error.equals("false") && update_error.equals("false")){
                String[] get_tpi = JsonPath.read(body, "$..data..items..third_party_id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                log.info("List of TPI --> "+thirdParty);
                for (int i = 0; i < get_tpi.length ; i++) {
                    log.info("search tpi --> "+get_tpi[i]);
                    softAssert.assertTrue((thirdParty.contains(get_tpi[i])),"does not contain the thirdparty id");
                }
            }
            else if (!thirdParty.isEmpty() && (create_error.equals("true") || update_error.equals("true"))) {
                String[] get_failed_tpi = JsonPath.read(body, "$..data..failed_items..third_party_id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                log.info("List of TPI --> "+thirdParty);
                for (int i = 0; i < get_failed_tpi.length ; i++) {
                    log.info("search failed tpi --> "+get_failed_tpi[i]);
                    softAssert.assertTrue((thirdParty.contains(get_failed_tpi[i])),"does not contain the failed thirdparty id");
                }
            }
            if(!delete_by_id.isEmpty() && delete_error.equals("false")){
                log.info("List of delete_by_id --> "+delete_by_id);
                softAssert.assertEquals(get_failed_delete_by_ids, "","get_failed_delete_by_ids not empty");
                String[] get_delete_by_id = JsonPath.read(body, "$..data.delete_by_ids").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                for (int i = 0; i < get_delete_by_id.length ; i++) {
                    //handle empty case
                    if(!get_delete_by_id[i].equals("")) {
                        log.info("search delete_by_id --> " + Integer.parseInt(get_delete_by_id[i]));
                        softAssert.assertTrue(delete_by_id.contains(Integer.parseInt(get_delete_by_id[i])), "does not contain the get_delete_by_id");
                    }
                    else
                        softAssert.assertTrue(false, "empty value found in deletebyID");
                }
            }
            else if (!delete_by_id.isEmpty() && delete_error.equals("true")) {
                log.info("List of delete_by_id --> "+delete_by_id);
                String[] get_failed_delete_by_id = JsonPath.read(body, "$..data..failed_delete_by_ids[*]").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                for (int i = 0; i < get_failed_delete_by_id.length ; i++) {
                    //handle empty case
                    if(!get_failed_delete_by_id[i].equals("")) {
                        log.info("search failed delete_by_id --> " + Integer.parseInt(get_failed_delete_by_id[i]));
                        softAssert.assertTrue(delete_by_id.contains(Integer.parseInt(get_failed_delete_by_id[i])), "does not contain the failed_delete_by_id");
                    }
                    else
                        softAssert.assertTrue(false,"get_failed_delete_by_id is empty");
                }
            }
            if(!delete_by_third_party.isEmpty() && delete_error.equals("false")){
                log.info("List of delete_by_third_party --> "+delete_by_third_party);
                softAssert.assertEquals(get_failed_delete_by_third_party_ids, "","get_failed_delete_by_third_party_ids not empty");
                String[] get_delete_by_tpi = JsonPath.read(body, "$..data.delete_by_third_party_ids").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                for (int i = 0; i < get_delete_by_tpi.length ; i++) {
                    log.info("search delete_by_third_party --> "+get_delete_by_tpi[i]);
                    softAssert.assertTrue((delete_by_third_party.contains(get_delete_by_tpi[i].trim())),"does not contain the get_delete_by_tpi");
                }
            }
            else if (!delete_by_third_party.isEmpty() && delete_error.equals("true")) {
                log.info("List of delete_by_third_party --> "+delete_by_third_party);
                String[] get_failed_delete_by_tpi = JsonPath.read(body, "$..data..failed_delete_by_third_party_ids[*]").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                for (int i = 0; i < get_failed_delete_by_tpi.length ; i++) {
                    log.info("search delete_by_third_party --> "+get_failed_delete_by_tpi[i]);
                    softAssert.assertTrue((delete_by_third_party.contains(get_failed_delete_by_tpi[i].trim())),"does not contain the failed_delete_by_tpi");
                }
            }

            if(create_error.equals("true")){
                String get_created_error_map = JsonPath.read(body, "$..data..created_error_map").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                String get_created_error_message = JsonPath.read(body, "$..data..created_error_map[*]").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                softAssert.assertNotNull(get_created_error_map);
                softAssert.assertEquals(baseServiceHelper.validateBulkErrorMessages(get_created_error_message),"created_error_map msg not same");
                softAssert.assertEquals(get_is_failed, "true","get_is_failed not true");

            }
            else if (create_error.equals("false") && update_error.equals("false") && delete_error.equals("false"))
                softAssert.assertEquals(get_is_failed, "false","get_is_failed not false");

            //TODO:FIX error message for updated when bug is fixed
            if(update_error.equals("true")) {
                String get_updated_error_map = JsonPath.read(body, "$..data..updated_error_map").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                String get_updated_error_message = JsonPath.read(body, "$..data..updated_error_map[*]").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                softAssert.assertNotNull(get_updated_error_map);
                softAssert.assertEquals(baseServiceHelper.validateBulkErrorMessages(get_updated_error_message),"updated_error_map msg not same");
                softAssert.assertEquals(get_is_failed, "true","get_is_failed not true");
            }
            else if(create_error.equals("false") && update_error.equals("false") && delete_error.equals("false"))
                softAssert.assertEquals(get_is_failed, "false","get_is_failed not false");

            if(delete_error.equals("true")) {
                softAssert.assertEquals(get_is_failed, "true","get_is_failed not true");
            }
            else if (create_error.equals("false") && update_error.equals("false") && delete_error.equals("false"))
                softAssert.assertEquals(get_is_failed, "false","get_is_failed not false");

        }
        else {
            softAssert.assertTrue(false, "StatusCode not 1");
        }
        softAssert.assertAll();
    }


    @Test(dataProvider = "getMenuDP",priority = 41,groups = {"BaseService", "aviral"}, enabled = true)
    public void getMenu(ArrayList<String> to_validate) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        System.out.println("to_validate ::: "+to_validate);
        //to_validate: restID, catID, subCatID,ItemID, VGID, variantID, CategoryName, SubCategoryName, itemName, VGName, VariantName, item instock, variant instock
        String restID = String.valueOf(restaurantId);
        String body = baseServiceHelper.getMenuhelper(restID,to_validate.get(13)).ResponseValidator.GetBodyAsText();
        String bodyCategories = JsonPath.read(body,"$..categories[?(@.id=="+to_validate.get(1)+")]").toString();
        String bodySubCategories = JsonPath.read(body,"$..categories[?(@.id=="+to_validate.get(1)+")]..subCategories[?(@.id=="+to_validate.get(2)+")]").toString();
        String bodyItems = JsonPath.read(body,"$..categories[?(@.id=="+to_validate.get(1)+")]"+"..subCategories[?(@.id=="+to_validate.get(2)+")]..menu[?(@.id=="+to_validate.get(3)+")]").toString();
        String bodyVG = JsonPath.read(body,"$..categories[?(@.id=="+to_validate.get(1)+")]"+"..subCategories[?(@.id=="+to_validate.get(2)+")]..menu[?(@.id=="+to_validate.get(3)+")]..variant_groups[?(@.group_id=="+to_validate.get(4)+")]").toString();
        String bodyV = JsonPath.read(body,"$..categories[?(@.id=="+to_validate.get(1)+")]"+"..subCategories[?(@.id=="+to_validate.get(2)+")]..menu[?(@.id=="+to_validate.get(3)+")].variants_new.variant_groups[?(@.group_id=="+to_validate.get(4)+")]..variations[?(@.id=="+to_validate.get(5)+")]").toString();

        //debugger
        log.info("bodyCategories -- >" +bodyCategories);
        log.info("bodySubCategories -- > "+bodySubCategories);
        log.info("bodyItems -- > "+bodyItems);
        log.info("bodyVG -- > "+bodyVG);
        log.info("bodyV -- > "+bodyV);

        String get_statusMessage = JsonPath.read(body, "$..statusMessage").toString().replace("[","").replace("]","").replace("\"","").trim();
        int statusCode = JsonPath.read(body,"$.statusCode");

        if(statusCode == 1) {
            if (to_validate.get(4).equals("null") && to_validate.get(5).equals("null") && to_validate.get(12).equals("null")) {
                String get_restID = JsonPath.read(body, "$..data.id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                String get_catID = JsonPath.read(body, "$..categories[?(@.id=="+to_validate.get(1)+")].id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                String get_catID_name = JsonPath.read(body, "$..categories[?(@.id=="+to_validate.get(1)+")].name").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                String get_subcatID = JsonPath.read(body, "$..categories[?(@.id=="+to_validate.get(1)+")]..subCategories[?(@.id=="+to_validate.get(2)+")].id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                String get_subcatID_name = JsonPath.read(body, "$..categories[?(@.id=="+to_validate.get(1)+")]..subCategories[?(@.id=="+to_validate.get(2)+")].name").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                String get_ItemID = JsonPath.read(body, "$..categories[?(@.id=="+to_validate.get(1)+")]"+"..subCategories[?(@.id=="+to_validate.get(2)+")]..menu[?(@.id=="+to_validate.get(3)+")].id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                String get_ItemID_name = JsonPath.read(body, "$..categories[?(@.id=="+to_validate.get(1)+")]"+"..subCategories[?(@.id=="+to_validate.get(2)+")]..menu[?(@.id=="+to_validate.get(3)+")].name").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                String get_ItemID_instock = JsonPath.read(body, "$..categories[?(@.id=="+to_validate.get(1)+")]"+"..subCategories[?(@.id=="+to_validate.get(2)+")]..menu[?(@.id=="+to_validate.get(3)+")].inStock").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                String[] get_all_inStock_items = JsonPath.read(body, "$..categories[?(@.id=="+to_validate.get(1)+")]"+"..subCategories[?(@.id=="+to_validate.get(2)+")]..menu[*]..inStock").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                String[] get_all_type_items = JsonPath.read(body, "$..categories[?(@.id=="+to_validate.get(1)+")]"+"..subCategories[?(@.id=="+to_validate.get(2)+")]..menu[*]..type").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                log.info("get_catID - "+get_catID+" get_catID_name - "+get_catID_name+" get_subcatID - "+get_subcatID+" get_subcatID_name - "+get_subcatID_name+" get_ItemID - "+get_ItemID+" get_ItemID_name - "+get_ItemID_name );
                softAssert.assertEquals(get_restID,to_validate.get(0),"restaurant id not the same");
                softAssert.assertEquals(get_catID,to_validate.get(1),"catid not same");
                softAssert.assertEquals(get_subcatID,to_validate.get(2),"catid not same");
                softAssert.assertEquals(get_ItemID,to_validate.get(3),"catid not same");
                softAssert.assertEquals(get_catID_name,to_validate.get(6),"cat name not same");
                softAssert.assertEquals(get_subcatID_name,to_validate.get(7),"subcat name not same");
                softAssert.assertEquals(get_ItemID_name,to_validate.get(8),"cat name not same");

                //iteminstock
                if(to_validate.get(11).equals("1")){
                    softAssert.assertTrue(baseServiceHelper.validateSameValueInArray(get_all_inStock_items,"1"),"all instock values not equal to 1");
                    softAssert.assertEquals(get_ItemID_instock,to_validate.get(11),"in_stock not the same");
                }
                else if (to_validate.get(11).equals("0")){
                    softAssert.assertTrue(baseServiceHelper.validateSameValueInArray(get_all_inStock_items,"0"),"all instock values not equal to 0");
                    softAssert.assertEquals(get_ItemID_instock,to_validate.get(11),"in_stock not the same");
                }
                //menuType
                if(to_validate.get(13).equals("REGULAR_MENU")) {
                    softAssert.assertTrue(baseServiceHelper.validateSameValueInArray(get_all_type_items,"REGULAR_ITEM"));
                }
                else if (to_validate.get(13).equals("POP_MENU")) {
                    softAssert.assertTrue(baseServiceHelper.validateSameValueInArray(get_all_type_items,"POP_ITEM"));
                }

            }
            else if (!to_validate.get(4).equals("null") && !to_validate.get(5).equals("null")) {
                String get_restID = JsonPath.read(body, "$..data.id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                String get_catID = JsonPath.read(body, "$..categories[?(@.id=="+to_validate.get(1)+")].id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                String get_catID_name = JsonPath.read(body, "$..categories[?(@.id=="+to_validate.get(1)+")].name").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                String get_subcatID = JsonPath.read(body, "$..categories[?(@.id=="+to_validate.get(1)+")]..subCategories[?(@.id=="+to_validate.get(2)+")].id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                String get_subcatID_name = JsonPath.read(body, "$..categories[?(@.id=="+to_validate.get(1)+")]..subCategories[?(@.id=="+to_validate.get(2)+")].name").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                String get_ItemID = JsonPath.read(body, "$..categories[?(@.id=="+to_validate.get(1)+")]"+"..subCategories[?(@.id=="+to_validate.get(2)+")]..menu[?(@.id=="+to_validate.get(3)+")].id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                String get_ItemID_name = JsonPath.read(body, "$..categories[?(@.id=="+to_validate.get(1)+")]"+"..subCategories[?(@.id=="+to_validate.get(2)+")]..menu[?(@.id=="+to_validate.get(3)+")].name").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                String get_ItemID_instock = JsonPath.read(body, "$..categories[?(@.id=="+to_validate.get(1)+")]"+"..subCategories[?(@.id=="+to_validate.get(2)+")]..menu[?(@.id=="+to_validate.get(3)+")].inStock").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                String get_VGID = JsonPath.read(body, "$..categories[?(@.id=="+to_validate.get(1)+")]"+"..subCategories[?(@.id=="+to_validate.get(2)+")]..menu[?(@.id=="+to_validate.get(3)+")].variants_new.variant_groups[?(@.group_id=="+to_validate.get(4)+")].group_id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                String get_VGID_name = JsonPath.read(body, "$..categories[?(@.id=="+to_validate.get(1)+")]"+"..subCategories[?(@.id=="+to_validate.get(2)+")]..menu[?(@.id=="+to_validate.get(3)+")].variants_new.variant_groups[?(@.group_id=="+to_validate.get(4)+")].name").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                String get_variantID = JsonPath.read(body, "$..categories[?(@.id=="+to_validate.get(1)+")]"+"..subCategories[?(@.id=="+to_validate.get(2)+")]..menu[?(@.id=="+to_validate.get(3)+")].variants_new.variant_groups[?(@.group_id=="+to_validate.get(4)+")].variations[?(@.id=="+to_validate.get(5)+")].id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                String get_variantID_name = JsonPath.read(body, "$..categories[?(@.id=="+to_validate.get(1)+")]"+"..subCategories[?(@.id=="+to_validate.get(2)+")]..menu[?(@.id=="+to_validate.get(3)+")].variants_new.variant_groups[?(@.group_id=="+to_validate.get(4)+")].variations[?(@.id=="+to_validate.get(5)+")].name").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                String get_variantID_inStock = JsonPath.read(body, "$..categories[?(@.id=="+to_validate.get(1)+")]"+"..subCategories[?(@.id=="+to_validate.get(2)+")]..menu[?(@.id=="+to_validate.get(3)+")].variants_new.variant_groups[?(@.group_id=="+to_validate.get(4)+")].variations[?(@.id=="+to_validate.get(5)+")].inStock").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
                String[] get_all_inStock_items = JsonPath.read(body, "$..categories[?(@.id=="+to_validate.get(1)+")]"+"..subCategories[?(@.id=="+to_validate.get(2)+")]..menu[*]..inStock").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                String[] get_all_inStock_variants = JsonPath.read(body, "$..categories[?(@.id=="+to_validate.get(1)+")]"+"..subCategories[?(@.id=="+to_validate.get(2)+")]..menu[?(@.id=="+to_validate.get(3)+")].variants_new.variant_groups[?(@.group_id=="+to_validate.get(4)+")]..variations[*]..inStock").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                String[] get_all_type_items = JsonPath.read(body, "$..categories[?(@.id=="+to_validate.get(1)+")]"+"..subCategories[?(@.id=="+to_validate.get(2)+")]..menu[*]..type").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim().split(",");
                log.info("get_catID - "+get_catID+" get_catID_name - "+get_catID_name+" get_subcatID - "+get_subcatID+" get_subcatID_name - "+get_subcatID_name+" get_ItemID - "+get_ItemID+" get_ItemID_name - "+get_ItemID_name+" get_VGID - "+get_VGID + " get_VGID_name - "+get_VGID_name+" get_variantID - "+get_variantID + " get_variantID_name - "+get_variantID_name );
                softAssert.assertEquals(get_restID,to_validate.get(0),"restaurant id not the same");
                softAssert.assertEquals(get_catID,to_validate.get(1),"catid not same");
                softAssert.assertEquals(get_subcatID,to_validate.get(2),"catid not same");
                softAssert.assertEquals(get_ItemID,to_validate.get(3),"catid not same");
                softAssert.assertEquals(get_catID_name,to_validate.get(6),"cat name not same");
                softAssert.assertEquals(get_subcatID_name,to_validate.get(7),"subcat name not same");
                softAssert.assertEquals(get_ItemID_name,to_validate.get(8),"cat name not same");

                //iteminstock
                if(to_validate.get(11).equals("1")){
                    softAssert.assertTrue(baseServiceHelper.validateSameValueInArray(get_all_inStock_items,"1"),"all instock values not equal to 1");
                    softAssert.assertEquals(get_ItemID_instock,to_validate.get(11),"in_stock not the same");
                }
                else if (to_validate.get(11).equals("0")){
                    softAssert.assertTrue(baseServiceHelper.validateSameValueInArray(get_all_inStock_items,"0"),"all item instock values not equal to 0");
                    softAssert.assertEquals(get_ItemID_instock,to_validate.get(11),"in_stock not the same");
                }

                //variantInstock
                if(to_validate.get(12).equals("1")){
                    softAssert.assertTrue(baseServiceHelper.validateSameValueInArray(get_all_inStock_variants,"1"),"all variant instock values not equal to 1");
                    softAssert.assertEquals(get_variantID_inStock,to_validate.get(12),"in_stock not the same");
                }
                else if (to_validate.get(12).equals("0")){
                    softAssert.assertTrue(baseServiceHelper.validateSameValueInArray(get_all_inStock_variants,"0"),"all variant instock values not equal to 0");
                    softAssert.assertEquals(get_variantID_inStock,to_validate.get(12),"in_stock not the same");
                }
                //menuType
                if(to_validate.get(13).equals("REGULAR_MENU")) {
                    softAssert.assertTrue(baseServiceHelper.validateSameValueInArray(get_all_type_items,"REGULAR_ITEM"));
                }
                else if (to_validate.get(13).equals("POP_MENU")) {
                    softAssert.assertTrue(baseServiceHelper.validateSameValueInArray(get_all_type_items,"POP_ITEM"));
                }
            }
        }
        else {
            softAssert.assertTrue(false, "status code not one");
        }

        softAssert.assertAll();

    }
    
    @Test(dataProvider = "itemoutofstock", description = "Test By passing invalid item_id in the param.")
    public void itemOutofStockInvalidItemID(String item_id,String from_time,String to_time,int expected_status_code,Object expected_status_message)
    {
    String response=baseServiceHelper.itemOutOfStock(item_id,from_time,to_time).ResponseValidator.GetBodyAsText();
    int status_Code=JsonPath.read(response,"$.statusCode");
    String status_message=JsonPath.read(response, "$.statusMessage");
    System.out.println("Status message is "+status_Code);
    System.out.println("Status message is "+status_message);
    if(status_Code==0)
    {
    	Assert.assertEquals(status_Code,expected_status_code);
    	if(!expected_status_message.toString().equalsIgnoreCase("null"))
    	{
    	Assert.assertEquals(status_message,expected_status_message.toString());
      }
    }
    else if(status_Code==1)
    {
    	String fm_time=JsonPath.read(response,"$.data.from_time");
    	String t_time=JsonPath.read(response,"$.data.to_time");
    	Assert.assertEquals(status_Code,expected_status_code);
        Assert.assertEquals(status_message,expected_status_message.toString());
        Assert.assertEquals(from_time, fm_time);
        Assert.assertEquals(to_time, t_time);
    }
     }
   


}
