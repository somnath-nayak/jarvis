package com.swiggy.api.erp.ff.tests.los;

import java.io.IOException;
import java.sql.SQLException;

import java.util.concurrent.TimeoutException;
import com.swiggy.api.erp.ff.dp.losDataProvider;
import org.apache.log4j.Logger;
import org.json.JSONException;

import org.testng.Assert;
import org.testng.annotations.Test;
import com.swiggy.api.erp.ff.helper.LOSHelper;

import framework.gameofthrones.Tyrion.RabbitMQHelper;

public class CreateOrderTests extends losDataProvider{

	LOSHelper losHelper = new LOSHelper();
	Logger log = Logger.getLogger(CreateOrderTests.class);

	@Test(dataProvider ="orderCreationData",groups = {"sanity", "regression"},description = "Create a manual order")
	public void testCreateOrderFromManualRestaurant(String order_id,String order_time,String epochTime) throws SQLException, IOException,JSONException {
		log.info("***************************************** testCreateOrderFromManualRestaurant started *****************************************");
		losHelper.createManualOrder(order_id, order_time, epochTime);
		Assert.assertTrue(losHelper.validateOrderExistInOMSOrderTable(order_id, 12), "Order is not present in oms_order table");
		log.info("######################################### testCreateOrderFromManualRestaurant completed #########################################");
	}

	@Test(dataProvider ="orderCreationData",groups = {"sanity", "regression"},description = "Create a Partner order")
	public void testCreateOrderFromPartnerRestaurant(String order_id,String order_time,String epochTime) throws SQLException, IOException,JSONException{
		log.info("***************************************** testCreateOrderFromPartnerRestaurant started *****************************************");
		losHelper.createOrderWithPartner(order_id, order_time, epochTime);
        Assert.assertTrue(losHelper.validateOrderExistInOMSOrderTable(order_id, 12), "Order is not present in oms_order table");
        log.info("######################################### testCreateOrderFromPartnerRestaurant completed #########################################");

	}

    @Test(dataProvider ="orderCreationData",groups = {"sanity", "regression"},description = "Create a ThirdParty Order")
	public void testCreateOrderFromThirdPartyRestaurant(String order_id,String order_time,String epochTime) throws SQLException, IOException, JSONException{
		log.info("***************************************** testCreateOrderFromThirdPartyRestaurant started *****************************************");
        String pushedOrderDetails = losHelper.createThirdPartyOrder(order_id, order_time, epochTime);
        Assert.assertTrue(losHelper.validateOrderExistInOMSOrderTable(order_id, 12), "Order is not present in oms_order table");
		log.info("######################################### testCreateOrderFromThirdPartyRestaurant completed #########################################");
	}
	
	
	@Test(dataProvider ="orderCreationData",groups = {"sanity", "regression"},description = "Create a LongDistance Order")
	public void testCreateLongDistanceOrder(String order_id,String order_time,String epochTime) throws IOException {
		log.info("***************************************** testCreateLongDistanceOrder started *****************************************");
        String pushedOrderDetails = losHelper.createLongDistanceOrder(order_id, order_time, epochTime);
        Assert.assertTrue(losHelper.validateOrderExistInOMSOrderTable(order_id, 12), "Order is not present in oms_order table");
		log.info("######################################### testCreateLongDistanceOrder completed #########################################");
	}
	
	@Test(dataProvider ="orderCreationData",groups = {"sanity", "regression"},description = "Create a Swiggy Assured Order")
	public void testCreateSwiggyAssuredOrder(String order_id,String order_time,String epochTime) throws SQLException, IOException, JSONException{
		log.info("***************************************** testCreateSwiggyAssuredOrder started *****************************************");
        String pushedOrderDetails = losHelper.createSwiggyAssuredOrder(order_id, order_time, epochTime);
        Assert.assertTrue(losHelper.validateOrderExistInOMSOrderTable(order_id, 12), "Order is not present in oms_order table");
		log.info("######################################### testCreateSwiggyAssuredOrder completed #########################################");

	}
	
	@Test(dataProvider ="orderCreationData",groups = {"sanity", "regression"},description = "Create a NewUser First Order")
	public void testCreateNewUsersFirstOrder(String order_id,String order_time,String epochTime) throws SQLException, IOException, JSONException{
		log.info("***************************************** testCreateNewUserFirstOrder started *****************************************");
        String pushedOrderDetails = losHelper.createNewUsersFirstOrder(order_id, order_time, epochTime);
        Assert.assertTrue(losHelper.validateOrderExistInOMSOrderTable(order_id, 12), "Order is not present in oms_order table");
		log.info("######################################### testCreateNewUserFirstOrder completed #########################################");
	}


	@Test(dataProvider ="orderCreationData",groups = {"sanity", "regression"},description = "Create a Dominos Order")
	public void testCreateDominoesOrder(String order_id,String order_time,String epochTime) throws SQLException, IOException, JSONException{
		log.info("************* testCreateDominosOrder started **No Need to RUN Delivery Status updates******************************************");

		String pushedOrderDetails = losHelper.createDominosOrder(order_id, order_time, epochTime);
        Assert.assertTrue(losHelper.validateOrderExistInOMSOrderTable(order_id, 12), "Order is not present in oms_order table");
		log.info("############## testCreateDominosOrder completed ## No Need to RUN Delivery Status updates#######################################");
	}

	@Test(dataProvider ="orderCreationData",groups = {"sanity", "regression"},description = "Create a POP Order")
	public void testCreatePOPOrder(String order_id,String order_time,String epochTime) throws SQLException, IOException, JSONException{
		log.info("***************************************** testCreatePOPOrder started *****************************************");
		String pushedOrderDetails=losHelper.createPOPOrder(order_id, order_time, epochTime);
        Assert.assertTrue(losHelper.validateOrderExistInOMSOrderTable(order_id, 12), "Order is not present in oms_order table");
		log.info("######################################### testCreatePOPOrder completed #########################################");
	}

	/*
    ################################# Negative scenarios #################################
    */
	
	@Test(dataProvider ="orderCreationData",groups = {"sanity", "regression"},description = "Try Create Order With Wrong JSON")
	public void testCreateOrderWithWrongJson(String order_id,String order_time,String epochTime) throws IOException, SQLException, TimeoutException, InterruptedException{
		log.info("***************************************** testCreateOrderWithWrongjson started *****************************************");

		losHelper.createOrderWithWrongJson(order_id, order_time, epochTime);
        Assert.assertFalse(losHelper.validateOrderExistInOMSOrderTable(order_id, 12), "Order with wrong JSON has been created in OMS DB... Order_id is = "+ order_id +"");
//		Assert.assertEquals(rmqHelper.getMessage("oms", "ff-new-order-exchange-dead-letters"), pushedMessage, "Order is not present in dead-letter-queue");
		log.info("######################################### testCreateOrderWithWrongjson completed #########################################");
	}
	
	
}
