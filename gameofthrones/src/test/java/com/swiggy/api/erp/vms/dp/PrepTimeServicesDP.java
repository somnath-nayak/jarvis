package com.swiggy.api.erp.vms.dp;

import com.swiggy.api.erp.vms.constants.RMSConstants;
import com.swiggy.api.erp.vms.helper.PrepTimeServiceHelper;
import org.testng.annotations.DataProvider;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class PrepTimeServicesDP {

    private static PrepTimeServiceHelper helper = new PrepTimeServiceHelper();

    @DataProvider(name = "deleteSlotPrepTime")
    public static Iterator<Object[]> deleteSlotPrepTime() {
        helper.createPrepTimeForRest(RMSConstants.restaurantId, RMSConstants.prep_auth);
        String[] slots = helper.fetchSlotWiseId(RMSConstants.restaurantId);
        List<Object[]> obj = new ArrayList<>();
        for(int i=0;i<slots.length;i++)
            obj.add(new Object[]{slots[i],RMSConstants.preptimeMessage, true});
        obj.add(new Object[]{RMSConstants.invalidprepid, RMSConstants.invalid_prep_message, false});
        return obj.iterator();
    }

    @DataProvider(name = "deleteDayPrepTime")
    public static Iterator<Object[]> deleteDayPrepTime() {
        helper.createPrepTimeForRest(RMSConstants.restaurantId, RMSConstants.prep_auth);
        String[] slots = helper.fetchSlotWiseId(RMSConstants.restaurantId);
        List<Object[]> obj = new ArrayList<>();
        for(int i=0;i<slots.length;i++)
            obj.add(new Object[]{slots[i], RMSConstants.preptimeMessage, true});
        obj.add(new Object[]{RMSConstants.invalidprepid, RMSConstants.invalid_prep_message, false});
        return obj.iterator();
    }


    @DataProvider(name = "slotWisePrepTime")
    public static Iterator<Object[]> slotWisePrepTime() {
        List<Object[]> obj = new ArrayList<>();
        obj.add(new Object[]{ RMSConstants.prepTimeRestaurantId,RMSConstants.preptimeMessage, RMSConstants.prep_auth, true});
        obj.add(new Object[]{ RMSConstants.prepTimeRestaurantId,RMSConstants.auth_error, RMSConstants.invalid_auth, false});
        obj.add(new Object[]{RMSConstants.invalidprepid, RMSConstants.preptimeMessage, RMSConstants.prep_auth, true});
        obj.add(new Object[]{"",RMSConstants.method_not_allowed, RMSConstants.prep_auth, false});

        return obj.iterator();
    }

    @DataProvider(name = "slotWisePrepTimeID")
    public static Object[][] slotWisePrepTimeID() {

        return new Object[][] {};

    }

    @DataProvider(name = "dayWisePrepTime")
    public static Iterator<Object[]> dayWisePrepTime() {
        List<Object[]> obj = new ArrayList<>();
        obj.add(new Object[]{ RMSConstants.prepTimeRestaurantId,RMSConstants.preptimeMessage, RMSConstants.prep_auth, true});
        obj.add(new Object[]{ RMSConstants.prepTimeRestaurantId,RMSConstants.auth_error, RMSConstants.invalid_auth, false});
        obj.add(new Object[]{RMSConstants.invalidprepid, RMSConstants.preptimeMessage, RMSConstants.prep_auth, true});
        obj.add(new Object[]{"",RMSConstants.method_not_allowed, RMSConstants.prep_auth, false});

        return obj.iterator();
    }

    @DataProvider(name = "multiRestPrepTime")
    public static Object[][] multiRestPrepTime() {

        return new Object[][] {{ RMSConstants.prepTimeRestaurantId,RMSConstants.preptimeMessage, RMSConstants.prep_auth, true}};

    }

    @DataProvider(name = "savePrepTime")
    public static Iterator<Object[]> savepreptime() {
        String toDate, fromDate, days="";
        Date date = new Date();
        String[] rests = RMSConstants.restaurantIds;

        //Delete Restaurant Prep Time Slots before Creating new
        for(int i=0;i<rests.length;i++)
            helper.delPreptime(rests[i]);

        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        fromDate = dateFormat.format(-1);
        toDate = dateFormat.format(date);
        List<Object[]> obj = new ArrayList<>();
        for(int j=0;j<RMSConstants.restaurantIds.length;j++) {
            days = "\"" + RMSConstants.days[0] + "\"";
            for (int i = 1; i < RMSConstants.days.length; i++) {
                days += "," + "\"" + RMSConstants.days[i] + "\"";
                obj.add(new Object[]{RMSConstants.restaurantIds[j], fromDate.toString(), toDate.toString(), RMSConstants.prep_time, RMSConstants.PrepType.GLOBAL.toString(), days, RMSConstants.preptimeMessage,RMSConstants.prep_auth,  true});
                obj.add(new Object[]{RMSConstants.restaurantIds[j], fromDate.toString(), toDate.toString(), RMSConstants.prep_time, RMSConstants.PrepType.SLOTWISE.toString(), days, RMSConstants.preptimeMessage,RMSConstants.prep_auth, true});
                obj.add(new Object[]{RMSConstants.restaurantIds[j], fromDate.toString(), toDate.toString(), RMSConstants.prep_time, RMSConstants.PrepType.TEST.toString(), days, RMSConstants.errorstatus,RMSConstants.prep_auth, false});
            }
        }
        obj.add(new Object[]{RMSConstants.restaurantIds[0], fromDate.toString(), toDate.toString(), RMSConstants.prep_time, RMSConstants.PrepType.TEST.toString(), days, RMSConstants.auth_error,RMSConstants.invalid_auth, false});
        obj.add(new Object[]{RMSConstants.restaurantIds[0], fromDate.toString(), toDate.toString(), RMSConstants.invalid_preptime, RMSConstants.PrepType.SLOTWISE.toString(), days, RMSConstants.bad_request,RMSConstants.prep_auth, false});
        obj.add(new Object[]{RMSConstants.restaurantIds[0], fromDate.toString(), toDate.toString(), RMSConstants.prep_time, RMSConstants.PrepType.SLOTWISE.toString(), days + RMSConstants.invalid_day, RMSConstants.bad_request,RMSConstants.prep_auth, false});
        return obj.iterator();

    }

    @DataProvider(name = "savePrepTimeMulti")
    public static Iterator<Object[]> savePrepTimeMulti() {
        String toDate, fromDate, days = "", rests;
        Date date = new Date();

        String[] rest = RMSConstants.restaurantIds;

        //Delete Restaurant Prep Time Slots before Creating new
        for(int i=0;i<rest.length;i++)
            helper.delPreptime(rest[i]);

        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        fromDate = dateFormat.format(-1);
        toDate = dateFormat.format(date);
        List<Object[]> obj = new ArrayList<>();
        rests = RMSConstants.restaurantIds[0];
        for(int j=0;j<RMSConstants.restaurantIds.length;j++) {
            days = "\"" + RMSConstants.days[0] + "\"";
            for (int i = 1; i < RMSConstants.days.length; i++) {
                days += "," + "\"" + RMSConstants.days[i] + "\"";
                obj.add(new Object[]{rests, fromDate.toString(), toDate.toString(), RMSConstants.prep_time, RMSConstants.PrepType.GLOBAL.toString(), days, RMSConstants.preptimeMessage,RMSConstants.prep_auth, true,true });
                obj.add(new Object[]{rests, fromDate.toString(), toDate.toString(), RMSConstants.prep_time, RMSConstants.PrepType.SLOTWISE.toString(), days, RMSConstants.preptimeMessage,RMSConstants.prep_auth, true,true});
                obj.add(new Object[]{rests, fromDate.toString(), toDate.toString(), RMSConstants.prep_time, RMSConstants.PrepType.TEST.toString(), days, RMSConstants.errorstatus,RMSConstants.prep_auth, true,false});
            }
            if(j < RMSConstants.restaurantIds.length - 1)
                rests += "," + RMSConstants.restaurantIds[j+1];
        }
        obj.add(new Object[]{rests, fromDate.toString(), toDate.toString(), RMSConstants.prep_time, RMSConstants.PrepType.TEST.toString(), days, RMSConstants.auth_error,RMSConstants.invalid_auth, true, false});
        obj.add(new Object[]{rests, fromDate.toString(), toDate.toString(), RMSConstants.invalid_preptime, RMSConstants.PrepType.SLOTWISE.toString(), days, RMSConstants.bad_request,RMSConstants.prep_auth, true, false});
        obj.add(new Object[]{rests, fromDate.toString(), toDate.toString(), RMSConstants.prep_time, RMSConstants.PrepType.SLOTWISE.toString(), days + RMSConstants.invalid_day, RMSConstants.bad_request,RMSConstants.prep_auth, true, false});


        return obj.iterator();

    }

    @DataProvider(name = "updatepreptime")
    public static Iterator<Object[]> updatepreptime() {
        String toDate, fromDate, days="", rests;
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        fromDate = dateFormat.format(-1);
        toDate = dateFormat.format(date);
        helper.createPrepTimeForRest(RMSConstants.restaurantId, RMSConstants.prep_auth);
        String[] slotwise_slots = helper.fetchSlotWiseId(RMSConstants.restaurantId, RMSConstants.PrepType.SLOTWISE.toString());
        String[] global_slots = helper.fetchSlotWiseId(RMSConstants.restaurantId, RMSConstants.PrepType.GLOBAL.toString());
        List<Object[]> obj = new ArrayList<>();
        days = "\"" + RMSConstants.days[0] + "\"";
        for (int j = 1; j < RMSConstants.days.length; j++) {
            days += "," + "\"" + RMSConstants.days[j] + "\"";
            obj.add(new Object[]{fromDate.toString(), toDate.toString(), RMSConstants.prep_time, RMSConstants.PrepType.SLOTWISE.toString(), days, slotwise_slots[0], RMSConstants.preptimeMessage, true});
        }
        for (int j = 1; j < RMSConstants.days.length; j++) {
            days += "," + "\"" + RMSConstants.days[j] + "\"";
            obj.add(new Object[]{fromDate.toString(), toDate.toString(), RMSConstants.prep_time, RMSConstants.PrepType.GLOBAL.toString(), days, global_slots[0], RMSConstants.preptimeMessage, true});
        }
        obj.add(new Object[]{fromDate.toString(), toDate.toString(), RMSConstants.prep_time, RMSConstants.PrepType.GLOBAL.toString(), days, RMSConstants.invalidprepid, RMSConstants.preperrormessage, false});
        obj.add(new Object[]{fromDate.toString(), toDate.toString(), RMSConstants.prep_time, RMSConstants.PrepType.SLOTWISE.toString(), days, RMSConstants.invalidprepid, RMSConstants.preperrormessage, false});
        return obj.iterator();
    }

    @DataProvider(name = "fetchDayPrepTimeMulti")
    public static Iterator<Object[]> fetchDayPrepTimeMulti() {
        List<Object[]> obj = new ArrayList<>();
        String[] rests = RMSConstants.restaurantIds;
        String rest = "";
        for(int i=0;i<rests.length-1;i++)
            rest += rests[i] + ",";
        rest += rests[rests.length-1];
        obj.add(new Object[]{RMSConstants.prepTimeRestaurantId,RMSConstants.preptimeMessage, RMSConstants.prep_auth, true});
        obj.add(new Object[]{RMSConstants.prepTimeRestaurantId,RMSConstants.auth_error, RMSConstants.invalid_auth, false});
        obj.add(new Object[]{rest,RMSConstants.preptimeMessage, RMSConstants.prep_auth, true});
        obj.add(new Object[]{rest,RMSConstants.auth_error, RMSConstants.invalid_auth, false});
        return obj.iterator();

    }

    @DataProvider(name = "fetchSlotPrepTimeMulti")
    public static Iterator<Object[]> fetchSlotPrepTimeMulti() {
        List<Object[]> obj = new ArrayList<>();
        String[] rests = RMSConstants.restaurantIds;
        String rest = "";
        for(int i=0;i<rests.length-1;i++)
            rest += rests[i] + ",";
        rest += rests[rests.length-1];
        obj.add(new Object[]{RMSConstants.prepTimeRestaurantId,RMSConstants.preptimeMessage, RMSConstants.prep_auth, true});
        obj.add(new Object[]{RMSConstants.prepTimeRestaurantId,RMSConstants.auth_error, RMSConstants.invalid_auth, false});
        obj.add(new Object[]{rest,RMSConstants.preptimeMessage, RMSConstants.prep_auth, true});
        obj.add(new Object[]{rest,RMSConstants.auth_error, RMSConstants.invalid_auth, false});
        return obj.iterator();
    }

    @DataProvider(name = "index")
    public static Iterator<Object[]> index() {

        List<Object[]> obj = new ArrayList<>();
        obj.add(new Object[]{RMSConstants.preptimeMessage, RMSConstants.prep_auth, true});
        obj.add(new Object[]{RMSConstants.auth_error, RMSConstants.invalid_auth, false});

        return obj.iterator();

    }

    @DataProvider(name = "healthCheck")
    public static Iterator<Object[]> healthCheck() {

        List<Object[]> obj = new ArrayList<>();
        obj.add(new Object[]{RMSConstants.preptimeMessage, RMSConstants.prep_auth, true});
        obj.add(new Object[]{RMSConstants.auth_error, RMSConstants.invalid_auth, false});

        return obj.iterator();
    }

}
