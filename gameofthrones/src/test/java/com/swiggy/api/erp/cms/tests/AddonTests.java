package com.swiggy.api.erp.cms.tests;

import com.swiggy.api.erp.cms.dp.AddonDp;
import com.swiggy.api.erp.cms.helper.FullMenuHelper;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

/**
 * Created by kiran.j on 2/22/18.
 */
public class AddonTests extends AddonDp {

    @Test(dataProvider = "createaddon", description = "Verifies Different Scenarios for Creating Addons")
    public void createAddon(String rest_id, String json, String[] errors, String token_id) {
        System.out.println(json);
        SoftAssert softAssert = new SoftAssert();
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        Processor processor = fullMenuHelper.createAddon(rest_id, json, token_id);
        if(errors.length == 0)
            softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive(processor));
        else if(errors.length == 1)
            softAssert.assertTrue(processor.ResponseValidator.GetResponseCode() == Integer.parseInt(errors[0]));
        else
            softAssert.assertTrue(fullMenuHelper.validateErrors(processor, errors));
        softAssert.assertAll();
    }

    @Test(dataProvider = "updateaddon", description = "Verifies Different Scenarios for Updating Addons")
    public void updateAddon(String rest_id, String json, String[] errors, String token_id, String id) {
        System.out.println(json);
        SoftAssert softAssert = new SoftAssert();
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        Processor processor = fullMenuHelper.updateAddon(rest_id, json, token_id, id);
        if(errors.length == 0)
            softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive(processor));
        else if(errors.length == 1)
            softAssert.assertTrue(processor.ResponseValidator.GetResponseCode() == Integer.parseInt(errors[0]));
        else
            softAssert.assertTrue(fullMenuHelper.validateErrors(processor, errors));
        softAssert.assertAll();
    }

    @Test(dataProvider = "createbulkaddon", description = "Verifies Different Scenarios for Creating Bulk Addons")
    public void createBulkAddon(String rest_id, String json, String[] errors, String token_id) {
        SoftAssert softAssert = new SoftAssert();
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        Processor processor = fullMenuHelper.createBulkAddon(rest_id, json, token_id);
        if(errors.length == 0)
            softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive(processor));
        else if(errors.length == 1)
            softAssert.assertTrue(processor.ResponseValidator.GetResponseCode() == Integer.parseInt(errors[0]));
        else
            softAssert.assertTrue(fullMenuHelper.validateErrors(processor, errors));
        softAssert.assertAll();
    }

    @Test(dataProvider = "updatebulkaddon", description = "Verifies Different Scenarios for Updating Bulk Addons")
    public void updateBulkAddon(String rest_id, String json, String[] errors, String token_id) {
        SoftAssert softAssert = new SoftAssert();
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        Processor processor = fullMenuHelper.updateBulkAddon(rest_id, json, token_id);
        if(errors.length == 0)
            softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive(processor));
        else if(errors.length == 1)
            softAssert.assertTrue(processor.ResponseValidator.GetResponseCode() == Integer.parseInt(errors[0]));
        else
            softAssert.assertTrue(fullMenuHelper.validateErrors(processor, errors));
        softAssert.assertAll();
    }
}
