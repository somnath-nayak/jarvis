package com.swiggy.api.erp.delivery.helper;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.text.StrSubstitutor;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class AlchemistKafkaEventHelper {


    String NewOrderJSONTemplate=null;
    String OrderStatusUpdateJSONTemplate=null;
    String TaskSequenceJSONTemplate=null;
    String CancelOrderJSONTemplate=null;
    String RejectOrderJSONTemplate=null;
    String KafkaEventsJSONTemplate=null;
    String TicketingEventJSONTemplate=null;
    String NewOrderEventTypeValue="6";
    String OrderStatusUpdateEventTypeValue="1";
    String TaskSequencingEventTypeValue="16";
    String CancelOrderEventTypeValue="8";
    String TicketingEventTypeValue="14";

    HashMap<String,String> configMap= new HashMap<>();
    DeliveryServiceApiHelper deliveryServiceApiHelper= new DeliveryServiceApiHelper();
    HashMap<String, JSONObject> masterTCResultsJSONMap= new HashMap<String, JSONObject>();
    JSONObject configMapJSON = null;
    Map<String,String> defaultMap=null;
    StrSubstitutor strSubstitutor=null;

    public AlchemistKafkaEventHelper(){
        File NewOrderFile = new File("../Data/Payloads/JSON/alchemistNewOrderEventData.json");
        File OrderStatusFile = new File("../Data/Payloads/JSON/alchemistOrderStatusUpdateEventData.json");
        File TaskSeqFile = new File("../Data/Payloads/JSON/alchemistTaskSequenceEventData.json");
        File CancelOrderFile = new File("../Data/Payloads/JSON/alchemistCancelOrderEventData.json");
        File EventTemplateFile = new File("../Data/Payloads/JSON/alchemistKakfaEventJson.json");
        File RejectFile= new File("../Data/Payloads/JSON/alchemistRejectedStatusUpdateEventData.json");
        File TicketFile= new File("../Data/Payloads/JSON/alchemistTicketingEventData.json");

        try {
            NewOrderJSONTemplate = FileUtils.readFileToString(NewOrderFile);
            OrderStatusUpdateJSONTemplate=FileUtils.readFileToString(OrderStatusFile);
            TaskSequenceJSONTemplate=FileUtils.readFileToString(TaskSeqFile);
            CancelOrderJSONTemplate=FileUtils.readFileToString(CancelOrderFile);
            KafkaEventsJSONTemplate=FileUtils.readFileToString(EventTemplateFile);
            RejectOrderJSONTemplate=FileUtils.readFileToString(RejectFile);
            TicketingEventJSONTemplate=FileUtils.readFileToString(TicketFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String returnNewOrderJSONForAlchemistKafka(HashMap<String,String> CustomOrderJSONMap) {
        defaultMap=setOrderJsonValues();
        if(CustomOrderJSONMap.size()>0) {
            for (String str : CustomOrderJSONMap.keySet()) {
                if (defaultMap.containsKey(str))
                    defaultMap.put(str, CustomOrderJSONMap.get(str));
            }
        }
        createNewOrderIdandBatchIDGenerator();
        defaultMap.put("order_id",configMap.get("order_id"));
        return returnFinalKafkaEventJSON(StringSubstitutor(defaultMap,NewOrderJSONTemplate), Long.valueOf(NewOrderEventTypeValue));
    }

    public String returnOrderStatusUpdateJSONForAlchemistKafka(HashMap<String,String> CustomOrderStatusJSONMap) {
        defaultMap=setOrderStatusUpdateJsonValues();
        if(CustomOrderStatusJSONMap.size()>0) {
            for (String str : CustomOrderStatusJSONMap.keySet()) {
                if (defaultMap.containsKey(str))
                    defaultMap.put(str, CustomOrderStatusJSONMap.get(str));
            }
        }
        return returnFinalKafkaEventJSON(StringSubstitutor(defaultMap,OrderStatusUpdateJSONTemplate), Long.valueOf(OrderStatusUpdateEventTypeValue));
    }

    public String returnRejectedStatusUpdateJSONForAlchemistKafka(HashMap<String,String> CustomRejectedStatusJSONMap) {
        defaultMap=setRejectedStatusUpdateJsonValues();
        if(CustomRejectedStatusJSONMap.size()>0) {
            for (String str : CustomRejectedStatusJSONMap.keySet()) {
                if (defaultMap.containsKey(str))
                    defaultMap.put(str, CustomRejectedStatusJSONMap.get(str));
            }
        }
        return returnFinalKafkaEventJSON(StringSubstitutor(defaultMap,RejectOrderJSONTemplate), Long.valueOf(OrderStatusUpdateEventTypeValue));
    }

    public String returnOrderCancelJSONForAlchemistKafka() {
        defaultMap= new HashMap<>();
        defaultMap.put("order_id",getOrderId());
        defaultMap.put("de_id",getDEId());
        return returnFinalKafkaEventJSON(StringSubstitutor(defaultMap,CancelOrderJSONTemplate), Long.valueOf(CancelOrderEventTypeValue));
    }

    public String returnTaskSequenceJSONForAlchemistKafka(HashMap<String,String> CustomTaskSequenceJSONMap) {
        defaultMap=setTaskSequenceJsonValues();
        if(CustomTaskSequenceJSONMap.size()>0) {
            for (String str : CustomTaskSequenceJSONMap.keySet()) {
                if (defaultMap.containsKey(str))
                    defaultMap.put(str, CustomTaskSequenceJSONMap.get(str));
            }
        }
        return returnFinalKafkaEventJSON(StringSubstitutor(defaultMap,TaskSequenceJSONTemplate), Long.valueOf(TaskSequencingEventTypeValue));
    }

    public String returnFinalKafkaEventJSON(String event_data,Long event_type) {
        defaultMap = new HashMap<>();
        defaultMap.put("event_data",event_data);
        defaultMap.put("event_type", String.valueOf(event_type));
        return StringSubstitutor(defaultMap,KafkaEventsJSONTemplate);
    }

    public String returnTicketingJSONForAlchemistKafka(HashMap<String,String> CustomTicketJSONMap) {
        defaultMap=setTicketingJsonValues();
        if(CustomTicketJSONMap.size()>0) {
            for (String str : CustomTicketJSONMap.keySet()) {
                if (defaultMap.containsKey(str))
                    defaultMap.put(str, CustomTicketJSONMap.get(str));
            }
        }
        return returnFinalKafkaEventJSON(StringSubstitutor(defaultMap,TicketingEventJSONTemplate), Long.valueOf(TicketingEventTypeValue));
    }

    public void createNewOrderIdandBatchIDGenerator( ) {
        StringBuilder orderID = new StringBuilder("Test_Order_");
        String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date()).replaceAll("[.]","");
        orderID=orderID.append(timeStamp);
        configMap.put("batch_id",timeStamp);
        configMap.put("order_id",orderID.toString());
    }

    public String createNewDEIDGenerator(int zone_id) {
        String de_id=deliveryServiceApiHelper.createDEandmakeDEActive(zone_id);
        configMap.put("de_id",de_id);
        return de_id;
    }

    public Map<String, String> setOrderJsonValues()
    {
        Map<String, String> orderDefaultValues= new HashMap<>();
        orderDefaultValues.put("restaurant_id","8171");
        orderDefaultValues.put("prep_time","361122");
        orderDefaultValues.put("restaurant_lat_lng","12.915772,77.61001699999997");
        orderDefaultValues.put("restaurant_customer_distance","3.5");
        orderDefaultValues.put("is_long_distance","false");
        orderDefaultValues.put("restaurant_area_code","4");
        orderDefaultValues.put("order_time",getcurrentDateTimefororderTime());
        orderDefaultValues.put("order_type","regular");
        orderDefaultValues.put("lat","12.918621");
        orderDefaultValues.put("lng","77.610493");
        return orderDefaultValues;
    }

    public Map<String,String> setOrderStatusUpdateJsonValues(){
        Map<String, String> orderStatusDefaultValues= new HashMap<>();
        orderStatusDefaultValues.put("order_id",configMap.get("order_id"));
        orderStatusDefaultValues.put("batch_id",configMap.get("batch_id"));
        orderStatusDefaultValues.put("order_status","assigned");
        orderStatusDefaultValues.put("current_time",getcurrentDateTimefororderTime());
        orderStatusDefaultValues.put("de_id",configMap.get("de_id"));
        orderStatusDefaultValues.put("zone_id","4");
        orderStatusDefaultValues.put("rain_mode","0");
        orderStatusDefaultValues.put("rain_mode_type","3");
        orderStatusDefaultValues.put("batch_count","1");
        orderStatusDefaultValues.put("city_id","1");
        orderStatusDefaultValues.put("area_code","4");
        orderStatusDefaultValues.put("banner_factor","0.5");
        orderStatusDefaultValues.put("restaurant_id","8171");
        return orderStatusDefaultValues;
    }

    public Map<String,String> setRejectedStatusUpdateJsonValues(){
        Map<String, String> orderStatusDefaultValues= new HashMap<>();
        orderStatusDefaultValues.put("order_id",configMap.get("order_id"));
        orderStatusDefaultValues.put("batch_id",configMap.get("batch_id"));
        orderStatusDefaultValues.put("order_status","rejected");
        orderStatusDefaultValues.put("current_time",getcurrentDateTimefororderTime());
        orderStatusDefaultValues.put("de_id",configMap.get("de_id"));
        orderStatusDefaultValues.put("zone_id","4");
        orderStatusDefaultValues.put("rain_mode","0");
        orderStatusDefaultValues.put("rain_mode_type","3");
        orderStatusDefaultValues.put("batch_count","1");
        orderStatusDefaultValues.put("city_id","1");
        orderStatusDefaultValues.put("area_code","4");
        orderStatusDefaultValues.put("banner_factor","0.5");
        orderStatusDefaultValues.put("restaurant_id","8171");
        return orderStatusDefaultValues;
    }

    public Map<String,String> setTaskSequenceJsonValues(){
        Map<String, String> TaskSequenceDefaultValues= new HashMap<>();
        TaskSequenceDefaultValues.put("order_id",configMap.get("order_id"));
        TaskSequenceDefaultValues.put("batch_id",configMap.get("batch_id"));
        TaskSequenceDefaultValues.put("de_id",configMap.get("de_id"));
        TaskSequenceDefaultValues.put("zone_id","4");
        TaskSequenceDefaultValues.put("first_mile","10.0");
        TaskSequenceDefaultValues.put("last_mile","15.0");
        TaskSequenceDefaultValues.put("fmt","2.0");
        TaskSequenceDefaultValues.put("lmt","30.0");
        TaskSequenceDefaultValues.put("de_wait_time","3.0");
        TaskSequenceDefaultValues.put("batch_count","1");
        TaskSequenceDefaultValues.put("batch_type","1");
        return TaskSequenceDefaultValues;
    }

    public Map<String,String> setTicketingJsonValues(){
        Map<String, String> TicketingDefaultValues= new HashMap<>();
        TicketingDefaultValues.put("order_id",configMap.get("order_id"));
        TicketingDefaultValues.put("de_id",configMap.get("de_id"));
        TicketingDefaultValues.put("ticket_id",configMap.get("batch_id"));
//        TicketingDefaultValues.put("de_id",configMap.get("de_id"));
        TicketingDefaultValues.put("first_mile","14.0");
        TicketingDefaultValues.put("last_mile","10");
        TicketingDefaultValues.put("fmt","11.0");
        TicketingDefaultValues.put("lmt","20.0");
        TicketingDefaultValues.put("de_wait_time","30.0");
        return TicketingDefaultValues;
    }

    public String getcurrentDateTimefororderTime()
    {
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now = new Date();
        String strDate = sdfDate.format(now);
        return strDate;
    }

    public String getBatchId(){ return configMap.get("batch_id"); }

    public String getOrderId(){ return configMap.get("order_id"); }

    public String getDEId(){ return configMap.get("de_id"); }

    public String StringSubstitutor(Map<String,String> defaultMap, String Template){
        strSubstitutor = new StrSubstitutor(defaultMap);
        String resolvedString = strSubstitutor.replace(Template);
        return resolvedString;
    }

    public void convertTestCaseResultstoJSON(HashMap<String,String> RuleTCDataMap){
        RuleTCDataMap.put("de_id",getDEId());
        RuleTCDataMap.put("order_id",getOrderId());
        RuleTCDataMap.put("batch_id",getBatchId());
        configMapJSON = new JSONObject(RuleTCDataMap);
        masterTCResultsJSONMap.put(RuleTCDataMap.get("TC_ID"),configMapJSON);
        System.out.println("Test Case Result JSON :"+ configMapJSON.toString() );
    }

    public void printTestSuiteResultJSON(){
        System.out.println("Test Suite Result JSON :"+  masterTCResultsJSONMap.toString());
    }

}