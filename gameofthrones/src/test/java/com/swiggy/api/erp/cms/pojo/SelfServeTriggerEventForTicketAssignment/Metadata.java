package com.swiggy.api.erp.cms.pojo.SelfServeTriggerEventForTicketAssignment;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.erp.cms.pojo.SelfServeTriggerEventForTicketAssignment
 **/

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "emails",
        "phone_numbers"
})
public class Metadata {

    @JsonProperty("emails")
    private List<String> emails = null;
    @JsonProperty("phone_numbers")
    private String phone_numbers;

    @JsonProperty("emails")
    public List<String> getEmails() {
        return emails;
    }

    @JsonProperty("emails")
    public void setEmails(List<String> emails) {
        this.emails = emails;
    }

    @JsonProperty("phone_numbers")
    public String getPhone_numbers() {
        return phone_numbers;
    }

    @JsonProperty("phone_numbers")
    public void setPhone_numbers(String phone_numbers) {
        this.phone_numbers = phone_numbers;
    }

    private void setDefaultValues () {
        if (this.getEmails() == null)
            this.setEmails(new ArrayList<>());
        if (this.getPhone_numbers() == null)
            this.setPhone_numbers("0000000000");
    }

    public Metadata build() {
        setDefaultValues();
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("emails", emails).append("phone_numbers", phone_numbers).toString();
    }

}
