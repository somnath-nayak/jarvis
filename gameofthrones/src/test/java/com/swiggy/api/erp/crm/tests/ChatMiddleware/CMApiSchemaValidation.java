package com.swiggy.api.erp.crm.tests.ChatMiddleware;

import com.swiggy.api.erp.crm.dp.cancellationflows.CreateData;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Cersei.ToolBox;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import org.apache.commons.io.FileUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class CMApiSchemaValidation {

    Initialize gameofthrones = new Initialize();
    SchemaValidatorUtils schemaValidatorUtils=new SchemaValidatorUtils();

    @Test(dataProviderClass=CreateData.class, dataProvider = "user credentials", enabled=true,description = "")
    public void authenticateUser(HashMap<Integer,String[]> data) throws Exception
    {
        String[] userdata;

        HashMap<String, String> requestheaders_authentication = new HashMap<String, String>();

        for(int i=1;i<=data.size();i++) {

            userdata = data.get(i);
            requestheaders_authentication.put("Content-Type", "application/json");
            requestheaders_authentication.put("userId", userdata[0]);
            requestheaders_authentication.put("username", userdata[1]);

            GameOfThronesService authenticate_user = new GameOfThronesService("chatmiddlewear", "authenticate", gameofthrones);
            Processor authenticate_response = new Processor(authenticate_user, requestheaders_authentication);
            String response_body=authenticate_response.ResponseValidator.GetBodyAsText();


            int http_status = authenticate_response.ResponseValidator.GetResponseCode();

            Assert.assertEquals(http_status, 200);

            String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/SchemaSet/Json/CRM/cm_authenticate_schema.txt");
            List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,response_body);
            Assert.assertTrue(missingNodeList.isEmpty());
        }
    }

    @Test( dataProviderClass = CreateData.class, dataProvider = "user data", enabled = true,description = "")
    public void firstLevelIssues(HashMap<String, String[]> data, HashMap<String, HashMap<String, String[]>> nodes) throws Exception{
        String[] userdata;
        HashMap<String, String> requestheaders_issues = new HashMap<String, String>();

        for (int i = 1; i <= data.size(); i++) {
            userdata = data.get("user" + i);
            requestheaders_issues.put("Content-Type", "application/json");
            requestheaders_issues.put("userId", userdata[0]);
            System.out.println(userdata[0] + "  --------" + userdata[1]);
            requestheaders_issues.put("username", userdata[1]);

            GameOfThronesService firstlevel_issuenodes = new GameOfThronesService("chatmiddlewear", "firstLevelIssueNodes", gameofthrones);
            Processor firslevelIssueNodes_response = new Processor(firstlevel_issuenodes, requestheaders_issues);

            int http_status = firslevelIssueNodes_response.ResponseValidator.GetResponseCode();
            String response_body=firslevelIssueNodes_response.ResponseValidator.GetBodyAsText();

            Assert.assertEquals(http_status, 200);

            String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir") + "/../Data/SchemaSet/Json/CRM/cm_issues_schema.txt");
            List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,response_body);
            Assert.assertTrue(missingNodeList.isEmpty());

        }
    }

    @Test( dataProviderClass = CreateData.class, dataProvider = "user info",enabled = true,description = "Create Conversation")
    public void conversationCreate(HashMap<Integer,String[]> data) throws Exception
    {
        String[] userdata =data.get(1);

        HashMap<String, String> requestheaders_conversation = new HashMap<String, String>();
        requestheaders_conversation.put("Content-Type", "application/json");
        requestheaders_conversation.put("userId", userdata[0]);
        requestheaders_conversation.put("username", userdata[1]);

        String payloadparam[] = new String[3];
        payloadparam[0]=userdata[2];
        payloadparam[1]=userdata[3];
        payloadparam[2]=userdata[4];


        GameOfThronesService conversation_create = new GameOfThronesService("chatmiddlewear", "conversationCreate", gameofthrones);
        Processor conversation_response = new Processor(conversation_create, requestheaders_conversation, payloadparam);

        System.out.println("Response" +conversation_response);
        int http_status= conversation_response.ResponseValidator.GetResponseCode();
        System.out.println("status code is:"+http_status);

        String response_body=conversation_response.ResponseValidator.GetBodyAsText();

        String jsonschema = new ToolBox().readFileAsString(System.getProperty("user.dir")+"/../Data/SchemaSet/Json/CRM/cm_createConversation_schema.txt");

        List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,response_body);
        Assert.assertTrue(missingNodeList.isEmpty());


    }

}
