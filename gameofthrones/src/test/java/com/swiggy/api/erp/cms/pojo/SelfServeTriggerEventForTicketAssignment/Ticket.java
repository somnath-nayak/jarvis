package com.swiggy.api.erp.cms.pojo.SelfServeTriggerEventForTicketAssignment;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.erp.cms.pojo.SelfServeTriggerEventForTicketAssignment
 **/
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id"
})
public class Ticket {

    @JsonProperty("id")
    private Integer id;

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).toString();
    }

}
