package com.swiggy.api.erp.ff.tests.verificationService;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.swiggy.api.erp.ff.dp.verificationService.VerificationServiceApiTestData;
import com.swiggy.api.erp.ff.helper.LOSHelper;
import com.swiggy.api.erp.ff.helper.OMSHelper;
import com.swiggy.api.erp.ff.helper.VerificationServiceHelper;

import framework.gameofthrones.JonSnow.Processor;

public class VerificationServiceApiTests {
	
	VerificationServiceHelper helper = new VerificationServiceHelper();
	LOSHelper losHelper = new LOSHelper();
	
	
	@Test(dataProvider = "queueOrderToAssignVerifier", groups = {"sanity", "regression"},description = "Create a manual order and queue it for assigning verifier", dataProviderClass = VerificationServiceApiTestData.class)
	public void queueManualOrderForAssigningVerifierTest(String channel, String userValidation, String distanceValidation, String duplicateValidation) throws IOException, InterruptedException{
		System.out.println("***************************************** queueManualOrderForAssigningVerifierTest started *****************************************");
		
		Processor response = helper.queueForAssigningVerifier("manual", channel, userValidation, distanceValidation, duplicateValidation);
		Assert.assertEquals(response.ResponseValidator.GetResponseCode(), Integer.parseInt("200"), "Response code is not 200");
		Assert.assertEquals(response.ResponseValidator.GetNodeValue("data"), "Queued Request for Assigning Verifier");
		Assert.assertEquals(response.ResponseValidator.GetNodeValue("response.message"), "Success", "Actual message did not match expected message");
		Assert.assertEquals(response.ResponseValidator.GetNodeValueAsInt("response.code"), Integer.parseInt("200"), "Code in response body did not match with HTTP response code");
		
		System.out.println("######################################### queueManualOrderForAssigningVerifierTest compleated #########################################");
	}
	
	@Test(dataProvider = "queueOrderToAssignVerifier1", groups = {"sanity", "regression"},description = "Create a partner order and queue it for assigning verifier", dataProviderClass = VerificationServiceApiTestData.class)
	public void queuePartnerOrderForAssigningVerifierTest(String channel, String userValidation, String distanceValidation, String duplicateValidation) throws IOException, InterruptedException{
		System.out.println("***************************************** queuePartnerOrderForAssigningVerifierTest started *****************************************");
		
		Processor response = helper.queueForAssigningVerifier("partner", channel, userValidation, distanceValidation, duplicateValidation);
		Assert.assertEquals(response.ResponseValidator.GetResponseCode(), Integer.parseInt("200"), "Response code is not 200");
		Assert.assertEquals(response.ResponseValidator.GetNodeValue("data"), "Queued Request for Assigning Verifier");
		Assert.assertEquals(response.ResponseValidator.GetNodeValue("response.message"), "Success", "Actual message did not match expected message");
		Assert.assertEquals(response.ResponseValidator.GetNodeValueAsInt("response.code"), Integer.parseInt("200"), "Code in response body did not match with HTTP response code");
		
		System.out.println("######################################### queuePartnerOrderForAssigningVerifierTest compleated #########################################");
	}
	
	@Test(dataProvider = "queueOrderToAssignVerifier2", groups = {"sanity", "regression"},description = "Create a third party order and queue it for assigning verifier", dataProviderClass = VerificationServiceApiTestData.class)
	public void queueThirdPartyOrderForAssigningVerifierTest(String channel, String userValidation, String distanceValidation, String duplicateValidation) throws IOException, InterruptedException{
		System.out.println("***************************************** queueThirdPartyOrderForAssigningVerifierTest started *****************************************");
		
		Processor response = helper.queueForAssigningVerifier("thirdparty", channel, userValidation, distanceValidation, duplicateValidation);
		Assert.assertEquals(response.ResponseValidator.GetResponseCode(), Integer.parseInt("200"), "Response code is not 200");
		Assert.assertEquals(response.ResponseValidator.GetNodeValue("data"), "Queued Request for Assigning Verifier");
		Assert.assertEquals(response.ResponseValidator.GetNodeValue("response.message"), "Success", "Actual message did not match expected message");
		Assert.assertEquals(response.ResponseValidator.GetNodeValueAsInt("response.code"), Integer.parseInt("200"), "Code in response body did not match with HTTP response code");
		
		System.out.println("######################################### queueThirdPartyOrderForAssigningVerifierTest compleated #########################################");
	}
	
	@Test(dataProvider = "queueOrderToAssignVerifier3", groups = {"sanity", "regression"},description = "Create a swiggy pop order and queue it for assigning verifier", dataProviderClass = VerificationServiceApiTestData.class)
	public void queuePopOrderForAssigningVerifierTest(String channel, String userValidation, String distanceValidation, String duplicateValidation) throws IOException, InterruptedException{
		System.out.println("***************************************** queuePopOrderForAssigningVerifierTest started *****************************************");
		
		Processor response = helper.queueForAssigningVerifier("pop", channel, userValidation, distanceValidation, duplicateValidation);
		Assert.assertEquals(response.ResponseValidator.GetResponseCode(), Integer.parseInt("200"), "Response code is not 200");
		Assert.assertEquals(response.ResponseValidator.GetNodeValue("data"), "Queued Request for Assigning Verifier");
		Assert.assertEquals(response.ResponseValidator.GetNodeValue("response.message"), "Success", "Actual message did not match expected message");
		Assert.assertEquals(response.ResponseValidator.GetNodeValueAsInt("response.code"), Integer.parseInt("200"), "Code in response body did not match with HTTP response code");
		
		System.out.println("######################################### queuePopOrderForAssigningVerifierTest compleated #########################################");
	}
	
	@Test(dataProvider = "queueOrderToAssignVerifier4", groups = {"sanity", "regression"},description = "Create a swiggy assured order and queue it for assigning verifier", dataProviderClass = VerificationServiceApiTestData.class)
	public void queueSwiggyAssuredOrderForAssigningVerifierTest(String channel, String userValidation, String distanceValidation, String duplicateValidation) throws IOException, InterruptedException{
		System.out.println("***************************************** queueSwiggyAssuredOrderForAssigningVerifierTest started *****************************************");
		
		Processor response = helper.queueForAssigningVerifier("swiggyassured", channel, userValidation, distanceValidation, duplicateValidation);
		Assert.assertEquals(response.ResponseValidator.GetResponseCode(), Integer.parseInt("200"), "Response code is not 200");
		Assert.assertEquals(response.ResponseValidator.GetNodeValue("data"), "Queued Request for Assigning Verifier");
		Assert.assertEquals(response.ResponseValidator.GetNodeValue("response.message"), "Success", "Actual message did not match expected message");
		Assert.assertEquals(response.ResponseValidator.GetNodeValueAsInt("response.code"), Integer.parseInt("200"), "Code in response body did not match with HTTP response code");
		
		System.out.println("######################################### queueSwiggyAssuredOrderForAssigningVerifierTest compleated #########################################");
	}
	
	@Test(dataProvider = "queueOrderToAssignVerifier5", groups = {"sanity", "regression"},description = "Create a long distance order and queue it for assigning verifier", dataProviderClass = VerificationServiceApiTestData.class)
	public void queueLongDistanceOrderForAssigningVerifierTest(String channel, String userValidation, String distanceValidation, String duplicateValidation) throws IOException, InterruptedException{
		System.out.println("***************************************** queueLongDistanceOrderForAssigningVerifierTest started *****************************************");
		
		Processor response = helper.queueForAssigningVerifier("longdistance", channel, userValidation, distanceValidation, duplicateValidation);
		Assert.assertEquals(response.ResponseValidator.GetResponseCode(), Integer.parseInt("200"), "Response code is not 200");
		Assert.assertEquals(response.ResponseValidator.GetNodeValue("data"), "Queued Request for Assigning Verifier");
		Assert.assertEquals(response.ResponseValidator.GetNodeValue("response.message"), "Success", "Actual message did not match expected message");
		Assert.assertEquals(response.ResponseValidator.GetNodeValueAsInt("response.code"), Integer.parseInt("200"), "Code in response body did not match with HTTP response code");
		
		System.out.println("######################################### queueLongDistanceOrderForAssigningVerifierTest compleated #########################################");
	}
	
	@Test(dataProvider = "queueOrderToAssignVerifier6", groups = {"sanity", "regression"},description = "Create a new user order and queue it for assigning verifier", dataProviderClass = VerificationServiceApiTestData.class)
	public void queueNewUserOrderForAssigningVerifierTest(String channel, String userValidation, String distanceValidation, String duplicateValidation) throws IOException, InterruptedException{
		System.out.println("***************************************** queueNewUserOrderForAssigningVerifierTest started *****************************************");
		
		Processor response = helper.queueForAssigningVerifier("newuser", channel, userValidation, distanceValidation, duplicateValidation);
		Assert.assertEquals(response.ResponseValidator.GetResponseCode(), Integer.parseInt("200"), "Response code is not 200");
		Assert.assertEquals(response.ResponseValidator.GetNodeValue("data"), "Queued Request for Assigning Verifier");
		Assert.assertEquals(response.ResponseValidator.GetNodeValue("response.message"), "Success", "Actual message did not match expected message");
		Assert.assertEquals(response.ResponseValidator.GetNodeValueAsInt("response.code"), Integer.parseInt("200"), "Code in response body did not match with HTTP response code");
		
		System.out.println("######################################### queueNewUserOrderForAssigningVerifierTest compleated #########################################");
	}
	

	@Test(groups = {"sanity", "regression"},description = "Create a manual order and relay it to delivery", dataProviderClass = VerificationServiceApiTestData.class)
	public void relayManualOrderToVendorAndDeliveryTest() throws IOException, InterruptedException {
		System.out.println("***************************************** relayManualOrderToVendorAndDeliveryTest started *****************************************");
		
		Processor response = helper.relayOrderToVendorAndDelivery(losHelper.getAnOrder("manual"));
		Assert.assertEquals(response.ResponseValidator.GetResponseCode(), Integer.parseInt("200"), "Response code is not 200");
		Assert.assertEquals(response.ResponseValidator.GetNodeValue("data"), "Relayed Order to Delivery and Vendor Systems", "Actual data did not match expected data");
		Assert.assertEquals(response.ResponseValidator.GetNodeValue("response.message"), "Success", "Actual message did not match expected message");
		Assert.assertEquals(response.ResponseValidator.GetNodeValueAsInt("response.code"), Integer.parseInt("200"), "Code in response body did not match with HTTP response code");
		
		System.out.println("######################################### relayManualOrderToVendorAndDeliveryTest compleated #########################################");
	}
	

	@Test(groups = {"sanity", "regression"},description = "Create a manual order and relay it to delivery and vendor", dataProviderClass = VerificationServiceApiTestData.class)
	public void relayPartnerOrderToVendorAndDeliveryTest() throws IOException, InterruptedException {
		System.out.println("***************************************** relayPartnerOrderToVendorAndDeliveryTest started *****************************************");
		
		Processor response = helper.relayOrderToVendorAndDelivery(losHelper.getAnOrder("partner"));
		Assert.assertEquals(response.ResponseValidator.GetResponseCode(), Integer.parseInt("200"), "Response code is not 200");
		Assert.assertEquals(response.ResponseValidator.GetNodeValue("data"), "Relayed Order to Delivery and Vendor Systems", "Actual data did not match expected data");
		Assert.assertEquals(response.ResponseValidator.GetNodeValue("response.message"), "Success", "Actual message did not match expected message");
		Assert.assertEquals(response.ResponseValidator.GetNodeValueAsInt("response.code"), Integer.parseInt("200"), "Code in response body did not match with HTTP response code");
		
		System.out.println("######################################### relayPartnerOrderToVendorAndDeliveryTest compleated #########################################");
	}
	

	@Test(groups = {"sanity", "regression"},description = "Create a third party order and relay it to delivery and vendor", dataProviderClass = VerificationServiceApiTestData.class)
	public void relayThirdPartyOrderToVendorAndDeliveryTest() throws IOException, InterruptedException {
		System.out.println("***************************************** relayThirdPartyOrderToVendorAndDeliveryTest started *****************************************");
		
		Processor response = helper.relayOrderToVendorAndDelivery(losHelper.getAnOrder("thirdparty"));
		Assert.assertEquals(response.ResponseValidator.GetResponseCode(), Integer.parseInt("200"), "Response code is not 200");
		Assert.assertEquals(response.ResponseValidator.GetNodeValue("data"), "Relayed Order to Delivery and Vendor Systems", "Actual data did not match expected data");
		Assert.assertEquals(response.ResponseValidator.GetNodeValue("response.message"), "Success", "Actual message did not match expected message");
		Assert.assertEquals(response.ResponseValidator.GetNodeValueAsInt("response.code"), Integer.parseInt("200"), "Code in response body did not match with HTTP response code");
		
		System.out.println("######################################### relayThirdPartyOrderToVendorAndDeliveryTest compleated #########################################");
	}
	

	@Test(groups = {"sanity", "regression"},description = "Create a long distance order and relay it to delivery and vendor", dataProviderClass = VerificationServiceApiTestData.class)
	public void relayLongDistanceOrderToVendorAndDeliveryTest() throws IOException, InterruptedException {
		System.out.println("***************************************** relayLongDistanceOrderToVendorAndDeliveryTest started *****************************************");
		
		Processor response = helper.relayOrderToVendorAndDelivery(losHelper.getAnOrder("longdistance"));
		Assert.assertEquals(response.ResponseValidator.GetResponseCode(), Integer.parseInt("200"), "Response code is not 200");
		Assert.assertEquals(response.ResponseValidator.GetNodeValue("data"), "Relayed Order to Delivery and Vendor Systems", "Actual data did not match expected data");
		Assert.assertEquals(response.ResponseValidator.GetNodeValue("response.message"), "Success", "Actual message did not match expected message");
		Assert.assertEquals(response.ResponseValidator.GetNodeValueAsInt("response.code"), Integer.parseInt("200"), "Code in response body did not match with HTTP response code");
		
		System.out.println("######################################### relayLongDistanceOrderToVendorAndDeliveryTest compleated #########################################");
	}
	

	@Test(groups = {"sanity", "regression"},description = "Create a new user order and relay it to delivery and vendor", dataProviderClass = VerificationServiceApiTestData.class)
	public void relayNewUserOrderToVendorAndDeliveryTest() throws IOException, InterruptedException {
		System.out.println("***************************************** relayNewUserOrderToVendorAndDeliveryTest started *****************************************");
		
		Processor response = helper.relayOrderToVendorAndDelivery(losHelper.getAnOrder("newuser"));
		Assert.assertEquals(response.ResponseValidator.GetResponseCode(), Integer.parseInt("200"), "Response code is not 200");
		Assert.assertEquals(response.ResponseValidator.GetNodeValue("data"), "Relayed Order to Delivery and Vendor Systems", "Actual data did not match expected data");
		Assert.assertEquals(response.ResponseValidator.GetNodeValue("response.message"), "Success", "Actual message did not match expected message");
		Assert.assertEquals(response.ResponseValidator.GetNodeValueAsInt("response.code"), Integer.parseInt("200"), "Code in response body did not match with HTTP response code");
		
		System.out.println("######################################### relayNewUserOrderToVendorAndDeliveryTest compleated #########################################");
	}
	

	@Test(groups = {"sanity", "regression"},description = "Create a Swiggy Pop order and relay it to delivery and vendor", dataProviderClass = VerificationServiceApiTestData.class)
	public void relaySwiggyPopOrderToVendorAndDeliveryTest() throws IOException, InterruptedException {
		System.out.println("***************************************** relaySwiggyPopOrderToVendorAndDeliveryTest started *****************************************");
		
		Processor response = helper.relayOrderToVendorAndDelivery(losHelper.getAnOrder("pop"));
		Assert.assertEquals(response.ResponseValidator.GetResponseCode(), Integer.parseInt("200"), "Response code is not 200");
		Assert.assertEquals(response.ResponseValidator.GetNodeValue("data"), "Relayed Order to Delivery and Vendor Systems", "Actual data did not match expected data");
		Assert.assertEquals(response.ResponseValidator.GetNodeValue("response.message"), "Success", "Actual message did not match expected message");
		Assert.assertEquals(response.ResponseValidator.GetNodeValueAsInt("response.code"), Integer.parseInt("200"), "Code in response body did not match with HTTP response code");
		
		System.out.println("######################################### relaySwiggyPopOrderToVendorAndDeliveryTest compleated #########################################");
	}
	

	@Test(groups = {"sanity", "regression"},description = "Create a Swiggy Assured order and relay it to delivery and vendor", dataProviderClass = VerificationServiceApiTestData.class)
	public void relaySwiggyAssuredOrderToVendorAndDeliveryTest() throws IOException, InterruptedException {
		System.out.println("***************************************** relaySwiggyAssuredOrderToVendorAndDeliveryTest started *****************************************");
		
		Processor response = helper.relayOrderToVendorAndDelivery(losHelper.getAnOrder("swiggyassured"));
		Assert.assertEquals(response.ResponseValidator.GetResponseCode(), Integer.parseInt("200"), "Response code is not 200");
		Assert.assertEquals(response.ResponseValidator.GetNodeValue("data"), "Relayed Order to Delivery and Vendor Systems", "Actual data did not match expected data");
		Assert.assertEquals(response.ResponseValidator.GetNodeValue("response.message"), "Success", "Actual message did not match expected message");
		Assert.assertEquals(response.ResponseValidator.GetNodeValueAsInt("response.code"), Integer.parseInt("200"), "Code in response body did not match with HTTP response code");
		
		System.out.println("######################################### relaySwiggyAssuredOrderToVendorAndDeliveryTest compleated #########################################");
	}
	
	
	/*
	 * ###############################################  Negative Scenarios ###############################################
	 * 
	 */
	
	@Test(groups = {"sanity", "regression"},description = "Relay invalid order to delivery and vendor", dataProviderClass = VerificationServiceApiTestData.class)
	public void relayInvalidOrderToVendorAndDeliveryTest() throws IOException{
		System.out.println("***************************************** relayInvalidOrderToVendorAndDeliveryTest started *****************************************");
		
		Processor response = helper.relayOrderToVendorAndDelivery("Invalid-Order");
		Assert.assertEquals(response.ResponseValidator.GetResponseCode(), Integer.parseInt("400"), "Response code is not 200");
		Assert.assertEquals(response.ResponseValidator.GetNodeValue("response.message"), "Errors with your submission", "Actual message did not match expected message");
		Assert.assertEquals(response.ResponseValidator.GetNodeValueAsStringFromJsonArray("response.errors"), "[{\"rejected_value\":\"orderId\",\"error_field\":\"orderId\",\"message\":\"Invalid Order Id\"}]", "error in response did not match with expected error");
		Assert.assertEquals(response.ResponseValidator.GetNodeValueAsInt("response.code"), Integer.parseInt("400"), "Code in response body did not match with HTTP response code");
		
		System.out.println("######################################### relayInvalidOrderToVendorAndDeliveryTest compleated #########################################");
	}
	
	@Test(groups = {"sanity", "regression"},description = "Relay non existing order to delivery and vendor", dataProviderClass = VerificationServiceApiTestData.class)
	public void relayNonExistingOrderToVendorAndDeliveryTest() throws IOException{
		System.out.println("***************************************** relayNonExistingOrderToVendorAndDeliveryTest started *****************************************");
		
		Processor response = helper.relayOrderToVendorAndDelivery("1234567891011");
		Assert.assertEquals(response.ResponseValidator.GetResponseCode(), Integer.parseInt("400"), "Response code is not 400");
		Assert.assertEquals(response.ResponseValidator.GetNodeValue("response.message"), "Errors with your submission", "Actual message did not match expected message");
		Assert.assertEquals(response.ResponseValidator.GetNodeValueAsStringFromJsonArray("response.errors"), "[{\"rejected_value\":\"orderId\",\"error_field\":\"orderId\",\"message\":\"Invalid Order Id\"}]", "error in response did not match with expected error");
		Assert.assertEquals(response.ResponseValidator.GetNodeValueAsInt("response.code"), Integer.parseInt("400"), "Code in response body did not match with HTTP response code");
		
		System.out.println("######################################### relayNonExistingOrderToVendorAndDeliveryTest compleated #########################################");
	}
	
	@Test(groups = {"sanity", "regression"},description = "Hitting relayOrderToVendorAndDelivery API as GET", dataProviderClass = VerificationServiceApiTestData.class)
	public void hitRelayOrderToVendorAndDeliveryAPIAsGETTest() throws IOException{
		System.out.println("***************************************** hitRelayOrderToVendorAndDeliveryAPIAsGETTest started *****************************************");
		
		Processor response = helper.hitRelayOrderToVendorAndDeliveryAPIAsGET();
		Assert.assertEquals(response.ResponseValidator.GetResponseCode(), Integer.parseInt("405"), "Response code is not 405");
		
		System.out.println("######################################### hitRelayOrderToVendorAndDeliveryAPIAsGETTest compleated #########################################");
	}
	
	@Test(groups = {"sanity", "regression"},description = "Hitting queueAssigningForVerifier API as GET", dataProviderClass = VerificationServiceApiTestData.class)
	public void hitQueueForAssigningVerifierIAsGETTest() throws IOException{
		System.out.println("***************************************** hitQueueForAssigningVerifierIAsGETTest started *****************************************");
		
		Processor response = helper.hitQueueForAssigningVerifierIAsGET();
		Assert.assertEquals(response.ResponseValidator.GetResponseCode(), Integer.parseInt("405"), "Response code is not 405");
		
		System.out.println("######################################### hitQueueForAssigningVerifierIAsGETTest compleated #########################################");
	}
}
