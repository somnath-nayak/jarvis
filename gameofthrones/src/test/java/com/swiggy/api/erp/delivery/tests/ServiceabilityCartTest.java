
package com.swiggy.api.erp.delivery.tests;

import com.ibm.icu.math.BigDecimal;
import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.erp.delivery.helper.*;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.RedisHelper;
import org.apache.noggit.JSONUtil;
import org.apache.solr.common.SolrDocumentList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.*;

public class ServiceabilityCartTest
{
    public static Integer area_id;
    public static Integer city_id;
    String restLatLon;
    Double restLat;
    Integer zone_id, zoneMaxSla;
    Double restLon;
    Double defaultCustomerLat;
    Double defaultCustomerLon;
    String solrUrl;
    String restaurantId, restaurantName;
    Double actualLastMile;
    Double restMaxBannerFactor;
    Double defaultLat;
    Double defaultLon;
    SolrHelper solrHelper = new SolrHelper();
    List<String> LdCoordinates = solrHelper.LdCoordinates;
    DeliveryHelperMethods deliveryHelperMethods=new DeliveryHelperMethods();
    DeliveryDataHelper deliveryDataHelper=new DeliveryDataHelper();
    Serviceability_Helper serviceablility_Helper=new Serviceability_Helper();
    ServiceablilityHelper servHelper = new ServiceablilityHelper();
    private static final String core="core_listing";
    RedisHelper redisHelper =new RedisHelper();
    String lDrestaurantId = solrHelper.lDrestaurantId;
    String lDrestLatLon = solrHelper.lDrestLatLon;
    Double lDrestLon = solrHelper.lDrestLon;
    Double lDrestLat = solrHelper.lDrestLat;
    String listing_item_count = "1", total_item_count = "1";


    Double LdDistance=6.5;
    Integer lDPolygonId = solrHelper.lDPolygonId, ldAreaId;
    Double ldLastMile, ldRestaurantLastMile, ldAreaLastMile;
    String longDistanceId;
    String defaultCustomerZone;
    String rainMode;
    Double ldRestAndAreaMinLastMile, lastMileAreaCap, lastMileRestaurantCap;
    Boolean tempLmEnabled;
    String row_not_found_key = "[rowNotFound]";                    //key => DeliveryDataBaseUtils returnRowNotFoundMap() map..
    String rain_mode;


    @BeforeTest
    public void doConfig()
    {
        area_id=10;
        city_id=Integer.parseInt(DeliveryDataBaseUtils.getCityIdFromArea(area_id.toString()));
        zone_id=Integer.parseInt(DeliveryDataBaseUtils.getZoneIdFromArea(area_id.toString()));
        solrUrl = deliveryHelperMethods.getSolrUrl();
        String solrquery="enabled:true AND area_id:"+area_id+" AND city_id:"+city_id;
        SolrDocumentList solrDocuments = deliveryDataHelper.getSolrOutput(solrUrl,core,solrquery);
        JSONObject restObject = null;
        tempLmEnabled = Boolean.parseBoolean(serviceablility_Helper.getValueFromDb("temp_lm_enabled", "zone", "id", String.valueOf(zone_id)));
        SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update("update zone set temp_lm_enabled=0 where id="+zone_id);
        zoneMaxSla = Integer.parseInt(serviceablility_Helper.getValueFromDb("max_delivery_time", "zone", "id", String.valueOf(zone_id)));
        SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update("update zone set max_delivery_time=200 where id="+zone_id);
        rainMode = serviceablility_Helper.getValueFromDb("rain_mode_type", "zone", "id", String.valueOf(zone_id));
        SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update("update zone set rain_mode_type=3 where id="+zone_id);
        serviceablility_Helper.clearCache("zone", String.valueOf(zone_id));
        Double cityRadius=Double.parseDouble(DeliveryDataBaseUtils.getDeliveryRadiusFromCity(city_id.toString()));
        SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update("update area set last_mile_cap="+(cityRadius-2)+" where id="+area_id);
        serviceablility_Helper.clearCache("area", String.valueOf(area_id));
        try {
            restObject = new JSONArray(JSONUtil.toJSON(solrDocuments)).getJSONObject(1);
            restLatLon=restObject.get("place").toString();
            restLat=Double.parseDouble(restLatLon.split(",")[0]);
            restLon=Double.parseDouble(restLatLon.split(",")[1]);
            restaurantId=restObject.get("id").toString();
            restaurantName = restObject.get("name").toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        servHelper.makeServiceable(restaurantId);
        lastMileAreaCap=Double.parseDouble(DeliveryDataBaseUtils.getMaxLastMileForArea(area_id.toString()));
        lastMileRestaurantCap=Double.parseDouble(DeliveryDataBaseUtils.getMaxLastMileForRestaurant(restaurantId.toString()));
        actualLastMile=Math.min(lastMileAreaCap,lastMileRestaurantCap);
        //Double[] latlonarray=deliveryDataHelper.getLatLng(restLat,restLon,90.0,.5);
        Map<String,String> latlonMap=deliveryDataHelper.latLonNotInGivenTagsButInZone(restLat,restLon,.5,"LONG_DISTANCE");
        defaultCustomerLat=Double.parseDouble(latlonMap.get("lat"));
        defaultCustomerLon=Double.parseDouble(latlonMap.get("lon"));
        defaultCustomerZone=latlonMap.get("zone_id");
        restMaxBannerFactor=Double.parseDouble(DeliveryDataBaseUtils.getMaxBannerFactorFromRestaurant(restaurantId));//getValueFromDBusingQeury("select max_banner_factor from restaurant where id ="+restaurantId, "max_banner_factor"));
        String key = "BL_BANNER_FACTOR_ZONE_"+zone_id;
        if(!(redisHelper.checkKeyExists("deliveryredis",1,key)))
        {
            redisHelper.setValue("deliveryredis",1,key,".5");
        }
        System.out.println("********************************* BEFORE TEST *************************************************");
    }

    @AfterTest
    public void afterTest()
    {
        System.out.println("********************************* AFTER TEST *************************************************");
        serviceablility_Helper.setValueInDb("zone", "rain_mode_type", rainMode, "id", String.valueOf(zone_id));
        if(tempLmEnabled)
            serviceablility_Helper.setValueInDb("zone", "temp_lm_enabled", "1", "id", String.valueOf(zone_id));
        else
            serviceablility_Helper.setValueInDb("zone", "temp_lm_enabled", "0", "id", String.valueOf(zone_id));
        serviceablility_Helper.setValueInDb("zone", "max_delivery_time", String.valueOf(zoneMaxSla), "id", String.valueOf(zone_id));
        serviceablility_Helper.setValueInDb("area", "last_mile_cap", String.valueOf(lastMileAreaCap), "id", String.valueOf(area_id));
        serviceablility_Helper.setValueInDb("restaurant", "max_second_mile", String.valueOf(lastMileRestaurantCap), "id", restaurantId);
    }

    @BeforeGroups(groups = {"cart_long_distance"})
    public void beforeLD()
    {
        //servHelper.makeServiceable(lDrestaurantId);
        //Method will return a coordinate inside a ld polygon

        //List<String> LdCoordinates = solrHelper.findPointInsideLDPolygonLatLon(area_id.toString(), city_id.toString(), lDrestLat, lDrestLon);


       LdCoordinates=solrHelper.findPointInsideLDPolygonLatLon(area_id.toString(), city_id.toString(), lDrestLat, lDrestLon);
        ldAreaId = Integer.parseInt(DeliveryDataBaseUtils.getAreaCodeFromRestaurant(lDrestaurantId));//getValueFromDBusingQeury("select area_code from restaurant where id="+lDrestaurantId, "area_code"));
        ldAreaLastMile = Double.parseDouble(DeliveryDataBaseUtils.getMaxLastMileForArea(area_id.toString()));//getValueFromDBusingQeury("select last_mile_cap from area where id="+area_id, "last_mile_cap"));
        ldLastMile = Double.parseDouble(DeliveryDataBaseUtils.getLDmaxLastMile(lDPolygonId.toString()));//getValueFromDBusingQeury("select max_last_mile from long_distance where polygon_id='" + lDPolygonId + "'", "max_last_mile"));
        longDistanceId = DeliveryDataBaseUtils.getLongDistanceIdFromPolygonId(lDPolygonId.toString());//getValueFromDBusingQeury("select id from long_distance where polygon_id='" + lDPolygonId + "'", "id");
        ldRestaurantLastMile = Double.parseDouble(DeliveryDataBaseUtils.getMaxLastMileForRestaurant(lDrestaurantId));//getValueFromDBusingQeury("select max_second_mile from restaurant where id=" + lDrestaurantId, "max_second_mile"));
        ldRestAndAreaMinLastMile = Math.min(lastMileAreaCap, Double.parseDouble(DeliveryDataBaseUtils.getMaxLastMileForRestaurant(lDrestaurantId)));//getValueFromDBusingQeury("select max_second_mile from restaurant where id=" + lDrestaurantId, "max_second_mile")));
        System.out.println("********************************* BEFORE GROUP *************************************************");
    }

    @AfterGroups(groups = {"cart_long_distance"})
    public void afterLD()
    {
        System.out.println("********************************* AFTER GROUP *************************************************");
        serviceablility_Helper.setValueInDb("area", "last_mile_cap", String.valueOf(ldAreaLastMile), "id", String.valueOf(ldAreaId));
        serviceablility_Helper.setValueInDb("long_distance", "max_last_mile", String.valueOf(ldLastMile), "id", longDistanceId);
        serviceablility_Helper.setValueInDb("restaurant", "max_second_mile", String.valueOf(ldRestaurantLastMile), "id", lDrestaurantId);
    }


    public String getCustomerZoneFromLatLon(Double lat,Double lon)
    {
        SolrDocumentList solrDocuments = deliveryDataHelper.getSolrOutput(solrUrl,"user_zone","polygon:\"Intersects(" + lat.toString() + "  " + lon.toString() + ")\"");
        String jsonDoc = JSONUtil.toJSON(solrDocuments);
        System.out.println("**** json doc **** " + jsonDoc);
        String customerZoneId=null;
        try
        {
            customerZoneId =((JSONObject)((new JSONArray(jsonDoc)).get(0))).get("id").toString();
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        return customerZoneId;
    }




    @Test(description = "SCTC_01 :- Verify the restaurant is serviceable in Cart when the last_mile_travel is less than the max last mile (minimum of area and restaurant's max last mile)")
    public void a1lessThanLastMileCheck()
    {
        String restZoneId = getCustomerZoneFromLatLon(restLat,restLon);

        String custZoneId = getCustomerZoneFromLatLon(defaultCustomerLat, defaultCustomerLon);
        Map<String, Integer> rainModeMap = DeliveryDataBaseUtils.setNoRainsAtRestaurantAndCustomer(restZoneId, custZoneId);

        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroCart(10, 1000, new String[]
                {listing_item_count, total_item_count, restaurantId, area_id.toString(), city_id.toString(), restaurantName, restLat.toString(), restLon.toString(), defaultCustomerLat.toString(), defaultCustomerLon.toString()}, new LinkedHashMap<String,String>() {
            {
                put("rain_mode", "0");
                put("serviceable", "2");
                put("is_long_distance", "0");
            }});
                if (rainModeMap.get("restaurantRainMode") != 0)
                    DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rainModeMap.get("restaurantRainMode").toString(), restZoneId);
                if (custZoneId != null && rainModeMap.get("customerRainMode") != 0)
                    DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rainModeMap.get("customerRainMode").toString(), custZoneId);
                Assert.assertEquals(mappedResponse.get("serviceable"), "2", "serviceable");

    }

    @Test(description = "SCTC_02 :- Verify the restaurant is serviceable in Cart when the last_mile_travel is less than the max last mile (minimum of area and restaurant's max last mile)")
    public void greaterThanLastMileCheck()
    {
        Double[] latlonarray=deliveryDataHelper.getLatLng(restLat,restLon,90.0,actualLastMile/1.3+10);
        String restZoneId = getCustomerZoneFromLatLon(restLat,restLon);
        String custZoneId = getCustomerZoneFromLatLon(latlonarray[0], latlonarray[1]);
        Map<String, Integer> rainModeMap = setNoRainsAtRestaurantAndCustomer(restZoneId, custZoneId);
        serviceablility_Helper.setNoRainsAtZone(String.valueOf(rainModeMap.get("restaurantRainMode")));
        serviceablility_Helper.setNoRainsAtZone(String.valueOf(rainModeMap.get("customerRainMode")));

        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroCart(10, 1000, new String[]
                {listing_item_count, total_item_count, restaurantId, area_id.toString(), city_id.toString(), restaurantName, restLat.toString(), restLon.toString(), latlonarray[0].toString(), latlonarray[1].toString()}, new LinkedHashMap<String,String>() {
            {

                put("serviceable", "0");
                put("non_serviceable_reason", "0");
                put("is_long_distance", "0");
            }});

        if(rainModeMap.get("restaurantRainMode") != 0)
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rainModeMap.get("restaurantRainMode").toString(), restZoneId);

        if(custZoneId != null && rainModeMap.get("customerRainMode") != 0)
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rainModeMap.get("customerRainMode").toString(), custZoneId);

        Assert.assertEquals(mappedResponse.get("serviceable"),"0", "serviceability");
        Assert.assertEquals(mappedResponse.get("non_serviceable_reason"),"0", "non_serviceable_reason");
    }

    @Test(description = "SLTC_03 :- Verify the restaurant is serviceable in Listing when the last_mile_travel is equal to the max last mile (minimum of area and restaurant's max last mile)")
    public void equalLastMileCheck()
    {
        Double[] latlonarray=deliveryDataHelper.getLatLng(restLat,restLon,90.0,actualLastMile/1.3 - 0.001);
        String restZoneId = getCustomerZoneFromLatLon(restLat,restLon);
        String custZoneId = getCustomerZoneFromLatLon(latlonarray[0], latlonarray[1]);
        Map<String, Integer> rainModeMap = setNoRainsAtRestaurantAndCustomer(restZoneId, custZoneId);
        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroCart(10, 1000, new String[]
                {listing_item_count, total_item_count, restaurantId, area_id.toString(), city_id.toString(), restaurantName, restLat.toString(), restLon.toString(), latlonarray[0].toString(), latlonarray[1].toString()}, new LinkedHashMap<String,String>() {
            {
                put("rain_mode", "0");
                put("serviceable", "2");
                put("is_long_distance", "0");
            }});

        if(rainModeMap.get("restaurantRainMode") != 0)
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rainModeMap.get("restaurantRainMode").toString(), restZoneId);

        if(custZoneId != null && rainModeMap.get("customerRainMode") != 0)
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rainModeMap.get("customerRainMode").toString(), custZoneId);

        Assert.assertEquals(mappedResponse.get("serviceable"),"2", "serviceability");
    }


    @Test(description = "SCTC_04 :- delivery_time < max_sla in zone")
    public void ltSlaCheck()
    {

        String restZoneId = getCustomerZoneFromLatLon(restLat,restLon);
        String custZoneId = getCustomerZoneFromLatLon(defaultCustomerLat, defaultCustomerLon);
        Map<String, Integer> rainModeMap = setNoRainsAtRestaurantAndCustomer(restZoneId, custZoneId);

        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroCart(10, 1000, new String[]
                {listing_item_count, total_item_count, restaurantId, area_id.toString(), city_id.toString(), restaurantName, restLat.toString(), restLon.toString(), defaultCustomerLat.toString(), defaultCustomerLon.toString()}, new LinkedHashMap<String,String>(){{
            put("rain_mode","0");
            put("serviceability","2");
            put("is_long_distance","0");
        }});
        if(rainModeMap.get("restaurantRainMode") != 0)
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rainModeMap.get("restaurantRainMode").toString(), restZoneId);

        if(custZoneId != null && rainModeMap.get("customerRainMode") != 0)
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rainModeMap.get("customerRainMode").toString(), custZoneId);

        Assert.assertEquals(mappedResponse.get("serviceable"),"2", "serviceability");
    }


    @Test(description = "SCTC_05 :- delivery_time > max_sla in zone")
    public void gtSlaCheck()
    {
        Double[] latlonarray=deliveryDataHelper.getLatLng(restLat,restLon,90.0,actualLastMile/1.3-2);
        String restZoneId = getCustomerZoneFromLatLon(restLat,restLon);
        String custZoneId = getCustomerZoneFromLatLon(latlonarray[0], latlonarray[1]);
        Map<String, Integer> rainModeMap = setNoRainsAtRestaurantAndCustomer(restZoneId, custZoneId);
        String maxSla = serviceablility_Helper.getValueFromDb("max_delivery_time", "zone", "id", String.valueOf(zone_id));
        DeliveryDataBaseUtils.setZoneMaxSla("10", String.valueOf(zone_id));

        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroCart(10, 1000, new String[]
                {listing_item_count, total_item_count, restaurantId, area_id.toString(), city_id.toString(), restaurantName, restLat.toString(), restLon.toString(), latlonarray[0].toString(), latlonarray[1].toString()}, new LinkedHashMap<String,String>(){{
            put("rain_mode","0");
            put("serviceable","0");
            put("non_serviceable_reason","1");
            put("is_long_distance","0");
        }});
        if(rainModeMap.get("restaurantRainMode") != 0)
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rainModeMap.get("restaurantRainMode").toString(), restZoneId);
        if(custZoneId != null && rainModeMap.get("customerRainMode") != 0)
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rainModeMap.get("customerRainMode").toString(), custZoneId);
        DeliveryDataBaseUtils.setZoneMaxSla(maxSla, String.valueOf(zone_id));
        Assert.assertEquals(mappedResponse.get("serviceable"),"0", "serviceability");
        Assert.assertEquals(mappedResponse.get("non_serviceable_reason"),"1", "non serviceable reason");
    }

    @Test(description = "SCTC_06 :- delivery_time == max_sla in zone")
    public void eqSlaCheck()
    {
        Double[] latlonarray=deliveryDataHelper.getLatLng(restLat,restLon,90.0,actualLastMile/1.3-2);
        String restZoneId = getCustomerZoneFromLatLon(restLat,restLon);
        String custZoneId = getCustomerZoneFromLatLon(latlonarray[0], latlonarray[1]);
        Map<String, Integer> rainModeMap = setNoRainsAtRestaurantAndCustomer(restZoneId, custZoneId);
        String maxSla = serviceablility_Helper.getValueFromDb("max_delivery_time", "zone", "id", String.valueOf(zone_id));

        Processor processor = serviceablility_Helper.cart(new String[]{listing_item_count, total_item_count, restaurantId, area_id.toString(), city_id.toString(), restaurantName, restLat.toString(), restLon.toString(), latlonarray[0].toString(), latlonarray[1].toString()});
        String response=processor.ResponseValidator.GetBodyAsText();
        Map<String, String> mappedresponse=serviceablility_Helper.getServiceabiltyCartResponseMap(response);
        String slaInResponse=mappedresponse.get("sla");
        DeliveryDataBaseUtils.setZoneMaxSla(slaInResponse, String.valueOf(zone_id));

        Map<String,String> mappedResponse1=serviceablility_Helper.retryCerebroCart(10, 1000, new String[]
                {listing_item_count, total_item_count, restaurantId, area_id.toString(), city_id.toString(), restaurantName, restLat.toString(), restLon.toString(), latlonarray[0].toString(), latlonarray[1].toString()}, new LinkedHashMap<String,String>(){{
            put("rain_mode","0");
            put("serviceable","0");
            put("non_serviceable_reason","1");
            put("is_long_distance","0");
        }});
        DeliveryDataBaseUtils.setZoneMaxSla(maxSla, String.valueOf(zone_id));

        if(rainModeMap.get("restaurantRainMode") != 0)
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rainModeMap.get("restaurantRainMode").toString(), restZoneId);
        if(custZoneId != null && rainModeMap.get("customerRainMode") != 0)
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rainModeMap.get("customerRainMode").toString(), custZoneId);
        Assert.assertEquals(mappedResponse1.get("serviceable"),"2", "serviceability");
    }

    @Test(description = "SCTC_07 :-  last_mile < temp_last_mile in zone")
    public void tempLastMileLtCheck()
    {
        serviceablility_Helper.setValueInDb("zone", "temp_lm_enabled", "1", "id", String.valueOf(zone_id));
        Double actualtempLastMile=Double.parseDouble(serviceablility_Helper.getValueFromDb("temp_last_mile", "zone", "id", String.valueOf(zone_id)));
        Double[] latlonarray=deliveryDataHelper.getLatLng(restLat,restLon,90.0,actualtempLastMile/1.3-1);
        String restZoneId = getCustomerZoneFromLatLon(restLat,restLon);
        String custZoneId = getCustomerZoneFromLatLon(latlonarray[0], latlonarray[1]);
        Map<String, Integer> rainModeMap = setNoRainsAtRestaurantAndCustomer(restZoneId, custZoneId);
        Map<String,String> mappedResponse=serviceablility_Helper.retryCerebroCart(10, 1000, new String[]
                {listing_item_count, total_item_count, restaurantId, area_id.toString(), city_id.toString(), restaurantName, restLat.toString(), restLon.toString(), latlonarray[0].toString(), latlonarray[1].toString()}, new LinkedHashMap<String,String>(){{
            put("rain_mode","0");
            put("serviceable","2");
            put("is_long_distance","0");
        }});

        serviceablility_Helper.setValueInDb("zone", "temp_lm_enabled", "0", "id", String.valueOf(zone_id));
        if(rainModeMap.get("restaurantRainMode") != 0)
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rainModeMap.get("restaurantRainMode").toString(), restZoneId);
        if(custZoneId != null && rainModeMap.get("customerRainMode") != 0)
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rainModeMap.get("customerRainMode").toString(), custZoneId);
        Assert.assertEquals(mappedResponse.get("serviceable"),"2", "serviceability");
    }


    @Test(description = "SCTC_08 :-  last_mile > temp_last_mile in zone")
    public void tempLastMileGtCheck()
    {
        serviceablility_Helper.setValueInDb("zone", "temp_lm_enabled", "1", "id", String.valueOf(zone_id));
        Double actualtempLastMile=Double.parseDouble(serviceablility_Helper.getValueFromDb("temp_last_mile", "zone", "id", String.valueOf(zone_id)));
        Double[] latlonarray=deliveryDataHelper.getLatLng(restLat,restLon,90.0,actualtempLastMile/1.3+1);

        String restZoneId = getCustomerZoneFromLatLon(restLat,restLon);
        String custZoneId = getCustomerZoneFromLatLon(latlonarray[0], latlonarray[1]);
        Map<String, Integer> rainModeMap = setNoRainsAtRestaurantAndCustomer(restZoneId, custZoneId);
        Map<String,String> mappedResponse = serviceablility_Helper.retryCerebroCart(10, 1000, new String[]
                {listing_item_count, total_item_count, restaurantId, area_id.toString(), city_id.toString(), restaurantName, restLat.toString(), restLon.toString(), latlonarray[0].toString(), latlonarray[1].toString()}, new LinkedHashMap<String,String>(){{
            put("rain_mode","0");
            put("serviceable","1");
            put("non_serviceable_reason","4");
            put("is_long_distance","0");
        }});

        serviceablility_Helper.setValueInDb("zone", "temp_lm_enabled", "0", "id", String.valueOf(zone_id));
        if(rainModeMap.get("restaurantRainMode") != 0)
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rainModeMap.get("restaurantRainMode").toString(), restZoneId);
        if(custZoneId != null && rainModeMap.get("customerRainMode") != 0)
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rainModeMap.get("customerRainMode").toString(), custZoneId);

        Assert.assertEquals(mappedResponse.get("serviceable"),"1", "serviceability");
        Assert.assertEquals(mappedResponse.get("non_serviceable_reason"),"4", "non serviceable reason");
    }


    @Test(description = "SCTC_09 :-  last_mile == temp_last_mile in zone")
    public void tempLastMileEqCheck()
    {
        serviceablility_Helper.setValueInDb("zone", "temp_lm_enabled", "1", "id", String.valueOf(zone_id));
        Double actualtempLastMile=Double.parseDouble(serviceablility_Helper.getValueFromDb("temp_last_mile", "zone", "id", String.valueOf(zone_id)));
        Double[] latlonarray=deliveryDataHelper.getLatLng(restLat,restLon,90.0,actualtempLastMile/1.3);

        String restZoneId = getCustomerZoneFromLatLon(restLat,restLon);
        String custZoneId = getCustomerZoneFromLatLon(latlonarray[0], latlonarray[1]);
        Map<String, Integer> rainModeMap = setNoRainsAtRestaurantAndCustomer(restZoneId, custZoneId);
        Map<String,String> mappedResponse = serviceablility_Helper.retryCerebroCart(10, 1000, new String[]
                {listing_item_count, total_item_count, restaurantId, area_id.toString(), city_id.toString(), restaurantName, restLat.toString(), restLon.toString(), latlonarray[0].toString(), latlonarray[1].toString()}, new LinkedHashMap<String,String>(){{
            put("rain_mode","0");
            put("serviceable","2");
            put("is_long_distance","0");
        }});

        serviceablility_Helper.setValueInDb("zone", "temp_lm_enabled", "0", "id", String.valueOf(zone_id));
        if(rainModeMap.get("restaurantRainMode") != 0)
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rainModeMap.get("restaurantRainMode").toString(), restZoneId);
        if(custZoneId != null && rainModeMap.get("customerRainMode") != 0)
            DeliveryDataBaseUtils.setZoneRainModeAndClearCache(rainModeMap.get("customerRainMode").toString(), custZoneId);
        Assert.assertEquals(mappedResponse.get("serviceable"),"2", "serviceability");
    }


    @Test(description = "SCTC_10 :- current banner factor < max_banner_factor")
    public void ltBanner()
    {
        String currentBF = redisHelper.getValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_" + zone_id).toString();
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+zone_id, (new Double(restMaxBannerFactor-.5)).toString());

        Map<String,String> mappedResponse = serviceablility_Helper.retryCerebroCart(10, 1000, new String[]
                {listing_item_count, total_item_count, restaurantId, area_id.toString(), city_id.toString(), restaurantName, restLat.toString(), restLon.toString(), defaultCustomerLat.toString(),defaultCustomerLon.toString()}, new LinkedHashMap<String,String>(){{
            put("rain_mode","0");
            put("serviceable","2");
            put("is_long_distance","0");
        }});

        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+zone_id, (new Double(currentBF)).toString());
        Assert.assertEquals(mappedResponse.get("serviceable"),"2", "serviceability");
    }


    @Test(description = "SCTC_11 :- current banner factor > max_banner_factor")
    public void gtBanner()
    {
        String currentBF = redisHelper.getValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_" + zone_id).toString();
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+zone_id,(new Double(restMaxBannerFactor+.5)).toString());

        Map<String,String> mappedResponse = serviceablility_Helper.retryCerebroCart(10, 1000, new String[]
                {listing_item_count, total_item_count, restaurantId, area_id.toString(), city_id.toString(), restaurantName, restLat.toString(), restLon.toString(),
                        defaultCustomerLat.toString(),defaultCustomerLon.toString()}, new LinkedHashMap<String,String>(){{
            put("rain_mode","0");
            put("serviceable","1");
            put("non_serviceable_reason", "7");
            put("is_long_distance","0");
        }});

        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+zone_id,(new Double(currentBF)).toString());
        Assert.assertEquals(mappedResponse.get("serviceable"),"1", "serviceability");
        Assert.assertEquals(mappedResponse.get("non_serviceable_reason"),"7", "non serviceable reason");
    }

    @Test(description = "SCTC_12 :- current banner factor == max_banner_factor")
    public void eqBanner()
    {
        String currentBF = redisHelper.getValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_" + zone_id).toString();
        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+zone_id,(new Double(restMaxBannerFactor)).toString());

        Map<String,String> mappedResponse = serviceablility_Helper.retryCerebroCart(10, 1000, new String[]
                {listing_item_count, total_item_count, restaurantId, area_id.toString(), city_id.toString(), restaurantName, restLat.toString(), restLon.toString(),
                        defaultCustomerLat.toString(),defaultCustomerLon.toString()}, new LinkedHashMap<String,String>(){{
            put("rain_mode","0");
            put("serviceable","2");
            put("is_long_distance","0");
        }});

        redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_"+zone_id,(new Double(currentBF)).toString());
        Assert.assertEquals(mappedResponse.get("serviceable"),"2", "serviceability");
    }


    @Test(description = "SCTC_14 :- zone is closed")
    public void zoneCloseServiceabilityTest()
    {
        Integer close_time = Integer.parseInt(serviceablility_Helper.getValueFromDb("close_time", "zone", "id", String.valueOf(zone_id)));
        Integer currentTimeMinutes = deliveryDataHelper.getcurrentDateTimeInMinutes();
        serviceablility_Helper.setValueInDb("zone", "close_time", String.valueOf(currentTimeMinutes-10), "id", String.valueOf(zone_id));

        Map<String,String> mappedResponse = serviceablility_Helper.retryCerebroCart(10, 1000, new String[]
                {listing_item_count, total_item_count, restaurantId, area_id.toString(), city_id.toString(), restaurantName, restLat.toString(), restLon.toString(),
                        defaultCustomerLat.toString(),defaultCustomerLon.toString()}, new LinkedHashMap<String,String>(){{
            put("rain_mode","0");
            put("serviceable","1");
            put("non_serviceable_reason", "5");
            put("is_long_distance","0");
        }});

        serviceablility_Helper.setValueInDb("zone", "close_time", String.valueOf(close_time), "id", String.valueOf(zone_id));
        Assert.assertEquals(mappedResponse.get("serviceable"),"1", "serviceability");
        Assert.assertEquals(mappedResponse.get("non_serviceable_reason"),"5", "non serviceable reason");
    }


    @Test(description = "SCTC_13 :- zone is open")
    public void zoneOpenServiceabilityTest()
    {
        Integer close_time = Integer.parseInt(serviceablility_Helper.getValueFromDb("close_time", "zone", "id", String.valueOf(zone_id)));
        Integer currentTimeMinutes = deliveryDataHelper.getcurrentDateTimeInMinutes();
        serviceablility_Helper.setValueInDb("zone", "close_time", String.valueOf(currentTimeMinutes+10), "id", String.valueOf(zone_id));

        Map<String,String> mappedResponse = serviceablility_Helper.retryCerebroCart(10, 1000, new String[]
                {listing_item_count, total_item_count, restaurantId, area_id.toString(), city_id.toString(), restaurantName, restLat.toString(), restLon.toString(),
                        defaultCustomerLat.toString(),defaultCustomerLon.toString()}, new LinkedHashMap<String,String>(){{
            put("rain_mode","0");
            put("serviceable","2");
            put("is_long_distance","0");
        }});

        serviceablility_Helper.setValueInDb("zone", "close_time", String.valueOf(close_time), "id", String.valueOf(zone_id));
        Assert.assertEquals(mappedResponse.get("serviceable"),"2", "serviceability");
    }


    @Test(description = "SCTC_15 :- current time == zone closed time in zone")
    public void zoneOpenBoundaryServiceabilityTest()
    {
        Integer close_time=Integer.parseInt(serviceablility_Helper.getValueFromDb("close_time", "zone", "id", String.valueOf(zone_id)));;
        Integer currentTimeMinutes=deliveryDataHelper.getcurrentDateTimeInMinutes();
        serviceablility_Helper.setValueInDb("zone", "close_time", String.valueOf(currentTimeMinutes), "id", String.valueOf(zone_id));

        Map<String,String> mappedResponse = serviceablility_Helper.retryCerebroCart(10, 1000, new String[]
                {listing_item_count, total_item_count, restaurantId, area_id.toString(), city_id.toString(), restaurantName, restLat.toString(), restLon.toString(),
                        defaultCustomerLat.toString(),defaultCustomerLon.toString()}, new LinkedHashMap<String,String>(){{
            put("rain_mode","0");
            put("serviceable","2");
            put("is_long_distance","0");
        }});

        serviceablility_Helper.setValueInDb("zone", "close_time", String.valueOf(close_time), "id", String.valueOf(zone_id));
        Assert.assertEquals(mappedResponse.get("serviceable"),"2", "serviceability");
    }


    @Test(description = "SCTC_16 :- rain_mode_type : 1 at restaurant => current last_mile < last_mile of restaurant's zone_rain_params of corresponding rain mode")
    public void rainsAtRestaurantMode1_LtLastMile()
    {
        rain_mode = "1";
        Integer restaurantRainMode = getRainMode(String.valueOf(zone_id));
        DeliveryDataBaseUtils.setRainModeAndCause(restaurantRainMode, Integer.parseInt(rain_mode), zone_id);

        String custZone = getCustomerZoneFromLatLon(defaultCustomerLat,defaultCustomerLon);
        Integer customerRainMode = getRainMode(String.valueOf(custZone));
        if(zone_id != Integer.parseInt(custZone) && customerRainMode != 3)
            serviceablility_Helper.setNoRainsAtZone(String.valueOf(custZone));
        Map<String, Object> restaurantZoneRainParams = DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("1", zone_id.toString());
        if(restaurantZoneRainParams.keySet().toString().equals(row_not_found_key))
            Assert.fail("add entry in zone rain params of rain mode : " + rain_mode + " and zone id : " + zone_id.toString());
        String restZoneMaxSla = restaurantZoneRainParams.get("zone_max_sla").toString();
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "200", rain_mode, zone_id.toString());
        Double ZRMMaxLastMile = Double.parseDouble(restaurantZoneRainParams.get("last_mile").toString());
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", "4", rain_mode, zone_id.toString());
        Map<String,String> mappedResponse = serviceablility_Helper.retryCerebroCart(10, 1000, new String[]
                {listing_item_count, total_item_count, restaurantId, area_id.toString(), city_id.toString(), restaurantName, restLat.toString(), restLon.toString(),
                        defaultCustomerLat.toString(),defaultCustomerLon.toString()}, new LinkedHashMap<String,String>(){{
            put("rain_mode","1");
            put("serviceable","2");
            put("is_long_distance","0");
        }});

        String rainModeInResponse = getResponseRainMode(mappedResponse);
        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", ZRMMaxLastMile.toString(), rain_mode, zone_id.toString());
        serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(restaurantRainMode), "id", String.valueOf(zone_id));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", restZoneMaxSla, rain_mode, zone_id.toString());
        if(customerRainMode != 3)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(customerRainMode), "id", String.valueOf(custZone));

        Assert.assertEquals(rainModeInResponse, "1", "rain mode");
        Assert.assertEquals(degradationMode, "2", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceable"), "2", "serviceability");

    }


    @Test(description = "SCTC_17 :- rain_mode_type : 1 at restaurant => current last_mile > last_mile of restaurant's zone_rain_params of corresponding rain mode")
    public void rainsAtRestaurantMode1_GtLastMile()
    {
        rain_mode = "1";
        Integer restaurantRainMode = getRainMode(String.valueOf(zone_id));
        DeliveryDataBaseUtils.setRainModeAndCause(restaurantRainMode, Integer.parseInt(rain_mode), zone_id);

        Double zoneRainParamsLastMile = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("last_mile").toString());//Double.parseDouble(getValueFromDBusingQeury("select last_mile from zone_rain_params where zone_id=" + zone_id + " and rain_mode=" + rain_mode, "last_mile"));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", "0.0", rain_mode, zone_id.toString());

        Double areaLastMile = Double.parseDouble(DeliveryDataBaseUtils.getMaxLastMileForArea(area_id.toString()));
        Double restaurantLastMile = Double.parseDouble(DeliveryDataBaseUtils.getMaxLastMileForRestaurant(restaurantId));
        DeliveryDataBaseUtils.setMaxLastMileForArea(area_id.toString(), "4.0");
        DeliveryDataBaseUtils.setMaxLastMileForRestaurant(restaurantId,"4.0");
        Double[] latlonarray = deliveryDataHelper.getLatLng(restLat, restLon, 90.0, 1.0);
        String custZone = getCustomerZoneFromLatLon(latlonarray[0], latlonarray[1]);
        Integer customerRainMode = getRainMode(String.valueOf(custZone));
        if(zone_id != Integer.parseInt(custZone) && customerRainMode != 3)
            serviceablility_Helper.setNoRainsAtZone(String.valueOf(custZone));

        String zrmMaxSla = DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("zone_max_sla").toString();
        if(Integer.parseInt(zrmMaxSla) < 200)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "200", rain_mode, zone_id.toString());

        Map<String,String> mappedResponse = serviceablility_Helper.retryCerebroCart(10, 1000, new String[]
                {listing_item_count, total_item_count, restaurantId, area_id.toString(), city_id.toString(), restaurantName, restLat.toString(), restLon.toString(),
                        latlonarray[0].toString(),latlonarray[1].toString()}, new LinkedHashMap<String,String>(){{
            put("rain_mode","1");
            put("serviceable","1");
            put("non_serviceable_reason","0");
            put("is_long_distance","0");
        }});

        String degradationMode = getResponseDegradationMode(mappedResponse);
        serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(restaurantRainMode), "id", String.valueOf(zone_id));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", zoneRainParamsLastMile.toString(), rain_mode, zone_id.toString());
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", zrmMaxSla, rain_mode, zone_id.toString());
        DeliveryDataBaseUtils.setMaxLastMileForArea(area_id.toString(), areaLastMile.toString());
        DeliveryDataBaseUtils.setMaxLastMileForRestaurant(restaurantId,restaurantLastMile.toString());
        if(customerRainMode != 3)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(customerRainMode), "id", String.valueOf(custZone));

        Assert.assertEquals(mappedResponse.get("rain_mode"), "1", "rain mode");
        Assert.assertEquals(degradationMode, "2", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceable"), "1", "serviceability");
        Assert.assertEquals(mappedResponse.get("non_serviceable_reason"), "0", "non serviceability reason");

    }


    @Test(description = "SCTC_18 :- rain_mode_type : 1 at restaurant => current sla < zone_max_sla of restaurant's zone_rain_params of corresponding rain mode")
    public void rainsAtRestaurantMode1_LtSLA()
    {
        rain_mode = "1";
        Integer restaurantRainMode = getRainMode(String.valueOf(zone_id));
        DeliveryDataBaseUtils.setRainModeAndCause(restaurantRainMode, Integer.parseInt(rain_mode), zone_id);//setRainModeAndCause(restaurantRainMode, 1, zone_id);
        Double zoneRainParamsLastMile = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("last_mile").toString());//Double.parseDouble(getValueFromDBusingQeury("select last_mile from zone_rain_params where zone_id=" + zone_id + " and rain_mode=1", "last_mile"));
        Double[] latlonarray = deliveryDataHelper.getLatLng(restLat, restLon, 90.0, (BigDecimal.valueOf(zoneRainParamsLastMile/1.3).divide(BigDecimal.valueOf(2))).doubleValue());

        String custZone = getCustomerZoneFromLatLon(latlonarray[0], latlonarray[1]);
        Integer customerRainMode = getRainMode(String.valueOf(custZone));
        if(zone_id != Integer.parseInt(custZone) && customerRainMode != 3)
            serviceablility_Helper.setNoRainsAtZone(String.valueOf(custZone));

        Map<String,String> mappedResponse = serviceablility_Helper.retryCerebroCart(10, 1000, new String[]
                {listing_item_count, total_item_count, restaurantId, area_id.toString(), city_id.toString(), restaurantName, restLat.toString(), restLon.toString(),
                        latlonarray[0].toString(),latlonarray[1].toString()}, new LinkedHashMap<String,String>(){{
            put("rain_mode","1");
            put("serviceable","2");
            put("is_long_distance","0");
        }});

        String degradationMode = getResponseDegradationMode(mappedResponse);
        serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(restaurantRainMode), "id", String.valueOf(zone_id));
        if(customerRainMode != 3)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(customerRainMode), "id", String.valueOf(custZone));

        Assert.assertEquals(mappedResponse.get("rain_mode"), "1", "rain mode");
        Assert.assertEquals(degradationMode, "2", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceable"), "2", "serviceability");

    }


    @Test(description = "SCTC_19 :- rain_mode_type : 1 at restaurant => current sla > zone_max_sla of restaurant's zone_rain_params of corresponding rain mode")
    public void rainsAtRestaurantMode1_GtSLA()
    {
        rain_mode = "1";
        Integer restaurantRainMode = getRainMode(String.valueOf(zone_id));
        DeliveryDataBaseUtils.setRainModeAndCause(restaurantRainMode, Integer.parseInt(rain_mode), zone_id);//setRainModeAndCause(restaurantRainMode, Integer.parseInt(rain_mode), zone_id);
        Double zoneRainParamsLastMile = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("last_mile").toString());//Double.parseDouble(getValueFromDBusingQeury("select last_mile from zone_rain_params where zone_id=" + zone_id + " and rain_mode=" + rain_mode, "last_mile"));
        Integer zoneRainParamsMaxSLA = Integer.parseInt(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("zone_max_sla").toString());//Double.parseDouble(getValueFromDBusingQeury("select zone_max_sla from zone_rain_params where zone_id=" + zone_id + " and rain_mode=" + rain_mode, "zone_max_sla"));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "10", rain_mode, String.valueOf(zone_id));
        Double[] latlonarray = deliveryDataHelper.getLatLng(restLat, restLon, 90.0, (BigDecimal.valueOf(zoneRainParamsLastMile/1.3).divide(BigDecimal.valueOf(2))).doubleValue());

        String custZone = getCustomerZoneFromLatLon(latlonarray[0], latlonarray[1]);
        Integer customerRainMode = getRainMode(String.valueOf(custZone));
        if(zone_id != Integer.parseInt(custZone) && customerRainMode != 3)
            serviceablility_Helper.setNoRainsAtZone(String.valueOf(custZone));

        Map<String,String> mappedResponse = serviceablility_Helper.retryCerebroCart(10, 1000, new String[]
                {listing_item_count, total_item_count, restaurantId, area_id.toString(), city_id.toString(), restaurantName, restLat.toString(), restLon.toString(),
                        latlonarray[0].toString(),latlonarray[1].toString()}, new LinkedHashMap<String,String>(){{
            put("rain_mode","1");
            put("serviceable","2");
            put("is_long_distance","0");
        }});

        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", String.valueOf(zoneRainParamsMaxSLA), rain_mode, String.valueOf(zone_id));

        serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(restaurantRainMode), "id", String.valueOf(zone_id));
        if(customerRainMode != 3)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(customerRainMode), "id", String.valueOf(custZone));

        Assert.assertEquals(mappedResponse.get("rain_mode"), "1", "rain mode");
        Assert.assertEquals(degradationMode, "2", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceable"), "1", "serviceability");
        Assert.assertEquals(mappedResponse.get("non_serviceable_reason"), "1", "non serviceable reason");

    }


    @Test(description = "SCTC_20 :- rain_mode_type : 2 at restaurant => current last mile > last_mile of restaurant's zone_rain_params of corresponding rain mode")
    public void rainsAtRestaurantMode2_LtLastMile()
    {
        rain_mode = "2";
        Integer restaurantRainMode = getRainMode(String.valueOf(zone_id));
        DeliveryDataBaseUtils.setRainModeAndCause(restaurantRainMode, Integer.parseInt(rain_mode), zone_id);
        //setRainModeAndCause(restaurantRainMode, Integer.parseInt(rain_mode), zone_id);
        Double zoneRainParamsLastMile = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("last_mile").toString());//Double.parseDouble(getValueFromDBusingQeury("select last_mile from zone_rain_params where zone_id=" + zone_id + " and rain_mode=" + rain_mode, "last_mile"));
        Double[] latlonarray = deliveryDataHelper.getLatLng(restLat, restLon, 90.0, (BigDecimal.valueOf(zoneRainParamsLastMile/1.3).divide(BigDecimal.valueOf(2))).doubleValue());

        Map<String, Object> restaurantZoneRainParamsDetails = DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("4", zone_id.toString());
        if(restaurantZoneRainParamsDetails.keySet().toString().equals(row_not_found_key))
            Assert.fail("add entry in zone rain params of rain mode : 4 and zone id : " + zone_id.toString());
        String restaurantZRMmaxSla = restaurantZoneRainParamsDetails.get("zone_max_sla").toString();
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "200", rain_mode, zone_id.toString());
        String custZone = getCustomerZoneFromLatLon(latlonarray[0], latlonarray[1]);
        Integer customerRainMode = getRainMode(String.valueOf(custZone));
        if(zone_id != Integer.parseInt(custZone) && customerRainMode != 3)
            serviceablility_Helper.setNoRainsAtZone(String.valueOf(custZone));

        System.out.println("*********** RAIN MODE ************** " + getRainMode(String.valueOf(zone_id)));

        Map<String,String> mappedResponse = serviceablility_Helper.retryCerebroCart(10, 1000, new String[]
                {listing_item_count, total_item_count, restaurantId, area_id.toString(), city_id.toString(), restaurantName, restLat.toString(), restLon.toString(),
                        latlonarray[0].toString(),latlonarray[1].toString()}, new LinkedHashMap<String,String>(){{
            put("rain_mode","2");
            put("serviceable","2");
            put("is_long_distance","0");
        }});

        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", restaurantZRMmaxSla, rain_mode, zone_id.toString());

        serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(restaurantRainMode), "id", String.valueOf(zone_id));
        if(customerRainMode != 3)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(customerRainMode), "id", String.valueOf(custZone));

        Assert.assertEquals(mappedResponse.get("rain_mode"), "2", "rain mode");
        Assert.assertEquals(degradationMode, "1", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceable"), "2", "restaurant not serviceable");
    }


    @Test(description = "SCTC_21 :- current last mile > last_mile of restaurant's zone_rain_params")
    public void rainsAtRestaurantMode2_GtLastMile()
    {
        rain_mode = "2";
        Integer restaurantRainMode = getRainMode(String.valueOf(zone_id));
        DeliveryDataBaseUtils.setRainModeAndCause(restaurantRainMode, Integer.parseInt(rain_mode), zone_id);
        //setRainModeAndCause(restaurantRainMode, Integer.parseInt(rain_mode), zone_id);

        Double zoneRainParamsLastMile = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("last_mile").toString());//Double.parseDouble(getValueFromDBusingQeury("select last_mile from zone_rain_params where zone_id=" + zone_id + " and rain_mode=" + rain_mode, "last_mile"));
        if(zoneRainParamsLastMile > 1)
        {
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", "1", rain_mode, String.valueOf(zone_id));
            zoneRainParamsLastMile = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("last_mile").toString());//Double.parseDouble(getValueFromDBusingQeury("select last_mile from zone_rain_params where zone_id=" + zone_id + " and rain_mode=" + rain_mode, "last_mile"));
        }
        Double[] latlonarray = deliveryDataHelper.getLatLng(restLat, restLon, 90.0, (BigDecimal.valueOf(zoneRainParamsLastMile/1.3).add(BigDecimal.valueOf(1.5))).doubleValue());
        String custZone = getCustomerZoneFromLatLon(latlonarray[0], latlonarray[1]);
        Integer customerRainMode = getRainMode(String.valueOf(custZone));
        if(zone_id != Integer.parseInt(custZone) && customerRainMode != 3)
            serviceablility_Helper.setNoRainsAtZone(String.valueOf(custZone));
        String zrmMaxSla = DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("zone_max_sla").toString();
        if(Integer.parseInt(zrmMaxSla) < 200)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "200", rain_mode, zone_id.toString());

        Map<String,String> mappedResponse = serviceablility_Helper.retryCerebroCart(10, 1000, new String[]
                {listing_item_count, total_item_count, restaurantId, area_id.toString(), city_id.toString(), restaurantName, restLat.toString(), restLon.toString(),
                        latlonarray[0].toString(),latlonarray[1].toString()}, new LinkedHashMap<String,String>(){{
            put("rain_mode","2");
            put("serviceable","1");
            put("non_serviceable_reason", "0");
            put("is_long_distance","0");
        }});
        String degradationMode = getResponseDegradationMode(mappedResponse);
        serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(restaurantRainMode), "id", String.valueOf(zone_id));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", zrmMaxSla, rain_mode, zone_id.toString());
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", "1", rain_mode, String.valueOf(zone_id));
        if(customerRainMode != 3)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(customerRainMode), "id", String.valueOf(custZone));

        Assert.assertEquals(mappedResponse.get("rain_mode"), "2", "rain mode");
        Assert.assertEquals(degradationMode, "1", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceable"), "1", "serviceability");
        Assert.assertEquals(mappedResponse.get("non_serviceable_reason"), "0", "non serviceable reason");

    }


    @Test(description = "SCTC_22 :- current sla < zone_max_sla of restaurant's zone_rain_params")
    public void rainsAtRestaurantMode2_LtSLA()
    {
        rain_mode = "2";
        Integer restaurantRainMode = getRainMode(String.valueOf(zone_id));
        DeliveryDataBaseUtils.setRainModeAndCause(restaurantRainMode, Integer.parseInt(rain_mode), zone_id);//setRainModeAndCause(restaurantRainMode, 2, zone_id);
        Double zoneRainParamsLastMile = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("last_mile").toString());//Double.parseDouble(getValueFromDBusingQeury("select last_mile from zone_rain_params where zone_id=" + zone_id + " and rain_mode=" + rain_mode, "last_mile"));
        Double[] latlonarray = deliveryDataHelper.getLatLng(restLat, restLon, 90.0, (BigDecimal.valueOf(zoneRainParamsLastMile/1.3).divide(BigDecimal.valueOf(2))).doubleValue());
        Map<String, Object> restaurantZoneRainParamsDetails = DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString());
        if(restaurantZoneRainParamsDetails.keySet().toString().equals(row_not_found_key))
            Assert.fail("add entry in zone rain params of rain mode : " + rain_mode + " and zone id : " + zone_id.toString());
        String restaurantZRMmaxSla = restaurantZoneRainParamsDetails.get("zone_max_sla").toString();
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "200", rain_mode, zone_id.toString());
        String custZone = getCustomerZoneFromLatLon(latlonarray[0], latlonarray[1]);
        Integer customerRainMode = getRainMode(String.valueOf(custZone));
        if(zone_id != Integer.parseInt(custZone) && customerRainMode != 3)
            serviceablility_Helper.setNoRainsAtZone(String.valueOf(custZone));

        Map<String,String> mappedResponse = serviceablility_Helper.retryCerebroCart(10, 1000, new String[]
                {listing_item_count, total_item_count, restaurantId, area_id.toString(), city_id.toString(), restaurantName, restLat.toString(), restLon.toString(),
                        latlonarray[0].toString(),latlonarray[1].toString()}, new LinkedHashMap<String,String>(){{
            put("rain_mode","2");
            put("serviceable","2");
            put("is_long_distance","0");
        }});

        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", restaurantZRMmaxSla, rain_mode, zone_id.toString());
        serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(restaurantRainMode), "id", String.valueOf(zone_id));
        if(customerRainMode != 3)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(customerRainMode), "id", String.valueOf(custZone));

        Assert.assertEquals(mappedResponse.get("serviceable"), "2", "serviceability");
        Assert.assertEquals(degradationMode, "1", "degradation mode");
        Assert.assertEquals(mappedResponse.get("rain_mode"), "2", "rain mode");

    }


    @Test(description = "SCTC_23 :- current sla > zone_max_sla of restaurant's zone_rain_param")
    public void rainsAtRestaurantMode2_GtSLA()
    {
        rain_mode = "2";
        Integer restaurantRainMode = getRainMode(String.valueOf(zone_id));
        DeliveryDataBaseUtils.setRainModeAndCause(restaurantRainMode, Integer.parseInt(rain_mode), zone_id);//setRainModeAndCause(restaurantRainMode, Integer.parseInt(rain_mode), zone_id);
        Double zoneRainParamsLastMile = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("last_mile").toString());//Double.parseDouble(getValueFromDBusingQeury("select last_mile from zone_rain_params where zone_id='" + zone_id + "' and rain_mode='" + rain_mode + "'", "last_mile"));
        Double zoneRainParamsMaxSLA = Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("zone_max_sla").toString());//Double.parseDouble(getValueFromDBusingQeury("select zone_max_sla from zone_rain_params where zone_id='" + zone_id + "' and rain_mode='" + rain_mode + "'", "zone_max_sla"));

        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "10", rain_mode, String.valueOf(zone_id));
        Double[] latlonarray = deliveryDataHelper.getLatLng(restLat, restLon, 90.0, (BigDecimal.valueOf(zoneRainParamsLastMile/1.3).divide(BigDecimal.valueOf(2))).doubleValue());
        System.out.println("*********** ZRM SLA ********** " + Double.parseDouble(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, zone_id.toString()).get("zone_max_sla").toString()));
        String custZone = getCustomerZoneFromLatLon(latlonarray[0], latlonarray[1]);
        Integer customerRainMode = getRainMode(String.valueOf(custZone));
        if(zone_id != Integer.parseInt(custZone) && customerRainMode != 3)
            serviceablility_Helper.setNoRainsAtZone(String.valueOf(custZone));
        System.out.println("*********** RAIN MODE ********** " + getRainMode(String.valueOf(zone_id)));

        Map<String,String> mappedResponse = serviceablility_Helper.retryCerebroCart(10, 1000, new String[]
                {listing_item_count, total_item_count, restaurantId, area_id.toString(), city_id.toString(), restaurantName, restLat.toString(), restLon.toString(),
                        latlonarray[0].toString(),latlonarray[1].toString()}, new LinkedHashMap<String,String>(){{
            put("rain_mode","2");
            put("serviceable","1");
            put("non_serviceable_reason", "1");
            put("is_long_distance","0");
        }});

        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", String.valueOf(zoneRainParamsMaxSLA), rain_mode, String.valueOf(zone_id));

        serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(restaurantRainMode), "id", String.valueOf(zone_id));
        if(customerRainMode != 3)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(customerRainMode), "id", String.valueOf(custZone));

        Assert.assertEquals(mappedResponse.get("serviceable"), "1", "serviceability");
        Assert.assertEquals(mappedResponse.get("non_serviceable_reason"), "1", "non serviceable reason");
        Assert.assertEquals(mappedResponse.get("rain_mode"), "2", "rain mode");
        Assert.assertEquals(degradationMode, "1", "degradation mode");

    }


    @Test(description = "SCTC_24 :- current last mile < last_mile of restaurant's zone_rain_params")
    public void rainsAtRestaurantMode4Gd1_LtLastMile()
    {
        Integer restaurantRainMode = getRainMode(String.valueOf(zone_id));
        DeliveryDataBaseUtils.setRainModeAndCause(restaurantRainMode, Integer.parseInt(rain_mode), zone_id);//setRainModeAndCause(restaurantRainMode, 4, zone_id);
        Double degradationState = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select degradation_state from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("degradation_state").toString());
        if(degradationState != 1)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", "1", "4", String.valueOf(zone_id));

        Double zoneRainParamsLastMile = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select last_mile from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("last_mile").toString());
        Double[] latlonarray = deliveryDataHelper.getLatLng(restLat, restLon, 90.0, (BigDecimal.valueOf(zoneRainParamsLastMile/1.3).divide(BigDecimal.valueOf(2))).doubleValue());
        Map<String, Object> restaurantZoneRainParamDetails = DeliveryDataBaseUtils.getZoneRainParamsFromZoneId("4", String.valueOf(zone_id));
        if(restaurantZoneRainParamDetails.keySet().toString().equals(row_not_found_key))
            Assert.fail("add entry in zone rain params of rain mode : " + rain_mode + " and zone id : " + zone_id.toString());
        Integer restZRMmaxSla = Integer.parseInt(restaurantZoneRainParamDetails.get("zone_max_sla").toString());
        if(restZRMmaxSla < 100)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "200", "4", String.valueOf(zone_id));

        String custZone = getCustomerZoneFromLatLon(latlonarray[0], latlonarray[1]);
        Integer customerRainMode = 0;
        if(zone_id != Integer.parseInt(custZone) && custZone != null)
        {
            customerRainMode = getRainMode(String.valueOf(custZone));
            if(customerRainMode != 3)
                serviceablility_Helper.setNoRainsAtZone(String.valueOf(custZone));
        }

        Map<String,String> mappedResponse = serviceablility_Helper.retryCerebroCart(10, 1000, new String[]
                {listing_item_count, total_item_count, restaurantId, area_id.toString(), city_id.toString(), restaurantName, restLat.toString(), restLon.toString(),
                        latlonarray[0].toString(),latlonarray[1].toString()}, new LinkedHashMap<String,String>(){{
            put("rain_mode","2");
            put("serviceable","2");
            put("is_long_distance","0");
        }});

        String degradationMode = getResponseDegradationMode(mappedResponse);
        serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(restaurantRainMode), "id", String.valueOf(zone_id));
        if(customerRainMode != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(customerRainMode), "id", String.valueOf(custZone));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", String.valueOf(restZRMmaxSla), "4", String.valueOf(zone_id));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", String.valueOf(degradationState), "4", String.valueOf(zone_id));

        Assert.assertEquals(mappedResponse.get("rain_mode"), "2", "rain mode");
        Assert.assertEquals(degradationMode, "1", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceable"), "2", "serviceability");

    }


    @Test(description = "SCTC_25 :- current last mile > last_mile of restaurant's zone_rain_params")
    public void rainsAtRestaurantMode4Gd1_GtLastMile()
    {
        rain_mode = "4";
        Integer restaurantRainMode = getRainMode(String.valueOf(zone_id));
        DeliveryDataBaseUtils.setRainModeAndCause(restaurantRainMode, Integer.parseInt(rain_mode), zone_id);//setRainModeAndCause(restaurantRainMode, 4, zone_id);
        Double degradationState = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select degradation_state from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("degradation_state").toString());
        if(degradationState != 1)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", "1", rain_mode, String.valueOf(zone_id));

        Double zoneRainParamsLastMile = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select last_mile from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("last_mile").toString());
        Double maxLastMile = zoneRainParamsLastMile;
        maxLastMile = (zoneRainParamsLastMile >= actualLastMile) ? zoneRainParamLastMile(zoneRainParamsLastMile, actualLastMile, rain_mode) : zoneRainParamsLastMile;
        Integer restZRMmaxSla = Integer.parseInt(DeliveryDataBaseUtils.getZoneRainParamsFromZoneId(rain_mode, String.valueOf(zone_id)).get("zone_max_sla").toString());
        if(restZRMmaxSla < 100)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", "200", rain_mode, String.valueOf(zone_id));
        Double[] latlonarray = deliveryDataHelper.getLatLng(restLat, restLon, 90.0, (BigDecimal.valueOf(maxLastMile/1.3).add(BigDecimal.valueOf(0.5))).doubleValue());
        String custZone = getCustomerZoneFromLatLon(latlonarray[0], latlonarray[1]);
        Integer customerRainMode = 0;
        if(zone_id != Integer.parseInt(custZone) && custZone != null)
        {
            customerRainMode = getRainMode(custZone);
            if(customerRainMode != 3)
                serviceablility_Helper.setNoRainsAtZone(String.valueOf(custZone));
        }

        Map<String,String> mappedResponse = serviceablility_Helper.retryCerebroCart(10, 1000, new String[]
                {listing_item_count, total_item_count, restaurantId, area_id.toString(), city_id.toString(), restaurantName, restLat.toString(), restLon.toString(),
                        latlonarray[0].toString(),latlonarray[1].toString()}, new LinkedHashMap<String,String>(){{
            put("rain_mode","2");
            put("serviceable","1");
            put("non_serviceable_reason","0");
            put("is_long_distance","0");
        }});

        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", String.valueOf(zoneRainParamsLastMile), rain_mode, String.valueOf(zone_id));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", String.valueOf(degradationState), rain_mode, String.valueOf(zone_id));
        serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(restaurantRainMode), "id", String.valueOf(zone_id));
        if(customerRainMode != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(customerRainMode), "id", String.valueOf(custZone));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", String.valueOf(restZRMmaxSla), rain_mode, String.valueOf(zone_id));

        Assert.assertEquals(mappedResponse.get("rain_mode"), "2", "rain mode");
        Assert.assertEquals(degradationMode, "1", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceable"), "1", "serviceability");
        Assert.assertEquals(mappedResponse.get("non_serviceable_reason"), "0", "non serviceable reason");

    }


    @Test(description = "SCTC_26 :- current sla < zone_max_sla of restaurant's zone_rain_params")
    public void rainsAtRestaurantMode4Gd1_LtSLA()
    {
        rain_mode = "4";
        Integer restaurantRainMode = getRainMode(String.valueOf(zone_id));
        DeliveryDataBaseUtils.setRainModeAndCause(restaurantRainMode, Integer.parseInt(rain_mode), zone_id);//setRainModeAndCause(restaurantRainMode, 4, zone_id);
        //String id = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select id from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("id").toString();
        Double degradationState = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select degradation_state from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("degradation_state").toString());
        if(degradationState != 1)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", "1", rain_mode, String.valueOf(zone_id));

        Double zoneRainParamsLastMile = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select last_mile from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("last_mile").toString());
        Double maxLastMile = zoneRainParamsLastMile;
        if(zoneRainParamsLastMile >= actualLastMile)
        {
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", String.valueOf((actualLastMile - 1)), rain_mode, String.valueOf(zone_id));
            zoneRainParamsLastMile = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select last_mile from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("last_mile").toString());
        }
        Double[] latlonarray = deliveryDataHelper.getLatLng(restLat, restLon, 90.0, (BigDecimal.valueOf(zoneRainParamsLastMile/1.3).divide(BigDecimal.valueOf(2))).doubleValue());

        String custZone = getCustomerZoneFromLatLon(latlonarray[0], latlonarray[1]);
        Integer customerRainMode = 0;
        if(zone_id != Integer.parseInt(custZone) && custZone != null)
        {
            customerRainMode = getRainMode(String.valueOf(custZone));
            if(customerRainMode != 3)
                serviceablility_Helper.setNoRainsAtZone(String.valueOf(custZone));
        }
        Map<String,String> mappedResponse = serviceablility_Helper.retryCerebroCart(10, 1000, new String[]
                {listing_item_count, total_item_count, restaurantId, area_id.toString(), city_id.toString(), restaurantName, restLat.toString(), restLon.toString(),
                        latlonarray[0].toString(),latlonarray[1].toString()}, new LinkedHashMap<String,String>(){{
            put("rain_mode","2");
            put("serviceable","2");
            put("is_long_distance","0");
        }});

        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", String.valueOf(maxLastMile), rain_mode, String.valueOf(zone_id));
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", String.valueOf(degradationState), rain_mode, String.valueOf(zone_id));

        serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(restaurantRainMode), "id", String.valueOf(zone_id));
        if(customerRainMode != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(customerRainMode), "id", String.valueOf(custZone));

        Assert.assertEquals(mappedResponse.get("rain_mode"), "2", "rain mode");
        Assert.assertEquals(degradationMode, "1", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceable"), "2", "serviceability");

    }


    @Test(description = "SCTC_27 :- current sla > zone_max_sla of restaurant's zone_rain_param")
    public void rainsAtRestaurantMode4Gd1_GtSLA()
    {
        rain_mode = "4";
        Integer restaurantRainMode = getRainMode(String.valueOf(zone_id));
        DeliveryDataBaseUtils.setRainModeAndCause(restaurantRainMode, Integer.parseInt(rain_mode), zone_id);//setRainModeAndCause(restaurantRainMode, 4, zone_id);
        Double degradationState = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select degradation_state from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("degradation_state").toString());
        if(degradationState != 1)
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("degradation_state", "1", rain_mode, String.valueOf(zone_id));

        Double zoneRainParamsLastMile = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select last_mile from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("last_mile").toString());
        Double maxLastMile = zoneRainParamsLastMile;
        if(zoneRainParamsLastMile >= actualLastMile)
        {
            DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", String.valueOf(actualLastMile - 1), rain_mode, String.valueOf(zone_id));
            zoneRainParamsLastMile = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select last_mile from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("last_mile").toString());
        }

        Double zoneRainParamsMaxSLA = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select zone_max_sla from zone_rain_params where zone_id=" + zone_id + " and rain_mode=4").get("zone_max_sla").toString());
        SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).update("update zone_rain_params set zone_max_sla='10' where zone_id=" + zone_id + " and rain_mode='4'");
        Double[] latlonarray = deliveryDataHelper.getLatLng(restLat, restLon, 90.0, (BigDecimal.valueOf(zoneRainParamsLastMile/1.3).divide(BigDecimal.valueOf(2))).doubleValue());

        String custZone = getCustomerZoneFromLatLon(latlonarray[0], latlonarray[1]);
        Integer customerRainMode = 0;
        if(zone_id != Integer.parseInt(custZone) && custZone != null)
        {
            customerRainMode = getRainMode(String.valueOf(custZone));
            if(customerRainMode != 3)
                serviceablility_Helper.setNoRainsAtZone(String.valueOf(custZone));
        }
        Map<String,String> mappedResponse = serviceablility_Helper.retryCerebroCart(10, 1000, new String[]
                {listing_item_count, total_item_count, restaurantId, area_id.toString(), city_id.toString(), restaurantName, restLat.toString(), restLon.toString(),
                        latlonarray[0].toString(),latlonarray[1].toString()}, new LinkedHashMap<String,String>(){{
            put("rain_mode","2");
            put("serviceable","1");
            put("non_serviceable_reason","1");
            put("is_long_distance","0");
        }});

        String degradationMode = getResponseDegradationMode(mappedResponse);
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("zone_max_sla", String.valueOf(zoneRainParamsMaxSLA), rain_mode, String.valueOf(zone_id));
        serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(restaurantRainMode), "id", String.valueOf(zone_id));
        if(customerRainMode != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(customerRainMode), "id", String.valueOf(custZone));

        Assert.assertEquals(mappedResponse.get("rain_mode"), "2", "rain mode");
        Assert.assertEquals(degradationMode, "1", "degradation mode");
        Assert.assertEquals(mappedResponse.get("serviceable"), "1", "serviceability");
        Assert.assertEquals(mappedResponse.get("non_serviceable_reason"), "1", "non serviceable reason");

    }

    public Double zoneRainParamLastMile(Double zoneRainParamLM, Double defaultMaxlastMile, String rainMode)
    {
        DeliveryDataBaseUtils.updateZoneRainParamAndClearCache("last_mile", String.valueOf(actualLastMile - 2), rainMode, String.valueOf(zone_id));
        zoneRainParamLM = Double.parseDouble(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForMap("select last_mile from zone_rain_params where zone_id=" + zone_id + " and rain_mode='" + rainMode + "'").get("last_mile").toString());
        return zoneRainParamLM;
    }

    public String getResponseDegradationMode(Map<String, String> mappedResponse)
    {
        String degradationMode = "";
        try {
            degradationMode =  (new JSONObject(mappedResponse.get("degradation"))).get("mode").toString();
        } catch (JSONException | NullPointerException e) {
            e.printStackTrace();
        }
        return degradationMode;
    }

    public String getResponseRainMode(Map<String, String> mappedResponse)
    {
        String degradationMode = "";
        try {
            degradationMode =  (new JSONObject(mappedResponse.get("surge_mode"))).get("rain_mode").toString();
        } catch (JSONException | NullPointerException e) {
            e.printStackTrace();
        }
        return degradationMode;
    }





    /*@Test(groups= "cart_long_distance", description = "SLTC_80 :- Verify Restaurant is not serviceable due to long distance sla, if current sla is greater than the long distance max sla")
    public void a14_LDnonLDequalToLDlm_LtLMGtSlaAtLD()
    {
        String lmAtArea = serviceablility_Helper.getValueFromDb("last_mile_cap", "area", "id", String.valueOf(area_id));
        String lmAtLD = serviceablility_Helper.getValueFromDb("max_last_mile", "long_distance", "polygon_id", String.valueOf(lDPolygonId));
        String LDmaxSla = serviceablility_Helper.getValueFromDb("max_sla", "long_distance", "polygon_id", String.valueOf(lDPolygonId));
        String[] coordinates = LdCoordinates.get(0).split(",");
        int distance = deliveryDataHelper.getDistance(lDrestLat, lDrestLon, Double.parseDouble(coordinates[0]), Double.parseDouble(coordinates[1]))/1000;

        updateLmAtAreaRestaurantAndLD(String.valueOf(distance - 2), String.valueOf(distance - 2));
        updateLDmaxSla("200", String.valueOf(longDistanceId));

        String zoneMaxSla = DeliveryDataBaseUtils.getZoneMaxSla(zone_id.toString());
        DeliveryDataBaseUtils.setZoneMaxSla("200", zone_id.toString());

        String restZoneId = getCustomerZoneFromLatLon(lDrestLat, lDrestLon);
        String custZoneId = getCustomerZoneFromLatLon(Double.parseDouble(coordinates[0]), Double.parseDouble(coordinates[1]));
        Map<String, Integer> rainModeMap = setNoRainsAtRestaurantAndCustomer(restZoneId, custZoneId);
        Map<String,String> mappedResponse = serviceablility_Helper.retryCerebroListing(10, 1000, new String[]
                {coordinates[0], coordinates[1], city_id.toString(), lDrestaurantId}, new LinkedHashMap<String,String>(){{
            put("rain_mode", "0");
            put("serviceability", "0");
            put("non_serviceable_reason", "0");
            put("long_distance", "1");
        }});
        if(rainModeMap.get("restaurantRainMode") != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(rainModeMap.get("restaurantRainMode")), "id", restZoneId);
        if(custZoneId != null && rainModeMap.get("customerRainMode") != 0)
            serviceablility_Helper.setValueInDb("zone", "rain_mode_type", String.valueOf(rainModeMap.get("customerRainMode")), "id", custZoneId);
        updateLmAtAreaRestaurantAndLD(lmAtArea, lmAtLD);
        updateLDmaxSla(LDmaxSla, String.valueOf(longDistanceId));
        DeliveryDataBaseUtils.setZoneMaxSla(zoneMaxSla, zone_id.toString());
        Assert.assertEquals(mappedResponse.get("long_distance"),"1", "long distance");
        Assert.assertEquals(mappedResponse.get("serviceability"),"0", "serviceability");
        Assert.assertEquals(mappedResponse.get("non_serviceable_reason"),"1", "non serviceability reason");
    }*/

    public void updateLmAtAreaRestaurantAndLD(String nonLDLm, String LDlm)
    {
        serviceablility_Helper.setValueInDb("area", "last_mile_cap", String.valueOf(nonLDLm), "id", String.valueOf(area_id));
        serviceablility_Helper.setValueInDb("restaurant", "max_second_mile", String.valueOf(nonLDLm), "id", lDrestaurantId);
        serviceablility_Helper.setValueInDb("long_distance", "max_last_mile", String.valueOf(LDlm), "polygon_id", String.valueOf(lDPolygonId));
        //serviceablility_Helper.setValueInDb("restaurant", "max_long_distance_last_mile", String.valueOf(Double.parseDouble(LDlm) + 1.0), "id", String.valueOf(lDrestaurantId));
    }

    public void updateLDmaxSla(String maxSla, String longDistanceId)
    {
        serviceablility_Helper.setValueInDb("long_distance", "max_sla", maxSla, "id", longDistanceId);
    }

    public Map<String, Integer> setNoRainsAtRestaurantAndCustomer(String restZoneId, String custZoneId)
    {
        Integer restRainMode = 0, customerRainMode = 0;
        Map<String, Integer> rainModeMap = new LinkedHashMap<String, Integer>();
        if(restZoneId != null)
        {
            restRainMode = getRainMode(restZoneId);
            rainModeMap.put("restaurantRainMode", restRainMode);
            if(restRainMode != 3)
                serviceablility_Helper.setNoRainsAtZone(restZoneId);
        }
        if(custZoneId != null)
        {
            customerRainMode = getRainMode(String.valueOf(custZoneId));
            rainModeMap.put("customerRainMode", customerRainMode);
            if(customerRainMode != 3)
                serviceablility_Helper.setNoRainsAtZone(String.valueOf(custZoneId));
        }
        System.out.println("***** MAP ****** " + rainModeMap);
        return rainModeMap;
    }

    public Integer getRainMode(String zone_id)
    {
        return Integer.parseInt(SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).
                queryForMap( "select rain_mode_type from zone where id="+zone_id).get("rain_mode_type").toString());
    }


    public void printMap(Map<?,?> printMap)
    {
        for(Object str:printMap.keySet())
        {
            System.out.println("**********************************************************************************");
            System.out.println(str+"::::"+printMap.get(str).toString());
        }
    }

}
