package com.swiggy.api.erp.cms.pojo.SelfServeV1Item;

public class Variant_groups {
	private String group_id;

    private String item_id;

    private Variations[] variations;

    private String name;

    private String external_group_id;

    public String getGroup_id ()
    {
        return group_id;
    }

    public void setGroup_id (String group_id)
    {
        this.group_id = group_id;
    }

    public String getItem_id ()
    {
        return item_id;
    }

    public void setItem_id (String item_id)
    {
        this.item_id = item_id;
    }

    public Variations[] getVariations ()
    {
        return variations;
    }

    public void setVariations (Variations[] variations)
    {
        this.variations = variations;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getExternal_group_id ()
    {
        return external_group_id;
    }

    public void setExternal_group_id (String external_group_id)
    {
        this.external_group_id = external_group_id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [group_id = "+group_id+", item_id = "+item_id+", variations = "+variations+", name = "+name+", external_group_id = "+external_group_id+"]";
    }
}
			
			