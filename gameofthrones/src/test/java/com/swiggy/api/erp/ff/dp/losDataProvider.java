package com.swiggy.api.erp.ff.dp;

import org.testng.annotations.DataProvider;

import com.swiggy.api.erp.ff.constants.LosConstants;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class losDataProvider {

	@DataProvider(name="createDataForOrderCreation")
	public static Iterator<Object[]> createDataForOrderCreation()
	{
		Date date = new Date();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		DateFormat order_idFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		String order_id = order_idFormat.format(date);
		String order_time = dateFormat.format(date);
		System.out.println("Order time = " + order_time);
		String epochTime = String.valueOf(Instant.now().getEpochSecond());
		System.out.println("Epoch time = " + epochTime);
		List<Object[]> list = new ArrayList<>();
		boolean check = false;
		for(int i=0;i<LosConstants.status.length;i++){
			check = i == 0;
			list.add(new Object[]{order_id, order_time, epochTime, LosConstants.status[i], check});
		}
		return list.iterator();
	}

	
	@DataProvider(name = "orderCreationData")
	public static Object[][] orderCreationData(){
		Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat order_idFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        String order_id = order_idFormat.format(date);
        String order_time = dateFormat.format(date);
        System.out.println("Order time = " + order_time);
        String epochTime = String.valueOf(Instant.now().getEpochSecond());
        System.out.println("Epoch time = " + epochTime);

        return new Object[][]{
                {order_id, order_time, epochTime}
        };
	}
}
