package com.swiggy.api.erp.cms.pojo.SelfServeUpdateTicketStatus;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.erp.cms.pojo.SelfServeUpdateTicketStatus
 **/

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "restaurantId",
        "uniqueId",
        "data",
        "status",
        "field_updated",
        "updated_at",
        "created_at",
        "item_name",
        "assigned_to",
        "group_id",
        "type"
})
public class UpdateTicketStatus {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("restaurantId")
    private Integer restaurantId;
    @JsonProperty("uniqueId")
    private String uniqueId;
    @JsonProperty("data")
    private String data;
    @JsonProperty("status")
    private String status;
    @JsonProperty("field_updated")
    private List<Field_updated> field_updated = null;
    @JsonProperty("updated_at")
    private Integer updated_at;
    @JsonProperty("created_at")
    private Integer created_at;
    @JsonProperty("item_name")
    private String item_name;
    @JsonProperty("assigned_to")
    private Integer assigned_to;
    @JsonProperty("group_id")
    private String group_id;
    @JsonProperty("type")
    private String type;

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("restaurantId")
    public Integer getRestaurantId() {
        return restaurantId;
    }

    @JsonProperty("restaurantId")
    public void setRestaurantId(Integer restaurantId) {
        this.restaurantId = restaurantId;
    }

    @JsonProperty("uniqueId")
    public String getUniqueId() {
        return uniqueId;
    }

    @JsonProperty("uniqueId")
    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    @JsonProperty("data")
    public String getData() {
        return data;
    }

    @JsonProperty("data")
    public void setData(String data) {
        this.data = data;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("field_updated")
    public List<Field_updated> getField_updated() {
        return field_updated;
    }

    @JsonProperty("field_updated")
    public void setField_updated(List<Field_updated> field_updated) {
        this.field_updated = field_updated;
    }

    @JsonProperty("updated_at")
    public Integer getUpdated_at() {
        return updated_at;
    }

    @JsonProperty("updated_at")
    public void setUpdated_at(Integer updated_at) {
        this.updated_at = updated_at;
    }

    @JsonProperty("created_at")
    public Integer getCreated_at() {
        return created_at;
    }

    @JsonProperty("created_at")
    public void setCreated_at(Integer created_at) {
        this.created_at = created_at;
    }

    @JsonProperty("item_name")
    public String getItem_name() {
        return item_name;
    }

    @JsonProperty("item_name")
    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    @JsonProperty("assigned_to")
    public Integer getAssigned_to() {
        return assigned_to;
    }

    @JsonProperty("assigned_to")
    public void setAssigned_to(Integer assigned_to) {
        this.assigned_to = assigned_to;
    }

    @JsonProperty("group_id")
    public String getGroup_id() {
        return group_id;
    }

    @JsonProperty("group_id")
    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    private void setDefaultValues (String data, String restID, String ticketID) {

    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("restaurantId", restaurantId).append("uniqueId", uniqueId).append("data", data).append("status", status).append("field_updated", field_updated).append("updated_at", updated_at).append("created_at", created_at).append("item_name", item_name).append("assigned_to", assigned_to).append("group_id", group_id).append("type", type).toString();
    }

}
