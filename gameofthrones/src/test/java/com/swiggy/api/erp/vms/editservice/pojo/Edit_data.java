package com.swiggy.api.erp.vms.editservice.pojo;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"oos_data"
})
public class Edit_data {

@JsonProperty("oos_data")
private List<Oos_datum> oos_data = null;

/**
* No args constructor for use in serialization
* 
*/
public Edit_data() {
}

/**
* 
* @param oos_data
*/
public Edit_data(List<Oos_datum> oos_data) {
super();
this.oos_data = oos_data;
}

@JsonProperty("oos_data")
public List<Oos_datum> getOos_data() {
return oos_data;
}

@JsonProperty("oos_data")
public void setOos_data(List<Oos_datum> oos_data) {
this.oos_data = oos_data;
}

public Edit_data withOos_data(List<Oos_datum> oos_data) {
this.oos_data = oos_data;
return this;
}

@Override
public String toString() {
return new ToStringBuilder(this).append("oos_data", oos_data).toString();
}

@Override
public int hashCode() {
return new HashCodeBuilder().append(oos_data).toHashCode();
}

@Override
public boolean equals(Object other) {
if (other == this) {
return true;
}
if ((other instanceof Edit_data) == false) {
return false;
}
Edit_data rhs = ((Edit_data) other);
return new EqualsBuilder().append(oos_data, rhs.oos_data).isEquals();
}

}