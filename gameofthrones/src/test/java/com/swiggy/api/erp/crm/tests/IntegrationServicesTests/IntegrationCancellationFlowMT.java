package com.swiggy.api.erp.crm.tests.IntegrationServicesTests;

import com.swiggy.api.erp.crm.constants.FlowMapper;
import com.swiggy.api.erp.crm.dp.MockServiceDP;
import com.swiggy.api.erp.crm.helper.CRMUpstreamValidation;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.RedisHelper;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static org.testng.Assert.assertTrue;

/**
 * Created by Swati Samaji on 17/01/19.
 */

public  class IntegrationCancellationFlowMT extends MockServiceDP {

    FlowMapper flowMapper = new FlowMapper();
    Initialize gameofthrones = new Initialize();
    RedisHelper redisHelper = new RedisHelper();
    CRMUpstreamValidation crmUpstreamValidation = new CRMUpstreamValidation();

    @Test(dataProvider = "integrationFlows", groups = {"regression"}, description = "End to end tests for cancellation flow")
    public void integrationCancellationFlow(HashMap<String, String> flowDetails) throws IOException, InterruptedException {

        String orderType = flowDetails.get("orderType").toString();
        String ffStatus = flowDetails.get("ffStatus").toString();
        String deliveryStatus = flowDetails.get("deliveryStatus").toString();
        String flowName = flowDetails.get("flow").toString();
        String conversationId, deviceId;
        String[] flow = flowMapper.setFlow().get(flowName);
        String queryparam[] = new String[1];
        String nodeId, orderId = null;
        int lengthofflow = flow.length;
        System.out.println("lengthofflow \t" + lengthofflow);


        redisHelper.setValueJson("checkoutredis", 0, "user_credit_7191663", "{\"userId\":7191663,\"swiggyMoney\":0.0,\"cancellationFee\":0.0}");
        String getValue = (String) redisHelper.getValue("checkoutredis", 0, "user_credit_7191663");
        System.out.println("key value" + getValue);

        HashMap<String, String> map = crmUpstreamValidation.e2e("7507220659", "Test@2211", "757", "613982", "1", deliveryStatus, ffStatus);
        System.out.print("map returned \t " + map);

        orderId = map.get("order_id");
        Assert.assertNotNull(orderId);
        conversationId = orderId;
        deviceId = orderId;
        String userId = "5770026";
        Assert.assertNotNull(orderId);

        for (int i = 0; i < lengthofflow - 1; i++) {

            nodeId = flow[i];
            if (nodeId == "1" || nodeId == "2") {
                continue;
            } else {
                HashMap<String, String> requestheaders_cancellationinbound = new HashMap<String, String>();
                requestheaders_cancellationinbound.put("Content-Type", "application/json");
                System.out.println("length of flow" + lengthofflow);
                queryparam[0] = nodeId;
                String[] paylaodparam = new String[]{userId, orderId,conversationId };

                // Invalid Tenant Id for negative Test
                requestheaders_cancellationinbound.put("tenantId", "efdeac62db1ef01652053dd85f474ef626f3710277a63e77e109fc35cdaff6e");
                GameOfThronesService cancellationInvalidinbound = new GameOfThronesService("crm", "validChildrenNew", gameofthrones);

                Processor cancellationInvalidinbound_response = new Processor(cancellationInvalidinbound,
                        requestheaders_cancellationinbound, paylaodparam, queryparam);
                Assert.assertEquals(cancellationInvalidinbound_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("status"), "FAILURE");
                String expectedErrorMessage = "is not registered with us. Please contact support to register the tenant with regression.";
                expectedErrorMessage = expectedErrorMessage.replace("\"","");
                String actualErrorMessage = cancellationInvalidinbound_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("exception..errorMessage").toString().replace("[", "").replace("]", "");
                System.out.println("Actual Error Message  " + actualErrorMessage);
                Assert.assertTrue(actualErrorMessage.contains(expectedErrorMessage),"Actual error message doesn't match with expected ");
                requestheaders_cancellationinbound.remove("tenantId");

                // Valid Tenant Id for Positive Test
                requestheaders_cancellationinbound.put("tenantId", "eefdeac62db1ef01652053dd85f474ef626f3710277a63e77e109fc35cdaff6e");
                GameOfThronesService cancellationinbound = new GameOfThronesService("crm", "validChildrenNew", gameofthrones);

                Processor cancellationinbound_response = new Processor(cancellationinbound,
                        requestheaders_cancellationinbound, paylaodparam, queryparam);
                Assert.assertEquals(cancellationinbound_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("status"), "SUCCESS");
                String addOnGroupList = cancellationinbound_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.childrenNodes..id").toString().replace("[", "").replace("]", "");
                List<String> addOnlist = Arrays.asList(addOnGroupList.split(","));

                System.out.println("addonlist" + addOnlist);
                System.out.println("flowname[i+1]" + flow[i + 1]);

                assertTrue(addOnlist.contains(flow[i + 1]));
                int index = addOnlist.indexOf(flow[i + 1]);

                if (i < (lengthofflow - 2)) {
                    Assert.assertEquals(cancellationinbound_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.childrenNodes.[" + index + "].isLeaf"), "false");
                } else {
                    Assert.assertEquals(cancellationinbound_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.childrenNodes.[" + index + "].isLeaf"), "true");
                }
            }
        }
    }
}
