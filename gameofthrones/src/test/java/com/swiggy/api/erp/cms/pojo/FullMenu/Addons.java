package com.swiggy.api.erp.cms.pojo.FullMenu;

import com.swiggy.api.erp.cms.constants.MenuConstants;

/**
 * Created by kiran.j on 2/7/18.
 */
public class Addons {

    private String id;

    private double price;

    private int order;

    private boolean in_stock;

    private int is_veg;

    private String name;

   // private String gst_details;
    private Gst_Details gst_details;

    private boolean is_default;

    private boolean veg;

    private boolean getdefault;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public boolean isIn_stock() {
        return in_stock;
    }

    public void setIn_stock(boolean in_stock) {
        this.in_stock = in_stock;
    }

    public int getIs_veg() {
        return is_veg;
    }

    public void setIs_veg(int isveg) {
        this.is_veg = isveg;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Gst_Details getGst_details() {
        return gst_details;
    }

    public void setGst_details(Gst_Details gst_details) {
        this.gst_details = gst_details;
    }

    public boolean isIs_default() {
        return is_default;
    }

    public void setIs_default(boolean is_default) {
        this.is_default = is_default;
    }

    public boolean isVeg() {
        return veg;
    }

    public void setVeg(boolean veg) {
        this.veg = veg;
    }

    public boolean isGetdefault() {
        return getdefault;
    }

    public void setGetdefault(boolean getdefault) {
        this.getdefault = getdefault;
    }

    public Addons build() {
        setDefaultValues();
        return this;
    }

    public void setDefaultValues() {
        if(this.getId() == null)
            this.setId(MenuConstants.addonid);
        if(this.getName() == null)
            this.setName(MenuConstants.addons_groups_name);
        if(this.getOrder() == 0)
            this.setOrder(MenuConstants.addons_groups_order);
        if(this.isIn_stock() == false)
            this.setIn_stock(true);
        this.setIs_veg(1);
    }

    public void setAddonBlank() {
        if(this.getId() == null)
            this.setId("");
        if(this.getName() == null)
            this.setName("");
        if(this.getOrder() == 0)
            this.setOrder(Integer.parseInt(null));
     
    }
    @Override
    public String toString() {
        return "{" +
                "id='" + id + '\'' +
                ", price=" + price +
                ", order=" + order +
                ", in_stock=" + in_stock +
                ", is_veg=" + is_veg +
                ", name='" + name + '\'' +
                ", gst_details='" + gst_details + '\'' +
                ", is_default=" + is_default +
                ", veg=" + veg +
                ", getdefault=" + getdefault +
                '}';
    }
}
