package com.swiggy.api.erp.cms.tests;
import org.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.dp.POP_dataProvider;
import com.swiggy.api.erp.cms.helper.POP_Restaurant_Open_Close_Helper;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.Processor;

/*
 * This class is responsible to open or close a restaurant with  Restaurant-ID = 4553
 *
 */

public class POP_Restaurant_Open_Close extends POP_dataProvider {
	
	Initialize gameofthrones = new Initialize();
	POP_Restaurant_Open_Close_Helper obj = new POP_Restaurant_Open_Close_Helper();

	@Test(dataProvider="closeRestaurantWithMenuMap", priority=0, groups={"Sanity_TC","Smoke_TC", "Regression_TC"},
			description="Close a Restaurant with existing Restaurant Menu Map")
	public void restaurant_Close_withRestMenuMap(int restaurant_id, String from_time, String to_time) throws JSONException
	{
		Processor p1 = obj.restaurant_Close_withRestaurantMenuMap(restaurant_id, from_time, to_time);
		String response = p1.ResponseValidator.GetBodyAsText();
		String statusMessage = JsonPath.read(response,"$.statusMessage");
		int statusCode = JsonPath.read(response,"$.statusCode");
		Assert.assertEquals(statusMessage, "success");
		Assert.assertEquals(statusCode, 1);
	}

	@Test(dataProvider="closeRestaurantWithoutMenuMap", priority=1, groups={"Smoke_TC","Regression_TC"},
			description="Close a Restaurant with non-existing Restaurant Menu Map")
	public void restaurant_Close_withoutRestMenuMap(int restaurant_id, String from_time, String to_time) throws JSONException
	{
		Processor p1 = obj.restaurant_Close_withoutRestaurantMenuMap(restaurant_id, from_time, to_time);
		String response = p1.ResponseValidator.GetBodyAsText(); 
		String statusMessage = JsonPath.read(response,"$.statusMessage");
		int statusCode = JsonPath.read(response,"$.statusCode");
	//	Assert.assertEquals(statusMessage, "success");
		//Assert.assertEquals(statusCode, 1);
	}

	@Test(dataProvider="openRestaurantWithMenuMap", priority=1, groups={"Sanity_TC","Smoke_TC","Regression_TC"},
			description="Open a Restaurant with existing Restaurant Menu Map")
	public void restaurant_Open_withRestMenuMap(int restaurant_id, String from_time, String to_time) throws JSONException
	{
		Processor p1 = obj.restaurant_Open_withRestaurantMenuMap(restaurant_id, from_time, to_time);

		String response = p1.ResponseValidator.GetBodyAsText();
		String statusMessage = JsonPath.read(response,"$.statusMessage");
		int statusCode = JsonPath.read(response,"$.statusCode");
		boolean data = JsonPath.read(response,"$.data");
		Assert.assertEquals(statusMessage, "success");
		Assert.assertEquals(statusCode, 1);
		Assert.assertEquals(data, true);

	}

	@Test(dataProvider="openRestaurantWithoutMenuMap", priority=2, groups={"Smoke_TC","Regression_TC"},
			description="Open a Restaurant with non-existing Restaurant Menu Map")
	public void restaurant_Open_withoutRestMenuMap(int restaurant_id, String from_time, String to_time) throws JSONException
	{
		Processor p1 = obj.restaurant_Open_withoutRestaurantMenuMap(restaurant_id, from_time, to_time);
		String response = p1.ResponseValidator.GetBodyAsText();
		String statusMessage = JsonPath.read(response,"$.statusMessage");
		int statusCode = JsonPath.read(response,"$.statusCode");
		boolean data = JsonPath.read(response,"$.data");
		Assert.assertEquals(statusMessage, "success");
		Assert.assertEquals(statusCode, 1);
		Assert.assertEquals(data, true);
	}

	@Test(dataProvider="openRestaurantWithMenuMap", priority=3, groups={"Smoke_TC","Regression_TC"},
			description="Open a Restaurant with invalid/non-existing Restaurant-ID")
	public void restaurant_Open_withoutRestID(int restaurant_id, String from_time, String to_time) throws JSONException
	{
		Processor p1 = obj.restaurant_Open_withoutRestaurantID(restaurant_id, from_time, to_time);
		String response = p1.ResponseValidator.GetBodyAsText();
		String message = JsonPath.read(response,"$.message");
		int status = JsonPath.read(response,"$.status");
		String exception = JsonPath.read(response,"$.exception");
		Assert.assertEquals(message, "Request method 'POST' not supported");
		Assert.assertEquals(status, 405);
		Assert.assertEquals(exception, "org.springframework.web.HttpRequestMethodNotSupportedException");
	}
}
