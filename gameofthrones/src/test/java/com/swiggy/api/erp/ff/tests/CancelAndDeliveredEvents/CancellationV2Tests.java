package com.swiggy.api.erp.ff.tests.CancelAndDeliveredEvents;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.ff.constants.ContextualCancellationConstants;
import com.swiggy.api.erp.ff.dp.ContextualCancellationData;
import com.swiggy.api.erp.ff.helper.ContextualCancellationHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 Created by Narendra on 28-Jan-2019.
 */
public class CancellationV2Tests extends ContextualCancellationData implements ContextualCancellationConstants {

    Initialize gameofthrones = new Initialize();
    ContextualCancellationHelper helper = new ContextualCancellationHelper();
    Logger log = Logger.getLogger(CancellationV2Tests.class);

    @Test(dataProvider = "cancellationApi", groups = { "sanity", "regression" },description = "Cancellation V2 Api Validator")
    public void testCancellationV2Api(String action, String disposition_id, String sub_disposition_id, String cancellation_fee, String cancellation_fee_applicability, String OrderId, HashMap hm) throws JSONException {
        log.info("***************************************** Cancellation V2 Api started *****************************************");
        String[] params = new String[] { action, disposition_id, sub_disposition_id, cancellation_fee, cancellation_fee_applicability };
        GameOfThronesService getSession = new GameOfThronesService("oms", "ffcancellationv2api", gameofthrones);
        Processor cancellationApi_response = new Processor(getSession, helper.getAuthHeaders(), params, new String[] { OrderId });
        String resp = cancellationApi_response.ResponseValidator.GetBodyAsText();
        String statusMessage = JsonPath.read(resp, "$.response.message");
        String cancellation_initiated = JsonPath.read(resp, "$.data.cancellation_status.cancellation_initiated").toString();
        if (hm.get("Testname") != "successCase" && hm.get("Testname") != "invalidinvalidCancellationFeeApplicability" && hm.get("Testname") != "invalidCancellationFee") {

            String message = JsonPath.read(resp, "$.response.errors[0].message");
            String error_field = JsonPath.read(resp, "$.response.errors[0].error_field").toString();
            String rejected_value = JsonPath.read(resp, "$.response.errors[0].rejected_value").toString();

            Assert.assertEquals(String.valueOf(cancellationApi_response.ResponseValidator.GetResponseCode()), hm.get("httpStatusCode"));
            Assert.assertEquals(statusMessage, hm.get("statusMessage"));
            Assert.assertEquals(message, hm.get("message"));
            Assert.assertEquals(error_field, hm.get("error_field"));
            Assert.assertEquals(rejected_value, hm.get("rejected_value"));
            Assert.assertEquals(cancellation_initiated, hm.get("cancellation_initiated"));
        }

        Assert.assertEquals(String.valueOf(cancellationApi_response.ResponseValidator.GetResponseCode()), hm.get("httpStatusCode"));
        Assert.assertEquals(statusMessage, hm.get("statusMessage"));
        log.info(hm.get("cancellation_initiated").getClass().getName());
        Assert.assertEquals(cancellation_initiated, hm.get("cancellation_initiated"));

        log.info("***************************************** Cancellation V2 Api Completed *****************************************");
    }

    @Test(dataProvider = "cancellationApiDbValidate", groups = { "sanity", "regression" },description = "Cancellation V2 Api DB Validator")
    public void testCancellationApiV2DbValidate(String action, String disposition_id, String sub_disposition_id, String cancellation_fee, String cancellation_fee_applicability, String OrderId) throws JSONException {
        log.info("***************************************** Cancellation V2 Api started *****************************************");
        String[] params = new String[] { action, disposition_id, sub_disposition_id, cancellation_fee, cancellation_fee_applicability };
        GameOfThronesService getSession = new GameOfThronesService("oms", "ffcancellationv2api", gameofthrones);
        Processor cancellationApi_response = new Processor(getSession, helper.getAuthHeaders(), params, new String[] { OrderId });
        String resp = cancellationApi_response.ResponseValidator.GetBodyAsText();
        List<Map<String, Object>> orderStatus = SystemConfigProvider.getTemplate("checkout").queryForList("SELECT order_status FROM swiggy_order_management.swiggy_orders where id = " + OrderId + ";");
        String statusMessage = JsonPath.read(resp, "$.response.message");
        Assert.assertEquals(String.valueOf(cancellationApi_response.ResponseValidator.GetResponseCode()), "200");
        Assert.assertEquals(statusMessage, "Success");
        Assert.assertEquals(orderStatus.get(0).get("order_status"), "cancelled");
        log.info("***************************************** Cancellation V2 Api Completed *****************************************");
    }

}
