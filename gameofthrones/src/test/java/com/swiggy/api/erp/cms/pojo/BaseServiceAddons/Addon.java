package com.swiggy.api.erp.cms.pojo.BaseServiceAddons;

import com.swiggy.api.erp.cms.helper.BaseServiceHelper;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.erp.cms.pojo.BaseServiceAddons
 **/
public class Addon
{
    BaseServiceHelper baseServiceHelper = new BaseServiceHelper();
    private String timestamp = baseServiceHelper.getTimeStampRandomised();

    private String created_by;

    private boolean in_stock;

    private boolean is_veg;

    private int restaurant_id;

    private String third_party_id;

    private int[] addon_group_ids;

    private String updated_at;

    @JsonProperty("default")
    private int is_default;

    private double price;

    private int order;

    private String name;

    private Gst_details gst_details;

    private String updated_by;

    private String created_at;

    private String active;

    public String getCreated_by ()
    {
        return created_by;
    }

    public void setCreated_by (String created_by)
    {
        this.created_by = created_by;
    }

    public boolean getIn_stock ()
    {
        return in_stock;
    }

    public void setIn_stock (boolean in_stock)
    {
        this.in_stock = in_stock;
    }

    public boolean getIs_veg ()
    {
        return is_veg;
    }

    public void setIs_veg (boolean is_veg)
    {
        this.is_veg = is_veg;
    }

    public int getRestaurant_id ()
    {
        return restaurant_id;
    }

    public void setRestaurant_id (int restaurant_id)
    {
        this.restaurant_id = restaurant_id;
    }


    public String getThird_party_id ()
    {
        return third_party_id;
    }

    public void setThird_party_id (String third_party_id)
    {
        this.third_party_id = third_party_id;
    }

    public int[] getAddon_group_ids ()
    {
        return addon_group_ids;
    }

    public void setAddon_group_ids (int[] addon_group_ids)
    {
        this.addon_group_ids = addon_group_ids;
    }

    public String getUpdated_at ()
    {
        return updated_at;
    }

    public void setUpdated_at (String updated_at)
    {
        this.updated_at = updated_at;
    }

    public int getIs_Default ()
    {
        return is_default;
    }

    public void setIs_Default (int is_default)
    {
        this.is_default = is_default;
    }

    public double getPrice () { return price; }

    public void setPrice (double price)
    {
        this.price = price;
    }

    public int getOrder ()
    {
        return order;
    }

    public void setOrder (int order)
    {
        this.order = order;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public Gst_details getGst_details ()
    {
        return gst_details;
    }

    public void setGst_details (Gst_details gst_details)
    {
        this.gst_details = gst_details;
    }

    public String getUpdated_by ()
    {
        return updated_by;
    }

    public void setUpdated_by (String updated_by)
    {
        this.updated_by = updated_by;
    }

    public String getCreated_at ()
    {
        return created_at;
    }

    public void setCreated_at (String created_at)
    {
        this.created_at = created_at;
    }

    public String getActive ()
    {
        return active;
    }

    public void setActive (String active)
    {
        this.active = active;
    }

    public void setDefaultValues(int restID) {
        Gst_details gst_details = new Gst_details();
        int[] tmp = {1438131};
        if(this.getIn_stock() == false)
            this.setIn_stock(true);
        if(this.getIs_veg() == false)
            this.setIs_veg(true);
        if(this.getRestaurant_id() == 0)
            this.setRestaurant_id(restID);
        if(this.getThird_party_id() == null)
            this.setThird_party_id("Automation_"+timestamp);
        if(this.getName() == null)
            this.setName("Automation_BaseService_"+timestamp);
        if(this.getActive() == null)
            this.setActive("true");
        if(this.getOrder() == 0)
            this.setOrder(0);
        if(this.getPrice() == 0.0)
            this.setPrice(100.0);
        if(this.getIs_Default() == 0)
            this.setIs_Default(0);
        if(this.getUpdated_at() == null)
            this.setUpdated_at("2017-01-03T05:20:54.847Z");
        if(this.getCreated_at() == null)
            this.setCreated_at("2017-01-03T05:20:54.846Z");
        if(this.getCreated_by() == null )
            this.setCreated_by("Automation_BaseService_Suite");
        if(this.getUpdated_by() == null)
            this.setUpdated_by("Automation_BaseService");
        if(this.getAddon_group_ids() == null)
            this.setAddon_group_ids(tmp);
        this.setGst_details(gst_details.build());

    }

    public Addon build(int restID) {
        setDefaultValues(restID);
        return this;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [created_by = "+created_by+", in_stock = "+in_stock+", is_veg = "+is_veg+", restaurant_id = "+restaurant_id+", third_party_id = "+third_party_id+", addon_group_ids = "+addon_group_ids+", updated_at = "+updated_at+", default = "+is_default+", price = "+price+", order = "+order+", name = "+name+", gst_details = "+gst_details+", updated_by = "+updated_by+", created_at = "+created_at+", active = "+active+"]";
    }
}
