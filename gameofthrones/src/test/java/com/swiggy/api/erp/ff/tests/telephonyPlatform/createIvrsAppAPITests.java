package com.swiggy.api.erp.ff.tests.telephonyPlatform;

import com.swiggy.api.erp.ff.dp.telephonyPlatform.TelephonyPlatformTestData;
import com.swiggy.api.erp.ff.helper.TelephonyPlatformHelper;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.WireMockHelper;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.util.HashMap;

public class createIvrsAppAPITests extends TelephonyPlatformTestData{

    TelephonyPlatformHelper helper = new TelephonyPlatformHelper();
    WireMockHelper wireMockHelper = new WireMockHelper();
    @BeforeSuite
    public void startMockServer(){
        System.out.println("Starting mock server.......!");
        wireMockHelper.startMockServer(6666);
        System.out.println("Mock Server Started.........!");
    }

    @AfterSuite
    public void stopMockServer() throws InterruptedException {
        System.out.println("Stopping mock stopping.......!");
        wireMockHelper.stopMockServer();
        System.out.println("Mock Server stopped.........!");
    }

    @Test(dataProvider = "createIvrsAppData", description = "Test create IVRS App api of telephony to create new apps")
    public void createIvrsAppAPITests(HashMap<String, String> header, String appName, String podName, String appUrl, String completedCallbackUrl, String failedCallbackUrl, String keyPressCallbackUrl, HashMap<String, String> expectedResponseMap){
        System.out.println("***************************************** createIvrsAppAPITests started *****************************************");
        Processor response = helper.createIvrsApp(header, appName, podName, appUrl, completedCallbackUrl, failedCallbackUrl, keyPressCallbackUrl);
        helper.assertRegisterIvrsAppAPIResponses(response, expectedResponseMap);

        if (response.ResponseValidator.GetResponseCode() == 200){
            helper.assertRegisterIvrsAppDataInDBAfterHittingAPI(response.ResponseValidator.GetNodeValueAsInt("appId"), response.ResponseValidator.GetNodeValueAsInt("clientId"));
        }

        System.out.println("######################################### createIvrsAppAPITests compleated #########################################");
    }
}
