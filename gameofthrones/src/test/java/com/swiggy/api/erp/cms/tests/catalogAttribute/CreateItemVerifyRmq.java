package com.swiggy.api.erp.cms.tests.catalogAttribute;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.dp.itemDP;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Tyrion.RabbitMQHelper;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class CreateItemVerifyRmq extends itemDP {
    Initialize gameofthrones = new Initialize();
    RabbitMQHelper helper = new RabbitMQHelper();
    CMSHelper cmsHelper = new CMSHelper();

    @BeforeClass
    public void purgeRmq() {
        try {
            helper.purgeQueue("oms", "item.nam.change.queue");
        } catch (IOException e) {
        } catch (TimeoutException e) {
        }
    }

    String itemName;
    int itmId;


    @Test(dataProvider = "createitempj",description = "Verify create item API")
    public void createItem(String name) {
        String response = cmsHelper.createItem(name).ResponseValidator.GetBodyAsText();
        itemName = JsonPath.read(response, "$.data.name");
        Assert.assertNotNull(itemName, "Item Name is Null");

    }

    @Test(dependsOnMethods = "createItem",description = "Verify item in the rabbitMQ")
    public void rmqItemNameChange() {
        if (itemName != null) {
            String str = helper.getMessage("oms", "item.nam.change.queue");
            System.out.println(str);
            String newNameRmq = JsonPath.read(str, "$.new_item.name");
            itmId = JsonPath.read(str, "$.new_item.id");
            Assert.assertEquals(itemName, newNameRmq);

        }
    }
}
