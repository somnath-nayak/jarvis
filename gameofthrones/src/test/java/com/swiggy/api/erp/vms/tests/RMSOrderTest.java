package com.swiggy.api.erp.vms.tests;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.delivery.helper.DeliveryDataHelper;
import com.swiggy.api.erp.ff.helper.*;
import com.swiggy.api.erp.vms.dp.RMSOrderDp;
import com.swiggy.api.erp.vms.helper.RMSCommonHelper;
import com.swiggy.api.erp.vms.helper.RMSHelper;
import com.swiggy.automation.common.utils.APIUtils;
import framework.gameofthrones.Cersei.ToolBox;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RMSOrderTest extends RMSOrderDp {

	RMSHelper helper = new RMSHelper();
	SchemaValidatorUtils schemaValidatorUtils = new SchemaValidatorUtils();


	@Test(dataProvider = "fetchOrder", groups = { "sanity",
			"smoke" }, description = "POST orders/v1/fetchOrders`/ :fetch Order for given restaurant Id")
	public void fetchOrder(String accessToken, String restaurantId, String statusMessage,
						   int statusCode, String order_id) throws Exception {

		SoftAssert softAssert = new SoftAssert();
		Thread.sleep(5000);
		Processor processor = helper.fetchOrder(restaurantId, accessToken);
		String resp = processor.ResponseValidator.GetBodyAsText();
//		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/fetchOrder");
//		List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
//		Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For POP Aggreagtor API");
//		boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
//		System.out.println("Contain empty nodes => " + isEmpty);

		JsonNode nodeObject = APIUtils.convertStringtoJSON(resp);
		JsonNode ndeObj = APIUtils.convertStringtoJSON(nodeObject.get("restaurantData").toString());
		JsonNode orders = ndeObj.get(0).get("orders");
		System.out.println("len is" + orders.size());
		for (int i = 0; i < orders.size(); i++) {
			System.out.println(orders.get(i).get("order_id"));
			String or_id = orders.get(i).get("order_id").asText();
			if (or_id.equals(order_id)) {
				System.out.println("VALIDATING API RESP PROPERTIES");
                softAssert.assertEquals(orders.get(i).get("status").get("order_status").asText(), "ordered", "order_status doesn't match");
                softAssert.assertEquals(orders.get(i).get("status").get("placed_status").asText(), "unplaced", "placed_status doesn't match");
                softAssert.assertEquals(orders.get(i).get("status").get("placingState").asText(), "WITH_RELAYER", "placingState doesn't match");
                softAssert.assertEquals(orders.get(i).get("status").get("delivery_status").asText(), "unassigned", "delivery_status doesn't match");
                softAssert.assertEquals(orders.get(i).get("status").get("edited_status").asText(), "unedited", "edited_status doesn't match");
                softAssert.assertNotNull(orders.get(i).get("status").get("ordered_time").asText(), "ordered_time doesn't match");
                softAssert.assertEquals(orders.get(i).get("status").get("placed_time").asText(), "null", "placed_time doesn't match");
                softAssert.assertEquals(orders.get(i).get("status").get("call_partner_time").asText(),"null", "call_partner_time doesn't match");
                softAssert.assertEquals(orders.get(i).get("status").get("edited_time").asText(),"null", "edited_time doesn't match");
                softAssert.assertEquals(orders.get(i).get("status").get("placed_time").asText(),"null", "placed_time doesn't match");
                softAssert.assertEquals(orders.get(i).get("status").get("food_prep_time").asText(),"null", "food_prep_time doesn't match");
                softAssert.assertEquals(orders.get(i).get("status").get("cancelled_time").asText(),"null", "cancelled_time doesn't match");
                softAssert.assertEquals(orders.get(i).get("current_order_action").asText(), "nothing", "current_order_action doesn't match");
                softAssert.assertEquals(orders.get(i).get("customer_comment").asText(),"", "customer_comment doesn't match");
                softAssert.assertEquals(orders.get(i).get("customer_area").asText(), "Test", "customer_area doesn't match");
                softAssert.assertEquals(orders.get(i).get("customer_distance").asText(), "1.777", "customer_distance doesn't match");
                softAssert.assertEquals(orders.get(i).get("restaurant_taxation_type").asText(), "GST", "restaurant_taxation_type doesn't match");
                softAssert.assertEquals(orders.get(i).get("gst").asText(), "23", "gst doesn't match");
                softAssert.assertEquals(orders.get(i).get("serviceCharge").asText(), "0", "serviceCharge doesn't match");
                softAssert.assertEquals(orders.get(i).get("spending").asText(), "0", "spending doesn't match");
                softAssert.assertEquals(orders.get(i).get("tax").asText(), "0", "tax doesn't match");
                softAssert.assertEquals(orders.get(i).get("discount").asText(), "140", "discount doesn't match");
                softAssert.assertEquals(orders.get(i).get("bill").asText(), "278", "bill doesn't match");
                softAssert.assertEquals(orders.get(i).get("restaurant_trade_discount").asText(), "140", "restaurant_trade_discount doesn't match");
                softAssert.assertEquals(orders.get(i).get("type").asText(), "regular", "type doesn't match");
                softAssert.assertEquals(orders.get(i).get("GST_details").get("cartCGST").asText(), "11.5", "cartCGST doesn't match");
                softAssert.assertEquals(orders.get(i).get("GST_details").get("cartIGST").asText(), "11", "cartIGST doesn't match");
                softAssert.assertEquals(orders.get(i).get("GST_details").get("cartSGST").asText(), "0.5", "cartSGST doesn't match");
                softAssert.assertEquals(orders.get(i).get("GST_details").get("itemCGST").asText(), "11", "itemCGST doesn't match");
                softAssert.assertEquals(orders.get(i).get("GST_details").get("itemIGST").asText(), "11", "itemIGST doesn't match");
                softAssert.assertEquals(orders.get(i).get("GST_details").get("packagingCGST").asText(), "0.5", "packagingCGST doesn't match");
                softAssert.assertEquals(orders.get(i).get("GST_details").get("packagingSGST").asText(), "0.5", "packagingSGST doesn't match");
                softAssert.assertEquals(orders.get(i).get("cart").get("charges").get("packing_charge").asText(), "5", "packing_charge doesn't match");
                softAssert.assertNotNull(orders.get(i).get("cart").get("items").get(0).get("item_id").asText(), "item_id doesn't match");
                softAssert.assertEquals(orders.get(i).get("cart").get("items").get(0).get("quantity").asText(), "1", "quantity doesn't match");
                softAssert.assertEquals(orders.get(i).get("cart").get("items").get(0).get("name").asText(), "Chicken Burger", "name doesn't match");
                softAssert.assertEquals(orders.get(i).get("cart").get("items").get(0).get("restaurant_discount_hit").asText(), "0", "restaurant_discount_hit doesn't match");
                softAssert.assertEquals(orders.get(i).get("cart").get("items").get(0).get("sub_total").asText(), "110", "sub_total doesn't match");
                softAssert.assertEquals(orders.get(i).get("cart").get("items").get(0).get("total").asText(), "115", "total doesn't match");
                softAssert.assertEquals(orders.get(i).get("cart").get("items").get(0).get("category").asText(), "Quick Bites", "category doesn't match");
                softAssert.assertEquals(orders.get(i).get("cart").get("items").get(0).get("sub_category").asText(), "nota", "sub_category doesn't match");
                softAssert.assertEquals(orders.get(i).get("cart").get("items").get(0).get("is_oos").asText(), "false", "is_oos doesn't match");
                softAssert.assertEquals(orders.get(i).get("cart").get("items").get(0).get("is_veg").asText(), "0", "is_veg doesn't match");
                softAssert.assertEquals(orders.get(i).get("cart").get("items").get(0).get("charges").get("Service Charges").asText(), "0", "Service Charges doesn't match");
                softAssert.assertEquals(orders.get(i).get("cart").get("items").get(0).get("charges").get("GST").asText(), "0", "GST doesn't match");
                softAssert.assertEquals(orders.get(i).get("cart").get("items").get(0).get("charges").get("Vat").asText(), "0", "Vat doesn't match");
                softAssert.assertEquals(orders.get(i).get("cart").get("items").get(0).get("charges").get("Service Tax").asText(), "0", "Service Tax doesn't match");
                softAssert.assertEquals(orders.get(i).get("cart").get("items").get(0).get("tax_expressions").get("GST_inclusive").asText(), "false", "GST_inclusive doesn't match");
                softAssert.assertEquals(orders.get(i).get("cart").get("items").get(0).get("tax_expressions").get("Service Charges").asText(), "0", "Service Charges doesn't match");
                softAssert.assertEquals(orders.get(i).get("cart").get("items").get(0).get("tax_expressions").get("Vat").asText(), "0", "Vat doesn't match");
                softAssert.assertEquals(orders.get(i).get("cart").get("items").get(0).get("tax_expressions").get("Service Tax").asText(), "0", "Service Tax doesn't match");
			}
		}

		softAssert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage, "statusMessage doesn't match");
		softAssert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode, "statusCode doesn't match");
		softAssert.assertEquals(
				processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.restaurantData..restaurantId"),
				"[" + restaurantId + "]", "restaurantId doesn't match");
		softAssert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.restaurantData..isOpen"), "isOpen doesn't match");
		softAssert.assertNotNull(
				processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.restaurantData..orders..order_id"), "order_id doesn't match");
		softAssert.assertNotNull(processor.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.restaurantData..orders..restaurant_details"), "restaurant_details doesn't match");
		softAssert.assertNotNull(processor.ResponseValidator.DoesNodeExists("$.restaurantData..batches"), "batches doesn't match");
		softAssert.assertNotNull(processor.ResponseValidator.DoesNodeExists("$.restaurantData..lastOrderEventTimestamps"), "lastOrderEventTimestamps doesn't match");
		softAssert.assertNotNull(processor.ResponseValidator.DoesNodeExists("$.restaurantData..updatedOrderIds"), "updatedOrderIds doesn't match");
		softAssert.assertNotNull(processor.ResponseValidator.DoesNodeExists("$.restaurantData..ordersToBeDeleted"), "ordersToBeDeleted doesn't match");

		System.out.println("AT THE END");
		softAssert.assertAll();

	}

	@Test(dataProvider = "fetchOrder_regression", groups = {
			"regression" }, description = "POST orders/v1/fetchOrders/ :Regression testcases to fetch Order for given restaurant Id")
	public void fetchOrder_regression(String accessToken, String restaurantId, String statusMessage, int statusCode) {

		Processor processor = helper.fetchOrder(restaurantId, accessToken);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);

	}

	@Test(dataProvider = "getOldOrderHistory", groups = { "sanity",
			"smoke" }, description = "GET orders/v1/oldHistory :fetch History of Old Order for given retaurant Id")
	public void getOldOrderHistory(String accessToken, String restaurantId, int statusCode, String statusMessage) {

		Processor processor = helper.getOldOrderdHistory(accessToken, restaurantId);
		String resp = processor.ResponseValidator.GetBodyAsText();
		// String jsonschema = new
		// ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/variantInStock.txt");;
		// List<String>
		// missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
		// Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are
		// Missing Or Not Matching For POP Aggreagtor API");
		// boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
		// System.out.println("Contain empty nodes => " + isEmpty);
		// Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);

	}

	@Test(dataProvider = "getOldOrderdHistory_regression", groups = {
			"regression" }, description = "GET orders/v1/oldHistory :Regression Test cases to fetch History of Old Order for given retaurant Id")
	public void getOldOrderdHistory_regression(String accessToken, String restaurantId, int statusCode,
			String statusMessage) {

		Processor processor = helper.getOldOrderdHistory(accessToken, restaurantId);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);

	}

	@Test(dataProvider = "getOrderedHistory", groups = { "sanity",
			"smoke" }, description = "GET orders/v1/history :fetch History of Order for given retaurant Id")
	public void getOrderedHistory(String accessToken, String restaurantId, String limit, String offset,
			String ordered_time__gte, String ordered_time__lte, String filterReq, int statusCode,
			String statusMessage) throws IOException, ProcessingException {

		Processor processor = helper.getOrderdHistory(accessToken, restaurantId, limit, offset, ordered_time__gte,
				ordered_time__lte, filterReq);
		String resp = processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/getOrderedHistory");
		List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
		Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For POP Aggreagtor API");
		boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
		String s = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..restId").replace("\"", " ")
				.replace('[', ' ').replace(']', ' ').trim();
		System.out.println(s);
		Assert.assertEquals(s, restaurantId);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..data..limit")
				.replace('[', ' ').replace(']', ' ').trim(), limit);

	}

	@Test(dataProvider = "getOrderedHistory_regression", groups = {
			"Regression" }, description = "GET orders/v1/history :Regression test cases to fetch History of Order for given retaurant Id")
	public void getOrderedHistory_regression(String accesToken, String restaurantId, String limit, String offset,
			String ordered_time__gte, String ordered_time__lte, String filterReq, int statusCode,
			String statusMessage) {

		Processor processor = helper.getOrderdHistory(accesToken, restaurantId, limit, offset, ordered_time__gte,
				ordered_time__lte, filterReq);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);

	}

	@Test(dataProvider = "getOrderHistoryCSV", groups = { "sanity",
			"smoke" }, description = "GET orders/v1/historyCsv :To generate Order History in CSV Format")
	public void getOrderHistoryCSV(String accessToken, String restaurantId, String isPoll, String orderedTimeGte,
			String orderedTimeLte, String requestedTimeStamp, int statusCode, String statusMessage) {

		Processor processor = helper.getOrderHistoryCSV(accessToken, restaurantId, isPoll, orderedTimeGte,
				orderedTimeLte, requestedTimeStamp);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
	}

	@Test(dataProvider = "getOrderHistoryCSV_regression", groups = {
			"Regression" }, description = "GET orders/v1/historyCsv :Regression test cases to generate Order History in CSV Format")
	public void getOrderHistoryCSV_regression(String accessToken, String restaurantId, String isPoll,
			String orderedTimeGte, String orderedTimeLte, String requestedTimeStamp, int statusCode,
			String statusMessage) {

		Processor processor = helper.getOrderHistoryCSV(accessToken, restaurantId, isPoll, orderedTimeGte,
				orderedTimeLte, requestedTimeStamp);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);

	}

	@Test(dataProvider = "getorderHistoryMultiChain", groups = { "sanity",
			"smoke" }, description = "GET orders/v0/history :fetch History of Order for given retaurant Id")
	public void getorderHistoryMultiChain(String accessToken, ArrayList<String> restaurantIds, int statusCode,
			String statusMessage) {

		Processor processor = helper.getorderHistoryMultiChain(accessToken, restaurantIds);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..restId"));
		Assert.assertNotNull(
				processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..data..statusMessage"));
	}

	@Test(dataProvider = "getorderHistoryMultiChain_regression", groups = {
			"Regression" }, description = "GET orders/v0/history :Regression test cases to fetch History of Order for given retaurant Id")
	public void getorderHistoryMultiChain_regression(String accessToken, ArrayList<String> restaurantIds,
			int statusCode, String statusMessage) {

		Processor processor = helper.getorderHistoryMultiChain(accessToken, restaurantIds);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);

	}

	@Test(dataProvider = "callback", groups = { "sanity", "smoke" }, description = "POST orders/v1/call :Request Callback for restaurant id")
	public void callback(String accessToken, String restaurantId, String orderId, int statusCode, String statusMessage)
			throws Exception {

	    Thread.sleep(5000);
		OMSHelper omsHellper = new OMSHelper();
		String orderIdInfo = omsHellper.getOrderInfoFromOMS(orderId);
		Processor processor = helper.callback(accessToken, restaurantId, orderId);
		String resp = processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/variantInStock");
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		Assert.assertTrue(missingNodeList.isEmpty(),
				missingNodeList + " Nodes Are Missing Or Not Matching For POP Aggreagtor API");
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);

        Processor p = helper.fetchOrder(restaurantId, accessToken);
        String resp1 = processor.ResponseValidator.GetBodyAsText();
//		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/fetchOrder");
//		List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
//		Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching For POP Aggreagtor API");
//		boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
//		System.out.println("Contain empty nodes => " + isEmpty);

        JsonNode nodeObject = APIUtils.convertStringtoJSON(resp1);
        JsonNode ndeObj = APIUtils.convertStringtoJSON(nodeObject.get("restaurantData").toString());
        JsonNode orders = ndeObj.get(0).get("orders");
        System.out.println("len is" + orders.size());
        for (int i = 0; i < orders.size(); i++) {
            System.out.println(orders.get(i).get("order_id"));
            String or_id = orders.get(i).get("order_id").asText();
            if (or_id.equals(orderId)) {
                Assert.assertEquals(orders.get(i).get("status").get("placed_status").asText(), "call_partner", "placed_status doesn't match");
            }
        }

        }

	@Test(dataProvider = "callback_regression", groups = {
			"regression" }, description = "POST orders/v1/call :Regression test cases to make Request Callback for restaurant id")
	public void callback_regression(String accessToken, String restaurantId, String orderId, int statusCode,
			String statusMessage) {

		Processor processor = helper.callback(accessToken, restaurantId, orderId);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);

	}

	//API is not used currently
	@Test(dataProvider = "dispatch", groups = { "sanity", "smoke" },
			description = "POST orders/v1/dispatched :Update order status to - Dispatch", enabled = false)
	public void dispatch(String accessToken, String restaurantId, String orderId, int statusCode,
			String statusMessage) {

		Processor processor = helper.dispatch(accessToken, restaurantId, orderId);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);

	}

	@Test(dataProvider = "dscallout_Dp", groups = { "sanity", "smoke" }, description = "POST v1/foodIssue/dsCallouts :DS callout", enabled = false)
	public void dscallout(String restaurantId, String orderId, String DS_statusMessage) throws Exception{

	    Thread.sleep(5000);
		Processor processor = helper.dscallout(restaurantId, orderId);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), DS_statusMessage);

	}

	@Test(dataProvider = "dscallout_Dp1", groups = { "regression" }, description = "POST v1/foodIssue/dsCallouts :DS callout regression use cases", enabled = false)
	public void dscallout1(String restaurantId, String orderId, int expectedStatusCode, String expectedStatusMessagee) {

		Processor processor = helper.dscallout(restaurantId, orderId);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), expectedStatusCode);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), expectedStatusMessagee);

	}

	@Test(dataProvider = "confirmOrder", groups = { "sanity",
			"smoke" }, description = "POST orders/v1/confirm :Confirm Order")
	public void confirmOrder(String accessToken, String restaurantId, String order_id, String statusMessage,
			int statusCode) throws Exception {

		Processor processor = helper.fetchOrder(restaurantId, accessToken);
		String resp = processor.ResponseValidator.GetBodyAsText();
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
		JsonNode nodeObject = APIUtils.convertStringtoJSON(resp);
		JsonNode ndeObj = APIUtils.convertStringtoJSON(nodeObject.get("restaurantData").toString());
		JsonNode orders = ndeObj.get(0).get("orders");
		System.out.println("len is" + orders.size());
		for (int i = 0; i < orders.size(); i++) {
			System.out.println(orders.get(i).get("order_id"));
			String or_id = orders.get(i).get("order_id").asText();
			if (or_id.equals(order_id)) {
				String s1 = orders.get(i).get("status").get("placed_status").asText();
				// Test case fail : status should be with_partner but we are getting unplaced
				// status since some events are not triggered
				Assert.assertEquals(s1, "with_partner");
				Thread.sleep(5000);
				Processor response = helper.confirm(accessToken, order_id, restaurantId);
				Thread.sleep(5000);
				System.out.println("checked the status");
				String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/confirm_order");
				List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema,
						response.ResponseValidator.GetBodyAsText());
				Assert.assertTrue(missingNodeList.isEmpty(),
						missingNodeList + " Nodes Are Missing Or Not Matching For LoginApi API");
				// Test case fail : status should be placed after confirm action but we are
				// getting unplaced status since some events are not triggered
				String s2 = orders.get(i).get("status").get("placed_status").asText();
				Assert.assertEquals(s2, "placed");
			}
		}
	}

	@Test(dataProvider = "confirmOrderWithPrepTime", groups = { "sanity",
			"smoke" }, description = "POST orders/v1/confirm :Confirm Order")
	public void confirmOrderWithPrepTime(String accessToken, String restaurantId, String order_id, String prepTime, String statusMessage,
							 int statusCode) throws Exception {

				Thread.sleep(10000);
				Processor response = helper.confirm(accessToken, order_id, restaurantId, prepTime);
				Thread.sleep(5000);
				Assert.assertEquals(response.ResponseValidator.GetNodeValue("$.statusMessage.statusMessage"), statusMessage);
				Assert.assertEquals(response.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);

				Processor processor = helper.fetchOrder(restaurantId, accessToken);
				String resp = processor.ResponseValidator.GetBodyAsText();
				JsonNode nodeObject = APIUtils.convertStringtoJSON(resp);
				JsonNode ndeObj = APIUtils.convertStringtoJSON(nodeObject.get("restaurantData").toString());
				for(int i=0; i<ndeObj.size();i++) {
					if(ndeObj.get(i).get("restaurantId").toString().equalsIgnoreCase(restaurantId)) {
						JsonNode orders = ndeObj.get(i).get("orders");
						for(int j=0; j<orders.size(); j++) {
							if(orders.get(j).get("order_id").toString().equalsIgnoreCase(order_id)) {
								String orderStatus = orders.get(j).get("status").get("placed_status").toString();
								String pTime = orders.get(j).get("status").get("food_prep_time").toString();
								Assert.assertEquals(orderStatus, "placed");
								Assert.assertEquals(pTime, prepTime);
							}
						}
					}
				}
	}

	@Test(dataProvider = "confirmOrder", groups = { "sanity",
			"smoke" }, description = "POST orders/v1/item/oos/ : mark order item OOS")
	public void markOrderItemOOS(String accessToken, String restaurantId, String order_id, String statusMessage,
			int statusCode) throws IOException, ProcessingException, InterruptedException {

		Thread.sleep(1000);
		Processor P = helper.confirm(accessToken, order_id, restaurantId);
		Thread.sleep(10000);
		Processor processor = helper.fetchOrder(restaurantId, accessToken);
		String resp = processor.ResponseValidator.GetBodyAsText();
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		String s1 = JsonPath
				.read(processor.ResponseValidator.GetBodyAsText(),
						"$.restaurantData..[?(@.order_id=='" + order_id + "')].status.placed_status")
				.toString().replace("[\"", "").replace("\"]", "").trim();
		System.out.println("s1 is" + s1);
		Assert.assertEquals(s1, "placed", "Order Status is Not Matching");
		Processor P2 = helper.callback(accessToken, restaurantId, order_id);
		Processor P3 = helper.markItemOOS(order_id, "5748192", "5748200", "", restaurantId, accessToken);
		resp = P3.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/markitem_oos");
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(jsonschema, resp);
		Assert.assertTrue(missingNodeList.isEmpty(),
				missingNodeList + " Nodes Are Missing Or Not Matching For MarkOutofStock API");
		Assert.assertEquals(P3.ResponseValidator.GetNodeValue("statusMessage"), "Done Successfully",
				"Status Message Didn't Match");
		Assert.assertEquals(P3.ResponseValidator.GetNodeValueAsInt("statusCode"), 0, "Status Code Didn't Match");
	}


	@Test(dataProvider = "foodPrepared", groups = {
			"smoke" , "sanity" }, description = "POST orders/v1/foodPrepared : Mark foodPreparedTime")
	public void foodPrepared(String accessToken, String orderId, String restaurantId, String statusMessage) throws Exception {

	    Thread.sleep(10000);
		Processor processor = helper.markReady(accessToken, orderId, restaurantId);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValue("$.data.foodPreparedTime"));

	}

	@Test(dataProvider = "foodPreparedRegression", groups = {
			"regression"}, description = "POST orders/v1/foodPrepared : Mark foodPreparedTime")
	public void foodPreparedRegression(String accessToken, String orderId, String restaurantId, String statusMessage) {

		Processor processor = helper.markReady(accessToken, orderId, restaurantId);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);

	}


	@Test(dataProvider = "trackOrder", groups = { "sanity", "smoke" }, description = "POST orders/v1/trackOrders :track Order ")
	public void trackOrder(String accessToken, String restaurantId, String order_id,
						   String statusMessage) throws Exception
	{
	    Thread.sleep(5000);
		/* TrackAllOrders will first update timeToReach in redis which will be returned by track Order API */

		Processor p = helper.trackAllOrders(order_id);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(p));
		Assert.assertEquals(p.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);

		Processor processor = helper.trackOrder(accessToken, restaurantId, order_id);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("$.data.[0].timeToReachRestaurant"), "10");
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("$.data.[0].orderId"), order_id);
	}

	@Test(dataProvider = "trackOrderRegression", groups = { "regression"}, description = "POST orders/v1/trackOrders :track Order ")
	public void trackOrderRegression(String accessToken, String restaurantId, String order_id,
						   String statusMessage) throws Exception
	{
		Processor processor = helper.trackOrder(accessToken, restaurantId, order_id);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
	}

	@Test(dataProvider = "trackAllOrders", groups = {
			"smoke" , "sanity" }, description = "PUT /orders/v1/trackOrders : Updates the de timeToReach in redis cache")
	public void trackAllOrders(String orderId, String statusMessage) throws Exception {

	    Thread.sleep(5000);
		Processor processor = helper.trackAllOrders(orderId);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
	}

	@Test(dataProvider = "trackOrder", groups = { "sanity", "smoke" }, description = "GET orders/v1/locate/ :locate Order ")
	public void locateOrder(String accessToken, String restaurantId, String order_id,
						   String statusMessage) throws Exception
	{
	    Thread.sleep(5000);
		/* This API just calls Delivery tracking API and returns the location which is
		not returned in UAT, so just verifying rms wrapper API code and status*/
		Processor processor = helper.locateOrder(accessToken, restaurantId, order_id);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
	}

	@Test(dataProvider = "locateOrderRegression", groups = { "regression"}, description = "GET orders/v1/locate/ :locate Order ")
	public void locateOrderRegression(String accessToken, String restaurantId, String order_id,
							String statusMessage) throws Exception
	{
		Processor processor = helper.locateOrder(accessToken, restaurantId, order_id);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
	}

	@Test(dataProvider = "getDispositions", groups = {
			"smoke" , "sanity" }, description = "GET order/v1/dispositions")
	public void getDispositions(String accessToken, String cities, String restaurantId,
									String statusMessage) throws IOException, ProcessingException{

		Processor processor = helper.getDispositions(accessToken, cities, restaurantId);
		String resp=processor.ResponseValidator.GetBodyAsText();
		String jsonschema = new ToolBox().readFileAsString("../Data/SchemaSet/Json/VMS/getDispositions");
		List<String> missingNodeList=schemaValidatorUtils.validateServiceSchema(jsonschema,resp);
		Assert.assertTrue(missingNodeList.isEmpty(),missingNodeList + " Nodes Are Missing Or Not Matching  API");
		boolean isEmpty=schemaValidatorUtils.hasEmptyNodes(resp);
		System.out.println("Contain empty nodes => " + isEmpty);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("$.data.dialogText"),
				"Requesting for a callback will cause a 7-8 mins delay in the delivery of the order.");
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("$.data.oosTitleText"), "Try clicking out of stock");
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("$.data.dispositions.[0].name"), "Item is out of stock");
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("$.data.dispositions.[1].name"), "Where is Delivery Executive");
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("$.data.dispositions.[2].name"), "Price(s) is incorrect");
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("$.data.dispositions.[3].name"), "Special instructions confirmation");
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("$.data.dispositions.[4].name"), "Delay in order preparation");
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("$.data.dispositions.[5].name"), "Add-on(s)/Variant(s) is not specified");
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("$.data.dispositions.[6].name"), "Report Delivery Executive behaviour");
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("$.data.dispositions.[7].name"), "Some other reason");


	}

	@Test(dataProvider = "getDispositionsRegression", groups = {
			"regression" }, description = "GET order/v1/dispositions")
	public void getDispositionsRegression(String accessToken, String cities, String restaurantId, int statusCode,
										  String statusMessage) throws IOException, ProcessingException{

		Processor processor = helper.getDispositions(accessToken, cities, restaurantId);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode);
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
	}

	@Test(dataProvider = "getActiveOrders", groups = {
			"smoke" , "sanity" }, description = "GET /orders/v1/activeOrders : get order list for which DE is assigned but not picked up")
	public void getActiveOrders(String orderId, String statusMessage) throws Exception {

	    Thread.sleep(5000);
		boolean assignStatus = RMSCommonHelper.assignDE(orderId);
		Assert.assertTrue(assignStatus, "DE Assignment failed");
		DeliveryDataHelper deliveryDataHelper = new DeliveryDataHelper();
		boolean deConfirm = deliveryDataHelper.changeStatefromController("confirmed", orderId);
		Assert.assertTrue(deConfirm, "DE Confirmation failed");
		boolean isOrder = false;
		Processor processor = helper.getActiveOrders();
		ArrayList al = processor.ResponseValidator.GetNodeValueAsJsonArray("$.data");
		System.out.println(al);
		for (int i = 0; i < al.size(); i++) {
			if (al.get(i).toString().equals(orderId)) {
				isOrder = true;
				break;
			}
		}
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertTrue(isOrder , "Order is present in active list");
	}

	@Test(dataProvider = "withPartner", groups = {
			"smoke" , "sanity" }, description = "PUT /order/v1/with-partner :update with partner time stamp")
	public void withPartner(String orderId, String restaurantId,
										  String statusMessage) throws Exception {

	    Thread.sleep(5000);
		Processor processor = helper.withPartner(orderId, restaurantId);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertNotNull(helper.getWithPartnerTime(orderId));
	}

	@Test(dataProvider = "withPartnerRegression", groups = {
			"regression" }, description = "PUT /order/v1/with-partner :update with partner time stamp")
	public void withPartnerRegression(String orderId, String restaurantId,
							String statusMessage) throws IOException, ProcessingException{

		Processor processor = helper.withPartner(orderId, restaurantId);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
	}

	@Test(dataProvider = "firstAcceptance", groups = {
			"sanity" , "smoke"}, description = "PUT /order/v1/first-acceptance :update partner first-acceptance event and timestamp")
	public void firstAcceptance(String orderId, String restaurantId, String event,
							String statusMessage) throws Exception {

	    Thread.sleep(5000);
		Processor processor = helper.firstAcceptance(orderId, restaurantId, event);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		List<Map<String, Object>> list = helper.firstAcceptanceEvent(orderId);
		Assert.assertNotNull(list.get(0).get("first_acceptance_timestamp"));
		Assert.assertEquals(list.get(0).get("first_acceptance_event").toString(), event);
	}

	@Test(dataProvider = "firstAcceptanceRegression", groups = {
			"regression" }, description = "PUT /order/v1/first-acceptance :update partner first-acceptance event and timestamp")
	public void firstAcceptanceRegression(String orderId, String restaurantId, String event,
								String statusMessage) throws IOException, ProcessingException{

		Processor processor = helper.firstAcceptance(orderId, restaurantId, event);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);

	}

	@Test(dataProvider = "markFoodPrepared", groups = {
			"sanity" , "smoke"}, description = "PUT /order/v1/mark-food-prepared :update partner mark-food-prepared timestamp")
	public void markFoodPrepared(String orderId, String restaurantId,
								String statusMessage) throws Exception {

	    Thread.sleep(5000);
		Processor processor = helper.markFoodPrepared(orderId, restaurantId);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);
		Assert.assertNotNull(processor.ResponseValidator.GetNodeValue("$.data.foodPreparedTime"));

	}

	@Test(dataProvider = "markFoodPreparedRegression", groups = {
			"regression"}, description = "PUT /order/v1/mark-food-prepared :update partner mark-food-prepared timestamp")
	public void markFoodPreparedRegression(String orderId, String restaurantId,
								 String statusMessage) throws IOException, ProcessingException{

		Processor processor = helper.markFoodPrepared(orderId, restaurantId);
		Assert.assertTrue(helper.aggregateResponseAndStatusCodes(processor));
		Assert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage);

	}

	@Test(dataProvider = "orderCAPRD", groups = { "sanity",
			"smoke" }, description = " Verify CAPRD states")
	public void orderCAPRD(String accessToken, String restaurantId, String order_id) throws Exception {

		DeliveryDataHelper deliveryDataHelper =new DeliveryDataHelper();
		boolean deConfirm = false;
		boolean deArrived = false;
		boolean dePickedUp = false;
		boolean deDelivered = false;
		boolean assignDeStatus = false;
		try {
			assignDeStatus = RMSCommonHelper.assignDE(order_id);
		} catch (Exception e)
			{ assignDeStatus = RMSCommonHelper.assignDE(order_id, de_id);}
		if (assignDeStatus) {
			Processor processor = helper.fetchOrder(restaurantId, accessToken);
			String deStatus = helper.deStatus(processor, order_id);
			Assert.assertEquals(deStatus, "assigned", "Delivery Status is Not Matching");
			deConfirm = deliveryDataHelper.changeStatefromController("confirmed", order_id);
		}

		if(deConfirm) {
			Processor processor = helper.fetchOrder(restaurantId, accessToken);
			String deStatus = helper.deStatus(processor, order_id);
			Assert.assertEquals(deStatus, "confirmed", "Delivery Status is Not Matching");
			deArrived = deliveryDataHelper.changeStatefromController("arrived", order_id);
		}

		if(deArrived) {
			Processor processor = helper.fetchOrder(restaurantId, accessToken);
			String deStatus = helper.deStatus(processor, order_id);
			Assert.assertEquals(deStatus, "arrived", "Delivery Status is Not Matching");
			dePickedUp = deliveryDataHelper.changeStatefromController("pickedup", order_id);

		}

		/*In RMS we don't track reached status, so moving it to delivered after picked up*/
		if(dePickedUp) {
			Processor processor = helper.fetchOrder(restaurantId, accessToken);
			String deStatus = helper.deStatus(processor, order_id);
			Assert.assertEquals(deStatus, "pickedup", "Delivery Status is Not Matching");
			deDelivered = deliveryDataHelper.changeStatefromController("delivered", order_id);
		}

		if(deDelivered) {
			Processor processor = helper.fetchOrder(restaurantId, accessToken);
			String deStatus = helper.deStatus(processor, order_id);
			Assert.assertEquals(deStatus, "delivered", "Delivery Status is Not Matching");
		}
	}

	@Test
	public void testOrder() throws Exception
	{
		/*UAT03*/
	    //RMSCommonHelper.createOrder("murthy@swiggy.in", "Admin@123");

        /*UAT 02*/
        //RMSCommonHelper.createOrder("ramzi@swiggy.in", "Test@1234");

        /*UAT 01*/
        RMSCommonHelper.createOrder("ramzi@swiggy.in", "Test@12345");

//		LOSHelper losHelper = new LOSHelper();
//		//losHelper.createOrderWithPartner("1232222228888", "2018-10-11 11:40:36", "1539238236");
//		//losHelper.createPOPOrder("123222221238", "2018-10-16 13:40:36", "1539677426");
//		losHelper.createSwiggyCafeOrder("123222221266", "2018-10-16 13:40:36", "1539677426", "cafe");


	}

}