package com.swiggy.api.erp.cms.helper;

import com.swiggy.api.erp.cms.constants.CmsConstants;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.SqlTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EDVOCronHelper {

    Initialize gameofthrones = new Initialize();

    public List<Map<String, Object>> getDominosRestaurant(int typeOfpartner) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        return sqlTemplate.queryForList(CmsConstants.dominosRestaurant + typeOfpartner);
    }

    public List<Map<String, Object>> getEDVOMealIds(String restId) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        return sqlTemplate.queryForList(CmsConstants.mealIdsbyRestId + restId);
    }

    public String getEDVOImage(String mealID) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        return String.valueOf(sqlTemplate.queryForList(CmsConstants.EDVOImage + mealID).get(0).get("image_id"));
    }


    public Processor updateMeals(String CronExpression, String restIds, String message, String headersAuthorization){
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        requestHeader.put("Authorization", headersAuthorization);
        GameOfThronesService gots = new GameOfThronesService("cmsEdvoMealCron", "updateMeals", gameofthrones);
        Processor processor = new Processor(gots, requestHeader,  new String[]{CronExpression, restIds,message});
        return processor;
    }

    public Processor createMeals(String CronExpression, String restIds, String headersAuthorization){
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        requestHeader.put("Authorization", headersAuthorization);
        GameOfThronesService gots = new GameOfThronesService("cmsEdvoMealCron", "createMeals", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, new String[]{CronExpression, restIds});
        return processor;
    }

    public String getLaunchMainText(String mealId) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        return String.valueOf(sqlTemplate.queryForList(CmsConstants.launchMainText + mealId).get(0).get("main_text"));
    }

    public String getLaunchSubText( String mealId){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        return String.valueOf(sqlTemplate.queryForList(CmsConstants.launchSubText + mealId).get(0).get("sub_text"));
    }

    public String getExitMainText(String mealId){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        return String.valueOf(sqlTemplate.queryForList(CmsConstants.exitMainText + mealId).get(0).get("main_text"));
    }

    public String getExitSubText(String mealId){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        return String.valueOf(sqlTemplate.queryForList(CmsConstants.exitSubText + mealId).get(0).get("sub_text"));
    }

    public String getScreenTitle(String groupId){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        return String.valueOf(sqlTemplate.queryForList(CmsConstants.screenTitle + groupId).get(0).get("screen_title"));
    }

    public String getScreenDescription(String groupId){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        return String.valueOf(sqlTemplate.queryForList(CmsConstants.screenDescription + groupId).get(0).get("screen_description"));
    }

    public void setLaunchMainText(String mainText, String mealId){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.updatelaunchMainText+"\""+mainText+"\""+CmsConstants.updateEDVOMealId+mealId);
    }

    public void setLaunchSubText(String subText, String mealId){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.updatelaunchSubText+"\""+subText+"\""+CmsConstants.updateEDVOMealId+mealId);
    }

    public void setExitMainText(String mainText, String mealId){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.updateExitMainText+"\""+mainText+"\""+CmsConstants.updateEDVOMealId+mealId);
    }

    public void setExitSubText(String subText, String mealId){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.updateExitSubText+"\""+subText+"\""+CmsConstants.updateEDVOMealId+mealId);
    }

    public void setScreenTitle(String title, String groupId){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.updateScreenTitle+"\""+title+"\""+CmsConstants.updateEDVOGroup+groupId);
    }

    public void setScreenDescription(String description, String groupId){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.updateScreenDescription+"\""+description+"\""+CmsConstants.updateEDVOGroup+groupId);
    }

    public void setScreenNumber(String groupId, int screenNumber){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.updateScreenNumber+screenNumber+CmsConstants.updateEDVOGroup+groupId);
    }

    public void setMinTotal(String groupId, int count){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.updateMinChoice+count+CmsConstants.updateMinChoice1+count+CmsConstants.updateEDVOmeal+groupId);
    }

    public void setMaxTotal(String groupId, String count){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        System.out.println("query------------------------------------------------------"+CmsConstants.updateMaxChoice+count+CmsConstants.updateMaxChoice1+count+CmsConstants.updateEDVOmeal+groupId);
        sqlTemplate.update(CmsConstants.updateMaxChoice+count+CmsConstants.updateMaxChoice1+count+CmsConstants.updateEDVOmeal+groupId);
    }

    public void setMinChoice(String groupId, String minChoice){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.updateMinChoice+minChoice+CmsConstants.updateEDVOmeal+groupId);

    }

    public void setMaxChoice(String groupId, String maxChoice){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.updateMaxChoice+maxChoice+CmsConstants.updateEDVOmeal+groupId);
    }

    public void setMinTotalForGroup(String groupId, String minTotal){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.updateMinTotal+minTotal+CmsConstants.updateEDVOmeal+groupId);
    }

    public String getMinChoice(String groupId){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        return String.valueOf(sqlTemplate.queryForList(CmsConstants.getMinChoice+CmsConstants.updateEDVOmeal+ groupId).get(0).get("min_choices"));
    }

    public String getMaxChoice(String groupId){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        return String.valueOf(sqlTemplate.queryForList(CmsConstants.getMaxChoice+CmsConstants.updateEDVOmeal+ groupId).get(0).get("max_choices"));
    }

    public String getMinTotal(String groupId){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        return String.valueOf(sqlTemplate.queryForList(CmsConstants.getMinTotal+CmsConstants.updateEDVOmeal+ groupId).get(0).get("min_total"));
    }

    public String getMaxTotal(String groupId){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        return String.valueOf(sqlTemplate.queryForList(CmsConstants.getMaxTotal+CmsConstants.updateEDVOmeal+ groupId).get(0).get("max_total"));
    }

    public String getItemMaxQuantity(String groupId, String itemId){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        return String.valueOf(sqlTemplate.queryForList(CmsConstants.getItemMaxQuantity+CmsConstants.updateEDVOGroup+ groupId+CmsConstants.getItemMaxQuantity1+itemId).get(0).get("max_quantity"));
    }

    public void setItemMaxQuantity(String count, String groupId, String itemId){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.setItemMaxQuantity+count+CmsConstants.updateEDVOGroup+groupId+CmsConstants.setItemMaxQuantity1+itemId);
    }
}
