package com.swiggy.api.erp.vms.dp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Iterator;

import org.apache.commons.lang3.time.DateUtils;
import org.testng.annotations.DataProvider;

import com.jayway.jsonpath.JsonPath;
import com.sun.xml.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import com.swiggy.api.erp.cms.helper.RestaurantHelper;
import com.swiggy.api.erp.vms.constants.RMSConstants;
import com.swiggy.api.erp.vms.helper.RMSCommonHelper;
import com.swiggy.api.erp.vms.helper.RMSHelper;
import com.swiggy.api.erp.vms.helper.RMSTDHelper;
import com.swiggy.api.erp.vms.td.pojo.RMSCreateTDEntry;
import com.swiggy.api.erp.vms.td.pojo.RMSEditTDBuilder;
import com.swiggy.api.erp.vms.td.pojo.RMSTDBuilder;
import com.swiggy.api.erp.vms.td.pojo.RMSUpdateTDEntry;
import com.swiggy.api.erp.vms.td.pojo.Restaurant;
import com.swiggy.api.erp.vms.td.pojo.RuleDiscount;
import com.swiggy.api.erp.vms.td.pojo.Slots;
import com.swiggy.api.erp.vms.tests.RMSTDServicesTest;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.erp.vms.td.pojo.Category;
import com.swiggy.api.erp.vms.td.pojo.Menu;
import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.rng.pojo.RestaurantList;
import com.swiggy.api.sf.snd.helper.SnDHelper;

import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;

public class RMSTDServicesTestDP extends RMSCommonHelper {


//	private static final int Flat_Restaurants = 4993;
//	private static final int Percentage_restaurants = 9990;
//	private static final int Freebie_restaurants = 5271;
////	private static final int Percentage_restaurants = 4993;
////	private static final int Freebie_restaurants = 4993;
//	private static final int FreeDelivery_restaurants = 9990;
//	private static final int FreeDelivery_restaurants1 = 9990;
////	private static final int FreeDelivery_restaurants1 = 4993;
////	private static final int Freebie_restaurants = 9990;
//	private static final int nonAccessable_restaurants = 11621;
	
	static String accessToken=null;
	
	SnDHelper sndHelper=new SnDHelper();
	RMSCommonHelper commonhelper=new RMSCommonHelper();
	RMSHelper helper=new RMSHelper();
	CheckoutHelper checkouthelper=new CheckoutHelper();
	RestaurantHelper categoryhelper = new RestaurantHelper();
	CMSHelper cmshelper = new CMSHelper();
	static HashMap<String, String> m=new HashMap<>();
	static HashMap<String, String> m2=new HashMap<>();
	
	static String restaurantId = null;
	static String restaurantId1 = null;
	public static String VENDOR_PASSWORD = null;
	public static String VENDOR_USERNAME = null;
	public static String consumerAppPassword = null;
	public static long consumerAppMobile = 0l;
	public static Integer Percentage_restaurants = 9990;
	public static Integer Flat_Restaurants = 4993;
	public static Integer FreeDelivery_restaurants1 = 9990;
	public static Integer FreeDelivery_restaurants = 9990;
	public static Integer nonAccessable_restaurants = 9990;
	public static Integer Freebie_restaurants = 9990;
	public static String OMS_USERNAME=null;
	public static String OMS_PASSWORD=null;
	static String OrderId=null;
	
	static {
		try {
			if (getEnv().equalsIgnoreCase("stage1")) {
				VENDOR_PASSWORD = "El3ment@ry";
				VENDOR_USERNAME = "5271";
				consumerAppPassword = "swiggy";
				consumerAppMobile = 7899772315l;
				restaurantId1 = "5271";
				restaurantId = "9990";
				Percentage_restaurants = 5271;
				Flat_Restaurants = 5271;
				FreeDelivery_restaurants1 =9990;
				FreeDelivery_restaurants = 9990;
				nonAccessable_restaurants = 30751;
				OMS_USERNAME="ramzi@swiggy.in";
				OMS_PASSWORD="Test@1234";
			} else if (getEnv().equalsIgnoreCase("stage2")) {
				VENDOR_PASSWORD = "El3ment@ry";
				VENDOR_USERNAME = "9990";
				consumerAppPassword = "swiggy";
				consumerAppMobile = 7899772315l;
				Percentage_restaurants = 11691;
				Flat_Restaurants = 5271;
				FreeDelivery_restaurants1 =9990;
				FreeDelivery_restaurants = 9990;
				nonAccessable_restaurants = 30751;
				restaurantId1 = "5271";
				restaurantId = "9990";
				OMS_USERNAME="ramzi@swiggy.in";
				OMS_PASSWORD="Test@1234";
			} else if (getEnv().equalsIgnoreCase("stage3")) {
				VENDOR_PASSWORD = "El3ment@ry";
				VENDOR_USERNAME = "5271";
				consumerAppPassword = "test1234";
				consumerAppMobile = 9886379321l;
				restaurantId1 = "5271";
				restaurantId = "9990";
				Percentage_restaurants = 9990;
				Flat_Restaurants = 5271;
				FreeDelivery_restaurants1 =9990;
				FreeDelivery_restaurants = 9990;
				nonAccessable_restaurants = 30751;
				OMS_USERNAME="ramzi@swiggy.in";
				OMS_PASSWORD="Test@1234";
			} else if (getEnv().equalsIgnoreCase("prod")) {
				VENDOR_PASSWORD = "$w199y@zolb";
				VENDOR_USERNAME = "9738948943";
				consumerAppPassword = "swiggy";
				consumerAppMobile = 7899772315l;
				restaurantId1 = "4993";
				restaurantId = "9990";
				Percentage_restaurants = 9990;
				Flat_Restaurants = 4993;
				FreeDelivery_restaurants1 =9990;
				FreeDelivery_restaurants = 9990;
				nonAccessable_restaurants = 5271;
				OMS_USERNAME="ramzi@swiggy.in";
				OMS_PASSWORD="Test@1234";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	static {
		RMSHelper rmsHelper=new RMSHelper();
	 try{
		 if (getEnv().equalsIgnoreCase("stage1")){		
		   accessToken=rmsHelper.getLogin(VENDOR_USERNAME, VENDOR_PASSWORD).ResponseValidator.GetNodeValue("data.access_token");
		   System.out.println(accessToken);
		 }
		 else if (getEnv().equalsIgnoreCase("stage2")){
		  accessToken=rmsHelper.getLogin(VENDOR_USERNAME, VENDOR_PASSWORD).ResponseValidator.GetNodeValue("data.access_token");
			System.out.println(accessToken); 
		 }
		 else if (getEnv().equalsIgnoreCase("stage3")){
		   accessToken=rmsHelper.getLogin(VENDOR_USERNAME, VENDOR_PASSWORD).ResponseValidator.GetNodeValue("data.access_token");
			System.out.println(accessToken); 
		 }
		 else if (getEnv().equalsIgnoreCase("prod")){
			 accessToken=rmsHelper.getLogin(VENDOR_USERNAME, VENDOR_PASSWORD).ResponseValidator.GetNodeValue("data.access_token");
			 System.out.println(accessToken);	 
		 }
	 }
		catch(Exception e){
			
		}
	}
	
	
	
	
	@DataProvider(name="createTD")
	public static Object[][] createTDData() throws IOException
	
	{
	 JsonHelper jsonHelper = new JsonHelper();
	 
	 ArrayList<Integer> restIds1=new ArrayList<>();
	 restIds1.add(Percentage_restaurants);
	 ArrayList<Restaurant> restList1 =new ArrayList<>();
	 Restaurant r1=new Restaurant();
	 r1.setId(Percentage_restaurants);
	 r1.setName("Myramzi");
	 r1.setCity(219);
	 r1.setArea("MyRamziArea");
	 r1.setRecommended(0);
	 r1.setCostForTwo(0);
	 r1.setDeliveryCharge(0);
	 r1.setMinimumOrder(0);
	 r1.setOpened(0);
	 r1.setDeliveryTime(0);
	 r1.setTmpClosed(0);
	 r1.setPostalCode(0);
	 r1.setCityDeliveryCharge(0);
	 r1.setThreshold(0);
	 r1.setDiscountType(0);
	 r1.setTradeCampaignHeaders(new ArrayList());
	 r1.setSelect("false");
	 r1.setNew("false");
	 r1.setCategories(new ArrayList());
	 restList1.add(r1);
	 RuleDiscount rd=new RuleDiscount();
	 rd.setType("Percentage");
	 rd.setDiscountLevel("Restaurant");
	 rd.setPercentDiscount(2);
	 rd.setMinCartAmount(0);
	 rd.setPercentDiscountAbsoluteCap(0);
	 Date Date1 = DateUtils.addDays(new Date(), 7);
	 	 
//	 Category cat1= new Category("1437332","Starters");
//	 List<Category> catList1=new ArrayList<>();
//	 catList1.add(cat1);
//	 r1.setCategories(catList1);
			 
/*		Slots s7=new Slots("1102", "FRI", "1100");
		ArrayList<Slots> list7=new ArrayList<>();
     list7.add(s7);*/
	 
	 RMSCreateTDEntry entry1=new RMSTDBuilder().
				setEnabled("true").
				setRestaurantIds(restIds1).
				setValidFrom(Long.valueOf(DateUtils.addDays(new Date(), 7).toInstant().toEpochMilli())).
				setValidTill(Long.valueOf(DateUtils.addMinutes(Date1, 2).toInstant().toEpochMilli())).
				setCampaignType("Percentage").setRuleDiscountType("Percentage").
				setCreatedBy(9738948943l).setDiscountLevel("Restaurant").
				setRestaurantList(restList1).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
				setUserRestriction("false").setRestaurantFirstOrder("false").
				setRuleDiscount(rd).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
				setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();
 
     ArrayList<Integer> restIds2=new ArrayList<>();
	 restIds2.add(Flat_Restaurants);
	 ArrayList<Restaurant> restList2 =new ArrayList<>();
	 Restaurant r2=new Restaurant();
	 r2.setId(Flat_Restaurants);
	 r2.setName("Myramzi2");
	 r2.setCity(219);
	 r2.setArea("MyRamziArea2");
	 r2.setRecommended(0);
	 r2.setCostForTwo(0);
	 r2.setDeliveryCharge(0);
	 r2.setMinimumOrder(0);
	 r2.setOpened(0);
	 r2.setDeliveryTime(0);
	 r2.setTmpClosed(0);
	 r2.setPostalCode(0);
	 r2.setCityDeliveryCharge(0);
	 r2.setThreshold(0);
	 r2.setDiscountType(0);
	 r2.setTradeCampaignHeaders(new ArrayList());
	 r2.setSelect("false");
	 r2.setNew("false");
	 r2.setCategories(new ArrayList());
	 restList2.add(r2);
	 RuleDiscount rd2=new RuleDiscount();
	 rd2.setType("Flat");
	 rd2.setDiscountLevel("Restaurant");
	 rd2.setFlatDiscountAmount(300);
	 rd2.setMinCartAmount(0);
	 Date Date2 = DateUtils.addDays(new Date(), 1);
	 


	 
	 RMSCreateTDEntry entry2=new RMSTDBuilder().
				setEnabled("true").
				setRestaurantIds(restIds2).
				setValidFrom(Long.valueOf(DateUtils.addDays(new Date(), 1).toInstant().toEpochMilli())).
				setValidTill(Long.valueOf(DateUtils.addMinutes(Date2, 2).toInstant().toEpochMilli())).
				setCampaignType("Flat").setRuleDiscountType("Flat").
				setCreatedBy(9886379321l).setDiscountLevel("Restaurant").
				setRestaurantList(restList2).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
				setUserRestriction("false").setRestaurantFirstOrder("false").
				setRuleDiscount(rd2).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
				setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();

		 ArrayList<Integer> restIds3=new ArrayList<>();
		 restIds3.add(Percentage_restaurants);
		 ArrayList<Restaurant> restList3 =new ArrayList<>();
		 Restaurant r3=new Restaurant();
		 r3.setId(Percentage_restaurants);
		 r3.setName("Myramzi");
		 r3.setCity(219);
		 r3.setArea("MyRamziArea");
		 r3.setRecommended(0);
		 r3.setCostForTwo(0);
		 r3.setDeliveryCharge(0);
		 r3.setMinimumOrder(0);
		 r3.setOpened(0);
		 r3.setDeliveryTime(0);
		 r3.setTmpClosed(0);
		 r3.setPostalCode(0);
		 r3.setCityDeliveryCharge(0);
		 r3.setThreshold(0);
		 r3.setDiscountType(0);
		 r3.setTradeCampaignHeaders(new ArrayList());
		 r3.setSelect("false");
		 r3.setNew("false");
		 r3.setCategories(new ArrayList());
		 restList3.add(r3);
		 RuleDiscount rd3=new RuleDiscount();
		 rd3.setType("Percentage");
		 rd3.setDiscountLevel("Restaurant");
		 rd3.setPercentDiscount(2);
		 rd3.setMinCartAmount(100);
		 rd3.setPercentDiscountAbsoluteCap(0);
		 Date Date3 = DateUtils.addDays(new Date(), 2);
		 
		 RMSCreateTDEntry entry3=new RMSTDBuilder().
					setEnabled("true").
					setRestaurantIds(restIds3).
					setValidFrom(Long.valueOf(DateUtils.addDays(new Date(), 2).toInstant().toEpochMilli())).
					setValidTill(Long.valueOf(DateUtils.addMinutes(Date3, 2).toInstant().toEpochMilli())).
					setCampaignType("Percentage").setRuleDiscountType("Percentage").
					setCreatedBy(9886379321l).setDiscountLevel("Restaurant").
					setRestaurantList(restList3).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
					setUserRestriction("false").setRestaurantFirstOrder("false").
					setRuleDiscount(rd3).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
					setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();
		 
		 ArrayList<Integer> restIds4=new ArrayList<>();
		 restIds4.add(Flat_Restaurants);
		 ArrayList<Restaurant> restList4 =new ArrayList<>();
		 Restaurant r4=new Restaurant();
		 r4.setId(Flat_Restaurants);
		 r4.setName("Myramzi2");
		 r4.setCity(219);
		 r4.setArea("MyRamziArea2");
		 r4.setRecommended(0);
		 r4.setCostForTwo(0);
		 r4.setDeliveryCharge(0);
		 r4.setMinimumOrder(0);
		 r4.setOpened(0);
		 r4.setDeliveryTime(0);
		 r4.setTmpClosed(0);
		 r4.setPostalCode(0);
		 r4.setCityDeliveryCharge(0);
		 r4.setThreshold(0);
		 r4.setDiscountType(0);
		 r4.setTradeCampaignHeaders(new ArrayList());
		 r4.setSelect("false");
		 r4.setNew("false");
		 r4.setCategories(new ArrayList());
		 restList4.add(r4);
		 RuleDiscount rd4=new RuleDiscount();
		 rd4.setType("Flat");
		 rd4.setDiscountLevel("Restaurant");
		 rd4.setFlatDiscountAmount(30);
		 rd4.setMinCartAmount(100);
		 Date Date4 = DateUtils.addDays(new Date(), 3);
		 
		 
		 RMSCreateTDEntry entry4=new RMSTDBuilder().
					setEnabled("true").
					setRestaurantIds(restIds4).
					setValidFrom(Long.valueOf(DateUtils.addDays(new Date(), 3).toInstant().toEpochMilli())).
					setValidTill(Long.valueOf(DateUtils.addMinutes(Date4, 2).toInstant().toEpochMilli())).
					setCampaignType("Flat").setRuleDiscountType("Flat").
					setCreatedBy(9886379321l).setDiscountLevel("Restaurant").
					setRestaurantList(restList4).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
					setUserRestriction("false").setRestaurantFirstOrder("false").
					setRuleDiscount(rd4).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
					setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();

		 ArrayList<Integer> restIds5=new ArrayList<>();
		 restIds5.add(Percentage_restaurants);
		 ArrayList<Restaurant> restList5 =new ArrayList<>();
		 Restaurant r5=new Restaurant();
		 r5.setId(Percentage_restaurants);
		 r5.setName("Myramzi");
		 r5.setCity(219);
		 r5.setArea("MyRamziArea");
		 r5.setRecommended(0);
		 r5.setCostForTwo(0);
		 r5.setDeliveryCharge(0);
		 r5.setMinimumOrder(0);
		 r5.setOpened(0);
		 r5.setDeliveryTime(0);
		 r5.setTmpClosed(0);
		 r5.setPostalCode(0);
		 r5.setCityDeliveryCharge(0);
		 r5.setThreshold(0);
		 r5.setDiscountType(0);
		 r5.setTradeCampaignHeaders(new ArrayList());
		 r5.setSelect("false");
		 r5.setNew("false");
		 r5.setCategories(new ArrayList());
		 restList5.add(r5);
		 RuleDiscount rd5=new RuleDiscount();
		 rd5.setType("Percentage");
		 rd5.setDiscountLevel("Restaurant");
		 rd5.setPercentDiscount(50);
		 rd5.setMinCartAmount(1000);
		 rd5.setPercentDiscountAbsoluteCap(0);
		 Date Date5 = DateUtils.addDays(new Date(), 4);
		 
		 RMSCreateTDEntry entry5=new RMSTDBuilder().
					setEnabled("true").
					setRestaurantIds(restIds5).
					setValidFrom(Long.valueOf(DateUtils.addDays(new Date(), 4).toInstant().toEpochMilli())).
					setValidTill(Long.valueOf(DateUtils.addMinutes(Date5, 2).toInstant().toEpochMilli())).
					setCampaignType("Percentage").setRuleDiscountType("Percentage").
					setCreatedBy(9886379321l).setDiscountLevel("Restaurant").
					setRestaurantList(restList5).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
					setUserRestriction("false").setRestaurantFirstOrder("false").
					setRuleDiscount(rd5).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
					setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();
		 

		 ArrayList<Integer> restIds8=new ArrayList<>();
		 restIds8.add(Percentage_restaurants);
		 ArrayList<Restaurant> restList8 =new ArrayList<>();
		 Restaurant r8=new Restaurant();
		 r8.setId(Percentage_restaurants);
		 r8.setName("Myramzi");
		 r8.setCity(219);
		 r8.setArea("MyRamziArea");
		 r8.setRecommended(0);
		 r8.setCostForTwo(0);
		 r8.setDeliveryCharge(0);
		 r8.setMinimumOrder(0);
		 r8.setOpened(0);
		 r8.setDeliveryTime(0);
		 r8.setTmpClosed(0);
		 r8.setPostalCode(0);
		 r8.setCityDeliveryCharge(0);
		 r8.setThreshold(0);
		 r8.setDiscountType(0);
		 r8.setTradeCampaignHeaders(new ArrayList());
		 r8.setSelect("false");
		 r8.setNew("false");
		 r8.setCategories(new ArrayList());
		 restList8.add(r8);
		 RuleDiscount rd8=new RuleDiscount();
		 rd8.setType("Percentage");
		 rd8.setDiscountLevel("Restaurant");
		 rd8.setPercentDiscount(50);
		 rd8.setMinCartAmount(0);
		 rd8.setPercentDiscountAbsoluteCap(0);
		 Date Date8 = DateUtils.addDays(new Date(), 4);
		 
		 RMSCreateTDEntry entry8=new RMSTDBuilder().
					setEnabled("true").
					setRestaurantIds(restIds8).
					setValidFrom(Long.valueOf(DateUtils.addDays(new Date(), 4).toInstant().toEpochMilli())).
					setValidTill(Long.valueOf(DateUtils.addMinutes(Date8, 2).toInstant().toEpochMilli())).
					setCampaignType("Percentage").setRuleDiscountType("Percentage").
					setCreatedBy(9886379321l).setDiscountLevel("Restaurant").
					setRestaurantList(restList8).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
					setUserRestriction("false").setRestaurantFirstOrder("false").
					setRuleDiscount(rd8).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
					setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();

		 
		 ArrayList<Integer> restIds9=new ArrayList<>();
		 restIds9.add(Flat_Restaurants);
		 ArrayList<Restaurant> restList9 =new ArrayList<>();
		 Restaurant r9=new Restaurant();
		 r9.setId(Flat_Restaurants);
		 r9.setName("Myramzi2");
		 r9.setCity(219);
		 r9.setArea("MyRamziArea2");
		 r9.setRecommended(0);
		 r9.setCostForTwo(0);
		 r9.setDeliveryCharge(0);
		 r9.setMinimumOrder(0);
		 r9.setOpened(0);
		 r9.setDeliveryTime(0);
		 r9.setTmpClosed(0);
		 r9.setPostalCode(0);
		 r9.setCityDeliveryCharge(0);
		 r9.setThreshold(0);
		 r9.setDiscountType(0);
		 r9.setTradeCampaignHeaders(new ArrayList());
		 r9.setSelect("false");
		 r9.setNew("false");
		 r9.setCategories(new ArrayList());
		 restList9.add(r9);
		 RuleDiscount rd9=new RuleDiscount();
		 rd9.setType("Flat");
		 rd9.setDiscountLevel("Restaurant");
		 rd9.setFlatDiscountAmount(300);
		 rd9.setMinCartAmount(1000);
		 Date Date9 = DateUtils.addDays(new Date(), 3);
		 
		 
		 RMSCreateTDEntry entry9=new RMSTDBuilder().
					setEnabled("true").
					setRestaurantIds(restIds9).
					setValidFrom(Long.valueOf(DateUtils.addDays(new Date(), 3).toInstant().toEpochMilli())).
					setValidTill(Long.valueOf(DateUtils.addMinutes(Date9, 2).toInstant().toEpochMilli())).
					setCampaignType("Flat").setRuleDiscountType("Flat").
					setCreatedBy(9886379321l).setDiscountLevel("Restaurant").
					setRestaurantList(restList9).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
					setUserRestriction("false").setRestaurantFirstOrder("false").
					setRuleDiscount(rd9).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
					setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();

		 ArrayList<Integer> restIds10=new ArrayList<>();
		 restIds10.add(Flat_Restaurants);
		 ArrayList<Restaurant> restList10 =new ArrayList<>();
		 Restaurant r10=new Restaurant();
		 r10.setId(Flat_Restaurants);
		 r10.setName("Myramzi2");
		 r10.setCity(219);
		 r10.setArea("MyRamziArea2");
		 r10.setRecommended(0);
		 r10.setCostForTwo(0);
		 r10.setDeliveryCharge(0);
		 r10.setMinimumOrder(0);
		 r10.setOpened(0);
		 r10.setDeliveryTime(0);
		 r10.setTmpClosed(0);
		 r10.setPostalCode(0);
		 r10.setCityDeliveryCharge(0);
		 r10.setThreshold(0);
		 r10.setDiscountType(0);
		 r10.setTradeCampaignHeaders(new ArrayList());
		 r10.setSelect("false");
		 r10.setNew("false");
		 r10.setCategories(new ArrayList());
		 restList10.add(r10);
		 RuleDiscount rd10=new RuleDiscount();
		 rd10.setType("Flat");
		 rd10.setDiscountLevel("Restaurant");
		 rd10.setFlatDiscountAmount(30);
		 rd10.setMinCartAmount(0);
		 Date Date10 = DateUtils.addDays(new Date(), 3);
		 
		 
		 RMSCreateTDEntry entry10=new RMSTDBuilder().
					setEnabled("true").
					setRestaurantIds(restIds10).
					setValidFrom(Long.valueOf(DateUtils.addDays(new Date(), 3).toInstant().toEpochMilli())).
					setValidTill(Long.valueOf(DateUtils.addMinutes(Date10, 2).toInstant().toEpochMilli())).
					setCampaignType("Flat").setRuleDiscountType("Flat").
					setCreatedBy(9886379321l).setDiscountLevel("Restaurant").
					setRestaurantList(restList10).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
					setUserRestriction("false").setRestaurantFirstOrder("false").
					setRuleDiscount(rd10).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
					setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();


		 ArrayList<Integer> restIds11=new ArrayList<>();
		 restIds11.add(Percentage_restaurants);
		 restIds11.add(Flat_Restaurants);		 
		 ArrayList<Restaurant> restList11 =new ArrayList<>();
		 Restaurant r11=new Restaurant();
		 Restaurant r12=new Restaurant();
		 r11.setId(Percentage_restaurants);
		 r11.setName("Myramzi");
		 r11.setCity(219);
		 r11.setArea("MyRamziArea");
		 r11.setRecommended(0);
		 r11.setCostForTwo(0);
		 r11.setDeliveryCharge(0);
		 r11.setMinimumOrder(0);
		 r11.setOpened(0);
		 r11.setDeliveryTime(0);
		 r11.setTmpClosed(0);
		 r11.setPostalCode(0);
		 r11.setCityDeliveryCharge(0);
		 r11.setThreshold(0);
		 r11.setDiscountType(0);
		 r11.setTradeCampaignHeaders(new ArrayList());
		 r11.setSelect("false");
		 r11.setNew("false");
		 r11.setCategories(new ArrayList<>());
		 

		 r12.setId(Flat_Restaurants);
		 r12.setName("Myramzi");
		 r12.setCity(219);
		 r12.setArea("MyRamziArea");
		 r12.setRecommended(0);
		 r12.setCostForTwo(0);
		 r12.setDeliveryCharge(0);
		 r12.setMinimumOrder(0);
		 r12.setOpened(0);
		 r12.setDeliveryTime(0);
		 r12.setTmpClosed(0);
		 r12.setPostalCode(0);
		 r12.setCityDeliveryCharge(0);
		 r12.setThreshold(0);
		 r12.setDiscountType(0);
		 r12.setTradeCampaignHeaders(new ArrayList());
		 r12.setSelect("false");
		 r12.setNew("false");
		 r12.setCategories(new ArrayList<>());
		 
		 restList11.add(r11);
		 restList11.add(r12);
		 RuleDiscount rd11=new RuleDiscount();
		 rd11.setType("Percentage");
		 rd11.setDiscountLevel("Restaurant");
		 rd11.setPercentDiscount(2);
		 rd11.setMinCartAmount(0);
		 rd11.setPercentDiscountAbsoluteCap(0);
		 Date Date11 = DateUtils.addDays(new Date(), 0);
		 	 		 
		 RMSCreateTDEntry entry11=new RMSTDBuilder().
					setEnabled("true").
					setRestaurantIds(restIds11).
					setValidFrom(Long.valueOf(DateUtils.addDays(new Date(), 0).toInstant().toEpochMilli())).
					setValidTill(Long.valueOf(DateUtils.addMinutes(Date11, 2).toInstant().toEpochMilli())).
					setCampaignType("Percentage").setRuleDiscountType("Percentage").
					setCreatedBy(9738948943l).setDiscountLevel("Restaurant").
					setRestaurantList(restList11).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
					setUserRestriction("false").setRestaurantFirstOrder("false").
					setRuleDiscount(rd11).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
					setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();

        
		 ArrayList<Integer> restIds13=new ArrayList<>();
		 restIds13.add(Flat_Restaurants);
		 restIds13.add(Percentage_restaurants);
		 ArrayList<Restaurant> restList13 =new ArrayList<>();
		 Restaurant r13=new Restaurant();
		 Restaurant r14=new Restaurant();
		 r13.setId(Flat_Restaurants);
		 r13.setName("Myramzi2");
		 r13.setCity(219);
		 r13.setArea("MyRamziArea2");
		 r13.setRecommended(0);
		 r13.setCostForTwo(0);
		 r13.setDeliveryCharge(0);
		 r13.setMinimumOrder(0);
		 r13.setOpened(0);
		 r13.setDeliveryTime(0);
		 r13.setTmpClosed(0);
		 r13.setPostalCode(0);
		 r13.setCityDeliveryCharge(0);
		 r13.setThreshold(0);
		 r13.setDiscountType(0);
		 r13.setTradeCampaignHeaders(new ArrayList());
		 r13.setSelect("false");
		 r13.setNew("false");
		 r13.setCategories(new ArrayList());
		
		 r14.setId(Flat_Restaurants);
		 r14.setName("Myramzi");
		 r14.setCity(219);
		 r14.setArea("MyRamziArea");
		 r14.setRecommended(0);
		 r14.setCostForTwo(0);
		 r14.setDeliveryCharge(0);
		 r14.setMinimumOrder(0);
		 r14.setOpened(0);
		 r14.setDeliveryTime(0);
		 r14.setTmpClosed(0);
		 r14.setPostalCode(0);
		 r14.setCityDeliveryCharge(0);
		 r14.setThreshold(0);
		 r14.setDiscountType(0);
		 r14.setTradeCampaignHeaders(new ArrayList());
		 r14.setSelect("false");
		 r14.setNew("false");
		 r14.setCategories(new ArrayList<>());
		
		 restList13.add(r13);
		 restList13.add(r14);
		 RuleDiscount rd13=new RuleDiscount();
		 rd13.setType("Flat");
		 rd13.setDiscountLevel("Restaurant");
		 rd13.setFlatDiscountAmount(300);
		 rd13.setMinCartAmount(0);
		 Date Date13 = DateUtils.addDays(new Date(), 1);
		 


		 
		 RMSCreateTDEntry entry13=new RMSTDBuilder().
					setEnabled("true").
					setRestaurantIds(restIds13).
					setValidFrom(Long.valueOf(DateUtils.addDays(new Date(), 1).toInstant().toEpochMilli())).
					setValidTill(Long.valueOf(DateUtils.addMinutes(Date13, 2).toInstant().toEpochMilli())).
					setCampaignType("Flat").setRuleDiscountType("Flat").
					setCreatedBy(9886379321l).setDiscountLevel("Restaurant").
					setRestaurantList(restList13).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
					setUserRestriction("false").setRestaurantFirstOrder("false").
					setRuleDiscount(rd13).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
					setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();

			 ArrayList<Integer> restIds15=new ArrayList<>();
			 restIds15.add(Percentage_restaurants);
			 restIds15.add(Flat_Restaurants);
			 ArrayList<Restaurant> restList15 =new ArrayList<>();
			 Restaurant r15=new Restaurant();
			 Restaurant r16=new Restaurant();
			 r15.setId(Percentage_restaurants);
			 r15.setName("Myramzi");
			 r15.setCity(219);
			 r15.setArea("MyRamziArea");
			 r15.setRecommended(0);
			 r15.setCostForTwo(0);
			 r15.setDeliveryCharge(0);
			 r15.setMinimumOrder(0);
			 r15.setOpened(0);
			 r15.setDeliveryTime(0);
			 r15.setTmpClosed(0);
			 r15.setPostalCode(0);
			 r15.setCityDeliveryCharge(0);
			 r15.setThreshold(0);
			 r15.setDiscountType(0);
			 r15.setTradeCampaignHeaders(new ArrayList());
			 r15.setSelect("false");
			 r15.setNew("false");
			 r15.setCategories(new ArrayList());
		
			 r16.setId(Flat_Restaurants);
			 r16.setName("Myramzi");
			 r16.setCity(219);
			 r16.setArea("MyRamziArea");
			 r16.setRecommended(0);
			 r16.setCostForTwo(0);
			 r16.setDeliveryCharge(0);
			 r16.setMinimumOrder(0);
			 r16.setOpened(0);
			 r16.setDeliveryTime(0);
			 r16.setTmpClosed(0);
			 r16.setPostalCode(0);
			 r16.setCityDeliveryCharge(0);
			 r16.setThreshold(0);
			 r16.setDiscountType(0);
			 r16.setTradeCampaignHeaders(new ArrayList());
			 r16.setSelect("false");
			 r16.setNew("false");
			 r16.setCategories(new ArrayList<>());
			
			 restList15.add(r15);
			 restList15.add(r16);
			 RuleDiscount rd15=new RuleDiscount();
			 rd15.setType("Percentage");
			 rd15.setDiscountLevel("Restaurant");
			 rd15.setPercentDiscount(2);
			 rd15.setMinCartAmount(100);
			 rd15.setPercentDiscountAbsoluteCap(0);
			 Date Date15 = DateUtils.addDays(new Date(), 2);
			 
			 RMSCreateTDEntry entry15=new RMSTDBuilder().
						setEnabled("true").
						setRestaurantIds(restIds15).
						setValidFrom(Long.valueOf(DateUtils.addDays(new Date(), 2).toInstant().toEpochMilli())).
						setValidTill(Long.valueOf(DateUtils.addMinutes(Date15, 2).toInstant().toEpochMilli())).
						setCampaignType("Percentage").setRuleDiscountType("Percentage").
						setCreatedBy(9886379321l).setDiscountLevel("Restaurant").
						setRestaurantList(restList15).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
						setUserRestriction("false").setRestaurantFirstOrder("false").
						setRuleDiscount(rd15).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
						setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();
			 
			 ArrayList<Integer> restIds17=new ArrayList<>();
			 restIds17.add(Flat_Restaurants);
			 restIds17.add(Percentage_restaurants);
			 ArrayList<Restaurant> restList17 =new ArrayList<>();
			 Restaurant r17=new Restaurant();
			 Restaurant r18=new Restaurant();
			
			 r17.setId(Flat_Restaurants);
			 r17.setName("Myramzi2");
			 r17.setCity(219);
			 r17.setArea("MyRamziArea2");
			 r17.setRecommended(0);
			 r17.setCostForTwo(0);
			 r17.setDeliveryCharge(0);
			 r17.setMinimumOrder(0);
			 r17.setOpened(0);
			 r17.setDeliveryTime(0);
			 r17.setTmpClosed(0);
			 r17.setPostalCode(0);
			 r17.setCityDeliveryCharge(0);
			 r17.setThreshold(0);
			 r17.setDiscountType(0);
			 r17.setTradeCampaignHeaders(new ArrayList());
			 r17.setSelect("false");
			 r17.setNew("false");
			 r17.setCategories(new ArrayList());
			 
			 r18.setId(Flat_Restaurants);
			 r18.setName("Myramzi");
			 r18.setCity(219);
			 r18.setArea("MyRamziArea");
			 r18.setRecommended(0);
			 r18.setCostForTwo(0);
			 r18.setDeliveryCharge(0);
			 r18.setMinimumOrder(0);
			 r18.setOpened(0);
			 r18.setDeliveryTime(0);
			 r18.setTmpClosed(0);
			 r18.setPostalCode(0);
			 r18.setCityDeliveryCharge(0);
			 r18.setThreshold(0);
			 r18.setDiscountType(0);
			 r18.setTradeCampaignHeaders(new ArrayList());
			 r18.setSelect("false");
			 r18.setNew("false");
			 r18.setCategories(new ArrayList<>());
			
			 restList17.add(r17);
			 restList17.add(r18);
			 RuleDiscount rd17=new RuleDiscount();
			 rd17.setType("Flat");
			 rd17.setDiscountLevel("Restaurant");
			 rd17.setFlatDiscountAmount(30);
			 rd17.setMinCartAmount(100);
			 Date Date17 = DateUtils.addDays(new Date(), 3);
			 
			 
			 RMSCreateTDEntry entry17=new RMSTDBuilder().
						setEnabled("true").
						setRestaurantIds(restIds17).
						setValidFrom(Long.valueOf(DateUtils.addDays(new Date(), 3).toInstant().toEpochMilli())).
						setValidTill(Long.valueOf(DateUtils.addMinutes(Date17, 2).toInstant().toEpochMilli())).
						setCampaignType("Flat").setRuleDiscountType("Flat").
						setCreatedBy(9886379321l).setDiscountLevel("Restaurant").
						setRestaurantList(restList17).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
						setUserRestriction("false").setRestaurantFirstOrder("false").
						setRuleDiscount(rd17).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
						setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();

			 ArrayList<Integer> restIds19=new ArrayList<>();
			 restIds19.add(Percentage_restaurants);
			 restIds19.add(Flat_Restaurants);
			 ArrayList<Restaurant> restList19 =new ArrayList<>();
			 Restaurant r19=new Restaurant();
			 Restaurant r20=new Restaurant();
			 
			 r19.setId(Percentage_restaurants);
			 r19.setName("Myramzi");
			 r19.setCity(219);
			 r19.setArea("MyRamziArea");
			 r19.setRecommended(0);
			 r19.setCostForTwo(0);
			 r19.setDeliveryCharge(0);
			 r19.setMinimumOrder(0);
			 r19.setOpened(0);
			 r19.setDeliveryTime(0);
			 r19.setTmpClosed(0);
			 r19.setPostalCode(0);
			 r19.setCityDeliveryCharge(0);
			 r19.setThreshold(0);
			 r19.setDiscountType(0);
			 r19.setTradeCampaignHeaders(new ArrayList());
			 r19.setSelect("false");
			 r19.setNew("false");
			 r19.setCategories(new ArrayList());
			 
			 r20.setId(Flat_Restaurants);
			 r20.setName("Myramzi");
			 r20.setCity(219);
			 r20.setArea("MyRamziArea");
			 r20.setRecommended(0);
			 r20.setCostForTwo(0);
			 r20.setDeliveryCharge(0);
			 r20.setMinimumOrder(0);
			 r20.setOpened(0);
			 r20.setDeliveryTime(0);
			 r20.setTmpClosed(0);
			 r20.setPostalCode(0);
			 r20.setCityDeliveryCharge(0);
			 r20.setThreshold(0);
			 r20.setDiscountType(0);
			 r20.setTradeCampaignHeaders(new ArrayList());
			 r20.setSelect("false");
			 r20.setNew("false");
			 r20.setCategories(new ArrayList());
			 
			 restList19.add(r19);
			 restList19.add(r20);
			 
			 RuleDiscount rd19=new RuleDiscount();
			 rd19.setType("Percentage");
			 rd19.setDiscountLevel("Restaurant");
			 rd19.setPercentDiscount(50);
			 rd19.setMinCartAmount(1000);
			 rd19.setPercentDiscountAbsoluteCap(0);
			 Date Date19 = DateUtils.addDays(new Date(), 4);
			 
			 RMSCreateTDEntry entry19=new RMSTDBuilder().
						setEnabled("true").
						setRestaurantIds(restIds19).
						setValidFrom(Long.valueOf(DateUtils.addDays(new Date(), 4).toInstant().toEpochMilli())).
						setValidTill(Long.valueOf(DateUtils.addMinutes(Date19, 2).toInstant().toEpochMilli())).
						setCampaignType("Percentage").setRuleDiscountType("Percentage").
						setCreatedBy(9886379321l).setDiscountLevel("Restaurant").
						setRestaurantList(restList19).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
						setUserRestriction("false").setRestaurantFirstOrder("false").
						setRuleDiscount(rd19).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
						setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();
			 

			 ArrayList<Integer> restIds21=new ArrayList<>();
			 restIds21.add(Percentage_restaurants);
			 restIds21.add(Flat_Restaurants);
			 ArrayList<Restaurant> restList21 =new ArrayList<>();
			 Restaurant r21=new Restaurant();
			 Restaurant r22=new Restaurant();
			 
			 r21.setId(Percentage_restaurants);
			 r21.setName("Myramzi");
			 r21.setCity(219);
			 r21.setArea("MyRamziArea");
			 r21.setRecommended(0);
			 r21.setCostForTwo(0);
			 r21.setDeliveryCharge(0);
			 r21.setMinimumOrder(0);
			 r21.setOpened(0);
			 r21.setDeliveryTime(0);
			 r21.setTmpClosed(0);
			 r21.setPostalCode(0);
			 r21.setCityDeliveryCharge(0);
			 r21.setThreshold(0);
			 r21.setDiscountType(0);
			 r21.setTradeCampaignHeaders(new ArrayList());
			 r21.setSelect("false");
			 r21.setNew("false");
			 r21.setCategories(new ArrayList());
			 
			 r22.setId(Flat_Restaurants);
			 r22.setName("Myramzi");
			 r22.setCity(219);
			 r22.setArea("MyRamziArea");
			 r22.setRecommended(0);
			 r22.setCostForTwo(0);
			 r22.setDeliveryCharge(0);
			 r22.setMinimumOrder(0);
			 r22.setOpened(0);
			 r22.setDeliveryTime(0);
			 r22.setTmpClosed(0);
			 r22.setPostalCode(0);
			 r22.setCityDeliveryCharge(0);
			 r22.setThreshold(0);
			 r22.setDiscountType(0);
			 r22.setTradeCampaignHeaders(new ArrayList());
			 r22.setSelect("false");
			 r22.setNew("false");
			 r22.setCategories(new ArrayList());
			
			 restList21.add(r21);
			 restList21.add(r22);
			 RuleDiscount rd21=new RuleDiscount();
			 rd21.setType("Percentage");
			 rd21.setDiscountLevel("Restaurant");
			 rd21.setPercentDiscount(50);
			 rd21.setMinCartAmount(0);
			 rd21.setPercentDiscountAbsoluteCap(0);
			 Date Date20 = DateUtils.addDays(new Date(), 4);
			 
			 RMSCreateTDEntry entry21=new RMSTDBuilder().
						setEnabled("true").
						setRestaurantIds(restIds21).
						setValidFrom(Long.valueOf(DateUtils.addDays(new Date(), 4).toInstant().toEpochMilli())).
						setValidTill(Long.valueOf(DateUtils.addMinutes(Date20, 2).toInstant().toEpochMilli())).
						setCampaignType("Percentage").setRuleDiscountType("Percentage").
						setCreatedBy(9886379321l).setDiscountLevel("Restaurant").
						setRestaurantList(restList21).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
						setUserRestriction("false").setRestaurantFirstOrder("false").
						setRuleDiscount(rd21).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
						setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();

			 
			 ArrayList<Integer> restIds23=new ArrayList<>();
			 restIds23.add(Flat_Restaurants);
			 restIds23.add(Percentage_restaurants);
			 ArrayList<Restaurant> restList23 =new ArrayList<>();
			 Restaurant r23=new Restaurant();
			 Restaurant r24=new Restaurant();
			 
			 r23.setId(Flat_Restaurants);
			 r23.setName("Myramzi2");
			 r23.setCity(219);
			 r23.setArea("MyRamziArea2");
			 r23.setRecommended(0);
			 r23.setCostForTwo(0);
			 r23.setDeliveryCharge(0);
			 r23.setMinimumOrder(0);
			 r23.setOpened(0);
			 r23.setDeliveryTime(0);
			 r23.setTmpClosed(0);
			 r23.setPostalCode(0);
			 r23.setCityDeliveryCharge(0);
			 r23.setThreshold(0);
			 r23.setDiscountType(0);
			 r23.setTradeCampaignHeaders(new ArrayList());
			 r23.setSelect("false");
			 r23.setNew("false");
			 r23.setCategories(new ArrayList());
			 
			 r24.setId(Percentage_restaurants);
			 r24.setName("Myramzi2");
			 r24.setCity(219);
			 r24.setArea("MyRamziArea2");
			 r24.setRecommended(0);
			 r24.setCostForTwo(0);
			 r24.setDeliveryCharge(0);
			 r24.setMinimumOrder(0);
			 r24.setOpened(0);
			 r24.setDeliveryTime(0);
			 r24.setTmpClosed(0);
			 r24.setPostalCode(0);
			 r24.setCityDeliveryCharge(0);
			 r24.setThreshold(0);
			 r24.setDiscountType(0);
			 r24.setTradeCampaignHeaders(new ArrayList());
			 r24.setSelect("false");
			 r24.setNew("false");
			 r24.setCategories(new ArrayList());
			 
			 restList23.add(r23);
			 restList23.add(r24);
			 RuleDiscount rd23=new RuleDiscount();
			 rd23.setType("Flat");
			 rd23.setDiscountLevel("Restaurant");
			 rd23.setFlatDiscountAmount(300);
			 rd23.setMinCartAmount(1000);
			 Date Date22 = DateUtils.addDays(new Date(), 3);
			 
			 
			 RMSCreateTDEntry entry23=new RMSTDBuilder().
						setEnabled("true").
						setRestaurantIds(restIds23).
						setValidFrom(Long.valueOf(DateUtils.addDays(new Date(), 3).toInstant().toEpochMilli())).
						setValidTill(Long.valueOf(DateUtils.addMinutes(Date22, 2).toInstant().toEpochMilli())).
						setCampaignType("Flat").setRuleDiscountType("Flat").
						setCreatedBy(9886379321l).setDiscountLevel("Restaurant").
						setRestaurantList(restList23).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
						setUserRestriction("false").setRestaurantFirstOrder("false").
						setRuleDiscount(rd23).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
						setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();

			 ArrayList<Integer> restIds25=new ArrayList<>();
			 restIds25.add(Flat_Restaurants);
			 restIds25.add(Percentage_restaurants);
			 ArrayList<Restaurant> restList25 =new ArrayList<>();
			 Restaurant r25=new Restaurant();
			 Restaurant r26=new Restaurant();
			
			 r25.setId(Flat_Restaurants);
			 r25.setName("Myramzi2");
			 r25.setCity(219);
			 r25.setArea("MyRamziArea2");
			 r25.setRecommended(0);
			 r25.setCostForTwo(0);
			 r25.setDeliveryCharge(0);
			 r25.setMinimumOrder(0);
			 r25.setOpened(0);
			 r25.setDeliveryTime(0);
			 r25.setTmpClosed(0);
			 r25.setPostalCode(0);
			 r25.setCityDeliveryCharge(0);
			 r25.setThreshold(0);
			 r25.setDiscountType(0);
			 r25.setTradeCampaignHeaders(new ArrayList());
			 r25.setSelect("false");
			 r25.setNew("false");
			 r25.setCategories(new ArrayList());
			 
			 r26.setId(Percentage_restaurants);
			 r26.setName("Myramzi2");
			 r26.setCity(219);
			 r26.setArea("MyRamziArea2");
			 r26.setRecommended(0);
			 r26.setCostForTwo(0);
			 r26.setDeliveryCharge(0);
			 r26.setMinimumOrder(0);
			 r26.setOpened(0);
			 r26.setDeliveryTime(0);
			 r26.setTmpClosed(0);
			 r26.setPostalCode(0);
			 r26.setCityDeliveryCharge(0);
			 r26.setThreshold(0);
			 r26.setDiscountType(0);
			 r26.setTradeCampaignHeaders(new ArrayList());
			 r26.setSelect("false");
			 r26.setNew("false");
			 r26.setCategories(new ArrayList());
			 
			 restList25.add(r25);
			 restList25.add(r26);
			 RuleDiscount rd25=new RuleDiscount();
			 rd25.setType("Flat");
			 rd25.setDiscountLevel("Restaurant");
			 rd25.setFlatDiscountAmount(30);
			 rd25.setMinCartAmount(0);
			 Date Date25 = DateUtils.addDays(new Date(), 3);
			 
			 
			 RMSCreateTDEntry entry25=new RMSTDBuilder().
						setEnabled("true").
						setRestaurantIds(restIds25).
						setValidFrom(Long.valueOf(DateUtils.addDays(new Date(), 3).toInstant().toEpochMilli())).
						setValidTill(Long.valueOf(DateUtils.addMinutes(Date25, 2).toInstant().toEpochMilli())).
						setCampaignType("Flat").setRuleDiscountType("Flat").
						setCreatedBy(9886379321l).setDiscountLevel("Restaurant").
						setRestaurantList(restList25).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
						setUserRestriction("false").setRestaurantFirstOrder("false").
						setRuleDiscount(rd25).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
						setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();

		 
		 
		 
		 ArrayList<Integer> restIds6=new ArrayList<>();
		 restIds6.add(FreeDelivery_restaurants1);
		 ArrayList<Restaurant> restList6 =new ArrayList<>();
		 Restaurant r6=new Restaurant();
		 r6.setId(FreeDelivery_restaurants1);
		 r6.setName("Myramzi");
		 r6.setCity(219);
		 r6.setArea("MyRamziArea");
		 r6.setRecommended(0);
		 r6.setCostForTwo(0);
		 r6.setDeliveryCharge(0);
		 r6.setMinimumOrder(0);
		 r6.setOpened(0);
		 r6.setDeliveryTime(0);
		 r6.setTmpClosed(0);
		 r6.setPostalCode(0);
		 r6.setCityDeliveryCharge(0);
		 r6.setThreshold(0);
		 r6.setDiscountType(0);
		 r6.setTradeCampaignHeaders(new ArrayList());
		 r6.setSelect("false");
		 r6.setNew("false");
		 r6.setCategories(new ArrayList());
		 restList6.add(r6);
		 RuleDiscount rd6=new RuleDiscount();
		 rd6.setType("FREE_DELIVERY");
		 rd6.setDiscountLevel("Restaurant");
		 rd6.setPercentDiscount(2);
		 rd6.setMinCartAmount(0);
		 rd6.setPercentDiscountAbsoluteCap(0);
		 Date Date6 = DateUtils.addDays(new Date(), 5);
	        
		 RMSCreateTDEntry entry6=new RMSTDBuilder().
					setEnabled("true").
					setRestaurantIds(restIds6).
					setValidFrom(Long.valueOf(DateUtils.addDays(new Date(), 5).toInstant().toEpochMilli())).
					setValidTill(Long.valueOf(DateUtils.addMinutes(Date6, 2).toInstant().toEpochMilli())).
					setCampaignType("FREE_DELIVERY").setRuleDiscountType("FREE_DELIVERY").
					setCreatedBy(9738948943l).setDiscountLevel("Restaurant").
					setRestaurantList(restList6).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
					setUserRestriction("false").setRestaurantFirstOrder("false").
					setRuleDiscount(rd6).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
					setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();
		 
		 ArrayList<Integer> restIds7=new ArrayList<>();
		 restIds7.add(FreeDelivery_restaurants);
		 ArrayList<Restaurant> restList7 =new ArrayList<>();
		 Restaurant r7=new Restaurant();
		 r7.setId(FreeDelivery_restaurants);
		 r7.setName("Myramzi");
		 r7.setCity(219);
		 r7.setArea("MyRamziArea");
		 r7.setRecommended(0);
		 r7.setCostForTwo(0);
		 r7.setDeliveryCharge(0);
		 r7.setMinimumOrder(0);
		 r7.setOpened(0);
		 r7.setDeliveryTime(0);
		 r7.setTmpClosed(0);
		 r7.setPostalCode(0);
		 r7.setCityDeliveryCharge(0);
		 r7.setThreshold(0);
		 r7.setDiscountType(0);
		 r7.setTradeCampaignHeaders(new ArrayList());
		 r7.setSelect("false");
		 r7.setNew("false");
		 r7.setCategories(new ArrayList());
		 restList7.add(r7);
		 RuleDiscount rd7=new RuleDiscount();
		 rd7.setType("FREE_DELIVERY");
		 rd7.setDiscountLevel("Restaurant");
		 rd7.setPercentDiscount(2);
		 rd7.setMinCartAmount(99);
		 rd7.setPercentDiscountAbsoluteCap(0);
		 Date Date7 = DateUtils.addDays(new Date(), 6);
		 
		Slots s7=new Slots("1102", "FRI", "1100");
				ArrayList<Slots> list7=new ArrayList<>();
		        list7.add(s7);
		 
		
		 RMSCreateTDEntry entry7=new RMSTDBuilder().
					setEnabled("true").
					setRestaurantIds(restIds7).
					setValidFrom(Long.valueOf(DateUtils.addDays(new Date(), 6).toInstant().toEpochMilli())).
					setValidTill(Long.valueOf(DateUtils.addMinutes(Date7, 2).toInstant().toEpochMilli())).
					setCampaignType("FREE_DELIVERY").setRuleDiscountType("FREE_DELIVERY").
					setCreatedBy(9738948943l).setDiscountLevel("Restaurant").
					setRestaurantList(restList7).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
					setUserRestriction("false").setRestaurantFirstOrder("false").
					setRuleDiscount(rd7).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
					setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();

		 
		 
		 ArrayList<Integer> restIds26=new ArrayList<>();
		 restIds26.add(Freebie_restaurants);
		 List<Restaurant> restList26 =new ArrayList<>();
		 Restaurant r28=new Restaurant();
		 r28.setId(Freebie_restaurants);
		 r28.setName("Myramzi");
		 r28.setCity(219);
		 r28.setArea("MyRamziArea");
		 r28.setRecommended(0);
		 r28.setCostForTwo(0);
		 r28.setDeliveryCharge(0);
		 r28.setMinimumOrder(0);
		 r28.setOpened(0);
		 r28.setDeliveryTime(0);
		 r28.setTmpClosed(0);
		 r28.setPostalCode(0);
		 r28.setCityDeliveryCharge(0);
		 r28.setThreshold(0);
		 r28.setDiscountType(0);
		 r28.setTradeCampaignHeaders(new ArrayList());
		 r28.setSelect("false");
		 r28.setNew("false");
		 Menu m26=new Menu("8428196","Baby corn Tandoori");
		 	ArrayList<Menu> menu26=new ArrayList<>();
		 	menu26.add(m26);
		 Category cat26= new Category("1250786","Punjabi Rasoi",menu26);	
     		 List<Category> catList26=new ArrayList<>();
     		 catList26.add(cat26);
		 r26.setCategories(catList26);
		 restList26.add(r28);
		 System.out.println("Restaurant List88888888888 ::::" +restList26);
		 RuleDiscount rd26=new RuleDiscount();
		 rd26.setType("Freebie");
		 rd26.setDiscountLevel("Restaurant");
		 rd26.setPercentDiscount(0);
		 rd26.setMinCartAmount(0);
		 rd26.setPercentDiscountAbsoluteCap(0);
		 rd26.setItemId(9144443);
		 System.out.println("Rule Discount8888 :: "+rd26);
		 Date Date26 = DateUtils.addDays(new Date(), 8);
		 
		 RMSCreateTDEntry entry26=new RMSTDBuilder().
					setEnabled("true").
					setRestaurantIds(restIds26).
					setValidFrom(Long.valueOf(DateUtils.addDays(new Date(), 8).toInstant().toEpochMilli())).
					setValidTill(Long.valueOf(DateUtils.addMinutes(Date26, 2).toInstant().toEpochMilli())).
					setCampaignType("Freebie").setRuleDiscountType("Freebie").
					setCreatedBy(9738948943l).setDiscountLevel("Restaurant").
					setRestaurantList(restList26).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
					setUserRestriction("false").setRestaurantFirstOrder("false").
					setRuleDiscount(rd26).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
					setCommissionOnFullBill("false").setTimeSlotRestriction("false").setItemName("Baby corn Tandoori").build();
	 
		return new Object[][] {	
								{jsonHelper.getObjectToJSON(entry1),accessToken,true},
								{jsonHelper.getObjectToJSON(entry2),accessToken,false},
								{jsonHelper.getObjectToJSON(entry3),accessToken,false},
								{jsonHelper.getObjectToJSON(entry4),accessToken,false},
								{jsonHelper.getObjectToJSON(entry5),accessToken,false},
								{jsonHelper.getObjectToJSON(entry8),accessToken,false},
								{jsonHelper.getObjectToJSON(entry9),accessToken,false},
								{jsonHelper.getObjectToJSON(entry10),accessToken,false},
								{jsonHelper.getObjectToJSON(entry11),accessToken,false},
								{jsonHelper.getObjectToJSON(entry13),accessToken,false},
								{jsonHelper.getObjectToJSON(entry15),accessToken,false},
								{jsonHelper.getObjectToJSON(entry17),accessToken,false},
								{jsonHelper.getObjectToJSON(entry19),accessToken,false},
								{jsonHelper.getObjectToJSON(entry21),accessToken,false},
								{jsonHelper.getObjectToJSON(entry23),accessToken,false},
								{jsonHelper.getObjectToJSON(entry25),accessToken,false},
								{jsonHelper.getObjectToJSON(entry6),accessToken,false},
								{jsonHelper.getObjectToJSON(entry7),accessToken,false}, 
			                    {jsonHelper.getObjectToJSON(entry26),accessToken,false}
		};
	}
	
	@DataProvider(name="createTD5")
	public static Object[][] CreateTDData5() throws IOException
	{
		 JsonHelper jsonHelper = new JsonHelper();
		 ArrayList<Integer> restIds6=new ArrayList<>();
		 restIds6.add(FreeDelivery_restaurants);
		 ArrayList<Restaurant> restList6 =new ArrayList<>();
		 Restaurant r6=new Restaurant();
		 r6.setId(FreeDelivery_restaurants);
		 r6.setName("Myramzi");
		 r6.setCity(219);
		 r6.setArea("MyRamziArea");
		 r6.setRecommended(0);
		 r6.setCostForTwo(0);
		 r6.setDeliveryCharge(0);
		 r6.setMinimumOrder(0);
		 r6.setOpened(0);
		 r6.setDeliveryTime(0);
		 r6.setTmpClosed(0);
		 r6.setPostalCode(0);
		 r6.setCityDeliveryCharge(0);
		 r6.setThreshold(0);
		 r6.setDiscountType(0);
		 r6.setTradeCampaignHeaders(new ArrayList());
		 r6.setSelect("false");
		 r6.setNew("false");
		 r6.setCategories(new ArrayList());
		 restList6.add(r6);
		 RuleDiscount rd6=new RuleDiscount();
		 rd6.setType("FREE_DELIVERY");
		 rd6.setDiscountLevel("Restaurant");
		 rd6.setPercentDiscount(2);
		 rd6.setMinCartAmount(45);
		 rd6.setPercentDiscountAbsoluteCap(0);
		 Date Date6 = DateUtils.addDays(new Date(), 5);
	        
		 RMSCreateTDEntry entry6=new RMSTDBuilder().
					setEnabled("true").
					setRestaurantIds(restIds6).
					setValidFrom(Long.valueOf(DateUtils.addDays(new Date(), 5).toInstant().toEpochMilli())).
					setValidTill(Long.valueOf(DateUtils.addMinutes(Date6, 2).toInstant().toEpochMilli())).
					setCampaignType("FREE_DELIVERY").setRuleDiscountType("FREE_DELIVERY").
					setCreatedBy(9738948943l).setDiscountLevel("Restaurant").
					setRestaurantList(restList6).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
					setUserRestriction("false").setRestaurantFirstOrder("false").
					setRuleDiscount(rd6).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
					setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();

		 			
		 return new Object[][] {	{jsonHelper.getObjectToJSON(entry6),accessToken} };
		 
    }
	
	@DataProvider(name="createTD6")
	public static Object[][] CreateTD6() throws IOException
	{
		 JsonHelper jsonHelper = new JsonHelper();
		 ArrayList<Integer> restIds7=new ArrayList<>();
		 restIds7.add(FreeDelivery_restaurants);
		 ArrayList<Restaurant> restList7 =new ArrayList<>();
		 Restaurant r7=new Restaurant();
		 r7.setId(FreeDelivery_restaurants);
		 r7.setName("Myramzi");
		 r7.setCity(219);
		 r7.setArea("MyRamziArea");
		 r7.setRecommended(0);
		 r7.setCostForTwo(0);
		 r7.setDeliveryCharge(0);
		 r7.setMinimumOrder(0);
		 r7.setOpened(0);
		 r7.setDeliveryTime(0);
		 r7.setTmpClosed(0);
		 r7.setPostalCode(0);
		 r7.setCityDeliveryCharge(0);
		 r7.setThreshold(0);
		 r7.setDiscountType(0);
		 r7.setTradeCampaignHeaders(new ArrayList());
		 r7.setSelect("false");
		 r7.setNew("false");
		 r7.setCategories(new ArrayList());
		 restList7.add(r7);
		 RuleDiscount rd7=new RuleDiscount();
		 rd7.setType("FREE_DELIVERY");
		 rd7.setDiscountLevel("item");
		 rd7.setPercentDiscount(2);
		 rd7.setMinCartAmount(0);
		 rd7.setPercentDiscountAbsoluteCap(0);
		 Date Date7 = DateUtils.addDays(new Date(), 5);
	        
		 RMSCreateTDEntry entry7=new RMSTDBuilder().
					setEnabled("true").
					setRestaurantIds(restIds7).
					setValidFrom(Long.valueOf(DateUtils.addDays(new Date(), 5).toInstant().toEpochMilli())).
					setValidTill(Long.valueOf(DateUtils.addMinutes(Date7, 2).toInstant().toEpochMilli())).
					setCampaignType("FREE_DELIVERY").setRuleDiscountType("FREE_DELIVERY").
					setCreatedBy(9738948943l).setDiscountLevel("Restaurant").
					setRestaurantList(restList7).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
					setUserRestriction("false").setRestaurantFirstOrder("false").
					setRuleDiscount(rd7).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
					setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();
		 
		 return new Object[][] {{jsonHelper.getObjectToJSON(entry7),accessToken}};		
	 
    }

	
	@DataProvider(name="createTD1")
	public static Object[][] createTDData1() throws IOException
	{
	 JsonHelper jsonHelper = new JsonHelper();
	 ArrayList<Integer> restIds1=new ArrayList<>();
	 restIds1.add(Percentage_restaurants);
	 ArrayList<Restaurant> restList1 =new ArrayList<>();
	 Restaurant r1=new Restaurant();
	 r1.setId(Percentage_restaurants);
	 r1.setName("Myramzi");
	 r1.setCity(219);
	 r1.setArea("MyRamziArea");
	 r1.setRecommended(0);
	 r1.setCostForTwo(0);
	 r1.setDeliveryCharge(0);
	 r1.setMinimumOrder(0);
	 r1.setOpened(0);
	 r1.setDeliveryTime(0);
	 r1.setTmpClosed(0);
	 r1.setPostalCode(0);
	 r1.setCityDeliveryCharge(0);
	 r1.setThreshold(0);
	 r1.setDiscountType(0);
	 r1.setTradeCampaignHeaders(new ArrayList());
	 r1.setSelect("false");
	 r1.setNew("false");
	 r1.setCategories(new ArrayList());
	 restList1.add(r1);
	 RuleDiscount rd=new RuleDiscount();
	 rd.setType("Percentage");
	 rd.setDiscountLevel("Restaurant");
	 rd.setPercentDiscount(0);
	 rd.setMinCartAmount(0);
	 rd.setPercentDiscountAbsoluteCap(0);
	 
	 RMSCreateTDEntry entry1=new RMSTDBuilder().
				setEnabled("true").
				setRestaurantIds(restIds1).
				setValidFrom(Long.valueOf(DateUtils.addHours(new Date(), 0).toInstant().toEpochMilli())).
				setValidTill(Long.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().toEpochMilli())).
				setCampaignType("Percentage").setRuleDiscountType("Percentage").
				setCreatedBy(9886379321l).setDiscountLevel("Restaurant").
				setRestaurantList(restList1).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
				setUserRestriction("false").setRestaurantFirstOrder("false").
				setRuleDiscount(rd).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
				setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();
	 

	 ArrayList<Integer> restIds3=new ArrayList<>();
	 restIds3.add(Percentage_restaurants);
	 ArrayList<Restaurant> restList3 =new ArrayList<>();
	 Restaurant r3=new Restaurant();
	 r3.setId(Percentage_restaurants);
	 r3.setName("Myramzi");
	 r3.setCity(219);
	 r3.setArea("MyRamziArea");
	 r3.setRecommended(0);
	 r3.setCostForTwo(0);
	 r3.setDeliveryCharge(0);
	 r3.setMinimumOrder(0);
	 r3.setOpened(0);
	 r3.setDeliveryTime(0);
	 r3.setTmpClosed(0);
	 r3.setPostalCode(0);
	 r3.setCityDeliveryCharge(0);
	 r3.setThreshold(0);
	 r3.setDiscountType(0);
	 r3.setTradeCampaignHeaders(new ArrayList());
	 r3.setSelect("false");
	 r3.setNew("false");
	 r3.setCategories(new ArrayList());
	 restList3.add(r3);
	 RuleDiscount rd3=new RuleDiscount();
	 rd3.setType("Percentage");
	 rd3.setDiscountLevel("Restaurant");
	 rd3.setPercentDiscount(60);
	 rd3.setMinCartAmount(0);
	 rd3.setPercentDiscountAbsoluteCap(0);
	 
	 RMSCreateTDEntry entry3=new RMSTDBuilder().
				setEnabled("true").
				setRestaurantIds(restIds3).
				setValidFrom(Long.valueOf(DateUtils.addHours(new Date(), 10).toInstant().toEpochMilli())).
				setValidTill(Long.valueOf(DateUtils.addMinutes(new Date(), 15).toInstant().toEpochMilli())).
				setCampaignType("Percentage").setRuleDiscountType("Percentage").
				setCreatedBy(9886379321l).setDiscountLevel("Restaurant").
				setRestaurantList(restList3).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
				setUserRestriction("false").setRestaurantFirstOrder("false").
				setRuleDiscount(rd3).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
				setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();
	 
	 
	 ArrayList<Integer> restIds7=new ArrayList<>();
	 restIds7.add(Percentage_restaurants);
	 ArrayList<Restaurant> restList7 =new ArrayList<>();
	 Restaurant r7=new Restaurant();
	 r7.setId(Percentage_restaurants);
	 r7.setName("Myramzi");
	 r7.setCity(219);
	 r7.setArea("MyRamziArea");
	 r7.setRecommended(0);
	 r7.setCostForTwo(0);
	 r7.setDeliveryCharge(0);
	 r7.setMinimumOrder(0);
	 r7.setOpened(0);
	 r7.setDeliveryTime(0);
	 r7.setTmpClosed(0);
	 r7.setPostalCode(0);
	 r7.setCityDeliveryCharge(0);
	 r7.setThreshold(0);
	 r7.setDiscountType(0);
	 r7.setTradeCampaignHeaders(new ArrayList());
	 r7.setSelect("false");
	 r7.setNew("false");
	 r7.setCategories(new ArrayList());
	 restList7.add(r7);
	 RuleDiscount rd7=new RuleDiscount();
	 rd7.setType("Percentage");
	 rd7.setDiscountLevel("Restaurant");
	 rd7.setPercentDiscount(-2);
	 rd7.setMinCartAmount(0);
	 rd7.setPercentDiscountAbsoluteCap(0);
	 
	 RMSCreateTDEntry entry7=new RMSTDBuilder().
				setEnabled("true").
				setRestaurantIds(restIds7).
				setValidFrom(Long.valueOf(DateUtils.addHours(new Date(), 10).toInstant().toEpochMilli())).
				setValidTill(Long.valueOf(DateUtils.addMinutes(new Date(), 15).toInstant().toEpochMilli())).
				setCampaignType("Percentage").setRuleDiscountType("Percentage").
				setCreatedBy(9886379321l).setDiscountLevel("Restaurant").
				setRestaurantList(restList7).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
				setUserRestriction("false").setRestaurantFirstOrder("false").
				setRuleDiscount(rd3).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
				setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();
	 
	 
	 
		return new Object[][] {{jsonHelper.getObjectToJSON(entry1),accessToken},
			{jsonHelper.getObjectToJSON(entry3),accessToken},
			{jsonHelper.getObjectToJSON(entry7),accessToken}
		};
		
	}
		

	 
	 
	 @DataProvider(name="createTD2")
		public static Object[][] createTDData2() throws IOException
		{
			JsonHelper jsonHelper = new JsonHelper();
		
	 ArrayList<Integer> restIds2=new ArrayList<>();
	 restIds2.add(Flat_Restaurants);
	 ArrayList<Restaurant> restList2 =new ArrayList<>();
	 Restaurant r2=new Restaurant();
	 r2.setId(Flat_Restaurants);
	 r2.setName("Myramzi2");
	 r2.setCity(219);
	 r2.setArea("MyRamziArea2");
	 r2.setRecommended(0);
	 r2.setCostForTwo(0);
	 r2.setDeliveryCharge(0);
	 r2.setMinimumOrder(0);
	 r2.setOpened(0);
	 r2.setDeliveryTime(0);
	 r2.setTmpClosed(0);
	 r2.setPostalCode(0);
	 r2.setCityDeliveryCharge(0);
	 r2.setThreshold(0);
	 r2.setDiscountType(0);
	 r2.setTradeCampaignHeaders(new ArrayList());
	 r2.setSelect("false");
	 r2.setNew("false");
	 r2.setCategories(new ArrayList());
	 restList2.add(r2);
	 RuleDiscount rd2=new RuleDiscount();
	 rd2.setType("Flat");
	 rd2.setDiscountLevel("Restaurant");
	 rd2.setFlatDiscountAmount(0);
	 rd2.setMinCartAmount(0);
	 
	 RMSCreateTDEntry entry2=new RMSTDBuilder().
				setEnabled("true").
				setRestaurantIds(restIds2).
				setValidFrom(Long.valueOf(DateUtils.addHours(new Date(), 0).toInstant().toEpochMilli())).
				setValidTill(Long.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().toEpochMilli())).
				setCampaignType("Flat").setRuleDiscountType("Flat").
				setCreatedBy(9886379321l).setDiscountLevel("Restaurant").
				setRestaurantList(restList2).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
				setUserRestriction("false").setRestaurantFirstOrder("false").
				setRuleDiscount(rd2).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
				setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();

		 ArrayList<Integer> restIds4=new ArrayList<>();
		 restIds4.add(Flat_Restaurants);
		 ArrayList<Restaurant> restList4 =new ArrayList<>();
		 Restaurant r4=new Restaurant();
		 r4.setId(Flat_Restaurants);
		 r4.setName("Myramzi2");
		 r4.setCity(219);
		 r4.setArea("MyRamziArea2");
		 r4.setRecommended(0);
		 r4.setCostForTwo(0);
		 r4.setDeliveryCharge(0);
		 r4.setMinimumOrder(0);
		 r4.setOpened(0);
		 r4.setDeliveryTime(0);
		 r4.setTmpClosed(0);
		 r4.setPostalCode(0);
		 r4.setCityDeliveryCharge(0);
		 r4.setThreshold(0);
		 r4.setDiscountType(0);
		 r4.setTradeCampaignHeaders(new ArrayList());
		 r4.setSelect("false");
		 r4.setNew("false");
		 r4.setCategories(new ArrayList());
		 restList4.add(r4);
		 RuleDiscount rd4=new RuleDiscount();
		 rd4.setType("Flat");
		 rd4.setDiscountLevel("Restaurant");
		 rd4.setFlatDiscountAmount(500);
		 rd4.setMinCartAmount(100);
		  
		 RMSCreateTDEntry entry4=new RMSTDBuilder().
					setEnabled("true").
					setRestaurantIds(restIds4).
					setValidFrom(Long.valueOf(DateUtils.addHours(new Date(), 20).toInstant().toEpochMilli())).
					setValidTill(Long.valueOf(DateUtils.addMinutes(new Date(), 25).toInstant().toEpochMilli())).
					setCampaignType("Flat").setRuleDiscountType("Flat").
					setCreatedBy(9886379321l).setDiscountLevel("Restaurant").
					setRestaurantList(restList4).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
					setUserRestriction("false").setRestaurantFirstOrder("false").
					setRuleDiscount(rd4).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
					setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();

			return new Object[][] {
								{jsonHelper.getObjectToJSON(entry2),accessToken},
				                {jsonHelper.getObjectToJSON(entry4),accessToken}
			};
			
		}
		 
		 @DataProvider(name="createTD4")
			public static Object[][] createTDData4() throws IOException
			{
				JsonHelper jsonHelper = new JsonHelper();
		
		 
		 ArrayList<Integer> restIds6=new ArrayList<>();
		 restIds6.add(nonAccessable_restaurants);
		 ArrayList<Restaurant> restList6 =new ArrayList<>();
		 Restaurant r6=new Restaurant();
		 r6.setId(nonAccessable_restaurants);
		 r6.setName("Myramzi2");
		 r6.setCity(219);
		 r6.setArea("MyRamziArea2");
		 r6.setRecommended(0);
		 r6.setCostForTwo(0);
		 r6.setDeliveryCharge(0);
		 r6.setMinimumOrder(0);
		 r6.setOpened(0);
		 r6.setDeliveryTime(0);
		 r6.setTmpClosed(0);
		 r6.setPostalCode(0);
		 r6.setCityDeliveryCharge(0);
		 r6.setThreshold(0);
		 r6.setDiscountType(0);
		 r6.setTradeCampaignHeaders(new ArrayList());
		 r6.setSelect("false");
		 r6.setNew("false");
		 r6.setCategories(new ArrayList());
		 restList6.add(r6);
		 RuleDiscount rd6=new RuleDiscount();
		 rd6.setType("Flat");
		 rd6.setDiscountLevel("Restaurant");
		 rd6.setFlatDiscountAmount(-30);
		 rd6.setMinCartAmount(1000);
		 
		 
		 RMSCreateTDEntry entry6=new RMSTDBuilder().
					setEnabled("true").
					setRestaurantIds(restIds6).
					setValidFrom(Long.valueOf(DateUtils.addHours(new Date(), 40).toInstant().toEpochMilli())).
					setValidTill(Long.valueOf(DateUtils.addMinutes(new Date(), 45).toInstant().toEpochMilli())).
					setCampaignType("Flat").setRuleDiscountType("Flat").
					setCreatedBy(9886379321l).setDiscountLevel("Restaurant").
					setRestaurantList(restList6).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
					setUserRestriction("false").setRestaurantFirstOrder("false").
					setRuleDiscount(rd6).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
					setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();

		 
			return new Object[][] {
				{jsonHelper.getObjectToJSON(entry6),accessToken}
			}; 
		}

		 
		 
		 @DataProvider(name="createTD3")
			public static Object[][] createTDData3() throws IOException
			{
				JsonHelper jsonHelper = new JsonHelper();			
		 
		 ArrayList<Integer> restIds5=new ArrayList<>();
		 restIds5.add(Percentage_restaurants);
		 ArrayList<Restaurant> restList5 =new ArrayList<>();
		 Restaurant r5=new Restaurant();
		 r5.setId(Percentage_restaurants);
		 r5.setName("Myramzi");
		 r5.setCity(219);
		 r5.setArea("MyRamziArea");
		 r5.setRecommended(0);
		 r5.setCostForTwo(0);
		 r5.setDeliveryCharge(0);
		 r5.setMinimumOrder(0);
		 r5.setOpened(0);
		 r5.setDeliveryTime(0);
		 r5.setTmpClosed(0);
		 r5.setPostalCode(0);
		 r5.setCityDeliveryCharge(0);
		 r5.setThreshold(0);
		 r5.setDiscountType(0);
		 r5.setTradeCampaignHeaders(new ArrayList());
		 r5.setSelect("false");
		 r5.setNew("false");
		 r5.setCategories(new ArrayList());
		 restList5.add(r5);
		 RuleDiscount rd5=new RuleDiscount();
		 rd5.setType("Percentage");
		 rd5.setDiscountLevel("Restaurant");
		 rd5.setPercentDiscount(2);
		 rd5.setMinCartAmount(-100);
		 rd5.setPercentDiscountAbsoluteCap(0);
		 
		 RMSCreateTDEntry entry5=new RMSTDBuilder().
					setEnabled("true").
					setRestaurantIds(restIds5).
					setValidFrom(Long.valueOf(DateUtils.addHours(new Date(), 30).toInstant().toEpochMilli())).
					setValidTill(Long.valueOf(DateUtils.addMinutes(new Date(), 35).toInstant().toEpochMilli())).
					setCampaignType("Percentage").setRuleDiscountType("Percentage").
					setCreatedBy(9886379321l).setDiscountLevel("Restaurant").
					setRestaurantList(restList5).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
					setUserRestriction("false").setRestaurantFirstOrder("false").
					setRuleDiscount(rd5).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
					setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();
	
	 
		return new Object[][] {
			{jsonHelper.getObjectToJSON(entry5),accessToken}
		};
	}
	
	
	@DataProvider(name="editTD")
	public static Object[][] editTDData() throws IOException
	{
	 JsonHelper jsonHelper = new JsonHelper();
	
	 ArrayList<Integer> restIds1=new ArrayList<>();
	 restIds1.add(Percentage_restaurants);
	 ArrayList<Restaurant> restList1 =new ArrayList<>();
	 Restaurant r1=new Restaurant();
	 r1.setId(Percentage_restaurants);
	 r1.setName("Myramzi");
	 r1.setCity(219);
	 r1.setArea("MyRamziArea");
	 r1.setRecommended(0);
	 r1.setCostForTwo(0);
	 r1.setDeliveryCharge(0);
	 r1.setMinimumOrder(0);
	 r1.setOpened(0);
	 r1.setDeliveryTime(0);
	 r1.setTmpClosed(0);
	 r1.setPostalCode(0);
	 r1.setCityDeliveryCharge(0);
	 r1.setThreshold(0);
	 r1.setDiscountType(0);
	 r1.setTradeCampaignHeaders(new ArrayList());
	 r1.setSelect("false");
	 r1.setNew("false");
	 r1.setCategories(new ArrayList());
	 restList1.add(r1);
	 RuleDiscount rd=new RuleDiscount();
	 rd.setType("Percentage");
	 rd.setDiscountLevel("Restaurant");
	 rd.setPercentDiscount(2);
	 rd.setMinCartAmount(0);
	 rd.setPercentDiscountAbsoluteCap(0);
	 
	 RMSCreateTDEntry entry1=new RMSTDBuilder().
				setEnabled("true").
				setRestaurantIds(restIds1).
				setValidFrom(Long.valueOf(DateUtils.addHours(new Date(), 0).toInstant().toEpochMilli())).
				setValidTill(Long.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().toEpochMilli())).
				setCampaignType("Percentage").setRuleDiscountType("Percentage").
				setUpdatedBy(9886379321l).setDiscountLevel("Restaurant").
				setRestaurantList(restList1).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
				setUserRestriction("false").setRestaurantFirstOrder("false").
				setRuleDiscount(rd).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
				setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();
	 RMSTDHelper rmsTDHelper = new RMSTDHelper();
	 int count =0;
	 int discountId = -1;
	 do{
	 Processor p = rmsTDHelper.createTD(jsonHelper.getObjectToJSON(entry1).toString(), accessToken);
	      try {
	    	  if (p.ResponseValidator.GetNodeValueAsInt("statusCode") !=0) {
	  			String[] res = p.ResponseValidator.GetNodeValue("data.dataRest.data").split(" ");
	  			System.out.println("IDDDD" +res[6]);
	  			Processor p2 =RngHelper.disableEnableTDFromMarketingDashboard(res[6], "false");
	  			//System.out.println("RND Response" +p.ResponseValidator.GetNodeValue("data.dataRest.data"));
	  		} else {
	  			System.out.println(p.ResponseValidator.GetNodeValueAsInt("data.dataRest.data"));
	  		 discountId = p.ResponseValidator.GetNodeValueAsInt("data.dataRest.data");
	  			break;
	  		}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	} while (count++ < 5);
		
	 ArrayList<Integer> restIds2=new ArrayList<>();
	 
	 restIds2.add(Percentage_restaurants);
	 
	 ArrayList<Restaurant> restList2 =new ArrayList<>();
	 Restaurant r2=new Restaurant();
	 
	 r2.setId(Percentage_restaurants);
	 r2.setName("Myramzi");
	 r2.setCity(219);
	 r2.setArea("MyRamziArea");
	 r2.setRecommended(0);
	 r2.setCostForTwo(0);
	 r2.setDeliveryCharge(0);
	 r2.setMinimumOrder(0);
	 r2.setOpened(0);
	 r2.setDeliveryTime(0);
	 r2.setTmpClosed(0);
	 r2.setPostalCode(0);
	 r2.setCityDeliveryCharge(0);
	 r2.setThreshold(0);
	 r2.setDiscountType(0);
	 r2.setTradeCampaignHeaders(new ArrayList());
	 r2.setSelect("false");
	 r2.setNew("false");
	 r2.setCategories(new ArrayList());
	 restList2.add(r2);
	 RuleDiscount rd2=new RuleDiscount();
	 rd2.setType("Percentage");
	 rd2.setDiscountLevel("Restaurant");
	 rd2.setPercentDiscount(4);
	 rd2.setMinCartAmount(0);
	 rd2.setPercentDiscountAbsoluteCap(0);
	 
	 RMSUpdateTDEntry entry2=new RMSEditTDBuilder().
				setEnabled("true").
				setRestaurantIds(restIds2).setId(discountId).
				//setId(12346).	
				setValidFrom(Long.valueOf(DateUtils.addHours(new Date(), 0).toInstant().toEpochMilli())).
				setValidTill(Long.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().toEpochMilli())).
				setCampaignType("Percentage").setRuleDiscountType("Percentage").
				setUpdatedBy(9886379321l).setDiscountLevel("Restaurant").
				setRestaurantList(restList2).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
				setUserRestriction("false").setRestaurantFirstOrder("false").
				setRuleDiscount(rd2).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
				setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();

	 ArrayList<Integer> restIds3=new ArrayList<>();
	 restIds3.add(Percentage_restaurants);
	 restIds3.add(Flat_Restaurants);
	 ArrayList<Restaurant> restList3 =new ArrayList<>();
	 Restaurant r3=new Restaurant();
	 Restaurant r4=new Restaurant();
	 
	 r3.setId(Percentage_restaurants);
	 r3.setName("Myramzi");
	 r3.setCity(219);
	 r3.setArea("MyRamziArea");
	 r3.setRecommended(0);
	 r3.setCostForTwo(0);
	 r3.setDeliveryCharge(0);
	 r3.setMinimumOrder(0);
	 r3.setOpened(0);
	 r3.setDeliveryTime(0);
	 r3.setTmpClosed(0);
	 r3.setPostalCode(0);
	 r3.setCityDeliveryCharge(0);
	 r3.setThreshold(0);
	 r3.setDiscountType(0);
	 r3.setTradeCampaignHeaders(new ArrayList());
	 r3.setSelect("false");
	 r3.setNew("false");
	 r3.setCategories(new ArrayList());
	 restList3.add(r3);

	 r4.setId(Flat_Restaurants);
	 r4.setName("Myramzi");
	 r4.setCity(219);
	 r4.setArea("MyRamziArea");
	 r4.setRecommended(0);
	 r4.setCostForTwo(0);
	 r4.setDeliveryCharge(0);
	 r4.setMinimumOrder(0);
	 r4.setOpened(0);
	 r4.setDeliveryTime(0);
	 r4.setTmpClosed(0);
	 r4.setPostalCode(0);
	 r4.setCityDeliveryCharge(0);
	 r4.setThreshold(0);
	 r4.setDiscountType(0);
	 r4.setTradeCampaignHeaders(new ArrayList());
	 r4.setSelect("false");
	 r4.setNew("false");
	 r4.setCategories(new ArrayList<>());
	 restList3.add(r4);
	 
	 RuleDiscount rd3=new RuleDiscount();
	 rd3.setType("Percentage");
	 rd3.setDiscountLevel("Restaurant");
	 rd3.setPercentDiscount(2);
	 rd3.setMinCartAmount(0);
	 rd3.setPercentDiscountAbsoluteCap(0);
	 
	 RMSCreateTDEntry entry3=new RMSTDBuilder().
				setEnabled("true").
				setRestaurantIds(restIds3).
				setValidFrom(Long.valueOf(DateUtils.addDays(new Date(), 2).toInstant().toEpochMilli())).
				setValidTill(Long.valueOf(DateUtils.addDays(new Date(), 3).toInstant().toEpochMilli())).
				setCampaignType("Percentage").setRuleDiscountType("Percentage").
				setUpdatedBy(9886379321l).setDiscountLevel("Restaurant").
				setRestaurantList(restList3).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
				setUserRestriction("false").setRestaurantFirstOrder("false").
				setRuleDiscount(rd3).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
				setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();
	 RMSTDHelper rmsTDHelper1 = new RMSTDHelper();
	 int count1 =0;
	 int discountId1 = -1;
	 do{
	 Processor p = rmsTDHelper1.createTD(jsonHelper.getObjectToJSON(entry3).toString(), accessToken);
		if (p.ResponseValidator.GetNodeValueAsInt("statusCode") == -5) {
			String[] res = p.ResponseValidator.GetNodeValue("data.dataRest.data").split(" ");
			System.out.println("IDDDD" +res[6]);
			Processor p2 =RngHelper.disableEnableTDFromMarketingDashboard(res[5], "false");
			
		} else {
			System.out.println(p.ResponseValidator.GetNodeValueAsInt("data.dataRest.data"));
		 discountId1 = p.ResponseValidator.GetNodeValueAsInt("data.dataRest.data");
			break;
		}
	} while (count1++ < 5);
		
	
	 ArrayList<Integer> restIds5=new ArrayList<>();
	 restIds5.add(Percentage_restaurants);
	 ArrayList<Restaurant> restList5 =new ArrayList<>();
	 Restaurant r5=new Restaurant();
	 r5.setId(Percentage_restaurants);
	 r5.setName("Myramzi");
	 r5.setCity(219);
	 r5.setArea("MyRamziArea");
	 r5.setRecommended(0);
	 r5.setCostForTwo(0);
	 r5.setDeliveryCharge(0);
	 r5.setMinimumOrder(0);
	 r5.setOpened(0);
	 r5.setDeliveryTime(0);
	 r5.setTmpClosed(0);
	 r5.setPostalCode(0);
	 r5.setCityDeliveryCharge(0);
	 r5.setThreshold(0);
	 r5.setDiscountType(0);
	 r5.setTradeCampaignHeaders(new ArrayList());
	 r5.setSelect("false");
	 r5.setNew("false");
	 r5.setCategories(new ArrayList());
	 restList5.add(r5);
	 RuleDiscount rd5=new RuleDiscount();
	 rd5.setType("Percentage");
	 rd5.setDiscountLevel("Restaurant");
	 rd5.setPercentDiscount(2);
	 rd5.setMinCartAmount(0);
	 rd5.setPercentDiscountAbsoluteCap(0);
	 
	 RMSUpdateTDEntry entry5=new RMSEditTDBuilder().
				setEnabled("true").
				setRestaurantIds(restIds5).setId(discountId1).
				//setId(12346).	
				setValidFrom(Long.valueOf(DateUtils.addDays(new Date(), 2).toInstant().toEpochMilli())).
				setValidTill(Long.valueOf(DateUtils.addDays(new Date(), 3).toInstant().toEpochMilli())).
				setCampaignType("Percentage").setRuleDiscountType("Percentage").
				setUpdatedBy(9886379321l).setDiscountLevel("Restaurant").
				setRestaurantList(restList5).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
				setUserRestriction("false").setRestaurantFirstOrder("false").
				setRuleDiscount(rd5).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
				setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();

		
		 ArrayList<Integer> restIds6=new ArrayList<>();
		 restIds6.add(Percentage_restaurants);
		 ArrayList<Restaurant> restList6 =new ArrayList<>();
		 Restaurant r6=new Restaurant();
		 r6.setId(Percentage_restaurants);
		 r6.setName("Myramzi");
		 r6.setCity(219);
		 r6.setArea("MyRamziArea");
		 r6.setRecommended(0);
		 r6.setCostForTwo(0);
		 r6.setDeliveryCharge(0);
		 r6.setMinimumOrder(0);
		 r6.setOpened(0);
		 r6.setDeliveryTime(0);
		 r6.setTmpClosed(0);
		 r6.setPostalCode(0);
		 r6.setCityDeliveryCharge(0);
		 r6.setThreshold(0);
		 r6.setDiscountType(0);
		 r6.setTradeCampaignHeaders(new ArrayList());
		 r6.setSelect("false");
		 r6.setNew("false");
		 r6.setCategories(new ArrayList());
		 restList6.add(r6);
		 RuleDiscount rd6=new RuleDiscount();
		 rd6.setType("Percentage");
		 rd6.setDiscountLevel("Restaurant");
		 rd6.setPercentDiscount(2);
		 rd6.setMinCartAmount(0);
		 rd6.setPercentDiscountAbsoluteCap(0);
		 
		 RMSCreateTDEntry entry6=new RMSTDBuilder().
					setEnabled("true").
					setRestaurantIds(restIds6).
					setValidFrom(Long.valueOf(DateUtils.addHours(new Date(), 2).toInstant().toEpochMilli())).
					setValidTill(Long.valueOf(DateUtils.addHours(new Date(), 3).toInstant().toEpochMilli())).
					setCampaignType("Percentage").setRuleDiscountType("Percentage").
					setUpdatedBy(9886379321l).setDiscountLevel("Restaurant").
					setRestaurantList(restList6).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
					setUserRestriction("false").setRestaurantFirstOrder("false").
					setRuleDiscount(rd6).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
					setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();
		 RMSTDHelper rmsTDHelper6 = new RMSTDHelper();
		 int count6 =0;
		 int discountId6 = -1;
		 do{
		 Processor p = rmsTDHelper6.createTD(jsonHelper.getObjectToJSON(entry1).toString(), accessToken);
			if (p.ResponseValidator.GetNodeValueAsInt("statusCode") == -5) {
				String[] res = p.ResponseValidator.GetNodeValue("data.dataRest.data").split(" ");
				System.out.println("IDDDD" +res[6]);
				Processor p2 =RngHelper.disableEnableTDFromMarketingDashboard(res[6], "false");
			} else {
				System.out.println(p.ResponseValidator.GetNodeValueAsInt("data.dataRest.data"));
			 discountId6 = p.ResponseValidator.GetNodeValueAsInt("data.dataRest.data");
				break;
			}
		} while (count6++ < 5);

	 ArrayList<Integer> restIds7=new ArrayList<>();
	 restIds7.add(Percentage_restaurants);
	 ArrayList<Restaurant> restList7 =new ArrayList<>();
	 Restaurant r7=new Restaurant();
	 r7.setId(Percentage_restaurants);
	 r7.setName("Myramzi");
	 r7.setCity(219);
	 r7.setArea("MyRamziArea");
	 r7.setRecommended(0);
	 r7.setCostForTwo(0);
	 r7.setDeliveryCharge(0);
	 r7.setMinimumOrder(0);
	 r7.setOpened(0);
	 r7.setDeliveryTime(0);
	 r7.setTmpClosed(0);
	 r7.setPostalCode(0);
	 r7.setCityDeliveryCharge(0);
	 r7.setThreshold(0);
	 r7.setDiscountType(0);
	 r7.setTradeCampaignHeaders(new ArrayList());
	 r7.setSelect("false");
	 r7.setNew("false");
	 r7.setCategories(new ArrayList());
	 restList7.add(r7);
	 RuleDiscount rd7=new RuleDiscount();
	 rd7.setType("Percentage");
	 rd7.setDiscountLevel("Restaurant");
	 rd7.setPercentDiscount(2);
	 rd7.setMinCartAmount(0);
	 rd7.setPercentDiscountAbsoluteCap(0);
	 
	 RMSUpdateTDEntry entry7=new RMSEditTDBuilder().
				setEnabled("true").
				setRestaurantIds(restIds7).setId(discountId6).
				//setId(12346).	
				setValidFrom(Long.valueOf(DateUtils.addHours(new Date(), 2).toInstant().toEpochMilli())).
				setValidTill(Long.valueOf(DateUtils.addHours(new Date(), 6).toInstant().toEpochMilli())).
				setCampaignType("Percentage").setRuleDiscountType("Percentage").
				setUpdatedBy(9886379321l).setDiscountLevel("Restaurant").
				setRestaurantList(restList7).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
				setUserRestriction("false").setRestaurantFirstOrder("false").
				setRuleDiscount(rd7).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
				setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();
		
		 ArrayList<Integer> restIds8=new ArrayList<>();
		 restIds8.add(Percentage_restaurants);
		 ArrayList<Restaurant> restList8 =new ArrayList<>();
		 Restaurant r8=new Restaurant();
		 r8.setId(Percentage_restaurants);
		 r8.setName("Myramzi");
		 r8.setCity(219);
		 r8.setArea("MyRamziArea");
		 r8.setRecommended(0);
		 r8.setCostForTwo(0);
		 r8.setDeliveryCharge(0);
		 r8.setMinimumOrder(0);
		 r8.setOpened(0);
		 r8.setDeliveryTime(0);
		 r8.setTmpClosed(0);
		 r8.setPostalCode(0);
		 r8.setCityDeliveryCharge(0);
		 r8.setThreshold(0);
		 r8.setDiscountType(0);
		 r8.setTradeCampaignHeaders(new ArrayList());
		 r8.setSelect("false");
		 r8.setNew("false");
		 r8.setCategories(new ArrayList());
		 restList8.add(r8);
		 RuleDiscount rd8=new RuleDiscount();
		 rd8.setType("Percentage");
		 rd8.setDiscountLevel("Restaurant");
		 rd8.setPercentDiscount(2);
		 rd8.setMinCartAmount(0);
		 rd8.setPercentDiscountAbsoluteCap(0);
		 
		 RMSCreateTDEntry entry8=new RMSTDBuilder().
					setEnabled("true").
					setRestaurantIds(restIds8).
					setValidFrom(Long.valueOf(DateUtils.addHours(new Date(), 0).toInstant().toEpochMilli())).
					setValidTill(Long.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().toEpochMilli())).
					setCampaignType("Percentage").setRuleDiscountType("Percentage").
					setUpdatedBy(9886379321l).setDiscountLevel("Restaurant").
					setRestaurantList(restList8).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
					setUserRestriction("false").setRestaurantFirstOrder("false").
					setRuleDiscount(rd8).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
					setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();
		 RMSTDHelper rmsTDHelper8 = new RMSTDHelper();
		 int count8 =0;
		 int discountId8 = -1;
		 do{
		 Processor p = rmsTDHelper8.createTD(jsonHelper.getObjectToJSON(entry1).toString(), accessToken);
			if (p.ResponseValidator.GetNodeValueAsInt("statusCode") == -5) {
				String[] res = p.ResponseValidator.GetNodeValue("data.dataRest.data").split(" ");
				System.out.println("IDDDD" +res[6]);
				Processor p2 =RngHelper.disableEnableTDFromMarketingDashboard(res[6], "false");
			} else {
				System.out.println(p.ResponseValidator.GetNodeValueAsInt("data.dataRest.data"));
			 discountId8 = p.ResponseValidator.GetNodeValueAsInt("data.dataRest.data");
				break;
			}
		} while (count8++ < 5);

	 ArrayList<Integer> restIds9=new ArrayList<>();
	 restIds9.add(Flat_Restaurants);
	 ArrayList<Restaurant> restList9 =new ArrayList<>();
	 Restaurant r9=new Restaurant();
	 r9.setId(Flat_Restaurants);
	 r9.setName("Myramzi");
	 r9.setCity(219);
	 r9.setArea("MyRamziArea");
	 r9.setRecommended(0);
	 r9.setCostForTwo(0);
	 r9.setDeliveryCharge(0);
	 r9.setMinimumOrder(0);
	 r9.setOpened(0);
	 r9.setDeliveryTime(0);
	 r9.setTmpClosed(0);
	 r9.setPostalCode(0);
	 r9.setCityDeliveryCharge(0);
	 r9.setThreshold(0);
	 r9.setDiscountType(0);
	 r9.setTradeCampaignHeaders(new ArrayList());
	 r9.setSelect("false");
	 r9.setNew("false");
	 r9.setCategories(new ArrayList());
	 restList9.add(r9);
	 RuleDiscount rd9=new RuleDiscount();
	 rd9.setType("Percentage");
	 rd9.setDiscountLevel("Restaurant");
	 rd9.setPercentDiscount(2);
	 rd9.setMinCartAmount(0);
	 rd9.setPercentDiscountAbsoluteCap(0);
	 
	 RMSUpdateTDEntry entry9=new RMSEditTDBuilder().
				setEnabled("true").
				setRestaurantIds(restIds9).setId(discountId8).
				setValidFrom(Long.valueOf(DateUtils.addDays(new Date(), 2).toInstant().toEpochMilli())).
				setValidTill(Long.valueOf(DateUtils.addDays(new Date(), 4).toInstant().toEpochMilli())).
				setCampaignType("Percentage").setRuleDiscountType("Percentage").
				setUpdatedBy(9886379321l).setDiscountLevel("Restaurant").
				setRestaurantList(restList9).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
				setUserRestriction("false").setRestaurantFirstOrder("false").
				setRuleDiscount(rd9).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
				setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();

		
		 ArrayList<Integer> restIds10=new ArrayList<>();
		 restIds10.add(Percentage_restaurants);
		 ArrayList<Restaurant> restList10 =new ArrayList<>();
		 Restaurant r10=new Restaurant();
		 r10.setId(Percentage_restaurants);
		 r10.setName("Myramzi");
		 r10.setCity(219);
		 r10.setArea("MyRamziArea");
		 r10.setRecommended(0);
		 r10.setCostForTwo(0);
		 r10.setDeliveryCharge(0);
		 r10.setMinimumOrder(0);
		 r10.setOpened(0);
		 r10.setDeliveryTime(0);
		 r10.setTmpClosed(0);
		 r10.setPostalCode(0);
		 r10.setCityDeliveryCharge(0);
		 r10.setThreshold(0);
		 r10.setDiscountType(0);
		 r10.setTradeCampaignHeaders(new ArrayList());
		 r10.setSelect("false");
		 r10.setNew("false");
		 r10.setCategories(new ArrayList());
		 restList10.add(r10);
		 RuleDiscount rd10=new RuleDiscount();
		 rd10.setType("Percentage");
		 rd10.setDiscountLevel("Restaurant");
		 rd10.setPercentDiscount(2);
		 rd10.setMinCartAmount(0);
		 rd10.setPercentDiscountAbsoluteCap(0);
		 
		 RMSCreateTDEntry entry10=new RMSTDBuilder().
					setEnabled("true").
					setRestaurantIds(restIds10).
					setValidFrom(Long.valueOf(DateUtils.addHours(new Date(), 0).toInstant().toEpochMilli())).
					setValidTill(Long.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().toEpochMilli())).
					setCampaignType("Percentage").setRuleDiscountType("Percentage").
					setUpdatedBy(9886379321l).setDiscountLevel("Restaurant").
					setRestaurantList(restList10).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
					setUserRestriction("false").setRestaurantFirstOrder("false").
					setRuleDiscount(rd10).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
					setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();
		 RMSTDHelper rmsTDHelper10 = new RMSTDHelper();
		 int count10 =0;
		 int discountId10 = -1;
		 do{
		 Processor p = rmsTDHelper10.createTD(jsonHelper.getObjectToJSON(entry1).toString(), accessToken);
			if (p.ResponseValidator.GetNodeValueAsInt("statusCode") == -5) {
				String[] res = p.ResponseValidator.GetNodeValue("data.dataRest.data").split(" ");
				System.out.println("IDDDD" +res[6]);
				Processor p2 =RngHelper.disableEnableTDFromMarketingDashboard(res[6], "false");
			} else {
				System.out.println(p.ResponseValidator.GetNodeValueAsInt("data.dataRest.data"));
			 discountId10 = p.ResponseValidator.GetNodeValueAsInt("data.dataRest.data");
				break;
			}
		} while (count10++ < 5);

	 ArrayList<Integer> restIds11=new ArrayList<>();
	 restIds11.add(Flat_Restaurants);
	 ArrayList<Restaurant> restList11 =new ArrayList<>();
	 Restaurant r11=new Restaurant();
	 r11.setId(Flat_Restaurants);
	 r11.setName("Myramzi");
	 r11.setCity(219);
	 r11.setArea("MyRamziArea");
	 r11.setRecommended(0);
	 r11.setCostForTwo(0);
	 r11.setDeliveryCharge(0);
	 r11.setMinimumOrder(0);
	 r11.setOpened(0);
	 r11.setDeliveryTime(0);
	 r11.setTmpClosed(0);
	 r11.setPostalCode(0);
	 r11.setCityDeliveryCharge(0);
	 r11.setThreshold(0);
	 r11.setDiscountType(0);
	 r11.setTradeCampaignHeaders(new ArrayList());
	 r11.setSelect("false");
	 r11.setNew("false");
	 r11.setCategories(new ArrayList());
	 restList11.add(r11);
	 RuleDiscount rd11=new RuleDiscount();
	 rd11.setType("Flat");
	 rd11.setDiscountLevel("Restaurant");
	 rd11.setFlatDiscountAmount(10);
	 rd11.setMinCartAmount(0);
	 rd11.setPercentDiscountAbsoluteCap(0);

	 RMSUpdateTDEntry entry11=new RMSEditTDBuilder().
				setEnabled("true").
				setRestaurantIds(restIds11).setId(discountId10).
				//setId(12346).	
				setValidFrom(Long.valueOf(DateUtils.addHours(new Date(), 0).toInstant().toEpochMilli())).
				setValidTill(Long.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().toEpochMilli())).
				setCampaignType("Flat").setRuleDiscountType("Flat").
				setUpdatedBy(9886379321l).setDiscountLevel("Restaurant").
				setRestaurantList(restList11).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
				setUserRestriction("false").setRestaurantFirstOrder("false").
				setRuleDiscount(rd11).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
				setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();

		
		 ArrayList<Integer> restIds12=new ArrayList<>();
		 restIds12.add(Percentage_restaurants);
		 ArrayList<Restaurant> restList12 =new ArrayList<>();
		 Restaurant r12=new Restaurant();
		 r12.setId(Percentage_restaurants);
		 r12.setName("Myramzi");
		 r12.setCity(219);
		 r12.setArea("MyRamziArea");
		 r12.setRecommended(0);
		 r12.setCostForTwo(0);
		 r12.setDeliveryCharge(0);
		 r12.setMinimumOrder(0);
		 r12.setOpened(0);
		 r12.setDeliveryTime(0);
		 r12.setTmpClosed(0);
		 r12.setPostalCode(0);
		 r12.setCityDeliveryCharge(0);
		 r12.setThreshold(0);
		 r12.setDiscountType(0);
		 r12.setTradeCampaignHeaders(new ArrayList());
		 r12.setSelect("false");
		 r12.setNew("false");
		 r12.setCategories(new ArrayList());
		 restList12.add(r12);
		 RuleDiscount rd12=new RuleDiscount();
		 rd12.setType("Percentage");
		 rd12.setDiscountLevel("Restaurant");
		 rd12.setPercentDiscount(2);
		 rd12.setMinCartAmount(0);
		 rd12.setPercentDiscountAbsoluteCap(0);
		 
		 RMSCreateTDEntry entry12=new RMSTDBuilder().
					setEnabled("true").
					setRestaurantIds(restIds12).
					setValidFrom(Long.valueOf(DateUtils.addHours(new Date(), 0).toInstant().toEpochMilli())).
					setValidTill(Long.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().toEpochMilli())).
					setCampaignType("Percentage").setRuleDiscountType("Percentage").
					setUpdatedBy(9886379321l).setDiscountLevel("Restaurant").
					setRestaurantList(restList12).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
					setUserRestriction("false").setRestaurantFirstOrder("false").
					setRuleDiscount(rd12).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
					setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();
		 RMSTDHelper rmsTDHelper12 = new RMSTDHelper();
		 int count12 =0;
		 int discountId12 = -1;
		 do{
		 Processor p = rmsTDHelper12.createTD(jsonHelper.getObjectToJSON(entry1).toString(), accessToken);
			if (p.ResponseValidator.GetNodeValueAsInt("statusCode") == -5) {
				String[] res = p.ResponseValidator.GetNodeValue("data.dataRest.data").split(" ");
				System.out.println("IDDDD" +res[6]);
				Processor p2 =RngHelper.disableEnableTDFromMarketingDashboard(res[6], "false");
			} else {
				System.out.println(p.ResponseValidator.GetNodeValueAsInt("data.dataRest.data"));
			 discountId12 = p.ResponseValidator.GetNodeValueAsInt("data.dataRest.data");
				break;
			}
		} while (count12++ < 5);
			
		 ArrayList<Integer> restIds13=new ArrayList<>();
		 
		 restIds12.add(Percentage_restaurants);
		 
		 ArrayList<Restaurant> restList13 =new ArrayList<>();
		 Restaurant r13=new Restaurant();
		 
		 r13.setId(Percentage_restaurants);
		 r13.setName("Myramzi");
		 r13.setCity(219);
		 r13.setArea("MyRamziArea");
		 r13.setRecommended(0);
		 r13.setCostForTwo(0);
		 r13.setDeliveryCharge(0);
		 r13.setMinimumOrder(0);
		 r13.setOpened(0);
		 r13.setDeliveryTime(0);
		 r13.setTmpClosed(0);
		 r13.setPostalCode(0);
		 r13.setCityDeliveryCharge(0);
		 r13.setThreshold(0);
		 r13.setDiscountType(0);
		 r13.setTradeCampaignHeaders(new ArrayList());
		 r13.setSelect("false");
		 r13.setNew("false");
		 r13.setCategories(new ArrayList());
		 restList13.add(r13);
		 RuleDiscount rd13=new RuleDiscount();
		 rd13.setType("Percentage");
		 rd13.setDiscountLevel("Restaurant");
		 rd13.setPercentDiscount(2);
		 rd13.setMinCartAmount(100);
		 rd13.setPercentDiscountAbsoluteCap(0);
		 
		 RMSUpdateTDEntry entry13=new RMSEditTDBuilder().
					setEnabled("true").
					setRestaurantIds(restIds13).setId(discountId12).
					setValidFrom(Long.valueOf(DateUtils.addHours(new Date(), 0).toInstant().toEpochMilli())).
					setValidTill(Long.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().toEpochMilli())).
					setCampaignType("Percentage").setRuleDiscountType("Percentage").
					setUpdatedBy(9886379321l).setDiscountLevel("Restaurant").
					setRestaurantList(restList13).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
					setUserRestriction("false").setRestaurantFirstOrder("false").
					setRuleDiscount(rd13).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
					setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();

		
		 ArrayList<Integer> restIds14=new ArrayList<>();
		 restIds14.add(Percentage_restaurants);
		 ArrayList<Restaurant> restList14 =new ArrayList<>();
		 Restaurant r14=new Restaurant();
		 r14.setId(Percentage_restaurants);
		 r14.setName("Myramzi");
		 r14.setCity(219);
		 r14.setArea("MyRamziArea");
		 r14.setRecommended(0);
		 r14.setCostForTwo(0);
		 r14.setDeliveryCharge(0);
		 r14.setMinimumOrder(0);
		 r14.setOpened(0);
		 r14.setDeliveryTime(0);
		 r14.setTmpClosed(0);
		 r14.setPostalCode(0);
		 r14.setCityDeliveryCharge(0);
		 r14.setThreshold(0);
		 r14.setDiscountType(0);
		 r14.setTradeCampaignHeaders(new ArrayList());
		 r14.setSelect("false");
		 r14.setNew("false");
		 r14.setCategories(new ArrayList());
		 restList14.add(r14);
		 RuleDiscount rd14=new RuleDiscount();
		 rd14.setType("Percentage");
		 rd14.setDiscountLevel("Restaurant");
		 rd14.setPercentDiscount(3);
		 rd14.setMinCartAmount(0);
		 rd14.setPercentDiscountAbsoluteCap(0);
		 
		 RMSCreateTDEntry entry14=new RMSTDBuilder().
					setEnabled("true").
					setRestaurantIds(restIds14).
					setValidFrom(Long.valueOf(DateUtils.addHours(new Date(), 0).toInstant().toEpochMilli())).
					setValidTill(Long.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().toEpochMilli())).
					setCampaignType("Percentage").setRuleDiscountType("Percentage").
					setUpdatedBy(9886379321l).setDiscountLevel("Restaurant").
					setRestaurantList(restList14).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
					setUserRestriction("false").setRestaurantFirstOrder("false").
					setRuleDiscount(rd14).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
					setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();
		 RMSTDHelper rmsTDHelper14 = new RMSTDHelper();
		 int count14 =0;
		 int discountId14 = -1;
		 do{
		 Processor p = rmsTDHelper14.createTD(jsonHelper.getObjectToJSON(entry1).toString(), accessToken);
			if (p.ResponseValidator.GetNodeValueAsInt("statusCode") == -5) {
				String[] res = p.ResponseValidator.GetNodeValue("data.dataRest.data").split(" ");
				System.out.println("IDDDD" +res[5]);
				Processor p2 =RngHelper.disableEnableTDFromMarketingDashboard(res[5], "false");
			} else {
				System.out.println(p.ResponseValidator.GetNodeValueAsInt("data.dataRest.data"));
			 discountId14 = p.ResponseValidator.GetNodeValueAsInt("data.dataRest.data");
				break;
			}
		} while (count14++ < 5);
			
		 ArrayList<Integer> restIds15=new ArrayList<>();
		 
		 restIds15.add(Percentage_restaurants);
		 
		 ArrayList<Restaurant> restList15 =new ArrayList<>();
		 Restaurant r15=new Restaurant();
		 
		 r15.setId(Percentage_restaurants);
		 r15.setName("Myramzi");
		 r15.setCity(219);
		 r15.setArea("MyRamziArea");
		 r15.setRecommended(0);
		 r15.setCostForTwo(0);
		 r15.setDeliveryCharge(0);
		 r15.setMinimumOrder(0);
		 r15.setOpened(0);
		 r15.setDeliveryTime(0);
		 r15.setTmpClosed(0);
		 r15.setPostalCode(0);
		 r15.setCityDeliveryCharge(0);
		 r15.setThreshold(0);
		 r15.setDiscountType(0);
		 r15.setTradeCampaignHeaders(new ArrayList());
		 r15.setSelect("false");
		 r15.setNew("false");
		 r15.setCategories(new ArrayList());
		 restList2.add(r15);
		 RuleDiscount rd15=new RuleDiscount();
		 rd15.setType("Percentage");
		 rd15.setDiscountLevel("Restaurant");
		 rd15.setPercentDiscount(3);
		 rd15.setMinCartAmount(0);
		 rd15.setPercentDiscountAbsoluteCap(0);
		 
		 RMSUpdateTDEntry entry15=new RMSEditTDBuilder().
					setEnabled("true").
					setRestaurantIds(restIds15).setId(discountId14).
					setValidFrom(Long.valueOf(DateUtils.addHours(new Date(), 0).toInstant().toEpochMilli())).
					setValidTill(Long.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().toEpochMilli())).
					setCampaignType("Percentage").setRuleDiscountType("Percentage").
					setUpdatedBy(9886379321l).setDiscountLevel("Restaurant").
					setRestaurantList(restList15).setSlots(new ArrayList<>()).setFirstOrderRestriction("true").
					setUserRestriction("false").setRestaurantFirstOrder("false").
					setRuleDiscount(rd15).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
					setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();

			return new Object[][] {
				{jsonHelper.getObjectToJSON(entry2),accessToken,true},
				{jsonHelper.getObjectToJSON(entry5),accessToken,false},
				{jsonHelper.getObjectToJSON(entry7),accessToken,false},
				{jsonHelper.getObjectToJSON(entry9),accessToken,false},
			    {jsonHelper.getObjectToJSON(entry11),accessToken,false},
				{jsonHelper.getObjectToJSON(entry13),accessToken,false},
				{jsonHelper.getObjectToJSON(entry15),accessToken,false}
			};	
		}


	@DataProvider(name = "getTDbyRest")
	public Object[][] getTDbyRest() throws IOException {
		
		ArrayList<String> lst1= new ArrayList<String>();
	       lst1.add(restaurantId1);
		
		ArrayList<String> lst2= new ArrayList<String>();
	       lst2.add(restaurantId1);
	       lst2.add(restaurantId);
		
		return new Object[][] {
			{lst1, RMSConstants.getDiscountbyRestMessage,true},
			{lst2, RMSConstants.getDiscountbyRestMessage,false}
		};
	}
	
	@DataProvider(name = "getTDbyRest1")
	public Object[][] getTDbyRest1() throws IOException {
		ArrayList<String> lst1= new ArrayList<String>();
	       lst1.add("-9990");
	       lst1.add(restaurantId1);
	       
		ArrayList<String> lst2= new ArrayList<String>();
	       lst2.add("0");
	       lst2.add("4993");
		
	       ArrayList<String> lst3= new ArrayList<String>();
	       lst3.add("3344");
	       
	       ArrayList<String> lst4= new ArrayList<String>();
	       lst4.add("0");
	       
	       ArrayList<String> lst5= new ArrayList<String>();
	       lst5.add("-9990");
		
		return new Object[][] {
			{lst3, -3, "Invalid Session"},
			{lst4, -3, "Invalid Session"},
			{lst5, -3, "Invalid Session"},
			{lst1, -3, "Invalid Session"},
			{lst2, -3, "Invalid Session"}
			
		};

	}
	
	@DataProvider(name = "metaData")
	public Object[][] metaData() throws IOException {
		
		return new Object[][] {
			{restaurantId1, RMSConstants.getMetatMessage}
		};

	}
	
	
	@DataProvider(name = "metaData1")
	public Object[][] metaData1() throws IOException {
		
		return new Object[][] {
			{"33344", -3, "Invalid Session"},
			{"0", -3, "Invalid Session"},
			{"-9990", -3, "Invalid Session"}
		};

	}
	
	@DataProvider(name = "getTDdetails")
	public Object[][] getTDdetails() throws IOException {
		
		 JsonHelper jsonHelper = new JsonHelper();
		 ArrayList<Integer> restIds1=new ArrayList<>();
		 
		 restIds1.add(Percentage_restaurants);
		 ArrayList<Restaurant> restList1 =new ArrayList<>();
		 Restaurant r1=new Restaurant();
		 r1.setId(Percentage_restaurants);
		 r1.setName("Myramzi");
		 r1.setCity(219);
		 r1.setArea("MyRamziArea");
		 r1.setRecommended(0);
		 r1.setCostForTwo(0);
		 r1.setDeliveryCharge(0);
		 r1.setMinimumOrder(0);
		 r1.setOpened(0);
		 r1.setDeliveryTime(0);
		 r1.setTmpClosed(0);
		 r1.setPostalCode(0);
		 r1.setCityDeliveryCharge(0);
		 r1.setThreshold(0);
		 r1.setDiscountType(0);
		 r1.setTradeCampaignHeaders(new ArrayList());
		 r1.setSelect("false");
		 r1.setNew("false");
		 r1.setCategories(new ArrayList());
		 restList1.add(r1);
		 RuleDiscount rd=new RuleDiscount();
		 rd.setType("Percentage");
		 rd.setDiscountLevel("Restaurant");
		 rd.setPercentDiscount(2);
		 rd.setMinCartAmount(0);
		 rd.setPercentDiscountAbsoluteCap(0);
		 
		 RMSCreateTDEntry entry1=new RMSTDBuilder().
					setEnabled("true").
					setRestaurantIds(restIds1).
					setValidFrom(Long.valueOf(DateUtils.addHours(new Date(), 0).toInstant().toEpochMilli())).
					setValidTill(Long.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().toEpochMilli())).
					setCampaignType("Percentage").setRuleDiscountType("Percentage").
					setUpdatedBy(9886379321l).setDiscountLevel("Restaurant").
					setRestaurantList(restList1).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
					setUserRestriction("false").setRestaurantFirstOrder("false").
					setRuleDiscount(rd).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
					setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();
		 RMSTDHelper rmsTDHelper = new RMSTDHelper();
		 int count =0;
		 int discountId = -1;
		 do{
		 Processor p = rmsTDHelper.createTD(jsonHelper.getObjectToJSON(entry1).toString(), accessToken);
			if (p.ResponseValidator.GetNodeValueAsInt("statusCode") == -5) {
				String[] res = p.ResponseValidator.GetNodeValue("data.dataRest.data").split(" ");
				System.out.println("IDDDD" +res[6]);
				Processor p2 =RngHelper.disableEnableTDFromMarketingDashboard(res[6], "false");
			} else {
				System.out.println(p.ResponseValidator.GetNodeValueAsInt("data.dataRest.data"));
			 discountId = p.ResponseValidator.GetNodeValueAsInt("data.dataRest.data");
				break;
			}
		} while (count++ < 5);
			

		
		return new Object[][] {{restaurantId, discountId ,RMSConstants.getDiscountDetailsMessage}};
	}
	
	@DataProvider(name = "getTDdetails1")
	public Object[][] getTDdetails1() throws IOException {
		
		return new Object[][] {
			{"-9990", 111622, -3, "Invalid Session"},
			{"0", 111622, -3, "Invalid Session"},
			{"33344", 111622, -3, "Invalid Session"},
			{"333", 0, -3, "Invalid Session"}
			};

	}

	@DataProvider(name="createCategoryTD")
	public static Object[][] createCategoryTDData() throws IOException
	
	{
	 JsonHelper jsonHelper = new JsonHelper();

	 ArrayList<Integer> restIds1=new ArrayList<>();
	 restIds1.add(Percentage_restaurants);
	 ArrayList<Restaurant> restList1 =new ArrayList<>();
	 Restaurant r1=new Restaurant();
	 r1.setId(Percentage_restaurants);
	 r1.setName("Myramzi");
	 r1.setCity(219);
	 r1.setArea("MyRamziArea");
	 r1.setRecommended(0);
	 r1.setCostForTwo(0);
	 r1.setDeliveryCharge(0);
	 r1.setMinimumOrder(0);
	 r1.setOpened(0);
	 r1.setDeliveryTime(0);
	 r1.setTmpClosed(0);
	 r1.setPostalCode(0);
	 r1.setCityDeliveryCharge(0);
	 r1.setThreshold(0);
	 r1.setDiscountType(0);
	 r1.setTradeCampaignHeaders(new ArrayList());
	 r1.setSelect("false");
	 r1.setNew("false");
	 Category cat1= new Category();
	 List<Category> catList1=new ArrayList<>();
	 RMSTDServicesTestDP catid = new RMSTDServicesTestDP();
	 //String catid1 = Integer.toString(catid.categoryhelper.createCat(Flat_Restaurants));
	 //cat1.setId(catid1);
	 List<Map<String, Object>> catname1=new ArrayList<>();
	 catname1 = catid.cmshelper.getCategories(Integer.toString(Percentage_restaurants));
	 
	 for(int i=0;i<1;i++) {
		 System.out.println("Catgeory ID" +catname1.get(i).get("cat_id"));
		 String catg1 = catname1.get(i).get("cat_id").toString();
		 cat1.setId(catg1);
		 System.out.println("Catgeory Name" +catname1.get(i).get("cat_name"));
		 String catgname1 = catname1.get(i).get("cat_name").toString();
		 cat1.setName(catgname1);
	 }
	 
	 catList1.add(cat1);
	 r1.setCategories(catList1);
	 
	 restList1.add(r1);
	 RuleDiscount rd=new RuleDiscount();
	 rd.setType("Percentage");
	 rd.setDiscountLevel("Category");
	 rd.setPercentDiscount(20);
	 rd.setMinCartAmount(5000);
	 rd.setPercentDiscountAbsoluteCap(0);
	 Date Date1 = DateUtils.addDays(new Date(), 7);
	 	 
//	 Category cat1= new Category("1437332","Starters");
//	 List<Category> catList1=new ArrayList<>();
//	 catList1.add(cat1);
//	 r1.setCategories(catList1);
			 
/*		Slots s7=new Slots("1102", "FRI", "1100");
		ArrayList<Slots> list7=new ArrayList<>();
     list7.add(s7);*/
	 
	 RMSCreateTDEntry entry1=new RMSTDBuilder().
				setEnabled("true").
				setRestaurantIds(restIds1).
				setValidFrom(Long.valueOf(DateUtils.addDays(new Date(), 7).toInstant().toEpochMilli())).
				setValidTill(Long.valueOf(DateUtils.addMinutes(Date1, 2).toInstant().toEpochMilli())).
				setCampaignType("Percentage").setRuleDiscountType("Percentage").
				setCreatedBy(9738948943l).setDiscountLevel("Category").
				setRestaurantList(restList1).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
				setUserRestriction("false").setRestaurantFirstOrder("false").
				setRuleDiscount(rd).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
				setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();

	 ArrayList<Integer> restIds2=new ArrayList<>();
	 restIds2.add(Flat_Restaurants);
	 ArrayList<Restaurant> restList2 =new ArrayList<>();
	 Restaurant r2=new Restaurant();
	 r2.setId(Flat_Restaurants);
	 r2.setName("Myramzi");
	 r2.setCity(219);
	 r2.setArea("MyRamziArea");
	 r2.setRecommended(0);
	 r2.setCostForTwo(0);
	 r2.setDeliveryCharge(0);
	 r2.setMinimumOrder(0);
	 r2.setOpened(0);
	 r2.setDeliveryTime(0);
	 r2.setTmpClosed(0);
	 r2.setPostalCode(0);
	 r2.setCityDeliveryCharge(0);
	 r2.setThreshold(0);
	 r2.setDiscountType(0);
	 r2.setTradeCampaignHeaders(new ArrayList());
	 r2.setSelect("false");
	 r2.setNew("false");
	 Category cat2= new Category();
	 
	 List<Category> catList2=new ArrayList<>();
	 RMSTDServicesTestDP catid2 = new RMSTDServicesTestDP();
	 List<Map<String, Object>> catname2=new ArrayList<>();
	 catname2 = catid2.cmshelper.getCategories(Integer.toString(Flat_Restaurants));
	 
	 for(int i=0;i<1;i++) {
		 System.out.println("Catgeory ID" +catname1.get(i).get("cat_id"));
		 String catg2 = catname2.get(i).get("cat_id").toString();
		 cat2.setId(catg2);
		 System.out.println("Catgeory Name" +catname1.get(i).get("cat_name"));
		 String catgname2 = catname2.get(i).get("cat_name").toString();
		 cat2.setName(catgname2);
	 }
	 
	 catList2.add(cat2);	
	 cat2 = new Category();
	 
	 for(int i=0;i<2;i++) {
		 System.out.println("Catgeory ID" +catname1.get(i).get("cat_id"));
		 String catg3 = catname1.get(i).get("cat_id").toString();
		 cat2.setId(catg3);
		 System.out.println("Catgeory Name" +catname1.get(i).get("cat_name"));
		 String catgname3 = catname1.get(i).get("cat_name").toString();
		 cat2.setName(catgname3);
	 }

	 catList2.add(cat2);
	 r2.setCategories(catList2);
	 restList2.add(r2);
	 
	 RuleDiscount rd2=new RuleDiscount();
	 rd2.setType("Percentage");
	 rd2.setDiscountLevel("Category");
	 rd2.setPercentDiscount(20);
	 rd2.setMinCartAmount(5000);
	 rd2.setPercentDiscountAbsoluteCap(0);
	 Date Date2 = DateUtils.addDays(new Date(), 7);
	 	 
	 RMSCreateTDEntry entry2=new RMSTDBuilder().
				setEnabled("true").
				setRestaurantIds(restIds2).
				setValidFrom(Long.valueOf(DateUtils.addDays(new Date(), 7).toInstant().toEpochMilli())).
				setValidTill(Long.valueOf(DateUtils.addMinutes(Date2, 40).toInstant().toEpochMilli())).
				setCampaignType("Percentage").setRuleDiscountType("Percentage").
				setCreatedBy(9738948943l).setDiscountLevel("Category").
				setRestaurantList(restList2).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
				setUserRestriction("false").setRestaurantFirstOrder("false").
				setRuleDiscount(rd2).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
				setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();

	 
	 ArrayList<Integer> restIds3=new ArrayList<>();
	 restIds1.add(Percentage_restaurants);
	 ArrayList<Restaurant> restList3 =new ArrayList<>();
	 Restaurant r3=new Restaurant();
	 r3.setId(Percentage_restaurants);
	 r3.setName("Myramzi");
	 r3.setCity(219);
	 r3.setArea("MyRamziArea");
	 r3.setRecommended(0);
	 r3.setCostForTwo(0);
	 r3.setDeliveryCharge(0);
	 r3.setMinimumOrder(0);
	 r3.setOpened(0);
	 r3.setDeliveryTime(0);
	 r3.setTmpClosed(0);
	 r3.setPostalCode(0);
	 r3.setCityDeliveryCharge(0);
	 r3.setThreshold(0);
	 r3.setDiscountType(0);
	 r3.setTradeCampaignHeaders(new ArrayList());
	 r3.setSelect("false");
	 r3.setNew("false");

	 Category cat3= new Category();
	 List<Category> catList3=new ArrayList<>();
	 RMSTDServicesTestDP catid3 = new RMSTDServicesTestDP();
	 List<Map<String, Object>> catname3=new ArrayList<>();
	 catname3 = catid3.cmshelper.getCategories(Integer.toString(Percentage_restaurants));
	 
	 for(int i=0;i<1;i++) {
		 String catg3 = catname3.get(i).get("cat_id").toString();
		 cat3.setId(catg3);
		 String catgname3 = catname3.get(i).get("cat_name").toString();
		 cat3.setName(catgname3);
	 }
	 
	 catList3.add(cat3);
	 r3.setCategories(catList3);

	 restList3.add(r3);
	 RuleDiscount rd3=new RuleDiscount();
	 rd3.setType("Percentage");
	 rd3.setDiscountLevel("Category");
	 rd3.setPercentDiscount(2);
	 rd3.setMinCartAmount(0);
	 rd3.setPercentDiscountAbsoluteCap(0);
	 Date Date3 = DateUtils.addDays(new Date(), 7);
	 	 
	 RMSCreateTDEntry entry3=new RMSTDBuilder().
				setEnabled("true").
				setRestaurantIds(restIds3).
				setValidFrom(Long.valueOf(DateUtils.addDays(new Date(), 7).toInstant().toEpochMilli())).
				setValidTill(Long.valueOf(DateUtils.addMinutes(Date3, 2).toInstant().toEpochMilli())).
				setCampaignType("Percentage").setRuleDiscountType("Percentage").
				setCreatedBy(9738948943l).setDiscountLevel("Category").
				setRestaurantList(restList3).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
				setUserRestriction("false").setRestaurantFirstOrder("false").
				setRuleDiscount(rd3).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
				setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();
 
     ArrayList<Integer> restIds4=new ArrayList<>();
	 restIds4.add(Flat_Restaurants);
	 ArrayList<Restaurant> restList4 =new ArrayList<>();
	 Restaurant r4=new Restaurant();
	 r4.setId(Flat_Restaurants);
	 r4.setName("Myramzi2");
	 r4.setCity(219);
	 r4.setArea("MyRamziArea2");
	 r4.setRecommended(0);
	 r4.setCostForTwo(0);
	 r4.setDeliveryCharge(0);
	 r4.setMinimumOrder(0);
	 r4.setOpened(0);
	 r4.setDeliveryTime(0);
	 r4.setTmpClosed(0);
	 r4.setPostalCode(0);
	 r4.setCityDeliveryCharge(0);
	 r4.setThreshold(0);
	 r4.setDiscountType(0);
	 r4.setTradeCampaignHeaders(new ArrayList());
	 r4.setSelect("false");
	 r4.setNew("false");

	 Category cat4= new Category();
	 List<Category> catList4=new ArrayList<>();
	 RMSTDServicesTestDP catid4 = new RMSTDServicesTestDP();
	 List<Map<String, Object>> catname4=new ArrayList<>();
	 catname4 = catid4.cmshelper.getCategories(Integer.toString(Flat_Restaurants));
	 
	 for(int i=0;i<1;i++) {
		 String catg4 = catname4.get(i).get("cat_id").toString();
		 cat4.setId(catg4);
		 String catgname4 = catname4.get(i).get("cat_name").toString();
		 cat4.setName(catgname4);
	 }
	 
	 catList4.add(cat4);
	 r4.setCategories(catList4);

	 restList4.add(r4);
	 RuleDiscount rd4=new RuleDiscount();
	 rd4.setType("Flat");
	 rd4.setDiscountLevel("Category");
	 rd4.setFlatDiscountAmount(300);
	 rd4.setMinCartAmount(0);
	 Date Date4 = DateUtils.addDays(new Date(), 1);
	 
	 
	 RMSCreateTDEntry entry4=new RMSTDBuilder().
				setEnabled("true").
				setRestaurantIds(restIds4).
				setValidFrom(Long.valueOf(DateUtils.addDays(new Date(), 1).toInstant().toEpochMilli())).
				setValidTill(Long.valueOf(DateUtils.addMinutes(Date4, 2).toInstant().toEpochMilli())).
				setCampaignType("Flat").setRuleDiscountType("Flat").
				setCreatedBy(9886379321l).setDiscountLevel("Category").
				setRestaurantList(restList4).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
				setUserRestriction("false").setRestaurantFirstOrder("false").
				setRuleDiscount(rd4).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
				setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();

		 ArrayList<Integer> restIds5=new ArrayList<>();
		 restIds5.add(Percentage_restaurants);
		 ArrayList<Restaurant> restList5 =new ArrayList<>();
		 Restaurant r5=new Restaurant();
		 r5.setId(Percentage_restaurants);
		 r5.setName("Myramzi");
		 r5.setCity(219);
		 r5.setArea("MyRamziArea");
		 r5.setRecommended(0);
		 r5.setCostForTwo(0);
		 r5.setDeliveryCharge(0);
		 r5.setMinimumOrder(0);
		 r5.setOpened(0);
		 r5.setDeliveryTime(0);
		 r5.setTmpClosed(0);
		 r5.setPostalCode(0);
		 r5.setCityDeliveryCharge(0);
		 r5.setThreshold(0);
		 r5.setDiscountType(0);
		 r5.setTradeCampaignHeaders(new ArrayList());
		 r5.setSelect("false");
		 r5.setNew("false");
		 
		 Category cat5= new Category();
		 List<Category> catList5=new ArrayList<>();
		 RMSTDServicesTestDP catid5 = new RMSTDServicesTestDP();
		 List<Map<String, Object>> catname5=new ArrayList<>();
		 catname5 = catid5.cmshelper.getCategories(Integer.toString(Percentage_restaurants));
		 for(int i=0;i<1;i++) {
			 String catg51 = catname5.get(i).get("cat_id").toString();
			 cat5.setId(catg51);
			 String catgname51 = catname5.get(i).get("cat_name").toString();
			 cat5.setName(catgname51);
		 }
		 
		 catList5.add(cat5);	
		 cat5 = new Category();
		 
		 for(int i=0;i<2;i++) {
			 String catg52 = catname5.get(i).get("cat_id").toString();
			 cat5.setId(catg52);
			 String catgname52 = catname5.get(i).get("cat_name").toString();
			 cat5.setName(catgname52);
		 }
		 catList5.add(cat5);
		 r5.setCategories(catList5);
		 restList5.add(r5);
		 RuleDiscount rd5=new RuleDiscount();
		 rd5.setType("Percentage");
		 rd5.setDiscountLevel("Category");
		 rd5.setPercentDiscount(2);
		 rd5.setMinCartAmount(100);
		 rd5.setPercentDiscountAbsoluteCap(0);
		 Date Date5 = DateUtils.addDays(new Date(), 2);
		 
		 RMSCreateTDEntry entry5=new RMSTDBuilder().
					setEnabled("true").
					setRestaurantIds(restIds5).
					setValidFrom(Long.valueOf(DateUtils.addDays(new Date(), 2).toInstant().toEpochMilli())).
					setValidTill(Long.valueOf(DateUtils.addMinutes(Date5, 2).toInstant().toEpochMilli())).
					setCampaignType("Percentage").setRuleDiscountType("Percentage").
					setCreatedBy(9886379321l).setDiscountLevel("Category").
					setRestaurantList(restList5).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
					setUserRestriction("false").setRestaurantFirstOrder("false").
					setRuleDiscount(rd5).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
					setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();
		 
		 ArrayList<Integer> restIds6=new ArrayList<>();
		 restIds6.add(Flat_Restaurants);
		 ArrayList<Restaurant> restList6 =new ArrayList<>();
		 Restaurant r6=new Restaurant();
		 r6.setId(Flat_Restaurants);
		 r6.setName("Myramzi2");
		 r6.setCity(219);
		 r6.setArea("MyRamziArea2");
		 r6.setRecommended(0);
		 r6.setCostForTwo(0);
		 r6.setDeliveryCharge(0);
		 r6.setMinimumOrder(0);
		 r6.setOpened(0);
		 r6.setDeliveryTime(0);
		 r6.setTmpClosed(0);
		 r6.setPostalCode(0);
		 r6.setCityDeliveryCharge(0);
		 r6.setThreshold(0);
		 r6.setDiscountType(0);
		 r6.setTradeCampaignHeaders(new ArrayList());
		 r6.setSelect("false");
		 r6.setNew("false");
		 Category cat6= new Category();
		 List<Category> catList6=new ArrayList<>();
		 RMSTDServicesTestDP catid6 = new RMSTDServicesTestDP();
		 List<Map<String, Object>> catname6=new ArrayList<>();
		 catname6 = catid6.cmshelper.getCategories(Integer.toString(Flat_Restaurants));
		
		 for(int i=0;i<1;i++) {
			 String catg61 = catname6.get(i).get("cat_id").toString();
			 cat6.setId(catg61);
			 String catgname61 = catname6.get(i).get("cat_name").toString();
			 cat6.setName(catgname61);
		 }
		 
		 catList6.add(cat6);	
		 cat6 = new Category();
		 
		 for(int i=0;i<2;i++) {
			 String catg62 = catname6.get(i).get("cat_id").toString();
			 cat6.setId(catg62);
			 String catgname62 = catname6.get(i).get("cat_name").toString();
			 cat6.setName(catgname62);
		 }
		 catList6.add(cat6);
		 r6.setCategories(catList6);		 
		 restList6.add(r6);
		 RuleDiscount rd6=new RuleDiscount();
		 rd6.setType("Flat");
		 rd6.setDiscountLevel("Category");
		 rd6.setFlatDiscountAmount(30);
		 rd6.setMinCartAmount(100);
		 Date Date6 = DateUtils.addDays(new Date(), 3);
		 
		 RMSCreateTDEntry entry6=new RMSTDBuilder().
					setEnabled("true").
					setRestaurantIds(restIds6).
					setValidFrom(Long.valueOf(DateUtils.addDays(new Date(), 3).toInstant().toEpochMilli())).
					setValidTill(Long.valueOf(DateUtils.addMinutes(Date6, 2).toInstant().toEpochMilli())).
					setCampaignType("Flat").setRuleDiscountType("Flat").
					setCreatedBy(9886379321l).setDiscountLevel("Category").
					setRestaurantList(restList6).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
					setUserRestriction("false").setRestaurantFirstOrder("false").
					setRuleDiscount(rd6).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
					setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();

		 ArrayList<Integer> restIds7=new ArrayList<>();
		 restIds7.add(Percentage_restaurants);
		 ArrayList<Restaurant> restList7 =new ArrayList<>();
		 Restaurant r7=new Restaurant();
		 r7.setId(Percentage_restaurants);
		 r7.setName("Myramzi");
		 r7.setCity(219);
		 r7.setArea("MyRamziArea");
		 r7.setRecommended(0);
		 r7.setCostForTwo(0);
		 r7.setDeliveryCharge(0);
		 r7.setMinimumOrder(0);
		 r7.setOpened(0);
		 r7.setDeliveryTime(0);
		 r7.setTmpClosed(0);
		 r7.setPostalCode(0);
		 r7.setCityDeliveryCharge(0);
		 r7.setThreshold(0);
		 r7.setDiscountType(0);
		 r7.setTradeCampaignHeaders(new ArrayList());
		 r7.setSelect("false");
		 r7.setNew("false");
		 
		 Category cat7= new Category();
		 List<Category> catList7=new ArrayList<>();
		 RMSTDServicesTestDP catid7 = new RMSTDServicesTestDP();
		 List<Map<String, Object>> catname7=new ArrayList<>();
		 catname7 = catid7.cmshelper.getCategories(Integer.toString(Percentage_restaurants));
		 
		 for(int i=0;i<1;i++) {
			 String catg7 = catname7.get(i).get("cat_id").toString();
			 cat7.setId(catg7);
			 String catgname7 = catname7.get(i).get("cat_name").toString();
			 cat7.setName(catgname7);
		 }
		 
		 catList7.add(cat7);
		 r7.setCategories(catList7);

		 restList7.add(r7);
		 RuleDiscount rd7=new RuleDiscount();
		 rd7.setType("Percentage");
		 rd7.setDiscountLevel("Category");
		 rd7.setPercentDiscount(50);
		 rd7.setMinCartAmount(1000);
		 rd7.setPercentDiscountAbsoluteCap(0);
		 Date Date7 = DateUtils.addDays(new Date(), 4);
		 
		 RMSCreateTDEntry entry7=new RMSTDBuilder().
					setEnabled("true").
					setRestaurantIds(restIds7).
					setValidFrom(Long.valueOf(DateUtils.addDays(new Date(), 4).toInstant().toEpochMilli())).
					setValidTill(Long.valueOf(DateUtils.addMinutes(Date7, 2).toInstant().toEpochMilli())).
					setCampaignType("Percentage").setRuleDiscountType("Percentage").
					setCreatedBy(9886379321l).setDiscountLevel("Category").
					setRestaurantList(restList7).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
					setUserRestriction("false").setRestaurantFirstOrder("false").
					setRuleDiscount(rd7).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
					setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();
		 

		 ArrayList<Integer> restIds8=new ArrayList<>();
		 restIds8.add(Percentage_restaurants);
		 ArrayList<Restaurant> restList8 =new ArrayList<>();
		 Restaurant r8=new Restaurant();
		 r8.setId(Percentage_restaurants);
		 r8.setName("Myramzi");
		 r8.setCity(219);
		 r8.setArea("MyRamziArea");
		 r8.setRecommended(0);
		 r8.setCostForTwo(0);
		 r8.setDeliveryCharge(0);
		 r8.setMinimumOrder(0);
		 r8.setOpened(0);
		 r8.setDeliveryTime(0);
		 r8.setTmpClosed(0);
		 r8.setPostalCode(0);
		 r8.setCityDeliveryCharge(0);
		 r8.setThreshold(0);
		 r8.setDiscountType(0);
		 r8.setTradeCampaignHeaders(new ArrayList());
		 r8.setSelect("false");
		 r8.setNew("false");
		 
		 Category cat8= new Category();
		 List<Category> catList8=new ArrayList<>();
		 RMSTDServicesTestDP catid8 = new RMSTDServicesTestDP();
		 List<Map<String, Object>> catname8=new ArrayList<>();
		 catname8 = catid8.cmshelper.getCategories(Integer.toString(Percentage_restaurants));
		
		 for(int i=0;i<1;i++) {
			 String catg81 = catname8.get(i).get("cat_id").toString();
			 cat8.setId(catg81);
			 String catgname81 = catname8.get(i).get("cat_name").toString();
			 cat8.setName(catgname81);
		 }
		 
		 catList8.add(cat8);	
		 cat8 = new Category();
		 
		 for(int i=0;i<2;i++) {
			 String catg82 = catname8.get(i).get("cat_id").toString();
			 cat8.setId(catg82);
			 String catgname82 = catname8.get(i).get("cat_name").toString();
			 cat8.setName(catgname82);
		 }
		 catList8.add(cat8);
		 r8.setCategories(catList8);		 
		 restList8.add(r8);
		 RuleDiscount rd8=new RuleDiscount();
		 rd8.setType("Percentage");
		 rd8.setDiscountLevel("Category");
		 rd8.setPercentDiscount(50);
		 rd8.setMinCartAmount(0);
		 rd8.setPercentDiscountAbsoluteCap(0);
		 Date Date8 = DateUtils.addDays(new Date(), 4);
		 
		 RMSCreateTDEntry entry8=new RMSTDBuilder().
					setEnabled("true").
					setRestaurantIds(restIds8).
					setValidFrom(Long.valueOf(DateUtils.addDays(new Date(), 4).toInstant().toEpochMilli())).
					setValidTill(Long.valueOf(DateUtils.addMinutes(Date8, 2).toInstant().toEpochMilli())).
					setCampaignType("Percentage").setRuleDiscountType("Percentage").
					setCreatedBy(9886379321l).setDiscountLevel("Category").
					setRestaurantList(restList8).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
					setUserRestriction("false").setRestaurantFirstOrder("false").
					setRuleDiscount(rd8).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
					setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();

		 
		 ArrayList<Integer> restIds9=new ArrayList<>();
		 restIds9.add(Flat_Restaurants);
		 ArrayList<Restaurant> restList9 =new ArrayList<>();
		 Restaurant r9=new Restaurant();
		 r9.setId(Flat_Restaurants);
		 r9.setName("Myramzi2");
		 r9.setCity(219);
		 r9.setArea("MyRamziArea2");
		 r9.setRecommended(0);
		 r9.setCostForTwo(0);
		 r9.setDeliveryCharge(0);
		 r9.setMinimumOrder(0);
		 r9.setOpened(0);
		 r9.setDeliveryTime(0);
		 r9.setTmpClosed(0);
		 r9.setPostalCode(0);
		 r9.setCityDeliveryCharge(0);
		 r9.setThreshold(0);
		 r9.setDiscountType(0);
		 r9.setTradeCampaignHeaders(new ArrayList());
		 r9.setSelect("false");
		 r9.setNew("false");
		 
		 Category cat9= new Category();
		 List<Category> catList9=new ArrayList<>();
		 RMSTDServicesTestDP catid9 = new RMSTDServicesTestDP();
		 List<Map<String, Object>> catname9=new ArrayList<>();
		 catname9 = catid9.cmshelper.getCategories(Integer.toString(Flat_Restaurants));
		 
		 for(int i=0;i<1;i++) {
			 String catg9 = catname9.get(i).get("cat_id").toString();
			 cat9.setId(catg9);
			 String catgname9 = catname9.get(i).get("cat_name").toString();
			 cat9.setName(catgname9);
		 }
		 
		 catList9.add(cat9);
		 r9.setCategories(catList9);
		 restList9.add(r9);
		 RuleDiscount rd9=new RuleDiscount();
		 rd9.setType("Flat");
		 rd9.setDiscountLevel("Category");
		 rd9.setFlatDiscountAmount(300);
		 rd9.setMinCartAmount(1000);
		 Date Date9 = DateUtils.addDays(new Date(), 3);
		 
		 
		 RMSCreateTDEntry entry9=new RMSTDBuilder().
					setEnabled("true").
					setRestaurantIds(restIds9).
					setValidFrom(Long.valueOf(DateUtils.addDays(new Date(), 3).toInstant().toEpochMilli())).
					setValidTill(Long.valueOf(DateUtils.addMinutes(Date9, 2).toInstant().toEpochMilli())).
					setCampaignType("Flat").setRuleDiscountType("Flat").
					setCreatedBy(9886379321l).setDiscountLevel("Category").
					setRestaurantList(restList9).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
					setUserRestriction("false").setRestaurantFirstOrder("false").
					setRuleDiscount(rd9).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
					setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();

		 ArrayList<Integer> restIds10=new ArrayList<>();
		 restIds10.add(Flat_Restaurants);
		 ArrayList<Restaurant> restList10 =new ArrayList<>();
		 Restaurant r10=new Restaurant();
		 r10.setId(Flat_Restaurants);
		 r10.setName("Myramzi2");
		 r10.setCity(219);
		 r10.setArea("MyRamziArea2");
		 r10.setRecommended(0);
		 r10.setCostForTwo(0);
		 r10.setDeliveryCharge(0);
		 r10.setMinimumOrder(0);
		 r10.setOpened(0);
		 r10.setDeliveryTime(0);
		 r10.setTmpClosed(0);
		 r10.setPostalCode(0);
		 r10.setCityDeliveryCharge(0);
		 r10.setThreshold(0);
		 r10.setDiscountType(0);
		 r10.setTradeCampaignHeaders(new ArrayList());
		 r10.setSelect("false");
		 r10.setNew("false");
		 
		 Category cat10= new Category();
		 List<Category> catList10=new ArrayList<>();
		 RMSTDServicesTestDP catid10 = new RMSTDServicesTestDP();
		 List<Map<String, Object>> catname10=new ArrayList<>();
		 catname10 = catid10.cmshelper.getCategories(Integer.toString(Flat_Restaurants));
		
		 for(int i=0;i<1;i++) {
			 String catg101 = catname10.get(i).get("cat_id").toString();
			 cat10.setId(catg101);
			 String catgname101 = catname10.get(i).get("cat_name").toString();
			 cat10.setName(catgname101);
		 }
		 
		 catList10.add(cat10);	
		 cat10 = new Category();
		 
		 for(int i=0;i<2;i++) {
			 String catg102 = catname10.get(i).get("cat_id").toString();
			 cat10.setId(catg102);
			 String catgname102 = catname10.get(i).get("cat_name").toString();
			 cat10.setName(catgname102);
		 }
		 catList10.add(cat10);
		 r10.setCategories(catList10);		 
		 restList10.add(r10);
		 RuleDiscount rd10=new RuleDiscount();
		 rd10.setType("Flat");
		 rd10.setDiscountLevel("Restaurant");
		 rd10.setFlatDiscountAmount(30);
		 rd10.setMinCartAmount(0);
		 Date Date10 = DateUtils.addDays(new Date(), 3);
		 
		 
		 RMSCreateTDEntry entry10=new RMSTDBuilder().
					setEnabled("true").
					setRestaurantIds(restIds10).
					setValidFrom(Long.valueOf(DateUtils.addDays(new Date(), 3).toInstant().toEpochMilli())).
					setValidTill(Long.valueOf(DateUtils.addMinutes(Date10, 2).toInstant().toEpochMilli())).
					setCampaignType("Flat").setRuleDiscountType("Flat").
					setCreatedBy(9886379321l).setDiscountLevel("Restaurant").
					setRestaurantList(restList10).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
					setUserRestriction("false").setRestaurantFirstOrder("false").
					setRuleDiscount(rd10).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
					setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();


		 ArrayList<Integer> restIds11=new ArrayList<>();
		 restIds11.add(Percentage_restaurants);
		 restIds11.add(Flat_Restaurants);		 
		 ArrayList<Restaurant> restList11 =new ArrayList<>();
		 Restaurant r11=new Restaurant();
		 Restaurant r12=new Restaurant();
		 r11.setId(Percentage_restaurants);
		 r11.setName("Myramzi");
		 r11.setCity(219);
		 r11.setArea("MyRamziArea");
		 r11.setRecommended(0);
		 r11.setCostForTwo(0);
		 r11.setDeliveryCharge(0);
		 r11.setMinimumOrder(0);
		 r11.setOpened(0);
		 r11.setDeliveryTime(0);
		 r11.setTmpClosed(0);
		 r11.setPostalCode(0);
		 r11.setCityDeliveryCharge(0);
		 r11.setThreshold(0);
		 r11.setDiscountType(0);
		 r11.setTradeCampaignHeaders(new ArrayList());
		 r11.setSelect("false");
		 r11.setNew("false");

		 Category cat11= new Category();
		 List<Category> catList11=new ArrayList<>();
		 RMSTDServicesTestDP catid11 = new RMSTDServicesTestDP();
		 List<Map<String, Object>> catname11=new ArrayList<>();
		 catname11 = catid11.cmshelper.getCategories(Integer.toString(Percentage_restaurants));
		 
		 for(int i=0;i<1;i++) {
			 String catg11 = catname11.get(i).get("cat_id").toString();
			 cat11.setId(catg11);
			 String catgname11 = catname11.get(i).get("cat_name").toString();
			 cat11.setName(catgname11);
		 }
		 
		 catList11.add(cat11);
		 r11.setCategories(catList11);
		 

		 r12.setId(Flat_Restaurants);
		 r12.setName("Myramzi");
		 r12.setCity(219);
		 r12.setArea("MyRamziArea");
		 r12.setRecommended(0);
		 r12.setCostForTwo(0);
		 r12.setDeliveryCharge(0);
		 r12.setMinimumOrder(0);
		 r12.setOpened(0);
		 r12.setDeliveryTime(0);
		 r12.setTmpClosed(0);
		 r12.setPostalCode(0);
		 r12.setCityDeliveryCharge(0);
		 r12.setThreshold(0);
		 r12.setDiscountType(0);
		 r12.setTradeCampaignHeaders(new ArrayList());
		 r12.setSelect("false");
		 r12.setNew("false");
		
		 Category cat12= new Category();
		 List<Category> catList12=new ArrayList<>();
		 RMSTDServicesTestDP catid12 = new RMSTDServicesTestDP();
		 List<Map<String, Object>> catname12=new ArrayList<>();
		 catname12 = catid12.cmshelper.getCategories(Integer.toString(Flat_Restaurants));
		 
		 for(int i=0;i<1;i++) {
			 String catg12 = catname12.get(i).get("cat_id").toString();
			 cat12.setId(catg12);
			 String catgname12 = catname12.get(i).get("cat_name").toString();
			 cat12.setName(catgname12);
		 }
		 
		 catList12.add(cat12);
		 r12.setCategories(catList12);
		 
		 restList11.add(r11);
		 restList11.add(r12);
		 RuleDiscount rd11=new RuleDiscount();
		 rd11.setType("Percentage");
		 rd11.setDiscountLevel("Category");
		 rd11.setPercentDiscount(2);
		 rd11.setMinCartAmount(0);
		 rd11.setPercentDiscountAbsoluteCap(0);
		 Date Date11 = DateUtils.addDays(new Date(), 0);
		 	 		 
		 RMSCreateTDEntry entry11=new RMSTDBuilder().
					setEnabled("true").
					setRestaurantIds(restIds11).
					setValidFrom(Long.valueOf(DateUtils.addDays(new Date(), 0).toInstant().toEpochMilli())).
					setValidTill(Long.valueOf(DateUtils.addMinutes(Date11, 2).toInstant().toEpochMilli())).
					setCampaignType("Percentage").setRuleDiscountType("Percentage").
					setCreatedBy(9738948943l).setDiscountLevel("Category").
					setRestaurantList(restList11).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
					setUserRestriction("false").setRestaurantFirstOrder("false").
					setRuleDiscount(rd11).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
					setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();

        
		 ArrayList<Integer> restIds13=new ArrayList<>();
		 restIds13.add(Flat_Restaurants);
		 restIds13.add(Percentage_restaurants);
		 ArrayList<Restaurant> restList13 =new ArrayList<>();
		 Restaurant r13=new Restaurant();
		 Restaurant r14=new Restaurant();
		 r13.setId(Percentage_restaurants);
		 r13.setName("Myramzi2");
		 r13.setCity(219);
		 r13.setArea("MyRamziArea2");
		 r13.setRecommended(0);
		 r13.setCostForTwo(0);
		 r13.setDeliveryCharge(0);
		 r13.setMinimumOrder(0);
		 r13.setOpened(0);
		 r13.setDeliveryTime(0);
		 r13.setTmpClosed(0);
		 r13.setPostalCode(0);
		 r13.setCityDeliveryCharge(0);
		 r13.setThreshold(0);
		 r13.setDiscountType(0);
		 r13.setTradeCampaignHeaders(new ArrayList());
		 r13.setSelect("false");
		 r13.setNew("false");
		 
		 Category cat13= new Category();
		 List<Category> catList13=new ArrayList<>();
		 RMSTDServicesTestDP catid13 = new RMSTDServicesTestDP();
		 List<Map<String, Object>> catname13=new ArrayList<>();
		 catname13 = catid13.cmshelper.getCategories(Integer.toString(Percentage_restaurants));
		
		 for(int i=0;i<1;i++) {
			 String catg131 = catname13.get(i).get("cat_id").toString();
			 cat13.setId(catg131);
			 String catgname131 = catname13.get(i).get("cat_name").toString();
			 cat13.setName(catgname131);
		 }
		 
		 catList13.add(cat13);	
		 cat13 = new Category();
		 
		 for(int i=0;i<2;i++) {
			 String catg132 = catname13.get(i).get("cat_id").toString();
			 cat13.setId(catg132);
			 String catgname132 = catname13.get(i).get("cat_name").toString();
			 cat13.setName(catgname132);
		 }
		 catList13.add(cat13);
		 r13.setCategories(catList13);		 
		
		 r14.setId(Flat_Restaurants);
		 r14.setName("Myramzi");
		 r14.setCity(219);
		 r14.setArea("MyRamziArea");
		 r14.setRecommended(0);
		 r14.setCostForTwo(0);
		 r14.setDeliveryCharge(0);
		 r14.setMinimumOrder(0);
		 r14.setOpened(0);
		 r14.setDeliveryTime(0);
		 r14.setTmpClosed(0);
		 r14.setPostalCode(0);
		 r14.setCityDeliveryCharge(0);
		 r14.setThreshold(0);
		 r14.setDiscountType(0);
		 r14.setTradeCampaignHeaders(new ArrayList());
		 r14.setSelect("false");
		 r14.setNew("false");

		 Category cat14= new Category();
		 List<Category> catList14=new ArrayList<>();
		 RMSTDServicesTestDP catid14 = new RMSTDServicesTestDP();
		 List<Map<String, Object>> catname14=new ArrayList<>();
		 catname14 = catid14.cmshelper.getCategories(Integer.toString(Flat_Restaurants));
		
		 for(int i=0;i<1;i++) {
			 String catg141 = catname14.get(i).get("cat_id").toString();
			 cat14.setId(catg141);
			 String catgname141 = catname14.get(i).get("cat_name").toString();
			 cat14.setName(catgname141);
		 }
		 
		 catList14.add(cat14);	
		 cat14 = new Category();
		 
		 for(int i=0;i<2;i++) {
			 String catg142 = catname14.get(i).get("cat_id").toString();
			 cat14.setId(catg142);
			 String catgname142 = catname14.get(i).get("cat_name").toString();
			 cat14.setName(catgname142);
		 }
		 catList14.add(cat14);
		 r14.setCategories(catList14);		 
		
		 restList13.add(r13);
		 restList13.add(r14);
		 RuleDiscount rd13=new RuleDiscount();
		 rd13.setType("Flat");
		 rd13.setDiscountLevel("Category");
		 rd13.setFlatDiscountAmount(300);
		 rd13.setMinCartAmount(0);
		 Date Date13 = DateUtils.addDays(new Date(), 1);
		 

		 RMSCreateTDEntry entry13=new RMSTDBuilder().
					setEnabled("true").
					setRestaurantIds(restIds13).
					setValidFrom(Long.valueOf(DateUtils.addDays(new Date(), 1).toInstant().toEpochMilli())).
					setValidTill(Long.valueOf(DateUtils.addMinutes(Date13, 2).toInstant().toEpochMilli())).
					setCampaignType("Flat").setRuleDiscountType("Flat").
					setCreatedBy(9886379321l).setDiscountLevel("Category").
					setRestaurantList(restList13).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
					setUserRestriction("false").setRestaurantFirstOrder("false").
					setRuleDiscount(rd13).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
					setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();

			 ArrayList<Integer> restIds15=new ArrayList<>();
			 restIds15.add(Percentage_restaurants);
			 restIds15.add(Flat_Restaurants);
			 ArrayList<Restaurant> restList15 =new ArrayList<>();
			 Restaurant r15=new Restaurant();
			 Restaurant r16=new Restaurant();
			 r15.setId(Percentage_restaurants);
			 r15.setName("Myramzi");
			 r15.setCity(219);
			 r15.setArea("MyRamziArea");
			 r15.setRecommended(0);
			 r15.setCostForTwo(0);
			 r15.setDeliveryCharge(0);
			 r15.setMinimumOrder(0);
			 r15.setOpened(0);
			 r15.setDeliveryTime(0);
			 r15.setTmpClosed(0);
			 r15.setPostalCode(0);
			 r15.setCityDeliveryCharge(0);
			 r15.setThreshold(0);
			 r15.setDiscountType(0);
			 r15.setTradeCampaignHeaders(new ArrayList());
			 r15.setSelect("false");
			 r15.setNew("false");

			 Category cat15= new Category();
			 List<Category> catList15=new ArrayList<>();
			 RMSTDServicesTestDP catid15 = new RMSTDServicesTestDP();
			 List<Map<String, Object>> catname15=new ArrayList<>();
			 catname15 = catid15.cmshelper.getCategories(Integer.toString(Percentage_restaurants));
			
			 for(int i=0;i<1;i++) {
				 String catg151 = catname15.get(i).get("cat_id").toString();
				 cat15.setId(catg151);
				 String catgname151 = catname15.get(i).get("cat_name").toString();
				 cat15.setName(catgname151);
			 }
			 
			 catList15.add(cat15);	
			 cat15 = new Category();
			 
			 for(int i=0;i<2;i++) {
				 String catg152 = catname15.get(i).get("cat_id").toString();
				 cat15.setId(catg152);
				 String catgname152 = catname15.get(i).get("cat_name").toString();
				 cat15.setName(catgname152);
			 }
			 catList15.add(cat15);
			 r15.setCategories(catList15);		 
					
			 r16.setId(Flat_Restaurants);
			 r16.setName("Myramzi");
			 r16.setCity(219);
			 r16.setArea("MyRamziArea");
			 r16.setRecommended(0);
			 r16.setCostForTwo(0);
			 r16.setDeliveryCharge(0);
			 r16.setMinimumOrder(0);
			 r16.setOpened(0);
			 r16.setDeliveryTime(0);
			 r16.setTmpClosed(0);
			 r16.setPostalCode(0);
			 r16.setCityDeliveryCharge(0);
			 r16.setThreshold(0);
			 r16.setDiscountType(0);
			 r16.setTradeCampaignHeaders(new ArrayList());
			 r16.setSelect("false");
			 r16.setNew("false");

			 Category cat16= new Category();
			 List<Category> catList16=new ArrayList<>();
			 RMSTDServicesTestDP catid16 = new RMSTDServicesTestDP();
			 List<Map<String, Object>> catname16=new ArrayList<>();
			 catname16 = catid16.cmshelper.getCategories(Integer.toString(Flat_Restaurants));
			
			 for(int i=0;i<1;i++) {
				 String catg161 = catname16.get(i).get("cat_id").toString();
				 cat16.setId(catg161);
				 String catgname161 = catname16.get(i).get("cat_name").toString();
				 cat16.setName(catgname161);
			 }
			 
			 catList16.add(cat16);	
			 cat16 = new Category();
			 
			 for(int i=0;i<2;i++) {
				 String catg162 = catname16.get(i).get("cat_id").toString();
				 cat16.setId(catg162);
				 String catgname162 = catname16.get(i).get("cat_name").toString();
				 cat16.setName(catgname162);
			 }
			 catList16.add(cat16);
			 r16.setCategories(catList16);		 
			
			 restList15.add(r15);
			 restList15.add(r16);
			 RuleDiscount rd15=new RuleDiscount();
			 rd15.setType("Percentage");
			 rd15.setDiscountLevel("Category");
			 rd15.setPercentDiscount(2);
			 rd15.setMinCartAmount(100);
			 rd15.setPercentDiscountAbsoluteCap(0);
			 Date Date15 = DateUtils.addDays(new Date(), 2);
			 
			 RMSCreateTDEntry entry15=new RMSTDBuilder().
						setEnabled("true").
						setRestaurantIds(restIds15).
						setValidFrom(Long.valueOf(DateUtils.addDays(new Date(), 2).toInstant().toEpochMilli())).
						setValidTill(Long.valueOf(DateUtils.addMinutes(Date15, 2).toInstant().toEpochMilli())).
						setCampaignType("Percentage").setRuleDiscountType("Percentage").
						setCreatedBy(9886379321l).setDiscountLevel("Category").
						setRestaurantList(restList15).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
						setUserRestriction("false").setRestaurantFirstOrder("false").
						setRuleDiscount(rd15).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
						setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();
			 
			 ArrayList<Integer> restIds17=new ArrayList<>();
			 restIds17.add(Flat_Restaurants);
			 restIds17.add(Percentage_restaurants);
			 ArrayList<Restaurant> restList17 =new ArrayList<>();
			 Restaurant r17=new Restaurant();
			 Restaurant r18=new Restaurant();
			
			 r17.setId(Percentage_restaurants);
			 r17.setName("Myramzi2");
			 r17.setCity(219);
			 r17.setArea("MyRamziArea2");
			 r17.setRecommended(0);
			 r17.setCostForTwo(0);
			 r17.setDeliveryCharge(0);
			 r17.setMinimumOrder(0);
			 r17.setOpened(0);
			 r17.setDeliveryTime(0);
			 r17.setTmpClosed(0);
			 r17.setPostalCode(0);
			 r17.setCityDeliveryCharge(0);
			 r17.setThreshold(0);
			 r17.setDiscountType(0);
			 r17.setTradeCampaignHeaders(new ArrayList());
			 r17.setSelect("false");
			 r17.setNew("false");

			 Category cat17= new Category();
			 List<Category> catList17=new ArrayList<>();
			 RMSTDServicesTestDP catid17 = new RMSTDServicesTestDP();
			 List<Map<String, Object>> catname17=new ArrayList<>();
			 catname17 = catid17.cmshelper.getCategories(Integer.toString(Percentage_restaurants));
			 
			 for(int i=0;i<1;i++) {
				 String catg17 = catname17.get(i).get("cat_id").toString();
				 cat17.setId(catg17);
				 String catgname17 = catname17.get(i).get("cat_name").toString();
				 cat17.setName(catgname17);
			 }
			 
			 catList17.add(cat17);
			 r17.setCategories(catList17);
				 
			 r18.setId(Flat_Restaurants);
			 r18.setName("Myramzi");
			 r18.setCity(219);
			 r18.setArea("MyRamziArea");
			 r18.setRecommended(0);
			 r18.setCostForTwo(0);
			 r18.setDeliveryCharge(0);
			 r18.setMinimumOrder(0);
			 r18.setOpened(0);
			 r18.setDeliveryTime(0);
			 r18.setTmpClosed(0);
			 r18.setPostalCode(0);
			 r18.setCityDeliveryCharge(0);
			 r18.setThreshold(0);
			 r18.setDiscountType(0);
			 r18.setTradeCampaignHeaders(new ArrayList());
			 r18.setSelect("false");
			 r18.setNew("false");

			 Category cat18= new Category();
			 List<Category> catList18=new ArrayList<>();
			 RMSTDServicesTestDP catid18 = new RMSTDServicesTestDP();
			 List<Map<String, Object>> catname18=new ArrayList<>();
			 catname18 = catid18.cmshelper.getCategories(Integer.toString(Flat_Restaurants));
			 
			 for(int i=0;i<1;i++) {
				 String catg18 = catname18.get(i).get("cat_id").toString();
				 cat18.setId(catg18);
				 String catgname18 = catname18.get(i).get("cat_name").toString();
				 cat18.setName(catgname18);
			 }
			 
			 catList18.add(cat18);
			 r18.setCategories(catList18);
			
			 restList17.add(r17);
			 restList17.add(r18);
			 RuleDiscount rd17=new RuleDiscount();
			 rd17.setType("Flat");
			 rd17.setDiscountLevel("Category");
			 rd17.setFlatDiscountAmount(30);
			 rd17.setMinCartAmount(100);
			 Date Date17 = DateUtils.addDays(new Date(), 3);
			 
			 
			 RMSCreateTDEntry entry17=new RMSTDBuilder().
						setEnabled("true").
						setRestaurantIds(restIds17).
						setValidFrom(Long.valueOf(DateUtils.addDays(new Date(), 3).toInstant().toEpochMilli())).
						setValidTill(Long.valueOf(DateUtils.addMinutes(Date17, 2).toInstant().toEpochMilli())).
						setCampaignType("Flat").setRuleDiscountType("Flat").
						setCreatedBy(9886379321l).setDiscountLevel("Category").
						setRestaurantList(restList17).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
						setUserRestriction("false").setRestaurantFirstOrder("false").
						setRuleDiscount(rd17).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
						setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();

			 ArrayList<Integer> restIds19=new ArrayList<>();
			 restIds19.add(Percentage_restaurants);
			 restIds19.add(Flat_Restaurants);
			 ArrayList<Restaurant> restList19 =new ArrayList<>();
			 Restaurant r19=new Restaurant();
			 Restaurant r20=new Restaurant();
			 
			 r19.setId(Percentage_restaurants);
			 r19.setName("Myramzi");
			 r19.setCity(219);
			 r19.setArea("MyRamziArea");
			 r19.setRecommended(0);
			 r19.setCostForTwo(0);
			 r19.setDeliveryCharge(0);
			 r19.setMinimumOrder(0);
			 r19.setOpened(0);
			 r19.setDeliveryTime(0);
			 r19.setTmpClosed(0);
			 r19.setPostalCode(0);
			 r19.setCityDeliveryCharge(0);
			 r19.setThreshold(0);
			 r19.setDiscountType(0);
			 r19.setTradeCampaignHeaders(new ArrayList());
			 r19.setSelect("false");
			 r19.setNew("false");

			 Category cat19= new Category();
			 List<Category> catList19=new ArrayList<>();
			 RMSTDServicesTestDP catid19 = new RMSTDServicesTestDP();
			 List<Map<String, Object>> catname19=new ArrayList<>();
			 catname19 = catid19.cmshelper.getCategories(Integer.toString(Percentage_restaurants));
			 
			 for(int i=0;i<1;i++) {
				 String catg19 = catname19.get(i).get("cat_id").toString();
				 cat19.setId(catg19);
				 String catgname19 = catname19.get(i).get("cat_name").toString();
				 cat19.setName(catgname19);
			 }
			 
			 catList19.add(cat19);
			 r19.setCategories(catList19);
			
			 r20.setId(Flat_Restaurants);
			 r20.setName("Myramzi");
			 r20.setCity(219);
			 r20.setArea("MyRamziArea");
			 r20.setRecommended(0);
			 r20.setCostForTwo(0);
			 r20.setDeliveryCharge(0);
			 r20.setMinimumOrder(0);
			 r20.setOpened(0);
			 r20.setDeliveryTime(0);
			 r20.setTmpClosed(0);
			 r20.setPostalCode(0);
			 r20.setCityDeliveryCharge(0);
			 r20.setThreshold(0);
			 r20.setDiscountType(0);
			 r20.setTradeCampaignHeaders(new ArrayList());
			 r20.setSelect("false");
			 r20.setNew("false");
			 Category cat20= new Category();
			 List<Category> catList20=new ArrayList<>();
			 RMSTDServicesTestDP catid20 = new RMSTDServicesTestDP();
			 List<Map<String, Object>> catname20=new ArrayList<>();
			 catname20 = catid20.cmshelper.getCategories(Integer.toString(Flat_Restaurants));
			
			 for(int i=0;i<1;i++) {
				 String catg201 = catname20.get(i).get("cat_id").toString();
				 cat20.setId(catg201);
				 String catgname201 = catname20.get(i).get("cat_name").toString();
				 cat20.setName(catgname201);
			 }
			 
			 catList20.add(cat20);	
			 cat20 = new Category();
			 
			 for(int i=0;i<2;i++) {
				 String catg202 = catname16.get(i).get("cat_id").toString();
				 cat20.setId(catg202);
				 String catgname202 = catname20.get(i).get("cat_name").toString();
				 cat20.setName(catgname202);
			 }
			 catList20.add(cat20);
			 r20.setCategories(catList20);		 
			 
			 restList19.add(r19);
			 restList19.add(r20);
			 
			 RuleDiscount rd19=new RuleDiscount();
			 rd19.setType("Percentage");
			 rd19.setDiscountLevel("Category");
			 rd19.setPercentDiscount(50);
			 rd19.setMinCartAmount(1000);
			 rd19.setPercentDiscountAbsoluteCap(0);
			 Date Date19 = DateUtils.addDays(new Date(), 4);
			 
			 RMSCreateTDEntry entry19=new RMSTDBuilder().
						setEnabled("true").
						setRestaurantIds(restIds19).
						setValidFrom(Long.valueOf(DateUtils.addDays(new Date(), 4).toInstant().toEpochMilli())).
						setValidTill(Long.valueOf(DateUtils.addMinutes(Date19, 2).toInstant().toEpochMilli())).
						setCampaignType("Percentage").setRuleDiscountType("Percentage").
						setCreatedBy(9886379321l).setDiscountLevel("Category").
						setRestaurantList(restList19).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
						setUserRestriction("false").setRestaurantFirstOrder("false").
						setRuleDiscount(rd19).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
						setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();
			 

			 ArrayList<Integer> restIds21=new ArrayList<>();
			 restIds21.add(Percentage_restaurants);
			 restIds21.add(Flat_Restaurants);
			 ArrayList<Restaurant> restList21 =new ArrayList<>();
			 Restaurant r21=new Restaurant();
			 Restaurant r22=new Restaurant();
			 
			 r21.setId(Percentage_restaurants);
			 r21.setName("Myramzi");
			 r21.setCity(219);
			 r21.setArea("MyRamziArea");
			 r21.setRecommended(0);
			 r21.setCostForTwo(0);
			 r21.setDeliveryCharge(0);
			 r21.setMinimumOrder(0);
			 r21.setOpened(0);
			 r21.setDeliveryTime(0);
			 r21.setTmpClosed(0);
			 r21.setPostalCode(0);
			 r21.setCityDeliveryCharge(0);
			 r21.setThreshold(0);
			 r21.setDiscountType(0);
			 r21.setTradeCampaignHeaders(new ArrayList());
			 r21.setSelect("false");
			 r21.setNew("false");
			 Category cat21= new Category();
			 List<Category> catList21=new ArrayList<>();
			 RMSTDServicesTestDP catid21 = new RMSTDServicesTestDP();
			 List<Map<String, Object>> catname21=new ArrayList<>();
			 catname21 = catid21.cmshelper.getCategories(Integer.toString(Percentage_restaurants));
			
			 for(int i=0;i<1;i++) {
				 String catg211 = catname21.get(i).get("cat_id").toString();
				 cat21.setId(catg211);
				 String catgname211 = catname21.get(i).get("cat_name").toString();
				 cat21.setName(catgname211);
			 }
			 
			 catList21.add(cat21);	
			 cat21 = new Category();
			 
			 for(int i=0;i<2;i++) {
				 String catg212 = catname21.get(i).get("cat_id").toString();
				 cat21.setId(catg212);
				 String catgname212 = catname21.get(i).get("cat_name").toString();
				 cat21.setName(catgname212);
			 }
			 catList21.add(cat21);
			 r21.setCategories(catList21);		 
			 
			 r22.setId(Flat_Restaurants);
			 r22.setName("Myramzi");
			 r22.setCity(219);
			 r22.setArea("MyRamziArea");
			 r22.setRecommended(0);
			 r22.setCostForTwo(0);
			 r22.setDeliveryCharge(0);
			 r22.setMinimumOrder(0);
			 r22.setOpened(0);
			 r22.setDeliveryTime(0);
			 r22.setTmpClosed(0);
			 r22.setPostalCode(0);
			 r22.setCityDeliveryCharge(0);
			 r22.setThreshold(0);
			 r22.setDiscountType(0);
			 r22.setTradeCampaignHeaders(new ArrayList());
			 r22.setSelect("false");
			 r22.setNew("false");

			 Category cat22= new Category();
			 List<Category> catList22=new ArrayList<>();
			 RMSTDServicesTestDP catid22 = new RMSTDServicesTestDP();
			 List<Map<String, Object>> catname22=new ArrayList<>();
			 catname22 = catid22.cmshelper.getCategories(Integer.toString(Flat_Restaurants));
			 
			 for(int i=0;i<1;i++) {
				 String catg22 = catname22.get(i).get("cat_id").toString();
				 cat22.setId(catg22);
				 String catgname22 = catname22.get(i).get("cat_name").toString();
				 cat22.setName(catgname22);
			 }
			 
			 catList22.add(cat22);
			 r22.setCategories(catList22);
			
			 restList21.add(r21);
			 restList21.add(r22);
			 RuleDiscount rd21=new RuleDiscount();
			 rd21.setType("Percentage");
			 rd21.setDiscountLevel("Category");
			 rd21.setPercentDiscount(50);
			 rd21.setMinCartAmount(0);
			 rd21.setPercentDiscountAbsoluteCap(0);
			 Date Date20 = DateUtils.addDays(new Date(), 4);
			 
			 RMSCreateTDEntry entry21=new RMSTDBuilder().
						setEnabled("true").
						setRestaurantIds(restIds21).
						setValidFrom(Long.valueOf(DateUtils.addDays(new Date(), 4).toInstant().toEpochMilli())).
						setValidTill(Long.valueOf(DateUtils.addMinutes(Date20, 2).toInstant().toEpochMilli())).
						setCampaignType("Percentage").setRuleDiscountType("Percentage").
						setCreatedBy(9886379321l).setDiscountLevel("Category").
						setRestaurantList(restList21).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
						setUserRestriction("false").setRestaurantFirstOrder("false").
						setRuleDiscount(rd21).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
						setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();

			 
			 ArrayList<Integer> restIds23=new ArrayList<>();
			 restIds23.add(Flat_Restaurants);
			 restIds23.add(Percentage_restaurants);
			 ArrayList<Restaurant> restList23 =new ArrayList<>();
			 Restaurant r23=new Restaurant();
			 Restaurant r24=new Restaurant();
			 
			 r23.setId(Flat_Restaurants);
			 r23.setName("Myramzi2");
			 r23.setCity(219);
			 r23.setArea("MyRamziArea2");
			 r23.setRecommended(0);
			 r23.setCostForTwo(0);
			 r23.setDeliveryCharge(0);
			 r23.setMinimumOrder(0);
			 r23.setOpened(0);
			 r23.setDeliveryTime(0);
			 r23.setTmpClosed(0);
			 r23.setPostalCode(0);
			 r23.setCityDeliveryCharge(0);
			 r23.setThreshold(0);
			 r23.setDiscountType(0);
			 r23.setTradeCampaignHeaders(new ArrayList());
			 r23.setSelect("false");
			 r23.setNew("false");

			 Category cat23= new Category();
			 List<Category> catList23=new ArrayList<>();
			 RMSTDServicesTestDP catid23 = new RMSTDServicesTestDP();
			 List<Map<String, Object>> catname23=new ArrayList<>();
			 catname23 = catid23.cmshelper.getCategories(Integer.toString(Flat_Restaurants));
			 
			 for(int i=0;i<1;i++) {
				 String catg23 = catname23.get(i).get("cat_id").toString();
				 cat23.setId(catg23);
				 String catgname23 = catname23.get(i).get("cat_name").toString();
				 cat23.setName(catgname23);
			 }
			 
			 catList23.add(cat23);
			 r23.setCategories(catList23);
			 
			 r24.setId(Percentage_restaurants);
			 r24.setName("Myramzi2");
			 r24.setCity(219);
			 r24.setArea("MyRamziArea2");
			 r24.setRecommended(0);
			 r24.setCostForTwo(0);
			 r24.setDeliveryCharge(0);
			 r24.setMinimumOrder(0);
			 r24.setOpened(0);
			 r24.setDeliveryTime(0);
			 r24.setTmpClosed(0);
			 r24.setPostalCode(0);
			 r24.setCityDeliveryCharge(0);
			 r24.setThreshold(0);
			 r24.setDiscountType(0);
			 r24.setTradeCampaignHeaders(new ArrayList());
			 r24.setSelect("false");
			 r24.setNew("false");
			 Category cat24= new Category();
			 List<Category> catList24=new ArrayList<>();
			 RMSTDServicesTestDP catid24 = new RMSTDServicesTestDP();
			 List<Map<String, Object>> catname24=new ArrayList<>();
			 catname24 = catid24.cmshelper.getCategories(Integer.toString(Percentage_restaurants));
			
			 for(int i=0;i<1;i++) {
				 String catg241 = catname24.get(i).get("cat_id").toString();
				 cat24.setId(catg241);
				 String catgname241 = catname24.get(i).get("cat_name").toString();
				 cat24.setName(catgname241);
			 }
			 
			 catList24.add(cat24);	
			 cat24 = new Category();
			 
			 for(int i=0;i<2;i++) {
				 String catg242 = catname24.get(i).get("cat_id").toString();
				 cat24.setId(catg242);
				 String catgname242 = catname24.get(i).get("cat_name").toString();
				 cat24.setName(catgname242);
			 }
			 catList24.add(cat24);
			 r24.setCategories(catList24);		 
			 
			 restList23.add(r23);
			 restList23.add(r24);
			 RuleDiscount rd23=new RuleDiscount();
			 rd23.setType("Flat");
			 rd23.setDiscountLevel("Category");
			 rd23.setFlatDiscountAmount(300);
			 rd23.setMinCartAmount(1000);
			 Date Date22 = DateUtils.addDays(new Date(), 3);
			 
			 
			 RMSCreateTDEntry entry23=new RMSTDBuilder().
						setEnabled("true").
						setRestaurantIds(restIds23).
						setValidFrom(Long.valueOf(DateUtils.addDays(new Date(), 3).toInstant().toEpochMilli())).
						setValidTill(Long.valueOf(DateUtils.addMinutes(Date22, 2).toInstant().toEpochMilli())).
						setCampaignType("Flat").setRuleDiscountType("Flat").
						setCreatedBy(9886379321l).setDiscountLevel("Category").
						setRestaurantList(restList23).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
						setUserRestriction("false").setRestaurantFirstOrder("false").
						setRuleDiscount(rd23).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
						setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();

			 ArrayList<Integer> restIds25=new ArrayList<>();
			 restIds25.add(Flat_Restaurants);
			 restIds25.add(Percentage_restaurants);
			 ArrayList<Restaurant> restList25 =new ArrayList<>();
			 Restaurant r25=new Restaurant();
			 Restaurant r26=new Restaurant();
			
			 r25.setId(Flat_Restaurants);
			 r25.setName("Myramzi2");
			 r25.setCity(219);
			 r25.setArea("MyRamziArea2");
			 r25.setRecommended(0);
			 r25.setCostForTwo(0);
			 r25.setDeliveryCharge(0);
			 r25.setMinimumOrder(0);
			 r25.setOpened(0);
			 r25.setDeliveryTime(0);
			 r25.setTmpClosed(0);
			 r25.setPostalCode(0);
			 r25.setCityDeliveryCharge(0);
			 r25.setThreshold(0);
			 r25.setDiscountType(0);
			 r25.setTradeCampaignHeaders(new ArrayList());
			 r25.setSelect("false");
			 r25.setNew("false");
			 Category cat25= new Category();
			 List<Category> catList25=new ArrayList<>();
			 RMSTDServicesTestDP catid25 = new RMSTDServicesTestDP();
			 List<Map<String, Object>> catname25=new ArrayList<>();
			 catname25 = catid25.cmshelper.getCategories(Integer.toString(Flat_Restaurants));
			
			 for(int i=0;i<1;i++) {
				 String catg251 = catname25.get(i).get("cat_id").toString();
				 cat25.setId(catg251);
				 String catgname251 = catname25.get(i).get("cat_name").toString();
				 cat25.setName(catgname251);
			 }
			 
			 catList25.add(cat25);	
			 cat25 = new Category();
			 
			 for(int i=0;i<2;i++) {
				 String catg252 = catname25.get(i).get("cat_id").toString();
				 cat25.setId(catg252);
				 String catgname252 = catname25.get(i).get("cat_name").toString();
				 cat25.setName(catgname252);
			 }
			 catList25.add(cat25);
			 r25.setCategories(catList25);		 
			 
			 r26.setId(Percentage_restaurants);
			 r26.setName("Myramzi2");
			 r26.setCity(219);
			 r26.setArea("MyRamziArea2");
			 r26.setRecommended(0);
			 r26.setCostForTwo(0);
			 r26.setDeliveryCharge(0);
			 r26.setMinimumOrder(0);
			 r26.setOpened(0);
			 r26.setDeliveryTime(0);
			 r26.setTmpClosed(0);
			 r26.setPostalCode(0);
			 r26.setCityDeliveryCharge(0);
			 r26.setThreshold(0);
			 r26.setDiscountType(0);
			 r26.setTradeCampaignHeaders(new ArrayList());
			 r26.setSelect("false");
			 r26.setNew("false");

			 Category cat26= new Category();
			 List<Category> catList26=new ArrayList<>();
			 RMSTDServicesTestDP catid26 = new RMSTDServicesTestDP();
			 List<Map<String, Object>> catname26=new ArrayList<>();
			 catname26 = catid26.cmshelper.getCategories(Integer.toString(Percentage_restaurants));
			 
			 for(int i=0;i<1;i++) {
				 String catg26 = catname26.get(i).get("cat_id").toString();
				 cat26.setId(catg26);
				 String catgname26 = catname26.get(i).get("cat_name").toString();
				 cat26.setName(catgname26);
			 }
			 
			 catList26.add(cat26);
			 r26.setCategories(catList26);
			 
			 restList25.add(r25);
			 restList25.add(r26);
			 RuleDiscount rd25=new RuleDiscount();
			 rd25.setType("Flat");
			 rd25.setDiscountLevel("Category");
			 rd25.setFlatDiscountAmount(30);
			 rd25.setMinCartAmount(0);
			 Date Date25 = DateUtils.addDays(new Date(), 3);
			 
			 
			 RMSCreateTDEntry entry25=new RMSTDBuilder().
						setEnabled("true").
						setRestaurantIds(restIds25).
						setValidFrom(Long.valueOf(DateUtils.addDays(new Date(), 3).toInstant().toEpochMilli())).
						setValidTill(Long.valueOf(DateUtils.addMinutes(Date25, 2).toInstant().toEpochMilli())).
						setCampaignType("Flat").setRuleDiscountType("Flat").
						setCreatedBy(9886379321l).setDiscountLevel("Category").
						setRestaurantList(restList25).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
						setUserRestriction("false").setRestaurantFirstOrder("false").
						setRuleDiscount(rd25).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
						setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();

		 		 	 	 	 
		return new Object[][] {	
								{jsonHelper.getObjectToJSON(entry1),accessToken},
								{jsonHelper.getObjectToJSON(entry2),accessToken},
								{jsonHelper.getObjectToJSON(entry3),accessToken},
								{jsonHelper.getObjectToJSON(entry4),accessToken},
								{jsonHelper.getObjectToJSON(entry5),accessToken},
								{jsonHelper.getObjectToJSON(entry6),accessToken},
								{jsonHelper.getObjectToJSON(entry7),accessToken},
								{jsonHelper.getObjectToJSON(entry8),accessToken},
								{jsonHelper.getObjectToJSON(entry9),accessToken},
								{jsonHelper.getObjectToJSON(entry10),accessToken},
								{jsonHelper.getObjectToJSON(entry11),accessToken},
								{jsonHelper.getObjectToJSON(entry13),accessToken},
								{jsonHelper.getObjectToJSON(entry15),accessToken},
								{jsonHelper.getObjectToJSON(entry17),accessToken},
								{jsonHelper.getObjectToJSON(entry19),accessToken},
								{jsonHelper.getObjectToJSON(entry21),accessToken},
								{jsonHelper.getObjectToJSON(entry23),accessToken},
								{jsonHelper.getObjectToJSON(entry25),accessToken}
		
		};

	
	}	


	@DataProvider(name="createCatTD1")
	public static Object[][] createCatTDData1() throws IOException
	{
	 JsonHelper jsonHelper = new JsonHelper();
	 ArrayList<Integer> restIds1=new ArrayList<>();
	 restIds1.add(Percentage_restaurants);
	 ArrayList<Restaurant> restList1 =new ArrayList<>();
	 Restaurant r1=new Restaurant();
	 r1.setId(Percentage_restaurants);
	 r1.setName("Myramzi");
	 r1.setCity(219);
	 r1.setArea("MyRamziArea");
	 r1.setRecommended(0);
	 r1.setCostForTwo(0);
	 r1.setDeliveryCharge(0);
	 r1.setMinimumOrder(0);
	 r1.setOpened(0);
	 r1.setDeliveryTime(0);
	 r1.setTmpClosed(0);
	 r1.setPostalCode(0);
	 r1.setCityDeliveryCharge(0);
	 r1.setThreshold(0);
	 r1.setDiscountType(0);
	 r1.setTradeCampaignHeaders(new ArrayList());
	 r1.setSelect("false");
	 r1.setNew("false");
	 
	 Category cat1= new Category();
	 List<Category> catList1=new ArrayList<>();
	 RMSTDServicesTestDP catid1 = new RMSTDServicesTestDP();
	 List<Map<String, Object>> catname1=new ArrayList<>();
	 catname1 = catid1.cmshelper.getCategories(Integer.toString(Percentage_restaurants));
	 
	 for(int i=0;i<1;i++) {
		 String catg1 = catname1.get(i).get("cat_id").toString();
		 cat1.setId(catg1);
		 String catgname1 = catname1.get(i).get("cat_name").toString();
		 cat1.setName(catgname1);
	 }
	 
	 catList1.add(cat1);
	 r1.setCategories(catList1);

	 restList1.add(r1);
	 RuleDiscount rd=new RuleDiscount();
	 rd.setType("Percentage");
	 rd.setDiscountLevel("Category");
	 rd.setPercentDiscount(0);
	 rd.setMinCartAmount(0);
	 rd.setPercentDiscountAbsoluteCap(0);
	 
	 RMSCreateTDEntry entry1=new RMSTDBuilder().
				setEnabled("true").
				setRestaurantIds(restIds1).
				setValidFrom(Long.valueOf(DateUtils.addHours(new Date(), 0).toInstant().toEpochMilli())).
				setValidTill(Long.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().toEpochMilli())).
				setCampaignType("Percentage").setRuleDiscountType("Percentage").
				setCreatedBy(9886379321l).setDiscountLevel("Category").
				setRestaurantList(restList1).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
				setUserRestriction("false").setRestaurantFirstOrder("false").
				setRuleDiscount(rd).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
				setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();
	 

	 ArrayList<Integer> restIds3=new ArrayList<>();
	 restIds3.add(Percentage_restaurants);
	 ArrayList<Restaurant> restList3 =new ArrayList<>();
	 Restaurant r3=new Restaurant();
	 r3.setId(Percentage_restaurants);
	 r3.setName("Myramzi");
	 r3.setCity(219);
	 r3.setArea("MyRamziArea");
	 r3.setRecommended(0);
	 r3.setCostForTwo(0);
	 r3.setDeliveryCharge(0);
	 r3.setMinimumOrder(0);
	 r3.setOpened(0);
	 r3.setDeliveryTime(0);
	 r3.setTmpClosed(0);
	 r3.setPostalCode(0);
	 r3.setCityDeliveryCharge(0);
	 r3.setThreshold(0);
	 r3.setDiscountType(0);
	 r3.setTradeCampaignHeaders(new ArrayList());
	 r3.setSelect("false");
	 r3.setNew("false");

	 Category cat3= new Category();
	 List<Category> catList3=new ArrayList<>();
	 RMSTDServicesTestDP catid3 = new RMSTDServicesTestDP();
	 List<Map<String, Object>> catname3=new ArrayList<>();
	 catname3 = catid3.cmshelper.getCategories(Integer.toString(Percentage_restaurants));
	 
	 for(int i=0;i<1;i++) {
		 String catg3 = catname3.get(i).get("cat_id").toString();
		 cat3.setId(catg3);
		 String catgname3 = catname3.get(i).get("cat_name").toString();
		 cat3.setName(catgname3);
	 }
	 
	 catList3.add(cat3);
	 r3.setCategories(catList3);

	 r3.setCategories(new ArrayList());
	 restList3.add(r3);
	 RuleDiscount rd3=new RuleDiscount();
	 rd3.setType("Percentage");
	 rd3.setDiscountLevel("Category");
	 rd3.setPercentDiscount(60);
	 rd3.setMinCartAmount(0);
	 rd3.setPercentDiscountAbsoluteCap(0);
	 
	 RMSCreateTDEntry entry3=new RMSTDBuilder().
				setEnabled("true").
				setRestaurantIds(restIds3).
				setValidFrom(Long.valueOf(DateUtils.addHours(new Date(), 10).toInstant().toEpochMilli())).
				setValidTill(Long.valueOf(DateUtils.addMinutes(new Date(), 15).toInstant().toEpochMilli())).
				setCampaignType("Percentage").setRuleDiscountType("Percentage").
				setCreatedBy(9886379321l).setDiscountLevel("Category").
				setRestaurantList(restList3).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
				setUserRestriction("false").setRestaurantFirstOrder("false").
				setRuleDiscount(rd3).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
				setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();
	 
	 
	 ArrayList<Integer> restIds7=new ArrayList<>();
	 restIds7.add(Percentage_restaurants);
	 ArrayList<Restaurant> restList7 =new ArrayList<>();
	 Restaurant r7=new Restaurant();
	 r7.setId(Percentage_restaurants);
	 r7.setName("Myramzi");
	 r7.setCity(219);
	 r7.setArea("MyRamziArea");
	 r7.setRecommended(0);
	 r7.setCostForTwo(0);
	 r7.setDeliveryCharge(0);
	 r7.setMinimumOrder(0);
	 r7.setOpened(0);
	 r7.setDeliveryTime(0);
	 r7.setTmpClosed(0);
	 r7.setPostalCode(0);
	 r7.setCityDeliveryCharge(0);
	 r7.setThreshold(0);
	 r7.setDiscountType(0);
	 r7.setTradeCampaignHeaders(new ArrayList());
	 r7.setSelect("false");
	 r7.setNew("false");

	 Category cat7= new Category();
	 List<Category> catList7=new ArrayList<>();
	 RMSTDServicesTestDP catid7 = new RMSTDServicesTestDP();
	 List<Map<String, Object>> catname7=new ArrayList<>();
	 catname7 = catid7.cmshelper.getCategories(Integer.toString(Percentage_restaurants));
	 
	 for(int i=0;i<1;i++) {
		 String catg7 = catname7.get(i).get("cat_id").toString();
		 cat7.setId(catg7);
		 String catgname7 = catname7.get(i).get("cat_name").toString();
		 cat7.setName(catgname7);
	 }
	 
	 catList7.add(cat7);
	 r7.setCategories(catList7);

	 restList7.add(r7);
	 RuleDiscount rd7=new RuleDiscount();
	 rd7.setType("Percentage");
	 rd7.setDiscountLevel("Category");
	 rd7.setPercentDiscount(-2);
	 rd7.setMinCartAmount(0);
	 rd7.setPercentDiscountAbsoluteCap(0);
	 
	 RMSCreateTDEntry entry7=new RMSTDBuilder().
				setEnabled("true").
				setRestaurantIds(restIds7).
				setValidFrom(Long.valueOf(DateUtils.addHours(new Date(), 10).toInstant().toEpochMilli())).
				setValidTill(Long.valueOf(DateUtils.addMinutes(new Date(), 15).toInstant().toEpochMilli())).
				setCampaignType("Percentage").setRuleDiscountType("Percentage").
				setCreatedBy(9886379321l).setDiscountLevel("Category").
				setRestaurantList(restList7).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
				setUserRestriction("false").setRestaurantFirstOrder("false").
				setRuleDiscount(rd3).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
				setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();
	 
	 
	 
		return new Object[][] {{jsonHelper.getObjectToJSON(entry1),accessToken},
			{jsonHelper.getObjectToJSON(entry3),accessToken},
			{jsonHelper.getObjectToJSON(entry7),accessToken}
		};
		
	}
		

	 
	 
	 @DataProvider(name="createCatTD2")
		public static Object[][] createCatTDData2() throws IOException
		{
			JsonHelper jsonHelper = new JsonHelper();
		
	 ArrayList<Integer> restIds2=new ArrayList<>();
	 restIds2.add(Flat_Restaurants);
	 ArrayList<Restaurant> restList2 =new ArrayList<>();
	 Restaurant r2=new Restaurant();
	 r2.setId(Flat_Restaurants);
	 r2.setName("Myramzi2");
	 r2.setCity(219);
	 r2.setArea("MyRamziArea2");
	 r2.setRecommended(0);
	 r2.setCostForTwo(0);
	 r2.setDeliveryCharge(0);
	 r2.setMinimumOrder(0);
	 r2.setOpened(0);
	 r2.setDeliveryTime(0);
	 r2.setTmpClosed(0);
	 r2.setPostalCode(0);
	 r2.setCityDeliveryCharge(0);
	 r2.setThreshold(0);
	 r2.setDiscountType(0);
	 r2.setTradeCampaignHeaders(new ArrayList());
	 r2.setSelect("false");
	 r2.setNew("false");
	 
	 Category cat2= new Category();
	 List<Category> catList2=new ArrayList<>();
	 RMSTDServicesTestDP catid2 = new RMSTDServicesTestDP();
	 List<Map<String, Object>> catname2=new ArrayList<>();
	 catname2 = catid2.cmshelper.getCategories(Integer.toString(Flat_Restaurants));
	
	 for(int i=0;i<1;i++) {
		 String catg21 = catname2.get(i).get("cat_id").toString();
		 cat2.setId(catg21);
		 String catgname21 = catname2.get(i).get("cat_name").toString();
		 cat2.setName(catgname21);
	 }
	 
	 catList2.add(cat2);	
	 cat2 = new Category();
	 
	 for(int i=0;i<2;i++) {
		 String catg22 = catname2.get(i).get("cat_id").toString();
		 cat2.setId(catg22);
		 String catgname22 = catname2.get(i).get("cat_name").toString();
		 cat2.setName(catgname22);
	 }
	 catList2.add(cat2);
	 r2.setCategories(catList2);		 

	 restList2.add(r2);
	 RuleDiscount rd2=new RuleDiscount();
	 rd2.setType("Flat");
	 rd2.setDiscountLevel("Category");
	 rd2.setFlatDiscountAmount(0);
	 rd2.setMinCartAmount(0);
	 
	 RMSCreateTDEntry entry2=new RMSTDBuilder().
				setEnabled("true").
				setRestaurantIds(restIds2).
				setValidFrom(Long.valueOf(DateUtils.addHours(new Date(), 0).toInstant().toEpochMilli())).
				setValidTill(Long.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().toEpochMilli())).
				setCampaignType("Flat").setRuleDiscountType("Flat").
				setCreatedBy(9886379321l).setDiscountLevel("Category").
				setRestaurantList(restList2).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
				setUserRestriction("false").setRestaurantFirstOrder("false").
				setRuleDiscount(rd2).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
				setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();

		 ArrayList<Integer> restIds4=new ArrayList<>();
		 restIds4.add(Flat_Restaurants);
		 ArrayList<Restaurant> restList4 =new ArrayList<>();
		 Restaurant r4=new Restaurant();
		 r4.setId(Flat_Restaurants);
		 r4.setName("Myramzi2");
		 r4.setCity(219);
		 r4.setArea("MyRamziArea2");
		 r4.setRecommended(0);
		 r4.setCostForTwo(0);
		 r4.setDeliveryCharge(0);
		 r4.setMinimumOrder(0);
		 r4.setOpened(0);
		 r4.setDeliveryTime(0);
		 r4.setTmpClosed(0);
		 r4.setPostalCode(0);
		 r4.setCityDeliveryCharge(0);
		 r4.setThreshold(0);
		 r4.setDiscountType(0);
		 r4.setTradeCampaignHeaders(new ArrayList());
		 r4.setSelect("false");
		 r4.setNew("false");
		 
		 Category cat4= new Category();
		 List<Category> catList4=new ArrayList<>();
		 RMSTDServicesTestDP catid4 = new RMSTDServicesTestDP();
		 List<Map<String, Object>> catname4=new ArrayList<>();
		 catname4 = catid4.cmshelper.getCategories(Integer.toString(Flat_Restaurants));
		
		 for(int i=0;i<1;i++) {
			 String catg41 = catname4.get(i).get("cat_id").toString();
			 cat4.setId(catg41);
			 String catgname41 = catname4.get(i).get("cat_name").toString();
			 cat4.setName(catgname41);
		 }
		 
		 catList4.add(cat4);	
		 cat4 = new Category();
		 
		 for(int i=0;i<2;i++) {
			 String catg42 = catname4.get(i).get("cat_id").toString();
			 cat4.setId(catg42);
			 String catgname42 = catname4.get(i).get("cat_name").toString();
			 cat4.setName(catgname42);
		 }
		 catList4.add(cat4);
		 r4.setCategories(catList4);		 

		 restList4.add(r4);
		 RuleDiscount rd4=new RuleDiscount();
		 rd4.setType("Flat");
		 rd4.setDiscountLevel("Restaurant");
		 rd4.setFlatDiscountAmount(500);
		 rd4.setMinCartAmount(100);
		  
		 RMSCreateTDEntry entry4=new RMSTDBuilder().
					setEnabled("true").
					setRestaurantIds(restIds4).
					setValidFrom(Long.valueOf(DateUtils.addHours(new Date(), 20).toInstant().toEpochMilli())).
					setValidTill(Long.valueOf(DateUtils.addMinutes(new Date(), 25).toInstant().toEpochMilli())).
					setCampaignType("Flat").setRuleDiscountType("Flat").
					setCreatedBy(9886379321l).setDiscountLevel("Restaurant").
					setRestaurantList(restList4).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
					setUserRestriction("false").setRestaurantFirstOrder("false").
					setRuleDiscount(rd4).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
					setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();

			return new Object[][] {
								{jsonHelper.getObjectToJSON(entry2),accessToken},
				                {jsonHelper.getObjectToJSON(entry4),accessToken}
			};
			
		}
		 
		 
		 
		 @DataProvider(name="createCatTD3")
			public static Object[][] createCatTDData3() throws IOException
			{
				JsonHelper jsonHelper = new JsonHelper();			
		 
		 ArrayList<Integer> restIds5=new ArrayList<>();
		 restIds5.add(Percentage_restaurants);
		 ArrayList<Restaurant> restList5 =new ArrayList<>();
		 Restaurant r5=new Restaurant();
		 r5.setId(Percentage_restaurants);
		 r5.setName("Myramzi");
		 r5.setCity(219);
		 r5.setArea("MyRamziArea");
		 r5.setRecommended(0);
		 r5.setCostForTwo(0);
		 r5.setDeliveryCharge(0);
		 r5.setMinimumOrder(0);
		 r5.setOpened(0);
		 r5.setDeliveryTime(0);
		 r5.setTmpClosed(0);
		 r5.setPostalCode(0);
		 r5.setCityDeliveryCharge(0);
		 r5.setThreshold(0);
		 r5.setDiscountType(0);
		 r5.setTradeCampaignHeaders(new ArrayList());
		 r5.setSelect("false");
		 r5.setNew("false");

		 Category cat5= new Category();
		 List<Category> catList5=new ArrayList<>();
		 RMSTDServicesTestDP catid5 = new RMSTDServicesTestDP();
		 List<Map<String, Object>> catname5=new ArrayList<>();
		 catname5 = catid5.cmshelper.getCategories(Integer.toString(Percentage_restaurants));
		 
		 for(int i=0;i<1;i++) {
			 String catg5 = catname5.get(i).get("cat_id").toString();
			 cat5.setId(catg5);
			 String catgname5 = catname5.get(i).get("cat_name").toString();
			 cat5.setName(catgname5);
		 }
		 
		 catList5.add(cat5);
		 r5.setCategories(catList5);

		 restList5.add(r5);
		 RuleDiscount rd5=new RuleDiscount();
		 rd5.setType("Percentage");
		 rd5.setDiscountLevel("Category");
		 rd5.setPercentDiscount(2);
		 rd5.setMinCartAmount(-100);
		 rd5.setPercentDiscountAbsoluteCap(0);
		 
		 RMSCreateTDEntry entry5=new RMSTDBuilder().
					setEnabled("true").
					setRestaurantIds(restIds5).
					setValidFrom(Long.valueOf(DateUtils.addHours(new Date(), 30).toInstant().toEpochMilli())).
					setValidTill(Long.valueOf(DateUtils.addMinutes(new Date(), 35).toInstant().toEpochMilli())).
					setCampaignType("Percentage").setRuleDiscountType("Percentage").
					setCreatedBy(9886379321l).setDiscountLevel("Category").
					setRestaurantList(restList5).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
					setUserRestriction("false").setRestaurantFirstOrder("false").
					setRuleDiscount(rd5).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
					setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();
	
	 
		return new Object[][] {
			{jsonHelper.getObjectToJSON(entry5),accessToken}
		};
	}

		 @DataProvider(name="editCatTD")
			public static Object[][] editCatTDData() throws IOException
			{
			 JsonHelper jsonHelper = new JsonHelper();
			
			 ArrayList<Integer> restIds1=new ArrayList<>();
			 restIds1.add(Percentage_restaurants);
			 ArrayList<Restaurant> restList1 =new ArrayList<>();
			 Restaurant r1=new Restaurant();
			 r1.setId(Percentage_restaurants);
			 r1.setName("Myramzi");
			 r1.setCity(219);
			 r1.setArea("MyRamziArea");
			 r1.setRecommended(0);
			 r1.setCostForTwo(0);
			 r1.setDeliveryCharge(0);
			 r1.setMinimumOrder(0);
			 r1.setOpened(0);
			 r1.setDeliveryTime(0);
			 r1.setTmpClosed(0);
			 r1.setPostalCode(0);
			 r1.setCityDeliveryCharge(0);
			 r1.setThreshold(0);
			 r1.setDiscountType(0);
			 r1.setTradeCampaignHeaders(new ArrayList());
			 r1.setSelect("false");
			 r1.setNew("false");
			 
			 Category cat1= new Category();
			 List<Category> catList1=new ArrayList<>();
			 RMSTDServicesTestDP catid1 = new RMSTDServicesTestDP();
			 List<Map<String, Object>> catname1=new ArrayList<>();
			 catname1 = catid1.cmshelper.getCategories(Integer.toString(Percentage_restaurants));
			 
			 for(int i=0;i<1;i++) {
				 String catg1 = catname1.get(i).get("cat_id").toString();
				 cat1.setId(catg1);
				 String catgname1 = catname1.get(i).get("cat_name").toString();
				 cat1.setName(catgname1);
			 }
			 
			 catList1.add(cat1);
			 r1.setCategories(catList1);

			 restList1.add(r1);
			 RuleDiscount rd=new RuleDiscount();
			 rd.setType("Percentage");
			 rd.setDiscountLevel("Category");
			 rd.setPercentDiscount(2);
			 rd.setMinCartAmount(0);
			 rd.setPercentDiscountAbsoluteCap(0);
			 
			 RMSCreateTDEntry entry1=new RMSTDBuilder().
						setEnabled("true").
						setRestaurantIds(restIds1).
						setValidFrom(Long.valueOf(DateUtils.addHours(new Date(), 0).toInstant().toEpochMilli())).
						setValidTill(Long.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().toEpochMilli())).
						setCampaignType("Percentage").setRuleDiscountType("Percentage").
						setUpdatedBy(9886379321l).setDiscountLevel("Category").
						setRestaurantList(restList1).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
						setUserRestriction("false").setRestaurantFirstOrder("false").
						setRuleDiscount(rd).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
						setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();
			 RMSTDHelper rmsTDHelper = new RMSTDHelper();
			 int count =0;
			 int discountId = -1;
			 do{
			 Processor p = rmsTDHelper.createTD(jsonHelper.getObjectToJSON(entry1).toString(), accessToken);
				if (p.ResponseValidator.GetNodeValueAsInt("statusCode") == -5) {
					String[] res = p.ResponseValidator.GetNodeValue("data.dataCat.data").split(" ");
					System.out.println("IDDDD" +res[6]);
					Processor p2 =RngHelper.disableEnableTDFromMarketingDashboard(res[6], "false");
					//System.out.println("RND Response" +p.ResponseValidator.GetNodeValue("data.dataRest.data"));
				} else {
					System.out.println(p.ResponseValidator.GetNodeValueAsInt("data.dataCat.data"));
				 discountId = p.ResponseValidator.GetNodeValueAsInt("data.dataCat.data");
					break;
				}
			} while (count++ < 5);
				
			 ArrayList<Integer> restIds2=new ArrayList<>();
			 
			 restIds2.add(Percentage_restaurants);
			 
			 ArrayList<Restaurant> restList2 =new ArrayList<>();
			 Restaurant r2=new Restaurant();
			 
			 r2.setId(Percentage_restaurants);
			 r2.setName("Myramzi");
			 r2.setCity(219);
			 r2.setArea("MyRamziArea");
			 r2.setRecommended(0);
			 r2.setCostForTwo(0);
			 r2.setDeliveryCharge(0);
			 r2.setMinimumOrder(0);
			 r2.setOpened(0);
			 r2.setDeliveryTime(0);
			 r2.setTmpClosed(0);
			 r2.setPostalCode(0);
			 r2.setCityDeliveryCharge(0);
			 r2.setThreshold(0);
			 r2.setDiscountType(0);
			 r2.setTradeCampaignHeaders(new ArrayList());
			 r2.setSelect("false");
			 r2.setNew("false");

			 Category cat2= new Category();
			 List<Category> catList2=new ArrayList<>();
			 RMSTDServicesTestDP catid2 = new RMSTDServicesTestDP();
			 List<Map<String, Object>> catname2=new ArrayList<>();
			 catname2 = catid2.cmshelper.getCategories(Integer.toString(Percentage_restaurants));
			
			 for(int i=0;i<1;i++) {
				 String catg21 = catname2.get(i).get("cat_id").toString();
				 cat2.setId(catg21);
				 String catgname21 = catname2.get(i).get("cat_name").toString();
				 cat2.setName(catgname21);
			 }
			 
			 catList2.add(cat2);	
			 cat2 = new Category();
			 
			 for(int i=0;i<2;i++) {
				 String catg22 = catname2.get(i).get("cat_id").toString();
				 cat2.setId(catg22);
				 String catgname22 = catname2.get(i).get("cat_name").toString();
				 cat2.setName(catgname22);
			 }
			 catList2.add(cat2);
			 r2.setCategories(catList2);		 

			 restList2.add(r2);
			 RuleDiscount rd2=new RuleDiscount();
			 rd2.setType("Percentage");
			 rd2.setDiscountLevel("Category");
			 rd2.setPercentDiscount(4);
			 rd2.setMinCartAmount(0);
			 rd2.setPercentDiscountAbsoluteCap(0);
			 
			 RMSUpdateTDEntry entry2=new RMSEditTDBuilder().
						setEnabled("true").
						setRestaurantIds(restIds2).setId(discountId).
						//setId(12346).	
						setValidFrom(Long.valueOf(DateUtils.addHours(new Date(), 0).toInstant().toEpochMilli())).
						setValidTill(Long.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().toEpochMilli())).
						setCampaignType("Percentage").setRuleDiscountType("Percentage").
						setUpdatedBy(9886379321l).setDiscountLevel("Category").
						setRestaurantList(restList2).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
						setUserRestriction("false").setRestaurantFirstOrder("false").
						setRuleDiscount(rd2).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
						setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();

//				return new Object[][] {
//					{jsonHelper.getObjectToJSON(entry2),accessToken}
//				};	
//			}
//				
//			@DataProvider(name="editCatTD1")
//			public static Object[][] editCatTDData1() throws IOException
//			{
//			 JsonHelper jsonHelper = new JsonHelper();
							
			 ArrayList<Integer> restIds3=new ArrayList<>();
			 restIds3.add(Percentage_restaurants);
			 restIds3.add(Flat_Restaurants);
			 ArrayList<Restaurant> restList3 =new ArrayList<>();
			 Restaurant r3=new Restaurant();
			 Restaurant r4=new Restaurant();
			 
			 r3.setId(Percentage_restaurants);
			 r3.setName("Myramzi");
			 r3.setCity(219);
			 r3.setArea("MyRamziArea");
			 r3.setRecommended(0);
			 r3.setCostForTwo(0);
			 r3.setDeliveryCharge(0);
			 r3.setMinimumOrder(0);
			 r3.setOpened(0);
			 r3.setDeliveryTime(0);
			 r3.setTmpClosed(0);
			 r3.setPostalCode(0);
			 r3.setCityDeliveryCharge(0);
			 r3.setThreshold(0);
			 r3.setDiscountType(0);
			 r3.setTradeCampaignHeaders(new ArrayList());
			 r3.setSelect("false");
			 r3.setNew("false");
			 
			 Category cat3= new Category();
			 List<Category> catList3=new ArrayList<>();
			 RMSTDServicesTestDP catid3 = new RMSTDServicesTestDP();
			 List<Map<String, Object>> catname3=new ArrayList<>();
			 catname3 = catid3.cmshelper.getCategories(Integer.toString(Percentage_restaurants));
			 
			 for(int i=0;i<1;i++) {
				 String catg3 = catname3.get(i).get("cat_id").toString();
				 cat3.setId(catg3);
				 String catgname3 = catname3.get(i).get("cat_name").toString();
				 cat3.setName(catgname3);
			 }
			 
			 catList3.add(cat3);
			 r3.setCategories(catList3);

			 restList3.add(r3);

			 r4.setId(Flat_Restaurants);
			 r4.setName("Myramzi");
			 r4.setCity(219);
			 r4.setArea("MyRamziArea");
			 r4.setRecommended(0);
			 r4.setCostForTwo(0);
			 r4.setDeliveryCharge(0);
			 r4.setMinimumOrder(0);
			 r4.setOpened(0);
			 r4.setDeliveryTime(0);
			 r4.setTmpClosed(0);
			 r4.setPostalCode(0);
			 r4.setCityDeliveryCharge(0);
			 r4.setThreshold(0);
			 r4.setDiscountType(0);
			 r4.setTradeCampaignHeaders(new ArrayList());
			 r4.setSelect("false");
			 r4.setNew("false");
			 
			 Category cat4= new Category();
			 List<Category> catList4=new ArrayList<>();
			 RMSTDServicesTestDP catid4 = new RMSTDServicesTestDP();
			 List<Map<String, Object>> catname4=new ArrayList<>();
			 catname4 = catid4.cmshelper.getCategories(Integer.toString(Flat_Restaurants));
			 
			 for(int i=0;i<1;i++) {
				 String catg4 = catname4.get(i).get("cat_id").toString();
				 cat4.setId(catg4);
				 String catgname4 = catname4.get(i).get("cat_name").toString();
				 cat4.setName(catgname4);
			 }
			 
			 catList4.add(cat4);
			 r4.setCategories(catList4);

			 restList3.add(r4);
			 
			 RuleDiscount rd3=new RuleDiscount();
			 rd3.setType("Percentage");
			 rd3.setDiscountLevel("Category");
			 rd3.setPercentDiscount(2);
			 rd3.setMinCartAmount(0);
			 rd3.setPercentDiscountAbsoluteCap(0);
			 
			 RMSCreateTDEntry entry3=new RMSTDBuilder().
						setEnabled("true").
						setRestaurantIds(restIds3).
						setValidFrom(Long.valueOf(DateUtils.addDays(new Date(), 2).toInstant().toEpochMilli())).
						setValidTill(Long.valueOf(DateUtils.addDays(new Date(), 3).toInstant().toEpochMilli())).
						setCampaignType("Percentage").setRuleDiscountType("Percentage").
						setUpdatedBy(9886379321l).setDiscountLevel("Category").
						setRestaurantList(restList3).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
						setUserRestriction("false").setRestaurantFirstOrder("false").
						setRuleDiscount(rd3).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
						setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();
			 RMSTDHelper rmsTDHelper1 = new RMSTDHelper();
			 int count1 =0;
			 int discountId1 = -1;
			 do{
			 Processor p = rmsTDHelper1.createTD(jsonHelper.getObjectToJSON(entry3).toString(), accessToken);
				if (p.ResponseValidator.GetNodeValueAsInt("statusCode") == -5) {
					String[] res = p.ResponseValidator.GetNodeValue("data.dataCat.data").split(" ");
					System.out.println("IDDDD" +res[6]);
					Processor p2 =RngHelper.disableEnableTDFromMarketingDashboard(res[5], "false");
					
				} else {
					System.out.println(p.ResponseValidator.GetNodeValueAsInt("data.dataCat.data"));
				 discountId1 = p.ResponseValidator.GetNodeValueAsInt("data.dataCat.data");
					break;
				}
			} while (count1++ < 5);
				
			
			 ArrayList<Integer> restIds5=new ArrayList<>();
			 restIds5.add(Percentage_restaurants);
			 ArrayList<Restaurant> restList5 =new ArrayList<>();
			 Restaurant r5=new Restaurant();
			 r5.setId(Percentage_restaurants);
			 r5.setName("Myramzi");
			 r5.setCity(219);
			 r5.setArea("MyRamziArea");
			 r5.setRecommended(0);
			 r5.setCostForTwo(0);
			 r5.setDeliveryCharge(0);
			 r5.setMinimumOrder(0);
			 r5.setOpened(0);
			 r5.setDeliveryTime(0);
			 r5.setTmpClosed(0);
			 r5.setPostalCode(0);
			 r5.setCityDeliveryCharge(0);
			 r5.setThreshold(0);
			 r5.setDiscountType(0);
			 r5.setTradeCampaignHeaders(new ArrayList());
			 r5.setSelect("false");
			 r5.setNew("false");
			 
			 Category cat5= new Category();
			 List<Category> catList5=new ArrayList<>();
			 RMSTDServicesTestDP catid5 = new RMSTDServicesTestDP();
			 List<Map<String, Object>> catname5=new ArrayList<>();
			 catname5 = catid5.cmshelper.getCategories(Integer.toString(Percentage_restaurants));
			 
			 for(int i=0;i<1;i++) {
				 String catg5 = catname5.get(i).get("cat_id").toString();
				 cat5.setId(catg5);
				 String catgname5 = catname5.get(i).get("cat_name").toString();
				 cat5.setName(catgname5);
			 }
			 
			 catList5.add(cat5);
			 r5.setCategories(catList5);

			 restList5.add(r5);
			 RuleDiscount rd5=new RuleDiscount();
			 rd5.setType("Percentage");
			 rd5.setDiscountLevel("Category");
			 rd5.setPercentDiscount(2);
			 rd5.setMinCartAmount(0);
			 rd5.setPercentDiscountAbsoluteCap(0);
			 
			 RMSUpdateTDEntry entry5=new RMSEditTDBuilder().
						setEnabled("true").
						setRestaurantIds(restIds5).setId(discountId1).
						//setId(12346).	
						setValidFrom(Long.valueOf(DateUtils.addDays(new Date(), 2).toInstant().toEpochMilli())).
						setValidTill(Long.valueOf(DateUtils.addDays(new Date(), 3).toInstant().toEpochMilli())).
						setCampaignType("Percentage").setRuleDiscountType("Percentage").
						setUpdatedBy(9886379321l).setDiscountLevel("Category").
						setRestaurantList(restList5).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
						setUserRestriction("false").setRestaurantFirstOrder("false").
						setRuleDiscount(rd5).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
						setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();

//
//				return new Object[][] {
//					{jsonHelper.getObjectToJSON(entry2),accessToken},
//					{jsonHelper.getObjectToJSON(entry5),accessToken}
//				};	
//			}
//			 
//				@DataProvider(name="editCatTD2")
//				public static Object[][] editCatTDData2() throws IOException
//				{
//				 JsonHelper jsonHelper = new JsonHelper();
				
				 ArrayList<Integer> restIds6=new ArrayList<>();
				 restIds6.add(Percentage_restaurants);
				 ArrayList<Restaurant> restList6 =new ArrayList<>();
				 Restaurant r6=new Restaurant();
				 r6.setId(Percentage_restaurants);
				 r6.setName("Myramzi");
				 r6.setCity(219);
				 r6.setArea("MyRamziArea");
				 r6.setRecommended(0);
				 r6.setCostForTwo(0);
				 r6.setDeliveryCharge(0);
				 r6.setMinimumOrder(0);
				 r6.setOpened(0);
				 r6.setDeliveryTime(0);
				 r6.setTmpClosed(0);
				 r6.setPostalCode(0);
				 r6.setCityDeliveryCharge(0);
				 r6.setThreshold(0);
				 r6.setDiscountType(0);
				 r6.setTradeCampaignHeaders(new ArrayList());
				 r6.setSelect("false");
				 r6.setNew("false");
				 r6.setCategories(new ArrayList());
				 restList6.add(r6);
				 RuleDiscount rd6=new RuleDiscount();
				 rd6.setType("Percentage");
				 rd6.setDiscountLevel("Restaurant");
				 rd6.setPercentDiscount(2);
				 rd6.setMinCartAmount(0);
				 rd6.setPercentDiscountAbsoluteCap(0);
				 
				 RMSCreateTDEntry entry6=new RMSTDBuilder().
							setEnabled("true").
							setRestaurantIds(restIds6).
							setValidFrom(Long.valueOf(DateUtils.addHours(new Date(), 2).toInstant().toEpochMilli())).
							setValidTill(Long.valueOf(DateUtils.addHours(new Date(), 3).toInstant().toEpochMilli())).
							setCampaignType("Percentage").setRuleDiscountType("Percentage").
							setUpdatedBy(9886379321l).setDiscountLevel("Restaurant").
							setRestaurantList(restList6).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
							setUserRestriction("false").setRestaurantFirstOrder("false").
							setRuleDiscount(rd6).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
							setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();
				 RMSTDHelper rmsTDHelper6 = new RMSTDHelper();
				 int count6 =0;
				 int discountId6 = -1;
				 do{
				 Processor p = rmsTDHelper6.createTD(jsonHelper.getObjectToJSON(entry6).toString(), accessToken);
					if (p.ResponseValidator.GetNodeValueAsInt("statusCode") == -5) {
						String[] res = p.ResponseValidator.GetNodeValue("data.dataRest.data").split(" ");
						System.out.println("IDDDD" +res[6]);
						Processor p2 =RngHelper.disableEnableTDFromMarketingDashboard(res[6], "false");
					} else {
						System.out.println(p.ResponseValidator.GetNodeValueAsInt("data.dataRest.data"));
					 discountId6 = p.ResponseValidator.GetNodeValueAsInt("data.dataRest.data");
						break;
					}
				} while (count6++ < 5);

			 ArrayList<Integer> restIds7=new ArrayList<>();
			 restIds7.add(Percentage_restaurants);
			 ArrayList<Restaurant> restList7 =new ArrayList<>();
			 Restaurant r7=new Restaurant();
			 r7.setId(Percentage_restaurants);
			 r7.setName("Myramzi");
			 r7.setCity(219);
			 r7.setArea("MyRamziArea");
			 r7.setRecommended(0);
			 r7.setCostForTwo(0);
			 r7.setDeliveryCharge(0);
			 r7.setMinimumOrder(0);
			 r7.setOpened(0);
			 r7.setDeliveryTime(0);
			 r7.setTmpClosed(0);
			 r7.setPostalCode(0);
			 r7.setCityDeliveryCharge(0);
			 r7.setThreshold(0);
			 r7.setDiscountType(0);
			 r7.setTradeCampaignHeaders(new ArrayList());
			 r7.setSelect("false");
			 r7.setNew("false");
			
			 Category cat7= new Category();
			 List<Category> catList7=new ArrayList<>();
			 RMSTDServicesTestDP catid7 = new RMSTDServicesTestDP();
			 List<Map<String, Object>> catname7=new ArrayList<>();
			 catname7 = catid7.cmshelper.getCategories(Integer.toString(Percentage_restaurants));
			 
			 for(int i=0;i<1;i++) {
				 String catg7 = catname7.get(i).get("cat_id").toString();
				 cat7.setId(catg7);
				 String catgname7 = catname7.get(i).get("cat_name").toString();
				 cat7.setName(catgname7);
			 }
			 
			 catList7.add(cat7);
			 r7.setCategories(catList7);

			 restList7.add(r7);
			 RuleDiscount rd7=new RuleDiscount();
			 rd7.setType("Percentage");
			 rd7.setDiscountLevel("Category");
			 rd7.setPercentDiscount(2);
			 rd7.setMinCartAmount(0);
			 rd7.setPercentDiscountAbsoluteCap(0);
			 
			 RMSUpdateTDEntry entry7=new RMSEditTDBuilder().
						setEnabled("true").
						setRestaurantIds(restIds7).setId(discountId6).
						//setId(12346).	
						setValidFrom(Long.valueOf(DateUtils.addHours(new Date(), 2).toInstant().toEpochMilli())).
						setValidTill(Long.valueOf(DateUtils.addHours(new Date(), 6).toInstant().toEpochMilli())).
						setCampaignType("Percentage").setRuleDiscountType("Percentage").
						setUpdatedBy(9886379321l).setDiscountLevel("Category").
						setRestaurantList(restList7).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
						setUserRestriction("false").setRestaurantFirstOrder("false").
						setRuleDiscount(rd7).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
						setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();

//			 return new Object[][] {
//	    			{jsonHelper.getObjectToJSON(entry2),accessToken},
//					{jsonHelper.getObjectToJSON(entry5),accessToken},
//					{jsonHelper.getObjectToJSON(entry7),accessToken}
//				};	
//			}
//
			 

//
//				@DataProvider(name="editCatTD4")
//				public static Object[][] editCatTDData4() throws IOException
//				{
//				 JsonHelper jsonHelper = new JsonHelper();
				
				 ArrayList<Integer> restIds10=new ArrayList<>();
				 restIds10.add(Percentage_restaurants);
				 ArrayList<Restaurant> restList10 =new ArrayList<>();
				 Restaurant r10=new Restaurant();
				 r10.setId(Percentage_restaurants);
				 r10.setName("Myramzi");
				 r10.setCity(219);
				 r10.setArea("MyRamziArea");
				 r10.setRecommended(0);
				 r10.setCostForTwo(0);
				 r10.setDeliveryCharge(0);
				 r10.setMinimumOrder(0);
				 r10.setOpened(0);
				 r10.setDeliveryTime(0);
				 r10.setTmpClosed(0);
				 r10.setPostalCode(0);
				 r10.setCityDeliveryCharge(0);
				 r10.setThreshold(0);
				 r10.setDiscountType(0);
				 r10.setTradeCampaignHeaders(new ArrayList());
				 r10.setSelect("false");
				 r10.setNew("false");
				 
				 Category cat10= new Category();
				 List<Category> catList10=new ArrayList<>();
				 RMSTDServicesTestDP catid10 = new RMSTDServicesTestDP();
				 List<Map<String, Object>> catname10=new ArrayList<>();
				 catname10 = catid10.cmshelper.getCategories(Integer.toString(Percentage_restaurants));
				 
				 for(int i=0;i<1;i++) {
					 String catg10 = catname10.get(i).get("cat_id").toString();
					 cat10.setId(catg10);
					 String catgname10 = catname10.get(i).get("cat_name").toString();
					 cat10.setName(catgname10);
				 }
				 
				 catList10.add(cat10);
				 r10.setCategories(catList10);

				 restList10.add(r10);
				 RuleDiscount rd10=new RuleDiscount();
				 rd10.setType("Percentage");
				 rd10.setDiscountLevel("Category");
				 rd10.setPercentDiscount(2);
				 rd10.setMinCartAmount(0);
				 rd10.setPercentDiscountAbsoluteCap(0);
				 
				 RMSCreateTDEntry entry10=new RMSTDBuilder().
							setEnabled("true").
							setRestaurantIds(restIds10).
							setValidFrom(Long.valueOf(DateUtils.addHours(new Date(), 0).toInstant().toEpochMilli())).
							setValidTill(Long.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().toEpochMilli())).
							setCampaignType("Percentage").setRuleDiscountType("Percentage").
							setUpdatedBy(9886379321l).setDiscountLevel("Category").
							setRestaurantList(restList10).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
							setUserRestriction("false").setRestaurantFirstOrder("false").
							setRuleDiscount(rd10).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
							setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();
				 RMSTDHelper rmsTDHelper10 = new RMSTDHelper();
				 int count10 =0;
				 int discountId10 = -1;
				 do{
				 Processor p = rmsTDHelper10.createTD(jsonHelper.getObjectToJSON(entry1).toString(), accessToken);
					if (p.ResponseValidator.GetNodeValueAsInt("statusCode") == -5) {
						String[] res = p.ResponseValidator.GetNodeValue("data.dataCat.data").split(" ");
						System.out.println("IDDDD" +res[6]);
						Processor p2 =RngHelper.disableEnableTDFromMarketingDashboard(res[6], "false");
					} else {
						System.out.println(p.ResponseValidator.GetNodeValueAsInt("data.dataCat.data"));
					 discountId10 = p.ResponseValidator.GetNodeValueAsInt("data.dataCat.data");
						break;
					}
				} while (count10++ < 5);

			 ArrayList<Integer> restIds11=new ArrayList<>();
			 restIds11.add(Flat_Restaurants);
			 ArrayList<Restaurant> restList11 =new ArrayList<>();
			 Restaurant r11=new Restaurant();
			 r11.setId(Flat_Restaurants);
			 r11.setName("Myramzi");
			 r11.setCity(219);
			 r11.setArea("MyRamziArea");
			 r11.setRecommended(0);
			 r11.setCostForTwo(0);
			 r11.setDeliveryCharge(0);
			 r11.setMinimumOrder(0);
			 r11.setOpened(0);
			 r11.setDeliveryTime(0);
			 r11.setTmpClosed(0);
			 r11.setPostalCode(0);
			 r11.setCityDeliveryCharge(0);
			 r11.setThreshold(0);
			 r11.setDiscountType(0);
			 r11.setTradeCampaignHeaders(new ArrayList());
			 r11.setSelect("false");
			 r11.setNew("false");
			 
			 Category cat11= new Category();
			 List<Category> catList11=new ArrayList<>();
			 RMSTDServicesTestDP catid11 = new RMSTDServicesTestDP();
			 List<Map<String, Object>> catname11=new ArrayList<>();
			 catname11 = catid11.cmshelper.getCategories(Integer.toString(Flat_Restaurants));
			
			 for(int i=0;i<1;i++) {
				 String catg111 = catname11.get(i).get("cat_id").toString();
				 cat11.setId(catg111);
				 String catgname111 = catname11.get(i).get("cat_name").toString();
				 cat11.setName(catgname111);
			 }
			 
			 catList11.add(cat11);	
			 cat11 = new Category();
			 
			 for(int i=0;i<2;i++) {
				 String catg112 = catname11.get(i).get("cat_id").toString();
				 cat11.setId(catg112);
				 String catgname112 = catname11.get(i).get("cat_name").toString();
				 cat11.setName(catgname112);
			 }
			 catList11.add(cat11);
			 r11.setCategories(catList11);		 

			 restList11.add(r11);
			 RuleDiscount rd11=new RuleDiscount();
			 rd11.setType("Flat");
			 rd11.setDiscountLevel("Category");
			 rd11.setFlatDiscountAmount(10);
			 rd11.setMinCartAmount(0);
			 rd11.setPercentDiscountAbsoluteCap(0);

			 RMSUpdateTDEntry entry11=new RMSEditTDBuilder().
						setEnabled("true").
						setRestaurantIds(restIds11).setId(discountId10).
						//setId(12346).	
						setValidFrom(Long.valueOf(DateUtils.addHours(new Date(), 0).toInstant().toEpochMilli())).
						setValidTill(Long.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().toEpochMilli())).
						setCampaignType("Flat").setRuleDiscountType("Flat").
						setUpdatedBy(9886379321l).setDiscountLevel("Category").
						setRestaurantList(restList11).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
						setUserRestriction("false").setRestaurantFirstOrder("false").
						setRuleDiscount(rd11).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
						setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();

//					return new Object[][] {
//		    			{jsonHelper.getObjectToJSON(entry2),accessToken},
//						{jsonHelper.getObjectToJSON(entry5),accessToken},
//						{jsonHelper.getObjectToJSON(entry7),accessToken},
//						{jsonHelper.getObjectToJSON(entry9),accessToken},	
//    					{jsonHelper.getObjectToJSON(entry11),accessToken}
//					};
//			 
//		    }
//
//				@DataProvider(name="editCatTD5")
//				public static Object[][] editCatTDData5() throws IOException
//				{
//				 JsonHelper jsonHelper = new JsonHelper();
				
				 ArrayList<Integer> restIds12=new ArrayList<>();
				 restIds12.add(Percentage_restaurants);
				 ArrayList<Restaurant> restList12 =new ArrayList<>();
				 Restaurant r12=new Restaurant();
				 r12.setId(Percentage_restaurants);
				 r12.setName("Myramzi");
				 r12.setCity(219);
				 r12.setArea("MyRamziArea");
				 r12.setRecommended(0);
				 r12.setCostForTwo(0);
				 r12.setDeliveryCharge(0);
				 r12.setMinimumOrder(0);
				 r12.setOpened(0);
				 r12.setDeliveryTime(0);
				 r12.setTmpClosed(0);
				 r12.setPostalCode(0);
				 r12.setCityDeliveryCharge(0);
				 r12.setThreshold(0);
				 r12.setDiscountType(0);
				 r12.setTradeCampaignHeaders(new ArrayList());
				 r12.setSelect("false");
				 r12.setNew("false");
				 
				 Category cat12= new Category();
				 List<Category> catList12=new ArrayList<>();
				 RMSTDServicesTestDP catid12 = new RMSTDServicesTestDP();
				 List<Map<String, Object>> catname12=new ArrayList<>();
				 catname12 = catid12.cmshelper.getCategories(Integer.toString(Percentage_restaurants));
				
				 for(int i=0;i<1;i++) {
					 String catg121 = catname12.get(i).get("cat_id").toString();
					 cat12.setId(catg121);
					 String catgname121 = catname12.get(i).get("cat_name").toString();
					 cat12.setName(catgname121);
				 }
				 
				 catList12.add(cat12);	
				 cat12 = new Category();
				 
				 for(int i=0;i<2;i++) {
					 String catg122 = catname12.get(i).get("cat_id").toString();
					 cat12.setId(catg122);
					 String catgname122 = catname12.get(i).get("cat_name").toString();
					 cat1.setName(catgname122);
				 }
				 catList12.add(cat12);
				 r12.setCategories(catList12);		 

				 restList12.add(r12);
				 RuleDiscount rd12=new RuleDiscount();
				 rd12.setType("Percentage");
				 rd12.setDiscountLevel("Category");
				 rd12.setPercentDiscount(2);
				 rd12.setMinCartAmount(0);
				 rd12.setPercentDiscountAbsoluteCap(0);
				 
				 RMSCreateTDEntry entry12=new RMSTDBuilder().
							setEnabled("true").
							setRestaurantIds(restIds12).
							setValidFrom(Long.valueOf(DateUtils.addHours(new Date(), 0).toInstant().toEpochMilli())).
							setValidTill(Long.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().toEpochMilli())).
							setCampaignType("Percentage").setRuleDiscountType("Percentage").
							setUpdatedBy(9886379321l).setDiscountLevel("Category").
							setRestaurantList(restList12).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
							setUserRestriction("false").setRestaurantFirstOrder("false").
							setRuleDiscount(rd12).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
							setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();
				 RMSTDHelper rmsTDHelper12 = new RMSTDHelper();
				 int count12 =0;
				 int discountId12 = -1;
				 do{
				 Processor p = rmsTDHelper12.createTD(jsonHelper.getObjectToJSON(entry12).toString(), accessToken);
					if (p.ResponseValidator.GetNodeValueAsInt("statusCode") == -5) {
						String[] res = p.ResponseValidator.GetNodeValue("data.dataCat.data").split(" ");
						System.out.println("IDDDD" +res[6]);
						Processor p2 =RngHelper.disableEnableTDFromMarketingDashboard(res[6], "false");
					} else {
						System.out.println(p.ResponseValidator.GetNodeValueAsInt("data.dataCat.data"));
					 discountId12 = p.ResponseValidator.GetNodeValueAsInt("data.dataCat.data");
						break;
					}
				} while (count12++ < 5);
					
				 ArrayList<Integer> restIds13=new ArrayList<>();
				 
				 restIds13.add(Percentage_restaurants);
				 
				 ArrayList<Restaurant> restList13 =new ArrayList<>();
				 Restaurant r13=new Restaurant();
				 
				 r13.setId(Percentage_restaurants);
				 r13.setName("Myramzi");
				 r13.setCity(219);
				 r13.setArea("MyRamziArea");
				 r13.setRecommended(0);
				 r13.setCostForTwo(0);
				 r13.setDeliveryCharge(0);
				 r13.setMinimumOrder(0);
				 r13.setOpened(0);
				 r13.setDeliveryTime(0);
				 r13.setTmpClosed(0);
				 r13.setPostalCode(0);
				 r13.setCityDeliveryCharge(0);
				 r13.setThreshold(0);
				 r13.setDiscountType(0);
				 r13.setTradeCampaignHeaders(new ArrayList());
				 r13.setSelect("false");
				 r13.setNew("false");
				 
				 Category cat13= new Category();
				 List<Category> catList13=new ArrayList<>();
				 RMSTDServicesTestDP catid13 = new RMSTDServicesTestDP();
				 List<Map<String, Object>> catname13=new ArrayList<>();
				 catname13 = catid13.cmshelper.getCategories(Integer.toString(Percentage_restaurants));
				 
				 for(int i=0;i<1;i++) {
					 String catg13 = catname13.get(i).get("cat_id").toString();
					 cat13.setId(catg13);
					 String catgname13 = catname13.get(i).get("cat_name").toString();
					 cat13.setName(catgname13);
				 }
				 
				 catList13.add(cat13);
				 r13.setCategories(catList13);

				 restList13.add(r13);
				 RuleDiscount rd13=new RuleDiscount();
				 rd13.setType("Percentage");
				 rd13.setDiscountLevel("Category");
				 rd13.setPercentDiscount(2);
				 rd13.setMinCartAmount(100);
				 rd13.setPercentDiscountAbsoluteCap(0);
				 
				 RMSUpdateTDEntry entry13=new RMSEditTDBuilder().
							setEnabled("true").
							setRestaurantIds(restIds13).setId(discountId12).
							setValidFrom(Long.valueOf(DateUtils.addHours(new Date(), 0).toInstant().toEpochMilli())).
							setValidTill(Long.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().toEpochMilli())).
							setCampaignType("Percentage").setRuleDiscountType("Percentage").
							setUpdatedBy(9886379321l).setDiscountLevel("Category").
							setRestaurantList(restList13).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
							setUserRestriction("false").setRestaurantFirstOrder("false").
							setRuleDiscount(rd13).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
							setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();
//
//					return new Object[][] {
//		    			{jsonHelper.getObjectToJSON(entry2),accessToken},
//							{jsonHelper.getObjectToJSON(entry5),accessToken},
//							{jsonHelper.getObjectToJSON(entry7),accessToken},
//							{jsonHelper.getObjectToJSON(entry9),accessToken},	
//	    					{jsonHelper.getObjectToJSON(entry11),accessToken},
//    						{jsonHelper.getObjectToJSON(entry13),accessToken}
//					};	
//				}
//
//				@DataProvider(name="editCatTD6")
//				public static Object[][] editCatTDData6() throws IOException
//				{
//				 JsonHelper jsonHelper = new JsonHelper();
//				
				 ArrayList<Integer> restIds14=new ArrayList<>();
				 restIds14.add(Percentage_restaurants);
				 ArrayList<Restaurant> restList14 =new ArrayList<>();
				 Restaurant r14=new Restaurant();
				 r14.setId(Percentage_restaurants);
				 r14.setName("Myramzi");
				 r14.setCity(219);
				 r14.setArea("MyRamziArea");
				 r14.setRecommended(0);
				 r14.setCostForTwo(0);
				 r14.setDeliveryCharge(0);
				 r14.setMinimumOrder(0);
				 r14.setOpened(0);
				 r14.setDeliveryTime(0);
				 r14.setTmpClosed(0);
				 r14.setPostalCode(0);
				 r14.setCityDeliveryCharge(0);
				 r14.setThreshold(0);
				 r14.setDiscountType(0);
				 r14.setTradeCampaignHeaders(new ArrayList());
				 r14.setSelect("false");
				 r14.setNew("false");
	 
				 Category cat14= new Category();
				 List<Category> catList14=new ArrayList<>();
				 RMSTDServicesTestDP catid14 = new RMSTDServicesTestDP();
				 List<Map<String, Object>> catname14=new ArrayList<>();
				 catname14 = catid14.cmshelper.getCategories(Integer.toString(Percentage_restaurants));
				
				 for(int i=0;i<1;i++) {
					 String catg141 = catname14.get(i).get("cat_id").toString();
					 cat14.setId(catg141);
					 String catgname141 = catname14.get(i).get("cat_name").toString();
					 cat14.setName(catgname141);
				 }
				 
				 catList14.add(cat14);	
				 cat14 = new Category();
				 
				 for(int i=0;i<2;i++) {
					 String catg142 = catname14.get(i).get("cat_id").toString();
					 cat14.setId(catg142);
					 String catgname142 = catname14.get(i).get("cat_name").toString();
					 cat14.setName(catgname142);
				 }
				 catList14.add(cat14);
				 r14.setCategories(catList14);		 

				 restList14.add(r14);
				 RuleDiscount rd14=new RuleDiscount();
				 rd14.setType("Percentage");
				 rd14.setDiscountLevel("Category");
				 rd14.setPercentDiscount(3);
				 rd14.setMinCartAmount(0);
				 rd14.setPercentDiscountAbsoluteCap(0);
				 
				 RMSCreateTDEntry entry14=new RMSTDBuilder().
							setEnabled("true").
							setRestaurantIds(restIds14).
							setValidFrom(Long.valueOf(DateUtils.addHours(new Date(), 0).toInstant().toEpochMilli())).
							setValidTill(Long.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().toEpochMilli())).
							setCampaignType("Percentage").setRuleDiscountType("Percentage").
							setUpdatedBy(9886379321l).setDiscountLevel("Category").
							setRestaurantList(restList14).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
							setUserRestriction("false").setRestaurantFirstOrder("false").
							setRuleDiscount(rd14).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
							setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();
				 RMSTDHelper rmsTDHelper14 = new RMSTDHelper();
				 int count14 =0;
				 int discountId14 = -1;
				 do{
				 Processor p = rmsTDHelper14.createTD(jsonHelper.getObjectToJSON(entry1).toString(), accessToken);
					if (p.ResponseValidator.GetNodeValueAsInt("statusCode") == -5) {
						String[] res = p.ResponseValidator.GetNodeValue("data.dataCat.data").split(" ");
						System.out.println("IDDDD" +res[5]);
						Processor p2 =RngHelper.disableEnableTDFromMarketingDashboard(res[5], "false");
					} else {
						System.out.println(p.ResponseValidator.GetNodeValueAsInt("data.dataCat.data"));
					 discountId14 = p.ResponseValidator.GetNodeValueAsInt("data.dataCat.data");
						break;
					}
				} while (count14++ < 5);
					
				 ArrayList<Integer> restIds15=new ArrayList<>();
				 
				 restIds15.add(Percentage_restaurants);
				 
				 ArrayList<Restaurant> restList15 =new ArrayList<>();
				 Restaurant r15=new Restaurant();
				 
				 r15.setId(Percentage_restaurants);
				 r15.setName("Myramzi");
				 r15.setCity(219);
				 r15.setArea("MyRamziArea");
				 r15.setRecommended(0);
				 r15.setCostForTwo(0);
				 r15.setDeliveryCharge(0);
				 r15.setMinimumOrder(0);
				 r15.setOpened(0);
				 r15.setDeliveryTime(0);
				 r15.setTmpClosed(0);
				 r15.setPostalCode(0);
				 r15.setCityDeliveryCharge(0);
				 r15.setThreshold(0);
				 r15.setDiscountType(0);
				 r15.setTradeCampaignHeaders(new ArrayList());
				 r15.setSelect("false");
				 r15.setNew("false");
				 
				 Category cat15= new Category();
				 List<Category> catList15=new ArrayList<>();
				 RMSTDServicesTestDP catid15 = new RMSTDServicesTestDP();
				 List<Map<String, Object>> catname15=new ArrayList<>();
				 catname15 = catid15.cmshelper.getCategories(Integer.toString(Percentage_restaurants));
				
				 for(int i=0;i<1;i++) {
					 String catg151 = catname15.get(i).get("cat_id").toString();
					 cat15.setId(catg151);
					 String catgname151 = catname15.get(i).get("cat_name").toString();
					 cat15.setName(catgname151);
				 }
				 
				 catList15.add(cat15);	
				 cat15 = new Category();
				 
				 for(int i=0;i<2;i++) {
					 String catg152 = catname15.get(i).get("cat_id").toString();
					 cat15.setId(catg152);
					 String catgname152 = catname15.get(i).get("cat_name").toString();
					 cat15.setName(catgname152);
				 }
				 catList15.add(cat15);
				 r15.setCategories(catList15);		 

				 restList15.add(r15);
				 RuleDiscount rd15=new RuleDiscount();
				 rd15.setType("Percentage");
				 rd15.setDiscountLevel("Category");
				 rd15.setPercentDiscount(3);
				 rd15.setMinCartAmount(0);
				 rd15.setPercentDiscountAbsoluteCap(0);
				 
				 RMSUpdateTDEntry entry15=new RMSEditTDBuilder().
							setEnabled("true").
							setRestaurantIds(restIds15).setId(discountId14).
							setValidFrom(Long.valueOf(DateUtils.addHours(new Date(), 0).toInstant().toEpochMilli())).
							setValidTill(Long.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().toEpochMilli())).
							setCampaignType("Percentage").setRuleDiscountType("Percentage").
							setUpdatedBy(9886379321l).setDiscountLevel("Category").
							setRestaurantList(restList15).setSlots(new ArrayList<>()).setFirstOrderRestriction("true").
							setUserRestriction("false").setRestaurantFirstOrder("false").
							setRuleDiscount(rd15).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
							setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();

					return new Object[][] {
    		    			{jsonHelper.getObjectToJSON(entry2),accessToken},
							{jsonHelper.getObjectToJSON(entry5),accessToken},
							{jsonHelper.getObjectToJSON(entry7),accessToken},
	    					{jsonHelper.getObjectToJSON(entry11),accessToken},
    						{jsonHelper.getObjectToJSON(entry13),accessToken},
    						{jsonHelper.getObjectToJSON(entry15),accessToken}
					};	
				}

			@DataProvider(name="editCatTD3")
			public static Object[][] editCatTDData3() throws IOException
			{
			 JsonHelper jsonHelper = new JsonHelper();
			
			 ArrayList<Integer> restIds8=new ArrayList<>();
			 restIds8.add(Percentage_restaurants);
			 ArrayList<Restaurant> restList8 =new ArrayList<>();
			 Restaurant r8=new Restaurant();
			 r8.setId(Percentage_restaurants);
			 r8.setName("Myramzi");
			 r8.setCity(219);
			 r8.setArea("MyRamziArea");
			 r8.setRecommended(0);
			 r8.setCostForTwo(0);
			 r8.setDeliveryCharge(0);
			 r8.setMinimumOrder(0);
			 r8.setOpened(0);
			 r8.setDeliveryTime(0);
			 r8.setTmpClosed(0);
			 r8.setPostalCode(0);
			 r8.setCityDeliveryCharge(0);
			 r8.setThreshold(0);
			 r8.setDiscountType(0);
			 r8.setTradeCampaignHeaders(new ArrayList());
			 r8.setSelect("false");
			 r8.setNew("false");
			 
			 Category cat8= new Category();
			 List<Category> catList8=new ArrayList<>();
			 RMSTDServicesTestDP catid8 = new RMSTDServicesTestDP();
			 List<Map<String, Object>> catname8=new ArrayList<>();
			 catname8 = catid8.cmshelper.getCategories(Integer.toString(Percentage_restaurants));
			 
			 for(int i=0;i<1;i++) {
				 String catg8 = catname8.get(i).get("cat_id").toString();
				 cat8.setId(catg8);
				 String catgname8 = catname8.get(i).get("cat_name").toString();
				 cat8.setName(catgname8);
			 }
			 
			 catList8.add(cat8);
			 r8.setCategories(catList8);

			 restList8.add(r8);
			 RuleDiscount rd8=new RuleDiscount();
			 rd8.setType("Percentage");
			 rd8.setDiscountLevel("Category");
			 rd8.setPercentDiscount(2);
			 rd8.setMinCartAmount(0);
			 rd8.setPercentDiscountAbsoluteCap(0);
			 
			 RMSCreateTDEntry entry8=new RMSTDBuilder().
						setEnabled("true").
						setRestaurantIds(restIds8).
						setValidFrom(Long.valueOf(DateUtils.addHours(new Date(), 0).toInstant().toEpochMilli())).
						setValidTill(Long.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().toEpochMilli())).
						setCampaignType("Percentage").setRuleDiscountType("Percentage").
						setUpdatedBy(9886379321l).setDiscountLevel("Category").
						setRestaurantList(restList8).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
						setUserRestriction("false").setRestaurantFirstOrder("false").
						setRuleDiscount(rd8).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
						setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();
			 RMSTDHelper rmsTDHelper8 = new RMSTDHelper();
			 int count8 =0;
			 int discountId8 = -1;
			 do{
			 Processor p = rmsTDHelper8.createTD(jsonHelper.getObjectToJSON(entry8).toString(), accessToken);
				if (p.ResponseValidator.GetNodeValueAsInt("statusCode") == -5) {
					String[] res = p.ResponseValidator.GetNodeValue("data.dataCat.data").split(" ");
					System.out.println("IDDDD" +res[6]);
					Processor p2 =RngHelper.disableEnableTDFromMarketingDashboard(res[6], "false");
				} else {
					System.out.println(p.ResponseValidator.GetNodeValueAsInt("data.dataCat.data"));
				 discountId8 = p.ResponseValidator.GetNodeValueAsInt("data.dataCat.data");
					break;
				}
			} while (count8++ < 5);

		 ArrayList<Integer> restIds9=new ArrayList<>();
		 restIds9.add(Flat_Restaurants);
		 ArrayList<Restaurant> restList9 =new ArrayList<>();
		 Restaurant r9=new Restaurant();
		 r9.setId(Flat_Restaurants);
		 r9.setName("Myramzi");
		 r9.setCity(219);
		 r9.setArea("MyRamziArea");
		 r9.setRecommended(0);
		 r9.setCostForTwo(0);
		 r9.setDeliveryCharge(0);
		 r9.setMinimumOrder(0);
		 r9.setOpened(0);
		 r9.setDeliveryTime(0);
		 r9.setTmpClosed(0);
		 r9.setPostalCode(0);
		 r9.setCityDeliveryCharge(0);
		 r9.setThreshold(0);
		 r9.setDiscountType(0);
		 r9.setTradeCampaignHeaders(new ArrayList());
		 r9.setSelect("false");
		 r9.setNew("false");
		 r9.setCategories(new ArrayList());
		 restList9.add(r9);
		 RuleDiscount rd9=new RuleDiscount();
		 rd9.setType("Percentage");
		 rd9.setDiscountLevel("Restaurant");
		 rd9.setPercentDiscount(2);
		 rd9.setMinCartAmount(0);
		 rd9.setPercentDiscountAbsoluteCap(0);
		 
		 RMSUpdateTDEntry entry9=new RMSEditTDBuilder().
					setEnabled("true").
					setRestaurantIds(restIds9).setId(discountId8).
					setValidFrom(Long.valueOf(DateUtils.addDays(new Date(), 2).toInstant().toEpochMilli())).
					setValidTill(Long.valueOf(DateUtils.addDays(new Date(), 4).toInstant().toEpochMilli())).
					setCampaignType("Percentage").setRuleDiscountType("Percentage").
					setUpdatedBy(9886379321l).setDiscountLevel("Restaurant").
					setRestaurantList(restList9).setSlots(new ArrayList<>()).setFirstOrderRestriction("false").
					setUserRestriction("false").setRestaurantFirstOrder("false").
					setRuleDiscount(rd9).setRestaurantHit(100).setSwiggyHit(0).setTaxesOnDiscountedBill("false").
					setCommissionOnFullBill("false").setTimeSlotRestriction("false").build();

		 return new Object[][] {
				{jsonHelper.getObjectToJSON(entry9),accessToken}
			};	
		}

	@DataProvider(name = "getTDMenuData")
	public static Object[][] getTDMenuData() {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] {{ m.get("accessToken"), restaurantId1, "Fetched Menu Data Successfully" }};
	}

	@DataProvider(name = "getTDMenuDataRegression")
	public static Object[][] getTDMenuDataRegression() {
		m = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);
		return new Object[][] {
				{ "Ybuyd83=dhe", restaurantId1, "1234", "Invalid Session", -3 },
				{ m.get("accessToken"), "", "1234", "Invalid Input", -1},
				{ m.get("accessToken"), restaurantId1, "-1", "Failed To Fetch Discount Details", -4}
		};
	}

}	
