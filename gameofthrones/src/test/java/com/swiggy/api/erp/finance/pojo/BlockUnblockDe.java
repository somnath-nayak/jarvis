package com.swiggy.api.erp.finance.pojo;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.erp.finance.pojo
 **/

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import edu.emory.mathcs.backport.java.util.Arrays;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "deIds",
        "reason"
})
public class BlockUnblockDe {

    @JsonProperty("deIds")
    private List<Integer> deIds = null;
    @JsonProperty("reason")
    private String reason;

    @JsonProperty("deIds")
    public List<Integer> getDeIds() {
        return deIds;
    }

    @JsonProperty("deIds")
    public void setDeIds(List<Integer> deIds) {
        this.deIds = deIds;
    }

    @JsonProperty("reason")
    public String getReason() {
        return reason;
    }

    @JsonProperty("reason")
    public void setReason(String reason) {
        this.reason = reason;
    }

    private void setDefaultValues(Integer[] deID) {
        List<Integer> list = new ArrayList<>(Arrays.asList(deID));
        if (this.getDeIds() == null)
            this.setDeIds(list);
        if (this.getReason() == null)
            this.setReason("CashMgmt regression test Delivery");
    }

    public BlockUnblockDe build(Integer[] deID){
        setDefaultValues(deID);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("deIds", deIds).append("reason", reason).toString();
    }

}
