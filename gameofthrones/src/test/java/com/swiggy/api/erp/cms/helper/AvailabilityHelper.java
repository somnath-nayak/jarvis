package com.swiggy.api.erp.cms.helper;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;

import java.util.HashMap;

public class AvailabilityHelper {

    Initialize gameofthrones = new Initialize();

    public Processor restRegularMenu(String restId) {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("cmsrestaurantservice", "restregularmenu", gameofthrones);
        String[] urlParams={restId};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

    public Processor restRegularPop(String restId) {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("cmsrestaurantservice", "restpopmenu", gameofthrones);
        String[] urlParams={restId};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

    public Processor itemRegular(String restId) {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("cmsrestaurantservice", "itemregular", gameofthrones);
        String[] urlParams={restId};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

    public Processor itemPop(String restId) {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("cmsrestaurantservice", "itempop", gameofthrones);
        String[] urlParams={restId};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

    public Processor restLastId(String restId) {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("cmsrestaurantservice", "restlastid", gameofthrones);
        String[] urlParams={restId};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }
  /*  ===================================================================================================================== */

    public Processor restTimeSlots(String restId) {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("availabilityrest", "restauranttimeslots", gameofthrones);
        String[] urlParams={restId};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

    public Processor restHolidaySlots(String restId) {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("availabilityrest", "restaurantholidayslots", gameofthrones);
        String[] urlParams={restId};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

    public Processor areaTimeSlots(String areaId) {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("availabilityrest", "areatimeslots", gameofthrones);
        String[] urlParams={areaId};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

    public Processor cityTimeSlots(String cityId) {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("availabilityrest", "citytimeslots", gameofthrones);
        String[] urlParams={cityId};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

    public Processor areaTimeSlotsRest(String restId) {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("availabilityrest", "areatimeslotsrestid", gameofthrones);
        String[] urlParams={restId};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

    public Processor restAreaSlots(String restId) {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("availabilityrest", "restareaslots", gameofthrones);
        String[] urlParams={restId};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

    public Processor restCitySlots(String restId) {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("availabilityrest", "restcityslots", gameofthrones);
        String[] urlParams={restId};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

    public Processor restIdByArea(String restId) {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("availabilityrest", "restidbyarea", gameofthrones);
        String[] urlParams={restId};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

    public Processor restIdByCity(String restId) {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("availabilityrest", "restidbycity", gameofthrones);
        String[] urlParams={restId};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

    public Processor areaIdCityId(String cityId) {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("availabilityrest", "areaidbycityid", gameofthrones);
        String[] urlParams={cityId};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

    public Processor itemHolidaySlots(String itemId) {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("availabilitycms", "itemholidayslot", gameofthrones);
        String[] urlParams={itemId};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

    public Processor itemSlots(String itemId) {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("availabilitycms", "itemslot", gameofthrones);
        String[] urlParams={itemId};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

    public Processor itemHolidaySlotType(String cityId, String type) {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("availabilitycms", "itemholidayslottype", gameofthrones);
        String[] urlParams={cityId,type};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

    public Processor itemSlotType(String cityId, String type) {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("availabilitycms", "itemslottype", gameofthrones);
        String[] urlParams={cityId,type};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

    public Processor menuHolidaySlots(String menuId) {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("availabilitycms", "menuholidayslot", gameofthrones);
        String[] urlParams={menuId};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

    public Processor itemRest(String cityId) {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("availabilitycms", "itemrest", gameofthrones);
        String[] urlParams={cityId};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

    public Processor getItemIds(String restId, String type) {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("availabilitycms", "getitemids", gameofthrones);
        String[] urlParams={restId, type};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

    public Processor itemRest(String cityId, String type) {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("availabilitycms", "itemrest", gameofthrones);
        String[] urlParams={cityId,type};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

    public Processor itemMenuId(String cityId) {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("availabilitycms", "itemmenuid", gameofthrones);
        String[] urlParams={cityId};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

    public Processor menuRestId(String menuId) {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("availabilitycms", "menurestid", gameofthrones);
        String[] urlParams={menuId};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

    public Processor itemAllTypeSlot(String restId, String type) {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("availabilitycms", "itemalltypeslots", gameofthrones);
        String[] urlParams={restId,type};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

    public Processor itemMapping(String cityId) {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("availabilitycms", "itemmapping", gameofthrones);
        String[] urlParams={cityId};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

    public Processor itemAbsoluteSlots(String cityId, String type) {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("availabilitycms", "itemabsoluteslotsrestaurant", gameofthrones);
        String[] urlParams={cityId, type};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

    public Processor restMenuMap(String cityId) {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("availabilitycms", "restmenumap", gameofthrones);
        String[] urlParams={cityId};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

    public Processor availCartApi(String menu, String regularItem) {
        Processor processor = null;
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        JsonHelper jsonHelper = new JsonHelper();
        String[] payloadparams = new String[]{menu, regularItem};

        GameOfThronesService got = new GameOfThronesService("availabilityapi", "availcartapi", gameofthrones);
        try {
            processor = new Processor(got, headers, payloadparams, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return processor;
    }


}
