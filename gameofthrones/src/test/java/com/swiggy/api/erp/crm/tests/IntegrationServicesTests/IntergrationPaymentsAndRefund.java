package com.swiggy.api.erp.crm.tests.refundCoupons;

import com.swiggy.api.erp.crm.constants.FlowMapper;
import com.swiggy.api.erp.crm.dp.MockServiceDP;
import com.swiggy.api.erp.crm.helper.CRMUpstreamValidation;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.RedisHelper;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.swiggy.api.sf.snd.helper.SnDHelper;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import static org.testng.Assert.assertTrue;

public class IntergrationPaymentsAndRefund  extends MockServiceDP {

    FlowMapper flowMapper = new FlowMapper();
    Initialize gameofthrones = new Initialize();
    RedisHelper redisHelper = new RedisHelper();
    CRMUpstreamValidation crmUpstreamValidation = new CRMUpstreamValidation();

    @Test(dataProvider = "mockPaymentAndRefundFlow", groups = {"regression"}, description = "End to end tests for Cod Block flow")
    public void integrationPaymentsAndRefund(HashMap<String, String> flowDetails) throws IOException, InterruptedException {
        HashMap<String, String> requestheaders_inbound = new HashMap<String, String>();
        requestheaders_inbound.put("Content-Type", "application/json");
        String ffStatus = flowDetails.get("ffStatus").toString();
        String deliveryStatus = flowDetails.get("delivery_status").toString();
        String flowName = flowDetails.get("flow").toString();
        String conversationId, deviceId,nodeId;
        String paymentMode =flowDetails.get("paymentMode").toString();
        String orderId;// = flowDetails.get("order").toString();
        String[] flow = flowMapper.setFlow().get(flowName);
        String test = flowDetails.get("test").toString();
        String queryparam[] = new String[1];
        int lengthofflow = flow.length;

        Processor logginResponse = SnDHelper.consumerLogin("7507220659","Test@2211");
        String customerId = logginResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.customer_id");//"387395";
        redisHelper.setValueJson("checkoutredis", 0, "user_credit_7191663", "{\"userId\":"+customerId+",\"swiggyMoney\":0.0,\"cancellationFee\":0.0}");
        String getValue = (String) redisHelper.getValue("checkoutredis", 0, "user_credit_7191663");
        System.out.println("key value" + getValue);
        HashMap<String, String> map = crmUpstreamValidation.e2e("8884292249", "welcome123", "7053", "7030572", "1", deliveryStatus, ffStatus);
        orderId = map.get("order_id");
        Assert.assertNotNull(orderId);
        conversationId = orderId;
        deviceId = orderId;

        int cnt = lengthofflow - 1;
        for (int i = 0; i < lengthofflow -1 ; i++) {
            nodeId = flow[i];
            if (nodeId == "1") {
                continue;
            } else {
                String nextNodeId = flow[i + 1];
                queryparam[0] = nodeId;
                String[] paylaodparam = new String[]{customerId, orderId, conversationId};

                System.out.println("length of flow" + lengthofflow);
                GameOfThronesService paymentAndRefund = new GameOfThronesService("crm", "codBlock", gameofthrones);

                Processor paymentAndRefund_response = new Processor(paymentAndRefund,
                        requestheaders_inbound, paylaodparam, queryparam);

                Assert.assertEquals(paymentAndRefund_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("status"),
                        "SUCCESS", "Actual status code doesnt match with actual status code ");

                String addOnGroupList = paymentAndRefund_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.childrenNodes..id").toString().replace("[", "").replace("]", "");

                List<String> addOnlist = Arrays.asList(addOnGroupList.split(","));

                System.out.println("addonlist" + addOnlist);
                System.out.println("flowname[i+1]" + flow[i + 1]);
                int index = addOnlist.indexOf(flow[i + 1]);

                if (test.equals("negative")) {
                    if (i == cnt - 1)
                        Assert.assertEquals(!addOnlist.contains(nextNodeId), true, "Failure in Negative scenario ");
                    else
                        Assert.assertEquals(addOnlist.contains(nextNodeId), true, "Failure in Negative scenario ");
                } else {
                    if (i < (lengthofflow - 2)) {
                        Assert.assertEquals(paymentAndRefund_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.childrenNodes.[" + index + "].isLeaf"), "false");
                    } else {
                        Assert.assertEquals(paymentAndRefund_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.childrenNodes.[" + index + "].isLeaf"), "true");
                    }
                }
            }
        }
    }
}

