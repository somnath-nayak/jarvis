package com.swiggy.api.erp.cms.pojo.BaseServiceBulkVariant;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.erp.cms.pojo.BaseServiceBulkVariant
 **/
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "cgst",
        "igst",
        "inclusive",
        "sgst"
})
public class Gst_details {

    @JsonProperty("cgst")
    private String cgst;
    @JsonProperty("igst")
    private String igst;
    @JsonProperty("inclusive")
    private Boolean inclusive;
    @JsonProperty("sgst")
    private String sgst;

    @JsonProperty("cgst")
    public String getCgst() {
        return cgst;
    }

    @JsonProperty("cgst")
    public void setCgst(String cgst) {
        this.cgst = cgst;
    }

    @JsonProperty("igst")
    public String getIgst() {
        return igst;
    }

    @JsonProperty("igst")
    public void setIgst(String igst) {
        this.igst = igst;
    }

    @JsonProperty("inclusive")
    public Boolean getInclusive() {
        return inclusive;
    }

    @JsonProperty("inclusive")
    public void setInclusive(Boolean inclusive) {
        this.inclusive = inclusive;
    }

    @JsonProperty("sgst")
    public String getSgst() {
        return sgst;
    }

    @JsonProperty("sgst")
    public void setSgst(String sgst) {
        this.sgst = sgst;
    }

    public void setDefaultValues() {
        if(this.getIgst() == null)
            this.setIgst("[VARIANT_PRICE]*0.2");
        if(this.getCgst() == null)
            this.setCgst("[VARIANT_PRICE]*0.2");
        if(this.getSgst() == null)
            this.setSgst("[VARIANT_PRICE]*0.2");
        if(this.getInclusive() == null)
            this.setInclusive(true);
    }

    public Gst_details build() {
        setDefaultValues();
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("cgst", cgst).append("igst", igst).append("inclusive", inclusive).append("sgst", sgst).toString();
    }

}
