package com.swiggy.api.erp.cms.tests;

import com.swiggy.api.erp.cms.dp.VariantGroupDP;
import com.swiggy.api.erp.cms.helper.FullMenuHelper;
import com.swiggy.api.erp.cms.pojo.VariantGroups.VariantGroup;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

/**
 * Created by kiran.j on 2/19/18.
 */
public class VariantGroupTests extends VariantGroupDP{

    @Test(dataProvider = "variantGroup", description = "Verifies Different Scenarios for Creating Variant Groups")
    public void createvariantGroups(String rest_id, String json, String[] errors) {
        SoftAssert softAssert = new SoftAssert();
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        Processor processor = fullMenuHelper.createVariantGroup(rest_id, json);
        if(errors.length == 0)
            softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive(processor));
        else
            softAssert.assertTrue(fullMenuHelper.validateErrors(processor, errors));
        softAssert.assertAll();
    }

    @Test(dataProvider = "updatevariantGroup", description = "Verifies Different Scenarios for Updating Variant Groups")
    public void updatevariantGroups(String rest_id, VariantGroup json, String[] errors) {
        SoftAssert softAssert = new SoftAssert();
        try {
            FullMenuHelper fullMenuHelper = new FullMenuHelper();
            Processor processor = fullMenuHelper.updateVariantGroup(rest_id, json);
            if (errors.length == 0)
                softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive(processor));
            else
                softAssert.assertTrue(fullMenuHelper.validateErrors(processor, errors));
            softAssert.assertAll();
        } catch (Exception e) {
            softAssert.assertTrue(false, "Some Exception Occured");
            e.printStackTrace();
        }
    }

    @Test(dataProvider = "bulkvariantGroup", description = "Verifies Different Scenarios for Creating Bulk Variant Groups")
    public void createBulkVariantGroups(String rest_id, String json, String[] errors, String token_id) {
        System.out.println(json);
        SoftAssert softAssert = new SoftAssert();
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        Processor processor = fullMenuHelper.bulkCreateVariantGroup(rest_id, json, token_id);
        if(errors.length == 0)
            softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive(processor));
        else
            softAssert.assertTrue(fullMenuHelper.validateErrors(processor, errors));
        softAssert.assertAll();
    }

    @Test(dataProvider = "bulkupdatevariantGroup", description = "Verifies Different Scenarios for Updating Bulk Variant Groups")
    public void updateBulkVariantGroups(String rest_id, String json, String[] errors, String token_id) {
        System.out.println(json);
        SoftAssert softAssert = new SoftAssert();
        FullMenuHelper fullMenuHelper = new FullMenuHelper();
        Processor processor = fullMenuHelper.bulkUpdateVariantGroup(rest_id, json, token_id);
        if(errors.length == 0)
            softAssert.assertTrue(fullMenuHelper.verifyEntityResponseForPositive(processor));
        else
            softAssert.assertTrue(fullMenuHelper.validateErrors(processor, errors));
        softAssert.assertAll();
    }
}
