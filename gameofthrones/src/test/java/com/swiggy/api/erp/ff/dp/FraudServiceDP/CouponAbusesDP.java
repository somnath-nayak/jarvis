package com.swiggy.api.erp.ff.dp.FraudServiceDP;

import org.testng.annotations.DataProvider;

public class CouponAbusesDP {

    CouponAbusesData dataService=new CouponAbusesData();


    @DataProvider(name = "CouponCheck")
    public Object[][] getCouponCheckDataSet() {

        return dataService.getData();
    }


    @DataProvider(name = "withoutCouponDetails")
    public Object[][] getwithouCouponDetailsDataSet() {

        return dataService.getWithoutCouponDetailsData();
    }


    @DataProvider(name = "fieldLevelValidation")
    public Object[][] getfieldLevelValidationDataSet() {

        return dataService.getFieldLevelData();
    }

    @DataProvider(name = "rmqPaymentConfirmation")
    public Object[][] getRmqPaymentConfirmationDataSet() {

        return dataService.getRmqPaymentConfirmationData();
    }

    @DataProvider(name = "rmqOrderCancellation")
    public Object[][] getOrderCancellationDataSet() {

        return dataService.getRmqOrderCancellationData();
    }


    @DataProvider(name = "rmqOrderCancellationPaymentConfirmation")
    public Object[][] getrmqOrderCancellationPaymentConfirmationDataSet() {

        return dataService.getRmqOrderCancellationPaymentConfirmation();
    }

    @DataProvider(name = "multipleCouponCategories")
    public Object[][] getMultipleCouponCategoriesDataSet() {

        return dataService.getMultipleCouponCategoriesData();
    }


    @DataProvider(name = "apiResponsePaymentConfirmation")
    public Object[][] getApiResponsePaymentConfirmationDataSet() {

        return dataService.getAPIPaymentConfirmationData();
    }


    @DataProvider(name = "apiResponsePaymentCancellation")
    public Object[][] getApiResponsePaymentCancellationDataSet() {

        return dataService.getAPIPaymentCancellationDataData();
    }

}



