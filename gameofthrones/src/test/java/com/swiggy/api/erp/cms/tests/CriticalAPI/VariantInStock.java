package com.swiggy.api.erp.cms.tests.CriticalAPI;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.dp.Critical_API_dataProvider;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import org.testng.Assert;
import org.testng.annotations.Test;

public class VariantInStock extends Critical_API_dataProvider {
    CMSHelper cms=new CMSHelper();
    @Test(dataProvider = "iteminstock", description = "Verify Variant Stock")
        public void variantInStock(String var_id, String var_gid,String msg) {
        String response=cms.variantInStock(var_id,var_gid).ResponseValidator.GetBodyAsText();
        String statusmsg= JsonPath.read(response,"$.statusMessage").toString().replace("[","").replace("]","");
        String statuscode= JsonPath.read(response,"$.statusCode").toString().replace("[","").replace("]","");
        if(statuscode.equals("0")){
        Assert.assertEquals(statusmsg,msg);
        Assert.assertEquals(Integer.parseInt(statuscode),0);}
        else{
            Assert.assertEquals(statusmsg,msg);
            Assert.assertEquals(Integer.parseInt(statuscode),1);
        }




    }
}