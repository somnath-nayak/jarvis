package com.swiggy.api.erp.delivery.dp;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.testng.annotations.DataProvider;

import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.erp.delivery.helper.DeliveryDataHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryHelperMethods;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;

import framework.gameofthrones.JonSnow.Processor;

public class MultiRestaurantBatchingDataProvider {
	
	CheckoutHelper helperc = new CheckoutHelper();
	DeliveryServiceHelper helpdel=new DeliveryServiceHelper();
	DeliveryHelperMethods delmeth=new DeliveryHelperMethods();
	DeliveryDataHelper deldata=new DeliveryDataHelper();
	
	static String order_id,order_id1;
	@DataProvider(name = "QE-282")
	public Object[][] AutoassigntestDP282() throws Exception {
		delmeth.dbhelperupdate(DeliveryConstant.updatebatch);
		delmeth.dbhelperupdate(DeliveryConstant.assigndetounassinged);
		helpdel.updateZoneBatchingParams(DeliveryConstant.zone_id,
				DeliveryConstant.zone_name, DeliveryConstant.city,
				DeliveryConstant.norainmode,
				DeliveryConstant.maxOrdersInBatch_two,
				DeliveryConstant.maxSlaOfBatch_more,
				DeliveryConstant.batchingVersion_multi,
				DeliveryConstant.maxOrderDiff_more,
				DeliveryConstant.maxCustEdge_more,
				DeliveryConstant.maxBatchElapsedTime_more,
				DeliveryConstant.maxItemsInBatch_more,
				DeliveryConstant.maxCustEdgeV2_more,
				DeliveryConstant.maxOrderDiffV2_more,
				DeliveryConstant.maxRestEdge_more,
				DeliveryConstant.maxBill_more);
		Thread.sleep(5000);
		String order_id1= createorderjson(DeliveryConstant.multi_rest_batching_rest1,DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile,DeliveryConstant.multi_rest_area,"","").toString();
		String order_id2= createorderjson(DeliveryConstant.multi_rest_batching_rest2, DeliveryConstant.multi_rest_batching_rest2_lat_long, DeliveryConstant.multi_rest_last_mile, DeliveryConstant.multi_rest_area,DeliveryConstant.multirest_batching_customer2_lat, DeliveryConstant.multirest_batching_customer2_lng).toString();
		return new Object[][] {
		{ order_id1,order_id2}
		};
	}
	@DataProvider(name = "QE-283")
	public Object[][] AutoassigntestDP283() throws Exception {
		helpdel.updateZoneBatchingParams(DeliveryConstant.zone_id,
				DeliveryConstant.zone_name, DeliveryConstant.city,
				DeliveryConstant.norainmode,
				DeliveryConstant.maxOrdersInBatch_two,
				DeliveryConstant.maxSlaOfBatch_more,
				DeliveryConstant.batchingVersion_multi,
				DeliveryConstant.maxOrderDiff_more,
				DeliveryConstant.maxCustEdge_more,
				DeliveryConstant.maxBatchElapsedTime_more,
				DeliveryConstant.maxItemsInBatch_more,
				DeliveryConstant.maxCustEdgeV2_more,
				DeliveryConstant.maxOrderDiffV2_more,
				DeliveryConstant.maxRestEdge_more,
				DeliveryConstant.maxBill_more);
		Thread.sleep(5000);
		String order_id1= createorderjson(DeliveryConstant.multi_rest_batching_rest1,DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile,DeliveryConstant.multi_rest_area,"","").toString();
		String order_id2= createorderjson(DeliveryConstant.multi_rest_batching_rest2, DeliveryConstant.multi_rest_batching_rest2_lat_long, DeliveryConstant.multi_rest_last_mile, DeliveryConstant.multi_rest_other_area,"", "").toString();
		return new Object[][] {
		{ order_id1,order_id2}
		};
	}
	@DataProvider(name = "QE-284")
	public Object[][] AutoassigntestDP284() throws Exception {
		helpdel.updateZoneBatchingParams(DeliveryConstant.zone_id,
				DeliveryConstant.zone_name, DeliveryConstant.city,
				DeliveryConstant.norainmode,
				DeliveryConstant.maxOrdersInBatch_two,
				DeliveryConstant.maxSlaOfBatch_more,
				DeliveryConstant.batchingVersion_multi,
				DeliveryConstant.maxOrderDiff_more,
				DeliveryConstant.maxCustEdge_more,
				DeliveryConstant.maxBatchElapsedTime_more,
				DeliveryConstant.maxItemsInBatch_more,
				DeliveryConstant.maxCustEdgeV2_more,
				DeliveryConstant.maxOrderDiffV2_more,
				DeliveryConstant.maxRestEdge_more,
				DeliveryConstant.maxBill_more);
		Thread.sleep(5000);
		String order_id1= createorderjson(DeliveryConstant.multi_rest_batching_rest1,DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile,DeliveryConstant.multi_rest_area,"","").toString();
		String order_id2= createorderjson(DeliveryConstant.multi_rest_batching_rest2, DeliveryConstant.multi_rest_batching_rest2_lat_long, DeliveryConstant.multi_rest_last_mile, DeliveryConstant.multi_rest_area,"", "").toString();
		return new Object[][] {
		{ order_id1,order_id2}
		};
	}
	
	@DataProvider(name = "QE-285")
	public Object[][] AutoassigntestDP285() throws Exception {
		helpdel.updateZoneBatchingParams(DeliveryConstant.zone_id,
				DeliveryConstant.zone_name, DeliveryConstant.city,
				DeliveryConstant.norainmode,
				DeliveryConstant.maxOrdersInBatch_two,
				DeliveryConstant.maxSlaOfBatch_more,
				DeliveryConstant.batchingVersion_multi,
				DeliveryConstant.maxOrderDiff_more,
				DeliveryConstant.maxCustEdge_more,
				DeliveryConstant.maxBatchElapsedTime_more,
				DeliveryConstant.maxItemsInBatch_more,
				DeliveryConstant.maxCustEdgeV2_more,
				DeliveryConstant.maxOrderDiffV2_more,
				DeliveryConstant.maxRestEdge_more,
				DeliveryConstant.maxBill_more);
		Thread.sleep(5000);
		String order_id1= createorderjson(DeliveryConstant.multi_rest_batching_rest1,DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile,DeliveryConstant.multi_rest_area,"","").toString();
		String order_id2= createorderjson(DeliveryConstant.multi_rest_batching_rest2, DeliveryConstant.multi_rest_batching_rest2_lat_long, DeliveryConstant.multi_rest_last_mile, DeliveryConstant.multi_rest_area,"", "").toString();
		return new Object[][] {
		{ order_id1,order_id2}
		};
	}
	@DataProvider(name = "QE-286")
	public Object[][] AutoassigntestDP286() throws Exception {
		helpdel.updateZoneBatchingParams(DeliveryConstant.zone_id,
				DeliveryConstant.zone_name, DeliveryConstant.city,
				DeliveryConstant.norainmode,
				DeliveryConstant.maxOrdersInBatch_two,
				DeliveryConstant.maxSlaOfBatch_more,
				DeliveryConstant.batchingVersion_multi,
				DeliveryConstant.maxOrderDiff_more,
				DeliveryConstant.maxCustEdge_more,
				DeliveryConstant.maxBatchElapsedTime_more,
				DeliveryConstant.maxItemsInBatch_more,
				DeliveryConstant.maxCustEdgeV2_more,
				DeliveryConstant.maxOrderDiffV2_more,
				DeliveryConstant.maxRestEdge_more,
				DeliveryConstant.maxBill_more);
		Thread.sleep(5000);
		String order_id1= createorderjson(DeliveryConstant.multi_rest_batching_rest1,DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile,DeliveryConstant.multi_rest_area,"","").toString();
		String order_id2= createorderjson(DeliveryConstant.multi_rest_batching_rest2, DeliveryConstant.multi_rest_batching_rest2_lat_long, DeliveryConstant.multi_rest_last_mile, DeliveryConstant.multi_rest_area,"", "").toString();
		return new Object[][] {
		{ order_id1,order_id2}
		};
	}
	@DataProvider(name = "QE-287")
	public Object[][] AutoassigntestDP287() throws Exception {
		helpdel.updateZoneBatchingParams(DeliveryConstant.zone_id,
				DeliveryConstant.zone_name, DeliveryConstant.city,
				DeliveryConstant.norainmode,
				DeliveryConstant.maxOrdersInBatch_two,
				DeliveryConstant.maxSlaOfBatch_more,
				DeliveryConstant.batchingVersion_single,
				DeliveryConstant.maxOrderDiff_more,
				DeliveryConstant.maxCustEdge_more,
				DeliveryConstant.maxBatchElapsedTime_more,
				DeliveryConstant.maxItemsInBatch_more,
				DeliveryConstant.maxCustEdgeV2_more,
				DeliveryConstant.maxOrderDiffV2_more,
				DeliveryConstant.maxRestEdge_more,
				DeliveryConstant.maxBill_more);
		Thread.sleep(5000);
		String order_id1= createorderjson(DeliveryConstant.multi_rest_batching_rest1,DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile,DeliveryConstant.multi_rest_area,"","").toString();
		String order_id2= createorderjson(DeliveryConstant.multi_rest_batching_rest2, DeliveryConstant.multi_rest_batching_rest2_lat_long, DeliveryConstant.multi_rest_last_mile, DeliveryConstant.multi_rest_area,"", "").toString();
		return new Object[][] {
		{ order_id1,order_id2}
		};
	}
	@DataProvider(name = "QE-288")
	public Object[][] AutoassigntestDP288() throws Exception {
		helpdel.updateZoneBatchingParams(DeliveryConstant.zone_id,
				DeliveryConstant.zone_name, DeliveryConstant.city,
				DeliveryConstant.norainmode,
				DeliveryConstant.maxOrdersInBatch_two,
				DeliveryConstant.maxSlaOfBatch_more,
				DeliveryConstant.batchingVersion_multi,
				DeliveryConstant.maxOrderDiff_more,
				DeliveryConstant.maxCustEdge_more,
				DeliveryConstant.maxBatchElapsedTime_more,
				DeliveryConstant.maxItemsInBatch_more,
				DeliveryConstant.maxCustEdgeV2_more,
				DeliveryConstant.maxOrderDiffV2_more,
				DeliveryConstant.maxRestEdge_more,
				DeliveryConstant.maxBill_more);
		Thread.sleep(5000);
		String order_id1= createorderjson(DeliveryConstant.multi_rest_batching_rest1,DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile,DeliveryConstant.multi_rest_area,"","").toString();
		String order_id2= createorderjson(DeliveryConstant.multi_rest_batching_rest2, DeliveryConstant.multi_rest_batching_rest2_lat_long, DeliveryConstant.multi_rest_last_mile, DeliveryConstant.multi_rest_area,"", "").toString();
		return new Object[][] {
		{ order_id1,order_id2}
		};
	}
	@DataProvider(name = "QE-289")
	public Object[][] AutoassigntestDP289() throws Exception {
		helpdel.updateDeliveryRestaurantParams(DeliveryConstant.restmultiid,DeliveryConstant.batcingenabled_false);
		Thread.sleep(5000);
		String order_id1= createorderjson(DeliveryConstant.multi_rest_batching_rest1,DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile,DeliveryConstant.multi_rest_area,"","").toString();
		String order_id2= createorderjson(DeliveryConstant.multi_rest_batching_rest2, DeliveryConstant.multi_rest_batching_rest2_lat_long, DeliveryConstant.multi_rest_last_mile, DeliveryConstant.multi_rest_area,"", "").toString();
		return new Object[][] {
		{ order_id1,order_id2}
		};
	}
	@DataProvider(name = "QE-290")
	public Object[][] AutoassigntestDP290() throws Exception {
		helpdel.updateZoneBatchingParams(DeliveryConstant.zone_id,
				DeliveryConstant.zone_name, DeliveryConstant.city,
				DeliveryConstant.norainmode,
				DeliveryConstant.maxOrdersInBatch_two,
				DeliveryConstant.maxSlaOfBatch_more,
				DeliveryConstant.batchingVersion_multi,
				DeliveryConstant.maxOrderDiff_more,
				DeliveryConstant.maxCustEdge_more,
				DeliveryConstant.maxBatchElapsedTime_more,
				DeliveryConstant.maxItemsInBatch_more,
				DeliveryConstant.maxCustEdgeV2_more,
				DeliveryConstant.maxOrderDiffV2_more,
				DeliveryConstant.maxRestEdge_less,
				DeliveryConstant.maxBill_more);
		Thread.sleep(5000);
		String order_id1= createorderjson(DeliveryConstant.multi_rest_batching_rest1,DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile,DeliveryConstant.multi_rest_area,"","").toString();
		String order_id2= createorderjson(DeliveryConstant.multi_rest_batching_rest2, DeliveryConstant.multi_rest_batching_rest2_lat_long, DeliveryConstant.multi_rest_last_mile, DeliveryConstant.multi_rest_area,"", "").toString();
		return new Object[][] {
		{ order_id1,order_id2}
		};
	}
	@DataProvider(name = "QE-291")
	public Object[][] AutoassigntestDP291() throws Exception {
		helpdel.updateDeliveryRestaurantParams(DeliveryConstant.multirestid_different_zone, DeliveryConstant.batchingenabled_true);
		Thread.sleep(5000);
		helpdel.updateZoneBatchingParams(DeliveryConstant.zone_id,
				DeliveryConstant.zone_name, DeliveryConstant.city,
				DeliveryConstant.norainmode,
				DeliveryConstant.maxOrdersInBatch_two,
				DeliveryConstant.maxSlaOfBatch_more,
				DeliveryConstant.batchingVersion_multi,
				DeliveryConstant.maxOrderDiff_more,
				DeliveryConstant.maxCustEdge_more,
				DeliveryConstant.maxBatchElapsedTime_more,
				DeliveryConstant.maxItemsInBatch_more,
				DeliveryConstant.maxCustEdgeV2_more,
				DeliveryConstant.maxOrderDiffV2_more,
				DeliveryConstant.maxRestEdge_more,
				DeliveryConstant.maxBill_more);
		Thread.sleep(5000);
		String order_id1= createorderjson(DeliveryConstant.multi_rest_batching_rest1,DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile,DeliveryConstant.multi_rest_area,"","").toString();
		String order_id2= createorderjson(DeliveryConstant.multi_rest_batching_rest2, DeliveryConstant.multi_rest_batching_rest2_lat_long, DeliveryConstant.multi_rest_last_mile, DeliveryConstant.multi_rest_other_area,"", "").toString();
		return new Object[][] {
		{ order_id1,order_id2}
		};
	}
	@DataProvider(name = "QE-292")
	public Object[][] AutoassigntestDP292() throws Exception {
		helpdel.updateZoneBatchingParams(DeliveryConstant.zone_id,
				DeliveryConstant.zone_name, DeliveryConstant.city,
				DeliveryConstant.norainmode,
				DeliveryConstant.maxOrdersInBatch_two,
				DeliveryConstant.maxSlaOfBatch_more,
				DeliveryConstant.batchingVersion_multi,
				DeliveryConstant.maxOrderDiff_more,
				DeliveryConstant.maxCustEdge_more,
				DeliveryConstant.maxBatchElapsedTime_more,
				DeliveryConstant.maxItemsInBatch_more,
				DeliveryConstant.maxCustEdgeV2_more,
				DeliveryConstant.maxOrderDiffV2_more,
				DeliveryConstant.maxRestEdge_more,
				DeliveryConstant.maxBill_less);
		Thread.sleep(5000);
		String order_id1= createorderjson(DeliveryConstant.multi_rest_batching_rest1,DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile,DeliveryConstant.multi_rest_area,"","").toString();
		String order_id2= createorderjson(DeliveryConstant.multi_rest_batching_rest2, DeliveryConstant.multi_rest_batching_rest2_lat_long, DeliveryConstant.multi_rest_last_mile, DeliveryConstant.multi_rest_area,"", "").toString();
		return new Object[][] {
		{ order_id1,order_id2}
		};
	}
	@DataProvider(name = "QE-293")
	public Object[][] AutoassigntestDP293() throws Exception {
		helpdel.updateZoneBatchingParams(DeliveryConstant.zone_id,
				DeliveryConstant.zone_name, DeliveryConstant.city,
				DeliveryConstant.norainmode,
				DeliveryConstant.maxOrdersInBatch_two,
				DeliveryConstant.maxSlaOfBatch_more,
				DeliveryConstant.batchingVersion_multi,
				DeliveryConstant.maxOrderDiff_more,
				DeliveryConstant.maxCustEdge_more,
				DeliveryConstant.maxBatchElapsedTime_more,
				DeliveryConstant.maxItemsInBatch_more,
				DeliveryConstant.maxCustEdgeV2_more,
				DeliveryConstant.maxOrderDiffV2_more,
				DeliveryConstant.maxRestEdge_more,
				DeliveryConstant.maxBill_more);
		Thread.sleep(5000);
		String order_id1= createorderjson(DeliveryConstant.multi_rest_batching_rest1,DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile,DeliveryConstant.multi_rest_area,"","").toString();
		helpdel.assignOrder(order_id1,DeliveryConstant.MayurDE);
		Thread.sleep(10000);
		helpdel.zipDialConfirmDE(order_id1);
		Thread.sleep(10000);
		helpdel.zipDialArrivedDE(order_id1);
		Thread.sleep(10000);
		helpdel.zipDialPickedUpDE(order_id1);
		Thread.sleep(10000);
		String order_id2= createorderjson(DeliveryConstant.multi_rest_batching_rest2, DeliveryConstant.multi_rest_batching_rest2_lat_long, DeliveryConstant.multi_rest_last_mile, DeliveryConstant.multi_rest_area,"", "").toString();
				
		return new Object[][] {
		{ order_id1,order_id2}
		};
	}
	@DataProvider(name = "QE-294")
	public Object[][] AutoassigntestDP294() throws Exception {
		helpdel.updateZoneBatchingParams(DeliveryConstant.zone_id,
				DeliveryConstant.zone_name, DeliveryConstant.city,
				DeliveryConstant.norainmode,
				DeliveryConstant.maxOrdersInBatch_two,
				DeliveryConstant.maxSlaOfBatch_more,
				DeliveryConstant.batchingVersion_multi,
				DeliveryConstant.maxOrderDiff_more,
				DeliveryConstant.maxCustEdge_more,
				DeliveryConstant.maxBatchElapsedTime_more,
				DeliveryConstant.maxItemsInBatch_more,
				DeliveryConstant.maxCustEdgeV2_more,
				DeliveryConstant.maxOrderDiffV2_more,
				DeliveryConstant.maxRestEdge_more,
				DeliveryConstant.maxBill_more);
		Thread.sleep(5000);
		String order_id1= createorderjson(DeliveryConstant.multi_rest_batching_rest1,DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile,DeliveryConstant.multi_rest_area,"","").toString();
		String order_id2= createorderjson(DeliveryConstant.multi_rest_batching_rest2, DeliveryConstant.multi_rest_batching_rest2_lat_long, DeliveryConstant.multi_rest_last_mile, DeliveryConstant.multi_rest_area,"", "").toString();
		return new Object[][] {
		{ order_id1,order_id2}
		};
	}
	@DataProvider(name = "QE-295")
	public Object[][] AutoassigntestDP295() throws Exception {
		helpdel.updateZoneBatchingParams(DeliveryConstant.zone_id,
				DeliveryConstant.zone_name, DeliveryConstant.city,
				DeliveryConstant.norainmode,
				DeliveryConstant.maxOrdersInBatch_two,
				DeliveryConstant.maxSlaOfBatch_less,
				DeliveryConstant.batchingVersion_multi,
				DeliveryConstant.maxOrderDiff_more,
				DeliveryConstant.maxCustEdge_more,
				DeliveryConstant.maxBatchElapsedTime_more,
				DeliveryConstant.maxItemsInBatch_more,
				DeliveryConstant.maxCustEdgeV2_more,
				DeliveryConstant.maxOrderDiffV2_more,
				DeliveryConstant.maxRestEdge_more,
				DeliveryConstant.maxBill_more);
		Thread.sleep(5000);
		String order_id1= createorderjson(DeliveryConstant.multi_rest_batching_rest1,DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile,DeliveryConstant.multi_rest_area,"","").toString();
		String order_id2= createorderjson(DeliveryConstant.multi_rest_batching_rest2, DeliveryConstant.multi_rest_batching_rest2_lat_long, DeliveryConstant.multi_rest_last_mile, DeliveryConstant.multi_rest_area,"", "").toString();
		return new Object[][] {
		{ order_id1,order_id2}
		};
	}
	@DataProvider(name = "QE-296")
	public Object[][] AutoassigntestDP296() throws Exception {
		helpdel.updateZoneBatchingParams(DeliveryConstant.zone_id,
				DeliveryConstant.zone_name, DeliveryConstant.city,
				DeliveryConstant.norainmode,
				DeliveryConstant.maxOrdersInBatch_two,
				DeliveryConstant.maxSlaOfBatch_more,
				DeliveryConstant.batchingVersion_multi,
				DeliveryConstant.maxOrderDiff_more,
				DeliveryConstant.maxCustEdge_more,
				DeliveryConstant.maxBatchElapsedTime_more,
				DeliveryConstant.maxItemsInBatch_less,
				DeliveryConstant.maxCustEdgeV2_more,
				DeliveryConstant.maxOrderDiffV2_more,
				DeliveryConstant.maxRestEdge_more,
				DeliveryConstant.maxBill_more);
		Thread.sleep(5000);
		String order_id1= createorderjson(DeliveryConstant.multi_rest_batching_rest1,DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile,DeliveryConstant.multi_rest_area,"","").toString();
		String order_id2= createorderjson(DeliveryConstant.multi_rest_batching_rest2, DeliveryConstant.multi_rest_batching_rest2_lat_long, DeliveryConstant.multi_rest_last_mile, DeliveryConstant.multi_rest_area,"", "").toString();
		return new Object[][] {
		{ order_id1,order_id2}
		};
	}
	@DataProvider(name = "QE-297")
	public Object[][] AutoassigntestDP297() throws Exception {
		helpdel.updateZoneBatchingParams(DeliveryConstant.zone_id,
				DeliveryConstant.zone_name, DeliveryConstant.city,
				DeliveryConstant.norainmode,
				DeliveryConstant.maxOrdersInBatch_two,
				DeliveryConstant.maxSlaOfBatch_more,
				DeliveryConstant.batchingVersion_multi,
				DeliveryConstant.maxOrderDiff_more,
				DeliveryConstant.maxCustEdge_more,
				DeliveryConstant.maxBatchElapsedTime_more,
				DeliveryConstant.maxItemsInBatch_more,
				DeliveryConstant.maxCustEdgeV2_less,
				DeliveryConstant.maxOrderDiffV2_more,
				DeliveryConstant.maxRestEdge_more,
				DeliveryConstant.maxBill_more);
			Thread.sleep(5000);
			String order_id1= createorderjson(DeliveryConstant.multi_rest_batching_rest1,DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile,DeliveryConstant.multi_rest_area,"","").toString();
			String order_id2= createorderjson(DeliveryConstant.multi_rest_batching_rest2, DeliveryConstant.multi_rest_batching_rest2_lat_long, DeliveryConstant.multi_rest_last_mile, DeliveryConstant.multi_rest_area, DeliveryConstant.multirest_batching_customer2_lat, DeliveryConstant.multirest_batching_customer2_lng).toString();
			return new Object[][] {
			{ order_id1,order_id2}
			};
		}
	@DataProvider(name = "QE-298")
	public Object[][] AutoassigntestDP298() throws Exception {
		helpdel.updateZoneBatchingParams(DeliveryConstant.zone_id,
				DeliveryConstant.zone_name, DeliveryConstant.city,
				DeliveryConstant.norainmode,
				DeliveryConstant.maxOrdersInBatch_two,
				DeliveryConstant.maxSlaOfBatch_more,
				DeliveryConstant.batchingVersion_multi,
				DeliveryConstant.maxOrderDiff_more,
				DeliveryConstant.maxCustEdge_more,
				DeliveryConstant.maxBatchElapsedTime_more,
				DeliveryConstant.maxItemsInBatch_more,
				DeliveryConstant.maxCustEdgeV2_more,
				DeliveryConstant.maxOrderDiffV2_less,
				DeliveryConstant.maxRestEdge_more,
				DeliveryConstant.maxBill_more);
		Thread.sleep(5000);
		String order_id1= createorderjson(DeliveryConstant.multi_rest_batching_rest1,DeliveryConstant.multi_rest_batching_rest1_lat_long, DeliveryConstant.multi_rest_last_mile,DeliveryConstant.multi_rest_area,"","").toString();
		String order_id2= createorderjson(DeliveryConstant.multi_rest_batching_rest2,DeliveryConstant.multi_rest_batching_rest2_lat_long, DeliveryConstant.multi_rest_last_mile,DeliveryConstant.multi_rest_area,DeliveryConstant.multirest_batching_customer2_lat,DeliveryConstant.multirest_batching_customer2_lng).toString();
		return new Object[][] {
		{ order_id1,order_id2}
		};
	}
	
	
	public static String createOrder() throws Exception {
		CheckoutHelper helperc = new CheckoutHelper();
		Object[][] data = cartItems();
		Processor p = helperc.placeOrder(DeliveryConstant.mobile,
				DeliveryConstant.password, data[0][0].toString(),
				data[0][1].toString(), data[0][2].toString());
		return order_id = p.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.order_id");
	}


	public static Object[][] cartItems() {

		return new Object[][] { { "1636498", "2", "4852" } };
	}
	
	public static String createOrder1() throws Exception {
		CheckoutHelper helperc = new CheckoutHelper();
		Object[][] data = cartItems1();
		Processor p = helperc.placeOrder(DeliveryConstant.mobile,
				DeliveryConstant.password, data[0][0].toString(),
				data[0][1].toString(), data[0][2].toString());
		return order_id1 = p.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.order_id");
	}


	public static Object[][] cartItems1() {

		return new Object[][] { { "233966", "2", "1669" } };
	}
	
	public Object createorderjson(String restaurant_id,String restaurant_lat_lng,String restaurant_customer_distance,String restaurant_area_code,String lat,String lng) throws JSONException
	{
		Map<String, String> defMap= new HashMap<>();
		defMap.put("restaurant_id",restaurant_id);
		defMap.put("restaurant_lat_lng",restaurant_lat_lng);
		defMap.put("restaurant_customer_distance", restaurant_customer_distance);
		defMap.put("restaurant_area_code", restaurant_area_code);
	if(lat!="")
	{
		defMap.put("lat", lat);
	}
	if(lng!="")
	{
		defMap.put("lng",lng);
	}
		String order_time=deldata.getcurrentDateTimefororderTime();
		defMap.put("order_time",order_time);
			
		String orderJson=deldata.returnOrderJson(defMap);
		boolean order_status=deldata.pushOrderJsonToRMQAndValidate(orderJson);
		Object order_id=null;
		System.out.println(orderJson);
		if(order_status)
		{
			order_id=(new JSONObject(orderJson)).get("order_id").toString();
		}
		return order_id;
	}

}