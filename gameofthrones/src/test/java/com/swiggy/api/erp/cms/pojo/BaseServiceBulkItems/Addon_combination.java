package com.swiggy.api.erp.cms.pojo.BaseServiceBulkItems;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.erp.cms.pojo.BaseServiceBulkItems
 **/

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "addon_id",
        "group_id",
        "price",
        "third_party_addon_id",
        "third_party_group_id"
})
public class Addon_combination {

    @JsonProperty("addon_id")
    private String addon_id;
    @JsonProperty("group_id")
    private String group_id;
    @JsonProperty("price")
    private Integer price;
    @JsonProperty("third_party_addon_id")
    private String third_party_addon_id;
    @JsonProperty("third_party_group_id")
    private String third_party_group_id;

    @JsonProperty("addon_id")
    public String getAddon_id() {
        return addon_id;
    }

    @JsonProperty("addon_id")
    public void setAddon_id(String addon_id) {
        this.addon_id = addon_id;
    }

    @JsonProperty("group_id")
    public String getGroup_id() {
        return group_id;
    }

    @JsonProperty("group_id")
    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    @JsonProperty("price")
    public Integer getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(Integer price) {
        this.price = price;
    }

    @JsonProperty("third_party_addon_id")
    public String getThird_party_addon_id() {
        return third_party_addon_id;
    }

    @JsonProperty("third_party_addon_id")
    public void setThird_party_addon_id(String third_party_addon_id) {
        this.third_party_addon_id = third_party_addon_id;
    }

    @JsonProperty("third_party_group_id")
    public String getThird_party_group_id() {
        return third_party_group_id;
    }

    @JsonProperty("third_party_group_id")
    public void setThird_party_group_id(String third_party_group_id) {
        this.third_party_group_id = third_party_group_id;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("addon_id", addon_id).append("group_id", group_id).append("price", price).append("third_party_addon_id", third_party_addon_id).append("third_party_group_id", third_party_group_id).toString();
    }

}
