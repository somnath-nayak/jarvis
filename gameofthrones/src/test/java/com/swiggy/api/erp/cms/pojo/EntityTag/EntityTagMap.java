package com.swiggy.api.erp.cms.pojo.EntityTag;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;

public class EntityTagMap {
    @JsonProperty("category")
    private String category;
    @JsonProperty("entity_id")
    private Integer entityId;
    @JsonProperty("entity_type")
    private String entityType;
    @JsonProperty("partition_id")
    private Integer partitionId;
    @JsonProperty("position")
    private Integer position;
    @JsonProperty("tag_id")
    private Integer tagId;
    @JsonProperty("tag_name")
    private String tagName;
    @JsonProperty("updated_by")
    private String updatedBy;

    /**
     * No args constructor for use in serialization
     *
     */
    public EntityTagMap() {
    }

    /**
     *
     * @param updatedBy
     * @param position
     * @param category
     * @param tagName
     * @param entityId
     * @param partitionId
     * @param entityType
     * @param tagId
     */
    public EntityTagMap(String category, Integer entityId, String entityType, Integer partitionId, Integer position, Integer tagId,  String updatedBy) {
        super();
        this.category = category;
        this.entityId = entityId;
        this.entityType = entityType;
        this.partitionId = partitionId;
        this.position = position;
        this.tagId = tagId;
        //this.tagName = tagName;
        this.updatedBy = updatedBy;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Integer getEntityId() {
        return entityId;
    }

    public void setEntityId(Integer entityId) {
        this.entityId = entityId;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public Integer getPartitionId() {
        return partitionId;
    }

    public void setPartitionId(Integer partitionId) {
        this.partitionId = partitionId;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Integer getTagId() {
        return tagId;
    }

    public void setTagId(Integer tagId) {
        this.tagId = tagId;
    }

   /* public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }*/

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("category", category).append("entity_id", entityId).append("entity_type", entityType).append("partition_id", partitionId).append("position", position).append("tag_id", tagId).append("updated_by", updatedBy).toString();
    }

}