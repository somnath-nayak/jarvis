package com.swiggy.api.erp.ff.dp.losDp;

import org.testng.annotations.DataProvider;

public class DEIncomingUpdateData {
		
	@DataProvider (name = "updateIncomingwithValidData")
	public Object[][] updateIncomingwithValidData() {
		return new Object[][] {
			{"manual","25.0"},
			{"with_partner","10.0"},
			{"thirdParty","50.0"}
			
		};
	}
	
	@DataProvider (name = "deIncomingwithInValidData")
	public Object[][] deIncomingwithInValidData() {
		return new Object[][] {
			{"{\"Message\":\"invalidJson\"}"}
			
		};
	}
}
