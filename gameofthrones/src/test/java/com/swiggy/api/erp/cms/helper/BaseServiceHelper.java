package com.swiggy.api.erp.cms.helper;

import com.swiggy.api.erp.cms.constants.CmsConstants;
import com.swiggy.api.erp.cms.constants.Constants;
import com.swiggy.api.erp.cms.pojo.AddItemToRest.AddItemToRestaurant;
import com.swiggy.api.erp.cms.pojo.AddItemToRest.RestBuilder;
import com.swiggy.api.erp.cms.pojo.ItemSlotCreation.*;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.DBHelper;
import framework.gameofthrones.Tyrion.JsonHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;

import java.io.IOException;
import java.math.BigInteger;
import java.util.*;

public class BaseServiceHelper {

    static Initialize gameofthrones = new Initialize();
    static RestaurantHelper restaurantHelper = new RestaurantHelper();
    static CMSHelper cmsHelper =  new CMSHelper();

    public int catId;
    public int subCatId;

    private Map<String, String> getDefaultHeader(){
        Map<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        return requestHeader;
    }

    public int getCategoryId(int restId) {
        catId = restaurantHelper.createCat(restId);
        return catId;
    }

    public int getSubCategoryId(int restId, int cat) {
        subCatId = restaurantHelper.createsubcat(restId, cat);
        return subCatId;
    }

    public int createRestaurantAndReturnID() {
        int restID = restaurantHelper.createDefaultRestaurant();
        System.out.println("Restaurant ID "+ restID );
        return restID;


    }

    public int getMenuIdForRestId(int restaurantId) {
        //List<Map<String, Object>> cms = cmsHelper.getMenuMap(Integer.toString(restaurantId));
        String query = Constants.menumap_query+Integer.toString(restaurantId);
        System.out.println("QUERY ::: "+query);
        DBHelper.pollDB(Constants.cmsDB,query,"restaurant_id",Integer.toString(restaurantId),2,30);
        System.out.println("Rest_ID ::: "+restaurantId+" -- MenuID ::: ");
        int menuId = Integer.parseInt(cmsHelper.getMenuMap(Integer.toString(restaurantId)).get(0).get(CmsConstants.menuid).toString());
        return menuId;
    }

    public Long getItemId(int restId, String name) {
        DBHelper dbHelper = new DBHelper();
        Map<String, Object> data;
        try {
            data = dbHelper.getMySqlTemplate("cms").queryForMap("select * from items where name=? and restaurant_id=?", name, restId);
        }catch (Exception e){
            return null;
        }
        return new BigInteger(data.get("id").toString()).longValue();
    }

    public Processor deleteItemByItemId(String id) {
        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "delete_item", gameofthrones);
        return new Processor(gots, (HashMap<String, String>) getDefaultHeader(), null, new String[]{id});
    }

    public Processor createVariantGroup(String itemId, String name, String order, String third_party) {
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        String[] payload = {itemId,name,order,third_party};
        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "CMS_CREATE_VARIANTGROUP", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, payload);
        return processor;
    }

    public String getTimeStamp() {
        long ts = System.nanoTime();
        String timeStamp = String.valueOf(ts);
        //debugger
        //System.out.println("TimeStamp::: "+timeStamp);
        return timeStamp;
    }

    public String getTimeStampRandomised() {
        Random random = new Random();
        int x = random.nextInt(100);
        String timeStamp = getTimeStamp();
        return timeStamp+String.valueOf(x);
    }

    public String getName() {
        String ts = getTimeStampRandomised();
        String name = "AutomationBS_"+ts;
        return name;
    }

    public int createCategoryIdAndReturnInt(int restId){
        catId = restaurantHelper.createCat(restId);
        return catId;
    }

    public int createSubCategoryAndReturnInt(int restId, int cat) {
        subCatId = restaurantHelper.createsubcat(restId,cat);
        return subCatId;
    }

    public String createItemIdAndReturnString(int restId, int cat, int subcat) {
        return restaurantHelper.createItem(restId, cat, subcat);
    }

    //returns ItemID, name, addon free limit, addon limit, price, service charges, restID, serves how many, in_stock
    public ArrayList<String> createItem(int rest_id, int cat_id, int subcat_id, int inStock) throws IOException {
        Processor processor = null;
        ArrayList<String> returnValues = new ArrayList<>();
        CMSHelper cmsHelper = new CMSHelper();
        RestBuilder restBuilder = new RestBuilder();
        AddItemToRestaurant itemToRestaurant = restBuilder.buildItemSlot();
        itemToRestaurant.setCategoryId(Integer.toString(subcat_id));
        itemToRestaurant.setMainCategoryId(cat_id);
        itemToRestaurant.setRestaurantId(rest_id);
        itemToRestaurant.setInStock(inStock);
        processor = cmsHelper.createItem(itemToRestaurant);
        returnValues.add(processor.ResponseValidator.GetNodeValue("data.uniqueId"));
        returnValues.add(itemToRestaurant.getName());
        returnValues.add(String.valueOf(itemToRestaurant.getAddon_free_limit()));
        returnValues.add(String.valueOf(itemToRestaurant.getAddon_limit()));
        returnValues.add(String.valueOf(itemToRestaurant.getPrice()));
        returnValues.add(itemToRestaurant.getServiceCharges());
        returnValues.add(String.valueOf(itemToRestaurant.getRestaurantId()));
        returnValues.add(String.valueOf(itemToRestaurant.getServesHowMany()));
        returnValues.add(String.valueOf(itemToRestaurant.getInStock()));
        return returnValues;
    }

    //@Overload
    public Processor createVariantGroup(String[] payload) {

        //String[] payload = {itemId,name,order,third_party};
        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "CMS_CREATE_VARIANTGROUP", gameofthrones);
        Processor processor = new Processor(gots, requestHeaders(), payload);
        return processor;
    }

    public Processor updateVariantGroup(String p_variantGroup_id,String q_variantGroup_id,String itemId, String name, String order, String third_party, String updated_by) {

        String[] payload = {p_variantGroup_id,itemId,name,order,third_party,updated_by};
        String[] queryParams = {q_variantGroup_id};
        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "CMS_UPDATE_VARIANTGROUP", gameofthrones);
        Processor processor = new Processor(gots, requestHeaders(), payload, queryParams);
        return processor;
    }

    public Processor getVariantGroupByID (String id) {

        String[] queryParams = {id};
        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "CMS_GETBYID_VARIANTGROUP", gameofthrones);
        Processor processor = new Processor(gots, requestHeaders(), null, queryParams);
        return processor;
    }

    public Processor deleteVariantGroupByID (String id) {

        String[] queryParams = {id};
        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "CMS_DELETE_VARIANTGROUP", gameofthrones);
        Processor processor = new Processor(gots, requestHeaders(), null, queryParams);
        return processor;
    }

    public Processor createVariant(String default_val,String cgst,String igst,String inclusive,String sgst,String in_stock,String is_veg,String name,String order,String price,String third_party,String new_variant_group_id) {

        String[] payload = {default_val,cgst,igst,inclusive,sgst,in_stock,is_veg,name,order,price,third_party,new_variant_group_id};
        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "CMS_CREATE_VARIANT", gameofthrones);
        Processor processor = new Processor(gots, requestHeaders(), payload);
        return processor;
    }

    //@Overload
    public Processor createVariant(String[] payload) {

        // String[] payload = {default_val,cgst,igst,inclusive,sgst,in_stock,is_veg,name,order,price,third_party,new_variant_group_id};
        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "CMS_CREATE_VARIANT", gameofthrones);
        Processor processor = new Processor(gots, requestHeaders(), payload);
        return processor;
    }


    public Processor updateVariant(String q_variant_id, String default_val,String cgst,String igst,String inclusive,String sgst,String p_variant_id,String in_stock,String is_veg,String name,String order,String price,String third_party,String updated_by, String variant_group ) {

        String[] payload = {default_val,cgst,igst,inclusive,sgst,p_variant_id,in_stock,is_veg,name,order,price,third_party,updated_by,variant_group};
        String[] queryParams = {q_variant_id};
        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "CMS_UPDATE_VARIANT", gameofthrones);
        Processor processor = new Processor(gots, requestHeaders(), payload, queryParams);
        return processor;
    }

    public Processor getVariantByID (String id) {

        String[] queryParams = {id};
        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "CMS_GETBYID_VARIANT", gameofthrones);
        Processor processor = new Processor(gots, requestHeaders(), null, queryParams);
        return processor;
    }

    public Processor searchVariant (String variantName) {

        String[] queryParams = {variantName};
        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "CMS_SEARCH_VARIANT", gameofthrones);
        Processor processor = new Processor(gots, requestHeaders(), null, queryParams);
        return processor;
    }

    public Processor deleteVariantHelper (String variantID) {

        String[] queryParams = {variantID};
        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "CMS_DELETE_VARIANT", gameofthrones);
        Processor processor = new Processor(gots, requestHeaders(), null, queryParams);
        return processor;
    }


    public Processor createItemHelper (String[] payload) {
        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "create_item", gameofthrones);
        Processor processor = new Processor(gots, requestHeaders(), payload, null);
        return processor;
    }

    public Processor createItemHelper (String payload) {
        HashMap<String, String> requestHeader = new HashMap<>();
        requestHeader.put("Content-Type", "application/json");
        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "create_item", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, new String[] {payload}, null);
        return processor;
    }

    public Processor createAddonGroupHelper(String[] payload) {

        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "CMS_CREATE_ADDONGROUP", gameofthrones);
        Processor processor = new Processor(gots, requestHeaders(), payload);
        return processor;
    }

    public Processor updateAddonGroupHelper(String[] payload, String addongroupID) {

        String[] queryparams = {addongroupID};
        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "CMS_UPDATE_ADDONGROUP", gameofthrones);
        Processor processor = new Processor(gots, requestHeaders(), payload, queryparams);
        return processor;
    }

    public Processor getAddonGroupByIdHelper(String addon_group_id) {

        String[] queryparams = {addon_group_id};
        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "CMS_GETBYID_ADDONGROUP", gameofthrones);
        Processor processor = new Processor(gots, requestHeaders(), null, queryparams);
        return processor;
    }

    public Processor searchAddonGroupByName(String addon_group_name) {

        String[] queryparams = {addon_group_name};
        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "CMS_SEARCH_ADDONGROUP", gameofthrones);
        Processor processor = new Processor(gots, requestHeaders(), null, queryparams);
        return processor;
    }

    public Processor deleteAddonGroupHelper(String addon_group_id) {

        String[] queryparams = {addon_group_id};
        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "CMS_DELETE_ADDONGROUP", gameofthrones);
        Processor processor = new Processor(gots, requestHeaders(), null, queryparams);
        return processor;
    }

    public Processor createAddonHelper(String payloadJson) {

        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "CMS_CREATE_ADDON", gameofthrones);
        String[] payload ={payloadJson};
        Processor processor = new Processor(gots, requestHeaders(), payload);
        return processor;
    }

    public Processor updateAddonHelper(String payloadJson, String addonID) {

        String[] payload ={payloadJson};
        String[] queryparams = {addonID};
        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "CMS_UPDATE_ADDON", gameofthrones);
        Processor processor = new Processor(gots, requestHeaders(), payload, queryparams);
        return processor;
    }


    public Processor getAddonByIdHelper(String addon_id) {

        String[] queryparams = {addon_id};
        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "CMS_GETBYID_ADDON", gameofthrones);
        Processor processor = new Processor(gots, requestHeaders(), null, queryparams);
        return processor;
    }


    public Processor searchAddonByName(String addon_name) {

        String[] queryparams = {addon_name};
        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "CMS_SEARCH_ADDON", gameofthrones);
        Processor processor = new Processor(gots, requestHeaders(), null, queryparams);
        return processor;
    }

    public Processor deleteAddonHelper(String addon_id) {

        String[] queryparams = {addon_id};
        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "CMS_DELETE_ADDON", gameofthrones);
        Processor processor = new Processor(gots, requestHeaders(), null, queryparams);
        return processor;
    }

    public Processor updateItemsHelper(String payloadJson, String itemID) {

        String[] payload ={payloadJson};
        String[] queryparams = {itemID};
        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "CMS_UPDATE_ITEM", gameofthrones);
        Processor processor = new Processor(gots, requestHeaders(), payload, queryparams);
        return processor;
    }

    /**
     * Bulk Update Items
     * @param payloadJson
     * @return Processor
     */
    public Processor bulkUpdateItems(String payloadJson) {
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        GameOfThronesService got = new GameOfThronesService("cmsbaseservice", "bulk_update_items", gameofthrones);
        Processor processor = new Processor(got, requestHeader, new String[]{payloadJson}, null);
        return processor;
    }


    public Processor createSingleItemHelper (String jsonPayload) {

        String[] payload ={jsonPayload};
        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "CMS_CREATE_ITEM", gameofthrones);
        Processor processor = new Processor(gots, requestHeaders(), payload);
        return processor;
    }

    public Processor getItemByIdHelper(String itemID) {

        String[] queryparams = {itemID};
        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "CMS_GETBYID_ITEM", gameofthrones);
        Processor processor = new Processor(gots, requestHeaders(), null,queryparams);
        return processor;
    }


    public List<Map<String, Object>> getVariantsFromDB(String query,String itemId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(Constants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(query+itemId);
        return list;

    }

    public List<Map<String, Object>> getVariantGroupsFromDB(String query,String itemId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(Constants.cmsDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(query+itemId);
        return list;
    }

    public Processor searchItemByNameHelper(String item_name) {

        String[] queryparams = {item_name};
        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "CMS_SEARCH_ITEM", gameofthrones);
        Processor processor = new Processor(gots, requestHeaders(), null, queryparams);
        return processor;
    }

    public Processor deleteItemHelper(String itemID) {

        String[] queryparams = {itemID};
        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "CMS_DELETE_ITEM", gameofthrones);
        Processor processor = new Processor(gots, requestHeaders(), null, queryparams);
        return processor;
    }

    public Processor createCategoryHelper(String payloadJson) {

        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "CMS_CREATE_CATEGORY", gameofthrones);
        String[] payload ={payloadJson};
        Processor processor = new Processor(gots, requestHeaders(), payload);
        return processor;
    }

    public Processor updateCategoryHelper(String payloadJson, String categoryID) {

        String[] payload ={payloadJson};
        String[] queryparams = {categoryID};
        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "CMS_UPDATE_CATEGORY", gameofthrones);
        Processor processor = new Processor(gots, requestHeaders(), payload, queryparams);
        return processor;
    }

    public Processor getCategoryByIdHelper(String catID) {

        String[] queryparams = {catID};
        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "CMS_GETBYID_CATEGORY", gameofthrones);
        Processor processor = new Processor(gots, requestHeaders(), null,queryparams);
        return processor;
    }

    public Processor getCategoryByRestIdHelper(String restID) {

        String[] queryparams = {restID};
        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "CMS_GETBYRESTID_CATEGORY", gameofthrones);
        Processor processor = new Processor(gots, requestHeaders(), null,queryparams);
        return processor;
    }

    public Processor getSubCategoryByCatIdHelper(String catID) {

        String[] queryparams = {catID};
        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "CMS_GETBYCATID_SUBCATEGORY", gameofthrones);
        Processor processor = new Processor(gots, requestHeaders(), null,queryparams);
        return processor;
    }

    public Processor searchCategoryHelper(String restId,String catName) {

        String[] queryparams = {restId,catName};
        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "CMS_SEARCH_CATEGORY", gameofthrones);
        Processor processor = new Processor(gots, requestHeaders(), null,queryparams);
        return processor;
    }

    public Processor deleteCategoryHelper(String catID) {

        String[] queryparams = {catID};
        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "CMS_DELETE_CATEGORY", gameofthrones);
        Processor processor = new Processor(gots, requestHeaders(), null, queryparams);
        return processor;
    }

    public Processor bulkAddonGroupHelper(String jsonPayload) {

        String[] payload = {jsonPayload};
        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "CMS_BULK_ADDONGROUPS", gameofthrones);
        Processor processor = new Processor(gots, requestHeaders(), payload);
        return processor;
    }

    public Processor bulkAddonHelper(String jsonPayload) {

        String[] payload = {jsonPayload};
        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "CMS_BULK_ADDONS", gameofthrones);
        Processor processor = new Processor(gots, requestHeaders(), payload);
        return processor;
    }

    public Processor bulkVariantGroupHelper(String jsonPayload) {

        String[] payload = {jsonPayload};
        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "CMS_BULK_VARIANTGROUPS", gameofthrones);
        Processor processor = new Processor(gots, requestHeaders(), payload);
        return processor;
    }

    public Processor bulkVariantHelper(String jsonPayload) {

        String[] payload = {jsonPayload};
        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "CMS_BULK_VARIANTS", gameofthrones);
        Processor processor = new Processor(gots, requestHeaders(), payload);
        return processor;
    }

    public Processor bulkCategoriesHelper(String jsonPayload) {

        String[] payload = {jsonPayload};
        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "CMS_BULK_CATEGORIES", gameofthrones);
        Processor processor = new Processor(gots, requestHeaders(), payload);
        return processor;
    }

    public Processor bulkItemsHelper(String jsonPayload) {

        String[] payload = {jsonPayload};
        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "CMS_BULK_ITEMS", gameofthrones);
        Processor processor = new Processor(gots, requestHeaders(), payload);
        return processor;
    }


    public Processor createSingleVariant(String jsonPayload) {

        String[] payload = {jsonPayload};
        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "CREATE_VARIANT", gameofthrones);
        Processor processor = new Processor(gots, requestHeaders(), payload);
        return processor;
    }

    public Processor getMenuhelper(String restID, String type) {

        String[] params = {restID, type};
        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "CMS_GETMENU", gameofthrones);
        Processor processor = new Processor(gots, requestHeaders(), null,params);
        return processor;
    }

    public boolean validateSameValueInArray(String[] inStock, String value) {
        for (int i = 0; i < inStock.length ; i++) {
            if(!inStock[i].equals(value))
                return false;
        }
        return true;
    }

    public boolean validateBulkErrorMessages(String message) {
        ArrayList<String>list_of_messages = new ArrayList<>();
        list_of_messages.add("DUPLICATE_ENTITY_WITH_SAME_NAME_DETAILED");
        list_of_messages.add("DUPLICATE_ENTITY_WITH_SAME_NAME");
        list_of_messages.add("INVALID_OPERATION");
        list_of_messages.add("DUPLICATE_ENTITY_WITH_SAME_THIRD_PARTY_ID_DETAILED");
        return list_of_messages.contains(message);
    }

    public HashMap<String, String> requestHeaders() {
        HashMap<String, String> requestHeader = new HashMap<>();
        requestHeader.put("Content-Type", "application/json");
        return requestHeader;

    }

    public Processor itemOutOfStock(String item_id,String from_time,String to_time) {
    	HashMap<String, String> requestHeader = new HashMap<String, String>();
    	requestHeader.put("Content-Type", "application/json");
    	requestHeader.put("authorization","Basic dmVuZG9yLXN5c3RlbTpzZWxmc2VydmUtc3lzdGVt");
        String[] payload = {item_id,from_time,to_time};
        String[] querypaylaod= {item_id};
        GameOfThronesService gots = new GameOfThronesService("cmsbaseservice", "itemoutofstock", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, payload,querypaylaod);
        return processor;
    }
    

    
	public int getRestId(boolean newRest) {
		int restID;
		if (newRest == true) {
			restID = restaurantHelper.createDefaultRestaurant();
			System.out.println("Restaurant ID " + restID);
		} else
			restID = CmsConstants.restaurantId;
		return restID;
	}


    public Processor createAnItemSlot(int itemId) throws Exception{
        Processor processor = null;
        try {
            HashMap<String, String> headers=new HashMap<>();
            JsonHelper jsonHelper = new JsonHelper();
            headers.put("Content-Type", "application/json");

            AddItemSlot addItemSlot = new AddItemSlot();
            addItemSlot.build(itemId);


            GameOfThronesService service = new GameOfThronesService("cmsbaseservice", "additemslots", gameofthrones);
            processor = new Processor(service, headers, new String[]{jsonHelper.getObjectToJSON(addItemSlot)}, null);

        }catch (Exception e){
            e.printStackTrace();
        }return processor;
    }

}
