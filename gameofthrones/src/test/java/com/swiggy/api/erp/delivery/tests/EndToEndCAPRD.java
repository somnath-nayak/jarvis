package com.swiggy.api.erp.delivery.tests;

import com.swiggy.api.erp.delivery.helper.EndToEndHelp;
import com.swiggy.api.sf.checkout.pojo.CreateMenuEntry;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Daenerys.Initializer;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import com.swiggy.api.erp.delivery.dp.EndtoEndDP;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceHelper;
import com.swiggy.api.erp.delivery.helper.OrderStatus;
import java.io.IOException;

public class EndToEndCAPRD extends EndtoEndDP{
	EndToEndHelp endToEndHelp= new EndToEndHelp();
	DeliveryServiceHelper delhelp=new DeliveryServiceHelper();
	//Initialize gameofthrones= new Initialize();
	Initialize gameofthrones =Initializer.getInitializer();

	@Test(dataProvider = "endtoend")
	public void createOrder(CreateMenuEntry payload, String de_id) throws IOException {
	String order_id=endToEndHelp.createOrder(payload);
	delhelp.processOrderInDeliveryStage(order_id, OrderStatus.DELIVERED, de_id);
	}



}
