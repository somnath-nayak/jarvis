package com.swiggy.api.erp.delivery.dp;

import com.swiggy.api.castleblack.CastleBlackHelper;
import com.swiggy.api.erp.delivery.helper.EndToEndHelp;
import com.swiggy.api.sf.checkout.constants.CheckoutConstants;
import com.swiggy.api.sf.checkout.pojo.*;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import framework.gameofthrones.Aegon.PropertiesHandler;
import org.testng.annotations.DataProvider;

import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;

import framework.gameofthrones.JonSnow.Processor;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class EndtoEndDP {
	static String order_id;
	CastleBlackHelper castleBlackHelper= new CastleBlackHelper();
	//EndToEndHelp endToEndHelp= new EndToEndHelp();
	SANDHelper sandHelper= new SANDHelper();

	String Env;
	public PropertiesHandler properties = new PropertiesHandler();

	public EndtoEndDP() {
		if (System.getenv("ENVIRONMENT") == null)
			Env = properties.propertiesMap.get("environment");
		else
			Env = System.getenv("ENVIRONMENT");
	}
	
	public static String createOrder() throws Exception {
		CheckoutHelper helperc = new CheckoutHelper();
		Object[][] data = cartItems();
		Processor p = helperc.placeOrder(DeliveryConstant.mobile,
				DeliveryConstant.password, data[0][0].toString(),
				data[0][1].toString(), data[0][2].toString());
		return order_id = p.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.order_id");
	}

	public static Object[][] cartItems() {
		return new Object[][] { { "815791", "2", "4977" } };
	}


	@DataProvider(name = "endtoend")
	public Object[][] endtoend() throws Exception {
		EndToEndHelp endToEndHelp = new EndToEndHelp();
		List<String>rest=endToEndHelp.Aggregator(new String[] {"12.9166","77.610"});
		System.out.println(rest.size());
		List<Cart> cart = new ArrayList<>();
		//cart.add(new Cart(null, null, endToEndHelp.getRestaurantMenu("683"), 1));
		int n=new Random().nextInt(rest.size());
		cart.add(new Cart(null, null, endToEndHelp.getMenuItemWithoutVariantWithMinAmt(rest.get(n),0), 1));
		CreateMenuEntry createOrder = new CreateOrderBuilder()
				.password("rkonowhere")
				.mobile(7406734416l)
				.cart(cart)
				.restaurant(Integer.valueOf(rest.get(n)))
				.paymentMethod("Cash")
				.orderComments("Test-Order")
				.buildAll();

		return new Object[][] {{createOrder,"1026"}};
	}

	@DataProvider(name = "endtoendprod")
	public Object[][] endtoendprod() throws Exception {
		EndToEndHelp endToEndHelp = new EndToEndHelp();
		List<Cart> cart = new ArrayList<>();
		//cart.add(new Cart(null, null, endToEndHelp.getRestaurantMenu("683"), 1));
		cart.add(new Cart(null, null, endToEndHelp.getRestaurantMenu("9990"), 1));
		CreateMenuEntry createOrder = new CreateOrderBuilder()
				.password("swiggy")
				.mobile(7899772315l)
				.cart(cart)
				.restaurant(9990)
				.paymentMethod("Cash")
				.orderComments("Test-Order")
				.buildAll();

		return new Object[][] {{createOrder,"216"}};
	}



	/*@DataProvider(name = "endtoend1")
	public Object[][] endtoend1(ITestContext testContext) throws Exception {
		CheckoutHelper checkoutHelper = new CheckoutHelper();
		checkoutHelper.menuItemsList("683");
		JsonHelper jsonHelper= new JsonHelper();
		List<Cart> cart = new ArrayList<>();
		cart.add(new Cart(null, null, "737823", 1));
		CreateMenuEntry createOrder = new CreateOrderBuilder()
				.password("rkonowhere")
				.mobile(7406734416l)
				.cart(cart)
				.restaurant(683)
				.paymentMethod("Cash")
				.orderComments("Test-Order")
				.buildAll();
		Object[] arr= {"stage1", createOrder,"2418"};
		//return new Object[][] {{arr, "2418"}};
		Object[][] dataSet = new Object[][] { arr};
		return ToolBox.returnReducedDataSet(Env, dataSet, testContext.getIncludedGroups(), dataSet.length, dataSet.length);
	}*/

	@DataProvider(name="login")
	public Object[][] login()
	{
		return new Object[][]{{CheckoutConstants.mobile1,CheckoutConstants.password1}};
	}

	@DataProvider(name = "orderCancel")
	public Object[][] orderCancel(){return new Object[][]{{"7899772315"}};

	}

	@DataProvider(name = "orderCancelRestId")
	public Object[][] orderCancelRestId(){return new Object[][]{{"9990"},{"4993"}};

	}

}
