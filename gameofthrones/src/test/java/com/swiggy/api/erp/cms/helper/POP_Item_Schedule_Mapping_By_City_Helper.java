package com.swiggy.api.erp.cms.helper;

import com.jayway.jsonpath.JsonPath;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/*
 * This class depends on the pop_area_schedule class
 *   Please delete the schedule from uat02 for respective area-code=3 as mentioned in POP_Area_Schedule class
 *   New item scheduling can be done from this class for same area-code and inventory and display-sequence 
 *   can be updated for the same
 */

public class POP_Item_Schedule_Mapping_By_City_Helper  {
	Initialize gameofthrones = new Initialize();
	static JsonPath jsonpath = null;
	public static int id = 0;
	public static int newlyCreatedCityScheduleId = 0;
	public static int newlyCreatedItemScheduleId = 0;
	POP_City_Schedule_Helper city_schedule = new POP_City_Schedule_Helper();	
	
	JSONObject json = new JSONObject();
	
	public HashMap<String, String> setHeaders1() throws JSONException {
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		JSONObject innerjson = new JSONObject();
		innerjson.put("user", "cms-tester");

		JSONObject json = new JSONObject();
		json.put("source", "cms");
		json.put("meta", innerjson);

		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("user-meta", json.toString());
		return requestheaders;
	}

	public String[] setPayload1(int item_id, int newlyCreatedCityScheduleId,
			String date, int display_sequence, int inventory) throws JSONException {
		return new String[] { Integer.toString(item_id), Integer.toString(newlyCreatedCityScheduleId),
				date, Integer.toString(display_sequence), Integer.toString(inventory)};
	}
	
	public String[] setPayload2(int display_sequence, int inventory) throws JSONException {
		return new String[] {Integer.toString(display_sequence), Integer.toString(inventory)};
	}

	public Processor createItemScheduleByCity_helper(int item_id, String date, int display_sequence, int inventory,
			String slot_type, int city_id, int openTime, int closeTime, String day, String menu_type)
					throws JSONException {
		POP_City_Schedule_Helper city_schedule = new POP_City_Schedule_Helper();
		Processor p1 = city_schedule.createCity_schedule_helper(slot_type, city_id, openTime, closeTime, day, menu_type);
		String city_schedule_response = p1.ResponseValidator.GetBodyAsText();
		newlyCreatedCityScheduleId = JsonPath.read(city_schedule_response, "$.data.id");

		GameOfThronesService service1 = new GameOfThronesService("cmsbaseservice", "itemschedulemappingbycitycreate",
				gameofthrones);
		Processor p2 = new Processor(service1, 
				setHeaders1(), 
				setPayload1(item_id, newlyCreatedCityScheduleId, date, display_sequence, inventory),
				null);
		String resp = p2.ResponseValidator.GetBodyAsText();
		newlyCreatedItemScheduleId = JsonPath.read(resp, "$.data.id");
		return p2;	
	}

	public Processor createItemScheduleByExistingCity_helper(int item_id, int city_schedule_id, String date, 
			int display_sequence, int inventory)
					throws JSONException {
		GameOfThronesService service1 = new GameOfThronesService("cmsbaseservice", "itemschedulemappingbycitycreate",
				gameofthrones);
		Processor p1 = new Processor(service1, 
				setHeaders1(), 
				setPayload1(item_id, city_schedule_id, date, display_sequence, inventory),
				null);
		return p1;	
	}

	public Processor deleteCityScheduleHelper() throws JSONException
	{
		Processor p = city_schedule.deleteCity_schedule_helper(newlyCreatedCityScheduleId);
		return p;
	}
	
	public Processor updateItemScheduleByCity_helper(int display_sequence, int inventory, int itemScheduleIdByCity) throws JSONException {
		
		GameOfThronesService service1 = new GameOfThronesService("cmsbaseservice", "itemschedulemappingbycityupdate",
				gameofthrones);
		Processor p1 = new Processor(service1, 
									 setHeaders1(), 
									 setPayload2(display_sequence, inventory),
									 new String[] { ""+ itemScheduleIdByCity });
		return p1;
	}


	public Processor getItemScheduleByCity_helper(int itemScheduleIdByCity) throws JSONException {
		GameOfThronesService service1 = new GameOfThronesService("cmsbaseservice", "itemschedulemappingbycityget", gameofthrones);
		Processor p1 = new Processor(service1, 
									setHeaders1(), 
									null, 	
									new String[] { ""+ itemScheduleIdByCity  });
		return p1;
	}

	public Processor deleteItemScheduleByCity_helper(int itemScheduleIdByCity) throws JSONException {
		GameOfThronesService service1 = new GameOfThronesService("cmsbaseservice", "itemschedulemappingbycitydelete", gameofthrones);
		Processor p1 = new Processor(service1, 
									 setHeaders1(), 
									 null, 
									 new String[] { ""+ itemScheduleIdByCity });
		POP_City_Schedule_Helper obj2 = new POP_City_Schedule_Helper();
		obj2.deleteCity_schedule_helper(newlyCreatedCityScheduleId);
		return p1;
	}
}
