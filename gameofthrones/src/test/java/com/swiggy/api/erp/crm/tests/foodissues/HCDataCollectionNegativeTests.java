package com.swiggy.api.erp.crm.tests.foodissues;

import static org.testng.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import com.swiggy.api.erp.crm.dp.foodissues.HCDataCollectionDP;

public class HCDataCollectionNegativeTests {

    Initialize gameofthrones = new Initialize();
    HCDataCollectionDP dp = new HCDataCollectionDP();
    public static String[] nodes = { "201", "202", "203","204","205" };

    @Test (dataProviderClass = HCDataCollectionDP.class,dataProvider = "orderManual", description = "Data Collection - Manual Negative")
    public void HCDataManual(HashMap<String,String> data,String conversationId,String status) throws Exception
    {

        HashMap<String, String> requestheaders_getData = new HashMap<String, String>();
        requestheaders_getData.put("Content-Type", "application/json");

        //Set the status of the created order according to the data provider input

        if(status!="unplaced")
            dp.statusUpdate(status);

        //Number of nodes to visit
        System.out.println("Number of nodes to visit "+ nodes.length);

        for(int i=0;i<nodes.length;i++)
        {

            String[] payload = new String[]{data.get("orderId").replace("[","").replace("]",""),conversationId};

            GameOfThronesService collectFoodIssueData = new GameOfThronesService("crm","validChild",gameofthrones);
            Processor dataCollection_response = new Processor(collectFoodIssueData,requestheaders_getData, payload, new String[] {nodes[i]});

            //Assert that the valid child hit got failure
            Assert.assertEquals(dataCollection_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("status"), "FAILURE");

            //Assert that node is not valid
            Assert.assertEquals(dataCollection_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("exception.errorCode"), "NODE_NOT_VALID");

        }
    }

    @Test (dataProviderClass = HCDataCollectionDP.class,dataProvider = "orderPartner", description = "Data Collection - Partner Negative")
    public void HCDataPartner(HashMap<String,String> data,String conversationId,String status) throws Exception
    {

        HashMap<String, String> requestheaders_getData = new HashMap<String, String>();
        requestheaders_getData.put("Content-Type", "application/json");

        //Set the status of the created order according to the data provider input

        if(status!="unplaced")
            dp.statusUpdate(status);

        //Number of nodes to visit
        System.out.println("Number of nodes to visit "+ nodes.length);

        for(int i=0;i<nodes.length;i++)
        {

            String[] payload = new String[]{data.get("orderId").replace("[","").replace("]",""),conversationId};

            GameOfThronesService collectFoodIssueData = new GameOfThronesService("crm","validChild",gameofthrones);
            Processor dataCollection_response = new Processor(collectFoodIssueData,requestheaders_getData, payload, new String[] {nodes[i]});

            //Assert that the valid child hit got failure
            Assert.assertEquals(dataCollection_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("status"), "FAILURE");

            //Assert that node is not valid
            Assert.assertEquals(dataCollection_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("exception.errorCode"), "NODE_NOT_VALID");


        }
    }

    @Test (dataProviderClass = HCDataCollectionDP.class,dataProvider = "orderDominos", description = "Data Collection - Dominos Negative")
    public void HCDataDominos(HashMap<String,String> data,String conversationId,String status) throws Exception
    {

        HashMap<String, String> requestheaders_getData = new HashMap<String, String>();
        requestheaders_getData.put("Content-Type", "application/json");

        //Set the status of the created order according to the data provider input

        if(status!="unplaced")
            dp.statusUpdate(status);

        //Number of nodes to visit
        System.out.println("Number of nodes to visit "+ nodes.length);

        for(int i=0;i<nodes.length;i++)
        {

            String[] payload = new String[]{data.get("orderId").replace("[","").replace("]",""),conversationId};

            GameOfThronesService collectFoodIssueData = new GameOfThronesService("crm","validChild",gameofthrones);
            Processor dataCollection_response = new Processor(collectFoodIssueData,requestheaders_getData, payload, new String[] {nodes[i]});

            //Assert that the valid child hit got failure
            Assert.assertEquals(dataCollection_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("status"), "FAILURE");

            //Assert that node is not valid
            Assert.assertEquals(dataCollection_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("exception.errorCode"), "NODE_NOT_VALID");


        }
    }

    @Test (dataProviderClass = HCDataCollectionDP.class,dataProvider = "orderSwiggyAssured", description = "Data Collection - SwiggyAssured Negative")
    public void HCDataSwiggyAssured(HashMap<String,String> data,String conversationId,String status) throws Exception
    {

        HashMap<String, String> requestheaders_getData = new HashMap<String, String>();
        requestheaders_getData.put("Content-Type", "application/json");

        //Set the status of the created order according to the data provider input

        if(status!="unplaced")
            dp.statusUpdate(status);

        //Number of nodes to visit
        System.out.println("Number of nodes to visit "+ nodes.length);

        for(int i=0;i<nodes.length;i++)
        {

            String[] payload = new String[]{data.get("orderId").replace("[","").replace("]",""),conversationId};

            GameOfThronesService collectFoodIssueData = new GameOfThronesService("crm","validChild",gameofthrones);
            Processor dataCollection_response = new Processor(collectFoodIssueData,requestheaders_getData, payload, new String[] {nodes[i]});

            //Assert that the valid child hit got failure
            Assert.assertEquals(dataCollection_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("status"), "FAILURE");

            //Assert that node is not valid
            Assert.assertEquals(dataCollection_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("exception.errorCode"), "NODE_NOT_VALID");


        }
    }

    @Test (dataProviderClass = HCDataCollectionDP.class,dataProvider = "orderPop", description = "Data Collection - POP Negative")
    public void HCDataPop(HashMap<String,String> data,String conversationId,String status) throws Exception
    {

        HashMap<String, String> requestheaders_getData = new HashMap<String, String>();
        requestheaders_getData.put("Content-Type", "application/json");

        //Set the status of the created order according to the data provider input

        if(status!="unplaced")
            dp.statusUpdate(status);

        //Number of nodes to visit
        System.out.println("Number of nodes to visit "+ nodes.length);

        for(int i=0;i<nodes.length;i++)
        {

            String[] payload = new String[]{data.get("orderId").replace("[","").replace("]",""),conversationId};

            GameOfThronesService collectFoodIssueData = new GameOfThronesService("crm","validChild",gameofthrones);
            Processor dataCollection_response = new Processor(collectFoodIssueData,requestheaders_getData, payload, new String[] {nodes[i]});

            //Assert that the valid child hit got failure
            Assert.assertEquals(dataCollection_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("status"), "FAILURE");

            //Assert that node is not valid
            Assert.assertEquals(dataCollection_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("exception.errorCode"), "NODE_NOT_VALID");


        }
    }

    @Test (dataProviderClass = HCDataCollectionDP.class,dataProvider = "orderNewUser", description = "Data Collection - NewUser Negative")
    public void HCDataNewUser(HashMap<String,String> data,String conversationId,String status) throws Exception
    {

        HashMap<String, String> requestheaders_getData = new HashMap<String, String>();
        requestheaders_getData.put("Content-Type", "application/json");

        //Set the status of the created order according to the data provider input

        if(status!="unplaced")
            dp.statusUpdate(status);

        //Number of nodes to visit
        System.out.println("Number of nodes to visit "+ nodes.length);

        for(int i=0;i<nodes.length;i++)
        {

            String[] payload = new String[]{data.get("orderId").replace("[","").replace("]",""),conversationId};

            GameOfThronesService collectFoodIssueData = new GameOfThronesService("crm","validChild",gameofthrones);
            Processor dataCollection_response = new Processor(collectFoodIssueData,requestheaders_getData, payload, new String[] {nodes[i]});

            //Assert that the valid child hit got failure
            Assert.assertEquals(dataCollection_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("status"), "FAILURE");

            //Assert that node is not valid
            Assert.assertEquals(dataCollection_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("exception.errorCode"), "NODE_NOT_VALID");


        }
    }

    @Test (dataProviderClass = HCDataCollectionDP.class,dataProvider = "orderLongDistance", description = "Data Collection - Long Distance Negative")
    public void HCDataLongDistance(HashMap<String,String> data,String conversationId,String status) throws Exception
    {

        HashMap<String, String> requestheaders_getData = new HashMap<String, String>();
        requestheaders_getData.put("Content-Type", "application/json");

        //Set the status of the created order according to the data provider input

        if(status!="unplaced")
            dp.statusUpdate(status);

        //Number of nodes to visit
        System.out.println("Number of nodes to visit "+ nodes.length);

        for(int i=0;i<nodes.length;i++)
        {

            String[] payload = new String[]{data.get("orderId").replace("[","").replace("]",""),conversationId};

            GameOfThronesService collectFoodIssueData = new GameOfThronesService("crm","validChild",gameofthrones);
            Processor dataCollection_response = new Processor(collectFoodIssueData,requestheaders_getData, payload, new String[] {nodes[i]});

            //Assert that the valid child hit got failure
            Assert.assertEquals(dataCollection_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("status"), "FAILURE");

            //Assert that node is not valid
            Assert.assertEquals(dataCollection_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("exception.errorCode"), "NODE_NOT_VALID");


        }
    }
}
