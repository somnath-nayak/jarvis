package com.swiggy.api.erp.crm.tests.MockServiceTests;

import com.swiggy.api.erp.crm.constants.FlowMapper;
import com.swiggy.api.erp.crm.constants.MockServiceConstants;
import com.swiggy.api.erp.crm.dp.MockServiceDP;
import com.swiggy.api.erp.crm.helper.MockServicesHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.WireMockHelper;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static org.testng.Assert.assertTrue;

public class MockCancellationFlows extends MockServiceDP {

    MockServicesHelper serviceHelper = new MockServicesHelper();
    WireMockHelper wireMockHelper = new WireMockHelper();

    FlowMapper flowMapper = new FlowMapper();
    MockServiceConstants mockServiceConstants = new MockServiceConstants();

    Initialize gameofthrones = new Initialize();

    @BeforeClass
    public void startMockServer() {
        serviceHelper.connectDB("helpcenter","mock");
        wireMockHelper.startMockServer(9005);

    }

    @AfterClass
    public void stopMockServer() {
        wireMockHelper.stopMockServer();
        serviceHelper.connectDB("helpcenter","integrated");
    }

    @Test(dataProvider = "mockRegularFlows", groups = {"regression"}, description = "Mock Regular - Cancellation Flows")
    public void mockRegularCancellationFlow(HashMap<String,String> flowDetails) throws IOException, InterruptedException {
        System.out.println("Starting Test " + flowDetails);

        String orderType = flowDetails.get("orderType").toString();
        String ffStatus = flowDetails.get("ffStatus").toString();
        String disposition = flowDetails.get("disposition").toString();
        String deliveryStatus = flowDetails.get("deliveryStatus").toString();
        String flowName = flowDetails.get("flow").toString();
        String sla = flowDetails.get("sla").toString();
        String conversationId, deviceId;
        String restaurant_type = flowDetails.get("restaurant_type").toString();

        HashMap<String, String> orderDetails = mockServiceConstants.getOrderDetails(orderType);
        System.out.println("order details" +orderDetails);

        HashMap<String, String> deliveryDetails =  mockServiceConstants.getDeliveryDetails(sla, deliveryStatus);
        String[] dispositionDetails = mockServiceConstants.getDispositionDetails().get(disposition);
        String[] flow = flowMapper.setFlow().get(flowName);

        String queryparam[] = new String[1];

        SoftAssert s = new SoftAssert();

        String nodeId, orderId;
        orderId = orderDetails.get("orderId").toString();
        conversationId = orderId;
        deviceId = orderId;

        int lengthofflow = flow.length;
        System.out.println("lengthofflow \t" + lengthofflow);

        //Following code is just to mock the ff order details api
//        serviceHelper.stubOrderDetailAPIResponse(orderDetails.get("orderId").toString(), orderDetails.get("restId").toString(),
//                orderDetails.get("itemId").toString(), orderDetails.get("cost").toString(), orderType, ffStatus, restaurant_type);

        // Order Details Call to checkout
        serviceHelper.stubOrderDetailAPIResponse2(orderDetails.get("orderId").toString(), orderDetails.get("restId").toString(),
                orderDetails.get("itemId").toString(), orderDetails.get("cost").toString(), orderType, ffStatus, restaurant_type);

        //Following will fetch the fixed response for dispoition and sub-disposition for any order
        serviceHelper.stubFetchAllDispositionIDs(orderDetails.get("orderId").toString(), dispositionDetails[0], dispositionDetails[1]);

        //Following will fetch the fixed cancellation fee(0) for any order
        serviceHelper.stubGetCancellationFee(orderDetails.get("orderId").toString());




        //Following will return a fixed success response for ff cancellation api for any order
        serviceHelper.stubFFcancellation(orderDetails.get("orderId").toString());

        serviceHelper.stubDeliveryTrackAPIResponse(
                orderDetails.get("orderId").toString(),
                deliveryDetails.get("deliveryStatus").toString(),
                deliveryDetails.get("assignedPredTime").toString(),
                deliveryDetails.get("assignedActualTime").toString(),
                deliveryDetails.get("confirmedPredTime").toString(),
                deliveryDetails.get("confirmedActualTime").toString(),
                deliveryDetails.get("arrivedPredTime").toString(),
                deliveryDetails.get("arrivedActualTime").toString(),
                deliveryDetails.get("pickedupPredTime").toString(),
                deliveryDetails.get("pickedupActualTime").toString(),
                deliveryDetails.get("reachedPredTime").toString(),
                deliveryDetails.get("reachedActualTime").toString(),
                deliveryDetails.get("deliveredPredTime").toString(),
                deliveryDetails.get("deliveredActualTime").toString());

        serviceHelper.stubRefundPGFlag(orderDetails.get("orderId").toString());

     //   serviceHelper.stubFraudServiceFF();

        for (int i = 0; i < lengthofflow-1; i++) {
            nodeId = flow[i];
            if(nodeId == "1" ){
                continue;
            } else {
                queryparam[0] = nodeId;
                String[] paylaodparam = new String[]{conversationId, deviceId, orderId};

                System.out.println("length of flow" + lengthofflow);

                HashMap<String, String> requestheaders_cancellationinbound = new HashMap<String, String>();
                requestheaders_cancellationinbound.put("Content-Type", "application/json");
                GameOfThronesService cancellationinbound = new GameOfThronesService("crm", "cancelFlow1", gameofthrones);

                Processor cancellationinbound_response = new Processor(cancellationinbound,
                        requestheaders_cancellationinbound, paylaodparam, queryparam);

                Assert.assertEquals(cancellationinbound_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("status"), "SUCCESS");

                String NodeList = cancellationinbound_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.childrenNodes..id").toString().replace("[", "").replace("]", "");
                List<String> nodes = Arrays.asList(NodeList.split(","));

                System.out.println("addonlist" + nodes);

                System.out.println("flowname[i+1]" + flow[i + 1]);

                assertTrue(nodes.contains(flow[i + 1]));
                int index = nodes.indexOf(flow[i + 1]);

                if (i < (lengthofflow - 2)) {
                    Assert.assertEquals(cancellationinbound_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.childrenNodes.[" + index + "].isLeaf"), "false");
                } else {
                    Assert.assertEquals(cancellationinbound_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.childrenNodes.[" + index + "].isLeaf"), "true");
                }
            }

        }
    }

    @Test(dataProvider = "mockPOPFlows", enabled = true, description = "Mock Pop - Cancellation Flows")
    public void mockPOPCancellationFlow(HashMap<String,String> flowDetails) throws IOException, InterruptedException {
        System.out.println("Starting Test " + flowDetails);

        String orderType = flowDetails.get("orderType").toString();
        System.out.println("order type " + orderType);
        String ffStatus = flowDetails.get("ffStatus").toString();
        String disposition = flowDetails.get("disposition").toString();
        String deliveryStatus = flowDetails.get("deliveryStatus").toString();
        String flowName = flowDetails.get("flow").toString();
        String sla = flowDetails.get("sla").toString();
        String conversationId, deviceId;
        String restaurant_type = flowDetails.get("restaurant_type").toString();

        HashMap<String, String> orderDetails = mockServiceConstants.getOrderDetails(orderType);
        System.out.println("orderDetails" +orderDetails);

        HashMap<String, String> deliveryDetails = mockServiceConstants.getDeliveryDetails(sla, deliveryStatus);
        String[] dispositionDetails = mockServiceConstants.getDispositionDetails().get(disposition);
        String[] flow = flowMapper.setFlow().get(flowName);

        String queryparam[] = new String[1];

        SoftAssert s = new SoftAssert();

        String nodeId, orderId;
        orderId = orderDetails.get("orderId").toString();
        conversationId = orderId;
        deviceId = orderId;

        int lengthofflow = flow.length;
        System.out.println("lengthofflow \t" + lengthofflow);

        //Following code is just to mock the ff order details api
//        serviceHelper.stubOrderDetailAPIResponse(orderDetails.get("orderId").toString(), orderDetails.get("restId").toString(),
//                orderDetails.get("itemId").toString(), orderDetails.get("cost").toString(), orderType, ffStatus, restaurant_type);


        serviceHelper.stubOrderDetailAPIResponse2(orderDetails.get("orderId").toString(), orderDetails.get("restId").toString(),
                orderDetails.get("itemId").toString(), orderDetails.get("cost").toString(), orderType, ffStatus, restaurant_type);

        //Following will fetch the fixed response for dispoition and sub-disposition for any order
        serviceHelper.stubFetchAllDispositionIDs(orderDetails.get("orderId").toString(), dispositionDetails[0], dispositionDetails[1]);

        //Following will fetch the fixed cancellation fee(0) for any order
        serviceHelper.stubGetCancellationFee(orderDetails.get("orderId").toString());

        //Following will return a fixed success response for ff cancellation api for any order
        serviceHelper.stubFFcancellation(orderDetails.get("orderId").toString());

        serviceHelper.stubDeliveryTrackAPIResponse(
                orderDetails.get("orderId").toString(),
                deliveryDetails.get("deliveryStatus").toString(),
                deliveryDetails.get("assignedPredTime").toString(),
                deliveryDetails.get("assignedActualTime").toString(),
                deliveryDetails.get("confirmedPredTime").toString(),
                deliveryDetails.get("confirmedActualTime").toString(),
                deliveryDetails.get("arrivedPredTime").toString(),
                deliveryDetails.get("arrivedActualTime").toString(),
                deliveryDetails.get("pickedupPredTime").toString(),
                deliveryDetails.get("pickedupActualTime").toString(),
                deliveryDetails.get("reachedPredTime").toString(),
                deliveryDetails.get("reachedActualTime").toString(),
                deliveryDetails.get("deliveredPredTime").toString(),
                deliveryDetails.get("deliveredActualTime").toString());

        serviceHelper.stubRefundPGFlag(orderDetails.get("orderId").toString());

 //       serviceHelper.stubFraudServiceFF();

        for (int i = 0; i < lengthofflow - 1; i++) {
            nodeId = flow[i];
            queryparam[0] = nodeId;
            String[] paylaodparam = new String[]{conversationId, deviceId, orderId};

            System.out.println("length of flow" + lengthofflow);

            HashMap<String, String> requestheaders_cancellationinbound = new HashMap<String, String>();
            requestheaders_cancellationinbound.put("Content-Type", "application/json");
            GameOfThronesService cancellationinbound = new GameOfThronesService("crm", "cancelFlow1", gameofthrones);

            Processor cancellationinbound_response = new Processor(cancellationinbound,
                    requestheaders_cancellationinbound, paylaodparam, queryparam);

            Assert.assertEquals(cancellationinbound_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("status"), "SUCCESS");

            String NodeList = cancellationinbound_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.childrenNodes..id").toString().replace("[", "").replace("]", "");
            List<String> nodes = Arrays.asList(NodeList.split(","));

            System.out.println("addonlist" + nodes);

            System.out.println("flowname[i+1]" + flow[i + 1]);

            assertTrue(nodes.contains(flow[i + 1]));
            int index = nodes.indexOf(flow[i + 1]);

            if (i < (lengthofflow - 2)) {
                Assert.assertEquals(cancellationinbound_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.childrenNodes.[" + index + "].isLeaf"), "false");
            } else {
                Assert.assertEquals(cancellationinbound_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.childrenNodes.[" + index + "].isLeaf"), "true");
            }

        }
    }

    @Test(dataProvider = "mockSelfServeFlows", enabled = true, description = "Mock Dominos - Cancellation Flows")
    public void mockSelfServeCancellationFlow(String flow,String ffstatus,String placedStatus,String delstatus) throws IOException, InterruptedException {
        System.out.println("Starting Test " + flow);

        String orderType = "self-delivery";
        String flowName = flow;
        String sla="onTrack";
        String conversationId, deviceId;

        HashMap<String, String> orderDetails = mockServiceConstants.getOrderDetails(orderType);
        HashMap<String, String> deliveryDetails = mockServiceConstants.getDeliveryDetails(sla, delstatus);
        System.out.println("orderDetails" +orderDetails);

        String[] flows = flowMapper.setFlow().get(flowName);

        String queryparam[] = new String[1];
        SoftAssert s = new SoftAssert();

        String nodeId, orderId;
        orderId = orderDetails.get("orderId").toString();
        conversationId = orderId;
        deviceId = orderId;

        int lengthofflow = flows.length;
        System.out.println("lengthofflow \t" + lengthofflow);

        //Following code is just to mock the ff order details api
        serviceHelper.stubOrderDetailsDominosAPIResponse(orderDetails.get("orderId").toString(), orderDetails.get("restId").toString(),
                orderDetails.get("itemId").toString(), orderDetails.get("cost").toString(), orderType, ffstatus,placedStatus);

        //Following will fetch the fixed cancellation fee(0) for any order
        serviceHelper.stubGetCancellationFee(orderDetails.get("orderId").toString());

        //Following will return a fixed success response for ff cancellation api for any order
        serviceHelper.stubFFcancellation(orderDetails.get("orderId").toString());

        serviceHelper.stubRefundPGFlag(orderDetails.get("orderId").toString());

    //    serviceHelper.stubFraudServiceFF();

                serviceHelper.stubDeliveryTrackAPIResponse(
                orderDetails.get("orderId").toString(),
                deliveryDetails.get("deliveryStatus").toString(),
                deliveryDetails.get("assignedPredTime").toString(),
                deliveryDetails.get("assignedActualTime").toString(),
                deliveryDetails.get("confirmedPredTime").toString(),
                deliveryDetails.get("confirmedActualTime").toString(),
                deliveryDetails.get("arrivedPredTime").toString(),
                deliveryDetails.get("arrivedActualTime").toString(),
                deliveryDetails.get("pickedupPredTime").toString(),
                deliveryDetails.get("pickedupActualTime").toString(),
                deliveryDetails.get("reachedPredTime").toString(),
                deliveryDetails.get("reachedActualTime").toString(),
                deliveryDetails.get("deliveredPredTime").toString(),
                deliveryDetails.get("deliveredActualTime").toString());

        for (int i = 0; i < lengthofflow - 1; i++) {
            nodeId = flows[i];
            queryparam[0] = nodeId;
            String[] paylaodparam = new String[]{conversationId, deviceId, orderId};

            System.out.println("length of flow" + lengthofflow);

            HashMap<String, String> requestheaders_cancellationinbound = new HashMap<String, String>();
            requestheaders_cancellationinbound.put("Content-Type", "application/json");
            GameOfThronesService cancellationinbound = new GameOfThronesService("crm", "cancelFlow1", gameofthrones);

            Processor cancellationinbound_response = new Processor(cancellationinbound,
                    requestheaders_cancellationinbound, paylaodparam, queryparam);

            Assert.assertEquals(cancellationinbound_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("status"), "SUCCESS");

            String NodeList = cancellationinbound_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.childrenNodes..id").toString().replace("[", "").replace("]", "");
            List<String> nodes = Arrays.asList(NodeList.split(","));

            System.out.println("addonlist" + nodes);

            System.out.println("flowname[i+1]" + flows[i + 1]);

            assertTrue(nodes.contains(flows[i + 1]));
            int index = nodes.indexOf(flows[i + 1]);

            if (i < (lengthofflow - 2)) {
                Assert.assertEquals(cancellationinbound_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.childrenNodes.[" + index + "].isLeaf"), "false");
            } else {
                Assert.assertEquals(cancellationinbound_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.childrenNodes.[" + index + "].isLeaf"), "true");
            }

        }
    }

    @Test(dataProvider = "mockCafeFlows", enabled = true, description = "Mock Cafe - Cancellation Flows")
    public void mockCafeCancellationFlow(String flow,String ffstatus,String placedStatus) throws IOException, InterruptedException {

        System.out.println("Starting Test " + flow);

        String orderType = "cafe";
        String flowName = flow;
        String conversationId, deviceId;

        HashMap<String, String> orderDetails = mockServiceConstants.getOrderDetails(orderType);
        System.out.println("orderDetails" +orderDetails);


        String[] flows = flowMapper.setFlow().get(flowName);

        String queryparam[] = new String[1];

        SoftAssert s = new SoftAssert();

        String nodeId, orderId;
        orderId = orderDetails.get("orderId").toString();
        conversationId = orderId;
        deviceId = orderId;

        int lengthofflow = flows.length;
        System.out.println("lengthofflow \t" + lengthofflow);

        //Following code is just to mock the ff order details api
        serviceHelper.stubOrderDetailsCafeAPIResponse(orderDetails.get("orderId").toString(), orderDetails.get("restId").toString(),
                orderDetails.get("itemId").toString(), orderDetails.get("cost").toString(), orderType, ffstatus,placedStatus);

        //Following will fetch the fixed cancellation fee(0) for any order
        serviceHelper.stubGetCancellationFee(orderDetails.get("orderId").toString());

        //Following will return a fixed success response for ff cancellation api for any order
        serviceHelper.stubFFcancellation(orderDetails.get("orderId").toString());

        serviceHelper.stubRefundPGFlag(orderDetails.get("orderId").toString());

   //     serviceHelper.stubFraudServiceFF();

        for (int i = 0; i < lengthofflow - 1; i++) {
            nodeId = flows[i];
            queryparam[0] = nodeId;
            String[] paylaodparam = new String[]{conversationId, deviceId, orderId};

            System.out.println("length of flow" + lengthofflow);

            HashMap<String, String> requestheaders_cancellationinbound = new HashMap<String, String>();
            requestheaders_cancellationinbound.put("Content-Type", "application/json");
            GameOfThronesService cancellationinbound = new GameOfThronesService("crm", "cancelFlow1", gameofthrones);

            Processor cancellationinbound_response = new Processor(cancellationinbound,
                    requestheaders_cancellationinbound, paylaodparam, queryparam);

            Assert.assertEquals(cancellationinbound_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("status"), "SUCCESS");

            String NodeList = cancellationinbound_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.childrenNodes..id").toString().replace("[", "").replace("]", "");
            List<String> nodes = Arrays.asList(NodeList.split(","));

            System.out.println("addonlist" + nodes);

            System.out.println("flowname[i+1]" + flows[i + 1]);

            assertTrue(nodes.contains(flows[i + 1]));
            int index = nodes.indexOf(flows[i + 1]);

            if (i < (lengthofflow - 2)) {
                Assert.assertEquals(cancellationinbound_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.childrenNodes.[" + index + "].isLeaf"), "false");
            } else {
                Assert.assertEquals(cancellationinbound_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.childrenNodes.[" + index + "].isLeaf"), "true");
            }

        }
    }

    @Test(dataProvider = "mockTakeAwayFlows", groups = {"regression"}, description = "Mock Take Away - Cancellation Flows")
    public void mockTakeAwayCancellationFlow(HashMap<String,String> flowDetails) throws IOException, InterruptedException {
        System.out.println("Starting Test " + flowDetails);
        String orderType = flowDetails.get("orderType").toString();
        String ffStatus = flowDetails.get("ffStatus").toString();
        String disposition = flowDetails.get("disposition").toString();
        String deliveryStatus = flowDetails.get("deliveryStatus").toString();
        String flowName = flowDetails.get("flow").toString();
        String sla = flowDetails.get("sla").toString();
        String conversationId, deviceId;
        String restaurant_type = flowDetails.get("restaurant_type").toString();

        HashMap<String, String> orderDetails = mockServiceConstants.getOrderDetails(orderType);
        System.out.println("order details" +orderDetails);

        HashMap<String, String> deliveryDetails =  mockServiceConstants.getDeliveryDetails(sla, deliveryStatus);
        String[] dispositionDetails = mockServiceConstants.getDispositionDetails().get(disposition);
        String[] flow = flowMapper.setFlow().get(flowName);

        String queryparam[] = new String[1];

        SoftAssert s = new SoftAssert();

        String nodeId, orderId;
        orderId = orderDetails.get("orderId").toString();
        conversationId = orderId;
        deviceId = orderId;

        int lengthofflow = flow.length;
        System.out.println("lengthofflow \t" + lengthofflow);

        //Following code is just to mock the ff order details api
//        serviceHelper.stubOrderDetailAPIResponse(orderDetails.get("orderId").toString(), orderDetails.get("restId").toString(),
//                orderDetails.get("itemId").toString(), orderDetails.get("cost").toString(), orderType, ffStatus, restaurant_type);

        // Order details call to checkout
        serviceHelper.stubOrderDetailAPIResponse2(orderDetails.get("orderId").toString(), orderDetails.get("restId").toString(),
                orderDetails.get("itemId").toString(), orderDetails.get("cost").toString(), orderType, ffStatus, restaurant_type);

        //Following will fetch the fixed response for dispoition and sub-disposition for any order
        serviceHelper.stubFetchAllDispositionIDs(orderDetails.get("orderId").toString(), dispositionDetails[0], dispositionDetails[1]);

        //Following will fetch the fixed cancellation fee(0) for any order
        serviceHelper.stubGetCancellationFee(orderDetails.get("orderId").toString());

        //Following will return a fixed success response for ff cancellation api for any order
        serviceHelper.stubFFcancellation(orderDetails.get("orderId").toString());

        serviceHelper.stubDeliveryTrackAPIResponse(
                orderDetails.get("orderId").toString(),
                deliveryDetails.get("deliveryStatus").toString(),
                deliveryDetails.get("assignedPredTime").toString(),
                deliveryDetails.get("assignedActualTime").toString(),
                deliveryDetails.get("confirmedPredTime").toString(),
                deliveryDetails.get("confirmedActualTime").toString(),
                deliveryDetails.get("arrivedPredTime").toString(),
                deliveryDetails.get("arrivedActualTime").toString(),
                deliveryDetails.get("pickedupPredTime").toString(),
                deliveryDetails.get("pickedupActualTime").toString(),
                deliveryDetails.get("reachedPredTime").toString(),
                deliveryDetails.get("reachedActualTime").toString(),
                deliveryDetails.get("deliveredPredTime").toString(),
                deliveryDetails.get("deliveredActualTime").toString());

        serviceHelper.stubRefundPGFlag(orderDetails.get("orderId").toString());

 //       serviceHelper.stubFraudServiceFF();

        for (int i = 0; i < lengthofflow-1; i++) {
            nodeId = flow[i];
            queryparam[0] = nodeId;
            String[] paylaodparam = new String[]{conversationId, deviceId,  orderId};

            System.out.println("length of flow" + lengthofflow);

            HashMap<String, String> requestheaders_cancellationinbound = new HashMap<String, String>();
            requestheaders_cancellationinbound.put("Content-Type", "application/json");
            GameOfThronesService cancellationinbound = new GameOfThronesService("crm", "cancelFlow1", gameofthrones);

            Processor cancellationinbound_response = new Processor(cancellationinbound,
                    requestheaders_cancellationinbound, paylaodparam, queryparam);

            Assert.assertEquals(cancellationinbound_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("status"), "SUCCESS");

            String NodeList = cancellationinbound_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.childrenNodes..id").toString().replace("[", "").replace("]", "");
            List<String> nodes = Arrays.asList(NodeList.split(","));

            System.out.println("addonlist" + nodes);

            System.out.println("flowname[i+1]" + flow[i + 1]);

            assertTrue(nodes.contains(flow[i + 1]));
            int index = nodes.indexOf(flow[i + 1]);

            if (i < (lengthofflow-2)) {
                Assert.assertEquals(cancellationinbound_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.childrenNodes.[" + index + "].isLeaf"), "false");
            } else {
                Assert.assertEquals(cancellationinbound_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.childrenNodes.[" + index + "].isLeaf"), "true");
            }

        }
    }
}

