package com.swiggy.api.erp.crm.tests.MockServiceTests;

import com.swiggy.api.erp.crm.constants.FlowMapper;
import com.swiggy.api.erp.crm.constants.MockServiceConstants;
import com.swiggy.api.erp.crm.dp.MockServiceDP;
import com.swiggy.api.erp.crm.helper.MockEditServiceHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.WireMockHelper;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import static org.testng.Assert.assertTrue;

public class MockEditFlows extends MockServiceDP{

    FlowMapper flowMap = new FlowMapper();
    Initialize gameofthrones = new Initialize();
    MockServiceConstants mockServiceConstants = new MockServiceConstants();
    MockEditServiceHelper mockEditServiceHelper = new MockEditServiceHelper();
    WireMockHelper wireMockHelper = new WireMockHelper();

    @BeforeClass
    public void startMockServer() {
        wireMockHelper.startMockServer(9005);
    }

    @AfterClass
    public void stopMockServer() {
        wireMockHelper.stopMockServer();
    }

    @Test(dataProvider = "editFlows", groups = {"regression"}, description = "Mock Regular - Edit Flows")
    public void mockRegularEditFlow(HashMap<String, String> flowDetails) throws IOException, InterruptedException
    {
        System.out.println("Starting Test " + flowDetails);

        String queryparam[] = new String[1];
        String conversationId, deviceId,portalResponse;

        //Header for the APIs
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("Content-Type", "application/json");

        //Data details from data provider
        String orderType = flowDetails.get("orderType").toString();
        String ffStatus = flowDetails.get("ffStatus").toString();
        String disposition = flowDetails.get("disposition").toString();
        String deliveryStatus = flowDetails.get("deliveryStatus").toString();
        String flowName = flowDetails.get("flow").toString();
        String sla = flowDetails.get("sla").toString();
        String orderEditable=flowDetails.get("orderEditable").toString();
        String itemAdditionAllowed=flowDetails.get("itemAdditionAllowed").toString();

        //Get Delivery Details
        HashMap<String, String> deliveryDetails =  mockServiceConstants.getDeliveryDetails(sla, deliveryStatus);
        String[] dispositionDetails = mockServiceConstants.getDispositionDetails().get(disposition);

        //Get the order details
        HashMap<String, String> orderDetails = mockServiceConstants.getOrderDetails(orderType);
        System.out.println("order details" +orderDetails);

        //Get the order details
        String orderId = orderDetails.get("orderId").toString();
        String restId = orderDetails.get("restId").toString();
        String itemId = orderDetails.get("itemId").toString();
        String cost = orderDetails.get("cost").toString();

        //Get the flow name from the data provider
        String flow=flowName;

        //Mock FF order details API
        mockEditServiceHelper.stubOrderDetailAPIResponse(orderId,restId,itemId,cost,orderType,ffStatus);

        //  Order details call to checkout
        mockEditServiceHelper.stubOrderDetailAPIResponse2(orderDetails.get("orderId").toString(), orderDetails.get("restId").toString(),
                orderDetails.get("itemId").toString(), orderDetails.get("cost").toString(), orderType, ffStatus, "TAKEAWAY");


        //Mock Delivery Track Api Response
        mockEditServiceHelper.stubDeliveryTrackAPIResponse(
                orderDetails.get("orderId").toString(),
                deliveryDetails.get("deliveryStatus").toString(),
                deliveryDetails.get("assignedPredTime").toString(),
                deliveryDetails.get("assignedActualTime").toString(),
                deliveryDetails.get("confirmedPredTime").toString(),
                deliveryDetails.get("confirmedActualTime").toString(),
                deliveryDetails.get("arrivedPredTime").toString(),
                deliveryDetails.get("arrivedActualTime").toString(),
                deliveryDetails.get("pickedupPredTime").toString(),
                deliveryDetails.get("pickedupActualTime").toString(),
                deliveryDetails.get("reachedPredTime").toString(),
                deliveryDetails.get("reachedActualTime").toString(),
                deliveryDetails.get("deliveredPredTime").toString(),
                deliveryDetails.get("deliveredActualTime").toString());

        //Mock FF dispositions and cancellation
        mockEditServiceHelper.stubGetCancellationFee(orderId);

        mockEditServiceHelper.stubFFcancellation(orderId);

        mockEditServiceHelper.stubFetchAllDispositionIDs(orderDetails.get("orderId").toString(), dispositionDetails[0], dispositionDetails[1]);

        //Mock Edit Check API
        mockEditServiceHelper.stubEditCheckAPI(itemId,cost);

        //Mock Edit Confirm API
        mockEditServiceHelper.stubEditConfirmAPI(orderId,itemId,cost);

        //Mock Checkout Order Detail API
        mockEditServiceHelper.stubCheckoutOrderDetailAPIResponse(orderId,restId,itemId,cost,orderType);

        //Mock Vendor Order Editable API
        mockEditServiceHelper.stubVendorOrderEditableAPIResponse(orderId,orderEditable,itemAdditionAllowed);

        //Get the list of nodes from the flow
        String[] flowToExecute= flowMap.setFlow().get(flow);
        int lengthofflow = flowToExecute.length;
        System.out.println(flowToExecute);

        for(int i=0;i<lengthofflow-1;i++){
            //Get the payload type based on the node id
            String apiName=flowMap.decidePayload(flowToExecute[i]);
            System.out.println(apiName);

            String nodeId = flowToExecute[i];
            if(nodeId == "1" ){
                continue;
            } else {
                queryparam[0] = nodeId;
                conversationId = orderId;
                portalResponse = flowDetails.get("portalResponse");
                deviceId = orderId;
                String[] paylaodparam = new String[]{};
                paylaodparam = new String[]{conversationId, orderId, portalResponse};

                GameOfThronesService editFlows = new GameOfThronesService("crm", apiName, gameofthrones);
                Processor editFlow_response = new Processor(editFlows, requestheaders, paylaodparam, queryparam);

                Assert.assertEquals(editFlow_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("status"), "SUCCESS");


                String NodeList = editFlow_response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.childrenNodes..id").toString().replace("[", "").replace("]", "");
                List<String> nodes = Arrays.asList(NodeList.split(","));

                System.out.println("nodeList" + nodes);

                System.out.println("flowname[i+1]" + flowToExecute[i + 1]);

                assertTrue(nodes.contains(flowToExecute[i + 1]));
            }
        }

    }
}
