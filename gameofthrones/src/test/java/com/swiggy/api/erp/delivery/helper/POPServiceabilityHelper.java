package com.swiggy.api.erp.delivery.helper;

import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;

import java.util.HashMap;

public class POPServiceabilityHelper {
	Initialize gameofthrones = new Initialize();
	DeliveryHelperMethods delmeth = new DeliveryHelperMethods();

	public boolean poplisting(String city_id, String customer_lat_long,
			String restid) throws InterruptedException {
		String query = "select * from restaurant r inner join area a on r.area_code=a.id where r.id in ("
				+ restid + ")" + ";";
		Object obj = delmeth.dbhelperget(query, "zone_id");
		if (null == obj) {
			return false;
		} else {
			String zoneid = obj.toString();
			delmeth.redisconnectsetboolean(DeliveryConstant.popbanner, zoneid,
					DeliveryConstant.redisdb, true);
			GameOfThronesService service = new GameOfThronesService(
					"deliverycerebro", "listingpop", gameofthrones);
			HashMap<String, String> requestheaders = delmeth.singleheader();
			String[] payloadparams = new String[] { city_id, customer_lat_long };
			Thread.sleep(20000);
			Processor processor = new Processor(service, requestheaders,
					payloadparams);
            return (processor.ResponseValidator.GetResponseCode()) == 200;
		}
	}

	public boolean poplistingwithbanner(String city_id,
			String customer_lat_long, String restid)
			throws InterruptedException {
		String query = "select * from restaurant r inner join area a on r.area_code=a.id where r.id in ("
				+ restid + ")" + ";";
		Object obj = delmeth.dbhelperget(query, "zone_id");
		if (null == obj) {
			return false;
		} else {
			String zoneid = obj.toString();
			delmeth.redisconnectsetboolean(DeliveryConstant.popbanner, zoneid,
					DeliveryConstant.redisdb, false);
			GameOfThronesService service = new GameOfThronesService(
					"deliverycerebro", "listingpop", gameofthrones);
			HashMap<String, String> requestheaders = delmeth.singleheader();
			String[] payloadparams = new String[] { city_id, customer_lat_long };
			Thread.sleep(20000);
			Processor processor = new Processor(service, requestheaders,
					payloadparams);
            return (processor.ResponseValidator.GetResponseCode()) == 200;
		}

	}

	public boolean popcart(String restaurant_id, String area_id,
			String city_id, String rest_lat_long, String cust_address_lat_long)
			throws InterruptedException {
		GameOfThronesService service = new GameOfThronesService(
				"deliverycerebro", "cartpop", gameofthrones);
		HashMap<String, String> requestheaders = delmeth.singleheader();
		String[] payloadparams = new String[] { city_id, area_id, city_id,
				rest_lat_long, cust_address_lat_long };
		Processor processor = new Processor(service, requestheaders,
				payloadparams);
		Thread.sleep(20000);
        return (processor.ResponseValidator.GetResponseCode()) == 200;
	}
}
