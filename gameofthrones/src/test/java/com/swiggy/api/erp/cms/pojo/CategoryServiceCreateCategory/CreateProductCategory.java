package com.swiggy.api.erp.cms.pojo.CategoryServiceCreateCategory;

import com.swiggy.api.erp.cms.constants.CatalogV2Constants;
import com.swiggy.api.erp.cms.helper.CatalogV2Helper;

import java.util.Arrays;

public class CreateProductCategory {
    CatalogV2Helper catalogV2Helper=new CatalogV2Helper();

    private String name;

    private String description;

    private String template_url;

    private Attributes[] attributes;

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getTemplate_url() {
        return template_url;
    }

    public void setTemplate_url(String template_url) {
        this.template_url = template_url;
    }

    public Attributes[] getAttributes ()
    {
        return attributes;
    }

    public void setAttributes (Attributes[] attributes)
    {
        this.attributes = attributes;
    }

    public CreateProductCategory build() {
        setDefaultValues();
        return this;
    }

    public void setDefaultValues() {
        Attributes attributes=new Attributes();
        attributes.build();

        if(this.getName()==null){
            this.setName(catalogV2Helper.categoryName());
        }
        if(this.getDescription()==null){
            this.setDescription(CatalogV2Constants.description);
        }
        if(this.getTemplate_url()==null){
            this.setTemplate_url(CatalogV2Constants.template_url);
        }
        if(this.getAttributes()==null){
            this.setAttributes(new Attributes[]{attributes});
        }

    }


    @Override
   public String toString()
   {
       return "ClassPojo [description = "+description+", name = "+name+", attributes = "+attributes+"]";
   }
}
