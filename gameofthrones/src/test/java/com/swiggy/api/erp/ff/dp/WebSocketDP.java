package com.swiggy.api.erp.ff.dp;

import org.testng.ITestContext;
import org.testng.annotations.DataProvider;

public class WebSocketDP {

    @DataProvider
    public static Object[][] assignmentNegative(ITestContext testContext) {
        Object[] arr1 = { "assignment1", "234", "", "7685486", "45653"};
        Object[] arr2 = { "", "234", "", "7685486", "45653"};
        Object[] arr3 = { "assignment1", "23423232", "", "7685486", "45653"};
        Object[] arr4 = { "assignment", "", "", "7685486", "45653"};
        Object[] arr5 = { "assignment", "234", "", "", "45653"};
        Object[] arr6 = { "assignment1", "234", "", "7685486", ""};
        Object[][] dataSet = new Object[][] { arr1, arr2, arr3, arr4, arr5, arr6 };
        return dataSet;
    }

}
