package com.swiggy.api.erp.delivery.dp;

import org.testng.annotations.DataProvider;

public class CerebroEnhancementsDataProvider {

    // The following lat lng is for zone 6

    @DataProvider(name = "DP")                                              // test_case : QE-564,QE-565,QE-566
        public Object [][] DataProvider(){
        return new Object[][] { { "12.956542", "77.701255", "1" } };        // lat lng which is lying under any zone polygon, city_id
    }

    @DataProvider(name = "DP1")                                             // test_case: QE-567
    public Object [][] DataProvider1(){
        return new Object[][] { { "12.956542", "77.701255", "402" } };      // lat, lng, city_id, rest_id (restaurant and customer should be in diff zone)
    }

    @DataProvider(name = "DP2")                                             // test_case: QE-568
    public Object [][] DataProvider2(){
        return new Object[][] { { "12.955920", "77.688173", "1" } };        // lat lng which is not lying under any zone polygon, city_id
    }
}
