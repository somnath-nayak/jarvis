package com.swiggy.api.erp.cms.pojo.BaseServiceBulkItems;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.erp.cms.pojo.BaseServiceBulkItems
 **/

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "external_group_id",
        "group_id",
        "item_id",
        "name",
        "variations"
})
public class Variant_group {

    @JsonProperty("external_group_id")
    private String external_group_id;
    @JsonProperty("group_id")
    private String group_id;
    @JsonProperty("item_id")
    private String item_id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("variations")
    private List<Variation> variations = null;

    @JsonProperty("external_group_id")
    public String getExternal_group_id() {
        return external_group_id;
    }

    @JsonProperty("external_group_id")
    public void setExternal_group_id(String external_group_id) {
        this.external_group_id = external_group_id;
    }

    @JsonProperty("group_id")
    public String getGroup_id() {
        return group_id;
    }

    @JsonProperty("group_id")
    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    @JsonProperty("item_id")
    public String getItem_id() {
        return item_id;
    }

    @JsonProperty("item_id")
    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("variations")
    public List<Variation> getVariations() {
        return variations;
    }

    @JsonProperty("variations")
    public void setVariations(List<Variation> variations) {
        this.variations = variations;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("external_group_id", external_group_id).append("group_id", group_id).append("item_id", item_id).append("name", name).append("variations", variations).toString();
    }

}
