package com.swiggy.api.erp.cms.dp;

import java.io.IOException;
import java.net.URLEncoder;

import org.json.JSONException;
import org.testng.annotations.DataProvider;

import com.ibm.icu.text.DateFormat;
import com.ibm.icu.text.SimpleDateFormat;
import com.ibm.icu.util.Calendar;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.helper.SelfServeHelper;

public class CategoryInStockOutStockDp {

	SelfServeHelper sshelper=new SelfServeHelper();
	
	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	Calendar cal = Calendar.getInstance();
	long date=System.currentTimeMillis()+5*60*1000;
 	String fromDate =dateFormat.format(cal.getTime());
	String toDate = dateFormat.format(date);
	String InvalidDateFormate=dateFormat.format(date);
	String restId="9990";
	
	
	
	@DataProvider(name = "CategoryOutOfStock")
	public Object[][] CategoryOutStock() throws Exception {
	
		return new Object[][] { 
		     {"9990","980985",fromDate,toDate,true},
			 {"9990","0",fromDate,toDate,false},
			 {"9990","980985",InvalidDateFormate,toDate,false}
		};
		

	}
	@DataProvider(name = "CategoryInStock")
	public Object[][] CategoryInStock() throws IOException, JSONException {

		return new Object[][] { 
		     {"9990","980985",true},
			 {"9990","0",false},
			 {"0","0",false}
		};

	}

	@DataProvider(name = "ItemInstock")
	public Object[][] ItemInstockOutSTock() throws Exception {
		
		
		String thirdpartyitemId= sshelper.getMenu(restId,"true").ResponseValidator.GetBodyAsText();
		String[] itemids=JsonPath.read(thirdpartyitemId,"$.data.restaurantModel.categories[*].subCategories[*].menu[*].thirdPartyID").toString().replace("[","").replace("]","").split(",");
		for (int i=0;i<itemids.length;i++)
		{
			thirdpartyitemId=itemids[i].replace("\"", "");
			if(thirdpartyitemId!=null && !thirdpartyitemId.isEmpty())
				{
				thirdpartyitemId=thirdpartyitemId;
				break;
				}
			
		
		}			
			return new Object[][] { 
	  	//(String partner_type,String partner_id,String third_party_item_ids,String source,String in_stock,boolean successState)
		
		     {"3",restId,thirdpartyitemId,"VENDOR","true",true},
		     {"3","0",thirdpartyitemId,"VENDOR","true",false},
	         {"0",restId,thirdpartyitemId,"VENDOR","true",false}
		};

	}
	
	

	@DataProvider(name = "ItemOutSTock")
	public Object[][] ItemOutSTock() throws Exception {
		String itemId= sshelper.getMenu("5271","true").ResponseValidator.GetBodyAsText();
		itemId=JsonPath.read(itemId,"$.data.restaurantModel.categories[0].subCategories[0].menu[0].thirdPartyID").toString();
	
	return new Object[][] { 
		
		//(String partner_type,String partner_id,String third_party_item_ids,String source,String in_stock,String fromTime,String toTime,boolean successState)
		     {"3",restId,itemId,"VENDOR","true",fromDate,toDate,true},
		     {"0",restId,"9222472_test","VENDOR","true",fromDate,toDate,false},
		     {"3","0","9222472_test","VENDOR","true",fromDate,toDate,false},
		     {"3",restId,"9222472_test","VENDOR","true",InvalidDateFormate,toDate,false},
			
		};

	}
	
	
	
	
	
	
}
