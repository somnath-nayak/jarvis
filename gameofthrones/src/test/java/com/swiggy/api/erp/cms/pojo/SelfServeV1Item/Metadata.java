package com.swiggy.api.erp.cms.pojo.SelfServeV1Item;

import com.swiggy.api.erp.cms.constants.SelfServeConstants;

public class Metadata
{
private String phone_numbers;

public String getPhone_numbers ()
{
return phone_numbers;
}

public void setPhone_numbers (String phone_numbers)
{
this.phone_numbers = phone_numbers;
}
public Metadata build() {
	setDefaultValues();
	return this;
}

private void setDefaultValues() {
	if(this.getPhone_numbers()==null)
          this.setPhone_numbers(SelfServeConstants.phoneno);
}
@Override
public String toString()
{
return "ClassPojo [phone_numbers = "+phone_numbers+"]";
}
}