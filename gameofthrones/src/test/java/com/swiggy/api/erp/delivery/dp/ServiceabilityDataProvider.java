package com.swiggy.api.erp.delivery.dp;

import org.testng.annotations.DataProvider;

public class ServiceabilityDataProvider {


	@DataProvider(name = "Listing")
	public static Object[][] ListingData() {
		return new Object[][]{
				{"12.9081", "77.6476", "1", "6404", "3"},
		};
	}

	@DataProvider(name = "deliveryListing_Simple")
	public Object[][] deliveryListing_Simple(){
		//String lat, String lng, String city_id, String[] resIds
		return new Object[][] {
				{ "12.9081", "77.6476", "1", "6404", "3"}};

	}


	@DataProvider(name = "deliveryCartSLA")
	public Object[][] deliveryCartSLA(){
		//restaurant_id, area_id, city_id, lat_long, address_lat_long
		return new Object[][] {
				{ "223", "1", "1", "12.935975,77.61602300000004", "12.935975,77.61602300000004"}};

	}


		@DataProvider(name = "Listing391")
		public static Object[][] ListingDat1() {
			return new Object[][] {

			{ "12.935975", "77.61602300000004", "223", "1"},
		};
		}
		@DataProvider(name = "Listing388")
		public static Object[][] ListingData2() {
			return new Object[][] {

			{ "18.146173", "78.4650954", "677", "1"},
		};
		}
		
		@DataProvider(name = "Cart")
		public static Object[][] CartData2() {
			return new Object[][] {

			{"677", "4","4","416019","3", "782991","783169"},
		};
		}

		@DataProvider(name = "Menu")
		public static Object[][] MenuData1() {
			return new Object[][] {

			{ "12.935975", "77.61602300000004", "677", "1"},
		};
		}
		
		@DataProvider(name="ServiceabilityDP_Listing")
		public static Object[][]ServiceabilityDP_Listing(){
			return new Object[][] {{"12.9569", "77.7015", "620"}};
		}
		
		@DataProvider(name="Cart1")
		public static Object[][]ServiceabilityDP_Cart(){
			return new Object[][] {{"620", "1499352", "1", "6", "13169218"}};
		}

	@DataProvider(name = "deliveryListing_Simple1")
	public Object[][] deliveryListing_Simple1(){
		//String lat, String lng, String city_id, String[] resIds
		return new Object[][] {
//				{ "12.956542", "77.701255", "1", "true"}
				{"22.643613815307617","88.42252349853516","7","true"},
//				{ "12.934859", "77.620062", "1", "6404"}
//				{ "22.5875123", "88.4184024", "7", "6404"}//,
//				{"22.5886812","88.4174046","7","6404"}
//				{"40.77","73.98","7",""},
				{"22.5957418","88.41636749999998","7","true"}, //10
				{"22.511605","88.32452799999999","7","true"}, //130
				{"22.523715","88.345415","7","true"}, //80
				{"22.539097","88.34778499999993","7","false"}, //120
				{"22.549131","88.40059199999996","7","false"}, //400
				{"22.570001","88.351045","7","false"}, //170
				{"22.6231121","88.44471569999996","7","false"},//220
				{"22.49342","88.38900999999998","7","false"},//180
				{"22.643613815307617","88.42252349853516","7","false"},//0
				{"22.56466293334961","88.3217544555664","7","false"},//5
				{"22.585786","88.49001999999996","7","false"},//100
				{"22.507288","88.367795","7","false"},//110
				{"22.5689776","88.43358190000004","7",""},//, 5
				{"22.49366","88.38941399999999","7",""},//10
				{"22.466655","88.31104170000003","7",""},//30
				{"22.6340389251709","88.37774658203125","7",""}//,//120
//				{"22.538976669311523","88.32232666015625","7","true"}//, perfect
//				{"22.624101","88.35336800000005","7",""}//30
//				{"22.631269","88.3499543","7",""} //- perfect
		};

	}

	@DataProvider(name = "deliveryListingResponseCountCheck")
	public Object[][] deliveryListingResponseCountCheck(){
		return new Object[][] {
//				{ "12.935975", "77.61602300000004", "1", "true",0},
//				{"22.538976669311523","88.32232666015625","7","true",2},
//				{"22.5957418","88.41636749999998","7","true",1}, //10
//				{"22.511605","88.32452799999999","7","true",1}, //130
//				{"22.523715","88.345415","7","true",1}, //80
//				{"22.539097","88.34778499999993","7","true",1}, //120
//				{"22.549131","88.40059199999996","7","true",1}, //400
//				{"22.570001","88.351045","7","true",1}, //170
//				{"22.6231121","88.44471569999996","7","true",1},//220
//				{"22.49342","88.38900999999998","7","true",1},//180
				{"22.643613815307617","88.42252349853516","7","true",1}//,//0
//				{"22.56466293334961","88.3217544555664","7","true",1},//5
//				{"22.585786","88.49001999999996","7","true",1},//100
//				{"22.507288","88.367795","7","true",1},//110
//				{"22.5689776","88.43358190000004","7","true",1},//, 5
//				{"22.49366","88.38941399999999","7","true",1},//10
//				{"22.466655","88.31104170000003","7","true",1},//30
//				{"22.6340389251709","88.37774658203125","7","true",1},//120
//				{"22.538976669311523","88.32232666015625","7","true",1}//, perfect//, perfect
		};}

	//cacheCheck
	@DataProvider(name = "cacheCheck")
	public Object[][] cacheCheck(){
		return new Object[][] {
				{"22.538976669311523","88.32232666015625","7","true",2.0}//, perfect
		};}


	@DataProvider(name = "distanceFilterCheck")
	public Object[][] distanceFilterCheck(){
		return new Object[][] {
				{ "12.976042", "77.72689300000002", "7", "true",3.0},
//				{"22.538976669311523","88.32232666015625","7","true",10.0}//, perfect
		};}

	@DataProvider(name = "slaFilterCheck")
	public Object[][] slaFilterCheck(){
		return new Object[][] {
				{ "12.935975", "77.61602300000004", "1", "true",50},
//				{"22.538976669311523","88.32232666015625","7","true",10}//, perfect
		};}

	@DataProvider(name = "bannerFactorCheck")
	public Object[][] bannerFactorCheck(){
		return new Object[][] {
				{ "12.935975", "77.61602300000004", "1", "true",3.0},
//				{"22.538976669311523","88.32232666015625","7","true",10.0}//, perfect
		};}

	@DataProvider(name = "delivery_Daily")
	public Object[][] delivery_Daily(){
		//String lat, String lng, String city_id, String[] resIds
		return new Object[][] {
				{"22.5886812","88.4174046","6404","7","1",false,false}
		};

	}

}

