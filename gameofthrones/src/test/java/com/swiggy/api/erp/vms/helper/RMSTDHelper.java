package com.swiggy.api.erp.vms.helper;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Daenerys.Initializer;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/***
 * 
 * @author ramzi
 *
 */
public class RMSTDHelper {
	static Initialize gameofthrones = Initializer.getInitializer();
	String token;
	public List<String> getRestaurantList() {

		List<String> list = Arrays.asList("30751");
		return list;
	}

	public static HashMap<String, String> getDefaultHeaders(String accessToken) {
		HashMap<String, String> header = new HashMap<String, String>();
		header.put("Content-Type", "application/json");
		header.put("Cookie", "Swiggy_Session-alpha="+accessToken);
		return header;
	}

	

	public String getLoginToken(String userName, String password) {
		GameOfThronesService services=new GameOfThronesService("rms", "rmslogin", gameofthrones);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		Processor processor = new Processor(services, requestheaders, new String[] { userName, password });
		token = processor.ResponseValidator.GetNodeValue("data.access_token");
		System.out.println("tokeeeeeeennnnnnnnnnnnn" + token);
		return token;
	}
	
	public Processor createTD(String payload,String accessToken) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders = getDefaultHeaders(accessToken);
		GameOfThronesService service = new GameOfThronesService("rms", "createTd", gameofthrones);
		String[] payloadparams = new String[] { payload };
		Processor processor = new Processor(service, requestHeaders, payloadparams);
		return processor;
	}
	
	public Processor editTD(String payload,String accessToken) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders = getDefaultHeaders(accessToken);
		GameOfThronesService service = new GameOfThronesService("rms", "editTd", gameofthrones);
		String[] payloadparams = new String[] { payload };
		Processor processor = new Processor(service, requestHeaders, payloadparams);
		return processor;
	}
	
	public Processor getTDbyRest(String sid, ArrayList<String> restaurantId) {
		GameOfThronesService service = new GameOfThronesService("rms", "getTDbyRest", gameofthrones);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + sid);
		
		//String[] payloadparams = new String[] { restaurantId };
		Processor processor = new Processor(service, requestheaders, new String[] {String.valueOf(restaurantId).replace('[', ' ').replace(']', ' ').replace(", ", ",").trim() }, null);
		return processor;
	}
	

	public Processor getTDdetails(String sid, String restaurantId, int discountId) {
		GameOfThronesService service = new GameOfThronesService("rms", "getDiscountDetails", gameofthrones);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + sid);
		
		//String[] payloadparams = new String[] { restaurantId };
		Processor processor = new Processor(service, requestheaders, null, new String[] {restaurantId, String.valueOf(discountId) });
		return processor;
	}
	
	public Processor metaData(String sid, String restaurantId) {
		GameOfThronesService service = new GameOfThronesService("rms", "getMetaData", gameofthrones);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + sid);
		
		//String[] payloadparams = new String[] { restaurantId };
		Processor processor = new Processor(service, requestheaders, null, new String[] {restaurantId });
		return processor;
	}

	public Processor getTDMenuData(String accessToken, String restaurantId) {
		GameOfThronesService service = new GameOfThronesService("rms", "getTDMenuData", gameofthrones);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);

		Processor processor = new Processor(service, requestheaders, null, new String[] {restaurantId});
		return processor;
	}

	public Processor getTDMetrics(String accessToken, String restaurantId, String discountId) {
		GameOfThronesService service = new GameOfThronesService("rms", "getTDMetrics", gameofthrones);
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("Content-Type", "application/json");
		requestheaders.put("Cookie", "Swiggy_Session-alpha=" + accessToken);

		Processor processor = new Processor(service, requestheaders, null, new String[] {restaurantId, discountId});
		return processor;
	}
	
}
