package com.swiggy.api.erp.ff.tests.omtRewrite.omtMiddleware;

import org.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.swiggy.api.erp.ff.dp.omtRewrite.OmtMiddlewareData;
import com.swiggy.api.erp.ff.helper.OmtMiddlewareHelper;
import framework.gameofthrones.JonSnow.Processor;

import java.util.HashMap;

public class GetOrderDetailsAPITests extends OmtMiddlewareData {
	OmtMiddlewareHelper OmtMiddlewareHelper = new OmtMiddlewareHelper();
	
	@Test(dataProvider = "getOrderDetailsSuperOrder", description = "OMT Middleware get order details API")
	public void getOrderDetailsAPITestsValidOrders(String orderId, String scenario, int expectedResponse) throws NumberFormatException, JSONException, InterruptedException, NullPointerException 
	{
		System.out.println("get order details super orders API tests with " + scenario + " started");
		Thread.sleep(1000);
		Processor response = OmtMiddlewareHelper.getOrderDetails(orderId);	
		Assert.assertEquals(response.ResponseValidator.GetResponseCode(), expectedResponse,"Actual respponse did not match with the expected response " + orderId);
		SoftAssert softAssertion = new SoftAssert();
		softAssertion.assertEquals(response.ResponseValidator.GetNodeValue("$.statusMessage"), "Success!","Actual Status message did not match with expected " + orderId);
		softAssertion.assertEquals(response.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.order_tags"), OmtMiddlewareHelper.getOrderTags(Long.parseLong(orderId)));
		softAssertion.assertAll();
		System.out.println(" Get order details with super orders " + scenario + " completed....");
	}

	@Test(dataProvider = "getOrderDetailsInvalidOrders", description = "OMT Middleware get  super order details API")
	public void getOrderDetailsAPITestsInValidOrders(String orderId, String scenario, HashMap<String, String> expectedResponseMap) throws NumberFormatException, JSONException {
		System.out.println("get order details Invalid orders API tests with " + scenario + " started");
		Processor response = OmtMiddlewareHelper.getOrderDetails(orderId);
		Assert.assertEquals(response.ResponseValidator.GetResponseCode(), Integer.parseInt(expectedResponseMap.get("statusCode")),"Actual response did not match with the expected response " + orderId);
		Assert.assertEquals(response.ResponseValidator.GetNodeValueAsInt("statusCode"), Integer.parseInt(expectedResponseMap.get("statusCode")),"Actual Status code in response did not match with the expected Status code " + orderId);
		Assert.assertEquals(response.ResponseValidator.GetNodeValue("statusMessage"), expectedResponseMap.get("statusMessage"),"Actual Status message in response did not match with the expected Status message " + orderId);
		System.out.println(" Get order details for invalid orders " + scenario + " completed....");
	}

	@Test(dataProvider = "getOrderDetailsForPostPutDelete", description = "OMT Middle ware get order details As POST API")
	public void getOrderDetailsAPITestAsPost(String orderId, String scenario, int expectedResponse)throws NumberFormatException, JSONException {	
		System.out.println("get order details as POST method" + scenario + " started");
		Processor response = OmtMiddlewareHelper.getOrderDetailsAsPost(orderId);
		Assert.assertEquals(response.ResponseValidator.GetResponseCode(), expectedResponse,"Actual response did not match with the expected response " + orderId);
		System.out.println(" Get order details as post" + scenario + " completed....");
	}

	@Test(dataProvider = "getOrderDetailsForPostPutDelete", description = "OMT Middleware get order details AS PUT API")
	public void getOrderDetailsAPITestAsPut(String orderId, String scenario, int expectedResponse) throws NumberFormatException, JSONException {	
		System.out.println("get order details as PUT method" + scenario + " started");
		Processor response = OmtMiddlewareHelper.getOrderDetailsAsPut(orderId);
		Assert.assertEquals(response.ResponseValidator.GetResponseCode(), expectedResponse, "Actual response did not match with the expected response " + orderId);
		System.out.println(" Get order details as put" + scenario + " completed....");
	}

	@Test(dataProvider = "getOrderDetailsForPostPutDelete", description = "OMT Middle ware get order details AS DELETE API")
	public void getOrderDetailsAPITestAsDelete(String orderId, String scenario, int expectedResponse) throws NumberFormatException, JSONException {	
		System.out.println("get order details as Delete method " + scenario + " started");
		Processor response = OmtMiddlewareHelper.getOrderDetailsAsDelete(orderId);
		Assert.assertEquals(response.ResponseValidator.GetResponseCode(), expectedResponse, "Actual response did not match with the expected response " + orderId);
		System.out.println(" Get order details as delete " + scenario + " completed....");
	}
	
	@Test(dataProvider = "getOrderDetailsNonSuperOrder", description = "OMT Middle ware get order details non Super  API")
	public void getOrderDetailsNonSuperOrder(String orderId, String scenario, int expectedResponse) throws NumberFormatException, JSONException {	
		System.out.println("get order details for non super order " + scenario + " started");
		Processor response = OmtMiddlewareHelper.getOrderDetails(orderId);		
		SoftAssert softAssertion = new SoftAssert();
		Assert.assertEquals(response.ResponseValidator.GetResponseCode(), expectedResponse, "Actual response did not match with the expected response " + orderId);
		softAssertion.assertNotEquals(response.ResponseValidator.DoesNodeExists("data.order_tags"), true);
		System.out.println(" Get order details for non super order " + scenario + " completed....");
	}
}
