package com.swiggy.api.erp.delivery.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import com.swiggy.api.erp.delivery.helper.*;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.RedisHelper;
import org.apache.noggit.JSONUtil;
import org.apache.solr.common.SolrDocumentList;
import org.testng.annotations.Test;

import javax.validation.constraints.Null;
import java.util.Map;

/**
 * 	Created by somnath.nayak on 20/06/18.
 */

public class Jarvis {
    JarvisHelper jarvisHelper = new JarvisHelper();
    CMSHelper cmsHelper =new CMSHelper();
    String RecheckServiceability;
    DeliveryHelperMethods deliveryHelperMethods = new DeliveryHelperMethods();
    DeliveryDataHelper deliveryDataHelper =  new DeliveryDataHelper();
    Serviceability_Helper serviceability_helper = new Serviceability_Helper();
    RedisHelper redis = new RedisHelper();

    @Test (description = "Make Restaurant Serviceable")
    public void serviceablityCheck () throws Exception{
        //System.setProperty("rest_id_for_serviceability","361");
        String restaurant_id = System.getenv("rest_id_for_serviceability");
        //String[] RestLatLng = cmsHelper.getLatLngByRestID(restaurant_id).split(",");
        String[] RestLatLng = getRestaurantLatlng(restaurant_id).split(",");
        String[] ApproxCustomerLatLng = deliveryDataHelper.getApproxLatLongAtADistance(Double.parseDouble(RestLatLng[0]), Double.parseDouble(RestLatLng[1]), 1.0 ).split(",");
        Processor processor = jarvisHelper.cerebro_listing(ApproxCustomerLatLng[0], ApproxCustomerLatLng[1], restaurant_id);
        String response = processor.ResponseValidator.GetBodyAsText();
        String serviceability = JsonPath.read(response,"$..listingServiceabilityResponse.serviceability").toString();
        System.out.println("Serviceability is"+ serviceability);
        if(!serviceability.equals("[2]")){
            String non_serviceable_reason = JsonPath.read(response, "$..listingServiceabilityResponse.non_serviceable_reason").toString();
            System.out.println("Non-Serviceable Reason is"+ non_serviceable_reason);
            int iteration=1;
            do{
                String area_id =null;
                String zone_id =null;
                String city_id =null;

                Map<String,Object> restMap= SystemConfigProvider.getTemplate("deliverydb").queryForMap("select a.id as area_id,a.city_id,a.zone_id from restaurant r inner join area a on a.id=r.area_code where r.id="+restaurant_id);
                if (restMap!=null){
                    area_id = restMap.get("area_id").toString();
                    zone_id = restMap.get("zone_id").toString();
                    city_id = restMap.get("city_id").toString();
                }
                else
                {
                    System.out.println("********** Restaurant Not Found **********");
                    return;
                }
                switch (non_serviceable_reason) {
                    case "[0]":
                        System.out.println("Restaurant is non-serviceable because of LAST_MILE");
                        SystemConfigProvider.getTemplate("deliverydb").update("UPDATE delivery.area SET last_mile_cap = 10.00 WHERE id ="+area_id);
                        serviceability_helper.clearCache("area",area_id);
                        SystemConfigProvider.getTemplate("deliverydb").update("UPDATE delivery.zone SET temp_lm_enabled=0,rain_mode_type=3,rain_mode=0 WHERE id="+zone_id);
                        serviceability_helper.clearCache("zone",zone_id);
                        SystemConfigProvider.getTemplate("deliverydb").update("UPDATE delivery.restaurant SET max_second_mile=10.00 WHERE id="+restaurant_id);
                        serviceability_helper.clearCache("restaurant",restaurant_id);
                        Thread.sleep(5000);
                        break;

                    case "[1]":
                        System.out.println("Restaurant is non-serviceable because of MAX_SLA");
                        SystemConfigProvider.getTemplate("deliverydb").execute("UPDATE delivery.city SET max_delivery_time=85 WHERE id ="+city_id);
                        serviceability_helper.clearCache("city",city_id);
                        SystemConfigProvider.getTemplate("deliverydb").execute("UPDATE delivery.zone SET max_delivery_time=100,rain_mode=0,rain_mode_type=3 WHERE id ="+zone_id);
                        serviceability_helper.clearCache("zone",zone_id);
                        Thread.sleep(5000);
                        break;

                    case "[2]":
                        System.out.println("Restaurant is non-serviceable because of ITEMS_EXCEED");
                        System.out.println("*************** listing_item_count limit is exceeded ***************");
                        break;

                    case "[3]":
                        System.out.println("Restaurant is non-serviceable because of RAIN");
                        SystemConfigProvider.getTemplate("deliverydb").execute("UPDATE delivery.zone SET rain_mode_type=3,rain_mode=0 WHERE id="+zone_id);
                        serviceability_helper.clearCache("zone",zone_id);
                        Thread.sleep(5000);
                        break;

                    case "[4]":
                        System.out.println("Restaurant is non-serviceable because of TEMP_LAST_MILE");
                        SystemConfigProvider.getTemplate("deliverydb").execute("UPDATE delivery.zone SET temp_lm_enabled=0 WHERE id="+zone_id);
                        serviceability_helper.clearCache("zone",zone_id);
                        Thread.sleep(5000);
                        break;

                    case "[5]":
                        System.out.println("Restaurant is non-serviceable because of ZONE_NOT_OPEN");
                        SystemConfigProvider.getTemplate("deliverydb").execute("UPDATE delivery.zone SET open_time=0,close_time=2359 WHERE id="+zone_id);
                        serviceability_helper.clearCache("zone",zone_id);
                        Thread.sleep(5000);
                        break;

                    case "[6]":
                        System.out.println("Restaurant is non-serviceable because of RESTAURANT_NOT_OPEN");
                        SystemConfigProvider.getTemplate("deliverydb").execute("UPDATE delivery.restaurant SET enabled=1,serviceable=1 WHERE id="+restaurant_id);
                        serviceability_helper.clearCache("restaurant",restaurant_id);
                        Thread.sleep(7000);
                        break;

                    case "[7]":
                        System.out.println("Restaurant is non-serviceable because of BANNER_FACTOR");
                        redis.setValue("deliveryredis",1,"BL_BANNER_FACTOR_ZONE_"+zone_id,"0.8");
                        SystemConfigProvider.getTemplate("deliverydb").execute("UPDATE delivery.zone SET is_open=1 WHERE id="+zone_id);
                        serviceability_helper.clearCache("zone",zone_id);
                        Thread.sleep(5000);
                        break;

                    case "[8]":
                        System.out.println("Restaurant is non-serviceable because of NON_BATCHABLE_ACTIVE_ORDER");
                        System.out.println("*************** There is no implementation against this reason ***************");
                        break;

                    case "[9]":
                        System.out.println("Restaurant is non-serviceable because of BLACK_ZONE");
                        System.out.println("*************** Restaurants are showing non-serviceable because customer lies under Black Zone ***************");
                        break;

                    case "[10]":
                        System.out.println("Restaurant is non-serviceable because of TIMEOUT");
                        System.out.println("*************** Restaurants are showing non-serviceable because its getting timeout just increase hystrix timeout ***************");
                        break;

                    case "[11]":
                        System.out.println("Restaurant is non-serviceable because of CAPACITY_MAX_OUT");
                        SystemConfigProvider.getTemplate("deliverydb").execute("UPDATE delivery.zone SET preorder_capacity_per_slot=100 WHERE id="+zone_id);
                        serviceability_helper.clearCache("zone",zone_id);
                        Thread.sleep(5000);
                        break;

                    default:
                        System.out.println("Non-Serviceable Reason is Unknown");
                }
                Processor new_processor = jarvisHelper.cerebro_listing(ApproxCustomerLatLng[0], ApproxCustomerLatLng[1], restaurant_id);
                String new_response = new_processor.ResponseValidator.GetBodyAsText();
                RecheckServiceability = JsonPath.read(new_response,"$..listingServiceabilityResponse.serviceability").toString();
                iteration++;

            } while (!RecheckServiceability.equals("[2]") && iteration<=7);
            if (RecheckServiceability.equals("[2]")){
                System.out.println("********** Restaurant "+restaurant_id+" is serviceable now **********");
            }else {
                System.out.println("********** Sorry !!! Jarvis is not able to make your restaurant serviceable **********");
            }
        }else{System.out.println("*************** Restaurant is already Serviceable ***************");}


    }

    public String getRestaurantLatlng(String restaurant_id){
        String solrUrl = deliveryHelperMethods.getSolrUrl();
        SolrDocumentList solrDocuments = deliveryDataHelper.getSolrOutput(solrUrl, "core_listing", "id:"+ restaurant_id +"");
        String jsonDoc = JSONUtil.toJSON(solrDocuments);
        System.out.println(jsonDoc);
        //String[] restLatLng = JsonPath.read(jsonDoc, "$[*].place").toString().replace("[\"", "").replace("\"]", "").split(",");
        String restLatLng = JsonPath.read(jsonDoc, "$[*].place").toString().replace("[\"", "").replace("\"]", "");
        return restLatLng;
    }

}