package com.swiggy.api.erp.ff.tests;


import com.swiggy.api.erp.ff.dp.WebSocketDP;
import com.swiggy.api.erp.ff.helper.WebSocketHelper;
import org.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.Test;

public class WebSocketTests {
    WebSocketHelper webSocketHelper = new WebSocketHelper();

    @Test(groups = {"smoke", "sanity", "regression"})
    public void assignSingleOrderToOneOE() throws JSONException, InterruptedException {
        String assign = "{\"update_type\":\"assignment\",\"oe_ids\":[\"234\"],\"message\":{},\"sent_timestamp_gmt\":1509093760,\"order_id\":\"7685486\",\"order_pk\":\"45653\"}";
        String webSocketMsg = webSocketHelper.sendMessageToRabbitForWebSocket(assign);
        Assert.assertEquals(webSocketMsg, assign);
    }

    @Test(dataProvider = "assignmentNegative", dataProviderClass = WebSocketDP.class, groups = {"regression"})
    public void assignSingleOrderToOneOE(String updateType, String oeIds, String orderId, String message,
                                         String orderIdPk) throws JSONException, InterruptedException {
        String assign = "{\"update_type\":\""+ updateType +"\",\"oe_ids\":[\""+oeIds+"\"],\"message\":" +
                "{"+message+"},\"sent_timestamp_gmt\":1509093760,\"order_id\":\""+orderId+"\",\"order_pk\":\""+orderIdPk+"\"}";
        String webSocketMsg = webSocketHelper.sendMessageToRabbitForWebSocket(assign);
        Assert.assertEquals(webSocketMsg, "connected");
    }

    @Test(groups = {"smoke", "sanity", "regression"})
    public void unAssign() throws InterruptedException {
        String assign = "{\"update_type\":\"unassignment\",\"oe_ids\":[\"234\"],\"message\":{},\"sent_timestamp_gmt\":1509093760,\"order_id\":\"7685486\",\"order_pk\":\"45653\"}";
        String webSocketMsg = webSocketHelper.sendMessageToRabbitForWebSocket(assign);
        Assert.assertEquals(webSocketMsg, assign);
    }


    public void orderUpdate(){
        //createOrder
        //AssignOrder To OE
        //Send Order Updates
        //Check Order Updates are propagated to OE
    }


    public void  updateStatus(){
        //createOrder
        //AssignOrder To OE
        //Send Order Updates
        //Check Order Updates are propagated to OE
    }



}
