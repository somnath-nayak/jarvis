package com.swiggy.api.erp.crm.dp.cancellationflows;

import com.mysql.jdbc.ResultSetMetaData;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CreateData {
	public static int payloadSize;

	@Test
	public List resultSetToArrayList(ResultSet rs) throws SQLException {
		ResultSetMetaData md = (ResultSetMetaData) rs.getMetaData();
		int columns = md.getColumnCount();
		ArrayList list = new ArrayList(300);
		while (rs.next()) {
			HashMap row = new HashMap(columns);
			for (int i = 1; i <= columns; ++i) {
				row.put(md.getColumnName(i), rs.getObject(i));
			}
			list.add(row);
		}

		return list;
	}

	@DataProvider(name = "canceltest")
	public static Object[][] cancellation_inbound() {

		String[][] payloadparams = new String[2][1];
		payloadparams[0][0] = "1234"; // unDelivered order
		// payloadparams[1][0]="9876"; // Delivered order

		payloadSize = payloadparams.length;
		System.out.println("Size of payloadpareas : " + payloadSize);

		return payloadparams;
	}

	@DataProvider(name = "user credentials")
	public static Object[][] userCredentials2() {
		HashMap<Integer, String[]> data = new HashMap<Integer, String[]>();
		// HashMap<String,String[]> generalNode= new HashMap<String,
		// String[]>();

		data.put(1, new String[] { "393325", "satyam", "123" });// userId,username,orderId
		data.put(2, new String[] { "40", "aaa", "222" });

		// generalNode.put("general1", new String[] {"206","I have queries
		// related to placing an order on Swiggy"});
		// generalNode.put("general2", new String[] {"212","I have issues with
		// logging in on Swiggy"});

		return new Object[][] { { data } };
	}

	@DataProvider(name = "user data")
	public static Object[][] userData() {
		HashMap<String, String[]> userdata = new HashMap<String, String[]>();
		HashMap<String, String[]> generalNode = new HashMap<String, String[]>();
		HashMap<String, String[]> legalNode = new HashMap<String, String[]>();
		HashMap<String, String[]> OderSpecificNode = new HashMap<String, String[]>();

		HashMap<Integer, HashMap<String, String[]>> nodes = new HashMap<Integer, HashMap<String, String[]>>();

		userdata.put("user1", new String[] { "38", "aarif", "123" });// userId,
																		// username,
																		// orderId
		userdata.put("user2", new String[] { "40", "aaa", "222" });

		generalNode.put("general1", new String[] { "206", "I have queries related to placing an order on Swiggy" });
		generalNode.put("general2", new String[] { "212", "I have issues with logging in on Swiggy" });

		legalNode.put("legal1", new String[] { "", "" });

		OderSpecificNode.put("orderspecific1", new String[] { "240", "Where is my order?" });
		 OderSpecificNode.put("orderspecific2", new String[] { "5", "I want to cancel my order" });
		OderSpecificNode.put("orderspecific3", new String[] { "234", "I want to modify items in this order" });
		OderSpecificNode.put("orderspecific4", new String[] { "233", "I want to add special instructions to this order" });
		OderSpecificNode.put("orderspecific5", new String[] { "247", "I have payment & refund related queries for this order"});
		OderSpecificNode.put("orderspecific6", new String[] { "253", "I have coupon related queries" });

		nodes.put(1, generalNode);
		nodes.put(2, legalNode);
		nodes.put(3, OderSpecificNode);

		return new Object[][] { { userdata, nodes } };
	}

	@DataProvider(name = "user info")
	public static Object[][] userInfo() {
		HashMap<Integer, String[]> data = new HashMap<Integer, String[]>();

		data.put(1, new String[] { "38", "aarif", "222", "5", "Order specific issues" }); // userId,
																							// username,
																							// orderid,issueId,
																							// issueType
		data.put(2, new String[] { "40", "aaa", "222", "5", "Order specific issues" });

		return new Object[][] { { data, } };
	}

	@DataProvider(name = "node")
	public static Object[][] node() {
		HashMap<String, String[]> data = new HashMap<String, String[]>();

		// ack_text, action_template, data_template, description, is_leaf,
		// prompt_text
		data.put("233",
				new String[] { "NULL", "NULL", "NULL",
						"Have something to add for the delivery executive?You can reach out to us and we'll convey the same to the delivery executive",
						"NULL", });

		return new Object[][] { { data, } };

	}

	// Unplaced, Unassigned
	// Unplaced, Assigned
	// Unplaced, Confirmed
	// Unplaced, Arrived
	// Unplaced, PickedUp
	// Unplaced, Reached

	@DataProvider(name = "unplacedunassigned")
	public static Object[][] flowMapperUnplacedUnassigned() {
		// FF Status, Payment Method, Delivery Status
		return new Object[][] { 
			    { "flow1", new String[] { "Unplaced", "Unassigned" } },
				{ "flow2", new String[] { "Unplaced", "Unassigned" } },
				{ "flow3", new String[] { "Unplaced", "Unassigned" } },
				{ "flow4", new String[] { "Unplaced", "Unassigned" } },
				{ "flow5", new String[] { "Unplaced", "Unassigned" } },
				{ "flow6", new String[] { "Unplaced", "Unassigned" } },
				{ "flow7", new String[] { "Unplaced", "Unassigned" } },
				{ "flow8", new String[] { "Unplaced", "Unassigned" } },
				{ "flow9", new String[] { "Unplaced", "Unassigned" } },
				{ "flow10", new String[] { "Unplaced", "Unassigned" } },
				{ "flow11", new String[] { "Unplaced", "Unassigned" } },
				{ "flow12", new String[] { "Unplaced", "Unassigned" } },
				{ "flow13", new String[] { "Unplaced", "Unassigned" } },
				{ "flow14", new String[] { "Unplaced", "Unassigned" } },
				{ "flow15", new String[] { "Unplaced", "Unassigned" } },
				{ "flow16", new String[] { "Unplaced", "Unassigned" } },
				{ "flow17", new String[] { "Unplaced", "Unassigned" } },
				{ "flow18", new String[] { "Unplaced", "Unassigned" } },
				{ "flow19", new String[] { "Unplaced", "Unassigned" } },
				{ "flow20", new String[] { "Unplaced", "Unassigned" } },
				{ "flow21", new String[] { "Unplaced", "Unassigned" } },
				{ "flow22", new String[] { "Unplaced", "Unassigned" } },
				{ "flow23", new String[] { "Unplaced", "Unassigned" } },
				{ "flow24", new String[] { "Unplaced", "Unassigned" } },
				{ "flow25", new String[] { "Unplaced", "Unassigned" } },
				{ "flow26", new String[] { "Unplaced", "Unassigned" } },
				{ "flow27", new String[] { "Unplaced", "Unassigned" } },
				{ "flow28", new String[] { "Unplaced", "Unassigned" } },
				{ "flow29", new String[] { "Unplaced", "Unassigned" } },
				{ "flow30", new String[] { "Unplaced", "Unassigned" } },
				{ "flow31", new String[] { "Unplaced", "Unassigned" } },
				{ "flow32", new String[] { "Unplaced", "Unassigned" } },
				{ "flow33", new String[] { "Unplaced", "Unassigned" } },
				{ "flow34", new String[] { "Unplaced", "Unassigned" } },
				{ "flow35", new String[] { "Unplaced", "Unassigned" } },
				{ "flow36", new String[] { "Unplaced", "Unassigned" } },
				{ "flow37", new String[] { "Unplaced", "Unassigned" } },
				{ "flow38", new String[] { "Unplaced", "Unassigned" } },
				{ "flow39", new String[] { "Unplaced", "Unassigned" } },
				{ "flow40", new String[] { "Unplaced", "Unassigned" } },
				{ "flow41", new String[] { "Unplaced", "Unassigned" } },
				{ "flow42", new String[] { "Unplaced", "Unassigned" } },
				{ "flow43", new String[] { "Unplaced", "Unassigned" } },
				{ "flow44", new String[] { "Unplaced", "Unassigned" } },
				{ "flow45", new String[] { "Unplaced", "Unassigned" } },
				{ "flow46", new String[] { "Unplaced", "Unassigned" } },
				{ "flow47", new String[] { "Unplaced", "Unassigned" } },
				{ "flow48", new String[] { "Unplaced", "Unassigned" } },
				{ "flow49", new String[] { "Unplaced", "Unassigned" } },
				{ "flow50", new String[] { "Unplaced", "Unassigned" } },
				{ "flow51", new String[] { "Unplaced", "Unassigned" } },
				{ "flow52", new String[] { "Unplaced", "Unassigned" } },
				{ "flow53", new String[] { "Unplaced", "Unassigned" } },
				{ "flow54", new String[] { "Unplaced", "Unassigned" } },
				{ "flow55", new String[] { "Unplaced", "Unassigned" } },
				{ "flow56", new String[] { "Unplaced", "Unassigned" } },
				{ "flow57", new String[] { "Unplaced", "Unassigned" } },
				{ "flow58", new String[] { "Unplaced", "Unassigned" } },
				{ "flow59", new String[] { "Unplaced", "Unassigned" } },
				{ "flow60", new String[] { "Unplaced", "Unassigned" } },
				{ "flow61", new String[] { "Unplaced", "Unassigned" } },
				{ "flow62", new String[] { "Unplaced", "Unassigned" } },
				{ "flow63", new String[] { "Unplaced", "Unassigned" } },
				{ "flow64", new String[] { "Unplaced", "Unassigned" } },
				{ "flow65", new String[] { "Unplaced", "Unassigned" } },
				{ "flow66", new String[] { "Unplaced", "Unassigned" } },
				{ "flow67", new String[] { "Unplaced", "Unassigned" } },
				{ "flow68", new String[] { "Unplaced", "Unassigned" } },
				{ "flow69", new String[] { "Unplaced", "Unassigned" } },
				{ "flow70", new String[] { "Unplaced", "Unassigned" } },
				{ "flow71", new String[] { "Unplaced", "Unassigned" } },
				{ "flow72", new String[] { "Unplaced", "Unassigned" } },
				{ "flow73", new String[] { "Unplaced", "Unassigned" } },
				{ "flow74", new String[] { "Unplaced", "Unassigned" } },
				{ "flow75", new String[] { "Unplaced", "Unassigned" } },
				{ "flow76", new String[] { "Unplaced", "Unassigned" } },
				{ "flow77", new String[] { "Unplaced", "Unassigned" } },
				{ "flow78", new String[] { "Unplaced", "Unassigned" } },
				{ "flow79", new String[] { "Unplaced", "Unassigned" } },
				{ "flow80", new String[] { "Unplaced", "Unassigned" } },
				{ "flow81", new String[] { "Unplaced", "Unassigned" } },
				{ "flow82", new String[] { "Unplaced", "Unassigned" } },
				{ "flow83", new String[] { "Unplaced", "Unassigned" } },
				{ "flow84", new String[] { "Unplaced", "Unassigned" } } };
	}
	
	@DataProvider(name = "arrived")
	public static Object[][] flowMapperArrived() {
		// FF Status, Payment Method, Delivery Status
		return new Object[][] { 
			{ "flow1", new String[] { "Placed", "Arrived" } },
			{ "flow2", new String[] { "Placed", "Arrived" } },
			{ "flow3", new String[] { "Placed", "Arrived" } },
			{ "flow4", new String[] { "Placed", "Arrived" } },
			{ "flow5", new String[] { "Placed", "Arrived" } },
			{ "flow6", new String[] { "Placed", "Arrived" } },
			{ "flow7", new String[] { "Placed", "Arrived" } },
			{ "flow8", new String[] { "Placed", "Arrived" } },
			{ "flow9", new String[] { "Placed", "Arrived" } },
			{ "flow10", new String[] { "Placed", "Arrived" } },
			{ "flow11", new String[] { "Placed", "Arrived" } },
			{ "flow12", new String[] { "Placed", "Arrived" } },
			{ "flow13", new String[] { "Placed", "Arrived" } },
			{ "flow14", new String[] { "Placed", "Arrived" } },
			{ "flow15", new String[] { "Placed", "Arrived" } },
			{ "flow16", new String[] { "Placed", "Arrived" } },
			{ "flow17", new String[] { "Placed", "Arrived" } },
			{ "flow18", new String[] { "Placed", "Arrived" } },
			{ "flow19", new String[] { "Placed", "Arrived" } },
			{ "flow20", new String[] { "Placed", "Arrived" } },
			{ "flow21", new String[] { "Placed", "Arrived" } },
			{ "flow22", new String[] { "Placed", "Arrived" } },
			{ "flow23", new String[] { "Placed", "Arrived" } },
			{ "flow24", new String[] { "Placed", "Arrived" } },
			{ "flow25", new String[] { "Placed", "Arrived" } },
			{ "flow26", new String[] { "Placed", "Arrived" } },
			{ "flow27", new String[] { "Placed", "Arrived" } },
			{ "flow28", new String[] { "Placed", "Arrived" } },
			{ "flow29", new String[] { "Placed", "Arrived" } },
			{ "flow30", new String[] { "Placed", "Arrived" } },
			{ "flow31", new String[] { "Placed", "Arrived" } },
			{ "flow32", new String[] { "Placed", "Arrived" } },
			{ "flow33", new String[] { "Placed", "Arrived" } },
			{ "flow34", new String[] { "Placed", "Arrived" } },
			{ "flow35", new String[] { "Placed", "Arrived" } },
			{ "flow36", new String[] { "Placed", "Arrived" } },
			{ "flow37", new String[] { "Placed", "Arrived" } },
			{ "flow38", new String[] { "Placed", "Arrived" } },
			{ "flow39", new String[] { "Placed", "Arrived" } },
			{ "flow40", new String[] { "Placed", "Arrived" } },
			{ "flow41", new String[] { "Placed", "Arrived" } },
			{ "flow42", new String[] { "Placed", "Arrived" } },
			{ "flow43", new String[] { "Placed", "Arrived" } },
			{ "flow44", new String[] { "Placed", "Arrived" } },
			{ "flow45", new String[] { "Placed", "Arrived" } },
			{ "flow46", new String[] { "Placed", "Arrived" } },
			{ "flow47", new String[] { "Placed", "Arrived" } },
			{ "flow48", new String[] { "Placed", "Arrived" } },
			{ "flow49", new String[] { "Placed", "Arrived" } },
			{ "flow50", new String[] { "Placed", "Arrived" } },
			{ "flow51", new String[] { "Placed", "Arrived" } },
			{ "flow52", new String[] { "Placed", "Arrived" } },
			{ "flow53", new String[] { "Placed", "Arrived" } },
			{ "flow54", new String[] { "Placed", "Arrived" } },
			{ "flow55", new String[] { "Placed", "Arrived" } },
			{ "flow56", new String[] { "Placed", "Arrived" } },
			{ "flow57", new String[] { "Placed", "Arrived" } },
			{ "flow58", new String[] { "Placed", "Arrived" } },
			{ "flow59", new String[] { "Placed", "Arrived" } },
			{ "flow60", new String[] { "Placed", "Arrived" } },
			{ "flow61", new String[] { "Placed", "Arrived" } },
			{ "flow62", new String[] { "Placed", "Arrived" } },
			{ "flow63", new String[] { "Placed", "Arrived" } },
			{ "flow64", new String[] { "Placed", "Arrived" } },
			{ "flow65", new String[] { "Placed", "Arrived" } },
			{ "flow66", new String[] { "Placed", "Arrived" } },
			{ "flow67", new String[] { "Placed", "Arrived" } },
			{ "flow68", new String[] { "Placed", "Arrived" } },
			{ "flow69", new String[] { "Placed", "Arrived" } },
			{ "flow70", new String[] { "Placed", "Arrived" } },
			{ "flow71", new String[] { "Placed", "Arrived" } },
			{ "flow72", new String[] { "Placed", "Arrived" } },
			{ "flow73", new String[] { "Placed", "Arrived" } },
			{ "flow74", new String[] { "Placed", "Arrived" } },
			{ "flow75", new String[] { "Placed", "Arrived" } },
			{ "flow76", new String[] { "Placed", "Arrived" } },
			{ "flow77", new String[] { "Placed", "Arrived" } },
			{ "flow78", new String[] { "Placed", "Arrived" } },
			{ "flow79", new String[] { "Placed", "Arrived" } },
			{ "flow80", new String[] { "Placed", "Arrived" } },
			{ "flow81", new String[] { "Placed", "Arrived" } },
			{ "flow82", new String[] { "Placed", "Arrived" } },
			{ "flow83", new String[] { "Placed", "Arrived" } },
			{ "flow84", new String[] { "Placed", "Arrived" } }};
}

	@DataProvider(name = "assigned")
	public static Object[][] flowMapperAssigned() {
		// FF Status, Payment Method, Delivery Status
		return new Object[][] { 
			{ "flow1", new String[] { "Placed", "Assigned" } },
			{ "flow2", new String[] { "Placed", "Assigned" } },
			{ "flow3", new String[] { "Placed", "Assigned" } },
			{ "flow4", new String[] { "Placed", "Assigned" } },
			{ "flow5", new String[] { "Placed", "Assigned" } },
			{ "flow6", new String[] { "Placed", "Assigned" } },
			{ "flow7", new String[] { "Placed", "Assigned" } },
			{ "flow8", new String[] { "Placed", "Assigned" } },
			{ "flow9", new String[] { "Placed", "Assigned" } },
			{ "flow10", new String[] { "Placed", "Assigned" } },
			{ "flow11", new String[] { "Placed", "Assigned" } },
			{ "flow12", new String[] { "Placed", "Assigned" } },
			{ "flow13", new String[] { "Placed", "Assigned" } },
			{ "flow14", new String[] { "Placed", "Assigned" } },
			{ "flow15", new String[] { "Placed", "Assigned" } },
			{ "flow16", new String[] { "Placed", "Assigned" } },
			{ "flow17", new String[] { "Placed", "Assigned" } },
			{ "flow18", new String[] { "Placed", "Assigned" } },
			{ "flow19", new String[] { "Placed", "Assigned" } },
			{ "flow20", new String[] { "Placed", "Assigned" } },
			{ "flow21", new String[] { "Placed", "Assigned" } },
			{ "flow22", new String[] { "Placed", "Assigned" } },
			{ "flow23", new String[] { "Placed", "Assigned" } },
			{ "flow24", new String[] { "Placed", "Assigned" } },
			{ "flow25", new String[] { "Placed", "Assigned" } },
			{ "flow26", new String[] { "Placed", "Assigned" } },
			{ "flow27", new String[] { "Placed", "Assigned" } },
			{ "flow28", new String[] { "Placed", "Assigned" } },
			{ "flow29", new String[] { "Placed", "Assigned" } },
			{ "flow30", new String[] { "Placed", "Assigned" } },
			{ "flow31", new String[] { "Placed", "Assigned" } },
			{ "flow32", new String[] { "Placed", "Assigned" } },
			{ "flow33", new String[] { "Placed", "Assigned" } },
			{ "flow34", new String[] { "Placed", "Assigned" } },
			{ "flow35", new String[] { "Placed", "Assigned" } },
			{ "flow36", new String[] { "Placed", "Assigned" } },
			{ "flow37", new String[] { "Placed", "Assigned" } },
			{ "flow38", new String[] { "Placed", "Assigned" } },
			{ "flow39", new String[] { "Placed", "Assigned" } },
			{ "flow40", new String[] { "Placed", "Assigned" } },
			{ "flow41", new String[] { "Placed", "Assigned" } },
			{ "flow42", new String[] { "Placed", "Assigned" } },
			{ "flow43", new String[] { "Placed", "Assigned" } },
			{ "flow44", new String[] { "Placed", "Assigned" } },
			{ "flow45", new String[] { "Placed", "Assigned" } },
			{ "flow46", new String[] { "Placed", "Assigned" } },
			{ "flow47", new String[] { "Placed", "Assigned" } },
			{ "flow48", new String[] { "Placed", "Assigned" } },
			{ "flow49", new String[] { "Placed", "Assigned" } },
			{ "flow50", new String[] { "Placed", "Assigned" } },
			{ "flow51", new String[] { "Placed", "Assigned" } },
			{ "flow52", new String[] { "Placed", "Assigned" } },
			{ "flow53", new String[] { "Placed", "Assigned" } },
			{ "flow54", new String[] { "Placed", "Assigned" } },
			{ "flow55", new String[] { "Placed", "Assigned" } },
			{ "flow56", new String[] { "Placed", "Assigned" } },
			{ "flow57", new String[] { "Placed", "Assigned" } },
			{ "flow58", new String[] { "Placed", "Assigned" } },
			{ "flow59", new String[] { "Placed", "Assigned" } },
			{ "flow60", new String[] { "Placed", "Assigned" } },
			{ "flow61", new String[] { "Placed", "Assigned" } },
			{ "flow62", new String[] { "Placed", "Assigned" } },
			{ "flow63", new String[] { "Placed", "Assigned" } },
			{ "flow64", new String[] { "Placed", "Assigned" } },
			{ "flow65", new String[] { "Placed", "Assigned" } },
			{ "flow66", new String[] { "Placed", "Assigned" } },
			{ "flow67", new String[] { "Placed", "Assigned" } },
			{ "flow68", new String[] { "Placed", "Assigned" } },
			{ "flow69", new String[] { "Placed", "Assigned" } },
			{ "flow70", new String[] { "Placed", "Assigned" } },
			{ "flow71", new String[] { "Placed", "Assigned" } },
			{ "flow72", new String[] { "Placed", "Assigned" } },
			{ "flow73", new String[] { "Placed", "Assigned" } },
			{ "flow74", new String[] { "Placed", "Assigned" } },
			{ "flow75", new String[] { "Placed", "Assigned" } },
			{ "flow76", new String[] { "Placed", "Assigned" } },
			{ "flow77", new String[] { "Placed", "Assigned" } },
			{ "flow78", new String[] { "Placed", "Assigned" } },
			{ "flow79", new String[] { "Placed", "Assigned" } },
			{ "flow80", new String[] { "Placed", "Assigned" } },
			{ "flow81", new String[] { "Placed", "Assigned" } },
			{ "flow82", new String[] { "Placed", "Assigned" } },
			{ "flow83", new String[] { "Placed", "Assigned" } },
			{ "flow84", new String[] { "Placed", "Assigned" } } };
}
	@DataProvider(name = "confirmed")
	public static Object[][] flowMapperConfirmed() {
		// FF Status, Payment Method, Delivery Status
		return new Object[][] { 
			{ "flow1", new String[] { "Placed", "Confirmed" } },
			{ "flow2", new String[] { "Placed", "Confirmed" } },
			{ "flow3", new String[] { "Placed", "Confirmed" } },
			{ "flow4", new String[] { "Placed", "Confirmed" } },
			{ "flow5", new String[] { "Placed", "Confirmed" } },
			{ "flow6", new String[] { "Placed", "Confirmed" } },
			{ "flow7", new String[] { "Placed", "Confirmed" } },
			{ "flow8", new String[] { "Placed", "Confirmed" } },
			{ "flow9", new String[] { "Placed", "Confirmed" } },
			{ "flow10", new String[] { "Placed", "Confirmed" } },
			{ "flow11", new String[] { "Placed", "Confirmed" } },
			{ "flow12", new String[] { "Placed", "Confirmed" } },
			{ "flow13", new String[] { "Placed", "Confirmed" } },
			{ "flow14", new String[] { "Placed", "Confirmed" } },
			{ "flow15", new String[] { "Placed", "Confirmed" } },
			{ "flow16", new String[] { "Placed", "Confirmed" } },
			{ "flow17", new String[] { "Placed", "Confirmed" } },
			{ "flow18", new String[] { "Placed", "Confirmed" } },
			{ "flow19", new String[] { "Placed", "Confirmed" } },
			{ "flow20", new String[] { "Placed", "Confirmed" } },
			{ "flow21", new String[] { "Placed", "Confirmed" } },
			{ "flow22", new String[] { "Placed", "Confirmed" } },
			{ "flow23", new String[] { "Placed", "Confirmed" } },
			{ "flow24", new String[] { "Placed", "Confirmed" } },
			{ "flow25", new String[] { "Placed", "Confirmed" } },
			{ "flow26", new String[] { "Placed", "Confirmed" } },
			{ "flow27", new String[] { "Placed", "Confirmed" } },
			{ "flow28", new String[] { "Placed", "Confirmed" } },
			{ "flow29", new String[] { "Placed", "Confirmed" } },
			{ "flow30", new String[] { "Placed", "Confirmed" } },
			{ "flow31", new String[] { "Placed", "Confirmed" } },
			{ "flow32", new String[] { "Placed", "Confirmed" } },
			{ "flow33", new String[] { "Placed", "Confirmed" } },
			{ "flow34", new String[] { "Placed", "Confirmed" } },
			{ "flow35", new String[] { "Placed", "Confirmed" } },
			{ "flow36", new String[] { "Placed", "Confirmed" } },
			{ "flow37", new String[] { "Placed", "Confirmed" } },
			{ "flow38", new String[] { "Placed", "Confirmed" } },
			{ "flow39", new String[] { "Placed", "Confirmed" } },
			{ "flow40", new String[] { "Placed", "Confirmed" } },
			{ "flow41", new String[] { "Placed", "Confirmed" } },
			{ "flow42", new String[] { "Placed", "Confirmed" } },
			{ "flow43", new String[] { "Placed", "Confirmed" } },
			{ "flow44", new String[] { "Placed", "Confirmed" } },
			{ "flow45", new String[] { "Placed", "Confirmed" } },
			{ "flow46", new String[] { "Placed", "Confirmed" } },
			{ "flow47", new String[] { "Placed", "Confirmed" } },
			{ "flow48", new String[] { "Placed", "Confirmed" } },
			{ "flow49", new String[] { "Placed", "Confirmed" } },
			{ "flow50", new String[] { "Placed", "Confirmed" } },
			{ "flow51", new String[] { "Placed", "Confirmed" } },
			{ "flow52", new String[] { "Placed", "Confirmed" } },
			{ "flow53", new String[] { "Placed", "Confirmed" } },
			{ "flow54", new String[] { "Placed", "Confirmed" } },
			{ "flow55", new String[] { "Placed", "Confirmed" } },
			{ "flow56", new String[] { "Placed", "Confirmed" } },
			{ "flow57", new String[] { "Placed", "Confirmed" } },
			{ "flow58", new String[] { "Placed", "Confirmed" } },
			{ "flow59", new String[] { "Placed", "Confirmed" } },
			{ "flow60", new String[] { "Placed", "Confirmed" } },
			{ "flow61", new String[] { "Placed", "Confirmed" } },
			{ "flow62", new String[] { "Placed", "Confirmed" } },
			{ "flow63", new String[] { "Placed", "Confirmed" } },
			{ "flow64", new String[] { "Placed", "Confirmed" } },
			{ "flow65", new String[] { "Placed", "Confirmed" } },
			{ "flow66", new String[] { "Placed", "Confirmed" } },
			{ "flow67", new String[] { "Placed", "Confirmed" } },
			{ "flow68", new String[] { "Placed", "Confirmed" } },
			{ "flow69", new String[] { "Placed", "Confirmed" } },
			{ "flow70", new String[] { "Placed", "Confirmed" } },
			{ "flow71", new String[] { "Placed", "Confirmed" } },
			{ "flow72", new String[] { "Placed", "Confirmed" } },
			{ "flow73", new String[] { "Placed", "Confirmed" } },
			{ "flow74", new String[] { "Placed", "Confirmed" } },
			{ "flow75", new String[] { "Placed", "Confirmed" } },
			{ "flow76", new String[] { "Placed", "Confirmed" } },
			{ "flow77", new String[] { "Placed", "Confirmed" } },
			{ "flow78", new String[] { "Placed", "Confirmed" } },
			{ "flow79", new String[] { "Placed", "Confirmed" } },
			{ "flow80", new String[] { "Placed", "Confirmed" } },
			{ "flow81", new String[] { "Placed", "Confirmed" } },
			{ "flow82", new String[] { "Placed", "Confirmed" } },
			{ "flow83", new String[] { "Placed", "Confirmed" } },
			{ "flow84", new String[] { "Placed", "Confirmed" } } };
}
	
	@DataProvider(name = "pickedup")
	public static Object[][] flowMapperPickedup() {
		// FF Status, Payment Method, Delivery Status
		return new Object[][] { 
			{ "flow1", new String[] { "Placed", "PickedUp" } },
			{ "flow2", new String[] { "Placed", "PickedUp" } },
			{ "flow3", new String[] { "Placed", "PickedUp" } },
			{ "flow4", new String[] { "Placed", "PickedUp" } },
			{ "flow5", new String[] { "Placed", "PickedUp" } },
			{ "flow6", new String[] { "Placed", "PickedUp" } },
			{ "flow7", new String[] { "Placed", "PickedUp" } },
			{ "flow8", new String[] { "Placed", "PickedUp" } },
			{ "flow9", new String[] { "Placed", "PickedUp" } },
			{ "flow10", new String[] { "Placed", "PickedUp" } },
			{ "flow11", new String[] { "Placed", "PickedUp" } },
			{ "flow12", new String[] { "Placed", "PickedUp" } },
			{ "flow13", new String[] { "Placed", "PickedUp" } },
			{ "flow14", new String[] { "Placed", "PickedUp" } },
			{ "flow15", new String[] { "Placed", "PickedUp" } },
			{ "flow16", new String[] { "Placed", "PickedUp" } },
			{ "flow17", new String[] { "Placed", "PickedUp" } },
			{ "flow18", new String[] { "Placed", "PickedUp" } },
			{ "flow19", new String[] { "Placed", "PickedUp" } },
			{ "flow20", new String[] { "Placed", "PickedUp" } },
			{ "flow21", new String[] { "Placed", "PickedUp" } },
			{ "flow22", new String[] { "Placed", "PickedUp" } },
			{ "flow23", new String[] { "Placed", "PickedUp" } },
			{ "flow24", new String[] { "Placed", "PickedUp" } },
			{ "flow25", new String[] { "Placed", "PickedUp" } },
			{ "flow26", new String[] { "Placed", "PickedUp" } },
			{ "flow27", new String[] { "Placed", "PickedUp" } },
			{ "flow28", new String[] { "Placed", "PickedUp" } },
			{ "flow29", new String[] { "Placed", "PickedUp" } },
			{ "flow30", new String[] { "Placed", "PickedUp" } },
			{ "flow31", new String[] { "Placed", "PickedUp" } },
			{ "flow32", new String[] { "Placed", "PickedUp" } },
			{ "flow33", new String[] { "Placed", "PickedUp" } },
			{ "flow34", new String[] { "Placed", "PickedUp" } },
			{ "flow35", new String[] { "Placed", "PickedUp" } },
			{ "flow36", new String[] { "Placed", "PickedUp" } },
			{ "flow37", new String[] { "Placed", "PickedUp" } },
			{ "flow38", new String[] { "Placed", "PickedUp" } },
			{ "flow39", new String[] { "Placed", "PickedUp" } },
			{ "flow40", new String[] { "Placed", "PickedUp" } },
			{ "flow41", new String[] { "Placed", "PickedUp" } },
			{ "flow42", new String[] { "Placed", "PickedUp" } },
			{ "flow43", new String[] { "Placed", "PickedUp" } },
			{ "flow44", new String[] { "Placed", "PickedUp" } },
			{ "flow45", new String[] { "Placed", "PickedUp" } },
			{ "flow46", new String[] { "Placed", "PickedUp" } },
			{ "flow47", new String[] { "Placed", "PickedUp" } },
			{ "flow48", new String[] { "Placed", "PickedUp" } },
			{ "flow49", new String[] { "Placed", "PickedUp" } },
			{ "flow50", new String[] { "Placed", "PickedUp" } },
			{ "flow51", new String[] { "Placed", "PickedUp" } },
			{ "flow52", new String[] { "Placed", "PickedUp" } },
			{ "flow53", new String[] { "Placed", "PickedUp" } },
			{ "flow54", new String[] { "Placed", "PickedUp" } },
			{ "flow55", new String[] { "Placed", "PickedUp" } },
			{ "flow56", new String[] { "Placed", "PickedUp" } },
			{ "flow57", new String[] { "Placed", "PickedUp" } },
			{ "flow58", new String[] { "Placed", "PickedUp" } },
			{ "flow59", new String[] { "Placed", "PickedUp" } },
			{ "flow60", new String[] { "Placed", "PickedUp" } },
			{ "flow61", new String[] { "Placed", "PickedUp" } },
			{ "flow62", new String[] { "Placed", "PickedUp" } },
			{ "flow63", new String[] { "Placed", "PickedUp" } },
			{ "flow64", new String[] { "Placed", "PickedUp" } },
			{ "flow65", new String[] { "Placed", "PickedUp" } },
			{ "flow66", new String[] { "Placed", "PickedUp" } },
			{ "flow67", new String[] { "Placed", "PickedUp" } },
			{ "flow68", new String[] { "Placed", "PickedUp" } },
			{ "flow69", new String[] { "Placed", "PickedUp" } },
			{ "flow70", new String[] { "Placed", "PickedUp" } },
			{ "flow71", new String[] { "Placed", "PickedUp" } },
			{ "flow72", new String[] { "Placed", "PickedUp" } },
			{ "flow73", new String[] { "Placed", "PickedUp" } },
			{ "flow74", new String[] { "Placed", "PickedUp" } },
			{ "flow75", new String[] { "Placed", "PickedUp" } },
			{ "flow76", new String[] { "Placed", "PickedUp" } },
			{ "flow77", new String[] { "Placed", "PickedUp" } },
			{ "flow78", new String[] { "Placed", "PickedUp" } },
			{ "flow79", new String[] { "Placed", "PickedUp" } },
			{ "flow80", new String[] { "Placed", "PickedUp" } },
			{ "flow81", new String[] { "Placed", "PickedUp" } },
			{ "flow82", new String[] { "Placed", "PickedUp" } },
			{ "flow83", new String[] { "Placed", "PickedUp" } },
			{ "flow84", new String[] { "Placed", "PickedUp" } }};
}

	@DataProvider(name = "reached")
	public static Object[][] flowMapperReached() {
		// FF Status, Payment Method, Delivery Status
		return new Object[][] { 
			{ "flow1", new String[] { "Placed", "Reached" } },
			{ "flow2", new String[] { "Placed", "Reached" } },
			{ "flow3", new String[] { "Placed", "Reached" } },
			{ "flow4", new String[] { "Placed", "Reached" } },
			{ "flow5", new String[] { "Placed", "Reached" } },
			{ "flow6", new String[] { "Placed", "Reached" } },
			{ "flow7", new String[] { "Placed", "Reached" } },
			{ "flow8", new String[] { "Placed", "Reached" } },
			{ "flow9", new String[] { "Placed", "Reached" } },
			{ "flow10", new String[] { "Placed", "Reached" } },
			{ "flow11", new String[] { "Placed", "Reached" } },
			{ "flow12", new String[] { "Placed", "Reached" } },
			{ "flow13", new String[] { "Placed", "Reached" } },
			{ "flow14", new String[] { "Placed", "Reached" } },
			{ "flow15", new String[] { "Placed", "Reached" } },
			{ "flow16", new String[] { "Placed", "Reached" } },
			{ "flow17", new String[] { "Placed", "Reached" } },
			{ "flow18", new String[] { "Placed", "Reached" } },
			{ "flow19", new String[] { "Placed", "Reached" } },
			{ "flow20", new String[] { "Placed", "Reached" } },
			{ "flow21", new String[] { "Placed", "Reached" } },
			{ "flow22", new String[] { "Placed", "Reached" } },
			{ "flow23", new String[] { "Placed", "Reached" } },
			{ "flow24", new String[] { "Placed", "Reached" } },
			{ "flow25", new String[] { "Placed", "Reached" } },
			{ "flow26", new String[] { "Placed", "Reached" } },
			{ "flow27", new String[] { "Placed", "Reached" } },
			{ "flow28", new String[] { "Placed", "Reached" } },
			{ "flow29", new String[] { "Placed", "Reached" } },
			{ "flow30", new String[] { "Placed", "Reached" } },
			{ "flow31", new String[] { "Placed", "Reached" } },
			{ "flow32", new String[] { "Placed", "Reached" } },
			{ "flow33", new String[] { "Placed", "Reached" } },
			{ "flow34", new String[] { "Placed", "Reached" } },
			{ "flow35", new String[] { "Placed", "Reached" } },
			{ "flow36", new String[] { "Placed", "Reached" } },
			{ "flow37", new String[] { "Placed", "Reached" } },
			{ "flow38", new String[] { "Placed", "Reached" } },
			{ "flow39", new String[] { "Placed", "Reached" } },
			{ "flow40", new String[] { "Placed", "Reached" } },
			{ "flow41", new String[] { "Placed", "Reached" } },
			{ "flow42", new String[] { "Placed", "Reached" } },
			{ "flow43", new String[] { "Placed", "Reached" } },
			{ "flow44", new String[] { "Placed", "Reached" } },
			{ "flow45", new String[] { "Placed", "Reached" } },
			{ "flow46", new String[] { "Placed", "Reached" } },
			{ "flow47", new String[] { "Placed", "Reached" } },
			{ "flow48", new String[] { "Placed", "Reached" } },
			{ "flow49", new String[] { "Placed", "Reached" } },
			{ "flow50", new String[] { "Placed", "Reached" } },
			{ "flow51", new String[] { "Placed", "Reached" } },
			{ "flow52", new String[] { "Placed", "Reached" } },
			{ "flow53", new String[] { "Placed", "Reached" } },
			{ "flow54", new String[] { "Placed", "Reached" } },
			{ "flow55", new String[] { "Placed", "Reached" } },
			{ "flow56", new String[] { "Placed", "Reached" } },
			{ "flow57", new String[] { "Placed", "Reached" } },
			{ "flow58", new String[] { "Placed", "Reached" } },
			{ "flow59", new String[] { "Placed", "Reached" } },
			{ "flow60", new String[] { "Placed", "Reached" } },
			{ "flow61", new String[] { "Placed", "Reached" } },
			{ "flow62", new String[] { "Placed", "Reached" } },
			{ "flow63", new String[] { "Placed", "Reached" } },
			{ "flow64", new String[] { "Placed", "Reached" } },
			{ "flow65", new String[] { "Placed", "Reached" } },
			{ "flow66", new String[] { "Placed", "Reached" } },
			{ "flow67", new String[] { "Placed", "Reached" } },
			{ "flow68", new String[] { "Placed", "Reached" } },
			{ "flow69", new String[] { "Placed", "Reached" } },
			{ "flow70", new String[] { "Placed", "Reached" } },
			{ "flow71", new String[] { "Placed", "Reached" } },
			{ "flow72", new String[] { "Placed", "Reached" } },
			{ "flow73", new String[] { "Placed", "Reached" } },
			{ "flow74", new String[] { "Placed", "Reached" } },
			{ "flow75", new String[] { "Placed", "Reached" } },
			{ "flow76", new String[] { "Placed", "Reached" } },
			{ "flow77", new String[] { "Placed", "Reached" } },
			{ "flow78", new String[] { "Placed", "Reached" } },
			{ "flow79", new String[] { "Placed", "Reached" } },
			{ "flow80", new String[] { "Placed", "Reached" } },
			{ "flow81", new String[] { "Placed", "Reached" } },
			{ "flow82", new String[] { "Placed", "Reached" } },
			{ "flow83", new String[] { "Placed", "Reached" } },
			{ "flow84", new String[] { "Placed", "Reached" } } };
}

	@DataProvider(name = "Delivered")
	public static Object[][] flowMapperDelivered() {
		// FF Status, Payment Method, Delivery Status
		return new Object[][] { 
			    { "flow1", new String[] { "Placed", "Delivered" } },
				{ "flow2", new String[] { "Placed", "Delivered" } },
				{ "flow3", new String[] { "Placed", "Delivered" } },
				{ "flow4", new String[] { "Placed", "Delivered" } },
				{ "flow5", new String[] { "Placed", "Delivered" } },
				{ "flow6", new String[] { "Placed", "Delivered" } },
				{ "flow7", new String[] { "Placed", "Delivered" } },
				{ "flow8", new String[] { "Placed", "Delivered" } },
				{ "flow9", new String[] { "Placed", "Delivered" } },
				{ "flow10", new String[] { "Placed", "Delivered" } },
				{ "flow11", new String[] { "Placed", "Delivered" } },
				{ "flow12", new String[] { "Placed", "Delivered" } },
				{ "flow13", new String[] { "Placed", "Delivered" } },
				{ "flow14", new String[] { "Placed", "Delivered" } },
				{ "flow15", new String[] { "Placed", "Delivered" } },
				{ "flow16", new String[] { "Placed", "Delivered" } },
				{ "flow17", new String[] { "Placed", "Delivered" } } };
	}

	@DataProvider(name ="FAQs")
	public static Object[][] nodeMapping()
	{
		// key - node id, values - title, isLeaf flag, Resolution flag, Description   
		return new Object[][]{
			{ "473", new String[] { "Can I edit my order?", "true", "true" } },
			{ "474", new String[] { "I want to cancel my order", "true", "true"} },
			{ "475", new String[] { "Will Swiggy be accountable for quality/quantity?", "true", "true"} },
			{ "476", new String[] { "Is there a minimum order value?", "true", "true" } },
			{ "477", new String[] { "Do you charge for delivery?", "true", "true" } },
			{ "478", new String[] { "How long do you take to deliver?", "true", "true" } },
			{ "479", new String[] { "What are your delivery hours?", "true", "true" } },
			{ "480", new String[] { "Can I order from any location?", "true", "true" } },
			{ "481", new String[] { "Is single order from many restaurants possible?", "true", "true" } },
			{ "482", new String[] { "Do you support bulk orders?", "true", "true" } },
			{ "483", new String[] { "Can I order in advance?", "true", "true" } },
			{ "484", new String[] { "Can I change the address / number?", "true", "true" } },
			{ "485", new String[] { "Did not receive OTP?", "true", "true" } },
			{ "486", new String[] { "Did not receive referral coupon?", "true", "true" } },
			{ "487", new String[] { "Deactivate my account", "true", "true" } },
			{ "488", new String[] { "Unable to view the details in my profile", "true", "true" } },
			{ "489", new String[] { "What is Swiggy Money?", "true", "true" } },
			{ "490", new String[] { "Do you accept Sodexo, Ticket Restaurant etc.?", "true", "true" } },
			{ "491", new String[] { "I want an invoice for my order", "true", "true" } },
			{ "492", new String[] { "I want to partner my restaurant with Swiggy", "true", "true" } },
			{ "493", new String[] { "I have a sponsorship proposal for Swiggy", "true", "true" } },
			{ "494", new String[] { "I want to explore career opportunities with Swiggy", "true", "true" } },
			{ "495", new String[] { "I want to provide feedback", "true", "true" } } };
		}
	
}

