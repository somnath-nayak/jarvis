package com.swiggy.api.erp.ff.helper;

import com.rabbitmq.client.AMQP;
import com.swiggy.api.erp.ff.constants.FraudServiceConstants;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.RabbitMQHelper;
import framework.gameofthrones.Tyrion.RedisHelper;
import framework.gameofthrones.Tyrion.WireMockHelper;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.Assert;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

public class FraudServiceHelper
{
    static Initialize gameofthrones = new Initialize();
    GameOfThronesService gameOfThronesService;
    Processor processor;
    WireMockHelper wireMockHelper = new WireMockHelper();
    RedisHelper redisHelper = new RedisHelper();
    RabbitMQHelper  rmqhelper = new RabbitMQHelper();
    public Processor getCODCheckServiceProcessor(String body) {

        HashMap<String,String> header = new HashMap<>();
        header.put("Content-Type","application/json");


        gameOfThronesService =  new GameOfThronesService("fraudservice","fraudservice", gameofthrones);

        String[] payloadParams = new String[] { body };


        processor = new Processor(gameOfThronesService, header, payloadParams);

        return processor;


    }



    public Processor getMockDeliveryProcessor()
    {



        gameOfThronesService =  new GameOfThronesService("wiremock","mockdelivery", gameofthrones);



        processor = new Processor(gameOfThronesService);

        return processor;


    }


    public Processor getMockDspProcessor()
    {



        gameOfThronesService =  new GameOfThronesService("wiremock","mockdsp", gameofthrones);

        HashMap<String,String>hmap = new HashMap<>();
        hmap.put("content-type","application/json");

        processor = new Processor(gameOfThronesService,hmap,null);

        return processor;


    }



    public void mockDSResponse( int statusCode, String prediction,String Status) throws IOException {
        String uri = "v1/predict/COD_Fraud";
        String contentType  = "application/json";
        File file = new File("../Data/MockAPI/ResponseBody/FraudServiceMocks/DSCODFraudMockResponse.json");
        String resBody = FileUtils.readFileToString(file);
        resBody = resBody.replace("${prediction}", prediction).replace("${status}", Status);
        wireMockHelper.setupStubPost(uri, statusCode, contentType, resBody);
    }

    //    public void mockDSResponse( int statusCode, String prediction) throws IOException {
//        String uri = "v1/predict/COD_Fraud";
//        String contentType  = "application/json";
//        File file = new File("../Data/MockAPI/ResponseBody/FraudServiceMocks/DSCODFraudMockResponse.json");
//        String resBody = FileUtils.readFileToString(file);
//        resBody = resBody.replace("${prediction}", prediction);
//        wireMockHelper.setupStubPost(uri, statusCode, contentType, resBody);
//    }
    public void mockDSResponse( int statusCode, String prediction,String Status,int delay) throws IOException {
        String uri = "v1/predict/COD_Fraud";
        String contentType  = "application/json";
        File file = new File("../Data/MockAPI/ResponseBody/FraudServiceMocks/DSCODFraudMockResponse.json");
        String resBody = FileUtils.readFileToString(file);
        resBody = resBody.replace("${prediction}", prediction).replace("${status}", Status);
        wireMockHelper.setupStubPost(uri, statusCode, contentType, resBody,delay);
    }


    public void mockDeliveryBoyResponse( int statusCode) throws IOException {
        String uri = "services/delivery-boy/mobile/";
        String contentType  = "application/json";
        File file = new File("../Data/MockAPI/ResponseBody/FraudServiceMocks/DeliveryBoyDetailsMockResponse.json");
        String resBody = FileUtils.readFileToString(file);
        wireMockHelper.setupStub(uri, statusCode, contentType, resBody);
    }


    public void AddToExceptionList(String userId) {
        redisHelper.sAdd("ffredis","cod_exclusion_list",userId);
    }

    public void AddCityConfigurations(String cityId, String startTime,String endTime,String fee)
    {
        redisHelper.setValue("ffredis",0,"late_night_start_hour_city_"+cityId,startTime);
        redisHelper.setValue("ffredis",0,"late_night_end_hour_city_"+cityId,endTime);
        redisHelper.setValue("ffredis",0,"late_night_fee_city_"+cityId,fee);
    }

    public void AddCityDefaultConfigurations(String cityId,String fee)
    {
        redisHelper.setValue("ffredis",0,"cod_fallback_limit_"+cityId,fee);
    }

    public void AddAreaLevelConfigurations(String areaId,String fee)
    {
        redisHelper.setValue("ffredis",0,"threshold_contingency_area_"+areaId,fee);
    }

    public void AddCityLevelConfigurations(String cityId,String fee)
    {
        redisHelper.setValue("ffredis",0,"threshold_contingency_city_"+cityId,fee);
    }


    public void clearRedisKeys(String cityId)
    {

        redisHelper.deleteKey("ffredis",0,"cod_exclusion_list");
        redisHelper.deleteKey("ffredis",0,"late_night_start_hour_city_"+cityId);
        redisHelper.deleteKey("ffredis",0,"late_night_end_hour_city_"+cityId);
        redisHelper.deleteKey("ffredis",0,"late_night_fee_city_"+cityId);
        redisHelper.deleteKey("ffredis",0,"threshold_contingency_area_"+cityId);
    }
    public void flushRedis()
    {
        redisHelper.flushAll("ffredis");
    }

    public void addCouponThreshold(String couponCategory, String limit)
    {
        redisHelper.setValue("ffredis",0,"coupon_category_count_threshold_for_category:"+couponCategory,limit);
    }

    public void addCouponDeviceCount(String categoryId, String DeviceId,String categoryCount)
    {
        redisHelper.setHashValue("ffredis",0,"coupon_category_count_map_for_device_id:"+DeviceId,categoryId,categoryCount);
    }


    public String getCouponDeviceCount(String DeviceId, String categoryId)
    {
        System.out.println(redisHelper.getHashValue("ffredis",0,"coupon_category_count_map_for_device_id:"+DeviceId,categoryId)+"====================================");
        return redisHelper.getHashValue("ffredis",0,"coupon_category_count_map_for_device_id:"+DeviceId,categoryId);
    }
    public void statusCheck(Processor processor)
    {
        Assert.assertEquals(200,processor.ResponseValidator.GetResponseCode());
    }

    public String getPreviousHour()
    {
        {
            DateFormat dateFormat = new SimpleDateFormat("HH");
            Date date = new Date();
            return(Integer.parseInt(dateFormat.format(date))-1)+"";

        }
    }


    public Processor getCODStatusProcessor(String customerId)
    {
        gameOfThronesService = new GameOfThronesService("fraudservice","codstatus",gameofthrones);
        HashMap<String ,String> headers = new HashMap<>();
        headers.put("content-type","application/json");
        String[] urlparams = new String[]{customerId};
        processor = new Processor(gameOfThronesService,headers,null,urlparams);
        return processor;

    }


    public Processor getCouponAbusesProcessor(String body)
    {
        gameOfThronesService = new GameOfThronesService("fraudservice","couponabuses",gameofthrones);
        HashMap<String ,String> headers = new HashMap<>();
        headers.put("content-type","application/json");
        String[] payload = new String[]{body};
        processor = new Processor(gameOfThronesService,headers,payload);
        return processor;

    }

    public Processor getCouponAbusesAPIProcessor(String deviceId, String category)
    {
        gameOfThronesService = new GameOfThronesService("fraudservice","couponabusesapi",gameofthrones);
        HashMap<String ,String> headers = new HashMap<>();
        headers.put("content-type","application/json");
        String[] payload = new String[]{deviceId,category};
        processor = new Processor(gameOfThronesService,headers,payload);
        return processor;

    }



    public void pushMessageToRMQForCouponCount(String transactionStatus,String deviceId, String categoryId) throws IOException {
        File file = new File("../Data/Payloads/JSON/fraudServiceOnPaymentConfirmation.json");
        String message = FileUtils.readFileToString(file);
        int length = 10;
        boolean useLetters = true;
        boolean useNumbers = false;
        String eventId = RandomStringUtils.random(length, useLetters, useNumbers);
        message = message.replace("${deviceId}", deviceId).replace("${cat}",categoryId).replace("${transcation_status}",transactionStatus).replace("${eventId}",eventId);
        rmqhelper.pushMessageToExchange(FraudServiceConstants.hostName,FraudServiceConstants.fraudExchagne,new AMQP.BasicProperties().builder().contentType("application/json"),message);
    }



}
