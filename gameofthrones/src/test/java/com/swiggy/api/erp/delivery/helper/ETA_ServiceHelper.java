package com.swiggy.api.erp.delivery.helper;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.delivery.constants.ClusterConstants;
import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.KafkaHelper;
import org.apache.noggit.JSONUtil;
import org.apache.solr.common.SolrDocumentList;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;

/**
 * 	Created by somnath.nayak on 20/11/18.
 */

public class ETA_ServiceHelper {

    Initialize gameofthrones = null;

    public ETA_ServiceHelper()
    {
        gameofthrones = new Initialize();
    }

    ServiceablilityHelper serviceablilityHelper = new ServiceablilityHelper();
    DeliveryDataHelper deliveryDataHelper = new DeliveryDataHelper();
    DeliveryHelperMethods deliveryHelperMethods = new DeliveryHelperMethods();
    DeliveryServiceHelper deliveryServiceHelper = new DeliveryServiceHelper();
    SolrHelper solrHelper = new SolrHelper();
    KafkaHelper kafkaHelper = new KafkaHelper();

    static String topic = "de_locations";
    static String kafkaDB = "deliverykafka";


    public Processor getServiceOrderTracking(String order_id) {
        GameOfThronesService gameOfThronesService = new GameOfThronesService("deliveryservice", "serviceOrderTracking", gameofthrones);
        Processor processor = new Processor(gameOfThronesService, DeliveryConstant.headers(), null, new String[]{order_id});
        return processor;
    }

    public Processor getControllerOrderTracking(String order_id) {
        GameOfThronesService gameOfThronesService = new GameOfThronesService("deliverycontroller", "controllerOrderTracking", gameofthrones);
        Processor processor = new Processor(gameOfThronesService, DeliveryConstant.headers(), null, new String[]{order_id});
        return processor;
    }


    // Placing order in hard codded restaurants

    public List<String> getRestaurantLatlng_CityId(String restaurant_id){
        String solrUrl = deliveryHelperMethods.getSolrUrl();
        SolrDocumentList solrDocuments = deliveryDataHelper.getSolrOutput(solrUrl, "core_listing", "id:"+ restaurant_id +"");
        String jsonDoc = JSONUtil.toJSON(solrDocuments);
        System.out.println(jsonDoc);
        //String[] restLatLng = JsonPath.read(jsonDoc, "$[*].place").toString().replace("[\"", "").replace("\"]", "").split(",");
        String restLatLng = JsonPath.read(jsonDoc, "$[*].place").toString().replace("[\"", "").replace("\"]", "");
        String city_id = JsonPath.read(jsonDoc, "$[*].city_id").toString().replace("[", "").replace("]", "");
        return Arrays.asList(restLatLng,city_id);
    }

    public List<String> getLastMile_Area(String lat, String lng, String restaurant_id){
        Processor processor = serviceablilityHelper.cerebroListingWithRestId(lat,lng,restaurant_id);
        String response = processor.ResponseValidator.GetBodyAsText();
        String lastMileTravel = JsonPath.read(response, "$..listingServiceabilityResponse.last_mile_travel").toString().replace("[", "").replace("]", "");
        String area_id =JsonPath.read(response, "$.serviceableRestaurants..areaId").toString().replace("[", "").replace("]", "");
        return Arrays.asList(lastMileTravel,area_id);
    }

    public String getCurrentTime() {
        Date date = new Date();
        String currentTimeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
        return currentTimeStamp;
    }

    public String getOrderId(String restaurant_id, String lat, String lng){
        String epochTime = String.valueOf(Instant.now().getEpochSecond());
        Map<String, String> orderData= new HashMap<>();
        orderData.put("restaurant_id",restaurant_id);
        orderData.put("restaurant_lat_lng",getRestaurantLatlng_CityId(restaurant_id).get(0));
        orderData.put("restaurant_city_code", getRestaurantLatlng_CityId(restaurant_id).get(1));
        orderData.put("restaurant_customer_distance",getLastMile_Area(lat,lng,restaurant_id).get(0));
        orderData.put("restaurant_area_code",getLastMile_Area(lat,lng,restaurant_id).get(1));
        orderData.put("order_time",getCurrentTime());
        orderData.put("order_type","partner");
        orderData.put("is_long_distance","false");
        orderData.put("lat",lat);
        orderData.put("lng",lng);
        orderData.put("ordered_time_in_seconds",epochTime);

        String order_json = deliveryDataHelper.returnOrderJson(orderData);
        String generatedOrder = null;
        try {
            JSONObject order = new JSONObject(order_json);
            generatedOrder = order.get("order_id").toString();
        } catch (JSONException je) {
            je.printStackTrace();
        }
        Boolean validate = deliveryDataHelper.pushOrderJsonToExchangeAndValidate(order_json);   //Push order to Exchange
        //Boolean validate = deliveryDataHelper.pushOrderJsonToRMQAndValidate(order_json);      //Push order to Queue
        if (validate) {
            System.out.println("The generated order id is : " + generatedOrder);
            return generatedOrder;
        } else {
            System.out.println("********** Failed to push order json into rmq **********");
            return null;
        }
    }


    // Placing order in random restaurants

    public Map<String, String> getRestaurantDetails(){                 //used in getOrderID()
        String solrUrl = deliveryHelperMethods.getSolrUrl();
        SolrDocumentList solrDocuments = solrHelper.getSolrOutput(solrUrl,"core_listing","enabled:true AND city_id:1 AND area_id:6",1);
        String jsonDoc = JSONUtil.toJSON(solrDocuments);
        System.out.println(jsonDoc);
        String rest_id = JsonPath.read(jsonDoc, "$[*].id").toString().replace("[\"", "").replace("\"]", "");
        String restLatLng = JsonPath.read(jsonDoc, "$[*].place").toString().replace("[\"", "").replace("\"]", "");
        String rest_area = JsonPath.read(jsonDoc, "$[*].area_id").toString().replace("[", "").replace("]", "");
        String rest_zone = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).queryForList(DeliveryConstant.zoneIDfromAreaID + rest_area).get(0).get("zone_id").toString();

        String city_id = JsonPath.read(jsonDoc, "$[*].city_id").toString().replace("[", "").replace("]", "");
        String[] ApproxCustomerLatLng = deliveryDataHelper.getApproxLatLongAtADistance(Double.parseDouble(restLatLng.split(",")[0]), Double.parseDouble(restLatLng.split(",")[1]), 1.5 ).split(",");

        Processor processor = serviceablilityHelper.cerebroListingWithRestId(ApproxCustomerLatLng[0],ApproxCustomerLatLng[1],rest_id);
        String response = processor.ResponseValidator.GetBodyAsText();
        String lastMileTravel = JsonPath.read(response, "$..listingServiceabilityResponse.last_mile_travel").toString().replace("[", "").replace("]", "");

        Map<String, String> map = new HashMap<>();
        map.put("rest_id", rest_id);
        map.put("restLatLng", restLatLng);
        map.put("rest_area", rest_area);
        map.put("rest_zone", rest_zone);
        map.put("city_id", city_id);
        map.put("ApproxCustomerLatLng",ApproxCustomerLatLng[0] + "," + ApproxCustomerLatLng[1]);
        map.put("ApproxCustomerLat", ApproxCustomerLatLng[0]);
        map.put("ApproxCustomerLng", ApproxCustomerLatLng[1]);
        map.put("lastMileTravel", lastMileTravel);
        return map;
    }

    public String getOrderId(){
        String epochTime = String.valueOf(Instant.now().getEpochSecond());
        Map<String, String> orderData= new HashMap<>();
        orderData.put("restaurant_id",getRestaurantDetails().get("rest_id"));
        orderData.put("restaurant_lat_lng",getRestaurantDetails().get("restLatLng"));
        orderData.put("restaurant_city_code", getRestaurantDetails().get("city_id"));
        orderData.put("restaurant_customer_distance",getRestaurantDetails().get("lastMileTravel"));
        orderData.put("restaurant_area_code",getRestaurantDetails().get("rest_area"));
        orderData.put("order_time",getCurrentTime());
        orderData.put("order_type","partner");
        orderData.put("is_long_distance","false");
        orderData.put("lat",getRestaurantDetails().get("ApproxCustomerLat"));
        orderData.put("lng",getRestaurantDetails().get("ApproxCustomerLng"));
        orderData.put("ordered_time_in_seconds",epochTime);

        String order_json = deliveryDataHelper.returnOrderJson(orderData);
        String generatedOrder = null;
        try {
            JSONObject order = new JSONObject(order_json);
            generatedOrder = order.get("order_id").toString();
        } catch (JSONException je) {
            je.printStackTrace();
        }
        Boolean validate = deliveryDataHelper.pushOrderJsonToExchangeAndValidate(order_json);   //Push order to Exchange
        //Boolean validate = deliveryDataHelper.pushOrderJsonToRMQAndValidate(order_json);      //Push order to Queue
        if (validate) {
            System.out.println("The generated order id is : " + generatedOrder);
            return generatedOrder;
        } else {
            System.out.println("********** Failed to push order json into rmq **********");
            return null;
        }
    }

    public String DElocationMesssageForKafka(String de_id, String batch_id){
        String message = "{\"deId\":"+de_id+",\"location\": {\"coordinates\": [\"78.3062933\",\"17.4920883\"],\"accuracy\": \"10.0\",\"latlng\": \"17.4920883,78.3062933\",\"type\": \"Point\",\"time\": 1537281389260,\"discardedLocationsStr\": \"[]\",\"batchId\":"+batch_id+"}}";
        return message;
    }

    public void pushDElocationMesssageToKafka(String de_id, String batch_id) throws Exception{
        String message = "{\"deId\":"+de_id+",\"location\": {\"coordinates\": [\"78.3062933\",\"17.4920883\"],\"accuracy\": \"10.0\",\"latlng\": \"17.4920883,78.3062933\",\"type\": \"Point\",\"time\": 1537281389260,\"discardedLocationsStr\": \"[]\",\"batchId\":"+batch_id+"}}";
        kafkaHelper.runProducer(kafkaDB,topic,message);
    }

    public String getLatLngPrecisionOfxDecimalPoints(String latLng, int precision){
       double lat = Double.parseDouble(latLng.split(",")[0]);
       double lng = Double.parseDouble(latLng.split(",")[1]);
       StringBuilder pattern = new StringBuilder("#.");
       for (int i=1; i<=precision; i++){ pattern.append("#");}
       DecimalFormat df = new DecimalFormat(pattern.toString());
       String Latitude =  df.format(lat);
       String Longitude = df.format(lng);
       return Latitude+","+Longitude;

    }

    public void processOrder(String order_id, String order_state){
        switch (order_state) {
            case "confirmed":
                threadWait(12000);
                deliveryServiceHelper.zipDialConfirm(order_id);
                break;

            case "arrived":
                threadWait(12000);
                deliveryServiceHelper.zipDialConfirm(order_id);
                threadWait(12000);
                deliveryServiceHelper.zipDialArrived(order_id);
                break;

            case "pickedUp":
                threadWait(12000);
                deliveryServiceHelper.zipDialConfirm(order_id);
                threadWait(12000);
                deliveryServiceHelper.zipDialArrived(order_id);
                threadWait(12000);
                deliveryServiceHelper.zipDialPickedUp(order_id);
                break;

            case "reached":
                threadWait(12000);
                deliveryServiceHelper.zipDialConfirm(order_id);
                threadWait(12000);
                deliveryServiceHelper.zipDialArrived(order_id);
                threadWait(12000);
                deliveryServiceHelper.zipDialPickedUp(order_id);
                threadWait(12000);
                deliveryServiceHelper.zipDialReached(order_id);
                break;

            case "delivered":
                threadWait(12000);
                deliveryServiceHelper.zipDialConfirm(order_id);
                threadWait(12000);
                deliveryServiceHelper.zipDialArrived(order_id);
                threadWait(12000);
                deliveryServiceHelper.zipDialPickedUp(order_id);
                threadWait(12000);
                deliveryServiceHelper.zipDialReached(order_id);
                threadWait(12000);
                deliveryServiceHelper.zipDialDelivered(order_id);
                break;

            case "deRejected":
                threadWait(12000);
                deliveryServiceHelper.zipDialRejectedDE(order_id);
                break;

            default:
                System.out.println("order state is Unknown");
        }
    }

    public long epochConverter(String timestamp, String pattern)throws Exception{
        SimpleDateFormat df = new SimpleDateFormat(pattern);    //"yyyy-MM-dd HH:mm:ss.S"
        df.setTimeZone(TimeZone.getTimeZone("GMT+05:30"));
        Date date = df.parse(timestamp);
        long epochTime = date.getTime();
        System.out.println("<<---------------EPOCH TIME--------------->> "+ epochTime);
        return  epochTime;
    }

    public String epochToTimeStampConverter(long epochTime, String pattern){
        Date date1 = new Date(epochTime);
        DateFormat format = new SimpleDateFormat(pattern);
        format.setTimeZone(TimeZone.getTimeZone("GMT+05:30"));
        String timestamp = format.format(date1);
        System.out.println("<<---------------FORMATTED TIMESTAMP--------------->> "+timestamp);
        return timestamp;
    }

    public void threadWait(long time){
        try {
            Thread.sleep(time);
        }catch (Exception e){e.printStackTrace();}

    }

}
