package com.swiggy.api.erp.cms.pojo.ItemHolidaySlot;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;

public class ItemHolidaySlot {

    private Data data;
    @JsonProperty("user_meta")
    private UserMeta userMeta;

    /**
     * No args constructor for use in serialization
     *
     */
    public ItemHolidaySlot() {
    }

    /**
     *
     * @param userMeta
     * @param data
     */
    public ItemHolidaySlot(Data data, UserMeta userMeta) {
        super();
        this.data = data;
        this.userMeta = userMeta;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public UserMeta getUserMeta() {
        return userMeta;
    }

    public void setUserMeta(UserMeta userMeta) {
        this.userMeta = userMeta;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("data", data).append("userMeta", userMeta).toString();
    }

}