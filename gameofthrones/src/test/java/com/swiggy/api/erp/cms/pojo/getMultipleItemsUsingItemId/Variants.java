package com.swiggy.api.erp.cms.pojo.getMultipleItemsUsingItemId;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "exclude_list",
        "variant_groups"
})
public class Variants {

    @JsonProperty("exclude_list")
    private List<Object> excludeList = null;
    @JsonProperty("variant_groups")
    private List<VariantGroup> variantGroups = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public Variants() {
    }

    /**
     *
     * @param excludeList
     * @param variantGroups
     */
    public Variants(List<Object> excludeList, List<VariantGroup> variantGroups) {
        super();
        this.excludeList = excludeList;
        this.variantGroups = variantGroups;
    }

    @JsonProperty("exclude_list")
    public List<Object> getExcludeList() {
        return excludeList;
    }

    @JsonProperty("exclude_list")
    public void setExcludeList(List<Object> excludeList) {
        this.excludeList = excludeList;
    }

    @JsonProperty("variant_groups")
    public List<VariantGroup> getVariantGroups() {
        return variantGroups;
    }

    @JsonProperty("variant_groups")
    public void setVariantGroups(List<VariantGroup> variantGroups) {
        this.variantGroups = variantGroups;
    }

}