package com.swiggy.api.erp.delivery.tests;

import com.swiggy.api.erp.delivery.dp.DeliveryControllerRegressionDataProvider;
import com.swiggy.api.erp.delivery.helper.*;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.HashMap;


/**
 * Created by Swetha B A on 23/11/18.
 */

public class DeliveryControllerRegression extends DeliveryControllerRegressionDataProvider {

    public static String deID;
    Integer zoneID = 1;
    DeliveryDataHelper deldatahelp=new DeliveryDataHelper();
    DeliveryHelperMethods delmeth = new DeliveryHelperMethods();
    DeliveryServiceHelper deliveryServiceHelper = new DeliveryServiceHelper();
    AutoassignHelper autoassignHelper = new AutoassignHelper();
    HashMap<String,String> map = new HashMap<String,String>();

    @BeforeClass
    public void setup(){
        delmeth.dbhelperupdate(updateDeliveryBoy);
        String updateMyOrderToGetAssigned = "update batch set de_id=3456 where de_id is NULL and zone_id="+zoneID;
        delmeth.dbhelperupdate(updateMyOrderToGetAssigned);
        deID = deldatahelp.CreateDE(zoneID);
        System.out.println("********************************************DE id is "+deID);
    }

    @BeforeMethod
    public void beforeMethod(){
        String query = "update delivery_boys set enabled=1, batch_id=NULL, status='FR' where id = "+deID;
        delmeth.dbhelperupdate(query);
    }

    @AfterMethod
    public void makeDEInactive(){
        deliveryServiceHelper.makeDEFree(deID);
        deliveryServiceHelper.markDEInactive(deID,"0");
        delmeth.dbhelperupdate(updateDeliveryBoy);
        for(String maps : map.keySet()){
            String key =maps.toString();
            String value = map.get(maps).toString();
            System.out.println(key + " " + value);
        }
    }

    public String createNewDE(String orderId){
        String deID1;
        deID1 = deldatahelp.CreateDE(zoneID);
        System.out.println("********************************************DE id 1 is "+deID1);
        String getRestaurantlatlon1 = "select r.lat_long from trips t inner join restaurant r on t.restaurant_id=r.id where t.order_id=" + orderId;
        String restaurantlatlon1 = delmeth.dbhelperget(getRestaurantlatlon1, "lat_long").toString();
        String[] latlong1 = restaurantlatlon1.split(",");
        deldatahelp.updateDELocationInRedis(deID1,latlong1[0],latlong1[1]);
        deliveryServiceHelper.makeDEActive(deID1);
        deldatahelp.addDEzonemap(deID1,zoneID);
        return deID1;
    }

    @Test(dataProvider = "verifyOrderIsAssignedIfFMIsLess",description = "Verify whether order is getting assigned to DE if first mile distance is less than configred limit")
    public void verifyOrderIsAssignedIfFMIsLessThanConfigured(String orderId,String ordered_time){
        boolean assignstatus = false;
        boolean cronStatus = autoassignHelper.runAutoAssign(cityId);
        Assert.assertTrue(cronStatus,"Cron Status is : "+cronStatus);
        String query = "select de_id from trips where order_id=" + orderId;
        assignstatus = deldatahelp.polldb("deliverydb", query, "de_id", deID, 2, 60);
        System.out.println("Order is assigned "+assignstatus);
        if(!assignstatus){
            map.put(orderId,deID);
            map.put(orderId,ordered_time);
        }
        Assert.assertTrue(assignstatus,"Order is assigned");
        deliveryServiceHelper.ordercancel(orderId);

    }

    @Test(dataProvider = "verifyOrderNotAssignedIfDEDifferntZone", description = "Verify whether order of zone A is not assigned to DE of whom zone A is not one of the service zone")
    public void verifyOrderNotAssignedIfDeDifferntZone(String orderId){
        boolean assignstatus = false;
        boolean cronStatus = autoassignHelper.runAutoAssign(cityId);
        Assert.assertTrue(cronStatus,"Cron Status is : "+cronStatus);
        String query = "select de_id from trips where order_id=" + orderId;
        assignstatus = deldatahelp.polldb("deliverydb", query, "de_id", deID, 2, 60);
        System.out.println("Order is assigned "+assignstatus);
        Assert.assertFalse(assignstatus,"Order is assigned");
        deliveryServiceHelper.ordercancel(orderId);
    }

    @Test(dataProvider = "verifyOrderIsRejectedIfDENotConfirmed",description = "Verify order A is getting auto/system rejected if DE D does not confirm the order within 3 mins")
    public void verifyOrderIsRejectedIfDENotConfirmed(String orderId,String ordered_time)throws InterruptedException{
        boolean assignstatus = false;
        boolean cronStatus = autoassignHelper.runAutoAssign(cityId);
        Assert.assertTrue(cronStatus,"Cron Status is : "+cronStatus);
        String query = "select de_id from trips where order_id=" + orderId;
            assignstatus = deldatahelp.polldb("deliverydb", query, "de_id", deID, 2, 60);
        System.out.println("Order is assigned "+assignstatus);
        Assert.assertTrue(assignstatus,"Order is assigned");
        if(!assignstatus){
            map.put(orderId,deID);
            map.put(orderId,ordered_time);
        }
        deliveryServiceHelper.systemRejectOrder(deID,orderId);
        Thread.sleep(30000);
        Object assignedStatus1 = delmeth.dbhelperget(query, "de_id");
        System.out.println("************************************"+assignedStatus1);
        Assert.assertNull(assignedStatus1);
        deliveryServiceHelper.ordercancel(orderId);
    }

    @Test(dataProvider= "verifyCancelledOrderNotConsiderForAssignment",description = "Verify that cancelled order is not considered for assignment ")
    public void verifyCancelledOrderNotConsiderForAssignment(String statusMessage,String orderID){
        System.out.println(statusMessage);
        Assert.assertEquals(statusMessage,"Trying to assign cancelled order "+orderID);
    }


    @Test(dataProvider="verifyDEScoreAddedForWithDE",description = "Verify with DE score is added to DE score when with DE order is assigned to DE")
    public void verifyDEScoreAddedForWithDE(String firstOrderId, String secondOrderId,String batchId1,String batchId2) throws InterruptedException{
        String query = "select order_id from trips where `de_id`=" +deID+" and with_de = 1;";
        String updateBatchId = "update batch set cancelled=1 where id ="+batchId1;
        String updateBatchId1 = "update batch set cancelled=1 where id ="+batchId2;
        boolean cronStatus = false;
        cronStatus = autoassignHelper.runAutoAssign(cityId);
        Assert.assertTrue(cronStatus,"Cron Status is : "+cronStatus);
        String orderID = delmeth.dbhelperget(query, "order_id").toString();
        Assert.assertEquals(firstOrderId,orderID);
        deliveryServiceHelper.ordercancel(firstOrderId);
        delmeth.dbhelperupdate(updateBatchId);
        deliveryServiceHelper.ordercancel(secondOrderId);
        delmeth.dbhelperupdate(updateBatchId1);
    }

    @Test(dataProvider = "verifyOrderDelayAddedToOrderAssignment", description = "Verify Weight of Order Delay is added to DE score when order assignment is delayed beyond Order delay step factor")
    public void verifyOrderDelayAddedToOrderAssignment(String firstOrderId, String secondOrderId){
        String query = "select order_id from trips where `de_id`=" +deID;
        boolean cronStatus = false;
        cronStatus = autoassignHelper.runAutoAssign(cityId);
        Assert.assertTrue(cronStatus,"Cron Status is : "+cronStatus);
        String orderID = delmeth.dbhelperget(query, "order_id").toString();
        Assert.assertEquals(firstOrderId,orderID);
        deliveryServiceHelper.ordercancel(firstOrderId);
        deliveryServiceHelper.ordercancel(secondOrderId);
    }


    @Test(dataProvider = "verifyWithinSiftWeightAdded", description = "Verify Within Shift Weight is added to DE score when order is assigned to DE during DE shift times")
    public void verifyWithinSiftWeightAdded(String deID1, String deID2,String orderId){
        boolean cronStatus = autoassignHelper.runAutoAssign(cityId);
        Assert.assertTrue(cronStatus,"Cron Status is : "+cronStatus);
        String query = "select de_id from trips where order_id=" + orderId;
        String de_id = delmeth.dbhelperget(query, "de_id").toString();
        Assert.assertEquals(de_id,deID1);
        deliveryServiceHelper.makeDEFree(deID1);
        deliveryServiceHelper.markDEInactive(deID1,"0");
        deliveryServiceHelper.makeDEFree(deID2);
        deliveryServiceHelper.markDEInactive(deID2,"0");
        deliveryServiceHelper.ordercancel(orderId);

    }

    @Test(dataProvider = "verifyWeightOfCycleDEIsAdded", description = "Verify Weight of Cycle DEs is added to DE score when order is assigned to cycle DE")
    public void verifyWeightOfCycleDEIsAdded(String deID1, String deID2, String orderId){
        boolean cronStatus = autoassignHelper.runAutoAssign(cityId);
        Assert.assertTrue(cronStatus,"Cron Status is : "+cronStatus);
        String query = "select de_id from trips where order_id=" + orderId;
        String de_id = delmeth.dbhelperget(query, "de_id").toString();
        Assert.assertEquals(de_id,deID1);
        deliveryServiceHelper.ordercancel(orderId);
        deliveryServiceHelper.makeDEFree(deID1);
        deliveryServiceHelper.markDEInactive(deID1,"0");
        deliveryServiceHelper.makeDEFree(deID2);
        deliveryServiceHelper.markDEInactive(deID2,"0");

    }


    @Test(dataProvider = "verifyOrderIsAssignedToCycleDE",description = "Verify order is assigned to cycle DE if first mile distance is less than Max first mile distance for Cycle DEs")
    public void verifyOrderIsAssignedToCycleDE(String deID2, String orderId){
        boolean cronStatus = autoassignHelper.runAutoAssign(cityId);
        Assert.assertTrue(cronStatus,"Cron Status is : "+cronStatus);
        String query = "select de_id from trips where order_id=" + orderId;
        String de_id = delmeth.dbhelperget(query, "de_id").toString();
        Assert.assertEquals(de_id,deID2);
        deliveryServiceHelper.ordercancel(orderId);
        deliveryServiceHelper.makeDEFree(deID2);
        deliveryServiceHelper.markDEInactive(deID2,"0");
    }

    @Test(dataProvider = "verifyOrderAssignedIfLMIsLess", description = "Verify order is assigned to cycle DE if last mile distance is less than Max last mile distance for Cycle DEs")
    public void verifyOrderAssignedIfLMIsLess(String deID1, String orderId){
        boolean cronStatus = autoassignHelper.runAutoAssign(cityId);
        Assert.assertTrue(cronStatus,"Cron Status is : "+cronStatus);
        String query = "select de_id from trips where order_id=" + orderId;
        String de_id = delmeth.dbhelperget(query, "de_id").toString();
        Assert.assertEquals(de_id,deID1);
        deliveryServiceHelper.ordercancel(orderId);
        deliveryServiceHelper.makeDEFree(deID1);
        deliveryServiceHelper.markDEInactive(deID1,"0");
    }

    @Test(dataProvider = "verifyOrderAssignedToDEIfHaltedXmins",description = "Verify order assignment to DE is halted by x mins even when DE is eligible for order assignment where 'x' is Min JIT delay configured")
    public void verifyOrderAssignedToDEIfHaltedXmins(String orderId, String disableJitForRestaurant) throws InterruptedException{
        boolean assignstatus = false;
        boolean cronStatus = autoassignHelper.runAutoAssign(cityId);
        Assert.assertTrue(cronStatus,"Cron Status is : "+cronStatus);
        String query = "select de_id from trips where order_id=" + orderId;
        assignstatus = deldatahelp.polldb("deliverydb", query, "de_id", deID, 2, 60);
        Assert.assertFalse(assignstatus);
        Thread.sleep(70000);
        autoassignHelper.runAutoAssign(cityId);
        assignstatus = deldatahelp.polldb("deliverydb", query, "de_id", deID, 2, 60);
        Assert.assertTrue(assignstatus);
        delmeth.dbhelperupdate(disableJitForRestaurant);
    }

    @Test(dataProvider="verifyFMIsIncreasedByStepFactor",description = "Verify max first mile distance is increased by 'step factor for dynamic First Mile' value when no DE is within normal first mile")
    public void verifyFMIsIncreasedByStepFactor(String orderId)throws InterruptedException{
        boolean assignstatus = false;
        boolean cronStatus = autoassignHelper.runAutoAssign(cityId);
        Assert.assertTrue(cronStatus,"Cron Status is : "+cronStatus);
        String query = "select de_id from trips where order_id=" + orderId;
        assignstatus = deldatahelp.polldb("deliverydb", query, "de_id", deID, 2, 60);
        Assert.assertFalse(assignstatus);
        Thread.sleep(240000);
        assignstatus = deldatahelp.polldb("deliverydb", query, "de_id", deID, 2, 60);
        Assert.assertTrue(assignstatus);
    }

    @Test(dataProvider="verifyDEOfOtherServiceZoneIsConsider", description = "Verify DE of other service zone is considered for assignment if DE is within max customer zone max first mile distance")
    public void verifyDEOfOtherServiceZoneIsConsider(String orderId){
        boolean assignstatus = false;
        boolean cronStatus = autoassignHelper.runAutoAssign(cityId);
        Assert.assertTrue(cronStatus,"Cron Status is : "+cronStatus);
        String query = "select de_id from trips where order_id=" + orderId;
        assignstatus = deldatahelp.polldb("deliverydb", query, "de_id", deID, 2, 60);
        System.out.println("Order is assigned "+assignstatus);
        Assert.assertTrue(assignstatus,"Order is assigned");

    }

    @Test(dataProvider = "verifyBusyDENotConsiderForAssignment",description = "Verify that already busy DE is not considered for assignment")
    public void verifyBusyDENotConsiderForAssignment(String deID,String statusMessage){
        Assert.assertEquals(statusMessage,"Manual Assignment failed! Order has already been assigned to "+deID);
    }

    @Test(dataProvider = "verifyWeightOfOrderDelayCalculated",description = "Verify that \"Weight of Order Delay\" is calculated for an order when order delay is greater than the delay threshold")
    public void verifyWeightOfOrderDelayCalculated(String orderId){
        boolean cronStatus = autoassignHelper.runAutoAssign(cityId);
        Assert.assertTrue(cronStatus,"Cron Status is : "+cronStatus);
        String query = "select order_id from trips where de_id=" + deID;
        String order_id = delmeth.dbhelperget(query,"order_id").toString();
        Assert.assertEquals(order_id,orderId);
    }

    @Test(dataProvider="verifyCODOrderNotAssignedToTempDE", description = "Verify COD order is not assigned to temp DE")
    public void verifyCODOrderNotAssignedToTempDE(String orderId){
        boolean assignstatus = false;
        boolean cronStatus = autoassignHelper.runAutoAssign(cityId);
        Assert.assertTrue(cronStatus,"Cron Status is : "+cronStatus);
        String query = "select de_id from trips where order_id=" + orderId;
        Object de_id =delmeth.dbhelperget(query,"de_id");
        Assert.assertNull(de_id);
    }

    @Test(dataProvider = "verifyInActiveDENotConsiderForAssignment",description = "Verify that DE is not considered for assignment if DE is not active")
    public void verifyInActiveDENotConsiderForAssignment(String statusMessage,String orderId){
        Assert.assertEquals(statusMessage,"Delivery boy is busy");
    }

    @Test(dataProvider="verifyOrderISAssignedToDifferentDEIfRejected",description = "verify order A is assigned to DE D1 if it was auto/system rejected by DE D")
    public void verifyOrderISAssignedToDifferentDEIfRejected(String orderId,String deID1,String deID2) throws InterruptedException{
        boolean assignstatus = false;
        boolean cronStatus = autoassignHelper.runAutoAssign(cityId);
        Assert.assertTrue(cronStatus,"Cron Status is : "+cronStatus);
        String query = "select de_id from trips where order_id=" + orderId;
        assignstatus = deldatahelp.polldb("deliverydb", query, "de_id", deID, 2, 60);
        Assert.assertTrue(assignstatus);
        deliveryServiceHelper.systemRejectOrder(deID1,orderId);
        cronStatus = autoassignHelper.runAutoAssign(cityId);
        Thread.sleep(60000);
        Assert.assertTrue(cronStatus,"Cron Status is : "+cronStatus);
        Assert.assertEquals(delmeth.dbhelperget(query,"de_id").toString(),deID1);
    }



    @Test(dataProvider = "verifyOrderNotAssignedToSameDEIfRejected", description = "Verify order A is not getting auto-assigned to DE D if DE D already auto/system rejected the order A")
    public void verifyOrderNotAssignedToSameDEIfRejected(String orderId) throws InterruptedException {
        boolean assignstatus = false;
        boolean cronStatus = false;
        cronStatus = autoassignHelper.runAutoAssign(cityId);
        Assert.assertTrue(cronStatus,"Cron Status is : "+cronStatus);
        String query = "select de_id from trips where order_id=" + orderId;
        String De_Id = delmeth.dbhelperget(query, "de_id").toString();
        assignstatus = deldatahelp.polldb("deliverydb", query, "de_id", deID, 2, 60);
        System.out.println("Order is assigned "+assignstatus);
        deliveryServiceHelper.ordercancel(orderId);
        Assert.assertTrue(assignstatus,"Order is assigned");
        deliveryServiceHelper.systemRejectOrder(De_Id, orderId);
        Thread.sleep(2000);
        String secondDE = createNewDE(orderId);
        cronStatus = autoassignHelper.runAutoAssign(cityId);
        Thread.sleep(120000);
        Assert.assertTrue(cronStatus,"Cron Status is : "+cronStatus);
        String query1 = "select de_id from trips where order_id=" + orderId;
        String newDe_Id = delmeth.dbhelperget(query1, "de_id").toString();
        Assert.assertNotEquals(De_Id,newDe_Id);
        deliveryServiceHelper.markDEInactive(newDe_Id,"0");

    }



}
