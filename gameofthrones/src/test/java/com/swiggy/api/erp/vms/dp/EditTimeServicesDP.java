package com.swiggy.api.erp.vms.dp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.testng.annotations.DataProvider;

import com.swiggy.api.erp.ff.helper.OMSHelper;
import com.swiggy.api.erp.vms.constants.RMSConstants;
import com.swiggy.api.erp.vms.editservice.pojo.Addon;
import com.swiggy.api.erp.vms.editservice.pojo.EditServiceBuilder;
import com.swiggy.api.erp.vms.editservice.pojo.EditServiceEntry;
import com.swiggy.api.erp.vms.editservice.pojo.Edit_data;
import com.swiggy.api.erp.vms.editservice.pojo.Item;
import com.swiggy.api.erp.vms.editservice.pojo.Meta_data;
import com.swiggy.api.erp.vms.editservice.pojo.Oos_addon;
import com.swiggy.api.erp.vms.editservice.pojo.Oos_and_alternates;
import com.swiggy.api.erp.vms.editservice.pojo.Oos_datum;
import com.swiggy.api.erp.vms.editservice.pojo.Oos_variant;
import com.swiggy.api.erp.vms.editservice.pojo.Variant;
import com.swiggy.api.erp.vms.helper.RMSCommonHelper;
import com.swiggy.api.erp.vms.helper.RMSHelper;
import com.swiggy.api.sf.checkout.pojo.Cart;
import com.swiggy.api.sf.snd.helper.SnDHelper;

import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;

public class EditTimeServicesDP {

	static RMSCommonHelper r = new RMSCommonHelper();
	static String restaurantId = null;
	public static String VENDOR_PASSWORD = null;
	public static String VENDOR_USERNAME = null;
	public static String consumerAppPassword = null;
	public static long consumerAppMobile = 0l;
	public static String OMS_USERNAME = null;
	public static String OMS_PASSWORD = null;
	static HashMap<String, String> omsSessionData = null;
	static HashMap<String, String> rmsSessionData = null;

	static {
		try {
			if (RMSCommonHelper.getEnv().equalsIgnoreCase("stage1")) {
				VENDOR_PASSWORD = "El3ment@ry";
				VENDOR_USERNAME = "5271";
				consumerAppPassword = "test1234";
				consumerAppMobile = 9886379321l;
				restaurantId = "5271";
				OMS_USERNAME = "ramzi@swiggy.in";
				OMS_PASSWORD = "Test@1234";
			} else if (RMSCommonHelper.getEnv().equalsIgnoreCase("stage2")) {
				VENDOR_PASSWORD = "imran@123";
				VENDOR_USERNAME = "9990";
				consumerAppPassword = "welcome123";
				consumerAppMobile = 8884292249l;
				restaurantId = "219";
				OMS_USERNAME = "murthy@swiggy.in";
				OMS_PASSWORD = "Murthy@123";
			} else if (RMSCommonHelper.getEnv().equalsIgnoreCase("stage3")) {
				VENDOR_PASSWORD = "El3ment@ry";
				VENDOR_USERNAME = "9990";
				consumerAppPassword = "test1234";
				consumerAppMobile = 7022026831l;
				restaurantId = "5271";
				OMS_USERNAME = "ramzi@swiggy.in";
				OMS_PASSWORD = "Test@1234";
			} else if (RMSCommonHelper.getEnv().equalsIgnoreCase("prod")) {
				VENDOR_PASSWORD = "El3ment@ry";
				VENDOR_USERNAME = "5271";
				consumerAppPassword = "test1234";
				consumerAppMobile = 9886379321l;
				restaurantId = "9990";
				OMS_USERNAME = "ramzi@swiggy.in";
				OMS_PASSWORD = "Test@1234";
			}
			omsSessionData = RMSCommonHelper.getOMSSessionData(OMS_USERNAME, OMS_PASSWORD);
			rmsSessionData = RMSCommonHelper.getRMSSessionData(VENDOR_USERNAME, VENDOR_PASSWORD);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@DataProvider(name = "OrderEditRequest")
	public Object[][] orderEditRequest() throws Exception {
		JsonHelper jsonHelper = new JsonHelper();
		long currentOrder = RMSHelper.getOrderIdMaxValue();
		Edit_data edit = new Edit_data();
		ArrayList<Oos_datum> oosDataList = new ArrayList<Oos_datum>();
		Oos_datum ood = new Oos_datum();
		ArrayList<Addon> addonList = new ArrayList<Addon>();
		Addon addon = new Addon();
		ArrayList<Long> choices = new ArrayList<Long>();
		choices.add(5953824l);
		choices.add(5953803l);
		addon.setGroup_id(900196);
		addon.setChoices(choices);
		addonList.add(addon);
		ArrayList<Variant> variantsList = new ArrayList<Variant>();
		Variant variants = new Variant();
		ArrayList<Long> variation = new ArrayList<Long>();
		variation.add(5953805l);
		variants.setGroup_id(2019724);
		variants.setVariation(variation);
		variantsList.add(variants);

		Item item = new Item();
		item.setItem_id(228299);
		item.setAddons(addonList);
		item.setVariants(variantsList);

		Oos_and_alternates oosAndAlternatives = new Oos_and_alternates();
		ArrayList<Oos_addon> oosAddons = new ArrayList<Oos_addon>();
		ArrayList<Oos_variant> oosVariants = new ArrayList<Oos_variant>();

		Oos_addon oosa = new Oos_addon();
		oosa.setOos_group_id(2019724);
		ArrayList<Long> oosChoices = new ArrayList<Long>();
		oosChoices.add(5253805l);
		oosa.setOos_choices(oosChoices);
		ArrayList<Long> alternateChoices = new ArrayList<Long>();
		alternateChoices.add(5953822l);
		alternateChoices.add(5953801l);
		oosa.setAlternate_choices(alternateChoices);
		oosAddons.add(oosa);

		Oos_variant oosv = new Oos_variant();
		ArrayList<Long> oos_variations = new ArrayList<Long>();
		oos_variations.add(4051356l);
		oos_variations.add(4051367l);
		oosv.setOos_group_id(481263);
		ArrayList<Long> alternate_variations = new ArrayList<Long>();
		alternate_variations.add(5953821l);
		alternate_variations.add(5953802l);
		oosv.setOos_variations(oos_variations);
		oosv.setOos_variations(alternate_variations);
		oosVariants.add(oosv);
		oosAndAlternatives.setOos_addons(oosAddons);
		oosAndAlternatives.setOos_variants(oosVariants);
		oosAndAlternatives.setAlternate_items(new ArrayList());
		oosAndAlternatives.setIs_item_oos(false);

		ood.setItem(item);
		ood.setOos_and_alternates(oosAndAlternatives);

		oosDataList.add(ood);
		edit.setOos_data(oosDataList);

		Meta_data metadata = new Meta_data();
		metadata.setRequested_by("FF");
		metadata.setRequested_timestamp("2018-10-01T07:13:44.721+0000");
		EditServiceEntry entry = new EditServiceBuilder().setOrderId(++currentOrder).setEditData(edit)
				.setEditReason("ITEM_OUT_OF_STOCK").setMetaData(metadata).build();
		EditServiceEntry entry2 = new EditServiceBuilder().setOrderId(0).setEditData(edit)
				.setEditReason("ITEM_OUT_OF_STOCK").setMetaData(metadata).build();
		EditServiceEntry entry3 = new EditServiceBuilder().setOrderId(-currentOrder).setEditData(edit)
				.setEditReason("ITEM_OUT_OF_STOCK").setMetaData(metadata).build();
		System.out.println(jsonHelper.getObjectToJSON(entry));
		return new Object[][] { { jsonHelper.getObjectToJSON(entry), 0, "request accepted" },
				{ jsonHelper.getObjectToJSON(entry), -2,
						"Duplicate edit request received for OrderId: " + currentOrder },
				{ jsonHelper.getObjectToJSON(entry2), -4, "request submission constraint validation failure" },
				{ jsonHelper.getObjectToJSON(entry3), -4, "request submission constraint validation failure" } };
	}

	@DataProvider(name = "orderEditRequestStatusById")
	public Object[][] orderEditRequestStatus() throws Exception {
		JsonHelper jsonHelper = new JsonHelper();
		long currentOrder = RMSHelper.getOrderIdMaxValue();
		Edit_data edit = new Edit_data();
		ArrayList<Oos_datum> oosDataList = new ArrayList<Oos_datum>();
		Oos_datum ood = new Oos_datum();
		ArrayList<Addon> addonList = new ArrayList<Addon>();
		Addon addon = new Addon();
		ArrayList<Long> choices = new ArrayList<Long>();
		choices.add(5953824l);
		choices.add(5953803l);
		addon.setGroup_id(900196);
		addon.setChoices(choices);
		addonList.add(addon);
		ArrayList<Variant> variantsList = new ArrayList<Variant>();
		Variant variants = new Variant();
		ArrayList<Long> variation = new ArrayList<Long>();
		variation.add(5953805l);
		variants.setGroup_id(2019724);
		variants.setVariation(variation);
		variantsList.add(variants);

		Item item = new Item();
		item.setItem_id(228299);
		item.setAddons(addonList);
		item.setVariants(variantsList);

		Oos_and_alternates oosAndAlternatives = new Oos_and_alternates();
		ArrayList<Oos_addon> oosAddons = new ArrayList<Oos_addon>();
		ArrayList<Oos_variant> oosVariants = new ArrayList<Oos_variant>();

		Oos_addon oosa = new Oos_addon();
		oosa.setOos_group_id(2019724);
		ArrayList<Long> oosChoices = new ArrayList<Long>();
		oosChoices.add(5253805l);
		oosa.setOos_choices(oosChoices);
		ArrayList<Long> alternateChoices = new ArrayList<Long>();
		alternateChoices.add(5953822l);
		alternateChoices.add(5953801l);
		oosa.setAlternate_choices(alternateChoices);
		oosAddons.add(oosa);

		Oos_variant oosv = new Oos_variant();
		ArrayList<Long> oos_variations = new ArrayList<Long>();
		oos_variations.add(4051356l);
		oos_variations.add(4051367l);
		oosv.setOos_group_id(481263);
		ArrayList<Long> alternate_variations = new ArrayList<Long>();
		alternate_variations.add(5953821l);
		alternate_variations.add(5953802l);
		oosv.setOos_variations(oos_variations);
		oosv.setOos_variations(alternate_variations);
		oosVariants.add(oosv);
		oosAndAlternatives.setOos_addons(oosAddons);
		oosAndAlternatives.setOos_variants(oosVariants);
		oosAndAlternatives.setAlternate_items(new ArrayList());
		oosAndAlternatives.setIs_item_oos(false);

		ood.setItem(item);
		ood.setOos_and_alternates(oosAndAlternatives);

		oosDataList.add(ood);
		edit.setOos_data(oosDataList);

		Meta_data metadata = new Meta_data();
		metadata.setRequested_by("FF");
		metadata.setRequested_timestamp("2018-10-01T07:13:44.721+0000");

		EditServiceEntry entry = new EditServiceBuilder().setOrderId(++currentOrder).setEditData(edit)
				.setEditReason("ITEM_OUT_OF_STOCK").setMetaData(metadata).build();
		System.out.println(jsonHelper.getObjectToJSON(entry));

		return new Object[][] { { jsonHelper.getObjectToJSON(entry), String.valueOf(currentOrder), "CONFIRMED" } };
	}

	@DataProvider(name = "OrderEditRequestStatus")
	public Object[][] orderEditRequestStatus_BKP() {

		return new Object[][] { { "104" } };
	}

	@SuppressWarnings("unchecked")
	@DataProvider(name = "CheckIfOrderEditable")
	public Object[][] orderEditableCheck() throws Exception {
		HashMap<String, Object> orderInfo = placeNormalOrder(restaurantId, consumerAppMobile, consumerAppPassword,
				RMSConstants.PAYMENTMODE_CASH, RMSConstants.TEST_ORDER_COMMENTS);
		return new Object[][] { { ((HashMap<String, String>) orderInfo.get("orderData")).get("orderId"), 0, "success" },
				{ "-121", -4, "request submission constraint validation failure" },
				{ "0", -3, "failure in rule execution" }};
	}
	
	@SuppressWarnings("unchecked")
	@DataProvider(name = "orderEditRequestStatusByIdWithInvalidData")
	public Object[][] orderEditRequestStatusByIdWithInvalidData() throws Exception {
		
		return new Object[][] { 
				{ "-121", -4, "request submission constraint validation failure" },
				{ "0", -5, "resource not found" },
				{ "234134234", -5, "resource not found" }};
	}

	public static HashMap<String, Object> placeNormalOrder(String restaurantId, long consumerAppMobile,
			String consumerAppPassword, String paymentMode, String comments) throws Exception {
		OMSHelper omsHelper = new OMSHelper();
		List<Cart> items = new ArrayList<>();
		Processor response = SnDHelper.menuV3RestId(restaurantId);
		ArrayList<Long> itemList = RMSCommonHelper.getInStockItemsFromMenu(response);
		String latLng = RMSCommonHelper.getRestLatLngFromMenu(response);
		Collections.shuffle(itemList);
		items.add(new Cart(null, null, String.valueOf(itemList.get(0)), 1));
		String orderReponse[] = RMSCommonHelper.createOrder(consumerAppMobile, consumerAppPassword, latLng, items,
				restaurantId, paymentMode, comments);
		if (RMSCommonHelper.isOrderAvailableAtFF(orderReponse[0])) {
			boolean flg = omsHelper.verifyOrder(orderReponse[0], "010", omsSessionData.get("X-CSRFToken"),
					omsSessionData.get("sessionId"));
		} else {
			throw new Exception("Order Not Reached FF...So DownStream Dependency Failed For FF");
		}
		return prepareOrderInfo(orderReponse, restaurantId);
	}

	private static HashMap<String, Object> prepareOrderInfo(String[] orderReponse, String restaurantId) {
		HashMap<String, Object> orderInfo = new HashMap<>();
		HashMap<String, Object> restaurantInfo = new HashMap<>();
		HashMap<String, Object> myFetchOrderData = new HashMap<>();
		orderInfo.put("orderReponse", orderReponse[1]);
		orderInfo.put("orderId", orderReponse[0]);
		restaurantInfo.put("restaurantId", restaurantId);
		myFetchOrderData.put("orderData", orderInfo);
		myFetchOrderData.put("restaurantData", restaurantInfo);
		myFetchOrderData.put("rmsSessionData", rmsSessionData);
		return myFetchOrderData;
	}

}
