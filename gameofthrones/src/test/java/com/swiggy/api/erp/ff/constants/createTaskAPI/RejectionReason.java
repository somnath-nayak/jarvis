package com.swiggy.api.erp.ff.constants.createTaskAPI;

public enum RejectionReason {

    ROLE_NOT_FOUND("No role for request type");

    private String description;

    RejectionReason(String description) {
        this.description = description;
    }

    public String toString() {
        return this.description;
    }
}
