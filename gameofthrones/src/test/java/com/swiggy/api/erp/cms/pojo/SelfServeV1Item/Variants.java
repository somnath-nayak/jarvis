package com.swiggy.api.erp.cms.pojo.SelfServeV1Item;

public class Variants {
	 private Variant_groups[] variant_groups;

	    private Exclude_list[][] exclude_list;

	    public Variant_groups[] getVariant_groups ()
	    {
	        return variant_groups;
	    }

	    public void setVariant_groups (Variant_groups[] variant_groups)
	    {
	        this.variant_groups = variant_groups;
	    }

	    public Exclude_list[][] getExclude_list ()
	    {
	        return exclude_list;
	    }

	    public void setExclude_list (Exclude_list[][] exclude_list)
	    {
	        this.exclude_list = exclude_list;
	    }

	    @Override
	    public String toString()
	    {
	        return "ClassPojo [variant_groups = "+variant_groups+", exclude_list = "+exclude_list+"]";
	    }
	}
				
				