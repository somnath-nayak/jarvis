package com.swiggy.api;

import com.rabbitmq.client.AMQP;
import com.swiggy.api.erp.ff.constants.OMSConstants;
import com.swiggy.api.erp.ff.helper.LOSHelper;
import com.swiggy.api.erp.ff.helper.OMSHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.DBHelper;
import framework.gameofthrones.Tyrion.ExcelHelper;
import framework.gameofthrones.Tyrion.RabbitMQHelper;
import framework.gameofthrones.Tyrion.RedisHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;
import framework.gameofthrones.joffrey.WebSocketSynchronousConnection;
import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.springframework.data.redis.core.RedisTemplate;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.ws.rs.core.Cookie;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;

public class TestClass {

    SqlTemplate sqlTemplate = null;

    @Test
    public void test1() throws Exception {
        WebSocketSynchronousConnection ws = new WebSocketSynchronousConnection();
        ws.start("http://13.229.112.83:8092/eventbus?oe_id=259");
        System.out.println("Message " + ws.getMessages());
    }

    @Test
    public void testGetMessageFromRabbit() {
        RabbitMQHelper helper = new RabbitMQHelper();
        String message = helper.getMessage("oms", "ff-rms-status-update-dead-letters");
        System.out.println("Message   " + message);
    }



    @Test
    public void testMysql() {
        List<Map<String, Object>> list = sqlTemplate.queryForList("select * from fgpair");
        System.out.println("Key  " + list.get(1).get("key"));
    }

    @Test(invocationCount = 10)
    public void testMysq1() throws InterruptedException {
        List<Map<String, Object>> list = SystemConfigProvider.getTemplate("cms").queryForList("select ag.id, ad.id, ad.name, ad.price from addon_groups ag inner join addons ad on ag.`id`= ad.addon_group_id where ag.item_id= 171092");
        List<Map<String, Object>> list1 = SystemConfigProvider.getTemplate("deliverydb").queryForList("select * from zone LIMIT 1");
        System.out.println("sasasasas "+ list1);
        Thread.sleep(2000);
    }

    @Test
    public void testPostgres() {
        DBHelper dbHelper = new DBHelper();
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate("oms");
        List<Map<String, Object>> list = sqlTemplate.queryForList("select type from oms_order where id=1");
        System.out.println("Key  " + list.get(0).get("type"));
    }


    @Test
    public void testRedis() {
        RedisHelper redisHelper = new RedisHelper();
        RedisTemplate sqlTemplate = redisHelper.getRedisTemplate("storefrontredis", 1);
        Object o = sqlTemplate.opsForValue().get("DE_OTP_216");
        System.out.println("Key" + o);


    }

    @Test
    public void testCookies() {
        Initialize init = new Initialize();
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("X-CSRFToken", "iG3Y73V9pab4v79Vx6KBVEt8xFbEX9Jk");
        headers.put("Set-Cookie", "sessionid=7mry3hbf7qylirije9nxvyvan5hyavqd; Path=/; Domain=oms-u-cuat-01.swiggyops.de");
        headers.put("Set-Cookie", "csrftoken=iG3Y73V9pab4v79Vx6KBVEt8xFbEX9Jk; Path=/; Domain=oms-u-cuat-01.swiggyops.de");


        List<Cookie> cookies = new ArrayList<>();
        cookies.add(new Cookie("sessionid", "7mry3hbf7qylirije9nxvyvan5hyavqd"));
        cookies.add(new Cookie("csrftoken", "iG3Y73V9pab4v79Vx6KBVEt8xFbEX9Jk"));


        GameOfThronesService gameOfThronesService = new GameOfThronesService("oms", "cancelorder", init);
        Processor processor = new Processor(headers, new String[]{"31324897", "1501658785"}, null, null, gameOfThronesService);
        System.out.println(processor.ResponseValidator.GetResponseCode());
    }


    @Test
    public void testCookie() throws Exception {
        OMSHelper omsHelper = new OMSHelper();
//        String orderID = "" + omsHelper.getMeAnOrder("manual");
//        String payload = "{\"order_id\": \"" + orderID + "\", \"status\": \"placed\", \"time\": 1501658785}";
        HashMap<String, String> session = omsHelper.getSession(OMSConstants.USERNAME, OMSConstants.PASSSWORD);
//        StringEntity se = new StringEntity(payload);
        HttpPost httppost = new HttpPost("http://oms-u-cuat-01.swiggyops.de/update_status/");
        httppost.addHeader("Content-Type", "application/json");
        httppost.addHeader("X-CSRFToken", session.get("csrfToken"));

//        httppost.setEntity(se);
        CookieStore cookieStore = new BasicCookieStore();
        BasicClientCookie cookie = new BasicClientCookie("sessionid", session.get("sessionId"));
        cookie.setDomain("oms-u-cuat-01.swiggyops.de");
        cookie.setPath("/");
        BasicClientCookie cookie2 = new BasicClientCookie("csrftoken", session.get("csrfToken"));
        cookie2.setDomain("oms-u-cuat-01.swiggyops.de");
        cookie2.setPath("/");

        cookieStore.addCookie(cookie);
        cookieStore.addCookie(cookie2);
        HttpClient client = HttpClientBuilder.create().setDefaultCookieStore(cookieStore).build();
        HttpResponse response = client.execute(httppost);

        System.out.println(response.getStatusLine());
        System.out.println(response.getEntity().getContent());
    }


    @Test()
    public void getSession() throws Exception {

        OMSHelper omsHelper = new OMSHelper();
        LOSHelper losHelper = new LOSHelper();

        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat order_idFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        String order_id = order_idFormat.format(date);
        String order_time = dateFormat.format(date);
        String epochTime = String.valueOf(Instant.now().getEpochSecond());

        String orderID = "3456760";//""+ losHelper.createManualOrder(order_id, order_time, epochTime);
        System.out.println(orderID);

        HashMap<String, String> session = omsHelper.getSession(OMSConstants.USERNAME, OMSConstants.PASSSWORD);
        omsHelper.verifyOrder(orderID, "001", session.get("sessionId"), session.get("sessionId"));
        omsHelper.changeOrderStatusToPlaced(orderID);
    }


    @Test()
    public void insert() throws Exception {
        DBHelper dbHelper = new DBHelper();
        int number = 1000001053;
        List<String> sql = new ArrayList<>();
        for (int i = 0; i < 7; i++) {
            sql.add(" INSERT INTO `wp_users` (`user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`, `mobile`, `referral_code`, `referred_by`, `swiggy_money`, `referral_used`, `device_id`, `device_type`, `app_version`, `user_agent`, `os_version`, `is_verified`, `subscribed_to_sms`, `subscribed_to_email`, `disabled_reason`, `modified_by`, `modified_at`, `modified_time`, `has_wallet`, `email_verified`, `google_ad_id`, `test_add_column`, `paytmSsoToken`, `last_order_city`, `mobikwikSsoToken`, `freechargeSsoToken`, `email_varification_flag`, `cancellation_fee`)\n" +
                    "VALUES ('nethravathi.tc@swiggy.com', '$P$BrhY/DxyjUwYjc4GM0Yg6jf3wNNObB0', '', 'nethravathi.tc@swiggy.com', '', '2017-07-11 10:17:10', '', 1, 'Nethra Test', '" + number + "', 'W16AY0', NULL, 220, 0, '25587e1a-47e3-471d-963c-4bd85566befc', NULL, NULL, 'web', NULL, 1, 1, 1, '', '', NULL, NULL, 0, 0, '3e96a442-43d0-4484-8f82-abd2395d7cde', 0, NULL, 0, NULL, NULL, 0, 0);");
            number++;
        }
        String[] abc = sql.toArray(new String[sql.size()]);
        dbHelper.getMySqlTemplate("cms").executeBatch(abc);
    }



    @Test()
    public void queryObject() throws Exception {
        String orderCount = "select order_count as count from delivery.batch where id=?";
        DBHelper dbHelper = new DBHelper();
        Integer abc = (Integer) dbHelper.getMySqlTemplate("deliverydb").queryForObject(orderCount, "15681", Integer.class);
        System.out.println("Value    "+abc);
    }



    @Test(invocationCount = 1)
    public void PushOrderMessage() throws Exception {

        RabbitMQHelper helper = new RabbitMQHelper();


        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat order_idFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        String order_id = order_idFormat.format(date);
        String order_time = dateFormat.format(date);
        //String epochTime = String.valueOf(Instant.now().getEpochSecond());
        String orderMessage = "{\"payment_method\":\"Ok its cash Cash\",\"GST_on_commission\":{\"SGST\":0.92,\"CGST\":0.92,\"IGST\":0.92},\"restaurant_id\":\"10820\",\"swiggy_discount_hit\":0,\"customer_ip\":\"10.0.15.223\",\"billing_lng\":\"77.603578\",\"payment_txn_id\":\"\",\"cancellation_fee_collected\":0,\"post_status\":\"processing\",\"pg_response_time\":\""+order_time+"\",\"restaurant_city_name\":\"Bangalore\",\"coupon_applied\":\"\",\"pay_by_system_value\":false,\"with_de\":true,\"order_notes\":\"\",\"order_time\":\""+order_time+"\",\"cust_lat_lng\":{\"lat\":\"12.912287\",\"lng\":\"77.629782\"},\"de_pickedup_refund\":0,\"restaurant_coverage_area\":\"jayanagar\",\"cancellation_order_summary_link\":\"/invoice/download?token=64900184_null\",\"order_incoming\":\"1\",\"restaurant_type\":\"F\",\"commission_on_full_bill\":true,\"swuid\":\"44accebe-d38e-47bb-8a5e-cd7468af7be6\",\"restaurant_area_name\":\"Jayanagar\",\"payment_confirmation_channel\":\"api\",\"agreement_type\":\"1\",\"partner_id\":\"\",\"free_shipping\":\"0\",\"restaurant_new_slug\":\"shanmukh-9th-block-jayanagar\",\"distance_calc_method\":\"GDMA Cache\",\"customer_id\":\"705262\",\"delivered_time_in_seconds\":\"0\",\"restaurant_taxation_type\":\"VAT\",\"is_assured\":0,\"order_id\":" + order_id +",\"has_rating\":\"0\",\"order_total\":246,\"type_of_partner\":0,\"order_discount\":0,\"TCS_on_bill\":{\"STCS\":5.58,\"CTCS\":5.58,\"ITCS\":5.58},\"key\":\"OBPH62\",\"delivery_address\":{\"address_line1\":\"Test IBC Knowledge Park Bhavani Nagar Suddagunte Palya Bengaluru Karnataka 560029 India\",\"name\":\"Shashank\",\"area\":\"Suddagunte Palya\",\"mobile\":\"8892028093\",\"reverse_geo_code_failed\":false,\"email\":\"shashank.shekhar@swiggy.in\",\"lat\":\"12.932559\",\"flat_no\":\"Test\",\"address\":\"Test IBC Knowledge Park Bhavani Nagar Suddagunte Palya Bengaluru Karnataka 560029 India\",\"landmark\":\"IBC\",\"lng\":\"77.603578\",\"id\":\"781738\"},\"original_order_total\":246,\"is_partner_enable\":false,\"is_refund_initiated\":0,\"post_name\":\"\",\"is_first_order_delivered\":true,\"tax_expressions\":{\"Service Charges\":\"[CART_SUBTOTAL]*0\",\"Service Tax\":\"[CART_SUBTOTAL]*0.058\",\"Vat\":\"[CART_SUBTOTAL]*0\",\"service_charge_GST_inclusive\":false,\"packaging_GST_inclusive\":false,\"item_GST_inclusive\":false},\"prep_time\":\"22\",\"is_replicated\":false,\"restaurant_phone_numbers\":\"08041735566,08041745566\",\"order_placement_status\":\"0\",\"restaurant_commission_exp\":\"[bill_without_taxes]*0.131004366812227\",\"order_spending\":\"0\",\"cart_id\":1433,\"customer_user_agent\":\"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.10 Safari/537.36\",\"cancellation_fee_collected_total\":0,\"payment_txn_status\":\"success\",\"commission\":\"23.03\",\"rendering_details\":[{\"display_text\":\"Bill Total\",\"intermediateText\":\"\",\"is_negative\":0,\"info_text\":\"\",\"hierarchy\":1,\"value\":\"186.21\",\"is_collapsible\":1,\"currency\":\"rupees\",\"meta\":null,\"sub_details\":[{\"display_text\":\"Item Total\",\"is_negative\":0,\"info_text\":null,\"hierarchy\":1,\"value\":\"\",\"is_collapsible\":0,\"currency\":\"rupees\",\"meta\":null,\"key\":\"item_total\",\"type\":\"display\",\"visibilityExpression\":\"true\"},{\"display_text\":\"Packing Charges\",\"is_negative\":0,\"info_text\":\"\",\"hierarchy\":1,\"value\":\"7.00\",\"is_collapsible\":0,\"currency\":\"rupees\",\"meta\":null,\"key\":\"packing_charges\",\"type\":\"display\",\"visibilityExpression\":\"true\"},{\"display_text\":\"VAT and Service Tax\",\"is_negative\":0,\"info_text\":\"\",\"hierarchy\":1,\"value\":\"10.21\",\"is_collapsible\":0,\"currency\":\"rupees\",\"meta\":null,\"key\":\"vat_and_service_tax\",\"type\":\"display\",\"visibilityExpression\":\"true\"},{\"display_text\":\"GST\",\"is_negative\":0,\"info_text\":null,\"hierarchy\":1,\"value\":\"0.00\",\"is_collapsible\":0,\"currency\":\"rupees\",\"meta\":null,\"key\":\"GST\",\"type\":\"display\",\"visibilityExpression\":\"true\"}],\"key\":\"bill_total\",\"type\":\"display\",\"visibilityExpression\":\"true\"},{\"display_text\":\"Line Separator\",\"intermediateText\":\"\",\"is_negative\":0,\"info_text\":null,\"hierarchy\":1,\"value\":\"\",\"is_collapsible\":0,\"currency\":\"rupees\",\"meta\":null,\"key\":\"line_separator\",\"type\":\"separator\",\"visibilityExpression\":\"true\"},{\"display_text\":\"Coupon Discount\",\"intermediateText\":\"\",\"is_negative\":1,\"info_text\":null,\"hierarchy\":1,\"value\":\"Apply\",\"is_collapsible\":0,\"currency\":\"rupees\",\"meta\":null,\"key\":\"apply_coupon\",\"type\":\"button\",\"visibilityExpression\":\"#is_coupon_valid == 0\"},{\"display_text\":\"Delivery Charges\",\"intermediateText\":\"\",\"is_negative\":0,\"info_text\":\"\",\"hierarchy\":1,\"value\":\"40.00\",\"is_collapsible\":1,\"currency\":\"rupees\",\"meta\":null,\"key\":\"delivery_charges\",\"type\":\"display\",\"visibilityExpression\":\"(#delivery_charges > #threshold_fee && #delivery_charges > #distance_fee && #delivery_charges > #time_fee) || #delivery_charges == 0\"},{\"display_text\":\"Surge Fee\",\"intermediateText\":\"\",\"is_negative\":0,\"info_text\":\"testing_Due to operational reasons an additional convenience fee is applicable on this order, to be passed on to our delivery executives. Thank you for understanding._Testing\",\"hierarchy\":1,\"value\":\"20.00\",\"is_collapsible\":0,\"currency\":\"rupees\",\"meta\":null,\"key\":\"surge_fee\",\"type\":\"display\",\"visibilityExpression\":\"true\"},{\"display_text\":\"Instruction Text\",\"intermediateText\":\"\",\"is_negative\":0,\"info_text\":null,\"hierarchy\":1,\"value\":\"\",\"is_collapsible\":0,\"currency\":\"rupees\",\"meta\":null,\"key\":\"instruction_text\",\"type\":\"free_text_input\",\"visibilityExpression\":\"true\"},{\"display_text\":\"Line Separator\",\"intermediateText\":\"\",\"is_negative\":0,\"info_text\":null,\"hierarchy\":1,\"value\":\"\",\"is_collapsible\":0,\"currency\":\"rupees\",\"meta\":null,\"key\":\"line_separator\",\"type\":\"separator\",\"visibilityExpression\":\"true\"},{\"display_text\":\"To Pay\",\"intermediateText\":\"\",\"is_negative\":0,\"info_text\":\"\",\"hierarchy\":1,\"value\":\"246.00\",\"is_collapsible\":0,\"currency\":\"rupees\",\"meta\":{\"align\":\"right\",\"bold\":true},\"key\":\"grand_total\",\"type\":\"display\",\"visibilityExpression\":\"true\"}],\"order_status\":\"processing\",\"restaurant_customer_distance\":\"3.6\",\"billing_lat\":\"12.932559\",\"coupon_discount\":0,\"order_payment_method\":\"Cash\",\"restaurant_address\":\"No 1313, 25th Main Road, Jayanagara 9th Block, Jayanagar\",\"convenience_fee\":\"20\",\"order_delivery_charge\":40,\"listing_version_shown\":\"2-default\",\"TCS_on_bill_expressions\":{\"STCS\":\"0.03\",\"CTCS\":\"0.03\",\"ITCS\":\"0.03\"},\"order_restaurant_bill\":\"610\",\"restaurant_email\":\"sawan.choubisa@swiggy.in\",\"sla_time\":\"40\",\"order_delivery_status\":\"\",\"converted_to_cod\":false,\"sla_difference\":\"0\",\"GST_on_commission_expressions\":{\"SGST\":\"0.04\",\"CGST\":\"0.04\",\"IGST\":\"0.04\"},\"delayed_placing\":1,\"restaurant_discount_hit\":0,\"restaurant_has_inventory\":\"0\",\"restaurant_mobile\":\"\",\"is_assured_restaurant\":false,\"sla\":\"40\",\"last_failed_order_id\":0,\"nodal_spending\":\"143.47\",\"post_type\":\"\",\"restaurant_name\":\"Shanmukha\",\"delivery_time_in_seconds\":\"0\",\"restaurant_payment_mode\":{\"agreement_type_string\":\"Postpaid\",\"agreement_type\":\"1\"},\"actual_sla_time\":\"0\",\"order_type\":\"regular\",\"on_time\":false,\"restaurant_lat_lng\":\"12.926491,77.61963300000002\",\"order_tax\":10.208,\"menu_shown\":{\"variant_id\":1},\"sid\":\"4df319d7-d598-4c97-8b37-bd37b4a210d4\",\"tid\":\"b8c7128e-a5bf-4d06-851c-b3c58ed02828\",\"billing_address_id\":\"781738\",\"restaurant_city_code\":\"1\",\"overbooking\":\"0\",\"ordered_time_in_seconds\":1511332731,\"restaurant_cover_image\":\"exojagrnia7n8s3fryvj\",\"order_items\":[{\"packing_charges\":\"7\",\"total\":\"176\",\"global_sub_category\":\"sample\",\"item_charges\":{\"Service Charges\":\"0\",\"GST\":\"0\",\"Vat\":\"0\",\"Service Tax\":\"0\"},\"item_swiggy_discount_hit\":0,\"name\":\"Corn Chilli\",\"global_main_category\":\"sample main category\",\"category_details\":{\"category\":\"Starters\",\"sub_category\":\"Chilli & Manchurian\"},\"is_veg\":\"1\",\"image_id\":\"tlcyf50ehkzouv9hj5qj\",\"item_tax_expressions\":{\"GST_inclusive\":false,\"Service Charges\":\"0\",\"Vat\":\"0\",\"Service Tax\":\"0\"},\"item_id\":\"79427\",\"variants\":[],\"addons\":[],\"subtotal\":\"169\",\"quantity\":\"1\",\"item_restaurant_discount_hit\":0}],\"free_gifts\":[],\"is_select\":false,\"is_ivr_enabled\":\"0\",\"restaurant_area_code\":\"1\",\"swiggy_money\":0,\"payment\":\"successful\",\"device_id\":\"44accebe-d38e-47bb-8a5e-cd7468af7be6\",\"restaurant_locality\":\"9th Block\",\"configurations\":{\"self_delivery\":false},\"charges\":{\"GST\":\"0\",\"Delivery Charges\":\"40\",\"Service Charges\":\"0\",\"Service Tax\":\"10.21\",\"Packing Charges\":\"7\",\"Convenience Fee\":\"20\",\"Cancellation Fee\":\"0\",\"Vat\":\"0\"},\"cancellation_fee_applied\":0,\"coupon_code\":\"\",\"service_tax_on_commission\":\"0\"}";
        helper.pushMessage("delivery", "dc.swiggy_orders", new AMQP.BasicProperties().builder().contentType("application/json"), orderMessage);

    }

    @Test
    public void readExcel() throws Exception {
        ExcelHelper excelHelper = new ExcelHelper();
        excelHelper.getExcelList("/Users/abhijit.p/Downloads/priceparity.xlsx", "Menu_Item_Mapping");

    }


    @Test
    public void getRedisLatLong() throws Exception {
        DBHelper dbHelper = new DBHelper();
        dbHelper.getMySqlTemplate("delivery").queryForList("");
    }

}
