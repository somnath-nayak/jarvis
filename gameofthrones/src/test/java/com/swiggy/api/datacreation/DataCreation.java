package com.swiggy.api.datacreation;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.swiggy.api.erp.cms.constants.CmsConstants;
import com.swiggy.api.erp.cms.helper.AreaHelper;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import com.swiggy.api.erp.cms.helper.CityHelper;
import com.swiggy.api.erp.cms.helper.RestaurantHelper;
import com.swiggy.api.erp.cms.pojo.AddItemToRest.AddItemToRestaurant;
import com.swiggy.api.erp.cms.pojo.Area;
import com.swiggy.api.erp.cms.pojo.Category;
import com.swiggy.api.erp.cms.pojo.City;
import com.swiggy.api.erp.cms.pojo.Restaurant;
import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.erp.delivery.helper.SITDataHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import com.swiggy.api.sf.snd.helper.SnDHelper;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.simple.JSONObject;
import org.testng.Assert;

import java.io.IOException;
import java.math.BigInteger;
import java.time.Instant;
import java.util.*;

/**
 * Created by kiran.j on 4/4/18.
 */
public class DataCreation {

    public static final Logger log = Logger.getLogger(DataCreation.class);
    public static HashMap<String, Object> map = new HashMap<String, Object>();
    public ObjectMapper om = new ObjectMapper();
    SITDataHelper sitDataHelper = new SITDataHelper();
    RestaurantHelper restaurantHelper = new RestaurantHelper();
    CMSHelper cmsHelper = new CMSHelper();
    CityHelper cityHelper = new CityHelper();
    AreaHelper areaHelper = new AreaHelper();
    SANDHelper sandHelper = new SANDHelper();
    JsonHelper jsonHelper = new JsonHelper();
    CheckoutHelper checkoutHelper = new CheckoutHelper();

    public static HashMap<String, Object> getMap() {
        return map;
    }

    public static void setMap(HashMap<String, Object> map) {
        DataCreation.map = map;
    }

    public HashMap<String, Object> unwrapJson(String json) {
        //HashMap<String, Object> map = new HashMap<>();
        try {
            om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            JsonNode jsonNode = om.readValue(json, JsonNode.class);
            Iterator<JsonNode> itr = jsonNode.iterator();
            while(itr.hasNext()) {
                JsonNode temp = itr.next();
                map.put(temp.get("pod_name").toString().replace("\"", ""), temp.get("data"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }

    public String[] getFieldsFromJson(String json) {
        String[] fields = new String[1000];
        int k = 0;
        try{
            JsonNode jsonNode = om.readValue(json, JsonNode.class);
            Iterator<Map.Entry<String, JsonNode>> itr = jsonNode.fields();
            while(itr.hasNext()) {
                fields[k++] = itr.next().toString();
            }
        } catch (Exception  e) {
            e.printStackTrace();
        }
        return Arrays.copyOfRange(fields, 0, k);
    }

    public JsonNode getJson(Object json) throws IOException {
        return (JsonNode) json;
    }


    public int createDataInOrder() throws IOException, JSONException {
        JsonNode city_json, city_polygon = null;
        int rest_id = 0;
        om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        HashMap<String, Object> map_data = getMap();
        init();

        /*City Creation */
        city_json = (JsonNode)((map_data.get("cms")));
        city_json = city_json.get("city");
        Iterator itr = city_json.iterator();
        while(itr.hasNext()) {
            JsonNode json = (JsonNode) itr.next();
            int city_id = createCity(json);
            JsonNode area_json = json.get("area");
            Iterator itr2 = area_json.iterator();
            while(itr2.hasNext()) {
                JsonNode json2 = (JsonNode) itr2.next();
                int area_id = createArea(json2, city_id);
                rest_id = createRestaurant(json2.get("restaurant"), city_id, area_id);
            }
        }
        return rest_id;
    }

    public int createCity(JsonNode json)  {
        int city_id = 0;
        try {
            City city = om.treeToValue(json, City.class);
            if(cmsHelper.isCityPresent(city.getName()))
                city_id = cmsHelper.getCityId(city.getName());
            else {
                city_id = cityHelper.createCity(city);
                sitDataHelper.updateRainParamsForCity(city_id);
                createCityPolygon(json, city_id);
            }
        } catch (Exception e) {
            Assert.assertTrue(false, "Unable to Create City "+e);
        }
        return city_id;
    }

    public int createCityPolygon(JsonNode json, int city_id) {
        int polygon_id = 0;
        try {
            System.out.println("city id is"+city_id);
            JsonNode city_polygon = json.get("city_polygon");
            sandHelper.cityPolygon(city_polygon.get("name").toString().replace("\"",""), city_polygon.get("tag").toString().replace("\"",""), city_polygon.get("lat_lng").toString().replace("\"",""));
            Thread.sleep(5000);
            polygon_id = (int)cmsHelper.getPolygonIdFromName(city_polygon.get("name").toString());
            System.out.println("Polygon id is"+polygon_id);
            sandHelper.mapCityPolygonWithCity(Integer.toString(city_id),Integer.toString(polygon_id));
        } catch (Exception e) {
            Assert.assertTrue(false, "Unable to Create City-Polygon "+e);
        }
        return polygon_id;
    }

    public boolean createZonePolygon(JsonNode json, int area_id) {
        int zone_id = 0;
        try {
            CMSHelper cmsHelper = new CMSHelper();
            zone_id = getZoneId(area_id);
            //zone_id = 322;
            sitDataHelper.addPolygonZone(addPolygonZone(Integer.toString(zone_id), json.get("lat_long").toString().replace("\"", ""),
                    json.get("ops").get("userId").toString().replace("\"", ""), json.get("ops").get("email").toString().replace("\"", "")));
            sitDataHelper.updateZoneParams(zone_id);
        } catch (Exception e) {
            Assert.assertTrue(false, "Unable to Create Zone Polygon "+e);
        }
        return sitDataHelper.updateZoneParams(zone_id);
    }

    public int createArea(JsonNode json, int city_id) {
        int area_id = 0;
        try {
            om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            Iterator itr = json.iterator();
            Area area = om.treeToValue(json, Area.class);
            System.out.println("restaurant is" + jsonHelper.getObjectToJSON(area));
                if(cmsHelper.isAreaPresent(area.getName()))
                    area_id = cmsHelper.getAreaId(area.getName());
                else {
                    area.setCity(Integer.toString(city_id));
                    area.setCity_id(city_id);
                    area_id = areaHelper.createArea(area);
                    //area_id = 26;
                    createZonePolygon(json.get("zone"), area_id);
            }
        } catch (Exception e) {
            Assert.assertTrue(false, "Unable to Create Area "+e);
        }
        return area_id;
    }

    public int createRestaurant(JsonNode json, int city_id, int area_id) {
        int rest_id = 0;
        try {
            Iterator itr = json.iterator();
            System.out.println("restaurant is" + json);
            while (itr.hasNext()) {
                JsonNode rest_json = (JsonNode) itr.next();
                om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                Restaurant restaurant = om.treeToValue(rest_json, Restaurant.class);
                if (cmsHelper.getRestaurantIdByNameExact(restaurant.getName()).size() > 0) {
                    BigInteger rest = ((BigInteger) cmsHelper.getRestaurantIdByNameExact(restaurant.getName()).get(0).get("id"));
                    rest_id = rest.intValue();
                }
                else {
                    restaurant.setArea(Integer.toString(area_id));
                    restaurant.setCity(Integer.toString(city_id));
                    Processor processor = restaurantHelper.createRestaurant(restaurant);
                    rest_id = processor.ResponseValidator.GetNodeValueAsInt("swiggy_id");
                    restaurantHelper.enableRestaurant(rest_id, rest_json.get("lat_long").toString().replace("\"",""));
                }
                    createCategory(rest_json.get("category"), rest_id);
            }
        } catch (Exception e) {
            Assert.assertTrue(false, "Unable to Create Restaurant "+e);
        }
        return rest_id;
    }

    public void createCategory(JsonNode json, int rest_id) {
        try{
            Thread.sleep(10000);
            Iterator itr = json.iterator();
            while (itr.hasNext()) {
                JsonNode cat_json = (JsonNode) itr.next();
                int cat_id = 0;
                om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                Category category = om.treeToValue(cat_json, Category.class);
                cat_id = cmsHelper.getCategoryIdByName(Integer.toString(rest_id), category.getName());
                if(cat_id <= 0) {
                    cat_id = restaurantHelper.createCat(category, rest_id);
                    Assert.assertTrue(cat_id > 0, "Category Creation Failed");
                }
                createSubcat(cat_json.get("subcategory"), rest_id, cat_id);
            }
        } catch (Exception e) {
            Assert.assertTrue(false, "Unable to Create Category "+e);
        }
    }

    public void createSubcat(JsonNode json, int rest_id, int cat_id) {
        try{
            Thread.sleep(5000);
            Iterator itr = json.iterator();
            while (itr.hasNext()) {
                JsonNode cat_json = (JsonNode) itr.next();
                om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                Category subCategory = om.treeToValue(cat_json, Category.class);
                int subcat_id = 0;
                subcat_id = cmsHelper.getSubCategoryIdByName(Integer.toString(rest_id), Integer.toString(cat_id), subCategory.getName());
                if(subcat_id <= 0) {
                    subcat_id = restaurantHelper.createsubcat(subCategory, rest_id, cat_id);
                    Assert.assertTrue(subcat_id > 0, "Sub-Category Creation Failed");
                }
                createItem(cat_json.get("item"), rest_id, cat_id, subcat_id);
            }
        } catch (Exception e) {
            Assert.assertTrue(false, "Unable to Create Sub-Category "+e);
        }
    }

    public void createItem(JsonNode json, int rest_id, int cat_id, int subcat_id) {
        try{
            Thread.sleep(5000);
            Iterator itr = json.iterator();
            while (itr.hasNext()) {
                JsonNode cat_json = (JsonNode) itr.next();
                om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                AddItemToRestaurant itemToRestaurant = om.treeToValue(cat_json, AddItemToRestaurant.class);
                int item_id = 0;
                item_id = cmsHelper.getItemIdByName(Integer.toString(rest_id), itemToRestaurant.getName());
                if(item_id <= 0)
                    item_id = Integer.parseInt(restaurantHelper.createItem(itemToRestaurant, rest_id, cat_id, subcat_id));
            }
        } catch (Exception e) {
            Assert.assertTrue(false, "Unable to Create Item "+e);
        }
    }

    public  String addPolygonZone(String zone_id, String lat_long, String user_id, String email) throws JSONException
    {
        JSONObject zonePolygon= new JSONObject();
        zonePolygon.put("zoneId",zone_id);
        zonePolygon.put("path",lat_long);
        JSONObject ops= new JSONObject();
        ops.put("userId",user_id);
        ops.put("email",email);
        zonePolygon.put("ops",ops);
        System.out.println(zonePolygon.toString());
        return zonePolygon.toString();
    }

    public int getZoneId(int area_id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName);
        List<Map<String, Object>> list = sqlTemplate.queryForList(DeliveryConstant.zone_query+ area_id);
        return (list.size() > 0) ? (Integer)(list.get(0).get("zone_id")) : 0;
    }

    public void createUserAndLogin(String name, String mobile, String email, String password, String item_id, String quantity, String rest_id ) throws Exception {
        Processor processor = SnDHelper.consumerSignUp(name, mobile, email, password);
        System.out.println(processor.ResponseValidator.GetBodyAsText());
        String tid = String.valueOf(processor.ResponseValidator.GetNodeValue("$.tid"));
        System.out.println("tid for signup is   " + tid);
        String otp = SnDHelper.getOTP(tid);
        SnDHelper.verifyOTP(otp, tid);
        SnDHelper.consumerLogin(mobile, password);
        Processor processor2 = checkoutHelper.placeOrder(mobile, password, item_id, quantity, rest_id);
        System.out.println(processor2);
    }

    public void addAddress(String rest_id, String mobile, String password) {
        HashMap<String, String> address_map = new HashMap<String, String>();
        String epochTime = String.valueOf(Instant.now().getEpochSecond());
        address_map.put("name", "test automation-"+epochTime);
        address_map.put("mobile","8618869769");
        address_map.put("address","199, Near Sai Baba Temple");
        address_map.put("landmark","Swiggy office Land Mark");
        address_map.put("area","Swiggy Test");
        address_map.put("flat_no","4321");
        address_map.put("city","Bangalore");
        address_map.put("annotation","HOME");
        String lats[] = cmsHelper.getRestaurantDetails(rest_id).get("lat_long").toString().split(",");
        address_map.put("lat", lats[0]);
        address_map.put("lng", lats[1]);
        Processor loginResponse = SnDHelper.consumerLogin(mobile, password);
        String tid = loginResponse.ResponseValidator.GetNodeValue("tid");
        String token = loginResponse.ResponseValidator.GetNodeValue("data.token");
        CheckoutHelper.addNewAddress(address_map, tid, token);
    }

    public void init() {
        if(!cmsHelper.isUserPresent(DataCreationConstants.sit_user)) {
            SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
            sqlTemplate.execute(DataCreationConstants.wp_user);
        }
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName);
        sqlTemplate.execute(DataCreationConstants.zone_alter);
        if(sqlTemplate.queryForList(DataCreationConstants.event_log_type).size() == 0) {
            sqlTemplate.execute(DataCreationConstants.insert_event);
        }
    }

}
