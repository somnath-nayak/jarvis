package com.swiggy.api.datacreation;

/**
 * Created by kiran.j on 5/23/18.
 */
public interface DataCreationConstants {

    String wp_user = "INSERT INTO `wp_users` (`user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`, `mobile`, `referral_code`, `referred_by`, `swiggy_money`, `referral_used`, `device_id`, `device_type`, `app_version`, `user_agent`, `os_version`, `is_verified`, `subscribed_to_sms`, `subscribed_to_email`, `disabled_reason`, `modified_by`, `modified_time`, `has_wallet`, `email_verified`, `google_ad_id`, `paytmSsoToken`, `last_order_city`, `mobikwikSsoToken`, `freechargeSsoToken`, `email_varification_flag`, `cancellation_fee`)\n" +
            "VALUES ('1000000003@swiggy360.com', '$P$BrhY/DxyjUwYjc4GM0Yg6jf3wNNObB0', '', '1000000003@swiggy360.com', '', '2017-07-11 10:17:10', '', 1, 'GOT Drogon', '1000000003', 'W16AY0', NULL, 220, 0, '25587e1a-47e3-471d-963c-4bd85566befc', NULL, NULL, 'web', NULL, 1, 1, 1, '', '', NULL, 0, 0, '3e96a442-43d0-4484-8f82-abd2395d7cde', NULL, 0, NULL, NULL, 0, 0);";
    String sit_user = "1000000003";
    String event_log_type = "select * from event_audit_log_type";
    String insert_event = "INSERT INTO `event_audit_log_type` (`id`, `description`)\n" +
            "VALUES\n" +
            "\t(1,'Created new zone'),\n" +
            "\t(2,'Adding areas to zone'),\n" +
            "\t(3,'Deleting a Zone'),\n" +
            "\t(4,'Cash limit for zone'),\n" +
            "\t(5,'Polygon for Zone');";
    String zone_alter = "alter table zone modify column distance_ticker_weights varchar(100) DEFAULT NULL";
}
