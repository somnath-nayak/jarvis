package com.swiggy.healthcheck;

import junit.framework.Assert;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.sun.jersey.api.client.ClientResponse;
import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.utils.STAFAgent;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;
import com.swiggy.services.client.SwiggyClient;

public class SwiggyServerHealthChecker extends RestTestHelper {

	private static final int HTTP_STATUS_CODE_SUCCESS = 200;
	private static final int HTTP_STATUS_CODE_UNAUTH = 401;

	@BeforeClass
	public void setUp() {
		System.out
				.println("!!!!!!!!!!!!!!!SWIGGY SERVERS HEALTH CHECKUP[UP/DOWN] COMPLETED!!!!!!!!!!!!!!");
	}

	@AfterClass
	public void tearDown() {
		System.out
				.println("!!!!!!!!!!!!!!!SWIGGY SERVERS HEALTH CHECKUP[UP/DOWN] COMPLETED!!!!!!!!!!!!!!");
	}

	@Test
	public void testCheckIfAPIServerUp() throws SwiggyAPIAutomationException {

		try {
			SwiggyClient a = new SwiggyClient(false, false, false);
			ClientResponse resp = a.sendRequest(
					STAFAgent.getSTAFEndPoint("backend_host_healthcheck"), false);
			System.out.println("****************************************");
			System.out.println("STATUS: testCheckIfAPIServerUp :\n" + resp);
			System.out
					.println("############################################################################");
			if (resp.getStatus() == HTTP_STATUS_CODE_SUCCESS) {
				Assert.assertTrue(
						STAFAgent.getSTAFEndPoint("backend_host_healthcheck")
								+ ": API Server  is Up", true);
			} else {
				Assert.assertTrue(
						STAFAgent.getSTAFEndPoint("backend_host_healthcheck")
								+ ": API Server  is Down Now", false);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(STAFAgent.getSTAFEndPoint("backend_host_healthcheck")
					+ ": API Server Not accessible!!!!..Please Check");
		}

	}

	@Test
	public void testCheckIfDEServerUp() throws SwiggyAPIAutomationException {

		try {
			SwiggyClient a = new SwiggyClient(false, false, false);
			ClientResponse resp = a.sendRequest(
					STAFAgent.getSTAFEndPoint("delivery_host_healthcheck"), false);
			System.out.println("****************************************");
			System.out.println("STATUS: testCheckIfDEServerUp :\n" + resp);
			System.out
					.println("############################################################################");
			if (resp.getStatus() == HTTP_STATUS_CODE_SUCCESS
					|| resp.getStatus() == HTTP_STATUS_CODE_UNAUTH) {
				Assert.assertTrue(
						STAFAgent.getSTAFEndPoint("delivery_host_healthcheck")
								+ ": DE Server  is Up", true);
			} else {
				Assert.assertTrue(
						STAFAgent.getSTAFEndPoint("delivery_host_healthcheck")
								+ ": DE Server  is Down Now", false);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(STAFAgent.getSTAFEndPoint("delivery_host_healthcheck")
					+ ": DE Server Not accessible!!!!..Please Check");
		}

	}

	@Test
	public void testCheckIfOMSServerUp() throws SwiggyAPIAutomationException {

		try {
			SwiggyClient a = new SwiggyClient(false, false, false);
			ClientResponse resp = a.sendRequest(
					STAFAgent.getSTAFEndPoint("ff_host_healthcheck"), false);
			System.out.println("****************************************");
			System.out.println("STATUS: testCheckIfPortalServerUp :\n" + resp);
			System.out
					.println("############################################################################");
			if (resp.getStatus() == HTTP_STATUS_CODE_SUCCESS) {
				Assert.assertTrue(STAFAgent.getSTAFEndPoint("ff_host_healthcheck")
						+ ": OMS Server  is Up", true);
			} else {
				Assert.assertTrue(STAFAgent.getSTAFEndPoint("ff_host_healthcheck")
						+ ": OMS Server  is Down Now", false);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(STAFAgent.getSTAFEndPoint("ff_host_healthcheck")
					+ ": OMS Server Not accessible!!!!..Please Check");
		}

	}

	@Test
	public void testCheckIfPortalServerUp() throws SwiggyAPIAutomationException {

		try {
			SwiggyClient a = new SwiggyClient(false, false, false);
			ClientResponse resp = a.sendRequest(
					STAFAgent.getSTAFEndPoint("portal_host_healthcheck"), true);
			System.out.println("****************************************");
			System.out.println("STATUS: testCheckIfPortalServerUp :\n" + resp);
			System.out
					.println("############################################################################");
			if (resp.getStatus() == HTTP_STATUS_CODE_SUCCESS) {
				Assert.assertTrue(
						STAFAgent.getSTAFEndPoint("portal_host_healthcheck")
								+ ": Portal Server  is Up", true);
			} else {
				Assert.assertTrue(
						STAFAgent.getSTAFEndPoint("portal_host_healthcheck")
								+ ": Portal Server  is Down Now", false);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(STAFAgent.getSTAFEndPoint("portal_host_healthcheck")
					+ ": Portal Server Not accessible!!!!..Please Check");
		}

	}

	@Test
	public void testCheckIfCMSServerUp() throws SwiggyAPIAutomationException {

		try {
			SwiggyClient a = new SwiggyClient(false, false, false);
			ClientResponse resp = a.sendRequest(
					STAFAgent.getSTAFEndPoint("cms_host_healthcheck"), false);
			System.out.println("****************************************");
			System.out.println("STATUS: testCheckIfCMSServerUp :\n" + resp);
			System.out
					.println("############################################################################");
			if (resp.getStatus() == HTTP_STATUS_CODE_SUCCESS) {
				Assert.assertTrue(STAFAgent.getSTAFEndPoint("cms_host_healthcheck")
						+ ": CMS Server  is Up", true);
			} else {
				Assert.assertTrue(STAFAgent.getSTAFEndPoint("cms_host_healthcheck")
						+ ": CMS Server  is Down Now", false);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(STAFAgent.getSTAFEndPoint("cms_host_healthcheck")
					+ ": CMS Server Not accessible!!!!..Please Check");
		}

	}

	@Test
	public void testCheckIfDEOPSServerUp() throws SwiggyAPIAutomationException {

		try {
			SwiggyClient a = new SwiggyClient(false, false, false);
			ClientResponse resp = a.sendRequest(
					STAFAgent.getSTAFEndPoint("ops_host_healthcheck"), false);
			System.out.println("****************************************");
			System.out.println("STATUS: testCheckIfDE_OPSServerUp :\n" + resp);
			System.out
					.println("############################################################################");
			if (resp.getStatus() == HTTP_STATUS_CODE_SUCCESS) {
				Assert.assertTrue(STAFAgent.getSTAFEndPoint("ops_host_healthcheck")
						+ ": DE_OPS Server  is Up", true);
			} else {
				Assert.assertTrue(STAFAgent.getSTAFEndPoint("ops_host_healthcheck")
						+ ": DE_OPS Server  is Down Now", false);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(STAFAgent.getSTAFEndPoint("ops_host_healthcheck")
					+ ": DE_OPS Server Not accessible!!!!..Please Check");
		}

	}

}
