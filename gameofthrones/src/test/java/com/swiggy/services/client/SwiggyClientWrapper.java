package com.swiggy.services.client;

import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.swiggy.services.client.SwiggyClient.RequestType;
import org.apache.log4j.Logger;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriBuilder;
import java.util.Map;

/**
 *
 * Refer to tests for examples of how to use this class
 */
public class SwiggyClientWrapper {

	public static final Logger log = Logger
			.getLogger(SwiggyClientWrapper.class);
	public SwiggyClient swiggyClient;
	private String host;
	private Integer port;
	private String baseURL;
	private String fixedPath;
	private String url = null;

	/*
	 * 
	 * @param host - host of the service provider
	 * 
	 * @param port - port number
	 * 
	 * @param fixedPath - a fixed path of the UR
	 * 
	 * @param isRequestWrapped - does request needs root element wrapped for
	 * json request
	 * 
	 * @param isResponseWrapped - does response have root element wrapped
	 */
	public SwiggyClientWrapper(String host, Integer port, String fixedPath,
			boolean isRequestWrapped, boolean isResponseWrapped) {
		this.host = host;
		this.port = port;
		this.fixedPath = fixedPath;
		this.url = fixedPath;
		swiggyClient = new SwiggyClient(isRequestWrapped, isResponseWrapped,
				false);
	}
	
	
	public SwiggyClientWrapper(String baseURL, String fixedPath,
			boolean isRequestWrapped, boolean isResponseWrapped) {
		this.baseURL = baseURL;
		this.fixedPath = fixedPath;
		this.url = fixedPath;
		swiggyClient = new SwiggyClient(isRequestWrapped, isResponseWrapped,
				false);
	}

	public SwiggyClientWrapper(String host, Integer port, String fixedPath,
			boolean isRequestWrapped, boolean isResponseWrapped,
			boolean enableHttps) {
		this.host = host;
		this.port = port;
		this.fixedPath = fixedPath;
		this.url = fixedPath;
		swiggyClient = new SwiggyClient(isRequestWrapped, isResponseWrapped,
				enableHttps);
	}



	/**
	 * Sends request
	 * 
	 * @param swiggyClientRequest
	 */
	public void sendRequest(SwiggyClientRequest<?> swiggyClientRequest)
			throws SwiggyClientException {
		if (swiggyClientRequest == null) {
			throw new IllegalArgumentException("Null value supplied");
		}
		sendRequest(swiggyClientRequest.getDynamicPath(),
				swiggyClientRequest.getDynamicPathVariable(),
				swiggyClientRequest.getQueryParameter(),
				swiggyClientRequest.getRequest(),
				swiggyClientRequest.getContentType(),
				swiggyClientRequest.getRequestType(),
				swiggyClientRequest.getExpectedStatusCode(),
				swiggyClientRequest.getHeaders(),
				swiggyClientRequest.isExpectingEmptyResponseBody());
	}

	public <T> T sendRequest(SwiggyClientRequest<?> swiggyClientRequest,
			Class<T> responseClass) throws SwiggyClientException {
		if (swiggyClientRequest == null) {
			throw new IllegalArgumentException("Null value supplied");
		}
		return sendRequest(swiggyClientRequest.getDynamicPath(),
				swiggyClientRequest.getDynamicPathVariable(),
				swiggyClientRequest.getQueryParameter(),
				swiggyClientRequest.getRequest(), responseClass,
				swiggyClientRequest.getContentType(),
				swiggyClientRequest.getAcceptType(),
				swiggyClientRequest.getRequestType(),
				swiggyClientRequest.getExpectedStatusCode(),
				swiggyClientRequest.getHeaders());
	}

	public <T> ClientResponse sendRequest1(
            SwiggyClientRequest<?> swiggyClientRequest, Class<T> responseClass)
			throws SwiggyClientException {
		if (swiggyClientRequest == null) {
			throw new IllegalArgumentException("Null value supplied");
		}
		return sendRequest1(swiggyClientRequest.getDynamicPath(),
				swiggyClientRequest.getDynamicPathVariable(),
				swiggyClientRequest.getQueryParameter(),
				swiggyClientRequest.getRequest(), responseClass,
				swiggyClientRequest.getContentType(),
				swiggyClientRequest.getAcceptType(),
				swiggyClientRequest.getRequestType(),
				swiggyClientRequest.getExpectedStatusCode(),
				swiggyClientRequest.getHeaders());
	}

	public <T> ResponseMaster<T> sendRequest(
            SwiggyClientRequest<?> swiggyClientRequest, Class<T> responseClass,
            boolean responseHeaderExpected) throws SwiggyClientException {
		if (swiggyClientRequest == null) {
			throw new IllegalArgumentException("Null value supplied");
		}
		return sendRequest(swiggyClientRequest.getDynamicPath(),
				swiggyClientRequest.getDynamicPathVariable(),
				swiggyClientRequest.getQueryParameter(),
				swiggyClientRequest.getRequest(), responseClass,
				swiggyClientRequest.getContentType(),
				swiggyClientRequest.getAcceptType(),
				swiggyClientRequest.getRequestType(),
				swiggyClientRequest.getExpectedStatusCode(),
				swiggyClientRequest.getHeaders(), responseHeaderExpected);
	}

	/**
	 * 
	 * @param path
	 *            - the path (excluding the fixed path, and query parameter)
	 * @param pathVariables
	 *            - Map of Key value which will be replaced in the path
	 *            supplied, e.g. if path is $PID/validateLoyaltyCards and if
	 *            pathVariables is provided with PID as key and value as 123 The
	 *            URL will replace $PID and will form 123/validateLoyaltyCards
	 * @param queryParameter
	 *            - Query request URL parameter to be supplied
	 * @param request
	 *            - request object
	 * @param contentType
	 *            - contentType of request body
	 * @param requestType
	 *            - type of the request
	 * @param expectedStatusCode
	 *            - expected status code from response
	 * @param headers
	 *            - request headers
	 * @param expectingEmptyResponseBody
	 *            - expected status code from response
	 */
	public void sendRequest(String path, Map<String, String> pathVariables,
			Map<String, Object> queryParameter, Object request,
			MediaType contentType, RequestType requestType,
			Integer expectedStatusCode, Map<String, Object> headers,
			boolean expectingEmptyResponseBody) throws SwiggyClientException {
		ClientResponse clientResponse = sendRequest(path, pathVariables,
				queryParameter, request, contentType, MediaType.WILDCARD_TYPE,
				requestType, expectedStatusCode, headers);
		if (expectingEmptyResponseBody) {
			if (clientResponse.hasEntity()) {
				clientResponse.bufferEntity();
				String errorMsg = getError(clientResponse);
				throw new SwiggyClientException(
						"No response was expected, but got " + errorMsg);
			}
		}
	}

	private ClientResponse sendRequest(String path,
			Map<String, String> pathVariables,
			Map<String, Object> queryParameter, Object request,
			MediaType contentType, MediaType acceptMediaType,
			RequestType requestType, Integer expectedStatusCode,
			Map<String, Object> headers) throws SwiggyClientException {
		String url = getUrl(path, pathVariables, queryParameter);
		if (contentType == null) {
			throw new SwiggyClientException(
					"contentType should not be null : contentType: "
							+ contentType);
		}
		if (acceptMediaType == null) {
			throw new SwiggyClientException(
					"acceptMediaType should not be null : acceptMediaType: "
							+ acceptMediaType);
		}
		if (requestType == null) {
			throw new SwiggyClientException(
					"requestType should not be null : requestType: "
							+ requestType);
		}
		ClientResponse clientResponse = swiggyClient.sendRequest(url, request,
				contentType, acceptMediaType, requestType, headers);

		return clientResponse;
	}

	/**
	 * 
	 * @param path
	 *            - the path (excluding the fixed path, and query parameter)
	 * @param pathVariables
	 *            - Map of Key value which will be replaced in the path
	 *            supplied, e.g. if path is $PID/validateLoyaltyCards and if
	 *            pathVariables is provided with PID as key and value as 123 The
	 *            URL will replace $PID and will form 123/validateLoyaltyCards
	 * @param queryParameter
	 *            - Query request URL parameter to be supplied
	 * @param request
	 *            - request object
	 * @param responseClass
	 *            - Expected response class
	 * @param contentType
	 *            - contentType of request body
	 * @param acceptMediaType
	 *            - accept Type of response body
	 * @param requestType
	 *            - type of the request
	 * @param expectedStatusCode
	 *            - expected status code from response
	 * @param headers
	 *            - request headers
	 * @return
	 */
	public <T> T sendRequest(String path, Map<String, String> pathVariables,
			Map<String, Object> queryParameter, Object request,
			Class<T> responseClass, MediaType contentType,
			MediaType acceptMediaType, RequestType requestType,
			Integer expectedStatusCode, Map<String, Object> headers)
			throws SwiggyClientException {
		// long startTime;
		// long stopTime;
		// int indexStart = url.indexOf("/") + 1;
		// startTime = System.nanoTime();
		ResponseMaster<T> responseMaster = sendRequest(path, pathVariables,
				queryParameter, request, responseClass, contentType,
				acceptMediaType, requestType, expectedStatusCode, headers,
				false);
		// stopTime = System.nanoTime();
		// System.out.println(
		// "Time measured for " + url.substring(indexStart) + " is : " +
		// (stopTime - startTime) / 1000000 + " ms");
		return responseMaster.getResponseObject();
	}

	public <T> ClientResponse sendRequest1(String path,
			Map<String, String> pathVariables,
			Map<String, Object> queryParameter, Object request,
			Class<T> responseClass, MediaType contentType,
			MediaType acceptMediaType, RequestType requestType,
			Integer expectedStatusCode, Map<String, Object> headers)
			throws SwiggyClientException {
		// long startTime;
		// long stopTime;
		// int indexStart = url.indexOf("/") + 1;
		// startTime = System.nanoTime();
		ClientResponse clientResponse = sendRequest1(path, pathVariables,
				queryParameter, request, responseClass, contentType,
				acceptMediaType, requestType, expectedStatusCode, headers,
				false);
		// stopTime = System.nanoTime();
		// System.out.println(
		// "Time measured for " + url.substring(indexStart) + " is : " +
		// (stopTime - startTime) / 1000000 + " ms");
		return clientResponse;
	}

	public <T> ResponseMaster<T> sendRequest(String path,
                                             Map<String, String> pathVariables,
                                             Map<String, Object> queryParameter, Object request,
                                             Class<T> responseClass, MediaType contentType,
                                             MediaType acceptMediaType, RequestType requestType,
                                             Integer expectedStatusCode, Map<String, Object> headers,
                                             boolean responseHeaderExpected) throws SwiggyClientException {
		if (responseClass == null) {
			throw new SwiggyClientException(
					"Response Class should not be null");
		}
		ResponseMaster<T> result = null;
		T resultEntity = null;
		MultivaluedMap<String, String> resultHeaders = null;
		long startTime;
		long stopTime;
		int indexStart = url.indexOf("/") + 1;
		startTime = System.nanoTime();
		ClientResponse clientResponse = sendRequest(path, pathVariables,
				queryParameter, request, contentType, acceptMediaType,
				requestType, expectedStatusCode, headers);
		stopTime = System.nanoTime();
		System.out.println("Time measured for " + url.substring(indexStart)
				+ " is : " + (stopTime - startTime) / 1000000 + " ms");
		clientResponse.bufferEntity();
		try {
			resultEntity = clientResponse.getEntity(responseClass);
			if (responseHeaderExpected) {
				resultHeaders = clientResponse.getHeaders();
			}
			result = new ResponseMaster<T>(resultEntity, resultHeaders);
		} catch (ClientHandlerException ex) {
			// throw new SwiggyClientException(
			// "Unable to unmarshal response, expected :"
			// + responseClass.getCanonicalName()
			// + " received message: " + getError(clientResponse)
			// + ex.getMessage());
		} catch (WebApplicationException ex) {
			// throw new SwiggyClientException(
			// "Unable to unmarshal response, expected :"
			// + responseClass.getCanonicalName()
			// + " received message : " + getError(clientResponse)
			// + ex.getMessage());
		} finally {
			clientResponse.close();
		}
		return result;
	}

	public <T> ClientResponse sendRequest1(String path,
			Map<String, String> pathVariables,
			Map<String, Object> queryParameter, Object request,
			Class<T> responseClass, MediaType contentType,
			MediaType acceptMediaType, RequestType requestType,
			Integer expectedStatusCode, Map<String, Object> headers,
			boolean responseHeaderExpected) throws SwiggyClientException {
		if (responseClass == null) {
			throw new SwiggyClientException(
					"Response Class should not be null");
		}
		MultivaluedMap<String, String> resultHeaders = null;
		long startTime;
		long stopTime;
		int indexStart = url.indexOf("/") + 1;
		startTime = System.nanoTime();
		ClientResponse clientResponse = sendRequest(path, pathVariables,
				queryParameter, request, contentType, acceptMediaType,
				requestType, expectedStatusCode, headers);
		stopTime = System.nanoTime();
		System.out.println("Time measured for " + url.substring(indexStart)
				+ " is : " + (stopTime - startTime) / 1000000 + " ms");
		// clientResponse.bufferEntity();
		// try {
		// resultEntity = clientResponse.getEntity(responseClass);
		// if (responseHeaderExpected) {
		// resultHeaders = clientResponse.getHeaders();
		// }
		// result = new ResponseMaster<T>(resultEntity, resultHeaders);
		// } catch (ClientHandlerException ex) {
		// throw new SwiggyClientException(
		// "Unable to unmarshal response, expected :"
		// + responseClass.getCanonicalName()
		// + " received message: " + getError(clientResponse)
		// + ex.getMessage());
		// } catch (WebApplicationException ex) {
		// throw new SwiggyClientException(
		// "Unable to unmarshal response, expected :"
		// + responseClass.getCanonicalName()
		// + " received message : " + getError(clientResponse)
		// + ex.getMessage());
		// } finally {
		// clientResponse.close();
		// }
		// return result;
		return clientResponse;
	}

	private String getError(ClientResponse clientResponse) {
		try {
			clientResponse.getEntityInputStream().reset();
			return clientResponse.getEntity(String.class);
		} catch (Exception e) {
			return null;
		}
	}

	private String getUrl(String pathResourceUrl,
			Map<String, String> webResourceParameters,
			Map<String, Object> queryParam) {
		String webResourceUrl = pathResourceUrl;
		if (pathResourceUrl != null && webResourceParameters != null) {
			for (Map.Entry<String, String> entry : webResourceParameters
					.entrySet()) {
				webResourceUrl = webResourceUrl.replace("$" + entry.getKey(),
						entry.getValue());
			}
		}

		UriBuilder uriBuilder = UriBuilder.fromUri(fixedPath);
		//uriBuilder.scheme("http");
		//uriBuilder.host(host);
		//uriBuilder.port(port);
		uriBuilder.uri(baseURL);
		if (webResourceUrl != null) {
			uriBuilder.path(webResourceUrl);
		}
		if (queryParam != null) {
			for (Map.Entry<String, Object> entry : queryParam.entrySet()) {
				uriBuilder.queryParam(entry.getKey(), entry.getValue());
			}
		}

		String url = uriBuilder.build().toString();
		return url;
	}

}
