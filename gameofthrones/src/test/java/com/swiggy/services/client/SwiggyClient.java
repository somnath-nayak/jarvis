package com.swiggy.services.client;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.sun.jersey.api.json.JSONConfiguration;
import org.codehaus.jackson.jaxrs.JacksonJaxbJsonProvider;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.SerializationConfig;

import javax.ws.rs.core.MediaType;
import java.util.Map;

/**
 * 
 * @author mohammedramzi
 *
 */
public class SwiggyClient {

	public enum RequestType {
		GET, POST, PUT, DELETE
    }

	public static class TestJacksonJaxbProviderWrapped extends
			JacksonJaxbJsonProvider {
		public TestJacksonJaxbProviderWrapped() {
			super();
			configure(SerializationConfig.Feature.WRAP_ROOT_VALUE, true);
			configure(DeserializationConfig.Feature.UNWRAP_ROOT_VALUE, true);
		}
	}

	public static class TestJacksonJaxbProviderUnwrapped extends
			JacksonJaxbJsonProvider {
		public TestJacksonJaxbProviderUnwrapped() {
			super();
			configure(SerializationConfig.Feature.WRAP_ROOT_VALUE, false);
			configure(DeserializationConfig.Feature.UNWRAP_ROOT_VALUE, false);
		}
	}

	public static class TestJacksonJaxbProviderUnwrappedRequest extends
			JacksonJaxbJsonProvider {
		public TestJacksonJaxbProviderUnwrappedRequest() {
			super();
			configure(SerializationConfig.Feature.WRAP_ROOT_VALUE, false);
			configure(DeserializationConfig.Feature.UNWRAP_ROOT_VALUE, true);
		}
	}

	public static class TestJacksonJaxbProviderUnwrappedResponse extends
			JacksonJaxbJsonProvider {
		public TestJacksonJaxbProviderUnwrappedResponse() {
			super();
			configure(SerializationConfig.Feature.WRAP_ROOT_VALUE, true);
			configure(DeserializationConfig.Feature.UNWRAP_ROOT_VALUE, false);
		}
	}

	public Client client;
	private boolean httpsEnable = false;

	/**
	 *
	 * @param isRequestWrapped
	 *            determines if the Request is wrapped with root element in JSON
	 * @param isResponseWrapped
	 *            set to true if the Response is going to be wrapped with root
	 *            element in JSON
	 */
	public SwiggyClient(boolean isRequestWrapped, boolean isResponseWrapped,
			boolean enableHttps) {
		ClientConfig clientConfig = null;
		httpsEnable = enableHttps;
		if (enableHttps == false)
			clientConfig = new DefaultClientConfig();
		else
			clientConfig = HttpsClientConfig.ClientConfig();
		clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING,
				Boolean.TRUE);
		if (isRequestWrapped && isResponseWrapped) {
			clientConfig.getClasses().add(TestJacksonJaxbProviderWrapped.class);
		} else if (!isRequestWrapped && !isResponseWrapped) {
			clientConfig.getClasses().add(
					TestJacksonJaxbProviderUnwrapped.class);
		} else if (!isRequestWrapped) {
			clientConfig.getClasses().add(
					TestJacksonJaxbProviderUnwrappedRequest.class);
		} else {
			clientConfig.getClasses().add(
					TestJacksonJaxbProviderUnwrappedResponse.class);
		}
		client = Client.create(clientConfig);
	}

	/**
	 * Sends request to the URL provided to invoke a service call
	 * 
	 * @param url
	 *            - service end point
	 * @param request
	 *            - request object to be marshalled in XML/JSON
	 * @param contentType
	 *            - contentType of request to be marshalled in
	 * @param acceptMediaType
	 *            - Response type desired
	 * @param requestType
	 *            - Type of the request (GET, POST, PUT, DELETE)
	 * @param headers
	 *            - additional headers of the request
	 * @return
	 */
	public ClientResponse sendRequest(String url, Object request,
			MediaType contentType, MediaType acceptMediaType,
			RequestType requestType, Map<String, Object> headers) {
		WebResource resource;
		if (!httpsEnable)
			resource = client.resource(url);
		else
			resource = client.resource(url.replaceFirst("http:", "https:"));

		client.addFilter(new LoggingFilter(System.out));
		WebResource.Builder builder = resource.accept(acceptMediaType).type(
				contentType);
		if (headers != null && !headers.isEmpty()) {
			for (Map.Entry<String, Object> header : headers.entrySet()) {
				builder.header(header.getKey(), header.getValue());
			}
		}

		ClientResponse clientResponse = null;
		switch (requestType) {
		case GET:
			clientResponse = builder.get(ClientResponse.class);
			break;
		case POST:
			clientResponse = builder.post(ClientResponse.class, request);
			break;
		case PUT:
			if (request != null) {
				clientResponse = builder.put(ClientResponse.class, request);
			} else {
				clientResponse = builder.put(ClientResponse.class);
			}
			break;
		case DELETE:
			if (request != null) {
				clientResponse = builder.delete(ClientResponse.class, request);
			} else {
				clientResponse = builder.delete(ClientResponse.class);
			}
			break;
		}
		return clientResponse;
	}

	public ClientResponse sendRequest(String url, boolean httpsEnable) {
		WebResource resource;
		if (!httpsEnable)
			resource = client.resource("http://" + url);
		else
			resource = client.resource("https://" + url);

		client.addFilter(new LoggingFilter(System.out));

		ClientResponse clientResponse = null;

		WebResource.Builder builder = resource.accept(
				MediaType.APPLICATION_JSON).type(MediaType.APPLICATION_JSON);
		clientResponse = builder.get(ClientResponse.class);
		return clientResponse;
	}

}
