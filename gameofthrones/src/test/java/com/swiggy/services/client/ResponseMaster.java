package com.swiggy.services.client;

import java.util.List;
import java.util.Map;

/**
 * 
 * @author mohammedramzi
 *
 */
public class ResponseMaster<T> {
	private T responseObject;
	private Map<String, List<String>> headers;

	public ResponseMaster(T responseObject, Map<String, List<String>> headers) {
		this.responseObject = responseObject;
		this.headers = headers;
	}

	public T getResponseObject() {
		return responseObject;
	}

	public Map<String, List<String>> getHeaders() {
		return headers;
	}
}
