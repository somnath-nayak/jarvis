package com.swiggy.services.client;

/**
 * 
 * User: saurav ghosh Date: 2/13/15 Time: 5:26 PM To change this template use
 * File | Settings | File Templates.
 */
public class SwiggyClientException extends Exception {

    public SwiggyClientException(Exception ex) {
	super(ex);
    }

    public SwiggyClientException(String message) {
	super(message);
    }

    public SwiggyClientException(Exception ex, String message) {
	super(message, ex);
    }
}
