package com.swiggy.Utility;

import java.security.SecureRandom;
import java.util.Random;

public class GenerateRandomUtils {


    public static String generateRandomNumber(int charLength) {
        return String.valueOf(charLength < 1 ? 0 : new Random()
                .nextInt((9 * (int) Math.pow(10, charLength - 1)) - 1)
                + (int) Math.pow(10, charLength - 1));
    }
    public static String generateRandomString( int len ){
         String AB = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
         SecureRandom rnd = new SecureRandom();
         StringBuilder sb = new StringBuilder( len );
         for( int i = 0; i < len; i++ )
            sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
         return sb.toString().toUpperCase();
    }
    public static String generateRandomAlphaNumericString( int len ){
        String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        SecureRandom rnd = new SecureRandom();
        StringBuilder sb = new StringBuilder( len );
        for( int i = 0; i < len; i++ )
            sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
        return sb.toString().toLowerCase();
    }



    public static void main(String a[])
    {
        System.out.println(generateRandomNumber(10));
        System.out.println(generateRandomString(10));
        System.out.println(generateRandomAlphaNumericString(10));
    }

    }


