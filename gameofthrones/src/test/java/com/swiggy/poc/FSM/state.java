package com.swiggy.poc.FSM;

import com.lowagie.text.*;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import framework.gameofthrones.Cersei.GoogleSheetHelper;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;

public class state {

    String GoogleDriveSheetPath = "";
    static List<ApartmentData> tenantdetails = new ArrayList<ApartmentData>();


    public static void main(String args[]) {
        try {
            getData();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

    }

    public static void getData() throws IOException, GeneralSecurityException, URISyntaxException {
        String spreadsheetId = "1rFN4dg1osYzdUgt1h-swiFBUZOkIGeVgxlAYIWCDJSk";
        //holidayiq
        //String range = "urls!A2:W191";
        //swiggy
        String range = "details!A2:K141";
        GoogleSheetHelper sheetAPI = new GoogleSheetHelper();

            List<List<Object>> values = sheetAPI.getSpreadSheetRecords(spreadsheetId, range);
            int count = 0;
            for (List<Object> row : values) {
                //System.out.println("row.get(2) = " + row.get(2));
                String block = row.get(0).toString().trim().toLowerCase();
                String apartmentno = row.get(1).toString().trim().toLowerCase();
                String housetype = row.get(2).toString().trim().toLowerCase();

                String ownername = row.get(3).toString().trim().toLowerCase();
                String ownercellphone = row.get(4).toString().trim().toLowerCase();
                String owneremail = row.get(5).toString().trim().toLowerCase();
                String residentname = row.get(6).toString().trim().toLowerCase();
                String residentcellnumber = row.get(7).toString().trim().toLowerCase();
                String residentemailid = row.get(8).toString().trim().toLowerCase();

                System.out.println("ownername = " + ownername);
                ApartmentData newrow = new ApartmentData(block, apartmentno, housetype, ownername, ownercellphone,
                        owneremail, residentname, residentcellnumber, residentemailid);
                tenantdetails.add(newrow);
            }

    }



        private static void createTable (Section subCatPart)
            throws BadElementException {
            PdfPTable table = new PdfPTable(3);

            // t.setBorderColor(BaseColor.GRAY);
            // t.setPadding(4);
            // t.setSpacing(4);
            // t.setBorderWidth(1);

            PdfPCell c1 = new PdfPCell(new Phrase("Table Header 1"));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase("Table Header 2"));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase("Table Header 3"));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);
            table.setHeaderRows(1);

            table.addCell("1.0");
            table.addCell("1.1");
            table.addCell("1.2");
            table.addCell("2.1");
            table.addCell("2.2");
            table.addCell("2.3");

            subCatPart.add(table);

        }


        private static void createdocument(){

            try {
                String FILE = "/Users/ashish.bajpai/FirstPdf.pdf";
                Document document = new Document();
                PdfWriter.getInstance(document, new FileOutputStream(FILE));
                document.open();
                /*addMetaData(document);
                addTitlePage(document);
                addContent(document);*/
                document.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    private static void addMetaData(Document document) {
        document.addTitle("My first PDF");
        document.addSubject("Using iText");
        document.addKeywords("Java, PDF, iText");
        document.addAuthor("Lars Vogel");
        document.addCreator("Lars Vogel");
    }



}


 class ApartmentData{

    String Block, ApartmentNumber, HouseType, OwnerName, OwnercellphoneNumber, OwnerEmailId,CurrentResidentName, ResidentCellphoneNumber, ResidentEmailID;

    public ApartmentData(String _Block, String _ApartmentNumber, String _HouseType, String _OwnerName, String _OwnercellphoneNumber, String _OwnerEmailId, String _CurrentResidentName, String _ResidentCellphoneNumber, String _ResidentEmailID){
        Block = _Block;
        ApartmentNumber = _ApartmentNumber;
        HouseType = _HouseType;
        OwnerName = _OwnerName;
        OwnercellphoneNumber = _OwnercellphoneNumber;
        OwnerEmailId = _OwnerEmailId;
        CurrentResidentName = _CurrentResidentName;
        ResidentCellphoneNumber = _ResidentCellphoneNumber;
        ResidentEmailID = _ResidentEmailID;
    }



}
