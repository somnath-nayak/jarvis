package com.swiggy.poc.validatorfactory;

/**
 * Created by ashish.bajpai on 26/09/17.
 */
public class MetaObject {
    private String Title;
    private String Description;
    private String Language;

    public String getDescription() {
        return Description;
    }

    public String getLanguage() {
        return Language;
    }

    public String getTitle() {
        return Title;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public void setLanguage(String language) {
        Language = language;
    }

    public void setTitle(String title) {
        Title = title;
    }
}
