package com.swiggy.poc.bdd;

/**
 * Created by ashish.bajpai on 25/08/17.
 */

import com.intuit.karate.cucumber.CucumberRunner;
import com.intuit.karate.cucumber.KarateStats;
import com.intuit.karate.testng.KarateRunner;
import net.masterthought.cucumber.Configuration;
import net.masterthought.cucumber.ReportBuilder;
import org.apache.commons.io.FileUtils;
import org.testng.annotations.BeforeClass;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.testng.AssertJUnit.assertTrue;

//@CucumberOptions(features = "classpath:animals/cats/cats-get.feature")//, tags = {"~@ignore"})
public class CatsRunner extends KarateRunner {

    @BeforeClass
    public  void before() {
        System.setProperty("karate.env", "stage");
        String karateOutputPath = "target/surefire-reports";
        KarateStats stats = CucumberRunner.parallel(getClass(), 1, karateOutputPath);
        generateReport(karateOutputPath);
    }

    private  void generateReport(String karateOutputPath) {
        Collection<File> jsonFiles = FileUtils.listFiles(new File(karateOutputPath), new String[] {"json"}, true);
        List<String> jsonPaths = new ArrayList(jsonFiles.size());
        for (File file : jsonFiles) {
            jsonPaths.add(file.getAbsolutePath());
        }
        Configuration config = new Configuration(new File("target"), "demo");
        ReportBuilder reportBuilder = new ReportBuilder(jsonPaths, config);
        reportBuilder.generateReports();
    }

    /*@Test
    public void testParallel() {
        KarateStats stats = CucumberRunner.parallel(getClass(), 1, "target/surefire-reports");
//        assertTrue("scenarios failed", stats.getFailCount() == 0);
    }*/

}