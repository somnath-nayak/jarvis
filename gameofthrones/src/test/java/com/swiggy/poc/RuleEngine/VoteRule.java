package com.swiggy.poc.RuleEngine;

import org.easyrules.annotation.Action;
import org.easyrules.annotation.Condition;
import org.easyrules.core.BasicRule;

/**
 * Created by ashish.bajpai on 09/05/17.
 */
public class VoteRule extends BasicRule {

    public Person person;

    public VoteRule(Person prsn){
        super("VoteRule", "let's find out if you can vote or not", 2);
        this.person = prsn;
    }

    @Condition
    public boolean evaluate(){
        return person.getAdult();
    }

    @Action
    public void execute(){
        System.out.println("Sorry buddy you can not wait");
    }
}
