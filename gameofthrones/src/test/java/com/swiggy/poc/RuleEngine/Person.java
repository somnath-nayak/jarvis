package com.swiggy.poc.RuleEngine;

/**
 * Created by ashish.bajpai on 09/05/17.
 */
public class Person {

    public String name;
    public Integer age;
    public Boolean isAdult;

    public Person(String name, Integer age){
        setName(name);
        setAge(age);
    }

    public String getName() {
        return name;
    }

    public Boolean getAdult() {
        return isAdult;
    }

    public Integer getAge() {
        return age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAdult(Boolean adult) {
        isAdult = adult;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
