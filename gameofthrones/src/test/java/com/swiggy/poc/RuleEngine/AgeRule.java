package com.swiggy.poc.RuleEngine;

import org.easyrules.core.BasicRule;

/**
 * Created by ashish.bajpai on 09/05/17.
 */

public class AgeRule extends BasicRule {

    private static final int MinAge = 18;

    private Person person;

    public AgeRule(Person persn){
        super("AgeRule","Verify the min age to vote", 1);
        this.person = persn;
    }

    @Override
    public boolean evaluate(){
        return person.getAge() > MinAge;
    }

    @Override
    public void execute(){
        person.setAdult(true);
        System.out.println("You are an adult please go ahead.");
    }

}
