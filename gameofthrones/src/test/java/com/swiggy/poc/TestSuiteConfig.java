package com.swiggy.poc;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Cersei.GameOfThronesListener;
import org.apache.log4j.Logger;
import org.testng.ITestContext;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

/**
 * Created by ashish.bajpai on 16/11/17.
 */
public class TestSuiteConfig {

    static Logger log = Logger.getLogger(TestSuiteConfig.class);
    static Initialize init = new Initialize();
    //static Date beginTime = new Date();
    //static Date endTime = null;
    static String ExecutionTime = null;
    @BeforeSuite(alwaysRun = true)
    public void testBeforeSuite(ITestContext context) {
        //init.FrameworkData.starttime = beginTime;
        //System.out.println("testBeforeSuite()");
        context.getSuite().getXmlSuite().getParameter("password");
    }

    @AfterSuite(alwaysRun = true)
    public void testAfterSuite(ITestContext context) {
        /*log.info("From after suite");
        //endTime = new Date();
        init.FrameworkData.endtime = new Date();
        init.NotificationLevel = AlertLevel.LOW;
        SwiggyListener.mailer.sendmail(init);*/
        //GameOfThronesListener.Mailer.Environment = init.EnvironmentDetails.setup.getEnvironment();

        GameOfThronesListener.Mailer.sendhtmlreport();
        System.out.println("password = " + context.getSuite().getXmlSuite().getParameter("password"));
    }

    @BeforeTest(alwaysRun = true)
    public void testBeforeTest() {
        //System.out.println("beginning test run");
    }

    @AfterTest(alwaysRun = true)
    public void testAfterTest() {
        //log.info("ending test run");
    }

}
