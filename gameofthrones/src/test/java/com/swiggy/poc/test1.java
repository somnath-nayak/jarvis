package com.swiggy.poc;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Baelish.SEOData;
import framework.gameofthrones.Baelish.SEOHelper;
import framework.gameofthrones.Cersei.DataGenerator;
import framework.gameofthrones.Cersei.GoogleSheetHelper;
import framework.gameofthrones.Cersei.ToolBox;
import framework.gameofthrones.JonSnow.APIDetails;
import framework.gameofthrones.Daenerys.InitializeEnvironmentObject;
import framework.gameofthrones.Daenerys.SystemProperties;
//import framework.gameofthrones.JonSnow.HandleRedirects;
import framework.gameofthrones.JonSnow.*;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.glassfish.jersey.filter.LoggingFilter;
import org.jbehave.core.annotations.Given;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import framework.gameofthrones.JonSnow.Validator;
//import framework.gameofthrones.JonSnow.XMLValidator;


import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ashish.bajpai on 26/03/17.
 */
public class test1 {

    Initialize gameofthrones = new Initialize();

    /*@BeforeSuite
    public void beforesuite(ITestResult testResult){

    }*/

    @Given("user is logged in with $userid and $password")
    public void loggedinuser()
    {

    }

    //@Test(invocationCount = 100000, threadPoolSize = 1000)
    @Test(invocationCount = 1, threadPoolSize = 30)
    public void ReadProperties()
    {
        //Initialize intialize = new Initialize();
        try {
            InitializeEnvironmentObject init = new InitializeEnvironmentObject();
            String Endpoints = init.setup.getEnvironmentData().getServices().getEndpoints().getEndpoint().get(1).getBaseurl().toString();
            SystemProperties sysprops = new SystemProperties();
            sysprops.printallsystemproperties();
            System.out.println("Endpoints = " + Endpoints);

        } catch (IOException e) {
            e.printStackTrace();
        }
        //ObjectRepositoryHelper objrepo = new ObjectRepositoryHelper();
    }

    @Test
    public void jsoup(){
        try{
            Document doc = Jsoup.connect("http://en.wikipedia.org/").get();
            Elements metatag = doc.select("meta[name=generator]");
            System.out.println("metatag = " + metatag.attr("content"));
            Elements newsHeadlines = doc.select("#mp-itn b a");
            System.out.println("newsHeadlines = " + newsHeadlines.attr("href").toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void CheckFramework(){
        Initialize gameofthrones = new Initialize();
//gameofthrones.EnvironmentDetails.setup.getEnvironmentData().getServices().getApis().GetAPIDetails()
        System.out.println(gameofthrones.EnvironmentDetails.setup.GetAPIDetails("pps", " paynowcod").Baseurl.toString());
        //gameofthrones.EnvironmentDetails.setup.GetAPIDetails();
        //gameofthrones.EnvironmentDetails.setup.getEnvironmentData().getServices().getEndpoints().getEndpoint().get()
    }


    @Test(dataProvider = "dp1")
    public void servicetest(){
        APIData api = getapidetails("devconfig","androidconfig");
        System.out.println("api.CompleteURL = " + api.CompleteURL);
        gameofthrones.EnvironmentDetails.setup.GetAPIDetails("devconfig","androidconfig").CompleteURL = "http://www.swiggy.com";
        GameOfThronesService service = new GameOfThronesService(api.Webservice,api.APIName, gameofthrones );

        //gameofthrones.EnvironmentDetails.setup.getEnvironmentData().getServices().getApis().GetAPIDetails()
        //System.out.println(gameofthrones.EnvironmentDetails.setup.GetAPIDetails("pps", " paynowcod").Baseurl.toString());
        //gameofthrones.EnvironmentDetails.setup.GetAPIDetails();
        //gameofthrones.EnvironmentDetails.setup.getEnvironmentData().getServices().getEndpoints().getEndpoint().get()
    }

    @Test
    public void servicetest2(){
        APIData api = getapidetails("devconfig","androidconfig");
        System.out.println("api.CompleteURL = " + api.CompleteURL);
        gameofthrones.EnvironmentDetails.setup.GetAPIDetails("devconfig","androidconfig").CompleteURL = "http://www.swiggy.com";
        GameOfThronesService service = new GameOfThronesService(api.Webservice,api.APIName, gameofthrones );
//      service.parameterizeurl(service.APIDetails.CompleteURL,new String[] = {"android"});
        System.out.println(" updated url = " + gameofthrones.EnvironmentDetails.setup.GetAPIDetails("devconfig","androidconfig").CompleteURL);


        //gameofthrones.EnvironmentDetails.setup.getEnvironmentData().getServices().getApis().GetAPIDetails()
        //System.out.println(gameofthrones.EnvironmentDetails.setup.GetAPIDetails("pps", " paynowcod").Baseurl.toString());
        //gameofthrones.EnvironmentDetails.setup.GetAPIDetails();
        //gameofthrones.EnvironmentDetails.setup.getEnvironmentData().getServices().getEndpoints().getEndpoint().get()
    }

    private APIData getapidetails(String webservicename, String APIName){
        return gameofthrones.EnvironmentDetails.setup.GetAPIDetails(webservicename, APIName);
    }



    @Test
    public void GenerateCombinations() {
        List<String> lst1 = new ArrayList<String>();
        List<String> lst2 = new ArrayList<String>();
        List<String> lst3 = new ArrayList<String>();

        lst1.add("first");
        lst1.add("second");
        lst1.add("third");

        lst2.add("Abba");
        lst2.add("Judas");
        lst2.add("Beatles");

        lst3.add("one");
        lst3.add("two");
        lst3.add("three");

        List<List<String>> lst4 = new ArrayList<List<String>>();

        lst4.add(lst1);
        lst4.add(lst2);
        lst4.add(lst3);

        DataGenerator generator = new DataGenerator();

        //Object[][] returnlist = new Object[][]();

        generator.generatevariants2(lst1, lst2, lst3);
    }

    @Test
    public void testjersey(){
        /*Client client = Client.create();

        WebResource wresource = client.resource("http://www.swiggy.com");
        wresource.addFilter(new HandleRedirects());
        ClientResponse clientresponse = wresource.accept("application/json").header("user-agent","Mozilla/5.0").get(ClientResponse.class);
        System.out.println("clientresponse.getStatus() = " + clientresponse.getStatus());

        if(clientresponse.getStatus() != 200){
            System.out.println("clientresponse = " + clientresponse.getStatus());
        }

        String Responsebody = clientresponse.getEntity(String.class);

        System.out.println("Responsebody = " + Responsebody);*/
    }


    @Test
    public void testjersey2(){
        Client client = ClientBuilder.newClient( new ClientConfig().register( LoggingFilter.class ) );
        WebTarget webTarget = client.target("http://swiggy.com");
        webTarget.register(HandleRedirects.class);
        webTarget.property(ClientProperties.FOLLOW_REDIRECTS, Boolean.TRUE);
        Invocation.Builder invocationBuilder =  webTarget.request(MediaType.TEXT_HTML);
        Response response = invocationBuilder.get();
        String respdata = response.readEntity(String.class);
        System.out.println("respdata = " + respdata);
    }


    @Test
    public void testjersey3(){
        Client client = ClientBuilder.newClient( new ClientConfig().register(LoggingFilter.class ) );
        WebTarget webTarget = client.target("http://www.swiggy.com");
        webTarget.register(HandleRedirects.class);
        webTarget.property(ClientProperties.FOLLOW_REDIRECTS, Boolean.TRUE);
        Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON);
        //Response response = invocationBuilder.post(Entity.entity(String,MediaType.APPLICATION_JSON));
        //String respdata = response.readEntity(String.class);
        //System.out.println("respdata = " + respdata);
    }

    @DataProvider(name = "dp1")
    private Object[][] dataprovider(){

        return new Object[][] {{"android"},{"ios"},{"android/3.16"},{"ios/3.16"}};

    }

    @Test
    public void createruntimesrvice(){
//        APIData api1 = new APIData();

    }

    @Test(invocationCount = 10, threadPoolSize = 3, description = "this is the dummy test this is the dummy test this is the dummy test this is the dummy test this is the dummy test this is the dummy test this is the dummy test", groups = "dummy")
    public void customrequest(){
        WebServiceDetails service = new WebServiceDetails("devconfig","storefront","http://localhost/config","false", "","","");
        APIDetails api = new APIDetails("androidconfig", "android", "GET", "false", "false","json","devconfig");
        Processor processor = new Processor(api, service);
    }

    @Test(groups = "ca_ios_end, ")
    public void newrequest(){
        GameOfThronesService service = new GameOfThronesService("devconfig","androidconfig", gameofthrones);
        Processor processor = new Processor(service);
        //Processor processor = new Processor(api, service);
    }


    @Test
    public void newpostrequest(){
        GameOfThronesService service1 = new GameOfThronesService("apploginv2","loginv2", gameofthrones);
        Processor processor = new Processor(service1);
    }


    @Test
    public void newpostrequest1(){
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("test-header1", "myheader");
        requestheaders.put("test-header2", "myheader2");
        requestheaders.put("test-header3", "myheader3");
        requestheaders.put("test-header4", "myheader4");

        GameOfThronesService service1 = new GameOfThronesService("apploginv2","loginv2", gameofthrones);
        Processor processor = new Processor(service1, requestheaders);

        //Processor processor = new Processor(api, service);
    }


    @Test(dataProvider = "dptest", threadPoolSize = 10)
    public void newpostrequest2(String value){
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        System.out.println("value = " + value);
        requestheaders.put("test-header1", "myheader");
        requestheaders.put("test-header2", "myheader2");
        requestheaders.put("test-header3", "myheader3");
        requestheaders.put("test-header4", "myheader4");

        GameOfThronesService service1 = new GameOfThronesService("apploginv2","login1", gameofthrones);

        String[] payloadparams = new String[2];
        payloadparams[0] = "!#i;Pob!IiAcde%@!i@3$32xDeas1!@$f#@!iPh!IiRe";
        payloadparams[1] = "7338471121";

        Processor processor = new Processor(service1, requestheaders, payloadparams);

        GameOfThronesService service2 = new GameOfThronesService("apploginv2","login1", gameofthrones);

        String[] payloadparams2 = new String[2];
        payloadparams[0] = "!#i;Pob!IiAcde%@!i@3$32xDeas1!@$f#@!iPh!IiRe";
        payloadparams[1] = "7338471121";

        Processor processor2 = new Processor(service2, requestheaders, payloadparams2);



        //Processor processor = new Processor(api, service);
    }


    @Test(groups = "daily_Sanity, 15_Min_Sanity")
    public void vmsE2E(){
        HashMap<String, String> requestheaders_login = new HashMap<String, String>();
        requestheaders_login.put("Content-Type", "application/json");

        GameOfThronesService login = new GameOfThronesService("rms","login", gameofthrones);
        Processor login_response = new Processor(login, requestheaders_login);

        String tokenid = login_response.ResponseValidator.GetNodeValue("data.access_token");
        int restaurantId = login_response.ResponseValidator.GetNodeValueAsInt("data.restaurants[0].rest_id");



        HashMap<String, String> requestheaders_createuser = new HashMap<String, String>();
        requestheaders_createuser.put("Content-Type", "application/json");
        requestheaders_createuser.put("Cookie", "Swiggy_Session-alpha="+tokenid);
        GameOfThronesService createuser = new GameOfThronesService("rms","createuser", gameofthrones);
        Processor createuser_response = new Processor(createuser, requestheaders_createuser);


        HashMap<String, String> requestheaders_updateuser = new HashMap<String, String>();
        requestheaders_updateuser.put("Content-Type", "application/json");
        requestheaders_updateuser.put("Cookie", "Swiggy_Session-alpha="+tokenid);
        GameOfThronesService updateuser = new GameOfThronesService("rms","updateuser", gameofthrones);
        Processor updateuser_response = new Processor(updateuser, requestheaders_updateuser);



        HashMap<String, String> requestheaders_disableuser = new HashMap<String, String>();
        requestheaders_disableuser.put("Content-Type", "application/json");
        requestheaders_disableuser.put("Cookie", "Swiggy_Session-alpha="+tokenid);
        GameOfThronesService disableuser = new GameOfThronesService("rms","disableuser", gameofthrones);
        String[] params = new String[]{"true"};
        String[] params2 = new String[]{"false"};
        Processor disableuser_response = new Processor(disableuser, requestheaders_disableuser,params2);

        Processor enableuser_response = new Processor(disableuser, requestheaders_disableuser,params);
        System.out.println("enableuser_response.ResponseValidator.GetBodyAsText() = " + enableuser_response.ResponseValidator.GetBodyAsText());


        HashMap<String, String> requestheaders_fetchusers = new HashMap<String, String>();
        requestheaders_fetchusers.put("Content-Type", "application/json");
        requestheaders_fetchusers.put("Cookie", "Swiggy_Session-alpha="+tokenid);
        GameOfThronesService fetchusers = new GameOfThronesService("rms","fetchusers", gameofthrones);
        String[] restaurantid = new String[]{String.valueOf(restaurantId)};
        Processor fetchusers_response = new Processor(fetchusers, requestheaders_fetchusers,null,restaurantid);

        HashMap<String, String> requestheaders_logout = new HashMap<String, String>();
        requestheaders_logout.put("Content-Type", "application/json");
        requestheaders_logout.put("Cookie", "Swiggy_Session-alpha="+tokenid);
        GameOfThronesService logoutuser = new GameOfThronesService("rms","logoutrms", gameofthrones);
        //String[] restaurantid = new String[]{String.valueOf(restaurantId)};
        Processor logoutuser_response = new Processor(logoutuser, requestheaders_logout,null,null);



//System.out.println(login_response.ResponseValidator.GetNodeValue("$.statusMessage"));
        //System.out.println("Body = "+login_response.ResponseValidator.GetBodyAsText());
        //login_response.ResponseValidator.GetCookies();
        //login_response.ResponseValidator.ComparewithExpectedResponse();


/*
        HashMap<String, String> requestheaders_createuser = new HashMap<String, String>();
        requestheaders_createuser.put("Content-Type", "application/json");
        requestheaders_createuser.put("Cookie", "Swiggy_Session-alpha="+tokenid);
        GameOfThronesService createuser = new GameOfThronesService("rms","createuser", gameofthrones);
        Processor createuser_response = new Processor(createuser, requestheaders_createuser);
*/


    }

    @Test(groups = "sanity,newfeature1")
    public void DBtest(){
    }

    @DataProvider (name = "dptest")
    public Object[][] dp_test() {
        String[] arr1 = {"Nike"};
        String[] arr2 = {"Puma"};
        String[] arr3 = {"Adidas"};
        String[] arr4 = {"Reebok"};
        /*String[] arr5 = {"Biba"};
        String[] arr6 = {"Cat"};*/

        String str1 = "com.swiggy.poc.test1";
        String str2 = "test2";


        /*return new Object[][]{
                new Object[]{arr1},
                new Object[]{arr2},
                new Object[]{arr3},
                new Object[]{arr4}
        };*/
        //return new Object[][]{{str1},{str2}};
        return new Object[][]{arr1,arr2, arr3, arr4};
    }

    @Test
    public void seocralwer(){
        Validator valid = null;
        /*valid = new XMLValidator();
        if (valid.DoesNodeExists()){
            System.out.println("Yes its positive");
        }
        else{
            System.out.println("No its negative");
        }


        /*WebDriver driver = new ChromeDriver();
        System.out.println("Hello Google...");
        driver.get("https://www.swiggy.com");*/
    }

    @Test
    public void jsouptest(){
        Document doc = null;
        try {
            doc = Jsoup.connect("https://www.swiggy.com/bangalore/restaurants").get();
            System.out.println("doc = " + doc.head().className());
        } catch (IOException e) {
            e.printStackTrace();
        }
        String title = doc.title();

    }

    @Test
    public void seotest(){
        Document doc = null;
        String url = "https://www.swiggy.com/bangalore/restaurants";
        //String url = "http://www.holidayiq.com/destinations/bangalore";
        //String url = "https://www.swiggy.com";
        int h1tagcount = CountH1Tags(url);
        System.out.println("h1tagcount = " + h1tagcount);
        //Document doc = Jsoup.parse("https://www.swiggy.com/bangalore/restaurants");
        //Document doc = Jsoup.connect("https://www.swiggy.com/bangalore/restaurants");
        try {
            //doc = Jsoup.connect("https://www.swiggy.com/bangalore/restaurants").get();
            //doc = Jsoup.connect("http://www.holidayiq.com/destinations/bangalore/map.html").get();
            doc = Jsoup.connect(url).get();
            //System.out.println("doc = " + doc.head().className());
            //if(checkrobots(doc, "index, follow")){
            //if(checkcanonical(doc)){
            /*if(checkcasesensitive(url)){
                System.out.println("value exists and matches");
            }
            else{
                System.out.println("value does not exists or does not match");
            }*/
            String envvalue = gameofthrones.Properties.propertiesMap.get("environment");
            System.out.println("envvalue = " + envvalue);
        } catch (IOException e) {
            e.printStackTrace();
        }
        /*Elements metaTags = doc.getElementsByTag("meta");

        MetaObject ex = new MetaObject();
        System.out.println("metaTags = " + metaTags.size());
        for (Element metaTag : metaTags) {
            String content = metaTag.attr("content");
            String name = metaTag.attr("name");

            if(name.equals("viewport")){
                System.out.println(" Value of view port = " + content);
            }


        }*/
    }


    public boolean checkmobileurl(Document doc){
        boolean returnval = false;
        String nodeattribute = doc.select("link[rel=alternate]").attr("media");
        int nodesize = doc.select("link[rel=alternate]").size();
        System.out.println("nodeattribute = " + nodeattribute);
        if(nodeattribute.trim().length()>0 && nodesize > 0){
            returnval = true;
        }
        return returnval;
    }


    public boolean checkindex(Document doc){
        boolean returnval = false;
        String nodeattribute = doc.select("meta[name=robots]").attr("content");
        int nodesize = doc.select("meta[name=ROBOTS]").size();

        System.out.println("nodeattribute = " + nodeattribute);

        if (nodeattribute.trim().length() > 0 && nodesize > 0 && nodeattribute.split(",")[0].trim().toLowerCase().equals( "index")) {
            returnval = true;
        }

        return returnval;
    }


    public boolean checkrobots(Document doc, String expectedvalue){
        boolean returnval = false;
        String nodeattribute = doc.select("meta[name=robots]").attr("content");
        int nodesize = doc.select("meta[name=robots]").size();

        System.out.println("nodeattribute = " + nodeattribute);

        if (nodeattribute.trim().length() > 0 && nodesize > 0 && nodeattribute.trim().toLowerCase().equals( expectedvalue)) {
            returnval = true;
        }

        return returnval;
    }

    public boolean checkcasesensitive(String url){
        String lowercaseurl = url.toLowerCase().trim();
        String uppercaseurl = url.toUpperCase().trim();
        boolean returnval = false;
        boolean responsecode = false;
        boolean documentsamimarity = false;

        try {
            Connection.Response response1 = Jsoup.connect(lowercaseurl).followRedirects(true).execute();
            //System.out.println("response2.statusCode() = " + response1.statusCode()+" : "+lowercaseurl);
            Connection.Response response2 = Jsoup.connect(uppercaseurl).followRedirects(false).execute();
            Connection.Response response3 = Jsoup.connect(uppercaseurl).followRedirects(true).execute();

            //System.out.println("response2.statusCode() = " + response2.statusCode()+" : "+uppercaseurl+" : "+response2.header("location"));
            //System.out.println("check case = " + org.apache.commons.lang3.StringUtils.isAllLowerCase(response2.header("location")));

            if (response2.statusCode() == 301){
                System.out.println("response2.statusCode() = " + response2.statusCode());
                responsecode = true;
            }

            String path = response1.url().toURI().getPath().toString();
            System.out.println(" Is lowercase " + StringUtils.isAllLowerCase("test"));
            //System.out.println("response1.parse().html() = " + response1.parse().html());
            //System.out.println("response2.parse().html() = " + response2.parse().html());
            System.out.println("response1.url().toURI().getPath().toString() = " + response1.url().toURI().getPath().toString());
            System.out.println("response1.url().toString().equals(response2.url().toString()) = " + response1.url().toURI().toString().equals(response2.url().toString()));
            //System.out.println("response1.toString() = " + response1.parse().html().equals(response2.parse().html()));
            Document lowercasedoc = response1.parse();
            ToolBox tools = new ToolBox();
            String content1 = DigestUtils.sha1Hex(lowercasedoc.toString());
            Document upperercasedoc = response2.parse();
            String content2 = DigestUtils.sha1Hex(upperercasedoc.toString());
            //System.out.println("StringUtils.difference(content1, content2) = " + StringUtils.difference(content1, content2));
            if (content1.equals(content2)){
                System.out.println(" Documents are equal ");
                returnval = true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return returnval;
    }



    public boolean checkcanonical(Document doc){
        boolean returnval = false;
        String nodeattribute = doc.select("link[rel=canonical]").attr("href");
        int nodesize = doc.select("link[rel=canonical]").size();
        System.out.println("nodeattribute = " + nodeattribute);
        if(nodeattribute.trim().length()>0 && nodesize > 0){
            returnval = true;
        }

        return returnval;
    }

    public int VerifyStatusCode(String url, boolean redirectionAllowed){
        boolean followredirects = false;
        Connection.Response response1 = null;
        if (redirectionAllowed){
            followredirects = true;
        }
        try {
            response1 = Jsoup.connect(url).followRedirects(followredirects).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response1.statusCode();
    }

    public int CountH1Tags(String url){
        try {
            System.out.println("url = " + url);
            Document response = Jsoup.connect(url).get();
            //Elements ntmAmount = response.getElementsByTag("h1");
            Elements ntmAmount = response.getElementsByTag("h1");
            System.out.println("ntmAmount = " + ntmAmount.text());
            return ntmAmount.size();

        } catch (IOException e) {
            e.printStackTrace();
            return 0;
        }

    }

    @Test(groups = "dummy")
    public void formdata(){
        GameOfThronesService deliveryboylogin = new GameOfThronesService("deliveryapplogin", "dblogin", gameofthrones);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("authorization", "Basic R0dZU1dJOjIwMTVTVyFHR1k=");
        headers.put("content-type", "application/form-data");

        HashMap<String, String> formcontent = new HashMap<String, String>();
        headers.put("id", "216");
        headers.put("version", "1.8.2.1");

        String[] params= { "216", "8.1.2.1"};


        Processor response = new  Processor(deliveryboylogin, headers, params, null, formcontent);
        Processor response1 = new  Processor(deliveryboylogin, headers, null, null, formcontent);


    }

    private void formdatalogin(){
        GameOfThronesService deliveryboylogin = new GameOfThronesService("deliveryapplogin", "dblogin", gameofthrones);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("authorization", "Basic R0dZU1dJOjIwMTVTVyFHR1k=");
        headers.put("content-type", "application/form-data");

        HashMap<String, String> formcontent = new HashMap<String, String>();
        headers.put("id", "216");
        headers.put("version", "1.8.2.1");

        String[] params= { "216", "8.1.2.1"};


        Processor response = new  Processor(deliveryboylogin, headers, params, null, formcontent);
        Processor response1 = new  Processor(deliveryboylogin, headers, null, null, formcontent);


    }

    @Test(groups = "dummy")
    public  void testreloadenvironment(){
        System.out.println("environment1 = " + gameofthrones.EnvironmentDetails.setup.getEnvironment());
        formdatalogin();
        gameofthrones.EnvironmentReload("stage1");
        System.out.println("environment1 = " + gameofthrones.EnvironmentDetails.setup.getEnvironment());
        formdatalogin();
        gameofthrones = new Initialize("preprod");
        System.out.println("environment2 = " + gameofthrones.EnvironmentDetails.setup.getEnvironment());
        formdatalogin();


    }

    @Test
    public void googlesheetdata() throws GeneralSecurityException, URISyntaxException {
        //String spreadsheetId = "18KwPxYWOZZmbjYTnComj0ckIuMjtSqw6TBE0DyBogAI";
        //String range = "URLS-SEO!A2:W83";
        String spreadsheetId = "1rFN4dg1osYzdUgt1h-swiFBUZOkIGeVgxlAYIWCDJSk";
        String range = "urls!A2:W291";
        GoogleSheetHelper sheetAPI = new GoogleSheetHelper();
        try {
            List<List<Object>> values = sheetAPI.getSpreadSheetRecords(spreadsheetId, range);
            int count = 0;
            for (List<Object> row : values) {
                System.out.println("row.get(2) = " + row.get(2));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void seotest2() throws GeneralSecurityException, URISyntaxException ,IOException {
        SEOHelper seohelp = new SEOHelper();
        List<SEOData> seodata = seohelp.seochecks;

        for (SEOData row: seodata){
            System.out.println("row = " + row.H1TagCount);
        }
    }
}
