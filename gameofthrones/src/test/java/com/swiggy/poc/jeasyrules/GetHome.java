package com.swiggy.poc.jeasyrules;

import org.jeasy.rules.annotation.Action;
import org.jeasy.rules.annotation.Condition;
import org.jeasy.rules.annotation.Fact;
import org.jeasy.rules.annotation.Rule;

/**
 * Created by ashish.bajpai on 24/11/17.
 */
@Rule(name = "Reach home", description = "avoid traffic man" )
public class GetHome {

    @Condition
    public boolean homeway(@Fact("home") boolean clear){
        return clear;
    }

    @Action
    public void trafficclarity(){
        System.out.println("Its a jam, you should take another route");
    }

}
