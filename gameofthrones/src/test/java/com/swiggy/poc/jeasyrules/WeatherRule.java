package com.swiggy.poc.jeasyrules;


import org.jeasy.rules.annotation.Action;
import org.jeasy.rules.annotation.Condition;
import org.jeasy.rules.annotation.Fact;
import org.jeasy.rules.annotation.Rule;

/**
 * Created by ashish.bajpai on 23/11/17.
 */
@Rule(name = "Weather Rule", description = "Take umbrella" )
public class WeatherRule {
    @Condition
    public boolean itrains(@Fact("rain") boolean rain){
        return rain;
    }

    @Action
    public void TakeUmbrella(){
        System.out.println("Its raining, you should take umbrella");
    }


    @Action
    public void DonotTakeUmbrella(){
        System.out.println("Its not raining, you should not take umbrella");
    }

}
