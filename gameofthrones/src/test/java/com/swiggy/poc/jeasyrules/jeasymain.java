package com.swiggy.poc.jeasyrules;

import framework.gameofthrones.Baelish.*;
import org.jeasy.rules.api.Facts;
import org.jeasy.rules.api.Rules;
import org.jeasy.rules.api.RulesEngine;
import org.jeasy.rules.core.DefaultRulesEngine;
import org.testng.annotations.Test;

import java.util.List;

/**
 * Created by ashish.bajpai on 23/11/17.
 */
public class jeasymain {
	
	
		@Test
		public void SEOtest()  throws Exception {
//    public static void main(String[] args) throws Exception {

        


        SEOHelper seohelp = new SEOHelper();
        List<SEOData> seodata = seohelp.seochecks;
        SEOResult seor=new SEOResult();
        

        int count = 0;

        for (SEOData row: seodata){
        
                //Getting the true and false check to calculate total
        		seor.getURL(row.URL);
        		seor.getStatusCode(row.StatusCode);
        		seor.getIndex(row.Index);
        		seor.getCanonical(row.Canonical);
        		seor.getCaseSensitive(row.Casesensitiveurl);
        		seor.getUrlWithoutwww(row.withoutwww);
        		seor.getMetaTitle(row.Metatitle);
        		seor.getMetaDescription(row.MetaDescription);
        		seor.getH1Count(row.H1TagCount);
        		seor.getRobot(row.RobotsTxt);
        		seor.checkTrue();
                Facts facts = new Facts();
                facts.put("statuscode", Boolean.valueOf(row.StatusCode.toLowerCase().toString().trim()));
                //facts.put("mobileurl",Boolean.valueOf(row.MobileUrl.toLowerCase().toString().trim()));
                facts.put("index",Boolean.valueOf(row.Index.toLowerCase().toString().trim()));
                facts.put("canonical",Boolean.valueOf(row.Canonical.toLowerCase().toString().trim()));
                facts.put("casesensitiveurl",Boolean.valueOf(row.Casesensitiveurl.toLowerCase().toString().trim()));
                facts.put("urlwithoutwww",Boolean.valueOf(row.withoutwww.toLowerCase().toString().trim()));
                facts.put("metatitle",Boolean.valueOf(row.Metatitle.toLowerCase().toString().trim()));
                facts.put("metadescription",Boolean.valueOf(row.MetaDescription.toLowerCase().toString().trim()));
                facts.put("H1count",Boolean.valueOf(row.H1TagCount.toLowerCase().toString().trim()));
                facts.put("robottxt",Boolean.valueOf(row.RobotsTxt.toLowerCase().toString().trim()));
                Rules rules = new Rules(new StatusCodeCheck(row), /*new MobileUrl(row),*/ new IndexCheck(row), new CanonicalCheck(row), 
                		new CaseSensitiveURL(row),new URLWithoutwww(row), new MetaTitle(row), new MetaDescription(row),
                		new H1tagcount(row), new RobotTxt(row));
                RulesEngine rulesEngine = new DefaultRulesEngine();
                rulesEngine.fire(rules, facts);
                
        }
        //Sending mail and setting properties
        SEOEmail se=new SEOEmail();
        
        se.setProperties();
        se.sendEmail();
    }




}
