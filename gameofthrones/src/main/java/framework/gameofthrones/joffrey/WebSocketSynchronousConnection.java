package framework.gameofthrones.joffrey;

import org.junit.Assert;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.client.WebSocketConnectionManager;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.sockjs.client.RestTemplateXhrTransport;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.Transport;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.TimeUnit;

public class WebSocketSynchronousConnection {
    public static final int WAIT_TIMEOUT = 3;

    private BlockingQueue<TextMessage> messages;
    private WebSocketClientHandler handler;
    private WebSocketConnectionManager connectionManager;
    private final SockJsClient sockJsClient;

    public WebSocketSynchronousConnection() {
        messages = new SynchronousQueue();
        List<Transport> transports = new ArrayList<Transport>();
        transports.add(new WebSocketTransport(new StandardWebSocketClient()));
        transports.add(new RestTemplateXhrTransport());
        sockJsClient = new SockJsClient(transports);
    }

    public void start(String wsUri) throws InterruptedException {

        handler = new WebSocketClientHandler(messages);
/*
        sockJsClient.doHandshake(handler,
                String.format(wsUri));
*/

        connectionManager = new WebSocketConnectionManager(sockJsClient, handler, wsUri);
        connectionManager.start();
        TextMessage message = messages.poll(WAIT_TIMEOUT, TimeUnit.SECONDS);
        Assert.assertEquals(new TextMessage("connected"), message);
    }

    public void stop() {
        connectionManager.stop();
    }

    public TextMessage sendMessage(TextMessage message, long waitSec) throws IOException, InterruptedException {
        handler.sendMessage(message);
        return messages.poll(waitSec, TimeUnit.SECONDS);
    }

    public TextMessage pollMessage(long waitSec) throws InterruptedException {
        return messages.poll(waitSec, TimeUnit.SECONDS);
    }

    public int getMessages() throws InterruptedException {
        return messages.size();
    }

}