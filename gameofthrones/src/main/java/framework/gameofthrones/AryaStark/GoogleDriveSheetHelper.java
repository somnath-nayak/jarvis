package framework.gameofthrones.AryaStark;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.api.services.sheets.v4.model.BatchGetValuesResponse;
import com.google.api.services.sheets.v4.model.ValueRange;
import org.testng.annotations.BeforeClass;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.List;

public class GoogleDriveSheetHelper {

    private static Credential credential;
    private static final String APPLICATION_NAME = "Google Sheets Example";
    private static Sheets sheetsService;
    private static String SPREADSHEET_ID = "1rFN4dg1osYzdUgt1h-swiFBUZOkIGeVgxlAYIWCDJSk";// ...




    public static void main(String args[]) throws IOException, GeneralSecurityException {
        sheetsService = getSheetsService();
        List<String> ranges = Arrays.asList("E1", "E4");
        BatchGetValuesResponse readResult = sheetsService.spreadsheets().values()
                .batchGet(SPREADSHEET_ID)
                .setRanges(ranges)
                .execute();

        ValueRange januaryTotal = readResult.getValueRanges().get(0);
        //assertThat(januaryTotal.getValues().get(0).get(0)).isEqualTo("40");

        ValueRange febTotal = readResult.getValueRanges().get(1);
        //assertThat(febTotal.getValues().get(0).get(0)).isEqualTo("25");
    }


    public static Credential authorize() throws IOException, GeneralSecurityException {

        List<String> scopes = Arrays.asList(SheetsScopes.SPREADSHEETS);

        return credential;
    }


    public static Sheets getSheetsService() throws IOException, GeneralSecurityException {
        Credential credential = authorize();
        return new Sheets.Builder(
                GoogleNetHttpTransport.newTrustedTransport(),
                JacksonFactory.getDefaultInstance(), credential)
                .setApplicationName(APPLICATION_NAME)
                .build();
    }



}
