package framework.gameofthrones.JonSnow;

/**
 * Created by ashish.bajpai on 11/08/17.
 */
public enum PayloadType {
    XML,
    JSON,
    FORMDATA,
    URLENCODED
}
