package framework.gameofthrones.JonSnow;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Daenerys.Setup;
import framework.gameofthrones.Daenerys.environment;
import org.apache.commons.lang.text.StrSubstitutor;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.glassfish.jersey.filter.LoggingFilter;


import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by akb on 22/07/17.
 */
public class GameOfThronesService {

    public APIData APIDetails = null;
    public environment EnvironmentData = null;

    private String requestsendmethodtype = "GET";


    public GameOfThronesService(String Servicename, String apiundertest, Initialize init)
    {
        /*testenvconfiguration = config;
        apidetails = new APIDetails(Servicename, apiundertest.toString(), config);
        loadapidetails(apidetails);
        loadobjectdetails(Servicename, apiundertest);

        if (PayloadRequired)
        {
            Payload ld = new Payload(apiundertest,config,payloaddataformat);
            Payload = ld.filecontent;
        }*/

         APIDetails = init.EnvironmentDetails.setup.GetAPIDetails(Servicename, apiundertest);
        EnvironmentData = init.EnvironmentDetails.setup.getEnvironmentData();
        //execute();
        //System.out.println("Path complete = " + APIDetails.CompleteURL);
    }

    public GameOfThronesService(String Servicename, String apiundertest, Initialize init, String AddedParam)
    {
        /*testenvconfiguration = config;
        apidetails = new APIDetails(Servicename, apiundertest.toString(), config);
        loadapidetails(apidetails);
        loadobjectdetails(Servicename, apiundertest);

        if (PayloadRequired)
        {
            Payload ld = new Payload(apiundertest,config,payloaddataformat);
            Payload = ld.filecontent;
        }*/

        APIDetails = init.EnvironmentDetails.setup.GetAPIDetails(Servicename, apiundertest);

        EnvironmentData = init.EnvironmentDetails.setup.getEnvironmentData();
        //execute();
        //System.out.println("Path complete = " + APIDetails.CompleteURL);
    }

    /*private APIData CreateApiObject(){

    }*/


    public void processpayload(){

    }

    public void processurl(String[] StringParameters){

        String fullurl = APIDetails.CompleteURL;
        String newurl = fullurl;
        boolean apipathmatcher = APIDetails.APIPath.toString().contains("${") && APIDetails.APIPath.toString().contains("}");
        boolean baseurlmacther = APIDetails.Baseurl.toString().contains("${");

        if (apipathmatcher || baseurlmacther){
            newurl = prepareparameterizedURL(fullurl,StringParameters);
        }
        APIDetails.CompleteURL = newurl;
    }



    public void processpayload(String[] StringParameters){

        //String payloadbody = APIDetails.;
        /*String fullurl = APIDetails.CompleteURL;
        boolean apipathmatcher = APIDetails.APIPath.toString().contains("${") && APIDetails.APIPath.toString().contains("}");
        boolean baseurlmacther = APIDetails.Baseurl.toString().contains("${");

        if ((Boolean.parseBoolean(APIDetails.PayloadRequired.toString())){
            newurl = prepareparameterizedURL(fullurl,StringParameters);
        }
        APIDetails.CompleteURL = newurl;*/


    }

    private void GetPayloadType(String Servicename, String apiundertest){
        if (Boolean.parseBoolean(APIDetails.PayloadRequired)){
            //processpayload();
        }
    }

    /*private String PreparepayloadBody(){

    }*/

    public String parameterizeurl(String URL, String[] StringParameters){
        return prepareparameterizedURL(URL, StringParameters);
    }

    private void execute(){
        Client client = ClientBuilder.newClient( new ClientConfig().register( LoggingFilter.class ) );
        WebTarget webTarget = client.target(APIDetails.Baseurl+"/"+APIDetails.APIPath);
        webTarget.register(HandleRedirects.class);
        webTarget.property(ClientProperties.FOLLOW_REDIRECTS, Boolean.TRUE);
        Invocation.Builder invocationBuilder =  webTarget.request(MediaType.TEXT_HTML);
        Response response = invocationBuilder.get();
        String respdata = response.readEntity(String.class);
        System.out.println("respdata = " + respdata);
    }



    private String prepareparameterizedURL(String URL, String[] StringParameters) {
        Map<String, String> valuesMap = new HashMap<String, String>();
        int paramnumber = 0;
        for (String param : StringParameters) {
            valuesMap.put(Integer.toString(paramnumber), param);
            paramnumber++;
        }
        String templateString = URL;
        StrSubstitutor sub = new StrSubstitutor(valuesMap);
        String resolvedString = sub.replace(templateString);
        return resolvedString;
    }

    //private String prepareurl(String url, String )
}
