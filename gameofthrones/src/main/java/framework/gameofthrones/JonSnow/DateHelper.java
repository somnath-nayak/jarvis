package framework.gameofthrones.JonSnow;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import static java.time.temporal.ChronoUnit.DAYS;
import static java.time.temporal.ChronoUnit.MINUTES;

/**
 * @author : aviral.nigam
 * @package : framework.gameofthrones.JonSnow
 **/
public class DateHelper {

    public Long getCurrentEpochTimeInMillis() {
        return Instant.now().toEpochMilli();
    }

    public Long getCurrentEpochTimeInSeconds() {
        return Instant.now().getEpochSecond();
    }

    public Long getDaysAddedToCurrentEpochInMillis(Integer days) {
        return Instant.now().plus(days,DAYS).toEpochMilli();
    }

    public Long getMinutesAddedToCurrentEpochInMillis(Integer minutes) {
        return Instant.now().plus(minutes,MINUTES).toEpochMilli();
    }

    public String getCurrentDateTimeIn_yyyyMMddHHmmss() {
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        String date_time = dateFormat.format(date);
        return date_time;
    }

    public String getDateTimePlusOrMinusCurrentDate_yyyy_mm_dd_hh_mm_ss(Integer days) {
        Calendar calender = Calendar.getInstance();
        calender.add(Calendar.DATE, days);
        SimpleDateFormat sdf = new SimpleDateFormat();
        sdf.applyPattern("yyyy-MM-dd HH:mm:ss");
        String sdf_date = sdf.format(calender.getTime());
        System.out.println("You asked for the Date_Time  ====>  "+ sdf_date);
        return sdf_date;
    }

    //Give negative days value for getting minus days
    public String getDatePlusOrMinusCurrentDate_yyyy_mm_dd(Integer days) {
        Calendar calender = Calendar.getInstance();
        calender.add(Calendar.DATE, days);
        SimpleDateFormat sdf = new SimpleDateFormat();
        sdf.applyPattern("yyyy-MM-dd");
        String sdf_date = sdf.format(calender.getTime());
        System.out.println("You asked for the Date  ====>  "+ sdf_date);
        return sdf_date;
    }

    //Give negative days value for getting minus days
    public String getDatePlusOrMinusCurrentDate_dd_mm_yyyy(Integer days) {
        Calendar calender = Calendar.getInstance();
        calender.add(Calendar.DATE, days);
        SimpleDateFormat sdf = new SimpleDateFormat();
        sdf.applyPattern("dd-MM-yyyy");
        String sdf_date = sdf.format(calender.getTime());
        System.out.println("You asked for the Date  ====>  "+ sdf_date);
        return sdf_date;
    }

    public String getCurrentDateTimeIn_yyyy_mm_dd_hh_mm_ss() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String strDate = sdf.format(cal.getTime());
        System.out.println("Current Date_Time  =====>  " + strDate);
        return strDate;
    }

    public String getCurrentDateIn_yyyy_mm_dd() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String strDate = sdf.format(cal.getTime());
        System.out.println("Current Date  =====>  " + strDate);
        return strDate;
    }

    public String getCurrentDateIn_dd_mm_yyyy() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String strDate = sdf.format(cal.getTime());
        System.out.println("Current Date  =====>  " + strDate);
        return strDate;
    }

    public String getCurrentDayOfWeek() {
        Date now = new Date();
        SimpleDateFormat simpleDateformat = new SimpleDateFormat("EEEE"); // the day of the week spelled out completely
        String dayOfWeek = simpleDateformat.format(now);
        System.out.println("Current Day =====> "+dayOfWeek);
        return dayOfWeek;
    }

    public int getCurrentDayOfWeekInNumeric() {
        Date now = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.setTime(now);
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        int finalDay = --day;
        if (finalDay == 0) // to handle the case of sunday
            finalDay = 7;
        System.out.println("Current Day Of Week In Numeric =====> "+finalDay);
        return finalDay; //since Days of week are indexed starting at 1
    }

    public String getDayForAGivenDate(String yyyy_mm_dd) {
        SimpleDateFormat format1=new SimpleDateFormat("yyyy-MM-dd");
        Date dt1= null;
        try {
            dt1 = format1.parse(yyyy_mm_dd);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat format2=new SimpleDateFormat("EEEE");
        String finalDay=format2.format(dt1);
        System.out.println("Day for Date: "+yyyy_mm_dd+" is:"+finalDay);
        return finalDay;
    }

    public int getDayForAGivenDateAsNumeric(String yyyy_mm_dd) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        Date date = null;
        try {
            date = sdf.parse(yyyy_mm_dd);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        calendar.setTime(date);
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        int finalDay = --day;
        if (finalDay == 0) // to handle the case of sunday
            finalDay = 7;
        System.out.println("Current Day Of Week In Numeric for given date =====> "+finalDay);
        return finalDay; //since Days of week are indexed starting at 1 so 6 means friday
    }

    public String getMinutesPlusOrMinusCurrentDateTime_yyyy_mm_dd_hh_mm_ss(int minutes) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MINUTE, minutes);
        String finalDateTime = dateFormat.format(cal.getTime());
        System.out.println("Final Date-Time after add/subtract(min.): "+minutes+ " to current date-time is: "+finalDateTime);
        return finalDateTime;
    }

    public String getMinutesPlusOrMinusToAGivenDateTime_yyyy_mm_dd_hh_mm_ss(String yyyy_mm_dd_hh_mm_ss, int minutes) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = dateFormat.parse(yyyy_mm_dd_hh_mm_ss);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        System.out.println("Given Date-Time ::: "+yyyy_mm_dd_hh_mm_ss);
        cal.add(Calendar.MINUTE, minutes);
        String finalDateTime = dateFormat.format(cal.getTime());
        System.out.println("Added/Subtracted Date-Time ::: "+finalDateTime);
        return finalDateTime;
    }

    public String getCurrentTimeInHundredHours() {
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("HH:mm");
        String time = dateFormat.format(date);
        String[] formated_time = time.split(":");
        String hundredhours = formated_time[0] + formated_time[1];
        System.out.println("Current Time in O Hundred hours ::: "+hundredhours);
        return hundredhours;
    }

    public String getMinutesPlusMinusCurrentTimeInHundredHours(int minutes) {
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("HH:mm");
        String time = dateFormat.format(date);
        System.out.println("Current Time ::: "+time);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MINUTE, minutes);
        String finalTime = dateFormat.format(cal.getTime());
        String[] formated_time = finalTime.split(":");
        String hundredhours = formated_time[0] + formated_time[1];
        System.out.println("Added/Subtracted Time ::: "+hundredhours);
        return hundredhours;
    }

    public String convertCurrentDateTimeInHHMMToEpochMillis(String hhmm){
        if(hhmm.length() < 4) {
            hhmm = "0"+hhmm;
        }
        String current_date_time = getCurrentDateTimeIn_yyyy_mm_dd_hh_mm_ss();
        String[] current = current_date_time.split(" ");
        System.out.println("==Current Date ::: "+current[0]);
        String temp_hhmm = hhmm.substring(0,2)+":"+hhmm.substring(2,4);
        String current_dateTime = current[0]+" "+temp_hhmm;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        sdf.setTimeZone(TimeZone.getTimeZone("IST"));
        Date date = null;
        try {
            date = sdf.parse(current_dateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long timeInMillis = date.getTime();
        System.out.println("-- CURRENT DATE_TIME :::"+current_dateTime+ "   === EPOCH :::"+timeInMillis);
        return String.valueOf(timeInMillis);
    }

    public String convertEpochToyyyy_mm_dd_hh_mm_ss (Long epoch_in_millis) {
        Date date = new Date(epoch_in_millis);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("IST"));
        String formattedDate = sdf.format(date);
        System.out.println("=== EPOCH IN MILLIS :::"+epoch_in_millis+ "  --Converted To DATE_TIME(yyyy-MM-dd HH:mm:ss) :::"+formattedDate);
        return formattedDate;

    }


    public String convertEpochTodd_mm_yyy (Long epoch_in_millis) {
        Date date = new Date(epoch_in_millis);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        sdf.setTimeZone(TimeZone.getTimeZone("IST"));
        String formattedDate = sdf.format(date);
        System.out.println("=== EPOCH IN MILLIS :::"+epoch_in_millis+ "  --Converted To DATE_TIME(yyyy-MM-dd HH:mm:ss) :::"+formattedDate);
        return formattedDate;

    }


    public String convertCurrentDateTimeIndd_mm_yyyy_hh_mmToEpoch(String dd_mm_yyyy, String hhmm) {
        if(hhmm.length() < 4) {
            hhmm = "0"+hhmm;
        }
        String givenDate = dd_mm_yyyy;
        System.out.println("==Given Date ::: "+givenDate);
        String temp_hhmm = hhmm.substring(0,2)+":"+hhmm.substring(2,4);
        String current_dateTime = givenDate+" "+temp_hhmm;
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        sdf.setTimeZone(TimeZone.getTimeZone("IST"));
        Date date = null;
        try {
            date = sdf.parse(current_dateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long timeInMillis = date.getTime();
        System.out.println("EPOCH time for the given date::: "+timeInMillis);
        long time = Math.floorDiv(timeInMillis,1000);
        System.out.println("-- CURRENT DATE_TIME :::"+current_dateTime+ "   === EPOCH :::"+time);
        return String.valueOf(time);
    }

    public String convertCurrentDateTimeIndd_mm_yyyy_hh_mmToEpochMillis(String dd_mm_yyyy, String hhmm) {
        if(hhmm.length() < 4) {
            hhmm = "0"+hhmm;
        }
        String givenDate = dd_mm_yyyy;
        System.out.println("==Given Date ::: "+givenDate);
        String temp_hhmm = hhmm.substring(0,2)+":"+hhmm.substring(2,4);
        String current_dateTime = givenDate+" "+temp_hhmm;
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        sdf.setTimeZone(TimeZone.getTimeZone("IST"));
        Date date = null;
        try {
            date = sdf.parse(current_dateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long timeInMillis = date.getTime();
        System.out.println("-- Given DATE_TIME :::"+current_dateTime+ "   === EPOCH (Millis):::"+timeInMillis);
        return String.valueOf(timeInMillis);
    }
}
