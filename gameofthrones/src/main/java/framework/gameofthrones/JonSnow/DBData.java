package framework.gameofthrones.JonSnow;

import framework.gameofthrones.Daenerys.Connstring;
import framework.gameofthrones.Daenerys.Queue;
import framework.gameofthrones.Daenerys.environment;

/**
 * DB Data
 * @author abhijit.p
 */
public class DBData {

    public String name;
    public String server;
    public int Port;
    public String username;
    public String password;
    public String dbType;
    public String dbID;

    public DBData(String dbName,environment Env){
        GetDBDetails(dbName, Env);
    }

    private void GetDBDetails(String dbName,environment Env) {
        Connstring connstring = Env.getDatabase().getConnectionstrings().getDBDetails(dbName);
        name = connstring.getName();
        server = connstring.getServer();
        Port = connstring.getPort();
        username = connstring.getUsername();
        password = connstring.getPassword();
        dbID = connstring.getDbid();
        dbType = connstring.getProtocol();
    }

}
