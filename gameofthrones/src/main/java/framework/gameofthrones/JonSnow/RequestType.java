package framework.gameofthrones.JonSnow;

/**
 * Created by ashish.bajpai on 10/08/17.
 */
public enum RequestType {

    POST,
    PUT,
    GET,
    DELETE,
    PATCH,

}
