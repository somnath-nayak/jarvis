package framework.gameofthrones.JonSnow;

/**
 * Created by ashish.bajpai on 17/07/17.
 */
public class APIDetails {
    /*public String Webservice;
    public String Core;
    public String Baseurl;
    public String AuthRequired;
    public String UserID;
    public String SecurityToken;
    public String Password;*/
    public String APIName;
    public String APIPath;
    public String RequestMethod;
    public String PayloadRequired;
    public String QueryParamsRequired;
    public String PayloadType;
    public String ServiceName;
    public String PayloadBody;

    public APIDetails(String apiname, String apipath, String requestMethod,
                      String payloadRequired, String queryParamsRequired, String payloadtype, String servicename){
//        Webservice = webservice;
//        Core = core;
//        Baseurl = baseurl;
//        AuthRequired = authrequired;
//        UserID = userid;
//        SecurityToken = securitytoken;
//        Password = password;
        //String webservice, String core, String baseurl, String authrequired,
        //String userid, String securitytoken, String password,
        APIName = apiname;
        APIPath = apipath;
        RequestMethod = requestMethod;
        PayloadRequired = payloadRequired;
        QueryParamsRequired = queryParamsRequired;
        PayloadType = payloadtype;
        ServiceName = servicename;
    }
}
