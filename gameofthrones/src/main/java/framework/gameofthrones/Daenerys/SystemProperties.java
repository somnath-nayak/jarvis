package framework.gameofthrones.Daenerys;

import java.util.Hashtable;
import java.util.Properties;

/**
 * Created by ashish.bajpai on 20/04/15.
 */
public class SystemProperties

{
    public Properties SystemProperties = null;

    public SystemProperties(){
        GetSystemProperties();
    }

    public void printallsystemproperties()
    {
        Properties properties = System.getProperties();
        properties.list(System.out);
    }

    public Hashtable<Object,Object> GetSystemProperties()
    {
        Properties properties = System.getProperties();
        SystemProperties = properties;
        return properties;
    }

    public String GetSystemProperty(String SystemPropertyKey)
    {
        return SystemProperties.getProperty("SystemPropertyKey");
    }
}
