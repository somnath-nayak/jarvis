package framework.gameofthrones.Daenerys;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by ashish.bajpai on 05/02/15.
 */
@XmlRootElement(name = "endpoints")
public class endpoints
{
    private List<endpoint> endpoint;
    //private String name;

    public List<endpoint> getEndpoint() {
        return endpoint;
    }

    @XmlElement()
    public void setEndpoint(List<endpoint> endpoint) {
        this.endpoint = endpoint;
    }



    /*@XmlAttribute
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }*/
}
