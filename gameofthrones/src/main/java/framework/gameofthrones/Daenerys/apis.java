package framework.gameofthrones.Daenerys;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by ashish.bajpai on 10/02/15.
 */
@XmlRootElement(name = "apis")
public class apis
{
    private String servicename;
    private List<api> apis;

//    public apis(String _apiname, String _servicename)
//    {
//        setServicename(_servicename);
//        //servname = _servicename;
//    }

    public apis()
    {

    }

    //private String name;


    public List<api> getApi() {
        return apis;
    }

    @XmlElement()
    public void setApi(List<api> api) {
        this.apis = api;
    }


//    public void setServicename(String servicename) {
//        this.servicename = servicename;
//    }
//
//    public String getServicename() {
//        return servicename;
//    }

    /*public api apidetails(endpoint servicedetails, String Apiname){
        List<api> apilist = this.getApi();
        for (api:apilist) {

        }

    }*/

    public api GetAPIDetails(String serviceName, String APIName){
        api api = new api();

        for (api ap: apis) {

            if (ap.getApiname().trim().toLowerCase().equals(APIName.toLowerCase().trim())
                    && ap.getServicename().trim().toLowerCase().equals(serviceName.toLowerCase().trim())){
                api = ap;
            }

        }
        return api;
    }


}
