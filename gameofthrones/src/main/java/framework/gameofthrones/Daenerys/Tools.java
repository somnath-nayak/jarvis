package framework.gameofthrones.Daenerys;


import framework.gameofthrones.Aegon.OS;
import org.apache.commons.io.IOUtils;
//import org.apache.commons.lang.text.StrSubstitutor;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.log4j.Logger;
/*import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;*/

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;

/**
 * Created by ashish.bajpai
 */
public class Tools
{

    static Logger log = Logger.getLogger(Tools.class);
    private String operatingsystem = System.getProperty("os.name").toLowerCase();

    public void createdirectoryStructure(String DirectoryPath)
    {
        File files = new File(DirectoryPath);
        if (files.exists()) {
            if (files.mkdirs()) {
                System.out.println("Directory Structure Created Successfully");
                log.error("Directory Structure Created Successfully");
            } else {
                System.out.println("Error: Failed to create directory Structure..");
                log.error("Error: Failed to create directory Structure..");
            }
        }
    }
    public void printcurrentdirectory(Object classname)
    {
        URL location = Object.class.getProtectionDomain()
                .getCodeSource().getLocation();
        System.out.println(location.getFile());
        System.out.println(System.getProperty("user.dir"));
    }

    public boolean CheckIfFileexists(String Pathoffile)
    {
        boolean result = false;
        File f = new File(Pathoffile);
        result = f.exists();

        return result;
    }

    public String readFileAsString(String filePath) throws IOException
    {
        log.info("Begining reading file '" + filePath + "'");
        // StringBuffer fileData = new StringBuffer();
        BufferedReader reader = new BufferedReader(new FileReader(filePath));
		/*
		 * char[] buf = new char[1024]; int numRead=0;
		 * while((numRead=reader.read(buf)) != -1){ String readData =
		 * String.valueOf(buf, 0, numRead); fileData.append(readData); }
		 * reader.close();
		 */

        // log.info("File content is : "+IOUtils.toString(reader));
        return IOUtils.toString(reader);
    }

    /*public String commaparser()
    {
        Map<String, String> valuesMap = new HashMap<String, String>();
        valuesMap.put("0", ",");
        String templateString = "//div[contains(@class${0}'header')]/div/a/div[@class='badge bagcount']";
        StrSubstitutor sub = new StrSubstitutor(valuesMap);
        String resolvedString = sub.replace(templateString);
        return resolvedString;
    }*/

    public void PrintList(List<?> Listtoprint)
    {
        String str = "";
        for (Object object : Listtoprint)
        {
            str = str + object.toString() + ",";
        }
        log.info(str);
    }

    public void printpage()
    {
        try
        {
            URL url = new URL("http://www.swiggy.com/");
            URLConnection con;

            con = url.openConnection();
            InputStream in = con.getInputStream();
            String encoding = con.getContentEncoding();
            encoding = encoding == null ? "UTF-8" : encoding;
            String body = IOUtils.toString(in, encoding);
            System.out.println(body);
        } catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public void getbasefolderpath()
    {
        OS osname = getosname();


        switch (osname)
        {
            case LINUX:

                break;

            default:
                break;
        }
    }

    public OS getosname()
    {
        if (isWindows())
        {
            return OS.WINDOWS;
        } else if (isMac())
        {
            return OS.MACOS;
        } else if (isUnix())
        {
            return OS.LINUX;
        } else if (isSolaris())
        {
            return OS.SOLARIS;
        } else
        {
            return null;
        }
    }

    private boolean isWindows()
    {

        return (operatingsystem.indexOf("win") >= 0);

    }

    private boolean isMac()
    {

        return (operatingsystem.indexOf("mac") >= 0);

    }

    private boolean isUnix()
    {

        return (operatingsystem.indexOf("nix") >= 0
                || operatingsystem.indexOf("nux") >= 0 || operatingsystem
                .indexOf("aix") > 0);

    }

    private boolean isSolaris()
    {

        return (operatingsystem.indexOf("sunos") >= 0);

    }

    /**
     *
     * @param URL
     *            (The complete URL of the webpage ex: www.swiggy.com
     * @return The DOM of the url as string
     */
    public static String getWebPageDOM(String URL)
    {
        String response = null;
        try
        {
            InputStreamReader is = curler(URL);
            response = IOUtils.toString(is);
        } catch (IOException e)
        {
            log.error("Exception occurred whil reading the webpage as DOM"
                    + e.getMessage());

        }
        return response;
    }

    private static InputStreamReader curler(String URL) throws IOException
    {
        String cmd = "curl -s " + URL;
        String[] list = cmd.split(" ");
        ProcessBuilder pb = new ProcessBuilder(list);
        Process p = pb.start();
        InputStreamReader is = new InputStreamReader(p.getInputStream());
        return is;
    }

    public static BufferedReader getWebPageDOMByLine(String URL)
    {
        InputStreamReader is = null;
        try
        {
            is = curler(URL);
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        return new BufferedReader(is);
    }

    /**
     * Method to reduce the contents of a double dimensional dataprovider
     * depending on the group that is included for the test. Currently this
     * method runs all data variants for ExhaustiveRegression, regressionDpCount
     * data variants for Regression and miniRegressionDpCount data variants for
     * MiniRegression
     *
     * @param dataSet
     * @param includedGroups
     * @param miniRegressionDpCount
     * @param regressionDpCount
     * @return
     */
    public static Object[][] returnReducedDataSet(Object[][] dataSet,
                                                  String[] includedGroups, int miniRegressionDpCount,
                                                  int regressionDpCount)
    {
        if (ArrayUtils.contains(includedGroups, "ExhaustiveRegression"))
        {
            // Don't remove any of the data
        } else if (ArrayUtils.contains(includedGroups, "Regression"))
        {
            dataSet = removeNItemsFromArray(dataSet, regressionDpCount);
        } else
            dataSet = removeNItemsFromArray(dataSet, miniRegressionDpCount);
        return dataSet;
    }

    // For Environment Specific dataset
    public static Object[][] returnReducedDataSet(String Env,
                                                  Object[][] dataSet, String[] includedGroups,
                                                  int miniRegressionDpCount, int regressionDpCount)
    {
        if (Env != null)
            dataSet = returnEnvSpecificDataSet(Env, dataSet);
        else
            dataSet = returnDataSetIfEnvNull(dataSet);
        if (ArrayUtils.contains(includedGroups, "ExhaustiveRegression"))
        {
            // Don't remove any of the data
        } else if (ArrayUtils.contains(includedGroups, "Regression"))
        {
            dataSet = removeNItemsFromArray(dataSet, regressionDpCount);
        } else
            dataSet = removeNItemsFromArray(dataSet, miniRegressionDpCount);
        return dataSet;
    }

    /**
     * Method to remove elements from a double dimensional array
     *
     * @param dataSet
     *            the array to remove elements from
     * @param finalArrayCount
     *            final length of the array needed
     * @return the reduced double dimensional array
     */
    public static Object[][] removeNItemsFromArray(Object[][] dataSet,
                                                   int finalArrayCount)
    {
        Random random = new Random();
        for (int i = dataSet.length - finalArrayCount; i > 0; i--)
        {
            int indexToBeRemoved = random.nextInt(dataSet.length);
            dataSet = ArrayUtils.remove(dataSet, indexToBeRemoved);
        }
        return dataSet;
    }
/*

    public static List<String> getAbsoluteUrlSet(List<String> urls)
    {
        List<String> absoluteurls = new ArrayList<String>();
        Set<String> set = new HashSet<String>();
        int counter = 1;
        for (String url : urls)
        {
            System.out.println("Printing the url "+counter+" : " + url);
            log.info("Printing the url "+counter+" : " + url);

            if (!url.contains("fashion.swiggy.com") || !url.contains("bit.ly") && url.trim().length() > 0)
            {
                if (url.contains("http://") || url.contains("https://"))
                {
                    set.add(url);
                } else
                {
                    if (url.trim().equals("#"))
                    {
                        set.add(initialize.TestEnvironment.URL_PORTALHOME+"/" + url);;
                    } else {
                        if(!url.startsWith("/")) url = "/"+url;
                        set.add(initialize.TestEnvironment.URL_PORTALHOME+ url);
                    }
                }
            }
            counter++;
        }
        absoluteurls.addAll(set);
        return absoluteurls;
    }
*/

    /**
     * Method to get the fully qualified URL given a host name
     *
     * @param url
     * @return
     */
    public static String getFullyQualifiedUrl(String url)
    {
        String fullyQualifiedUrl = "";
        if (url.contains("http://") && url.contains(".swiggy.com"))
        {
            fullyQualifiedUrl = url;
        } else if (url.contains(".swiggy.com"))
        {
            fullyQualifiedUrl = "http://" + url;
        } else
        {
            fullyQualifiedUrl = "http://" + url + ".swiggy.com";
        }
        return fullyQualifiedUrl;
    }

    public boolean createFile(String fullyQualifiedPath)
    {
        File file = new File(fullyQualifiedPath);
        try
        {

            if (file.exists())
            {
				/*
				 * System.out.println("Is Execute allow : " +
				 * file.canExecute()); System.out.println("Is Write allow : " +
				 * file.canWrite()); System.out.println("Is Read allow : " +
				 * file.canRead());
				 */
            }

			/*
			 * System.out.println("Is Execute allow : " + file.canExecute());
			 * System.out.println("Is Write allow : " + file.canWrite());
			 * System.out.println("Is Read allow : " + file.canRead());
			 */

            if (file.createNewFile())
            {
                System.out.println("File is created!");
            } else
            {
                System.out.println("File already exists.");
            }


            /*file.setExecutable(true);
            file.setReadable(true);
            file.setWritable(true);*/

        } catch (IOException e)
        {

            e.printStackTrace();
        }
        return file.canWrite() && file.canRead();
    }

    public void WriteDatatoFile(String Filepath, String Data, Boolean append)
    {
        try
        {
            createFile(Filepath);
            PrintWriter out = new PrintWriter(new BufferedWriter(
                    new FileWriter(Filepath, append)));
            out.println(Data);
            out.close();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public void AppendDataToFile(String data, String filepath)
    {
        WriteDatatoFile(filepath, data, true);
    }

    public String getHTTPResponse(String url)
    {
        String USER_AGENT = "Mozilla/5.0";
        String requestSendMethod = "GET";
        return getHTTPResponse(url, USER_AGENT, requestSendMethod);

    }

    public String getHTTPResponseasjsonString(String url)
    {
        String USER_AGENT = "Mozilla/5.0";
        String requestSendMethod = "GET";
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Accept", "application/json");
        return getHTTPResponse(url, USER_AGENT, requestSendMethod, headers,
                false);
    }

    public String getHTTPResponse(String url, HashMap<String, String> headers)
    {
        String USER_AGENT = "Mozilla/5.0";
        String requestSendMethod = "GET";
        return getHTTPResponse(url, USER_AGENT, requestSendMethod, headers,
                false);
    }

    public String getHTTPResponse(String url, HashMap<String, String> headers,
                                  Boolean Maintainlineseperation)
    {
        String USER_AGENT = "Mozilla/5.0";
        String requestSendMethod = "GET";
        return getHTTPResponse(url, USER_AGENT, requestSendMethod, headers,
                true);
    }

    public int getHTTPResponseCode(String url)
    {
        String USER_AGENT = "Mozilla/5.0";
        String requestSendMethod = "GET";
        return getHTTPResponseCode(url, USER_AGENT, requestSendMethod);
    }

    public int getHTTPResponseCode_RFP(String url)
    {
        String USER_AGENT = "Mozilla/5.0";
        String requestSendMethod = "GET";
        return getHTTPResponseCodeRFP(url, USER_AGENT, requestSendMethod);
    }

    private String getHTTPResponse(String url, String useragent,
                                   String requestSendMethod, HashMap<String, String> headers,
                                   boolean maintainlines)
    {
        try
        {
            String USER_AGENT = useragent;
            Thread.sleep(500);
            // System.out.println(url);
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod(requestSendMethod);
            con.setRequestProperty("User-Agent", USER_AGENT);
            // con.setRequestProperty("Accept","application/json");
            for (Map.Entry<String, String> entry : headers.entrySet())
            {
                con.setRequestProperty(entry.getKey(), entry.getValue());
            }
            int responseCode = con.getResponseCode();
            // System.out.println("Sending 'GET' request to URL : " + url);
            // System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            int counter = 0;
            while ((inputLine = in.readLine()) != null)
            {
                if (maintainlines)
                {
                    response.append(inputLine + "\n");
                } else
                {
                    response.append(inputLine);
                }
                counter++;
            }

            log.info("Total lines in response:" + counter);
            in.close();
            return response.toString();
        } catch ( IOException e)
        {
            e.printStackTrace();
            return null;
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    private String getHTTPResponse(String url, String useragent,
                                   String requestSendMethod)
    {
        try
        {
            String USER_AGENT = useragent;
            Thread.sleep(500);
            // System.out.println(url);
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod(requestSendMethod);
            con.setRequestProperty("User-Agent", USER_AGENT);
            // con.setRequestProperty("Accept","application/json");
            // for
            int responseCode = con.getResponseCode();
            // System.out.println("Sending 'GET' request to URL : " + url);
            // System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null)
            {

                response.append(inputLine);
            }

            log.info("Total lines in response:" + response.length());
            in.close();
            return response.toString();
        } catch ( IOException e)
        {
            e.printStackTrace();
            return null;
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    private int getHTTPResponseCode(String url, String useragent,
                                    String requestSendMethod)
    {
        int returnvalue = 0;
        try
        {

            String USER_AGENT = useragent;
            Thread.sleep(500);
            // System.out.println(url);
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod(requestSendMethod);
            con.setRequestProperty("User-Agent", USER_AGENT);
            // con.setRequestProperty("Accept","application/json");
            // for
            returnvalue = con.getResponseCode();

        } catch ( IOException e)
        {
            e.printStackTrace();
            return -1;
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
            return -1;
        }
        return returnvalue;
    }

    private int getHTTPResponseCodeRFP(String url, String useragent,String requestSendMethod)
    {
        int returnvalue = 0;
        try
        {

            String USER_AGENT = useragent;
            Thread.sleep(500);
            // System.out.println(url);
            URL obj = new URL(url.replace("http:", "https:"));
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod(requestSendMethod);
            con.setRequestProperty("User-Agent", USER_AGENT);
            // con.setRequestProperty("Accept","application/json");
            // for
            returnvalue = con.getResponseCode();

        } catch ( IOException e)
        {
            e.printStackTrace();
            return -1;
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
            return -1;
        }
        return returnvalue;
    }

    private static Object[][] returnEnvSpecificDataSet(String Env,
                                                       Object[][] dataSet)
    {
        List dataSetAsList = Arrays.asList(dataSet);
        ArrayList dataSetOfEnv = new ArrayList();
        int index = 0;
        for (int i = 0; i < dataSetAsList.size(); i++)
        {
            String[] test = (String[]) dataSetAsList.get(i);
            // System.out.println(test[0]);
            if (test[0].toUpperCase().contains(Env.toUpperCase()))
            {
                dataSetOfEnv.add(index,
                        ArrayUtils.subarray(test, 1, test.length));
                index++;
            }
        }
        Object[][] newDataSet = new Object[dataSetOfEnv.size()][];
        for (int i = 0; i < dataSetOfEnv.size(); i++)
        {
            newDataSet[i] = (Object[]) dataSetOfEnv.get(i);

        }

        return newDataSet;
    }

    private static Object[][] returnDataSetIfEnvNull(Object[][] dataSet)
    {
        List dataSetAsList = Arrays.asList(dataSet);
        ArrayList dataSetOfEnv = new ArrayList();
        int index = 0;
        for (int i = 0; i < dataSetAsList.size(); i++)
        {
            String[] test = (String[]) dataSetAsList.get(i);
            dataSetOfEnv.add(index, ArrayUtils.subarray(test, 1, test.length));
            index++;
        }
        Object[][] newDataSet = new Object[dataSetOfEnv.size()][];
        for (int i = 0; i < dataSetOfEnv.size(); i++)
        {
            newDataSet[i] = (Object[]) dataSetOfEnv.get(i);
        }

        return newDataSet;
    }
/*

    public static boolean checkUIElements(Initialize initialize)
    {
        boolean isValid = true;
        List<String> allFieldsFromPages = new ArrayList<String>();
        List<String> failedFields = new ArrayList<String>();
        boolean needToAddToChecklist = false;
        Reflections reflection = new Reflections(
                ClasspathHelper
                        .forPackage("com.swiggy.got.pages"));
        Set<Class<? extends WebPage>> classes = reflection
                .getSubTypesOf(WebPage.class);
        for (Class<? extends WebPage> singlePage : classes)
        {
            Field[] fields = singlePage.getDeclaredFields();
            for (Field singleField : fields)
            {
                needToAddToChecklist = false;
                if (IElement.class.isAssignableFrom(singleField.getType()))
                {
                    needToAddToChecklist = true;
                } else if (List.class.isAssignableFrom(singleField.getType()))
                {
                    ParameterizedType parameterizedType = (ParameterizedType) singleField
                            .getGenericType();
                    Class<?> actualClass = (Class<?>) parameterizedType
                            .getActualTypeArguments()[0];
                    if (actualClass.equals(IElement.class))
                    {
                        needToAddToChecklist = true;
                    }
                }
                if (needToAddToChecklist)
                {
                    allFieldsFromPages.add(singlePage.getSimpleName() + "."
                            + singleField.getName());
                }
            }
        }
        for (String singleUIElement : allFieldsFromPages)
        {
            if (null == initialize.UIObjects
                    .GetUIElementDetails(singleUIElement))
            {
                // Assert.fail("Issue with " + singleUIElement);
                failedFields.add(singleUIElement);
            }
        }
        if (failedFields.size() != 0)
        {
            isValid = false;
        }
        System.out.println("==========ERRONEOUS UIELEMENTS==========");
        for (String singleUIElement : failedFields)
        {
            System.out.println(singleUIElement);
        }
        System.out.println("========================================");
        return isValid;
    }
*/

    public void deleteFile(String Path)
    {
        File file = new File(Path);

        if (file.exists())
        {
            file.delete();
        }
    }

    public static int compareDates(Date cDate)
    {
        Calendar cal1 = Calendar.getInstance();
        Date sDate = cal1.getTime();
        System.out.println("currentDate..........." + sDate);
        System.out.println("couponDate............" + cDate);
        return cDate.compareTo(sDate);
    }

    public static int getAnyNumberBetweenZeroAnd(int max)
    {
        return new Random().nextInt(max);
    }

}
