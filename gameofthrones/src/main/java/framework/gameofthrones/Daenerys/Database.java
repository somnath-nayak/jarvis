package framework.gameofthrones.Daenerys;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by ashish.bajpai on 05/02/15.
 */
@XmlRootElement(name = "database")
public class Database
{
    private Connectionstring connectionstrings;

    public Connectionstring getConnectionstrings() {
        return connectionstrings;
    }
    
    @XmlElement(name = "connectionstrings")
    public void setConnectionstrings(Connectionstring connectionstrings) {
        this.connectionstrings = connectionstrings;
    }
}
