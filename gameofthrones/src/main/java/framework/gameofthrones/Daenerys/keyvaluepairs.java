package framework.gameofthrones.Daenerys;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="keyvaluepairs")
public class keyvaluepairs {
	List<keyvaluepair> keyvaluepair;

	public List<keyvaluepair> getKeyvaluepair() {
		return keyvaluepair;
	}

	@XmlElement
	public void setKeyvaluepair(List<keyvaluepair> keyvaluepair) {
		this.keyvaluepair = keyvaluepair;
	}
}
