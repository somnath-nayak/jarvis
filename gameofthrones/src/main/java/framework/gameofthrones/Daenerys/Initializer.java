package framework.gameofthrones.Daenerys;

import framework.gameofthrones.Aegon.Initialize;

public class Initializer {

    private static Initialize init;

    public static Initialize getInitializer()
    {
        if(init==null)
        {
            init=new Initialize();
            return init;
        }
        return init;

    }
}
