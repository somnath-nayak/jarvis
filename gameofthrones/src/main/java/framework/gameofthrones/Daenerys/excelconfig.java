package framework.gameofthrones.Daenerys;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="excelconfig")
public class excelconfig {
	
 private List<Excel>excel;

public List<Excel> getExcel() {
	return excel;
}

@XmlElement
public void setExcel(List<Excel> excel) {
	this.excel = excel;
}	
}
