package framework.gameofthrones.Daenerys;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by ashish.bajpai on 05/02/15.
 */
@XmlRootElement(name = "connectionstrings")
public class Connectionstring
{
    private List<Connstring> connectiondetails;
    private String name;
    
    
    public List<Connstring> getConnectionstring() {
        return connectiondetails;
    }
    
    @XmlElement(name = "connectiondetails")
    public void setConnectionstring(List<Connstring> connectionstring) {
        this.connectiondetails = connectionstring;
    }
    
    @XmlAttribute
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Connstring getDBDetails(String name){
        Connstring conn = new Connstring();
        for (Connstring connstring: connectiondetails) {
            if (connstring.getName().trim().toLowerCase().equals(name)){
                conn = connstring;
            }
        }
        return conn;
    }

}
