package framework.gameofthrones.Daenerys;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "suiteconf")
public class Suiteconf {
	String inputexceldatafilepath;
	String sheetname;

	public String getSheetname() {
		return sheetname;
	}
	@XmlAttribute
	public void setSheetname(String sheetname) {
		this.sheetname = sheetname;
	}
	public String getInputexceldatafilepath() {
		return inputexceldatafilepath;
	}
	@XmlAttribute
	public void setInputexceldatafilepath(String inputexceldatafilepath) {
		this.inputexceldatafilepath = inputexceldatafilepath;
	}
}
