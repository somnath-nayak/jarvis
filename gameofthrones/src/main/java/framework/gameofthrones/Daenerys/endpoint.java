package framework.gameofthrones.Daenerys;

import javax.xml.bind.annotation.XmlAttribute;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by ashish.bajpai on 10/02/17.
 */

public class endpoint {

    private String  core, servicename,baseurl,authenticationrequired,userid,password,securitytoken,stafservicename;
    
    public String getStafservicename() {
		return stafservicename;
	}

    @XmlAttribute
	public void setStafservicename(String stafservicename) {
		this.stafservicename = stafservicename;
	}

	private int port;

    public endpoint(String _core, String _servicename, String _baseurl, String _authenticationrequired,int port, String _userid, String _password, String _securitytoken)
    {
        setServicename(_servicename);
        setCore(_core);
        setBaseurl(_baseurl);
        setAuthenticationrequired(_authenticationrequired);
        setUserid(_userid);
        setPassword(_password);
        setServicename(_securitytoken);
    }

    public endpoint()
    {

    }
    @XmlAttribute
    public void setCore(String core) {
        this.core = core;
    }

    public String getCore() {
        return core;
    }
    
    @XmlAttribute
    public void setPort(int port) {
        this.port = port;
    }

    public int getPort() {
        return port;
    }

    @XmlAttribute
    public void setServicename(String servicename) {
        this.servicename = servicename;
    }

    public String getServicename() {
        return servicename;
    }
    @XmlAttribute
    public void setAuthenticationrequired(String authenticationrequired) {
        this.authenticationrequired = authenticationrequired;
    }
    @XmlAttribute
    public void setBaseurl(String baseurl) {
        this.baseurl = baseurl;
    }
    @XmlAttribute
    public void setPassword(String password) {
        this.password = password;
    }
    @XmlAttribute
    public void setSecuritytoken(String securitytoken) {
        this.securitytoken = securitytoken;
    }
    @XmlAttribute
    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getAuthenticationrequired() {
        return authenticationrequired;
    }

    public String getBaseurl() {
        return baseurl;
    }

    public String getPassword() {
        return password;
    }

    public String getSecuritytoken() {
        return securitytoken;
    }

    public String getUserid() {
        return userid;
    }

    public List<api> GetAPIs(String APIName){

        return GetAllAPIs("pgresponse");
    }

    public api getAPIDetails(endpoint endpoint, String APIName){
        List<api> apis = new ArrayList<api>();

        Iterator<api> itr = apis.iterator();
        api returnapi = null;
        while (itr.hasNext()){
            System.out.println("itr = " + itr.next().getApiname());
            api apidetails = itr.next();
            if(apidetails.getApiname().equals(APIName)){
                System.out.println("apidetails = " + apidetails.getPath().toString());
                returnapi = apidetails;
            }
        }
        return returnapi;
    }

    private List<api> GetAllAPIs(String ServiceName){
        List<api> apis = new ArrayList<api>();
        Iterator<api> itr = apis.iterator();
        while (itr.hasNext()){
            api apidetails = itr.next();
            if(apidetails.getServicename().equals(ServiceName)){

                api apidata = apidetails;
            }
        }
        return  apis;
    }
}
