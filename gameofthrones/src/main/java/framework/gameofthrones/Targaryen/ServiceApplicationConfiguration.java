package framework.gameofthrones.Targaryen;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * @author abhijit.p
 * @since 12 Feb 2018
 */
@Configuration
@ImportResource({"classpath:applicationContext.xml"})
public class ServiceApplicationConfiguration {


}
