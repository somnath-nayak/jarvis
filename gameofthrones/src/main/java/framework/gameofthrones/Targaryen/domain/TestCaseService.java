package framework.gameofthrones.Targaryen.domain;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

/**
 * @author abhijit.p
 * @since 12 Feb 2018
 */
public interface TestCaseService {

    @Transactional(rollbackFor=Exception.class)
    Testcase addTestCase(Testcase testcase);

    @Transactional(rollbackFor=Exception.class)
    Testsuite addTestSuite(Testsuite testcase, boolean update);

    @Transactional(rollbackFor=Exception.class)
    TestResult addTestResult(TestResult result);

    @Transactional(rollbackFor=Exception.class)
    TestRun addUpdateTestRun(TestRun testrun, boolean updateProgress);

    @Transactional(rollbackFor=Exception.class)
    Testcase updateTestCase(Testcase testcase);

    Testsuite getTestsuite(Long suiteid);

    List<TestConfig> getAllTestconfig();

    TestConfig getTestconfig(Long id);

    List<Testcase> getManualTestCases(Long suiteid);

    List<Testcase> getTestcaseByTestName(String testname);

    Testcase getTestcaseByTestId(Long id);

    List<Testcase> getAllTestcasesBySuiteName(String testsuite);

    List<Testcase> getAllTestcases();

    List<Testcase> getTestcasesBySuiteId(Long testsuiteId);

    Testsuite getTestsuiteBySuiteName(String suitename);

    List<TestResult> getTestResults(Long testcaseid);

    List<TestResult> getTestResultByRunIdStatus(Long runId, Integer status);

    List<TestRun> getTestRunsForSuiteId(Long suiteId);

    Testcase getTestcaseBySuiteIdAndTestName(Long testSuiteId, String testMethodName);

    List<Testsuite> getAllSuites();

    List<TestResult> getTestresult(Long testcaseid, Integer count, Integer page);

    String getEnvironment();

    List<TestRun> getTestRunsForSuiteId(Long suiteId, Integer count, Integer page);

    List<TestRun> getTestRunsForSuiteIdEnv(Long suiteId, Integer count, Integer page, String env);

    TestRun getTestRunsForRunId(Long runId);

    @Transactional(rollbackFor=Exception.class)
    TestResult marktestPass(Long resultid, String testcomment, String userid);

    @Transactional(rollbackFor=Exception.class)
    TestResult marktestFail(Long resultid, String testcomment, String userid);

    List<Team> getTeams(boolean enabled);

    Long getTestrunCount();

    Long getTestresultCount();

    Long getTestcaseCount();

    @Transactional(rollbackFor=Exception.class)
    TestConfig addUpdateTestConfig(TestConfig testConfig);

    List<Environment> getEnvironments();

    @Transactional(rollbackFor=Exception.class)
    void addEnvironments(String environment);

}
