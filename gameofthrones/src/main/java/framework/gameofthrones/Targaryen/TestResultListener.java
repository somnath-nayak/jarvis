package framework.gameofthrones.Targaryen;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.RetryAnalyzer;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Targaryen.domain.*;
import framework.gameofthrones.Targaryen.entity.TEST_STATUS;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.internal.ConfigurationMethod;
import org.testng.internal.IResultListener2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author abhijit.p
 * @since 12 Feb 2018
 */
public class TestResultListener implements IResultListener2 {


    private static Logger log = LoggerFactory.getLogger(TestResultListener.class);
    public static AnnotationConfigApplicationContext testContext = null;
    private static TestCaseService testService = null;
    private TestRun testrun = null;
    private static AtomicInteger totalTests = new AtomicInteger(0);
    private static AtomicInteger passedTests = new AtomicInteger(0);
    private static AtomicInteger failedTests = new AtomicInteger(0);
    private static AtomicInteger skippedTests = new AtomicInteger(0);
    private static AtomicInteger manualTests = new AtomicInteger(0);
    private static Testsuite suite;
    private static final boolean updateProgress = true;
    private static Map<String, Testcase> testcaseNameTestcaseMap = new HashMap<>();


    @Override
    public void onTestStart(ITestResult iTestResult) {
        if(isEnabled(suite)){
            try {
                if (!(iTestResult.getMethod() instanceof ConfigurationMethod)) {
                    String testMethodName = iTestResult.getMethod().getMethodName();
                    Testcase testcase = new Testcase();
                    testcase.setDescription(iTestResult.getMethod().getDescription());
                    testcase.setEnabled(iTestResult.getMethod().getEnabled());
                    testcase.setTestcaseMethodName(testMethodName);
                    testcase.setTestsuitename(iTestResult.getTestContext().getSuite()
                            .getName());

                    String[] groups = iTestResult.getMethod().getGroups();

                    if(groups != null && groups.length > 0){
                        testcase.setGroups(StringUtils.join(groups, ",").toLowerCase());
                    }
                    testcase = testService.addTestCase(testcase);
                    testcaseNameTestcaseMap.put(testcase.getTestcaseMethodName(),
                            testcase);
                }

            } catch (Exception ex) {
                log.error("Error getting test service {}", ex);
            }
        }
    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {
        if(isEnabled(suite)){
            try {
                passedTests.incrementAndGet();
                totalTests.incrementAndGet();
                TestResult testResult = getTestResult(iTestResult);
                testService.addTestResult(testResult);
                updateTestRunStats();
                log.info(iTestResult.getMethod().getMethodName() + " -- PASSED");
            } catch (Exception ex) {
                log.error("Error getting test service {}", ex);
            }
        }
    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        if(iTestResult.getMethod().getRetryAnalyzer() != null  &&  iTestResult.getMethod().getRetryAnalyzer() instanceof RetryAnalyzer){
            RetryAnalyzer retryAnalyzer = (RetryAnalyzer) iTestResult.getMethod().getRetryAnalyzer();
            if(retryAnalyzer.isRetryAvailable()){
                log.info("Retry available, not logging failed result to DB for Method " + iTestResult.getMethod().getMethodName());
                return;
            }
        }

        if(isEnabled(suite)){
            try {
                failedTests.incrementAndGet();
                totalTests.incrementAndGet();
                TestResult testResult = getTestResult(iTestResult);
                testService.addTestResult(testResult);
                updateTestRunStats();
                log.info(iTestResult.getMethod().getMethodName() + " -- FAILED");
            } catch (Exception ex) {
                log.error("Error getting test service {}", ex);
            }
        }
    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {
        if(iTestResult.getMethod().getRetryAnalyzer() != null  &&  iTestResult.getMethod().getRetryAnalyzer() instanceof RetryAnalyzer){
            RetryAnalyzer retryAnalyzer = (RetryAnalyzer) iTestResult.getMethod().getRetryAnalyzer();
            if(retryAnalyzer.isRetryAvailableForValidateSkipTest()){
                log.info("Retry available, not logging failed result to DB for Method " + iTestResult.getMethod().getMethodName());
                return;
            }
        }
        if(isEnabled(suite)){
            try {
                onTestStart(iTestResult);
                skippedTests.incrementAndGet();
                totalTests.incrementAndGet();
                TestResult testResult = getTestResult(iTestResult);
                testService.addTestResult(testResult);
                updateTestRunStats();
                log.info(iTestResult.getMethod().getMethodName() + " -- SKIPPED");
            } catch (Exception ex) {
                log.error("Error getting test service {}", ex);
            }
        }
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {

    }

    @Override
    public void onStart(ITestContext iTestContext) {
        init(iTestContext);
    }

    @Override
    public void onFinish(ITestContext iTestContext) {
        if(isEnabled(suite)){
            try {
                testrun.setStatus(TEST_STATUS.ENDED.toString());
                testrun.setTotalPassed(passedTests.get());
                testrun.setTotalFailed(failedTests.get());
                testrun.setTotalSkipped(skippedTests.get());
                testrun.setTotalTests(totalTests.get());
                testrun = testService.addUpdateTestRun(testrun, updateProgress);
            } catch (Exception ex) {
                log.error("Error getting test service {}", ex);
            }
        }
    }

    private TestResult getTestResult(ITestResult result) {
        if(isEnabled(suite)){
            try {
                TestResult testResult = new TestResult();
                String suiteName = result.getTestContext().getSuite().getName();
                String testMethod = result.getName();

                Testsuite suite = testService.getTestsuiteBySuiteName(suiteName);
                Testcase testcase = testService.getTestcaseBySuiteIdAndTestName(
                        suite.getId(), testMethod);
                testResult.setTestcaseid(testcase.getId());
                testResult.setResult(result.getStatus());
                testResult.setTestrunid(testrun.getId());
                testResult.setStarttime(new Date(result.getStartMillis()));
                testResult.setEndtime(new Date(result.getEndMillis()));
                if (result.getParameters() != null
                        && result.getParameters().length > 0) {
                    testResult.setTestparams(ArrayUtils.toString(result
                            .getParameters()));
                }
                if (result.getThrowable() != null) {
                    String stTrace = ExceptionUtils.getFullStackTrace(result
                            .getThrowable());
                    testResult.setError(stTrace);
                    testResult.setErrormessage(result.getThrowable().getMessage());
                }

                return testResult;
            } catch (Exception ex) {
                log.error("Error getting test service {}", ex);
            }
        }
        return null;
    }

    private void init(ITestContext tContext) {
        String os = System.getProperty("os.name");
        log.info("OS is " + os);
        if (testService == null || testrun == null) {
            try {
                testContext = new AnnotationConfigApplicationContext(
                        ServiceApplicationConfiguration.class);
                testService = (TestCaseService) testContext
                        .getBean("testCaseService");
                String suiteName = tContext.getSuite().getName();
                if (suiteName != null && !suiteName.equals("Default suite")) {
                    String branchname = null;
                    if (os.contains("Windows 7")) {
                        branchname = exec(new String[] { "git describe --all --exact-match 2>/dev/null | sed 's=.*/=='" });
                    }else{
                        branchname = exec(new String[] { "/bin/sh", "-c", "git describe --all --exact-match 2>/dev/null | sed 's=.*/=='" });
                    }
                    String team = tContext.getSuite().getParameter("team");
                    String core = tContext.getSuite().getParameter("core");
                    String type = tContext.getSuite().getParameter("type");

                    suite = new Testsuite();
                    suite.setSuitename(suiteName);
                    suite.setEnabled(Boolean.TRUE);
                    if (StringUtils.isNotBlank(team)) {
                        suite.setTeam(team);
                        if (StringUtils.isNotBlank(core)) {
                            suite.setCore(core);
                            if (StringUtils.isNotBlank(type)) {
                                suite.setType(type);
                            }
                        }
                    }

                    suite = testService.addTestSuite(suite,
                            true);
                    if(suite == null){
                        return;
                    }
                    TestRun testrun;
                    List<TestRun> latesttestrun = testService.getTestRunsForSuiteIdEnv(suite.getId(), 1, 0,testService.getEnvironment());
                    testService.addEnvironments(testService.getEnvironment());
                    testrun = new TestRun();
                    testrun.setTestsuiteidfk(suite.getId());
                    testrun.setEnvironment(testService.getEnvironment());
                    testrun.setTotalTests(totalTests.get());
                    testrun.setTotalPassed(passedTests.get());
                    testrun.setTotalFailed(failedTests.get());
                    testrun.setTotalSkipped(skippedTests.get());
                    testrun.setBranch(branchname);
                    testrun.setStatus(TEST_STATUS.RUNNING.toString());
                    this.testrun = testService.addUpdateTestRun(testrun,
                            updateProgress);
                    SystemConfigProvider.setRunID(this.testrun.getId());
                    log.debug("Run ID :: "+ this.testrun.getId());
                }
            } catch (Exception ex) {
                log.error("Error getting test service {}", ex);
            }
        }
    }

    private synchronized void updateTestRunStats() {
        if(isEnabled(suite)){
            try {
                this.testrun.setTotalPassed(passedTests.get());
                this.testrun.setTotalFailed(failedTests.get());
                this.testrun.setTotalSkipped(skippedTests.get());
                this.testrun.setTotalTests(totalTests.get());
                this.testrun.setTotalManual(manualTests.get());
                this.testrun = testService.addUpdateTestRun(this.testrun, false);
            } catch (Exception ex) {
                log.error("Error getting test service {}", ex);
            }
        }
    }

    public static String exec(String[] cmd) throws IOException {

        try {
            String output;
            String error = null;
            ProcessBuilder pBuilder = new ProcessBuilder(cmd);
            Process p = pBuilder.start();
            BufferedReader outputReader = new BufferedReader(
                    new InputStreamReader(p.getInputStream()));
            BufferedReader errorReader = new BufferedReader(
                    new InputStreamReader(p.getErrorStream()));
            output = outputReader.readLine();
            error = errorReader.readLine();

            if (output != null && !output.isEmpty()) {
                return output.trim();
            } else if (error != null && !error.isEmpty()) {
                return error.trim();
            }
            return null;
        } catch (Exception ex) {
            return "";
        }

    }


    public static void sleep(int time, TimeUnit unit) {
        try {
            Thread.sleep(TimeUnit.MILLISECONDS.convert(time, unit));
        } catch (InterruptedException e) {
            log.error("Thread interrupted");
        }
    }

    private boolean isEnabled(Testsuite suite){
        return suite != null && suite.getEnabled();
    }

    @Override
    public void beforeConfiguration(ITestResult iTestResult) {

    }

    @Override
    public void onConfigurationSuccess(ITestResult iTestResult) {

    }

    @Override
    public void onConfigurationFailure(ITestResult iTestResult) {

    }

    @Override
    public void onConfigurationSkip(ITestResult iTestResult) {

    }
}
