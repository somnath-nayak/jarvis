package framework.gameofthrones.Targaryen.repository;


import framework.gameofthrones.Targaryen.entity.TestrunEntity;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author abhijit.p
 * @since 13 Feb 2018
 */
@CacheConfig(cacheNames="testruncache")
public interface TestrunRepository extends JpaRepository<TestrunEntity, Long>{
	
	List<TestrunEntity> findByTestsuiteid(Long testsuiteid);
	
	List<TestrunEntity> findByTestsuiteidAndEnvironment(Long testsuiteid, String environment, Pageable pageable);
	
	@Cacheable(keyGenerator="customKey")
	List<TestrunEntity> findByTestsuiteid(Long testsuiteid, Pageable pageable);

	@Override
	@CacheEvict(beforeInvocation=true,allEntries=true)
	<S extends TestrunEntity> S save(S entity);
	
	@Override
	@CacheEvict(beforeInvocation=true, allEntries=true)
	<S extends TestrunEntity> S saveAndFlush(S entity);
	
	@Override
	@Cacheable
	long count();
}
