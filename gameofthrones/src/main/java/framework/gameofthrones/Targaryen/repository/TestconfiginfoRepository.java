package framework.gameofthrones.Targaryen.repository;


import framework.gameofthrones.Targaryen.entity.TestConfigInfoEntity;
import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * @author abhijit.p
 * @since 13 Feb 2018
 */
public interface TestconfiginfoRepository extends JpaRepository<TestConfigInfoEntity, Long>{

	@Override
	Optional<TestConfigInfoEntity> findById(Long id);
	
	List<TestConfigInfoEntity> findByTestconfigidfk(Long testconfig_id_fk);
	
	@Override
	<S extends TestConfigInfoEntity> S save(S entity);
	
	@Override
	<S extends TestConfigInfoEntity> S saveAndFlush(S entity);
	
	@Override
	void delete(TestConfigInfoEntity entity);
	
}
