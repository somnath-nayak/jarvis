package framework.gameofthrones.Targaryen.repository;


import framework.gameofthrones.Targaryen.entity.TestsuiteEntity;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

/**
 * @author abhijit.p
 * @since 13 Feb 2018
 */
@CacheConfig(cacheNames="testsuitecache")
public interface TestsuiteRepository extends JpaRepository<TestsuiteEntity,Long>{
	
	String FIND_SUITE_NAMES = "Select distinct(ts.team) from testsuite ts where ts.team IS NOT NULL";
	
	@Cacheable(key="#p0")
    TestsuiteEntity findByTestsuitenameIgnoreCase(String testsuitename);

	@Override
	@Cacheable(key="#p0")
	Optional<TestsuiteEntity> findById(Long id);
	
	@Override
	@Cacheable(keyGenerator="customKey")
	List<TestsuiteEntity> findAll();
	
	@Cacheable(keyGenerator="customKey")
	@Query(value=FIND_SUITE_NAMES,nativeQuery = true)
	List<String> allTeams();
	
	@Override
	@CacheEvict(beforeInvocation=true, allEntries=true)
	<S extends TestsuiteEntity> S save(S entity);
	
	@Override
	@CacheEvict(beforeInvocation=true, allEntries=true)
	<S extends TestsuiteEntity> S saveAndFlush(S entity);
	
	@Override
	@CacheEvict(beforeInvocation=true, allEntries=true)
	void delete(TestsuiteEntity entity);
	
}
