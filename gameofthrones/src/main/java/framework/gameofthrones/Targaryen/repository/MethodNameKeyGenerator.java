package framework.gameofthrones.Targaryen.repository;

import java.lang.reflect.Method;

/**
 * @author abhijit.p
 * @since 13 Feb 2018
 */
public class MethodNameKeyGenerator implements org.springframework.cache.interceptor.KeyGenerator{


	@Override
	public Object generate(Object target, Method method, Object... params) {
		String param = "";
		if(params != null && params.length > 0){
			 param = "." + params[0].toString();
		}
		return method.getDeclaringClass().getName() + "." + method.getName() + param;
	}

}
