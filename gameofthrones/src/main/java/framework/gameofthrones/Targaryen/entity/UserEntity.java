package framework.gameofthrones.Targaryen.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author abhijit.p
 * @since 12 Feb 2018
 */
@Entity
@Table(name="users")
public class UserEntity extends BaseEntity{

	private static final long serialVersionUID = -2692661594180108496L;

	@Column(name = "role_id_fk", nullable = true, length=11, insertable=false, updatable=false)
    private Integer role;
	
	@Column(name = "email", nullable = false, length=100)
    private String email;
	
	@Column(name = "username", nullable = true, length=200)
    private String username;
	
	@Column(name = "is_account_nonlocked", nullable = true, length=1)
    private Boolean is_account_nonlocked;
	
	@Column(name = "is_account_nonexpired", nullable = true, length=1)
    private Boolean is_account_nonexpired;
	
	@Column(name = "enabled", nullable = true, length=1)
    private Boolean enabled;
	
	@OneToOne(fetch=FetchType.EAGER, targetEntity=RoleEntity.class)
	@JoinColumn(name="role_id_fk", referencedColumnName="id")
	private RoleEntity roles;

	public String getLogin() {
		return email;
	}

	public void setLogin(String login) {
		this.email = login;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Boolean getIs_account_nonlocked() {
		return is_account_nonlocked;
	}

	public void setIs_account_nonlocked(Boolean is_account_nonlocked) {
		this.is_account_nonlocked = is_account_nonlocked;
	}

	public Boolean getIs_account_nonexpired() {
		return is_account_nonexpired;
	}

	public void setIs_account_nonexpired(Boolean is_account_nonexpired) {
		this.is_account_nonexpired = is_account_nonexpired;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}	
	
	public void setRole(Integer role) {
		this.role = role;
	}

	
	/**
	 * A default role "USER" is applied to all logged in users.
	 */
	public List<String> getRoles() {
		List<String> roleslist = new ArrayList<String>();
		roleslist.add("USER");
		if(roles != null){
			roleslist.add(roles.getRole());
		}
		return roleslist;
	}
	
	
}
