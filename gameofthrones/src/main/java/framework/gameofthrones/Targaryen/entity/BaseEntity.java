package framework.gameofthrones.Targaryen.entity;


import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import org.hibernate.annotations.GenericGenerator;


/**
 * @author abhijit.p
 * @since 13 Feb 2018
 */
@MappedSuperclass
public abstract class BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, insertable = false)
    protected Long id;

    @Column(name = "created_by", updatable = false, length = 50)
    protected String createdBy;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_on", nullable = false, insertable = true, updatable = false)
    protected Date createdOn;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_modified_on", nullable = false, insertable = true, updatable = true)
    protected Date lastModifiedOn;
    @Version
    @Column(name = "version")
    protected Long version = 0L;

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    @PrePersist
    protected void onCreate() {
        lastModifiedOn = createdOn = new Date();
        createdBy = (createdBy == null || createdBy.equals("")) ? "System" : createdBy;
    }

    @PreUpdate
    protected void onUpdate() {
        lastModifiedOn = new java.util.Date();
    }

    /**
     * @return the created
     */
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the created to set
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return the updated
     */
    public Date getLastModifiedOn() {
        return lastModifiedOn;
    }

    /**
     * @param lastModifiedOn the updated to set
     */
    public void setLastModifiedOn(Date lastModifiedOn) {
        this.lastModifiedOn = lastModifiedOn;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public BaseEntity(Long id, String createdBy) {
        super();
        this.id = id;
        this.createdBy = createdBy;
    }

    public BaseEntity(Long id, String createdBy, Date createdOn) {
        super();
        this.id = id;
        this.createdBy = createdBy;
        this.createdOn = createdOn;
    }

    public BaseEntity() {
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("BaseEntity [id=").append(id).append(", createdBy=").append(createdBy).append(", createdOn=").append(createdOn).append(", lastModifiedOn=").append(lastModifiedOn).append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

}
