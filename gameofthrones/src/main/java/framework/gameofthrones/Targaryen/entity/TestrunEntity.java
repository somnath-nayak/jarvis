package framework.gameofthrones.Targaryen.entity;


import javax.persistence.*;

/**
 * @author abhijit.p
 * @since 12 Feb 2018
 */
@Entity
@Table(name="testrun")
public class TestrunEntity extends BaseEntity{

	private static final long serialVersionUID = 2951583501593164508L;
	
	@Column(name = "status", nullable = false, length=255)
    private String teststatus;
	
	@Column(name = "testsuite_id_fk", nullable = false, length=20, insertable=true, updatable=true)
	@JoinColumn(columnDefinition="testsuite_id_fk", name="testsuite_id_fk", referencedColumnName="id")
	private Long testsuiteid;
	
	@ManyToOne(targetEntity=TestsuiteEntity.class, fetch=FetchType.EAGER, cascade = CascadeType.REFRESH)
	@JoinColumn(columnDefinition="testsuite_id_fk", name="testsuite_id_fk", referencedColumnName="id", insertable=false, updatable=false)
	private TestsuiteEntity testsuite;
	

	@Column(name = "env", nullable = true, length=255)
    private String environment;
	
	@Column(name = "failure", nullable = true, length=2000)
    private String failure;
	

	@Column(name = "passed", nullable = false, length=11)
    private Integer passed;
	
	@Column(name = "failed", nullable = false, length=11)
    private Integer failed;
	
	@Column(name = "skipped", nullable = false, length=11)
    private Integer skipped;
	
	@Column(name = "manual", nullable = false, length=11)
    private Integer manual;
	
	@Column(name = "total", nullable = false, length=11)
    private Integer totaltests;
	
	@Column(name = "branch", nullable = true, length=100)
    private String branch;
	
	public Integer getTotaltests() {
		return totaltests;
	}

	public void setTotaltests(Integer totaltests) {
		this.totaltests = totaltests;
	}

	public TEST_STATUS getTeststatus() {
		return TEST_STATUS.getStatus(teststatus);
	}

	public void setTeststatus(TEST_STATUS teststatus) {
		this.teststatus = teststatus.toString();
	}

	public Long getTestsuiteid() {
		return testsuiteid;
	}

	public void setTestsuiteid(Long testsuiteid) {
		this.testsuiteid = testsuiteid;
	}

	public String getEnvironment() {
		return environment;
	}

	public void setEnvironment(String environment) {
		this.environment = environment;
	}

	public Integer getPassed() {
		return passed;
	}

	public void setPassed(Integer passed) {
		this.passed = passed;
	}

	public Integer getFailed() {
		return failed;
	}

	public void setFailed(Integer failed) {
		this.failed = failed;
	}

	public Integer getSkipped() {
		return skipped;
	}

	public void setSkipped(Integer skipped) {
		this.skipped = skipped;
	}

	public String getFailure() {
		return failure;
	}

	public void setFailure(String failure) {
		this.failure = failure;
	}
	
	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}
	
	public Integer getManual() {
		return manual;
	}

	public void setManual(Integer manual) {
		this.manual = manual;
	}
	
	public TestsuiteEntity getTestsuite() {
		return testsuite;
	}
	
}
