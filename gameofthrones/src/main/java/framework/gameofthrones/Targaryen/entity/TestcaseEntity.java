package framework.gameofthrones.Targaryen.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * @author abhijit.p
 * @since 12 Feb 2018
 */


@Entity
@Table(name="testcase", uniqueConstraints={@UniqueConstraint(columnNames = {"testcase_id" , "testsuite_id_fk"})})
public class TestcaseEntity extends BaseEntity{
	
	private static final long serialVersionUID = -8222519245081378466L;
	
	@Column(name = "testcase_id", nullable = false, length=200)
    private String testcasename;
	
	
	@Column(name = "testsuite_id_fk", nullable = false, length=20)
	@JoinColumn(table="testsuite",nullable=false,referencedColumnName="id", insertable=true, updatable=false)
	private Long testsuiteid;
	
	
	@Column(name = "enabled", nullable = false)
	private Boolean enabled;
	
	@Column(name = "isManual", nullable = false)
	private Boolean isManual;
	

	@Column(name = "description", nullable = true, length=2000)
	private String description;

	@ManyToOne(targetEntity=TestsuiteEntity.class, fetch=FetchType.EAGER, cascade = CascadeType.REMOVE)
	@JoinColumn(nullable=false, referencedColumnName="id", name="testsuite_id_fk",insertable=false, updatable=false)
	private TestsuiteEntity testsuite;
	
	@Column(name = "groups", nullable = true, length=500)
    private String groups;


	public String getTestcase_id() {
		return testcasename;
	}

	public void setTestcase_id(String testcase_id) {
		this.testcasename = testcase_id;
	}

	public Long getTestsuiteid() {
		return testsuiteid;
	}

	public void setTestsuiteid(Long testsuiteid) {
		this.testsuiteid = testsuiteid;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public TestsuiteEntity getTestsuite() {
		return testsuite;
	}
	
	public Boolean getIsManual() {
		return isManual;
	}

	public void setIsManual(Boolean isManual) {
		this.isManual = isManual;
	}
	
	public String getGroups() {
		return groups;
	}

	public void setGroups(String groups) {
		this.groups = groups;
	}
	
}
