package framework.gameofthrones.Targaryen.entity;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;
import java.util.List;

/**
 * @author abhijit.p
 * @since 12 Feb 2018
 */
@Entity
@Table(name="testconfig")
public class TestConfigEntity  extends BaseEntity{

	private static final long serialVersionUID = 4776905374484890653L;
	
	@Column(name = "environment", nullable = true, length=255)
	private String environment;
	
	@Column(name = "testsuite_id_fk", nullable = false, length=20, insertable=true, updatable=true)
	private Long testsuiteid;
	
	@Column(name = "hour", nullable = true, length=11, insertable=true, updatable=true)
	private Integer hour;
	
	@Column(name = "minute", nullable = true, length=11, insertable=true, updatable=true)
	private Integer minute;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "start_time", nullable = true, insertable=true, updatable=true)
	private Date starttime;
	

	@OneToMany(fetch=FetchType.EAGER, targetEntity=TestConfigInfoEntity.class)
	@JoinTable(name="testconfiginfo", joinColumns= @JoinColumn(name="testconfig_id_fk"), inverseJoinColumns = @JoinColumn(name = "id"))
	private List<TestConfigInfoEntity> testconfiginfos;

	public List<TestConfigInfoEntity> getTestconfigInfos() {
		return testconfiginfos;
	}
	
	public Long getTestsuiteid() {
		return testsuiteid;
	}

	public void setTestsuiteid(Long testsuiteid) {
		this.testsuiteid = testsuiteid;
	}
	
	public String getEnvironment() {
		return environment;
	}

	public void setEnvironment(String environment) {
		this.environment = environment;
	}
	
	public Date getStarttime() {
		return starttime;
	}

	public void setStarttime(Date starttime) {
		this.starttime = starttime;
	}
	
	public Integer getHour() {
		return hour;
	}

	public void setHour(Integer hour) {
		this.hour = hour;
	}

	public Integer getMinute() {
		return minute;
	}

	public void setMinute(Integer minute) {
		this.minute = minute;
	}

	
}
