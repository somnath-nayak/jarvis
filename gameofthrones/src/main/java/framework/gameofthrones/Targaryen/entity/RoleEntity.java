package framework.gameofthrones.Targaryen.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * @author abhijit.p
 * @since 12 Feb 2018
 */


@Entity
@Table(name="roles")
public class RoleEntity extends BaseEntity{

	private static final long serialVersionUID = 7098589062756864992L;

	@Column(name = "role", nullable = true, length=50)
    private String role;

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
	
}
