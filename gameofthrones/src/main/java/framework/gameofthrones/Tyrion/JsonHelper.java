package framework.gameofthrones.Tyrion;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.DeserializationConfig.Feature;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.type.TypeReference;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class JsonHelper {

    /**
     * Convert JavaObject to JSON
     *
     * @param className
     * @return String
     * @throws IOException
     * @throws JsonMappingException
     * @throws JsonGenerationException
     */
    public String getObjectToJSON(Object className) throws IOException {
        String json = null;
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(Feature.FAIL_ON_UNKNOWN_PROPERTIES, true);
        mapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
        json = mapper.writeValueAsString(className);
        return json;

    }

    public String getObjectToJSONallowNullValues(Object className) throws IOException {
        String json = null;
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        json = mapper.writeValueAsString(className);
        return json;
    }

    public Map<String, Object> getJSONToMap(String json) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> map = new HashMap<String, Object>();
        mapper.configure(SerializationConfig.Feature.AUTO_DETECT_FIELDS, true);
        map = mapper.readValue(json, new TypeReference<Map<String, String>>(){});
        return map;
    }



}
