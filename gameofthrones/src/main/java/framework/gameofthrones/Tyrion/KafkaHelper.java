package framework.gameofthrones.Tyrion;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Daenerys.Connstring;
import framework.gameofthrones.Daenerys.Initializer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import org.apache.kafka.clients.consumer.Consumer;
import java.util.concurrent.ExecutionException;

/**
 * Created by abhijit.pati on 20/09/16.
 */
public class KafkaHelper {

    //Initialize init = new Initialize();
    Initialize init =Initializer.getInitializer();
    ProducerRecord<String,String> prodrec=null;
    KafkaProducer<String,String> producerTemplate=null;
    private KafkaTemplate getKafkaProducerTemplate(String name){
        Connstring connstring = init.EnvironmentDetails.setup.getEnvironmentData().getKafkaConnection().getConnectionstrings().getDBDetails(name);

        Map<String, Object> props = new HashMap<>();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,connstring.getServer()+":"+connstring.getPort() );
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);

        ProducerFactory producerFactory = new DefaultKafkaProducerFactory(props);
        KafkaTemplate kafkaTemplate = new KafkaTemplate(producerFactory);
        return kafkaTemplate;
    }

    public void sendMessage(String name,String topicName, String msg) {
        System.out.println("inside send message");
        ListenableFuture<SendResult> result =  getKafkaProducerTemplate(name).send(topicName, msg);
        System.out.println("Kafka Message is-----"+msg);

    }
    public Producer<Long, String> createProducer(String kafkaDb) {
        Connstring connstring = init.EnvironmentDetails.setup.getEnvironmentData().getKafkaConnection().getConnectionstrings().getDBDetails(kafkaDb);
        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, connstring.getServer()+":"+connstring.getPort());
        props.put(ProducerConfig.CLIENT_ID_CONFIG, "KafkaExampleProducer");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
                LongSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
                StringSerializer.class.getName());
        props.put(ProducerConfig.BATCH_SIZE_CONFIG,1);
        return new KafkaProducer<>(props);
    }

    public void runProducer(String name,String topic,String msg) throws Exception {
        final Producer<Long, String> producer = createProducer(name);
        long time = System.currentTimeMillis();

        try {
            final ProducerRecord<Long, String> record = new ProducerRecord<>(topic, msg);

            RecordMetadata metadata = producer.send(record).get();

            long elapsedTime = System.currentTimeMillis() - time;
            System.out.printf("sent record(key=%s value=%s) " +
                            "meta(partition=%d, offset=%d) time=%d\n",
                    record.key(), record.value(), metadata.partition(),
                    metadata.offset(), elapsedTime);
        }
        catch(Exception e){
            e.printStackTrace();

        }
        finally {
            producer.flush();
            producer.close();

        }

    }

    public Consumer<Long, String> createConsumer(String name, String topicName) {
        Connstring connstring = init.EnvironmentDetails.setup.getEnvironmentData().getKafkaConnection().getConnectionstrings().getDBDetails(name);
        Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, connstring.getServer()+":"+connstring.getPort());
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "consumer_group");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, LongDeserializer.class.getName());
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, 10);
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");
        Consumer<Long, String> consumer = new KafkaConsumer<>(props);
        consumer.subscribe(Collections.singletonList(topicName));
        return consumer;
    }

    public ConsumerRecords<Long, String> runConsumer(Consumer<Long, String> consumer) {
        ConsumerRecords<Long, String> consumerRecords;
        consumerRecords = consumer.poll(10000);
        consumer.commitAsync();
        consumer.close();
        return consumerRecords;
    }

    public void  sendMessagewithKey(String topicName,String batch_id,String value){
        prodrec=new ProducerRecord<>(topicName,batch_id,value);
        RecordMetadata metadata = null;
        try {
            metadata = producerTemplate.send(prodrec).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        System.out.printf("Sent record to Kafka (key=%s value=%s) " + "meta(partition=%d, offset=%d) time=%d\n",
                prodrec.key(), prodrec.value(), metadata.partition(),
                metadata.offset(), System.currentTimeMillis());
    }

    public void  pushMessage(String topicName,String value){
        prodrec=new ProducerRecord<>(topicName,value);
        RecordMetadata metadata = null;
        try {
            metadata = producerTemplate.send(prodrec).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        System.out.printf("Sent record to Kafka (key=%s value=%s) " + "meta(partition=%d, offset=%d) time=%d\n",
                prodrec.key(), prodrec.value(), metadata.partition(),
                metadata.offset(), System.currentTimeMillis());
    }

    public void createKafkaProducer(String name){
        Connstring connstring = init.EnvironmentDetails.setup.getEnvironmentData().getKafkaConnection().getConnectionstrings().getDBDetails(name);
        Properties props=new Properties() ;
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,connstring.getServer()+":"+connstring.getPort());
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.BATCH_SIZE_CONFIG,1);
        producerTemplate=new KafkaProducer<>(props);
    }
}



