package framework.gameofthrones.Tyrion;

import com.rabbitmq.client.*;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.QueueData;
import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;

public class RabbitMQHelper {


    Initialize init = new Initialize();
    String msg="";


    public Map<String, Object> getRMQConnection(String hostName, String queueName){
        Map<String,Object> rmqDetails = new HashMap<>();
        try{
            ConnectionFactory factory = new ConnectionFactory();
            QueueData qdetails = init.EnvironmentDetails.setup.GetQueueDetails(hostName, queueName);
            factory.setHost(qdetails.HostUrl);
            factory.setPort(qdetails.Port);
            if(qdetails.username != null){
                if (!qdetails.username.isEmpty()){
                    System.out.println("Setting The Username :: "+qdetails.username);
                    factory.setUsername(qdetails.username);
                }
            }
            if(qdetails.password != null){
                if(!qdetails.password.isEmpty()){
                    System.out.println("Setting The Username :: "+qdetails.password);
                    factory.setPassword(qdetails.password);
                }
            }
            Connection connection = factory.newConnection();
            Channel channel = connection.createChannel();
            rmqDetails.put("queueData", qdetails);
            rmqDetails.put("channel", channel);
            rmqDetails.put("connection", connection);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return rmqDetails;
    }

    public String getMessage(String hostName, String queueName) {
        String message=null;
        try {
            Map<String, Object> rmqDetails = getRMQConnection(hostName, queueName);
            Channel channel = (Channel)rmqDetails.get("channel");
            QueueData qdetails = (QueueData)rmqDetails.get("queueData");
            channel.queueDeclare(qdetails.Name, true, false, false, null);
            GetResponse response= channel.basicGet(qdetails.Name, false);
            message = new String(response.getBody());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return message;
    }

    public void pushMessage(String hostName, String queueName,AMQP.BasicProperties.Builder amp,  Object message) {
        try {
            Map<String, Object> rmqDetails = getRMQConnection(hostName, queueName);
            Channel channel = (Channel)rmqDetails.get("channel");
            QueueData qdetails = (QueueData)rmqDetails.get("queueData");
            Connection connection = (Connection) rmqDetails.get("connection");

            System.out.println("23333====="+qdetails.Name);
            channel.queueDeclare(qdetails.Name, true, false, false, null);
            channel.basicPublish("",qdetails.Name, amp.build(), message.toString().getBytes());
            System.out.println(" [x] Sent '" + message + "'");
            channel.close();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void pushMessageToExchange(String hostName, String queueName, AMQP.BasicProperties.Builder amp,  Object message) {
        try {
            Map<String, Object> rmqDetails = getRMQConnection(hostName, queueName);
            Channel channel = (Channel)rmqDetails.get("channel");
            QueueData qdetails = (QueueData)rmqDetails.get("queueData");
            Connection connection = (Connection)rmqDetails.get("connection");

            System.out.println("23333====="+qdetails.Name);
            //channel.queueDeclare(qdetails.Name, true, false, false, null);
            channel.basicPublish(qdetails.Exchangekey, qdetails.Routingkey, amp.build(), message.toString().getBytes());
            System.out.println(" [x] Sent '" + message + "'");
            channel.close();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Purge Queue
     * @param hostName
     * @param queueName
     */
    public void purgeQueue(String hostName, String queueName) throws IOException, TimeoutException {

        Map<String, Object> rmqDetails = getRMQConnection(hostName, queueName);
        Channel channel = (Channel)rmqDetails.get("channel");
        QueueData qdetails = (QueueData)rmqDetails.get("queueData");
        Connection connection = (Connection) rmqDetails.get("connection");

        AMQP.Queue.PurgeOk purged = channel.queuePurge(qdetails.Name);
        channel.close();
        connection.close();
    }

    private byte[] convertToBytes(Object object) throws IOException {
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
             ObjectOutput out = new ObjectOutputStream(bos)) {
            out.writeObject(object);
            return bos.toByteArray();
        }
    }

    private Object convertFromBytes(byte[] bytes) throws IOException, ClassNotFoundException {
        try (ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
             ObjectInput in = new ObjectInputStream(bis)) {
            return in.readObject();
        }
    }

}