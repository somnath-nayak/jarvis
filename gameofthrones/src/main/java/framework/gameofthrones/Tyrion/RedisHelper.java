package framework.gameofthrones.Tyrion;


import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Daenerys.Connstring;
import framework.gameofthrones.Tyrion.RedisHelper;
import io.advantageous.boon.core.Sys;
//import org.codehaus.groovy.runtime.powerassert.SourceText;
import org.springframework.data.redis.connection.RedisKeyCommands;
import framework.gameofthrones.Daenerys.Initializer;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.serializer.GenericToStringSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import redis.clients.jedis.JedisPoolConfig;

import java.util.Set;
import java.util.Map;
import java.util.*;

public class RedisHelper{

    //Initialize init = new Initialize();
    Initialize init =Initializer.getInitializer();

    public RedisTemplate getRedisTemplate(String name, int database){

        Connstring connstring = init.EnvironmentDetails.setup.getEnvironmentData().getRedisConnection().getConnectionstrings().getDBDetails(name);

        JedisConnectionFactory jedisConnectionFactory = new JedisConnectionFactory();
        if(database != 0) {
            jedisConnectionFactory.setDatabase(database); }
      // JedisPoolConfig poolConfig = new JedisPoolConfig();
      //  poolConfig.setMaxTotal(1);
      //  jedisConnectionFactory.setPoolConfig(poolConfig);
        jedisConnectionFactory.setHostName(connstring.getServer());
        jedisConnectionFactory.setPort(connstring.getPort());
        jedisConnectionFactory.setUsePool(true);
        jedisConnectionFactory.setTimeout(5000);
        jedisConnectionFactory.afterPropertiesSet();
        RedisTemplate redisTemplate = new RedisTemplate();
        redisTemplate.setConnectionFactory(jedisConnectionFactory);
        redisTemplate.setKeySerializer(new StringRedisSerializer() );
        redisTemplate.setHashValueSerializer(new GenericToStringSerializer<>(Object.class ) );
        redisTemplate.setValueSerializer(new GenericToStringSerializer<>( Object.class ) );
        redisTemplate.afterPropertiesSet();
        System.out.println(redisTemplate.toString());
        return redisTemplate;
    }

    public Object getValue( final String name, int database, final String key ) {
        RedisTemplate rTemplate = getRedisTemplate(name, database);
        return rTemplate.opsForValue().get( key );
    }

    public Object getValues( final String name, int database, final String[] keys ) {
        RedisTemplate rTemplate = getRedisTemplate(name, database);
        return rTemplate.opsForValue().multiGet( Arrays.asList(keys) );
    }


    public void setValue( final String name, int database, final String key, final int value1 ) {
        RedisTemplate rTemplate = getRedisTemplate(name, database);
        rTemplate.opsForValue().set( key, value1 );
    }

    public void deleteKey( final String name, int database, final String key ) {
        System.out.println(key);
        RedisTemplate rTemplate = getRedisTemplate(name, database);
        rTemplate.delete(key);
    }

    public Set Zrange(final String name, int database, final String key, final long start, long end  ) {
        RedisTemplate rTemplate = getRedisTemplate(name, database);
        return rTemplate.opsForZSet().range(key, start, end);
    }
    
    public void setValueJson( final String name, int database, final String key, final String value ) {
        RedisTemplate rTemplate = getRedisTemplate(name, database);
        rTemplate.opsForValue().set( key, value );
    }
    public void setValue(final String name, int database, final String key, final String value1)
    {
        RedisTemplate rTemplate= getRedisTemplate(name, database);
        rTemplate.opsForValue().set( key, value1 );
    }


//to get hash value
    public String  getHashValue(String name, int database,String hashKey, String hashValue)
    {
        RedisTemplate rTemplate= getRedisTemplate(name,database);
        Map<byte[], byte[]> o = rTemplate.getConnectionFactory().getConnection().hGetAll(hashKey.getBytes());
//        try {
//            Thread.sleep(1000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        String valuetoreturn=new String(o.get(hashValue.getBytes()));
        return valuetoreturn;
    }

    public void setHashValue(String name, int database,String hashKey, String hashValue, String value)
    {
        RedisTemplate rTemplate= getRedisTemplate(name,database);
        byte[] bhashkey  = hashKey.getBytes();
        byte[] bhashvalue  = hashValue.getBytes();
        byte[] bvaluetoset  = value.getBytes();
        rTemplate.getConnectionFactory().getConnection().hSet(bhashkey,bhashvalue,bvaluetoset);
    }

    public void deleteKeyFromHashMap(String name, int database,String hashKey, String hashValue){

        RedisTemplate rTemplate= getRedisTemplate(name,database);
        rTemplate.getConnectionFactory().getConnection().hDel(hashKey.getBytes(),hashValue.getBytes());
    }


    public void flushAll( final String name) {
        RedisTemplate rTemplate = getRedisTemplate(name, 0);
        rTemplate.getConnectionFactory().getConnection().flushAll();
    }

    public void publish(String name, String channel, String message){
        RedisTemplate redisTemplate = getRedisTemplate(name, 0);
        redisTemplate.convertAndSend(channel, message);
    }

    public Long sAdd(String name, String key, String value){
        RedisTemplate redisTemplate = getRedisTemplate(name, 0);
        return redisTemplate.opsForSet().add(key, value);
    }

    public boolean sMembers(String name, String key, String value){
        RedisTemplate redisTemplate = getRedisTemplate(name, 0);
        return redisTemplate.opsForSet().isMember(key, value);
    }
    public Set getsMembers(String dbname,String keyinitial,String entity, int redisdb){
		 RedisHelper red = new RedisHelper();
		 String s=keyinitial;
		 String key=s.concat(entity);
	          return red.getRedisTemplate(dbname, redisdb).opsForSet().members(key);
	    }

    public List<String> hKeys(String name, int database, String hashKey){
        List<String> hKeys= new ArrayList<>();
        Set<byte[]> s  = getRedisTemplate(name, database).getConnectionFactory().getConnection().hKeys(hashKey.getBytes());
        Iterator iterator = s.iterator();
        while(iterator.hasNext()){
            byte element[] = (byte[])iterator.next();
            String value = new String(element);
            hKeys.add(value);
            }
       return hKeys;
        }
    public ArrayList<String> getAllKeys(String name, int database){
        String pattern= "*";
        RedisKeyCommands redisKeyCommands = getRedisTemplate(name, database).getConnectionFactory().getConnection().keyCommands();
        Set<byte[]> s=redisKeyCommands.keys(pattern.getBytes());
        Iterator iterator = s.iterator();
        ArrayList<String> allKeys=new ArrayList<String>();
        while(iterator.hasNext()){
            byte element[] = (byte[])iterator.next();
            String value = new String(element);
            allKeys.add(value);
        }
        return allKeys;
    }

    public HashMap<String,String> hKeysValues(String name, int database, String hashKey){
        HashMap<String,String> hKeysValues= new HashMap<>();
        Map<byte[],byte[]> s  = getRedisTemplate(name, database).getConnectionFactory().getConnection().hGetAll(hashKey.getBytes());
        for (Map.Entry<byte[], byte[]> entry : s.entrySet())
        {
            hKeysValues.put(new String(entry.getKey()),new String(entry.getValue()));
        }
        return hKeysValues;
    }

    public Set<String> getAllKeysLike(String name,Integer database,String pattern)
    {
        RedisTemplate rTemplate= getRedisTemplate(name, database);
        Set<String> redisKeys = rTemplate.keys(pattern);
        return redisKeys;
    }

    public boolean checkKeyExists(String name,Integer database,String key)
    {
        RedisTemplate rTemplate= getRedisTemplate(name, database);
        return rTemplate.hasKey(key);

    }

    public Long getHLen(String name, Integer database,String hashKey ) {
        RedisTemplate redisTemplate =  getRedisTemplate(name, database);
        Long get_hlen = redisTemplate.getConnectionFactory().getConnection().hLen(hashKey.getBytes());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return get_hlen;
    }

    public void flushRedisDB(String name, Integer database) {
        RedisTemplate redisTemplate =  getRedisTemplate(name, database);
        redisTemplate.getConnectionFactory().getConnection().flushDb();
    }

}