package framework.gameofthrones.Tyrion;

import com.mysql.jdbc.PreparedStatement;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * SQL Template
 * @author abhijit.p
 */
public interface SqlTemplate {
   
    void execute(String sql);

    void executeBatch(String[] sql);

    Object queryForObject(String query, String value, Object abc);
    List<Object> queryForObjectRowMapper(String query, String value, RowMapper rowMapper);

    List<Map<String, Object>> queryForList(String query);

    int update(String query);

    void setDataSource(DataSource dataSource);

    Map<String, Object> queryForMap(String sql);

    Map<String, Object> queryForMap(String sql, @Nullable Object... args);
    List<String> queryForListFromSingleRow(String query, Object T);
    List<Map<String, Object>> queryForList(String query, Object[] param);
    int update(String query,Object[] param);
    List<String> findAll(String sql);
    Integer queryForObjectInteger(String query, Object[] param, Object abc);
    
    Boolean doInPreparedStatement(PreparedStatement ps) throws SQLException;
    
    void execute(String query,PreparedStatementCallback<Boolean> obj);
}

