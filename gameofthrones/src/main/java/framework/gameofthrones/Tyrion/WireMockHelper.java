package framework.gameofthrones.Tyrion;


import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.admin.model.ListStubMappingsResult;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.github.tomakehurst.wiremock.http.DelayDistribution;
import com.github.tomakehurst.wiremock.http.UniformDistribution;
import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

public class WireMockHelper {

    @Test
    public void startMockServer(int port) {
        WireMockServer wireMockServer = new WireMockServer(WireMockConfiguration.options().port(port));
        WireMock.configureFor("localhost", port);
        if(wireMockServer.isRunning()) {
            System.out.print("Here in if loop." + wireMockServer.toString());
            wireMockServer.stop();
        }
        wireMockServer.start();

    }

    public void startMockServer() {
        WireMockServer wireMockServer = new WireMockServer();
        if(wireMockServer.isRunning()) {
            System.out.print("Here in if loop." + wireMockServer.toString());
            wireMockServer.stop();
        }
        wireMockServer.start();
    }

    public void stopMockServer() {
        WireMockServer wireMockServer = new WireMockServer();
        wireMockServer.stop();
    }

    public void setupStub(String uri, int statusCode, String contentType, String body) {
        stubFor(get(urlMatching(".*/" + uri + ".*"))
                .willReturn(aResponse()
                        .withStatus(statusCode)
                        .withHeader("Content-Type", contentType)
                        .withBody(body)));
    }

    public void setupStub(String uri, int statusCode, String contentType, String body, int minDelay, int maxDelay) {
        DelayDistribution dd = new UniformDistribution(minDelay, maxDelay);
        stubFor(get(urlMatching(".*/" + uri + ".*"))
                .willReturn(aResponse()
                        .withRandomDelay(dd)
                        .withStatus(statusCode)
                        .withHeader("Content-Type", contentType)
                        .withBody(body)));
    }

    public void setupStub(String uri, int statusCode, String contentType, String body, int delay) {
        stubFor(get(urlMatching(".*/"+uri+".*"))
                .willReturn(aResponse()
                        .withFixedDelay(delay)
                        .withStatus(statusCode)
                        .withHeader("Content-Type", contentType)
                        .withBody(body)));

    }

    public void setupStubEdit(String uri, int statusCode, String contentType, String body, int delay) {
        stubFor(get("/"+uri)
                .willReturn(aResponse()
                        .withFixedDelay(delay)
                        .withStatus(statusCode)
                        .withHeader("Content-Type", contentType)
                        .withBody(body)));

    }


    public void setupStubPost(String uri, int statusCode, String contentType, String reqBody, String resBody) {
        stubFor(post(urlMatching(".*/" + uri + ".*")).withRequestBody(equalToJson(reqBody))
                .willReturn(aResponse()
                        .withStatus(statusCode)
                        .withHeader("Content-Type", contentType)
                        .withBody(resBody)));
    }

    public void setupStubPost(String uri, int statusCode, String contentType, String reqBody, String resBody, int minDelay, int maxDelay) {
        DelayDistribution dd = new UniformDistribution(minDelay, maxDelay);
        stubFor(post(urlMatching(".*/" + uri + ".*")).withRequestBody(equalToJson(reqBody))
                .willReturn(aResponse()
                        .withRandomDelay(dd)
                        .withStatus(statusCode)
                        .withHeader("Content-Type", contentType)
                        .withBody(resBody)));
    }

    public void setupStubPost(String uri, int statusCode, String contentType, String reqBody, String resBody, int delay) {
        stubFor(post(urlMatching(".*/" + uri + ".*")).withRequestBody(equalToJson(reqBody))
                .willReturn(aResponse()
                        .withFixedDelay(delay)
                        .withStatus(statusCode)
                        .withHeader("Content-Type", contentType)
                        .withBody(resBody)));
    }

    public void setupStubPost(String uri, int statusCode, String contentType, String resBody) {
        stubFor(post(urlMatching(".*/" + uri + ".*"))
                .willReturn(aResponse()
                        .withStatus(statusCode)
                        .withHeader("Content-Type", contentType)
                        .withBody(resBody)));
    }

    public void setupStubPost(String uri, int statusCode, String contentType, String resBody, int minDelay, int maxDelay) {
        DelayDistribution dd = new UniformDistribution(minDelay, maxDelay);
        stubFor(post(urlMatching(".*/" + uri + ".*"))
                .willReturn(aResponse()
                        .withRandomDelay(dd)
                        .withStatus(statusCode)
                        .withHeader("Content-Type", contentType)
                        .withBody(resBody)));
    }

    public void setupStubPost(String uri, int statusCode, String contentType, String resBody, int delay) {
        stubFor(post(urlMatching(".*/" + uri + ".*"))
                .willReturn(aResponse()
                        .withFixedDelay(delay)
                        .withStatus(statusCode)
                        .withHeader("Content-Type", contentType)
                        .withBody(resBody)));
    }

    public void setupStubPut(String uri, int statusCode, String contentType, String resBody, int delay){
        stubFor(put(urlMatching(".*/"+uri+".*"))
                .willReturn(aResponse()
                        .withFixedDelay(delay)
                        .withStatus(statusCode)
                        .withHeader("Content-Type", contentType)
                        .withBody(resBody)));
    }
    public void listAll(){
        ListStubMappingsResult listStubMappingsResult = listAllStubMappings();
        List<StubMapping> mappings = listStubMappingsResult.getMappings();
        for(StubMapping stubMapping : mappings){
            removeStub(stubMapping);
        }
    }
    @Test
    public void test() throws IOException {
        WireMockHelper wireMockHelper = new WireMockHelper();
        wireMockHelper.startMockServer(6666);
        wireMockHelper.listAll();
    }
}
//4eb7e81a-0256-4d54-8a12-404a403c2a3a
//dac45d29-1fd0-4cdf-8a81-362e0eeedd8c
//76894dfd-00e2-41d3-9b74-7f32842e588d
//abd6ac6e-7467-4cba-a8e8-a5b86c4dd331