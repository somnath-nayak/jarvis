package framework.gameofthrones.Tyrion;

import com.monitorjbl.xlsx.StreamingReader;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


public class ExcelHelper {

    /**
     * Return Excel Sheet List<Map>. The Map Output will not contain the Empty Cells
     * @param fileLocation
     * @param sheetName
     * @return List<Map<String, String>>
     * @throws IOException
     */
    public List<Map<String, String>> getExcelList(String fileLocation, String sheetName) throws IOException {
        XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(new File(fileLocation)));
        Map<Integer,String> header = getExcelHeaders(wb.getSheet(sheetName), 0);
        return getExcelDetails(wb, sheetName, header, 0, -1, -1);
    }


    /**
     * Return Excel Sheet List<Map>. The Map Output will not contain the Empty Cells
     * @param fileLocation
     * @param sheetName
     * @return List<Map<String, String>>
     * @throws IOException
     */
    public List<Map<String, String>> getExcelList(String fileLocation, String sheetName, int rowNum, int maxRows, int maxColumn) throws IOException, InvalidFormatException {

        InputStream st = new FileInputStream(new File(fileLocation));
        Workbook wb = StreamingReader.builder().rowCacheSize(1000).bufferSize(4096).open(st);

        Map<Integer,String> header = getExcelHeaders(wb.getSheet(sheetName), rowNum);
        return getExcelDetails(wb, sheetName, header, rowNum, maxRows, maxColumn);
    }

    public Map getExcelHeaders(Sheet sheet, int rowNum){
        Map<Integer,String> header = new LinkedHashMap<>();
        for( Row row : sheet ) {
            for(Cell cell :row){
                header.put(cell.getColumnIndex(), cell.getStringCellValue());
            }
            if(row.getRowNum() == rowNum){ break; }
        }
        return header;
    }


    private List<Map<String, String>> getExcelDetails(Workbook wb, String sheetName, Map<Integer,String> header, int rowNum, int maxRows, int maxColumn){
        List<Map<String,String>> excelList = new ArrayList<>();
        for( Row row : wb.getSheet(sheetName) ) {
            if(maxRows !=-1 && row.getRowNum() > maxRows){
                return excelList;
            }
            Map<String,String>  excelMap = new HashMap<>();
            if(row.getRowNum() > rowNum){
                for(Cell cell :row){
                    if (maxColumn != -1 && cell.getColumnIndex() > maxColumn)
                        break;
                    if(header.get(cell.getColumnIndex()) != null){
                        switch( cell.getCellType()) {
                            case Cell.CELL_TYPE_STRING :
                                excelMap.put( header.get(cell.getColumnIndex()),cell.getRichStringCellValue().getString());
                                break;
                            case Cell.CELL_TYPE_NUMERIC:
                                if(org.apache.poi.ss.usermodel.DateUtil.isCellDateFormatted(cell))
                                    excelMap.put(header.get(cell.getColumnIndex()),cell.getDateCellValue().toString());
                                else
                                    excelMap.put(header.get(cell.getColumnIndex()),Integer.toString((int)cell.getNumericCellValue()));
                                break;
                            case Cell.CELL_TYPE_FORMULA:
                                excelMap.put(header.get(cell.getColumnIndex()),cell.getCellFormula());
                                break;
                            default:
                                excelMap.put(header.get(cell.getColumnIndex()), cell.getStringCellValue());
                                break;
                        }
                    }
                }
                excelList.add(excelMap);
            }
        }
        return excelList;
    }

}
