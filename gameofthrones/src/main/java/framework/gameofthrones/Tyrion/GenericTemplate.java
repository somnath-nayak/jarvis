package framework.gameofthrones.Tyrion;

import com.mysql.jdbc.PreparedStatement;
import io.advantageous.boon.core.Sys;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * Generic Template
 * @author abhijit.p
 */
public class GenericTemplate implements SqlTemplate{

    private JdbcTemplate jdbcTemplate;
    private static Logger log = LoggerFactory.getLogger(GenericTemplate.class);

    @Override
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public List<Map<String, Object>> queryForList(String query){
        log.debug("GenericTemplate : queryForList : Query :- " + query);
        List<Map<String, Object>> queryForList = this.jdbcTemplate.queryForList(query);
        return queryForList;
    }
    @Override
    public void execute(String query, PreparedStatementCallback<Boolean> obj)
    {
       this.jdbcTemplate.execute(query,obj);
    }
    @Override
    public Boolean doInPreparedStatement(PreparedStatement ps)
            throws SQLException {
        
        return ps.execute();
    }
    
    @Override
    public Object queryForObject(String query, String value, Object abc){
        log.debug("GenericTemplate : queryForList : Query :- " + query);
        Object name = this.jdbcTemplate.queryForObject(
                query, new Object[] { value }, abc.getClass());
        return name;
    }

    @Override
    public List<Object> queryForObjectRowMapper(String query, String value, RowMapper rowMapper){
        log.debug("GenericTemplate : queryForList : Query :- " + query);
        List<Object> name = this.jdbcTemplate.query(query, rowMapper);
        return name;
    }

    @Override
    public int update(String query){
        int rowsUpdated = this.jdbcTemplate.update(query);

        log.debug("GenericTemplate : update : Query :- " + query +"  Rows Updated :- "+rowsUpdated);
        return rowsUpdated;
    }

    @Override
    public Map<String, Object> queryForMap(String query)
    {
        log.debug("GenericTemplate : queryForMap : Query :- " + query);
        Map<String, Object> queryMap = this.jdbcTemplate.queryForMap(query);
        return queryMap;
    }

    @Override
    public Map<String, Object> queryForMap(String sql, @Nullable Object... args){
        log.debug("GenericTemplate : queryForMap : Query :- " + sql);
        Map<String, Object> queryMap = this.jdbcTemplate.queryForMap(sql, args);
        return queryMap;
    }

    @Override
    public void execute(String sql){
        this.jdbcTemplate.execute(sql);
    }
    

    @Override
    public void executeBatch(String[] sql){
        this.jdbcTemplate.batchUpdate(sql);
    }

    @Override
    public Integer queryForObjectInteger(String query, Object[] param, Object abc){
        Integer name = this.jdbcTemplate.queryForObject(query, param, Integer.class);
        return name;

    }
    @Override
    public int update(String query,Object[] param){
        int rowsUpdated = this.jdbcTemplate.update(query,param);

        log.debug("GenericTemplate : update : Query :- " + query +"  Rows Updated :- "+rowsUpdated);
        return rowsUpdated;
    }
    @Override
    public List<Map<String, Object>> queryForList(String query,Object[] param){
        log.debug("GenericTemplate : queryForList : Query :- " + query);
        List<Map<String, Object>> queryForList = this.jdbcTemplate.queryForList(query,param);
        return queryForList;
    }
    @Override
    public List<String> findAll(String sql){

        List<String> data  = this.jdbcTemplate.query(sql,new BeanPropertyRowMapper(List.class));
        System.out.println(data);
        return data;
    }
    @Override
    public List<String> queryForListFromSingleRow(String query,Object abc){
        log.debug("GenericTemplate : queryForList : Query :- " + query);
        List<String> queryForList = this.jdbcTemplate.queryForList(query,String.class);
        return queryForList;
    }


}