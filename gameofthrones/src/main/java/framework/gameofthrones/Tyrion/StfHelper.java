package framework.gameofthrones.Tyrion;
import com.google.gson.*;
import io.advantageous.boon.core.Sys;
import okhttp3.*;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.logging.Logger;
public class StfHelper  {
    private OkHttpClient client;
    private JsonParser jsonParser;
    private String stfUrl="http://172.16.101.48:7200/";
    private String authToken;
    private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    public StfHelper(String authToken) {
        this.client = new OkHttpClient();
        this.jsonParser = new JsonParser();
        this.authToken=authToken;
    }


    public boolean connectDevice(String deviceSerial) {
        Request request = new Request.Builder()
                .addHeader("Authorization", "Bearer " + authToken)
                .url(stfUrl + "api/v1/devices/" + deviceSerial)
                .build();
        Response response;
        try {
            response = client.newCall(request).execute();
            JsonObject jsonObject = jsonParser.parse(response.body().string()).getAsJsonObject();

            if (!isDeviceFound(jsonObject)) {
                return false;
            }

            JsonObject deviceObject = jsonObject.getAsJsonObject("device");
            boolean present = deviceObject.get("present").getAsBoolean();
            boolean ready = deviceObject.get("ready").getAsBoolean();
            boolean using = deviceObject.get("using").getAsBoolean();
            JsonElement ownerElement = deviceObject.get("owner");
            boolean owner = !(ownerElement instanceof JsonNull);

            if (!present || !ready || using || owner) {
                return false;
            }

            return addDeviceToUser(deviceSerial);
        } catch (IOException e) {
            throw new IllegalArgumentException("STF service is unreachable", e);
        }
    }


    public String connectDevice(){
        String deviceSerial=null;
        ArrayList<String> listDevices=getListOfDevices();

        if(listDevices.size()!=0){
            deviceSerial=listDevices.get(0);
            connectDevice(deviceSerial);
            return deviceSerial;
        }
      return deviceSerial;
    }



    public ArrayList<String> getListOfDevices(){
        ArrayList<String> listOfDevices=new ArrayList<>();
        Request request = new Request.Builder()
                .addHeader("Authorization", "Bearer " + authToken)
                .url(stfUrl + "api/v1/devices")
                .build();
        System.out.println(request.url());
        Response response;


        try {
            response = client.newCall(request).execute();
            JsonObject jsonObject = jsonParser.parse(response.body().string()).getAsJsonObject();
            System.out.println(jsonObject);
            JsonArray jsonArray=jsonObject.getAsJsonArray("devices");
            for (JsonElement pa : jsonArray) {
                JsonObject devicesObject = pa.getAsJsonObject();
                if(devicesObject.get("present").getAsBoolean()){
                    listOfDevices.add(devicesObject.get("serial").getAsString());
                }
            }


        } catch (IOException e) {
            throw new IllegalArgumentException("STF service is unreachable", e);
        }
        return listOfDevices;

    }

    public String remoteConnectToUser(String deviceSerialNumber){
        String remoteConnectUrl=null;
        connectDevice(deviceSerialNumber);
        RequestBody requestBody = RequestBody.create(JSON, "{\"serial\": \"" + deviceSerialNumber + "\"}");
        ArrayList<String> listOfDevices=new ArrayList<>();
        Request request = new Request.Builder()
                .addHeader("Authorization", "Bearer " + authToken)
                .url(stfUrl + "api/v1/user/devices/"+deviceSerialNumber+"/remoteConnect")
                .post(requestBody)
                .build();
        System.out.println(request.url());
        Response response;

        try {
            response = client.newCall(request).execute();
            JsonObject jsonObject = jsonParser.parse(response.body().string()).getAsJsonObject();
            remoteConnectUrl=jsonObject.get("remoteConnectUrl").getAsString();

            System.out.println(jsonObject);

        } catch (IOException e) {
            throw new IllegalArgumentException("STF service is unreachable", e);
        }
        return remoteConnectUrl;
    }
    public String remoteConnectToUser(){
        String remoteConnectUrl=null;
        String deviceSerialNumber=connectDevice();
        RequestBody requestBody = RequestBody.create(JSON, "{\"serial\": \"" + deviceSerialNumber + "\"}");
        ArrayList<String> listOfDevices=new ArrayList<>();
        Request request = new Request.Builder()
                .addHeader("Authorization", "Bearer " + authToken)
                .url(stfUrl + "api/v1/user/devices/"+deviceSerialNumber+"/remoteConnect")
                .post(requestBody)
                .build();
        System.out.println(request.url());
        Response response;

        try {
            response = client.newCall(request).execute();
            JsonObject jsonObject = jsonParser.parse(response.body().string()).getAsJsonObject();
            remoteConnectUrl=jsonObject.get("remoteConnectUrl").getAsString();

        } catch (IOException e) {
            throw new IllegalArgumentException("STF service is unreachable", e);
        }
        return remoteConnectUrl;
    }

    public void removeDisconnectToUser(String deviceSerialNumber){
        ArrayList<String> listOfDevices=new ArrayList<>();
        Request request = new Request.Builder()
                .addHeader("Authorization", "Bearer " + authToken)
                .url(stfUrl + "api/v1/user/devices/"+deviceSerialNumber+"/remoteConnect")
                .delete()
                .build();
        System.out.println(request.url());

        Response response;
        try {
            response = client.newCall(request).execute();
            JsonObject jsonObject = jsonParser.parse(response.body().string()).getAsJsonObject();

        } catch (IOException e) {
            throw new IllegalArgumentException("STF service is unreachable", e);
        }
    }

    public void disconnectDeviceFromSTF(String deviceSerialNumber){
        ArrayList<String> listOfDevices=new ArrayList<>();
        Request request = new Request.Builder()
                .addHeader("Authorization", "Bearer " + authToken)
                .url(stfUrl + "api/v1/user/devices/"+deviceSerialNumber)
                .delete()
                .build();
        System.out.println(request.url());
        Response response;
        try {
            response = client.newCall(request).execute();
            JsonObject jsonObject = jsonParser.parse(response.body().string()).getAsJsonObject();

        } catch (IOException e) {
            throw new IllegalArgumentException("STF service is unreachable", e);
        }
    }






    private boolean isDeviceFound(JsonObject jsonObject) {
        return jsonObject.get("success").getAsBoolean();
    }

    private boolean addDeviceToUser(String deviceSerial) {
        RequestBody requestBody = RequestBody.create(JSON, "{\"serial\": \"" + deviceSerial + "\"}");
        Request request = new Request.Builder()
                .addHeader("Authorization", "Bearer " + authToken)
                .url(stfUrl + "api/v1/user/devices")
                .post(requestBody)
                .build();
        Response response;
        try {
            response = client.newCall(request).execute();
            JsonObject jsonObject = jsonParser.parse(response.body().string()).getAsJsonObject();
            System.out.println(jsonObject);
            return isDeviceFound(jsonObject);
        } catch (IOException e) {
            throw new IllegalArgumentException("STF service is unreachable", e);
        }
    }

    public boolean releaseDevice(String deviceSerial) {
        Request request = new Request.Builder()
                .addHeader("Authorization", "Bearer " + authToken)
                .url(stfUrl + "user/devices/" + deviceSerial)
                .delete()
                .build();
        Response response;
        try {
            response = client.newCall(request).execute();
            JsonObject jsonObject = jsonParser.parse(response.body().string()).getAsJsonObject();

            return isDeviceFound(jsonObject);
        } catch (IOException e) {
            throw new IllegalArgumentException("STF service is unreachable", e);
        }
    }
}

