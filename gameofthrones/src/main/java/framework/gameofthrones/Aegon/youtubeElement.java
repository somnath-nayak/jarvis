package framework.gameofthrones.Aegon;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.base.Function;




public class youtubeElement implements IElement
{
	private WebElement webElement;
	private WebDriver webDriver;
	private List<WebElement> lwebElement;
	

	public youtubeElement(WebDriver webDriver, WebElement webElement)
	{
		this.setWebDriver(webDriver);
		this.setWebElement(webElement);
	}
	public youtubeElement(WebDriver webDriver, WebElement webElement,List<WebElement> lwebElement)
	{
		this.setWebDriver(webDriver);
		this.setWebElement(webElement);
		this.setlWebElement(lwebElement);
	}


	

	public void click(int... timeouts)
	{
		System.out.println("Click");
	
			new FluentWait<WebElement>(webElement)
					.pollingEvery(500, TimeUnit.MILLISECONDS)
					.withTimeout(15, TimeUnit.SECONDS)
					.withMessage(
							"Not able to click. Finder - "
									+ webElement.toString())
					.until(new Function<WebElement, Boolean>()
					{
						public Boolean apply(WebElement webElement)
						{
							boolean isClickable = false;
							System.out.println("Trying to click on - "
									+ webElement.toString());
							try
							{
								webElement.click();
								isClickable = true;
							} catch (WebDriverException e)
							{
							}
							return isClickable;
						}
					});
		}
	

	public void submit(int... timeouts)
	{
		
			webElement.submit();
	}
	

	public void sendKeys(final CharSequence keysToSend, int... timeouts)
	{
	
			/*new FluentWait<WebElement>(webElement)
					.pollingEvery(500, TimeUnit.MILLISECONDS)
					.withTimeout(15, TimeUnit.SECONDS)
					.withMessage(
							"Not able to send Keys. Finder - "
									+ webElement.toString())
					.until(new Function<WebElement, Boolean>()
					{
						public Boolean apply(WebElement webElement)
						{
							boolean isKeySendPossible = false;
							//System.out.println("Trying to sendKeys to - "
								//	+ webElement.toString());
							try
							{
								webElement.sendKeys(keysToSend);
								isKeySendPossible = true;
							} catch (WebDriverException e)
							{
							}
							return isKeySendPossible;
						}
					});*/
			System.out.println(webElement + "send keys");
				webElement.sendKeys(keysToSend);
		
	}

	public void clear(int... timeouts)
	{
	
			webElement.clear();

	}

	public String getTagName(int... timeouts)
	{
		
			 return webElement.getTagName();
		
	}

	public String getAttribute(String name, int... timeouts)
	{
		return webElement.getAttribute(name);
		
	}

	public boolean isSelected(int... timeouts)
	{
		
		return webElement.isSelected();
	}

	public boolean isEnabled(int... timeouts)
	{
		
		return webElement.isEnabled();
	}

	public String getText(int... timeouts)
	{
		
		return webElement.getText();
	}

	public List<WebElement> findElements(By by)
	{
		
		return webDriver.findElements(by);
	}

	@Override
	public List<MobileElement> findMobileElements(By by) {
		return null;
	}

	public WebElement findElement(By by)
	{
		
		//return webDriver.findElement(by);

		return webElement.findElement(by);



	}

	public boolean isDisplayed(int... timeouts)
	{
		
		return webElement.isDisplayed();
	}

	public Point getLocation(int... timeouts)
	{
		
		return webElement.getLocation();
	}

	public Dimension getSize(int... timeouts)
	{
		
		return  webElement.getSize();
	}

	public String getCssValue(String propertyName, int... timeouts)
	{
		
		return webElement.getCssValue(propertyName);
	}

	public WebElement getWebElement()
	{
		return webElement;
	}

	public void setWebElement(WebElement webElement)
	{
		this.webElement = webElement;
	}
	public void setlWebElement(List<WebElement> webElement)
	{
		this.lwebElement = webElement;
	}




	public WebDriver getWebDriver()
	{
		return webDriver;
	}

	public void setWebDriver(WebDriver webDriver)
	{
		this.webDriver = webDriver;
	}

	

	public boolean waitForElementPresent(int timeout)
	{
		boolean isFound = false;
	//	WebDriverWait wait = new WebDriverWait(webDriver, timeout);
	//	wait.until(ExpectedConditions.visibilityOf(getWebElement()));
		return isFound;
	}

	public boolean waitForElementPresent()
	{
		int timeout = 5000;
		return waitForElementPresent(timeout);
	}

	public void hover(int... timeouts)
	{
		
			//new MyntActions(webDriver).moveToElement(this).pause(500).perform();
	}

	@Override
	public void hoverAndClick(IElement element, int... timeouts) {

	}


	public Select getSelect(int... timeouts)
	{
		return new Select(getWebElement());
	}

	@Override
	public By getByLocator() {
		return null;
	}

	@Override
	public ArrayList<String> getAllText(int... timeouts) {
		if (lwebElement != null) {
			ArrayList<String> al = new ArrayList<>();
			for (int i = 0; i < lwebElement.size(); i++) {
				al.add(lwebElement.get(i).getText());
			}
			return al;
		}
		else{
			return null;
		}
	}



	@Override
	public String[] getSimilarTagsContentList() {
		// TODO Auto-generated method stub
		return null;
	}


}
