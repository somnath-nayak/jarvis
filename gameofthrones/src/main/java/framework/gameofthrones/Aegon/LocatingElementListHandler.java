package framework.gameofthrones.Aegon;


import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;


public class LocatingElementListHandler implements InvocationHandler
{
	private final ElementLocator ElementLocator;
	private final int DEFAULT_FIND_TIMEOUT = 15;

	public LocatingElementListHandler(ElementLocator ElementLocator)
	{
		this.ElementLocator = ElementLocator;
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] objects)
			throws Throwable
	{
		List<IElement> elements;
		int findTimeout;
		if (null == objects)
		{
			elements = ElementLocator.findElements(DEFAULT_FIND_TIMEOUT);
		} else
		{
			int parameterCount = objects.length;
			if (parameterCount == 1)
			{
				elements = ElementLocator
						.findElements(DEFAULT_FIND_TIMEOUT);
			} else if (parameterCount == 2)
			{
				findTimeout = ((int[]) objects[parameterCount - 1])[0];
				elements = ElementLocator.findElements(findTimeout);
			} else
			{
				elements = ElementLocator
						.findElements(DEFAULT_FIND_TIMEOUT);
			}
		}

		try
		{
			return method.invoke(elements, objects);
		} catch (InvocationTargetException e)
		{
			throw e.getCause();
		}
	}

	public ElementLocator getMyntElementLocator()
	{
		return ElementLocator;
	}
}
