package framework.gameofthrones.Aegon;
import framework.gameofthrones.Tyrion.SqlTemplate;

/**
 * @author abhijit.p
 * @since 20 Jan 2018
**/
public class SystemConfigProvider {

    private static DBDetails dbDetails;
    private static long runID=0;
    public static DBDetails getTopology() {
        return dbDetails;
    }

    public static void setTopology(DBDetails dbDetails) {
        if(SystemConfigProvider.dbDetails == null){
            SystemConfigProvider.dbDetails = dbDetails;
        }
    }

    public static void setRunID(long runID) {
        if(SystemConfigProvider.runID == 0){
            SystemConfigProvider.runID = runID;
        }
    }

    public static long getRunId(){
        return runID;
    }
    public static SqlTemplate getTemplate(String dbName){
        return dbDetails.getDatabaseService(dbName);
    }
}
