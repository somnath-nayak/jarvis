package framework.gameofthrones.Aegon;

import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kiran.j on 12/5/17.
 */
public interface IElement {

    void clear(int... timeouts);
    void click(int... timeouts);
    WebElement findElement(By by);
    List<WebElement> findElements(By by);
    List<MobileElement> findMobileElements(By by);
    String getAttribute(String name, int... timeouts);
    String getCssValue(String propertyName, int... timeouts);
    Point getLocation(int... timeouts);
    Dimension getSize(int... timeouts);
    String getTagName(int... timeouts);
    String getText(int... timeouts);
    boolean isDisplayed(int... timeouts);
    boolean isEnabled(int... timeouts);
    boolean isSelected(int... timeouts);
    void sendKeys(CharSequence keysToSend, int... timeouts);
    void submit(int... timeouts);
    String[] getSimilarTagsContentList();
    WebElement getWebElement();
    void hover(int... timeouts);
    void hoverAndClick(IElement element,int... timeouts);
    Select getSelect(int... timeouts);
    By getByLocator();
    ArrayList<String> getAllText(int... timeouts);
}
