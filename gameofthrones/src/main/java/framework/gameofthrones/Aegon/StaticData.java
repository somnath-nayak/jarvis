package framework.gameofthrones.Aegon;

import framework.gameofthrones.Daenerys.api;
import framework.gameofthrones.Daenerys.endpoint;
import framework.gameofthrones.Daenerys.environment;

import java.util.HashMap;
import java.util.List;

/**
 * Created by ashish.bajpai on 19/04/17.
 */
public class StaticData {

	public String datafolder = "../Data";
	private String propertyfilename = "settings.properties";
	private String objectrepository = "ObjectRepository/Elements.xml";
	private String environmentFile = "Environments/environment.xml";
	private String environmentfolder = "Environments";
	private String payloadfolder = "Payloads";
	private String MailTemplateFolder = "StaticData";
	private static String mobilepropertyfilename = "uisettings.properties";

	private String TestRunReportName = "testrunreport.txt";

	// private PropertiesHandler props = new PropertiesHandler();
	private String GetTestEnvironment = "stage2";
	public String PropertyFilePath = getDatafolder() + "/" + propertyfilename;
	public String EnvironmentFilePath = getDatafolder() + "/" + environmentfolder + "/environment_"
			+ GetTestEnvironment + ".xml";
	public String ObjectRepositoryPath = getDatafolder() + "/" + objectrepository;
	// public String EnvironmentFilePath = "./"+getDatafolder()+"/"+environmentFile;
	public String Payloadpath = getDatafolder() + "/" + payloadfolder;
	public String TestRunReportPath = getDatafolder() + "/" + MailTemplateFolder + "/" + TestRunReportName;
	public String MobilePropertyFilePath = getDatafolder() + "/" +getMobilePlatform()+"_"+ mobilepropertyfilename;

	/*
	 * Ramzi:
	 */
	// public String inputexceldatafilepath =
	// "./resources/test_InputData_Backend.xls";
	// public String excelDatasheetname = "SANITY";



	public HashMap<String, HashMap<String, String>> getRequiredStafDataFromGOT() {
		Initialize init = new Initialize();

		environment env = init.EnvironmentDetails.setup.getEnvironmentData();
		HashMap<String, HashMap<String, String>> hm1= getSTAFData(env);
		HashMap<String, HashMap<String, String>> hm = new HashMap<String, HashMap<String, String>>();

		hm.put("STAF_ENV_INFO", getEnvData(env));
		hm.put("STAF_API_INFO", getAPIData(env));
		hm.put("STAF_SUITE_INFO", getSuiteData(hm1));
		hm.put("KAYVALUE_PAIRS", hm1.get("KAYVALUE_PAIRS"));
		return hm;
	}

	public HashMap<String, String> getEnvData(environment env) {
		List<endpoint> apisendpoints = env.getServices().getEndpoints().getEndpoint();
		HashMap<String, String> hm = new HashMap<String, String>();
		for (endpoint aa : apisendpoints) {
			hm.put(aa.getServicename(), aa.getBaseurl());

		}
		return hm;
	}

	public HashMap<String, String> getAPIData(environment env) {
		List<api> a = env.getServices().getApis().getApi();
		HashMap<String, String> hm = new HashMap<String, String>();
		for (api aa : a) {
			if(aa.getOptionalstafurlpath().trim().isEmpty()||aa.getOptionalstafurlpath()=="" ) {
			              hm.put(aa.getStafapiname(), aa.getRequestmethod() + "=" + aa.getPath());
			}
			else {
				 hm.put(aa.getStafapiname(), aa.getRequestmethod() + "=" + aa.getOptionalstafurlpath());
			}
			
		}

		return hm;
	}

	public HashMap<String, HashMap<String, String>> getSTAFData(environment env) {
		HashMap<String, HashMap<String, String>> hm = new HashMap<String, HashMap<String, String>>();
		hm.put("EXCEL_CONFIGS", env.getStafdata().getExcelConfig());
		hm.put("KAYVALUE_PAIRS", env.getStafdata().getKeyValuePairs());
		return hm;

	}

	public HashMap<String, String> getSuiteData(HashMap<String, HashMap<String, String>> hm) {
		HashMap<String, String> data = null;

		try {
			data = new HashMap<String, String>();
			data.put("inputexceldatafilepath", hm.get("EXCEL_CONFIGS").get("inputexceldatafilepath"));
			data.put("excelDatasheetname", hm.get("EXCEL_CONFIGS").get("excelDatasheetname"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	// public String getInputexceldatafilepath() {
	// return inputexceldatafilepath;
	// }
	//
	// public void setInputexceldatafilepath(String inputexceldatafilepath) {
	// this.inputexceldatafilepath = inputexceldatafilepath;
	// }
	//
	// public String getExcelDatasheetname() {
	// return excelDatasheetname;
	// }
	//
	// public void setExcelDatasheetname(String excelDatasheetname) {
	// this.excelDatasheetname = excelDatasheetname;
	// }

	public String getMobilePropertyFilePath() {
		String platform=System.getenv("platform");
		if(platform!=null && platform.contains("web")){
			return getDatafolder() + "/" +"web_uisettings.properties";
		}
		else {
			return MobilePropertyFilePath;
		}
	}

	public StaticData() {
		String environment = System.getenv("environment");

		// ITestContext context;
		// context.getSuite().getXmlSuite().getParameter("emails");

		if (environment != null) {
			GetTestEnvironment = environment;
		}
	}

	public StaticData(String environmentname) {
		setGetTestEnvironment(environmentname);
		// String environment = environmentname;
		// GetTestEnvironment = environment;
		// PropertiesHandler props = new PropertiesHandler();
		// props.SwitchEnvironment(environmentname);

		// ITestContext context;
		// context.getSuite().getXmlSuite().getParameter("emails");

		EnvironmentFilePath = getDatafolder() + "/" + environmentfolder + "/environment_" + GetTestEnvironment
				+ ".xml";
		//System.out.println("environment file path = " + EnvironmentFilePath);
	}

	public StaticData(PropertiesHandler props) {
		String environment = System.getenv("environment");
		if (environment != null)
			GetTestEnvironment = environment;
		else
			GetTestEnvironment = props.propertiesMap.get("environment");

		// System.out.println("Environment :: "+GetTestEnvironment);
		EnvironmentFilePath = getDatafolder() + "/" + environmentfolder + "/environment_" + GetTestEnvironment
				+ ".xml";
	}

	public String getDatafolder() {
		return datafolder;
	}

	public void setDatafolder(String datafolder) {
		this.datafolder = datafolder;
	}

	public String getPropertyFilePath() {
		return PropertyFilePath;
	}

	public void setPropertyFilePath(String propertyFilePath) {
		PropertyFilePath = propertyFilePath;
	}

	public void setPropertyfilename(String propertyfilename) {
		this.propertyfilename = propertyfilename;
	}

	public void setObjectrepository(String objectrepository) {
		this.objectrepository = objectrepository;
	}

	public void setObjectRepositoryPath(String objectRepositoryPath) {
		ObjectRepositoryPath = objectRepositoryPath;
	}

	public String getPropertyfilename() {
		return propertyfilename;
	}

	public String getObjectrepository() {
		return objectrepository;
	}

	public String getObjectRepositoryPath() {
		return ObjectRepositoryPath;
	}

	public void setEnvironmentFilePath(String environmentFilePath) {
		EnvironmentFilePath = environmentFilePath;
	}

	public String getEnvironmentFilePath() {
		return EnvironmentFilePath;
	}

	public String getPayloadfolder() {
		return Payloadpath;
	}

	public void setPayloadpath(String payloads) {
		this.Payloadpath = payloads;
	}

	public String getGetTestEnvironment() {
		return GetTestEnvironment;
	}

	public void setGetTestEnvironment(String getTestEnvironment) {
		GetTestEnvironment = getTestEnvironment;
	}

	public String getTestRunReportPath() {
		return TestRunReportPath;
	}

	public void setTestRunReportPath(String testRunReportPath) {
		TestRunReportPath = testRunReportPath;
	}
	public String getMobilePlatform(){
		String platform=System.getenv("platform");
		String pod=System.getenv("pod");
		if(pod==null){
			pod="consumer";
		}
		if(platform!=null && pod!=null){
			return platform+"_"+pod;
		}
		else if(platform != null && platform.contains("web")) {
				return "web";
		}
			return "android_consumer";
	}
}