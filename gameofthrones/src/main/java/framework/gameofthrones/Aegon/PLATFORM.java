package framework.gameofthrones.Aegon;

/**
 * Created by ashish.bajpai on 18/04/17.
 */
public enum PLATFORM {
    IOS,
    ANDROID,
    WEB,
    MOBILEWEB,
    WINDOWS
}
