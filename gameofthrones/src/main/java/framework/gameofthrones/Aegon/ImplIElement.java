//package framework.gameofthrones.Aegon;
//
//import io.appium.java_client.AppiumDriver;
//import io.appium.java_client.MobileElement;
//import io.appium.java_client.android.AndroidDriver;
//import io.appium.java_client.android.AndroidKeyCode;
//import io.appium.java_client.ios.IOSDriver;
//import org.openqa.selenium.*;
//import org.openqa.selenium.interactions.Actions;
//import org.openqa.selenium.support.ui.Select;
//
//import java.util.ArrayList;
//import java.util.List;
//
//
// /* Created by kiran.j on 12/5/17.*/
//
//public class ImplIElement implements IElement {
//
//    private WebElement webElement;
//    private MobileElement mobileElement;
//    private By by;
//    private WebDriver webDriver;
//    private AndroidDriver androidDriver;
//    private IOSDriver iosDriver;
//    private ElementIdentificationType elementIdentificationType;
//
//    public WebDriver getWebDriver() {
//        return webDriver;
//    }
//
//    public void setWebDriver(WebDriver webDriver) {
//        this.webDriver = webDriver;
//    }
//
//    public AndroidDriver getAndroidDriver() {
//        return androidDriver;
//    }
//
//    public void setAndroidDriver(AndroidDriver androidDriver) {
//        this.androidDriver = androidDriver;
//    }
//
//    public IOSDriver getIosDriver() {
//        return iosDriver;
//    }
//
//    public void setIosDriver(IOSDriver iosDriver) {
//        this.iosDriver = iosDriver;
//    }
//
//    public void clear(int... timeouts) {
//        switch (elementIdentificationType)
//        {
//
//            case WEBDRIVER: webElement.clear();
//                            break;
//            case ANDROIDRIVER:  while(!mobileElement.getText().isEmpty())
//                                pressDeleteKey(androidDriver);
//                                break;
//            case IOSDRIVER: while(!mobileElement.getText().isEmpty())
//                            pressDeleteKey(iosDriver);
//                            break;
//
//        }
//    }
//
//    public void click(int... timeouts) {
//        switch (elementIdentificationType) {
//            case JS:
//                    break;
//            case WEBDRIVER:
//                break;
//            case ANDROIDRIVER:
//
//                break;
//            case IOSDRIVER:
//                break;
//        }
//    }
//
//    public WebElement findElement(By by) {
//        WebElement element = null;
//        switch (elementIdentificationType) {
//            case WEBDRIVER:
//                 element = webDriver.findElement(by);
//            case JS:
//                 break;
//            case ANDROIDRIVER:
//                 element = androidDriver.findElement(by);
//                 break;
//            case IOSDRIVER:
//                element = iosDriver.findElement(by);
//                break;
//        }
//        return element;
//    }
//
//    public List<WebElement> findElements(By by) {
//        List<WebElement> elements = null;
//        elements = webDriver.findElements(by);
//        return elements;
//    }
//
//    public List<MobileElement> findMobileElements(By by) {
//        List<MobileElement> elements = new ArrayList<>();
//        switch (elementIdentificationType) {
//            case ANDROIDRIVER: elements = androidDriver.findElements(by);
//                               break;
//            case IOSDRIVER: elements = iosDriver.findElements(by);
//                            break;
//        }
//        return elements;
//    }
//
//    public String getAttribute(String name, int... timeouts) {
//        String attribute = "";
//        switch (elementIdentificationType) {
//            case JS:
//                break;
//            case WEBDRIVER:
//                attribute = webElement.getAttribute(name);
//                break;
//        }
//        return attribute;
//    }
//
//    public String getCssValue(String propertyName, int... timeouts) {
//        String cssValue = "";
//        switch (elementIdentificationType)
//        {
//            case JS:
//                break;
//            case WEBDRIVER:cssValue = webElement.getCssValue(propertyName);
//                break;
//        }
//        return cssValue;
//    }
//
//    public Point getLocation(int... timeouts) {
//        Point location = null;
//        switch (elementIdentificationType)
//        {
//            case JS:
//
//                break;
//            case WEBDRIVER: location = webElement.getLocation();
//            case ANDROIDRIVER:
//            case IOSDRIVER: location = mobileElement.getLocation();
//                            break;
//        }
//        return location;
//    }
//
//
//    public Dimension getSize(int... timeouts) {
//        Dimension size = null;
//        switch (elementIdentificationType)
//        {
//            case JS:
//
//                break;
//            case WEBDRIVER: size = webElement.getSize();
//                            break;
//            case ANDROIDRIVER:
//            case IOSDRIVER: size = mobileElement.getSize();
//                            break;
//        }
//        return size;
//    }
//
//    public String getTagName(int... timeouts) {
//        return null;
//    }
//
//
//    public String getText(int... timeouts) {
//        String text = "";
//        switch (elementIdentificationType)
//        {
//
//            case WEBDRIVER: text = webElement.getText();
//                            break;
//            case ANDROIDRIVER:
//            case IOSDRIVER: text = mobileElement.getText();
//                            break;
//        }
//        return text;
//    }
//
//    public boolean isDisplayed(int... timeouts) {
//        boolean isDisplayed = false;
//        switch (elementIdentificationType)
//        {
//            case JS:
//                break;
//            case WEBDRIVER: isDisplayed = webElement.isDisplayed();
//                            break;
//            case ANDROIDRIVER:
//            case IOSDRIVER: isDisplayed = mobileElement.isDisplayed();
//                            break;
//        }
//        return isDisplayed;
//    }
//
//    public boolean isEnabled(int... timeouts) {
//        boolean isEnabled = false;
//        switch (elementIdentificationType)
//        {
//            case JS:
//                break;
//            case WEBDRIVER: isEnabled = webElement.isEnabled();
//                            break;
//            case ANDROIDRIVER:
//            case IOSDRIVER: isEnabled = mobileElement.isEnabled();
//                            break;
//        }
//        return isEnabled;
//    }
//
//    public boolean isSelected(int... timeouts) {
//        boolean isSelected = false;
//        switch (elementIdentificationType)
//        {
//            case JS:
//                break;
//            case WEBDRIVER: isSelected = webElement.isSelected();
//                            break;
//            case ANDROIDRIVER:
//            case IOSDRIVER: isSelected = mobileElement.isSelected();
//                            break;
//        }
//        return isSelected;
//    }
//
//    public void sendKeys(CharSequence keysToSend, int... timeouts) {
//        switch (elementIdentificationType)
//        {
//
//            case WEBDRIVER: webElement.sendKeys(keysToSend);
//                            break;
//            case ANDROIDRIVER:
//            case IOSDRIVER: mobileElement.sendKeys(keysToSend);
//                            break;
//        }
//
//    }
//
//    public void submit(int... timeouts) {
//        switch (elementIdentificationType)
//        {
//
//            case WEBDRIVER: webElement.submit();
//                            break;
//            case ANDROIDRIVER:
//            case IOSDRIVER: mobileElement.submit();
//                            break;
//        }
//
//    }
//
//    public String[] getSimilarTagsContentList() {
//
//        return new String[0];
//    }
//
//    public WebElement getWebElement() {
//        return webElement;
//    }
//
//    public void hover(int... timeouts) {
//        switch (elementIdentificationType) {
//            case JS:
//
//                break;
//            case WEBDRIVER:
//                Actions action = new Actions(webDriver);
//                action.moveToElement(webElement).perform();
//                break;
//        }
//    }
//
//    public void hoverAndClick(IElement element, int... timeouts) {
//        switch (elementIdentificationType)
//        {
//            case JS:
//
//                break;
//            case WEBDRIVER:
//                Actions action = new Actions(webDriver);
//                action.moveToElement(webElement).click().build().perform();
//                break;
//        }
//
//    }
//
//    @Override
//    public Select getSelect(int... timeouts) {
//        return null;
//    }
//
//    @Override
//    public By getByLocator() {
//        return null;
//    }
//
//    public void pressDeleteKey(AppiumDriver<MobileElement> driver) {
//        ((AndroidDriver<MobileElement>)driver).pressKeyCode(AndroidKeyCode.DEL);
//    }
//}
