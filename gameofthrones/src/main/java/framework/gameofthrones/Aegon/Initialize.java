package framework.gameofthrones.Aegon;

import framework.gameofthrones.Daenerys.InitializeEnvironmentObject;

import java.io.IOException;

/**
 * Created by ashish.bajpai@swiggy.in on 27/06/17.
 * @author ashish.bajpai
 *
 */
public  class Initialize {

    public PropertiesHandler Properties;
    public ObjectRepositoryHelper ObjectRepository;
    public InitializeEnvironmentObject EnvironmentDetails;
    private final DBDetailsImpl dbDetailsImpl = new DBDetailsImpl();

    public Initialize(){
        loadenvironment();
    }


    public Initialize(String environment){
        EnvironmentReload(environment);
    }

    public void EnvironmentReload(String environmentname){
        loadenvironment();
        System.out.println("environmentname from Initialize = " + environmentname);
        Properties.SwitchEnvironment(environmentname);
        reloadenvironment(environmentname);
    }

    private void loadenvironment(){
        try{
            Properties = new PropertiesHandler();
            System.out.println("PropertiesHandler while loading new file = " + Properties.propertiesMap.get("environment"));
            System.setProperty("env", Properties.propertiesMap.get("environment"));
            ObjectRepository = new ObjectRepositoryHelper(PLATFORM.IOS);
            EnvironmentDetails = new InitializeEnvironmentObject(Properties);
            dbDetailsImpl.importDb(EnvironmentDetails.setup.getEnvironmentData().getDatabase().getConnectionstrings().getConnectionstring());
            SystemConfigProvider.setTopology(dbDetailsImpl);
        }
        catch(IOException ioException){
            ioException.printStackTrace();
        }
    }


    private void reloadenvironment(String environmentname){
        try{
            Properties = new PropertiesHandler(environmentname);
            System.out.println("PropertiesHandler while loading new file = " + Properties.propertiesMap.get("environment"));
            ObjectRepository = new ObjectRepositoryHelper(PLATFORM.IOS);
            EnvironmentDetails = new InitializeEnvironmentObject(environmentname);
        }
        catch(IOException ioException){
            ioException.printStackTrace();
        }

    }


}
