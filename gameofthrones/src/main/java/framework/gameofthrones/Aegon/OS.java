/**
 *
 */
package framework.gameofthrones.Aegon;

/**
 * @author Ashish Kumar Bajpai
 * @Createdon 15-May-2015
 * @Organization
 *
 */
public enum OS
{
    WINDOWS,
    MACOS,
    LINUX,
    SOLARIS,
    ANDROID,
    IOS
}
