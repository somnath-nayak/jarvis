package framework.gameofthrones.Aegon;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;

import java.io.*;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Properties;

/**
 * Created by kiran.j on 11/3/17.
 */
public class InitializeUI {

    public static PropertiesHandler Properties;
    public AndroidDriver androidDriver;
    public IOSDriver iosDriver;
    public WebDriver webdriver;
    public UIElements UIObjects;
    public AppiumDriver appiumDriver;
    public static HashMap<String, String> uipropertiesMap = new HashMap<String, String>();
    public static StaticData staticData = new StaticData();
    public static String uipropertiesfilepath = staticData.getMobilePropertyFilePath();
    public String platformnew;
    public AppiumServerJava appiumServer;
    public static boolean isDeeplink;


    public static String getApp_package() {
        return app_package;
    }

    public static String app_package;

    public WebDriver getWebdriver() {
        return webdriver;
    }

    public void setWebdriver(WebDriver webdriver) {
        this.webdriver = webdriver;
    }

    public static HashMap<String, String> getUipropertiesMap() {
        return uipropertiesMap;
    }

    public AndroidDriver getAndroidDriver() {
        return androidDriver;
    }
    public AppiumDriver getAppiumDriver() {
        if(platformnew.equalsIgnoreCase("Android")){
            return androidDriver;
        }
        else{
            return iosDriver;
        }


    }

    public void setAndroidDriver(AndroidDriver androidDriver) {
        this.androidDriver = androidDriver;
    }

    public IOSDriver getIosDriver() {
        return iosDriver;
    }

    public void setIosDriver(IOSDriver iosDriver) {
        this.iosDriver = iosDriver;
    }

    public boolean isDeeplink() {
        return isDeeplink;
    }

    public static void setDeeplink(boolean deeplink) {
        isDeeplink = deeplink;
    }

    public InitializeUI(){
        platformnew=System.getenv("platform");
        UIPropertyReader();
        SetUp(System.getenv("deviceId"));
        UIObjects = new UIElements();
    }
    public InitializeUI(String deviceId){
        platformnew=System.getenv("platform");
        UIPropertyReader();
        SetUp(deviceId);
        UIObjects = new UIElements();
    }

    public void SetUp(String deviceId) {
        String platform;
        if(System.getProperty("platform") != null && !System.getProperty("platform").isEmpty())
            platform = System.getProperty("platform");
        else
            platform = (uipropertiesMap.get("platform"));

        if(platform.contains("web"))
            Web();
        else
            Mobile(deviceId);
    }

    public WebDriver Web() {
        HashMap<String, String> webprops = getUipropertiesMap();
        String browser = webprops.get("browser");
        String driverpath;

        if(System.getProperty("path") != null && !System.getProperty("path").isEmpty())
            driverpath = System.getProperty("path");
        else
            driverpath = webprops.get("driver_path");
        switch (browser) {
            case "chrome":  System.setProperty("webdriver.chrome.driver", driverpath);
                webdriver = new ChromeDriver();
                break;
            case "safari": webdriver = new SafariDriver();
                break;
            case "firefox": System.setProperty("webdriver.gecko.driver", driverpath);
                webdriver = new FirefoxDriver();
                break;
            case "htmlunit": webdriver = new HtmlUnitDriver();
                break;
            case "ie":  System.setProperty("webdriver.ie.driver", driverpath);
                webdriver = new InternetExplorerDriver();
                break;
            case "headless" : System.setProperty("phantomjs.binary.path", driverpath);
                webdriver = new PhantomJSDriver();
                break;
            case "chrome-headless" : System.setProperty("webdriver.chrome.driver", driverpath);
                ChromeOptions chromeOptions = new ChromeOptions();
                chromeOptions.addArguments("--headless", "--disable-gpu", "--window-size=1920,1200","--ignore-certificate-errors");
                webdriver = new ChromeDriver(chromeOptions);
                break;

            default: break;
        }
        return webdriver;
    }

    public void UIPropertyReader()
    {
        try {
            System.out.println("Current Directory = " + System.getProperty("user.dir")+" = "+uipropertiesfilepath);
            File file = new File(uipropertiesfilepath);
            FileInputStream inputstream = new FileInputStream(file);
            Properties properties = new Properties();
            properties.load(inputstream);
            inputstream.close();

            Enumeration enuKeys = properties.keys();
            while (enuKeys.hasMoreElements()){
                String propertykey = (String)enuKeys.nextElement();
                String propertyvalue = properties.getProperty(propertykey);
                uipropertiesMap.put(propertykey,propertyvalue);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();

        }
    }

    public void Mobile(String deviceId) {


        HashMap<String, String> mobile_props = new HashMap<String, String>();
        mobile_props = getUipropertiesMap();
        String platform = mobile_props.get("os");
        if(System.getProperty("app_package") != null && !System.getProperty("app_package").isEmpty() || System.getenv("packageName")!=null)
            app_package = System.getenv("packageName");
        else
            app_package = mobile_props.get(platform+"_app_package");

        File classpathRoot = new File(System.getProperty("user.dir"));
        File appDir = new File(classpathRoot, mobile_props.get("base_path"));
        DesiredCapabilities cap = new DesiredCapabilities();

        File app;

        if(System.getenv("appPath")!=null){
            System.out.println("ENV");
            cap.setCapability("app", System.getenv("appPath"));

        }
        else {
            String apkPath=getApkPathFromFileForJenkin();
            if(apkPath!=null)
                app=new File(apkPath);
            else{
                app = new File(appDir, mobile_props.get(platform+"_app_path"));
            }
            cap.setCapability("app", app.getAbsolutePath());
        }


        //cap.setCapability(CapabilityType.BROWSER_NAME, mobile_props.get(platform+"_platform_name"));
        //cap.setCapability(CapabilityType.VERSION, mobile_props.get(platform+"_os_version"));
        cap.setCapability("appPackage", app_package);
        //cap.setCapability("platformName", mobile_props.get(platform+"_platform_name"));
        cap.setCapability("appActivity", mobile_props.get(platform+"_app_activity"));
        cap.setCapability("deviceName", mobile_props.get(platform+"_device_name"));
        cap.setCapability("newCommandTimeout",0);
        cap.setCapability("nativeWebScreenshot",true);
        cap.setCapability("androidScreenshotPath","target/screenshots");
        // cap.setCapability("udid",System.getenv("deviceId"));
        //cap.setCapability(MobileCapabilityType.AUTOMATION_NAME,"uiautomator2");


        if(platform.equalsIgnoreCase("ios")) {
            cap.setCapability("bundleid", mobile_props.get("bundle_id"));
            cap.setCapability("platformName", mobile_props.get(platform + "_platform_name"));
            cap.setCapability("automationName", mobile_props.get("automation_name"));
            cap.setCapability("updatedWDABundleId", "io.appium.WebDriverAgentRunner");


            if (!isDeeplink) {

                cap.setCapability("bundleid", mobile_props.get("bundle_id"));
                cap.setCapability("platformName", mobile_props.get(platform + "_platform_name"));
                cap.setCapability("automationName", mobile_props.get("automation_name"));
                cap.setCapability("platformVersion", mobile_props.get(platform + "_version"));
                cap.setCapability("xcodeOrgId", "9X7BN9YP4C");
                cap.setCapability("xcodeSigningId", "iPhone Developer");
                cap.setCapability("updatedWDABundleId", "io.appium.WebDriverAgentRunner");
                cap.setCapability("preventWDAAttachments", true);
                cap.setCapability("udid", "6a0dde57b0c612a6729b2bdf0d401393fd02aa7c");
//test
            }
        }
        try {
            if(platform.equalsIgnoreCase("android")) {
                androidDriver = (new AndroidDriver(new URL("http://" + mobile_props.get("url") + ":" + mobile_props.get("port") + "/wd/hub"), cap));

            }
            else {
                iosDriver = (new IOSDriver(new URL("http://" + mobile_props.get("url")+":"+mobile_props.get("port") + "/wd/hub"), cap));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
    public void createSessionForUpgrade() {

        HashMap<String, String> mobile_props = new HashMap<String, String>();
        mobile_props = getUipropertiesMap();
        String platform = mobile_props.get("os");
        if(System.getProperty("app_package") != null && !System.getProperty("app_package").isEmpty() || System.getenv("packageName")!=null)
            app_package = System.getenv("packageName");
        else
            app_package = mobile_props.get(platform+"_app_package");

        File classpathRoot = new File(System.getProperty("user.dir"));
        File appDir = new File(classpathRoot, mobile_props.get("base_path"));
        DesiredCapabilities cap = new DesiredCapabilities();
        cap.setCapability(CapabilityType.BROWSER_NAME, mobile_props.get(platform+"_platform_name"));
        //cap.setCapability(CapabilityType.VERSION, mobile_props.get(platform+"_os_version"));
        cap.setCapability("appPackage", app_package);
        cap.setCapability("platformName", mobile_props.get(platform+"_platform_name"));
        cap.setCapability("appActivity", mobile_props.get(platform+"_app_activity"));
        cap.setCapability("deviceName", mobile_props.get(platform+"_device_name"));
        cap.setCapability(MobileCapabilityType.NO_RESET, "true");
        cap.setCapability(MobileCapabilityType.AUTOMATION_NAME,"uiautomator2");
        cap.setCapability("newCommandTimeout",0);
        if(platform.equalsIgnoreCase("ios")) {
            cap.setCapability("bundleid",mobile_props.get("bundle_id"));
            cap.setCapability("automationName",mobile_props.get("automation_name"));
            cap.setCapability("platformVersion", mobile_props.get(platform+"_version"));
            //cap.setCapability("fullReset", true);
        }
        try {
            if(platform.equalsIgnoreCase("android")) {
                androidDriver = (new AndroidDriver(new URL("http://" + mobile_props.get("url") + ":" + mobile_props.get("port") + "/wd/hub"), cap));

            }
            else {
                iosDriver = (new IOSDriver(new URL("http://" + mobile_props.get("url")+":"+mobile_props.get("port") + "/wd/hub"), cap));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public String getApkPathFromFileForJenkin()
    {
        File file=new File(System.getProperty("user.dir"));
        String apkPath=null;
        try (BufferedReader br = new BufferedReader(new FileReader(file.getParent()+"/play.txt"))) {
            String line;
            while ((line = br.readLine()) != null) {
                apkPath=line;
                System.out.println(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return apkPath;
    }

}


