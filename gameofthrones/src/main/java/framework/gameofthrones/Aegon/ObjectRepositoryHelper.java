package framework.gameofthrones.Aegon;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Created by ashish.bajpai on 25/04/17.
 */
public  class ObjectRepositoryHelper {

    //abstract GetObjectRepository(PLATFORM plaform);

    private StaticData staticData = new StaticData();
    private HashMap<String, Element> iOSobjectrepository = new HashMap<String, Element>();
    private HashMap<String, Element> AndroidObjectRepository = new HashMap<String, Element>();
    private HashMap<String, Element> Mwebobjectrepository = new HashMap<String, Element>();
    private HashMap<String, Element> WebsiteObjectrepository = new HashMap<String, Element>();
    public HashMap<String, Element> Objectrepository = new HashMap<String, Element>();



    public Element GetElement(String elementname){
        return Objectrepository.get(elementname);
    }



    public ObjectRepositoryHelper(PLATFORM platform) {
        ObjectRepositoryReader();

        switch (platform){

            case IOS:
                Objectrepository = iOSobjectrepository;
                break;
            case ANDROID:
                Objectrepository = AndroidObjectRepository;
                break;
            case WEB:
                Objectrepository = WebsiteObjectrepository;
                break;
            case MOBILEWEB:
                Objectrepository = Mwebobjectrepository;
                break;
            case WINDOWS:
                Objectrepository = Mwebobjectrepository;
                break;
        }
        getrepodetails();

    }


    private void getrepodetails(){
        Iterator it = Objectrepository.entrySet().iterator();

        //System.out.println("it.toString(). = " + it.toString().);

        while(it.hasNext()){
            HashMap.Entry entry = (HashMap.Entry)it.next();
            //System.out.println("entry " + entry.getKey()+","+entry.getValue().toString());
        }
    }


    public void ObjectRepositoryReader() {
        try {
                String orfilepath = staticData.getObjectRepositoryPath();
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                Document document = db.parse(new File(orfilepath));
                NodeList nodeList = document.getElementsByTagName("Element");
                //System.out.println("nodeList.getLength() = " + nodeList.getLength());
                for (int x = 0, size = nodeList.getLength(); x < size; x++) {
                    //System.out.println("nodeList.item(x).getAttributes().getNamedItem(\"element\") = " + nodeList.item(x).getAttributes().getNamedItem("name").getNodeValue());
                    Element newelement = new Element();
                    newelement.ElementName = nodeList.item(x).getAttributes().getNamedItem("name").getNodeValue();
                    newelement.Jvalue = nodeList.item(x).getAttributes().getNamedItem("jValue").getNodeValue();
                    newelement.Platform = nodeList.item(x).getAttributes().getNamedItem("platform").getNodeValue();
                    newelement.Type = nodeList.item(x).getAttributes().getNamedItem("type").getNodeValue();
                    newelement.Value = nodeList.item(x).getAttributes().getNamedItem("value").getNodeValue();

                    //System.out.println("newelement.Platform.trim().toLowerCase() = " + newelement.Platform.trim().toLowerCase());
                    if (newelement.Platform.trim().toLowerCase().equals("ios")){
                        iOSobjectrepository.put(newelement.ElementName,newelement);
                    }

                    if (newelement.Platform.trim().toLowerCase().equals("android") ){
                        AndroidObjectRepository.put(newelement.ElementName,newelement);
                    }

                    if (newelement.Platform.trim().toLowerCase().equals("website")){
                        WebsiteObjectrepository.put(newelement.ElementName,newelement);
                    }

                    if (newelement.Platform.trim().toLowerCase().equals("mweb")){
                        Mwebobjectrepository.put(newelement.ElementName,newelement);
                    }



                }
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }



}