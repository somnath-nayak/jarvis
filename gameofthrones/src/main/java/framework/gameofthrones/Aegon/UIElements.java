package framework.gameofthrones.Aegon;


import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.commons.configuration.SubnodeConfiguration;
import org.apache.commons.configuration.XMLConfiguration;



public class UIElements

{
	
	private String uielementxmlname_mobile = "UIelements.xml";
	private String uielementxmlname_web = "WebUIelements.xml";

	private String configurationfolderpath;
	private XMLConfiguration uielementxmlconfig;
	public UIElements UIObjects;
	public HashMap<String, UIElement> uielements = new HashMap<String, UIElement>();

	public UIElements()
	{
		uielementxmlconfig = new XMLConfiguration();
		uielementxmlconfig.setDelimiterParsingDisabled(true); 
		try {
			System.out.println(System.getProperty("user.dir"));
			uielementxmlconfig.load(GetUIElementsxmlpath(System.getProperty("user.dir")+"/Data"));
		} catch (ConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		BuildUIObject(GetAllUIElements());
	}

	private void BuildUIObject(List<HierarchicalConfiguration> AllUIElements)
	{
		String name;
		int count = 1;
		for (HierarchicalConfiguration listitem : AllUIElements)
		{
			UIElement element = new UIElement();
			name = GetAttributevalue(listitem, "name");
			element.name = name;
			element.andoridValue = GetAttributevalue(listitem, "andoridValue");
			element.iOsValue=GetAttributevalue(listitem,"iOsValue");
			element.mWebValue=GetAttributevalue(listitem,"mwebValue");
			element.dWebValue=GetAttributevalue(listitem,"dWebValue");


			element.type = ElementIdentificationType
					.valueOf(GetAttributevalue(listitem, "type").toUpperCase());
			//System.out.println(name+"="+element.value);
			uielements.put(name, element);
		}

	}

	public String GetAttributevalue(HierarchicalConfiguration configuration,
			String Attributename)
	{
		
		String value = configuration.getString("[@" + Attributename + "]");
		return value;
	}

	public UIElement GetUIElementDetails(String elementid)
	{
		UIElement elementdetail = null;
		for (Entry<String, UIElement> item : uielements.entrySet())
		{
			if (item.getKey().equals(elementid))
			{
				elementdetail = item.getValue();
				//System.out.println("ELEMENTDETAIL=="+elementdetail.toString());
				break;
			}
		}
		return elementdetail;
	}
	
	private String GetUIElementsxmlpath(String Basefolderpath) {
		String configurationfilepath = "";
		configurationfolderpath = Basefolderpath;
		System.out.println("platform is=========================="+ System.getProperty("platform") + Basefolderpath);
		if(System.getenv("platform").contains("web")) {
			configurationfilepath = configurationfolderpath + "/"
					+uielementxmlname_web ;
		}
		else {
			configurationfilepath = configurationfolderpath + "/"
					+ uielementxmlname_mobile;
		}
		System.out.println("============="+configurationfilepath);
		return configurationfilepath;
	}
	public String GetUIElementsxmlpath() {
		String configurationfilepath = "";
		if(System.getenv("platform").contains("web")){
			configurationfilepath = configurationfolderpath + "/" + uielementxmlname_web;
		}
		else {
			configurationfilepath = configurationfolderpath + "/" + uielementxmlname_mobile;
		}

		return configurationfilepath;
	}
	public List<HierarchicalConfiguration> GetAllUIElements() {
		List<HierarchicalConfiguration> returnval = uielementxmlconfig.configurationsAt("Element");
		//System.out.println(returnval.size());
		return returnval;
	}

}
