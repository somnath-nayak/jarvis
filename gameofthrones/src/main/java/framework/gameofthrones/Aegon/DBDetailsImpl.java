package framework.gameofthrones.Aegon;

import framework.gameofthrones.Daenerys.Connstring;
import framework.gameofthrones.Tyrion.GenericTemplate;
import framework.gameofthrones.Tyrion.SqlTemplate;

import java.util.List;
import java.util.Properties;

/**
 * @author abhijit.p
 * @since 20 Jan 2018
 * DB Details Impl
 */
public class DBDetailsImpl implements DBDetails{

    protected void importDb(List<Connstring> connstrings){
        for(Connstring conn: connstrings){
            com.zaxxer.hikari.HikariDataSource dataSource = new com.zaxxer.hikari.HikariDataSource();
            Properties properties = new Properties();
            if(conn.getProtocol().equalsIgnoreCase("mysql")){
                dataSource.setDataSourceClassName("com.mysql.jdbc.jdbc2.optional.MysqlDataSource");
                dataSource.setIdleTimeout(10000);
                dataSource.setMaximumPoolSize(1);
                dataSource.setConnectionTimeout(10000);
                dataSource.setValidationTimeout(5000);
                dataSource.setInitializationFailFast(true);
                dataSource.setMaxLifetime(30000);
                properties.setProperty("port", String.valueOf(conn.getPort()));
            }else{
                dataSource.setDataSourceClassName("org.postgresql.ds.PGSimpleDataSource");
            }
            dataSource.setUsername(conn.getUsername());
            dataSource.setPassword(conn.getPassword());
            properties.setProperty("serverName", conn.getServer());
            properties.setProperty("databaseName", conn.getDbid());
            dataSource.setDataSourceProperties(properties);
            GenericTemplate dbTemplate = new GenericTemplate();
            dbTemplate.setDataSource(dataSource);
            if(!factory.containsSingleton(conn.getName())){
                factory.registerSingleton(conn.getName(), dbTemplate);
            }
        }
    }

    @Override
    public SqlTemplate getDatabaseService(String dbName) {
        System.out.println("Get Bean "+dbName);
        return factory.getBean(dbName, GenericTemplate.class);
    }
}
