package framework.gameofthrones.Baelish;

import framework.gameofthrones.Baelish.SEOData;
import org.jeasy.rules.annotation.Action;
import org.jeasy.rules.annotation.Condition;
import org.jeasy.rules.annotation.Fact;
import org.jeasy.rules.annotation.Rule;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

/**
 * Created by ashish.bajpai on 24/11/17.
 */
@Rule(name = "Status Code Check", description = "Check the url's status code" )
public class StatusCodeCheck {

    private String targeturl;
    private SEOData seodata;
    public static final String CLASS_NAME="Status Code";
    SEOResult sr=new SEOResult();

    /*public StatusCode(String URL){
        targeturl = URL;
    }*/

    public StatusCodeCheck(SEOData data){
        seodata = data;
        targeturl = seodata.URL;
    }



    @Condition
    public boolean statuscode(@Fact("statuscode") boolean statuscode){
        return statuscode;
    }

    @Action
    public void statuscodesuccess(){
    	sr.getURL(targeturl);
        Document doc = null;
        //String url = "ht  tps://www.swiggy.com/bangalore/restaurants";
        boolean followredirects = true;
        Connection.Response response1 = null;

        try {
            response1 = Jsoup.connect(targeturl).followRedirects(followredirects).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }


        int scode = response1.statusCode();
        //Checking if status code is 200 on hitting the url
        if (scode==200){
            System.out.println("scode = " + scode);
            sr.totalPass();
        }
        else{
            System.out.println("scode = " + scode);
            String failure=targeturl+": "+CLASS_NAME;
            sr.totalFail(failure);
        }

    }

}
