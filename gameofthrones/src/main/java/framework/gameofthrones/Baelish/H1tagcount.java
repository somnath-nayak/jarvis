package framework.gameofthrones.Baelish;

import java.io.IOException;
import java.util.List;

import org.jeasy.rules.annotation.Action;
import org.jeasy.rules.annotation.Condition;
import org.jeasy.rules.annotation.Fact;
import org.jeasy.rules.annotation.Rule;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

@Rule(name = "H1 tag count", description = "Total H1 tag count" )
public class H1tagcount {
	private String targeturl;
	public static final String CLASS_NAME="H1 Tag Count";
    private SEOData seodata;
     SEOResult sr=new SEOResult();
    
    public H1tagcount(SEOData data){
        seodata = data;
        targeturl = seodata.URL;
    }

    @Condition
    public boolean totalH1(@Fact("H1count") boolean H1count){
        return H1count;
    }
    
    @Action
    public void checkTotalH1(){
    	sr.getURL(targeturl);
        Document doc = null;
        try {
            //doc = Jsoup.connect(targeturl).cookie("dweb_ab", "new_dweb").followRedirects(true).get();
            doc = Jsoup.connect(targeturl).followRedirects(true).get();
            
        } catch (IOException e) {
            e.printStackTrace();
        }
        boolean returnval = false;
        
        int H1count=doc.getElementsByTag("H1").size();
        int h1count=doc.getElementsByTag("h1").size();
        System.out.println("Total H1 tags ="+h1count);
        
        boolean H1attribute=doc.getElementsByTag("H1").hasAttr("Display:none");
        if(H1attribute){
        	System.out.println("Bad SEO practice");
        }
        System.out.println("Total H1 tags ="+H1count);
        //Checking the H1 tag is present only once in any doc 
        if(H1count==0 || H1count>1){
        	Element body=doc.body();
        	String failure=targeturl+": "+CLASS_NAME;
        	
        	String bb=body.toString();
        	System.out.println(bb);
        	System.out.println("Page does not have any H1 tags or has more than 1");
        	sr.totalFail(failure);
        }
        else{
        	sr.totalPass();
        }
    }
}
