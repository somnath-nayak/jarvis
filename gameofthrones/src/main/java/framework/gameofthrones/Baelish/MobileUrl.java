package framework.gameofthrones.Baelish;

import framework.gameofthrones.Baelish.SEOData;
import org.jeasy.rules.annotation.Action;
import org.jeasy.rules.annotation.Condition;
import org.jeasy.rules.annotation.Fact;
import org.jeasy.rules.annotation.Rule;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

/**
 * Created by ashish.bajpai on 14/12/17.
 */
@Rule(name = "Mobile Url Check", description = "Check the mobile url's existence" )
public class MobileUrl {
    private String targeturl;
    private SEOData seodata;
    public static final String CLASS_NAME="Mobile URL";
    SEOResult sr=new SEOResult();

    /*public StatusCode(String URL){
        targeturl = URL;
    }*/

    public MobileUrl(SEOData data){
        seodata = data;
        targeturl = seodata.URL;
    }

    @Condition
    public boolean mobileurl(@Fact("mobileurl") boolean mobileurl){
        return mobileurl;
    }

    @Action
    public void checkmobileurl(){
    	sr.getURL(targeturl);
        Document doc = null;
        try {
            doc = Jsoup.connect(targeturl).get();
        } catch (IOException e) {
            e.printStackTrace();
        }
        boolean returnval = false;
        String nodeattribute = doc.select("link[rel=alternate]").attr("media");
        int nodesize = doc.select("link[rel=alternate]").size();
        System.out.println("nodeattribute = " + nodeattribute);
        if(nodeattribute.trim().length()>0 && nodesize > 0){
        	sr.totalPass();
        }
        else
        {
        	String failure=targeturl+": "+CLASS_NAME;
        	sr.totalFail(failure);
        }
        //return returnval;
    }



}
