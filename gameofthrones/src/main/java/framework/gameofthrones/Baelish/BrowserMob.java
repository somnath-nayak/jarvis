package framework.gameofthrones.Baelish;


import com.jayway.jsonpath.JsonPath;
import com.jayway.restassured.response.Response;
import framework.gameofthrones.Daenerys.Tools;
import net.lightbody.bmp.BrowserMobProxy;
import net.lightbody.bmp.BrowserMobProxyServer;
import net.lightbody.bmp.client.ClientUtil;
import net.lightbody.bmp.core.har.Har;
import net.lightbody.bmp.proxy.CaptureType;
import net.minidev.json.JSONArray;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.io.IOException;


public class BrowserMob {


    public static void main(String args[]){
        BrowserMobProxy proxy = new BrowserMobProxyServer();
        proxy.start(0);
        System.setProperty("webdriver.chrome.driver", "/Users/ashish.bajpai/git/swiggy_test/gameofthrones/Data/WebDrivers/chromedriver");
        // get the Selenium proxy object
        Proxy seleniumProxy = ClientUtil.createSeleniumProxy(proxy);

        // configure it as a desired capability
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(CapabilityType.PROXY, seleniumProxy);

        // start the browser up
        WebDriver driver = new ChromeDriver(capabilities);

        // enable more detailed HAR capture, if desired (see CaptureType for the complete list)
        proxy.enableHarCaptureTypes(CaptureType.REQUEST_CONTENT, CaptureType.RESPONSE_CONTENT);

        // create a new HAR with the label "yahoo.com"
        proxy.newHar("swiggy-dweb");

        // open yahoo.com
        driver.get("https://www.swiggy.com/bangalore/restaurants");
        driver.get("https://www.flipkart.com");
        driver.close();
        // get the HAR data

        try {
            Har har = proxy.getHar();
            har.writeTo(new File("/Users/ashish.bajpai/gothar.har"));
            proxy.abort();
            Tools tools = new Tools();
            JSONArray log  = JsonPath.parse(tools.readFileAsString("/Users/ashish.bajpai/gothar.har")).read("$.*");
            //Response response = new Response());
            System.out.println("log = " + log.toJSONString());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
