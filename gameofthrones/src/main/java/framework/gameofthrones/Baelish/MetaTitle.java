package framework.gameofthrones.Baelish;

import java.io.IOException;

import org.jeasy.rules.annotation.Action;
import org.jeasy.rules.annotation.Condition;
import org.jeasy.rules.annotation.Fact;
import org.jeasy.rules.annotation.Rule;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

@Rule(name = "Meta title", description = "Meta title is present" )
public class MetaTitle {
	private String targeturl;
    private SEOData seodata;
    public static final String CLASS_NAME="Meta Title";
    SEOResult sr=new SEOResult();
    
    public MetaTitle(SEOData data){
        seodata = data;
        targeturl = seodata.URL;
    }

    @Condition
    public boolean ismetaTitle(@Fact("metatitle") boolean metatitle){
        return metatitle;
    }
    
    @Action
    public void checkMetatitle(){
    	sr.getURL(targeturl);
        Document doc = null;
        try {
            doc = Jsoup.connect(targeturl).get();
        } catch (IOException e) {
            e.printStackTrace();
        }
        boolean returnval = false;
        String titletext = doc.select("title").text();
        boolean titlepresent=doc.select("title").hasText();
        System.out.println("title= "+titletext);
        System.out.println("title is present= "+titlepresent);
      //Checking if the meta description is present and has some text
        if(titlepresent==true){
        	System.out.println("MetaTitle is present");
        	sr.totalPass();
        }
        else
        {
        	String failure=targeturl+": "+CLASS_NAME;
        	sr.totalFail(failure);
        }
    }
}
