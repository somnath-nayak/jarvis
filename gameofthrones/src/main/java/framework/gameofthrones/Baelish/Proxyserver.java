package framework.gameofthrones.Baelish;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.jayway.jsonpath.JsonPath;
import io.advantageous.boon.core.Sys;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpResponse;
import net.lightbody.bmp.BrowserMobProxy;
import net.lightbody.bmp.BrowserMobProxyServer;
import net.lightbody.bmp.core.har.Har;
import net.lightbody.bmp.core.har.HarEntry;
import net.lightbody.bmp.core.har.HarLog;
import net.lightbody.bmp.filters.RequestFilter;
import net.lightbody.bmp.filters.ResponseFilter;
import net.lightbody.bmp.util.HttpMessageContents;
import net.lightbody.bmp.util.HttpMessageInfo;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.URI;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Predicate;

public class Proxyserver {
    BrowserMobProxy proxy;
    int port;
    ArrayList<Events> al;
    ArrayList<Events> al_diff;

    int counter=0;


    public Proxyserver(int port){
       proxy= new BrowserMobProxyServer();
       this.port=port;
       deleteFile();
       al=new ArrayList<>();

    }
    public Proxyserver(){
        proxy= new BrowserMobProxyServer();
        this.port=0;
        al=new ArrayList<>();

    }
    public void startServer() throws UnknownHostException{
        InetAddress IP= InetAddress.getLocalHost();
        System.out.println("IP of my system is := "+IP.getHostAddress());
        Inet4Address addresst=(Inet4Address)Inet4Address.getByName(IP.getHostAddress());
        proxy.setTrustAllServers(true);
        proxy.setMitmDisabled(false);
        proxy.newHar("citizen.har");
        proxy.start(port,addresst);
        requestFilter();
    }

    public void stopServer(){
        proxy.stop();
    }

    public void requestFilter(){
        proxy.addRequestFilter(new RequestFilter() {
            @Override
            public HttpResponse filterRequest(HttpRequest httpRequest, HttpMessageContents httpMessageContents, HttpMessageInfo httpMessageInfo) {
                System.out.println("Request Url originl = "+httpMessageInfo.getOriginalUrl());
                System.out.println("Request Url speed = "+proxy.getReadBandwidthLimit());
                System.out.println("Request Url speed = "+proxy.getWriteBandwidthLimit());
                Har har = proxy.getHar();
                HarLog log = har.getLog();
                File harFile = new File("citizen.har");
                //har.writeTo(harFile);
                //System.out.println("Request Info= "+httpMessageInfo.getOriginalRequest());
                try{
                    //System.out.println("\n");
                if(httpMessageInfo.getOriginalUrl().contains("54.255.142.198:9001") || httpMessageInfo.getOriginalUrl().contains("analytics.swiggy.com")){

                    /*List<HarEntry> entries = new CopyOnWriteArrayList<HarEntry>(log.getEntries());
                    System.out.println(entries);
                    for (HarEntry entry : entries) {
                        System.out.println(entry.getRequest().getUrl());
                    }
                    proxy.stop();*/

                    List<NameValuePair> params = URLEncodedUtils.parse(new URI(httpMessageInfo.getOriginalUrl().toString()), "UTF-8");
                    ObjectMapper mapper = new ObjectMapper();
                    counter++;
                    Events eventsObj=mapper.readValue(paramJson(httpMessageInfo.getOriginalUrl().toString().split("\\?")[1]).toString(),Events.class);

                    //writeOnFile(eventsObj);
                    //addtoList(eventsObj);
                    compareEvents(eventsObj);



                    for (NameValuePair param : params) {
                            //System.out.println(param.getName() + " : " + param.getValue());

                        }
                }
                }
                catch (Exception e){
                    System.out.println(e);
                }
                return null;
            }
        });
        proxy.addResponseFilter(new ResponseFilter() {
            @Override
            public void filterResponse(HttpResponse httpResponse, HttpMessageContents httpMessageContents, HttpMessageInfo httpMessageInfo) {
                System.out.println("Response = "+httpResponse.getStatus());
                //System.out.println("Response = "+httpMessageContents.getTextContents());

            }
        });
    }

    public static void main(String[] args) throws UnknownHostException{
        Proxyserver b=new Proxyserver(9091);
        b.startServer();

    }

    public String paramJson(String paramIn) {
        paramIn = paramIn.replaceAll("=", "\":\"");
        paramIn = paramIn.replaceAll("&", "\",\"");
        return "{\"" + paramIn + "\"}";
    }

    public  void addtoList(Events eventsObj){
        al.add(eventsObj);
        createJsonFormatAndWriteToFile();
        System.out.println(counter);

    }

    public void createJsonFormatAndWriteToFile(){
        try {
            System.out.println("Writing to file = "+System.getProperty("user.dir"));
            File fout_payload = new File(System.getProperty("user.dir") + "/events.txt");
            if(!fout_payload.exists()){
                fout_payload.createNewFile();
            }

            FileOutputStream fos_payload = new FileOutputStream(fout_payload, false);
            BufferedWriter bw_payload = new BufferedWriter(new OutputStreamWriter(fos_payload));
            ObjectMapper mapper = new ObjectMapper();
            bw_payload.write("{\"data\":"+mapper.writeValueAsString(al)+"}\n");
            JSONObject json=new JSONObject();
            //bw_payload.write(json.toJSONString());
            bw_payload.close();
        }
        catch(Exception e){
            System.out.println(e);

        }
    }


    public void writeOnFile(Events eventsObj){
        try {
            System.out.println(System.getProperty("user.dir"));
            File fout_payload = new File(System.getProperty("user.dir") + "/events.txt");
            if(!fout_payload.exists()){
                fout_payload.createNewFile();
            }

            FileOutputStream fos_payload = new FileOutputStream(fout_payload, true);

            BufferedWriter bw_payload = new BufferedWriter(new OutputStreamWriter(fos_payload));
            ObjectMapper mapper = new ObjectMapper();
            bw_payload.write(mapper.writeValueAsString(eventsObj)+",\n");
            bw_payload.close();
        }
        catch(Exception e){
            System.out.println(e);

        }
    }
    public void deleteFile(){
        File file = new File(System.getProperty("user.dir") + "/events.txt");

        if(file.delete())
        {
            System.out.println("File deleted successfully");
        }
    }

    public ArrayList<Events> compareEvents(Events events){
        try {
            FileReader filea = new FileReader(System.getProperty("user.dir") + "/events_ref.txt");
            BufferedReader reader = new BufferedReader(filea);
            String json = "";
            try {
                StringBuilder sb = new StringBuilder();
                String line = reader.readLine();

                while (line != null) {
                    sb.append(line);
                    sb.append("\n");
                    line = reader.readLine();
                }
                json = sb.toString();
            } finally {
                reader.close();
            }
            JSONObject object = new JSONObject(json);
            JSONArray array = object.getJSONArray("data");
            ObjectMapper mapper = new ObjectMapper();
            ArrayList<Events> nodeList = new ArrayList<>();
            for(int i=0; i<array.length(); i++){ // loop through the nodes
                nodeList.add(mapper.readValue(array.getJSONObject(i).toString(),Events.class));
            }

            Predicate<Events> p1 = e -> e.getP().equals(events.getP()) && e.getE().equals(events.getE()) && e.getSn().equals(events.getSn()) && e.getOn().equals(events.getOn()) && e.getOp().equals(events.getOp()) && e.getSc().equals(events.getSc());


            //boolean name2Exists = nodeList.stream().anyMatch(e -> e.getP().equals(e.getP()));
            boolean isPresent=nodeList.stream().anyMatch(p1);
            System.out.println("Hello = " +isPresent);
            events.setPresent(nodeList.stream().anyMatch(p1));
            addtoList(events);




        }
        catch(Exception e){
            System.out.println(e);
        }





        return null;
    }


}
