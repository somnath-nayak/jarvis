package framework.gameofthrones.Baelish;

import java.io.IOException;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;


public class SEOEmail {
	String to = "anshal.anand@swiggy.in";
	String to1 = "engg-portal@swiggy.in";
	String to2 = "thejasvi.bhat@swiggy.in";
	String to3 = "ashish.bajpai@swiggy.in";
	//String from = "testportalqa@gmail.com";
	String from = "swiggytest@swiggy.in";
	final String username = "swiggytest@swiggy.in";//change accordingly
	//final String password = "portalQA";//change accordingly
	final String password = "swiggytest@123";
	static int total;
	static int count=0;
	static int j=0;
	static String finalreport;
	static String[][] result=new String[200][4]; 
	static String[] failure_results=new String[600];
	static String url;
	public static int pass=0;
	static int count_replacer=0;
	public static String failState="";
	String host = "smtp.gmail.com";

	//Getting total checks
	public void total(String Url,int totalall){
		total=totalall;
		url=Url;

	}
	
	//Getting total pass
	public void getpass(int pass_val){
		pass=pass_val;
		setResult();

	}
	
	//Getting failure list
	public void getfailurelist(String fail){
		failure_results[j]=fail;
		j++;
		if(j !=0){
			setFailedState();
		}
	}
	
	//Setting Test state
	public void setFailedState(){
		failState = ":FAILED";
	}
	
	//Setting the total,pass,fail on to an array
	public void setResult(){
		if(pass+(total-pass)==total){
			String[] row_val=new String[4];
			row_val[0]=url;
			row_val[1]=String.valueOf(total);
			row_val[2]=String.valueOf(pass);
			row_val[3]=String.valueOf(total-pass);
			for(int i=0;i<4;i++){
				result[count][i]=row_val[i];
			}
			count++; pass=0;}
	}

	Properties props = new Properties();

	public void setProperties(){
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.socketFactory.port", "465");   
		props.put("mail.smtp.socketFactory.class",    
				"javax.net.ssl.SSLSocketFactory"); 
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.port", "465");
	}
	
	//Creating the results data
	private String getrowhtml(){
        String row = "<tr><td border='1';>${URL}</td><td border='1';>${total}</td>"
        		+ "<td border='1';>${pass}</td><td border='1';>${fail}</td></tr>";
        return row;
    }

	//Entering the data
	private String replaceDefault(String data){
		
		String data_replace=data;
		data_replace=data_replace.replace("${URL}", result[count_replacer][0]);
		data_replace=data_replace.replace("${total}", result[count_replacer][1]);
		data_replace=data_replace.replace("${pass}", result[count_replacer][2]);
		data_replace=data_replace.replace("${fail}", result[count_replacer][3]);
        count_replacer++;
        return data_replace;
    }
	
	Session session = Session.getInstance(props,
			new javax.mail.Authenticator() {
		protected PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(username, password);
		}
	});

	public void sendEmail() throws Exception{
		try {
			
			// Create a default MimeMessage object.
			Message message = new MimeMessage(session);

			// Set From: header field of the header.
			message.setFrom(new InternetAddress(from));

			// Set To: header field of the header.
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(to));
			Address[] tocc=new Address[] {new InternetAddress(to1),new InternetAddress(to2),new InternetAddress(to3)};
			//Address[] tocc=new Address[] {new InternetAddress(to)};//,new InternetAddress(to2),new InternetAddress(to3)};
			message.setRecipients(Message.RecipientType.CC, tocc);
			
			// Set Subject: header field
			message.setSubject("SEO Check Report "+failState);

			 
			String allpart1="<Center><h1>SEO Check Report</h1><br></Center>  \n\t\t\t\t\t\t\t Hi,<br>"
					+ " \n\t\t\t\t\t\t\t   Please find the report for SEO check for all the different URL\'s. \n\t\t\t\t\t\t\t  <br>"
					+ "<table border='1';> \n\t\t\t\t\t\t\t  "
					+ " <tr > \n\t\t\t\t\t\t\t   "
					+ "<th border='1';><b>URL</b></th><th border='1';><b>Total</b></th> \n\t\t\t\t\t\t\t   "
					+ "<th border='1';><b>Passed</b></th><th border='1';><b>Failed</b></th><tr> ";

			
			
				
			
			String table_data="";
						
			for(int j=0;j<count;j++){
				
					String default_data=getrowhtml();
					table_data=table_data+replaceDefault(default_data);
					
				}
			String allpart2=table_data;
			
			
			String allpart3="</table><br>Failure case<br>";
			

			

			String overallBody="";
			for(int i=0;i<j;i++){

				String failure_case=failure_results[i];
				overallBody=overallBody+" --> "+failure_case+"<br>";
				
			}
			String allpart4=overallBody;
			String all=allpart1+allpart2+allpart3+allpart4;

			// Send message
			System.out.println(all);
			message.setContent(all, "text/html");
			Transport.send(message);

			System.out.println("Sent message successfully....");

		} catch (MessagingException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
}
