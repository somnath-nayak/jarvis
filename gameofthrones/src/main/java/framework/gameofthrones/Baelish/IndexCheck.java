package framework.gameofthrones.Baelish;

import framework.gameofthrones.Baelish.SEOData;
import org.jeasy.rules.annotation.Action;
import org.jeasy.rules.annotation.Condition;
import org.jeasy.rules.annotation.Fact;
import org.jeasy.rules.annotation.Rule;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

/**
 * Created by ashish.bajpai on 14/12/17.
 */
@Rule(name = "Index in Robots text", description = "Index exists in robots meta tag" )
public class IndexCheck {
    private String targeturl;
    private SEOData seodata;
    public static final String CLASS_NAME="IndexCheck";
    SEOResult sr=new SEOResult();

    

    public IndexCheck(SEOData data){
        seodata = data;
        targeturl = seodata.URL;
    }

    @Condition
    public boolean mobileurl(@Fact("index") boolean index){
        return index;
    }

    @Action
    public void checkmobileurl(){
    	sr.getURL(targeturl);
        Document doc = null;
        try {
            doc = Jsoup.connect(targeturl).get();
            
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        String nodeattribute = doc.select("meta[name=robots]").attr("content");
        int nodesize = doc.select("meta[name=ROBOTS]").size();

        System.out.println("nodeattribute = " + nodeattribute);
        
        //Checking if index attribute is present 
        if (nodeattribute.trim().length() > 0 && nodesize > 0 && nodeattribute.split(",")[0].trim().toLowerCase().equals( "index")) {
           System.out.println("Index Check Done");
           sr.totalPass();
        }
        else
        {
        	String failure=targeturl+": "+CLASS_NAME;
        	sr.totalFail(failure);
        }
    }



}
