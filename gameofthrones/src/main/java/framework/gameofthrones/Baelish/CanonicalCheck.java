package framework.gameofthrones.Baelish;

import java.io.IOException;

import org.jeasy.rules.annotation.Action;
import org.jeasy.rules.annotation.Condition;
import org.jeasy.rules.annotation.Fact;
import org.jeasy.rules.annotation.Rule;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/* Created by Anshal on 2/1/18*/
@Rule(name = "Canonical in Doc file", description = "Canonical URL is present" )
public class CanonicalCheck {
	private String targeturl;
    private SEOData seodata;
    public static final String CLASS_NAME="Canonical Check";
    SEOResult sr=new SEOResult();
    
    public CanonicalCheck(SEOData data){
        seodata = data;
        targeturl = seodata.URL;
    }

    @Condition
    public boolean isCanonical(@Fact("canonical") boolean canonical){
        return canonical;
    }
    
    @Action
    public void checkCanonical(){
    	sr.getURL(targeturl);
        Document doc = null;
        try {
            doc = Jsoup.connect(targeturl).get();
        } catch (IOException e) {
            e.printStackTrace();
        }
        boolean returnval = false;
        String nodeattribute = doc.select("link[rel=canonical]").attr("href");
        boolean linkpresent=doc.select("link[rel=canonical]").hasAttr("href");
        int nodesize = doc.select("link[rel=canonical]").size();
        System.out.println("nodeattribute = " + nodeattribute);
        System.out.println("link present = " + linkpresent);
        
        //Checking if the canonical link is present in the doc
        if(nodeattribute.trim().length()>0 && linkpresent==true && nodesize > 0 ){
        	sr.totalPass();
        }
        else
        {
        	String failure=targeturl+": "+CLASS_NAME;
        	sr.totalFail(failure);
        }
    }
}
