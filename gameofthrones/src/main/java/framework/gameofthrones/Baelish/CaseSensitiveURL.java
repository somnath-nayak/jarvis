package framework.gameofthrones.Baelish;

import java.io.IOException;

import org.jeasy.rules.annotation.Action;
import org.jeasy.rules.annotation.Condition;
import org.jeasy.rules.annotation.Fact;
import org.jeasy.rules.annotation.Rule;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

@Rule(name = "Case sensitive URL", description = "Lower and upper case url point to same site" )
public class CaseSensitiveURL {
	private String targeturl;
    private SEOData seodata;
    private String canonical_original=null;
    private String changed_canonical=null;
    SEOResult sr=new SEOResult();
    public static final String CLASS_NAME="Case Sensitive URL";
    boolean returnval = false;
	 public CaseSensitiveURL(SEOData data){
	        seodata = data;
	        targeturl = seodata.URL;
	    }



	    @Condition
	    public boolean isCaseSensitive(@Fact("casesensitiveurl") boolean casesensitiveurl){
	        return casesensitiveurl;
	    }
	    
	    @Action
	    public void checkCaseSensitive(){
	    	sr.getURL(targeturl);
	    	Document doc=null;
	    	String url_lowercase=targeturl.toLowerCase();
	    	String url_uppercase=targeturl.toUpperCase();
	    	
	    	//Getting the original canonical
	    	try {
				doc=Jsoup.connect(targeturl).get();
				canonical_original=doc.select("link[rel=canonical]").attr("href");
				System.out.println("Original Canonical = "+canonical_original);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	    	char[] url_random=url_lowercase.toCharArray();
	    	for(int i=0;i<url_random.length;i++){
	    		if(url_random[i]=='a' || url_random[i]=='e' || url_random[i]=='i' || url_random[i]=='o' || url_random[i]=='u' ){
	    			char x=url_random[i];
	    			url_random[i]=Character.toUpperCase(x);
	    		}
	    		
	    	}
	    	
	    	String random_url=new String(url_random);
	    	System.out.println("Upper case URL ="+url_uppercase);
	        System.out.println("Lower case URL ="+url_lowercase);
	        System.out.println("Random case URL ="+random_url);
	        
	        //Getting the changed canonical
	        try {
				changed_canonical=(Jsoup.connect(random_url).get()).select("link[rel=canonical]").attr("href");
				System.out.println("Changed Canonical = "+changed_canonical);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	        
	    	boolean followredirects = true;
	    	boolean followredirectsfalse = false;
	        Connection.Response response1 = null;
	        
	        Connection.Response response3 = null;
	        
	        //Storing the status code with redirect and without
	        try {
	            
	            response1 = Jsoup.connect(random_url).followRedirects(followredirects).execute();
	            //response2 = Jsoup.connect(url_uppercase).followRedirects(followredirectsfalse).execute();
	            response3 = Jsoup.connect(random_url).followRedirects(followredirectsfalse).execute();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	        
	        
	        int scode1 = response3.statusCode();
	        int scode2 = response1.statusCode();
	        
	        //Checking if the status on without redirect is 301 and with redirect is 200
	        if (scode1==301 || scode1==307){
	            System.out.println("scode = " + scode1);
	            if(scode2==200){
	            	returnval=true;
	            	System.out.println("Redirection successful");
	            	sr.totalPass();
	            }
	        }
	        //Checking canonical is same as before
	        else if(scode1==200){
	        	if(canonical_original.equals(changed_canonical)){
	        		returnval=true;
	            	System.out.println("Case sensitive canonical successful");
	            	sr.totalPass();
	        	}
	        }
	        else{
	            System.out.println("scode = " + scode1);
	            System.out.println("Changed Canonical = " + changed_canonical);
	            String failure=targeturl+": "+CLASS_NAME;
	            sr.totalFail(failure);
	        }
	        
	        
	        
	        
	    }
}
