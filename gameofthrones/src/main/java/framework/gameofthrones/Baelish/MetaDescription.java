package framework.gameofthrones.Baelish;

import java.io.IOException;

import org.jeasy.rules.annotation.Action;
import org.jeasy.rules.annotation.Condition;
import org.jeasy.rules.annotation.Fact;
import org.jeasy.rules.annotation.Rule;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

@Rule(name = "Meta Description", description = "Meta Description is present" )
public class MetaDescription {
	private String targeturl;
    private SEOData seodata;
    public static final String CLASS_NAME="Meta Description";
    boolean returnval = false;
    SEOResult sr=new SEOResult();
    
    public MetaDescription(SEOData data){
        seodata = data;
        targeturl = seodata.URL;
    }

    @Condition
    public boolean isMetaDescription(@Fact("metadescription") boolean metadescription){
        return metadescription;
    }
    
    @Action
    public void checkMetaDescription(){
    	sr.getURL(targeturl);
        Document doc = null;
        try {
            doc = Jsoup.connect(targeturl).get();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        String metadescription = doc.select("meta[name=description]").attr("content");
        
        System.out.println("Meta Description= "+metadescription);
        //Checking if the length of meta description is more than 0
        if(metadescription.length()>=0){
        	System.out.println("Meta Description present");
        	sr.totalPass();
        }
        else
        {
        	String failure=targeturl+": "+CLASS_NAME;
        	sr.totalFail(failure);
        }
    }
}
