package framework.gameofthrones.Baelish;

import java.io.IOException;

import org.jeasy.rules.annotation.Action;
import org.jeasy.rules.annotation.Condition;
import org.jeasy.rules.annotation.Fact;
import org.jeasy.rules.annotation.Rule;
import org.jsoup.Connection;
import org.jsoup.Jsoup;


@Rule(name = "URLWithoutwww", description = "URL without www in slug" )
public class URLWithoutwww {
	private String targeturl;
    private SEOData seodata;
    SEOResult sr=new SEOResult();
    public static final String CLASS_NAME="URL without www";
	 public URLWithoutwww(SEOData data){
	        seodata = data;
	        targeturl = seodata.URL;
	    }



	    @Condition
	    public boolean isurlwithoutwww(@Fact("urlwithoutwww") boolean urlwithoutwww){
	        return urlwithoutwww;
	    }
	    
	    @Action
	    public void checkURLwww(){
	    	sr.getURL(targeturl);
	    	String url=targeturl;
	    	String urlwithoutwww=url.replace("www.", "");
	        
	    	boolean followredirects = true;
	    	boolean followredirectsfalse = false;
	        Connection.Response response1 = null;
	        Connection.Response response2 = null;
	        
	        System.out.println("original URL="+url);
	        System.out.println("without www URL="+urlwithoutwww);
	        
	        try {
	            
	            response1 = Jsoup.connect(urlwithoutwww).followRedirects(followredirects).execute();
	            response2 = Jsoup.connect(urlwithoutwww).followRedirects(followredirectsfalse).execute();
	            
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	        
	        
	        int scode1 = response2.statusCode();
	        int scode2 = response1.statusCode();
	        
	        //Checking if redirection is successful for url without www
	        if (scode1==301 || scode1==307){
	            System.out.println("scode = " + scode1);
	            if(scode2==200){
	            	System.out.println("Redirection successful for url without www");
	            	sr.totalPass();
	            }
	        }
	        else{
	            System.out.println("scode = " + scode1);
	            String failure=targeturl+": "+CLASS_NAME;
	            sr.totalFail(failure);
	        }
	        
	        
	        
	        
	    }
}
