package framework.gameofthrones.Baelish;

import org.apache.kafka.common.metrics.Stat;

/**
 * Created by ashish.bajpai on 23/11/17.
 */
public class SEOData {

    public String Category;
    public String SubCategory;
    public String URL;
    public String StatusCode;
    public String MobileUrl;
    public String Index;
    public String NoIndex;
    public String Canonical;
    public String AndroidURL;
    public String iOSUrl;
    public String Casesensitiveurl;
    public String withoutwww;
    public String Metatitle;
    public String MetaDescription;
    public String RobotsTxt;
    public String GA;
    public String ComScoreCode;
    public String MobileFriendliness;
    public String HeaderCheck;
    public String StaticAssets;
    public String Check301;
    public String PageSpeedScore;
    public String H1TagCount;

    public SEOData(String _Category, String _SubCategory, String _url, String _statuscode, String _mobileurl,
                   String _index, String _noindex, String _canonical, String _androidurl, String _iosurl,
                   String _casesensitiveurl, String _withoutwww, String _metatitle, String _metadescription,
                   String _robotstxt, String _ga, String _comscorecode, String _mobilefriendliness, String _headercheck,
                   String _staticassets, String _check301, String _PageSpeedScore, String _h1tagcount){
        Category = _Category;
        SubCategory = _SubCategory;
        URL = _url;
        StatusCode = _statuscode;
        MobileUrl = _mobileurl;
        Index = _index;
        NoIndex = _noindex;
        Canonical = _canonical;
        AndroidURL = _androidurl;
        iOSUrl = _iosurl;
        Casesensitiveurl = _casesensitiveurl;
        withoutwww = _withoutwww;
        Metatitle = _metatitle;
        MetaDescription = _metadescription;
        RobotsTxt = _robotstxt;
        GA = _ga;
        ComScoreCode = _comscorecode;
        MobileFriendliness = _mobilefriendliness;
        HeaderCheck = _headercheck;
        StaticAssets = _staticassets;
        Check301 = _check301;
        PageSpeedScore = _PageSpeedScore;
        H1TagCount = _h1tagcount;
    }
}
