
package framework.gameofthrones.Cersei;

import com.sun.jna.platform.win32.Sspi;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.RetryAnalyzer;
import org.apache.commons.lang.ArrayUtils;
import org.apache.log4j.Logger;
import org.testng.IRetryAnalyzer;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;
import org.testng.annotations.AfterSuite;
import org.testng.util.Strings;
import org.testng.xml.XmlSuite;

import java.io.File;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;


public class GameOfThronesListener extends TestListenerAdapter implements
        IRetryAnalyzer {




    static Logger log = Logger.getLogger(GameOfThronesListener.class);
    public static int passed = 0, failed = 0, skipped = 0, executed = 0,executiontrycount = 0, maxretrycount = 0,dataproviderSkip = 0;
    static ITestContext TestContext = null;
    //public static MailData mailer;
    private Timestamp starttime = null;
    private Timestamp endtime = null;
    public String ExecutionTime = null;
    String methodNameThis, methodNamePrev = "";
    static String expected, actual;
    static boolean forSingleTestCase = false, toberetried = false;
    static String jSonResponse = "";
    //String enableJenkinReport, responseDir, generateResponse, reportFilePath;
    //boolean wantJenkinReport = false;
    boolean comparekeyvaluepair = false;
    public static ArrayList<String> testMethodsEntries = new ArrayList<>();
    private List<TestExecutionReportRow> reportrows = new ArrayList<TestExecutionReportRow>();
    private String Emaillist = "ashish.bajpai@swiggy.in";
    public Initialize initialize;
    public static TestExecutionReport Mailer;
    public static MailData MailContent;
    private String Environment;

    // public static
    /*public GameOfThronesListener()
    {
        //mailer = new MailData(MailType.TestRunReport);
        //setAllReportingParams();
    }*/

    public GameOfThronesListener(){
        deletePreviousReportFile();
        Initialize init = new Initialize();
        System.out.println("init = " + init.EnvironmentDetails.setup.getEnvironment());
        Environment = init.EnvironmentDetails.setup.getEnvironment();
    }



    @Override
    public void onTestFailure(ITestResult tr)
    {
        TestInvocationListener invocationListener = new TestInvocationListener();


        failed++;

        addrow(tr);
        /*if (executiontrycount >= maxretrycount
                || null == tr.getMethod().getRetryAnalyzer())
        {
            failed++;
            addrow(tr);

        } else
        {
            tr.setStatus(1);
        }
        executiontrycount++;*/


        /*System.out.println("FAILED");
        TestInvocationListener testinvocation = new TestInvocationListener();
        log.info("Test Result while failure :" + tr.getStatus());
        String inputParams = ArrayUtils.toString(tr.getParameters());
        log("FAILED");
        if (executiontrycount >= maxretrycount
                || null == tr.getMethod().getRetryAnalyzer())
        {
            // Add to failed set only if the retry count has maxed out
            failed++;
            addrow(tr);

        } else
        {
            // Reset the test fail status if retry count has not maxed out
            tr.setStatus(1);
        }
        executiontrycount++;
        methodNamePrev = tr.getMethod().getMethodName();
        testMethodsEntries.add(methodNamePrev);*/

    }

    @Override
    public void onTestSkipped(ITestResult tr)
    {

        log("SKIPPED");
        skipped++;
        //addrow(tr);
        if(tr.getMethod().getRetryAnalyzer() != null  &&  tr.getMethod().getRetryAnalyzer() instanceof RetryAnalyzer){
            RetryAnalyzer retryAnalyzer = (RetryAnalyzer) tr.getMethod().getRetryAnalyzer();
            if(retryAnalyzer.isRetryAvailableForValidateSkipTest()){
                log.info("Retry available, not logging failed result to DB for Method " + tr.getMethod().getMethodName());
                if(tr.getParameters().length<=0) {
                    addrow(tr);
                    dataproviderSkip++;
                }
                return;
            }
        }
        methodNamePrev = tr.getMethod().getMethodName();
        testMethodsEntries.add(methodNamePrev);
        String inputParams = ArrayUtils.toString(tr.getParameters());
        // ReportGenerator.reportGenerator(ArrayUtils.toString(tr.getParameters()),
        // expected, actual, true);

    }

    private void log(String string)
    {
        log.info(string);
    }

    @Override
    public void onTestSuccess(ITestResult tr)
    {
        executiontrycount = 0;
        String inputParams = ArrayUtils.toString(tr.getParameters());
        log("PASSED");
        log.info("Test Result while success :" + tr.getStatus());
        passed++;
        addrow(tr);

        methodNamePrev = tr.getMethod().getMethodName();
        testMethodsEntries.add(methodNamePrev);

    }

    @Override
    public void onTestStart(ITestResult result)
    {
        methodNameThis = result.getMethod().getMethodName();
        System.out.println("'TEST EXECUTION STARTED ##TC_NAME##:: "
                + methodNameThis + "'");
        /*if (mailer.TestSuiteName == "")
        {
            mailer.TestSuiteName = result.getTestContext().getSuite().getName();
        }*/
        executed++;
        /*if (!methodNameThis.equalsIgnoreCase(methodNamePrev))
        {
            if (methodNamePrev.length() == 0)
            {
                forSingleTestCase = `true;
            } else
            {
                if (wantJenkinReport)
                {
                    ReportGenerator.tearDownReport(methodNamePrev,
                            reportFilePath);
                    ReportGenerator.reportInit();
                }
            }

        }*/

    }

    @Override
    public void onStart(ITestContext testContext)
    {
        TestContext = testContext;
        starttime = new Timestamp(TestContext.getStartDate().getTime());
        Emaillist = TestContext.getSuite().getXmlSuite().getParameter("emails");
        /*System.out.println("paramvalue = " + paramvalue);
        log.info(paramvalue);*/

    }

    @Override
    public void onFinish(ITestContext testContext)
    {

        endtime = new Timestamp(TestContext.getEndDate().getTime());
        System.out.println("starttime = " + starttime);
        System.out.println("endtimw = " + endtime);

        GetExecutionTime();
        String environment = testContext.getSuite().getXmlSuite().getParameter("environment");
        String suitename = testContext.getSuite().getXmlSuite().getName();
        //MailReportSummary summary = new MailReportSummary()
        //MailData data = new MailData();
        System.out.println("Emaillist = " + Emaillist);
        //MetaDataMail mailmeta = new MetaDataMail(Emaillist, "", "", getSubjectLine(testContext));
        MailReportSummary summary = new MailReportSummary(Environment, String.valueOf(executed),
                String.valueOf(failed), String.valueOf(passed), String.valueOf(skipped), getResult(), ExecutionTime,String.valueOf(dataproviderSkip));
        MailContent = new MailData(summary, reportrows);
        //TestExecutionReport sendmail = new TestExecutionReport(reportrows,Emaillist);
        //TestExecutionReport sendmail;
        Mailer = new TestExecutionReport(MailContent);

        Initialize init = new Initialize();
        //GameOfThronesListener.Mailer.Environment = init.EnvironmentDetails.setup.getEnvironment();
        GameOfThronesListener.Mailer.sendhtmlreport();
    }

    private String getSubjectLine(ITestContext tc){
        String subjectline = "Test Execution Report -  ["+getResult()+"] - ${Env}"+" - "+tc.getSuite().getXmlSuite().getName();
        return subjectline;
    }

    private String getResult(){
        String result = "FAIL";
        if (executed > 0 && failed == 0){
            result  = "PASS";
        }

        if (executed > 0 && failed == 0 && skipped > 1){
            result  = "FAIL";
        }
        return result;
    }

    private void addnewrow(String testname, String result, String parameters, String failurereason){
        TestExecutionReportRow newrow = new TestExecutionReportRow(testname, result, parameters, failurereason);
        reportrows.add(newrow);
        System.out.println("result = " + newrow.Result);
    }

    private void addrow(ITestResult result)
    {
        String parameters = "";
        String executionresult = "FAIL";
        Object[] params = result.getParameters();
        String cause = "";
        if (result.isSuccess())
        {
            executionresult = "PASS";
        } else
        {
            cause = processCause(result);
        }
        if (params.length > 0)
        {
            for (int i = 0; i < params.length; i++)
            {
                if(params[i] == null)
                    params[i] = "null";
                if(params[i] == "" || params[i] == " ")
                    params[i] = " ";
                parameters = parameters + params[i].toString() + "  ";
            }
        }
        int retriescount = getExecutionRetriesCount(result.getMethod().getMethodName());
        String inputparameters = ArrayUtils.toString(result.getParameters());
        //String testname = result.getName();

        String testname = result.getMethod().getDescription();
        if (Strings.isNullOrEmpty(testname)){
            testname = result.getName();
        }
        String failurereason = processCause(result);
        
        //TestExecutionReportRow newrow = new TestExecutionReportRow(testname, executionresult, inputparameters, failurereason);
        addnewrow(testname, executionresult, inputparameters, failurereason);
        //System.out.println("New Row is added");

        System.out.println(result.getMethod().getMethodName() + " : "
                + executionresult + " " + cause + " " + parameters);
    }




    private String processCause(ITestResult result)
    {
        /*String cause;
        cause = result.getThrowable().getClass().getSimpleName();
        if (cause.equals("AssertionError"))
        {
            cause = result.getThrowable().getMessage();
        }
        if (cause.length() > 100)
        {
            cause = cause.substring(0, 100) + "...";
        }
        cause = cause.replace("<", "&lt;");
        cause = cause.replace(">", "&gt;");
        return cause;*/

        String cause = "";
        if (!result.isSuccess()) {
            cause = result.getThrowable().getMessage();
            //System.out.println("cause = " + cause.length());
            if(Strings.isNotNullAndNotEmpty(cause)) {
                if (cause.length() > 100) {
                    cause = cause.substring(0, 100) + "...";
                }
            }
        }
        return cause;

    }

    public String getAbsolutePath(String relativePath)
    {
        File path = new File(relativePath.substring(2, relativePath.length()));
        return path.getAbsolutePath();

    }

    private void checkAndCreateDir(String reportPath, String responsePath)
    {
        File report, response;
        if (reportPath.contains("."))
            report = new File(reportPath.substring(2, reportPath.length()));
        else
            report = new File(reportPath);

        if (responsePath.contains("."))
            response = new File(
                    responsePath.substring(2, responsePath.length()));
        else
            response = new File(responsePath);

        if (!report.getAbsoluteFile().exists())
            report.getAbsoluteFile().mkdirs();
        if (!response.getAbsoluteFile().exists())
            response.getAbsoluteFile().mkdirs();
    }

    private void checkAndCreateDir(String reportPath)
    {
        File report;
        if (reportPath.contains("."))
            report = new File(reportPath.substring(2, reportPath.length()));
        else
            report = new File(reportPath);

        System.out.println("Absolute Path" + report.getAbsoluteFile());
        System.out.println(report.getAbsoluteFile().exists());
        if (!report.getAbsoluteFile().exists())
            report.getAbsoluteFile().mkdirs();
    }

    /*public void setAllReportingParams()
    {
        System.out.println("Inside setAllReportingParams...");
        enableJenkinReport = System.getenv("enablejenkinreport");
        responseDir = System.getenv("responsedir");
        generateResponse = System.getenv("generateresponse");
        comparekeyvaluepair = Boolean.parseBoolean(System
                .getenv("comparekeyvaluepair"));
        System.out.println("Enable Jenkin Reports :" + enableJenkinReport);
        System.out.println("Response Dir Path :" + responseDir);
        System.out.println("Generate Response :" + responseDir);
        System.out.println("Compare key-value of response :"
                + comparekeyvaluepair);
        if (enableJenkinReport != null)
        {
            reportFilePath = enableJenkinReport.split(",")[1].trim();
            wantJenkinReport = Boolean.parseBoolean(enableJenkinReport
                    .split(",")[0].trim());
        } else
        {
            wantJenkinReport = false;
            reportFilePath = "";
            System.out.println("Jenkin Report is disabled");
        }

        if (responseDir != null && reportFilePath != "")
        {
            checkAndCreateDir(reportFilePath, responseDir);
            System.out.println("First condition");
        } else if (responseDir == null)
        {
            checkAndCreateDir(reportFilePath);
            System.out.println("Sec condition");
        }
        if (responseDir != null && reportFilePath == "")
        {
            checkAndCreateDir(responseDir);
            System.out.println("Third condition");
        }
    }*/

    private void GetExecutionTime()
    {
        String stime = starttime.toString();
        String etime = endtime.toString();
        SimpleDateFormat format = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss.SSS");
        Date d1 = null;
        Date d2 = null;
        try
        {
            d1 = format.parse(stime);
            d2 = format.parse(etime);
            long diff = d2.getTime() - d1.getTime();
            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffDays = diff / (24 * 60 * 60 * 1000);

            log.info(diffDays + " days, ");
            log.info(diffHours + " hours, ");
            log.info(diffMinutes + " minutes, ");
            log.info(diffSeconds + " seconds.");
            ExecutionTime = Long.toString(diffSeconds);
            log.info("ExecutionTime = " + ExecutionTime);
        } catch (Exception e)
        {
            e.printStackTrace();

        }

    }

    @Override
    public boolean retry(ITestResult iTestResult)
    {
        if (executiontrycount <= maxretrycount)
        {
            return true;
        }
        executiontrycount = 0;
        return false;
    }



    private void deletePreviousReportFile()
    {
        String pwd = System.getProperty("user.dir");
        File file = new File(pwd+"/newreport.html");
        if(file.delete())
        {
            System.out.println("File deleted successfully " + file.getAbsolutePath());
        }
        else
        {
            System.out.println("Failed to delete the file "+ file.getAbsolutePath());
        }
    }


    private int getExecutionRetriesCount(String methodName) {
        return Collections.frequency(testMethodsEntries, methodName);
    }
}

