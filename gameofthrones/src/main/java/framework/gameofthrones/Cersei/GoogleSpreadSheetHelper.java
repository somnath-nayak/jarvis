package framework.gameofthrones.Cersei;

/**
 * @author manu.chadha
 */

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.List;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.api.services.sheets.v4.model.ValueRange;
import io.advantageous.boon.core.Sys;

public class GoogleSpreadSheetHelper {


    private static final String APPLICATION_NAME = "Google Sheets APIS";
    /*P12 file location*/
    private final static String P12FILE = "../../../swiggy-got-c466599cb349.p12";

    private final static String SERVICE_ACCOUNT_ID = "admin-744@swiggy-got.iam.gserviceaccount.com";

    private static final List<String> SCOPES = Arrays.asList(SheetsScopes.SPREADSHEETS_READONLY);

    private static JsonFactory getJsonFactory()
    {
        return JacksonFactory.getDefaultInstance();
    }

    private static HttpTransport getHttpTransport()
            throws GeneralSecurityException, IOException
    {
        return GoogleNetHttpTransport.newTrustedTransport();
    }
    public static Credential getCredentials()
            throws GeneralSecurityException, IOException {
        URL path = GoogleSpreadSheetHelper.class.getResource(P12FILE);
        File p12 = new File(path.getFile());
        System.out.println(p12.getAbsoluteFile());
        List<String> SCOPES_ARRAY =SCOPES;
        Credential credential = new GoogleCredential.Builder()
                .setTransport(getHttpTransport())
                .setJsonFactory(getJsonFactory())
                .setServiceAccountId(SERVICE_ACCOUNT_ID)
                .setServiceAccountScopes(SCOPES_ARRAY)
                .setServiceAccountPrivateKeyFromP12File(p12)
                .build();

        return credential;
    }

    public static Sheets getSheetsService() throws IOException, GeneralSecurityException, URISyntaxException {
        Credential credential = getCredentials();
        return  new Sheets.Builder(getHttpTransport(),
                getJsonFactory(),
                credential)
                .setApplicationName(APPLICATION_NAME)
                .build();
    }

    public List<List<Object>> getSpreadSheetRecords(String spreadsheetId, String range) throws IOException, GeneralSecurityException, URISyntaxException {
        Sheets service = getSheetsService();
        ValueRange response = service.spreadsheets().values()
                .get(spreadsheetId, range)
                .execute();
        List<List<Object>> values = response.getValues();
        if (values != null && values.size() != 0) {
            return values;
        } else {
            System.out.println("No data found.");
            return null;
        }
    }


}