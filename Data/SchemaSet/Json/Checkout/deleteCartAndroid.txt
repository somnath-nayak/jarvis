{
  "definitions": {},
  "$schema": "http://json-schema.org/draft-07/schema#",
  "$id": "http://example.com/root.json",
  "type": "object",
  "title": "The Root Schema",
  "required": [
    "statusCode",
    "statusMessage",
    "data",
    "tid",
    "sid",
    "deviceId",
    "successful"
  ],
  "properties": {
    "statusCode": {
      "$id": "#/properties/statusCode",
      "type": "integer",
      "title": "The Statuscode Schema",
      "default": 0,
      "examples": [
        0
      ]
    },
    "statusMessage": {
      "$id": "#/properties/statusMessage",
      "type": "string",
      "title": "The Statusmessage Schema",
      "default": "",
      "examples": [
        "CART_DELETED_SUCCESSFULLY"
      ],
      "pattern": "^(.*)$"
    },
    "data": {
      "$id": "#/properties/data",
      "type": "object",
      "title": "The Data Schema",
      "required": [
        "result",
        "emailId",
        "phoneNo"
      ],
      "properties": {
        "result": {
          "$id": "#/properties/data/properties/result",
          "type": "string",
          "title": "The Result Schema",
          "default": "",
          "examples": [
            "success"
          ],
          "pattern": "^(.*)$"
        },
        "emailId": {
          "$id": "#/properties/data/properties/emailId",
          "type": "string",
          "title": "The Emailid Schema",
          "default": "",
          "examples": [
            "sowmyamoray18@gmail.com"
          ],
          "pattern": "^(.*)$"
        },
        "phoneNo": {
          "$id": "#/properties/data/properties/phoneNo",
          "type": "string",
          "title": "The Phoneno Schema",
          "default": "",
          "examples": [
            "8073825187"
          ],
          "pattern": "^(.*)$"
        }
      }
    },
    "tid": {
      "$id": "#/properties/tid",
      "type": "string",
      "title": "The Tid Schema",
      "default": "",
      "examples": [
        "639c2d46-a2ff-4823-b8c7-df12f916832c"
      ],
      "pattern": "^(.*)$"
    },
    "sid": {
      "$id": "#/properties/sid",
      "type": "string",
      "title": "The Sid Schema",
      "default": "",
      "examples": [
        "bbxa8d0d-8f30-405f-a08f-80ad8256336b"
      ],
      "pattern": "^(.*)$"
    },
    "deviceId": {
      "$id": "#/properties/deviceId",
      "type": "string",
      "title": "The Deviceid Schema",
      "default": "",
      "examples": [
        "507003c7-99e5-41a6-a18e-9f6405fc55eb"
      ],
      "pattern": "^(.*)$"
    },
    "successful": {
      "$id": "#/properties/successful",
      "type": "boolean",
      "title": "The Successful Schema",
      "default": false,
      "examples": [
        true
      ]
    }
  }
}