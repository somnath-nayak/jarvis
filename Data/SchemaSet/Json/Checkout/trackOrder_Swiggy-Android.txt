{
  "definitions": {},
  "$schema": "http://json-schema.org/draft-06/schema#",
  "$id": "http://example.com/root.json",
  "type": "object",
  "title": "The Root Schema",
  "required": [
    "statusCode",
    "statusMessage",
    "data",
    "tid",
    "sid",
    "deviceId",
    "successful"
  ],
  "properties": {
    "statusCode": {
      "$id": "#/properties/statusCode",
      "type": "number",
      "title": "The Statuscode Schema",
      "default": 0,
      "examples": [
        0
      ]
    },
    "statusMessage": {
      "$id": "#/properties/statusMessage",
      "type": "string",
      "title": "The Statusmessage Schema",
      "default": "",
      "examples": [
        "done successfully"
      ],
      "pattern": "^(.*)$"
    },
    "data": {
      "$id": "#/properties/data",
      "type": "object",
      "title": "The Data Schema",
      "required": [
        "order_id",
        "delivery_status",
        "restaurant",
        "billing_address",
        "delivery_boy",
        "batched_order_message",
        "message",
        "message_position_text",
        "message_position_number",
        "flag_for_expand",
        "show_eta",
        "expected_time",
        "orderState",
        "swiggy_assured",
        "order_total",
        "order_items_count",
        "configurations",
        "batched_destinations",
        "annotation",
        "annotation_image_url",
        "order_new_state_sequence",
        "order_state_sequence",
        "abCase",
        "on_time"
      ],
      "properties": {
        "order_id": {
          "$id": "#/properties/data/properties/order_id",
          "type": "string",
          "title": "The Order_id Schema",
          "default": "",
          "examples": [
            "18982564335"
          ],
          "pattern": "^(.*)$"
        },
        "delivery_status": {
          "$id": "#/properties/data/properties/delivery_status",
          "type": "string",
          "title": "The Delivery_status Schema",
          "default": "",
          "examples": [
            "unassigned"
          ],
          "pattern": "^(.*)$"
        },
        "restaurant": {
          "$id": "#/properties/data/properties/restaurant",
          "type": "object",
          "title": "The Restaurant Schema",
          "required": [
            "lat",
            "lng"
          ],
          "properties": {
            "lat": {
              "$id": "#/properties/data/properties/restaurant/properties/lat",
              "type": "string",
              "title": "The Lat Schema",
              "default": "",
              "examples": [
                "27.8374445"
              ],
              "pattern": "^(.*)$"
            },
            "lng": {
              "$id": "#/properties/data/properties/restaurant/properties/lng",
              "type": "string",
              "title": "The Lng Schema",
              "default": "",
              "examples": [
                "76.17257740000002"
              ],
              "pattern": "^(.*)$"
            }
          }
        },
        "billing_address": {
          "$id": "#/properties/data/properties/billing_address",
          "type": "object",
          "title": "The Billing_address Schema",
          "required": [
            "lat",
            "lng"
          ],
          "properties": {
            "lat": {
              "$id": "#/properties/data/properties/billing_address/properties/lat",
              "type": "string",
              "title": "The Lat Schema",
              "default": "",
              "examples": [
                "27.835629"
              ],
              "pattern": "^(.*)$"
            },
            "lng": {
              "$id": "#/properties/data/properties/billing_address/properties/lng",
              "type": "string",
              "title": "The Lng Schema",
              "default": "",
              "examples": [
                "76.173089"
              ],
              "pattern": "^(.*)$"
            }
          }
        },
        "delivery_boy": {
          "$id": "#/properties/data/properties/delivery_boy",
          "type": "object",
          "title": "The Delivery_boy Schema",
          "required": [
            "lat",
            "lng"
          ],
          "properties": {
            "lat": {
              "$id": "#/properties/data/properties/delivery_boy/properties/lat",
              "type": "string",
              "title": "The Lat Schema",
              "default": "",
              "examples": [
                ""
              ],
              "pattern": "^(.*)$"
            },
            "lng": {
              "$id": "#/properties/data/properties/delivery_boy/properties/lng",
              "type": "string",
              "title": "The Lng Schema",
              "default": "",
              "examples": [
                ""
              ],
              "pattern": "^(.*)$"
            }
          }
        },
        "batched_order_message": {
          "$id": "#/properties/data/properties/batched_order_message",
          "type": "string",
          "title": "The Batched_order_message Schema",
          "default": "",
          "examples": [
            ""
          ],
          "pattern": "^(.*)$"
        },
        "message": {
          "$id": "#/properties/data/properties/message",
          "type": "string",
          "title": "The Message Schema",
          "default": "",
          "examples": [
            "Awaiting restaurant confirmation. Delivery Executive will be assigned shortly"
          ],
          "pattern": "^(.*)$"
        },
        "message_position_text": {
          "$id": "#/properties/data/properties/message_position_text",
          "type": "string",
          "title": "The Message_position_text Schema",
          "default": "",
          "examples": [
            "Order Received"
          ],
          "pattern": "^(.*)$"
        },
        "message_position_number": {
          "$id": "#/properties/data/properties/message_position_number",
          "type": "string",
          "title": "The Message_position_number Schema",
          "default": "",
          "examples": [
            "1"
          ],
          "pattern": "^(.*)$"
        },
        "flag_for_expand": {
          "$id": "#/properties/data/properties/flag_for_expand",
          "type": "string",
          "title": "The Flag_for_expand Schema",
          "default": "",
          "examples": [
            "0"
          ],
          "pattern": "^(.*)$"
        },
        "show_eta": {
          "$id": "#/properties/data/properties/show_eta",
          "type": "boolean",
          "title": "The Show_eta Schema",
          "default": false,
          "examples": [
            false
          ]
        },
        "expected_time": {
          "$id": "#/properties/data/properties/expected_time",
          "type": "number",
          "title": "The Expected_time Schema",
          "default": 0,
          "examples": [
            0
          ]
        },
        "orderState": {
          "$id": "#/properties/data/properties/orderState",
          "type": "number",
          "title": "The Orderstate Schema",
          "default": 0,
          "examples": [
            1
          ]
        },
        "swiggy_assured": {
          "$id": "#/properties/data/properties/swiggy_assured",
          "type": "string",
          "title": "The Swiggy_assured Schema",
          "default": "",
          "examples": [
            "0"
          ],
          "pattern": "^(.*)$"
        },
        "order_total": {
          "$id": "#/properties/data/properties/order_total",
          "type": "number",
          "title": "The Order_total Schema",
          "default": 0,
          "examples": [
            1
          ]
        },
        "order_items_count": {
          "$id": "#/properties/data/properties/order_items_count",
          "type": "number",
          "title": "The Order_items_count Schema",
          "default": 0,
          "examples": [
            1
          ]
        },
        "configurations": {
          "$id": "#/properties/data/properties/configurations",
          "type": "object",
          "title": "The Configurations Schema"
        },
        "batched_destinations": {
          "$id": "#/properties/data/properties/batched_destinations",
          "type": "array",
          "title": "The Batched_destinations Schema"
        },
        "annotation": {
          "$id": "#/properties/data/properties/annotation",
          "type": "string",
          "title": "The Annotation Schema",
          "default": "",
          "examples": [
            "Testing"
          ],
          "pattern": "^(.*)$"
        },
        "annotation_image_url": {
          "$id": "#/properties/data/properties/annotation_image_url",
          "type": "string",
          "title": "The Annotation_image_url Schema",
          "default": "",
          "examples": [
            "set/others/annotation/icon/url/from/cms"
          ],
          "pattern": "^(.*)$"
        },
        "order_new_state_sequence": {
          "$id": "#/properties/data/properties/order_new_state_sequence",
          "type": "object",
          "title": "The Order_new_state_sequence Schema"
        },
        "order_state_sequence": {
          "$id": "#/properties/data/properties/order_state_sequence",
          "type": "object",
          "title": "The Order_state_sequence Schema",
          "required": [
            "1",
            "2",
            "3",
            "4"
          ],
          "properties": {
            "1": {
              "$id": "#/properties/data/properties/order_state_sequence/properties/1",
              "type": "object",
              "title": "The 1 Schema",
              "required": [
                "name",
                "icon"
              ],
              "properties": {
                "name": {
                  "$id": "#/properties/data/properties/order_state_sequence/properties/1/properties/name",
                  "type": "string",
                  "title": "The Name Schema",
                  "default": "",
                  "examples": [
                    "Order Received"
                  ],
                  "pattern": "^(.*)$"
                },
                "icon": {
                  "$id": "#/properties/data/properties/order_state_sequence/properties/1/properties/icon",
                  "type": "string",
                  "title": "The Icon Schema",
                  "default": "",
                  "examples": [
                    "image_id"
                  ],
                  "pattern": "^(.*)$"
                }
              }
            },
            "2": {
              "$id": "#/properties/data/properties/order_state_sequence/properties/2",
              "type": "object",
              "title": "The 2 Schema",
              "required": [
                "name",
                "icon"
              ],
              "properties": {
                "name": {
                  "$id": "#/properties/data/properties/order_state_sequence/properties/2/properties/name",
                  "type": "string",
                  "title": "The Name Schema",
                  "default": "",
                  "examples": [
                    "Food Is Being Prepared"
                  ],
                  "pattern": "^(.*)$"
                },
                "icon": {
                  "$id": "#/properties/data/properties/order_state_sequence/properties/2/properties/icon",
                  "type": "string",
                  "title": "The Icon Schema",
                  "default": "",
                  "examples": [
                    "image_id"
                  ],
                  "pattern": "^(.*)$"
                }
              }
            },
            "3": {
              "$id": "#/properties/data/properties/order_state_sequence/properties/3",
              "type": "object",
              "title": "The 3 Schema",
              "required": [
                "name",
                "icon"
              ],
              "properties": {
                "name": {
                  "$id": "#/properties/data/properties/order_state_sequence/properties/3/properties/name",
                  "type": "string",
                  "title": "The Name Schema",
                  "default": "",
                  "examples": [
                    "Order Picked Up"
                  ],
                  "pattern": "^(.*)$"
                },
                "icon": {
                  "$id": "#/properties/data/properties/order_state_sequence/properties/3/properties/icon",
                  "type": "string",
                  "title": "The Icon Schema",
                  "default": "",
                  "examples": [
                    "image_id"
                  ],
                  "pattern": "^(.*)$"
                }
              }
            },
            "4": {
              "$id": "#/properties/data/properties/order_state_sequence/properties/4",
              "type": "object",
              "title": "The 4 Schema",
              "required": [
                "name",
                "icon"
              ],
              "properties": {
                "name": {
                  "$id": "#/properties/data/properties/order_state_sequence/properties/4/properties/name",
                  "type": "string",
                  "title": "The Name Schema",
                  "default": "",
                  "examples": [
                    "Order Delivered"
                  ],
                  "pattern": "^(.*)$"
                },
                "icon": {
                  "$id": "#/properties/data/properties/order_state_sequence/properties/4/properties/icon",
                  "type": "string",
                  "title": "The Icon Schema",
                  "default": "",
                  "examples": [
                    "image_id"
                  ],
                  "pattern": "^(.*)$"
                }
              }
            }
          }
        },
        "abCase": {
          "$id": "#/properties/data/properties/abCase",
          "type": "string",
          "title": "The Abcase Schema",
          "default": "",
          "examples": [
            "B"
          ],
          "pattern": "^(.*)$"
        },
        "on_time": {
          "$id": "#/properties/data/properties/on_time",
          "type": "boolean",
          "title": "The On_time Schema",
          "default": false,
          "examples": [
            false
          ]
        }
      }
    },
    "tid": {
      "$id": "#/properties/tid",
      "type": "string",
      "title": "The Tid Schema",
      "default": "",
      "examples": [
        "04501d53-3dd5-40fd-9016-9b7dbd2d01f0"
      ],
      "pattern": "^(.*)$"
    },
    "sid": {
      "$id": "#/properties/sid",
      "type": "string",
      "title": "The Sid Schema",
      "default": "",
      "examples": [
        "bam1e266-f3dc-4525-9c46-b31e3af3d24b"
      ],
      "pattern": "^(.*)$"
    },
    "deviceId": {
      "$id": "#/properties/deviceId",
      "type": "string",
      "title": "The Deviceid Schema",
      "default": "",
      "examples": [
        "bd374c1d-2dc3-4e52-b8d6-baf1b9c558dd"
      ],
      "pattern": "^(.*)$"
    },
    "successful": {
      "$id": "#/properties/successful",
      "type": "boolean",
      "title": "The Successful Schema",
      "default": false,
      "examples": [
        true
      ]
    }
  }
}