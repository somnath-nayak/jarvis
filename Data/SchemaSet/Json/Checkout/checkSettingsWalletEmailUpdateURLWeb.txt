{
  "definitions": {},
  "$schema": "http://json-schema.org/draft-07/schema#",
  "$id": "http://example.com/root.json",
  "type": "object",
  "title": "The Root Schema",
  "required": [
    "statusCode",
    "statusMessage",
    "data",
    "tid",
    "sid",
    "deviceId"
  ],
  "properties": {
    "statusCode": {
      "$id": "#/properties/statusCode",
      "type": "integer",
      "title": "The Statuscode Schema",
      "default": 0,
      "examples": [
        0
      ]
    },
    "statusMessage": {
      "$id": "#/properties/statusMessage",
      "type": "string",
      "title": "The Statusmessage Schema",
      "default": "",
      "examples": [
        "done successfully"
      ],
      "pattern": "^(.*)$"
    },
    "data": {
      "$id": "#/properties/data",
      "type": "object",
      "title": "The Data Schema",
      "required": [
        "option"
      ],
      "properties": {
        "option": {
          "$id": "#/properties/data/properties/option",
          "type": "string",
          "title": "The Option Schema",
          "default": "",
          "examples": [
            "wallet_email_update_url"
          ],
          "pattern": "^(.*)$"
        }
      }
    },
    "tid": {
      "$id": "#/properties/tid",
      "type": "string",
      "title": "The Tid Schema",
      "default": "",
      "examples": [
        "8909f055-c6fa-4843-9468-2e2fb6b59505"
      ],
      "pattern": "^(.*)$"
    },
    "sid": {
      "$id": "#/properties/sid",
      "type": "string",
      "title": "The Sid Schema",
      "default": "",
      "examples": [
        "bgp1686d-2c7c-48d2-9edb-790944f726e5"
      ],
      "pattern": "^(.*)$"
    },
    "deviceId": {
      "$id": "#/properties/deviceId",
      "type": "string",
      "title": "The Deviceid Schema",
      "default": "",
      "examples": [
        "165292ea-c7c9-49d2-82d9-89821c8014d7"
      ],
      "pattern": "^(.*)$"
    }
  }
}