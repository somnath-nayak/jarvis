{
  "definitions": {},
  "$schema": "http://json-schema.org/draft-06/schema#",
  "$id": "http://example.com/root.json",
  "type": "object",
  "required": [
    "statusCode",
    "statusMessage",
    "data",
    "tid",
    "sid",
    "deviceId",
    "successful"
  ],
  "properties": {
    "statusCode": {
      "$id": "#/properties/statusCode",
      "type": "number"
    },
    "statusMessage": {
      "$id": "#/properties/statusMessage",
      "type": "string",
      "pattern": "^(.*)$"
    },
    "data": {
      "$id": "#/properties/data",
      "type": "object",
      "required": [
        "appliedTradeCampaignHeaders",
        "rainMode",
        "flatFeeApplied",
        "select",
        "superTradeDiscount",
        "cartId",
        "result",
        "user_swiggy_money",
        "cart_menu_items",
        "mealItems",
        "cart_subtotal_without_packing",
        "cart_subtotal",
        "restaurant_packing_charges",
        "cart_total",
        "order_total",
        "total_without_coupon_discount",
        "total_packing_charges",
        "discount_total",
        "coupon_discount_total",
        "trade_discount_without_freebie",
        "trade_discount_total",
        "discount_total_without_freebie",
        "trade_discount_reward_list",
        "swiggy_money",
        "cart_total_count",
        "restaurant_details",
        "is_coupon_valid",
        "is_group_parent",
        "free_gifts",
        "free_shipping",
        "coupon_error_message",
        "is_sla_honored",
        "sla_time",
        "sla_range_min",
        "sla_range_max",
        "payment_banner_message",
        "cart_banner_message",
        "cod_enabled",
        "cod_disabled_message",
        "cart_popup_title",
        "cart_popup_msg",
        "delivery_fee_type",
        "cart_popup_version",
        "delivery_fee_info",
        "offers",
        "is_assured",
        "order_incoming",
        "waive_off_amount",
        "order_spending",
        "new_rendering_model",
        "rendering_model",
        "rendering_details",
        "display_sequence",
        "cancellation_fee",
        "total_tax",
        "cart_type",
        "configurations",
        "messages",
        "GST_details",
        "threshold_fee",
        "distance_fee",
        "time_fee",
        "special_fee",
        "convenience_fee",
        "delivery_charges",
        "threshold_fee_message",
        "distance_fee_message",
        "time_fee_message",
        "special_fee_message",
        "bill_total",
        "item_total",
        "subscription_total",
        "subscription_items",
        "show_super_nudge",
        "is_super_trade_discount",
        "free_delivery_discount",
        "cart_token"
      ],
      "properties": {
        "appliedTradeCampaignHeaders": {
          "$id": "#/properties/data/properties/appliedTradeCampaignHeaders",
          "type": "array"
        },
        "rainMode": {
          "$id": "#/properties/data/properties/rainMode",
          "type": "boolean"
        },
        "flatFeeApplied": {
          "$id": "#/properties/data/properties/flatFeeApplied",
          "type": "boolean"
        },
        "select": {
          "$id": "#/properties/data/properties/select",
          "type": "boolean"
        },
        "superTradeDiscount": {
          "$id": "#/properties/data/properties/superTradeDiscount",
          "type": "boolean"
        },
        "cartId": {
          "$id": "#/properties/data/properties/cartId",
          "type": "number"
        },
        "result": {
          "$id": "#/properties/data/properties/result",
          "type": "string",
          "pattern": "^(.*)$"
        },
        "user_swiggy_money": {
          "$id": "#/properties/data/properties/user_swiggy_money",
          "type": "number"
        },
        "cart_menu_items": {
          "$id": "#/properties/data/properties/cart_menu_items",
          "type": "array",
          "items": {
            "$id": "#/properties/data/properties/cart_menu_items/items",
            "type": "object",
            "required": [
              "rewardType",
              "mealQuantity",
              "menu_item_id",
              "name",
              "base_price",
              "variants_price",
              "addons_price",
              "quantity",
              "subtotal",
              "subtotal_trade_discount",
              "packing_charge",
              "total",
              "final_price",
              "is_customized",
              "id_hash",
              "valid_addons",
              "valid_variants",
              "valid_variants_v2",
              "is_veg",
              "isVeg",
              "addons",
              "variants",
              "item_taxes",
              "GST_details",
              "cloudinaryImageId",
              "in_stock",
              "inventory",
              "inventory_message",
              "inventory_insufficient_message",
              "added_by_user_id",
              "added_by_user_name",
              "group_user_item_map",
              "is_gstInclusive",
              "attributes"
            ],
            "properties": {
              "rewardType": {
                "$id": "#/properties/data/properties/cart_menu_items/items/properties/rewardType",
                "type": "null"
              },
              "mealQuantity": {
                "$id": "#/properties/data/properties/cart_menu_items/items/properties/mealQuantity",
                "type": "number"
              },
              "menu_item_id": {
                "$id": "#/properties/data/properties/cart_menu_items/items/properties/menu_item_id",
                "type": "number"
              },
              "name": {
                "$id": "#/properties/data/properties/cart_menu_items/items/properties/name",
                "type": "string",
                "pattern": "^(.*)$"
              },
              "base_price": {
                "$id": "#/properties/data/properties/cart_menu_items/items/properties/base_price",
                "type": "number"
              },
              "variants_price": {
                "$id": "#/properties/data/properties/cart_menu_items/items/properties/variants_price",
                "type": "null"
              },
              "addons_price": {
                "$id": "#/properties/data/properties/cart_menu_items/items/properties/addons_price",
                "type": "null"
              },
              "quantity": {
                "$id": "#/properties/data/properties/cart_menu_items/items/properties/quantity",
                "type": "number"
              },
              "subtotal": {
                "$id": "#/properties/data/properties/cart_menu_items/items/properties/subtotal",
                "type": "number"
              },
              "subtotal_trade_discount": {
                "$id": "#/properties/data/properties/cart_menu_items/items/properties/subtotal_trade_discount",
                "type": "number"
              },
              "packing_charge": {
                "$id": "#/properties/data/properties/cart_menu_items/items/properties/packing_charge",
                "type": "number"
              },
              "total": {
                "$id": "#/properties/data/properties/cart_menu_items/items/properties/total",
                "type": "number"
              },
              "final_price": {
                "$id": "#/properties/data/properties/cart_menu_items/items/properties/final_price",
                "type": "number"
              },
              "is_customized": {
                "$id": "#/properties/data/properties/cart_menu_items/items/properties/is_customized",
                "type": "null"
              },
              "id_hash": {
                "$id": "#/properties/data/properties/cart_menu_items/items/properties/id_hash",
                "type": "string",
                "pattern": "^(.*)$"
              },
              "valid_addons": {
                "$id": "#/properties/data/properties/cart_menu_items/items/properties/valid_addons",
                "type": "array"
              },
              "valid_variants": {
                "$id": "#/properties/data/properties/cart_menu_items/items/properties/valid_variants",
                "type": "null"
              },
              "valid_variants_v2": {
                "$id": "#/properties/data/properties/cart_menu_items/items/properties/valid_variants_v2",
                "type": "object",
                "required": [
                  "variant_groups",
                  "pricing_models"
                ],
                "properties": {
                  "variant_groups": {
                    "$id": "#/properties/data/properties/cart_menu_items/items/properties/valid_variants_v2/properties/variant_groups",
                    "type": "array"
                  },
                  "pricing_models": {
                    "$id": "#/properties/data/properties/cart_menu_items/items/properties/valid_variants_v2/properties/pricing_models",
                    "type": "array"
                  }
                }
              },
              "is_veg": {
                "$id": "#/properties/data/properties/cart_menu_items/items/properties/is_veg",
                "type": "string",
                "pattern": "^(.*)$"
              },
              "isVeg": {
                "$id": "#/properties/data/properties/cart_menu_items/items/properties/isVeg",
                "type": "number"
              },
              "addons": {
                "$id": "#/properties/data/properties/cart_menu_items/items/properties/addons",
                "type": "array"
              },
              "variants": {
                "$id": "#/properties/data/properties/cart_menu_items/items/properties/variants",
                "type": "array"
              },
              "item_taxes": {
                "$id": "#/properties/data/properties/cart_menu_items/items/properties/item_taxes",
                "type": "object",
                "required": [
                  "service_tax",
                  "vat",
                  "service_charges",
                  "GST"
                ],
                "properties": {
                  "service_tax": {
                    "$id": "#/properties/data/properties/cart_menu_items/items/properties/item_taxes/properties/service_tax",
                    "type": "number"
                  },
                  "vat": {
                    "$id": "#/properties/data/properties/cart_menu_items/items/properties/item_taxes/properties/vat",
                    "type": "number"
                  },
                  "service_charges": {
                    "$id": "#/properties/data/properties/cart_menu_items/items/properties/item_taxes/properties/service_charges",
                    "type": "number"
                  },
                  "GST": {
                    "$id": "#/properties/data/properties/cart_menu_items/items/properties/item_taxes/properties/GST",
                    "type": "number"
                  }
                }
              },
              "GST_details": {
                "$id": "#/properties/data/properties/cart_menu_items/items/properties/GST_details",
                "type": "object",
                "required": [
                  "SGST",
                  "CGST",
                  "IGST"
                ],
                "properties": {
                  "SGST": {
                    "$id": "#/properties/data/properties/cart_menu_items/items/properties/GST_details/properties/SGST",
                    "type": "number"
                  },
                  "CGST": {
                    "$id": "#/properties/data/properties/cart_menu_items/items/properties/GST_details/properties/CGST",
                    "type": "number"
                  },
                  "IGST": {
                    "$id": "#/properties/data/properties/cart_menu_items/items/properties/GST_details/properties/IGST",
                    "type": "number"
                  }
                }
              },
              "cloudinaryImageId": {
                "$id": "#/properties/data/properties/cart_menu_items/items/properties/cloudinaryImageId",
                "type": "string",
                "pattern": "^(.*)$"
              },
              "in_stock": {
                "$id": "#/properties/data/properties/cart_menu_items/items/properties/in_stock",
                "type": "number"
              },
              "inventory": {
                "$id": "#/properties/data/properties/cart_menu_items/items/properties/inventory",
                "type": "number"
              },
              "inventory_message": {
                "$id": "#/properties/data/properties/cart_menu_items/items/properties/inventory_message",
                "type": "null"
              },
              "inventory_insufficient_message": {
                "$id": "#/properties/data/properties/cart_menu_items/items/properties/inventory_insufficient_message",
                "type": "null"
              },
              "added_by_user_id": {
                "$id": "#/properties/data/properties/cart_menu_items/items/properties/added_by_user_id",
                "type": "number"
              },
              "added_by_user_name": {
                "$id": "#/properties/data/properties/cart_menu_items/items/properties/added_by_user_name",
                "type": "string",
                "pattern": "^(.*)$"
              },
              "group_user_item_map": {
                "$id": "#/properties/data/properties/cart_menu_items/items/properties/group_user_item_map",
                "type": "object"
              },
              "is_gstInclusive": {
                "$id": "#/properties/data/properties/cart_menu_items/items/properties/is_gstInclusive",
                "type": "boolean"
              },
              "attributes": {
                "$id": "#/properties/data/properties/cart_menu_items/items/properties/attributes",
                "type": "null"
              }
            }
          }
        },
        "mealItems": {
          "$id": "#/properties/data/properties/mealItems",
          "type": "array"
        },
        "cart_subtotal_without_packing": {
          "$id": "#/properties/data/properties/cart_subtotal_without_packing",
          "type": "number"
        },
        "cart_subtotal": {
          "$id": "#/properties/data/properties/cart_subtotal",
          "type": "number"
        },
        "restaurant_packing_charges": {
          "$id": "#/properties/data/properties/restaurant_packing_charges",
          "type": "number"
        },
        "cart_total": {
          "$id": "#/properties/data/properties/cart_total",
          "type": "number"
        },
        "order_total": {
          "$id": "#/properties/data/properties/order_total",
          "type": "number"
        },
        "total_without_coupon_discount": {
          "$id": "#/properties/data/properties/total_without_coupon_discount",
          "type": "number"
        },
        "total_packing_charges": {
          "$id": "#/properties/data/properties/total_packing_charges",
          "type": "number"
        },
        "discount_total": {
          "$id": "#/properties/data/properties/discount_total",
          "type": "number"
        },
        "coupon_discount_total": {
          "$id": "#/properties/data/properties/coupon_discount_total",
          "type": "number"
        },
        "trade_discount_without_freebie": {
          "$id": "#/properties/data/properties/trade_discount_without_freebie",
          "type": "number"
        },
        "trade_discount_total": {
          "$id": "#/properties/data/properties/trade_discount_total",
          "type": "number"
        },
        "discount_total_without_freebie": {
          "$id": "#/properties/data/properties/discount_total_without_freebie",
          "type": "number"
        },
        "trade_discount_reward_list": {
          "$id": "#/properties/data/properties/trade_discount_reward_list",
          "type": "array"
        },
        "swiggy_money": {
          "$id": "#/properties/data/properties/swiggy_money",
          "type": "number"
        },
        "cart_total_count": {
          "$id": "#/properties/data/properties/cart_total_count",
          "type": "number"
        },
        "restaurant_details": {
          "$id": "#/properties/data/properties/restaurant_details",
          "type": "object",
          "required": [
            "bgColor",
            "textColor",
            "id",
            "lat",
            "lng",
            "phone_no",
            "address",
            "name",
            "third_party_vendor_type",
            "cloudinary_image_id",
            "slug",
            "city",
            "area_name"
          ],
          "properties": {
            "bgColor": {
              "$id": "#/properties/data/properties/restaurant_details/properties/bgColor",
              "type": "null"
            },
            "textColor": {
              "$id": "#/properties/data/properties/restaurant_details/properties/textColor",
              "type": "null"
            },
            "id": {
              "$id": "#/properties/data/properties/restaurant_details/properties/id",
              "type": "string",
              "pattern": "^(.*)$"
            },
            "lat": {
              "$id": "#/properties/data/properties/restaurant_details/properties/lat",
              "type": "string",
              "pattern": "^(.*)$"
            },
            "lng": {
              "$id": "#/properties/data/properties/restaurant_details/properties/lng",
              "type": "string",
              "pattern": "^(.*)$"
            },
            "phone_no": {
              "$id": "#/properties/data/properties/restaurant_details/properties/phone_no",
              "type": "string",
              "pattern": "^(.*)$"
            },
            "address": {
              "$id": "#/properties/data/properties/restaurant_details/properties/address",
              "type": "string",
              "pattern": "^(.*)$"
            },
            "name": {
              "$id": "#/properties/data/properties/restaurant_details/properties/name",
              "type": "string",
              "pattern": "^(.*)$"
            },
            "third_party_vendor_type": {
              "$id": "#/properties/data/properties/restaurant_details/properties/third_party_vendor_type",
              "type": "null"
            },
            "cloudinary_image_id": {
              "$id": "#/properties/data/properties/restaurant_details/properties/cloudinary_image_id",
              "type": "string",
              "pattern": "^(.*)$"
            },
            "slug": {
              "$id": "#/properties/data/properties/restaurant_details/properties/slug",
              "type": "string",
              "pattern": "^(.*)$"
            },
            "city": {
              "$id": "#/properties/data/properties/restaurant_details/properties/city",
              "type": "string",
              "pattern": "^(.*)$"
            },
            "area_name": {
              "$id": "#/properties/data/properties/restaurant_details/properties/area_name",
              "type": "string",
              "pattern": "^(.*)$"
            }
          }
        },
        "is_coupon_valid": {
          "$id": "#/properties/data/properties/is_coupon_valid",
          "type": "number"
        },
        "is_group_parent": {
          "$id": "#/properties/data/properties/is_group_parent",
          "type": "boolean"
        },
        "free_gifts": {
          "$id": "#/properties/data/properties/free_gifts",
          "type": "array"
        },
        "free_shipping": {
          "$id": "#/properties/data/properties/free_shipping",
          "type": "number"
        },
        "coupon_error_message": {
          "$id": "#/properties/data/properties/coupon_error_message",
          "type": "string",
          "pattern": "^(.*)$"
        },
        "is_sla_honored": {
          "$id": "#/properties/data/properties/is_sla_honored",
          "type": "boolean"
        },
        "sla_time": {
          "$id": "#/properties/data/properties/sla_time",
          "type": "number"
        },
        "sla_range_min": {
          "$id": "#/properties/data/properties/sla_range_min",
          "type": "number"
        },
        "sla_range_max": {
          "$id": "#/properties/data/properties/sla_range_max",
          "type": "number"
        },
        "payment_banner_message": {
          "$id": "#/properties/data/properties/payment_banner_message",
          "type": "string",
          "pattern": "^(.*)$"
        },
        "cart_banner_message": {
          "$id": "#/properties/data/properties/cart_banner_message",
          "type": "string",
          "pattern": "^(.*)$"
        },
        "cod_enabled": {
          "$id": "#/properties/data/properties/cod_enabled",
          "type": "boolean"
        },
        "cod_disabled_message": {
          "$id": "#/properties/data/properties/cod_disabled_message",
          "type": "string",
          "pattern": "^(.*)$"
        },
        "cart_popup_title": {
          "$id": "#/properties/data/properties/cart_popup_title",
          "type": "string",
          "pattern": "^(.*)$"
        },
        "cart_popup_msg": {
          "$id": "#/properties/data/properties/cart_popup_msg",
          "type": "string",
          "pattern": "^(.*)$"
        },
        "delivery_fee_type": {
          "$id": "#/properties/data/properties/delivery_fee_type",
          "type": "number"
        },
        "cart_popup_version": {
          "$id": "#/properties/data/properties/cart_popup_version",
          "type": "string",
          "pattern": "^(.*)$"
        },
        "delivery_fee_info": {
          "$id": "#/properties/data/properties/delivery_fee_info",
          "type": "string",
          "pattern": "^(.*)$"
        },
        "offers": {
          "$id": "#/properties/data/properties/offers",
          "type": "array",
          "items": {
            "$id": "#/properties/data/properties/offers/items",
            "type": "object",
            "required": [
              "payment_mode",
              "promotion_text"
            ],
            "properties": {
              "payment_mode": {
                "$id": "#/properties/data/properties/offers/items/properties/payment_mode",
                "type": "string",
                "pattern": "^(.*)$"
              },
              "promotion_text": {
                "$id": "#/properties/data/properties/offers/items/properties/promotion_text",
                "type": "string",
                "pattern": "^(.*)$"
              }
            }
          }
        },
        "is_assured": {
          "$id": "#/properties/data/properties/is_assured",
          "type": "number"
        },
        "order_incoming": {
          "$id": "#/properties/data/properties/order_incoming",
          "type": "number"
        },
        "waive_off_amount": {
          "$id": "#/properties/data/properties/waive_off_amount",
          "type": "number"
        },
        "order_spending": {
          "$id": "#/properties/data/properties/order_spending",
          "type": "number"
        },
        "new_rendering_model": {
          "$id": "#/properties/data/properties/new_rendering_model",
          "type": "boolean"
        },
        "rendering_model": {
          "$id": "#/properties/data/properties/rendering_model",
          "type": "string",
          "pattern": "^(.*)$"
        },
        "rendering_details": {
          "$id": "#/properties/data/properties/rendering_details",
          "type": "array",
          "items": {
            "$id": "#/properties/data/properties/rendering_details/items",
            "type": "object",
            "required": [
              "type",
              "key",
              "intermediateText",
              "value",
              "hierarchy",
              "currency",
              "display_text",
              "info_text",
              "icon"
            ],
            "properties": {
              "type": {
                "$id": "#/properties/data/properties/rendering_details/items/properties/type",
                "type": "string",
                "pattern": "^(.*)$"
              },
              "key": {
                "$id": "#/properties/data/properties/rendering_details/items/properties/key",
                "type": "string",
                "pattern": "^(.*)$"
              },
              "intermediateText": {
                "$id": "#/properties/data/properties/rendering_details/items/properties/intermediateText",
                "type": "string",
                "pattern": "^(.*)$"
              },
              "value": {
                "$id": "#/properties/data/properties/rendering_details/items/properties/value",
                "type": "string",
                "pattern": "^(.*)$"
              },
              "hierarchy": {
                "$id": "#/properties/data/properties/rendering_details/items/properties/hierarchy",
                "type": "number"
              },
              "currency": {
                "$id": "#/properties/data/properties/rendering_details/items/properties/currency",
                "type": "string",
                "pattern": "^(.*)$"
              },
              "display_text": {
                "$id": "#/properties/data/properties/rendering_details/items/properties/display_text",
                "type": "string",
                "pattern": "^(.*)$"
              },
              "info_text": {
                "$id": "#/properties/data/properties/rendering_details/items/properties/info_text",
                "type": "string",
                "pattern": "^(.*)$"
              },
              "icon": {
                "$id": "#/properties/data/properties/rendering_details/items/properties/icon",
                "type": "string",
                "pattern": "^(.*)$"
              }
            }
          }
        },
        "display_sequence": {
          "$id": "#/properties/data/properties/display_sequence",
          "type": "array",
          "items": {
            "$id": "#/properties/data/properties/display_sequence/items",
            "type": "string",
            "pattern": "^(.*)$"
          }
        },
        "cancellation_fee": {
          "$id": "#/properties/data/properties/cancellation_fee",
          "type": "number"
        },
        "total_tax": {
          "$id": "#/properties/data/properties/total_tax",
          "type": "number"
        },
        "cart_type": {
          "$id": "#/properties/data/properties/cart_type",
          "type": "string",
          "pattern": "^(.*)$"
        },
        "configurations": {
          "$id": "#/properties/data/properties/configurations",
          "type": "object"
        },
        "messages": {
          "$id": "#/properties/data/properties/messages",
          "type": "array"
        },
        "GST_details": {
          "$id": "#/properties/data/properties/GST_details",
          "type": "object",
          "required": [
            "item_SGST",
            "item_CGST",
            "item_IGST",
            "packaging_SGST",
            "packaging_IGST",
            "packaging_CGST",
            "service_charge_SGST",
            "service_charge_IGST",
            "service_charge_CGST",
            "external_GST",
            "cart_SGST",
            "cart_CGST",
            "cart_IGST"
          ],
          "properties": {
            "item_SGST": {
              "$id": "#/properties/data/properties/GST_details/properties/item_SGST",
              "type": "number"
            },
            "item_CGST": {
              "$id": "#/properties/data/properties/GST_details/properties/item_CGST",
              "type": "number"
            },
            "item_IGST": {
              "$id": "#/properties/data/properties/GST_details/properties/item_IGST",
              "type": "number"
            },
            "packaging_SGST": {
              "$id": "#/properties/data/properties/GST_details/properties/packaging_SGST",
              "type": "number"
            },
            "packaging_IGST": {
              "$id": "#/properties/data/properties/GST_details/properties/packaging_IGST",
              "type": "number"
            },
            "packaging_CGST": {
              "$id": "#/properties/data/properties/GST_details/properties/packaging_CGST",
              "type": "number"
            },
            "service_charge_SGST": {
              "$id": "#/properties/data/properties/GST_details/properties/service_charge_SGST",
              "type": "number"
            },
            "service_charge_IGST": {
              "$id": "#/properties/data/properties/GST_details/properties/service_charge_IGST",
              "type": "number"
            },
            "service_charge_CGST": {
              "$id": "#/properties/data/properties/GST_details/properties/service_charge_CGST",
              "type": "number"
            },
            "external_GST": {
              "$id": "#/properties/data/properties/GST_details/properties/external_GST",
              "type": "number"
            },
            "cart_SGST": {
              "$id": "#/properties/data/properties/GST_details/properties/cart_SGST",
              "type": "number"
            },
            "cart_CGST": {
              "$id": "#/properties/data/properties/GST_details/properties/cart_CGST",
              "type": "number"
            },
            "cart_IGST": {
              "$id": "#/properties/data/properties/GST_details/properties/cart_IGST",
              "type": "number"
            }
          }
        },
        "threshold_fee": {
          "$id": "#/properties/data/properties/threshold_fee",
          "type": "number"
        },
        "distance_fee": {
          "$id": "#/properties/data/properties/distance_fee",
          "type": "number"
        },
        "time_fee": {
          "$id": "#/properties/data/properties/time_fee",
          "type": "number"
        },
        "special_fee": {
          "$id": "#/properties/data/properties/special_fee",
          "type": "number"
        },
        "convenience_fee": {
          "$id": "#/properties/data/properties/convenience_fee",
          "type": "number"
        },
        "delivery_charges": {
          "$id": "#/properties/data/properties/delivery_charges",
          "type": "number"
        },
        "threshold_fee_message": {
          "$id": "#/properties/data/properties/threshold_fee_message",
          "type": "string",
          "pattern": "^(.*)$"
        },
        "distance_fee_message": {
          "$id": "#/properties/data/properties/distance_fee_message",
          "type": "string",
          "pattern": "^(.*)$"
        },
        "time_fee_message": {
          "$id": "#/properties/data/properties/time_fee_message",
          "type": "string",
          "pattern": "^(.*)$"
        },
        "special_fee_message": {
          "$id": "#/properties/data/properties/special_fee_message",
          "type": "string",
          "pattern": "^(.*)$"
        },
        "bill_total": {
          "$id": "#/properties/data/properties/bill_total",
          "type": "number"
        },
        "item_total": {
          "$id": "#/properties/data/properties/item_total",
          "type": "number"
        },
        "subscription_total": {
          "$id": "#/properties/data/properties/subscription_total",
          "type": "number"
        },
        "subscription_items": {
          "$id": "#/properties/data/properties/subscription_items",
          "type": "array"
        },
        "show_super_nudge": {
          "$id": "#/properties/data/properties/show_super_nudge",
          "type": "boolean"
        },
        "is_super_trade_discount": {
          "$id": "#/properties/data/properties/is_super_trade_discount",
          "type": "boolean"
        },
        "free_delivery_discount": {
          "$id": "#/properties/data/properties/free_delivery_discount",
          "type": "number"
        },
        "cart_token": {
          "$id": "#/properties/data/properties/cart_token",
          "type": "string",
          "pattern": "^(.*)$"
        }
      }
    },
    "tid": {
      "$id": "#/properties/tid",
      "type": "string",
      "pattern": "^(.*)$"
    },
    "sid": {
      "$id": "#/properties/sid",
      "type": "string",
      "pattern": "^(.*)$"
    },
    "deviceId": {
      "$id": "#/properties/deviceId",
      "type": "string",
      "pattern": "^(.*)$"
    },
    "successful": {
      "$id": "#/properties/successful",
      "type": "boolean"
    }
  }
}