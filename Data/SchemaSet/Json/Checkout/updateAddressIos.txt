{
  "definitions": {},
  "$schema": "http://json-schema.org/draft-07/schema#",
  "$id": "http://example.com/root.json",
  "type": "object",
  "title": "The Root Schema",
  "required": [
    "statusCode",
    "statusMessage",
    "data",
    "tid",
    "sid",
    "deviceId",
    "successful"
  ],
  "properties": {
    "statusCode": {
      "$id": "#/properties/statusCode",
      "type": "number",
      "title": "The Statuscode Schema",
      "default": 0,
      "examples": [
        0
      ]
    },
    "statusMessage": {
      "$id": "#/properties/statusMessage",
      "type": "string",
      "title": "The Statusmessage Schema",
      "default": "",
      "examples": [
        "Address updated successfully"
      ],
      "pattern": "^(.*)$"
    },
    "data": {
      "$id": "#/properties/data",
      "type": "object",
      "title": "The Data Schema",
       "oneOf": [
                    {
                        "required": [
                                    "address_id",
                                    "delivery_valid",
                                    "estimated_sla",
                                    "estimated_sla_min",
                                    "estimated_sla_max",
                                    "recalculation_required"
                                          ]
                    },
                    {
                        "required": [
                                    "address_id"
                                          ]
                    }

                ],
      "properties": {
        "address_id": {
          "$id": "#/properties/data/properties/address_id",
          "type": "number",
          "title": "The Address_id Schema",
          "default": 0,
          "examples": [
            34826757
          ]
        },
        "delivery_valid": {
          "$id": "#/properties/data/properties/delivery_valid",
          "type": "number",
          "title": "The Delivery_valid Schema",
          "default": 0,
          "examples": [
            1
          ]
        },
        "estimated_sla": {
          "$id": "#/properties/data/properties/estimated_sla",
          "type": "number",
          "title": "The Estimated_sla Schema",
          "default": 0,
          "examples": [
            44
          ]
        },
        "estimated_sla_min": {
          "$id": "#/properties/data/properties/estimated_sla_min",
          "type": "number",
          "title": "The Estimated_sla_min Schema",
          "default": 0,
          "examples": [
            40
          ]
        },
        "estimated_sla_max": {
          "$id": "#/properties/data/properties/estimated_sla_max",
          "type": "number",
          "title": "The Estimated_sla_max Schema",
          "default": 0,
          "examples": [
            50
          ]
        },
        "recalculation_required": {
          "$id": "#/properties/data/properties/recalculation_required",
          "type": "boolean",
          "title": "The Recalculation_required Schema",
          "default": false,
          "examples": [
            true
          ]
        }
      }
    },
    "tid": {
      "$id": "#/properties/tid",
      "type": "string",
      "title": "The Tid Schema",
      "default": "",
      "examples": [
        "639c2d46-a2ff-4823-b8c7-df12f916832c"
      ],
      "pattern": "^(.*)$"
    },
    "sid": {
      "$id": "#/properties/sid",
      "type": "string",
      "title": "The Sid Schema",
      "default": "",
      "examples": [
        "b93fff09-9f18-4991-9e5f-bfec0ffc1170"
      ],
      "pattern": "^(.*)$"
    },
    "deviceId": {
      "$id": "#/properties/deviceId",
      "type": "string",
      "title": "The Deviceid Schema",
      "default": "",
      "examples": [
        "1cfec8a8-0de2-484f-8f75-af007a055468"
      ],
      "pattern": "^(.*)$"
    },
    "successful": {
      "$id": "#/properties/successful",
      "type": "boolean",
      "title": "The Successful Schema",
      "default": false,
      "examples": [
        true
      ]
    }
  }
}