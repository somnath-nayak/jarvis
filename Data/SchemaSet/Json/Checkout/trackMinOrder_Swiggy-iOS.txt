{
  "definitions": {},
  "$schema": "http://json-schema.org/draft-06/schema#",
  "$id": "http://example.com/root.json",
  "type": "object",
  "title": "The Root Schema",
  "required": [
    "statusCode",
    "statusMessage",
    "data",
    "tid",
    "sid",
    "deviceId",
    "successful"
  ],
  "properties": {
    "statusCode": {
      "$id": "#/properties/statusCode",
      "type": "number",
      "title": "The Statuscode Schema",
      "default": 0,
      "examples": [
        0
      ]
    },
    "statusMessage": {
      "$id": "#/properties/statusMessage",
      "type": "string",
      "title": "The Statusmessage Schema",
      "default": "",
      "examples": [
        "ORDER_TRACKED_SUCCESSFULLY"
      ],
      "pattern": "^(.*)$"
    },
    "data": {
      "$id": "#/properties/data",
      "type": "object",
      "title": "The Data Schema",
      "required": [
        "delivery_status",
        "distance",
        "restaurant",
        "billing_address",
        "delivery_boy",
        "expected_time",
        "message_position_text",
        "message_position_number",
        "annotation",
        "annotation_image_url"
      ],
      "properties": {
        "delivery_status": {
          "$id": "#/properties/data/properties/delivery_status",
          "type": "string",
          "title": "The Delivery_status Schema",
          "default": "",
          "examples": [
            "unassigned"
          ],
          "pattern": "^(.*)$"
        },
        "distance": {
          "$id": "#/properties/data/properties/distance",
          "type": "null",
          "title": "The Distance Schema",
          "default": null,
          "examples": [
            null
          ]
        },
        "restaurant": {
          "$id": "#/properties/data/properties/restaurant",
          "type": "object",
          "title": "The Restaurant Schema",
          "required": [
            "lat",
            "lng"
          ],
          "properties": {
            "lat": {
              "$id": "#/properties/data/properties/restaurant/properties/lat",
              "type": "string",
              "title": "The Lat Schema",
              "default": "",
              "examples": [
                "27.8374445"
              ],
              "pattern": "^(.*)$"
            },
            "lng": {
              "$id": "#/properties/data/properties/restaurant/properties/lng",
              "type": "string",
              "title": "The Lng Schema",
              "default": "",
              "examples": [
                "76.17257740000002"
              ],
              "pattern": "^(.*)$"
            }
          }
        },
        "billing_address": {
          "$id": "#/properties/data/properties/billing_address",
          "type": "object",
          "title": "The Billing_address Schema",
          "required": [
            "lat",
            "lng"
          ],
          "properties": {
            "lat": {
              "$id": "#/properties/data/properties/billing_address/properties/lat",
              "type": "string",
              "title": "The Lat Schema",
              "default": "",
              "examples": [
                "27.835629"
              ],
              "pattern": "^(.*)$"
            },
            "lng": {
              "$id": "#/properties/data/properties/billing_address/properties/lng",
              "type": "string",
              "title": "The Lng Schema",
              "default": "",
              "examples": [
                "76.173089"
              ],
              "pattern": "^(.*)$"
            }
          }
        },
        "delivery_boy": {
          "$id": "#/properties/data/properties/delivery_boy",
          "type": "object",
          "title": "The Delivery_boy Schema",
          "required": [
            "lat",
            "lng"
          ],
          "properties": {
            "lat": {
              "$id": "#/properties/data/properties/delivery_boy/properties/lat",
              "type": "string",
              "title": "The Lat Schema",
              "default": "",
              "examples": [
                ""
              ],
              "pattern": "^(.*)$"
            },
            "lng": {
              "$id": "#/properties/data/properties/delivery_boy/properties/lng",
              "type": "string",
              "title": "The Lng Schema",
              "default": "",
              "examples": [
                ""
              ],
              "pattern": "^(.*)$"
            }
          }
        },
        "expected_time": {
          "$id": "#/properties/data/properties/expected_time",
          "type": "number",
          "title": "The Expected_time Schema",
          "default": 0,
          "examples": [
            0
          ]
        },
        "message_position_text": {
          "$id": "#/properties/data/properties/message_position_text",
          "type": "string",
          "title": "The Message_position_text Schema",
          "default": "",
          "examples": [
            "Order Received"
          ],
          "pattern": "^(.*)$"
        },
        "message_position_number": {
          "$id": "#/properties/data/properties/message_position_number",
          "type": "string",
          "title": "The Message_position_number Schema",
          "default": "",
          "examples": [
            "1"
          ],
          "pattern": "^(.*)$"
        },
        "annotation": {
          "$id": "#/properties/data/properties/annotation",
          "type": "string",
          "title": "The Annotation Schema",
          "default": "",
          "examples": [
            "Testing"
          ],
          "pattern": "^(.*)$"
        },
        "annotation_image_url": {
          "$id": "#/properties/data/properties/annotation_image_url",
          "type": "string",
          "title": "The Annotation_image_url Schema",
          "default": "",
          "examples": [
            "set/others/annotation/icon/url/from/cms"
          ],
          "pattern": "^(.*)$"
        }
      }
    },
    "tid": {
      "$id": "#/properties/tid",
      "type": "string",
      "title": "The Tid Schema",
      "default": "",
      "examples": [
        "a5b90950-da0a-4ddd-b363-9abc1e3fa0c8"
      ],
      "pattern": "^(.*)$"
    },
    "sid": {
      "$id": "#/properties/sid",
      "type": "string",
      "title": "The Sid Schema",
      "default": "",
      "examples": [
        "bf45ad53-b5c8-42f5-afab-f11d9a9581ec"
      ],
      "pattern": "^(.*)$"
    },
    "deviceId": {
      "$id": "#/properties/deviceId",
      "type": "string",
      "title": "The Deviceid Schema",
      "default": "",
      "examples": [
        "600911ca-cde1-4f21-9084-2bb6e4a9ed09"
      ],
      "pattern": "^(.*)$"
    },
    "successful": {
      "$id": "#/properties/successful",
      "type": "boolean",
      "title": "The Successful Schema",
      "default": false,
      "examples": [
        true
      ]
    }
  }
}