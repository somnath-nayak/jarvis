{
  "$id": "http://example.com/example.json",
  "type": "object",
  "definitions": {},
  "$schema": "http://json-schema.org/draft-06/schema#",
  "properties": {
    "statusCode": {
      "$id": "/properties/statusCode",
      "type": "integer",
      "title": "The Statuscode Schema ",
      "default": 0,
      "examples": [
        0
      ]
    },
    "statusMessage": {
      "$id": "/properties/statusMessage",
      "type": "string",
      "title": "The Statusmessage Schema ",
      "default": "",
      "examples": [
        "done successfully"
      ]
    },
    "data": {
      "$id": "/properties/data",
      "type": "object",
      "properties": {
        "corporates": {
          "$id": "/properties/data/properties/corporates",
          "type": "object",
          "properties": {
            "1": {
              "$id": "/properties/data/properties/corporates/properties/1",
              "type": "object",
              "properties": {
                "id": {
                  "$id": "/properties/data/properties/corporates/properties/1/properties/id",
                  "type": "string",
                  "title": "The Id Schema ",
                  "default": "",
                  "examples": [
                    "1"
                  ]
                },
                "name": {
                  "$id": "/properties/data/properties/corporates/properties/1/properties/name",
                  "type": "string",
                  "title": "The Name Schema ",
                  "default": "",
                  "examples": [
                    "Test corporate"
                  ]
                },
                "enabled": {
                  "$id": "/properties/data/properties/corporates/properties/1/properties/enabled",
                  "type": "integer",
                  "title": "The Enabled Schema ",
                  "default": 0,
                  "examples": [
                    1
                  ]
                },
                "imageId": {
                  "$id": "/properties/data/properties/corporates/properties/1/properties/imageId",
                  "type": "string",
                  "title": "The Imageid Schema ",
                  "default": "",
                  "examples": [
                    "test"
                  ]
                },
                "cafes": {
                  "$id": "/properties/data/properties/corporates/properties/1/properties/cafes",
                  "type": "array",
                  "items": {
                    "$id": "/properties/data/properties/corporates/properties/1/properties/cafes/items",
                    "type": "object",
                    "properties": {
                      "id": {
                        "$id": "/properties/data/properties/corporates/properties/1/properties/cafes/items/properties/id",
                        "type": "string",
                        "title": "The Id Schema ",
                        "default": "",
                        "examples": [
                          "2"
                        ]
                      },
                      "name": {
                        "$id": "/properties/data/properties/corporates/properties/1/properties/cafes/items/properties/name",
                        "type": "string",
                        "title": "The Name Schema ",
                        "default": "",
                        "examples": [
                          "TestCafe"
                        ]
                      },
                      "enabled": {
                        "$id": "/properties/data/properties/corporates/properties/1/properties/cafes/items/properties/enabled",
                        "type": "integer",
                        "title": "The Enabled Schema ",
                        "default": 0,
                        "examples": [
                          1
                        ]
                      },
                      "imageId": {
                        "$id": "/properties/data/properties/corporates/properties/1/properties/cafes/items/properties/imageId",
                        "type": "string",
                        "title": "The Imageid Schema ",
                        "default": "",
                        "examples": [
                          "test"
                        ]
                      },
                      "address": {
                        "$id": "/properties/data/properties/corporates/properties/1/properties/cafes/items/properties/address",
                        "type": "string",
                        "title": "The Address Schema ",
                        "default": "",
                        "examples": [
                          ""
                        ]
                      },
                      "restaurants": {
                        "$id": "/properties/data/properties/corporates/properties/1/properties/cafes/items/properties/restaurants",
                        "type": "array",
                        "items": {
                          "$id": "/properties/data/properties/corporates/properties/1/properties/cafes/items/properties/restaurants/items",
                          "type": "object",
                          "properties": {
                            "id": {
                              "$id": "/properties/data/properties/corporates/properties/1/properties/cafes/items/properties/restaurants/items/properties/id",
                              "type": "string",
                              "title": "The Id Schema ",
                              "default": "",
                              "examples": [
                                "3453"
                              ]
                            }
                          }
                        }
                      }
                    }
                  }
                },
                "passcodeAvailable": {
                  "$id": "/properties/data/properties/corporates/properties/1/properties/passcodeAvailable",
                  "type": "boolean",
                  "title": "The Passcodeavailable Schema ",
                  "default": false,
                  "examples": [
                    true
                  ]
                }
              }
            },
            "3": {
              "$id": "/properties/data/properties/corporates/properties/3",
              "type": "object",
              "properties": {
                "id": {
                  "$id": "/properties/data/properties/corporates/properties/3/properties/id",
                  "type": "string",
                  "title": "The Id Schema ",
                  "default": "",
                  "examples": [
                    "3"
                  ]
                },
                "name": {
                  "$id": "/properties/data/properties/corporates/properties/3/properties/name",
                  "type": "string",
                  "title": "The Name Schema ",
                  "default": "",
                  "examples": [
                    "Swiggy corporate 1 (2 cafe)"
                  ]
                },
                "enabled": {
                  "$id": "/properties/data/properties/corporates/properties/3/properties/enabled",
                  "type": "integer",
                  "title": "The Enabled Schema ",
                  "default": 0,
                  "examples": [
                    1
                  ]
                },
                "imageId": {
                  "$id": "/properties/data/properties/corporates/properties/3/properties/imageId",
                  "type": "string",
                  "title": "The Imageid Schema ",
                  "default": "",
                  "examples": [
                    "test"
                  ]
                },
                "cafes": {
                  "$id": "/properties/data/properties/corporates/properties/3/properties/cafes",
                  "type": "array",
                  "items": {
                    "$id": "/properties/data/properties/corporates/properties/3/properties/cafes/items",
                    "type": "object",
                    "properties": {
                      "id": {
                        "$id": "/properties/data/properties/corporates/properties/3/properties/cafes/items/properties/id",
                        "type": "string",
                        "title": "The Id Schema ",
                        "default": "",
                        "examples": [
                          "6"
                        ]
                      },
                      "name": {
                        "$id": "/properties/data/properties/corporates/properties/3/properties/cafes/items/properties/name",
                        "type": "string",
                        "title": "The Name Schema ",
                        "default": "",
                        "examples": [
                          "Swiggy cafe2"
                        ]
                      },
                      "enabled": {
                        "$id": "/properties/data/properties/corporates/properties/3/properties/cafes/items/properties/enabled",
                        "type": "integer",
                        "title": "The Enabled Schema ",
                        "default": 0,
                        "examples": [
                          1
                        ]
                      },
                      "imageId": {
                        "$id": "/properties/data/properties/corporates/properties/3/properties/cafes/items/properties/imageId",
                        "type": "string",
                        "title": "The Imageid Schema ",
                        "default": "",
                        "examples": [
                          "test"
                        ]
                      },
                      "address": {
                        "$id": "/properties/data/properties/corporates/properties/3/properties/cafes/items/properties/address",
                        "type": "string",
                        "title": "The Address Schema ",
                        "default": "",
                        "examples": [
                          ""
                        ]
                      },
                      "restaurants": {
                        "$id": "/properties/data/properties/corporates/properties/3/properties/cafes/items/properties/restaurants",
                        "type": "array",
                        "items": {
                          "$id": "/properties/data/properties/corporates/properties/3/properties/cafes/items/properties/restaurants/items",
                          "type": "object",
                          "properties": {
                            "id": {
                              "$id": "/properties/data/properties/corporates/properties/3/properties/cafes/items/properties/restaurants/items/properties/id",
                              "type": "string",
                              "title": "The Id Schema ",
                              "default": "",
                              "examples": [
                                "2698"
                              ]
                            }
                          }
                        }
                      }
                    }
                  }
                },
                "passcodeAvailable": {
                  "$id": "/properties/data/properties/corporates/properties/3/properties/passcodeAvailable",
                  "type": "boolean",
                  "title": "The Passcodeavailable Schema ",
                  "default": false,
                  "examples": [
                    true
                  ]
                }
              }
            },
            "4": {
              "$id": "/properties/data/properties/corporates/properties/4",
              "type": "object",
              "properties": {
                "id": {
                  "$id": "/properties/data/properties/corporates/properties/4/properties/id",
                  "type": "string",
                  "title": "The Id Schema ",
                  "default": "",
                  "examples": [
                    "4"
                  ]
                },
                "name": {
                  "$id": "/properties/data/properties/corporates/properties/4/properties/name",
                  "type": "string",
                  "title": "The Name Schema ",
                  "default": "",
                  "examples": [
                    "Swiggy corporate 2 (1 cafe)"
                  ]
                },
                "enabled": {
                  "$id": "/properties/data/properties/corporates/properties/4/properties/enabled",
                  "type": "integer",
                  "title": "The Enabled Schema ",
                  "default": 0,
                  "examples": [
                    1
                  ]
                },
                "imageId": {
                  "$id": "/properties/data/properties/corporates/properties/4/properties/imageId",
                  "type": "string",
                  "title": "The Imageid Schema ",
                  "default": "",
                  "examples": [
                    "test"
                  ]
                },
                "cafes": {
                  "$id": "/properties/data/properties/corporates/properties/4/properties/cafes",
                  "type": "array",
                  "items": {
                    "$id": "/properties/data/properties/corporates/properties/4/properties/cafes/items",
                    "type": "object",
                    "properties": {
                      "id": {
                        "$id": "/properties/data/properties/corporates/properties/4/properties/cafes/items/properties/id",
                        "type": "string",
                        "title": "The Id Schema ",
                        "default": "",
                        "examples": [
                          "5"
                        ]
                      },
                      "name": {
                        "$id": "/properties/data/properties/corporates/properties/4/properties/cafes/items/properties/name",
                        "type": "string",
                        "title": "The Name Schema ",
                        "default": "",
                        "examples": [
                          "Swiggy cafe1"
                        ]
                      },
                      "enabled": {
                        "$id": "/properties/data/properties/corporates/properties/4/properties/cafes/items/properties/enabled",
                        "type": "integer",
                        "title": "The Enabled Schema ",
                        "default": 0,
                        "examples": [
                          1
                        ]
                      },
                      "imageId": {
                        "$id": "/properties/data/properties/corporates/properties/4/properties/cafes/items/properties/imageId",
                        "type": "string",
                        "title": "The Imageid Schema ",
                        "default": "",
                        "examples": [
                          "test"
                        ]
                      },
                      "address": {
                        "$id": "/properties/data/properties/corporates/properties/4/properties/cafes/items/properties/address",
                        "type": "string",
                        "title": "The Address Schema ",
                        "default": "",
                        "examples": [
                          ""
                        ]
                      },
                      "restaurants": {
                        "$id": "/properties/data/properties/corporates/properties/4/properties/cafes/items/properties/restaurants",
                        "type": "array",
                        "items": {
                          "$id": "/properties/data/properties/corporates/properties/4/properties/cafes/items/properties/restaurants/items",
                          "type": "object",
                          "properties": {
                            "id": {
                              "$id": "/properties/data/properties/corporates/properties/4/properties/cafes/items/properties/restaurants/items/properties/id",
                              "type": "string",
                              "title": "The Id Schema ",
                              "default": "",
                              "examples": [
                                "2422"
                              ]
                            }
                          }
                        }
                      }
                    }
                  }
                },
                "passcodeAvailable": {
                  "$id": "/properties/data/properties/corporates/properties/4/properties/passcodeAvailable",
                  "type": "boolean",
                  "title": "The Passcodeavailable Schema ",
                  "default": false,
                  "examples": [
                    true
                  ]
                }
              }
            },
            "5": {
              "$id": "/properties/data/properties/corporates/properties/5",
              "type": "object",
              "properties": {
                "id": {
                  "$id": "/properties/data/properties/corporates/properties/5/properties/id",
                  "type": "string",
                  "title": "The Id Schema ",
                  "default": "",
                  "examples": [
                    "5"
                  ]
                },
                "name": {
                  "$id": "/properties/data/properties/corporates/properties/5/properties/name",
                  "type": "string",
                  "title": "The Name Schema ",
                  "default": "",
                  "examples": [
                    "TCS"
                  ]
                },
                "enabled": {
                  "$id": "/properties/data/properties/corporates/properties/5/properties/enabled",
                  "type": "integer",
                  "title": "The Enabled Schema ",
                  "default": 0,
                  "examples": [
                    1
                  ]
                },
                "imageId": {
                  "$id": "/properties/data/properties/corporates/properties/5/properties/imageId",
                  "type": "string",
                  "title": "The Imageid Schema ",
                  "default": "",
                  "examples": [
                    "TCS"
                  ]
                },
                "cafes": {
                  "$id": "/properties/data/properties/corporates/properties/5/properties/cafes",
                  "type": "array",
                  "items": {
                    "$id": "/properties/data/properties/corporates/properties/5/properties/cafes/items",
                    "type": "object",
                    "properties": {
                      "id": {
                        "$id": "/properties/data/properties/corporates/properties/5/properties/cafes/items/properties/id",
                        "type": "string",
                        "title": "The Id Schema ",
                        "default": "",
                        "examples": [
                          "8"
                        ]
                      },
                      "name": {
                        "$id": "/properties/data/properties/corporates/properties/5/properties/cafes/items/properties/name",
                        "type": "string",
                        "title": "The Name Schema ",
                        "default": "",
                        "examples": [
                          "Cafe 0001"
                        ]
                      },
                      "enabled": {
                        "$id": "/properties/data/properties/corporates/properties/5/properties/cafes/items/properties/enabled",
                        "type": "integer",
                        "title": "The Enabled Schema ",
                        "default": 0,
                        "examples": [
                          1
                        ]
                      },
                      "imageId": {
                        "$id": "/properties/data/properties/corporates/properties/5/properties/cafes/items/properties/imageId",
                        "type": "string",
                        "title": "The Imageid Schema ",
                        "default": "",
                        "examples": [
                          "cafe0001"
                        ]
                      },
                      "address": {
                        "$id": "/properties/data/properties/corporates/properties/5/properties/cafes/items/properties/address",
                        "type": "string",
                        "title": "The Address Schema ",
                        "default": "",
                        "examples": [
                          ""
                        ]
                      },
                      "restaurants": {
                        "$id": "/properties/data/properties/corporates/properties/5/properties/cafes/items/properties/restaurants",
                        "type": "array",
                        "items": {
                          "$id": "/properties/data/properties/corporates/properties/5/properties/cafes/items/properties/restaurants/items",
                          "type": "object",
                          "properties": {
                            "id": {
                              "$id": "/properties/data/properties/corporates/properties/5/properties/cafes/items/properties/restaurants/items/properties/id",
                              "type": "string",
                              "title": "The Id Schema ",
                              "default": "",
                              "examples": [
                                "39762"
                              ]
                            }
                          }
                        }
                      }
                    }
                  }
                },
                "passcodeAvailable": {
                  "$id": "/properties/data/properties/corporates/properties/5/properties/passcodeAvailable",
                  "type": "boolean",
                  "title": "The Passcodeavailable Schema ",
                  "default": false,
                  "examples": [
                    true
                  ]
                }
              }
            },
            "6": {
              "$id": "/properties/data/properties/corporates/properties/6",
              "type": "object",
              "properties": {
                "id": {
                  "$id": "/properties/data/properties/corporates/properties/6/properties/id",
                  "type": "string",
                  "title": "The Id Schema ",
                  "default": "",
                  "examples": [
                    "6"
                  ]
                },
                "name": {
                  "$id": "/properties/data/properties/corporates/properties/6/properties/name",
                  "type": "string",
                  "title": "The Name Schema ",
                  "default": "",
                  "examples": [
                    "WIPRO"
                  ]
                },
                "enabled": {
                  "$id": "/properties/data/properties/corporates/properties/6/properties/enabled",
                  "type": "integer",
                  "title": "The Enabled Schema ",
                  "default": 0,
                  "examples": [
                    1
                  ]
                },
                "imageId": {
                  "$id": "/properties/data/properties/corporates/properties/6/properties/imageId",
                  "type": "string",
                  "title": "The Imageid Schema ",
                  "default": "",
                  "examples": [
                    "WIPRO"
                  ]
                },
                "cafes": {
                  "$id": "/properties/data/properties/corporates/properties/6/properties/cafes",
                  "type": "array",
                  "items": {
                    "$id": "/properties/data/properties/corporates/properties/6/properties/cafes/items",
                    "type": "object",
                    "properties": {
                      "id": {
                        "$id": "/properties/data/properties/corporates/properties/6/properties/cafes/items/properties/id",
                        "type": "string",
                        "title": "The Id Schema ",
                        "default": "",
                        "examples": [
                          "9"
                        ]
                      },
                      "name": {
                        "$id": "/properties/data/properties/corporates/properties/6/properties/cafes/items/properties/name",
                        "type": "string",
                        "title": "The Name Schema ",
                        "default": "",
                        "examples": [
                          "Cafe 0002"
                        ]
                      },
                      "enabled": {
                        "$id": "/properties/data/properties/corporates/properties/6/properties/cafes/items/properties/enabled",
                        "type": "integer",
                        "title": "The Enabled Schema ",
                        "default": 0,
                        "examples": [
                          1
                        ]
                      },
                      "imageId": {
                        "$id": "/properties/data/properties/corporates/properties/6/properties/cafes/items/properties/imageId",
                        "type": "string",
                        "title": "The Imageid Schema ",
                        "default": "",
                        "examples": [
                          "cafe0002"
                        ]
                      },
                      "address": {
                        "$id": "/properties/data/properties/corporates/properties/6/properties/cafes/items/properties/address",
                        "type": "string",
                        "title": "The Address Schema ",
                        "default": "",
                        "examples": [
                          ""
                        ]
                      },
                      "restaurants": {
                        "$id": "/properties/data/properties/corporates/properties/6/properties/cafes/items/properties/restaurants",
                        "type": "array",
                        "items": {
                          "$id": "/properties/data/properties/corporates/properties/6/properties/cafes/items/properties/restaurants/items",
                          "type": "object",
                          "properties": {
                            "id": {
                              "$id": "/properties/data/properties/corporates/properties/6/properties/cafes/items/properties/restaurants/items/properties/id",
                              "type": "string",
                              "title": "The Id Schema ",
                              "default": "",
                              "examples": [
                                "39382"
                              ]
                            }
                          }
                        }
                      }
                    }
                  }
                },
                "passcodeAvailable": {
                  "$id": "/properties/data/properties/corporates/properties/6/properties/passcodeAvailable",
                  "type": "boolean",
                  "title": "The Passcodeavailable Schema ",
                  "default": false,
                  "examples": [
                    true
                  ]
                }
              }
            }
          }
        }
      }
    },
    "tid": {
      "$id": "/properties/tid",
      "type": "string",
      "title": "The Tid Schema ",
      "default": "",
      "examples": [
        "db0c5f19-775c-4af4-8a1b-2b8181855716"
      ]
    },
    "sid": {
      "$id": "/properties/sid",
      "type": "string",
      "title": "The Sid Schema ",
      "default": "",
      "examples": [
        "8mc9d352-7f8d-4ae8-ad73-f428add42692"
      ]
    },
    "deviceId": {
      "$id": "/properties/deviceId",
      "type": "string",
      "title": "The Deviceid Schema ",
      "default": "",
      "examples": [
        "cbf75989-aaf0-4eb8-979d-5831ab581239"
      ]
    }
  }
}