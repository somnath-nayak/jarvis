{
  "definitions": {}, 
  "$schema": "http://json-schema.org/draft-04/schema#", 
  "id": "http://example.com/example.json", 
  "type": "object", 
  "properties": {
    "statusCode": {
      "id": "/properties/statusCode", 
      "type": "number", 
      "title": "The Statuscode Schema.", 
      "description": "An explanation about the purpose of this instance.", 
      "default": 0
    }, 
    "statusMessage": {
      "id": "/properties/statusMessage", 
      "type": "string", 
      "title": "The Statusmessage Schema.", 
      "description": "An explanation about the purpose of this instance.", 
      "default": ""
    }, 
    "data": {
      "id": "/properties/data", 
      "type": "object", 
      "properties": {
        "items": {
          "id": "/properties/data/properties/items", 
          "type": "array", 
          "items": {
            "id": "/properties/data/properties/items/items", 
            "type": "object", 
            "properties": {
              "id": {
                "id": "/properties/data/properties/items/items/properties/id", 
                "type": "number", 
                "title": "The Id Schema.", 
                "description": "An explanation about the purpose of this instance.", 
                "default": 0
              }, 
              "score": {
                "id": "/properties/data/properties/items/items/properties/score", 
                "type": "number", 
                "title": "The Score Schema.", 
                "description": "An explanation about the purpose of this instance.", 
                "default": 0
              }
            }
          }
        }, 
        "sortMethod": {
          "id": "/properties/data/properties/sortMethod", 
          "type": "string", 
          "title": "The Sortmethod Schema.", 
          "description": "An explanation about the purpose of this instance.", 
          "default": ""
        }, 
        "id": {
          "id": "/properties/data/properties/id", 
          "type": "string", 
          "title": "The Id Schema.", 
          "description": "An explanation about the purpose of this instance.", 
          "default": ""
        }
      }
    }
  }
}