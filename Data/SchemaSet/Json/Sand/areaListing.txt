{
	"definitions": {},
	"$schema": "http://json-schema.org/draft-04/schema#",
	"id": "http://exampe.com/exa2mple.json",
	"type": "object",
	"properties": {
		"statusCode": {
			"id": "/properties/statusCode",
			"type": "integer",
			"title": "The Statuscode Schema.",
			"description": "An explanation about the purpose of this instance.",
			"default": 0
		},
		"statusMessage": {
			"id": "/properties/statusMessage",
			"type": "string",
			"title": "The Statusmessage Schema.",
			"description": "An explanation about the purpose of this instance.",
			"default": ""
		},
		"data": {
			"id": "/properties/data",
			"type": "object",
			"properties": {
				"restaurants": {
					"id": "/properties/data/properties/restaurants",
					"type": "array",
					"items": {
						"id": "/properties/data/properties/restaurants/items",
						"type": "object",
						"properties": {
							"id": {
								"id": "/properties/data/properties/restaurants/items/properties/id",
								"type": "string",
								"title": "The Id Schema.",
								"description": "An explanation about the purpose of this instance.",
								"default": ""
							},
							"name": {
								"id": "/properties/data/properties/restaurants/items/properties/name",
								"type": "string",
								"title": "The Name Schema.",
								"description": "An explanation about the purpose of this instance.",
								"default": ""
							},
							"type": {
								"id": "/properties/data/properties/restaurants/items/properties/type",
								"type": "string",
								"title": "The Type Schema.",
								"description": "An explanation about the purpose of this instance.",
								"default": ""
							},
							"areaId": {
								"id": "/properties/data/properties/restaurants/items/properties/areaId",
								"type": "integer",
								"title": "The Areaid Schema.",
								"description": "An explanation about the purpose of this instance.",
								"default": 0
							},
							"cityId": {
								"id": "/properties/data/properties/restaurants/items/properties/cityId",
								"type": "integer",
								"title": "The Cityid Schema.",
								"description": "An explanation about the purpose of this instance.",
								"default": 0
							},
							"slugs": {
								"id": "/properties/data/properties/restaurants/items/properties/slugs",
								"type": "object",
								"properties": {
									"restaurant": {
										"id": "/properties/data/properties/restaurants/items/properties/slugs/properties/restaurant",
										"type": "string",
										"title": "The Restaurant Schema.",
										"description": "An explanation about the purpose of this instance.",
										"default": ""
									},
									"city": {
										"id": "/properties/data/properties/restaurants/items/properties/slugs/properties/city",
										"type": "string",
										"title": "The City Schema.",
										"description": "An explanation about the purpose of this instance.",
										"default": ""
									}
								}
							},
							"uuid": {
								"id": "/properties/data/properties/restaurants/items/properties/uuid",
								"type": ["string", "null"],
								"title": "The Uuid Schema.",
								"description": "An explanation about the purpose of this instance.",
								"default": ""
							},
							"cloudinaryImageId": {
								"id": "/properties/data/properties/restaurants/items/properties/cloudinaryImageId",
								"type": ["string", "null"],
								"title": "The Cloudinaryimageid Schema.",
								"description": "An explanation about the purpose of this instance.",
								"default": ""
							},
							"place": {
								"id": "/properties/data/properties/restaurants/items/properties/place",
								"type": ["string", "null"],
								"title": "The Place Schema.",
								"description": "An explanation about the purpose of this instance.",
								"default": ""
							},
							"address": {
								"id": "/properties/data/properties/restaurants/items/properties/address",
								"type": ["string", "null"],
								"title": "The Address Schema.",
								"description": "An explanation about the purpose of this instance.",
								"default": ""
							},
							"locality": {
								"id": "/properties/data/properties/restaurants/items/properties/locality",
								"type": "string",
								"title": "The Locality Schema.",
								"description": "An explanation about the purpose of this instance.",
								"default": ""
							},
							"areaName": {
								"id": "/properties/data/properties/restaurants/items/properties/areaName",
								"type": "string",
								"title": "The Areaname Schema.",
								"description": "An explanation about the purpose of this instance.",
								"default": ""
							},
							"costForTwo": {
								"id": "/properties/data/properties/restaurants/items/properties/costForTwo",
								"type": "integer",
								"title": "The Costfortwo Schema.",
								"description": "An explanation about the purpose of this instance.",
								"default": 0
							},
							"cuisines": {
								"id": "/properties/data/properties/restaurants/items/properties/cuisines",
								"type": "array",
								"items": {
									"id": "/properties/data/properties/restaurants/items/properties/cuisines/items",
									"type": "string",
									"title": "The 0 Schema.",
									"description": "An explanation about the purpose of this instance.",
									"default": ""
								}
							},
							"avgRating": {
								"id": "/properties/data/properties/restaurants/items/properties/avgRating",
								"type": "number",
								"title": "The Avgrating Schema.",
								"description": "An explanation about the purpose of this instance.",
								"default": 0
							},
							"ratingsCount": {
								"id": "/properties/data/properties/restaurants/items/properties/ratingsCount",
								"type": "integer",
								"title": "The Ratingscount Schema.",
								"description": "An explanation about the purpose of this instance.",
								"default": 0
							},
							"veg": {
								"id": "/properties/data/properties/restaurants/items/properties/veg",
								"type": "boolean",
								"title": "The Veg Schema.",
								"description": "An explanation about the purpose of this instance.",
								"default": false
							},
							"favourite": {
								"id": "/properties/data/properties/restaurants/items/properties/favourite",
								"type": "boolean",
								"title": "The Favourite Schema.",
								"description": "An explanation about the purpose of this instance.",
								"default": false
							},
							"tags": {
								"id": "/properties/data/properties/restaurants/items/properties/tags",
								"type": "null",
								"title": "The Tags Schema.",
								"description": "An explanation about the purpose of this instance.",
								"default": null
							},
							"dynamicTags": {
								"id": "/properties/data/properties/restaurants/items/properties/dynamicTags",
								"type": "null",
								"title": "The Dynamictags Schema.",
								"description": "An explanation about the purpose of this instance.",
								"default": null
							},
							"feeDetails": {
								"id": "/properties/data/properties/restaurants/items/properties/feeDetails",
								"type": "null",
								"title": "The Feedetails Schema.",
								"description": "An explanation about the purpose of this instance.",
								"default": null
							},
							"isNew": {
								"id": "/properties/data/properties/restaurants/items/properties/isNew",
								"type": "boolean",
								"title": "The Isnew Schema.",
								"description": "An explanation about the purpose of this instance.",
								"default": false
							},
							"tradeDiscountInfo": {
								"id": "/properties/data/properties/restaurants/items/properties/tradeDiscountInfo",
								"type": "null",
								"title": "The Tradediscountinfo Schema.",
								"description": "An explanation about the purpose of this instance.",
								"default": null
							},
							"parentId": {
								"id": "/properties/data/properties/restaurants/items/properties/parentId",
								"type": ["integer", "null"],
								"title": "The Parentid Schema.",
								"description": "An explanation about the purpose of this instance.",
								"default": 0
							},
							"partnerInfo": {
								"id": "/properties/data/properties/restaurants/items/properties/partnerInfo",
								"type": "null",
								"title": "The Partnerinfo Schema.",
								"description": "An explanation about the purpose of this instance.",
								"default": null
							},
							"paymentInfo": {
								"id": "/properties/data/properties/restaurants/items/properties/paymentInfo",
								"type": "object",
								"properties": {
									"codOnly": {
										"id": "/properties/data/properties/restaurants/items/properties/paymentInfo/properties/codOnly",
										"type": "boolean",
										"title": "The Codonly Schema.",
										"description": "An explanation about the purpose of this instance.",
										"default": false
									}
								}
							},
							"exclusive": {
								"id": "/properties/data/properties/restaurants/items/properties/exclusive",
								"type": "boolean",
								"title": "The Exclusive Schema.",
								"description": "An explanation about the purpose of this instance.",
								"default": false
							},
							"city": {
								"id": "/properties/data/properties/restaurants/items/properties/city",
								"type": "object",
								"properties": {
									"cityEntity": {
										"id": "/properties/data/properties/restaurants/items/properties/city/properties/cityEntity",
										"type": "object",
										"properties": {
											"id": {
												"id": "/properties/data/properties/restaurants/items/properties/city/properties/cityEntity/properties/id",
												"type": "integer",
												"title": "The Id Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": 0
											},
											"name": {
												"id": "/properties/data/properties/restaurants/items/properties/city/properties/cityEntity/properties/name",
												"type": "string",
												"title": "The Name Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": ""
											},
											"openTime": {
												"id": "/properties/data/properties/restaurants/items/properties/city/properties/cityEntity/properties/openTime",
												"type": "integer",
												"title": "The Opentime Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": 0
											},
											"closeTime": {
												"id": "/properties/data/properties/restaurants/items/properties/city/properties/cityEntity/properties/closeTime",
												"type": "integer",
												"title": "The Closetime Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": 0
											},
											"enabled": {
												"id": "/properties/data/properties/restaurants/items/properties/city/properties/cityEntity/properties/enabled",
												"type": "integer",
												"title": "The Enabled Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": 0
											},
											"bannerMessage": {
												"id": "/properties/data/properties/restaurants/items/properties/city/properties/cityEntity/properties/bannerMessage",
												"type": "string",
												"title": "The Bannermessage Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": ""
											},
											"maxDeliveryTime": {
												"id": "/properties/data/properties/restaurants/items/properties/city/properties/cityEntity/properties/maxDeliveryTime",
												"type": "integer",
												"title": "The Maxdeliverytime Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": 0
											},
											"minDeliveryTime": {
												"id": "/properties/data/properties/restaurants/items/properties/city/properties/cityEntity/properties/minDeliveryTime",
												"type": "integer",
												"title": "The Mindeliverytime Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": 0
											},
											"slug": {
												"id": "/properties/data/properties/restaurants/items/properties/city/properties/cityEntity/properties/slug",
												"type": "string",
												"title": "The Slug Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": ""
											},
											"isOpen": {
												"id": "/properties/data/properties/restaurants/items/properties/city/properties/cityEntity/properties/isOpen",
												"type": "integer",
												"title": "The Isopen Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": 0
											},
											"dbHelpline": {
												"id": "/properties/data/properties/restaurants/items/properties/city/properties/cityEntity/properties/dbHelpline",
												"type": "string",
												"title": "The Dbhelpline Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": ""
											},
											"showRatings": {
												"id": "/properties/data/properties/restaurants/items/properties/city/properties/cityEntity/properties/showRatings",
												"type": "integer",
												"title": "The Showratings Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": 0
											},
											"deliveryChargeThreshold": {
												"id": "/properties/data/properties/restaurants/items/properties/city/properties/cityEntity/properties/deliveryChargeThreshold",
												"type": "integer",
												"title": "The Deliverychargethreshold Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": 0
											},
											"northEastLatLng": {
												"id": "/properties/data/properties/restaurants/items/properties/city/properties/cityEntity/properties/northEastLatLng",
												"type": "string",
												"title": "The Northeastlatlng Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": ""
											},
											"southWestLatLng": {
												"id": "/properties/data/properties/restaurants/items/properties/city/properties/cityEntity/properties/southWestLatLng",
												"type": "string",
												"title": "The Southwestlatlng Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": ""
											},
											"restaurantHelpline": {
												"id": "/properties/data/properties/restaurants/items/properties/city/properties/cityEntity/properties/restaurantHelpline",
												"type": "string",
												"title": "The Restauranthelpline Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": ""
											},
											"customerCareNumber": {
												"id": "/properties/data/properties/restaurants/items/properties/city/properties/cityEntity/properties/customerCareNumber",
												"type": "string",
												"title": "The Customercarenumber Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": ""
											},
											"deliveryRadius": {
												"id": "/properties/data/properties/restaurants/items/properties/city/properties/cityEntity/properties/deliveryRadius",
												"type": "number",
												"title": "The Deliveryradius Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": ""
											},
											"state": {
												"id": "/properties/data/properties/restaurants/items/properties/city/properties/cityEntity/properties/state",
												"type": "string",
												"title": "The State Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": ""
											},
											"compulsoryDeliveryCharge": {
												"id": "/properties/data/properties/restaurants/items/properties/city/properties/cityEntity/properties/compulsoryDeliveryCharge",
												"type": "number",
												"title": "The Compulsorydeliverycharge Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": ""
											},
											"capOnCodRestriction": {
												"id": "/properties/data/properties/restaurants/items/properties/city/properties/cityEntity/properties/capOnCodRestriction",
												"type": "integer",
												"title": "The Caponcodrestriction Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": 0
											},
											"capOnCodMinValue": {
												"id": "/properties/data/properties/restaurants/items/properties/city/properties/cityEntity/properties/capOnCodMinValue",
												"type": "number",
												"title": "The Caponcodminvalue Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": ""
											},
											"capOnCodMessage": {
												"id": "/properties/data/properties/restaurants/items/properties/city/properties/cityEntity/properties/capOnCodMessage",
												"type": "string",
												"title": "The Caponcodmessage Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": ""
											},
											"restaurantSlaFeatureGate": {
												"id": "/properties/data/properties/restaurants/items/properties/city/properties/cityEntity/properties/restaurantSlaFeatureGate",
												"type": "integer",
												"title": "The Restaurantslafeaturegate Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": 0
											},
											"globalFlatFeeApplicable": {
												"id": "/properties/data/properties/restaurants/items/properties/city/properties/cityEntity/properties/globalFlatFeeApplicable",
												"type": "integer",
												"title": "The Globalflatfeeapplicable Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": 0
											},
											"cartSlaFeatureGate": {
												"id": "/properties/data/properties/restaurants/items/properties/city/properties/cityEntity/properties/cartSlaFeatureGate",
												"type": "integer",
												"title": "The Cartslafeaturegate Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": 0
											},
											"assuredMaxOrderValue": {
												"id": "/properties/data/properties/restaurants/items/properties/city/properties/cityEntity/properties/assuredMaxOrderValue",
												"type": "integer",
												"title": "The Assuredmaxordervalue Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": 0
											},
											"assuredEnabled": {
												"id": "/properties/data/properties/restaurants/items/properties/city/properties/cityEntity/properties/assuredEnabled",
												"type": "boolean",
												"title": "The Assuredenabled Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": false
											},
											"longDistanceSolrRadius": {
												"id": "/properties/data/properties/restaurants/items/properties/city/properties/cityEntity/properties/longDistanceSolrRadius",
												"type": "number",
												"title": "The Longdistancesolrradius Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": 0
											}
										}
									},
									"timeSlot": {
										"id": "/properties/data/properties/restaurants/items/properties/city/properties/timeSlot",
										"type": "object",
										"properties": {
											"openHour": {
												"id": "/properties/data/properties/restaurants/items/properties/city/properties/timeSlot/properties/openHour",
												"type": "integer",
												"title": "The Openhour Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": 0
											},
											"closeHour": {
												"id": "/properties/data/properties/restaurants/items/properties/city/properties/timeSlot/properties/closeHour",
												"type": "integer",
												"title": "The Closehour Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": 0
											}
										}
									}
								}
							},
							"area": {
								"id": "/properties/data/properties/restaurants/items/properties/area",
								"type": "object",
								"properties": {
									"areaEntity": {
										"id": "/properties/data/properties/restaurants/items/properties/area/properties/areaEntity",
										"type": "object",
										"properties": {
											"id": {
												"id": "/properties/data/properties/restaurants/items/properties/area/properties/areaEntity/properties/id",
												"type": "integer",
												"title": "The Id Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": 0
											},
											"name": {
												"id": "/properties/data/properties/restaurants/items/properties/area/properties/areaEntity/properties/name",
												"type": "string",
												"title": "The Name Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": ""
											},
											"slug": {
												"id": "/properties/data/properties/restaurants/items/properties/area/properties/areaEntity/properties/slug",
												"type": "string",
												"title": "The Slug Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": ""
											},
											"cityId": {
												"id": "/properties/data/properties/restaurants/items/properties/area/properties/areaEntity/properties/cityId",
												"type": "integer",
												"title": "The Cityid Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": 0
											},
											"enabled": {
												"id": "/properties/data/properties/restaurants/items/properties/area/properties/areaEntity/properties/enabled",
												"type": "integer",
												"title": "The Enabled Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": 0
											},
											"openTime": {
												"id": "/properties/data/properties/restaurants/items/properties/area/properties/areaEntity/properties/openTime",
												"type": "integer",
												"title": "The Opentime Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": 0
											},
											"closeTime": {
												"id": "/properties/data/properties/restaurants/items/properties/area/properties/areaEntity/properties/closeTime",
												"type": "integer",
												"title": "The Closetime Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": 0
											},
											"isOpen": {
												"id": "/properties/data/properties/restaurants/items/properties/area/properties/areaEntity/properties/isOpen",
												"type": "integer",
												"title": "The Isopen Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": 0
											},
											"bannerMessage": {
												"id": "/properties/data/properties/restaurants/items/properties/area/properties/areaEntity/properties/bannerMessage",
												"type": ["string","null"],
												"title": "The Bannermessage Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": ""
											},
											"beefUpMinutes": {
												"id": "/properties/data/properties/restaurants/items/properties/area/properties/areaEntity/properties/beefUpMinutes",
												"type": "integer",
												"title": "The Beefupminutes Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": 0
											},
											"backInTimer": {
												"id": "/properties/data/properties/restaurants/items/properties/area/properties/areaEntity/properties/backInTimer",
												"type": "integer",
												"title": "The Backintimer Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": 0
											},
											"lastMileCap": {
												"id": "/properties/data/properties/restaurants/items/properties/area/properties/areaEntity/properties/lastMileCap",
												"type": "number",
												"title": "The Lastmilecap Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": ""
											},
											"deAreaClosedMultiplier": {
												"id": "/properties/data/properties/restaurants/items/properties/area/properties/areaEntity/properties/deAreaClosedMultiplier",
												"type": "number",
												"title": "The Deareaclosedmultiplier Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": 0
											},
											"deAreaOpenMultiplier": {
												"id": "/properties/data/properties/restaurants/items/properties/area/properties/areaEntity/properties/deAreaOpenMultiplier",
												"type": "number",
												"title": "The Deareaopenmultiplier Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": 0
											},
											"postalCode": {
												"id": "/properties/data/properties/restaurants/items/properties/area/properties/areaEntity/properties/postalCode",
												"type": "integer",
												"title": "The Postalcode Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": 0
											},
											"surgeFee": {
												"id": "/properties/data/properties/restaurants/items/properties/area/properties/areaEntity/properties/surgeFee",
												"type": "integer",
												"title": "The Surgefee Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": 0
											},
											"osmMultiplier": {
												"id": "/properties/data/properties/restaurants/items/properties/area/properties/areaEntity/properties/osmMultiplier",
												"type": "number",
												"title": "The Osmmultiplier Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": ""
											},
											"cancellationFeeExpression": {
												"id": "/properties/data/properties/restaurants/items/properties/area/properties/areaEntity/properties/cancellationFeeExpression",
												"type": "string",
												"title": "The Cancellationfeeexpression Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": ""
											}
										}
									},
									"timeSlot": {
										"id": "/properties/data/properties/restaurants/items/properties/area/properties/timeSlot",
										"type": "object",
										"properties": {
											"openHour": {
												"id": "/properties/data/properties/restaurants/items/properties/area/properties/timeSlot/properties/openHour",
												"type": "integer",
												"title": "The Openhour Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": 0
											},
											"closeHour": {
												"id": "/properties/data/properties/restaurants/items/properties/area/properties/timeSlot/properties/closeHour",
												"type": "integer",
												"title": "The Closehour Schema.",
												"description": "An explanation about the purpose of this instance.",
												"default": 0
											}
										}
									}
								}
							},
							"sla": {
								"id": "/properties/data/properties/restaurants/items/properties/sla",
								"type": "null",
								"title": "The Sla Schema.",
								"description": "An explanation about the purpose of this instance.",
								"default": null
							},
							"availability": {
								"id": "/properties/data/properties/restaurants/items/properties/availability",
								"type": "null",
								"title": "The Availability Schema.",
								"description": "An explanation about the purpose of this instance.",
								"default": null
							},
							"select": {
								"id": "/properties/data/properties/restaurants/items/properties/select",
								"type": "boolean",
								"title": "The Select Schema.",
								"description": "An explanation about the purpose of this instance.",
								"default": false
							}
						}
					}
				},
				"areaName": {
					"id": "/properties/data/properties/areaName",
					"type": "string",
					"title": "The Areaname Schema.",
					"description": "An explanation about the purpose of this instance.",
					"default": ""
				},
				"cityName": {
					"id": "/properties/data/properties/cityName",
					"type": "string",
					"title": "The Cityname Schema.",
					"description": "An explanation about the purpose of this instance.",
					"default": ""
				}
			}
		},
		"tid": {
			"id": "/properties/tid",
			"type": "string",
			"title": "The Tid Schema.",
			"description": "An explanation about the purpose of this instance.",
			"default": ""
		},
		"sid": {
			"id": "/properties/sid",
			"type": "string",
			"title": "The Sid Schema.",
			"description": "An explanation about the purpose of this instance.",
			"default": ""
		},
		"deviceId": {
			"id": "/properties/deviceId",
			"type": "string",
			"title": "The Deviceid Schema.",
			"description": "An explanation about the purpose of this instance.",
			"default": ""
		}
	},
	"required": [
		"statusCode",
		"statusMessage",
		"data",
		"tid",
		"sid",
		"deviceId"
	]
}