{
  "definitions": {},
  "$schema": "http://json-schema.org/draft-07/schema#",
  "$id": "http://example.com/root.json",
  "type": "object",
  "title": "The Root Schema",
  "required": [
    "statusCode",
    "statusMessage",
    "type",
    "userDetails"
  ],
  "properties": {
    "statusCode": {
      "$id": "#/properties/statusCode",
      "type": "integer",
      "title": "The Statuscode Schema",
      "default": 0,
      "examples": [
        0
      ]
    },
    "statusMessage": {
      "$id": "#/properties/statusMessage",
      "type": "string",
      "title": "The Statusmessage Schema",
      "default": "",
      "examples": [
        "User Details Fetched"
      ],
      "pattern": "^(.*)$"
    },
    "type": {
      "$id": "#/properties/type",
      "type": "string",
      "title": "The Type Schema",
      "default": "",
      "examples": [
        "user"
      ],
      "pattern": "^(.*)$"
    },
    "userDetails": {
      "$id": "#/properties/userDetails",
      "type": "array",
      "title": "The Userdetails Schema",
      "items": {
        "$id": "#/properties/userDetails/items",
        "type": "object",
        "title": "The Items Schema",
        "required": [
          "id",
          "user_id",
          "password",
          "mobile_no",
          "name",
          "created_by",
          "updated_by",
          "email",
          "is_active",
          "registration_complete",
          "is_editable",
          "role_id",
          "activation_date",
          "deactivation_date",
          "created_at",
          "updated_at",
          "user_restaurant_map",
          "restaurantArray"
        ],
        "properties": {
          "id": {
            "$id": "#/properties/userDetails/items/properties/id",
            "type": "integer",
            "title": "The Id Schema",
            "default": 0,
            "examples": [
              1
            ]
          },
          "user_id": {
            "$id": "#/properties/userDetails/items/properties/user_id",
            "type": "integer",
            "title": "The User_id Schema",
            "default": 0,
            "examples": [
              9990
            ]
          },
          "password": {
            "$id": "#/properties/userDetails/items/properties/password",
            "type": "string",
            "title": "The Password Schema",
            "default": "",
            "examples": [
              "$2a$10$PoI62uP02/E/eVfLHOKOv.mBp.LZJf.BpDsbNi.qlPhygCD5UFX1."
            ],
            "pattern": "^(.*)$"
          },
          "mobile_no": {
            "$id": "#/properties/userDetails/items/properties/mobile_no",
            "type": ["string","null"],
            "title": "The Mobile_no Schema",
            "default": "",
            "examples": [
              "qwertyqwqe"
            ],
            "pattern": "^(.*)$"
          },
          "name": {
            "$id": "#/properties/userDetails/items/properties/name",
            "type": "string",
            "title": "The Name Schema",
            "default": "",
            "examples": [
              "abhishek"
            ],
            "pattern": "^(.*)$"
          },
          "created_by": {
            "$id": "#/properties/userDetails/items/properties/created_by",
            "type": "string",
            "title": "The Created_by Schema",
            "default": "",
            "examples": [
              "abhishek"
            ],
            "pattern": "^(.*)$"
          },
          "updated_by": {
            "$id": "#/properties/userDetails/items/properties/updated_by",
            "type": "string",
            "title": "The Updated_by Schema",
            "default": "",
            "examples": [
              "Admin"
            ],
            "pattern": "^(.*)$"
          },
          "email": {
            "$id": "#/properties/userDetails/items/properties/email",
            "type": "string",
            "title": "The Email Schema",
            "default": "",
            "examples": [
              "rachana.pathak@swiggy.in2"
            ],
            "pattern": "^(.*)$"
          },
          "is_active": {
            "$id": "#/properties/userDetails/items/properties/is_active",
            "type": "boolean",
            "title": "The Is_active Schema",
            "default": false,
            "examples": [
              true
            ]
          },
          "is_primary": {
            "$id": "#/properties/userDetails/items/properties/is_primary",
            "type": "boolean",
            "title": "The Is_primary Schema",
            "default": false,
            "examples": [
              false
            ]
          },
          "registration_complete": {
            "$id": "#/properties/userDetails/items/properties/registration_complete",
            "type": "boolean",
            "title": "The Registration_complete Schema",
            "default": false,
            "examples": [
              true
            ]
          },
          "is_editable": {
            "$id": "#/properties/userDetails/items/properties/is_editable",
            "type": "boolean",
            "title": "The Is_editable Schema",
            "default": false,
            "examples": [
              false
            ]
          },
          "role_id": {
            "$id": "#/properties/userDetails/items/properties/role_id",
            "type": "integer",
            "title": "The Role_id Schema",
            "default": 0,
            "examples": [
              3
            ]
          },
          "activation_date": {
            "$id": "#/properties/userDetails/items/properties/activation_date",
            "type": "string",
            "title": "The Activation_date Schema",
            "default": "",
            "examples": [
              "2018-08-21T10:35:04.000Z"
            ],
            "pattern": "^(.*)$"
          },
          "deactivation_date": {
            "$id": "#/properties/userDetails/items/properties/deactivation_date",
            "type": "string",
            "title": "The Deactivation_date Schema",
            "default": "",
            "examples": [
              "2017-05-19T10:17:47.000Z"
            ],
            "pattern": "^(.*)$"
          },
          "created_at": {
            "$id": "#/properties/userDetails/items/properties/created_at",
            "type": "string",
            "title": "The Created_at Schema",
            "default": "",
            "examples": [
              "2016-08-31T13:00:00.000Z"
            ],
            "pattern": "^(.*)$"
          },
          "updated_at": {
            "$id": "#/properties/userDetails/items/properties/updated_at",
            "type": "string",
            "title": "The Updated_at Schema",
            "default": "",
            "examples": [
              "2018-08-21T10:35:04.000Z"
            ],
            "pattern": "^(.*)$"
          },
          "user_restaurant_map": {
            "$id": "#/properties/userDetails/items/properties/user_restaurant_map",
            "type": "object",
            "title": "The User_restaurant_map Schema",
            "required": [
              "created_at",
              "updated_at",
              "restaurant_id",
              "user_id"
            ],
            "properties": {
              "created_at": {
                "$id": "#/properties/userDetails/items/properties/user_restaurant_map/properties/created_at",
                "type": "string",
                "title": "The Created_at Schema",
                "default": "",
                "examples": [
                  "2018-01-25T08:22:22.000Z"
                ],
                "pattern": "^(.*)$"
              },
              "updated_at": {
                "$id": "#/properties/userDetails/items/properties/user_restaurant_map/properties/updated_at",
                "type": "string",
                "title": "The Updated_at Schema",
                "default": "",
                "examples": [
                  "2018-01-25T08:22:22.000Z"
                ],
                "pattern": "^(.*)$"
              },
              "restaurant_id": {
                "$id": "#/properties/userDetails/items/properties/user_restaurant_map/properties/restaurant_id",
                "type": "integer",
                "title": "The Restaurant_id Schema",
                "default": 0,
                "examples": [
                  5271
                ]
              },
              "user_id": {
                "$id": "#/properties/userDetails/items/properties/user_restaurant_map/properties/user_id",
                "type": "integer",
                "title": "The User_id Schema",
                "default": 0,
                "examples": [
                  1
                ]
              }
            }
          },
          "restaurantArray": {
            "$id": "#/properties/userDetails/items/properties/restaurantArray",
            "type": "array",
            "title": "The Restaurantarray Schema",
            "items": {
              "$id": "#/properties/userDetails/items/properties/restaurantArray/items",
              "type": "integer",
              "title": "The Items Schema",
              "default": 0,
              "examples": [
                5271
              ]
            }
          }
        }
      }
    }
  }
}